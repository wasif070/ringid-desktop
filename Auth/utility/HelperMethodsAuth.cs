﻿using System;
using System.Diagnostics;
using System.IO;
using Auth.AppInterfaces;
using Auth.Parser;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;

namespace Auth.utility
{
    public class HelperMethodsAuth
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HelperMethodsAuth).Name);

        public static string GetUrlEncoded(string url)
        {
            return System.Web.HttpUtility.UrlEncode(url);
        }
        public static void StartAuthAsyncCommunication(IAuthSignalHandler iAuthSignalHandler)
        {
            AuthAsyncCommunication.Instance.StartService(iAuthSignalHandler);
        }
        public static void StopAuthAsyncCommunication()
        {
            AuthAsyncCommunication.Instance.StopService();
        }
        public static void CreateDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    return;
                }

                DirectoryInfo di = Directory.CreateDirectory(path);
                if (DefaultSettings.DEBUG)
                {
                    log.Info("'" + path + "' directory was created successfully.");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Creating '" + path + "' directory.", ex);
            }
        }

        public static void GoToSite(string url, bool useIE = false)
        {
            if (useIE) { Process.Start("IExplore.exe", url); return; }
            Process myProcess = new Process();
            try
            {
                // true is the default, but it is important not to set it to false
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = url;
                myProcess.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: GoToSite()." + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

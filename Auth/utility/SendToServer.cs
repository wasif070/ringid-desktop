﻿using Auth.Parser;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using Models.Entity;
using Newtonsoft.Json;
using Models.Utility;
namespace Auth.utility
{
    public class SendToServer
    {
        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(SendToServer).Name);
        static Random random = new Random();
        public static int PACKET_INCREMENT_VALUE = 0;
        #endregion "Private Fields"

        public static String GetRanDomPacketID()
        {
            String key;
            if (PACKET_INCREMENT_VALUE > 9998)
            {
                PACKET_INCREMENT_VALUE = 0;
            }
            else
            {
                PACKET_INCREMENT_VALUE++;
            }
            key = DefaultSettings.LOGIN_RING_ID.ToString() + PACKET_INCREMENT_VALUE.ToString() + ModelUtility.CurrentTimeMillis().ToString();

            return key;
        }

        public static String GetRanDomPacketIDBeforeLogin(string tempID)
        {
            String key;
            if (PACKET_INCREMENT_VALUE > 9998)
            {
                PACKET_INCREMENT_VALUE = 0;
            }
            else
            {
                PACKET_INCREMENT_VALUE++;
            }
            key = tempID + PACKET_INCREMENT_VALUE.ToString() + ModelUtility.CurrentTimeMillis().ToString();

            return key;
        }

        public static String GetRanDomPacketID(bool call)
        {
            String key;
            long foobar = random.Next(100);
            long time = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            key = foobar.ToString() + time;
            return key;
        }

        public static void SendConfirmationPak(string packetIDfromServer)
        {
            String pakId = DefaultSettings.LOGIN_RING_ID.ToString() + AppConstants.TYPE_SIGN_OUT.ToString() + DateTime.Now.Millisecond.ToString();
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_CONFIRMATION;
            pakToSend[JsonKeys.PacketIdFromServer] = packetIDfromServer;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

            string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_CONFIRMATION, data);
        }

        public static void SendByteConfirmationPacketIdFromServer(long packetID, int action)
        {
            if (DefaultSettings.LOGIN_SESSIONID != null)
            {
                byte[] session_bytes = System.Text.Encoding.UTF8.GetBytes(DefaultSettings.LOGIN_SESSIONID);
                byte[] send_bytes = new byte[10 + session_bytes.Length];
                int index = 0;

                send_bytes[index++] = (byte)(action >> 8);
                send_bytes[index++] = (byte)(action);

                send_bytes[index++] = (byte)(packetID >> 56);
                send_bytes[index++] = (byte)(packetID >> 48);
                send_bytes[index++] = (byte)(packetID >> 40);
                send_bytes[index++] = (byte)(packetID >> 32);
                send_bytes[index++] = (byte)(packetID >> 24);
                send_bytes[index++] = (byte)(packetID >> 16);
                send_bytes[index++] = (byte)(packetID >> 8);
                send_bytes[index++] = (byte)(packetID);

                Array.Copy(session_bytes, 0, send_bytes, index, session_bytes.Length);
                SendDatagramPacket(AppConstants.REQUEST_TYPE_CONFIRMATION, AppConstants.SINGLE_PACKET, send_bytes);
            }
            else
            {
                log.Error("sendByteConfirmationPacketIdFromServer ==> null session");
            }
        }

        public static void SendNormalPacket(int requestType, string obj)
        {
            try
            {
                SendDatagramPacket(requestType, AppConstants.SINGLE_PACKET, System.Text.Encoding.UTF8.GetBytes(obj));
#if AUTH_LOG
                log.Info("Send packet to" + ServerAndPortSettings.AUTH_SERVER_IP + ":" + ServerAndPortSettings.COMMUNICATION_PORT + "==>" + obj);
#endif
            }
            catch (Exception e)
            {
                log.Error("SendNormalPacket Ex-->" + e.Message + "\n" + e.StackTrace);
            }
        }

        //public static void SendFriendSearchReqest(int catagory, string searchItem)
        //{
        //    try
        //    {
        //        JObject packet = new JObject();
        //        packet[JsonKeys.Action] = AppConstants.TYPE_CONTACT_LIST;
        //        String pakId = SendToServer.GetRanDomPacketID();
        //        packet[JsonKeys.PacketId] = pakId;
        //        packet[JsonKeys.Action] = AppConstants.TYPE_CONTACT_SEARCH;
        //        packet[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
        //        packet[JsonKeys.SearchParam] = searchItem;
        //        packet[JsonKeys.SearchCategory] = catagory;
        //        string data = JsonConvert.SerializeObject(packet, Formatting.None);
        //        SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("SendNormalPacket Ex-->" + e.Message + "\n" + e.StackTrace);
        //    }
        //}

        public static void SendBrokenPacket(int isBroken, Dictionary<String, byte[]> packets, int requestType = AppConstants.REQUEST_TYPE_UPDATE)
        {
            foreach (byte[] data in packets.Values)
            {
                if (isBroken == AppConstants.BROKEN_PACKET)
                {
                    if (requestType == AppConstants.REQUEST_TYPE_CHAT)
                    {
                        SendDatagramPacket(requestType, AppConstants.BROKEN_PACKET, data);
                    }
                    else
                    {
                        SendDatagramPacket(data);
                    }
                }
                else
                {
                    SendDatagramPacket(requestType, AppConstants.SINGLE_PACKET, data);
                }
                try
                { 
                    Thread.Sleep(25);
                }
                catch (Exception ex)
                {
                    log.Error("SendBrokenPacket Ex-->" + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        public static void SendDatagramPacket(byte[] data)
        {
            try
            {
                AuthAsyncCommunication.Instance.Send(data);
            }
            catch (Exception e)
            {
                log.Error("SendDatagramPacket Ex-->" + e.Message + "\n" + e.StackTrace);
            }
        }

        public static void SendDatagramPacket(int requestType, int packetType, byte[] data)
        {
            try
            {
                byte[] temp = new byte[data.Length + 2];
                temp[0] = (byte)requestType;
                temp[1] = (byte)packetType;
                Array.Copy(data, 0, temp, 2, data.Length);
                AuthAsyncCommunication.Instance.Send(temp);
            }
            catch (Exception e)
            {
                log.Error("SendDatagramPacket Ex-->" + e.Message + "\n" + e.StackTrace);
            }
        }

        public static bool CheckAllBrokenPacketConfirmation(Dictionary<String, byte[]> packets)
        {
            if (packets.Count > 0)
            {
                List<string> tempList = new List<string>();
                foreach (string packetId in packets.Keys)
                {
                    if (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(packetId))
                    {
                        tempList.Add(packetId);
                    }
                }
                foreach (string item in tempList)
                {
                    packets.Remove(item);
                }
            }
            return packets.Count > 0;
        }

        public static void RemoveAllBrokenPacketConfirmation(IList<String> packetIds)
        {
            foreach (String packetId in packetIds)
            {
                JObject obj;
                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out obj);
            }
        }

        public static Dictionary<String, byte[]> BuildBrokenPacket(string obj, int action, string packetId)
        {
            Dictionary<String, byte[]> packets = new Dictionary<String, byte[]>();
            try
            {
                byte[] data = System.Text.Encoding.UTF8.GetBytes(obj);
                if (data.Length < AppConstants.CLIENT_DATA_SIZE)
                {
                    lock (packets)
                    {
                        packets[packetId] = data;
                    }
                }
                else
                {
                    packets = SendDividedPacket.BuildDevidedPackets(action, data, data.Length);
                }
            }
            catch (Exception e)
            {
                log.Error("BuildBrokenPacket Ex-->" + e.Message + "\n" + e.StackTrace);
            }
            return packets;
        }



        /*Make requests*/
        //public static void SendMutualFreindRequest(long userID)
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.UserIdentity] = userID;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_MUTUAL_FRIENDS, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //    //   SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
        //}

        //public static void SendFreindContactListRequest(long friendUtId, int StartLimit)
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.UserTableId] = friendUtId;
        //    pakToSend[JsonKeys.StartLimit] = StartLimit;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_FRIEND_CONTACT_LIST, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}

        //public static void SendFreindContactListRequest(long friendUtId, long pvtid, int limit)
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.UserTableId] = friendUtId;
        //    pakToSend[JsonKeys.PivotId] = pvtid;
        //    pakToSend[JsonKeys.Limit] = limit;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}

        //<<<<<<< .mine
        //public static void SendAlbumRequest(long friendId, string friendAlbumId, int startLimit)
        //{
        //    JObject pakToSend = new JObject();
        //    int action = AppConstants.TYPE_MY_ALBUM_IMAGES;
        //    if (friendId > 0)
        //    {
        //        action = AppConstants.TYPE_FRIEND_ALBUM_IMAGES;
        //        pakToSend[JsonKeys.FriendId] = friendId;
        //    }
        //    pakToSend[JsonKeys.AlbumId] = friendAlbumId;
        //    pakToSend[JsonKeys.StartLimit] = startLimit;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, action, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}

        //public static void ImageLikersOrImageCommentLikersList(long imageId, long commentId = 0, long startLimit = 0) //Practise Named Parameter 
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.ImageId] = imageId;
        //    pakToSend[JsonKeys.StartLimit] = startLimit;
        //    if (commentId > 0) pakToSend[JsonKeys.CommentId] = commentId;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, commentId > 0 ? AppConstants.TYPE_IMAGE_COMMENT_LIKES : AppConstants.TYPE_LIKES_FOR_IMAGE, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}

        //public static void ListMediaLikes(long contentId, long nfId, int st = 0)
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.ContentId] = contentId;
        //    if (nfId > 0) pakToSend[JsonKeys.NewsfeedId] = nfId;
        //    pakToSend[JsonKeys.StartLimit] = st;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_MEDIA_LIKE_LIST, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}
        //=======
        //        public static void SendFreindContactListRequest(long friendUtId, int StartLimit)
        //        {
        //            JObject pakToSend = new JObject();
        //            pakToSend[JsonKeys.UserTableId] = friendUtId;
        //            pakToSend[JsonKeys.StartLimit] = StartLimit;
        //            (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_FRIEND_CONTACT_LIST, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //        }
        //        public static void SendFreindContactListRequest(long friendUtId, long pvtid, int limit)
        //        {
        //            JObject pakToSend = new JObject();
        //            pakToSend[JsonKeys.UserTableId] = friendUtId;
        //            pakToSend[JsonKeys.PivotId] = pvtid;
        //            pakToSend[JsonKeys.Limit] = limit;
        //            (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //        }
        //        public static void SendAlbumRequest(long friendId, string friendAlbumId, int startLimit)
        //        {
        //            JObject pakToSend = new JObject();
        //            int action = AppConstants.TYPE_MY_ALBUM_IMAGES;
        //            if (friendId > 0)
        //            {
        //                action = AppConstants.TYPE_FRIEND_ALBUM_IMAGES;
        //                pakToSend[JsonKeys.FriendId] = friendId;
        //            }
        //            pakToSend[JsonKeys.AlbumId] = friendAlbumId;
        //            pakToSend[JsonKeys.StartLimit] = startLimit;
        //            (new Auth.Service.AuthRequestNoResult(pakToSend, action, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //        }
        //        public static void BreakingMediaCloudFeeds()
        //        {
        //            JObject pakToSend = new JObject();
        //            (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_MEDIA_PAGE_TRENDING_FEED, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //        }
        //>>>>>>> .r1628

        //public static void ListNewsPortalInfoCategories(long utId, int ProfileType)
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.UserTableId] = utId;
        //    pakToSend[JsonKeys.ProfileType] = ProfileType;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_NEWSPORTAL_CATEGORIES_LIST, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}

        //public static void ListMediaCommentLikes(long contentId, long commentId, long nfId, int st = 0)
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.ContentId] = contentId;
        //    pakToSend[JsonKeys.CommentId] = commentId;
        //    pakToSend[JsonKeys.StartLimit] = st;
        //    if (nfId > 0) pakToSend[JsonKeys.NewsfeedId] = nfId;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_MEDIACOMMENT_LIKE_LIST, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}

        //public static void ListMediaAlbumsOfaUser(long utId, int mediaType)
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.UserTableId] = utId;
        //    pakToSend[JsonKeys.MediaType] = mediaType;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_MEDIA_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}

        //public static void MediaFullSearchByType(string searchSgtn, int searchtype, int scl = 1, long pvtid = 0)//later pvtid,scl has to be parameterized
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.SearchParam] = searchSgtn;
        //    pakToSend[JsonKeys.MediaSuggestionType] = searchtype;
        //    pakToSend[JsonKeys.Scroll] = scl;//1;//for now
        //    pakToSend[JsonKeys.PivotId] = pvtid;//0;//for now
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}
        //public static void ListHashTagContents(long hashTagId, int scl = 1, long pvtid = 0)//later pvtid,scl has to be parameterized
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.HashTagId] = hashTagId;
        //    pakToSend[JsonKeys.Scroll] = scl;//1;//for now
        //    pakToSend[JsonKeys.PivotId] = pvtid;//0;//for now
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}
        //public static void WorkEducationSkillRequest(long? Utid)
        //{
        //    JObject pakToSend = new JObject();
        //    if (Utid != null)
        //        pakToSend[JsonKeys.UserTableId] = Utid;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_LIST_WORKS_EDUCATIONS_SKILLS, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}

        //public static void SuggestionUsersDetailsRequest(JArray suggestionIdsList)
        //{
        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.ContactUtIdList] = suggestionIdsList;
        //    (new Auth.Service.AuthRequestNoResult(pakToSend, AppConstants.TYPE_USERS_DETAILS, AppConstants.REQUEST_TYPE_REQUEST)).StartThread();
        //}
        //public static JObject AddWorkInfo(WorkDTO work)
        //{
        //    JObject workObject = new JObject();
        //    workObject[JsonKeys.CompanyName] = work.CompanyName;
        //    if (work.Position != null) workObject[JsonKeys.Position] = work.Position;
        //    if (work.City != null) workObject[JsonKeys.City] = work.City;
        //    if (work.Description != null) workObject[JsonKeys.Description] = work.Description;
        //    workObject[JsonKeys.FromTime] = work.FromTime;
        //    workObject[JsonKeys.ToTime] = work.ToTime;

        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.WorkObj] = workObject;
        //    return pakToSend;
        //}
        //public static JObject UpdateEducation(EducationDTO upDatedEducation)
        //{
        //    JObject educationObject = new JObject();
        //    educationObject[JsonKeys.Id] = upDatedEducation.Id;
        //    educationObject[JsonKeys.SchoolName] = upDatedEducation.SchoolName;
        //    if (upDatedEducation.Description != null) { educationObject[JsonKeys.Description] = upDatedEducation.Description; }
        //    educationObject[JsonKeys.FromTime] = upDatedEducation.FromTime;
        //    educationObject[JsonKeys.ToTime] = upDatedEducation.ToTime;
        //    educationObject[JsonKeys.Graduated] = upDatedEducation.Graduated;
        //    educationObject[JsonKeys.IsSchool] = upDatedEducation.IsSchool;
        //    // if (attfor > 0)
        //    educationObject[JsonKeys.AttendedFor] = upDatedEducation.AttendedFor;
        //    if (upDatedEducation.Degree != null) { educationObject[JsonKeys.Degree] = upDatedEducation.Degree; }
        //    if (upDatedEducation.Concentration != null) { educationObject[JsonKeys.Concentration] = upDatedEducation.Concentration; }

        //    JObject pakToSend = new JObject();
        //    pakToSend[JsonKeys.EducationObj] = educationObject;
        //    return pakToSend;
        //}
    }
}

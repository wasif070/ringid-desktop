<<<<<<< HEAD
﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Auth.utility
{
    public class HttpRequest
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HttpRequest).Name);

        public static string PARAM_LOGIN_TYPE = "lt";
        public static string PARAM_VERSION = "v";
        public static string PARAM_PLATFORM = "pf";
        public static string PARAM_TIME = "t";
        public static string PARAM_RINGID = "ringID";

        public static string PARAM_MOBILE = "mbl";
        public static string PARAM_DIALING_CODE = "mblDc";
        public static string PARAM_FACEBOOK = "fb";
        public static string PARAM_TWITTER = "twtr";
        public static string PARAM_DEVICE_ID = "did";
        public static string PARAM_EMAIL = "el";
        public static string PARAM_APP_TYPE = "apt";
        public static string PARAM_CATEGORY_ID = "categoryId";
        public static string PARAM_DEFAULT = "default";
        public static string PARAM_LIMIT = "lmt";
        public static string PARAM_TYPE = "type";
        public static string PARAM_SUMMARY_AND_LIMIT = "sum&lmt";
        public static string PARAM_COLLECTION_LIMIT = "cLmt";
        public static string PARAM_LIMIT_AND_FROM = "lm&frm";
        public static string PARAM_COLLECTION_ID = "cId";
        public static string PARAM_LANGUAGE = "lng";

        /// <summary>
        ///              {"sucs":true,"ac":"http://devauth.ringid.com/rac/","ims":"http://devimages.ringid.com/","imsres":"http://devimagesres.ringid.com/","stmapi":"http://devsticker.ringid.com/","stmres":"http://devstickerres.ringid.com/","vodapi":"http://devmediacloudapi.ringid.com/","vodres":"http://devmediacloud.ringid.com/","dd":8,"utId":1,"web":"https://dev.ringid.com/"}
        /// </summary>
        /// <returns></returns>


        public static CommunicationServerDTO GetBaseRacURLs()
        {
            string postData = String.Format(PARAM_PLATFORM + "={0}&" + PARAM_VERSION + "={1}&" + PARAM_APP_TYPE + "={2}&" + PARAM_TIME + "={3}", (int)AppConstants.PLATFORM_DESKTOP, Int32.Parse(AppConfig.VERSION_PC), (int)SettingsConstants.APP_TYPE_RINGID_FULL, (long)ModelUtility.CurrentTimeMillis());
            string response = makeWebRequestPOST(ServerAndPortSettings.GetCommunicationServer, postData, WebRequestMethods.Http.Get/*"GET"*/);
            CommunicationServerDTO communicationServerDTO = new CommunicationServerDTO(response);
            return communicationServerDTO;
        }

        public static CommunicationPortsDTO GetNewRingID()
        {
            CommunicationPortsDTO communicationPortsDTO = null;
            if (!string.IsNullOrEmpty(DefaultSettings.DEVICE_UNIQUE_ID))
            {
                string postData = String.Format(PARAM_DEVICE_ID + "={0}&" + PARAM_APP_TYPE + "={1}&" + PARAM_TIME + "={2}", HttpUtility.UrlEncode(DefaultSettings.DEVICE_UNIQUE_ID), (int)SettingsConstants.APP_TYPE_RINGID_FULL, (long)ModelUtility.CurrentTimeMillis());
                string response = makeWebRequestPOST(ServerAndPortSettings.AUTHENTICATION_CONTROLLER + PARAM_RINGID.ToLower(), postData, WebRequestMethods.Http.Post /*"POST"*/);
                communicationPortsDTO = new CommunicationPortsDTO(response);
            }
            else { log.Info("Not found DefaultSettings.DEVICE_UNIQUE_ID==>" + DefaultSettings.DEVICE_UNIQUE_ID); }
            return communicationPortsDTO;
        }

        public static CommunicationPortsDTO GetCommunicationPortsByRingID(string ringId)
        {
            string postData = makePostData(SettingsConstants.RINGID_LOGIN, PARAM_RINGID, ringId);
            CommunicationPortsDTO communicationPortsDTO = getCommunicationPortsDTO(postData);
            return communicationPortsDTO;
        }

        public static CommunicationPortsDTO GetCommunicationPortsByEmail(string email)
        {
            string postData = makePostData(SettingsConstants.EMAIL_LOGIN, PARAM_EMAIL, email);
            CommunicationPortsDTO communicationPortsDTO = getCommunicationPortsDTO(postData);
            return communicationPortsDTO;
        }

        public static CommunicationPortsDTO GetCommunicationPortsByMobile(string mobile, string mblDc)
        {
            string postData = String.Format(PARAM_LOGIN_TYPE + "={0}&" + PARAM_DIALING_CODE + "={1}&" + PARAM_MOBILE + "={2}&" + PARAM_APP_TYPE + "={3}&" + PARAM_TIME + "={4}", (int)SettingsConstants.MOBILE_LOGIN, HttpUtility.UrlEncode(mblDc), HttpUtility.UrlEncode(mobile), (int)SettingsConstants.APP_TYPE_RINGID_FULL, (long)ModelUtility.CurrentTimeMillis());
            CommunicationPortsDTO communicationPortsDTO = getCommunicationPortsDTO(postData);
            return communicationPortsDTO;
        }

        public static CommunicationPortsDTO GetCommunicationPortsSocialMedia(int type, string socialMediaID)
        {
            string urlParam = string.Empty;
            if (type == SettingsConstants.FACEBOOK_LOGIN) urlParam = PARAM_FACEBOOK;
            else if (type == SettingsConstants.TWITTER_LOGIN) urlParam = PARAM_TWITTER;
            string postData = makePostData(type, urlParam, socialMediaID);
            CommunicationPortsDTO communicationPortsDTO = getCommunicationPortsDTO(postData);
            return communicationPortsDTO;
        }

        public static string GetRingIDFromSocialMediaID(string smId, int loginType) //null if rc=2, i.e no such fb/tw id associated or no response
        {
            string postData = (loginType == 4) ? (String.Format(PARAM_FACEBOOK + "={0}&" + PARAM_TIME + "={1}&" + PARAM_LOGIN_TYPE + "=" + SettingsConstants.FACEBOOK_LOGIN, smId, (long)ModelUtility.CurrentTimeMillis())) : (String.Format(PARAM_TWITTER + "={0}&" + PARAM_TIME + "={1}&" + PARAM_LOGIN_TYPE + "=" + SettingsConstants.TWITTER_LOGIN, smId, (long)ModelUtility.CurrentTimeMillis()));
            string response = makeWebRequestPOST(ServerAndPortSettings.GetComportURL, postData, WebRequestMethods.Http.Post);
            if (!string.IsNullOrEmpty(response)) return Models.Utility.HelperMethodsModel.ParseRingIDFromResponse(response);
            else return null;
        }

        public static string GetRingIDFromEmail(string email)
        {
            string postData = String.Format(PARAM_EMAIL + "={0}&" + PARAM_LOGIN_TYPE + "={1}&" + PARAM_TIME + "={2}", HttpUtility.UrlEncode(email), (int)SettingsConstants.EMAIL_LOGIN, (long)ModelUtility.CurrentTimeMillis());
            string response = makeWebRequestPOST(ServerAndPortSettings.GetComportURL, postData, WebRequestMethods.Http.Post /*"POST"*/);
            if (!string.IsNullOrEmpty(response)) return Models.Utility.HelperMethodsModel.ParseRingIDFromResponse(response);
            else return null;
        }

        public static string GetRingIDFromPhone(string mbl, string mbldc)
        {
            string postData = String.Format(PARAM_DIALING_CODE + "={0}&" + PARAM_MOBILE + "={1}&" + PARAM_LOGIN_TYPE + "={2}&" + PARAM_TIME + "={3}", HttpUtility.UrlEncode(mbldc), HttpUtility.UrlEncode(mbl), (int)SettingsConstants.MOBILE_LOGIN, (long)ModelUtility.CurrentTimeMillis());
            string response = makeWebRequestPOST(ServerAndPortSettings.GetComportURL, postData, WebRequestMethods.Http.Post /*"POST"*/);
            if (!string.IsNullOrEmpty(response)) return Models.Utility.HelperMethodsModel.ParseRingIDFromResponse(response);
            else return null;
        }

        public static string GetAccessTokenForDigitsVerification(long rid) //null or INVALID_DATA otherwise
        {
            if (rid > 0)
            {
                string postData = String.Format("uId={0}", rid);
                string response = makeWebRequestPOST(ServerAndPortSettings.GetDigitAccessTokenURL, postData, WebRequestMethods.Http.Post /*"POST"*/);
                return response;
            }
            else return null;
        }

        public static StickerDTO GetNewStickerCategoryIds()
        {
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrlForNewStickerCategoryIDs, null, WebRequestMethods.Http.Get);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                StickerDTO stickerDTO = oSerializer.Deserialize<StickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static StickerDTO GetDefaultStickers()
        {
            string postData = String.Format(PARAM_DEFAULT + "=1&" + PARAM_TIME + "={0}&" + PARAM_PLATFORM + "={1}&" + PARAM_VERSION + "={2}", (long)ModelUtility.CurrentTimeMillis(), (int)AppConstants.PLATFORM_DESKTOP, Int32.Parse(AppConfig.VERSION_PC));
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get/*"GET"*/, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                StickerDTO stickerDTO = oSerializer.Deserialize<StickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetAllStickersForNewAPI(int categoryLimit, int collectionLimit)
        {
            string postData = String.Empty;
            postData = String.Format(PARAM_SUMMARY_AND_LIMIT + "={0}&" + PARAM_COLLECTION_LIMIT + "={1}", categoryLimit, collectionLimit);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetStickerOfParticularType(int startIndex, int categoryLimit, int collectionLimit, int stickerType)
        {
            string postData = String.Empty;
            postData = String.Format(PARAM_LIMIT_AND_FROM + "={0}&" + PARAM_LIMIT + "={1}&" + PARAM_COLLECTION_LIMIT + "={2}&" + PARAM_TYPE + "={3}", startIndex, categoryLimit, collectionLimit, stickerType);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetStickersOfParticularCollection(int startIndex, int categoryLimit, int stickerType, int collectionId)
        {
            string postData = String.Empty;
            postData = String.Format(PARAM_LIMIT_AND_FROM + "={0}&" + PARAM_LIMIT + "={1}&" + PARAM_TYPE + "={2}&" + PARAM_COLLECTION_ID + "={3}", startIndex, categoryLimit, stickerType, collectionId);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetStickersOfParticularLanguage(int startIndex, int categoryLimit, int stickerType, string languageName)
        {
            string postData = String.Empty;
            postData = String.Format(PARAM_LIMIT_AND_FROM + "={0}&" + PARAM_LIMIT + "={1}&" + PARAM_TYPE + "={2}&" + PARAM_LANGUAGE + "={3}", startIndex, categoryLimit, stickerType, languageName);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetMarketStickerImagesByCategoryID(int categoryId)
        {
            string postData = String.Format(PARAM_CATEGORY_ID + "={0}&" + PARAM_TIME + "={1}&" + PARAM_PLATFORM + "={2}&" + PARAM_VERSION + "={3}", categoryId, (long)ModelUtility.CurrentTimeMillis(), (int)AppConstants.PLATFORM_DESKTOP, Int32.Parse(AppConfig.VERSION_PC));
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get/*"GET"*/, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        private static string makePostData(int loginType, string loginParam, string ringIDEmailSocialID)
        {
            return String.Format(PARAM_LOGIN_TYPE + "={0}&" + loginParam + "={1}&" + PARAM_APP_TYPE + "={2}&t={3}", loginType, HttpUtility.UrlEncode(ringIDEmailSocialID), (int)SettingsConstants.APP_TYPE_RINGID_FULL, (long)ModelUtility.CurrentTimeMillis());
        }

        public static void IncreaseDownloadCount(int categoryId)
        {
            JObject obj = new JObject(); // these json keys formation are different.That's why JsonKeys class not used.
            obj["authIP"] = ServerAndPortSettings.AUTH_SERVER_IP;
            obj["authPort"] = ServerAndPortSettings.COMMUNICATION_PORT;
            obj["sID"] = DefaultSettings.LOGIN_SESSIONID;
            obj["utID"] = DefaultSettings.LOGIN_TABLE_ID;
            obj["sCatID"] = categoryId;
            string postData = JsonConvert.SerializeObject(obj, Formatting.None);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrlForIncreaseDownloadCount, postData, WebRequestMethods.Http.Post/*"POST"*/, false, true);
        }

        public static ChatBgDTO GetChatBackgroundImage()
        {
            string postData = String.Format("all=1&t={0}", (long)ModelUtility.CurrentTimeMillis());
            string response = makeWebRequestPOST(ServerAndPortSettings.ChatBackgroundImageServiceUrl, postData, WebRequestMethods.Http.Get/*"GET"*/, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                ChatBgDTO chatBgDto = oSerializer.Deserialize<ChatBgDTO>(response);
                return chatBgDto;
            }
            return null;
        }

        private static string makeWebRequestPOST(string requestUri, string postData, string method, bool appVersion = false, bool isStickerIncreaseDownloadCountRequest = false)
        {
            string responseFromServer = null;
            string reqeustUrl = requestUri + (method.Equals(WebRequestMethods.Http.Get/*"GET"*/) ? "?" + postData : "");
            if (requestUri != null)
            {
                try
                {
                    ServicePointManager.DefaultConnectionLimit = 4;
                    ServicePointManager.Expect100Continue = false;
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(reqeustUrl);
                    if (httpWebRequest != null)
                    {
                        httpWebRequest.Method = method;
                        httpWebRequest.Proxy = null;
                        httpWebRequest.Timeout = 60 * 1000;
                        if (appVersion) httpWebRequest.Headers["x-app-version"] = AppConfig.VERSION_PC;
                        if (method.Equals(WebRequestMethods.Http.Post))
                        {
                            byte[] byteArray = postData != null ? System.Text.Encoding.UTF8.GetBytes(postData) : null;
                            if (isStickerIncreaseDownloadCountRequest) httpWebRequest.ContentType = "application/json";
                            else httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                            if (byteArray != null) httpWebRequest.ContentLength = byteArray.Length;
                            using (Stream dataStream = httpWebRequest.GetRequestStream())
                            {
                                if (byteArray != null) dataStream.Write(byteArray, 0, byteArray.Length);
                                dataStream.Close();
                            }
                        }
                        using (var webResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                        {
                            using (Stream responseStream = webResponse.GetResponseStream())
                            {
                                using (StreamReader reader = new StreamReader(responseStream))
                                {
                                    responseFromServer = reader.ReadToEnd();
                                    reader.Close();
                                }
                                responseStream.Close();
                            }
                            webResponse.Close();
                            httpWebRequest.Abort();
                        }
                    }

#if HTTP_LOG
                log.Info("Reqeuest==> " + reqeustUrl + "  Resoponse==>" + responseFromServer);
#endif
                }
                catch (WebException e) { log.Error(e.StackTrace); }
            }
            return responseFromServer;
        }

        private static CommunicationPortsDTO getCommunicationPortsDTO(string postData)
        {
            string response = makeWebRequestPOST(ServerAndPortSettings.GetComportURL, postData, WebRequestMethods.Http.Post /*"POST"*/);
#if HTTP_LOG
            log.Info("getCommunicationPortsDTO==> " + postData + " response==>" + response);
#endif
            return new CommunicationPortsDTO(response);
        }

        public static bool DownloadImageFile(string uri, string fileName)
        {
            if (File.Exists(fileName)) return false;
            return DownloadFile(uri, fileName);
        }

        public static bool DownloadFile(string uri, string fileName)
        {
            bool flag = false;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add("x-app-version", AppConfig.VERSION_PC);
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    if ((response.StatusCode == HttpStatusCode.OK ||
                    response.StatusCode == HttpStatusCode.Moved ||
                    response.StatusCode == HttpStatusCode.Redirect) &&
                    response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                    {
                        using (Stream inputStream = response.GetResponseStream())
                        using (Stream outputStream = File.OpenWrite(fileName))
                        {
                            byte[] buffer = new byte[4096];
                            int bytesRead;
                            do
                            {
                                bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                                outputStream.Write(buffer, 0, bytesRead);
                            } while (bytesRead != 0);
                        }
                        flag = true;
                    }
                }
            }
            catch (Exception e)
            {
                flag = false;
#if HTTP_LOG
                log.Error("DownloadImageFile ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
#endif
            }
            finally
            {
                if (httpWebRequest != null)
                {
                    httpWebRequest.Abort();
                    httpWebRequest = null;
                }
            }
            return flag;
        }

        public static bool DownloadImageWithValidationCheck(string uri, string fileName)
        {
            if (File.Exists(fileName)) return false;
            return DownloadFile(uri, fileName);
        }
    }
}
=======
﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Auth.utility
{
    public class HttpRequest
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(HttpRequest).Name);

        public static string PARAM_LOGIN_TYPE = "lt";
        public static string PARAM_VERSION = "v";
        public static string PARAM_PLATFORM = "pf";
        public static string PARAM_TIME = "t";
        public static string PARAM_RINGID = "ringID";

        public static string PARAM_MOBILE = "mbl";
        public static string PARAM_DIALING_CODE = "mblDc";
        public static string PARAM_FACEBOOK = "fb";
        public static string PARAM_TWITTER = "twtr";
        public static string PARAM_DEVICE_ID = "did";
        public static string PARAM_EMAIL = "el";
        public static string PARAM_APP_TYPE = "apt";
        public static string PARAM_CATEGORY_ID = "categoryId";
        public static string PARAM_DYNAMIC_CATEGORY_ID = "dCatId";
        public static string PARAM_DEFAULT = "default";
        public static string PARAM_LIMIT = "lmt";
        public static string PARAM_TYPE = "type";
        public static string PARAM_SUMMARY_AND_LIMIT = "sum&lmt";
        public static string PARAM_COLLECTION_LIMIT = "cLmt";
        public static string PARAM_LIMIT_AND_FROM = "lm&frm";
        public static string PARAM_COLLECTION_ID = "cId";
        public static string PARAM_LANGUAGE = "lng";

        /// <summary>
        ///              {"sucs":true,"ac":"http://devauth.ringid.com/rac/","ims":"http://devimages.ringid.com/","imsres":"http://devimagesres.ringid.com/","stmapi":"http://devsticker.ringid.com/","stmres":"http://devstickerres.ringid.com/","vodapi":"http://devmediacloudapi.ringid.com/","vodres":"http://devmediacloud.ringid.com/","dd":8,"utId":1,"web":"https://dev.ringid.com/"}
        /// </summary>
        /// <returns></returns>


        public static CommunicationServerDTO GetBaseRacURLs()
        {
            string postData = String.Format(PARAM_PLATFORM + "={0}&" + PARAM_VERSION + "={1}&" + PARAM_APP_TYPE + "={2}&" + PARAM_TIME + "={3}", (int)AppConstants.PLATFORM_DESKTOP, Int32.Parse(AppConfig.VERSION_PC), (int)SettingsConstants.APP_TYPE_RINGID_FULL, (long)ModelUtility.CurrentTimeMillis());
            string response = makeWebRequestPOST(ServerAndPortSettings.GetCommunicationServer, postData, WebRequestMethods.Http.Get/*"GET"*/);
            CommunicationServerDTO communicationServerDTO = new CommunicationServerDTO(response);
            return communicationServerDTO;
        }

        public static CommunicationPortsDTO GetNewRingID()
        {
            CommunicationPortsDTO communicationPortsDTO = null;
            if (!string.IsNullOrEmpty(DefaultSettings.DEVICE_UNIQUE_ID))
            {
                string postData = String.Format(PARAM_DEVICE_ID + "={0}&" + PARAM_APP_TYPE + "={1}&" + PARAM_TIME + "={2}", HttpUtility.UrlEncode(DefaultSettings.DEVICE_UNIQUE_ID), (int)SettingsConstants.APP_TYPE_RINGID_FULL, (long)ModelUtility.CurrentTimeMillis());
                string response = makeWebRequestPOST(ServerAndPortSettings.AUTHENTICATION_CONTROLLER + PARAM_RINGID.ToLower(), postData, WebRequestMethods.Http.Post /*"POST"*/);
                communicationPortsDTO = new CommunicationPortsDTO(response);
            }
            else { log.Info("Not found DefaultSettings.DEVICE_UNIQUE_ID==>" + DefaultSettings.DEVICE_UNIQUE_ID); }
            return communicationPortsDTO;
        }

        public static CommunicationPortsDTO GetCommunicationPortsByRingID(string ringId)
        {
            string postData = makePostData(SettingsConstants.RINGID_LOGIN, PARAM_RINGID, ringId);
            CommunicationPortsDTO communicationPortsDTO = getCommunicationPortsDTO(postData);
            return communicationPortsDTO;
        }

        public static CommunicationPortsDTO GetCommunicationPortsByEmail(string email)
        {
            string postData = makePostData(SettingsConstants.EMAIL_LOGIN, PARAM_EMAIL, email);
            CommunicationPortsDTO communicationPortsDTO = getCommunicationPortsDTO(postData);
            return communicationPortsDTO;
        }

        public static CommunicationPortsDTO GetCommunicationPortsByMobile(string mobile, string mblDc)
        {
            string postData = String.Format(PARAM_LOGIN_TYPE + "={0}&" + PARAM_DIALING_CODE + "={1}&" + PARAM_MOBILE + "={2}&" + PARAM_APP_TYPE + "={3}&" + PARAM_TIME + "={4}", (int)SettingsConstants.MOBILE_LOGIN, HttpUtility.UrlEncode(mblDc), HttpUtility.UrlEncode(mobile), (int)SettingsConstants.APP_TYPE_RINGID_FULL, (long)ModelUtility.CurrentTimeMillis());
            CommunicationPortsDTO communicationPortsDTO = getCommunicationPortsDTO(postData);
            return communicationPortsDTO;
        }

        public static CommunicationPortsDTO GetCommunicationPortsSocialMedia(int type, string socialMediaID)
        {
            string urlParam = string.Empty;
            if (type == SettingsConstants.FACEBOOK_LOGIN) urlParam = PARAM_FACEBOOK;
            else if (type == SettingsConstants.TWITTER_LOGIN) urlParam = PARAM_TWITTER;
            string postData = makePostData(type, urlParam, socialMediaID);
            CommunicationPortsDTO communicationPortsDTO = getCommunicationPortsDTO(postData);
            return communicationPortsDTO;
        }

        public static string GetRingIDFromSocialMediaID(string smId, int loginType) //null if rc=2, i.e no such fb/tw id associated or no response
        {
            string postData = (loginType == 4) ? (String.Format(PARAM_FACEBOOK + "={0}&" + PARAM_TIME + "={1}&" + PARAM_LOGIN_TYPE + "=" + SettingsConstants.FACEBOOK_LOGIN, smId, (long)ModelUtility.CurrentTimeMillis())) : (String.Format(PARAM_TWITTER + "={0}&" + PARAM_TIME + "={1}&" + PARAM_LOGIN_TYPE + "=" + SettingsConstants.TWITTER_LOGIN, smId, (long)ModelUtility.CurrentTimeMillis()));
            string response = makeWebRequestPOST(ServerAndPortSettings.GetComportURL, postData, WebRequestMethods.Http.Post);
            if (!string.IsNullOrEmpty(response)) return Models.Utility.HelperMethodsModel.ParseRingIDFromResponse(response);
            else return null;
        }

        public static string GetRingIDFromEmail(string email)
        {
            string postData = String.Format(PARAM_EMAIL + "={0}&" + PARAM_LOGIN_TYPE + "={1}&" + PARAM_TIME + "={2}", HttpUtility.UrlEncode(email), (int)SettingsConstants.EMAIL_LOGIN, (long)ModelUtility.CurrentTimeMillis());
            string response = makeWebRequestPOST(ServerAndPortSettings.GetComportURL, postData, WebRequestMethods.Http.Post /*"POST"*/);
            if (!string.IsNullOrEmpty(response)) return Models.Utility.HelperMethodsModel.ParseRingIDFromResponse(response);
            else return null;
        }

        public static string GetRingIDFromPhone(string mbl, string mbldc)
        {
            string postData = String.Format(PARAM_DIALING_CODE + "={0}&" + PARAM_MOBILE + "={1}&" + PARAM_LOGIN_TYPE + "={2}&" + PARAM_TIME + "={3}", HttpUtility.UrlEncode(mbldc), HttpUtility.UrlEncode(mbl), (int)SettingsConstants.MOBILE_LOGIN, (long)ModelUtility.CurrentTimeMillis());
            string response = makeWebRequestPOST(ServerAndPortSettings.GetComportURL, postData, WebRequestMethods.Http.Post /*"POST"*/);
            if (!string.IsNullOrEmpty(response)) return Models.Utility.HelperMethodsModel.ParseRingIDFromResponse(response);
            else return null;
        }

        public static string GetAccessTokenForDigitsVerification(long rid) //null or INVALID_DATA otherwise
        {
            if (rid > 0)
            {
                string postData = String.Format("uId={0}", rid);
                string response = makeWebRequestPOST(ServerAndPortSettings.GetDigitAccessTokenURL, postData, WebRequestMethods.Http.Post /*"POST"*/);
                return response;
            }
            else return null;
        }

        public static StickerDTO GetNewStickerCategoryIds()
        {
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrlForNewStickerCategoryIDs, null, WebRequestMethods.Http.Get);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                StickerDTO stickerDTO = oSerializer.Deserialize<StickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static StickerDTO GetDefaultStickers()
        {
            string postData = String.Format(PARAM_DEFAULT + "=1&" + PARAM_TIME + "={0}&" + PARAM_PLATFORM + "={1}&" + PARAM_VERSION + "={2}", (long)ModelUtility.CurrentTimeMillis(), (int)AppConstants.PLATFORM_DESKTOP, Int32.Parse(AppConfig.VERSION_PC));
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get/*"GET"*/, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                StickerDTO stickerDTO = oSerializer.Deserialize<StickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetAllStickersForNewAPI(int categoryLimit, int collectionLimit)
        {
            string postData = String.Empty;
            postData = String.Format(PARAM_SUMMARY_AND_LIMIT + "={0}&" + PARAM_COLLECTION_LIMIT + "={1}", categoryLimit, collectionLimit);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetStickerOfParticularType(int startIndex, int categoryLimit, int collectionLimit, int stickerType)
        {
            string postData = String.Empty;
            postData = String.Format(PARAM_LIMIT_AND_FROM + "={0}&" + PARAM_LIMIT + "={1}&" + PARAM_COLLECTION_LIMIT + "={2}&" + PARAM_TYPE + "={3}", startIndex, categoryLimit, collectionLimit, stickerType);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetMoreDynamicSticker(int startIndex, int categoryLimit, int collectionLimit, int stickerType, int dynamicCategoryId)
        {
            string postData = String.Empty;
            postData = String.Format(PARAM_LIMIT_AND_FROM + "={0}&" + PARAM_LIMIT + "={1}&" + PARAM_COLLECTION_LIMIT + "={2}&" + PARAM_TYPE + "={3}&" + PARAM_DYNAMIC_CATEGORY_ID + "={4}", startIndex, categoryLimit, collectionLimit, stickerType, dynamicCategoryId);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetStickersOfParticularCollection(int startIndex, int categoryLimit, int stickerType, int collectionId)
        {
            string postData = String.Empty;
            postData = String.Format(PARAM_LIMIT_AND_FROM + "={0}&" + PARAM_LIMIT + "={1}&" + PARAM_TYPE + "={2}&" + PARAM_COLLECTION_ID + "={3}", startIndex, categoryLimit, stickerType, collectionId);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetStickersOfParticularLanguage(int startIndex, int categoryLimit, int stickerType, string languageName)
        {
            string postData = String.Empty;
            postData = String.Format(PARAM_LIMIT_AND_FROM + "={0}&" + PARAM_LIMIT + "={1}&" + PARAM_TYPE + "={2}&" + PARAM_LANGUAGE + "={3}", startIndex, categoryLimit, stickerType, languageName);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        public static MarketStickerDTO GetMarketStickerImagesByCategoryID(int categoryId)
        {
            string postData = String.Format(PARAM_CATEGORY_ID + "={0}&" + PARAM_TIME + "={1}&" + PARAM_PLATFORM + "={2}&" + PARAM_VERSION + "={3}", categoryId, (long)ModelUtility.CurrentTimeMillis(), (int)AppConstants.PLATFORM_DESKTOP, Int32.Parse(AppConfig.VERSION_PC));
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrl, postData, WebRequestMethods.Http.Get/*"GET"*/, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                MarketStickerDTO stickerDTO = oSerializer.Deserialize<MarketStickerDTO>(response);
                return stickerDTO;
            }
            return null;
        }

        private static string makePostData(int loginType, string loginParam, string ringIDEmailSocialID)
        {
            return String.Format(PARAM_LOGIN_TYPE + "={0}&" + loginParam + "={1}&" + PARAM_APP_TYPE + "={2}&t={3}", loginType, HttpUtility.UrlEncode(ringIDEmailSocialID), (int)SettingsConstants.APP_TYPE_RINGID_FULL, (long)ModelUtility.CurrentTimeMillis());
        }

        public static void IncreaseDownloadCount(int categoryId)
        {
            JObject obj = new JObject(); // these json keys formation are different.That's why JsonKeys class not used.
            obj["authIP"] = ServerAndPortSettings.AUTH_SERVER_IP;
            obj["authPort"] = ServerAndPortSettings.COMMUNICATION_PORT;
            obj["sID"] = DefaultSettings.LOGIN_SESSIONID;
            obj["utID"] = DefaultSettings.LOGIN_TABLE_ID;
            obj["sCatID"] = categoryId;
            string postData = JsonConvert.SerializeObject(obj, Formatting.None);
            string response = makeWebRequestPOST(ServerAndPortSettings.StickerMarketServiceUrlForIncreaseDownloadCount, postData, WebRequestMethods.Http.Post/*"POST"*/, false, true);
        }

        public static ChatBgDTO GetChatBackgroundImage()
        {
            string postData = String.Format("all=1&t={0}", (long)ModelUtility.CurrentTimeMillis());
            string response = makeWebRequestPOST(ServerAndPortSettings.ChatBackgroundImageServiceUrl, postData, WebRequestMethods.Http.Get/*"GET"*/, true);
            if (response != null)
            {
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                ChatBgDTO chatBgDto = oSerializer.Deserialize<ChatBgDTO>(response);
                return chatBgDto;
            }
            return null;
        }

        private static string makeWebRequestPOST(string requestUri, string postData, string method, bool appVersion = false, bool isStickerIncreaseDownloadCountRequest = false)
        {
            string responseFromServer = null;
            string reqeustUrl = requestUri + (method.Equals(WebRequestMethods.Http.Get/*"GET"*/) ? "?" + postData : "");
            if (requestUri != null)
            {
                try
                {
                    ServicePointManager.DefaultConnectionLimit = 4;
                    ServicePointManager.Expect100Continue = false;
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(reqeustUrl);
                    if (httpWebRequest != null)
                    {
                        httpWebRequest.Method = method;
                        httpWebRequest.Proxy = null;
                        httpWebRequest.Timeout = 60 * 1000;
                        if (appVersion) httpWebRequest.Headers["x-app-version"] = AppConfig.VERSION_PC;
                        if (method.Equals(WebRequestMethods.Http.Post))
                        {
                            byte[] byteArray = postData != null ? System.Text.Encoding.UTF8.GetBytes(postData) : null;
                            if (isStickerIncreaseDownloadCountRequest) httpWebRequest.ContentType = "application/json";
                            else httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                            if (byteArray != null) httpWebRequest.ContentLength = byteArray.Length;
                            using (Stream dataStream = httpWebRequest.GetRequestStream())
                            {
                                if (byteArray != null) dataStream.Write(byteArray, 0, byteArray.Length);
                                dataStream.Close();
                            }
                        }
                        using (var webResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                        {
                            using (Stream responseStream = webResponse.GetResponseStream())
                            {
                                using (StreamReader reader = new StreamReader(responseStream))
                                {
                                    responseFromServer = reader.ReadToEnd();
                                    reader.Close();
                                }
                                responseStream.Close();
                            }
                            webResponse.Close();
                            httpWebRequest.Abort();
                        }
                    }

#if HTTP_LOG
                log.Info("Reqeuest==> " + reqeustUrl + "  Resoponse==>" + responseFromServer);
#endif
                }
                catch (WebException e) { log.Error(e.StackTrace); }
            }
            return responseFromServer;
        }

        private static CommunicationPortsDTO getCommunicationPortsDTO(string postData)
        {
            string response = makeWebRequestPOST(ServerAndPortSettings.GetComportURL, postData, WebRequestMethods.Http.Post /*"POST"*/);
#if HTTP_LOG
            log.Info("getCommunicationPortsDTO==> " + postData + " response==>" + response);
#endif
            return new CommunicationPortsDTO(response);
        }

        public static bool DownloadImageFile(string uri, string fileName)
        {
            if (File.Exists(fileName)) return false;
            return DownloadFile(uri, fileName);
        }

        public static bool DownloadFile(string uri, string fileName)
        {
            bool flag = false;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Headers.Add("x-app-version", AppConfig.VERSION_PC);
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    if ((response.StatusCode == HttpStatusCode.OK ||
                    response.StatusCode == HttpStatusCode.Moved ||
                    response.StatusCode == HttpStatusCode.Redirect) &&
                    response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                    {
                        using (Stream inputStream = response.GetResponseStream())
                        using (Stream outputStream = File.OpenWrite(fileName))
                        {
                            byte[] buffer = new byte[4096];
                            int bytesRead;
                            do
                            {
                                bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                                outputStream.Write(buffer, 0, bytesRead);
                            } while (bytesRead != 0);
                        }
                        flag = true;
                    }
                }
            }
            catch (Exception e)
            {
                flag = false;
#if HTTP_LOG
                log.Error("DownloadImageFile ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
#endif
            }
            finally
            {
                if (httpWebRequest != null)
                {
                    httpWebRequest.Abort();
                    httpWebRequest = null;
                }
            }
            return flag;
        }

        public static bool DownloadImageWithValidationCheck(string uri, string fileName)
        {
            if (File.Exists(fileName)) return false;
            return DownloadFile(uri, fileName);
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

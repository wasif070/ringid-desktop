﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Auth.Parser
{
    public class PacketRepository
    {
        private static PacketRepository instance;

        public static PacketRepository Instance
        {
            get { return instance = instance ?? new PacketRepository(); }
        }

        private ConcurrentQueue<AuthMsgParser> receivedQueue;

        public ConcurrentQueue<AuthMsgParser> ReceivedQueue
        {
            get { return receivedQueue; }
            set { receivedQueue = value; }
        }

        public PacketRepository()
        {
            receivedQueue = new ConcurrentQueue<AuthMsgParser>();
        }

    }
}

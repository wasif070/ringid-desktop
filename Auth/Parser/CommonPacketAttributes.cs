﻿using System;
using System.Collections.Generic;

namespace Auth.Parser
{
    public class CommonPacketAttributes
    {
        private bool success;

        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        private int totalPackets;

        public int TotalPackets
        {
            get { return totalPackets; }
            set { totalPackets = value; }
        }

        private int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        private int packetNumber;

        public int PacketNumber
        {
            get { return packetNumber; }
            set { packetNumber = value; }
        }
        private byte[] userIDs;

        public byte[] UserIDs
        {
            get { return userIDs; }
            set { userIDs = value; }
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        private int reasonCode;

        public int ReasonCode
        {
            get { return reasonCode; }
            set { reasonCode = value; }
        }

        private long userID;

        public long UserID
        {
            get { return userID; }
            set { userID = value; }
        }
        private long userIdentity;

        public long UserIdentity
        {
            get { return userIdentity; }
            set { userIdentity = value; }
        }

        private String userName;

        public String UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        private String profileImage;

        public String ProfileImage
        {
            get { return profileImage; }
            set { profileImage = value; }
        }
        private string profileImageId;

        public string ProfileImageId
        {
            get { return profileImageId; }
            set { profileImageId = value; }
        }

        private long updateTime;

        public long UpdateTime
        {
            get { return updateTime; }
            set { updateTime = value; }
        }
        private long contactUpdateTime;

        public long ContactUpdateTime
        {
            get { return contactUpdateTime; }
            set { contactUpdateTime = value; }
        }
        private int contactType;

        public int ContactType
        {
            get { return contactType; }
            set { contactType = value; }
        }

        private int newContactType;

        public int NewContactType
        {
            get { return newContactType; }
            set { newContactType = value; }
        }

        private int deleted;

        public int Deleted
        {
            get { return deleted; }
            set { deleted = value; }
        }
        private int blockValue;

        public int BlockValue
        {
            get { return blockValue; }
            set { blockValue = value; }
        }

        private int friendshipStatus;

        public int FriendshipStatus
        {
            get { return friendshipStatus; }
            set { friendshipStatus = value; }
        }

        private int isChangeRequester;

        public int IsChangeRequester
        {
            get { return isChangeRequester; }
            set { isChangeRequester = value; }
        }

        private int mutualFriendCount;

        public int MutualFriendCount
        {
            get { return mutualFriendCount; }
            set { mutualFriendCount = value; }
        }

        private string sessionID;

        public string SessionID
        {
            get { return sessionID; }
            set { sessionID = value; }
        }

        private int callAccess;
        public int CallAccess
        {
            get { return callAccess; }
            set { callAccess = value; }
        }

        private int chatAccess;
        public int ChatAccess
        {
            get { return chatAccess; }
            set { chatAccess = value; }
        }

        private int feedAccess;
        public int FeedAccess
        {
            get { return feedAccess; }
            set { feedAccess = value; }
        }

        private long contactAddedTime;
        public long ContactAddedTime
        {
            get { return contactAddedTime; }
            set { contactAddedTime = value; }
        }

        private List<CommonPacketAttributes> contactList;

        public List<CommonPacketAttributes> ContactList
        {
            get { return contactList; }
            set { contactList = value; }
        }

        public override String ToString()
        {
            return "CommonPacketAttributes{" + "success=" + success + ", totalPackets=" + totalPackets + ", totalRecords=" + totalRecords + ", packetNumber=" + packetNumber + ", userIDs=" + userIDs + ", message=" + message + ", reasonCode=" + reasonCode + ", userID=" + userID + ", userIdentity=" + userIdentity + ", userName=" + userName + ", profileImage=" + profileImage + ", profileImageId=" + profileImageId + ", updateTime=" + updateTime + ", contactUpdateTime=" + contactUpdateTime + ", contactType=" + contactType + ", newContactType=" + newContactType + ", deleted=" + deleted + ", blockValue=" + blockValue + ", friendshipStatus=" + friendshipStatus + ", iscr=" + isChangeRequester + ", callAccess=" + callAccess + ", chatAccess=" + chatAccess + ", feedAccess=" + feedAccess + ", contactList=" + contactList + '}';
        }
    }
}

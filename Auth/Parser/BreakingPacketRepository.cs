﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Auth.Parser
{
    public class BreakingPacketRepository
    {
        public ConcurrentDictionary<long, BreakingPacketData> breaking_packets;
        private static BreakingPacketRepository instance;

        public static BreakingPacketRepository Instance
        {
            get { return instance = instance ?? new BreakingPacketRepository(); }
            set { instance = value; }
        }

        public BreakingPacketRepository()
        {
            breaking_packets = new ConcurrentDictionary<long, BreakingPacketData>();
        }

        /* BreakingPacketList functions*/
        public void PutBreakingPacketList(long key, BreakingPacketData list)
        {
            breaking_packets[key] = list;
        }

        public bool ContainsBreakingPacketList(long key)
        {
            return breaking_packets.ContainsKey(key);
        }

        public BreakingPacketData GetBreakingPacketList(long key)
        {
            BreakingPacketData _breakingPacketData = null;
            breaking_packets.TryGetValue(key, out _breakingPacketData);
            return _breakingPacketData;
        }

        public void RemoveBreakingPacketList(long key)
        {
            BreakingPacketData breakingPacketData;
            breaking_packets.TryRemove(key, out breakingPacketData);
        }

        public void RemoveAllBreakingPacketList()
        {
            breaking_packets.Clear();
        }

        public ConcurrentDictionary<long, BreakingPacketData> GetBreakingPackets()
        {
            return breaking_packets;
        }

    }
}

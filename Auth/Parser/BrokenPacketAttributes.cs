﻿
namespace Auth.Parser
{
    public class BrokenPacketAttributes
    {

        private long serverPacketID;

        public long ServerPacketID
        {
            get { return serverPacketID; }
            set { serverPacketID = value; }
        }

        private int action;

        public int Action
        {
            get { return action; }
            set { action = value; }
        }

        private int totalPackets;

        public int TotalPackets
        {
            get { return totalPackets; }
            set { totalPackets = value; }
        }

        private int packetNumber;

        public int PacketNumber
        {
            get { return packetNumber; }
            set { packetNumber = value; }
        }

        private long uniqueKey;

        public long UniqueKey
        {
            get { return uniqueKey; }
            set { uniqueKey = value; }
        }

        private byte[] data;

        public byte[] Data
        {
            get { return data; }
            set { data = value; }
        }


    }
}

﻿
namespace Auth.Parser
{
    public class AttributeCodes
    {

        public const int ACTION = 1;
        public const int SERVER_PACKET_ID = 2;
        public const int SESSION_ID = 3;
        public const int TOTAL_PACKET = 4;
        public const int PACKET_NUMBER = 5;
        public const int UNIQUE_KEY = 6;
        public const int USER_ID = 7;
        public const int FRIEND_ID = 8;
        public const int DEVICE = 9;
        public const int CLIENT_PACKET_ID = 10;
        public const int CALL_ID = 11;
        public const int FRIEND_IDENTITY = 12;
        public const int CALL_TIME = 13;
        public const int PRESENCE = 14;
        public const int IS_DEVIDED_PACKET = 15;
        public const int USER_IDENTITY = 16;
        public const int USER_NAME = 17;
        public const int TOTAL_RECORDS = 18;
        public const int USER_TABLE_IDS = 19;
        public const int SUCCESS = 20;
        public const int MESSAGE = 21;
        public const int REASON_CODE = 22;
        public const int STATUS = 23;
        public const int DELETED = 24;
        public const int WEB_UNIQUE_KEY = 28;
        public const int WEB_TAB_ID = 29;
        public const int DATA = 127;

        /* Contact list constants */
        public const int CONTACT = 101;
        public const int CONTACT_TYPE = 102;
        public const int NEW_CONTACT_TYPE = 103;
        public const int FRIENDSHIP_STATUS = 104;
        public const int BLOCK_VALUE = 105;
        public const int CHANGE_REQUESTER = 106;
        public const int CONTACT_UPDATE_TIME = 107;
        public const int MUTUAL_FRIEND_COUNT = 108;
        public const int CALL_ACCESS = 110;
        public const int CHAT_ACCESS = 111;
        public const int FEED_ACCESS = 112;
        /* USER DETAILS */
        public const int PASSWORD = 128;
        public const int RESET_PASSWORD = 129;
        public const int EMAIL = 130;
        public const int DEVICE_UNIQUE_ID = 131;
        public const int DEVICE_TOKEN = 132;
        public const int MOBILE_PHONE = 133;
        public const int BIRTH_DATE = 134;
        public const int MARRIAGE_DAY = 135;
        public const int GENDER = 136;
        public const int COUNTRY_ID = 137;
        public const int CURRENT_CITY = 138;
        public const int HOME_CITY = 139;
        public const int LANGUAGE_ID = 140;
        public const int REGISTRATION_DATE = 141;
        public const int DIALING_CODE = 142;
        public const int IS_MY_NUMBER_VERIFIED = 143;
        public const int IS_EMAIL_VERIFIED = 145;
        public const int MY_NUMBER_VERIFICATION_CODE = 146;
        public const int MYNUMBER_VERIFICATION_CODE_SENT_TIME = 147;
        public const int EMAIL_VERIFICATION_CODE = 148;
        public const int RECOVERY_VERIFICATION_CODE = 149;
        public const int RECOVERY_VERIFICATION_CODE_SENT_TIME = 150;
        public const int PROFILE_IMAGE = 151;
        public const int PROFILE_IMAGE_ID = 152;
        public const int COVER_IMAGE = 153;
        public const int COVER_IMAGE_ID = 154;
        public const int COVER_IMAGE_X = 155;
        public const int COVER_IMAGE_Y = 156;
        public const int ABOUT_ME = 157;
        public const int TOTAL_FRIENDS = 158;
        public const int RING_EMAIL = 159;
        public const int UPDATE_TIME = 160;
        public const int NOTIFICATION_VALIDITY = 161;
        public const int WEB_LOGIN_ENABLED = 162;
        public const int PC_LOGIN_ENABLED = 163;
        public const int COMMON_FRIEND_SUGGESTION = 164;
        public const int PHONE_NUMBER_SUGGESTION = 165;
        public const int CONTACT_LIST_SUGGESTION = 166;
        public const int CONTACT_ADDED_TIME = 168;
    }
}

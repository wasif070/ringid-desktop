﻿using Auth.utility;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Auth.AppInterfaces;
namespace Auth.Parser
{
    class PacketProcessor
    {
        private byte[] received_bytes;
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(PacketProcessor).Name);
        IAuthSignalHandler iAuthSignalHandler;

        public PacketProcessor(byte[] p_packet, IAuthSignalHandler iAuthSignalHandler)
        {
            this.received_bytes = p_packet;
            this.iAuthSignalHandler = iAuthSignalHandler;
        }

        public void Run()
        {
            try
            {
                int first_byte = (int)received_bytes[0];
                switch (first_byte)
                {
                    case 0:
                    case 2:
                        HeaderAttributes headers = Parser.ParseHeader(received_bytes, 1);

                        if (headers.ServerPacketID > 0)
                        {
                            SendToServer.SendByteConfirmationPacketIdFromServer(headers.ServerPacketID, headers.Action);
                        }
                        switch (first_byte)
                        {
                            case 0: // NORMAL_JSON_PACKET
                                String str = Encoding.UTF8.GetString(received_bytes, headers.HeaderLength, received_bytes.Length - headers.HeaderLength).Trim();
                                AuthMsgParser auth0 = new AuthMsgParser(str, headers.Action, headers.ServerPacketID, headers.ClientPacketID, this.iAuthSignalHandler);
                                PacketRepository.Instance.ReceivedQueue.Enqueue(auth0);
                                break;
                            case 2: // NORMAL_BYTE_PACKET
                                AuthMsgParser auth2 = new AuthMsgParser(received_bytes, headers.Action, headers.ServerPacketID, headers.ClientPacketID, headers.HeaderLength, received_bytes.Length, this.iAuthSignalHandler);
                                PacketRepository.Instance.ReceivedQueue.Enqueue(auth2);
                                break;
                        }
                        break;

                    case 1: //BREAKING_JSON_PACKET
                    case 3: //BREAKING_BYTE_PACKET
                        BrokenPacketAttributes attributes = Parser.ParseBrokenPacket(received_bytes, 1);

                        int total_packets = attributes.TotalPackets;
                        long uniqueKey = attributes.UniqueKey;
                        //sint action = attributes.getAction();
                        if (attributes.ServerPacketID > 0)
                        {
                            SendToServer.SendByteConfirmationPacketIdFromServer(attributes.ServerPacketID, attributes.Action);
                        }

                        byte[] data_bytes = attributes.Data;
                        byte[] all_data = null;
                        int packet_number = attributes.PacketNumber;
                        BreakingPacketData breakingPacketData = null;
                        lock (BreakingPacketRepository.Instance.breaking_packets)
                        {
                            BreakingPacketRepository.Instance.breaking_packets.TryGetValue(uniqueKey, out breakingPacketData);
                            if (breakingPacketData == null)
                            {
                                breakingPacketData = new BreakingPacketData();
                                breakingPacketData.Data = new ConcurrentDictionary<int, byte[]>();
                                breakingPacketData.Time = DateTime.Now.Millisecond;
                                BreakingPacketRepository.Instance.breaking_packets[uniqueKey] = breakingPacketData;
                            }
                        }

                        if (breakingPacketData != null)
                        {
                            lock (breakingPacketData.Data)
                            {
                                if (!breakingPacketData.Data.ContainsKey(packet_number))
                                {
                                    breakingPacketData.Data[packet_number] = data_bytes;
                                }
                                if (total_packets == breakingPacketData.Data.Count)
                                {
                                    int total_read = 0;
                                    foreach (byte[] bytes in breakingPacketData.Data.Values)
                                    {
                                        if (bytes != null)
                                        {
                                            total_read += bytes.Length;
                                        }
                                    }

                                    all_data = new byte[total_read];
                                    total_read = 0;
                                    for (int i = 0; i < breakingPacketData.Data.Count; i++)
                                    {
                                        byte[] broken_data = breakingPacketData.Data[i];
                                        Array.Copy(broken_data, 0, all_data, total_read, broken_data.Length);
                                        total_read += broken_data.Length;
                                    }
                                }
                            }
                        }

                        if (all_data != null && all_data.Length > 0)
                        {
                            HeaderAttributes headers1 = Parser.ParseHeader(all_data, 0);
                            //if (headers1.ServerPacketID > 0)
                            //{
                            //    SendToServer.SendByteConfirmationPacketIdFromServer(headers1.ServerPacketID, headers1.Action);
                            //}

                            switch (first_byte)
                            {
                                case 1: //BREAKING_JSON_PACKET    
                                    String str = Encoding.UTF8.GetString(all_data, headers1.HeaderLength, all_data.Length - headers1.HeaderLength).Trim();
                                    AuthMsgParser auth1 = new AuthMsgParser(str, headers1.Action, attributes.ServerPacketID /*headers1.ServerPacketID*/, headers1.ClientPacketID, this.iAuthSignalHandler);
                                    PacketRepository.Instance.ReceivedQueue.Enqueue(auth1);
                                    break;
                                case 3: //BREAKING_BYTE_PACKET
                                    AuthMsgParser auth3 = new AuthMsgParser(all_data, headers1.Action, attributes.ServerPacketID /*headers1.ServerPacketID*/, headers1.ClientPacketID, headers1.HeaderLength, all_data.Length, this.iAuthSignalHandler);
                                    PacketRepository.Instance.ReceivedQueue.Enqueue(auth3);
                                    break;
                            }
                            BreakingPacketRepository.Instance.RemoveBreakingPacketList(uniqueKey);
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                log.Error("Packet Processor Exception ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
            finally
            {
                PacketQueueProcessor.CreateInstance();
            }

        }

    }
}

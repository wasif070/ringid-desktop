﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Auth.Parser
{
    public class BreakingPacketData
    {
        private long time;

        public long Time
        {
            get { return time; }
            set { time = value; }
        }
        private ConcurrentDictionary<int, byte[]> data;

        public ConcurrentDictionary<int, byte[]> Data
        {
            get { return data; }
            set { data = value; }
        }
    }
}

﻿using Auth.utility;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Auth.Parser
{
    class SendDividedPacket
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SendDividedPacket).Name);
        public static Dictionary<String, byte[]> BuildDevidedPackets(int action, byte[] all_bytes, int length)
        {
            Dictionary<String, byte[]> tempMap = new Dictionary<String, byte[]>();
            int total_packets = length / AppConstants.CLIENT_DATA_SIZE;
            if (length - total_packets * AppConstants.CLIENT_DATA_SIZE > 0)
            {
                total_packets++;
            }

            int current_packet = 0;
            int read_bytes = 0;
            String uniqueKey = SendToServer.GetRanDomPacketID(); //"123" + sixDigit;  
            try
            {
                while (read_bytes < length)
                {
                    int diff = length - read_bytes;
                    int data_size = AppConstants.CLIENT_DATA_SIZE;
                    if (diff < AppConstants.CLIENT_DATA_SIZE)
                    {
                        data_size = diff;
                    }

                    int header_length = 129;
                    byte[] header_bytes = new byte[header_length];

                    header_bytes[0] = (byte)AppConstants.REQUEST_TYPE_UPDATE;
                    header_bytes[1] = (byte)AppConstants.BROKEN_PACKET;
                    int index = AddInteger(AttributeCodes.ACTION, action, 2, header_bytes, 2);

                    String packet_id = SendToServer.GetRanDomPacketID(); //"123" + sixDigit;
                    index = AddString(AttributeCodes.CLIENT_PACKET_ID, packet_id, header_bytes, index);

                    if (total_packets < 128)
                    {
                        index = AddInteger(AttributeCodes.TOTAL_PACKET, total_packets, 1, header_bytes, index);
                        index = AddInteger(AttributeCodes.PACKET_NUMBER, current_packet, 1, header_bytes, index);
                    }
                    else
                    {
                        index = AddInteger(AttributeCodes.TOTAL_PACKET, total_packets, 2, header_bytes, index);
                        index = AddInteger(AttributeCodes.PACKET_NUMBER, current_packet, 2, header_bytes, index);
                    }

                    index = AddString(AttributeCodes.UNIQUE_KEY, uniqueKey, header_bytes, index);

                    byte[] send_bytes = new byte[index + data_size + 3];
                    Array.Copy(header_bytes, 0, send_bytes, 0, index);
                    AddBytes(AttributeCodes.DATA, all_bytes, read_bytes, send_bytes, index, data_size);

                    current_packet++;
                    read_bytes += data_size;
                    // tempMap.put(packet_id, send_bytes); 
                    lock (tempMap)
                    {
                        tempMap[packet_id] = send_bytes;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("BuildDevidedPackets ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }

            return tempMap;
        }

        private static int AddInteger(int attribute, int value, int length, byte[] send_bytes, int index)
        {
            send_bytes[index++] = (byte)attribute;
            send_bytes[index++] = (byte)length;

            switch (length)
            {
                case 1:
                    {
                        send_bytes[index++] = (byte)(value);
                        break;
                    }
                case 2:
                    {
                        send_bytes[index++] = (byte)(value >> 8);
                        send_bytes[index++] = (byte)(value);
                        break;
                    }
                case 4:
                    {
                        send_bytes[index++] = (byte)(value >> 24);
                        send_bytes[index++] = (byte)(value >> 16);
                        send_bytes[index++] = (byte)(value >> 8);
                        send_bytes[index++] = (byte)(value);
                        break;
                    }
            }

            return index;
        }

        private static int AddString(int attribute, String value, byte[] send_bytes, int index)
        {
            send_bytes[index++] = (byte)attribute;

            byte[] id_bytes = Encoding.UTF8.GetBytes(value);
            int length = id_bytes.Length;

            send_bytes[index++] = (byte)length;
            Array.Copy(id_bytes, 0, send_bytes, index, length);
            index += length;

            return index;
        }

        private static int AddBytes(int attribute, byte[] data_bytes, int read_bytes, byte[] send_bytes, int index, int length)
        {
            send_bytes[index++] = (byte)attribute;

            send_bytes[index++] = (byte)(length >> 8);
            send_bytes[index++] = (byte)(length);

            Array.Copy(data_bytes, read_bytes, send_bytes, index, length);
            index += length;
            return index;
        }
    }
}

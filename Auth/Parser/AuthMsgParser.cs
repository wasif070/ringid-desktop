<<<<<<< HEAD
﻿#region "Previous Code"
//using System;
//using System.Collections.Concurrent;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using Auth.Service.FriendList;
//using Auth.Service.SigninSignup;
//using Auth.Service.Suggestion;
//using Auth.utility;
//using Auth.utility.Feed;
//using log4net;
//using Models.Constants;
//using Models.DAO;
//using Models.Entity;
//using Models.Stores;
//using Models.Utility;
//using Newtonsoft.Json.Linq;
//using Auth.Service.Chat;
//using Auth.AppInterfaces;


//namespace Auth.Parser
//{
//    public class AuthMsgParser
//    {
//        #region "Private Fields"
//        private readonly ILog log = LogManager.GetLogger(typeof(AuthMsgParser).Name);
//        private string _JsonResponse = null;
//        private JObject _JobjFromResponse = null;

//        private int action;
//        private string client_packet_id;
//        private long server_packet_id;

//        private byte[] byte_response = null;
//        private int byte_start_pos;
//        private int byte_data_length;
//        private CommonPacketAttributes packet_attributes;
//        IAuthSignalHandler iAuthSignalHandler;
//        #endregion "Private Fields"

//        #region "Public Fields"
//        public static int SpamReasonListSeqCount = 0, FeedTagListSeqCount = 0, WorkListSequenceCount = 0, EducationListSequenceCount = 0, MediaItemsSequenceCount = 0, SkillListSequenceCount = 0, FriendContactListSequenceCount = 0, MutualFriendsSeqCount = 0, LikesSequnceCount = 0, CommentsSeqCount = 0, ImageLikesSequenceCount = 0, ImageCommentsSequenceCount = 0, ImageCommentLikesSequeceCount = 0, CircleMembersListSeqCount = 0, MediaAlbumListSeqCount = 0;// AlbumMediaContentListSeqCount = 0;
//        #endregion "Public Fields"

//        #region "Constructors"
//        public AuthMsgParser(string resp, int action, long server_packet_id, string client_packet_id, IAuthSignalHandler iAuthSignalHandler)
//        {
//            this._JsonResponse = resp != null ? resp.Trim() : "";
//            this.action = action;
//            this.server_packet_id = server_packet_id;
//            this.client_packet_id = client_packet_id;
//            this.iAuthSignalHandler = iAuthSignalHandler;
//        }

//        public AuthMsgParser(byte[] resp, int action, long server_packet_id, string client_packet_id, int start_pos, int data_length, IAuthSignalHandler iAuthSignalHandler)
//        {
//            this.byte_response = resp;
//            this.action = action;
//            this.server_packet_id = server_packet_id;
//            this.client_packet_id = client_packet_id;
//            this.byte_start_pos = start_pos;
//            this.byte_data_length = data_length;
//            this.iAuthSignalHandler = iAuthSignalHandler;
//        }
//        #endregion "Constructors"

//        #region "Initit StartProcess"

//        public void StartProcess()
//        {
//            try
//            {
//                if (_JsonResponse != null) //process json here
//                {
//                    if (this.action != 200)
//                    {
//                        //if (DefaultSettings.DEBUG)
//                        //{
//                        //    log.Info("Received for action=" + this.action + " from Auth JSON==> " + this._JsonResponse);
//                        //}
//#if AUTH_LOG
//                        log.Info("Received for action=" + this.action + " from Auth JSON==> " + this._JsonResponse);
//#endif
//                        _JobjFromResponse = JObject.Parse(_JsonResponse);
//                    }

//                    if (!String.IsNullOrEmpty(client_packet_id))
//                    {
//                        lock (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY)
//                        {
//                            if (this.action == 200)
//                            {
//                                _JobjFromResponse = new JObject();
//                                _JobjFromResponse.Add(JsonKeys.PacketId, client_packet_id);
//                                _JobjFromResponse.Add(JsonKeys.Action, this.action);
//                            }
//                            else if (this.action == AppConstants.TYPE_ADD_STATUS
//                                || this.action == AppConstants.TYPE_EDIT_STATUS
//                                || this.action == AppConstants.TYPE_MULTIPLE_IMAGE_POST
//                                || this.action == AppConstants.TYPE_SHARE_STATUS
//                                || this.action == AppConstants.TYPE_ADD_STATUS_COMMENT
//                                || this.action == AppConstants.TYPE_EDIT_STATUS_COMMENT
//                                || this.action == AppConstants.TYPE_ADD_IMAGE_COMMENT
//                                || this.action == AppConstants.TYPE_EDIT_IMAGE_COMMENT
//                                || this.action == AppConstants.TYPE_ADD_COMMENT_ON_MEDIA
//                                || this.action == AppConstants.TYPE_EDIT_COMMENT_ON_MEDIA
//                                || this.action == AppConstants.TYPE_START_GROUP_CHAT)
//                            {
//                                client_packet_id = client_packet_id + "_" + this.action;
//                            }
//                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.AddOrUpdate(client_packet_id, _JobjFromResponse, (key, oldValue) => _JobjFromResponse);

//                        }
//                    }
//                    this.processJsonResponse();
//                }
//                else  //process byte here
//                {
//                    if (!String.IsNullOrEmpty(client_packet_id))
//                    {
//                        _JobjFromResponse = new JObject();
//                        _JobjFromResponse.Add(JsonKeys.PacketId, client_packet_id);
//                        lock (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY)
//                        {
//                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.AddOrUpdate(client_packet_id, _JobjFromResponse, (key, oldValue) => _JobjFromResponse);
//                        }
//                    }

//                    if (this.action != 200)
//                    {
//                        this.packet_attributes = Parser.ParsePacket(this.byte_response, this.byte_start_pos, this.byte_data_length);
//#if AUTH_LOG
//                        log.Info("Received for action=" + this.action + " from Auth BYTE==> " + packet_attributes.ToString());
//#endif
//                    }
//                    this.processByteResponse();
//                }
//            }
//            catch (Exception e)
//            {
//                if (this._JsonResponse != null)
//                {

//                    log.Error("Auth parse Exception for action=" + this.action + " from Auth JSON==>" + e.Message + "\n" + e.StackTrace + " Response==>" + this._JsonResponse);

//                }
//                else if (this.packet_attributes != null)
//                {

//                    log.Error("Auth parse Exception for action=" + this.action + " from Auth BYTE==>" + e.Message + "\n" + e.StackTrace + " Response==>" + this.packet_attributes.ToString());

//                }
//            }

//        }

//        private void processByteResponse()
//        {
//            switch (this.action)
//            {
//                case AppConstants.TYPE_CONTACT_UTIDS:  //29
//                    this.ProcessByteContactUtids();
//                    break;

//                case AppConstants.TYPE_CONTACT_LIST:  //23
//                    this.ProcessByteContactList();
//                    break;

//                case AppConstants.TYPE_MULTIPLE_SESSION_WARNING: //75
//                    this.processMultipleSessionWarning();
//                    break;
//                default:
//                    break;

//            }
//        }

//        private void processJsonResponse()
//        {
//            switch (this.action)
//            {
//                case AppConstants.TYPE_INVALID_LOGIN_SESSION: //19
//                    this.ProcessInvalidLoginSession();
//                    break;
//                case AppConstants.TYPE_SUGGESTION_IDS: //31
//                    this.ProcessSuggestionUtids();
//                    break;
//                case AppConstants.TYPE_USERS_DETAILS: //32
//                    this.ProcessSuggestionUsersDetails();
//                    break;
//                case AppConstants.TYPE_CONTACT_SEARCH: //34
//                    this.ProcessContactSearch();
//                    break;

//                case AppConstants.TYPE_COMMENTS_FOR_STATUS: //84
//                    this.ProcessCommentsForStatus();
//                    break;
//                case AppConstants.TYPE_MEDIA_FEED: //87
//                    this.ProcessMediaFeed();
//                    break;
//                case AppConstants.TYPE_NEWS_FEED: //88
//                    this.ProcessNewsFeed();
//                    break;
//                case AppConstants.TYPE_LIKES_FOR_STATUS: //92
//                    this.ProcessFetchLikesForStatus();
//                    break;
//                case AppConstants.TYPE_MY_BOOK: //94
//                    this.ProcessMyBook();
//                    break;
//                case AppConstants.TYPE_MY_ALBUM_IMAGES: //97
//                    this.ProcessMyAlbumImages();
//                    break;
//                case AppConstants.TYPE_FRIEND_ALBUM_IMAGES: //109
//                    this.ProcessFriendAlbumImages();
//                    break;
//                case AppConstants.TYPE_FRIEND_NEWSFEED: //110
//                    this.ProcessFriendNewsFeeds();
//                    break;
//                //case AppConstants.TYPE_YOU_MAY_KNOW_LIST: //106
//                //    this.ProcessSuggestionList();
//                //    break;
//                case AppConstants.TYPE_FRIEND_CONTACT_LIST: //107
//                case AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141: //211
//                    this.ProcessFriendContactList();
//                    break;
//                case AppConstants.TYPE_MY_NOTIFICATIONS: //111
//                    this.ProcessMyNotifications();
//                    break;
//                case AppConstants.TYPE_SINGLE_NOTIFICATION: //113
//                    this.ProcessSingleNotification();
//                    break;
//                //case AppConstants.TYPE_SINGLE_STATUS_NOTIFICATION: //114
//                //    this.ProcessSingleFeedDetails();
//                //    break;
//                case AppConstants.TYPE_SINGLE_FEED_SHARE_LIST://115
//                    this.ProcessSingleFeedShareList();
//                    break;
//                case AppConstants.TYPE_LIST_LIKES_OF_COMMENT: //116
//                    this.ProcessFetchLikesForComment();
//                    break;
//                case AppConstants.TYPE_MULTIPLE_IMAGE_POST://117
//                    this.ProcessAddStatus();
//                    break;
//                case AppConstants.TYPE_MUTUAL_FRIENDS://118
//                    this.ProcessTypeMutualFriends();
//                    break;

//<<<<<<< .mine
//                #region For Multisession
//                case AppConstants.TYPE_ADD_FRIEND://127
//                    this.ProcessPushAddFriend();
//                    break;
//                case AppConstants.TYPE_DELETE_FRIEND://128
//                    this.ProcessDeleteFriendUpdate();
//                    break;
//                case AppConstants.TYPE_ACCEPT_FRIEND://129
//                    this.ProcessPushAcceptFriend();
//                    break;
//                #endregion
//=======
//                case AppConstants.TYPE_ADD_STATUS: //177
//                    this.ProcessAddStatus();
//                    break;
//                case AppConstants.TYPE_START_GROUP_CHAT: //134
//                    this.ProcessUpdateStartGroupChat();
//                    break;
//                case AppConstants.TYPE_START_FRIEND_CHAT: //175
//                    this.ProcessUpdateStartFriendChat();
//                    break;
//                case AppConstants.TYPE_LIKE_STATUS: //184
//                    this.ProcessUpdateLikeStatus(true);
//                    break;
//                case AppConstants.TYPE_UNLIKE_STATUS: //186
//                    this.ProcessUpdateUnlikeStatus(true);
//                    break;
//                case AppConstants.TYPE_SINGLE_FRIEND_PRESENCE_INFO://199
//                    this.ProcessTypeFriendPresenceInfo();
//                    break;
//                case AppConstants.TYPE_UPDATE_DIGITS_VERIFY://209
//                    this.ProcessTypeUpdateDigitsVerify();
//                    break;
//                case AppConstants.TYPTE_MISS_CALL_LIST: // 224
//                    this.ProcessMissCallList();
//                    break;
//                case AppConstants.TYPE_WHO_SHARES_LIST: //249
//                    this.ProcessWhoSharesList();
//                    break;
//                case AppConstants.TYPE_MEDIA_SHARE_LIST://250
//                    this.ProcessSingleFeedShareList();
//                    break;
//                case AppConstants.TYPE_MEDIA_ALBUM_LIST: //256
//                    this.ProcessMediaAlbumList();
//                    break;
//                case AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST: //261
//                    this.ProcessMediaAlbumContentList();
//                    break;
//                //case AppConstants.TYPE_MEDIA_CONTENT_DETAILS://262
//                //    this.ProcessMediaContentDetails();
//                //    break;
//                case AppConstants.TYPE_MEDIA_LIKE_LIST: //269
//                    this.ProcessMediaLikeList();
//                    break;
//                case AppConstants.TYPE_MEDIA_COMMENT_LIST://270
//                    this.ProcessMediaCommentList();
//                    break;
//                case AppConstants.TYPE_MEDIACOMMENT_LIKE_LIST://271
//                    this.ProcessMediaCommentLikeList();
//                    break;
//                case AppConstants.TYPE_VIEW_DOING_LIST: //273
//                    this.ProcessDoingList();
//                    break;
//                case AppConstants.TYPE_VIEW_TAGGED_LIST: //274
//                    this.ProcessFeedTagList();
//                    break;
//                case AppConstants.TYPE_MEDIA_SUGGESTION://277
//                    this.ProcessMediaSearchSuggestions();
//                    break;
//                case AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD: //278
//                    this.ProcessMediaFullSearch();
//                    break;
//                case AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS: //279
//                    this.ProcessHashtagMediaContents();
//                    break;
//                case AppConstants.TYPE_HASHTAG_SUGGESTION: //280
//                    this.ProcessHashtagSuggestion();
//                    break;
//                case AppConstants.TYPE_SEARCH_TRENDS: //281
//                    this.ProcessMediaSearchTrends();
//                    break;
//                case AppConstants.TYPE_STORE_CONTACT_LIST: //284
//                    this.ProcessStoreContactList();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_CATEGORIES_LIST: //294
//                    this.ProcessNewsPortalCategoriesList();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_FEED: //295
//                    this.ProcessNewsPortalFeed();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_LIST://299
//                    this.ProcessDiscoverNewsPortals();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_BREAKING_FEED://302
//                    this.ProcessBreakingNewsPortalFeeds();
//                    break;
//                case AppConstants.TYPE_BUSINESS_PAGE_FEED: //306
//                    this.ProcessBusinessPageFeeds();
//                    break;
//                case AppConstants.TYPE_MEDIA_PAGE_FEED://307
//                    this.ProcessMediaCloudFeeds();
//                    break;
//                case AppConstants.TYPE_MEDIA_PAGE_TRENDING_FEED://308
//                    this.ProcessTrendingMediaCloudFeeds();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKE_COMMENT: //323
//                    this.ProcessUpdateLikeComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_UNLIKE_COMMENT: //323
//                    this.ProcessUpdateUnLikeComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_FRIEND: //327
//                    ProcessAddFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_FRIEND: //328
//                    ProcessDeleteFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_ACCEPT_FRIEND: //329
//                    ProcessAcceptFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_START_GROUP_CHAT: //334
//                    this.ProcessUpdateStartGroupChat();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER: //335
//                    this.ProcessUpdateAddGroupMember();
//                    break;
//                case AppConstants.TYPE_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO://336
//                    this.ProcessTypeFriendPresenceInfo();
//                    break;
//                case AppConstants.TYPE_UPDATE_SEND_REGISTER: //374
//                    this.ProcessUpdateSendRegister();
//                    break;
//                case AppConstants.TYPE_UPDATE_START_FRIEND_CHAT: //375
//                    this.ProcessUpdateStartFriendChat();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_STATUS: //377
//                    this.ProcessUpdateAddStatus(true);
//                    break;
//                case AppConstants.TYPE_UPDATE_SHARE_STATUS: //391
//                    this.ProcessUpdateAddStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDIT_STATUS: //378
//                    this.ProcessUpdateEditStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_STATUS: //379
//                    this.ProcessUpdateDeleteStatus();
//                    break;
//>>>>>>> .r1628

//                case AppConstants.TYPE_ADD_STATUS: //177
//                    this.ProcessAddStatus();
//                    break;
//                case AppConstants.TYPE_START_GROUP_CHAT: //134
//                    this.ProcessUpdateStartGroupChat();
//                    break;
//                case AppConstants.TYPE_START_FRIEND_CHAT: //175
//                    this.ProcessUpdateStartFriendChat();
//                    break;
//                case AppConstants.TYPE_LIKE_STATUS: //184
//                    this.ProcessUpdateLikeStatus(true);
//                    break;
//                case AppConstants.TYPE_UNLIKE_STATUS: //186
//                    this.ProcessUpdateUnlikeStatus(true);
//                    break;
//                case AppConstants.TYPE_SINGLE_FRIEND_PRESENCE_INFO://199
//                    this.ProcessTypeFriendPresenceInfo();
//                    break;
//                case AppConstants.TYPE_UPDATE_DIGITS_VERIFY://209
//                    this.ProcessTypeUpdateDigitsVerify();
//                    break;
//                case AppConstants.TYPTE_MISS_CALL_LIST: // 224
//                    this.ProcessMissCallList();
//                    break;
//                case AppConstants.TYPE_WHO_SHARES_LIST: //249
//                    this.ProcessWhoSharesList();
//                    break;
//                case AppConstants.TYPE_MEDIA_SHARE_LIST://250
//                    this.ProcessSingleFeedShareList();
//                    break;
//                case AppConstants.TYPE_MEDIA_ALBUM_LIST: //256
//                    this.ProcessMediaAlbumList();
//                    break;
//                case AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST: //261
//                    this.ProcessMediaAlbumContentList();
//                    break;
//                //case AppConstants.TYPE_MEDIA_CONTENT_DETAILS://262
//                //    this.ProcessMediaContentDetails();
//                //    break;
//                case AppConstants.TYPE_MEDIA_LIKE_LIST: //269
//                    this.ProcessMediaLikeList();
//                    break;
//                case AppConstants.TYPE_MEDIA_COMMENT_LIST://270
//                    this.ProcessMediaCommentList();
//                    break;
//                case AppConstants.TYPE_MEDIACOMMENT_LIKE_LIST://271
//                    this.ProcessMediaCommentLikeList();
//                    break;
//                case AppConstants.TYPE_VIEW_DOING_LIST: //273
//                    this.ProcessDoingList();
//                    break;
//                case AppConstants.TYPE_VIEW_TAGGED_LIST: //274
//                    this.ProcessFeedTagList();
//                    break;
//                case AppConstants.TYPE_MEDIA_SUGGESTION://277
//                    this.ProcessMediaSearchSuggestions();
//                    break;
//                case AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD: //278
//                    this.ProcessMediaFullSearch();
//                    break;
//                case AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS: //279
//                    this.ProcessHashtagMediaContents();
//                    break;
//                case AppConstants.TYPE_HASHTAG_SUGGESTION: //280
//                    this.ProcessHashtagSuggestion();
//                    break;
//                case AppConstants.TYPE_SEARCH_TRENDS: //281
//                    this.ProcessMediaSearchTrends();
//                    break;
//                case AppConstants.TYPE_STORE_CONTACT_LIST: //284
//                    this.ProcessStoreContactList();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_CATEGORIES_LIST: //294
//                    this.ProcessNewsPortalCategoriesList();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_FEED: //295
//                    this.ProcessNewsPortalFeed();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_LIST://299
//                    this.ProcessDiscoverNewsPortals();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_BREAKING_FEED://302
//                    this.ProcessBreakingNewsPortalFeeds();
//                    break;
//                case AppConstants.TYPE_BUSINESS_PAGE_FEED: //306
//                    this.ProcessBusinessPageFeeds();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKE_COMMENT: //323
//                    this.ProcessUpdateLikeComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_UNLIKE_COMMENT: //323
//                    this.ProcessUpdateUnLikeComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_FRIEND: //327
//                    ProcessAddFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_FRIEND: //328
//                    ProcessDeleteFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_ACCEPT_FRIEND: //329
//                    ProcessAcceptFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_START_GROUP_CHAT: //334
//                    this.ProcessUpdateStartGroupChat();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER: //335
//                    this.ProcessUpdateAddGroupMember();
//                    break;
//                case AppConstants.TYPE_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO://336
//                    this.ProcessTypeFriendPresenceInfo();
//                    break;
//                case AppConstants.TYPE_UPDATE_SEND_REGISTER: //374
//                    this.ProcessUpdateSendRegister();
//                    break;
//                case AppConstants.TYPE_UPDATE_START_FRIEND_CHAT: //375
//                    this.ProcessUpdateStartFriendChat();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_STATUS: //377
//                    this.ProcessUpdateAddStatus(true);
//                    break;
//                case AppConstants.TYPE_UPDATE_SHARE_STATUS: //391
//                    this.ProcessUpdateAddStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDIT_STATUS: //378
//                    this.ProcessUpdateEditStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_STATUS: //379
//                    this.ProcessUpdateDeleteStatus();
//                    break;

//                case AppConstants.TYPE_UPDATE_LIKE_STATUS: //384
//                    this.ProcessUpdateLikeStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKE_UNLIKE_IMAGE: //385 
//                    this.ProcessUpdateLikeUnlikeImage();
//                    break;
//                case AppConstants.TYPE_UPDATE_UNLIKE_STATUS: //386
//                    this.ProcessUpdateUnlikeStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDIT_STATUS_COMMENT: //389
//                    this.ProcessUpdateEditComment();
//                    break;

//                #region Image tasks
//                case AppConstants.TYPE_UPDATE_ADD_IMAGE_COMMENT: //380
//                    this.ProcessUpdateAddImageComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_STATUS_COMMENT: //381
//                    this.ProcessUpdateAddStatusComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_IMAGE_COMMENT: //382
//                    this.ProcessUpdateDeleteImageComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_STATUS_COMMENT: //383
//                    this.ProcessUpdateDeleteStatusComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDIT_IMAGE_COMMENT: //394
//                    this.ProcessUpdateEditImageComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT: //397
//                    this.ProcessUpdateLikeUnlikeImageComment();
//                    break;
//                //case AppConstants.TYPE_IMAGE_DETAILS: //121
//                //    this.ProcessSingleImageDetail();
//                //    break;
//                case AppConstants.TYPE_COMMENTS_FOR_IMAGE: // 89
//                    this.ProcessImageComments();
//                    break;
//                case AppConstants.TYPE_IMAGE_COMMENT_LIKES: //196
//                    this.ProcessLikersImageComment();
//                    break;
//                case AppConstants.TYPE_LIKES_FOR_IMAGE: //93
//                    this.ProcessLikersImage();
//                    break;
//                case AppConstants.TYPE_LIKE_UNLIKE_IMAGE_COMMENT: // 197
//                    this.ProcessLikeUnlikeImageComment();
//                    break;
//                case AppConstants.TYPE_ADD_IMAGE_COMMENT: //180
//                    this.ProcessAddImageComment();
//                    break;
//                case AppConstants.TYPE_EDIT_IMAGE_COMMENT: //194
//                    this.ProcessEditImageComment();
//                    break;
//                #endregion

//                case AppConstants.TYPE_UPDATE_LIKEUNLIKE_MEDIA: //464
//                    this.ProcessUpdateLikeUnlikeMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_COMMENT_MEDIA: //465
//                    this.ProcessUpdateCommentMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDITCOMMENT_MEDIA: //466
//                    this.ProcessUpdateEditCommentMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETECOMMENT_MEDIA: //467
//                    this.ProcessUpdateDeleteCommentMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKEUNLIKECOMMENT_MEDIA: //468
//                    this.ProcessUpdateLikeUnlikeCommentMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_STORE_CONTACT_LIST: //484
//                    this.ProcessStoreContactList();
//                    break;
//                case AppConstants.TYPE_NEW_CIRCLE: //51
//                case AppConstants.TYPE_LEAVE_CIRCLE: //53
//                case AppConstants.TYPE_CIRCLE_LIST: //70
//                case AppConstants.TYPE_CIRCLE_MEMBERS_LIST: //99
//                case AppConstants.TYPE_SEARCH_CIRCLE_MEMBER: //101
//                case AppConstants.TYPE_DELETE_CIRCLE: //152
//                case AppConstants.TYPE_REMOVE_CIRCLE_MEMBER:
//                case AppConstants.TYPE_CIRCLE_NEWSFEED: //198
//                case AppConstants.TYPE_CIRCLE_DETAILS:
//                    //case AppConstants.TYPE_UPDATE_DELETE_CIRCLE: //352
//                    //case AppConstants.TYPE_UPDATE_REMOVE_CIRCLE_MEMBER: //354
//                    //case AppConstants.TYPE_UPDATE_ADD_CIRCLE_MEMBER: //356
//                    //case AppConstants.TYPE_UPDATE_EDIT_CIRCLE_MEMBER: //358
//                    AMPCircle.Instance.ProcessCircle(action, _JobjFromResponse);
//                    break;
//                case AppConstants.TYPE_PRESENCE: //78
//                case AppConstants.TYPE_ADD_WORK: // 227;
//                case AppConstants.TYPE_ADD_EDUCATION: // 231;
//                case AppConstants.TYPE_LIST_WORK: // 234
//                case AppConstants.TYPE_LIST_EDUCATION: //235
//                case AppConstants.TYPE_LIST_SKILL: //236
//                case AppConstants.TYPE_ADD_SKILL: // 237;
//                    AMPProfileAbout.Instance.ProcessAbout(action, _JobjFromResponse);
//                    break;
//                case AppConstants.TYPE_SPAM_REASON_LIST: //1001
//                    this.ProcessSpamReasonList();
//                    break;
//                default:
//                    break;
//            }
//        }

//        #endregion "Initit StartProcess"

//        #region "Contact List"

//        private void ProcessByteContactUtids() //29
//        {
//            try
//            {
//                if (Auth.Service.FriendList.ContactUtIdsRequest.Instance != null && Auth.Service.FriendList.ContactUtIdsRequest.Instance.IsRunning())
//                {
//                    Auth.Service.FriendList.ContactUtIdsRequest.Instance._TotalRecords = this.packet_attributes.TotalRecords;
//                }
//                if (this.packet_attributes.Success && this.packet_attributes.UserIDs != null)
//                {
//                    byte[] userIDsBytes = this.packet_attributes.UserIDs;

//                    for (int i = 0; i < userIDsBytes.Length; i += 11)
//                    {
//                        long userTableId = Parser.GetLong(userIDsBytes, i, 8);
//                        int ct = (int)userIDsBytes[i + 8];
//                        int mb = (int)userIDsBytes[i + 9];
//                        int fns = (int)userIDsBytes[i + 10];

//                        UserBasicInfoDTO user = new UserBasicInfoDTO();
//                        user.UserTableId = userTableId;
//                        user.MatchedBy = mb;
//                        user.FriendShipStatus = fns;
//                        FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY[user.UserTableId] = user;
//                    }

//                    if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == this.packet_attributes.TotalRecords)
//                    {
//                        List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
//                        foreach (Models.Entity.UserBasicInfoDTO item in FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Values)
//                        {
//                            userList.Add(item);
//                        }
//                        FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Clear();
//                        userList = Auth.utility.HelperMethodsAuth.SortFriendListByFriendshipStatusAccepted(userList);
//                        foreach (UserBasicInfoDTO user in userList)
//                        {
//                            FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY[user.UserTableId] = user;
//                        }
//                        if (ContactListRequest.Instance == null || ContactListRequest.Instance.IsRunning() == false)
//                        {
//                            new ContactListRequest();
//                        }
//                    }
//                }
//                else
//                {
//                    HelperMethodsAuth.AuthHandlerInstance.UI_ProcessByteContactList(null, this.packet_attributes.Success);
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("Exception in TYPE_CONTACT_UTIDS 29==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }

//        private void ProcessByteContactList() //23
//        {
//            try
//            {
//                List<UserBasicInfoDTO> basicInfos = null;
//                if (this.packet_attributes.Success)
//                {
//                    if (this.packet_attributes.ContactList != null && this.packet_attributes.ContactList.Count > 0)
//                    {
//                        basicInfos = new List<UserBasicInfoDTO>();

//                        foreach (CommonPacketAttributes contact in this.packet_attributes.ContactList)
//                        {
//                            UserBasicInfoDTO entity = null;
//                            FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.TryGetValue(contact.UserID, out entity);
//                            if (entity != null)
//                            {
//                                FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.TryRemove(contact.UserID, out entity);
//                                entity.UserTableId = contact.UserID;
//                                entity.UserIdentity = contact.UserIdentity;
//                                if (contact.UserIdentity == 0)
//                                {
//#if AUTH_LOG
//                                    log.Info("Contact's UserIdentity==0 Found, UserTableId==>" + entity.UserTableId);
//#endif
//                                }
//                                entity.FullName = contact.UserName;
//                                //entity.BlockedValue = contact.BlockValue;
//                                entity.ContactType = contact.ContactType;
//                                entity.ContactAddedTime = contact.ContactAddedTime;
//                                entity.Delete = contact.Deleted;
//                                entity.FriendShipStatus = (entity.Delete == StatusConstants.STATUS_DELETED) ? 0 : contact.FriendshipStatus;
//                                //entity.NewContactType = contact.NewContactType;
//                                entity.ProfileImage = contact.ProfileImage;
//                                entity.ProfileImageId = contact.ProfileImageId;
//                                entity.ContactUpdateTime = contact.ContactUpdateTime;
//                                entity.UpdateTime = contact.UpdateTime;
//                                entity.NumberOfMutualFriends = contact.MutualFriendCount;
//                                //if (contact.IsChangeRequester > 0) entity.IsChangeRequester = true;
//                                //else entity.IsChangeRequester = false;
//                                entity.CallAccess = contact.CallAccess;
//                                entity.ChatAccess = contact.ChatAccess;
//                                entity.FeedAccess = contact.FeedAccess;
//                                entity.BlockedBy = (entity.CallAccess == 0 && entity.ChatAccess == 0 && entity.FeedAccess == 0) ? 1 : 0;

//                                UserBasicInfoDTO tempDTO;
//                                if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(entity.UserIdentity, out tempDTO))
//                                {
//                                    entity.ContactAddedReadUnread = tempDTO.ContactAddedReadUnread;
//                                    entity.ChatBgUrl = tempDTO.ChatBgUrl;
//                                    if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
//                                    {
//                                        entity.FavoriteRank = tempDTO.FavoriteRank;
//                                    }
//                                    if (entity.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
//                                    {
//                                        entity.BlockedBy = tempDTO.BlockedBy;
//                                    }

//                                    if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && entity.FriendShipStatus != tempDTO.FriendShipStatus)
//                                    {
//                                        entity.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
//                                        entity.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
//                                        entity.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
//                                    }
//                                    else
//                                    {
//                                        entity.ImSoundEnabled = tempDTO.ImSoundEnabled;
//                                        entity.ImNotificationEnabled = tempDTO.ImNotificationEnabled;
//                                        entity.ImPopupEnabled = tempDTO.ImPopupEnabled;
//                                    }
//                                }

//                                if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && entity.ContactAddedReadUnread != SettingsConstants.RECENT_FRND_READ &&
//                                    (entity.ContactAddedTime > (Models.Utility.ModelUtility.CurrentTimeMillisLocal() - Models.Utility.ModelUtility.MilliSecondsInSevenDay)))
//                                {
//                                    entity.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;
//                                }

//                                if (entity.UserIdentity > 0)
//                                {
//                                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                                    {
//                                        //if ((!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(entity.UserIdentity)
//                                        //    || (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(entity.UserIdentity) && FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[entity.UserIdentity].FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_INCOMING))
//                                        //    && entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
//                                        //{
//                                        //    AppConstants.ADD_FRIEND_NOTIFICATION_COUNT++;
//                                        //}
//                                        //else if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED || entity.FriendShipStatus == 0)
//                                        //{
//                                        //    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(entity.UserIdentity)
//                                        //        && FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[entity.UserIdentity].FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING
//                                        //        && AppConstants.ADD_FRIEND_NOTIFICATION_COUNT > 0)
//                                        //    {
//                                        //        AppConstants.ADD_FRIEND_NOTIFICATION_COUNT--;
//                                        //    }
//                                        //}
//                                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[entity.UserIdentity] = entity;
//                                    }

//                                    lock (FriendDictionaries.Instance.UTID_UID_DICTIONARY)
//                                    {
//                                        FriendDictionaries.Instance.UTID_UID_DICTIONARY[entity.UserTableId] = entity.UserIdentity;
//                                    }
//                                }
//                                lock (FriendDictionaries.Instance.TEMP_CONTACT_LIST)
//                                {
//                                    FriendDictionaries.Instance.TEMP_CONTACT_LIST.Add(entity);
//                                }

//                                basicInfos.Add(entity);
//                            }
//                        }
//                        //   HelperMethodsAuth.AuthHandlerInstance.AddFriendListFromServer(basicInfos);
//                    }

//                    //if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0 && DefaultSettings.FRIEND_LIST_LOADED == false)
//                    //{
//                    //    HelperMethodsAuth.AuthHandlerInstance.AddFriendsNotificationForIncomingRequest();
//                    //    HelperMethodsAuth.AuthHandlerInstance.NotifyAllContactListLoaded();
//                    //    new InsertIntoUserBasicInfoTable(FriendDictionaries.Instance.TEMP_CONTACT_LIST).Start();
//                    //}
//                }
//                HelperMethodsAuth.AuthHandlerInstance.UI_ProcessByteContactList(basicInfos, this.packet_attributes.Success);
//                //else
//                //{
//                //    if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0 && DefaultSettings.FRIEND_LIST_LOADED == false)
//                //    {
//                //        //log.Info("########### Action-23 !SUCCCESS END############");
//                //        HelperMethodsAuth.AuthHandlerInstance.NotifyAllContactListLoaded();
//                //    }
//                //}
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in TYPE_CONTACT_LIST 23==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }

//        }

//        private void ProcessAddFriendUpdate() //someone sent me friend request//327
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    UserBasicInfoDTO userBasicInfo = null;
//                    long userID = _JobjFromResponse[JsonKeys.UserIdentity] != null ? long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString()) : 0;
//                    int FriendshipStatus = _JobjFromResponse[JsonKeys.FriendshipStatus] != null ? Int32.Parse(_JobjFromResponse[JsonKeys.FriendshipStatus].ToString()) : 0;

//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userID, out userBasicInfo);
//                        if (userBasicInfo == null)
//                        {
//                            userBasicInfo = new UserBasicInfoDTO();
//                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(userID, userBasicInfo);
//                        }
//                        HelperMethodsModel.BindAddFriendDetails(_JobjFromResponse, userBasicInfo);
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userBasicInfo.UserIdentity] = userBasicInfo;
//                    }
//                    lock (FriendDictionaries.Instance.UTID_UID_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UTID_UID_DICTIONARY[userBasicInfo.UserTableId] = userBasicInfo.UserIdentity;
//                    }

//                    lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(userBasicInfo.UserTableId);
//                    }
//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userBasicInfo.UserIdentity);
//                    }

//                    List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
//                    userBasicInfoList.Add(userBasicInfo);
//                    new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();

//                    HelperMethodsAuth.AuthHandlerInstance.UI_ProcessAddFriendUpdate(userBasicInfo);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessAddFriendUpdate==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessAcceptFriendUpdate() //someone accepted my request//329
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                    {
//                        UserBasicInfoDTO userBasicInfo = null;

//                        lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                        {
//                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString()), out userBasicInfo);

//                            if (userBasicInfo == null)
//                            {
//                                userBasicInfo = new UserBasicInfoDTO();
//                                FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString()), userBasicInfo);
//                            }
//                        }

//                        if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                        {
//                            userBasicInfo.UserIdentity = long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString());
//                        }
//                        if (_JobjFromResponse[JsonKeys.UserTableId] != null)
//                        {
//                            userBasicInfo.UserTableId = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                        }
//                        if (_JobjFromResponse[JsonKeys.FullName] != null)
//                        {
//                            userBasicInfo.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                        }
//                        if (_JobjFromResponse[JsonKeys.Gender] != null)
//                        {
//                            userBasicInfo.Gender = (string)_JobjFromResponse[JsonKeys.Gender];
//                        }
//                        if (_JobjFromResponse[JsonKeys.FriendshipStatus] != null)
//                        {
//                            userBasicInfo.FriendShipStatus = (int)_JobjFromResponse[JsonKeys.FriendshipStatus];
//                        }
//                        if (_JobjFromResponse[JsonKeys.ProfileImage] != null)
//                        {
//                            userBasicInfo.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
//                        }
//                        if (_JobjFromResponse[JsonKeys.ProfileImageId] != null)
//                        {
//                            userBasicInfo.ProfileImageId = (long)_JobjFromResponse[JsonKeys.ProfileImageId];
//                        }
//                        if (_JobjFromResponse[JsonKeys.NumberOfMutualFriend] != null)
//                        {
//                            userBasicInfo.NumberOfMutualFriends = (int)_JobjFromResponse[JsonKeys.NumberOfMutualFriend];
//                        }
//                        if (_JobjFromResponse[JsonKeys.MatchedBy] != null)
//                        {
//                            userBasicInfo.MatchedBy = (int)_JobjFromResponse[JsonKeys.MatchedBy];
//                        }
//                        if (_JobjFromResponse[JsonKeys.CallAccess] != null)
//                        {
//                            userBasicInfo.CallAccess = (int)_JobjFromResponse[JsonKeys.CallAccess];
//                        }
//                        if (_JobjFromResponse[JsonKeys.ChatAccess] != null)
//                        {
//                            userBasicInfo.ChatAccess = (int)_JobjFromResponse[JsonKeys.ChatAccess];
//                        }
//                        if (_JobjFromResponse[JsonKeys.FeedAccess] != null)
//                        {
//                            userBasicInfo.FeedAccess = (int)_JobjFromResponse[JsonKeys.FeedAccess];
//                        }

//                        userBasicInfo.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
//                        userBasicInfo.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
//                        userBasicInfo.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;

//                        List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
//                        userBasicInfoList.Add(userBasicInfo);
//                        new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();

//                        HelperMethodsAuth.AuthHandlerInstance.UI_ProcessAcceptFriendUpdate(userBasicInfo);

//                        //HelperMethodsAuth.AuthHandlerInstance.AddOrAcceptSingleFriendUI(userBasicInfo);
//                        //HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(userBasicInfo);
//                        //HelperMethodsAuth.AuthHandlerInstance.SetRequestsForFriendViewedandAdded(userBasicInfo);
//                        //HelperMethodsAuth.AuthHandlerInstance.ReloadFriendsMutualFriendsProfile();
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessAcceptFriendUpdate==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }

//        }

//        private void ProcessDeleteFriendUpdate() //328
//        {
//            try
//            {
//                if ((this.action == AppConstants.TYPE_DELETE_FRIEND && server_packet_id != 0) || this.action == AppConstants.TYPE_UPDATE_DELETE_FRIEND)
//                {
//                    if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                    {
//                        long userID = _JobjFromResponse[JsonKeys.UserIdentity] != null ? long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString()) : 0;

//                        UserBasicInfoDTO userBasicInfo = null;
//                        lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                        {
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userID, out userBasicInfo))
//                            {
//                                userBasicInfo.FriendShipStatus = 0;
//                                if (userBasicInfo.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
//                                {
//                                    userBasicInfo.BlockedBy = StatusConstants.BLOCKED_BY_ME;
//                                }
//                                userBasicInfo.ChatAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
//                                userBasicInfo.CallAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
//                                userBasicInfo.FeedAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;

//                                /*Favorite Rank*/
//                                if (userBasicInfo.FavoriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE)
//                                {
//                                    userBasicInfo.FavoriteRank = 0;
//                                    ContactListDAO.UpdateContactRanking(userBasicInfo);
//                                }
//                                userBasicInfo.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;

//                                List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
//                                userList.Add(userBasicInfo);
//                                new InsertIntoUserBasicInfoTable(userList).Start();

//                                HelperMethodsAuth.AuthHandlerInstance.UI_ProcessDeleteFriendUpdate(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.HideFriendProfileTabForNF(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.RemoveSingleFriendFromUIList(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.HideFriendAboutInfos(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.ReloadFriendsMutualFriendsProfile();
//                                //=======
//                                //                            HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(userBasicInfo);
//                                //                            HelperMethodsAuth.AuthHandlerInstance.HideFriendProfileTabForNF(userBasicInfo);
//                                //                            HelperMethodsAuth.AuthHandlerInstance.RemoveSingleFriendFromUIList(userBasicInfo);
//                                //                            HelperMethodsAuth.AuthHandlerInstance.HideFriendAboutInfos(userBasicInfo);
//                                //                            HelperMethodsAuth.AuthHandlerInstance.ReloadFriendsMutualFriendsProfile();
//                                //>>>>>>> .r8960
//                            }
//                        }
//                    }
//                    else
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.ReloadFriendsMutualFriendsProfile();
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessDeleteFriendUpdate==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessContactSearch() //34
//        {
//            try
//            {
//                int searchCount = 0;
//                int matchCount = 0; // Server often sends wrong data
//                int ProfileType = 0;
//                string searchParam = null;
//                List<UserBasicInfoDTO> listuserBasicInfoDTO = null;
//                List<NewsPortalDTO> portalList = null;
//                List<PageInfoDTO> pagesList = null;
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
//                    if (_JobjFromResponse[JsonKeys.SearchedContacList] != null)
//                    {
//                        JArray searchedContactList = (JArray)_JobjFromResponse[JsonKeys.SearchedContacList];
//                        if (_JobjFromResponse[JsonKeys.ProfileType] != null) ProfileType = ((int)_JobjFromResponse[JsonKeys.ProfileType]);
//                        if (ProfileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
//                        {
//                            List<long> portalIds = null;
//                            if (!NewsPortalDictionaries.Instance.NEWSPORTAL_SEARCH_RES_BY_PARAM.ContainsKey(searchParam))
//                            {
//                                portalIds = new List<long>();
//                                NewsPortalDictionaries.Instance.NEWSPORTAL_SEARCH_RES_BY_PARAM[searchParam] = portalIds;
//                            }
//                            else portalIds = NewsPortalDictionaries.Instance.NEWSPORTAL_SEARCH_RES_BY_PARAM[searchParam];
//                            portalList = new List<NewsPortalDTO>();
//                            foreach (JObject singleObj in searchedContactList)
//                            {
//                                NewsPortalDTO dto = new NewsPortalDTO();
//                                if (singleObj[JsonKeys.NewsPortalDTO] != null)
//                                {
//                                    JObject NpDto = (JObject)singleObj[JsonKeys.NewsPortalDTO];
//                                    if (NpDto[JsonKeys.NewsPortalSlogan] != null)
//                                    {
//                                        dto.PortalSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
//                                    }
//                                    if (NpDto[JsonKeys.Subscribe] != null)
//                                    {
//                                        dto.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
//                                    }
//                                    if (NpDto[JsonKeys.NewsPortalCatName] != null)
//                                    {
//                                        dto.PortalCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
//                                    }
//                                    if (NpDto[JsonKeys.NewsPortalCatId] != null)
//                                    {
//                                        dto.PortalCatId = (long)NpDto[JsonKeys.NewsPortalCatId];
//                                    }
//                                    if (NpDto[JsonKeys.SubscriberCount] != null)
//                                    {
//                                        dto.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
//                                    }
//                                    if (NpDto[JsonKeys.FullName] != null)
//                                    {
//                                        dto.PortalName = (string)NpDto[JsonKeys.FullName];
//                                    }
//                                    if (NpDto[JsonKeys.UserTableId] != null)
//                                    {
//                                        dto.PortalId = (long)NpDto[JsonKeys.UserTableId];
//                                    }
//                                    if (NpDto[JsonKeys.UserIdentity] != null)
//                                    {
//                                        dto.UserId = (long)NpDto[JsonKeys.UserIdentity];
//                                    }
//                                    if (NpDto[JsonKeys.ProfileImage] != null)
//                                    {
//                                        dto.ProfileImage = (string)NpDto[JsonKeys.ProfileImage];
//                                    }
//                                }
//                                if (singleObj[JsonKeys.UserTableId] != null)
//                                {
//                                    dto.PortalId = (long)singleObj[JsonKeys.UserTableId];
//                                }
//                                if (singleObj[JsonKeys.UserIdentity] != null)
//                                {
//                                    dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                                }
//                                if (singleObj[JsonKeys.FullName] != null)
//                                {
//                                    dto.PortalName = (string)singleObj[JsonKeys.FullName];
//                                }
//                                if (singleObj[JsonKeys.ProfileImage] != null)
//                                {
//                                    dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                                }
//                                if (!portalIds.Contains(dto.PortalId)) portalIds.Add(dto.PortalId);
//                                ///
//                                NewsPortalDictionaries.Instance.UTID_NEWSPORTALINFO_DICTIONARY[dto.PortalId] = dto;
//                                portalList.Add(dto);
//                            }
//                        }
//                        else if (ProfileType == SettingsConstants.PROFILE_TYPE_PAGES)
//                        {
//                            List<long> pageIds = null;
//                            if (!NewsPortalDictionaries.Instance.PAGES_SEARCH_RES_BY_PARAM.ContainsKey(searchParam))
//                            {
//                                pageIds = new List<long>();
//                                NewsPortalDictionaries.Instance.PAGES_SEARCH_RES_BY_PARAM[searchParam] = pageIds;
//                            }
//                            else pageIds = NewsPortalDictionaries.Instance.PAGES_SEARCH_RES_BY_PARAM[searchParam];
//                            pagesList = new List<PageInfoDTO>();
//                            foreach (JObject singleObj in searchedContactList)
//                            {
//                                PageInfoDTO dto = new PageInfoDTO();
//                                if (singleObj[JsonKeys.NewsPortalDTO] != null)
//                                {
//                                    JObject NpDto = (JObject)singleObj[JsonKeys.NewsPortalDTO];
//                                    if (NpDto[JsonKeys.NewsPortalSlogan] != null)
//                                    {
//                                        dto.PageSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
//                                    }
//                                    if (NpDto[JsonKeys.Subscribe] != null)
//                                    {
//                                        dto.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
//                                    }
//                                    if (NpDto[JsonKeys.NewsPortalCatName] != null)
//                                    {
//                                        dto.PageCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
//                                    }
//                                    if (NpDto[JsonKeys.NewsPortalCatId] != null)
//                                    {
//                                        dto.PageCatId = (long)NpDto[JsonKeys.NewsPortalCatId];
//                                    }
//                                    if (NpDto[JsonKeys.SubscriberCount] != null)
//                                    {
//                                        dto.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
//                                    }
//                                    if (NpDto[JsonKeys.FullName] != null)
//                                    {
//                                        dto.PageName = (string)NpDto[JsonKeys.FullName];
//                                    }
//                                    if (NpDto[JsonKeys.UserTableId] != null)
//                                    {
//                                        dto.PageId = (long)NpDto[JsonKeys.UserTableId];
//                                    }
//                                    if (NpDto[JsonKeys.UserIdentity] != null)
//                                    {
//                                        dto.UserId = (long)NpDto[JsonKeys.UserIdentity];
//                                    }
//                                    if (NpDto[JsonKeys.ProfileImage] != null)
//                                    {
//                                        dto.ProfileImage = (string)NpDto[JsonKeys.ProfileImage];
//                                    }
//                                }
//                                if (singleObj[JsonKeys.UserTableId] != null)
//                                {
//                                    dto.PageId = (long)singleObj[JsonKeys.UserTableId];
//                                }
//                                if (singleObj[JsonKeys.UserIdentity] != null)
//                                {
//                                    dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                                }
//                                if (singleObj[JsonKeys.FullName] != null)
//                                {
//                                    dto.PageName = (string)singleObj[JsonKeys.FullName];
//                                }
//                                if (singleObj[JsonKeys.ProfileImage] != null)
//                                {
//                                    dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                                }
//                                if (!pageIds.Contains(dto.PageId)) pageIds.Add(dto.PageId);
//                                ///
//                                NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY[dto.PageId] = dto;
//                                pagesList.Add(dto);
//                            }
//                        }
//                        else
//                        {
//                            listuserBasicInfoDTO = new List<UserBasicInfoDTO>();
//                            foreach (JObject singleObj in searchedContactList)
//                            {
//                                bool isMatch = false;
//                                searchCount++;
//                                UserBasicInfoDTO user = new UserBasicInfoDTO();
//                                if (singleObj[JsonKeys.FullName] != null)
//                                {
//                                    user.FullName = (string)singleObj[JsonKeys.FullName];
//                                    if (!string.IsNullOrWhiteSpace(user.FullName) && !string.IsNullOrWhiteSpace(searchParam) && user.FullName.ToLower().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.UserIdentity] != null)
//                                {
//                                    long uId = 0;
//                                    try
//                                    {
//                                        uId = long.Parse(singleObj[JsonKeys.UserIdentity].ToString());
//                                    }
//                                    catch (Exception e)
//                                    {
//                                        System.Diagnostics.Debug.WriteLine(e.Message + "\n" + e.StackTrace);
//                                    }
//                                    user.UserIdentity = uId;
//                                    if (user.UserIdentity.ToString().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.UserTableId] != null)
//                                {
//                                    user.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                                }
//                                if (singleObj[JsonKeys.ProfileImage] != null)
//                                {
//                                    user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                                }
//                                if (singleObj[JsonKeys.NumberOfMutualFriend] != null)
//                                {
//                                    user.NumberOfMutualFriends = (long)singleObj[JsonKeys.NumberOfMutualFriend];
//                                }
//                                if (singleObj[JsonKeys.MobilePhone] != null)
//                                {
//                                    user.MobileNumber = (string)singleObj[JsonKeys.MobilePhone];
//                                    if (!string.IsNullOrWhiteSpace(user.MobileNumber) && !string.IsNullOrWhiteSpace(searchParam) && user.MobileNumber.Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.DialingCode] != null)
//                                {
//                                    user.MobileDialingCode = (string)singleObj[JsonKeys.DialingCode];
//                                    if (!string.IsNullOrWhiteSpace(user.MobileDialingCode) && !string.IsNullOrWhiteSpace(searchParam) && user.MobileDialingCode.Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.Email] != null)
//                                {
//                                    user.Email = (string)singleObj[JsonKeys.Email];
//                                    if (!string.IsNullOrWhiteSpace(user.Email) && !string.IsNullOrWhiteSpace(searchParam) && user.Email.ToLower().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.HomeCity] != null)
//                                {
//                                    user.HomeCity = (string)singleObj[JsonKeys.HomeCity];
//                                    if (!string.IsNullOrWhiteSpace(user.HomeCity) && !string.IsNullOrWhiteSpace(searchParam) && user.HomeCity.ToLower().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.CurrentCity] != null)
//                                {
//                                    user.CurrentCity = (string)singleObj[JsonKeys.CurrentCity];
//                                    if (!string.IsNullOrWhiteSpace(user.CurrentCity) && !string.IsNullOrWhiteSpace(searchParam) && user.CurrentCity.ToLower().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (_JobjFromResponse[JsonKeys.SearchCategory] != null && isMatch)
//                                {
//                                    matchCount++;
//                                    listuserBasicInfoDTO.Add(user);
//                                    //lock (RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY)
//                                    //{
//                                    //    Dictionary<long, UserBasicInfoDTO> userMap = null;
//                                    //    RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY.TryGetValue((int)_JobjFromResponse[JsonKeys.SearchCategory], out userMap);

//                                    //    if (userMap != null)
//                                    //    {
//                                    //        if (userMap.ContainsKey(user.UserIdentity))
//                                    //        {
//                                    //            userMap[user.UserIdentity] = user;
//                                    //        }
//                                    //        else
//                                    //        {
//                                    //            userMap.Add(user.UserIdentity, user);
//                                    //        }
//                                    //    }
//                                    //    else
//                                    //    {
//                                    //        userMap = new Dictionary<long, UserBasicInfoDTO>();
//                                    //        userMap.Add(user.UserIdentity, user);
//                                    //        RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY.Add((int)_JobjFromResponse[JsonKeys.SearchCategory], userMap);
//                                    //    }
//                                    //}
//                                }
//                            }
//                        }
//                    }
//                }
//                if (ProfileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
//                    HelperMethodsAuth.NewsFeedHandlerInstance.SearchNewsPortals(searchParam, portalList);
//                else if (ProfileType == SettingsConstants.PROFILE_TYPE_PAGES)
//                    HelperMethodsAuth.NewsFeedHandlerInstance.SearchNewsPortals(searchParam, pagesList);
//                else
//                    HelperMethodsAuth.AuthHandlerInstance.UI_ProcessContactSearch(searchCount, matchCount, client_packet_id, listuserBasicInfoDTO);
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessContactSearch ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessMissCallList()//224
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {

//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessGroupList ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessStoreContactList()
//        {
//            try
//            {
//                new ContactUtIdsRequest();
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessStoreContact ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessSuggestionUsersDetails()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    JArray ContactList = (JArray)_JobjFromResponse[JsonKeys.ContactList];
//                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();

//                    for (int j = 0; j < ContactList.Count; j++)
//                    {
//                        JObject SingleContact = (JObject)ContactList.ElementAt(j);

//                        if (SingleContact != null)
//                        {
//                            UserBasicInfoDTO suggestionDTO = new UserBasicInfoDTO();


//                            if (SingleContact[JsonKeys.UserIdentity] != null)
//                            {
//                                suggestionDTO.UserIdentity = long.Parse(SingleContact[JsonKeys.UserIdentity].ToString());
//                            }

//                            if (SingleContact[JsonKeys.UserTableId] != null)
//                            {
//                                suggestionDTO.UserTableId = (long)SingleContact[JsonKeys.UserTableId];
//                            }
//                            if (SingleContact[JsonKeys.FullName] != null)
//                            {
//                                suggestionDTO.FullName = (string)SingleContact[JsonKeys.FullName];
//                            }

//                            if (SingleContact[JsonKeys.Gender] != null)
//                            {
//                                suggestionDTO.Gender = (string)SingleContact[JsonKeys.Gender];
//                            }

//                            if (SingleContact[JsonKeys.ProfileImage] != null)
//                            {
//                                suggestionDTO.ProfileImage = (string)SingleContact[JsonKeys.ProfileImage];
//                            }

//                            if (SingleContact[JsonKeys.ProfileImageId] != null)
//                            {
//                                suggestionDTO.ProfileImageId = (long)SingleContact[JsonKeys.ProfileImageId];
//                            }

//                            if (SingleContact[JsonKeys.UpdateTime] != null)
//                            {
//                                suggestionDTO.UpdateTime = (long)SingleContact[JsonKeys.UpdateTime];
//                            }
//                            if (SingleContact[JsonKeys.NumberOfMutualFriend] != null)
//                            {
//                                suggestionDTO.NumberOfMutualFriends = (int)SingleContact[JsonKeys.NumberOfMutualFriend];
//                            }

//                            lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY)
//                            {
//                                if (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.ContainsKey(suggestionDTO.UserIdentity))
//                                {
//                                    FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY[suggestionDTO.UserIdentity] = suggestionDTO;
//                                }
//                                else
//                                {
//                                    FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Add(suggestionDTO.UserIdentity, suggestionDTO);
//                                }
//                            }

//                            lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                            {
//                                if (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(suggestionDTO.UserTableId))
//                                {
//                                    FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY[suggestionDTO.UserTableId] = true;
//                                }
//                                else
//                                {
//                                    FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Add(suggestionDTO.UserTableId, true);
//                                }
//                            }

//                            list.Add(suggestionDTO);
//                        }

//                        HelperMethodsAuth.AuthHandlerInstance.AddSuggestions(list);
//                    }

//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessSuggestionUsersDetails ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessSuggestionUtids()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {

//                    JArray ContactUtids = (JArray)_JobjFromResponse[JsonKeys.ContactIds];
//                    JArray utIds = new JArray();
//                    foreach (var item in ContactUtids)
//                    {
//                        long utId = (long)item;
//                        if (DefaultSettings.INITIAL_SUGSTNS_COUNT < 15)
//                        {
//                            utIds.Add(utId);
//                            DefaultSettings.INITIAL_SUGSTNS_COUNT++;
//                        }
//                        lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                        {
//                            if (!FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(utId))
//                            {
//                                FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Add(utId, false);
//                            }
//                        }
//                    }
//                    if (utIds.Count > 0)
//                        SendToServer.SuggestionUsersDetailsRequest(utIds);
//                    /*for (int j = 0; j < ContactUtids.Count; j++)
//                    {
//                        long utid = (long)ContactUtids.ElementAt(j);

//                        if (!FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(utid))
//                        {
//                            lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                            {
//                                if (!FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(utid))
//                                {
//                                    FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Add(utid, false);
//                                }
//                            }
//                        }
//                    }

//                    JArray userIds = new JArray();
//                    lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                    {
//                        int count = 0;
//                        foreach (long id in FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Keys)
//                        {
//                            userIds.Add(id);
//                            count++;
//                            if (count == 15)
//                            {
//                                break;
//                            }
//                        }
//                    }
//                    SendToServer.SuggestionUsersDetailsRequest(userIds);*/

//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessSuggestionUtids ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessTypeMutualFriends()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.TotalRecords] != null)
//                {
//                    MutualFriendsSeqCount++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);
//                    int tf = (int)_JobjFromResponse[JsonKeys.TotalRecords];
//                    long uid = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    if (_JobjFromResponse[JsonKeys.MutualFriendIDs] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MutualFriendIDs];
//                        for (int i = 0; i < jarray.Count; i++)
//                        {
//                            //long i1 = (long)JsonConvert.DeserializeObject(jarray[i].ToString(), typeof(long));
//                            long i1 = (long)jarray.ElementAt(i);
//                            /*if (i < DefaultSettings.CONTENT_SHOW_LIMIT)
//                            {
//                                FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Add(i1);
//                            }*/

//                            lock (FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY)
//                            {
//                                if (FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.ContainsKey(uid))
//                                {
//                                    if (!FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[uid].FriendList.Any(f => f.Equals(i1)))
//                                    {
//                                        FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[uid].FriendList.Add(i1);
//                                    }
//                                }
//                                else
//                                {
//                                    FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.Add(uid, new FriendInfoDTO());
//                                    FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[uid].FriendList.Add(i1);
//                                }
//                                FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[uid].TotalFriend = tf;
//                            }
//                        }
//                    }
//                    //System.Diagnostics.Debug.WriteLine("MUT CONTACT LIST.." + MutualFriendsSeqCount + "SEQTOTAL.." + seqTotal);
//                    if (MutualFriendsSeqCount == seqTotal)
//                    {
//                        MutualFriendsSeqCount = 0;
//                        FriendInfoDTO fiDTO;
//                        FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.TryGetValue(uid, out fiDTO);
//                        int i = 0;
//                        while (i < fiDTO.FriendList.Count && i < DefaultSettings.CONTENT_SHOW_LIMIT)
//                        {
//                            FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Add(fiDTO.FriendList[i]);
//                            i++;
//                        }
//                        HelperMethodsAuth.AuthHandlerInstance.ShowFriendMutualContactList(uid, FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY);
//                    }
//                }
//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    long uid = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    HelperMethodsAuth.AuthHandlerInstance.NoMutualFriendFoundInFriendsContactList(uid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessTypeMutualFriends ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessFriendContactList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.TotalFriend] != null)
//                {
//                    //FriendContactListSequenceCount++;
//                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                    /*string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);*/
//                    int tf = (int)_JobjFromResponse[JsonKeys.TotalFriend];
//                    long uid;
//                    FriendDictionaries.Instance.UTID_UID_DICTIONARY.TryGetValue(utid, out uid);
//                    if (_JobjFromResponse[JsonKeys.ContactList] != null)
//                    {
//                        JArray contactList = (JArray)_JobjFromResponse[JsonKeys.ContactList];
//                        lock (FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY)
//                        {
//                            foreach (JObject singleObj in contactList)
//                            {
//                                UserBasicInfoDTO userBasicInfo;
//                                lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                                {
//                                    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(long.Parse(singleObj[JsonKeys.UserIdentity].ToString()), out userBasicInfo);

//                                    if (userBasicInfo == null)
//                                    {
//                                        userBasicInfo = new UserBasicInfoDTO();
//                                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(long.Parse(singleObj[JsonKeys.UserIdentity].ToString()), userBasicInfo);
//                                    }
//                                }
//                                //UserBasicInfoDTO userBasicInfo = new UserBasicInfoDTO();
//                                if (singleObj[JsonKeys.UserIdentity] != null)
//                                {
//                                    userBasicInfo.UserIdentity = long.Parse(singleObj[JsonKeys.UserIdentity].ToString());
//                                }
//                                if (singleObj[JsonKeys.UserTableId] != null)
//                                {
//                                    userBasicInfo.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                                }
//                                if (singleObj[JsonKeys.FullName] != null)
//                                {
//                                    userBasicInfo.FullName = (string)singleObj[JsonKeys.FullName];
//                                }
//                                if (singleObj[JsonKeys.Gender] != null)
//                                {
//                                    userBasicInfo.Gender = (string)singleObj[JsonKeys.Gender];
//                                }
//                                if (singleObj[JsonKeys.FriendshipStatus] != null)
//                                {
//                                    userBasicInfo.FriendShipStatus = (int)singleObj[JsonKeys.FriendshipStatus];
//                                }
//                                if (singleObj[JsonKeys.ProfileImage] != null)
//                                {
//                                    userBasicInfo.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                                }
//                                if (singleObj[JsonKeys.ProfileImageId] != null)
//                                {
//                                    userBasicInfo.ProfileImageId = (long)singleObj[JsonKeys.ProfileImageId];
//                                }
//                                if (singleObj[JsonKeys.NumberOfMutualFriend] != null)
//                                {
//                                    userBasicInfo.NumberOfMutualFriends = (int)singleObj[JsonKeys.NumberOfMutualFriend];
//                                }
//                                if (singleObj[JsonKeys.ContactUpdate] != null)
//                                {
//                                    userBasicInfo.ContactUpdateTime = (long)singleObj[JsonKeys.ContactUpdate];
//                                }
//                                if (singleObj[JsonKeys.UpdateTime] != null)
//                                {
//                                    userBasicInfo.UpdateTime = (long)singleObj[JsonKeys.UpdateTime];
//                                }
//                                FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY[userBasicInfo.UserIdentity] = userBasicInfo;
//                                lock (FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY)
//                                {
//                                    if (FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY.ContainsKey(uid))
//                                    {
//                                        if (!FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[uid].FriendList.Any(f => f.Equals(userBasicInfo.UserIdentity)))
//                                        {
//                                            FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[uid].FriendList.Add(userBasicInfo.UserIdentity);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY.Add(uid, new FriendInfoDTO());
//                                        FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[uid].FriendList.Add(userBasicInfo.UserIdentity);
//                                    }
//                                    FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[uid].TotalFriend = tf;
//                                }
//                            }
//                        }
//                    }
//                    //log.Info("FR CONTACT LIST.." + FriendContactListSequenceCount + "SEQTOTAL.." + seqTotal);
//                    //if (FriendContactListSequenceCount == seqTotal)
//                    {
//                        //FriendContactListSequenceCount = 0;                        
//                        HelperMethodsAuth.AuthHandlerInstance.ShowFriendTotalContactList(uid, FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY.Values.ToList());
//                        FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY.Clear();
//                    }
//                }
//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                    HelperMethodsAuth.AuthHandlerInstance.NoContactFoundInFriendsContactList(utid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessFriendContactList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessPushAddFriend()//127
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && server_packet_id != 0)
//                {
//                    UserBasicInfoDTO userBasicInfo = null;
//                    long userID = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    int FriendshipStatus = _JobjFromResponse[JsonKeys.FriendshipStatus] != null ? Int32.Parse(_JobjFromResponse[JsonKeys.FriendshipStatus].ToString()) : 0;

//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userID, out userBasicInfo);
//                        if (userBasicInfo == null)
//                        {
//                            userBasicInfo = new UserBasicInfoDTO();
//                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(userID, userBasicInfo);
//                        }
//                        HelperMethodsModel.BindAddFriendDetails(_JobjFromResponse, userBasicInfo);
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userBasicInfo.UserIdentity] = userBasicInfo;
//                    }
//                    lock (FriendDictionaries.Instance.UTID_UID_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UTID_UID_DICTIONARY[userBasicInfo.UserTableId] = userBasicInfo.UserIdentity;
//                    }

//                    lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(userBasicInfo.UserTableId);
//                    }
//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userBasicInfo.UserIdentity);
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.UI_ProcessPushAddFriend(userBasicInfo);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessPushAddFriend ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessPushAcceptFriend()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && server_packet_id != 0)
//                {
//                    UserBasicInfoDTO userBasicInfo = null;
//                    long userID = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userID, out userBasicInfo);
//                        if (userBasicInfo == null)
//                        {
//                            userBasicInfo = new UserBasicInfoDTO();
//                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(userID, userBasicInfo);
//                        }
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userBasicInfo.UserIdentity] = userBasicInfo;
//                    }

//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                    {
//                        userBasicInfo.UserIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    }
//                    if (_JobjFromResponse[JsonKeys.FullName] != null)
//                    {
//                        userBasicInfo.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                    }
//                    if (_JobjFromResponse[JsonKeys.ProfileImage] != null)
//                    {
//                        userBasicInfo.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
//                    }
//                    if (_JobjFromResponse[JsonKeys.ProfileImageId] != null)
//                    {
//                        userBasicInfo.ProfileImageId = (long)_JobjFromResponse[JsonKeys.ProfileImageId];
//                    }
//                    if (_JobjFromResponse[JsonKeys.FriendshipStatus] != null)
//                    {
//                        userBasicInfo.FriendShipStatus = (int)_JobjFromResponse[JsonKeys.FriendshipStatus];
//                    }
//                    if (_JobjFromResponse[JsonKeys.UpdateTime] != null)
//                    {
//                        userBasicInfo.UpdateTime = (long)_JobjFromResponse[JsonKeys.UpdateTime];
//                    }
//                    if (userBasicInfo.BlockedBy != 1)
//                    {
//                        if (_JobjFromResponse[JsonKeys.CallAccess] != null)
//                        {
//                            userBasicInfo.CallAccess = (int)_JobjFromResponse[JsonKeys.CallAccess];
//                        }
//                        if (_JobjFromResponse[JsonKeys.ChatAccess] != null)
//                        {
//                            userBasicInfo.ChatAccess = (int)_JobjFromResponse[JsonKeys.ChatAccess];
//                        }
//                        if (_JobjFromResponse[JsonKeys.FeedAccess] != null)
//                        {
//                            userBasicInfo.FeedAccess = (int)_JobjFromResponse[JsonKeys.FeedAccess];
//                        }
//                    }
//                    else
//                    {
//                        userBasicInfo.CallAccess = 0;
//                        userBasicInfo.ChatAccess = 0;
//                        userBasicInfo.FeedAccess = 0;
//                    }

//                    List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
//                    userBasicInfoList.Add(userBasicInfo);
//                    new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();

//                    HelperMethodsAuth.AuthHandlerInstance.UI_AcceptFriendRequest(userBasicInfo);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessPushAddFriend ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        #endregion"Contact List"

//        #region Image Related

//        private void ProcessUpdateEditImageComment()
//        {//{"sucs":true,"cmnId":655,"uId":"2110010304","cmn":"2s","fn":"Sirat Test2","imgId":1440,"sc":false}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    string cmnt = (string)_JobjFromResponse[JsonKeys.Comment];
//                    CommentDTO comment = null;
//                    lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                    {
//                        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId) && NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(commentId, out comment))
//                        {
//                            comment.Comment = (string)_JobjFromResponse[JsonKeys.Comment];
//                            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentId] = comment;
//                        }
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.EditImageComment(imageId, commentId, cmnt, nfid, (_JobjFromResponse[JsonKeys.ShowContinue] != null && (bool)_JobjFromResponse[JsonKeys.ShowContinue]), true);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateEditImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateDeleteImageComment()
//        {// {"sucs":true,"cmnId":686,"uId":"2110010304","fn":"Sirat Test2","imgId":1418,"sc":false,"loc":8}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                    {
//                        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imgId))
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imgId].ContainsKey(commentId))
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imgId].Remove(commentId);
//                            }
//                        }
//                    }
//                    //  List<CommentDTO> list = new List<CommentDTO>();
//                    // if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imgId)){
//                    // list.AddRange(NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imgId].Values.ToList());
//                    //HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imgId, list);
//                    HelperMethodsAuth.AuthHandlerInstance.DeleteImageComment(imgId, commentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateDeleteImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateAddImageComment()
//        {
//            //{"sucs":true,"cmnId":685,"tm":1442831123023,"uId":"2110011654","cmn":"12","fn":"sam6211","imgId":1440,"sc":false,"loc":5}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    long loc = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
//                    CommentDTO comment = HelperMethodsModel.BindCommentDetails(_JobjFromResponse);
//                    UserBasicInfoDTO user = null;
//                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(comment.UserIdentity, out user))
//                    {
//                        comment.ProfileImage = user.ProfileImage;
//                    }
//                    CommentDTO comntDto = null;
//                    lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                    {
//                        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId))
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(comment.CommentId, out comntDto))
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][comntDto.CommentId] = comment;
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);
//                            }
//                        }
//                        else
//                        {
//                            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.Add(imageId, new Dictionary<long, CommentDTO>());
//                            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);

//                        }
//                    }
//                    //  List<CommentDTO> list = new List<CommentDTO>();
//                    // list.AddRange(NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Values.ToList());
//                    //List<CommentDTO> tmp = list.OrderByDescending(p => p.Time).ToList();
//                    //HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId,tmp);
//                    HelperMethodsAuth.AuthHandlerInstance.AddImageComment(imageId, comment, nfid, true);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateAddImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeUnlikeImageComment()
//        {
//            //{"uId":"2110010304","sucs":true,"fn":"Sirat Test2","id":109,"cmnId":635,"tm":1442815321235,"imgId":1413,"lkd":1,"loc":1}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ImageId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.Liked] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    int liked = (int)_JobjFromResponse[JsonKeys.Liked];
//                    UserBasicInfoDTO user = new UserBasicInfoDTO();
//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                        user.UserIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    if (_JobjFromResponse[JsonKeys.FullName] != null)
//                        user.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                    UserBasicInfoDTO tmp = null;
//                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                    {
//                        user.FriendShipStatus = tmp.FriendShipStatus;
//                    }
//                    else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                    {
//                        user.FriendShipStatus = -1; //for myself
//                    }
//                    if (cmntId > 0)
//                    {
//                        lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                        {
//                            Dictionary<long, CommentDTO> commentsOfthisFeed = null;
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.TryGetValue(imgId, out commentsOfthisFeed))
//                            {
//                                CommentDTO dto = null;
//                                if (commentsOfthisFeed.TryGetValue(cmntId, out dto))
//                                {
//                                    if (liked == 1) { dto.TotalLikeComment++; dto.ILikeComment = 1; }
//                                    else if (dto.TotalLikeComment > 0) { dto.TotalLikeComment--; dto.ILikeComment = 0; }
//                                }
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateImageCommentLikeUnlike(imgId, cmntId, liked, user, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeUnlikeImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeUnlikeImage()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ImageId] != null && _JobjFromResponse[JsonKeys.Liked] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    int liked = (int)_JobjFromResponse[JsonKeys.Liked];
//                    UserBasicInfoDTO user = new UserBasicInfoDTO();
//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                        user.UserIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    if (_JobjFromResponse[JsonKeys.FullName] != null)
//                        user.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                    UserBasicInfoDTO tmp = null;
//                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                    {
//                        user.FriendShipStatus = tmp.FriendShipStatus;
//                    }
//                    else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                    {
//                        user.FriendShipStatus = -1; //for myself
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateImageLikeUnlike(imgId, liked, user, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeUnlikeImage ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessEditImageComment() //194
//        {
//            //{"sucs":true,"cmnId":547,"cmn":"chup","fn":"Smite.Bakasura.Desk","imgId":67,"sc":false}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null)
//                {
//                    if ((bool)_JobjFromResponse[JsonKeys.Success])
//                    {
//                        long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                        long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                        string cmnt = (string)_JobjFromResponse[JsonKeys.Comment];
//                        CommentDTO comment = null;
//                        lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(commentId, out comment))
//                            {
//                                comment.Comment = cmnt;
//                            }
//                        }
//                        HelperMethodsAuth.AuthHandlerInstance.EditImageComment(imageId, commentId, cmnt, nfid, (_JobjFromResponse[JsonKeys.ShowContinue] != null && (bool)_JobjFromResponse[JsonKeys.ShowContinue]));
//                    }
//                    else
//                    {
//                        string msg = "Failed to Comment!";
//                        if (_JobjFromResponse[JsonKeys.Message] != null)
//                            msg = (string)_JobjFromResponse[JsonKeys.Message];
//                        HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(msg, "Failed!");
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessEditImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessAddImageComment() //180
//        {
//            // {"sucs":true,"cmnId":547,"tm":1442491058781,"cmn":"asddsa","fn":"Smite.Bakasura.Desk","imgId":67,"sc":false,"loc":4}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null)
//                {
//                    if ((bool)_JobjFromResponse[JsonKeys.Success])
//                    {
//                        long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                        long loc = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
//                        CommentDTO comment = HelperMethodsModel.BindCommentDetails(_JobjFromResponse);
//                        comment.UserIdentity = DefaultSettings.LOGIN_USER_ID;
//                        comment.ProfileImage = DefaultSettings.userProfile.ProfileImage;
//                        CommentDTO comntDto = null;
//                        lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId))
//                            {
//                                if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(comment.CommentId, out comntDto))
//                                {
//                                    NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][comntDto.CommentId] = comment;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);
//                                }
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.Add(imageId, new Dictionary<long, CommentDTO>());
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);

//                            }
//                        }
//                        HelperMethodsAuth.AuthHandlerInstance.AddImageComment(imageId, comment, nfid);
//                    }
//                    else
//                    {
//                        string msg = "Failed to Comment!";
//                        if (_JobjFromResponse[JsonKeys.Message] != null)
//                            msg = (string)_JobjFromResponse[JsonKeys.Message];
//                        HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(msg, "Failed!");
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessAddImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessLikeUnlikeImageComment() //197
//        {
//            //{"sucs":true,"id":84,"cmnId":546,"tm":1442491530675,"imgId":67,"lkd":1,"loc":1}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null)
//                {
//                    if ((bool)_JobjFromResponse[JsonKeys.Success])
//                    {
//                        long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                        long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                        lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId))
//                            {
//                                if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].ContainsKey(commentId))
//                                {
//                                    CommentDTO comment = NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentId];
//                                    comment.ILikeComment = (short)_JobjFromResponse[JsonKeys.Liked];
//                                    comment.TotalLikeComment = (int)_JobjFromResponse[JsonKeys.LikeOrComment];
//                                    NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentId] = comment;
//                                }
//                            }
//                        }
//                        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId)
//                            && NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].ContainsKey(commentId))
//                            HelperMethodsAuth.AuthHandlerInstance.LikeImageComment(nfid, imageId, NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentId]);
//                        //List<CommentDTO> list = NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Values.ToList();
//                        //HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId, list);
//                    }
//                    else
//                    {
//                        string msg = "Failed to Like/Unlike!";
//                        if (_JobjFromResponse[JsonKeys.Message] != null)
//                            msg = (string)_JobjFromResponse[JsonKeys.Message];
//                        HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(msg, "Failed!");
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessLikeUnlikeImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessLikersImage() // 93
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ImageId] != null)
//                {
//                    //ImageLikesSequenceCount++;
//                    string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        List<UserBasicInfoDTO> users = new List<UserBasicInfoDTO>();
//                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        foreach (JObject userObj in jArray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (userObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)userObj[JsonKeys.UserIdentity];
//                            if (userObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)userObj[JsonKeys.FullName];
//                            if (userObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)userObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            //user.FriendShipStatus = (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp)) ? tmp.FriendShipStatus : -1; //-1 for self
//                            //lock (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP)
//                            //{
//                            //    if (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.ContainsKey(user.UserIdentity))
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP[user.UserIdentity] = user;
//                            //    else
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.Add(user.UserIdentity, user);
//                            //}
//                            users.Add(user);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadImageLikes(imgId, users);
//                    }
//                    //  if (ImageLikesSequenceCount == seqTotal)
//                    // {
//                    //     ImageLikesSequenceCount = 0;
//                    //HelperMethodsAuth.NewsFeedHandlerInstance.LoadImageLikes(imgId, users);
//                    //}//
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessLikersImage ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessLikersImageComment() //196
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    //LikesSequnceCount++;
//                    ImageCommentLikesSequeceCount++;
//                    string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        List<UserBasicInfoDTO> users = new List<UserBasicInfoDTO>();
//                        foreach (JObject userObj in jArray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (userObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)userObj[JsonKeys.UserIdentity];
//                            if (userObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)userObj[JsonKeys.FullName];
//                            if (userObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)userObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            //user.FriendShipStatus = (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp)) ? tmp.FriendShipStatus : -1; //-1 for self
//                            //lock (NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST)
//                            //{
//                            //    if (NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST.ContainsKey(user.UserIdentity))
//                            //        NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST[user.UserIdentity] = user;
//                            //    else
//                            //        NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST.Add(user.UserIdentity, user);
//                            //}
//                            users.Add(user);
//                        }
//                        HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentLikersFromServer(users, (long)_JobjFromResponse[JsonKeys.ImageId], (long)_JobjFromResponse[JsonKeys.CommentId]);
//                    }
//                    //  if (ImageCommentLikesSequeceCount == seqTotal)
//                    // {
//                    //      ImageCommentLikesSequeceCount = 0;
//                    //HandlerMehod
//                    //      List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();

//                    //      list.AddRange(NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST.Values.ToList());

//                    //      HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentLikersFromServer(list, (long)_JobjFromResponse[JsonKeys.ImageId], (long)_JobjFromResponse[JsonKeys.CommentId]);
//                    //    NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST.Clear();
//                    //}
//                }
//                else
//                {
//                    HelperMethodsAuth.AuthHandlerInstance.OnLikerListLoadFailed();
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessLikersImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessImageComments()
//        {
//            //throw new NotImplementedException();
//            //System.Diagnostics.Debug.WriteLine(_JobjFromResponse);
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null)
//                {
//                    if ((bool)_JobjFromResponse[JsonKeys.Success]) // found
//                    {
//                        long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
//                        int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
//                        long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                        //Dictionary<string, Dictionary<long, CommentDTO>> TEMP_DICTIONARY = new Dictionary<string, Dictionary<long, CommentDTO>>(); //<sequence , <commentId, CommentDTO>>

//                        if (_JobjFromResponse[JsonKeys.Comments] != null)
//                        {
//                            List<CommentDTO> list = new List<CommentDTO>();
//                            JArray commentList = (JArray)_JobjFromResponse[JsonKeys.Comments];
//                            foreach (JObject comment in commentList)
//                            {
//                                CommentDTO commentDTO = HelperMethodsModel.BindCommentDetails(comment);
//                                list.Add(commentDTO);
//                                //if (TEMP_DICTIONARY.ContainsKey(sequence))
//                                //{
//                                //    if (TEMP_DICTIONARY[sequence].ContainsKey(commentDTO.CommentId)) // overwrite previous dto
//                                //    {
//                                //        TEMP_DICTIONARY[sequence][commentDTO.CommentId] = commentDTO;
//                                //    }
//                                //    else // add new dto
//                                //    {
//                                //        TEMP_DICTIONARY[sequence].Add(commentDTO.CommentId, commentDTO);
//                                //    }
//                                //}
//                                //else//init dictionary first time
//                                //{
//                                //    TEMP_DICTIONARY.Add(sequence, new Dictionary<long, CommentDTO>());
//                                //    TEMP_DICTIONARY[sequence].Add(commentDTO.CommentId, commentDTO);
//                                //}

//                                ////if (ImageCommentsSequenceCount == seqTotal)
//                                ////{
//                                ////    List<CommentDTO> list = new List<CommentDTO>();
//                                ////    list.AddRange(NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Values.ToList());
//                                ////    HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId, list);
//                                ////}
//                                //lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                                //{
//                                //    if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId))
//                                //    {
//                                //        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].ContainsKey(commentDTO.CommentId))
//                                //            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentDTO.CommentId] = commentDTO;
//                                //        else
//                                //            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(commentDTO.CommentId, commentDTO);
//                                //    }
//                                //    else
//                                //    {
//                                //        NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.Add(imageId, new Dictionary<long, CommentDTO>());
//                                //        NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(commentDTO.CommentId, commentDTO);
//                                //    }
//                                //}
//                            }
//                            HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId, list, nfid);
//                        }
//                        // ImageCommentsSequenceCount++;

//                        //if (ImageCommentsSequenceCount == seqTotal)
//                        //{
//                        //ImageCommentsSequenceCount = 0;

//                        //list.AddRange(NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Values.ToList());
//                        //HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId, list, nfid);
//                        //TEMP_DICTIONARY.Clear();
//                        //  }
//                    }
//                    /*else
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.DisableLoaderAnimation();
//                        string msg = "Unable to Load Comments!";
//                        if (_JobjFromResponse[JsonKeys.Message] != null)
//                        {
//                            msg = (string)_JobjFromResponse[JsonKeys.Message];
//                        }
//                        long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        long imgId = (_JobjFromResponse[JsonKeys.ImageId] != null) ? (long)_JobjFromResponse[JsonKeys.ImageId] : 0;
//                        HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imgId, null, nfId);
//                    }*/
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessImageComments ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        //private void ProcessSingleImageDetail()
//        //{
//        //    try
//        //    {
//        //        ImageDTO imageDto = new ImageDTO();

//        //        //{"imgId":192,"iurl":"2110063361/1442288972579.jpg","cptn":"","ih":360,"iw":640,"imT":1,"albId":"default","albn":"Feed Photos","tm":1442288974849,"nl":0,"il":0,"ic":1,"nc":7,"sucs":true,"uId":"2110063361","fn":"smite.bakasura.desktop.2"}
//        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//        //        {
//        //            imageDto = HelperMethodsModel.BindImageDetails(_JobjFromResponse);
//        //        }
//        //        else
//        //        {
//        //            imageDto.ImageUrl = NotificationMessages.CONTENT_DOES_NOT_EXIST;
//        //        }
//        //        HelperMethodsAuth.AuthHandlerInstance.GetSingleImageDTOFromServer(imageDto);
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        log.Error("ProcessSingleImageDetail e ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//        //    }
//        //}

//        #endregion

//        #region NewsFeed

//        private void ProcessUpdateUnLikeComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                    {
//                        Dictionary<long, CommentDTO> commentsOfthisFeed = null;
//                        if (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out commentsOfthisFeed))
//                        {
//                            CommentDTO dto = null;
//                            if (commentsOfthisFeed.TryGetValue(cmntId, out dto))
//                            {
//                                if (dto.TotalLikeComment > 0) { dto.TotalLikeComment--; dto.ILikeComment = 0; }
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateCommentUnLike(nfId, cmntId, friendId, circleId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateUnLikeComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                    {
//                        Dictionary<long, CommentDTO> commentsOfthisFeed = null;
//                        if (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out commentsOfthisFeed))
//                        {
//                            CommentDTO dto = null;
//                            if (commentsOfthisFeed.TryGetValue(cmntId, out dto))
//                            {
//                                dto.TotalLikeComment++; dto.ILikeComment = 1;
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateCommentLike(nfId, cmntId, friendId, circleId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateDeleteStatusComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ActivistId] != null
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long auId = (long)_JobjFromResponse[JsonKeys.ActivistId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    Dictionary<long, CommentDTO> CommentsByNfid = null;
//                    lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                    {
//                        if (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out CommentsByNfid))
//                        {
//                            if (CommentsByNfid.ContainsKey(cmntId))
//                            {
//                                CommentsByNfid.Remove(cmntId);
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.DeleteFeedComment(nfId, friendId, circleId, cmntId, auId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateDeleteStatusComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateAddStatusComment()
//        {
//            // {"sucs":true,"cmnId":1951,"tm":1442225637709,"uId":"2110010304","cmn":"2","fn":"Sirat Test2","nfId":128,"auId":245,"nc":0,"rc":0,"sc":false,"loc":1}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ActivistId] != null
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.Comment] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    CommentDTO comment = HelperMethodsModel.BindCommentDetails(_JobjFromResponse);
//                    UserBasicInfoDTO user = null;
//                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(comment.UserIdentity, out user))
//                    {
//                        comment.ProfileImage = user.ProfileImage;
//                    }
//                    long auId = (long)_JobjFromResponse[JsonKeys.ActivistId];
//                    HelperMethodsAuth.NewsFeedHandlerInstance.AddFeedComment(comment, nfId, friendId, circleId, auId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateAddStatusComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateEditComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.Comment] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    string cmnt = (string)_JobjFromResponse[JsonKeys.Comment];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    HelperMethodsAuth.NewsFeedHandlerInstance.EditFeedComment(cmnt, cmntId, nfId, friendId, circleId, (_JobjFromResponse[JsonKeys.ShowContinue] != null && (bool)_JobjFromResponse[JsonKeys.ShowContinue]));
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateEditComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeStatus(bool MyLike = false)
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.ActivistId] != null && _JobjFromResponse[JsonKeys.LikeOrComment] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long auId = (long)_JobjFromResponse[JsonKeys.ActivistId];
//                    long nl = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    UserBasicInfoDTO user = null;
//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null && _JobjFromResponse[JsonKeys.FullName] != null)
//                    {
//                        user = new UserBasicInfoDTO();
//                        if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                            user.UserIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                        if (_JobjFromResponse[JsonKeys.FullName] != null)
//                            user.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                        UserBasicInfoDTO tmp = null;
//                        if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                        {
//                            user.FriendShipStatus = tmp.FriendShipStatus;
//                        }
//                        else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                        {
//                            user.FriendShipStatus = -1; //for myself
//                        }
//                    }
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        FeedDTO feed = null;
//                        if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfId, out feed))
//                        {
//                            feed.NumberOfLikes++;
//                            if (MyLike) feed.ILike = 1;
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateStatusLike(MyLike, nfId, friendId, circleId, auId, user, nl);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateUnlikeStatus(bool MyLike = false)
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.ActivistId] != null && _JobjFromResponse[JsonKeys.LikeOrComment] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long auId = (long)_JobjFromResponse[JsonKeys.ActivistId];
//                    long nl = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
//                    long ut = (_JobjFromResponse[JsonKeys.UpdateTime] != null) ? (long)_JobjFromResponse[JsonKeys.UpdateTime] : 0;
//                    long uId = (_JobjFromResponse[JsonKeys.UserIdentity] != null) ? (long)_JobjFromResponse[JsonKeys.UserIdentity] : 0;
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        FeedDTO feed = null;
//                        if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfId, out feed))
//                        {
//                            if (feed.NumberOfLikes > 0)
//                            {
//                                feed.NumberOfLikes--;
//                                if (MyLike) feed.ILike = 0;
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateStatusUnlike(MyLike, nfId, friendId, circleId, auId, ut, uId, nl);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateUnlikeStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessFetchLikesForComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    //LikesSequnceCount++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    int seqTotal = Convert.ToInt32(str.Split(delimiterChars)[1]);
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];

//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        List<UserBasicInfoDTO> users = new List<UserBasicInfoDTO>();
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            if (singleObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)singleObj[JsonKeys.FullName];
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            //lock (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP)
//                            //{
//                            //    if (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.ContainsKey(user.UserIdentity))
//                            //    {
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP[user.UserIdentity] = user;
//                            //    }
//                            //    else
//                            //    {
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.Add(user.UserIdentity, user);
//                            //    }
//                            //}
//                            users.Add(user);
//                        }

//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadStatusLikes(nfId, users);
//                    }
//                    // log.Info("LIKE CONTACT LIST.." + LikesSequnceCount + "SEQTOTAL.." + seqTotal);
//                    //if (LikesSequnceCount == seqTotal)
//                    //{
//                    //    LikesSequnceCount = 0;
//                    //}
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessFetchLikesForStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessCommentsForStatus()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    //CommentsSeqCount++;
//                    //Dictionary<long, CommentDTO> CommentsByNfid = null;
//                    //string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    //char[] delimiterChars = { '/' };
//                    //int seqTotal = Convert.ToInt32(str.Split(delimiterChars)[1]);
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    List<CommentDTO> list = new List<CommentDTO>();
//                    if (_JobjFromResponse[JsonKeys.Comments] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Comments];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            CommentDTO comment = HelperMethodsModel.BindCommentDetails(singleObj);
//                            list.Add(comment);
//                            //lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                            //{
//                            //    if (!NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out CommentsByNfid))
//                            //    {
//                            //        CommentsByNfid = new Dictionary<long, CommentDTO>();
//                            //        NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.Add(nfId, CommentsByNfid);
//                            //    }
//                            //    CommentsByNfid[comment.CommentId] = comment;
//                            //}
//                        }
//                    }
//                    //log.Info("COMMENT." + CommentsSeqCount + "SEQTOTAL.." + seqTotal);
//                    //if (CommentsSeqCount == seqTotal)
//                    //{
//                    //    CommentsSeqCount = 0;
//                    if (list.Count > 0)
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadFeedComments(list, nfId, friendId, circleId);
//                    //}
//                }
//                //else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false && _JobjFromResponse[JsonKeys.NewsfeedId] != null)// no more data
//                //{
//                //    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                //    HelperMethodsAuth.NewsFeedHandlerInstance.LoadFeedComments(null, nfId, 0, 0);
//                //}
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessCommentsForStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessTypeFriendPresenceInfo()
//        {
//            try
//            {
//                if (_JobjFromResponse != null)
//                {
//                    long friendIdentity = 0;
//                    if (_JobjFromResponse[JsonKeys.FriendId] != null)
//                    {
//                        friendIdentity = (long)_JobjFromResponse[JsonKeys.FriendId];
//                    }
//                    else if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                    {
//                        friendIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    }

//                    UserBasicInfoDTO _FriendDetailsInDictionary = null;
//                    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(friendIdentity, out _FriendDetailsInDictionary);

//                    if (_FriendDetailsInDictionary != null)
//                    {
//                        if (_JobjFromResponse[JsonKeys.Presence] != null)
//                        {
//                            _FriendDetailsInDictionary.Presence = (int)_JobjFromResponse[JsonKeys.Presence];
//                        }
//                        if (_JobjFromResponse[JsonKeys.LastOnlineTime] != null)
//                        {
//                            _FriendDetailsInDictionary.LastOnlineTime = (long)_JobjFromResponse[JsonKeys.LastOnlineTime];
//                        }
//                        if (_JobjFromResponse[JsonKeys.Device] != null)
//                        {
//                            _FriendDetailsInDictionary.Device = (int)_JobjFromResponse[JsonKeys.Device];
//                        }
//                        if (_JobjFromResponse[JsonKeys.Mood] != null)
//                        {
//                            _FriendDetailsInDictionary.Mood = (int)_JobjFromResponse[JsonKeys.Mood];
//                        }
//                        //HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(_FriendDetailsInDictionary);
//                        HelperMethodsAuth.AuthHandlerInstance.UI_ProcessTypeFriendPresenceInfo(_FriendDetailsInDictionary);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessTypeFriendPresenceInfo ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }

//        private void ProcessFetchLikesForStatus()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    LikesSequnceCount++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    int seqTotal = Convert.ToInt32(str.Split(delimiterChars)[1]);
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];

//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            if (singleObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)singleObj[JsonKeys.FullName];
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            list.Add(user);
//                            //lock (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP)
//                            //{
//                            //    if (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.ContainsKey(user.UserIdentity))
//                            //    {
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP[user.UserIdentity] = user;
//                            //    }
//                            //    else
//                            //    {
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.Add(user.UserIdentity, user);
//                            //    }
//                            //}
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadStatusLikes(nfId, list);
//                    }
//                    // log.Info("LIKE CONTACT LIST.." + LikesSequnceCount + "SEQTOTAL.." + seqTotal);
//                    //if (LikesSequnceCount == seqTotal)
//                    //{
//                    LikesSequnceCount = 0;
//                    //     HelperMethodsAuth.NewsFeedHandlerInstance.LoadStatusLikes(nfId);
//                    //}
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessFetchLikesForStatus ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        private void ProcessUpdateAddStatus(bool AddStatus = false) // 377
//        {
//            try
//            {

//                if (_JobjFromResponse != null)
//                {
//                    FeedDTO singleFeed = null;
//                    if (_JobjFromResponse[JsonKeys.NewsFeed] != null)
//                    {
//                        singleFeed = HelperMethodsModel.BindFeedDetails((JObject)_JobjFromResponse[JsonKeys.NewsFeed]);
//                    }
//                    else
//                    {
//                        singleFeed = HelperMethodsModel.BindFeedDetails(_JobjFromResponse);
//                    }

//                    if (singleFeed.MediaContent != null && singleFeed.MediaContent.MediaList != null && singleFeed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                    {
//                        HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(singleFeed.MediaContent.MediaList.ElementAt(0));
//                        if (AddStatus)
//                        {
//                            singleFeed.ILike = singleFeed.IComment = singleFeed.IShare = 0;
//                            SingleMediaDTO dt = singleFeed.MediaContent.MediaList[0];
//                            dt.IShare = dt.ILike = dt.IComment = 0;
//                        }
//                    }
//                    if (singleFeed.WhoShare != null)
//                    {
//                        FeedDTO whoShareFeed = singleFeed.WhoShare;
//                        whoShareFeed.ParentFeed = singleFeed;

//                        if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.UserIdentity)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendId)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.UserIdentity)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendId)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.NewsfeedId)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.ParentFeed.NewsfeedId))
//                        {

//                            lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                            {
//                                if (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.ContainsKey(whoShareFeed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[whoShareFeed.NewsfeedId] = singleFeed.NewsfeedId;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.Add(whoShareFeed.NewsfeedId, singleFeed.NewsfeedId);
//                                }
//                            }

//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(whoShareFeed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[whoShareFeed.NewsfeedId] = whoShareFeed;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Add(whoShareFeed.NewsfeedId, whoShareFeed);
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.UpdateShareStatus(whoShareFeed);

//                            if (whoShareFeed.UserIdentity == DefaultSettings.LOGIN_USER_ID || whoShareFeed.FriendId == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, whoShareFeed, DefaultSettings.FEED_TYPE_MY, 0);
//                            }
//                            if (whoShareFeed.UserIdentity != DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(whoShareFeed.UserIdentity))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[whoShareFeed.UserIdentity].InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(whoShareFeed.UserIdentity, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[whoShareFeed.UserIdentity].InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                    }
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, whoShareFeed, DefaultSettings.FEED_TYPE_FRIEND, whoShareFeed.UserIdentity);
//                            }

//                            if (whoShareFeed.FriendId > 0 && whoShareFeed.FriendId != DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(whoShareFeed.FriendId))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[whoShareFeed.FriendId].InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(whoShareFeed.FriendId, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[whoShareFeed.FriendId].InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                    }
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, whoShareFeed, DefaultSettings.FEED_TYPE_FRIEND, whoShareFeed.FriendId);
//                            }


//                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.Time, whoShareFeed.FeedCategory);
//                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, whoShareFeed, DefaultSettings.FEED_TYPE_ALL, 0);

//                        }
//                    }
//                    else
//                    {
//                        if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(singleFeed.UserIdentity)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(singleFeed.FriendId)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(singleFeed.NewsfeedId))
//                        {

//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(singleFeed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[singleFeed.NewsfeedId] = singleFeed;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Add(singleFeed.NewsfeedId, singleFeed);
//                                }
//                            }

//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.Time, singleFeed.FeedCategory);
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_ALL, 0);

//                            if (singleFeed.UserIdentity == DefaultSettings.LOGIN_USER_ID || singleFeed.FriendId == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_MY, 0);
//                            }

//                            if (singleFeed.UserIdentity != DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(singleFeed.UserIdentity))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.UserIdentity].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(singleFeed.UserIdentity, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.UserIdentity].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                }

//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_FRIEND, singleFeed.UserIdentity);
//                            }

//                            if (singleFeed.FriendId > 0 && singleFeed.FriendId != DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(singleFeed.FriendId))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.FriendId].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(singleFeed.FriendId, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.FriendId].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_FRIEND, singleFeed.FriendId);
//                            }
//                            if (singleFeed.GroupId > 0)
//                            {
//                                lock (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(singleFeed.GroupId))
//                                    {
//                                        NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.GroupId].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(singleFeed.GroupId, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.GroupId].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_CIRCLE, singleFeed.GroupId);
//                            }

//                            //HelperMethodsAuth.NewsFeedHandlerInstance.UpdateAddStatus(singleFeed);
//                        }
//                    }


//                }

//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateAddStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateEditStatus()
//        {
//            try
//            {
//                if (_JobjFromResponse != null)
//                {
//                    FeedDTO singleFeed = HelperMethodsModel.BindFeedDetails(_JobjFromResponse);
//                    string sts = (_JobjFromResponse[JsonKeys.Status] == null) ? null : singleFeed.Status;
//                    FeedDTO prevFeed;

//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(singleFeed.NewsfeedId, out prevFeed);

//                        if (prevFeed != null)
//                        {
//                            prevFeed.Status = singleFeed.Status;
//                        }

//                    }
//                    List<long> taggedIds = null, removedTags = null;
//                    if (singleFeed.TaggedFriendsList != null && singleFeed.TaggedFriendsList.Count > 0)
//                    {
//                        taggedIds = new List<long>();
//                        foreach (var item in singleFeed.TaggedFriendsList)
//                        {
//                            taggedIds.Add(item.UserTableId);
//                        }
//                    }
//                    if (_JobjFromResponse[JsonKeys.RemovedTagsFriendUtIds] != null)
//                    {
//                        removedTags = new List<long>();
//                        JArray array = (JArray)_JobjFromResponse[JsonKeys.RemovedTagsFriendUtIds];
//                        for (int j = 0; j < array.Count; j++)
//                        {
//                            removedTags.Add((long)array.ElementAt(j));
//                        }
//                    }
//                    else removedTags = taggedIds;
//                    HelperMethodsAuth.NewsFeedHandlerInstance.EditNewsFeed(true, singleFeed.NewsfeedId, sts, singleFeed.StatusTags, singleFeed.FriendId, singleFeed.GroupId, taggedIds, removedTags, singleFeed.locationDTO, singleFeed.Privacy, singleFeed.TaggedFriendsList);
//                    //HelperMethodsAuth.NewsFeedHandlerInstance.UpdateEditStatus(singleFeed);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateEditStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateDeleteStatus()
//        {
//            // {"actn":379,"nfId":666,"sucs":true,"pckFs":23446,"fn":"Shahadat","ln":"Hossain"}
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Remove(nfId);
//                    }
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                    {
//                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.RemoveData(nfId);

//                    }
//                    lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                    {
//                        NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.Remove(nfId);
//                    }

//                    if (friendId > 0 && NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(friendId))
//                    {
//                        lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                        {
//                            NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[friendId].RemoveData(nfId);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.RemoveSingleFeedUI(nfId, DefaultSettings.FEED_TYPE_FRIEND, friendId);
//                    }
//                    if (circleId > 0)
//                    {
//                        lock (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                        {
//                            NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[circleId].RemoveData(nfId);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.RemoveSingleFeedUI(nfId, DefaultSettings.FEED_TYPE_CIRCLE, circleId);
//                    }

//                    HelperMethodsAuth.NewsFeedHandlerInstance.RemoveSingleFeedUI(nfId, DefaultSettings.FEED_TYPE_ALL);

//                    //HelperMethodsAuth.NewsFeedHandlerInstance.UpdateDeleteStatus(nfId, friendId, circleId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateDeleteStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessFriendNewsFeeds() // 110
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    long _FriendIdentity = (long)_JobjFromResponse[JsonKeys.FriendId];
//<<<<<<< .mine
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {

//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            if (feed.WhoShare != null)
//                            {
//                                feed.WhoShare.ParentFeed = feed;
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.WhoShare.NewsfeedId] = feed.WhoShare;
//                                }
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(_FriendIdentity))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[_FriendIdentity].InsertDataFromServer(feed.WhoShare.NewsfeedId, feed.WhoShare.ActualTime, feed.WhoShare.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(_FriendIdentity, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[_FriendIdentity].InsertDataFromServer(feed.WhoShare.NewsfeedId, feed.WhoShare.ActualTime, feed.WhoShare.FeedCategory);
//                                    }

//                                }

//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(0, feed.WhoShare, DefaultSettings.FEED_TYPE_FRIEND, _FriendIdentity);

//                            }
//                            else
//                            {
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                                }

//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(_FriendIdentity))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[_FriendIdentity].InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(_FriendIdentity, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[_FriendIdentity].InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                    }

//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(0, feed, DefaultSettings.FEED_TYPE_FRIEND, _FriendIdentity);
//                            }
//                        }

//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessFriendNewsFeeds ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//<<<<<<< .mine
//        private void ProcessMyBook() //94
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {

//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            if (feed.WhoShare != null)
//                            {
//                                feed.WhoShare.ParentFeed = feed;

//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.WhoShare.NewsfeedId] = feed.WhoShare;
//                                }
//                                lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.WhoShare.NewsfeedId, feed.WhoShare.ActualTime, feed.WhoShare.FeedCategory);
//                                }

//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(0, feed.WhoShare, DefaultSettings.FEED_TYPE_MY);
//                            }
//                            else
//                            {
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                                }
//                                lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(0, feed, DefaultSettings.FEED_TYPE_MY);
//=======
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            //OBSOLETE CHECK 
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//                            lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(SettingsConstants.FEED_REGULAR, feed, DefaultSettings.FEED_TYPE_MY);
//                        }
//>>>>>>> .r1602
//                            }
//                        }

//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMyBook ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessMediaFeed()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {

//                    short mediaTrendingType = ((int)_JobjFromResponse[JsonKeys.MediaTrendingFeed] == SettingsConstants.MEDIA_FEED_TYPE_ALL) ? SettingsConstants.MEDIA_FEED_TYPE_ALL : SettingsConstants.MEDIA_FEED_TYPE_TRENDING;

//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int first_feed_state = 0;
//                        if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
//                            first_feed_state = client_packet_id.Equals(DefaultSettings.START_MEDIAFEEDS_PACKETID) ? 1 : 0; //0=not addupdatestatus nor first time,2=addupdatestatus,1=firsttimefeed
//                        else if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_TRENDING)
//                            first_feed_state = client_packet_id.Equals(DefaultSettings.START_MEDIAFEEDSTRENDING_PACKETID) ? 1 : 0; //0=not addupdatestatus nor first time,2=addupdatestatus,1=firsttimefeed

//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }

//                            if (feed.WhoShare != null)
//                            {
//                                FeedDTO whoShareFeed = feed.WhoShare;
//                                whoShareFeed.ParentFeed = feed;

//                                if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.UserIdentity)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendId)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.UserIdentity)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendId)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.NewsfeedId)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.ParentFeed.NewsfeedId))
//                                {

//                                    lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[whoShareFeed.NewsfeedId] = feed.NewsfeedId;
//                                    }

//                                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                    {
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[whoShareFeed.NewsfeedId] = whoShareFeed;
//                                    }

//<<<<<<< .mine
//                                    if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
//                                    {
//                                        NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.Time, whoShareFeed.FeedCategory);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_MEDIA);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(whoShareFeed.NewsfeedId);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                if (feed.FeedCategory == SettingsConstants.SPECIAL_FEED)
//                                {
//                                    if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
//                                    {
//                                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId);
//                                        if (data != null)
//                                        {
//                                            if (data.Tm != feed.Time)
//                                            {
//                                                NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.GetData(feed.NewsfeedId);
//                                        if (data != null)
//                                        {
//                                            if (data.Tm != feed.Time)
//                                            {
//                                                NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(feed.NewsfeedId);
//                                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(feed.NewsfeedId);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
//                                        }
//                                    }
//                                }
//                                else
//                                {
//                                    if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.UserIdentity)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendId)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                    {
//                                        if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
//                                        {
//                                            NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA);
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(feed.NewsfeedId);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//=======
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                //#if AUTH_LOG
//                log.Error("ProcessMediaFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//                //#endif
//            }
//        }
//private void ProcessBreakingNewsPortalFeeds() // 302
//{
//    try
//    {
//        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.BreakingNewsFeeds] != null)
//        {
//            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.BreakingNewsFeeds];
//            List<FeedDTO> breakingNws = new List<FeedDTO>();
//            foreach (JObject singleObj in jarray)
//            {
//                FeedDTO dto = new FeedDTO();
//                dto.NewsPortalInfo = new NewsDTO();
//                dto.NewsfeedId = dto.NewsPortalInfo.NewsId = (singleObj[JsonKeys.Id] != null) ? ((long)(singleObj[JsonKeys.Id])) : 0;
//                dto.ProfileImage = (singleObj[JsonKeys.ProfileImage] != null) ? ((string)(singleObj[JsonKeys.ProfileImage])) : string.Empty;
//                dto.NewsPortalInfo.NewsShortDescription = (singleObj[JsonKeys.NewsShortDescription] != null) ? ((string)(singleObj[JsonKeys.NewsShortDescription])) : string.Empty;
//                dto.NewsPortalInfo.NewsTitle = (singleObj[JsonKeys.NewsPortalTitle] != null) ? ((string)(singleObj[JsonKeys.NewsPortalTitle])) : string.Empty;
//                breakingNws.Add(dto);
//            }
//            HelperMethodsAuth.NewsFeedHandlerInstance.BreakingNewsSliderUI(breakingNws);
//        }
//    }
//    catch (Exception e)
//    {
//        log.Error("ProcessBreakingNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//    }
//}
//        private void ProcessBusinessPageFeeds()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int feed_state = client_packet_id.Equals(DefaultSettings.START_PAGESFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//>>>>>>> .r1602
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                //#if AUTH_LOG
//                log.Error("ProcessMediaFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//                //#endif
//            }
//        }
//        private void ProcessBreakingNewsPortalFeeds() // 302
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.BreakingNewsFeeds] != null)
//                {
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.BreakingNewsFeeds];
//                    List<FeedDTO> breakingNws = new List<FeedDTO>();
//                    foreach (JObject singleObj in jarray)
//                    {
//                        FeedDTO dto = new FeedDTO();
//                        dto.NewsPortalInfo = new NewsDTO();
//                        dto.NewsfeedId = dto.NewsPortalInfo.NewsId = (singleObj[JsonKeys.Id] != null) ? ((long)(singleObj[JsonKeys.Id])) : 0;
//                        dto.ProfileImage = (singleObj[JsonKeys.ProfileImage] != null) ? ((string)(singleObj[JsonKeys.ProfileImage])) : string.Empty;
//                        dto.NewsPortalInfo.NewsShortDescription = (singleObj[JsonKeys.NewsShortDescription] != null) ? ((string)(singleObj[JsonKeys.NewsShortDescription])) : string.Empty;
//                        dto.NewsPortalInfo.NewsTitle = (singleObj[JsonKeys.NewsPortalTitle] != null) ? ((string)(singleObj[JsonKeys.NewsPortalTitle])) : string.Empty;
//                        breakingNws.Add(dto);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.BreakingNewsSliderUI(breakingNws);
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessBreakingNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessBusinessPageFeeds()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int first_feed_state = client_packet_id.Equals(DefaultSettings.START_PAGESFEEDS_PACKETID) ? 1 : 0;
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//OBSOLETE CHECK 

//}
//        private void ProcessNewsPortalFeed() // 295
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int feed_state = client_packet_id.Equals(DefaultSettings.START_NEWSPORTALFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//>>>>>>> .r1602
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//                            //  if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                            //  {
//                            if (feed.NewsPortalInfo != null && feed.NewsPortalInfo.IsSaved)
//                            {
//                                if (NewsFeedDictionaries.Instance.SAVED_PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    NewsFeedDictionaries.Instance.SAVED_PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_PAGE_SAVED);
//                            }
//                            if (NewsFeedDictionaries.Instance.PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                            {
//                                if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_PAGE);
//                                }
//                            }
//                            //}
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessPagesFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsPortalFeed() // 295
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int first_feed_state = client_packet_id.Equals(DefaultSettings.START_NEWSPORTALFEEDS_PACKETID) ? 1 : 0;
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//OBSOLETE CHECK 

//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//                            if (feed.NewsPortalInfo != null && feed.NewsPortalInfo.IsSaved)
//                            {
//                                if (NewsFeedDictionaries.Instance.SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    NewsFeedDictionaries.Instance.SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory, false);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed_state, feed, DefaultSettings.FEED_TYPE_NEWSPORTAL_SAVED);
//                            }
//                            if (NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                            {
//                                if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory, false);
//                                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed_state, feed, DefaultSettings.FEED_TYPE_NEWSPORTAL);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsFeed()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int feed_state = client_packet_id.Equals(DefaultSettings.START_ALLFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//>>>>>>> .r1602
//OBSOLETE CHECK 
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//                            //if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                            //{
//                            if (feed.NewsPortalInfo != null && feed.NewsPortalInfo.IsSaved)
//                            {
//                                if (NewsFeedDictionaries.Instance.SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    NewsFeedDictionaries.Instance.SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_NEWSPORTAL_SAVED);
//                            }
//                            if (NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                            {
//                                if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_NEWSPORTAL);
//                                }
//                            }
//                            // }
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsFeed()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int first_feed_state = client_packet_id.Equals(DefaultSettings.START_ALLFEEDS_PACKETID) ? 1 : 0; //0=not addupdatestatus nor first time,2=addupdatestatus,1=firsttimefeed
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//if (feed.Time > DefaultSettings.MaxAllFeedsTime) DefaultSettings.MaxAllFeedsTime = feed.Time;
//if (feed.Time < DefaultSettings.MinAllFeedsTime) DefaultSettings.MinAllFeedsTime = feed.Time;
//if (feed.WhoShare != null)
//{
//    FeedDTO whoShareFeed = feed.WhoShare;
//    whoShareFeed.ParentFeed = feed;


//                            if (feed.WhoShare != null)
//                            {
//                                FeedDTO whoShareFeed = feed.WhoShare;
//                                whoShareFeed.ParentFeed = feed;

//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(whoShareFeed.NewsfeedId) == null)
//                                {
//                                    if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.UserIdentity)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendId)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.UserIdentity)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendId)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.NewsfeedId)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.ParentFeed.NewsfeedId))
//                                    {

//                                        lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                                        {
//                                            NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[whoShareFeed.NewsfeedId] = feed.NewsfeedId;
//                                        }
//                                        lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                        {
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[whoShareFeed.NewsfeedId] = whoShareFeed;
//                                        }
//=======
//                                        //if (whoShareFeed.Time > DefaultSettings.MaxAllFeedsTime) DefaultSettings.MaxAllFeedsTime = whoShareFeed.Time;
//                                        //if (whoShareFeed.Time < DefaultSettings.MinAllFeedsTime) DefaultSettings.MinAllFeedsTime = whoShareFeed.Time;
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.Time, whoShareFeed.FeedCategory);
//                                        //HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed, DefaultSettings.FEED_TYPE_ALL);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_ALL);
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    if (feed.FeedCategory == SettingsConstants.SPECIAL_FEED)
//                                    {
//                                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId);
//                                        if (data != null)
//                                        {
//                                            if (data.Tm != feed.Time)
//                                            {
//                                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.UserIdentity)
//                                           && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendId)
//                                           && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                        {
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//>>>>>>> .r1544

//                                        if (whoShareFeed.Time > DefaultSettings.MaxAllFeedsTime) DefaultSettings.MaxAllFeedsTime = whoShareFeed.Time;
//                                        if (whoShareFeed.Time < DefaultSettings.MinAllFeedsTime) DefaultSettings.MinAllFeedsTime = whoShareFeed.Time;
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.Time, whoShareFeed.FeedCategory);
//                                        //HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed, DefaultSettings.FEED_TYPE_ALL);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_ALL);
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    if (feed.FeedCategory == SettingsConstants.SPECIAL_FEED)
//                                    {
//                                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId);
//                                        if (data != null)
//                                        {
//                                            if (data.Tm != feed.Time)
//                                            {
//                                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.UserIdentity)
//                                           && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendId)
//                                           && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                        {
//                                            if (feed.Time > DefaultSettings.MaxAllFeedsTime) DefaultSettings.MaxAllFeedsTime = feed.Time;
//                                            if (feed.Time < DefaultSettings.MinAllFeedsTime) DefaultSettings.MinAllFeedsTime = feed.Time;
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                        }
//                                    }
//                                }
//                            }
//                        }

//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("processNewsFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessMediaAlbumList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableId] != null && _JobjFromResponse[JsonKeys.MediaType] != null)
//                {
//                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                    int mediatype = (int)_JobjFromResponse[JsonKeys.MediaType];
//                    if (utid == DefaultSettings.LOGIN_USER_TABLE_ID)
//                    {
//                        int tr = (int)_JobjFromResponse[JsonKeys.TotalRecords];
//                        if (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO) DefaultSettings.MY_AUDIO_ALBUMS_COUNT = tr;
//                        else if (mediatype == SettingsConstants.MEDIA_TYPE_VIDEO) DefaultSettings.MY_VIDEO_ALBUMS_COUNT = tr;
//                    }
//                    if (_JobjFromResponse[JsonKeys.MediaAlbumList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaAlbumList];
//                        if (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO)
//                        {
//                            List<MediaContentDTO> tmpAlbums = new List<MediaContentDTO>();
//                            lock (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID)
//                            {
//                                Dictionary<long, MediaContentDTO> albumsOfaUId = null;
//                                if (!NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.TryGetValue(utid, out albumsOfaUId))
//                                {
//                                    albumsOfaUId = new Dictionary<long, MediaContentDTO>();
//                                    NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.Add(utid, albumsOfaUId);
//                                }
//                                foreach (JObject obj in jarray)
//                                {
//                                    MediaContentDTO albumDto = new MediaContentDTO();
//                                    albumDto.UserTableId = utid;
//                                    if (obj[JsonKeys.Id] != null) albumDto.AlbumId = (long)obj[JsonKeys.Id];
//                                    if (obj[JsonKeys.AlbumName] != null) albumDto.AlbumName = (string)obj[JsonKeys.AlbumName];
//                                    if (obj[JsonKeys.MediaType] != null) albumDto.MediaType = (int)obj[JsonKeys.MediaType];
//                                    if (obj[JsonKeys.MediaAlbumImageURL] != null) albumDto.AlbumImageUrl = (string)obj[JsonKeys.MediaAlbumImageURL];
//                                    if (obj[JsonKeys.MemberCount] != null) albumDto.TotalMediaCount = (int)obj[JsonKeys.MemberCount];
//                                    if (albumsOfaUId.ContainsKey(albumDto.AlbumId)) albumsOfaUId[albumDto.AlbumId] = albumDto;
//                                    else albumsOfaUId.Add(albumDto.AlbumId, albumDto);
//                                    tmpAlbums.Add(albumDto);
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.LoadAlbumList(utid, mediatype, tmpAlbums);
//                        }
//                        else if (mediatype == SettingsConstants.MEDIA_TYPE_VIDEO)
//                        {
//                            List<MediaContentDTO> tmpAlbums = new List<MediaContentDTO>();
//                            lock (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID)
//                            {
//                                Dictionary<long, MediaContentDTO> albumsOfaUId = null;
//                                if (!NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.TryGetValue(utid, out albumsOfaUId))
//                                {
//                                    albumsOfaUId = new Dictionary<long, MediaContentDTO>();
//                                    NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.Add(utid, albumsOfaUId);
//                                }
//                                foreach (JObject obj in jarray)
//                                {
//                                    MediaContentDTO albumDto = new MediaContentDTO();
//                                    albumDto.UserTableId = utid;
//                                    if (obj[JsonKeys.Id] != null) albumDto.AlbumId = (long)obj[JsonKeys.Id];
//                                    if (obj[JsonKeys.AlbumName] != null) albumDto.AlbumName = (string)obj[JsonKeys.AlbumName];
//                                    if (obj[JsonKeys.MediaType] != null) albumDto.MediaType = (int)obj[JsonKeys.MediaType];
//                                    if (obj[JsonKeys.MediaAlbumImageURL] != null) albumDto.AlbumImageUrl = (string)obj[JsonKeys.MediaAlbumImageURL];
//                                    if (obj[JsonKeys.MemberCount] != null) albumDto.TotalMediaCount = (int)obj[JsonKeys.MemberCount];
//                                    if (albumsOfaUId.ContainsKey(albumDto.AlbumId)) albumsOfaUId[albumDto.AlbumId] = albumDto;
//                                    else albumsOfaUId.Add(albumDto.AlbumId, albumDto);
//                                    tmpAlbums.Add(albumDto);
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.LoadAlbumList(utid, mediatype, tmpAlbums);
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaAlbumList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessMediaAlbumContentList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableId] != null && _JobjFromResponse[JsonKeys.AlbumId] != null)
//                {
//                    MediaItemsSequenceCount++;
//                    string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
//                    int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
//                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                    int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
//                    long albumId = (long)_JobjFromResponse[JsonKeys.AlbumId];
//                    if (!NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.ContainsKey(albumId)) NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.Add(albumId, new List<SingleMediaDTO>());
//                    List<SingleMediaDTO> list = NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS[albumId];
//                    if (_JobjFromResponse[JsonKeys.MediaContentList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
//                        foreach (JObject ob in jarray)
//                        {
//                            SingleMediaDTO mediaDTO = HelperMethodsModel.BindSingleMediaDTO(ob);
//                            mediaDTO.UserTableId = utid;
//                            mediaDTO.AlbumId = albumId;
//                            list.Add(mediaDTO);
//                        }
//                    }
//                    if (seqTotal == MediaItemsSequenceCount)
//                    {
//                        Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
//                        if (DICT_BY_UTID.ContainsKey(utid) && DICT_BY_UTID[utid].ContainsKey(albumId))
//                        {
//                            MediaContentDTO album = DICT_BY_UTID[utid][albumId];
//                            if (album.MediaList == null) album.MediaList = new List<SingleMediaDTO>();
//                            album.MediaList.AddRange(list);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowAlbumItems(albumId, list);
//                        NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.Clear();
//                        MediaItemsSequenceCount = 0;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessMediaAlbumContentList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        //private void ProcessMediaAlbumContentList()
//        //{
//        //    try
//        //    {
//        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableId] != null && _JobjFromResponse[JsonKeys.AlbumId] != null)
//        //        {
//        //            //AlbumMediaContentListSeqCount++;
//        //            string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
//        //            //int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
//        //            //string CurrentSequence = sequence.Split(new Char[] { '/' })[0];
//        //            long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//        //            int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType]; ;
//        //            //long uid = (utid == DefaultSettings.LOGIN_USER_TABLE_ID) ? DefaultSettings.LOGIN_USER_ID : FriendDictionaries.Instance.UTID_UID_DICTIONARY[utid];
//        //            long albumId = (long)_JobjFromResponse[JsonKeys.AlbumId];
//        //            //Dictionary<string, Dictionary<long, SingleMediaDTO>> TEMP_DICTIONARY = null;
//        //            //TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_MY_SINGLE_MEDIA_DETAILS;
//        //            Dictionary<long, SingleMediaDTO> TEMP_DICTIONARY = new Dictionary<long, SingleMediaDTO>();
//        //            if (_JobjFromResponse[JsonKeys.MediaContentList] != null)
//        //            {
//        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
//        //                // lock (TEMP_DICTIONARY)
//        //                // {
//        //                //if (!TEMP_DICTIONARY.ContainsKey(CurrentSequence))
//        //                //{
//        //                //    TEMP_DICTIONARY.Add(CurrentSequence, new Dictionary<long, SingleMediaDTO>());
//        //                //}
//        //                foreach (JObject ob in jarray)
//        //                {
//        //                    SingleMediaDTO mediaDTO = HelperMethodsModel.BindSingleMediaDTO(ob);
//        //                    mediaDTO.UserTableId = utid;
//        //                    mediaDTO.AlbumId = albumId;
//        //                    //mediaType = imageDTO.MediaType;
//        //                    //if (TEMP_DICTIONARY[CurrentSequence].ContainsKey(imageDTO.ContentId))
//        //                    //{
//        //                    //    TEMP_DICTIONARY[CurrentSequence][imageDTO.ContentId] = imageDTO;
//        //                    //}
//        //                    //else
//        //                    //{
//        //                    //    TEMP_DICTIONARY[CurrentSequence].Add(imageDTO.ContentId, imageDTO);
//        //                    //}
//        //                    TEMP_DICTIONARY[mediaDTO.ContentId] = mediaDTO;
//        //                }
//        //                //}
//        //            }

//        //            // if (TEMP_DICTIONARY.Count == seqTotal)
//        //            // {
//        //            //int mediaType = TEMP_DICTIONARY.ElementAt(0).Value.MediaType;
//        //            Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
//        //            Dictionary<long, MediaContentDTO> albumsOfaUtId = null;
//        //            if (!DICT_BY_UTID.TryGetValue(utid, out albumsOfaUtId)) { albumsOfaUtId = new Dictionary<long, MediaContentDTO>(); DICT_BY_UTID.Add(utid, albumsOfaUtId); }
//        //            MediaContentDTO album = null;
//        //            if (albumsOfaUtId.TryGetValue(albumId, out album) && album.MediaList == null) album.MediaList = new List<SingleMediaDTO>();
//        //            List<SingleMediaDTO> lst = new List<SingleMediaDTO>();
//        //            // foreach (Dictionary<long, SingleMediaDTO> val in TEMP_DICTIONARY.Values)
//        //            //{
//        //            //    if (album != null) album.MediaList.AddRange(val.Values.ToList());
//        //            //    lst.AddRange(val.Values.ToList());
//        //            //}
//        //            if (album != null) album.MediaList.AddRange(TEMP_DICTIONARY.Values.ToList());
//        //            lst.AddRange(TEMP_DICTIONARY.Values.ToList());

//        //            HelperMethodsAuth.NewsFeedHandlerInstance.LoadItemsOfAnAlbum(lst, albumId, mediaType, utid, (album != null) ? album.TotalMediaCount : 0);//album.MediaList
//        //            //System.Diagnostics.Debug.WriteLine("ALBUM CONTENT LIST.." + AlbumMediaContentListSeqCount + "SEQTOTAL.." + seqTotal);                       
//        //            TEMP_DICTIONARY.Clear();
//        //            // }
//        //        }
//        //        else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false && _JobjFromResponse[JsonKeys.AlbumId] != null)
//        //        {
//        //            long albumId = (long)_JobjFromResponse[JsonKeys.AlbumId];
//        //            HelperMethodsAuth.NewsFeedHandlerInstance.NoMoreItemsInHashTagOrAlbum(albumId, 0);
//        //            HelperMethodsAuth.NewsFeedHandlerInstance.NoItemFoundInUCSingleAlbumSong();
//        //        }
//        //    }
//        //    catch (Exception ex)
//        //    {

//        //        log.Error("ProcessMediaAlbumContentList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//        //    }
//        //}


//        private void ProcessMediaLikeList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null)
//                {
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        List<UserBasicInfoDTO> tmpList = new List<UserBasicInfoDTO>();
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                                user.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                            if (singleObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)singleObj[JsonKeys.FullName];
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            tmpList.Add(user);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadLikeList(tmpList, 0, 0, contentId, 0);
//                    }
//                }
//                else if (_JobjFromResponse != null && !(bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    HelperMethodsAuth.NewsFeedHandlerInstance.NodataFoundLikeList();
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessMediaLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        private void ProcessMediaCommentList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.ContentId] != null)
//                {
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    //Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (_JobjFromResponse[JsonKeys.Comments] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Comments];
//                        //lock (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST)
//                        //{
//                        //    if (!NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem))
//                        //    {
//                        //        CommentsOfThisItem = new Dictionary<long, CommentDTO>();
//                        //        NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.Add(contentId, CommentsOfThisItem);
//                        //    }
//                        List<CommentDTO> list = new List<CommentDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            CommentDTO comment = HelperMethodsModel.BindCommentDetails(singleObj);
//                            list.Add(comment);
//                            //        lock (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST)
//                            //        {
//                            //            if (CommentsOfThisItem.ContainsKey(comment.CommentId)) CommentsOfThisItem[comment.CommentId] = comment;
//                            //            else CommentsOfThisItem.Add(comment.CommentId, comment);
//                            //        }
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadCommentList(list, nfId, 0, contentId);
//                        //}
//                    }
//                    //if (DefaultSettings.MediaCommentsSequenceCount == seqTotal && CommentsOfThisItem != null && CommentsOfThisItem.Count > 0)
//                    //{
//                    //    DefaultSettings.MediaCommentsSequenceCount = 0;
//                    //    lock (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST)
//                    //    {
//                    //        List<CommentDTO> lst = CommentsOfThisItem.Values.ToList();
//                    //        HelperMethodsAuth.NewsFeedHandlerInstance.LoadCommentList(lst, 0, 0, contentId);
//                    //    }
//                    //}
//                }
//                /* else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                 {
//                     HelperMethodsAuth.AuthHandlerInstance.DisableLoaderAnimation();
//                     string msg = "Unable to Load Comments!";
//                     if (_JobjFromResponse[JsonKeys.Message] != null)
//                     {
//                         msg = (string)_JobjFromResponse[JsonKeys.Message];
//                     }
//                     long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                     long contentId = (_JobjFromResponse[JsonKeys.ContentId] != null) ? (long)_JobjFromResponse[JsonKeys.ContentId] : 0;
//                     HelperMethodsAuth.NewsFeedHandlerInstance.LoadCommentList(null, nfId, 0, contentId); 
//                 }*/
//                HelperMethodsAuth.NewsFeedHandlerInstance.LoadCommentUI();
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaCommentList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessMediaCommentLikeList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        List<UserBasicInfoDTO> tmpList = new List<UserBasicInfoDTO>();
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                                user.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                            if (singleObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)singleObj[JsonKeys.FullName];
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            tmpList.Add(user);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadLikeList(tmpList, 0, 0, contentId, commentId);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessMediaCommentLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        private void ProcessSingleFeedShareList() //250,115
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Shares] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Shares];
//                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
//                    long minIdOfSequence = 99999;
//                    foreach (JObject obj in jarray)
//                    {
//                        if (obj[JsonKeys.UserTableId] != null && obj[JsonKeys.Id] != null && obj[JsonKeys.FullName] != null && obj[JsonKeys.UserIdentity] != null && obj[JsonKeys.ProfileImage] != null)
//                        {
//                            long uid = (long)obj[JsonKeys.UserIdentity];
//                            UserBasicInfoDTO user = null;
//                            if (uid == DefaultSettings.LOGIN_USER_ID) user = new UserBasicInfoDTO();
//                            else
//                            {
//                                if (!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(uid))
//                                    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(uid, new UserBasicInfoDTO { UserIdentity = uid });
//                                user = FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[uid];
//                            }
//                            user.FullName = (string)obj[JsonKeys.FullName];
//                            user.UserTableId = (long)obj[JsonKeys.UserTableId];
//                            user.UserIdentity = uid;
//                            user.ProfileImage = (string)obj[JsonKeys.ProfileImage];
//                            long Id = (long)obj[JsonKeys.Id];
//                            if (Id < minIdOfSequence) minIdOfSequence = Id;
//                            list.Add(user);
//                        }
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.ShowSingleFeedShareList(nfId, list, minIdOfSequence);
//                }
//            }
//            catch (Exception ex)
//            {
//                log.Error("ProcessMediaShareList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
//            }
//        }
//        private void ProcessWhoSharesList() // 249
//        {
//            try
//            {

//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    long parentNfid = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    int seqTotal = Convert.ToInt32(((string)_JobjFromResponse[JsonKeys.Sequence]).Split(new char[] { '/' })[1]);

//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        List<FeedDTO> tempList = new List<FeedDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            tempList.Add(feed);
//                        }

//                        lock (NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS)
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS.ContainsKey(parentNfid))
//                            {
//                                if (NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].ContainsKey((string)_JobjFromResponse[JsonKeys.Sequence]))
//                                {
//                                    NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid][(string)_JobjFromResponse[JsonKeys.Sequence]] = tempList;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].Add((string)_JobjFromResponse[JsonKeys.Sequence], tempList);
//                                }
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS.Add(parentNfid, new Dictionary<string, List<FeedDTO>>());
//                                NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].Add((string)_JobjFromResponse[JsonKeys.Sequence], tempList);
//                            }
//                        }

//                        if (tempList != null && tempList.Count > 0)
//                        {
//                            foreach (FeedDTO sharedFeed in tempList)
//                            {
//                                lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                                {
//                                    if (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.ContainsKey(sharedFeed.NewsfeedId))
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[sharedFeed.NewsfeedId] = parentNfid;
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.Add(sharedFeed.NewsfeedId, parentNfid);
//                                    }
//                                }

//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(sharedFeed.NewsfeedId))
//                                    {
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[sharedFeed.NewsfeedId] = sharedFeed;
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Add(sharedFeed.NewsfeedId, sharedFeed);
//                                    }
//                                }

//                                /*lock (NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(parentNfid))
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME[parentNfid].InsertData(sharedFeed.NewsfeedId, sharedFeed.ActualTime);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(parentNfid, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME[parentNfid].InsertData(sharedFeed.NewsfeedId, sharedFeed.ActualTime);
//                                    }
//                                }*/

//                            }

//                            if (seqTotal == NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].Count)
//                            {
//                                CustomSortedList whoShareList = new CustomSortedList();
//                                foreach (string key in NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].Keys)
//                                {
//                                    foreach (FeedDTO sharedFeed in NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid][key].ToList())
//                                    {
//                                        whoShareList.InsertData(sharedFeed.NewsfeedId, sharedFeed.ActualTime, sharedFeed.FeedCategory);
//                                    }
//                                }

//                                HelperMethodsAuth.NewsFeedHandlerInstance.WhoShareList(parentNfid, whoShareList);

//                                NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS.Clear();
//                            }
//                        }

//                    }
//                }
//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    long parentNfid = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    HelperMethodsAuth.NewsFeedHandlerInstance.WhoShareList(parentNfid, null);
//                    NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS.Clear();
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessWhoSharesList ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessFeedTagList() //274
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.FriendsTagList] != null)
//                {
//                    long nfid = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);
//                    JArray array = (JArray)_JobjFromResponse[JsonKeys.FriendsTagList];
//                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
//                    foreach (JObject obj in array)
//                    {
//                        if (obj[JsonKeys.UserTableId] != null && obj[JsonKeys.Name] != null && obj[JsonKeys.UserIdentity] != null && obj[JsonKeys.ProfileImage] != null)
//                        {
//                            long uid = (long)obj[JsonKeys.UserIdentity];
//                            if (!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(uid))
//                                FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(uid, new UserBasicInfoDTO { UserIdentity = uid });
//                            UserBasicInfoDTO user = FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[uid];
//                            user.FullName = (string)obj[JsonKeys.Name];
//                            user.UserTableId = (long)obj[JsonKeys.UserTableId];
//                            user.UserIdentity = uid;
//                            user.ProfileImage = (string)obj[JsonKeys.ProfileImage];
//                            list.Add(user);
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.ShowFeedTagList(nfid, list);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessFeedTagList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessDoingList() //273
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.DoingList] != null)
//                {
//                    JArray array = (JArray)_JobjFromResponse[JsonKeys.DoingList];
//                    List<DoingDTO> tmpList = new List<DoingDTO>();
//                    foreach (JObject obj in array)
//                    {
//                        if (obj[JsonKeys.Id] != null && obj[JsonKeys.Name] != null && obj[JsonKeys.Category] != null && obj[JsonKeys.Url] != null)
//                        {
//                            DoingDTO doing = new DoingDTO();
//                            doing.ID = (long)obj[JsonKeys.Id];
//                            doing.Name = HelperMethodsModel.FirstLetterToLower((string)obj[JsonKeys.Name]);
//                            doing.Category = (int)obj[JsonKeys.Category];
//                            doing.ImgUrl = (string)obj[JsonKeys.Url];
//                            if (obj[JsonKeys.DoingSort] != null) doing.SortId = (int)obj[JsonKeys.DoingSort];
//                            if (obj[JsonKeys.UpdateTime] != null)
//                            {
//                                doing.UpdateTime = (long)obj[JsonKeys.UpdateTime];
//                                if (doing.UpdateTime > DefaultSettings.VALUE_DOING_LIST_UT)
//                                {
//                                    DefaultSettings.VALUE_DOING_LIST_UT = doing.UpdateTime;
//                                    DatabaseActivityDAO.Instance.SaveDoingUpdateTimeIntoDB();
//                                }
//                            }
//                            lock (NewsFeedDictionaries.Instance.DOING_DICTIONARY)
//                            {
//                                if (NewsFeedDictionaries.Instance.DOING_DICTIONARY.ContainsKey(doing.ID))
//                                    NewsFeedDictionaries.Instance.DOING_DICTIONARY[doing.ID] = doing;
//                                else
//                                    NewsFeedDictionaries.Instance.DOING_DICTIONARY.Add(doing.ID, doing);
//                            }
//                            tmpList.Add(doing);
//                            HelperMethodsAuth.NewsFeedHandlerInstance.EnqueueDoingDTOforImageDownload(doing);
//                        }
//                    }
//                    new InsertIntoDoingTable(tmpList);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessDoingList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }


//        private void ProcessHashtagMediaContents()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.HashTagId] != null)
//                {
//                    MediaItemsSequenceCount++;
//                    string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
//                    int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
//                    long hashtagId = (long)_JobjFromResponse[JsonKeys.HashTagId];
//                    if (!NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.ContainsKey(hashtagId)) NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.Add(hashtagId, new List<SingleMediaDTO>());
//                    List<SingleMediaDTO> list = NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS[hashtagId];
//                    if (_JobjFromResponse[JsonKeys.MediaList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
//                        foreach (JObject ob in jarray)
//                        {
//                            SingleMediaDTO mediaDTO = HelperMethodsModel.BindSingleMediaDTO(ob);
//                            list.Add(mediaDTO);
//                        }
//                    }
//                    if (seqTotal == MediaItemsSequenceCount)
//                    {
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowHashTagItems(hashtagId, list);
//                        NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.Clear();
//                        MediaItemsSequenceCount = 0;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessHashtagMediaContents ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }

//        private void ProcessMediaFullSearch()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestionType] != null)
//                {
//                    int suggestionType = (int)_JobjFromResponse[JsonKeys.MediaSuggestionType];
//                    string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
//                    if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
//                        if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_SONGS)
//                        {
//                            List<SingleMediaDTO> list = new List<SingleMediaDTO>();
//                            foreach (JObject singleObj in jarray)
//                            {
//                                SingleMediaDTO dto = HelperMethodsModel.BindSingleMediaDTO(singleObj);
//                                list.Add(dto);
//                            }
//                            HelperMethodsAuth.AuthHandlerInstance.ShowMediaResultsInFullSearch(searchParam, list);
//                        }
//                        else if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_ALBUMS)
//                        {
//                            List<MediaContentDTO> list = new List<MediaContentDTO>();
//                            foreach (JObject singleObj in jarray)
//                            {
//                                MediaContentDTO dto = new MediaContentDTO();
//                                if (singleObj[JsonKeys.UserTableId] != null) dto.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                                if (singleObj[JsonKeys.AlbumName] != null) dto.AlbumName = (string)singleObj[JsonKeys.AlbumName];
//                                if (singleObj[JsonKeys.AlbumId] != null) dto.AlbumId = (long)singleObj[JsonKeys.AlbumId];
//                                if (singleObj[JsonKeys.MediaAlbumImageURL] != null) dto.AlbumImageUrl = (string)singleObj[JsonKeys.MediaAlbumImageURL];
//                                if (singleObj[JsonKeys.MediaType] != null) dto.MediaType = (int)singleObj[JsonKeys.MediaType];
//                                if (singleObj[JsonKeys.MediaItemsCount] != null) dto.TotalMediaCount = (int)singleObj[JsonKeys.MediaItemsCount];
//                                list.Add(dto);
//                            }
//                            HelperMethodsAuth.AuthHandlerInstance.ShowAlbumResultsInFullSearch(searchParam, list);
//                        }
//                        else if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_TAGS)
//                        {
//                            List<HashTagDTO> list = new List<HashTagDTO>();
//                            foreach (JObject singleObj in jarray)
//                            {
//                                HashTagDTO dto = new HashTagDTO();
//                                if (singleObj[JsonKeys.MediaItemsCount] != null) dto.NoOfItems = (int)singleObj[JsonKeys.MediaItemsCount];
//                                if (singleObj[JsonKeys.HashTagId] != null) dto.CategoryId = (long)singleObj[JsonKeys.HashTagId];
//                                if (singleObj[JsonKeys.HashTagName] != null) dto.HashTagSearchKey = (string)singleObj[JsonKeys.HashTagName];
//                                list.Add(dto);
//                            }
//                            HelperMethodsAuth.AuthHandlerInstance.ShowHashTagResultsInFullSearch(searchParam, list);
//                        }
//                    }
//                    else
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.NoDataInFullMediaSearch(searchParam, suggestionType);
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessMediaSearchSuggestions()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
//                {
//                    string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
//                    List<SearchMediaDTO> searchListOfaSearchParam = null;
//                    lock (MediaDictionaries.Instance.TEMP_SEARCH_SGTNS)
//                    {
//                        if (!MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.TryGetValue(searchParam, out searchListOfaSearchParam))
//                        {
//                            searchListOfaSearchParam = new List<SearchMediaDTO>();
//                            MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.Add(searchParam, searchListOfaSearchParam);
//                        }
//                    }
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
//                    List<SearchMediaDTO> lst = new List<SearchMediaDTO>();
//                    foreach (JObject singleObj in jarray)
//                    {
//                        SearchMediaDTO dto = new SearchMediaDTO();
//                        dto.SearchSuggestion = (string)singleObj[JsonKeys.MediaSearchKey];
//                        dto.SearchType = (int)singleObj[JsonKeys.MediaSuggestionType];
//                        lst.Add(dto);
//                        searchListOfaSearchParam.Add(dto);
//                    }
//                    if (lst.Count > 0)
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadMediaSearchs(searchParam, lst);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessMediaSearchTrends()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
//                {
//                    DefaultSettings.SearchTrendsSeq++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    int seqTotal = Convert.ToInt32(str.Split(delimiterChars)[1]);
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
//                    foreach (JObject singleObj in jarray)
//                    {
//                        SearchMediaDTO dto = new SearchMediaDTO();
//                        dto.SearchSuggestion = (string)singleObj[JsonKeys.MediaSearchKey];
//                        dto.SearchType = (int)singleObj[JsonKeys.MediaSuggestionType];
//                        lock (MediaDictionaries.Instance.MEDIA_TRENDS)
//                        {
//                            MediaDictionaries.Instance.MEDIA_TRENDS.Add(dto);
//                        }
//                    }
//                    //log.Info("SearchTrends ." + DefaultSettings.SearchTrendsSeq + "SEQTOTAL.." + seqTotal);
//                    if (DefaultSettings.SearchTrendsSeq == seqTotal)
//                    {
//                        lock (MediaDictionaries.Instance.MEDIA_TRENDS)
//                        {
//                            MediaDictionaries.Instance.MEDIA_TRENDS.Sort((x, y) => y.SearchSuggestion.Length.CompareTo(x.SearchSuggestion.Length));
//                        }
//                        DefaultSettings.SearchTrendsSeq = 0;
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadMediaSearchTrends();
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaSearchTrends ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//<<<<<<< .mine
//        private void ProcessSingleFeedDetails()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//=======
//                    if (profileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
//                    {
//                        List<NewsPortalDTO> list = new List<NewsPortalDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            NewsPortalDTO dto = new NewsPortalDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.PortalName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                            {
//                                dto.PortalId = (long)singleObj[JsonKeys.UserTableId];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PortalSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PortalCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PortalCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            NewsPortalDictionaries.Instance.UTID_NEWSPORTALINFO_DICTIONARY[dto.PortalId] = dto;
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                    else if (profileType == SettingsConstants.PROFILE_TYPE_PAGES)
//                    {
//                        List<PageInfoDTO> list = new List<PageInfoDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            PageInfoDTO dto = new PageInfoDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.PageName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                            {
//                                dto.PageId = (long)singleObj[JsonKeys.UserTableId];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PageSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PageCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PageCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY[dto.PageId] = dto;
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                    else if (profileType == SettingsConstants.PROFILE_TYPE_MUSICPAGE)
//                    {
//                        List<MusicPageDTO> list = new List<MusicPageDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            MusicPageDTO dto = new MusicPageDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.FullName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PageSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PageCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PageCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsPortalCategoriesList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsPortalUtid] != null
//                    && _JobjFromResponse[JsonKeys.NewsPortalCategoryList] != null && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    long utId = (long)_JobjFromResponse[JsonKeys.NewsPortalUtid];
//                    List<NewsCategoryDTO> list = new List<NewsCategoryDTO>();
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsPortalCategoryList];
//                    foreach (JObject singleObj in jarray)
//                    {
//                        NewsCategoryDTO dto = new NewsCategoryDTO();
//                        if (singleObj[JsonKeys.NewsPortalCategoryName] != null)
//                        {
//                            dto.CategoryName = (string)singleObj[JsonKeys.NewsPortalCategoryName];
//                        }
//                        if (singleObj[JsonKeys.Id] != null)
//                        {
//                            dto.CategoryId = (int)singleObj[JsonKeys.Id];
//                        }
//                        if (singleObj[JsonKeys.Type] != null)
//                        {
//                            dto.CategoryType = (int)singleObj[JsonKeys.Type];
//                        }
//                        if (singleObj[JsonKeys.Subscribe] != null)
//                        {
//                            dto.Subscribed = (bool)singleObj[JsonKeys.Subscribe];
//                        }
//                        list.Add(dto);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.LoadNewsPortalCategories(utId, list);
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//>>>>>>> .r1628

//                            if (feed.ParentFeed != null)
//                            {
//                                //FeedDTO whoShareFeed = feed.WhoShare;
//                                //whoShareFeed.ParentFeed = feed;

//                                lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                                {
//                                    NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[feed.NewsfeedId] = feed.ParentFeed.NewsfeedId;
//                                }

//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.ParentFeed.NewsfeedId] = feed.ParentFeed;
//                                }
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.ParentFeed.NewsfeedId, feed.ParentFeed.Time, feed.ParentFeed.FeedCategory);

//                                }
//                                //HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(whoShareFeed, DefaultSettings.FEED_TYPE_ALL);
//                            }
//                            else
//                            {
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                }
//                                //HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed, DefaultSettings.FEED_TYPE_ALL);
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedDetails(feed);
//                        }

//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessSingleFeedDetails ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessAddStatus()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    FeedDTO feed = HelperMethodsModel.BindFeedDetails(_JobjFromResponse);
//                    if ((feed.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO || feed.BookPostType == SettingsConstants.NEWS_FEED_TYPE_VIDEO) && feed.MediaContent == null)
//                        return;
//                    if (feed.MediaContent != null)
//                    {
//                        if (feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                        {
//                            HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                        }
//                        if (feed.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO)
//                        {
//                            if (!NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_USER_TABLE_ID))
//                            {
//                                NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.Add(DefaultSettings.LOGIN_USER_TABLE_ID, new Dictionary<long, MediaContentDTO>());
//                            }
//                            if (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID].ContainsKey(feed.MediaContent.AlbumId))
//                            {
//                                NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].TotalMediaCount += feed.MediaContent.MediaList.Count;
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID].Add(feed.MediaContent.AlbumId, new MediaContentDTO { AlbumId = feed.MediaContent.AlbumId, AlbumName = feed.MediaContent.AlbumName, MediaType = 1, TotalMediaCount = feed.MediaContent.MediaList.Count });
//                            }
//                            if (feed.MediaContent.MediaList != null)
//                            {
//                                foreach (SingleMediaDTO singleDto in feed.MediaContent.MediaList)
//                                {
//                                    if (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList == null)
//                                    {
//                                        NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList = new List<SingleMediaDTO>();
//                                    }
//                                    lock (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID)
//                                    {
//                                        if (!NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList.Any(P => P.ContentId == singleDto.ContentId))
//                                        {
//                                            NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList.Insert(0, singleDto);
//                                        }
//                                    }
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.AddOrUpdateMyAlbumModel(1, feed.MediaContent);
//                        }
//                        else if (feed.BookPostType == SettingsConstants.NEWS_FEED_TYPE_VIDEO)
//                        {
//                            if (!NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_USER_TABLE_ID))
//                            {
//                                NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.Add(DefaultSettings.LOGIN_USER_TABLE_ID, new Dictionary<long, MediaContentDTO>());
//                            }
//                            if (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID].ContainsKey(feed.MediaContent.AlbumId))
//                            {
//                                NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].TotalMediaCount += feed.MediaContent.MediaList.Count;
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID].Add(feed.MediaContent.AlbumId, new MediaContentDTO { AlbumId = feed.MediaContent.AlbumId, AlbumName = feed.MediaContent.AlbumName, MediaType = 2, TotalMediaCount = feed.MediaContent.MediaList.Count });
//                            }
//                            if (feed.MediaContent.MediaList != null)
//                            {
//                                foreach (SingleMediaDTO singleDto in feed.MediaContent.MediaList)
//                                {
//                                    if (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList == null)
//                                    {
//                                        NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList = new List<SingleMediaDTO>();
//                                    }
//                                    lock (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID)
//                                    {
//                                        if (!NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList.Any(P => P.ContentId == singleDto.ContentId))
//                                        {
//                                            NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList.Insert(0, singleDto);
//                                        }
//                                    }
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.AddOrUpdateMyAlbumModel(2, feed.MediaContent);
//                        }
//                    }
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(feed.NewsfeedId)) NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                        else NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Add(feed.NewsfeedId, feed);
//                    }

//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                    {
//                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertData(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, feed, DefaultSettings.FEED_TYPE_ALL, 0);
//                    lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                    {
//                        NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, feed, DefaultSettings.FEED_TYPE_MY, 0);

//                    if (feed.FriendId > 0) //in friend's wall post
//                    {

//                        if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(feed.FriendId))
//                        {
//                            lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[feed.FriendId].InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            }
//                        }
//                        else
//                        {
//                            lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(feed.FriendId, new CustomSortedList());
//                                NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[feed.FriendId].InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            }
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, feed, DefaultSettings.FEED_TYPE_FRIEND, feed.FriendId);
//                    }
//                    else if (feed.GroupId > 0) //in circle's wall post
//                    {

//                        if (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(feed.GroupId))
//                        {
//                            lock (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[feed.GroupId].InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            }
//                        }
//                        else
//                        {
//                            lock (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(feed.GroupId, new CustomSortedList());
//                                NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[feed.GroupId].InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            }
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, feed, DefaultSettings.FEED_TYPE_CIRCLE, feed.GroupId);
//                    }

//                    if (feed.ImageList != null && feed.ImageList.Count > 0)
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.AddProfileCoverImageToUIList(feed.ImageList, SettingsConstants.TYPE_NORMAL_BOOK_IMAGE);
//                    }
//                }
//                else
//                {
//                    string mg = String.Empty;
//                    if (_JobjFromResponse[JsonKeys.Message] != null)
//                    {
//                        mg = (string)_JobjFromResponse[JsonKeys.Message];
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(mg, "Failed!");
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessAddStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessDiscoverNewsPortals()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.NewsPortalList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.SubscribeType] != null)// && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.Subscribe] != null)
//                {
//                    int profileType = (_JobjFromResponse[JsonKeys.ProfileType] != null) ? ((int)_JobjFromResponse[JsonKeys.ProfileType]) : SettingsConstants.PROFILE_TYPE_NEWSPORTAL;
//                    int subscType = (int)_JobjFromResponse[JsonKeys.SubscribeType];
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsPortalList];

//                    if (profileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
//                    {
//                        List<NewsPortalDTO> list = new List<NewsPortalDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            NewsPortalDTO dto = new NewsPortalDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.PortalName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                            {
//                                dto.PortalId = (long)singleObj[JsonKeys.UserTableId];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PortalSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PortalCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PortalCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            NewsPortalDictionaries.Instance.UTID_NEWSPORTALINFO_DICTIONARY[dto.PortalId] = dto;
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                    else if (profileType == SettingsConstants.PROFILE_TYPE_PAGES)
//                    {
//                        List<PageInfoDTO> list = new List<PageInfoDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            PageInfoDTO dto = new PageInfoDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.PageName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                            {
//                                dto.PageId = (long)singleObj[JsonKeys.UserTableId];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PageSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PageCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PageCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY[dto.PageId] = dto;
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsPortalCategoriesList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsPortalUtid] != null
//                    && _JobjFromResponse[JsonKeys.NewsPortalCategoryList] != null && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    long utId = (long)_JobjFromResponse[JsonKeys.NewsPortalUtid];
//                    List<NewsCategoryDTO> list = new List<NewsCategoryDTO>();
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsPortalCategoryList];
//                    foreach (JObject singleObj in jarray)
//                    {
//                        NewsCategoryDTO dto = new NewsCategoryDTO();
//                        if (singleObj[JsonKeys.NewsPortalCategoryName] != null)
//                        {
//                            dto.CategoryName = (string)singleObj[JsonKeys.NewsPortalCategoryName];
//                        }
//                        if (singleObj[JsonKeys.Id] != null)
//                        {
//                            dto.CategoryId = (int)singleObj[JsonKeys.Id];
//                        }
//                        if (singleObj[JsonKeys.Type] != null)
//                        {
//                            dto.CategoryType = (int)singleObj[JsonKeys.Type];
//                        }
//                        if (singleObj[JsonKeys.Subscribe] != null)
//                        {
//                            dto.Subscribed = (bool)singleObj[JsonKeys.Subscribe];
//                        }
//                        list.Add(dto);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.LoadNewsPortalCategories(utId, list);
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }

//        //private void ProcessMediaContentDetails()
//        //{
//        //    try
//        //    {
//        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentDTO] != null
//        //            && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.UserTableId] != null)
//        //        {

//        //            if (_JobjFromResponse[JsonKeys.ReasonCode] != null && ReasonCodeConstants.REASON_CODE_NOT_FOUND == (long)_JobjFromResponse[JsonKeys.ReasonCode])
//        //            {
//        //                HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(ReasonCodeConstants.REASON_MSG_NOT_FOUND, "");
//        //            }
//        //            else
//        //            {
//        //                long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//        //                long utId = (long)_JobjFromResponse[JsonKeys.UserTableId];
//        //                //long uId = (utId == DefaultSettings.LOGIN_USER_TABLE_ID) ? DefaultSettings.LOGIN_USER_ID : FriendDictionaries.Instance.UTID_UID_DICTIONARY[utId];
//        //                JObject mediaContent = (JObject)_JobjFromResponse[JsonKeys.MediaContentDTO];
//        //                SingleMediaDTO singleItemDetails = HelperMethodsModel.BindSingleMediaDTO(mediaContent);
//        //                singleItemDetails.UserTableId = utId;
//        //                long uid = 0; bool NonFriendMedia = true;
//        //                if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.TryGetValue(utId, out uid))
//        //                {
//        //                    UserBasicInfoDTO dto = null;
//        //                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(uid, out dto) && dto.FriendShipStatus != 0)
//        //                    {
//        //                        NonFriendMedia = false;
//        //                        Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (singleItemDetails.MediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
//        //                        Dictionary<long, MediaContentDTO> albumsOfaUtId = null;
//        //                        if (!DICT_BY_UTID.TryGetValue(utId, out albumsOfaUtId)) { albumsOfaUtId = new Dictionary<long, MediaContentDTO>(); DICT_BY_UTID.Add(utId, albumsOfaUtId); }
//        //                        MediaContentDTO album = null;
//        //                        if (!albumsOfaUtId.TryGetValue(singleItemDetails.AlbumId, out album)) { album = new MediaContentDTO { AlbumId = singleItemDetails.AlbumId, MediaType = singleItemDetails.MediaType }; albumsOfaUtId.Add(album.AlbumId, album); }
//        //                        if (album.MediaList == null) album.MediaList = new List<SingleMediaDTO>();
//        //                        if (!album.MediaList.Any(P => P.ContentId == contentId))
//        //                            album.MediaList.Add(singleItemDetails);
//        //                    }
//        //                }
//        //                HelperMethodsAuth.NewsFeedHandlerInstance.MediaContentDetails(singleItemDetails, NonFriendMedia);
//        //            }
//        //        }
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        log.Error("ProcessMediaContentDetails ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//        //    }
//        //}

//        private void ProcessUpdateLikeUnlikeCommentMedia()
//        {
//            //Received for action=468 from Auth JSON==> {"uId":"2110010086","sucs":true,"fn":"sirat samyoun","id":120,"cmnId":625,"tm":1449402328539,"cntntId":1875,"lkd":1,"loc":1,"mdaT":2}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.CommentId] != null
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.Liked] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    int liked = (int)_JobjFromResponse[JsonKeys.Liked];
//                    Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem)
//                        && CommentsOfThisItem.ContainsKey(commentId))
//                    {
//                        if (liked == 1)
//                            CommentsOfThisItem[commentId].TotalLikeComment = CommentsOfThisItem[commentId].TotalLikeComment + 1;
//                        else if (CommentsOfThisItem[commentId].TotalLikeComment > 0)
//                            CommentsOfThisItem[commentId].TotalLikeComment = CommentsOfThisItem[commentId].TotalLikeComment - 1;
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateLikeUnlikeCommentMedia(liked, contentId, commentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeUnlikeMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateDeleteCommentMedia()
//        {
//            //Received update for action 467-->{"sucs":true,"cmnId":1,"uId":"2110010002","fn":"Ashraful ( এইটা আসল )","sc":false,"loc":4,"cntntId":1}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem)
//                        && CommentsOfThisItem.ContainsKey(commentId))
//                    {
//                        CommentsOfThisItem.Remove(commentId);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.DeleteMediaComment(contentId, commentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateDeleteCommentMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateEditCommentMedia()
//        {
//            //Received update for action 466-->{"sucs":true,"cmnId":2,"uId":"2110010002","cmn":"Edited comment 927 by ashraful","fn":"Ashraful ( এইটা আসল )","sc":false,"cntntId":1}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ShowContinue] != null
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.Comment] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    bool showContinue = (bool)_JobjFromResponse[JsonKeys.ShowContinue];
//                    string comment = (string)_JobjFromResponse[JsonKeys.Comment];
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem)
//                        && CommentsOfThisItem.ContainsKey(commentId))
//                    {
//                        CommentsOfThisItem[commentId].Comment = comment;
//                        CommentsOfThisItem[commentId].ShowContinue = showContinue;
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.EditMediaComment(comment, showContinue, commentId, contentId, nfId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateEditCommentMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateCommentMedia()
//        {
//            //Received for action=465 from Auth JSON==> {"sucs":true,"cmnId":622,"tm":1449379711978,"uId":"2110010086","cmn":"1","fn":"sirat samyoun","sc":false,"loc":1,"cntntId":1894,"mdaT":2}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaType] != null
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.UserIdentity] != null)
//                {
//                    CommentDTO comment = HelperMethodsModel.BindCommentDetails(_JobjFromResponse);
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
//                    long uid = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (!NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem))
//                    {
//                        CommentsOfThisItem = new Dictionary<long, CommentDTO>();
//                        NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.Add(contentId, CommentsOfThisItem);
//                    }
//                    lock (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST)
//                    {
//                        if (CommentsOfThisItem.ContainsKey(comment.CommentId)) CommentsOfThisItem[comment.CommentId] = comment;
//                        else CommentsOfThisItem.Add(comment.CommentId, comment);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.AddMediaComment(comment, contentId, nfid, true);  //UpdateCommentMedia(comment, contentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateCommentMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeUnlikeMedia()
//        {
//            //Received for action=464 from Auth JSON==> {"uId":"2110010086","sucs":true,"fn":"sirat samyoun","cntntId":1894,"lkd":1,"loc":1,"mdaT":2}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaType] != null
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.Liked] != null && _JobjFromResponse[JsonKeys.UserIdentity] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
//                    int liked = (int)_JobjFromResponse[JsonKeys.Liked];
//                    int loc = (int)_JobjFromResponse[JsonKeys.LikeOrComment];
//                    long uid = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    long utId = 0;
//                    int len = FriendDictionaries.Instance.UTID_UID_DICTIONARY.Count;
//                    for (int i = 0; i < len; i++)
//                    {
//                        if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.ElementAt(i).Value == uid)
//                        {
//                            utId = FriendDictionaries.Instance.UTID_UID_DICTIONARY.ElementAt(i).Key;
//                            break;
//                        }
//                    }
//                    if (utId > 0)
//                    {
//                        Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
//                        Dictionary<long, MediaContentDTO> albumsOfaUtId = null;
//                        if (DICT_BY_UTID.TryGetValue(uid, out albumsOfaUtId))
//                        {
//                            for (int i = 0; i < albumsOfaUtId.Count; i++)
//                            {
//                                if (albumsOfaUtId[i].MediaList != null && albumsOfaUtId[i].MediaList.Count > 0)
//                                {
//                                    SingleMediaDTO singleMedia = albumsOfaUtId[i].MediaList.Where(P => P.ContentId == contentId).FirstOrDefault();
//                                    if (singleMedia != null)
//                                    {
//                                        if (liked == 1) singleMedia.LikeCount++;
//                                        else if (singleMedia.LikeCount > 0)
//                                            singleMedia.LikeCount--;
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateLikeUnlikeMedia(loc, liked, contentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeUnlikeMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessHashtagSuggestion() //280
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
//                {
//                    JArray array = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];

//                    foreach (JObject obj in array)
//                    {
//                        if (obj[JsonKeys.MediaSearchKey] != null && obj[JsonKeys.MediaSuggestionType] != null && obj[JsonKeys.MediaHashTagId] != null)
//                        {
//                            HashTagDTO dto = new HashTagDTO();
//                            dto.CategoryId = (long)obj[JsonKeys.MediaHashTagId];
//                            dto.HashTagSearchKey = (string)obj[JsonKeys.MediaSearchKey];
//                            //dto.SuggestionType = (int)obj[JsonKeys.MediaSuggestionType];

//                            lock (MediaDictionaries.Instance.HASHTAG_SUGGESTIONS)
//                            {
//                                MediaDictionaries.Instance.HASHTAG_SUGGESTIONS[dto.CategoryId] = dto;
//                            }
//                        }
//                    }

//                    HelperMethodsAuth.AuthHandlerInstance.StartHashTagSuggestionThread();
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessHashtagSuggestion ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }

//        }

//        private void ProcessSpamReasonList() //1001
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Sequence] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    SpamReasonListSeqCount++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);

//                    if (_JobjFromResponse[JsonKeys.SpamReasonList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.SpamReasonList];


//                        foreach (JObject singleObj in jarray)
//                        {
//                            int reasonId = 0;
//                            string reason = string.Empty;

//                            if (singleObj[JsonKeys.Id] != null)
//                            {
//                                reasonId = (int)singleObj[JsonKeys.Id];
//                            }

//                            if (singleObj[JsonKeys.SpamReason] != null)
//                            {
//                                reason = (string)singleObj[JsonKeys.SpamReason];
//                            }

//                            if (reasonId > 0 && !string.IsNullOrEmpty(reason))
//                                NewsFeedDictionaries.Instance.TEMP_SPAM_REASON_LIST[reasonId] = reason;
//                        }

//                    }
//                    if (SpamReasonListSeqCount == seqTotal)
//                    {
//                        SpamReasonListSeqCount = 0;
//                        HelperMethodsAuth.NewsFeedHandlerInstance.OpenSpamReasonListPopUp();
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessSpamReasonList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }

//        #endregion

//        #region "Call & Chat"
//        private void ProcessUpdateSendRegister()
//        {

//            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//            {

//                try
//                {
//                    CallerDTO caller = new CallerDTO();
//                    HelperMethodsAuth.CallAuthResult_174_374(_JobjFromResponse, caller);
//                    HelperMethodsAuth.AuthHandlerInstance.ProcessUpdateSendRegister(caller);
//                }
//                catch (Exception e)
//                {

//                    log.Error("ProcessUpdateSendRegister ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//                }
//            }
//        }

//        private void ProcessUpdateStartFriendChat()//175-375
//        {
//            try
//            {
//                if (_JobjFromResponse != null)
//                {
//                    if (action == AppConstants.TYPE_START_FRIEND_CHAT && server_packet_id == 0)
//                        return;

//                    ChatInitiatorDTO initiatorDTO = new ChatInitiatorDTO();
//                    initiatorDTO.FriendIdentity = _JobjFromResponse[JsonKeys.FriendId] != null ? ModelUtility.ConvertToLong(_JobjFromResponse[JsonKeys.FriendId]) : 0;
//                    initiatorDTO.Presence = _JobjFromResponse[JsonKeys.Presence] != null ? (int)_JobjFromResponse[JsonKeys.Presence] : 0;
//                    initiatorDTO.Mood = _JobjFromResponse[JsonKeys.Mood] != null ? (int)_JobjFromResponse[JsonKeys.Mood] : 0;
//                    initiatorDTO.LastOnlineTime = _JobjFromResponse[JsonKeys.LastOnlineTime] != null ? (long)_JobjFromResponse[JsonKeys.LastOnlineTime] : 0;
//                    initiatorDTO.ReasonCode = _JobjFromResponse[JsonKeys.ReasonCode] != null ? (int)_JobjFromResponse[JsonKeys.ReasonCode] : 0;
//                    initiatorDTO.ChatServerIP = (string)_JobjFromResponse[JsonKeys.ChatServerIp];
//                    initiatorDTO.ChatRegisterPort = _JobjFromResponse[JsonKeys.ChatRegistrationPort] != null ? (int)_JobjFromResponse[JsonKeys.ChatRegistrationPort] : 0;
//                    initiatorDTO.Device = _JobjFromResponse[JsonKeys.Device] != null ? (int)_JobjFromResponse[JsonKeys.Device] : 0;
//                    initiatorDTO.DeviceToken = (string)_JobjFromResponse[JsonKeys.DeviceToken];
//                    initiatorDTO.FullName = (string)_JobjFromResponse[JsonKeys.Name];
//                    initiatorDTO.ApplicationType = _JobjFromResponse[JsonKeys.AppType] != null ? (int)_JobjFromResponse[JsonKeys.AppType] : 0;
//                    initiatorDTO.RemotePushType = _JobjFromResponse[JsonKeys.RemotePushType] != null ? (int)_JobjFromResponse[JsonKeys.RemotePushType] : 1;
//                    initiatorDTO.ServerPacketID = server_packet_id;
//                    initiatorDTO.ActionType = AppConstants.TYPE_UPDATE_START_FRIEND_CHAT;

//                    HelperMethodsAuth.AuthHandlerInstance.ProcessStartFriendChat(initiatorDTO);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateStartFriendChat ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateStartGroupChat()//134-334-335
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    if (action == AppConstants.TYPE_START_GROUP_CHAT && server_packet_id == 0)
//                        return;

//                    ChatInitiatorDTO initiatorDTO = new ChatInitiatorDTO();
//                    initiatorDTO.ActionType = action == AppConstants.TYPE_START_GROUP_CHAT ? AppConstants.TYPE_UPDATE_START_GROUP_CHAT : action;
//                    initiatorDTO.ReasonCode = _JobjFromResponse[JsonKeys.ReasonCode] != null ? (int)_JobjFromResponse[JsonKeys.ReasonCode] : 0;
//                    initiatorDTO.ChatServerIP = (string)_JobjFromResponse[JsonKeys.ChatServerIp];
//                    initiatorDTO.ChatRegisterPort = _JobjFromResponse[JsonKeys.ChatRegistrationPort] != null ? (int)_JobjFromResponse[JsonKeys.ChatRegistrationPort] : 0;
//                    initiatorDTO.GroupID = _JobjFromResponse[JsonKeys.TagId] != null ? (long)_JobjFromResponse[JsonKeys.TagId] : 0;
//                    initiatorDTO.ServerPacketID = server_packet_id;

//                    HelperMethodsAuth.AuthHandlerInstance.ProcessStartGroupChat(initiatorDTO);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateStartGroupChat ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateAddGroupMember() //335
//        {
//            ProcessUpdateStartGroupChat();
//        }

//        #endregion "Call & Chat"

//        #region Notification

//        private void ProcessMyNotifications()
//        {
//            List<NotificationDTO> notificationList = new List<NotificationDTO>();
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    int scl = _JobjFromResponse[JsonKeys.Scroll] != null ? (int)_JobjFromResponse[JsonKeys.Scroll] : 0;
//                    int totalNotificationCount = (_JobjFromResponse[JsonKeys.TotalNotificationCount] != null) ? (int)_JobjFromResponse[JsonKeys.TotalNotificationCount] : 0;
//                    if (_JobjFromResponse[JsonKeys.NotificationList] != null)
//                    {
//                        JArray notificationListJSON = (JArray)_JobjFromResponse[JsonKeys.NotificationList];
//                        foreach (JObject notificationJSON in notificationListJSON)
//                        {
//                            lock (notificationList)
//                            {
//                                NotificationDTO dto = HelperMethodsModel.BindNotificationInfo(notificationJSON);
//                                NotificationDTO existingDTO = NotificationDTO.FindExistingDTO(dto);
//                                //existing and new notification comes
//                                if (existingDTO != null && existingDTO.UpdateTime < dto.UpdateTime)
//                                {
//                                    if (!dto.PreviousIds.Contains(existingDTO.ID))
//                                    {
//                                        dto.PreviousIds.Add(existingDTO.ID);
//                                    }

//                                    dto.PreviousIds.AddRange(existingDTO.PreviousIds);
//                                    RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(existingDTO);
//                                    RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);
//                                    notificationList.Add(dto);
//                                    new DeleteFromNotificationHistoryTable(dto.PreviousIds);
//                                }
//                                //existing but old notification comes
//                                else if (existingDTO != null && existingDTO.UpdateTime > dto.UpdateTime)
//                                {
//                                    int idx = RingDictionaries.Instance.NOTIFICATION_LISTS.IndexOf(existingDTO);
//                                    if (!RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds.Contains(dto.ID))
//                                    {
//                                        RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds.Add(dto.ID);
//                                    }
//                                    new DeleteFromNotificationHistoryTable(RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds);
//                                }
//                                // fresh insert
//                                else
//                                {
//                                    RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);
//                                    notificationList.Add(dto);
//                                }

//                                if (dto.UpdateTime > AppConstants.NOTIFICATION_MAX_UT) { AppConstants.NOTIFICATION_MAX_UT = dto.UpdateTime; }
//                                else if (dto.UpdateTime < AppConstants.NOTIFICATION_MIN_UT || AppConstants.NOTIFICATION_MIN_UT == 0) { AppConstants.NOTIFICATION_MIN_UT = dto.UpdateTime; }
//                            }
//                        }
//                        if (scl == 1) HelperMethodsAuth.AuthHandlerInstance.ShowAllNotificationCount(totalNotificationCount);
//                        HelperMethodsAuth.AuthHandlerInstance.AddNotificationFromServer(notificationList, scl == 1 ? true : false);
//                        HelperMethodsAuth.AuthHandlerInstance.NotificationListCount();
//                    }
//                }
//                else
//                {
//                    if (_JobjFromResponse[JsonKeys.Scroll] != null && (short)_JobjFromResponse[JsonKeys.Scroll] == 2)
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.RemoveSeeMorePanel();
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.NotificationListCount();
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessMyNotifications ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessSingleNotification()
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Id] != null)
//                {
//                    List<NotificationDTO> list = new List<NotificationDTO>();
//                    NotificationDTO dto = HelperMethodsModel.BindNotificationInfo(_JobjFromResponse);

//                    bool increase = HelperMethodsAuth.AuthHandlerInstance.CheckNotificationCount(dto);

//                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
//                    {
//                        NotificationDTO existingDTO = NotificationDTO.FindExistingDTO(dto);
//                        //existing and new notification comes
//                        if (existingDTO != null && existingDTO.UpdateTime < dto.UpdateTime)
//                        {
//                            dto.PreviousIds.AddRange(existingDTO.PreviousIds);
//                            dto.PreviousIds.Add(existingDTO.ID);

//                            RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(existingDTO);
//                            RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);

//                            if (!list.Contains(dto))
//                            {
//                                list.Add(dto);
//                            }

//                            new DeleteFromNotificationHistoryTable(dto.PreviousIds);
//                        }
//                        //existing but old notification comes
//                        else if (existingDTO != null && existingDTO.UpdateTime > dto.UpdateTime)
//                        {
//                            int idx = RingDictionaries.Instance.NOTIFICATION_LISTS.IndexOf(existingDTO);

//                            if (!existingDTO.PreviousIds.Contains(dto.ID))
//                            {
//                                RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds.Add(dto.ID);
//                            }
//                            new DeleteFromNotificationHistoryTable(RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds);
//                        }
//                        else if (existingDTO != null && existingDTO.UpdateTime == dto.UpdateTime && existingDTO.ID == dto.ID)
//                        {
//                            //do nothing....omitting duplicate packets
//                        }
//                        // fresh insert
//                        else
//                        {
//                            RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);
//                            if (!list.Contains(dto))
//                            {
//                                list.Add(dto);
//                            }
//                        }
//                    }

//                    if (dto.UpdateTime > AppConstants.NOTIFICATION_MAX_UT) { AppConstants.NOTIFICATION_MAX_UT = dto.UpdateTime; }

//                    else if (dto.UpdateTime < AppConstants.NOTIFICATION_MIN_UT || AppConstants.NOTIFICATION_MIN_UT == 0) { AppConstants.NOTIFICATION_MIN_UT = dto.UpdateTime; }


//                    //list.Add(dto);

//                    HelperMethodsAuth.AuthHandlerInstance.IncreaseAllnotificationCount(increase);
//                    HelperMethodsAuth.AuthHandlerInstance.AddNotificationFromServer(list, true);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessSingleNotification ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        #endregion

//        #region Unnecessary
//        //private void ProcessUpdateChangeFriendAccess()
//        //{
//        //    try
//        //    {
//        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//        //        {
//        //            UserBasicInfoDTO basicInfo = null;
//        //            int prevContactType;

//        //            foreach (KeyValuePair<long, UserBasicInfoDTO> entry in FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ToArray())
//        //            {
//        //                UserBasicInfoDTO temp = entry.Value;
//        //                if (temp.UserTableId == (long)_JobjFromResponse[JsonKeys.UserTableId])
//        //                {
//        //                    basicInfo = temp;
//        //                    break;
//        //                }
//        //            }

//        //            if (basicInfo != null)
//        //            {
//        //                prevContactType = basicInfo.ContactType;
//        //                bool isDownGradeChange = (int)_JobjFromResponse[JsonKeys.ContactType] == StatusConstants.ACCESS_CHAT_CALL && basicInfo.ContactType == StatusConstants.ACCESS_FULL;
//        //                if (isDownGradeChange)
//        //                {
//        //                    basicInfo.IncomingNotification = false;
//        //                    basicInfo.ContactType = (int)_JobjFromResponse[JsonKeys.ContactType];
//        //                    basicInfo.NewContactType = 0;
//        //                    basicInfo.IsChangeRequester = false;
//        //                }
//        //                else
//        //                {
//        //                    basicInfo.IncomingNotification = true;
//        //                    basicInfo.NewContactType = (int)_JobjFromResponse[JsonKeys.ContactType];
//        //                    basicInfo.IsChangeRequester = false;
//        //                }

//        //                List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
//        //                contactList.Add(basicInfo);
//        //                new InsertIntoContactListTable(contactList);

//        //                HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(basicInfo);
//        //            }

//        //        }
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        System.Diagnostics.Debug.WriteLine("Exception in ProcessUpdateChangeFriendAccess==>" + e.Message + "\n" + e.StackTrace);
//        //    }
//        //}

//        //private void ProcessUpdateAcceptFriendAccess()
//        //{
//        //    try
//        //    {

//        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//        //        {
//        //            UserBasicInfoDTO basicInfo = null;
//        //            int prevContactType;

//        //            foreach (KeyValuePair<long, UserBasicInfoDTO> entry in FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ToArray())
//        //            {
//        //                UserBasicInfoDTO temp = entry.Value;
//        //                if (temp.UserTableId == (long)_JobjFromResponse[JsonKeys.UserTableId])
//        //                {
//        //                    basicInfo = temp;
//        //                    break;
//        //                }
//        //            }

//        //            if (basicInfo != null)
//        //            {
//        //                prevContactType = basicInfo.ContactType;
//        //                if ((bool)_JobjFromResponse[JsonKeys.Accept])
//        //                {
//        //                    basicInfo.ContactType = (int)_JobjFromResponse[JsonKeys.ContactType];
//        //                }
//        //                basicInfo.NewContactType = 0;
//        //                basicInfo.IsChangeRequester = false;

//        //                List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
//        //                contactList.Add(basicInfo);
//        //                new InsertIntoContactListTable(contactList);

//        //                HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(basicInfo);
//        //            }

//        //        }
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        System.Diagnostics.Debug.WriteLine("Exception in ProcessUpdateAcceptFriendAccess==>" + e.Message + "\n" + e.StackTrace);
//        //    }
//        //} 
//        #endregion

//        #region ImageAlbum

//        private void ProcessMyAlbumImages()
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
//                    int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
//                    string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];

//                    Dictionary<string, Dictionary<long, ImageDTO>> TEMP_DICTIONARY = null;

//                    if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_MY_PROFILE_IMAGES;
//                    }
//                    else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_MY_COVER_IMAGES;
//                    }
//                    else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_MY_FEED_IMAGES;
//                    }



//                    if (_JobjFromResponse[JsonKeys.ImageList] != null && !string.IsNullOrEmpty(albumId))
//                    {
//                        JArray imageList = (JArray)_JobjFromResponse[JsonKeys.ImageList];

//                        foreach (JObject singleImage in imageList)
//                        {
//                            //ImageDTO imageDTO = new ImageDTO();
//                            //imageDTO.ImageId = (long)singleImage[JsonKeys.ImageId];
//                            //imageDTO.ImageUrl = (string)singleImage[JsonKeys.ImageUrl];
//                            //imageDTO.Caption = (string)singleImage[JsonKeys.ImageCaption];
//                            //imageDTO.ImageHeight = (int)singleImage[JsonKeys.ImageHeight];
//                            //imageDTO.ImageWidth = (int)singleImage[JsonKeys.ImageWidth];
//                            //imageDTO.ImageType = (int)singleImage[JsonKeys.ImageType];
//                            //imageDTO.NumberOfLikes = (long)singleImage[JsonKeys.NumberOfLikes];
//                            //imageDTO.iLike = (int)singleImage[JsonKeys.ILike];
//                            //imageDTO.NumberOfComments = (long)singleImage[JsonKeys.NumberOfComments];
//                            //imageDTO.iComment = (int)singleImage[JsonKeys.IComment];

//                            ImageDTO imageDTO = HelperMethodsModel.BindImageDetails(singleImage);

//                            //handling missing uid and fullname
//                            imageDTO.UserIdentity = DefaultSettings.userProfile.UserIdentity;
//                            imageDTO.FullName = DefaultSettings.userProfile.FullName;

//                            imageDTO.TotalImagesInAlbum = (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum];
//                            imageDTO.AlbumId = albumId;
//                            lock (TEMP_DICTIONARY)
//                            {
//                                if (TEMP_DICTIONARY.ContainsKey(sequence))
//                                {
//                                    if (TEMP_DICTIONARY[sequence].ContainsKey(imageDTO.ImageId))
//                                    {
//                                        TEMP_DICTIONARY[sequence][imageDTO.ImageId] = imageDTO;
//                                    }
//                                    else
//                                    {
//                                        TEMP_DICTIONARY[sequence].Add(imageDTO.ImageId, imageDTO);
//                                    }
//                                }
//                                else
//                                {
//                                    TEMP_DICTIONARY.Add(sequence, new Dictionary<long, ImageDTO>());
//                                    TEMP_DICTIONARY[sequence].Add(imageDTO.ImageId, imageDTO);
//                                }
//                            }

//                            lock (NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES)
//                            {
//                                if (NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES.ContainsKey(albumId))
//                                {
//                                    if (NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId].ContainsKey(imageDTO.ImageId))
//                                    {
//                                        NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId][imageDTO.ImageId] = imageDTO;
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId].Add(imageDTO.ImageId, imageDTO);
//                                    }

//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES.Add(albumId, new Dictionary<long, ImageDTO>());
//                                    NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId].Add(imageDTO.ImageId, imageDTO);
//                                }
//                            }


//                        }
//                    }

//                    if (TEMP_DICTIONARY.Count == seqTotal)
//                    {
//                        int albumType = HelperMethodsModel.GetImageTypeFromAlbum(albumId);
//                        //if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //{
//                        //    albumType = DefaultSettings.PROFILE_IMAGE;

//                        //}
//                        //else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //{
//                        //    albumType = DefaultSettings.COVER_IMAGE;
//                        //}
//                        //else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //{
//                        //    albumType = DefaultSettings.FEED_IMAGE;
//                        //}

//                        List<ImageDTO> list = new List<ImageDTO>();
//                        foreach (string key in TEMP_DICTIONARY.Keys)
//                        {
//                            list.AddRange(TEMP_DICTIONARY[key].Values.ToList());
//                        }

//                        HelperMethodsAuth.AuthHandlerInstance.AddMyImagesFromServer(albumType, (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum], list);
//                        TEMP_DICTIONARY.Clear();

//                    }
//                }

//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];
//                    HelperMethodsAuth.AuthHandlerInstance.NoImageFoundInMyAlbum(albumId);
//                }

//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessMyAlbumImages ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessFriendAlbumImages() // 109
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    long friendId = (long)_JobjFromResponse[JsonKeys.FriendId];
//                    string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
//                    string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];

//                    Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> TEMP_DICTIONARY = null;
//                    if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_FRIEND_PROFILE_IMAGES;
//                    }
//                    else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_FRIEND_COVER_IMAGES;
//                    }
//                    else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_FRIEND_FEED_IMAGES;
//                    }

//                    if (_JobjFromResponse[JsonKeys.ImageList] != null && !string.IsNullOrEmpty(albumId))
//                    {
//                        JArray imageList = (JArray)_JobjFromResponse[JsonKeys.ImageList];

//                        foreach (JObject singleImage in imageList)
//                        {
//                            ImageDTO imageDTO = HelperMethodsModel.BindImageDetails(singleImage);

//                            imageDTO.FriendId = friendId;

//                            //handling missing uid and fullname
//                            imageDTO.UserIdentity = friendId;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(friendId))
//                            {
//                                imageDTO.FullName = FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[friendId].FullName;
//                            }

//                            imageDTO.TotalImagesInAlbum = (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum];

//                            lock (TEMP_DICTIONARY)
//                            {
//                                if (TEMP_DICTIONARY.ContainsKey(friendId))
//                                {
//                                    if (TEMP_DICTIONARY[friendId].ContainsKey(sequence))
//                                    {
//                                        if (TEMP_DICTIONARY[friendId][sequence].ContainsKey(imageDTO.ImageId))
//                                        {
//                                            TEMP_DICTIONARY[friendId][sequence][imageDTO.ImageId] = imageDTO;
//                                        }
//                                        else
//                                        {
//                                            TEMP_DICTIONARY[friendId][sequence].Add(imageDTO.ImageId, imageDTO);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        TEMP_DICTIONARY[friendId].Add(sequence, new Dictionary<long, ImageDTO>());
//                                        TEMP_DICTIONARY[friendId][sequence].Add(imageDTO.ImageId, imageDTO);
//                                    }
//                                }
//                                else
//                                {
//                                    TEMP_DICTIONARY.Add(friendId, new Dictionary<string, Dictionary<long, ImageDTO>>());
//                                    TEMP_DICTIONARY[friendId].Add(sequence, new Dictionary<long, ImageDTO>());
//                                    TEMP_DICTIONARY[friendId][sequence].Add(imageDTO.ImageId, imageDTO);
//                                }
//                            }
//                            lock (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES)
//                            {
//                                if (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES.ContainsKey(friendId))
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId].ContainsKey(albumId))
//                                    {
//                                        if (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId].ContainsKey(imageDTO.ImageId))
//                                            NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId][imageDTO.ImageId] = imageDTO;
//                                        else
//                                            NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId].Add(imageDTO.ImageId, imageDTO);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId].Add(albumId, new Dictionary<long, ImageDTO>());
//                                        NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId].Add(imageDTO.ImageId, imageDTO);
//                                    }
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES.Add(friendId, new Dictionary<string, Dictionary<long, ImageDTO>>());
//                                    NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId].Add(albumId, new Dictionary<long, ImageDTO>());
//                                    NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId].Add(imageDTO.ImageId, imageDTO);
//                                }
//                            }
//                        }
//                    }

//                    if (TEMP_DICTIONARY[friendId].Count == seqTotal)
//                    {
//                        int albumType = Models.Utility.HelperMethodsModel.GetImageTypeFromAlbum(albumId);
//                        //if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //    albumType = DefaultSettings.PROFILE_IMAGE;
//                        //else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //    albumType = DefaultSettings.COVER_IMAGE;
//                        //else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //    albumType = DefaultSettings.FEED_IMAGE;

//                        List<ImageDTO> list = new List<ImageDTO>();
//                        foreach (string key in TEMP_DICTIONARY[friendId].Keys)
//                        {
//                            list.AddRange(TEMP_DICTIONARY[friendId][key].Values.ToList());
//                        }

//                        HelperMethodsAuth.AuthHandlerInstance.AddFriendImagesFromServer(friendId, albumType, (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum], list);
//                        TEMP_DICTIONARY.Clear();
//                    }
//                }
//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    long uid = (long)_JobjFromResponse[JsonKeys.FriendId];
//                    string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];
//                    HelperMethodsAuth.AuthHandlerInstance.NoImageFoundInFriendsAlbum(uid, albumId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessFriendAlbumImages ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        #endregion

//        #region "Sing IN /Sign UP/Sign out"

//        private void ProcessTypeUpdateDigitsVerify()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    string mbl = (_JobjFromResponse[JsonKeys.MobilePhone] != null) ? (string)_JobjFromResponse[JsonKeys.MobilePhone] : "";
//                    string mblDc = (_JobjFromResponse[JsonKeys.DialingCode] != null) ? (string)_JobjFromResponse[JsonKeys.DialingCode] : "";
//                    if (DefaultSettings.TMP_RECOVERY_MOBILE != null) //recovery
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.ChangeDigitsVerificationUI(true, mbl, mblDc);
//                        DefaultSettings.TMP_RECOVERY_MOBILE = null;
//                    }
//                    else if (DefaultSettings.userProfile == null || !DefaultSettings.userProfile.MobileNumber.Equals(mbl) || !DefaultSettings.userProfile.MobileDialingCode.Equals(mblDc))
//                    {
//                        if (DigitsVerificationThread.Instance != null) DigitsVerificationThread.Instance.StopThisThread();
//                        DigitsVerificationThread.Instance = null;
//                        HelperMethodsAuth.AuthHandlerInstance.ChangeDigitsVerificationUI(true, mbl, mblDc);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessHashtagMediaContents ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        private void processMultipleSessionWarning()
//        {
//            try
//            {
//                if (packet_attributes != null && packet_attributes.SessionID.Equals(DefaultSettings.LOGIN_SESSIONID))
//                {
//                    DefaultSettings.LOGIN_SESSIONID = null;
//                    DefaultSettings.LOGIN_USER_ID = 0;
//                    DefaultSettings.VALUE_LOGIN_USER_PASSWORD = null;
//                    HelperMethodsAuth.AuthHandlerInstance.MultipleSessionWarning();
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("Exception in processMultipleSessionWarning==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessInvalidLoginSession() //19
//        {
//            try
//            {

//                HelperMethodsAuth.AuthHandlerInstance.StopNetworkThread();

//                if (SessionValidationRequest.Instance == null)
//                {
//                    new SessionValidationRequest();
//                }
//                SessionValidationRequest.Instance.StartThis();

//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessInvalidLoginSession ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        #endregion "Sing IN /Sign UP/Sign out"

//        //private void ProcessSuggestionList() //106
//        //{
//        //    try
//        //    {
//        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success])
//        //        {
//        //            JArray ContactList = (JArray)_JobjFromResponse[JsonKeys.ContactList];

//        //            for (int j = 0; j < ContactList.Count; j++)
//        //            {
//        //                JObject SingleContact = (JObject)ContactList.ElementAt(j);

//        //                if (SingleContact != null)
//        //                {
//        //                    UserBasicInfoDTO suggestionDTO = new UserBasicInfoDTO();


//        //                    if (SingleContact[JsonKeys.UserIdentity] != null)
//        //                    {
//        //                        suggestionDTO.UserIdentity = long.Parse(SingleContact[JsonKeys.UserIdentity].ToString());
//        //                    }

//        //                    if (SingleContact[JsonKeys.UserTableId] != null)
//        //                    {
//        //                        suggestionDTO.UserTableId = (long)SingleContact[JsonKeys.UserTableId];
//        //                    }
//        //                    if (SingleContact[JsonKeys.FullName] != null)
//        //                    {
//        //                        suggestionDTO.FullName = (string)SingleContact[JsonKeys.FullName];
//        //                    }

//        //                    if (SingleContact[JsonKeys.Gender] != null)
//        //                    {
//        //                        suggestionDTO.Gender = (string)SingleContact[JsonKeys.Gender];
//        //                    }

//        //                    if (SingleContact[JsonKeys.ProfileImage] != null)
//        //                    {
//        //                        suggestionDTO.ProfileImage = (string)SingleContact[JsonKeys.ProfileImage];
//        //                    }

//        //                    if (SingleContact[JsonKeys.ProfileImageId] != null)
//        //                    {
//        //                        suggestionDTO.ProfileImageId = (long)SingleContact[JsonKeys.ProfileImageId];
//        //                    }

//        //                    if (SingleContact[JsonKeys.UpdateTime] != null)
//        //                    {
//        //                        suggestionDTO.UpdateTime = (long)SingleContact[JsonKeys.UpdateTime];
//        //                    }
//        //                    if (SingleContact[JsonKeys.NumberOfMutualFriend] != null)
//        //                    {
//        //                        suggestionDTO.NumberOfMutualFriends = (int)SingleContact[JsonKeys.NumberOfMutualFriend];
//        //                    }

//        //                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY)
//        //                    {
//        //                        if (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.ContainsKey(suggestionDTO.UserIdentity))
//        //                        {
//        //                            FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY[suggestionDTO.UserIdentity] = suggestionDTO;
//        //                        }
//        //                        else
//        //                        {
//        //                            FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Add(suggestionDTO.UserIdentity, suggestionDTO);
//        //                        }
//        //                    }
//        //                }

//        //            }
//        //            HelperMethodsAuth.AuthHandlerInstance.AddSuggestionsToUIList();

//        //        }


//        //    }
//        //    catch (Exception e)
//        //    {
//        //        System.Diagnostics.Debug.WriteLine("Exception in ProcessSuggestionList==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//        //        if (log.IsErrorEnabled)
//        //        {
//        //            log.Error("Exception in ProcessSuggestionList ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//        //        }
//        //    }
//        //}
//    }
//}

#endregion

using System;
using Auth.AppInterfaces;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;


namespace Auth.Parser
{
    public class AuthMsgParser
    {
        #region "Private Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(AuthMsgParser).Name);
        private string _JsonResponse = null;
        private JObject _JobjFromResponse = null;

        private int action;
        private string client_packet_id;
        private long server_packet_id;

        private byte[] byte_response = null;
        private int byte_start_pos;
        private int byte_data_length;
        private CommonPacketAttributes packet_attributes;
        IAuthSignalHandler iAuthSignalHandler;
        #endregion "Private Fields"

        #region "Public Fields"
        public static int SpamReasonListSeqCount = 0, FeedTagListSeqCount = 0, WorkListSequenceCount = 0, EducationListSequenceCount = 0, MediaItemsSequenceCount = 0, SkillListSequenceCount = 0, FriendContactListSequenceCount = 0, MutualFriendsSeqCount = 0, LikesSequnceCount = 0, CommentsSeqCount = 0, ImageLikesSequenceCount = 0, ImageCommentsSequenceCount = 0, ImageCommentLikesSequeceCount = 0, CircleMembersListSeqCount = 0, MediaAlbumListSeqCount = 0;// AlbumMediaContentListSeqCount = 0;
        #endregion "Public Fields"

        #region "Constructors"
        public AuthMsgParser(string resp, int action, long server_packet_id, string client_packet_id, IAuthSignalHandler iAuthSignalHandler)
        {
            this._JsonResponse = resp != null ? resp.Trim() : "";
            this.action = action;
            this.server_packet_id = server_packet_id;
            this.client_packet_id = client_packet_id;
            this.iAuthSignalHandler = iAuthSignalHandler;
        }

        public AuthMsgParser(byte[] resp, int action, long server_packet_id, string client_packet_id, int start_pos, int data_length, IAuthSignalHandler iAuthSignalHandler)
        {
            this.byte_response = resp;
            this.action = action;
            this.server_packet_id = server_packet_id;
            this.client_packet_id = client_packet_id;
            this.byte_start_pos = start_pos;
            this.byte_data_length = data_length;
            this.iAuthSignalHandler = iAuthSignalHandler;
        }
        #endregion "Constructors"

        #region "Initit StartProcess"

        public void Process()
        {
            try
            {
                if (_JsonResponse != null) //process json here
                {
                    if (this.action != 200)
                    {
#if AUTH_LOG
                        log.Info("Received for action=" + this.action + " from Auth JSON==> " + this._JsonResponse);
#endif
                        _JobjFromResponse = JObject.Parse(_JsonResponse);
                    }

                    if (!String.IsNullOrEmpty(client_packet_id))
                    {
                        lock (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY)
                        {
                            if (this.action == 200)
                            {
                                _JobjFromResponse = new JObject();
                                _JobjFromResponse.Add(JsonKeys.PacketId, client_packet_id);
                                _JobjFromResponse.Add(JsonKeys.Action, this.action);
                            }
                            else if (this.action == AppConstants.TYPE_ADD_STATUS
                                || this.action == AppConstants.TYPE_EDIT_STATUS
                                || this.action == AppConstants.TYPE_MULTIPLE_IMAGE_POST
                                || this.action == AppConstants.TYPE_SHARE_STATUS
                                || this.action == AppConstants.TYPE_ADD_STATUS_COMMENT
                                || this.action == AppConstants.TYPE_EDIT_STATUS_COMMENT
                                || this.action == AppConstants.TYPE_EDIT_IMAGE_COMMENT
                                || this.action == AppConstants.TYPE_EDIT_COMMENT_ON_MEDIA
                                || this.action == AppConstants.TYPE_START_GROUP_CHAT)
                                // || this.action == AppConstants.TYPE_ADD_IMAGE_COMMENT
                                // || this.action == AppConstants.TYPE_ADD_COMMENT_ON_MEDIA
                                
                            {
                                client_packet_id = client_packet_id + "_" + this.action;
                            }
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.AddOrUpdate(client_packet_id, _JobjFromResponse, (key, oldValue) => _JobjFromResponse);

                        }
                    }
                    this.processJsonResponse();
                }
                else  //process byte here
                {
                    if (!String.IsNullOrEmpty(client_packet_id))
                    {
                        _JobjFromResponse = new JObject();
                        _JobjFromResponse.Add(JsonKeys.PacketId, client_packet_id);
                        lock (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY)
                        {
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.AddOrUpdate(client_packet_id, _JobjFromResponse, (key, oldValue) => _JobjFromResponse);
                        }
                    }

                    if (this.action != 200)
                    {
                        this.packet_attributes = Parser.ParsePacket(this.byte_response, this.byte_start_pos, this.byte_data_length);
#if AUTH_LOG
                        log.Info("Received for action=" + this.action + " from Auth BYTE==> " + packet_attributes.ToString());
#endif
                    }
                    this.processByteResponse();
                }
            }
            catch (Exception e)
            {
                if (this._JsonResponse != null)
                {

                    log.Error("Auth parse Exception for action=" + this.action + " from Auth JSON==>" + e.Message + "\n" + e.StackTrace + " Response==>" + this._JsonResponse);

                }
                else if (this.packet_attributes != null)
                {

                    log.Error("Auth parse Exception for action=" + this.action + " from Auth BYTE==>" + e.Message + "\n" + e.StackTrace + " Response==>" + this.packet_attributes.ToString());

                }
            }

        }

        private void processByteResponse()
        {
            switch (this.action)
            {
                case AppConstants.TYPE_CONTACT_UTIDS:  //29
                    iAuthSignalHandler.Process_CONTACT_UTIDS_29(packet_attributes);
                    break;

                case AppConstants.TYPE_CONTACT_LIST:  //23
                    iAuthSignalHandler.Process_CONTACT_LIST_23(packet_attributes);
                    break;

                case AppConstants.TYPE_MULTIPLE_SESSION_WARNING: //75
                    iAuthSignalHandler.Process_MULTIPLE_SESSION_WARNING_75(packet_attributes);
                    break;

                case AppConstants.TYPE_UNWANTED_LOGIN: //79
                    iAuthSignalHandler.Process_UNWANTED_LOGIN_79(packet_attributes);
                    break;
                default:
                    break;

            }
        }

        private void processJsonResponse()
        {
            switch (this.action)
            {
                case AppConstants.TYPE_INVALID_LOGIN_SESSION: //19
                    iAuthSignalHandler.Process_INVALID_LOGIN_SESSION_19(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_SUGGESTION_IDS: //31
                    iAuthSignalHandler.Process_SUGGESTION_IDS_31(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_USERS_DETAILS: //32
                    iAuthSignalHandler.Process_USERS_DETAILS_32(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CONTACT_SEARCH: //34
                    iAuthSignalHandler.Process_CONTACT_SEARCH_34(_JobjFromResponse, client_packet_id);//   this.ProcessContactSearch();
                    break;

                case AppConstants.TYPE_COMMENTS_FOR_STATUS: //84
                    iAuthSignalHandler.Process_COMMENTS_FOR_STATUS_84(_JobjFromResponse);//   this.ProcessCommentsForStatus();
                    break;
                case AppConstants.TYPE_MEDIA_FEED://87
                    iAuthSignalHandler.Process_MEDIA_FEED_87(_JobjFromResponse, client_packet_id);//     this.ProcessBreakingNewsPortalFeeds();
                    break;
                case AppConstants.TYPE_NEWS_FEED: //88
                    iAuthSignalHandler.Process_NEWS_FEED_88(_JobjFromResponse, client_packet_id);//   this.ProcessNewsFeed();
                    break;
                case AppConstants.ACTION_MERGED_LIKES_LIST_OF_COMMENT: //1116
                    iAuthSignalHandler.Process_ACTION_MERGED_LIKES_LIST_OF_COMMENT_1116(_JobjFromResponse);
                    break;
                case AppConstants.ACTION_MERGED_COMMENTS_LIST: //1084
                    iAuthSignalHandler.Process_ACTION_MERGED_COMMENTS_LIST_1084(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LIKES_FOR_STATUS: //92
                    iAuthSignalHandler.Process_LIKES_FOR_STATUS_92(_JobjFromResponse);//   this.ProcessFetchLikesForStatus();
                    break;
                case AppConstants.TYPE_LIKES_FOR_IMAGE: //93
                    iAuthSignalHandler.Process_LIKES_FOR_IMAGE_93(_JobjFromResponse);//   this.ProcessLikersImage();
                    break;
                case AppConstants.TYPE_MEDIA_LIKE_LIST: //269
                    iAuthSignalHandler.Process_MEDIA_LIKE_LIST_269(_JobjFromResponse);//     this.ProcessMediaLikeList();
                    break;
                case AppConstants.TYPE_MY_BOOK: //94
                    iAuthSignalHandler.Process_MY_BOOK_94(_JobjFromResponse, client_packet_id);//    this.ProcessMyBook();
                    break;
                case AppConstants.TYPE_IMAGE_ALBUM_LIST: //96
                    iAuthSignalHandler.Process_IMAGE_ALBUM_LIST_96(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_MY_ALBUM_IMAGES: //97
                    iAuthSignalHandler.Process_MY_ALBUM_IMAGES_97(_JobjFromResponse);//  this.ProcessMyAlbumImages();
                    break;
                case AppConstants.TYPE_FRIEND_ALBUM_IMAGES: //109
                    iAuthSignalHandler.Process_FRIEND_ALBUM_IMAGES_109(_JobjFromResponse);// this.ProcessFriendAlbumImages();
                    break;
                case AppConstants.TYPE_FRIEND_NEWSFEED: //110
                    iAuthSignalHandler.Process_FRIEND_NEWSFEED_110(_JobjFromResponse, client_packet_id);//   this.ProcessFriendNewsFeeds();
                    break;
                //case AppConstants.TYPE_YOU_MAY_KNOW_LIST: //106
                //    this.ProcessSuggestionList();
                //    break;
                case AppConstants.TYPE_FRIEND_CONTACT_LIST: //107
                    iAuthSignalHandler.Process_FRIEND_CONTACT_LIST_107(_JobjFromResponse);// 
                    break;
                case AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141: //211
                    iAuthSignalHandler.Process_FRIEND_CONTACT_LIST_211(_JobjFromResponse);//    this.ProcessFriendContactList();
                    break;
                case AppConstants.TYPE_MY_NOTIFICATIONS: //111
                    iAuthSignalHandler.Process_MY_NOTIFICATIONS_111(_JobjFromResponse);//   this.ProcessMyNotifications();
                    break;
                case AppConstants.TYPE_SINGLE_NOTIFICATION: //113
                    iAuthSignalHandler.Process_SINGLE_NOTIFICATION_113(_JobjFromResponse);//    this.ProcessSingleNotification();
                    break;
                //case AppConstants.TYPE_SINGLE_STATUS_NOTIFICATION: //114
                //    this.ProcessSingleFeedDetails();
                //    break;
                case AppConstants.TYPE_SINGLE_FEED_SHARE_LIST://115
                    iAuthSignalHandler.Process_SINGLE_FEED_SHARE_LIST_115(_JobjFromResponse);//     this.ProcessSingleFeedShareList();
                    break;
                case AppConstants.TYPE_LIST_LIKES_OF_COMMENT: //116
                    iAuthSignalHandler.Process_LIST_LIKES_OF_COMMENT_116(_JobjFromResponse);//    this.ProcessFetchLikesForComment();
                    break;
                case AppConstants.TYPE_MULTIPLE_IMAGE_POST://117
                    iAuthSignalHandler.Process_MULTIPLE_IMAGE_POST_117(_JobjFromResponse);//    this.ProcessAddStatus();
                    break;
                case AppConstants.TYPE_MUTUAL_FRIENDS://118
                    iAuthSignalHandler.Process_MUTUAL_FRIENDS_118(_JobjFromResponse);//    this.ProcessTypeMutualFriends();
                    break;

                #region For Multisession
                case AppConstants.TYPE_ADD_FRIEND://127
                    iAuthSignalHandler.Process_ADD_FRIEND_127(_JobjFromResponse, server_packet_id);//     this.ProcessPushAddFriend();
                    break;
                case AppConstants.TYPE_DELETE_FRIEND://128
                    iAuthSignalHandler.Process_DELETE_FRIEND_128(_JobjFromResponse, action, server_packet_id);//      this.ProcessDeleteFriendUpdate();
                    break;
                case AppConstants.TYPE_ACCEPT_FRIEND://129
                    iAuthSignalHandler.Process_ACCEPT_FRIEND_129(_JobjFromResponse, server_packet_id);//      this.ProcessPushAcceptFriend();
                    break;
                #endregion

                case AppConstants.TYPE_ADD_STATUS: //177
                    iAuthSignalHandler.Process_ADD_STATUS_177(_JobjFromResponse);//    this.ProcessAddStatus();
                    break;
                case AppConstants.TYPE_START_GROUP_CHAT: //134
                    iAuthSignalHandler.Process_START_GROUP_CHAT_134(_JobjFromResponse, action, server_packet_id, client_packet_id);//    this.ProcessUpdateStartGroupChat();
                    break;
                case AppConstants.TYPE_ACTION_GET_MORE_FEED_IMAGE: //139
                    iAuthSignalHandler.Process_MORE_FEED_IMAGES_139(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_START_FRIEND_CHAT: //175
                    iAuthSignalHandler.Process_START_FRIEND_CHAT_175(_JobjFromResponse, action, server_packet_id, client_packet_id);//    this.ProcessUpdateStartFriendChat();
                    break;
                //case AppConstants.TYPE_LIKE_STATUS: //184
                //    iAuthSignalHandler.Process_LIKE_STATUS_184(_JobjFromResponse);//    this.ProcessUpdateLikeStatus(true);
                //    break;
                //case AppConstants.TYPE_UNLIKE_STATUS: //186
                //    iAuthSignalHandler.Process_UNLIKE_STATUS_186(_JobjFromResponse);//     this.ProcessUpdateUnlikeStatus(true);
                //    break;
                case AppConstants.TYPE_SINGLE_FRIEND_PRESENCE_INFO://199
                    iAuthSignalHandler.Process_SINGLE_FRIEND_PRESENCE_INFO_199(_JobjFromResponse);//   this.ProcessTypeFriendPresenceInfo();
                    break;
                case AppConstants.TYPE_UPDATE_DIGITS_VERIFY://209
                    iAuthSignalHandler.Process_UPDATE_DIGITS_VERIFY_209(_JobjFromResponse);//      this.ProcessTypeUpdateDigitsVerify();
                    break;
                case AppConstants.TYPTE_MISS_CALL_LIST: // 224
                    iAuthSignalHandler.Process_MISS_CALL_LIST_224(_JobjFromResponse);//     this.ProcessMissCallList();
                    break;
                case AppConstants.TYPE_WHO_SHARES_LIST: //249
                    iAuthSignalHandler.Process_WHO_SHARES_LIST_249(_JobjFromResponse);//    this.ProcessWhoSharesList();
                    break;
                case AppConstants.TYPE_MEDIA_SHARE_LIST://250
                    iAuthSignalHandler.Process_MEDIA_SHARE_LIST_250(_JobjFromResponse);// this.ProcessSingleFeedShareList();
                    break;
                case AppConstants.TYPE_MEDIA_ALBUM_LIST: //256
                    iAuthSignalHandler.Process_MEDIA_ALBUM_LIST_256(_JobjFromResponse);//   this.ProcessMediaAlbumList();
                    break;
                case AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST: //261
                    iAuthSignalHandler.Process_MEDIA_ALBUM_CONTENT_LIST_261(_JobjFromResponse);//   this.ProcessMediaAlbumContentList();
                    break;
                //case AppConstants.TYPE_MEDIA_CONTENT_DETAILS://262
                //    this.ProcessMediaContentDetails();
                //    break;
                
                case AppConstants.TYPE_MEDIA_COMMENT_LIST://270
                    iAuthSignalHandler.Process_MEDIA_COMMENT_LIST_270(_JobjFromResponse);//     this.ProcessMediaCommentList();
                    break;
                case AppConstants.TYPE_MEDIACOMMENT_LIKE_LIST://271
                    iAuthSignalHandler.Process_MEDIACOMMENT_LIKE_LIST_271(_JobjFromResponse);//     this.ProcessMediaCommentLikeList();
                    break;
                case AppConstants.TYPE_VIEW_DOING_LIST: //273
                    iAuthSignalHandler.Process_VIEW_DOING_LIST_273(_JobjFromResponse);//       this.ProcessDoingList();
                    break;
                case AppConstants.TYPE_VIEW_TAGGED_LIST: //274
                    iAuthSignalHandler.Process_VIEW_TAGGED_LIST_274(_JobjFromResponse);//     this.ProcessFeedTagList();
                    break;
                case AppConstants.TYPE_MEDIA_SUGGESTION://277
                    iAuthSignalHandler.Process_MEDIA_SUGGESTION_277(_JobjFromResponse);//  this.ProcessMediaSearchSuggestions();
                    break;
                case AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD: //278
                    iAuthSignalHandler.Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(_JobjFromResponse);//  this.ProcessMediaFullSearch();
                    break;
                case AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS: //279
                    iAuthSignalHandler.Process_HASHTAG_MEDIA_CONTENTS_279(_JobjFromResponse);//    this.ProcessHashtagMediaContents();
                    break;
                case AppConstants.TYPE_HASHTAG_SUGGESTION: //280
                    iAuthSignalHandler.Process_HASHTAG_SUGGESTION_280(_JobjFromResponse);//    this.ProcessHashtagSuggestion();
                    break;
                case AppConstants.TYPE_SEARCH_TRENDS: //281
                    iAuthSignalHandler.Process_SEARCH_TRENDS_281(_JobjFromResponse);//    this.ProcessMediaSearchTrends();
                    break;
                case AppConstants.TYPE_STORE_CONTACT_LIST: //284
                    iAuthSignalHandler.Process_STORE_CONTACT_LIST_284(_JobjFromResponse);//   this.ProcessStoreContactList();
                    break;
                case AppConstants.TYPE_CELEBRITY_CATEGORY_LIST: //285
                    iAuthSignalHandler.Process_CELEBRITIES_CATEGORIES_LIST_285(_JobjFromResponse);//  this.ProcessNewsPortalCategoriesList();
                    break;
                case AppConstants.TYPE_CELEBRITY_LIST: // 286
                    iAuthSignalHandler.Process_CELEBRITY_DISCOVER_LIST_286(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CELEBRITY_FEED: //288
                    iAuthSignalHandler.Process_TYPE_CELEBRITY_FEED_288(_JobjFromResponse, client_packet_id);//   this.ProcessStoreContactList();
                    break;
                case AppConstants.TYPE_NEWSPORTAL_CATEGORIES_LIST: //294
                    iAuthSignalHandler.Process_NEWSPORTAL_CATEGORIES_LIST_294(_JobjFromResponse);//  this.ProcessNewsPortalCategoriesList();
                    break;
                case AppConstants.TYPE_NEWSPORTAL_FEED: //295
                    iAuthSignalHandler.Process_NEWSPORTAL_FEED_295(_JobjFromResponse, client_packet_id);//   this.ProcessNewsPortalFeed();
                    break;
                case AppConstants.TYPE_NEWSPORTAL_LIST://299
                    iAuthSignalHandler.Process_NEWSPORTAL_LIST_299(_JobjFromResponse);//  this.ProcessDiscoverNewsPortals();
                    break;
                case AppConstants.TYPE_NEWSPORTAL_BREAKING_FEED://302
                    iAuthSignalHandler.Process_NEWSPORTAL_BREAKING_FEED_302(_JobjFromResponse);//     this.ProcessBreakingNewsPortalFeeds();
                    break;
                case AppConstants.TYPE_BUSINESSPAGE_BREAKING_FEED://303
                    iAuthSignalHandler.Process_BUSINESSPAGE_BREAKING_FEED_303(_JobjFromResponse);//     this.ProcessBreakingPageFeeds();
                    break;
                case AppConstants.TYPE_BUSINESS_PAGE_FEED: //306
                    iAuthSignalHandler.Process_BUSINESS_PAGE_FEED_306(_JobjFromResponse, client_packet_id);//    this.ProcessBusinessPageFeeds();
                    break;
                //case AppConstants.TYPE_MEDIA_PAGE_FEED://307
                //    iAuthSignalHandler.Process_MEDIA_PAGE_FEED_307(_JobjFromResponse, client_packet_id);//     this.ProcessBreakingNewsPortalFeeds();
                //    break;
                case AppConstants.TYPE_MEDIA_PAGE_TRENDING_FEED: //308
                    iAuthSignalHandler.Process_MEDIA_PAGE_TRENDING_FEED_308(_JobjFromResponse);//    this.ProcessBusinessPageFeeds();
                    break;
                case AppConstants.TYPE_ALL_SAVED_FEEDS: //309
                    iAuthSignalHandler.Process_ALL_SAVED_FEEDS_309(_JobjFromResponse, client_packet_id);//    this.ProcessBusinessPageFeeds();
                    break;
                case AppConstants.ACTION_UPDATE_LIKE_UNLIKE_COMMENT: //1323
                    iAuthSignalHandler.Process_ACTION_UPDATE_LIKE_UNLIKE_COMMENT_1323(_JobjFromResponse); 
                    break;
                case AppConstants.TYPE_UPDATE_LIKE_COMMENT: //323
                    iAuthSignalHandler.Process_UPDATE_LIKE_COMMENT_323(_JobjFromResponse);//    this.ProcessUpdateLikeComment();
                    break;
                case AppConstants.TYPE_UPDATE_UNLIKE_COMMENT: //325
                    iAuthSignalHandler.Process_UPDATE_UNLIKE_COMMENT_325(_JobjFromResponse);//  this.ProcessUpdateUnLikeComment();
                    break;
                case AppConstants.TYPE_UPDATE_ADD_FRIEND: //327
                    iAuthSignalHandler.Process_UPDATE_ADD_FRIEND_327(_JobjFromResponse);//  ProcessAddFriendUpdate();
                    break;
                case AppConstants.TYPE_UPDATE_DELETE_FRIEND: //328
                    iAuthSignalHandler.Process_UPDATE_DELETE_FRIEND_328(_JobjFromResponse, action, server_packet_id);//     ProcessDeleteFriendUpdate();
                    break;
                case AppConstants.TYPE_UPDATE_ACCEPT_FRIEND: //329
                    iAuthSignalHandler.Process_UPDATE_ACCEPT_FRIEND_329(_JobjFromResponse);//    ProcessAcceptFriendUpdate();
                    break;
                case AppConstants.TYPE_UPDATE_START_GROUP_CHAT: //334
                    iAuthSignalHandler.Process_UPDATE_START_GROUP_CHAT_334(_JobjFromResponse, action, server_packet_id, client_packet_id);//   this.ProcessUpdateStartGroupChat();
                    break;
                case AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER: //335
                    iAuthSignalHandler.Process_UPDATE_ADD_GROUP_MEMBER_335(_JobjFromResponse, action, server_packet_id, client_packet_id);
                    //      this.ProcessUpdateAddGroupMember();
                    break;
                case AppConstants.TYPE_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO://336
                    iAuthSignalHandler.Process_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO_336(_JobjFromResponse);//  this.ProcessTypeFriendPresenceInfo();
                    break;
                //case AppConstants.TYPE_UPDATE_DELETE_CIRCLE:
                //    iAuthSignalHandler.Process_UPDATE_DELETE_CIRCLET_352(_JobjFromResponse);
                //    break;
                //case AppConstants.TYPE_UPDATE_REMOVE_CIRCLE_MEMBER:
                //    iAuthSignalHandler.Process_UPDATE_REMOVE_CIRCLE_MEMBER_354(_JobjFromResponse);
                //    break;
                //case AppConstants.TYPE_UPDATE_ADD_CIRCLE_MEMBER:
                //    iAuthSignalHandler.Process_UPDATE_ADD_CIRCLE_MEMBER_356(_JobjFromResponse);
                //    break;
                //case AppConstants.TYPE_UPDATE_EDIT_CIRCLE_MEMBER:
                //    iAuthSignalHandler.Process_UPDATE_EDIT_CIRCLE_MEMBER_358(_JobjFromResponse);
                //    break;
                case AppConstants.TYPE_UPDATE_SEND_REGISTER: //374
                    iAuthSignalHandler.Process_UPDATE_SEND_REGISTER_374(_JobjFromResponse);//    this.ProcessUpdateSendRegister();
                    break;
                case AppConstants.TYPE_UPDATE_START_FRIEND_CHAT: //375
                    iAuthSignalHandler.Process_UPDATE_START_FRIEND_CHAT_375(_JobjFromResponse, action, server_packet_id, client_packet_id);//   this.ProcessUpdateStartFriendChat();
                    break;
                case AppConstants.TYPE_UPDATE_ADD_STATUS: //377
                    iAuthSignalHandler.Process_UPDATE_ADD_STATUS_377(_JobjFromResponse);//   this.ProcessUpdateAddStatus(true);
                    break;
                case AppConstants.TYPE_UPDATE_SHARE_STATUS: //391
                    iAuthSignalHandler.Process_UPDATE_SHARE_STATUS_391(_JobjFromResponse);//    this.ProcessUpdateAddStatus();
                    break;
                case AppConstants.TYPE_UPDATE_EDIT_STATUS: //378
                    iAuthSignalHandler.Process_UPDATE_EDIT_STATUS_378(_JobjFromResponse);//     this.ProcessUpdateEditStatus();
                    break;
                case AppConstants.TYPE_UPDATE_DELETE_STATUS: //379
                    iAuthSignalHandler.Process_UPDATE_DELETE_STATUS_379(_JobjFromResponse);//     this.ProcessUpdateDeleteStatus();
                    break;

                case AppConstants.TYPE_UPDATE_LIKE_STATUS: //384
                    iAuthSignalHandler.Process_UPDATE_LIKE_STATUS_384(_JobjFromResponse);//   this.ProcessUpdateLikeStatus();
                    break;
                case AppConstants.ACTION_MERGED_UPDATE_LIKE_UNLIKE: //1384
                    iAuthSignalHandler.Process_MERGED_UPDATE_LIKE_UNLIKE_1384(_JobjFromResponse);//   this.ProcessUpdateLikeStatus();
                    break;
                case AppConstants.TYPE_UPDATE_LIKE_UNLIKE_IMAGE: //385 
                    iAuthSignalHandler.Process_UPDATE_LIKE_UNLIKE_IMAGE_385(_JobjFromResponse);//   this.ProcessUpdateLikeUnlikeImage();
                    break;
                case AppConstants.TYPE_UPDATE_UNLIKE_STATUS: //386
                    iAuthSignalHandler.Process_UPDATE_UNLIKE_STATUS_386(_JobjFromResponse);//  this.ProcessUpdateUnlikeStatus();
                    break;
                case AppConstants.TYPE_UPDATE_EDIT_STATUS_COMMENT: //389
                    iAuthSignalHandler.Process_UPDATE_EDIT_STATUS_COMMENT_389(_JobjFromResponse);//     this.ProcessUpdateEditComment();
                    break;

                #region Image tasks
                case AppConstants.TYPE_UPDATE_ADD_IMAGE_COMMENT: //380
                    iAuthSignalHandler.Process_UPDATE_ADD_IMAGE_COMMENT_380(_JobjFromResponse);//      this.ProcessUpdateAddImageComment();
                    break;
                case AppConstants.TYPE_UPDATE_ADD_STATUS_COMMENT: //381
                    iAuthSignalHandler.Process_UPDATE_ADD_STATUS_COMMENT_381(_JobjFromResponse);//      this.ProcessUpdateAddStatusComment();
                    break;
                case AppConstants.ACTION_MERGED_UPDATE_ADD_COMMENT: //1381
                    iAuthSignalHandler.Process_ACTION_MERGED_UPDATE_ADD_COMMENT_1381(_JobjFromResponse);//      this.ProcessUpdateAddStatusComment();
                    break;
                case AppConstants.TYPE_UPDATE_DELETE_IMAGE_COMMENT: //382
                    iAuthSignalHandler.Process_UPDATE_DELETE_IMAGE_COMMENT_382(_JobjFromResponse);//    this.ProcessUpdateDeleteImageComment();
                    break;
                case AppConstants.ACTION_MERGED_UPDATE_DELETE_COMMENT: //1383
                    iAuthSignalHandler.Process_ACTION_MERGED_UPDATE_DELETE_COMMENT_1383(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_DELETE_STATUS_COMMENT: //383
                    iAuthSignalHandler.Process_UPDATE_DELETE_STATUS_COMMENT_383(_JobjFromResponse);//    this.ProcessUpdateDeleteStatusComment();
                    break;
                case AppConstants.TYPE_UPDATE_EDIT_IMAGE_COMMENT: //394
                    iAuthSignalHandler.Process_UPDATE_EDIT_IMAGE_COMMENT_394(_JobjFromResponse);//     this.ProcessUpdateEditImageComment();
                    break;
                case AppConstants.TYPE_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT: //397
                    iAuthSignalHandler.Process_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT_397(_JobjFromResponse);//    this.ProcessUpdateLikeUnlikeImageComment();
                    break;
                //case AppConstants.TYPE_IMAGE_DETAILS: //121
                //    this.ProcessSingleImageDetail();
                //    break;
                case AppConstants.TYPE_COMMENTS_FOR_IMAGE: // 89
                    iAuthSignalHandler.Process_COMMENTS_FOR_IMAGE_89(_JobjFromResponse);//      this.ProcessImageComments();
                    break;
                case AppConstants.TYPE_IMAGE_COMMENT_LIKES: //196
                    iAuthSignalHandler.Process_IMAGE_COMMENT_LIKES_196(_JobjFromResponse);//      this.ProcessLikersImageComment();
                    break;
                
                case AppConstants.TYPE_LIKE_UNLIKE_IMAGE_COMMENT: // 197
                    iAuthSignalHandler.Process_LIKE_UNLIKE_IMAGE_COMMENT_197(_JobjFromResponse);//   this.ProcessLikeUnlikeImageComment();
                    break;
                //case AppConstants.TYPE_ADD_IMAGE_COMMENT: //180
                //    iAuthSignalHandler.Process_ADD_IMAGE_COMMENT_180(_JobjFromResponse);//  this.ProcessAddImageComment();
                //    break;
                //case AppConstants.TYPE_EDIT_IMAGE_COMMENT: //194
                //    iAuthSignalHandler.Process_EDIT_IMAGE_COMMENT_194(_JobjFromResponse);//      this.ProcessEditImageComment();
                //    break;
                #endregion

                case AppConstants.TYPE_UPDATE_LIKEUNLIKE_MEDIA: //464
                    iAuthSignalHandler.Process_UPDATE_LIKEUNLIKE_MEDIA_464(_JobjFromResponse);//     this.ProcessUpdateLikeUnlikeMedia();
                    break;
                case AppConstants.TYPE_UPDATE_COMMENT_MEDIA: //465
                    iAuthSignalHandler.Process_UPDATE_COMMENT_MEDIA_465(_JobjFromResponse);//    this.ProcessUpdateCommentMedia();
                    break;
                case AppConstants.TYPE_UPDATE_EDITCOMMENT_MEDIA: //466
                    iAuthSignalHandler.Process_UPDATE_EDITCOMMENT_MEDIA_466(_JobjFromResponse);//     this.ProcessUpdateEditCommentMedia();
                    break;
                case AppConstants.TYPE_UPDATE_DELETECOMMENT_MEDIA: //467
                    iAuthSignalHandler.Process_UPDATE_DELETECOMMENT_MEDIA_467(_JobjFromResponse);//     this.ProcessUpdateDeleteCommentMedia();
                    break;
                //case AppConstants.TYPE_UPDATE_LIKEUNLIKECOMMENT_MEDIA: //468
                //    iAuthSignalHandler.Process_UPDATE_LIKEUNLIKECOMMENT_MEDIA_468(_JobjFromResponse);//      this.ProcessUpdateLikeUnlikeCommentMedia();
                //    break;
                case AppConstants.TYPE_UPDATE_STORE_CONTACT_LIST: //484
                    iAuthSignalHandler.Process_UPDATE_STORE_CONTACT_LIST_484(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_NEW_CIRCLE: //51
                    iAuthSignalHandler.Process_NEW_CIRCLE_51(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LEAVE_CIRCLE: //53
                    iAuthSignalHandler.Process_LEAVE_CIRCLE_53(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CIRCLE_LIST: //70
                    iAuthSignalHandler.Process_CIRCLE_LIST_70(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CIRCLE_MEMBERS_LIST: //99
                    iAuthSignalHandler.Process_CIRCLE_MEMBERS_LIST_99(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_SEARCH_CIRCLE_MEMBER: //101
                    iAuthSignalHandler.Process_SEARCH_CIRCLE_MEMBER_101(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_DELETE_CIRCLE: //152
                    iAuthSignalHandler.Process_DELETE_CIRCLE_152(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_REMOVE_CIRCLE_MEMBER:
                    iAuthSignalHandler.Process_REMOVE_CIRCLE_MEMBER_154(_JobjFromResponse);
                    break;

                case AppConstants.TYPE_CIRCLE_NEWSFEED: //198
                    iAuthSignalHandler.Process_CIRCLE_NEWSFEED_198(_JobjFromResponse, client_packet_id);
                    break;
                case AppConstants.TYPE_CIRCLE_DETAILS:
                    ////case AppConstants.TYPE_UPDATE_DELETE_CIRCLE: //352
                    ////case AppConstants.TYPE_UPDATE_REMOVE_CIRCLE_MEMBER: //354
                    ////case AppConstants.TYPE_UPDATE_ADD_CIRCLE_MEMBER: //356
                    ////case AppConstants.TYPE_UPDATE_EDIT_CIRCLE_MEMBER: //358
                    //  AMPCircle.Instance.ProcessCircle(action, _JobjFromResponse);
                    iAuthSignalHandler.Process_CIRCLE_DETAILS_52(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_PRESENCE: //78
                    iAuthSignalHandler.Process_PRESENCE_78(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_WORK: // 227;
                    iAuthSignalHandler.Process_ADD_WORK_227(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_EDUCATION: // 231;
                    iAuthSignalHandler.Process_ADD_EDUCATION_231(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LIST_WORK: // 234
                    iAuthSignalHandler.Process_LIST_WORK_234(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LIST_EDUCATION: //235
                    iAuthSignalHandler.Process_LIST_EDUCATION_235(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LIST_SKILL: //236
                    iAuthSignalHandler.Process_LIST_SKILL_236(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_SKILL: // 237;
                    iAuthSignalHandler.Process_ADD_SKILL_237(_JobjFromResponse);//     AMPProfileAbout.Instance.ProcessAbout(action, _JobjFromResponse);
                    break;
                case AppConstants.TYPE_SPAM_REASON_LIST: //1001
                    iAuthSignalHandler.Process_SPAM_REASON_LIST_1001(_JobjFromResponse);//    this.ProcessSpamReasonList();
                    break;
                case AppConstants.TYPE_NEWSFEED_EDIT_HISTORY_LIST: //1016
                    iAuthSignalHandler.Process_NEWSFEED_EDIT_HISTORY_LIST_1016(_JobjFromResponse);//    this.ProcessSpamReasonList();
                    break;
                case AppConstants.TYPE_COMMENT_EDIT_HISTORY_LIST: //1021
                    iAuthSignalHandler.Process_COMMENT_EDIT_HISTORY_LIST_1021(_JobjFromResponse);//    this.ProcessSpamReasonList();
                    break;
                case AppConstants.TYPE_GET_WALLET_INFORMATION: //1026
                    iAuthSignalHandler.Process_TYPE_GET_WALLET_INFORMATION_1026(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION: //1027
                    iAuthSignalHandler.Process_TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION_1027(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_TRANSACTION_HISTORY:
                    iAuthSignalHandler.Process_TYPE_GET_TRANSACTION_HISTORY_1031(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_REFERRAL_REQUEST: //1037
                    iAuthSignalHandler.PROCESS_TYPE_UPDATE_REFERRAL_REQUEST_1037(_JobjFromResponse);
                    break;
                //case AppConstants.TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST: //1039
                //    iAuthSignalHandler.PROCESS_TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST_1039(_JobjFromResponse);
                 //   break;
                case AppConstants.TYPE_GET_GIFT_PRODUCTS://1039: // get_gift_products
                    iAuthSignalHandler.PROCESS_TYPE_GET_WALLET_GIFT_1039(_JobjFromResponse);
                    break;
                case  AppConstants.TYPE_GET_COIN_EARNING_RULE://1045: // coin earning rule list
                    iAuthSignalHandler.PROCESS_TYPE_GET_COIN_EARNING_RULE_LIST_1045(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_COIN_BUNDLE_LIST:// 1038: // coin_bundle_list
                    iAuthSignalHandler.PROCESS_TYPE_GET_WALLET_COIN_BUNDLE_LIST_1038(_JobjFromResponse);
                    break;
                //case AppConstants.TYPE_GET_TOP_CONTRIBUTORS:// 1044://contributor_list

                //    break;
                case AppConstants.TYPE_DAILY_CHECKIN_HISTORY://1054:
                    iAuthSignalHandler.PROCESS_TYPE_GET_DAILY_CHECKIN_HISTORY_1054(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_PAYMENT_RECEIVED://1046://payment received
                    iAuthSignalHandler.PROCESS_TYPE_PAYMENT_RECEIVED_1046(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_REFERRAL_NETWORK_SUMMARY:
                    iAuthSignalHandler.PROCESS_TYPE_REFERRAL_NETWORK_SUMMARY_1041(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_MY_REFERRALS_LIST:
                    iAuthSignalHandler.PROCESS_TYPE_MY_REFERRAL_LIST_1042(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_RECEIVED_GIFT:
                    iAuthSignalHandler.PROCESS_TYPE_GIFT_RECEIVED_1052(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_FOLLOW_UNFOLLOW_USER:
                    iAuthSignalHandler.PROCESS_UPDATE_FOLLOW_UNFOLLOW_USER(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_START_LIVE_STREAM: //2001
                    iAuthSignalHandler.PROCESS_TYPE_LIVE_STREAM_REQUEST_2001(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_FEATURED_LIVE_STREAMS: //2004
                    iAuthSignalHandler.PROCESS_TYPE_GET_FEATURED_LIVE_STREAMS_REQUEST_2004(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_RECENT_STREAMS: //2005
                    iAuthSignalHandler.PROCESS_TYPE_GET_RECENT_STREAMS_REQUEST_2005(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_LIVE_STREAM: //2006
                    iAuthSignalHandler.PROCESS_UPDATE_LIVE_STREAM_2006(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_STREAM_CATEGORY_LIST: //2007
                    iAuthSignalHandler.PROCESS_TYPE_GET_STREAM_CATEGORY_LIST_REQUEST_2007(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_MOST_VIEWED_STREAMS: //2008
                    iAuthSignalHandler.PROCESS_TYPE_GET_MOST_VIEWED_STREAMS_2008(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_NEAREST_STREAMS: //2009
                    iAuthSignalHandler.PROCESS_TYPE_GET_NEAREST_STREAMS_2009(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS: //2010
                    iAuthSignalHandler.PROCESS_TYPE_SEARCH_LIVE_STREAMS_REQUEST_2010(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_STREAMING_LIKE_COUNT: //2012
                    iAuthSignalHandler.PROCESS_UPDATE_STREAMING_LIKE_COUNT_2012(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_FOLLOWING_STREAMS: //2013
                    iAuthSignalHandler.PROCESS_GET_FOLLOWING_STREAMS_REQUEST_2013(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_CATEGORY_WISE_STREAM_COUNT: //2014
                    iAuthSignalHandler.PROCESS_GET_CATEGORY_WISE_STREAM_COUNT_REQUEST_2014(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CREATE_CHANNEL: //2015
                    iAuthSignalHandler.PROCESS_CREATE_CHANNEL_REQUEST_2015(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_CHANNEL_CATEGORY_LIST: //2016
                    iAuthSignalHandler.PROCESS_GET_CHANNEL_CATEGORY_LIST_REQUEST_2016(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_CHANNEL_INFO: //2017
                    iAuthSignalHandler.PROCESS_UPDATE_CHANNEL_INFO_REQUEST_2017(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_FOLLOWING_CHANNEL_LIST: //2019
                    iAuthSignalHandler.PROCESS_GET_FOLLOWING_CHANNEL_LIST_REQUEST_2019(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_MOST_VIEWED_CHANNEL_LIST: //2020
                    iAuthSignalHandler.PROCESS_GET_MOST_VIEWED_CHANNEL_LIST_REQUEST_2020(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_OWN_CHANNEL_LIST: //2021
                    iAuthSignalHandler.PROCESS_GET_OWN_CHANNEL_LIST_REQUEST_2021(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_CHANNEL_PROFILE_IMAGE: //2024
                    iAuthSignalHandler.PROCESS_ADD_CHANNEL_PROFILE_IMAGE_REQUEST_2024(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_CHANNEL_COVER_IMAGE: //2025
                    iAuthSignalHandler.PROCESS_ADD_CHANNEL_COVER_IMAGE_REQUEST_2025(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_MEDIA_TO_CHANNEL: //2026
                    iAuthSignalHandler.PROCESS_ADD_MEDIA_TO_CHANNEL_REQUEST_2026(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_CHANNEL_MEDIA_LIST: //2028
                    iAuthSignalHandler.PROCESS_GET_CHANNEL_MEDIA_LIST_REQUEST_2028(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_CHANNEL_MEDIA_STATUS: //2029
                    iAuthSignalHandler.PROCESS_UPDATE_CHANNEL_MEDIA_STATUS_2029(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_FEATURED_CHANNEL_LIST: //2030
                    iAuthSignalHandler.PROCESS_GET_FEATURED_CHANNEL_LIST_REQUEST_2030(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_CHANNEL_MEDIA_INFO: //2031
                    iAuthSignalHandler.PROCESS_UPDATE_CHANNEL_MEDIA_INFO_REQUEST_2031(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST: //2032
                    iAuthSignalHandler.PROCESS_SEARCH_CHANNEL_LIST_REQUEST_2032(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_CHANNEL_PLAYLIST: //2031
                    iAuthSignalHandler.PROCESS_GET_CHANNEL_PLAYLIST_REQUEST_2033(_JobjFromResponse);
                    break;
                default:
                    break;
            }
        }

        #endregion "Initit StartProcess"
    }
}

=======
﻿#region "Previous Code"
//using System;
//using System.Collections.Concurrent;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using Auth.Service.FriendList;
//using Auth.Service.SigninSignup;
//using Auth.Service.Suggestion;
//using Auth.utility;
//using Auth.utility.Feed;
//using log4net;
//using Models.Constants;
//using Models.DAO;
//using Models.Entity;
//using Models.Stores;
//using Models.Utility;
//using Newtonsoft.Json.Linq;
//using Auth.Service.Chat;
//using Auth.AppInterfaces;


//namespace Auth.Parser
//{
//    public class AuthMsgParser
//    {
//        #region "Private Fields"
//        private readonly ILog log = LogManager.GetLogger(typeof(AuthMsgParser).Name);
//        private string _JsonResponse = null;
//        private JObject _JobjFromResponse = null;

//        private int action;
//        private string client_packet_id;
//        private long server_packet_id;

//        private byte[] byte_response = null;
//        private int byte_start_pos;
//        private int byte_data_length;
//        private CommonPacketAttributes packet_attributes;
//        IAuthSignalHandler iAuthSignalHandler;
//        #endregion "Private Fields"

//        #region "Public Fields"
//        public static int SpamReasonListSeqCount = 0, FeedTagListSeqCount = 0, WorkListSequenceCount = 0, EducationListSequenceCount = 0, MediaItemsSequenceCount = 0, SkillListSequenceCount = 0, FriendContactListSequenceCount = 0, MutualFriendsSeqCount = 0, LikesSequnceCount = 0, CommentsSeqCount = 0, ImageLikesSequenceCount = 0, ImageCommentsSequenceCount = 0, ImageCommentLikesSequeceCount = 0, CircleMembersListSeqCount = 0, MediaAlbumListSeqCount = 0;// AlbumMediaContentListSeqCount = 0;
//        #endregion "Public Fields"

//        #region "Constructors"
//        public AuthMsgParser(string resp, int action, long server_packet_id, string client_packet_id, IAuthSignalHandler iAuthSignalHandler)
//        {
//            this._JsonResponse = resp != null ? resp.Trim() : "";
//            this.action = action;
//            this.server_packet_id = server_packet_id;
//            this.client_packet_id = client_packet_id;
//            this.iAuthSignalHandler = iAuthSignalHandler;
//        }

//        public AuthMsgParser(byte[] resp, int action, long server_packet_id, string client_packet_id, int start_pos, int data_length, IAuthSignalHandler iAuthSignalHandler)
//        {
//            this.byte_response = resp;
//            this.action = action;
//            this.server_packet_id = server_packet_id;
//            this.client_packet_id = client_packet_id;
//            this.byte_start_pos = start_pos;
//            this.byte_data_length = data_length;
//            this.iAuthSignalHandler = iAuthSignalHandler;
//        }
//        #endregion "Constructors"

//        #region "Initit StartProcess"

//        public void StartProcess()
//        {
//            try
//            {
//                if (_JsonResponse != null) //process json here
//                {
//                    if (this.action != 200)
//                    {
//                        //if (DefaultSettings.DEBUG)
//                        //{
//                        //    log.Info("Received for action=" + this.action + " from Auth JSON==> " + this._JsonResponse);
//                        //}
//#if AUTH_LOG
//                        log.Info("Received for action=" + this.action + " from Auth JSON==> " + this._JsonResponse);
//#endif
//                        _JobjFromResponse = JObject.Parse(_JsonResponse);
//                    }

//                    if (!String.IsNullOrEmpty(client_packet_id))
//                    {
//                        lock (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY)
//                        {
//                            if (this.action == 200)
//                            {
//                                _JobjFromResponse = new JObject();
//                                _JobjFromResponse.Add(JsonKeys.PacketId, client_packet_id);
//                                _JobjFromResponse.Add(JsonKeys.Action, this.action);
//                            }
//                            else if (this.action == AppConstants.TYPE_ADD_STATUS
//                                || this.action == AppConstants.TYPE_EDIT_STATUS
//                                || this.action == AppConstants.TYPE_MULTIPLE_IMAGE_POST
//                                || this.action == AppConstants.TYPE_SHARE_STATUS
//                                || this.action == AppConstants.TYPE_ADD_STATUS_COMMENT
//                                || this.action == AppConstants.TYPE_EDIT_STATUS_COMMENT
//                                || this.action == AppConstants.TYPE_ADD_IMAGE_COMMENT
//                                || this.action == AppConstants.TYPE_EDIT_IMAGE_COMMENT
//                                || this.action == AppConstants.TYPE_ADD_COMMENT_ON_MEDIA
//                                || this.action == AppConstants.TYPE_EDIT_COMMENT_ON_MEDIA
//                                || this.action == AppConstants.TYPE_START_GROUP_CHAT)
//                            {
//                                client_packet_id = client_packet_id + "_" + this.action;
//                            }
//                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.AddOrUpdate(client_packet_id, _JobjFromResponse, (key, oldValue) => _JobjFromResponse);

//                        }
//                    }
//                    this.processJsonResponse();
//                }
//                else  //process byte here
//                {
//                    if (!String.IsNullOrEmpty(client_packet_id))
//                    {
//                        _JobjFromResponse = new JObject();
//                        _JobjFromResponse.Add(JsonKeys.PacketId, client_packet_id);
//                        lock (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY)
//                        {
//                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.AddOrUpdate(client_packet_id, _JobjFromResponse, (key, oldValue) => _JobjFromResponse);
//                        }
//                    }

//                    if (this.action != 200)
//                    {
//                        this.packet_attributes = Parser.ParsePacket(this.byte_response, this.byte_start_pos, this.byte_data_length);
//#if AUTH_LOG
//                        log.Info("Received for action=" + this.action + " from Auth BYTE==> " + packet_attributes.ToString());
//#endif
//                    }
//                    this.processByteResponse();
//                }
//            }
//            catch (Exception e)
//            {
//                if (this._JsonResponse != null)
//                {

//                    log.Error("Auth parse Exception for action=" + this.action + " from Auth JSON==>" + e.Message + "\n" + e.StackTrace + " Response==>" + this._JsonResponse);

//                }
//                else if (this.packet_attributes != null)
//                {

//                    log.Error("Auth parse Exception for action=" + this.action + " from Auth BYTE==>" + e.Message + "\n" + e.StackTrace + " Response==>" + this.packet_attributes.ToString());

//                }
//            }

//        }

//        private void processByteResponse()
//        {
//            switch (this.action)
//            {
//                case AppConstants.TYPE_CONTACT_UTIDS:  //29
//                    this.ProcessByteContactUtids();
//                    break;

//                case AppConstants.TYPE_CONTACT_LIST:  //23
//                    this.ProcessByteContactList();
//                    break;

//                case AppConstants.TYPE_MULTIPLE_SESSION_WARNING: //75
//                    this.processMultipleSessionWarning();
//                    break;
//                default:
//                    break;

//            }
//        }

//        private void processJsonResponse()
//        {
//            switch (this.action)
//            {
//                case AppConstants.TYPE_INVALID_LOGIN_SESSION: //19
//                    this.ProcessInvalidLoginSession();
//                    break;
//                case AppConstants.TYPE_SUGGESTION_IDS: //31
//                    this.ProcessSuggestionUtids();
//                    break;
//                case AppConstants.TYPE_USERS_DETAILS: //32
//                    this.ProcessSuggestionUsersDetails();
//                    break;
//                case AppConstants.TYPE_CONTACT_SEARCH: //34
//                    this.ProcessContactSearch();
//                    break;

//                case AppConstants.TYPE_COMMENTS_FOR_STATUS: //84
//                    this.ProcessCommentsForStatus();
//                    break;
//                case AppConstants.TYPE_MEDIA_FEED: //87
//                    this.ProcessMediaFeed();
//                    break;
//                case AppConstants.TYPE_NEWS_FEED: //88
//                    this.ProcessNewsFeed();
//                    break;
//                case AppConstants.TYPE_LIKES_FOR_STATUS: //92
//                    this.ProcessFetchLikesForStatus();
//                    break;
//                case AppConstants.TYPE_MY_BOOK: //94
//                    this.ProcessMyBook();
//                    break;
//                case AppConstants.TYPE_MY_ALBUM_IMAGES: //97
//                    this.ProcessMyAlbumImages();
//                    break;
//                case AppConstants.TYPE_FRIEND_ALBUM_IMAGES: //109
//                    this.ProcessFriendAlbumImages();
//                    break;
//                case AppConstants.TYPE_FRIEND_NEWSFEED: //110
//                    this.ProcessFriendNewsFeeds();
//                    break;
//                //case AppConstants.TYPE_YOU_MAY_KNOW_LIST: //106
//                //    this.ProcessSuggestionList();
//                //    break;
//                case AppConstants.TYPE_FRIEND_CONTACT_LIST: //107
//                case AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141: //211
//                    this.ProcessFriendContactList();
//                    break;
//                case AppConstants.TYPE_MY_NOTIFICATIONS: //111
//                    this.ProcessMyNotifications();
//                    break;
//                case AppConstants.TYPE_SINGLE_NOTIFICATION: //113
//                    this.ProcessSingleNotification();
//                    break;
//                //case AppConstants.TYPE_SINGLE_STATUS_NOTIFICATION: //114
//                //    this.ProcessSingleFeedDetails();
//                //    break;
//                case AppConstants.TYPE_SINGLE_FEED_SHARE_LIST://115
//                    this.ProcessSingleFeedShareList();
//                    break;
//                case AppConstants.TYPE_LIST_LIKES_OF_COMMENT: //116
//                    this.ProcessFetchLikesForComment();
//                    break;
//                case AppConstants.TYPE_MULTIPLE_IMAGE_POST://117
//                    this.ProcessAddStatus();
//                    break;
//                case AppConstants.TYPE_MUTUAL_FRIENDS://118
//                    this.ProcessTypeMutualFriends();
//                    break;

//<<<<<<< .mine
//                #region For Multisession
//                case AppConstants.TYPE_ADD_FRIEND://127
//                    this.ProcessPushAddFriend();
//                    break;
//                case AppConstants.TYPE_DELETE_FRIEND://128
//                    this.ProcessDeleteFriendUpdate();
//                    break;
//                case AppConstants.TYPE_ACCEPT_FRIEND://129
//                    this.ProcessPushAcceptFriend();
//                    break;
//                #endregion
//=======
//                case AppConstants.TYPE_ADD_STATUS: //177
//                    this.ProcessAddStatus();
//                    break;
//                case AppConstants.TYPE_START_GROUP_CHAT: //134
//                    this.ProcessUpdateStartGroupChat();
//                    break;
//                case AppConstants.TYPE_START_FRIEND_CHAT: //175
//                    this.ProcessUpdateStartFriendChat();
//                    break;
//                case AppConstants.TYPE_LIKE_STATUS: //184
//                    this.ProcessUpdateLikeStatus(true);
//                    break;
//                case AppConstants.TYPE_UNLIKE_STATUS: //186
//                    this.ProcessUpdateUnlikeStatus(true);
//                    break;
//                case AppConstants.TYPE_SINGLE_FRIEND_PRESENCE_INFO://199
//                    this.ProcessTypeFriendPresenceInfo();
//                    break;
//                case AppConstants.TYPE_UPDATE_DIGITS_VERIFY://209
//                    this.ProcessTypeUpdateDigitsVerify();
//                    break;
//                case AppConstants.TYPTE_MISS_CALL_LIST: // 224
//                    this.ProcessMissCallList();
//                    break;
//                case AppConstants.TYPE_WHO_SHARES_LIST: //249
//                    this.ProcessWhoSharesList();
//                    break;
//                case AppConstants.TYPE_MEDIA_SHARE_LIST://250
//                    this.ProcessSingleFeedShareList();
//                    break;
//                case AppConstants.TYPE_MEDIA_ALBUM_LIST: //256
//                    this.ProcessMediaAlbumList();
//                    break;
//                case AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST: //261
//                    this.ProcessMediaAlbumContentList();
//                    break;
//                //case AppConstants.TYPE_MEDIA_CONTENT_DETAILS://262
//                //    this.ProcessMediaContentDetails();
//                //    break;
//                case AppConstants.TYPE_MEDIA_LIKE_LIST: //269
//                    this.ProcessMediaLikeList();
//                    break;
//                case AppConstants.TYPE_MEDIA_COMMENT_LIST://270
//                    this.ProcessMediaCommentList();
//                    break;
//                case AppConstants.TYPE_MEDIACOMMENT_LIKE_LIST://271
//                    this.ProcessMediaCommentLikeList();
//                    break;
//                case AppConstants.TYPE_VIEW_DOING_LIST: //273
//                    this.ProcessDoingList();
//                    break;
//                case AppConstants.TYPE_VIEW_TAGGED_LIST: //274
//                    this.ProcessFeedTagList();
//                    break;
//                case AppConstants.TYPE_MEDIA_SUGGESTION://277
//                    this.ProcessMediaSearchSuggestions();
//                    break;
//                case AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD: //278
//                    this.ProcessMediaFullSearch();
//                    break;
//                case AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS: //279
//                    this.ProcessHashtagMediaContents();
//                    break;
//                case AppConstants.TYPE_HASHTAG_SUGGESTION: //280
//                    this.ProcessHashtagSuggestion();
//                    break;
//                case AppConstants.TYPE_SEARCH_TRENDS: //281
//                    this.ProcessMediaSearchTrends();
//                    break;
//                case AppConstants.TYPE_STORE_CONTACT_LIST: //284
//                    this.ProcessStoreContactList();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_CATEGORIES_LIST: //294
//                    this.ProcessNewsPortalCategoriesList();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_FEED: //295
//                    this.ProcessNewsPortalFeed();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_LIST://299
//                    this.ProcessDiscoverNewsPortals();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_BREAKING_FEED://302
//                    this.ProcessBreakingNewsPortalFeeds();
//                    break;
//                case AppConstants.TYPE_BUSINESS_PAGE_FEED: //306
//                    this.ProcessBusinessPageFeeds();
//                    break;
//                case AppConstants.TYPE_MEDIA_PAGE_FEED://307
//                    this.ProcessMediaCloudFeeds();
//                    break;
//                case AppConstants.TYPE_MEDIA_PAGE_TRENDING_FEED://308
//                    this.ProcessTrendingMediaCloudFeeds();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKE_COMMENT: //323
//                    this.ProcessUpdateLikeComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_UNLIKE_COMMENT: //323
//                    this.ProcessUpdateUnLikeComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_FRIEND: //327
//                    ProcessAddFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_FRIEND: //328
//                    ProcessDeleteFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_ACCEPT_FRIEND: //329
//                    ProcessAcceptFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_START_GROUP_CHAT: //334
//                    this.ProcessUpdateStartGroupChat();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER: //335
//                    this.ProcessUpdateAddGroupMember();
//                    break;
//                case AppConstants.TYPE_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO://336
//                    this.ProcessTypeFriendPresenceInfo();
//                    break;
//                case AppConstants.TYPE_UPDATE_SEND_REGISTER: //374
//                    this.ProcessUpdateSendRegister();
//                    break;
//                case AppConstants.TYPE_UPDATE_START_FRIEND_CHAT: //375
//                    this.ProcessUpdateStartFriendChat();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_STATUS: //377
//                    this.ProcessUpdateAddStatus(true);
//                    break;
//                case AppConstants.TYPE_UPDATE_SHARE_STATUS: //391
//                    this.ProcessUpdateAddStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDIT_STATUS: //378
//                    this.ProcessUpdateEditStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_STATUS: //379
//                    this.ProcessUpdateDeleteStatus();
//                    break;
//>>>>>>> .r1628

//                case AppConstants.TYPE_ADD_STATUS: //177
//                    this.ProcessAddStatus();
//                    break;
//                case AppConstants.TYPE_START_GROUP_CHAT: //134
//                    this.ProcessUpdateStartGroupChat();
//                    break;
//                case AppConstants.TYPE_START_FRIEND_CHAT: //175
//                    this.ProcessUpdateStartFriendChat();
//                    break;
//                case AppConstants.TYPE_LIKE_STATUS: //184
//                    this.ProcessUpdateLikeStatus(true);
//                    break;
//                case AppConstants.TYPE_UNLIKE_STATUS: //186
//                    this.ProcessUpdateUnlikeStatus(true);
//                    break;
//                case AppConstants.TYPE_SINGLE_FRIEND_PRESENCE_INFO://199
//                    this.ProcessTypeFriendPresenceInfo();
//                    break;
//                case AppConstants.TYPE_UPDATE_DIGITS_VERIFY://209
//                    this.ProcessTypeUpdateDigitsVerify();
//                    break;
//                case AppConstants.TYPTE_MISS_CALL_LIST: // 224
//                    this.ProcessMissCallList();
//                    break;
//                case AppConstants.TYPE_WHO_SHARES_LIST: //249
//                    this.ProcessWhoSharesList();
//                    break;
//                case AppConstants.TYPE_MEDIA_SHARE_LIST://250
//                    this.ProcessSingleFeedShareList();
//                    break;
//                case AppConstants.TYPE_MEDIA_ALBUM_LIST: //256
//                    this.ProcessMediaAlbumList();
//                    break;
//                case AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST: //261
//                    this.ProcessMediaAlbumContentList();
//                    break;
//                //case AppConstants.TYPE_MEDIA_CONTENT_DETAILS://262
//                //    this.ProcessMediaContentDetails();
//                //    break;
//                case AppConstants.TYPE_MEDIA_LIKE_LIST: //269
//                    this.ProcessMediaLikeList();
//                    break;
//                case AppConstants.TYPE_MEDIA_COMMENT_LIST://270
//                    this.ProcessMediaCommentList();
//                    break;
//                case AppConstants.TYPE_MEDIACOMMENT_LIKE_LIST://271
//                    this.ProcessMediaCommentLikeList();
//                    break;
//                case AppConstants.TYPE_VIEW_DOING_LIST: //273
//                    this.ProcessDoingList();
//                    break;
//                case AppConstants.TYPE_VIEW_TAGGED_LIST: //274
//                    this.ProcessFeedTagList();
//                    break;
//                case AppConstants.TYPE_MEDIA_SUGGESTION://277
//                    this.ProcessMediaSearchSuggestions();
//                    break;
//                case AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD: //278
//                    this.ProcessMediaFullSearch();
//                    break;
//                case AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS: //279
//                    this.ProcessHashtagMediaContents();
//                    break;
//                case AppConstants.TYPE_HASHTAG_SUGGESTION: //280
//                    this.ProcessHashtagSuggestion();
//                    break;
//                case AppConstants.TYPE_SEARCH_TRENDS: //281
//                    this.ProcessMediaSearchTrends();
//                    break;
//                case AppConstants.TYPE_STORE_CONTACT_LIST: //284
//                    this.ProcessStoreContactList();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_CATEGORIES_LIST: //294
//                    this.ProcessNewsPortalCategoriesList();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_FEED: //295
//                    this.ProcessNewsPortalFeed();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_LIST://299
//                    this.ProcessDiscoverNewsPortals();
//                    break;
//                case AppConstants.TYPE_NEWSPORTAL_BREAKING_FEED://302
//                    this.ProcessBreakingNewsPortalFeeds();
//                    break;
//                case AppConstants.TYPE_BUSINESS_PAGE_FEED: //306
//                    this.ProcessBusinessPageFeeds();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKE_COMMENT: //323
//                    this.ProcessUpdateLikeComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_UNLIKE_COMMENT: //323
//                    this.ProcessUpdateUnLikeComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_FRIEND: //327
//                    ProcessAddFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_FRIEND: //328
//                    ProcessDeleteFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_ACCEPT_FRIEND: //329
//                    ProcessAcceptFriendUpdate();
//                    break;
//                case AppConstants.TYPE_UPDATE_START_GROUP_CHAT: //334
//                    this.ProcessUpdateStartGroupChat();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER: //335
//                    this.ProcessUpdateAddGroupMember();
//                    break;
//                case AppConstants.TYPE_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO://336
//                    this.ProcessTypeFriendPresenceInfo();
//                    break;
//                case AppConstants.TYPE_UPDATE_SEND_REGISTER: //374
//                    this.ProcessUpdateSendRegister();
//                    break;
//                case AppConstants.TYPE_UPDATE_START_FRIEND_CHAT: //375
//                    this.ProcessUpdateStartFriendChat();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_STATUS: //377
//                    this.ProcessUpdateAddStatus(true);
//                    break;
//                case AppConstants.TYPE_UPDATE_SHARE_STATUS: //391
//                    this.ProcessUpdateAddStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDIT_STATUS: //378
//                    this.ProcessUpdateEditStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_STATUS: //379
//                    this.ProcessUpdateDeleteStatus();
//                    break;

//                case AppConstants.TYPE_UPDATE_LIKE_STATUS: //384
//                    this.ProcessUpdateLikeStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKE_UNLIKE_IMAGE: //385 
//                    this.ProcessUpdateLikeUnlikeImage();
//                    break;
//                case AppConstants.TYPE_UPDATE_UNLIKE_STATUS: //386
//                    this.ProcessUpdateUnlikeStatus();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDIT_STATUS_COMMENT: //389
//                    this.ProcessUpdateEditComment();
//                    break;

//                #region Image tasks
//                case AppConstants.TYPE_UPDATE_ADD_IMAGE_COMMENT: //380
//                    this.ProcessUpdateAddImageComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_ADD_STATUS_COMMENT: //381
//                    this.ProcessUpdateAddStatusComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_IMAGE_COMMENT: //382
//                    this.ProcessUpdateDeleteImageComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETE_STATUS_COMMENT: //383
//                    this.ProcessUpdateDeleteStatusComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDIT_IMAGE_COMMENT: //394
//                    this.ProcessUpdateEditImageComment();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT: //397
//                    this.ProcessUpdateLikeUnlikeImageComment();
//                    break;
//                //case AppConstants.TYPE_IMAGE_DETAILS: //121
//                //    this.ProcessSingleImageDetail();
//                //    break;
//                case AppConstants.TYPE_COMMENTS_FOR_IMAGE: // 89
//                    this.ProcessImageComments();
//                    break;
//                case AppConstants.TYPE_IMAGE_COMMENT_LIKES: //196
//                    this.ProcessLikersImageComment();
//                    break;
//                case AppConstants.TYPE_LIKES_FOR_IMAGE: //93
//                    this.ProcessLikersImage();
//                    break;
//                case AppConstants.TYPE_LIKE_UNLIKE_IMAGE_COMMENT: // 197
//                    this.ProcessLikeUnlikeImageComment();
//                    break;
//                case AppConstants.TYPE_ADD_IMAGE_COMMENT: //180
//                    this.ProcessAddImageComment();
//                    break;
//                case AppConstants.TYPE_EDIT_IMAGE_COMMENT: //194
//                    this.ProcessEditImageComment();
//                    break;
//                #endregion

//                case AppConstants.TYPE_UPDATE_LIKEUNLIKE_MEDIA: //464
//                    this.ProcessUpdateLikeUnlikeMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_COMMENT_MEDIA: //465
//                    this.ProcessUpdateCommentMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_EDITCOMMENT_MEDIA: //466
//                    this.ProcessUpdateEditCommentMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_DELETECOMMENT_MEDIA: //467
//                    this.ProcessUpdateDeleteCommentMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_LIKEUNLIKECOMMENT_MEDIA: //468
//                    this.ProcessUpdateLikeUnlikeCommentMedia();
//                    break;
//                case AppConstants.TYPE_UPDATE_STORE_CONTACT_LIST: //484
//                    this.ProcessStoreContactList();
//                    break;
//                case AppConstants.TYPE_NEW_CIRCLE: //51
//                case AppConstants.TYPE_LEAVE_CIRCLE: //53
//                case AppConstants.TYPE_CIRCLE_LIST: //70
//                case AppConstants.TYPE_CIRCLE_MEMBERS_LIST: //99
//                case AppConstants.TYPE_SEARCH_CIRCLE_MEMBER: //101
//                case AppConstants.TYPE_DELETE_CIRCLE: //152
//                case AppConstants.TYPE_REMOVE_CIRCLE_MEMBER:
//                case AppConstants.TYPE_CIRCLE_NEWSFEED: //198
//                case AppConstants.TYPE_CIRCLE_DETAILS:
//                    //case AppConstants.TYPE_UPDATE_DELETE_CIRCLE: //352
//                    //case AppConstants.TYPE_UPDATE_REMOVE_CIRCLE_MEMBER: //354
//                    //case AppConstants.TYPE_UPDATE_ADD_CIRCLE_MEMBER: //356
//                    //case AppConstants.TYPE_UPDATE_EDIT_CIRCLE_MEMBER: //358
//                    AMPCircle.Instance.ProcessCircle(action, _JobjFromResponse);
//                    break;
//                case AppConstants.TYPE_PRESENCE: //78
//                case AppConstants.TYPE_ADD_WORK: // 227;
//                case AppConstants.TYPE_ADD_EDUCATION: // 231;
//                case AppConstants.TYPE_LIST_WORK: // 234
//                case AppConstants.TYPE_LIST_EDUCATION: //235
//                case AppConstants.TYPE_LIST_SKILL: //236
//                case AppConstants.TYPE_ADD_SKILL: // 237;
//                    AMPProfileAbout.Instance.ProcessAbout(action, _JobjFromResponse);
//                    break;
//                case AppConstants.TYPE_SPAM_REASON_LIST: //1001
//                    this.ProcessSpamReasonList();
//                    break;
//                default:
//                    break;
//            }
//        }

//        #endregion "Initit StartProcess"

//        #region "Contact List"

//        private void ProcessByteContactUtids() //29
//        {
//            try
//            {
//                if (Auth.Service.FriendList.ContactUtIdsRequest.Instance != null && Auth.Service.FriendList.ContactUtIdsRequest.Instance.IsRunning())
//                {
//                    Auth.Service.FriendList.ContactUtIdsRequest.Instance._TotalRecords = this.packet_attributes.TotalRecords;
//                }
//                if (this.packet_attributes.Success && this.packet_attributes.UserIDs != null)
//                {
//                    byte[] userIDsBytes = this.packet_attributes.UserIDs;

//                    for (int i = 0; i < userIDsBytes.Length; i += 11)
//                    {
//                        long userTableId = Parser.GetLong(userIDsBytes, i, 8);
//                        int ct = (int)userIDsBytes[i + 8];
//                        int mb = (int)userIDsBytes[i + 9];
//                        int fns = (int)userIDsBytes[i + 10];

//                        UserBasicInfoDTO user = new UserBasicInfoDTO();
//                        user.UserTableId = userTableId;
//                        user.MatchedBy = mb;
//                        user.FriendShipStatus = fns;
//                        FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY[user.UserTableId] = user;
//                    }

//                    if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == this.packet_attributes.TotalRecords)
//                    {
//                        List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
//                        foreach (Models.Entity.UserBasicInfoDTO item in FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Values)
//                        {
//                            userList.Add(item);
//                        }
//                        FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Clear();
//                        userList = Auth.utility.HelperMethodsAuth.SortFriendListByFriendshipStatusAccepted(userList);
//                        foreach (UserBasicInfoDTO user in userList)
//                        {
//                            FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY[user.UserTableId] = user;
//                        }
//                        if (ContactListRequest.Instance == null || ContactListRequest.Instance.IsRunning() == false)
//                        {
//                            new ContactListRequest();
//                        }
//                    }
//                }
//                else
//                {
//                    HelperMethodsAuth.AuthHandlerInstance.UI_ProcessByteContactList(null, this.packet_attributes.Success);
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("Exception in TYPE_CONTACT_UTIDS 29==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }

//        private void ProcessByteContactList() //23
//        {
//            try
//            {
//                List<UserBasicInfoDTO> basicInfos = null;
//                if (this.packet_attributes.Success)
//                {
//                    if (this.packet_attributes.ContactList != null && this.packet_attributes.ContactList.Count > 0)
//                    {
//                        basicInfos = new List<UserBasicInfoDTO>();

//                        foreach (CommonPacketAttributes contact in this.packet_attributes.ContactList)
//                        {
//                            UserBasicInfoDTO entity = null;
//                            FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.TryGetValue(contact.UserID, out entity);
//                            if (entity != null)
//                            {
//                                FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.TryRemove(contact.UserID, out entity);
//                                entity.UserTableId = contact.UserID;
//                                entity.UserIdentity = contact.UserIdentity;
//                                if (contact.UserIdentity == 0)
//                                {
//#if AUTH_LOG
//                                    log.Info("Contact's UserIdentity==0 Found, UserTableId==>" + entity.UserTableId);
//#endif
//                                }
//                                entity.FullName = contact.UserName;
//                                //entity.BlockedValue = contact.BlockValue;
//                                entity.ContactType = contact.ContactType;
//                                entity.ContactAddedTime = contact.ContactAddedTime;
//                                entity.Delete = contact.Deleted;
//                                entity.FriendShipStatus = (entity.Delete == StatusConstants.STATUS_DELETED) ? 0 : contact.FriendshipStatus;
//                                //entity.NewContactType = contact.NewContactType;
//                                entity.ProfileImage = contact.ProfileImage;
//                                entity.ProfileImageId = contact.ProfileImageId;
//                                entity.ContactUpdateTime = contact.ContactUpdateTime;
//                                entity.UpdateTime = contact.UpdateTime;
//                                entity.NumberOfMutualFriends = contact.MutualFriendCount;
//                                //if (contact.IsChangeRequester > 0) entity.IsChangeRequester = true;
//                                //else entity.IsChangeRequester = false;
//                                entity.CallAccess = contact.CallAccess;
//                                entity.ChatAccess = contact.ChatAccess;
//                                entity.FeedAccess = contact.FeedAccess;
//                                entity.BlockedBy = (entity.CallAccess == 0 && entity.ChatAccess == 0 && entity.FeedAccess == 0) ? 1 : 0;

//                                UserBasicInfoDTO tempDTO;
//                                if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(entity.UserIdentity, out tempDTO))
//                                {
//                                    entity.ContactAddedReadUnread = tempDTO.ContactAddedReadUnread;
//                                    entity.ChatBgUrl = tempDTO.ChatBgUrl;
//                                    if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
//                                    {
//                                        entity.FavoriteRank = tempDTO.FavoriteRank;
//                                    }
//                                    if (entity.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
//                                    {
//                                        entity.BlockedBy = tempDTO.BlockedBy;
//                                    }

//                                    if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && entity.FriendShipStatus != tempDTO.FriendShipStatus)
//                                    {
//                                        entity.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
//                                        entity.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
//                                        entity.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
//                                    }
//                                    else
//                                    {
//                                        entity.ImSoundEnabled = tempDTO.ImSoundEnabled;
//                                        entity.ImNotificationEnabled = tempDTO.ImNotificationEnabled;
//                                        entity.ImPopupEnabled = tempDTO.ImPopupEnabled;
//                                    }
//                                }

//                                if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && entity.ContactAddedReadUnread != SettingsConstants.RECENT_FRND_READ &&
//                                    (entity.ContactAddedTime > (Models.Utility.ModelUtility.CurrentTimeMillisLocal() - Models.Utility.ModelUtility.MilliSecondsInSevenDay)))
//                                {
//                                    entity.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;
//                                }

//                                if (entity.UserIdentity > 0)
//                                {
//                                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                                    {
//                                        //if ((!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(entity.UserIdentity)
//                                        //    || (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(entity.UserIdentity) && FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[entity.UserIdentity].FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_INCOMING))
//                                        //    && entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
//                                        //{
//                                        //    AppConstants.ADD_FRIEND_NOTIFICATION_COUNT++;
//                                        //}
//                                        //else if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED || entity.FriendShipStatus == 0)
//                                        //{
//                                        //    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(entity.UserIdentity)
//                                        //        && FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[entity.UserIdentity].FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING
//                                        //        && AppConstants.ADD_FRIEND_NOTIFICATION_COUNT > 0)
//                                        //    {
//                                        //        AppConstants.ADD_FRIEND_NOTIFICATION_COUNT--;
//                                        //    }
//                                        //}
//                                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[entity.UserIdentity] = entity;
//                                    }

//                                    lock (FriendDictionaries.Instance.UTID_UID_DICTIONARY)
//                                    {
//                                        FriendDictionaries.Instance.UTID_UID_DICTIONARY[entity.UserTableId] = entity.UserIdentity;
//                                    }
//                                }
//                                lock (FriendDictionaries.Instance.TEMP_CONTACT_LIST)
//                                {
//                                    FriendDictionaries.Instance.TEMP_CONTACT_LIST.Add(entity);
//                                }

//                                basicInfos.Add(entity);
//                            }
//                        }
//                        //   HelperMethodsAuth.AuthHandlerInstance.AddFriendListFromServer(basicInfos);
//                    }

//                    //if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0 && DefaultSettings.FRIEND_LIST_LOADED == false)
//                    //{
//                    //    HelperMethodsAuth.AuthHandlerInstance.AddFriendsNotificationForIncomingRequest();
//                    //    HelperMethodsAuth.AuthHandlerInstance.NotifyAllContactListLoaded();
//                    //    new InsertIntoUserBasicInfoTable(FriendDictionaries.Instance.TEMP_CONTACT_LIST).Start();
//                    //}
//                }
//                HelperMethodsAuth.AuthHandlerInstance.UI_ProcessByteContactList(basicInfos, this.packet_attributes.Success);
//                //else
//                //{
//                //    if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0 && DefaultSettings.FRIEND_LIST_LOADED == false)
//                //    {
//                //        //log.Info("########### Action-23 !SUCCCESS END############");
//                //        HelperMethodsAuth.AuthHandlerInstance.NotifyAllContactListLoaded();
//                //    }
//                //}
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in TYPE_CONTACT_LIST 23==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }

//        }

//        private void ProcessAddFriendUpdate() //someone sent me friend request//327
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    UserBasicInfoDTO userBasicInfo = null;
//                    long userID = _JobjFromResponse[JsonKeys.UserIdentity] != null ? long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString()) : 0;
//                    int FriendshipStatus = _JobjFromResponse[JsonKeys.FriendshipStatus] != null ? Int32.Parse(_JobjFromResponse[JsonKeys.FriendshipStatus].ToString()) : 0;

//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userID, out userBasicInfo);
//                        if (userBasicInfo == null)
//                        {
//                            userBasicInfo = new UserBasicInfoDTO();
//                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(userID, userBasicInfo);
//                        }
//                        HelperMethodsModel.BindAddFriendDetails(_JobjFromResponse, userBasicInfo);
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userBasicInfo.UserIdentity] = userBasicInfo;
//                    }
//                    lock (FriendDictionaries.Instance.UTID_UID_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UTID_UID_DICTIONARY[userBasicInfo.UserTableId] = userBasicInfo.UserIdentity;
//                    }

//                    lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(userBasicInfo.UserTableId);
//                    }
//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userBasicInfo.UserIdentity);
//                    }

//                    List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
//                    userBasicInfoList.Add(userBasicInfo);
//                    new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();

//                    HelperMethodsAuth.AuthHandlerInstance.UI_ProcessAddFriendUpdate(userBasicInfo);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessAddFriendUpdate==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessAcceptFriendUpdate() //someone accepted my request//329
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                    {
//                        UserBasicInfoDTO userBasicInfo = null;

//                        lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                        {
//                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString()), out userBasicInfo);

//                            if (userBasicInfo == null)
//                            {
//                                userBasicInfo = new UserBasicInfoDTO();
//                                FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString()), userBasicInfo);
//                            }
//                        }

//                        if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                        {
//                            userBasicInfo.UserIdentity = long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString());
//                        }
//                        if (_JobjFromResponse[JsonKeys.UserTableId] != null)
//                        {
//                            userBasicInfo.UserTableId = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                        }
//                        if (_JobjFromResponse[JsonKeys.FullName] != null)
//                        {
//                            userBasicInfo.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                        }
//                        if (_JobjFromResponse[JsonKeys.Gender] != null)
//                        {
//                            userBasicInfo.Gender = (string)_JobjFromResponse[JsonKeys.Gender];
//                        }
//                        if (_JobjFromResponse[JsonKeys.FriendshipStatus] != null)
//                        {
//                            userBasicInfo.FriendShipStatus = (int)_JobjFromResponse[JsonKeys.FriendshipStatus];
//                        }
//                        if (_JobjFromResponse[JsonKeys.ProfileImage] != null)
//                        {
//                            userBasicInfo.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
//                        }
//                        if (_JobjFromResponse[JsonKeys.ProfileImageId] != null)
//                        {
//                            userBasicInfo.ProfileImageId = (long)_JobjFromResponse[JsonKeys.ProfileImageId];
//                        }
//                        if (_JobjFromResponse[JsonKeys.NumberOfMutualFriend] != null)
//                        {
//                            userBasicInfo.NumberOfMutualFriends = (int)_JobjFromResponse[JsonKeys.NumberOfMutualFriend];
//                        }
//                        if (_JobjFromResponse[JsonKeys.MatchedBy] != null)
//                        {
//                            userBasicInfo.MatchedBy = (int)_JobjFromResponse[JsonKeys.MatchedBy];
//                        }
//                        if (_JobjFromResponse[JsonKeys.CallAccess] != null)
//                        {
//                            userBasicInfo.CallAccess = (int)_JobjFromResponse[JsonKeys.CallAccess];
//                        }
//                        if (_JobjFromResponse[JsonKeys.ChatAccess] != null)
//                        {
//                            userBasicInfo.ChatAccess = (int)_JobjFromResponse[JsonKeys.ChatAccess];
//                        }
//                        if (_JobjFromResponse[JsonKeys.FeedAccess] != null)
//                        {
//                            userBasicInfo.FeedAccess = (int)_JobjFromResponse[JsonKeys.FeedAccess];
//                        }

//                        userBasicInfo.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
//                        userBasicInfo.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
//                        userBasicInfo.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;

//                        List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
//                        userBasicInfoList.Add(userBasicInfo);
//                        new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();

//                        HelperMethodsAuth.AuthHandlerInstance.UI_ProcessAcceptFriendUpdate(userBasicInfo);

//                        //HelperMethodsAuth.AuthHandlerInstance.AddOrAcceptSingleFriendUI(userBasicInfo);
//                        //HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(userBasicInfo);
//                        //HelperMethodsAuth.AuthHandlerInstance.SetRequestsForFriendViewedandAdded(userBasicInfo);
//                        //HelperMethodsAuth.AuthHandlerInstance.ReloadFriendsMutualFriendsProfile();
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessAcceptFriendUpdate==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }

//        }

//        private void ProcessDeleteFriendUpdate() //328
//        {
//            try
//            {
//                if ((this.action == AppConstants.TYPE_DELETE_FRIEND && server_packet_id != 0) || this.action == AppConstants.TYPE_UPDATE_DELETE_FRIEND)
//                {
//                    if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                    {
//                        long userID = _JobjFromResponse[JsonKeys.UserIdentity] != null ? long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString()) : 0;

//                        UserBasicInfoDTO userBasicInfo = null;
//                        lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                        {
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userID, out userBasicInfo))
//                            {
//                                userBasicInfo.FriendShipStatus = 0;
//                                if (userBasicInfo.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
//                                {
//                                    userBasicInfo.BlockedBy = StatusConstants.BLOCKED_BY_ME;
//                                }
//                                userBasicInfo.ChatAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
//                                userBasicInfo.CallAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
//                                userBasicInfo.FeedAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;

//                                /*Favorite Rank*/
//                                if (userBasicInfo.FavoriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE)
//                                {
//                                    userBasicInfo.FavoriteRank = 0;
//                                    ContactListDAO.UpdateContactRanking(userBasicInfo);
//                                }
//                                userBasicInfo.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;

//                                List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
//                                userList.Add(userBasicInfo);
//                                new InsertIntoUserBasicInfoTable(userList).Start();

//                                HelperMethodsAuth.AuthHandlerInstance.UI_ProcessDeleteFriendUpdate(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.HideFriendProfileTabForNF(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.RemoveSingleFriendFromUIList(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.HideFriendAboutInfos(userBasicInfo);
//                                //HelperMethodsAuth.AuthHandlerInstance.ReloadFriendsMutualFriendsProfile();
//                                //=======
//                                //                            HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(userBasicInfo);
//                                //                            HelperMethodsAuth.AuthHandlerInstance.HideFriendProfileTabForNF(userBasicInfo);
//                                //                            HelperMethodsAuth.AuthHandlerInstance.RemoveSingleFriendFromUIList(userBasicInfo);
//                                //                            HelperMethodsAuth.AuthHandlerInstance.HideFriendAboutInfos(userBasicInfo);
//                                //                            HelperMethodsAuth.AuthHandlerInstance.ReloadFriendsMutualFriendsProfile();
//                                //>>>>>>> .r8960
//                            }
//                        }
//                    }
//                    else
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.ReloadFriendsMutualFriendsProfile();
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessDeleteFriendUpdate==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessContactSearch() //34
//        {
//            try
//            {
//                int searchCount = 0;
//                int matchCount = 0; // Server often sends wrong data
//                int ProfileType = 0;
//                string searchParam = null;
//                List<UserBasicInfoDTO> listuserBasicInfoDTO = null;
//                List<NewsPortalDTO> portalList = null;
//                List<PageInfoDTO> pagesList = null;
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
//                    if (_JobjFromResponse[JsonKeys.SearchedContacList] != null)
//                    {
//                        JArray searchedContactList = (JArray)_JobjFromResponse[JsonKeys.SearchedContacList];
//                        if (_JobjFromResponse[JsonKeys.ProfileType] != null) ProfileType = ((int)_JobjFromResponse[JsonKeys.ProfileType]);
//                        if (ProfileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
//                        {
//                            List<long> portalIds = null;
//                            if (!NewsPortalDictionaries.Instance.NEWSPORTAL_SEARCH_RES_BY_PARAM.ContainsKey(searchParam))
//                            {
//                                portalIds = new List<long>();
//                                NewsPortalDictionaries.Instance.NEWSPORTAL_SEARCH_RES_BY_PARAM[searchParam] = portalIds;
//                            }
//                            else portalIds = NewsPortalDictionaries.Instance.NEWSPORTAL_SEARCH_RES_BY_PARAM[searchParam];
//                            portalList = new List<NewsPortalDTO>();
//                            foreach (JObject singleObj in searchedContactList)
//                            {
//                                NewsPortalDTO dto = new NewsPortalDTO();
//                                if (singleObj[JsonKeys.NewsPortalDTO] != null)
//                                {
//                                    JObject NpDto = (JObject)singleObj[JsonKeys.NewsPortalDTO];
//                                    if (NpDto[JsonKeys.NewsPortalSlogan] != null)
//                                    {
//                                        dto.PortalSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
//                                    }
//                                    if (NpDto[JsonKeys.Subscribe] != null)
//                                    {
//                                        dto.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
//                                    }
//                                    if (NpDto[JsonKeys.NewsPortalCatName] != null)
//                                    {
//                                        dto.PortalCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
//                                    }
//                                    if (NpDto[JsonKeys.NewsPortalCatId] != null)
//                                    {
//                                        dto.PortalCatId = (long)NpDto[JsonKeys.NewsPortalCatId];
//                                    }
//                                    if (NpDto[JsonKeys.SubscriberCount] != null)
//                                    {
//                                        dto.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
//                                    }
//                                    if (NpDto[JsonKeys.FullName] != null)
//                                    {
//                                        dto.PortalName = (string)NpDto[JsonKeys.FullName];
//                                    }
//                                    if (NpDto[JsonKeys.UserTableId] != null)
//                                    {
//                                        dto.PortalId = (long)NpDto[JsonKeys.UserTableId];
//                                    }
//                                    if (NpDto[JsonKeys.UserIdentity] != null)
//                                    {
//                                        dto.UserId = (long)NpDto[JsonKeys.UserIdentity];
//                                    }
//                                    if (NpDto[JsonKeys.ProfileImage] != null)
//                                    {
//                                        dto.ProfileImage = (string)NpDto[JsonKeys.ProfileImage];
//                                    }
//                                }
//                                if (singleObj[JsonKeys.UserTableId] != null)
//                                {
//                                    dto.PortalId = (long)singleObj[JsonKeys.UserTableId];
//                                }
//                                if (singleObj[JsonKeys.UserIdentity] != null)
//                                {
//                                    dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                                }
//                                if (singleObj[JsonKeys.FullName] != null)
//                                {
//                                    dto.PortalName = (string)singleObj[JsonKeys.FullName];
//                                }
//                                if (singleObj[JsonKeys.ProfileImage] != null)
//                                {
//                                    dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                                }
//                                if (!portalIds.Contains(dto.PortalId)) portalIds.Add(dto.PortalId);
//                                ///
//                                NewsPortalDictionaries.Instance.UTID_NEWSPORTALINFO_DICTIONARY[dto.PortalId] = dto;
//                                portalList.Add(dto);
//                            }
//                        }
//                        else if (ProfileType == SettingsConstants.PROFILE_TYPE_PAGES)
//                        {
//                            List<long> pageIds = null;
//                            if (!NewsPortalDictionaries.Instance.PAGES_SEARCH_RES_BY_PARAM.ContainsKey(searchParam))
//                            {
//                                pageIds = new List<long>();
//                                NewsPortalDictionaries.Instance.PAGES_SEARCH_RES_BY_PARAM[searchParam] = pageIds;
//                            }
//                            else pageIds = NewsPortalDictionaries.Instance.PAGES_SEARCH_RES_BY_PARAM[searchParam];
//                            pagesList = new List<PageInfoDTO>();
//                            foreach (JObject singleObj in searchedContactList)
//                            {
//                                PageInfoDTO dto = new PageInfoDTO();
//                                if (singleObj[JsonKeys.NewsPortalDTO] != null)
//                                {
//                                    JObject NpDto = (JObject)singleObj[JsonKeys.NewsPortalDTO];
//                                    if (NpDto[JsonKeys.NewsPortalSlogan] != null)
//                                    {
//                                        dto.PageSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
//                                    }
//                                    if (NpDto[JsonKeys.Subscribe] != null)
//                                    {
//                                        dto.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
//                                    }
//                                    if (NpDto[JsonKeys.NewsPortalCatName] != null)
//                                    {
//                                        dto.PageCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
//                                    }
//                                    if (NpDto[JsonKeys.NewsPortalCatId] != null)
//                                    {
//                                        dto.PageCatId = (long)NpDto[JsonKeys.NewsPortalCatId];
//                                    }
//                                    if (NpDto[JsonKeys.SubscriberCount] != null)
//                                    {
//                                        dto.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
//                                    }
//                                    if (NpDto[JsonKeys.FullName] != null)
//                                    {
//                                        dto.PageName = (string)NpDto[JsonKeys.FullName];
//                                    }
//                                    if (NpDto[JsonKeys.UserTableId] != null)
//                                    {
//                                        dto.PageId = (long)NpDto[JsonKeys.UserTableId];
//                                    }
//                                    if (NpDto[JsonKeys.UserIdentity] != null)
//                                    {
//                                        dto.UserId = (long)NpDto[JsonKeys.UserIdentity];
//                                    }
//                                    if (NpDto[JsonKeys.ProfileImage] != null)
//                                    {
//                                        dto.ProfileImage = (string)NpDto[JsonKeys.ProfileImage];
//                                    }
//                                }
//                                if (singleObj[JsonKeys.UserTableId] != null)
//                                {
//                                    dto.PageId = (long)singleObj[JsonKeys.UserTableId];
//                                }
//                                if (singleObj[JsonKeys.UserIdentity] != null)
//                                {
//                                    dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                                }
//                                if (singleObj[JsonKeys.FullName] != null)
//                                {
//                                    dto.PageName = (string)singleObj[JsonKeys.FullName];
//                                }
//                                if (singleObj[JsonKeys.ProfileImage] != null)
//                                {
//                                    dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                                }
//                                if (!pageIds.Contains(dto.PageId)) pageIds.Add(dto.PageId);
//                                ///
//                                NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY[dto.PageId] = dto;
//                                pagesList.Add(dto);
//                            }
//                        }
//                        else
//                        {
//                            listuserBasicInfoDTO = new List<UserBasicInfoDTO>();
//                            foreach (JObject singleObj in searchedContactList)
//                            {
//                                bool isMatch = false;
//                                searchCount++;
//                                UserBasicInfoDTO user = new UserBasicInfoDTO();
//                                if (singleObj[JsonKeys.FullName] != null)
//                                {
//                                    user.FullName = (string)singleObj[JsonKeys.FullName];
//                                    if (!string.IsNullOrWhiteSpace(user.FullName) && !string.IsNullOrWhiteSpace(searchParam) && user.FullName.ToLower().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.UserIdentity] != null)
//                                {
//                                    long uId = 0;
//                                    try
//                                    {
//                                        uId = long.Parse(singleObj[JsonKeys.UserIdentity].ToString());
//                                    }
//                                    catch (Exception e)
//                                    {
//                                        System.Diagnostics.Debug.WriteLine(e.Message + "\n" + e.StackTrace);
//                                    }
//                                    user.UserIdentity = uId;
//                                    if (user.UserIdentity.ToString().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.UserTableId] != null)
//                                {
//                                    user.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                                }
//                                if (singleObj[JsonKeys.ProfileImage] != null)
//                                {
//                                    user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                                }
//                                if (singleObj[JsonKeys.NumberOfMutualFriend] != null)
//                                {
//                                    user.NumberOfMutualFriends = (long)singleObj[JsonKeys.NumberOfMutualFriend];
//                                }
//                                if (singleObj[JsonKeys.MobilePhone] != null)
//                                {
//                                    user.MobileNumber = (string)singleObj[JsonKeys.MobilePhone];
//                                    if (!string.IsNullOrWhiteSpace(user.MobileNumber) && !string.IsNullOrWhiteSpace(searchParam) && user.MobileNumber.Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.DialingCode] != null)
//                                {
//                                    user.MobileDialingCode = (string)singleObj[JsonKeys.DialingCode];
//                                    if (!string.IsNullOrWhiteSpace(user.MobileDialingCode) && !string.IsNullOrWhiteSpace(searchParam) && user.MobileDialingCode.Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.Email] != null)
//                                {
//                                    user.Email = (string)singleObj[JsonKeys.Email];
//                                    if (!string.IsNullOrWhiteSpace(user.Email) && !string.IsNullOrWhiteSpace(searchParam) && user.Email.ToLower().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.HomeCity] != null)
//                                {
//                                    user.HomeCity = (string)singleObj[JsonKeys.HomeCity];
//                                    if (!string.IsNullOrWhiteSpace(user.HomeCity) && !string.IsNullOrWhiteSpace(searchParam) && user.HomeCity.ToLower().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (singleObj[JsonKeys.CurrentCity] != null)
//                                {
//                                    user.CurrentCity = (string)singleObj[JsonKeys.CurrentCity];
//                                    if (!string.IsNullOrWhiteSpace(user.CurrentCity) && !string.IsNullOrWhiteSpace(searchParam) && user.CurrentCity.ToLower().Contains(searchParam))
//                                    {
//                                        isMatch = true;
//                                    }
//                                }
//                                if (_JobjFromResponse[JsonKeys.SearchCategory] != null && isMatch)
//                                {
//                                    matchCount++;
//                                    listuserBasicInfoDTO.Add(user);
//                                    //lock (RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY)
//                                    //{
//                                    //    Dictionary<long, UserBasicInfoDTO> userMap = null;
//                                    //    RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY.TryGetValue((int)_JobjFromResponse[JsonKeys.SearchCategory], out userMap);

//                                    //    if (userMap != null)
//                                    //    {
//                                    //        if (userMap.ContainsKey(user.UserIdentity))
//                                    //        {
//                                    //            userMap[user.UserIdentity] = user;
//                                    //        }
//                                    //        else
//                                    //        {
//                                    //            userMap.Add(user.UserIdentity, user);
//                                    //        }
//                                    //    }
//                                    //    else
//                                    //    {
//                                    //        userMap = new Dictionary<long, UserBasicInfoDTO>();
//                                    //        userMap.Add(user.UserIdentity, user);
//                                    //        RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY.Add((int)_JobjFromResponse[JsonKeys.SearchCategory], userMap);
//                                    //    }
//                                    //}
//                                }
//                            }
//                        }
//                    }
//                }
//                if (ProfileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
//                    HelperMethodsAuth.NewsFeedHandlerInstance.SearchNewsPortals(searchParam, portalList);
//                else if (ProfileType == SettingsConstants.PROFILE_TYPE_PAGES)
//                    HelperMethodsAuth.NewsFeedHandlerInstance.SearchNewsPortals(searchParam, pagesList);
//                else
//                    HelperMethodsAuth.AuthHandlerInstance.UI_ProcessContactSearch(searchCount, matchCount, client_packet_id, listuserBasicInfoDTO);
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessContactSearch ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessMissCallList()//224
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {

//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessGroupList ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessStoreContactList()
//        {
//            try
//            {
//                new ContactUtIdsRequest();
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessStoreContact ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessSuggestionUsersDetails()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    JArray ContactList = (JArray)_JobjFromResponse[JsonKeys.ContactList];
//                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();

//                    for (int j = 0; j < ContactList.Count; j++)
//                    {
//                        JObject SingleContact = (JObject)ContactList.ElementAt(j);

//                        if (SingleContact != null)
//                        {
//                            UserBasicInfoDTO suggestionDTO = new UserBasicInfoDTO();


//                            if (SingleContact[JsonKeys.UserIdentity] != null)
//                            {
//                                suggestionDTO.UserIdentity = long.Parse(SingleContact[JsonKeys.UserIdentity].ToString());
//                            }

//                            if (SingleContact[JsonKeys.UserTableId] != null)
//                            {
//                                suggestionDTO.UserTableId = (long)SingleContact[JsonKeys.UserTableId];
//                            }
//                            if (SingleContact[JsonKeys.FullName] != null)
//                            {
//                                suggestionDTO.FullName = (string)SingleContact[JsonKeys.FullName];
//                            }

//                            if (SingleContact[JsonKeys.Gender] != null)
//                            {
//                                suggestionDTO.Gender = (string)SingleContact[JsonKeys.Gender];
//                            }

//                            if (SingleContact[JsonKeys.ProfileImage] != null)
//                            {
//                                suggestionDTO.ProfileImage = (string)SingleContact[JsonKeys.ProfileImage];
//                            }

//                            if (SingleContact[JsonKeys.ProfileImageId] != null)
//                            {
//                                suggestionDTO.ProfileImageId = (long)SingleContact[JsonKeys.ProfileImageId];
//                            }

//                            if (SingleContact[JsonKeys.UpdateTime] != null)
//                            {
//                                suggestionDTO.UpdateTime = (long)SingleContact[JsonKeys.UpdateTime];
//                            }
//                            if (SingleContact[JsonKeys.NumberOfMutualFriend] != null)
//                            {
//                                suggestionDTO.NumberOfMutualFriends = (int)SingleContact[JsonKeys.NumberOfMutualFriend];
//                            }

//                            lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY)
//                            {
//                                if (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.ContainsKey(suggestionDTO.UserIdentity))
//                                {
//                                    FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY[suggestionDTO.UserIdentity] = suggestionDTO;
//                                }
//                                else
//                                {
//                                    FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Add(suggestionDTO.UserIdentity, suggestionDTO);
//                                }
//                            }

//                            lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                            {
//                                if (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(suggestionDTO.UserTableId))
//                                {
//                                    FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY[suggestionDTO.UserTableId] = true;
//                                }
//                                else
//                                {
//                                    FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Add(suggestionDTO.UserTableId, true);
//                                }
//                            }

//                            list.Add(suggestionDTO);
//                        }

//                        HelperMethodsAuth.AuthHandlerInstance.AddSuggestions(list);
//                    }

//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessSuggestionUsersDetails ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessSuggestionUtids()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {

//                    JArray ContactUtids = (JArray)_JobjFromResponse[JsonKeys.ContactIds];
//                    JArray utIds = new JArray();
//                    foreach (var item in ContactUtids)
//                    {
//                        long utId = (long)item;
//                        if (DefaultSettings.INITIAL_SUGSTNS_COUNT < 15)
//                        {
//                            utIds.Add(utId);
//                            DefaultSettings.INITIAL_SUGSTNS_COUNT++;
//                        }
//                        lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                        {
//                            if (!FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(utId))
//                            {
//                                FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Add(utId, false);
//                            }
//                        }
//                    }
//                    if (utIds.Count > 0)
//                        SendToServer.SuggestionUsersDetailsRequest(utIds);
//                    /*for (int j = 0; j < ContactUtids.Count; j++)
//                    {
//                        long utid = (long)ContactUtids.ElementAt(j);

//                        if (!FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(utid))
//                        {
//                            lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                            {
//                                if (!FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(utid))
//                                {
//                                    FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Add(utid, false);
//                                }
//                            }
//                        }
//                    }

//                    JArray userIds = new JArray();
//                    lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                    {
//                        int count = 0;
//                        foreach (long id in FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Keys)
//                        {
//                            userIds.Add(id);
//                            count++;
//                            if (count == 15)
//                            {
//                                break;
//                            }
//                        }
//                    }
//                    SendToServer.SuggestionUsersDetailsRequest(userIds);*/

//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessSuggestionUtids ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessTypeMutualFriends()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.TotalRecords] != null)
//                {
//                    MutualFriendsSeqCount++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);
//                    int tf = (int)_JobjFromResponse[JsonKeys.TotalRecords];
//                    long uid = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    if (_JobjFromResponse[JsonKeys.MutualFriendIDs] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MutualFriendIDs];
//                        for (int i = 0; i < jarray.Count; i++)
//                        {
//                            //long i1 = (long)JsonConvert.DeserializeObject(jarray[i].ToString(), typeof(long));
//                            long i1 = (long)jarray.ElementAt(i);
//                            /*if (i < DefaultSettings.CONTENT_SHOW_LIMIT)
//                            {
//                                FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Add(i1);
//                            }*/

//                            lock (FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY)
//                            {
//                                if (FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.ContainsKey(uid))
//                                {
//                                    if (!FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[uid].FriendList.Any(f => f.Equals(i1)))
//                                    {
//                                        FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[uid].FriendList.Add(i1);
//                                    }
//                                }
//                                else
//                                {
//                                    FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.Add(uid, new FriendInfoDTO());
//                                    FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[uid].FriendList.Add(i1);
//                                }
//                                FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[uid].TotalFriend = tf;
//                            }
//                        }
//                    }
//                    //System.Diagnostics.Debug.WriteLine("MUT CONTACT LIST.." + MutualFriendsSeqCount + "SEQTOTAL.." + seqTotal);
//                    if (MutualFriendsSeqCount == seqTotal)
//                    {
//                        MutualFriendsSeqCount = 0;
//                        FriendInfoDTO fiDTO;
//                        FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.TryGetValue(uid, out fiDTO);
//                        int i = 0;
//                        while (i < fiDTO.FriendList.Count && i < DefaultSettings.CONTENT_SHOW_LIMIT)
//                        {
//                            FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Add(fiDTO.FriendList[i]);
//                            i++;
//                        }
//                        HelperMethodsAuth.AuthHandlerInstance.ShowFriendMutualContactList(uid, FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY);
//                    }
//                }
//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    long uid = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    HelperMethodsAuth.AuthHandlerInstance.NoMutualFriendFoundInFriendsContactList(uid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessTypeMutualFriends ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessFriendContactList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.TotalFriend] != null)
//                {
//                    //FriendContactListSequenceCount++;
//                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                    /*string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);*/
//                    int tf = (int)_JobjFromResponse[JsonKeys.TotalFriend];
//                    long uid;
//                    FriendDictionaries.Instance.UTID_UID_DICTIONARY.TryGetValue(utid, out uid);
//                    if (_JobjFromResponse[JsonKeys.ContactList] != null)
//                    {
//                        JArray contactList = (JArray)_JobjFromResponse[JsonKeys.ContactList];
//                        lock (FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY)
//                        {
//                            foreach (JObject singleObj in contactList)
//                            {
//                                UserBasicInfoDTO userBasicInfo;
//                                lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                                {
//                                    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(long.Parse(singleObj[JsonKeys.UserIdentity].ToString()), out userBasicInfo);

//                                    if (userBasicInfo == null)
//                                    {
//                                        userBasicInfo = new UserBasicInfoDTO();
//                                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(long.Parse(singleObj[JsonKeys.UserIdentity].ToString()), userBasicInfo);
//                                    }
//                                }
//                                //UserBasicInfoDTO userBasicInfo = new UserBasicInfoDTO();
//                                if (singleObj[JsonKeys.UserIdentity] != null)
//                                {
//                                    userBasicInfo.UserIdentity = long.Parse(singleObj[JsonKeys.UserIdentity].ToString());
//                                }
//                                if (singleObj[JsonKeys.UserTableId] != null)
//                                {
//                                    userBasicInfo.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                                }
//                                if (singleObj[JsonKeys.FullName] != null)
//                                {
//                                    userBasicInfo.FullName = (string)singleObj[JsonKeys.FullName];
//                                }
//                                if (singleObj[JsonKeys.Gender] != null)
//                                {
//                                    userBasicInfo.Gender = (string)singleObj[JsonKeys.Gender];
//                                }
//                                if (singleObj[JsonKeys.FriendshipStatus] != null)
//                                {
//                                    userBasicInfo.FriendShipStatus = (int)singleObj[JsonKeys.FriendshipStatus];
//                                }
//                                if (singleObj[JsonKeys.ProfileImage] != null)
//                                {
//                                    userBasicInfo.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                                }
//                                if (singleObj[JsonKeys.ProfileImageId] != null)
//                                {
//                                    userBasicInfo.ProfileImageId = (long)singleObj[JsonKeys.ProfileImageId];
//                                }
//                                if (singleObj[JsonKeys.NumberOfMutualFriend] != null)
//                                {
//                                    userBasicInfo.NumberOfMutualFriends = (int)singleObj[JsonKeys.NumberOfMutualFriend];
//                                }
//                                if (singleObj[JsonKeys.ContactUpdate] != null)
//                                {
//                                    userBasicInfo.ContactUpdateTime = (long)singleObj[JsonKeys.ContactUpdate];
//                                }
//                                if (singleObj[JsonKeys.UpdateTime] != null)
//                                {
//                                    userBasicInfo.UpdateTime = (long)singleObj[JsonKeys.UpdateTime];
//                                }
//                                FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY[userBasicInfo.UserIdentity] = userBasicInfo;
//                                lock (FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY)
//                                {
//                                    if (FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY.ContainsKey(uid))
//                                    {
//                                        if (!FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[uid].FriendList.Any(f => f.Equals(userBasicInfo.UserIdentity)))
//                                        {
//                                            FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[uid].FriendList.Add(userBasicInfo.UserIdentity);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY.Add(uid, new FriendInfoDTO());
//                                        FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[uid].FriendList.Add(userBasicInfo.UserIdentity);
//                                    }
//                                    FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[uid].TotalFriend = tf;
//                                }
//                            }
//                        }
//                    }
//                    //log.Info("FR CONTACT LIST.." + FriendContactListSequenceCount + "SEQTOTAL.." + seqTotal);
//                    //if (FriendContactListSequenceCount == seqTotal)
//                    {
//                        //FriendContactListSequenceCount = 0;                        
//                        HelperMethodsAuth.AuthHandlerInstance.ShowFriendTotalContactList(uid, FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY.Values.ToList());
//                        FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY.Clear();
//                    }
//                }
//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                    HelperMethodsAuth.AuthHandlerInstance.NoContactFoundInFriendsContactList(utid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessFriendContactList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessPushAddFriend()//127
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && server_packet_id != 0)
//                {
//                    UserBasicInfoDTO userBasicInfo = null;
//                    long userID = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    int FriendshipStatus = _JobjFromResponse[JsonKeys.FriendshipStatus] != null ? Int32.Parse(_JobjFromResponse[JsonKeys.FriendshipStatus].ToString()) : 0;

//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userID, out userBasicInfo);
//                        if (userBasicInfo == null)
//                        {
//                            userBasicInfo = new UserBasicInfoDTO();
//                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(userID, userBasicInfo);
//                        }
//                        HelperMethodsModel.BindAddFriendDetails(_JobjFromResponse, userBasicInfo);
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userBasicInfo.UserIdentity] = userBasicInfo;
//                    }
//                    lock (FriendDictionaries.Instance.UTID_UID_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UTID_UID_DICTIONARY[userBasicInfo.UserTableId] = userBasicInfo.UserIdentity;
//                    }

//                    lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(userBasicInfo.UserTableId);
//                    }
//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userBasicInfo.UserIdentity);
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.UI_ProcessPushAddFriend(userBasicInfo);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessPushAddFriend ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessPushAcceptFriend()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && server_packet_id != 0)
//                {
//                    UserBasicInfoDTO userBasicInfo = null;
//                    long userID = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
//                    {
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userID, out userBasicInfo);
//                        if (userBasicInfo == null)
//                        {
//                            userBasicInfo = new UserBasicInfoDTO();
//                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(userID, userBasicInfo);
//                        }
//                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userBasicInfo.UserIdentity] = userBasicInfo;
//                    }

//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                    {
//                        userBasicInfo.UserIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    }
//                    if (_JobjFromResponse[JsonKeys.FullName] != null)
//                    {
//                        userBasicInfo.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                    }
//                    if (_JobjFromResponse[JsonKeys.ProfileImage] != null)
//                    {
//                        userBasicInfo.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
//                    }
//                    if (_JobjFromResponse[JsonKeys.ProfileImageId] != null)
//                    {
//                        userBasicInfo.ProfileImageId = (long)_JobjFromResponse[JsonKeys.ProfileImageId];
//                    }
//                    if (_JobjFromResponse[JsonKeys.FriendshipStatus] != null)
//                    {
//                        userBasicInfo.FriendShipStatus = (int)_JobjFromResponse[JsonKeys.FriendshipStatus];
//                    }
//                    if (_JobjFromResponse[JsonKeys.UpdateTime] != null)
//                    {
//                        userBasicInfo.UpdateTime = (long)_JobjFromResponse[JsonKeys.UpdateTime];
//                    }
//                    if (userBasicInfo.BlockedBy != 1)
//                    {
//                        if (_JobjFromResponse[JsonKeys.CallAccess] != null)
//                        {
//                            userBasicInfo.CallAccess = (int)_JobjFromResponse[JsonKeys.CallAccess];
//                        }
//                        if (_JobjFromResponse[JsonKeys.ChatAccess] != null)
//                        {
//                            userBasicInfo.ChatAccess = (int)_JobjFromResponse[JsonKeys.ChatAccess];
//                        }
//                        if (_JobjFromResponse[JsonKeys.FeedAccess] != null)
//                        {
//                            userBasicInfo.FeedAccess = (int)_JobjFromResponse[JsonKeys.FeedAccess];
//                        }
//                    }
//                    else
//                    {
//                        userBasicInfo.CallAccess = 0;
//                        userBasicInfo.ChatAccess = 0;
//                        userBasicInfo.FeedAccess = 0;
//                    }

//                    List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
//                    userBasicInfoList.Add(userBasicInfo);
//                    new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();

//                    HelperMethodsAuth.AuthHandlerInstance.UI_AcceptFriendRequest(userBasicInfo);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessPushAddFriend ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        #endregion"Contact List"

//        #region Image Related

//        private void ProcessUpdateEditImageComment()
//        {//{"sucs":true,"cmnId":655,"uId":"2110010304","cmn":"2s","fn":"Sirat Test2","imgId":1440,"sc":false}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    string cmnt = (string)_JobjFromResponse[JsonKeys.Comment];
//                    CommentDTO comment = null;
//                    lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                    {
//                        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId) && NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(commentId, out comment))
//                        {
//                            comment.Comment = (string)_JobjFromResponse[JsonKeys.Comment];
//                            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentId] = comment;
//                        }
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.EditImageComment(imageId, commentId, cmnt, nfid, (_JobjFromResponse[JsonKeys.ShowContinue] != null && (bool)_JobjFromResponse[JsonKeys.ShowContinue]), true);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateEditImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateDeleteImageComment()
//        {// {"sucs":true,"cmnId":686,"uId":"2110010304","fn":"Sirat Test2","imgId":1418,"sc":false,"loc":8}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                    {
//                        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imgId))
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imgId].ContainsKey(commentId))
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imgId].Remove(commentId);
//                            }
//                        }
//                    }
//                    //  List<CommentDTO> list = new List<CommentDTO>();
//                    // if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imgId)){
//                    // list.AddRange(NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imgId].Values.ToList());
//                    //HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imgId, list);
//                    HelperMethodsAuth.AuthHandlerInstance.DeleteImageComment(imgId, commentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateDeleteImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateAddImageComment()
//        {
//            //{"sucs":true,"cmnId":685,"tm":1442831123023,"uId":"2110011654","cmn":"12","fn":"sam6211","imgId":1440,"sc":false,"loc":5}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    long loc = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
//                    CommentDTO comment = HelperMethodsModel.BindCommentDetails(_JobjFromResponse);
//                    UserBasicInfoDTO user = null;
//                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(comment.UserIdentity, out user))
//                    {
//                        comment.ProfileImage = user.ProfileImage;
//                    }
//                    CommentDTO comntDto = null;
//                    lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                    {
//                        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId))
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(comment.CommentId, out comntDto))
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][comntDto.CommentId] = comment;
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);
//                            }
//                        }
//                        else
//                        {
//                            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.Add(imageId, new Dictionary<long, CommentDTO>());
//                            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);

//                        }
//                    }
//                    //  List<CommentDTO> list = new List<CommentDTO>();
//                    // list.AddRange(NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Values.ToList());
//                    //List<CommentDTO> tmp = list.OrderByDescending(p => p.Time).ToList();
//                    //HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId,tmp);
//                    HelperMethodsAuth.AuthHandlerInstance.AddImageComment(imageId, comment, nfid, true);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateAddImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeUnlikeImageComment()
//        {
//            //{"uId":"2110010304","sucs":true,"fn":"Sirat Test2","id":109,"cmnId":635,"tm":1442815321235,"imgId":1413,"lkd":1,"loc":1}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ImageId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.Liked] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    int liked = (int)_JobjFromResponse[JsonKeys.Liked];
//                    UserBasicInfoDTO user = new UserBasicInfoDTO();
//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                        user.UserIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    if (_JobjFromResponse[JsonKeys.FullName] != null)
//                        user.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                    UserBasicInfoDTO tmp = null;
//                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                    {
//                        user.FriendShipStatus = tmp.FriendShipStatus;
//                    }
//                    else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                    {
//                        user.FriendShipStatus = -1; //for myself
//                    }
//                    if (cmntId > 0)
//                    {
//                        lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                        {
//                            Dictionary<long, CommentDTO> commentsOfthisFeed = null;
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.TryGetValue(imgId, out commentsOfthisFeed))
//                            {
//                                CommentDTO dto = null;
//                                if (commentsOfthisFeed.TryGetValue(cmntId, out dto))
//                                {
//                                    if (liked == 1) { dto.TotalLikeComment++; dto.ILikeComment = 1; }
//                                    else if (dto.TotalLikeComment > 0) { dto.TotalLikeComment--; dto.ILikeComment = 0; }
//                                }
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateImageCommentLikeUnlike(imgId, cmntId, liked, user, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeUnlikeImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeUnlikeImage()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ImageId] != null && _JobjFromResponse[JsonKeys.Liked] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    int liked = (int)_JobjFromResponse[JsonKeys.Liked];
//                    UserBasicInfoDTO user = new UserBasicInfoDTO();
//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                        user.UserIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    if (_JobjFromResponse[JsonKeys.FullName] != null)
//                        user.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                    UserBasicInfoDTO tmp = null;
//                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                    {
//                        user.FriendShipStatus = tmp.FriendShipStatus;
//                    }
//                    else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                    {
//                        user.FriendShipStatus = -1; //for myself
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateImageLikeUnlike(imgId, liked, user, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeUnlikeImage ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessEditImageComment() //194
//        {
//            //{"sucs":true,"cmnId":547,"cmn":"chup","fn":"Smite.Bakasura.Desk","imgId":67,"sc":false}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null)
//                {
//                    if ((bool)_JobjFromResponse[JsonKeys.Success])
//                    {
//                        long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                        long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                        string cmnt = (string)_JobjFromResponse[JsonKeys.Comment];
//                        CommentDTO comment = null;
//                        lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(commentId, out comment))
//                            {
//                                comment.Comment = cmnt;
//                            }
//                        }
//                        HelperMethodsAuth.AuthHandlerInstance.EditImageComment(imageId, commentId, cmnt, nfid, (_JobjFromResponse[JsonKeys.ShowContinue] != null && (bool)_JobjFromResponse[JsonKeys.ShowContinue]));
//                    }
//                    else
//                    {
//                        string msg = "Failed to Comment!";
//                        if (_JobjFromResponse[JsonKeys.Message] != null)
//                            msg = (string)_JobjFromResponse[JsonKeys.Message];
//                        HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(msg, "Failed!");
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessEditImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessAddImageComment() //180
//        {
//            // {"sucs":true,"cmnId":547,"tm":1442491058781,"cmn":"asddsa","fn":"Smite.Bakasura.Desk","imgId":67,"sc":false,"loc":4}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null)
//                {
//                    if ((bool)_JobjFromResponse[JsonKeys.Success])
//                    {
//                        long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                        long loc = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
//                        CommentDTO comment = HelperMethodsModel.BindCommentDetails(_JobjFromResponse);
//                        comment.UserIdentity = DefaultSettings.LOGIN_USER_ID;
//                        comment.ProfileImage = DefaultSettings.userProfile.ProfileImage;
//                        CommentDTO comntDto = null;
//                        lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId))
//                            {
//                                if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(comment.CommentId, out comntDto))
//                                {
//                                    NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][comntDto.CommentId] = comment;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);
//                                }
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.Add(imageId, new Dictionary<long, CommentDTO>());
//                                NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);

//                            }
//                        }
//                        HelperMethodsAuth.AuthHandlerInstance.AddImageComment(imageId, comment, nfid);
//                    }
//                    else
//                    {
//                        string msg = "Failed to Comment!";
//                        if (_JobjFromResponse[JsonKeys.Message] != null)
//                            msg = (string)_JobjFromResponse[JsonKeys.Message];
//                        HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(msg, "Failed!");
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessAddImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessLikeUnlikeImageComment() //197
//        {
//            //{"sucs":true,"id":84,"cmnId":546,"tm":1442491530675,"imgId":67,"lkd":1,"loc":1}
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null)
//                {
//                    if ((bool)_JobjFromResponse[JsonKeys.Success])
//                    {
//                        long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                        long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                        lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId))
//                            {
//                                if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].ContainsKey(commentId))
//                                {
//                                    CommentDTO comment = NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentId];
//                                    comment.ILikeComment = (short)_JobjFromResponse[JsonKeys.Liked];
//                                    comment.TotalLikeComment = (int)_JobjFromResponse[JsonKeys.LikeOrComment];
//                                    NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentId] = comment;
//                                }
//                            }
//                        }
//                        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId)
//                            && NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].ContainsKey(commentId))
//                            HelperMethodsAuth.AuthHandlerInstance.LikeImageComment(nfid, imageId, NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentId]);
//                        //List<CommentDTO> list = NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Values.ToList();
//                        //HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId, list);
//                    }
//                    else
//                    {
//                        string msg = "Failed to Like/Unlike!";
//                        if (_JobjFromResponse[JsonKeys.Message] != null)
//                            msg = (string)_JobjFromResponse[JsonKeys.Message];
//                        HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(msg, "Failed!");
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessLikeUnlikeImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessLikersImage() // 93
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ImageId] != null)
//                {
//                    //ImageLikesSequenceCount++;
//                    string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                    int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        List<UserBasicInfoDTO> users = new List<UserBasicInfoDTO>();
//                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        foreach (JObject userObj in jArray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (userObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)userObj[JsonKeys.UserIdentity];
//                            if (userObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)userObj[JsonKeys.FullName];
//                            if (userObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)userObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            //user.FriendShipStatus = (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp)) ? tmp.FriendShipStatus : -1; //-1 for self
//                            //lock (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP)
//                            //{
//                            //    if (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.ContainsKey(user.UserIdentity))
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP[user.UserIdentity] = user;
//                            //    else
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.Add(user.UserIdentity, user);
//                            //}
//                            users.Add(user);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadImageLikes(imgId, users);
//                    }
//                    //  if (ImageLikesSequenceCount == seqTotal)
//                    // {
//                    //     ImageLikesSequenceCount = 0;
//                    //HelperMethodsAuth.NewsFeedHandlerInstance.LoadImageLikes(imgId, users);
//                    //}//
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessLikersImage ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessLikersImageComment() //196
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    //LikesSequnceCount++;
//                    ImageCommentLikesSequeceCount++;
//                    string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        List<UserBasicInfoDTO> users = new List<UserBasicInfoDTO>();
//                        foreach (JObject userObj in jArray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (userObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)userObj[JsonKeys.UserIdentity];
//                            if (userObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)userObj[JsonKeys.FullName];
//                            if (userObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)userObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            //user.FriendShipStatus = (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp)) ? tmp.FriendShipStatus : -1; //-1 for self
//                            //lock (NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST)
//                            //{
//                            //    if (NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST.ContainsKey(user.UserIdentity))
//                            //        NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST[user.UserIdentity] = user;
//                            //    else
//                            //        NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST.Add(user.UserIdentity, user);
//                            //}
//                            users.Add(user);
//                        }
//                        HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentLikersFromServer(users, (long)_JobjFromResponse[JsonKeys.ImageId], (long)_JobjFromResponse[JsonKeys.CommentId]);
//                    }
//                    //  if (ImageCommentLikesSequeceCount == seqTotal)
//                    // {
//                    //      ImageCommentLikesSequeceCount = 0;
//                    //HandlerMehod
//                    //      List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();

//                    //      list.AddRange(NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST.Values.ToList());

//                    //      HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentLikersFromServer(list, (long)_JobjFromResponse[JsonKeys.ImageId], (long)_JobjFromResponse[JsonKeys.CommentId]);
//                    //    NewsFeedDictionaries.Instance.IMAGE_COMMENT_LIKE_LIST.Clear();
//                    //}
//                }
//                else
//                {
//                    HelperMethodsAuth.AuthHandlerInstance.OnLikerListLoadFailed();
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessLikersImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessImageComments()
//        {
//            //throw new NotImplementedException();
//            //System.Diagnostics.Debug.WriteLine(_JobjFromResponse);
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null)
//                {
//                    if ((bool)_JobjFromResponse[JsonKeys.Success]) // found
//                    {
//                        long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
//                        int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
//                        long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
//                        //Dictionary<string, Dictionary<long, CommentDTO>> TEMP_DICTIONARY = new Dictionary<string, Dictionary<long, CommentDTO>>(); //<sequence , <commentId, CommentDTO>>

//                        if (_JobjFromResponse[JsonKeys.Comments] != null)
//                        {
//                            List<CommentDTO> list = new List<CommentDTO>();
//                            JArray commentList = (JArray)_JobjFromResponse[JsonKeys.Comments];
//                            foreach (JObject comment in commentList)
//                            {
//                                CommentDTO commentDTO = HelperMethodsModel.BindCommentDetails(comment);
//                                list.Add(commentDTO);
//                                //if (TEMP_DICTIONARY.ContainsKey(sequence))
//                                //{
//                                //    if (TEMP_DICTIONARY[sequence].ContainsKey(commentDTO.CommentId)) // overwrite previous dto
//                                //    {
//                                //        TEMP_DICTIONARY[sequence][commentDTO.CommentId] = commentDTO;
//                                //    }
//                                //    else // add new dto
//                                //    {
//                                //        TEMP_DICTIONARY[sequence].Add(commentDTO.CommentId, commentDTO);
//                                //    }
//                                //}
//                                //else//init dictionary first time
//                                //{
//                                //    TEMP_DICTIONARY.Add(sequence, new Dictionary<long, CommentDTO>());
//                                //    TEMP_DICTIONARY[sequence].Add(commentDTO.CommentId, commentDTO);
//                                //}

//                                ////if (ImageCommentsSequenceCount == seqTotal)
//                                ////{
//                                ////    List<CommentDTO> list = new List<CommentDTO>();
//                                ////    list.AddRange(NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Values.ToList());
//                                ////    HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId, list);
//                                ////}
//                                //lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
//                                //{
//                                //    if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId))
//                                //    {
//                                //        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].ContainsKey(commentDTO.CommentId))
//                                //            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][commentDTO.CommentId] = commentDTO;
//                                //        else
//                                //            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(commentDTO.CommentId, commentDTO);
//                                //    }
//                                //    else
//                                //    {
//                                //        NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.Add(imageId, new Dictionary<long, CommentDTO>());
//                                //        NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(commentDTO.CommentId, commentDTO);
//                                //    }
//                                //}
//                            }
//                            HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId, list, nfid);
//                        }
//                        // ImageCommentsSequenceCount++;

//                        //if (ImageCommentsSequenceCount == seqTotal)
//                        //{
//                        //ImageCommentsSequenceCount = 0;

//                        //list.AddRange(NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Values.ToList());
//                        //HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imageId, list, nfid);
//                        //TEMP_DICTIONARY.Clear();
//                        //  }
//                    }
//                    /*else
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.DisableLoaderAnimation();
//                        string msg = "Unable to Load Comments!";
//                        if (_JobjFromResponse[JsonKeys.Message] != null)
//                        {
//                            msg = (string)_JobjFromResponse[JsonKeys.Message];
//                        }
//                        long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                        long imgId = (_JobjFromResponse[JsonKeys.ImageId] != null) ? (long)_JobjFromResponse[JsonKeys.ImageId] : 0;
//                        HelperMethodsAuth.AuthHandlerInstance.FetchImageCommentsFromServer(imgId, null, nfId);
//                    }*/
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessImageComments ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        //private void ProcessSingleImageDetail()
//        //{
//        //    try
//        //    {
//        //        ImageDTO imageDto = new ImageDTO();

//        //        //{"imgId":192,"iurl":"2110063361/1442288972579.jpg","cptn":"","ih":360,"iw":640,"imT":1,"albId":"default","albn":"Feed Photos","tm":1442288974849,"nl":0,"il":0,"ic":1,"nc":7,"sucs":true,"uId":"2110063361","fn":"smite.bakasura.desktop.2"}
//        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//        //        {
//        //            imageDto = HelperMethodsModel.BindImageDetails(_JobjFromResponse);
//        //        }
//        //        else
//        //        {
//        //            imageDto.ImageUrl = NotificationMessages.CONTENT_DOES_NOT_EXIST;
//        //        }
//        //        HelperMethodsAuth.AuthHandlerInstance.GetSingleImageDTOFromServer(imageDto);
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        log.Error("ProcessSingleImageDetail e ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//        //    }
//        //}

//        #endregion

//        #region NewsFeed

//        private void ProcessUpdateUnLikeComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                    {
//                        Dictionary<long, CommentDTO> commentsOfthisFeed = null;
//                        if (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out commentsOfthisFeed))
//                        {
//                            CommentDTO dto = null;
//                            if (commentsOfthisFeed.TryGetValue(cmntId, out dto))
//                            {
//                                if (dto.TotalLikeComment > 0) { dto.TotalLikeComment--; dto.ILikeComment = 0; }
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateCommentUnLike(nfId, cmntId, friendId, circleId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateUnLikeComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                    {
//                        Dictionary<long, CommentDTO> commentsOfthisFeed = null;
//                        if (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out commentsOfthisFeed))
//                        {
//                            CommentDTO dto = null;
//                            if (commentsOfthisFeed.TryGetValue(cmntId, out dto))
//                            {
//                                dto.TotalLikeComment++; dto.ILikeComment = 1;
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateCommentLike(nfId, cmntId, friendId, circleId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateDeleteStatusComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ActivistId] != null
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long auId = (long)_JobjFromResponse[JsonKeys.ActivistId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    Dictionary<long, CommentDTO> CommentsByNfid = null;
//                    lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                    {
//                        if (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out CommentsByNfid))
//                        {
//                            if (CommentsByNfid.ContainsKey(cmntId))
//                            {
//                                CommentsByNfid.Remove(cmntId);
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.DeleteFeedComment(nfId, friendId, circleId, cmntId, auId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateDeleteStatusComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateAddStatusComment()
//        {
//            // {"sucs":true,"cmnId":1951,"tm":1442225637709,"uId":"2110010304","cmn":"2","fn":"Sirat Test2","nfId":128,"auId":245,"nc":0,"rc":0,"sc":false,"loc":1}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ActivistId] != null
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.Comment] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    CommentDTO comment = HelperMethodsModel.BindCommentDetails(_JobjFromResponse);
//                    UserBasicInfoDTO user = null;
//                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(comment.UserIdentity, out user))
//                    {
//                        comment.ProfileImage = user.ProfileImage;
//                    }
//                    long auId = (long)_JobjFromResponse[JsonKeys.ActivistId];
//                    HelperMethodsAuth.NewsFeedHandlerInstance.AddFeedComment(comment, nfId, friendId, circleId, auId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateAddStatusComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateEditComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.Comment] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    string cmnt = (string)_JobjFromResponse[JsonKeys.Comment];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    HelperMethodsAuth.NewsFeedHandlerInstance.EditFeedComment(cmnt, cmntId, nfId, friendId, circleId, (_JobjFromResponse[JsonKeys.ShowContinue] != null && (bool)_JobjFromResponse[JsonKeys.ShowContinue]));
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateEditComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeStatus(bool MyLike = false)
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.ActivistId] != null && _JobjFromResponse[JsonKeys.LikeOrComment] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long auId = (long)_JobjFromResponse[JsonKeys.ActivistId];
//                    long nl = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    UserBasicInfoDTO user = null;
//                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null && _JobjFromResponse[JsonKeys.FullName] != null)
//                    {
//                        user = new UserBasicInfoDTO();
//                        if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                            user.UserIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                        if (_JobjFromResponse[JsonKeys.FullName] != null)
//                            user.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
//                        UserBasicInfoDTO tmp = null;
//                        if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                        {
//                            user.FriendShipStatus = tmp.FriendShipStatus;
//                        }
//                        else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                        {
//                            user.FriendShipStatus = -1; //for myself
//                        }
//                    }
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        FeedDTO feed = null;
//                        if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfId, out feed))
//                        {
//                            feed.NumberOfLikes++;
//                            if (MyLike) feed.ILike = 1;
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateStatusLike(MyLike, nfId, friendId, circleId, auId, user, nl);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateUnlikeStatus(bool MyLike = false)
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.ActivistId] != null && _JobjFromResponse[JsonKeys.LikeOrComment] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long auId = (long)_JobjFromResponse[JsonKeys.ActivistId];
//                    long nl = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
//                    long ut = (_JobjFromResponse[JsonKeys.UpdateTime] != null) ? (long)_JobjFromResponse[JsonKeys.UpdateTime] : 0;
//                    long uId = (_JobjFromResponse[JsonKeys.UserIdentity] != null) ? (long)_JobjFromResponse[JsonKeys.UserIdentity] : 0;
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        FeedDTO feed = null;
//                        if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfId, out feed))
//                        {
//                            if (feed.NumberOfLikes > 0)
//                            {
//                                feed.NumberOfLikes--;
//                                if (MyLike) feed.ILike = 0;
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateStatusUnlike(MyLike, nfId, friendId, circleId, auId, ut, uId, nl);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateUnlikeStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessFetchLikesForComment()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    //LikesSequnceCount++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    int seqTotal = Convert.ToInt32(str.Split(delimiterChars)[1]);
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];

//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        List<UserBasicInfoDTO> users = new List<UserBasicInfoDTO>();
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            if (singleObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)singleObj[JsonKeys.FullName];
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            //lock (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP)
//                            //{
//                            //    if (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.ContainsKey(user.UserIdentity))
//                            //    {
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP[user.UserIdentity] = user;
//                            //    }
//                            //    else
//                            //    {
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.Add(user.UserIdentity, user);
//                            //    }
//                            //}
//                            users.Add(user);
//                        }

//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadStatusLikes(nfId, users);
//                    }
//                    // log.Info("LIKE CONTACT LIST.." + LikesSequnceCount + "SEQTOTAL.." + seqTotal);
//                    //if (LikesSequnceCount == seqTotal)
//                    //{
//                    //    LikesSequnceCount = 0;
//                    //}
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessFetchLikesForStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessCommentsForStatus()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    //CommentsSeqCount++;
//                    //Dictionary<long, CommentDTO> CommentsByNfid = null;
//                    //string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    //char[] delimiterChars = { '/' };
//                    //int seqTotal = Convert.ToInt32(str.Split(delimiterChars)[1]);
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    List<CommentDTO> list = new List<CommentDTO>();
//                    if (_JobjFromResponse[JsonKeys.Comments] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Comments];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            CommentDTO comment = HelperMethodsModel.BindCommentDetails(singleObj);
//                            list.Add(comment);
//                            //lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                            //{
//                            //    if (!NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out CommentsByNfid))
//                            //    {
//                            //        CommentsByNfid = new Dictionary<long, CommentDTO>();
//                            //        NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.Add(nfId, CommentsByNfid);
//                            //    }
//                            //    CommentsByNfid[comment.CommentId] = comment;
//                            //}
//                        }
//                    }
//                    //log.Info("COMMENT." + CommentsSeqCount + "SEQTOTAL.." + seqTotal);
//                    //if (CommentsSeqCount == seqTotal)
//                    //{
//                    //    CommentsSeqCount = 0;
//                    if (list.Count > 0)
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadFeedComments(list, nfId, friendId, circleId);
//                    //}
//                }
//                //else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false && _JobjFromResponse[JsonKeys.NewsfeedId] != null)// no more data
//                //{
//                //    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                //    HelperMethodsAuth.NewsFeedHandlerInstance.LoadFeedComments(null, nfId, 0, 0);
//                //}
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessCommentsForStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessTypeFriendPresenceInfo()
//        {
//            try
//            {
//                if (_JobjFromResponse != null)
//                {
//                    long friendIdentity = 0;
//                    if (_JobjFromResponse[JsonKeys.FriendId] != null)
//                    {
//                        friendIdentity = (long)_JobjFromResponse[JsonKeys.FriendId];
//                    }
//                    else if (_JobjFromResponse[JsonKeys.UserIdentity] != null)
//                    {
//                        friendIdentity = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    }

//                    UserBasicInfoDTO _FriendDetailsInDictionary = null;
//                    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(friendIdentity, out _FriendDetailsInDictionary);

//                    if (_FriendDetailsInDictionary != null)
//                    {
//                        if (_JobjFromResponse[JsonKeys.Presence] != null)
//                        {
//                            _FriendDetailsInDictionary.Presence = (int)_JobjFromResponse[JsonKeys.Presence];
//                        }
//                        if (_JobjFromResponse[JsonKeys.LastOnlineTime] != null)
//                        {
//                            _FriendDetailsInDictionary.LastOnlineTime = (long)_JobjFromResponse[JsonKeys.LastOnlineTime];
//                        }
//                        if (_JobjFromResponse[JsonKeys.Device] != null)
//                        {
//                            _FriendDetailsInDictionary.Device = (int)_JobjFromResponse[JsonKeys.Device];
//                        }
//                        if (_JobjFromResponse[JsonKeys.Mood] != null)
//                        {
//                            _FriendDetailsInDictionary.Mood = (int)_JobjFromResponse[JsonKeys.Mood];
//                        }
//                        //HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(_FriendDetailsInDictionary);
//                        HelperMethodsAuth.AuthHandlerInstance.UI_ProcessTypeFriendPresenceInfo(_FriendDetailsInDictionary);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessTypeFriendPresenceInfo ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }

//        private void ProcessFetchLikesForStatus()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    LikesSequnceCount++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    int seqTotal = Convert.ToInt32(str.Split(delimiterChars)[1]);
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];

//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            if (singleObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)singleObj[JsonKeys.FullName];
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            list.Add(user);
//                            //lock (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP)
//                            //{
//                            //    if (NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.ContainsKey(user.UserIdentity))
//                            //    {
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP[user.UserIdentity] = user;
//                            //    }
//                            //    else
//                            //    {
//                            //        NewsFeedDictionaries.Instance.TEMP_LIKE_LIST_FOR_POPUP.Add(user.UserIdentity, user);
//                            //    }
//                            //}
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadStatusLikes(nfId, list);
//                    }
//                    // log.Info("LIKE CONTACT LIST.." + LikesSequnceCount + "SEQTOTAL.." + seqTotal);
//                    //if (LikesSequnceCount == seqTotal)
//                    //{
//                    LikesSequnceCount = 0;
//                    //     HelperMethodsAuth.NewsFeedHandlerInstance.LoadStatusLikes(nfId);
//                    //}
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessFetchLikesForStatus ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        private void ProcessUpdateAddStatus(bool AddStatus = false) // 377
//        {
//            try
//            {

//                if (_JobjFromResponse != null)
//                {
//                    FeedDTO singleFeed = null;
//                    if (_JobjFromResponse[JsonKeys.NewsFeed] != null)
//                    {
//                        singleFeed = HelperMethodsModel.BindFeedDetails((JObject)_JobjFromResponse[JsonKeys.NewsFeed]);
//                    }
//                    else
//                    {
//                        singleFeed = HelperMethodsModel.BindFeedDetails(_JobjFromResponse);
//                    }

//                    if (singleFeed.MediaContent != null && singleFeed.MediaContent.MediaList != null && singleFeed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                    {
//                        HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(singleFeed.MediaContent.MediaList.ElementAt(0));
//                        if (AddStatus)
//                        {
//                            singleFeed.ILike = singleFeed.IComment = singleFeed.IShare = 0;
//                            SingleMediaDTO dt = singleFeed.MediaContent.MediaList[0];
//                            dt.IShare = dt.ILike = dt.IComment = 0;
//                        }
//                    }
//                    if (singleFeed.WhoShare != null)
//                    {
//                        FeedDTO whoShareFeed = singleFeed.WhoShare;
//                        whoShareFeed.ParentFeed = singleFeed;

//                        if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.UserIdentity)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendId)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.UserIdentity)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendId)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.NewsfeedId)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.ParentFeed.NewsfeedId))
//                        {

//                            lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                            {
//                                if (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.ContainsKey(whoShareFeed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[whoShareFeed.NewsfeedId] = singleFeed.NewsfeedId;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.Add(whoShareFeed.NewsfeedId, singleFeed.NewsfeedId);
//                                }
//                            }

//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(whoShareFeed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[whoShareFeed.NewsfeedId] = whoShareFeed;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Add(whoShareFeed.NewsfeedId, whoShareFeed);
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.UpdateShareStatus(whoShareFeed);

//                            if (whoShareFeed.UserIdentity == DefaultSettings.LOGIN_USER_ID || whoShareFeed.FriendId == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, whoShareFeed, DefaultSettings.FEED_TYPE_MY, 0);
//                            }
//                            if (whoShareFeed.UserIdentity != DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(whoShareFeed.UserIdentity))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[whoShareFeed.UserIdentity].InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(whoShareFeed.UserIdentity, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[whoShareFeed.UserIdentity].InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                    }
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, whoShareFeed, DefaultSettings.FEED_TYPE_FRIEND, whoShareFeed.UserIdentity);
//                            }

//                            if (whoShareFeed.FriendId > 0 && whoShareFeed.FriendId != DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(whoShareFeed.FriendId))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[whoShareFeed.FriendId].InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(whoShareFeed.FriendId, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[whoShareFeed.FriendId].InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.ActualTime, whoShareFeed.FeedCategory);
//                                    }
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, whoShareFeed, DefaultSettings.FEED_TYPE_FRIEND, whoShareFeed.FriendId);
//                            }


//                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.Time, whoShareFeed.FeedCategory);
//                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, whoShareFeed, DefaultSettings.FEED_TYPE_ALL, 0);

//                        }
//                    }
//                    else
//                    {
//                        if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(singleFeed.UserIdentity)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(singleFeed.FriendId)
//                            && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(singleFeed.NewsfeedId))
//                        {

//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(singleFeed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[singleFeed.NewsfeedId] = singleFeed;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Add(singleFeed.NewsfeedId, singleFeed);
//                                }
//                            }

//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.Time, singleFeed.FeedCategory);
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_ALL, 0);

//                            if (singleFeed.UserIdentity == DefaultSettings.LOGIN_USER_ID || singleFeed.FriendId == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_MY, 0);
//                            }

//                            if (singleFeed.UserIdentity != DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(singleFeed.UserIdentity))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.UserIdentity].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(singleFeed.UserIdentity, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.UserIdentity].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                }

//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_FRIEND, singleFeed.UserIdentity);
//                            }

//                            if (singleFeed.FriendId > 0 && singleFeed.FriendId != DefaultSettings.LOGIN_USER_ID)
//                            {
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(singleFeed.FriendId))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.FriendId].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(singleFeed.FriendId, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.FriendId].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_FRIEND, singleFeed.FriendId);
//                            }
//                            if (singleFeed.GroupId > 0)
//                            {
//                                lock (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(singleFeed.GroupId))
//                                    {
//                                        NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.GroupId].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(singleFeed.GroupId, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[singleFeed.GroupId].InsertDataFromServer(singleFeed.NewsfeedId, singleFeed.ActualTime, singleFeed.FeedCategory);
//                                    }
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, singleFeed, DefaultSettings.FEED_TYPE_CIRCLE, singleFeed.GroupId);
//                            }

//                            //HelperMethodsAuth.NewsFeedHandlerInstance.UpdateAddStatus(singleFeed);
//                        }
//                    }


//                }

//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateAddStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateEditStatus()
//        {
//            try
//            {
//                if (_JobjFromResponse != null)
//                {
//                    FeedDTO singleFeed = HelperMethodsModel.BindFeedDetails(_JobjFromResponse);
//                    string sts = (_JobjFromResponse[JsonKeys.Status] == null) ? null : singleFeed.Status;
//                    FeedDTO prevFeed;

//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(singleFeed.NewsfeedId, out prevFeed);

//                        if (prevFeed != null)
//                        {
//                            prevFeed.Status = singleFeed.Status;
//                        }

//                    }
//                    List<long> taggedIds = null, removedTags = null;
//                    if (singleFeed.TaggedFriendsList != null && singleFeed.TaggedFriendsList.Count > 0)
//                    {
//                        taggedIds = new List<long>();
//                        foreach (var item in singleFeed.TaggedFriendsList)
//                        {
//                            taggedIds.Add(item.UserTableId);
//                        }
//                    }
//                    if (_JobjFromResponse[JsonKeys.RemovedTagsFriendUtIds] != null)
//                    {
//                        removedTags = new List<long>();
//                        JArray array = (JArray)_JobjFromResponse[JsonKeys.RemovedTagsFriendUtIds];
//                        for (int j = 0; j < array.Count; j++)
//                        {
//                            removedTags.Add((long)array.ElementAt(j));
//                        }
//                    }
//                    else removedTags = taggedIds;
//                    HelperMethodsAuth.NewsFeedHandlerInstance.EditNewsFeed(true, singleFeed.NewsfeedId, sts, singleFeed.StatusTags, singleFeed.FriendId, singleFeed.GroupId, taggedIds, removedTags, singleFeed.locationDTO, singleFeed.Privacy, singleFeed.TaggedFriendsList);
//                    //HelperMethodsAuth.NewsFeedHandlerInstance.UpdateEditStatus(singleFeed);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateEditStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateDeleteStatus()
//        {
//            // {"actn":379,"nfId":666,"sucs":true,"pckFs":23446,"fn":"Shahadat","ln":"Hossain"}
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
//                    long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Remove(nfId);
//                    }
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                    {
//                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.RemoveData(nfId);

//                    }
//                    lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
//                    {
//                        NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.Remove(nfId);
//                    }

//                    if (friendId > 0 && NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(friendId))
//                    {
//                        lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                        {
//                            NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[friendId].RemoveData(nfId);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.RemoveSingleFeedUI(nfId, DefaultSettings.FEED_TYPE_FRIEND, friendId);
//                    }
//                    if (circleId > 0)
//                    {
//                        lock (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                        {
//                            NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[circleId].RemoveData(nfId);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.RemoveSingleFeedUI(nfId, DefaultSettings.FEED_TYPE_CIRCLE, circleId);
//                    }

//                    HelperMethodsAuth.NewsFeedHandlerInstance.RemoveSingleFeedUI(nfId, DefaultSettings.FEED_TYPE_ALL);

//                    //HelperMethodsAuth.NewsFeedHandlerInstance.UpdateDeleteStatus(nfId, friendId, circleId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateDeleteStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessFriendNewsFeeds() // 110
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    long _FriendIdentity = (long)_JobjFromResponse[JsonKeys.FriendId];
//<<<<<<< .mine
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {

//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            if (feed.WhoShare != null)
//                            {
//                                feed.WhoShare.ParentFeed = feed;
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.WhoShare.NewsfeedId] = feed.WhoShare;
//                                }
//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(_FriendIdentity))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[_FriendIdentity].InsertDataFromServer(feed.WhoShare.NewsfeedId, feed.WhoShare.ActualTime, feed.WhoShare.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(_FriendIdentity, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[_FriendIdentity].InsertDataFromServer(feed.WhoShare.NewsfeedId, feed.WhoShare.ActualTime, feed.WhoShare.FeedCategory);
//                                    }

//                                }

//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(0, feed.WhoShare, DefaultSettings.FEED_TYPE_FRIEND, _FriendIdentity);

//                            }
//                            else
//                            {
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                                }

//                                lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(_FriendIdentity))
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[_FriendIdentity].InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(_FriendIdentity, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[_FriendIdentity].InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                    }

//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(0, feed, DefaultSettings.FEED_TYPE_FRIEND, _FriendIdentity);
//                            }
//                        }

//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessFriendNewsFeeds ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//<<<<<<< .mine
//        private void ProcessMyBook() //94
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {

//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            if (feed.WhoShare != null)
//                            {
//                                feed.WhoShare.ParentFeed = feed;

//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.WhoShare.NewsfeedId] = feed.WhoShare;
//                                }
//                                lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.WhoShare.NewsfeedId, feed.WhoShare.ActualTime, feed.WhoShare.FeedCategory);
//                                }

//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(0, feed.WhoShare, DefaultSettings.FEED_TYPE_MY);
//                            }
//                            else
//                            {
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                                }
//                                lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(0, feed, DefaultSettings.FEED_TYPE_MY);
//=======
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            //OBSOLETE CHECK 
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//                            lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(SettingsConstants.FEED_REGULAR, feed, DefaultSettings.FEED_TYPE_MY);
//                        }
//>>>>>>> .r1602
//                            }
//                        }

//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMyBook ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessMediaFeed()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {

//                    short mediaTrendingType = ((int)_JobjFromResponse[JsonKeys.MediaTrendingFeed] == SettingsConstants.MEDIA_FEED_TYPE_ALL) ? SettingsConstants.MEDIA_FEED_TYPE_ALL : SettingsConstants.MEDIA_FEED_TYPE_TRENDING;

//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int first_feed_state = 0;
//                        if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
//                            first_feed_state = client_packet_id.Equals(DefaultSettings.START_MEDIAFEEDS_PACKETID) ? 1 : 0; //0=not addupdatestatus nor first time,2=addupdatestatus,1=firsttimefeed
//                        else if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_TRENDING)
//                            first_feed_state = client_packet_id.Equals(DefaultSettings.START_MEDIAFEEDSTRENDING_PACKETID) ? 1 : 0; //0=not addupdatestatus nor first time,2=addupdatestatus,1=firsttimefeed

//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }

//                            if (feed.WhoShare != null)
//                            {
//                                FeedDTO whoShareFeed = feed.WhoShare;
//                                whoShareFeed.ParentFeed = feed;

//                                if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.UserIdentity)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendId)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.UserIdentity)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendId)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.NewsfeedId)
//                                    && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.ParentFeed.NewsfeedId))
//                                {

//                                    lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[whoShareFeed.NewsfeedId] = feed.NewsfeedId;
//                                    }

//                                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                    {
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[whoShareFeed.NewsfeedId] = whoShareFeed;
//                                    }

//<<<<<<< .mine
//                                    if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
//                                    {
//                                        NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.Time, whoShareFeed.FeedCategory);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_MEDIA);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(whoShareFeed.NewsfeedId);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                if (feed.FeedCategory == SettingsConstants.SPECIAL_FEED)
//                                {
//                                    if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
//                                    {
//                                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId);
//                                        if (data != null)
//                                        {
//                                            if (data.Tm != feed.Time)
//                                            {
//                                                NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.GetData(feed.NewsfeedId);
//                                        if (data != null)
//                                        {
//                                            if (data.Tm != feed.Time)
//                                            {
//                                                NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(feed.NewsfeedId);
//                                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(feed.NewsfeedId);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
//                                        }
//                                    }
//                                }
//                                else
//                                {
//                                    if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.UserIdentity)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendId)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                    {
//                                        if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
//                                        {
//                                            NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA);
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(feed.NewsfeedId);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//=======
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                //#if AUTH_LOG
//                log.Error("ProcessMediaFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//                //#endif
//            }
//        }
//private void ProcessBreakingNewsPortalFeeds() // 302
//{
//    try
//    {
//        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.BreakingNewsFeeds] != null)
//        {
//            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.BreakingNewsFeeds];
//            List<FeedDTO> breakingNws = new List<FeedDTO>();
//            foreach (JObject singleObj in jarray)
//            {
//                FeedDTO dto = new FeedDTO();
//                dto.NewsPortalInfo = new NewsDTO();
//                dto.NewsfeedId = dto.NewsPortalInfo.NewsId = (singleObj[JsonKeys.Id] != null) ? ((long)(singleObj[JsonKeys.Id])) : 0;
//                dto.ProfileImage = (singleObj[JsonKeys.ProfileImage] != null) ? ((string)(singleObj[JsonKeys.ProfileImage])) : string.Empty;
//                dto.NewsPortalInfo.NewsShortDescription = (singleObj[JsonKeys.NewsShortDescription] != null) ? ((string)(singleObj[JsonKeys.NewsShortDescription])) : string.Empty;
//                dto.NewsPortalInfo.NewsTitle = (singleObj[JsonKeys.NewsPortalTitle] != null) ? ((string)(singleObj[JsonKeys.NewsPortalTitle])) : string.Empty;
//                breakingNws.Add(dto);
//            }
//            HelperMethodsAuth.NewsFeedHandlerInstance.BreakingNewsSliderUI(breakingNws);
//        }
//    }
//    catch (Exception e)
//    {
//        log.Error("ProcessBreakingNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//    }
//}
//        private void ProcessBusinessPageFeeds()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int feed_state = client_packet_id.Equals(DefaultSettings.START_PAGESFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//>>>>>>> .r1602
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                //#if AUTH_LOG
//                log.Error("ProcessMediaFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//                //#endif
//            }
//        }
//        private void ProcessBreakingNewsPortalFeeds() // 302
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.BreakingNewsFeeds] != null)
//                {
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.BreakingNewsFeeds];
//                    List<FeedDTO> breakingNws = new List<FeedDTO>();
//                    foreach (JObject singleObj in jarray)
//                    {
//                        FeedDTO dto = new FeedDTO();
//                        dto.NewsPortalInfo = new NewsDTO();
//                        dto.NewsfeedId = dto.NewsPortalInfo.NewsId = (singleObj[JsonKeys.Id] != null) ? ((long)(singleObj[JsonKeys.Id])) : 0;
//                        dto.ProfileImage = (singleObj[JsonKeys.ProfileImage] != null) ? ((string)(singleObj[JsonKeys.ProfileImage])) : string.Empty;
//                        dto.NewsPortalInfo.NewsShortDescription = (singleObj[JsonKeys.NewsShortDescription] != null) ? ((string)(singleObj[JsonKeys.NewsShortDescription])) : string.Empty;
//                        dto.NewsPortalInfo.NewsTitle = (singleObj[JsonKeys.NewsPortalTitle] != null) ? ((string)(singleObj[JsonKeys.NewsPortalTitle])) : string.Empty;
//                        breakingNws.Add(dto);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.BreakingNewsSliderUI(breakingNws);
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessBreakingNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessBusinessPageFeeds()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int first_feed_state = client_packet_id.Equals(DefaultSettings.START_PAGESFEEDS_PACKETID) ? 1 : 0;
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//OBSOLETE CHECK 

//}
//        private void ProcessNewsPortalFeed() // 295
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int feed_state = client_packet_id.Equals(DefaultSettings.START_NEWSPORTALFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//>>>>>>> .r1602
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//                            //  if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                            //  {
//                            if (feed.NewsPortalInfo != null && feed.NewsPortalInfo.IsSaved)
//                            {
//                                if (NewsFeedDictionaries.Instance.SAVED_PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    NewsFeedDictionaries.Instance.SAVED_PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_PAGE_SAVED);
//                            }
//                            if (NewsFeedDictionaries.Instance.PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                            {
//                                if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_PAGE);
//                                }
//                            }
//                            //}
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessPagesFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsPortalFeed() // 295
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int first_feed_state = client_packet_id.Equals(DefaultSettings.START_NEWSPORTALFEEDS_PACKETID) ? 1 : 0;
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//OBSOLETE CHECK 

//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//                            if (feed.NewsPortalInfo != null && feed.NewsPortalInfo.IsSaved)
//                            {
//                                if (NewsFeedDictionaries.Instance.SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    NewsFeedDictionaries.Instance.SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory, false);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed_state, feed, DefaultSettings.FEED_TYPE_NEWSPORTAL_SAVED);
//                            }
//                            if (NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                            {
//                                if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory, false);
//                                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed_state, feed, DefaultSettings.FEED_TYPE_NEWSPORTAL);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsFeed()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int feed_state = client_packet_id.Equals(DefaultSettings.START_ALLFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//>>>>>>> .r1602
//OBSOLETE CHECK 
//                            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                            {
//                                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                            }
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//                            //if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                            //{
//                            if (feed.NewsPortalInfo != null && feed.NewsPortalInfo.IsSaved)
//                            {
//                                if (NewsFeedDictionaries.Instance.SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    NewsFeedDictionaries.Instance.SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                }
//                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_NEWSPORTAL_SAVED);
//                            }
//                            if (NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                            {
//                                if (!NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                {
//                                    NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_NEWSPORTAL);
//                                }
//                            }
//                            // }
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsFeed()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        int first_feed_state = client_packet_id.Equals(DefaultSettings.START_ALLFEEDS_PACKETID) ? 1 : 0; //0=not addupdatestatus nor first time,2=addupdatestatus,1=firsttimefeed
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//if (feed.Time > DefaultSettings.MaxAllFeedsTime) DefaultSettings.MaxAllFeedsTime = feed.Time;
//if (feed.Time < DefaultSettings.MinAllFeedsTime) DefaultSettings.MinAllFeedsTime = feed.Time;
//if (feed.WhoShare != null)
//{
//    FeedDTO whoShareFeed = feed.WhoShare;
//    whoShareFeed.ParentFeed = feed;


//                            if (feed.WhoShare != null)
//                            {
//                                FeedDTO whoShareFeed = feed.WhoShare;
//                                whoShareFeed.ParentFeed = feed;

//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(whoShareFeed.NewsfeedId) == null)
//                                {
//                                    if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.UserIdentity)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendId)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.UserIdentity)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendId)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.NewsfeedId)
//                                        && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.ParentFeed.NewsfeedId))
//                                    {

//                                        lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                                        {
//                                            NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[whoShareFeed.NewsfeedId] = feed.NewsfeedId;
//                                        }
//                                        lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                        {
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[whoShareFeed.NewsfeedId] = whoShareFeed;
//                                        }
//=======
//                                        //if (whoShareFeed.Time > DefaultSettings.MaxAllFeedsTime) DefaultSettings.MaxAllFeedsTime = whoShareFeed.Time;
//                                        //if (whoShareFeed.Time < DefaultSettings.MinAllFeedsTime) DefaultSettings.MinAllFeedsTime = whoShareFeed.Time;
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.Time, whoShareFeed.FeedCategory);
//                                        //HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed, DefaultSettings.FEED_TYPE_ALL);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_ALL);
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    if (feed.FeedCategory == SettingsConstants.SPECIAL_FEED)
//                                    {
//                                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId);
//                                        if (data != null)
//                                        {
//                                            if (data.Tm != feed.Time)
//                                            {
//                                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.UserIdentity)
//                                           && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendId)
//                                           && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                        {
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//>>>>>>> .r1544

//                                        if (whoShareFeed.Time > DefaultSettings.MaxAllFeedsTime) DefaultSettings.MaxAllFeedsTime = whoShareFeed.Time;
//                                        if (whoShareFeed.Time < DefaultSettings.MinAllFeedsTime) DefaultSettings.MinAllFeedsTime = whoShareFeed.Time;
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.Time, whoShareFeed.FeedCategory);
//                                        //HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed, DefaultSettings.FEED_TYPE_ALL);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
//                                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_ALL);
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId) == null)
//                                {
//                                    if (feed.FeedCategory == SettingsConstants.SPECIAL_FEED)
//                                    {
//                                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId);
//                                        if (data != null)
//                                        {
//                                            if (data.Tm != feed.Time)
//                                            {
//                                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                                HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                            }
//                                        }
//                                        else
//                                        {
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.UserIdentity)
//                                           && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendId)
//                                           && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
//                                        {
//                                            if (feed.Time > DefaultSettings.MaxAllFeedsTime) DefaultSettings.MaxAllFeedsTime = feed.Time;
//                                            if (feed.Time < DefaultSettings.MinAllFeedsTime) DefaultSettings.MinAllFeedsTime = feed.Time;
//                                            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                            HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_ALL);
//                                        }
//                                    }
//                                }
//                            }
//                        }

//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("processNewsFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessMediaAlbumList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableId] != null && _JobjFromResponse[JsonKeys.MediaType] != null)
//                {
//                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                    int mediatype = (int)_JobjFromResponse[JsonKeys.MediaType];
//                    if (utid == DefaultSettings.LOGIN_USER_TABLE_ID)
//                    {
//                        int tr = (int)_JobjFromResponse[JsonKeys.TotalRecords];
//                        if (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO) DefaultSettings.MY_AUDIO_ALBUMS_COUNT = tr;
//                        else if (mediatype == SettingsConstants.MEDIA_TYPE_VIDEO) DefaultSettings.MY_VIDEO_ALBUMS_COUNT = tr;
//                    }
//                    if (_JobjFromResponse[JsonKeys.MediaAlbumList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaAlbumList];
//                        if (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO)
//                        {
//                            List<MediaContentDTO> tmpAlbums = new List<MediaContentDTO>();
//                            lock (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID)
//                            {
//                                Dictionary<long, MediaContentDTO> albumsOfaUId = null;
//                                if (!NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.TryGetValue(utid, out albumsOfaUId))
//                                {
//                                    albumsOfaUId = new Dictionary<long, MediaContentDTO>();
//                                    NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.Add(utid, albumsOfaUId);
//                                }
//                                foreach (JObject obj in jarray)
//                                {
//                                    MediaContentDTO albumDto = new MediaContentDTO();
//                                    albumDto.UserTableId = utid;
//                                    if (obj[JsonKeys.Id] != null) albumDto.AlbumId = (long)obj[JsonKeys.Id];
//                                    if (obj[JsonKeys.AlbumName] != null) albumDto.AlbumName = (string)obj[JsonKeys.AlbumName];
//                                    if (obj[JsonKeys.MediaType] != null) albumDto.MediaType = (int)obj[JsonKeys.MediaType];
//                                    if (obj[JsonKeys.MediaAlbumImageURL] != null) albumDto.AlbumImageUrl = (string)obj[JsonKeys.MediaAlbumImageURL];
//                                    if (obj[JsonKeys.MemberCount] != null) albumDto.TotalMediaCount = (int)obj[JsonKeys.MemberCount];
//                                    if (albumsOfaUId.ContainsKey(albumDto.AlbumId)) albumsOfaUId[albumDto.AlbumId] = albumDto;
//                                    else albumsOfaUId.Add(albumDto.AlbumId, albumDto);
//                                    tmpAlbums.Add(albumDto);
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.LoadAlbumList(utid, mediatype, tmpAlbums);
//                        }
//                        else if (mediatype == SettingsConstants.MEDIA_TYPE_VIDEO)
//                        {
//                            List<MediaContentDTO> tmpAlbums = new List<MediaContentDTO>();
//                            lock (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID)
//                            {
//                                Dictionary<long, MediaContentDTO> albumsOfaUId = null;
//                                if (!NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.TryGetValue(utid, out albumsOfaUId))
//                                {
//                                    albumsOfaUId = new Dictionary<long, MediaContentDTO>();
//                                    NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.Add(utid, albumsOfaUId);
//                                }
//                                foreach (JObject obj in jarray)
//                                {
//                                    MediaContentDTO albumDto = new MediaContentDTO();
//                                    albumDto.UserTableId = utid;
//                                    if (obj[JsonKeys.Id] != null) albumDto.AlbumId = (long)obj[JsonKeys.Id];
//                                    if (obj[JsonKeys.AlbumName] != null) albumDto.AlbumName = (string)obj[JsonKeys.AlbumName];
//                                    if (obj[JsonKeys.MediaType] != null) albumDto.MediaType = (int)obj[JsonKeys.MediaType];
//                                    if (obj[JsonKeys.MediaAlbumImageURL] != null) albumDto.AlbumImageUrl = (string)obj[JsonKeys.MediaAlbumImageURL];
//                                    if (obj[JsonKeys.MemberCount] != null) albumDto.TotalMediaCount = (int)obj[JsonKeys.MemberCount];
//                                    if (albumsOfaUId.ContainsKey(albumDto.AlbumId)) albumsOfaUId[albumDto.AlbumId] = albumDto;
//                                    else albumsOfaUId.Add(albumDto.AlbumId, albumDto);
//                                    tmpAlbums.Add(albumDto);
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.LoadAlbumList(utid, mediatype, tmpAlbums);
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaAlbumList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessMediaAlbumContentList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableId] != null && _JobjFromResponse[JsonKeys.AlbumId] != null)
//                {
//                    MediaItemsSequenceCount++;
//                    string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
//                    int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
//                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//                    int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
//                    long albumId = (long)_JobjFromResponse[JsonKeys.AlbumId];
//                    if (!NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.ContainsKey(albumId)) NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.Add(albumId, new List<SingleMediaDTO>());
//                    List<SingleMediaDTO> list = NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS[albumId];
//                    if (_JobjFromResponse[JsonKeys.MediaContentList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
//                        foreach (JObject ob in jarray)
//                        {
//                            SingleMediaDTO mediaDTO = HelperMethodsModel.BindSingleMediaDTO(ob);
//                            mediaDTO.UserTableId = utid;
//                            mediaDTO.AlbumId = albumId;
//                            list.Add(mediaDTO);
//                        }
//                    }
//                    if (seqTotal == MediaItemsSequenceCount)
//                    {
//                        Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
//                        if (DICT_BY_UTID.ContainsKey(utid) && DICT_BY_UTID[utid].ContainsKey(albumId))
//                        {
//                            MediaContentDTO album = DICT_BY_UTID[utid][albumId];
//                            if (album.MediaList == null) album.MediaList = new List<SingleMediaDTO>();
//                            album.MediaList.AddRange(list);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowAlbumItems(albumId, list);
//                        NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.Clear();
//                        MediaItemsSequenceCount = 0;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessMediaAlbumContentList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        //private void ProcessMediaAlbumContentList()
//        //{
//        //    try
//        //    {
//        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableId] != null && _JobjFromResponse[JsonKeys.AlbumId] != null)
//        //        {
//        //            //AlbumMediaContentListSeqCount++;
//        //            string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
//        //            //int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
//        //            //string CurrentSequence = sequence.Split(new Char[] { '/' })[0];
//        //            long utid = (long)_JobjFromResponse[JsonKeys.UserTableId];
//        //            int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType]; ;
//        //            //long uid = (utid == DefaultSettings.LOGIN_USER_TABLE_ID) ? DefaultSettings.LOGIN_USER_ID : FriendDictionaries.Instance.UTID_UID_DICTIONARY[utid];
//        //            long albumId = (long)_JobjFromResponse[JsonKeys.AlbumId];
//        //            //Dictionary<string, Dictionary<long, SingleMediaDTO>> TEMP_DICTIONARY = null;
//        //            //TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_MY_SINGLE_MEDIA_DETAILS;
//        //            Dictionary<long, SingleMediaDTO> TEMP_DICTIONARY = new Dictionary<long, SingleMediaDTO>();
//        //            if (_JobjFromResponse[JsonKeys.MediaContentList] != null)
//        //            {
//        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
//        //                // lock (TEMP_DICTIONARY)
//        //                // {
//        //                //if (!TEMP_DICTIONARY.ContainsKey(CurrentSequence))
//        //                //{
//        //                //    TEMP_DICTIONARY.Add(CurrentSequence, new Dictionary<long, SingleMediaDTO>());
//        //                //}
//        //                foreach (JObject ob in jarray)
//        //                {
//        //                    SingleMediaDTO mediaDTO = HelperMethodsModel.BindSingleMediaDTO(ob);
//        //                    mediaDTO.UserTableId = utid;
//        //                    mediaDTO.AlbumId = albumId;
//        //                    //mediaType = imageDTO.MediaType;
//        //                    //if (TEMP_DICTIONARY[CurrentSequence].ContainsKey(imageDTO.ContentId))
//        //                    //{
//        //                    //    TEMP_DICTIONARY[CurrentSequence][imageDTO.ContentId] = imageDTO;
//        //                    //}
//        //                    //else
//        //                    //{
//        //                    //    TEMP_DICTIONARY[CurrentSequence].Add(imageDTO.ContentId, imageDTO);
//        //                    //}
//        //                    TEMP_DICTIONARY[mediaDTO.ContentId] = mediaDTO;
//        //                }
//        //                //}
//        //            }

//        //            // if (TEMP_DICTIONARY.Count == seqTotal)
//        //            // {
//        //            //int mediaType = TEMP_DICTIONARY.ElementAt(0).Value.MediaType;
//        //            Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
//        //            Dictionary<long, MediaContentDTO> albumsOfaUtId = null;
//        //            if (!DICT_BY_UTID.TryGetValue(utid, out albumsOfaUtId)) { albumsOfaUtId = new Dictionary<long, MediaContentDTO>(); DICT_BY_UTID.Add(utid, albumsOfaUtId); }
//        //            MediaContentDTO album = null;
//        //            if (albumsOfaUtId.TryGetValue(albumId, out album) && album.MediaList == null) album.MediaList = new List<SingleMediaDTO>();
//        //            List<SingleMediaDTO> lst = new List<SingleMediaDTO>();
//        //            // foreach (Dictionary<long, SingleMediaDTO> val in TEMP_DICTIONARY.Values)
//        //            //{
//        //            //    if (album != null) album.MediaList.AddRange(val.Values.ToList());
//        //            //    lst.AddRange(val.Values.ToList());
//        //            //}
//        //            if (album != null) album.MediaList.AddRange(TEMP_DICTIONARY.Values.ToList());
//        //            lst.AddRange(TEMP_DICTIONARY.Values.ToList());

//        //            HelperMethodsAuth.NewsFeedHandlerInstance.LoadItemsOfAnAlbum(lst, albumId, mediaType, utid, (album != null) ? album.TotalMediaCount : 0);//album.MediaList
//        //            //System.Diagnostics.Debug.WriteLine("ALBUM CONTENT LIST.." + AlbumMediaContentListSeqCount + "SEQTOTAL.." + seqTotal);                       
//        //            TEMP_DICTIONARY.Clear();
//        //            // }
//        //        }
//        //        else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false && _JobjFromResponse[JsonKeys.AlbumId] != null)
//        //        {
//        //            long albumId = (long)_JobjFromResponse[JsonKeys.AlbumId];
//        //            HelperMethodsAuth.NewsFeedHandlerInstance.NoMoreItemsInHashTagOrAlbum(albumId, 0);
//        //            HelperMethodsAuth.NewsFeedHandlerInstance.NoItemFoundInUCSingleAlbumSong();
//        //        }
//        //    }
//        //    catch (Exception ex)
//        //    {

//        //        log.Error("ProcessMediaAlbumContentList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//        //    }
//        //}


//        private void ProcessMediaLikeList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null)
//                {
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        List<UserBasicInfoDTO> tmpList = new List<UserBasicInfoDTO>();
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                                user.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                            if (singleObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)singleObj[JsonKeys.FullName];
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            tmpList.Add(user);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadLikeList(tmpList, 0, 0, contentId, 0);
//                    }
//                }
//                else if (_JobjFromResponse != null && !(bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    HelperMethodsAuth.NewsFeedHandlerInstance.NodataFoundLikeList();
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessMediaLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        private void ProcessMediaCommentList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.ContentId] != null)
//                {
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    //Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (_JobjFromResponse[JsonKeys.Comments] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Comments];
//                        //lock (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST)
//                        //{
//                        //    if (!NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem))
//                        //    {
//                        //        CommentsOfThisItem = new Dictionary<long, CommentDTO>();
//                        //        NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.Add(contentId, CommentsOfThisItem);
//                        //    }
//                        List<CommentDTO> list = new List<CommentDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            CommentDTO comment = HelperMethodsModel.BindCommentDetails(singleObj);
//                            list.Add(comment);
//                            //        lock (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST)
//                            //        {
//                            //            if (CommentsOfThisItem.ContainsKey(comment.CommentId)) CommentsOfThisItem[comment.CommentId] = comment;
//                            //            else CommentsOfThisItem.Add(comment.CommentId, comment);
//                            //        }
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadCommentList(list, nfId, 0, contentId);
//                        //}
//                    }
//                    //if (DefaultSettings.MediaCommentsSequenceCount == seqTotal && CommentsOfThisItem != null && CommentsOfThisItem.Count > 0)
//                    //{
//                    //    DefaultSettings.MediaCommentsSequenceCount = 0;
//                    //    lock (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST)
//                    //    {
//                    //        List<CommentDTO> lst = CommentsOfThisItem.Values.ToList();
//                    //        HelperMethodsAuth.NewsFeedHandlerInstance.LoadCommentList(lst, 0, 0, contentId);
//                    //    }
//                    //}
//                }
//                /* else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                 {
//                     HelperMethodsAuth.AuthHandlerInstance.DisableLoaderAnimation();
//                     string msg = "Unable to Load Comments!";
//                     if (_JobjFromResponse[JsonKeys.Message] != null)
//                     {
//                         msg = (string)_JobjFromResponse[JsonKeys.Message];
//                     }
//                     long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                     long contentId = (_JobjFromResponse[JsonKeys.ContentId] != null) ? (long)_JobjFromResponse[JsonKeys.ContentId] : 0;
//                     HelperMethodsAuth.NewsFeedHandlerInstance.LoadCommentList(null, nfId, 0, contentId); 
//                 }*/
//                HelperMethodsAuth.NewsFeedHandlerInstance.LoadCommentUI();
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaCommentList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessMediaCommentLikeList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    if (_JobjFromResponse[JsonKeys.Likes] != null)
//                    {
//                        List<UserBasicInfoDTO> tmpList = new List<UserBasicInfoDTO>();
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            UserBasicInfoDTO user = new UserBasicInfoDTO();
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                                user.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                                user.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                            if (singleObj[JsonKeys.FullName] != null)
//                                user.FullName = (string)singleObj[JsonKeys.FullName];
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                                user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            UserBasicInfoDTO tmp = null;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(user.UserIdentity, out tmp))
//                            {
//                                user.FriendShipStatus = tmp.FriendShipStatus;
//                            }
//                            else if (user.UserIdentity == DefaultSettings.LOGIN_USER_ID)
//                            {
//                                user.FriendShipStatus = -1; //for myself
//                            }
//                            tmpList.Add(user);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadLikeList(tmpList, 0, 0, contentId, commentId);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessMediaCommentLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        private void ProcessSingleFeedShareList() //250,115
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Shares] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
//                {
//                    long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Shares];
//                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
//                    long minIdOfSequence = 99999;
//                    foreach (JObject obj in jarray)
//                    {
//                        if (obj[JsonKeys.UserTableId] != null && obj[JsonKeys.Id] != null && obj[JsonKeys.FullName] != null && obj[JsonKeys.UserIdentity] != null && obj[JsonKeys.ProfileImage] != null)
//                        {
//                            long uid = (long)obj[JsonKeys.UserIdentity];
//                            UserBasicInfoDTO user = null;
//                            if (uid == DefaultSettings.LOGIN_USER_ID) user = new UserBasicInfoDTO();
//                            else
//                            {
//                                if (!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(uid))
//                                    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(uid, new UserBasicInfoDTO { UserIdentity = uid });
//                                user = FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[uid];
//                            }
//                            user.FullName = (string)obj[JsonKeys.FullName];
//                            user.UserTableId = (long)obj[JsonKeys.UserTableId];
//                            user.UserIdentity = uid;
//                            user.ProfileImage = (string)obj[JsonKeys.ProfileImage];
//                            long Id = (long)obj[JsonKeys.Id];
//                            if (Id < minIdOfSequence) minIdOfSequence = Id;
//                            list.Add(user);
//                        }
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.ShowSingleFeedShareList(nfId, list, minIdOfSequence);
//                }
//            }
//            catch (Exception ex)
//            {
//                log.Error("ProcessMediaShareList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
//            }
//        }
//        private void ProcessWhoSharesList() // 249
//        {
//            try
//            {

//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    long parentNfid = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    int seqTotal = Convert.ToInt32(((string)_JobjFromResponse[JsonKeys.Sequence]).Split(new char[] { '/' })[1]);

//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        List<FeedDTO> tempList = new List<FeedDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            tempList.Add(feed);
//                        }

//                        lock (NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS)
//                        {
//                            if (NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS.ContainsKey(parentNfid))
//                            {
//                                if (NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].ContainsKey((string)_JobjFromResponse[JsonKeys.Sequence]))
//                                {
//                                    NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid][(string)_JobjFromResponse[JsonKeys.Sequence]] = tempList;
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].Add((string)_JobjFromResponse[JsonKeys.Sequence], tempList);
//                                }
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS.Add(parentNfid, new Dictionary<string, List<FeedDTO>>());
//                                NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].Add((string)_JobjFromResponse[JsonKeys.Sequence], tempList);
//                            }
//                        }

//                        if (tempList != null && tempList.Count > 0)
//                        {
//                            foreach (FeedDTO sharedFeed in tempList)
//                            {
//                                lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                                {
//                                    if (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.ContainsKey(sharedFeed.NewsfeedId))
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[sharedFeed.NewsfeedId] = parentNfid;
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.Add(sharedFeed.NewsfeedId, parentNfid);
//                                    }
//                                }

//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(sharedFeed.NewsfeedId))
//                                    {
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[sharedFeed.NewsfeedId] = sharedFeed;
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Add(sharedFeed.NewsfeedId, sharedFeed);
//                                    }
//                                }

//                                /*lock (NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    if (NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(parentNfid))
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME[parentNfid].InsertData(sharedFeed.NewsfeedId, sharedFeed.ActualTime);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(parentNfid, new CustomSortedList());
//                                        NewsFeedDictionaries.Instance.SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME[parentNfid].InsertData(sharedFeed.NewsfeedId, sharedFeed.ActualTime);
//                                    }
//                                }*/

//                            }

//                            if (seqTotal == NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].Count)
//                            {
//                                CustomSortedList whoShareList = new CustomSortedList();
//                                foreach (string key in NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid].Keys)
//                                {
//                                    foreach (FeedDTO sharedFeed in NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS[parentNfid][key].ToList())
//                                    {
//                                        whoShareList.InsertData(sharedFeed.NewsfeedId, sharedFeed.ActualTime, sharedFeed.FeedCategory);
//                                    }
//                                }

//                                HelperMethodsAuth.NewsFeedHandlerInstance.WhoShareList(parentNfid, whoShareList);

//                                NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS.Clear();
//                            }
//                        }

//                    }
//                }
//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    long parentNfid = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    HelperMethodsAuth.NewsFeedHandlerInstance.WhoShareList(parentNfid, null);
//                    NewsFeedDictionaries.Instance.TEMP_WHO_SHARE_NEWS_FEEDS.Clear();
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessWhoSharesList ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessFeedTagList() //274
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.FriendsTagList] != null)
//                {
//                    long nfid = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);
//                    JArray array = (JArray)_JobjFromResponse[JsonKeys.FriendsTagList];
//                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
//                    foreach (JObject obj in array)
//                    {
//                        if (obj[JsonKeys.UserTableId] != null && obj[JsonKeys.Name] != null && obj[JsonKeys.UserIdentity] != null && obj[JsonKeys.ProfileImage] != null)
//                        {
//                            long uid = (long)obj[JsonKeys.UserIdentity];
//                            if (!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(uid))
//                                FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.Add(uid, new UserBasicInfoDTO { UserIdentity = uid });
//                            UserBasicInfoDTO user = FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[uid];
//                            user.FullName = (string)obj[JsonKeys.Name];
//                            user.UserTableId = (long)obj[JsonKeys.UserTableId];
//                            user.UserIdentity = uid;
//                            user.ProfileImage = (string)obj[JsonKeys.ProfileImage];
//                            list.Add(user);
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.ShowFeedTagList(nfid, list);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessFeedTagList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }
//        private void ProcessDoingList() //273
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.DoingList] != null)
//                {
//                    JArray array = (JArray)_JobjFromResponse[JsonKeys.DoingList];
//                    List<DoingDTO> tmpList = new List<DoingDTO>();
//                    foreach (JObject obj in array)
//                    {
//                        if (obj[JsonKeys.Id] != null && obj[JsonKeys.Name] != null && obj[JsonKeys.Category] != null && obj[JsonKeys.Url] != null)
//                        {
//                            DoingDTO doing = new DoingDTO();
//                            doing.ID = (long)obj[JsonKeys.Id];
//                            doing.Name = HelperMethodsModel.FirstLetterToLower((string)obj[JsonKeys.Name]);
//                            doing.Category = (int)obj[JsonKeys.Category];
//                            doing.ImgUrl = (string)obj[JsonKeys.Url];
//                            if (obj[JsonKeys.DoingSort] != null) doing.SortId = (int)obj[JsonKeys.DoingSort];
//                            if (obj[JsonKeys.UpdateTime] != null)
//                            {
//                                doing.UpdateTime = (long)obj[JsonKeys.UpdateTime];
//                                if (doing.UpdateTime > DefaultSettings.VALUE_DOING_LIST_UT)
//                                {
//                                    DefaultSettings.VALUE_DOING_LIST_UT = doing.UpdateTime;
//                                    DatabaseActivityDAO.Instance.SaveDoingUpdateTimeIntoDB();
//                                }
//                            }
//                            lock (NewsFeedDictionaries.Instance.DOING_DICTIONARY)
//                            {
//                                if (NewsFeedDictionaries.Instance.DOING_DICTIONARY.ContainsKey(doing.ID))
//                                    NewsFeedDictionaries.Instance.DOING_DICTIONARY[doing.ID] = doing;
//                                else
//                                    NewsFeedDictionaries.Instance.DOING_DICTIONARY.Add(doing.ID, doing);
//                            }
//                            tmpList.Add(doing);
//                            HelperMethodsAuth.NewsFeedHandlerInstance.EnqueueDoingDTOforImageDownload(doing);
//                        }
//                    }
//                    new InsertIntoDoingTable(tmpList);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessDoingList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }


//        private void ProcessHashtagMediaContents()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.HashTagId] != null)
//                {
//                    MediaItemsSequenceCount++;
//                    string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
//                    int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
//                    long hashtagId = (long)_JobjFromResponse[JsonKeys.HashTagId];
//                    if (!NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.ContainsKey(hashtagId)) NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.Add(hashtagId, new List<SingleMediaDTO>());
//                    List<SingleMediaDTO> list = NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS[hashtagId];
//                    if (_JobjFromResponse[JsonKeys.MediaList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
//                        foreach (JObject ob in jarray)
//                        {
//                            SingleMediaDTO mediaDTO = HelperMethodsModel.BindSingleMediaDTO(ob);
//                            list.Add(mediaDTO);
//                        }
//                    }
//                    if (seqTotal == MediaItemsSequenceCount)
//                    {
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowHashTagItems(hashtagId, list);
//                        NewsFeedDictionaries.Instance.TEMP_SINGLE_ALBUM_CONTENTS.Clear();
//                        MediaItemsSequenceCount = 0;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessHashtagMediaContents ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }

//        private void ProcessMediaFullSearch()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestionType] != null)
//                {
//                    int suggestionType = (int)_JobjFromResponse[JsonKeys.MediaSuggestionType];
//                    string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
//                    if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
//                        if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_SONGS)
//                        {
//                            List<SingleMediaDTO> list = new List<SingleMediaDTO>();
//                            foreach (JObject singleObj in jarray)
//                            {
//                                SingleMediaDTO dto = HelperMethodsModel.BindSingleMediaDTO(singleObj);
//                                list.Add(dto);
//                            }
//                            HelperMethodsAuth.AuthHandlerInstance.ShowMediaResultsInFullSearch(searchParam, list);
//                        }
//                        else if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_ALBUMS)
//                        {
//                            List<MediaContentDTO> list = new List<MediaContentDTO>();
//                            foreach (JObject singleObj in jarray)
//                            {
//                                MediaContentDTO dto = new MediaContentDTO();
//                                if (singleObj[JsonKeys.UserTableId] != null) dto.UserTableId = (long)singleObj[JsonKeys.UserTableId];
//                                if (singleObj[JsonKeys.AlbumName] != null) dto.AlbumName = (string)singleObj[JsonKeys.AlbumName];
//                                if (singleObj[JsonKeys.AlbumId] != null) dto.AlbumId = (long)singleObj[JsonKeys.AlbumId];
//                                if (singleObj[JsonKeys.MediaAlbumImageURL] != null) dto.AlbumImageUrl = (string)singleObj[JsonKeys.MediaAlbumImageURL];
//                                if (singleObj[JsonKeys.MediaType] != null) dto.MediaType = (int)singleObj[JsonKeys.MediaType];
//                                if (singleObj[JsonKeys.MediaItemsCount] != null) dto.TotalMediaCount = (int)singleObj[JsonKeys.MediaItemsCount];
//                                list.Add(dto);
//                            }
//                            HelperMethodsAuth.AuthHandlerInstance.ShowAlbumResultsInFullSearch(searchParam, list);
//                        }
//                        else if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_TAGS)
//                        {
//                            List<HashTagDTO> list = new List<HashTagDTO>();
//                            foreach (JObject singleObj in jarray)
//                            {
//                                HashTagDTO dto = new HashTagDTO();
//                                if (singleObj[JsonKeys.MediaItemsCount] != null) dto.NoOfItems = (int)singleObj[JsonKeys.MediaItemsCount];
//                                if (singleObj[JsonKeys.HashTagId] != null) dto.CategoryId = (long)singleObj[JsonKeys.HashTagId];
//                                if (singleObj[JsonKeys.HashTagName] != null) dto.HashTagSearchKey = (string)singleObj[JsonKeys.HashTagName];
//                                list.Add(dto);
//                            }
//                            HelperMethodsAuth.AuthHandlerInstance.ShowHashTagResultsInFullSearch(searchParam, list);
//                        }
//                    }
//                    else
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.NoDataInFullMediaSearch(searchParam, suggestionType);
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessMediaSearchSuggestions()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
//                {
//                    string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
//                    List<SearchMediaDTO> searchListOfaSearchParam = null;
//                    lock (MediaDictionaries.Instance.TEMP_SEARCH_SGTNS)
//                    {
//                        if (!MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.TryGetValue(searchParam, out searchListOfaSearchParam))
//                        {
//                            searchListOfaSearchParam = new List<SearchMediaDTO>();
//                            MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.Add(searchParam, searchListOfaSearchParam);
//                        }
//                    }
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
//                    List<SearchMediaDTO> lst = new List<SearchMediaDTO>();
//                    foreach (JObject singleObj in jarray)
//                    {
//                        SearchMediaDTO dto = new SearchMediaDTO();
//                        dto.SearchSuggestion = (string)singleObj[JsonKeys.MediaSearchKey];
//                        dto.SearchType = (int)singleObj[JsonKeys.MediaSuggestionType];
//                        lst.Add(dto);
//                        searchListOfaSearchParam.Add(dto);
//                    }
//                    if (lst.Count > 0)
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadMediaSearchs(searchParam, lst);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessMediaSearchTrends()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
//                {
//                    DefaultSettings.SearchTrendsSeq++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    int seqTotal = Convert.ToInt32(str.Split(delimiterChars)[1]);
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
//                    foreach (JObject singleObj in jarray)
//                    {
//                        SearchMediaDTO dto = new SearchMediaDTO();
//                        dto.SearchSuggestion = (string)singleObj[JsonKeys.MediaSearchKey];
//                        dto.SearchType = (int)singleObj[JsonKeys.MediaSuggestionType];
//                        lock (MediaDictionaries.Instance.MEDIA_TRENDS)
//                        {
//                            MediaDictionaries.Instance.MEDIA_TRENDS.Add(dto);
//                        }
//                    }
//                    //log.Info("SearchTrends ." + DefaultSettings.SearchTrendsSeq + "SEQTOTAL.." + seqTotal);
//                    if (DefaultSettings.SearchTrendsSeq == seqTotal)
//                    {
//                        lock (MediaDictionaries.Instance.MEDIA_TRENDS)
//                        {
//                            MediaDictionaries.Instance.MEDIA_TRENDS.Sort((x, y) => y.SearchSuggestion.Length.CompareTo(x.SearchSuggestion.Length));
//                        }
//                        DefaultSettings.SearchTrendsSeq = 0;
//                        HelperMethodsAuth.NewsFeedHandlerInstance.LoadMediaSearchTrends();
//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessMediaSearchTrends ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//<<<<<<< .mine
//        private void ProcessSingleFeedDetails()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
//                        foreach (JObject singleObj in jarray)
//                        {
//                            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
//                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                            {
//                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                            }
//=======
//                    if (profileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
//                    {
//                        List<NewsPortalDTO> list = new List<NewsPortalDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            NewsPortalDTO dto = new NewsPortalDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.PortalName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                            {
//                                dto.PortalId = (long)singleObj[JsonKeys.UserTableId];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PortalSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PortalCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PortalCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            NewsPortalDictionaries.Instance.UTID_NEWSPORTALINFO_DICTIONARY[dto.PortalId] = dto;
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                    else if (profileType == SettingsConstants.PROFILE_TYPE_PAGES)
//                    {
//                        List<PageInfoDTO> list = new List<PageInfoDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            PageInfoDTO dto = new PageInfoDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.PageName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                            {
//                                dto.PageId = (long)singleObj[JsonKeys.UserTableId];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PageSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PageCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PageCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY[dto.PageId] = dto;
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                    else if (profileType == SettingsConstants.PROFILE_TYPE_MUSICPAGE)
//                    {
//                        List<MusicPageDTO> list = new List<MusicPageDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            MusicPageDTO dto = new MusicPageDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.FullName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PageSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PageCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PageCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserIdentity = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsPortalCategoriesList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsPortalUtid] != null
//                    && _JobjFromResponse[JsonKeys.NewsPortalCategoryList] != null && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    long utId = (long)_JobjFromResponse[JsonKeys.NewsPortalUtid];
//                    List<NewsCategoryDTO> list = new List<NewsCategoryDTO>();
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsPortalCategoryList];
//                    foreach (JObject singleObj in jarray)
//                    {
//                        NewsCategoryDTO dto = new NewsCategoryDTO();
//                        if (singleObj[JsonKeys.NewsPortalCategoryName] != null)
//                        {
//                            dto.CategoryName = (string)singleObj[JsonKeys.NewsPortalCategoryName];
//                        }
//                        if (singleObj[JsonKeys.Id] != null)
//                        {
//                            dto.CategoryId = (int)singleObj[JsonKeys.Id];
//                        }
//                        if (singleObj[JsonKeys.Type] != null)
//                        {
//                            dto.CategoryType = (int)singleObj[JsonKeys.Type];
//                        }
//                        if (singleObj[JsonKeys.Subscribe] != null)
//                        {
//                            dto.Subscribed = (bool)singleObj[JsonKeys.Subscribe];
//                        }
//                        list.Add(dto);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.LoadNewsPortalCategories(utId, list);
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//>>>>>>> .r1628

//                            if (feed.ParentFeed != null)
//                            {
//                                //FeedDTO whoShareFeed = feed.WhoShare;
//                                //whoShareFeed.ParentFeed = feed;

//                                lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
//                                {
//                                    NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[feed.NewsfeedId] = feed.ParentFeed.NewsfeedId;
//                                }

//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.ParentFeed.NewsfeedId] = feed.ParentFeed;
//                                }
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.ParentFeed.NewsfeedId, feed.ParentFeed.Time, feed.ParentFeed.FeedCategory);

//                                }
//                                //HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(whoShareFeed, DefaultSettings.FEED_TYPE_ALL);
//                            }
//                            else
//                            {
//                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                                {
//                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                                }
//                                //HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(feed, DefaultSettings.FEED_TYPE_ALL);
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.UpdateFeedDetails(feed);
//                        }

//                    }
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessSingleFeedDetails ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessAddStatus()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    FeedDTO feed = HelperMethodsModel.BindFeedDetails(_JobjFromResponse);
//                    if ((feed.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO || feed.BookPostType == SettingsConstants.NEWS_FEED_TYPE_VIDEO) && feed.MediaContent == null)
//                        return;
//                    if (feed.MediaContent != null)
//                    {
//                        if (feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
//                        {
//                            HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
//                        }
//                        if (feed.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO)
//                        {
//                            if (!NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_USER_TABLE_ID))
//                            {
//                                NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.Add(DefaultSettings.LOGIN_USER_TABLE_ID, new Dictionary<long, MediaContentDTO>());
//                            }
//                            if (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID].ContainsKey(feed.MediaContent.AlbumId))
//                            {
//                                NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].TotalMediaCount += feed.MediaContent.MediaList.Count;
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID].Add(feed.MediaContent.AlbumId, new MediaContentDTO { AlbumId = feed.MediaContent.AlbumId, AlbumName = feed.MediaContent.AlbumName, MediaType = 1, TotalMediaCount = feed.MediaContent.MediaList.Count });
//                            }
//                            if (feed.MediaContent.MediaList != null)
//                            {
//                                foreach (SingleMediaDTO singleDto in feed.MediaContent.MediaList)
//                                {
//                                    if (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList == null)
//                                    {
//                                        NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList = new List<SingleMediaDTO>();
//                                    }
//                                    lock (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID)
//                                    {
//                                        if (!NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList.Any(P => P.ContentId == singleDto.ContentId))
//                                        {
//                                            NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList.Insert(0, singleDto);
//                                        }
//                                    }
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.AddOrUpdateMyAlbumModel(1, feed.MediaContent);
//                        }
//                        else if (feed.BookPostType == SettingsConstants.NEWS_FEED_TYPE_VIDEO)
//                        {
//                            if (!NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_USER_TABLE_ID))
//                            {
//                                NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.Add(DefaultSettings.LOGIN_USER_TABLE_ID, new Dictionary<long, MediaContentDTO>());
//                            }
//                            if (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID].ContainsKey(feed.MediaContent.AlbumId))
//                            {
//                                NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].TotalMediaCount += feed.MediaContent.MediaList.Count;
//                            }
//                            else
//                            {
//                                NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID].Add(feed.MediaContent.AlbumId, new MediaContentDTO { AlbumId = feed.MediaContent.AlbumId, AlbumName = feed.MediaContent.AlbumName, MediaType = 2, TotalMediaCount = feed.MediaContent.MediaList.Count });
//                            }
//                            if (feed.MediaContent.MediaList != null)
//                            {
//                                foreach (SingleMediaDTO singleDto in feed.MediaContent.MediaList)
//                                {
//                                    if (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList == null)
//                                    {
//                                        NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList = new List<SingleMediaDTO>();
//                                    }
//                                    lock (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID)
//                                    {
//                                        if (!NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList.Any(P => P.ContentId == singleDto.ContentId))
//                                        {
//                                            NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID][feed.MediaContent.AlbumId].MediaList.Insert(0, singleDto);
//                                        }
//                                    }
//                                }
//                            }
//                            HelperMethodsAuth.NewsFeedHandlerInstance.AddOrUpdateMyAlbumModel(2, feed.MediaContent);
//                        }
//                    }
//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
//                    {
//                        if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(feed.NewsfeedId)) NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
//                        else NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Add(feed.NewsfeedId, feed);
//                    }

//                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                    {
//                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertData(feed.NewsfeedId, feed.Time, feed.FeedCategory);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, feed, DefaultSettings.FEED_TYPE_ALL, 0);
//                    lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                    {
//                        NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, feed, DefaultSettings.FEED_TYPE_MY, 0);

//                    if (feed.FriendId > 0) //in friend's wall post
//                    {

//                        if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(feed.FriendId))
//                        {
//                            lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[feed.FriendId].InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            }
//                        }
//                        else
//                        {
//                            lock (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(feed.FriendId, new CustomSortedList());
//                                NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[feed.FriendId].InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            }
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, feed, DefaultSettings.FEED_TYPE_FRIEND, feed.FriendId);
//                    }
//                    else if (feed.GroupId > 0) //in circle's wall post
//                    {

//                        if (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(feed.GroupId))
//                        {
//                            lock (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[feed.GroupId].InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            }
//                        }
//                        else
//                        {
//                            lock (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME)
//                            {
//                                NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(feed.GroupId, new CustomSortedList());
//                                NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[feed.GroupId].InsertData(feed.NewsfeedId, feed.ActualTime, feed.FeedCategory);
//                            }
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.InsertSingleFeedUI(2, feed, DefaultSettings.FEED_TYPE_CIRCLE, feed.GroupId);
//                    }

//                    if (feed.ImageList != null && feed.ImageList.Count > 0)
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.AddProfileCoverImageToUIList(feed.ImageList, SettingsConstants.TYPE_NORMAL_BOOK_IMAGE);
//                    }
//                }
//                else
//                {
//                    string mg = String.Empty;
//                    if (_JobjFromResponse[JsonKeys.Message] != null)
//                    {
//                        mg = (string)_JobjFromResponse[JsonKeys.Message];
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(mg, "Failed!");
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessAddStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessDiscoverNewsPortals()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.NewsPortalList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.SubscribeType] != null)// && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.Subscribe] != null)
//                {
//                    int profileType = (_JobjFromResponse[JsonKeys.ProfileType] != null) ? ((int)_JobjFromResponse[JsonKeys.ProfileType]) : SettingsConstants.PROFILE_TYPE_NEWSPORTAL;
//                    int subscType = (int)_JobjFromResponse[JsonKeys.SubscribeType];
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsPortalList];

//                    if (profileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
//                    {
//                        List<NewsPortalDTO> list = new List<NewsPortalDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            NewsPortalDTO dto = new NewsPortalDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.PortalName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                            {
//                                dto.PortalId = (long)singleObj[JsonKeys.UserTableId];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PortalSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PortalCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PortalCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            NewsPortalDictionaries.Instance.UTID_NEWSPORTALINFO_DICTIONARY[dto.PortalId] = dto;
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                    else if (profileType == SettingsConstants.PROFILE_TYPE_PAGES)
//                    {
//                        List<PageInfoDTO> list = new List<PageInfoDTO>();
//                        foreach (JObject singleObj in jarray)
//                        {
//                            PageInfoDTO dto = new PageInfoDTO();
//                            if (singleObj[JsonKeys.FullName] != null)
//                            {
//                                dto.PageName = (string)singleObj[JsonKeys.FullName];
//                            }
//                            if (singleObj[JsonKeys.UserTableId] != null)
//                            {
//                                dto.PageId = (long)singleObj[JsonKeys.UserTableId];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.PageSlogan = (string)singleObj[JsonKeys.NewsPortalSlogan];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalSlogan] != null)
//                            {
//                                dto.IsSubscribed = (bool)singleObj[JsonKeys.Subscribe];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatName] != null)
//                            {
//                                dto.PageCatName = (string)singleObj[JsonKeys.NewsPortalCatName];
//                            }
//                            if (singleObj[JsonKeys.NewsPortalCatId] != null)
//                            {
//                                dto.PageCatId = (long)singleObj[JsonKeys.NewsPortalCatId];
//                            }
//                            if (singleObj[JsonKeys.SubscriberCount] != null)
//                            {
//                                dto.SubscriberCount = (int)singleObj[JsonKeys.SubscriberCount];
//                            }
//                            if (singleObj[JsonKeys.ProfileImage] != null)
//                            {
//                                dto.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
//                            }
//                            if (singleObj[JsonKeys.UserIdentity] != null)
//                            {
//                                dto.UserId = (long)singleObj[JsonKeys.UserIdentity];
//                            }
//                            NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY[dto.PageId] = dto;
//                            list.Add(dto);
//                        }
//                        HelperMethodsAuth.NewsFeedHandlerInstance.ShowFollowingOrDiscoverPortals(subscType, list);
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessNewsPortalCategoriesList()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsPortalUtid] != null
//                    && _JobjFromResponse[JsonKeys.NewsPortalCategoryList] != null && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    long utId = (long)_JobjFromResponse[JsonKeys.NewsPortalUtid];
//                    List<NewsCategoryDTO> list = new List<NewsCategoryDTO>();
//                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsPortalCategoryList];
//                    foreach (JObject singleObj in jarray)
//                    {
//                        NewsCategoryDTO dto = new NewsCategoryDTO();
//                        if (singleObj[JsonKeys.NewsPortalCategoryName] != null)
//                        {
//                            dto.CategoryName = (string)singleObj[JsonKeys.NewsPortalCategoryName];
//                        }
//                        if (singleObj[JsonKeys.Id] != null)
//                        {
//                            dto.CategoryId = (int)singleObj[JsonKeys.Id];
//                        }
//                        if (singleObj[JsonKeys.Type] != null)
//                        {
//                            dto.CategoryType = (int)singleObj[JsonKeys.Type];
//                        }
//                        if (singleObj[JsonKeys.Subscribe] != null)
//                        {
//                            dto.Subscribed = (bool)singleObj[JsonKeys.Subscribe];
//                        }
//                        list.Add(dto);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.LoadNewsPortalCategories(utId, list);
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }

//        //private void ProcessMediaContentDetails()
//        //{
//        //    try
//        //    {
//        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentDTO] != null
//        //            && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.UserTableId] != null)
//        //        {

//        //            if (_JobjFromResponse[JsonKeys.ReasonCode] != null && ReasonCodeConstants.REASON_CODE_NOT_FOUND == (long)_JobjFromResponse[JsonKeys.ReasonCode])
//        //            {
//        //                HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(ReasonCodeConstants.REASON_MSG_NOT_FOUND, "");
//        //            }
//        //            else
//        //            {
//        //                long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//        //                long utId = (long)_JobjFromResponse[JsonKeys.UserTableId];
//        //                //long uId = (utId == DefaultSettings.LOGIN_USER_TABLE_ID) ? DefaultSettings.LOGIN_USER_ID : FriendDictionaries.Instance.UTID_UID_DICTIONARY[utId];
//        //                JObject mediaContent = (JObject)_JobjFromResponse[JsonKeys.MediaContentDTO];
//        //                SingleMediaDTO singleItemDetails = HelperMethodsModel.BindSingleMediaDTO(mediaContent);
//        //                singleItemDetails.UserTableId = utId;
//        //                long uid = 0; bool NonFriendMedia = true;
//        //                if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.TryGetValue(utId, out uid))
//        //                {
//        //                    UserBasicInfoDTO dto = null;
//        //                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(uid, out dto) && dto.FriendShipStatus != 0)
//        //                    {
//        //                        NonFriendMedia = false;
//        //                        Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (singleItemDetails.MediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
//        //                        Dictionary<long, MediaContentDTO> albumsOfaUtId = null;
//        //                        if (!DICT_BY_UTID.TryGetValue(utId, out albumsOfaUtId)) { albumsOfaUtId = new Dictionary<long, MediaContentDTO>(); DICT_BY_UTID.Add(utId, albumsOfaUtId); }
//        //                        MediaContentDTO album = null;
//        //                        if (!albumsOfaUtId.TryGetValue(singleItemDetails.AlbumId, out album)) { album = new MediaContentDTO { AlbumId = singleItemDetails.AlbumId, MediaType = singleItemDetails.MediaType }; albumsOfaUtId.Add(album.AlbumId, album); }
//        //                        if (album.MediaList == null) album.MediaList = new List<SingleMediaDTO>();
//        //                        if (!album.MediaList.Any(P => P.ContentId == contentId))
//        //                            album.MediaList.Add(singleItemDetails);
//        //                    }
//        //                }
//        //                HelperMethodsAuth.NewsFeedHandlerInstance.MediaContentDetails(singleItemDetails, NonFriendMedia);
//        //            }
//        //        }
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        log.Error("ProcessMediaContentDetails ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//        //    }
//        //}

//        private void ProcessUpdateLikeUnlikeCommentMedia()
//        {
//            //Received for action=468 from Auth JSON==> {"uId":"2110010086","sucs":true,"fn":"sirat samyoun","id":120,"cmnId":625,"tm":1449402328539,"cntntId":1875,"lkd":1,"loc":1,"mdaT":2}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.CommentId] != null
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.Liked] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    int liked = (int)_JobjFromResponse[JsonKeys.Liked];
//                    Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem)
//                        && CommentsOfThisItem.ContainsKey(commentId))
//                    {
//                        if (liked == 1)
//                            CommentsOfThisItem[commentId].TotalLikeComment = CommentsOfThisItem[commentId].TotalLikeComment + 1;
//                        else if (CommentsOfThisItem[commentId].TotalLikeComment > 0)
//                            CommentsOfThisItem[commentId].TotalLikeComment = CommentsOfThisItem[commentId].TotalLikeComment - 1;
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateLikeUnlikeCommentMedia(liked, contentId, commentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeUnlikeMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateDeleteCommentMedia()
//        {
//            //Received update for action 467-->{"sucs":true,"cmnId":1,"uId":"2110010002","fn":"Ashraful ( এইটা আসল )","sc":false,"loc":4,"cntntId":1}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem)
//                        && CommentsOfThisItem.ContainsKey(commentId))
//                    {
//                        CommentsOfThisItem.Remove(commentId);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.DeleteMediaComment(contentId, commentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateDeleteCommentMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateEditCommentMedia()
//        {
//            //Received update for action 466-->{"sucs":true,"cmnId":2,"uId":"2110010002","cmn":"Edited comment 927 by ashraful","fn":"Ashraful ( এইটা আসল )","sc":false,"cntntId":1}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ShowContinue] != null
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.Comment] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
//                {
//                    long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    bool showContinue = (bool)_JobjFromResponse[JsonKeys.ShowContinue];
//                    string comment = (string)_JobjFromResponse[JsonKeys.Comment];
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
//                    Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem)
//                        && CommentsOfThisItem.ContainsKey(commentId))
//                    {
//                        CommentsOfThisItem[commentId].Comment = comment;
//                        CommentsOfThisItem[commentId].ShowContinue = showContinue;
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.EditMediaComment(comment, showContinue, commentId, contentId, nfId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateEditCommentMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateCommentMedia()
//        {
//            //Received for action=465 from Auth JSON==> {"sucs":true,"cmnId":622,"tm":1449379711978,"uId":"2110010086","cmn":"1","fn":"sirat samyoun","sc":false,"loc":1,"cntntId":1894,"mdaT":2}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaType] != null
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.UserIdentity] != null)
//                {
//                    CommentDTO comment = HelperMethodsModel.BindCommentDetails(_JobjFromResponse);
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
//                    long uid = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    Dictionary<long, CommentDTO> CommentsOfThisItem = null;
//                    if (!NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem))
//                    {
//                        CommentsOfThisItem = new Dictionary<long, CommentDTO>();
//                        NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.Add(contentId, CommentsOfThisItem);
//                    }
//                    lock (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST)
//                    {
//                        if (CommentsOfThisItem.ContainsKey(comment.CommentId)) CommentsOfThisItem[comment.CommentId] = comment;
//                        else CommentsOfThisItem.Add(comment.CommentId, comment);
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.AddMediaComment(comment, contentId, nfid, true);  //UpdateCommentMedia(comment, contentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateCommentMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateLikeUnlikeMedia()
//        {
//            //Received for action=464 from Auth JSON==> {"uId":"2110010086","sucs":true,"fn":"sirat samyoun","cntntId":1894,"lkd":1,"loc":1,"mdaT":2}
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaType] != null
//                    && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.Liked] != null && _JobjFromResponse[JsonKeys.UserIdentity] != null)
//                {
//                    long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
//                    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
//                    int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
//                    int liked = (int)_JobjFromResponse[JsonKeys.Liked];
//                    int loc = (int)_JobjFromResponse[JsonKeys.LikeOrComment];
//                    long uid = (long)_JobjFromResponse[JsonKeys.UserIdentity];
//                    long utId = 0;
//                    int len = FriendDictionaries.Instance.UTID_UID_DICTIONARY.Count;
//                    for (int i = 0; i < len; i++)
//                    {
//                        if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.ElementAt(i).Value == uid)
//                        {
//                            utId = FriendDictionaries.Instance.UTID_UID_DICTIONARY.ElementAt(i).Key;
//                            break;
//                        }
//                    }
//                    if (utId > 0)
//                    {
//                        Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
//                        Dictionary<long, MediaContentDTO> albumsOfaUtId = null;
//                        if (DICT_BY_UTID.TryGetValue(uid, out albumsOfaUtId))
//                        {
//                            for (int i = 0; i < albumsOfaUtId.Count; i++)
//                            {
//                                if (albumsOfaUtId[i].MediaList != null && albumsOfaUtId[i].MediaList.Count > 0)
//                                {
//                                    SingleMediaDTO singleMedia = albumsOfaUtId[i].MediaList.Where(P => P.ContentId == contentId).FirstOrDefault();
//                                    if (singleMedia != null)
//                                    {
//                                        if (liked == 1) singleMedia.LikeCount++;
//                                        else if (singleMedia.LikeCount > 0)
//                                            singleMedia.LikeCount--;
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    HelperMethodsAuth.NewsFeedHandlerInstance.UpdateLikeUnlikeMedia(loc, liked, contentId, nfid);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateLikeUnlikeMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessHashtagSuggestion() //280
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
//                {
//                    JArray array = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];

//                    foreach (JObject obj in array)
//                    {
//                        if (obj[JsonKeys.MediaSearchKey] != null && obj[JsonKeys.MediaSuggestionType] != null && obj[JsonKeys.MediaHashTagId] != null)
//                        {
//                            HashTagDTO dto = new HashTagDTO();
//                            dto.CategoryId = (long)obj[JsonKeys.MediaHashTagId];
//                            dto.HashTagSearchKey = (string)obj[JsonKeys.MediaSearchKey];
//                            //dto.SuggestionType = (int)obj[JsonKeys.MediaSuggestionType];

//                            lock (MediaDictionaries.Instance.HASHTAG_SUGGESTIONS)
//                            {
//                                MediaDictionaries.Instance.HASHTAG_SUGGESTIONS[dto.CategoryId] = dto;
//                            }
//                        }
//                    }

//                    HelperMethodsAuth.AuthHandlerInstance.StartHashTagSuggestionThread();
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessHashtagSuggestion ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }

//        }

//        private void ProcessSpamReasonList() //1001
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Sequence] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    SpamReasonListSeqCount++;
//                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    char[] delimiterChars = { '/' };
//                    str = str.Split(delimiterChars)[1];
//                    int seqTotal = Convert.ToInt32(str);

//                    if (_JobjFromResponse[JsonKeys.SpamReasonList] != null)
//                    {
//                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.SpamReasonList];


//                        foreach (JObject singleObj in jarray)
//                        {
//                            int reasonId = 0;
//                            string reason = string.Empty;

//                            if (singleObj[JsonKeys.Id] != null)
//                            {
//                                reasonId = (int)singleObj[JsonKeys.Id];
//                            }

//                            if (singleObj[JsonKeys.SpamReason] != null)
//                            {
//                                reason = (string)singleObj[JsonKeys.SpamReason];
//                            }

//                            if (reasonId > 0 && !string.IsNullOrEmpty(reason))
//                                NewsFeedDictionaries.Instance.TEMP_SPAM_REASON_LIST[reasonId] = reason;
//                        }

//                    }
//                    if (SpamReasonListSeqCount == seqTotal)
//                    {
//                        SpamReasonListSeqCount = 0;
//                        HelperMethodsAuth.NewsFeedHandlerInstance.OpenSpamReasonListPopUp();
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("ProcessSpamReasonList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }

//        #endregion

//        #region "Call & Chat"
//        private void ProcessUpdateSendRegister()
//        {

//            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//            {

//                try
//                {
//                    CallerDTO caller = new CallerDTO();
//                    HelperMethodsAuth.CallAuthResult_174_374(_JobjFromResponse, caller);
//                    HelperMethodsAuth.AuthHandlerInstance.ProcessUpdateSendRegister(caller);
//                }
//                catch (Exception e)
//                {

//                    log.Error("ProcessUpdateSendRegister ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//                }
//            }
//        }

//        private void ProcessUpdateStartFriendChat()//175-375
//        {
//            try
//            {
//                if (_JobjFromResponse != null)
//                {
//                    if (action == AppConstants.TYPE_START_FRIEND_CHAT && server_packet_id == 0)
//                        return;

//                    ChatInitiatorDTO initiatorDTO = new ChatInitiatorDTO();
//                    initiatorDTO.FriendIdentity = _JobjFromResponse[JsonKeys.FriendId] != null ? ModelUtility.ConvertToLong(_JobjFromResponse[JsonKeys.FriendId]) : 0;
//                    initiatorDTO.Presence = _JobjFromResponse[JsonKeys.Presence] != null ? (int)_JobjFromResponse[JsonKeys.Presence] : 0;
//                    initiatorDTO.Mood = _JobjFromResponse[JsonKeys.Mood] != null ? (int)_JobjFromResponse[JsonKeys.Mood] : 0;
//                    initiatorDTO.LastOnlineTime = _JobjFromResponse[JsonKeys.LastOnlineTime] != null ? (long)_JobjFromResponse[JsonKeys.LastOnlineTime] : 0;
//                    initiatorDTO.ReasonCode = _JobjFromResponse[JsonKeys.ReasonCode] != null ? (int)_JobjFromResponse[JsonKeys.ReasonCode] : 0;
//                    initiatorDTO.ChatServerIP = (string)_JobjFromResponse[JsonKeys.ChatServerIp];
//                    initiatorDTO.ChatRegisterPort = _JobjFromResponse[JsonKeys.ChatRegistrationPort] != null ? (int)_JobjFromResponse[JsonKeys.ChatRegistrationPort] : 0;
//                    initiatorDTO.Device = _JobjFromResponse[JsonKeys.Device] != null ? (int)_JobjFromResponse[JsonKeys.Device] : 0;
//                    initiatorDTO.DeviceToken = (string)_JobjFromResponse[JsonKeys.DeviceToken];
//                    initiatorDTO.FullName = (string)_JobjFromResponse[JsonKeys.Name];
//                    initiatorDTO.ApplicationType = _JobjFromResponse[JsonKeys.AppType] != null ? (int)_JobjFromResponse[JsonKeys.AppType] : 0;
//                    initiatorDTO.RemotePushType = _JobjFromResponse[JsonKeys.RemotePushType] != null ? (int)_JobjFromResponse[JsonKeys.RemotePushType] : 1;
//                    initiatorDTO.ServerPacketID = server_packet_id;
//                    initiatorDTO.ActionType = AppConstants.TYPE_UPDATE_START_FRIEND_CHAT;

//                    HelperMethodsAuth.AuthHandlerInstance.ProcessStartFriendChat(initiatorDTO);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateStartFriendChat ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateStartGroupChat()//134-334-335
//        {
//            try
//            {
//                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    if (action == AppConstants.TYPE_START_GROUP_CHAT && server_packet_id == 0)
//                        return;

//                    ChatInitiatorDTO initiatorDTO = new ChatInitiatorDTO();
//                    initiatorDTO.ActionType = action == AppConstants.TYPE_START_GROUP_CHAT ? AppConstants.TYPE_UPDATE_START_GROUP_CHAT : action;
//                    initiatorDTO.ReasonCode = _JobjFromResponse[JsonKeys.ReasonCode] != null ? (int)_JobjFromResponse[JsonKeys.ReasonCode] : 0;
//                    initiatorDTO.ChatServerIP = (string)_JobjFromResponse[JsonKeys.ChatServerIp];
//                    initiatorDTO.ChatRegisterPort = _JobjFromResponse[JsonKeys.ChatRegistrationPort] != null ? (int)_JobjFromResponse[JsonKeys.ChatRegistrationPort] : 0;
//                    initiatorDTO.GroupID = _JobjFromResponse[JsonKeys.TagId] != null ? (long)_JobjFromResponse[JsonKeys.TagId] : 0;
//                    initiatorDTO.ServerPacketID = server_packet_id;

//                    HelperMethodsAuth.AuthHandlerInstance.ProcessStartGroupChat(initiatorDTO);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("ProcessUpdateStartGroupChat ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessUpdateAddGroupMember() //335
//        {
//            ProcessUpdateStartGroupChat();
//        }

//        #endregion "Call & Chat"

//        #region Notification

//        private void ProcessMyNotifications()
//        {
//            List<NotificationDTO> notificationList = new List<NotificationDTO>();
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//                {
//                    int scl = _JobjFromResponse[JsonKeys.Scroll] != null ? (int)_JobjFromResponse[JsonKeys.Scroll] : 0;
//                    int totalNotificationCount = (_JobjFromResponse[JsonKeys.TotalNotificationCount] != null) ? (int)_JobjFromResponse[JsonKeys.TotalNotificationCount] : 0;
//                    if (_JobjFromResponse[JsonKeys.NotificationList] != null)
//                    {
//                        JArray notificationListJSON = (JArray)_JobjFromResponse[JsonKeys.NotificationList];
//                        foreach (JObject notificationJSON in notificationListJSON)
//                        {
//                            lock (notificationList)
//                            {
//                                NotificationDTO dto = HelperMethodsModel.BindNotificationInfo(notificationJSON);
//                                NotificationDTO existingDTO = NotificationDTO.FindExistingDTO(dto);
//                                //existing and new notification comes
//                                if (existingDTO != null && existingDTO.UpdateTime < dto.UpdateTime)
//                                {
//                                    if (!dto.PreviousIds.Contains(existingDTO.ID))
//                                    {
//                                        dto.PreviousIds.Add(existingDTO.ID);
//                                    }

//                                    dto.PreviousIds.AddRange(existingDTO.PreviousIds);
//                                    RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(existingDTO);
//                                    RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);
//                                    notificationList.Add(dto);
//                                    new DeleteFromNotificationHistoryTable(dto.PreviousIds);
//                                }
//                                //existing but old notification comes
//                                else if (existingDTO != null && existingDTO.UpdateTime > dto.UpdateTime)
//                                {
//                                    int idx = RingDictionaries.Instance.NOTIFICATION_LISTS.IndexOf(existingDTO);
//                                    if (!RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds.Contains(dto.ID))
//                                    {
//                                        RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds.Add(dto.ID);
//                                    }
//                                    new DeleteFromNotificationHistoryTable(RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds);
//                                }
//                                // fresh insert
//                                else
//                                {
//                                    RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);
//                                    notificationList.Add(dto);
//                                }

//                                if (dto.UpdateTime > AppConstants.NOTIFICATION_MAX_UT) { AppConstants.NOTIFICATION_MAX_UT = dto.UpdateTime; }
//                                else if (dto.UpdateTime < AppConstants.NOTIFICATION_MIN_UT || AppConstants.NOTIFICATION_MIN_UT == 0) { AppConstants.NOTIFICATION_MIN_UT = dto.UpdateTime; }
//                            }
//                        }
//                        if (scl == 1) HelperMethodsAuth.AuthHandlerInstance.ShowAllNotificationCount(totalNotificationCount);
//                        HelperMethodsAuth.AuthHandlerInstance.AddNotificationFromServer(notificationList, scl == 1 ? true : false);
//                        HelperMethodsAuth.AuthHandlerInstance.NotificationListCount();
//                    }
//                }
//                else
//                {
//                    if (_JobjFromResponse[JsonKeys.Scroll] != null && (short)_JobjFromResponse[JsonKeys.Scroll] == 2)
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.RemoveSeeMorePanel();
//                    }
//                    HelperMethodsAuth.AuthHandlerInstance.NotificationListCount();
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessMyNotifications ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessSingleNotification()
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Id] != null)
//                {
//                    List<NotificationDTO> list = new List<NotificationDTO>();
//                    NotificationDTO dto = HelperMethodsModel.BindNotificationInfo(_JobjFromResponse);

//                    bool increase = HelperMethodsAuth.AuthHandlerInstance.CheckNotificationCount(dto);

//                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
//                    {
//                        NotificationDTO existingDTO = NotificationDTO.FindExistingDTO(dto);
//                        //existing and new notification comes
//                        if (existingDTO != null && existingDTO.UpdateTime < dto.UpdateTime)
//                        {
//                            dto.PreviousIds.AddRange(existingDTO.PreviousIds);
//                            dto.PreviousIds.Add(existingDTO.ID);

//                            RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(existingDTO);
//                            RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);

//                            if (!list.Contains(dto))
//                            {
//                                list.Add(dto);
//                            }

//                            new DeleteFromNotificationHistoryTable(dto.PreviousIds);
//                        }
//                        //existing but old notification comes
//                        else if (existingDTO != null && existingDTO.UpdateTime > dto.UpdateTime)
//                        {
//                            int idx = RingDictionaries.Instance.NOTIFICATION_LISTS.IndexOf(existingDTO);

//                            if (!existingDTO.PreviousIds.Contains(dto.ID))
//                            {
//                                RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds.Add(dto.ID);
//                            }
//                            new DeleteFromNotificationHistoryTable(RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds);
//                        }
//                        else if (existingDTO != null && existingDTO.UpdateTime == dto.UpdateTime && existingDTO.ID == dto.ID)
//                        {
//                            //do nothing....omitting duplicate packets
//                        }
//                        // fresh insert
//                        else
//                        {
//                            RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);
//                            if (!list.Contains(dto))
//                            {
//                                list.Add(dto);
//                            }
//                        }
//                    }

//                    if (dto.UpdateTime > AppConstants.NOTIFICATION_MAX_UT) { AppConstants.NOTIFICATION_MAX_UT = dto.UpdateTime; }

//                    else if (dto.UpdateTime < AppConstants.NOTIFICATION_MIN_UT || AppConstants.NOTIFICATION_MIN_UT == 0) { AppConstants.NOTIFICATION_MIN_UT = dto.UpdateTime; }


//                    //list.Add(dto);

//                    HelperMethodsAuth.AuthHandlerInstance.IncreaseAllnotificationCount(increase);
//                    HelperMethodsAuth.AuthHandlerInstance.AddNotificationFromServer(list, true);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessSingleNotification ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        #endregion

//        #region Unnecessary
//        //private void ProcessUpdateChangeFriendAccess()
//        //{
//        //    try
//        //    {
//        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//        //        {
//        //            UserBasicInfoDTO basicInfo = null;
//        //            int prevContactType;

//        //            foreach (KeyValuePair<long, UserBasicInfoDTO> entry in FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ToArray())
//        //            {
//        //                UserBasicInfoDTO temp = entry.Value;
//        //                if (temp.UserTableId == (long)_JobjFromResponse[JsonKeys.UserTableId])
//        //                {
//        //                    basicInfo = temp;
//        //                    break;
//        //                }
//        //            }

//        //            if (basicInfo != null)
//        //            {
//        //                prevContactType = basicInfo.ContactType;
//        //                bool isDownGradeChange = (int)_JobjFromResponse[JsonKeys.ContactType] == StatusConstants.ACCESS_CHAT_CALL && basicInfo.ContactType == StatusConstants.ACCESS_FULL;
//        //                if (isDownGradeChange)
//        //                {
//        //                    basicInfo.IncomingNotification = false;
//        //                    basicInfo.ContactType = (int)_JobjFromResponse[JsonKeys.ContactType];
//        //                    basicInfo.NewContactType = 0;
//        //                    basicInfo.IsChangeRequester = false;
//        //                }
//        //                else
//        //                {
//        //                    basicInfo.IncomingNotification = true;
//        //                    basicInfo.NewContactType = (int)_JobjFromResponse[JsonKeys.ContactType];
//        //                    basicInfo.IsChangeRequester = false;
//        //                }

//        //                List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
//        //                contactList.Add(basicInfo);
//        //                new InsertIntoContactListTable(contactList);

//        //                HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(basicInfo);
//        //            }

//        //        }
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        System.Diagnostics.Debug.WriteLine("Exception in ProcessUpdateChangeFriendAccess==>" + e.Message + "\n" + e.StackTrace);
//        //    }
//        //}

//        //private void ProcessUpdateAcceptFriendAccess()
//        //{
//        //    try
//        //    {

//        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
//        //        {
//        //            UserBasicInfoDTO basicInfo = null;
//        //            int prevContactType;

//        //            foreach (KeyValuePair<long, UserBasicInfoDTO> entry in FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ToArray())
//        //            {
//        //                UserBasicInfoDTO temp = entry.Value;
//        //                if (temp.UserTableId == (long)_JobjFromResponse[JsonKeys.UserTableId])
//        //                {
//        //                    basicInfo = temp;
//        //                    break;
//        //                }
//        //            }

//        //            if (basicInfo != null)
//        //            {
//        //                prevContactType = basicInfo.ContactType;
//        //                if ((bool)_JobjFromResponse[JsonKeys.Accept])
//        //                {
//        //                    basicInfo.ContactType = (int)_JobjFromResponse[JsonKeys.ContactType];
//        //                }
//        //                basicInfo.NewContactType = 0;
//        //                basicInfo.IsChangeRequester = false;

//        //                List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
//        //                contactList.Add(basicInfo);
//        //                new InsertIntoContactListTable(contactList);

//        //                HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(basicInfo);
//        //            }

//        //        }
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        System.Diagnostics.Debug.WriteLine("Exception in ProcessUpdateAcceptFriendAccess==>" + e.Message + "\n" + e.StackTrace);
//        //    }
//        //} 
//        #endregion

//        #region ImageAlbum

//        private void ProcessMyAlbumImages()
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
//                    int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
//                    string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];

//                    Dictionary<string, Dictionary<long, ImageDTO>> TEMP_DICTIONARY = null;

//                    if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_MY_PROFILE_IMAGES;
//                    }
//                    else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_MY_COVER_IMAGES;
//                    }
//                    else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_MY_FEED_IMAGES;
//                    }



//                    if (_JobjFromResponse[JsonKeys.ImageList] != null && !string.IsNullOrEmpty(albumId))
//                    {
//                        JArray imageList = (JArray)_JobjFromResponse[JsonKeys.ImageList];

//                        foreach (JObject singleImage in imageList)
//                        {
//                            //ImageDTO imageDTO = new ImageDTO();
//                            //imageDTO.ImageId = (long)singleImage[JsonKeys.ImageId];
//                            //imageDTO.ImageUrl = (string)singleImage[JsonKeys.ImageUrl];
//                            //imageDTO.Caption = (string)singleImage[JsonKeys.ImageCaption];
//                            //imageDTO.ImageHeight = (int)singleImage[JsonKeys.ImageHeight];
//                            //imageDTO.ImageWidth = (int)singleImage[JsonKeys.ImageWidth];
//                            //imageDTO.ImageType = (int)singleImage[JsonKeys.ImageType];
//                            //imageDTO.NumberOfLikes = (long)singleImage[JsonKeys.NumberOfLikes];
//                            //imageDTO.iLike = (int)singleImage[JsonKeys.ILike];
//                            //imageDTO.NumberOfComments = (long)singleImage[JsonKeys.NumberOfComments];
//                            //imageDTO.iComment = (int)singleImage[JsonKeys.IComment];

//                            ImageDTO imageDTO = HelperMethodsModel.BindImageDetails(singleImage);

//                            //handling missing uid and fullname
//                            imageDTO.UserIdentity = DefaultSettings.userProfile.UserIdentity;
//                            imageDTO.FullName = DefaultSettings.userProfile.FullName;

//                            imageDTO.TotalImagesInAlbum = (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum];
//                            imageDTO.AlbumId = albumId;
//                            lock (TEMP_DICTIONARY)
//                            {
//                                if (TEMP_DICTIONARY.ContainsKey(sequence))
//                                {
//                                    if (TEMP_DICTIONARY[sequence].ContainsKey(imageDTO.ImageId))
//                                    {
//                                        TEMP_DICTIONARY[sequence][imageDTO.ImageId] = imageDTO;
//                                    }
//                                    else
//                                    {
//                                        TEMP_DICTIONARY[sequence].Add(imageDTO.ImageId, imageDTO);
//                                    }
//                                }
//                                else
//                                {
//                                    TEMP_DICTIONARY.Add(sequence, new Dictionary<long, ImageDTO>());
//                                    TEMP_DICTIONARY[sequence].Add(imageDTO.ImageId, imageDTO);
//                                }
//                            }

//                            lock (NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES)
//                            {
//                                if (NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES.ContainsKey(albumId))
//                                {
//                                    if (NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId].ContainsKey(imageDTO.ImageId))
//                                    {
//                                        NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId][imageDTO.ImageId] = imageDTO;
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId].Add(imageDTO.ImageId, imageDTO);
//                                    }

//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES.Add(albumId, new Dictionary<long, ImageDTO>());
//                                    NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId].Add(imageDTO.ImageId, imageDTO);
//                                }
//                            }


//                        }
//                    }

//                    if (TEMP_DICTIONARY.Count == seqTotal)
//                    {
//                        int albumType = HelperMethodsModel.GetImageTypeFromAlbum(albumId);
//                        //if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //{
//                        //    albumType = DefaultSettings.PROFILE_IMAGE;

//                        //}
//                        //else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //{
//                        //    albumType = DefaultSettings.COVER_IMAGE;
//                        //}
//                        //else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //{
//                        //    albumType = DefaultSettings.FEED_IMAGE;
//                        //}

//                        List<ImageDTO> list = new List<ImageDTO>();
//                        foreach (string key in TEMP_DICTIONARY.Keys)
//                        {
//                            list.AddRange(TEMP_DICTIONARY[key].Values.ToList());
//                        }

//                        HelperMethodsAuth.AuthHandlerInstance.AddMyImagesFromServer(albumType, (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum], list);
//                        TEMP_DICTIONARY.Clear();

//                    }
//                }

//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];
//                    HelperMethodsAuth.AuthHandlerInstance.NoImageFoundInMyAlbum(albumId);
//                }

//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessMyAlbumImages ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        private void ProcessFriendAlbumImages() // 109
//        {
//            try
//            {
//                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
//                {
//                    long friendId = (long)_JobjFromResponse[JsonKeys.FriendId];
//                    string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
//                    int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
//                    string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];

//                    Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> TEMP_DICTIONARY = null;
//                    if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_FRIEND_PROFILE_IMAGES;
//                    }
//                    else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_FRIEND_COVER_IMAGES;
//                    }
//                    else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                    {
//                        TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_FRIEND_FEED_IMAGES;
//                    }

//                    if (_JobjFromResponse[JsonKeys.ImageList] != null && !string.IsNullOrEmpty(albumId))
//                    {
//                        JArray imageList = (JArray)_JobjFromResponse[JsonKeys.ImageList];

//                        foreach (JObject singleImage in imageList)
//                        {
//                            ImageDTO imageDTO = HelperMethodsModel.BindImageDetails(singleImage);

//                            imageDTO.FriendId = friendId;

//                            //handling missing uid and fullname
//                            imageDTO.UserIdentity = friendId;
//                            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(friendId))
//                            {
//                                imageDTO.FullName = FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[friendId].FullName;
//                            }

//                            imageDTO.TotalImagesInAlbum = (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum];

//                            lock (TEMP_DICTIONARY)
//                            {
//                                if (TEMP_DICTIONARY.ContainsKey(friendId))
//                                {
//                                    if (TEMP_DICTIONARY[friendId].ContainsKey(sequence))
//                                    {
//                                        if (TEMP_DICTIONARY[friendId][sequence].ContainsKey(imageDTO.ImageId))
//                                        {
//                                            TEMP_DICTIONARY[friendId][sequence][imageDTO.ImageId] = imageDTO;
//                                        }
//                                        else
//                                        {
//                                            TEMP_DICTIONARY[friendId][sequence].Add(imageDTO.ImageId, imageDTO);
//                                        }
//                                    }
//                                    else
//                                    {
//                                        TEMP_DICTIONARY[friendId].Add(sequence, new Dictionary<long, ImageDTO>());
//                                        TEMP_DICTIONARY[friendId][sequence].Add(imageDTO.ImageId, imageDTO);
//                                    }
//                                }
//                                else
//                                {
//                                    TEMP_DICTIONARY.Add(friendId, new Dictionary<string, Dictionary<long, ImageDTO>>());
//                                    TEMP_DICTIONARY[friendId].Add(sequence, new Dictionary<long, ImageDTO>());
//                                    TEMP_DICTIONARY[friendId][sequence].Add(imageDTO.ImageId, imageDTO);
//                                }
//                            }
//                            lock (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES)
//                            {
//                                if (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES.ContainsKey(friendId))
//                                {
//                                    if (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId].ContainsKey(albumId))
//                                    {
//                                        if (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId].ContainsKey(imageDTO.ImageId))
//                                            NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId][imageDTO.ImageId] = imageDTO;
//                                        else
//                                            NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId].Add(imageDTO.ImageId, imageDTO);
//                                    }
//                                    else
//                                    {
//                                        NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId].Add(albumId, new Dictionary<long, ImageDTO>());
//                                        NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId].Add(imageDTO.ImageId, imageDTO);
//                                    }
//                                }
//                                else
//                                {
//                                    NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES.Add(friendId, new Dictionary<string, Dictionary<long, ImageDTO>>());
//                                    NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId].Add(albumId, new Dictionary<long, ImageDTO>());
//                                    NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendId][albumId].Add(imageDTO.ImageId, imageDTO);
//                                }
//                            }
//                        }
//                    }

//                    if (TEMP_DICTIONARY[friendId].Count == seqTotal)
//                    {
//                        int albumType = Models.Utility.HelperMethodsModel.GetImageTypeFromAlbum(albumId);
//                        //if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //    albumType = DefaultSettings.PROFILE_IMAGE;
//                        //else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //    albumType = DefaultSettings.COVER_IMAGE;
//                        //else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
//                        //    albumType = DefaultSettings.FEED_IMAGE;

//                        List<ImageDTO> list = new List<ImageDTO>();
//                        foreach (string key in TEMP_DICTIONARY[friendId].Keys)
//                        {
//                            list.AddRange(TEMP_DICTIONARY[friendId][key].Values.ToList());
//                        }

//                        HelperMethodsAuth.AuthHandlerInstance.AddFriendImagesFromServer(friendId, albumType, (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum], list);
//                        TEMP_DICTIONARY.Clear();
//                    }
//                }
//                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
//                {
//                    long uid = (long)_JobjFromResponse[JsonKeys.FriendId];
//                    string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];
//                    HelperMethodsAuth.AuthHandlerInstance.NoImageFoundInFriendsAlbum(uid, albumId);
//                }
//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessFriendAlbumImages ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        #endregion

//        #region "Sing IN /Sign UP/Sign out"

//        private void ProcessTypeUpdateDigitsVerify()
//        {
//            try
//            {
//                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
//                {
//                    string mbl = (_JobjFromResponse[JsonKeys.MobilePhone] != null) ? (string)_JobjFromResponse[JsonKeys.MobilePhone] : "";
//                    string mblDc = (_JobjFromResponse[JsonKeys.DialingCode] != null) ? (string)_JobjFromResponse[JsonKeys.DialingCode] : "";
//                    if (DefaultSettings.TMP_RECOVERY_MOBILE != null) //recovery
//                    {
//                        HelperMethodsAuth.AuthHandlerInstance.ChangeDigitsVerificationUI(true, mbl, mblDc);
//                        DefaultSettings.TMP_RECOVERY_MOBILE = null;
//                    }
//                    else if (DefaultSettings.userProfile == null || !DefaultSettings.userProfile.MobileNumber.Equals(mbl) || !DefaultSettings.userProfile.MobileDialingCode.Equals(mblDc))
//                    {
//                        if (DigitsVerificationThread.Instance != null) DigitsVerificationThread.Instance.StopThisThread();
//                        DigitsVerificationThread.Instance = null;
//                        HelperMethodsAuth.AuthHandlerInstance.ChangeDigitsVerificationUI(true, mbl, mblDc);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {

//                log.Error("ProcessHashtagMediaContents ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

//            }
//        }
//        private void processMultipleSessionWarning()
//        {
//            try
//            {
//                if (packet_attributes != null && packet_attributes.SessionID.Equals(DefaultSettings.LOGIN_SESSIONID))
//                {
//                    DefaultSettings.LOGIN_SESSIONID = null;
//                    DefaultSettings.LOGIN_USER_ID = 0;
//                    DefaultSettings.VALUE_LOGIN_USER_PASSWORD = null;
//                    HelperMethodsAuth.AuthHandlerInstance.MultipleSessionWarning();
//                }
//            }
//            catch (Exception e)
//            {
//                log.Error("Exception in processMultipleSessionWarning==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//            }
//        }
//        private void ProcessInvalidLoginSession() //19
//        {
//            try
//            {

//                HelperMethodsAuth.AuthHandlerInstance.StopNetworkThread();

//                if (SessionValidationRequest.Instance == null)
//                {
//                    new SessionValidationRequest();
//                }
//                SessionValidationRequest.Instance.StartThis();

//            }
//            catch (Exception e)
//            {

//                log.Error("Exception in ProcessInvalidLoginSession ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//            }
//        }

//        #endregion "Sing IN /Sign UP/Sign out"

//        //private void ProcessSuggestionList() //106
//        //{
//        //    try
//        //    {
//        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success])
//        //        {
//        //            JArray ContactList = (JArray)_JobjFromResponse[JsonKeys.ContactList];

//        //            for (int j = 0; j < ContactList.Count; j++)
//        //            {
//        //                JObject SingleContact = (JObject)ContactList.ElementAt(j);

//        //                if (SingleContact != null)
//        //                {
//        //                    UserBasicInfoDTO suggestionDTO = new UserBasicInfoDTO();


//        //                    if (SingleContact[JsonKeys.UserIdentity] != null)
//        //                    {
//        //                        suggestionDTO.UserIdentity = long.Parse(SingleContact[JsonKeys.UserIdentity].ToString());
//        //                    }

//        //                    if (SingleContact[JsonKeys.UserTableId] != null)
//        //                    {
//        //                        suggestionDTO.UserTableId = (long)SingleContact[JsonKeys.UserTableId];
//        //                    }
//        //                    if (SingleContact[JsonKeys.FullName] != null)
//        //                    {
//        //                        suggestionDTO.FullName = (string)SingleContact[JsonKeys.FullName];
//        //                    }

//        //                    if (SingleContact[JsonKeys.Gender] != null)
//        //                    {
//        //                        suggestionDTO.Gender = (string)SingleContact[JsonKeys.Gender];
//        //                    }

//        //                    if (SingleContact[JsonKeys.ProfileImage] != null)
//        //                    {
//        //                        suggestionDTO.ProfileImage = (string)SingleContact[JsonKeys.ProfileImage];
//        //                    }

//        //                    if (SingleContact[JsonKeys.ProfileImageId] != null)
//        //                    {
//        //                        suggestionDTO.ProfileImageId = (long)SingleContact[JsonKeys.ProfileImageId];
//        //                    }

//        //                    if (SingleContact[JsonKeys.UpdateTime] != null)
//        //                    {
//        //                        suggestionDTO.UpdateTime = (long)SingleContact[JsonKeys.UpdateTime];
//        //                    }
//        //                    if (SingleContact[JsonKeys.NumberOfMutualFriend] != null)
//        //                    {
//        //                        suggestionDTO.NumberOfMutualFriends = (int)SingleContact[JsonKeys.NumberOfMutualFriend];
//        //                    }

//        //                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY)
//        //                    {
//        //                        if (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.ContainsKey(suggestionDTO.UserIdentity))
//        //                        {
//        //                            FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY[suggestionDTO.UserIdentity] = suggestionDTO;
//        //                        }
//        //                        else
//        //                        {
//        //                            FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Add(suggestionDTO.UserIdentity, suggestionDTO);
//        //                        }
//        //                    }
//        //                }

//        //            }
//        //            HelperMethodsAuth.AuthHandlerInstance.AddSuggestionsToUIList();

//        //        }


//        //    }
//        //    catch (Exception e)
//        //    {
//        //        System.Diagnostics.Debug.WriteLine("Exception in ProcessSuggestionList==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//        //        if (log.IsErrorEnabled)
//        //        {
//        //            log.Error("Exception in ProcessSuggestionList ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
//        //        }
//        //    }
//        //}
//    }
//}

#endregion

using System;
using Auth.AppInterfaces;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;


namespace Auth.Parser
{
    public class AuthMsgParser
    {
        #region "Private Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(AuthMsgParser).Name);
        private string _JsonResponse = null;
        private JObject _JobjFromResponse = null;

        private int action;
        private string client_packet_id;
        private long server_packet_id;

        private byte[] byte_response = null;
        private int byte_start_pos;
        private int byte_data_length;
        private CommonPacketAttributes packet_attributes;
        IAuthSignalHandler iAuthSignalHandler;
        #endregion "Private Fields"

        #region "Public Fields"
        public static int SpamReasonListSeqCount = 0, FeedTagListSeqCount = 0, WorkListSequenceCount = 0, EducationListSequenceCount = 0, MediaItemsSequenceCount = 0, SkillListSequenceCount = 0, FriendContactListSequenceCount = 0, MutualFriendsSeqCount = 0, LikesSequnceCount = 0, CommentsSeqCount = 0, ImageLikesSequenceCount = 0, ImageCommentsSequenceCount = 0, ImageCommentLikesSequeceCount = 0, CircleMembersListSeqCount = 0, MediaAlbumListSeqCount = 0;// AlbumMediaContentListSeqCount = 0;
        #endregion "Public Fields"

        #region "Constructors"
        public AuthMsgParser(string resp, int action, long server_packet_id, string client_packet_id, IAuthSignalHandler iAuthSignalHandler)
        {
            this._JsonResponse = resp != null ? resp.Trim() : "";
            this.action = action;
            this.server_packet_id = server_packet_id;
            this.client_packet_id = client_packet_id;
            this.iAuthSignalHandler = iAuthSignalHandler;
        }

        public AuthMsgParser(byte[] resp, int action, long server_packet_id, string client_packet_id, int start_pos, int data_length, IAuthSignalHandler iAuthSignalHandler)
        {
            this.byte_response = resp;
            this.action = action;
            this.server_packet_id = server_packet_id;
            this.client_packet_id = client_packet_id;
            this.byte_start_pos = start_pos;
            this.byte_data_length = data_length;
            this.iAuthSignalHandler = iAuthSignalHandler;
        }
        #endregion "Constructors"

        #region "Initit StartProcess"

        public void Process()
        {
            try
            {
                if (_JsonResponse != null) //process json here
                {
                    if (this.action != 200)
                    {
#if AUTH_LOG
                        log.Info("Received for action=" + this.action + " from Auth JSON==> " + this._JsonResponse);
#endif
                        _JobjFromResponse = JObject.Parse(_JsonResponse);
                    }

                    if (!String.IsNullOrEmpty(client_packet_id))
                    {
                        lock (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY)
                        {
                            if (this.action == 200)
                            {
                                _JobjFromResponse = new JObject();
                                _JobjFromResponse.Add(JsonKeys.PacketId, client_packet_id);
                                _JobjFromResponse.Add(JsonKeys.Action, this.action);
                            }
                            else if (this.action == AppConstants.TYPE_ADD_STATUS
                                || this.action == AppConstants.TYPE_EDIT_STATUS
                                || this.action == AppConstants.TYPE_MULTIPLE_IMAGE_POST
                                || this.action == AppConstants.TYPE_SHARE_STATUS
                                || this.action == AppConstants.TYPE_ADD_STATUS_COMMENT
                                || this.action == AppConstants.TYPE_EDIT_STATUS_COMMENT
                                || this.action == AppConstants.TYPE_EDIT_IMAGE_COMMENT
                                || this.action == AppConstants.TYPE_EDIT_COMMENT_ON_MEDIA
                                || this.action == AppConstants.TYPE_START_GROUP_CHAT)
                                // || this.action == AppConstants.TYPE_ADD_IMAGE_COMMENT
                                // || this.action == AppConstants.TYPE_ADD_COMMENT_ON_MEDIA
                                
                            {
                                client_packet_id = client_packet_id + "_" + this.action;
                            }
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.AddOrUpdate(client_packet_id, _JobjFromResponse, (key, oldValue) => _JobjFromResponse);

                        }
                    }
                    this.processJsonResponse();
                }
                else  //process byte here
                {
                    if (!String.IsNullOrEmpty(client_packet_id))
                    {
                        _JobjFromResponse = new JObject();
                        _JobjFromResponse.Add(JsonKeys.PacketId, client_packet_id);
                        lock (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY)
                        {
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.AddOrUpdate(client_packet_id, _JobjFromResponse, (key, oldValue) => _JobjFromResponse);
                        }
                    }

                    if (this.action != 200)
                    {
                        this.packet_attributes = Parser.ParsePacket(this.byte_response, this.byte_start_pos, this.byte_data_length);
#if AUTH_LOG
                        log.Info("Received for action=" + this.action + " from Auth BYTE==> " + packet_attributes.ToString());
#endif
                    }
                    this.processByteResponse();
                }
            }
            catch (Exception e)
            {
                if (this._JsonResponse != null)
                {

                    log.Error("Auth parse Exception for action=" + this.action + " from Auth JSON==>" + e.Message + "\n" + e.StackTrace + " Response==>" + this._JsonResponse);

                }
                else if (this.packet_attributes != null)
                {

                    log.Error("Auth parse Exception for action=" + this.action + " from Auth BYTE==>" + e.Message + "\n" + e.StackTrace + " Response==>" + this.packet_attributes.ToString());

                }
            }

        }

        private void processByteResponse()
        {
            switch (this.action)
            {
                case AppConstants.TYPE_CONTACT_UTIDS:  //29
                    iAuthSignalHandler.Process_CONTACT_UTIDS_29(packet_attributes);
                    break;

                case AppConstants.TYPE_CONTACT_LIST:  //23
                    iAuthSignalHandler.Process_CONTACT_LIST_23(packet_attributes);
                    break;

                case AppConstants.TYPE_MULTIPLE_SESSION_WARNING: //75
                    iAuthSignalHandler.Process_MULTIPLE_SESSION_WARNING_75(packet_attributes);
                    break;

                case AppConstants.TYPE_UNWANTED_LOGIN: //79
                    iAuthSignalHandler.Process_UNWANTED_LOGIN_79(packet_attributes);
                    break;
                default:
                    break;

            }
        }

        private void processJsonResponse()
        {
            switch (this.action)
            {
                case AppConstants.TYPE_INVALID_LOGIN_SESSION: //19
                    iAuthSignalHandler.Process_INVALID_LOGIN_SESSION_19(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_SUGGESTION_IDS: //31
                    iAuthSignalHandler.Process_SUGGESTION_IDS_31(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_USERS_DETAILS: //32
                    iAuthSignalHandler.Process_USERS_DETAILS_32(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CONTACT_SEARCH: //34
                    iAuthSignalHandler.Process_CONTACT_SEARCH_34(_JobjFromResponse, client_packet_id);//   this.ProcessContactSearch();
                    break;

                case AppConstants.TYPE_COMMENTS_FOR_STATUS: //84
                    iAuthSignalHandler.Process_COMMENTS_FOR_STATUS_84(_JobjFromResponse);//   this.ProcessCommentsForStatus();
                    break;
                case AppConstants.TYPE_MEDIA_FEED://87
                    iAuthSignalHandler.Process_MEDIA_FEED_87(_JobjFromResponse, client_packet_id);//     this.ProcessBreakingNewsPortalFeeds();
                    break;
                case AppConstants.TYPE_NEWS_FEED: //88
                    iAuthSignalHandler.Process_NEWS_FEED_88(_JobjFromResponse, client_packet_id);//   this.ProcessNewsFeed();
                    break;
                case AppConstants.ACTION_MERGED_LIKES_LIST_OF_COMMENT: //1116
                    iAuthSignalHandler.Process_ACTION_MERGED_LIKES_LIST_OF_COMMENT_1116(_JobjFromResponse);
                    break;
                case AppConstants.ACTION_MERGED_COMMENTS_LIST: //1084
                    iAuthSignalHandler.Process_ACTION_MERGED_COMMENTS_LIST_1084(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LIKES_FOR_STATUS: //92
                    iAuthSignalHandler.Process_LIKES_FOR_STATUS_92(_JobjFromResponse);//   this.ProcessFetchLikesForStatus();
                    break;
                case AppConstants.TYPE_LIKES_FOR_IMAGE: //93
                    iAuthSignalHandler.Process_LIKES_FOR_IMAGE_93(_JobjFromResponse);//   this.ProcessLikersImage();
                    break;
                case AppConstants.TYPE_MEDIA_LIKE_LIST: //269
                    iAuthSignalHandler.Process_MEDIA_LIKE_LIST_269(_JobjFromResponse);//     this.ProcessMediaLikeList();
                    break;
                case AppConstants.TYPE_MY_BOOK: //94
                    iAuthSignalHandler.Process_MY_BOOK_94(_JobjFromResponse, client_packet_id);//    this.ProcessMyBook();
                    break;
                case AppConstants.TYPE_IMAGE_ALBUM_LIST: //96
                    iAuthSignalHandler.Process_IMAGE_ALBUM_LIST_96(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_MY_ALBUM_IMAGES: //97
                    iAuthSignalHandler.Process_MY_ALBUM_IMAGES_97(_JobjFromResponse);//  this.ProcessMyAlbumImages();
                    break;
                case AppConstants.TYPE_FRIEND_ALBUM_IMAGES: //109
                    iAuthSignalHandler.Process_FRIEND_ALBUM_IMAGES_109(_JobjFromResponse);// this.ProcessFriendAlbumImages();
                    break;
                case AppConstants.TYPE_FRIEND_NEWSFEED: //110
                    iAuthSignalHandler.Process_FRIEND_NEWSFEED_110(_JobjFromResponse, client_packet_id);//   this.ProcessFriendNewsFeeds();
                    break;
                //case AppConstants.TYPE_YOU_MAY_KNOW_LIST: //106
                //    this.ProcessSuggestionList();
                //    break;
                case AppConstants.TYPE_FRIEND_CONTACT_LIST: //107
                    iAuthSignalHandler.Process_FRIEND_CONTACT_LIST_107(_JobjFromResponse);// 
                    break;
                case AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141: //211
                    iAuthSignalHandler.Process_FRIEND_CONTACT_LIST_211(_JobjFromResponse);//    this.ProcessFriendContactList();
                    break;
                case AppConstants.TYPE_MY_NOTIFICATIONS: //111
                    iAuthSignalHandler.Process_MY_NOTIFICATIONS_111(_JobjFromResponse);//   this.ProcessMyNotifications();
                    break;
                case AppConstants.TYPE_SINGLE_NOTIFICATION: //113
                    iAuthSignalHandler.Process_SINGLE_NOTIFICATION_113(_JobjFromResponse);//    this.ProcessSingleNotification();
                    break;
                //case AppConstants.TYPE_SINGLE_STATUS_NOTIFICATION: //114
                //    this.ProcessSingleFeedDetails();
                //    break;
                case AppConstants.TYPE_SINGLE_FEED_SHARE_LIST://115
                    iAuthSignalHandler.Process_SINGLE_FEED_SHARE_LIST_115(_JobjFromResponse);//     this.ProcessSingleFeedShareList();
                    break;
                case AppConstants.TYPE_LIST_LIKES_OF_COMMENT: //116
                    iAuthSignalHandler.Process_LIST_LIKES_OF_COMMENT_116(_JobjFromResponse);//    this.ProcessFetchLikesForComment();
                    break;
                case AppConstants.TYPE_MULTIPLE_IMAGE_POST://117
                    iAuthSignalHandler.Process_MULTIPLE_IMAGE_POST_117(_JobjFromResponse);//    this.ProcessAddStatus();
                    break;
                case AppConstants.TYPE_MUTUAL_FRIENDS://118
                    iAuthSignalHandler.Process_MUTUAL_FRIENDS_118(_JobjFromResponse);//    this.ProcessTypeMutualFriends();
                    break;

                #region For Multisession
                case AppConstants.TYPE_ADD_FRIEND://127
                    iAuthSignalHandler.Process_ADD_FRIEND_127(_JobjFromResponse, server_packet_id);//     this.ProcessPushAddFriend();
                    break;
                case AppConstants.TYPE_DELETE_FRIEND://128
                    iAuthSignalHandler.Process_DELETE_FRIEND_128(_JobjFromResponse, action, server_packet_id);//      this.ProcessDeleteFriendUpdate();
                    break;
                case AppConstants.TYPE_ACCEPT_FRIEND://129
                    iAuthSignalHandler.Process_ACCEPT_FRIEND_129(_JobjFromResponse, server_packet_id);//      this.ProcessPushAcceptFriend();
                    break;
                #endregion

                case AppConstants.TYPE_ADD_STATUS: //177
                    iAuthSignalHandler.Process_ADD_STATUS_177(_JobjFromResponse);//    this.ProcessAddStatus();
                    break;
                case AppConstants.TYPE_START_GROUP_CHAT: //134
                    iAuthSignalHandler.Process_START_GROUP_CHAT_134(_JobjFromResponse, action, server_packet_id, client_packet_id);//    this.ProcessUpdateStartGroupChat();
                    break;
                case AppConstants.TYPE_ACTION_GET_MORE_FEED_IMAGE: //139
                    iAuthSignalHandler.Process_MORE_FEED_IMAGES_139(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_START_FRIEND_CHAT: //175
                    iAuthSignalHandler.Process_START_FRIEND_CHAT_175(_JobjFromResponse, action, server_packet_id, client_packet_id);//    this.ProcessUpdateStartFriendChat();
                    break;
                //case AppConstants.TYPE_LIKE_STATUS: //184
                //    iAuthSignalHandler.Process_LIKE_STATUS_184(_JobjFromResponse);//    this.ProcessUpdateLikeStatus(true);
                //    break;
                //case AppConstants.TYPE_UNLIKE_STATUS: //186
                //    iAuthSignalHandler.Process_UNLIKE_STATUS_186(_JobjFromResponse);//     this.ProcessUpdateUnlikeStatus(true);
                //    break;
                case AppConstants.TYPE_SINGLE_FRIEND_PRESENCE_INFO://199
                    iAuthSignalHandler.Process_SINGLE_FRIEND_PRESENCE_INFO_199(_JobjFromResponse);//   this.ProcessTypeFriendPresenceInfo();
                    break;
                case AppConstants.TYPE_UPDATE_DIGITS_VERIFY://209
                    iAuthSignalHandler.Process_UPDATE_DIGITS_VERIFY_209(_JobjFromResponse);//      this.ProcessTypeUpdateDigitsVerify();
                    break;
                case AppConstants.TYPTE_MISS_CALL_LIST: // 224
                    iAuthSignalHandler.Process_MISS_CALL_LIST_224(_JobjFromResponse);//     this.ProcessMissCallList();
                    break;
                case AppConstants.TYPE_WHO_SHARES_LIST: //249
                    iAuthSignalHandler.Process_WHO_SHARES_LIST_249(_JobjFromResponse);//    this.ProcessWhoSharesList();
                    break;
                case AppConstants.TYPE_MEDIA_SHARE_LIST://250
                    iAuthSignalHandler.Process_MEDIA_SHARE_LIST_250(_JobjFromResponse);// this.ProcessSingleFeedShareList();
                    break;
                case AppConstants.TYPE_MEDIA_ALBUM_LIST: //256
                    iAuthSignalHandler.Process_MEDIA_ALBUM_LIST_256(_JobjFromResponse);//   this.ProcessMediaAlbumList();
                    break;
                case AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST: //261
                    iAuthSignalHandler.Process_MEDIA_ALBUM_CONTENT_LIST_261(_JobjFromResponse);//   this.ProcessMediaAlbumContentList();
                    break;
                //case AppConstants.TYPE_MEDIA_CONTENT_DETAILS://262
                //    this.ProcessMediaContentDetails();
                //    break;
                
                case AppConstants.TYPE_MEDIA_COMMENT_LIST://270
                    iAuthSignalHandler.Process_MEDIA_COMMENT_LIST_270(_JobjFromResponse);//     this.ProcessMediaCommentList();
                    break;
                case AppConstants.TYPE_MEDIACOMMENT_LIKE_LIST://271
                    iAuthSignalHandler.Process_MEDIACOMMENT_LIKE_LIST_271(_JobjFromResponse);//     this.ProcessMediaCommentLikeList();
                    break;
                case AppConstants.TYPE_VIEW_DOING_LIST: //273
                    iAuthSignalHandler.Process_VIEW_DOING_LIST_273(_JobjFromResponse);//       this.ProcessDoingList();
                    break;
                case AppConstants.TYPE_VIEW_TAGGED_LIST: //274
                    iAuthSignalHandler.Process_VIEW_TAGGED_LIST_274(_JobjFromResponse);//     this.ProcessFeedTagList();
                    break;
                case AppConstants.TYPE_MEDIA_SUGGESTION://277
                    iAuthSignalHandler.Process_MEDIA_SUGGESTION_277(_JobjFromResponse);//  this.ProcessMediaSearchSuggestions();
                    break;
                case AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD: //278
                    iAuthSignalHandler.Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(_JobjFromResponse);//  this.ProcessMediaFullSearch();
                    break;
                case AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS: //279
                    iAuthSignalHandler.Process_HASHTAG_MEDIA_CONTENTS_279(_JobjFromResponse);//    this.ProcessHashtagMediaContents();
                    break;
                case AppConstants.TYPE_HASHTAG_SUGGESTION: //280
                    iAuthSignalHandler.Process_HASHTAG_SUGGESTION_280(_JobjFromResponse);//    this.ProcessHashtagSuggestion();
                    break;
                case AppConstants.TYPE_SEARCH_TRENDS: //281
                    iAuthSignalHandler.Process_SEARCH_TRENDS_281(_JobjFromResponse);//    this.ProcessMediaSearchTrends();
                    break;
                case AppConstants.TYPE_STORE_CONTACT_LIST: //284
                    iAuthSignalHandler.Process_STORE_CONTACT_LIST_284(_JobjFromResponse);//   this.ProcessStoreContactList();
                    break;
                case AppConstants.TYPE_CELEBRITY_CATEGORY_LIST: //285
                    iAuthSignalHandler.Process_CELEBRITIES_CATEGORIES_LIST_285(_JobjFromResponse);//  this.ProcessNewsPortalCategoriesList();
                    break;
                case AppConstants.TYPE_CELEBRITY_LIST: // 286
                    iAuthSignalHandler.Process_CELEBRITY_DISCOVER_LIST_286(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CELEBRITY_FEED: //288
                    iAuthSignalHandler.Process_TYPE_CELEBRITY_FEED_288(_JobjFromResponse, client_packet_id);//   this.ProcessStoreContactList();
                    break;
                case AppConstants.TYPE_NEWSPORTAL_CATEGORIES_LIST: //294
                    iAuthSignalHandler.Process_NEWSPORTAL_CATEGORIES_LIST_294(_JobjFromResponse);//  this.ProcessNewsPortalCategoriesList();
                    break;
                case AppConstants.TYPE_NEWSPORTAL_FEED: //295
                    iAuthSignalHandler.Process_NEWSPORTAL_FEED_295(_JobjFromResponse, client_packet_id);//   this.ProcessNewsPortalFeed();
                    break;
                case AppConstants.TYPE_NEWSPORTAL_LIST://299
                    iAuthSignalHandler.Process_NEWSPORTAL_LIST_299(_JobjFromResponse);//  this.ProcessDiscoverNewsPortals();
                    break;
                case AppConstants.TYPE_NEWSPORTAL_BREAKING_FEED://302
                    iAuthSignalHandler.Process_NEWSPORTAL_BREAKING_FEED_302(_JobjFromResponse);//     this.ProcessBreakingNewsPortalFeeds();
                    break;
                case AppConstants.TYPE_BUSINESSPAGE_BREAKING_FEED://303
                    iAuthSignalHandler.Process_BUSINESSPAGE_BREAKING_FEED_303(_JobjFromResponse);//     this.ProcessBreakingPageFeeds();
                    break;
                case AppConstants.TYPE_BUSINESS_PAGE_FEED: //306
                    iAuthSignalHandler.Process_BUSINESS_PAGE_FEED_306(_JobjFromResponse, client_packet_id);//    this.ProcessBusinessPageFeeds();
                    break;
                //case AppConstants.TYPE_MEDIA_PAGE_FEED://307
                //    iAuthSignalHandler.Process_MEDIA_PAGE_FEED_307(_JobjFromResponse, client_packet_id);//     this.ProcessBreakingNewsPortalFeeds();
                //    break;
                case AppConstants.TYPE_MEDIA_PAGE_TRENDING_FEED: //308
                    iAuthSignalHandler.Process_MEDIA_PAGE_TRENDING_FEED_308(_JobjFromResponse);//    this.ProcessBusinessPageFeeds();
                    break;
                case AppConstants.TYPE_ALL_SAVED_FEEDS: //309
                    iAuthSignalHandler.Process_ALL_SAVED_FEEDS_309(_JobjFromResponse, client_packet_id);//    this.ProcessBusinessPageFeeds();
                    break;
                case AppConstants.ACTION_UPDATE_LIKE_UNLIKE_COMMENT: //1323
                    iAuthSignalHandler.Process_ACTION_UPDATE_LIKE_UNLIKE_COMMENT_1323(_JobjFromResponse); 
                    break;
                case AppConstants.TYPE_UPDATE_LIKE_COMMENT: //323
                    iAuthSignalHandler.Process_UPDATE_LIKE_COMMENT_323(_JobjFromResponse);//    this.ProcessUpdateLikeComment();
                    break;
                case AppConstants.TYPE_UPDATE_UNLIKE_COMMENT: //325
                    iAuthSignalHandler.Process_UPDATE_UNLIKE_COMMENT_325(_JobjFromResponse);//  this.ProcessUpdateUnLikeComment();
                    break;
                case AppConstants.TYPE_UPDATE_ADD_FRIEND: //327
                    iAuthSignalHandler.Process_UPDATE_ADD_FRIEND_327(_JobjFromResponse);//  ProcessAddFriendUpdate();
                    break;
                case AppConstants.TYPE_UPDATE_DELETE_FRIEND: //328
                    iAuthSignalHandler.Process_UPDATE_DELETE_FRIEND_328(_JobjFromResponse, action, server_packet_id);//     ProcessDeleteFriendUpdate();
                    break;
                case AppConstants.TYPE_UPDATE_ACCEPT_FRIEND: //329
                    iAuthSignalHandler.Process_UPDATE_ACCEPT_FRIEND_329(_JobjFromResponse);//    ProcessAcceptFriendUpdate();
                    break;
                case AppConstants.TYPE_UPDATE_START_GROUP_CHAT: //334
                    iAuthSignalHandler.Process_UPDATE_START_GROUP_CHAT_334(_JobjFromResponse, action, server_packet_id, client_packet_id);//   this.ProcessUpdateStartGroupChat();
                    break;
                case AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER: //335
                    iAuthSignalHandler.Process_UPDATE_ADD_GROUP_MEMBER_335(_JobjFromResponse, action, server_packet_id, client_packet_id);
                    //      this.ProcessUpdateAddGroupMember();
                    break;
                case AppConstants.TYPE_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO://336
                    iAuthSignalHandler.Process_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO_336(_JobjFromResponse);//  this.ProcessTypeFriendPresenceInfo();
                    break;
                //case AppConstants.TYPE_UPDATE_DELETE_CIRCLE:
                //    iAuthSignalHandler.Process_UPDATE_DELETE_CIRCLET_352(_JobjFromResponse);
                //    break;
                //case AppConstants.TYPE_UPDATE_REMOVE_CIRCLE_MEMBER:
                //    iAuthSignalHandler.Process_UPDATE_REMOVE_CIRCLE_MEMBER_354(_JobjFromResponse);
                //    break;
                //case AppConstants.TYPE_UPDATE_ADD_CIRCLE_MEMBER:
                //    iAuthSignalHandler.Process_UPDATE_ADD_CIRCLE_MEMBER_356(_JobjFromResponse);
                //    break;
                //case AppConstants.TYPE_UPDATE_EDIT_CIRCLE_MEMBER:
                //    iAuthSignalHandler.Process_UPDATE_EDIT_CIRCLE_MEMBER_358(_JobjFromResponse);
                //    break;
                case AppConstants.TYPE_UPDATE_SEND_REGISTER: //374
                    iAuthSignalHandler.Process_UPDATE_SEND_REGISTER_374(_JobjFromResponse);//    this.ProcessUpdateSendRegister();
                    break;
                case AppConstants.TYPE_UPDATE_START_FRIEND_CHAT: //375
                    iAuthSignalHandler.Process_UPDATE_START_FRIEND_CHAT_375(_JobjFromResponse, action, server_packet_id, client_packet_id);//   this.ProcessUpdateStartFriendChat();
                    break;
                case AppConstants.TYPE_UPDATE_ADD_STATUS: //377
                    iAuthSignalHandler.Process_UPDATE_ADD_STATUS_377(_JobjFromResponse);//   this.ProcessUpdateAddStatus(true);
                    break;
                case AppConstants.TYPE_UPDATE_SHARE_STATUS: //391
                    iAuthSignalHandler.Process_UPDATE_SHARE_STATUS_391(_JobjFromResponse);//    this.ProcessUpdateAddStatus();
                    break;
                case AppConstants.TYPE_UPDATE_EDIT_STATUS: //378
                    iAuthSignalHandler.Process_UPDATE_EDIT_STATUS_378(_JobjFromResponse);//     this.ProcessUpdateEditStatus();
                    break;
                case AppConstants.TYPE_UPDATE_DELETE_STATUS: //379
                    iAuthSignalHandler.Process_UPDATE_DELETE_STATUS_379(_JobjFromResponse);//     this.ProcessUpdateDeleteStatus();
                    break;

                case AppConstants.TYPE_UPDATE_LIKE_STATUS: //384
                    iAuthSignalHandler.Process_UPDATE_LIKE_STATUS_384(_JobjFromResponse);//   this.ProcessUpdateLikeStatus();
                    break;
                case AppConstants.ACTION_MERGED_UPDATE_LIKE_UNLIKE: //1384
                    iAuthSignalHandler.Process_MERGED_UPDATE_LIKE_UNLIKE_1384(_JobjFromResponse);//   this.ProcessUpdateLikeStatus();
                    break;
                case AppConstants.TYPE_UPDATE_LIKE_UNLIKE_IMAGE: //385 
                    iAuthSignalHandler.Process_UPDATE_LIKE_UNLIKE_IMAGE_385(_JobjFromResponse);//   this.ProcessUpdateLikeUnlikeImage();
                    break;
                case AppConstants.TYPE_UPDATE_UNLIKE_STATUS: //386
                    iAuthSignalHandler.Process_UPDATE_UNLIKE_STATUS_386(_JobjFromResponse);//  this.ProcessUpdateUnlikeStatus();
                    break;
                case AppConstants.TYPE_UPDATE_EDIT_STATUS_COMMENT: //389
                    iAuthSignalHandler.Process_UPDATE_EDIT_STATUS_COMMENT_389(_JobjFromResponse);//     this.ProcessUpdateEditComment();
                    break;

                #region Image tasks
                case AppConstants.TYPE_UPDATE_ADD_IMAGE_COMMENT: //380
                    iAuthSignalHandler.Process_UPDATE_ADD_IMAGE_COMMENT_380(_JobjFromResponse);//      this.ProcessUpdateAddImageComment();
                    break;
                case AppConstants.TYPE_UPDATE_ADD_STATUS_COMMENT: //381
                    iAuthSignalHandler.Process_UPDATE_ADD_STATUS_COMMENT_381(_JobjFromResponse);//      this.ProcessUpdateAddStatusComment();
                    break;
                case AppConstants.ACTION_MERGED_UPDATE_ADD_COMMENT: //1381
                    iAuthSignalHandler.Process_ACTION_MERGED_UPDATE_ADD_COMMENT_1381(_JobjFromResponse);//      this.ProcessUpdateAddStatusComment();
                    break;
                case AppConstants.TYPE_UPDATE_DELETE_IMAGE_COMMENT: //382
                    iAuthSignalHandler.Process_UPDATE_DELETE_IMAGE_COMMENT_382(_JobjFromResponse);//    this.ProcessUpdateDeleteImageComment();
                    break;
                case AppConstants.ACTION_MERGED_UPDATE_DELETE_COMMENT: //1383
                    iAuthSignalHandler.Process_ACTION_MERGED_UPDATE_DELETE_COMMENT_1383(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_DELETE_STATUS_COMMENT: //383
                    iAuthSignalHandler.Process_UPDATE_DELETE_STATUS_COMMENT_383(_JobjFromResponse);//    this.ProcessUpdateDeleteStatusComment();
                    break;
                case AppConstants.TYPE_UPDATE_EDIT_IMAGE_COMMENT: //394
                    iAuthSignalHandler.Process_UPDATE_EDIT_IMAGE_COMMENT_394(_JobjFromResponse);//     this.ProcessUpdateEditImageComment();
                    break;
                case AppConstants.TYPE_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT: //397
                    iAuthSignalHandler.Process_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT_397(_JobjFromResponse);//    this.ProcessUpdateLikeUnlikeImageComment();
                    break;
                //case AppConstants.TYPE_IMAGE_DETAILS: //121
                //    this.ProcessSingleImageDetail();
                //    break;
                case AppConstants.TYPE_COMMENTS_FOR_IMAGE: // 89
                    iAuthSignalHandler.Process_COMMENTS_FOR_IMAGE_89(_JobjFromResponse);//      this.ProcessImageComments();
                    break;
                case AppConstants.TYPE_IMAGE_COMMENT_LIKES: //196
                    iAuthSignalHandler.Process_IMAGE_COMMENT_LIKES_196(_JobjFromResponse);//      this.ProcessLikersImageComment();
                    break;
                
                case AppConstants.TYPE_LIKE_UNLIKE_IMAGE_COMMENT: // 197
                    iAuthSignalHandler.Process_LIKE_UNLIKE_IMAGE_COMMENT_197(_JobjFromResponse);//   this.ProcessLikeUnlikeImageComment();
                    break;
                //case AppConstants.TYPE_ADD_IMAGE_COMMENT: //180
                //    iAuthSignalHandler.Process_ADD_IMAGE_COMMENT_180(_JobjFromResponse);//  this.ProcessAddImageComment();
                //    break;
                //case AppConstants.TYPE_EDIT_IMAGE_COMMENT: //194
                //    iAuthSignalHandler.Process_EDIT_IMAGE_COMMENT_194(_JobjFromResponse);//      this.ProcessEditImageComment();
                //    break;
                #endregion

                case AppConstants.TYPE_UPDATE_LIKEUNLIKE_MEDIA: //464
                    iAuthSignalHandler.Process_UPDATE_LIKEUNLIKE_MEDIA_464(_JobjFromResponse);//     this.ProcessUpdateLikeUnlikeMedia();
                    break;
                case AppConstants.TYPE_UPDATE_COMMENT_MEDIA: //465
                    iAuthSignalHandler.Process_UPDATE_COMMENT_MEDIA_465(_JobjFromResponse);//    this.ProcessUpdateCommentMedia();
                    break;
                case AppConstants.TYPE_UPDATE_EDITCOMMENT_MEDIA: //466
                    iAuthSignalHandler.Process_UPDATE_EDITCOMMENT_MEDIA_466(_JobjFromResponse);//     this.ProcessUpdateEditCommentMedia();
                    break;
                case AppConstants.TYPE_UPDATE_DELETECOMMENT_MEDIA: //467
                    iAuthSignalHandler.Process_UPDATE_DELETECOMMENT_MEDIA_467(_JobjFromResponse);//     this.ProcessUpdateDeleteCommentMedia();
                    break;
                //case AppConstants.TYPE_UPDATE_LIKEUNLIKECOMMENT_MEDIA: //468
                //    iAuthSignalHandler.Process_UPDATE_LIKEUNLIKECOMMENT_MEDIA_468(_JobjFromResponse);//      this.ProcessUpdateLikeUnlikeCommentMedia();
                //    break;
                case AppConstants.TYPE_UPDATE_STORE_CONTACT_LIST: //484
                    iAuthSignalHandler.Process_UPDATE_STORE_CONTACT_LIST_484(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_NEW_CIRCLE: //51
                    iAuthSignalHandler.Process_NEW_CIRCLE_51(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LEAVE_CIRCLE: //53
                    iAuthSignalHandler.Process_LEAVE_CIRCLE_53(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CIRCLE_LIST: //70
                    iAuthSignalHandler.Process_CIRCLE_LIST_70(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CIRCLE_MEMBERS_LIST: //99
                    iAuthSignalHandler.Process_CIRCLE_MEMBERS_LIST_99(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_SEARCH_CIRCLE_MEMBER: //101
                    iAuthSignalHandler.Process_SEARCH_CIRCLE_MEMBER_101(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_DELETE_CIRCLE: //152
                    iAuthSignalHandler.Process_DELETE_CIRCLE_152(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_REMOVE_CIRCLE_MEMBER:
                    iAuthSignalHandler.Process_REMOVE_CIRCLE_MEMBER_154(_JobjFromResponse);
                    break;

                case AppConstants.TYPE_CIRCLE_NEWSFEED: //198
                    iAuthSignalHandler.Process_CIRCLE_NEWSFEED_198(_JobjFromResponse, client_packet_id);
                    break;
                case AppConstants.TYPE_CIRCLE_DETAILS:
                    ////case AppConstants.TYPE_UPDATE_DELETE_CIRCLE: //352
                    ////case AppConstants.TYPE_UPDATE_REMOVE_CIRCLE_MEMBER: //354
                    ////case AppConstants.TYPE_UPDATE_ADD_CIRCLE_MEMBER: //356
                    ////case AppConstants.TYPE_UPDATE_EDIT_CIRCLE_MEMBER: //358
                    //  AMPCircle.Instance.ProcessCircle(action, _JobjFromResponse);
                    iAuthSignalHandler.Process_CIRCLE_DETAILS_52(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_PRESENCE: //78
                    iAuthSignalHandler.Process_PRESENCE_78(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_WORK: // 227;
                    iAuthSignalHandler.Process_ADD_WORK_227(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_EDUCATION: // 231;
                    iAuthSignalHandler.Process_ADD_EDUCATION_231(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LIST_WORK: // 234
                    iAuthSignalHandler.Process_LIST_WORK_234(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LIST_EDUCATION: //235
                    iAuthSignalHandler.Process_LIST_EDUCATION_235(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_LIST_SKILL: //236
                    iAuthSignalHandler.Process_LIST_SKILL_236(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_SKILL: // 237;
                    iAuthSignalHandler.Process_ADD_SKILL_237(_JobjFromResponse);//     AMPProfileAbout.Instance.ProcessAbout(action, _JobjFromResponse);
                    break;
                case AppConstants.TYPE_SPAM_REASON_LIST: //1001
                    iAuthSignalHandler.Process_SPAM_REASON_LIST_1001(_JobjFromResponse);//    this.ProcessSpamReasonList();
                    break;
                case AppConstants.TYPE_NEWSFEED_EDIT_HISTORY_LIST: //1016
                    iAuthSignalHandler.Process_NEWSFEED_EDIT_HISTORY_LIST_1016(_JobjFromResponse);//    this.ProcessSpamReasonList();
                    break;
                case AppConstants.TYPE_COMMENT_EDIT_HISTORY_LIST: //1021
                    iAuthSignalHandler.Process_COMMENT_EDIT_HISTORY_LIST_1021(_JobjFromResponse);//    this.ProcessSpamReasonList();
                    break;
                case AppConstants.TYPE_GET_WALLET_INFORMATION: //1026
                    iAuthSignalHandler.Process_TYPE_GET_WALLET_INFORMATION_1026(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION: //1027
                    iAuthSignalHandler.Process_TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION_1027(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_TRANSACTION_HISTORY:
                    iAuthSignalHandler.Process_TYPE_GET_TRANSACTION_HISTORY_1031(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_REFERRAL_REQUEST: //1037
                    iAuthSignalHandler.PROCESS_TYPE_UPDATE_REFERRAL_REQUEST_1037(_JobjFromResponse);
                    break;
                //case AppConstants.TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST: //1039
                //    iAuthSignalHandler.PROCESS_TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST_1039(_JobjFromResponse);
                 //   break;
                case AppConstants.TYPE_GET_GIFT_PRODUCTS://1039: // get_gift_products
                    iAuthSignalHandler.PROCESS_TYPE_GET_WALLET_GIFT_1039(_JobjFromResponse);
                    break;
                case  AppConstants.TYPE_GET_COIN_EARNING_RULE://1045: // coin earning rule list
                    iAuthSignalHandler.PROCESS_TYPE_GET_COIN_EARNING_RULE_LIST_1045(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_COIN_BUNDLE_LIST:// 1038: // coin_bundle_list
                    iAuthSignalHandler.PROCESS_TYPE_GET_WALLET_COIN_BUNDLE_LIST_1038(_JobjFromResponse);
                    break;
                //case AppConstants.TYPE_GET_TOP_CONTRIBUTORS:// 1044://contributor_list

                //    break;
                case AppConstants.TYPE_DAILY_CHECKIN_HISTORY://1054:
                    iAuthSignalHandler.PROCESS_TYPE_GET_DAILY_CHECKIN_HISTORY_1054(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_PAYMENT_RECEIVED://1046://payment received
                    iAuthSignalHandler.PROCESS_TYPE_PAYMENT_RECEIVED_1046(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_REFERRAL_NETWORK_SUMMARY:
                    iAuthSignalHandler.PROCESS_TYPE_REFERRAL_NETWORK_SUMMARY_1041(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_MY_REFERRALS_LIST:
                    iAuthSignalHandler.PROCESS_TYPE_MY_REFERRAL_LIST_1042(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_RECEIVED_GIFT:
                    iAuthSignalHandler.PROCESS_TYPE_GIFT_RECEIVED_1052(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_FOLLOW_UNFOLLOW_USER:
                    iAuthSignalHandler.PROCESS_UPDATE_FOLLOW_UNFOLLOW_USER(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_START_LIVE_STREAM: //2001
                    iAuthSignalHandler.PROCESS_TYPE_LIVE_STREAM_REQUEST_2001(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_FEATURED_LIVE_STREAMS: //2004
                    iAuthSignalHandler.PROCESS_TYPE_GET_FEATURED_LIVE_STREAMS_REQUEST_2004(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_RECENT_STREAMS: //2005
                    iAuthSignalHandler.PROCESS_TYPE_GET_RECENT_STREAMS_REQUEST_2005(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_LIVE_STREAM: //2006
                    iAuthSignalHandler.PROCESS_UPDATE_LIVE_STREAM_2006(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_STREAM_CATEGORY_LIST: //2007
                    iAuthSignalHandler.PROCESS_TYPE_GET_STREAM_CATEGORY_LIST_REQUEST_2007(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_MOST_VIEWED_STREAMS: //2008
                    iAuthSignalHandler.PROCESS_TYPE_GET_MOST_VIEWED_STREAMS_2008(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_NEAREST_STREAMS: //2009
                    iAuthSignalHandler.PROCESS_TYPE_GET_NEAREST_STREAMS_2009(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS: //2010
                    iAuthSignalHandler.PROCESS_TYPE_SEARCH_LIVE_STREAMS_REQUEST_2010(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_STREAMING_LIKE_COUNT: //2012
                    iAuthSignalHandler.PROCESS_UPDATE_STREAMING_LIKE_COUNT_2012(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_FOLLOWING_STREAMS: //2013
                    iAuthSignalHandler.PROCESS_GET_FOLLOWING_STREAMS_REQUEST_2013(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_CATEGORY_WISE_STREAM_COUNT: //2014
                    iAuthSignalHandler.PROCESS_GET_CATEGORY_WISE_STREAM_COUNT_REQUEST_2014(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_CREATE_CHANNEL: //2015
                    iAuthSignalHandler.PROCESS_CREATE_CHANNEL_REQUEST_2015(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_CHANNEL_CATEGORY_LIST: //2016
                    iAuthSignalHandler.PROCESS_GET_CHANNEL_CATEGORY_LIST_REQUEST_2016(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_CHANNEL_INFO: //2017
                    iAuthSignalHandler.PROCESS_UPDATE_CHANNEL_INFO_REQUEST_2017(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_FOLLOWING_CHANNEL_LIST: //2019
                    iAuthSignalHandler.PROCESS_GET_FOLLOWING_CHANNEL_LIST_REQUEST_2019(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_MOST_VIEWED_CHANNEL_LIST: //2020
                    iAuthSignalHandler.PROCESS_GET_MOST_VIEWED_CHANNEL_LIST_REQUEST_2020(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_OWN_CHANNEL_LIST: //2021
                    iAuthSignalHandler.PROCESS_GET_OWN_CHANNEL_LIST_REQUEST_2021(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_CHANNEL_PROFILE_IMAGE: //2024
                    iAuthSignalHandler.PROCESS_ADD_CHANNEL_PROFILE_IMAGE_REQUEST_2024(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_CHANNEL_COVER_IMAGE: //2025
                    iAuthSignalHandler.PROCESS_ADD_CHANNEL_COVER_IMAGE_REQUEST_2025(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ADD_UPLOADED_CHANNEL_MEDIA: //2026
                    iAuthSignalHandler.PROCESS_ADD_UPLOADED_CHANNEL_MEDIA_2026(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_UPLOADED_CHANNEL_MEDIA: //2027
                    iAuthSignalHandler.PROCESS_GET_UPLOADED_CHANNEL_MEDIA_2027(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_CHANNEL_MEDIA_LIST: //2028
                    iAuthSignalHandler.PROCESS_GET_CHANNEL_MEDIA_LIST_REQUEST_2028(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_CHANNEL_MEDIA_STATUS: //2029
                    iAuthSignalHandler.PROCESS_UPDATE_CHANNEL_MEDIA_STATUS_2029(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_FEATURED_CHANNEL_LIST: //2030
                    iAuthSignalHandler.PROCESS_GET_FEATURED_CHANNEL_LIST_REQUEST_2030(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_UPDATE_CHANNEL_MEDIA_INFO: //2031
                    iAuthSignalHandler.PROCESS_UPDATE_CHANNEL_MEDIA_INFO_REQUEST_2031(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST: //2032
                    iAuthSignalHandler.PROCESS_SEARCH_CHANNEL_LIST_REQUEST_2032(_JobjFromResponse);
                    break;
                case AppConstants.TYPE_GET_CHANNEL_PLAYLIST: //2031
                    iAuthSignalHandler.PROCESS_GET_CHANNEL_PLAYLIST_REQUEST_2033(_JobjFromResponse);
                    break;
                default:
                    break;
            }
        }

        #endregion "Initit StartProcess"
    }
}

>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

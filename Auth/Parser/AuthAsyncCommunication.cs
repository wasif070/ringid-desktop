﻿using Auth.Parser;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Auth.AppInterfaces;

namespace Auth.Parser
{
    public class AuthAsyncCommunication : IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AuthAsyncCommunication).Name);

        private static AuthAsyncCommunication _Instance;
        private const int BUFFER_SIZE = 10240;
        private const int TIMEOUT_MILLISECONDS = 25;
        private bool _IsRunning = false;
        private Socket _Socket;
        private ManualResetEvent _ClientDone;
        private Thread _Thread = null;
        private IAuthSignalHandler iAuthSignalHandler;

        public static AuthAsyncCommunication Instance
        {
            get
            {
                _Instance = _Instance ?? new AuthAsyncCommunication();
                return AuthAsyncCommunication._Instance;
            }
        }

        //public void StartService()
        //{
        //    if (!(_Thread != null && _Thread.IsAlive))
        //    {
        //        _ClientDone = new ManualResetEvent(false);
        //        _Socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        //        if (DefaultSettings.DEBUG)
        //        {
        //            log.Info("StartService of AuthAsyncCommunication");
        //        }
        //        _Thread = new Thread(new ThreadStart(RunReceiver));
        //        _Thread.Name = this.GetType().Name;
        //        _Thread.Start();
        //    }
        //    else
        //    {
        //        //        log.Error("StartService of AuthAsyncCommunication Failed => Already Running");
        //    }
        //}
        public void StartService(IAuthSignalHandler iAuthSignalHandler)
        {
            if (!(_Thread != null && _Thread.IsAlive))
            {
                this.iAuthSignalHandler = iAuthSignalHandler;
                _ClientDone = new ManualResetEvent(false);
                _Socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                if (DefaultSettings.DEBUG)
                {
                    log.Info("StartService of AuthAsyncCommunication");
                }
                _Thread = new Thread(new ThreadStart(RunReceiver));
                _Thread.Name = this.GetType().Name;
                _Thread.Start();
            }
        }
        private void RunReceiver()
        {
            _IsRunning = true;
            while (_IsRunning)
            {
                ProcessReceive();
            }
        }

        private void ProcessReceive()
        {
            try
            {
                if (_Socket != null)
                {
                    SocketAsyncEventArgs socketAsyncEventArgs = new SocketAsyncEventArgs();
                    socketAsyncEventArgs.RemoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                    socketAsyncEventArgs.SetBuffer(new Byte[BUFFER_SIZE], 0, BUFFER_SIZE);
                    socketAsyncEventArgs.Completed += OnRecieveHandler;

                    _ClientDone.Reset();
                    _Socket.ReceiveFromAsync(socketAsyncEventArgs);
                    _ClientDone.WaitOne();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Receiving Auth Packet ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnRecieveHandler(object o, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                if (e.RemoteEndPoint.ToString().StartsWith(ServerAndPortSettings.AUTH_SERVER_IP))
                {
                    byte[] data = new byte[e.BytesTransferred];
                    Array.Copy(e.Buffer, 0, data, 0, e.BytesTransferred);
                    PacketProcessor processor = new PacketProcessor(data, iAuthSignalHandler);
                    processor.Run();
                }
            }
            else
            {
                //log.Error("Failed in Receiving Auth Packet ==> Status = " + e.SocketError.ToString());
            }
            _ClientDone.Set();

            e.Completed -= OnRecieveHandler;
            e.SetBuffer(null, 0, 0);
            e.Dispose();
        }

        public void Send(byte[] data)
        {
            try
            {
                if (_Socket != null && !String.IsNullOrWhiteSpace(ServerAndPortSettings.AUTH_SERVER_IP))
                {
                    SocketAsyncEventArgs socketAsyncEventArgs = new SocketAsyncEventArgs();
                    socketAsyncEventArgs.RemoteEndPoint = new IPEndPoint(IPAddress.Parse(ServerAndPortSettings.AUTH_SERVER_IP), ServerAndPortSettings.COMMUNICATION_PORT);
                    socketAsyncEventArgs.SetBuffer(data, 0, data.Length);
                    socketAsyncEventArgs.Completed += OnSendHandler;
                    _Socket.SendToAsync(socketAsyncEventArgs);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Sending Auth Packet ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void SendLog(byte[] data)
        {
            try
            {
                if (_Socket != null && !String.IsNullOrWhiteSpace(ServerAndPortSettings.AUTH_SERVER_IP))
                {
                    SocketAsyncEventArgs socketAsyncEventArgs = new SocketAsyncEventArgs();
                    socketAsyncEventArgs.RemoteEndPoint = new IPEndPoint(IPAddress.Parse("192.168.1.41"), 80);
                    socketAsyncEventArgs.SetBuffer(data, 0, data.Length);
                    socketAsyncEventArgs.Completed += OnSendHandler;
                    _Socket.SendToAsync(socketAsyncEventArgs);
                }
            }
            catch (Exception ex) { log.Error("Error in Sending Log Packet ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void OnSendHandler(object o, SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success) { log.Error("Failed in Sending Auth Packet ==> Status = " + e.SocketError.ToString()); }
            _ClientDone.Set();
            e.Completed -= OnSendHandler;
            e.SetBuffer(null, 0, 0);
            e.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
            }
            // free native resources
        }

        public void StopService()
        {
            try
            {
                _IsRunning = false;
                if (_Socket != null)
                {
                    _Socket.Shutdown(SocketShutdown.Both);
                    _Socket.Close();
                }
                Dispose();
                _Instance = null;
            }
            catch (Exception ex)
            {
                log.Error("Error in Closing Auth Socket ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Auth.Parser
{
    public class Parser
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(Parser).Name);
        public static CommonPacketAttributes ParsePacket(byte[] bytes, int offset, int len)
        {

            CommonPacketAttributes values = new CommonPacketAttributes();
            try
            {
                for (int index = offset; index < len; index++)
                {

                    int attribute = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);

                    int length = 0;

                    switch (attribute)
                    {

                        case AttributeCodes.SUCCESS:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.Success = (GetBool(bytes, index));
                                break;
                            }
                        case AttributeCodes.REASON_CODE:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.ReasonCode = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.MESSAGE:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.Message = (GetString(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.TOTAL_PACKET:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.TotalPackets = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.PACKET_NUMBER:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.PacketNumber = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.TOTAL_RECORDS:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.TotalRecords = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.USER_TABLE_IDS:
                            {
                                length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                                values.UserIDs = (GetBytes(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.USER_ID:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.UserID = (GetLong(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.USER_IDENTITY:
                            {
                                length = (bytes[index++] & 0xFF);
                                string value = (GetString(bytes, index, length));
                                long uID = 0;
                                try
                                {
                                    uID = long.Parse(value);
                                }
                                catch (Exception)
                                {
                                }
                                values.UserIdentity = uID;
                                break;
                            }
                        case AttributeCodes.USER_NAME:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.UserName = (GetString(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.PROFILE_IMAGE:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.ProfileImage = (GetString(bytes, index, length));
                                break;
                            }
                        //case AttributeCodes.PROFILE_IMAGE_ID:
                        //    {
                        //        length = (bytes[index++] & 0xFF);
                        //        values.ProfileImageId = (GetLong(bytes, index, length)); //TODO MUST
                        //        break;
                        //    }
                        case AttributeCodes.UPDATE_TIME:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.UpdateTime = (GetLong(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.CONTACT_UPDATE_TIME:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.ContactUpdateTime = (GetLong(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.CONTACT_TYPE:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.ContactType = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.NEW_CONTACT_TYPE:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.NewContactType = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.DELETED:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.Deleted = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.BLOCK_VALUE:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.BlockValue = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.FRIENDSHIP_STATUS:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.FriendshipStatus = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.CHANGE_REQUESTER:
                            {
                                length = (bytes[index++] & 0xFF);
                                values.IsChangeRequester = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.CONTACT:
                            {

                                length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);

                                byte[] contact_bytes = GetBytes(bytes, index, length);
                                List<CommonPacketAttributes> contactList = values.ContactList;
                                if (contactList != null)
                                {
                                    contactList.Add(ParsePacket(contact_bytes, 0, contact_bytes.Length));
                                    values.ContactList = (contactList);
                                }
                                else
                                {
                                    contactList = new List<CommonPacketAttributes>();
                                    contactList.Add(ParsePacket(contact_bytes, 0, contact_bytes.Length));
                                    values.ContactList = (contactList);
                                }
                                break;
                            }
                        case AttributeCodes.SESSION_ID:
                            {
                                length = GetInt(bytes, index++, 1);
                                values.SessionID = (GetString(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.MUTUAL_FRIEND_COUNT:
                            {
                                length = GetInt(bytes, index++, 1);
                                values.MutualFriendCount = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.CALL_ACCESS:
                            {
                                length = GetInt(bytes, index++, 1);
                                values.CallAccess = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.CHAT_ACCESS:
                            {
                                length = GetInt(bytes, index++, 1);
                                values.ChatAccess = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.FEED_ACCESS:
                            {
                                length = GetInt(bytes, index++, 1);
                                values.FeedAccess = (GetInt(bytes, index, length));
                                break;
                            }
                        case AttributeCodes.CONTACT_ADDED_TIME:
                            length = bytes[index++] & 0xFF;
                            values.ContactAddedTime = (GetLong(bytes, index, length));
                            break;
                        default:
                            {
                                length = (bytes[index++] & 0xFF);
                                break;
                            }
                    }
                    index += length - 1;
                }
            }
            catch (Exception e)
            {
                log.Error("Byte Packet Parse Exception ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
            return values;
        }

        public static BrokenPacketAttributes ParseBrokenPacket(byte[] bytes, int offset)
        {
            BrokenPacketAttributes values = new BrokenPacketAttributes();
            for (int index = offset; index < bytes.Length; index++)
            {
                int attribute = (bytes[index++] & 0xFF);
                int length = 0;

                switch (attribute)
                {
                    case AttributeCodes.ACTION:
                        {
                            length = (bytes[index++] & 0xFF);
                            values.Action = GetInt(bytes, index, length);
                            break;
                        }
                    case AttributeCodes.SERVER_PACKET_ID:
                        {
                            length = (bytes[index++] & 0xFF);
                            values.ServerPacketID = GetLong(bytes, index, length);
                            break;
                        }
                    case AttributeCodes.TOTAL_PACKET:
                        {
                            length = (bytes[index++] & 0xFF);
                            values.TotalPackets = GetInt(bytes, index, length);
                            break;
                        }
                    case AttributeCodes.PACKET_NUMBER:
                        {
                            length = (bytes[index++] & 0xFF);
                            values.PacketNumber = GetInt(bytes, index, length);
                            break;
                        }
                    case AttributeCodes.UNIQUE_KEY:
                        {
                            length = (bytes[index++] & 0xFF);
                            values.UniqueKey = GetLong(bytes, index, length);
                            break;
                        }
                    case AttributeCodes.DATA:
                        {
                            length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                            values.Data = GetBytes(bytes, index, length);
                            break;
                        }
                    default:
                        {
                            length = (bytes[index++] & 0xFF);
                            break;
                        }
                }
                index += length - 1;
            }

            return values;
        }

        public static int GetInt(byte[] data_bytes, int index, int length)
        {
            switch (length)
            {
                case 1:
                    {
                        return (data_bytes[index++] & 0xFF);
                    }
                case 2:
                    {
                        return (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
                    }
                case 4:
                    {
                        return (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
                    }
            }
            return 0;
        }

        public static long GetLong(byte[] data_bytes, int index, int length)
        {
            long result = 0;
            for (int i = (length - 1); i > -1; i--)
            {
                result |= (data_bytes[index++] & 0xFFL) << (8 * i);
            }
            return result;
        }

        public static String GetString(byte[] data_bytes, int index, int length)
        {
            byte[] str_bytes = new byte[length];
            Array.Copy(data_bytes, index, str_bytes, 0, length);
            return Encoding.UTF8.GetString(str_bytes);
        }

        public static String GetBigString(byte[] data_bytes, int index, int length)
        {
            byte[] str_bytes = new byte[length];
            Array.Copy(data_bytes, index, str_bytes, 0, length);
            return Encoding.UTF8.GetString(str_bytes);
        }

        public static byte[] GetBytes(byte[] received_bytes, int index, int length)
        {
            byte[] data_bytes = new byte[length];
            Array.Copy(received_bytes, index, data_bytes, 0, length);
            return data_bytes;
        }

        public static bool GetBool(byte[] data_bytes, int index)
        {
            int result = (data_bytes[index++] & 0xFF);
            return result == 1;
        }

        public static HeaderAttributes ParseHeader(byte[] bytes, int offset)
        {
            HeaderAttributes values = new HeaderAttributes();

            for (int index = offset; index < bytes.Length; index++)
            {
                int attribute = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);

                int length = 0;

                switch (attribute)
                {
                    case AttributeCodes.ACTION:
                        {
                            length = (bytes[index++] & 0xFF);
                            values.Action = GetInt(bytes, index, length);
                            break;
                        }
                    case AttributeCodes.SERVER_PACKET_ID:
                        {
                            length = (bytes[index++] & 0xFF);
                            values.ServerPacketID = GetLong(bytes, index, length);
                            break;
                        }
                    case AttributeCodes.CLIENT_PACKET_ID:
                        {
                            length = (bytes[index++] & 0xFF);
                            values.ClientPacketID = GetString(bytes, index, length);
                            break;
                        }
                    case AttributeCodes.WEB_UNIQUE_KEY:
                        {
                            length = GetInt(bytes, index++, 1);
                            break;
                        }
                    case AttributeCodes.WEB_TAB_ID:
                        {
                            length = GetInt(bytes, index++, 1);
                            break;
                        }
                    default:
                        values.HeaderLength = index - 2;
                        return values;
                }
                index += length - 1;
            }
            return values;
        }
    }
}

﻿using System;

namespace Auth.Parser
{
    public class HeaderAttributes
    {
        private int action;

        public int Action
        {
            get { return action; }
            set { action = value; }
        }
        private long serverPacketID;

        public long ServerPacketID
        {
            get { return serverPacketID; }
            set { serverPacketID = value; }
        }
        private string clientPacketID;

        public string ClientPacketID
        {
            get { return clientPacketID; }
            set { clientPacketID = value; }
        }
        private int headerLength;

        public int HeaderLength
        {
            get { return headerLength; }
            set { headerLength = value; }
        }



    }
}

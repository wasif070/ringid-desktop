﻿using log4net;
using System;
using System.Threading;

namespace Auth.Parser
{
    public class PacketQueueProcessor
    {
        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(PacketQueueProcessor).Name);
        private static PacketQueueProcessor instance;
        //    private Thread thread = null;
        private bool running = false;
        #endregion "Private Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                while (PacketRepository.Instance.ReceivedQueue.Count > 0)
                {
                    AuthMsgParser processor = null;
                    PacketRepository.Instance.ReceivedQueue.TryDequeue(out processor);
                    if (processor != null)
                    {
                        //PacketProcessor processor = new PacketProcessor(packet);
                        //PacketProcessor processor = new PacketProcessor(packet);
                        processor.Process();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(" Exception in Packet Enqueue ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
            finally
            {
                running = false;
            }
            //  instance = null;
        }
        #endregion "Private methods"

        #region "Public Methods"

        public static void CreateInstance()
        {
            //log.Info("PacketQueueProcessor => Start = " + (instance == null || instance.thread.IsAlive == false));
            if (instance == null)
            {
                instance = new PacketQueueProcessor();
            }
            instance.StartProcess();
        }

        public void StartProcess()
        {
            if (!running)
            {
                Thread trhd = new Thread(Run);
                trhd.Start();
            }
        }

        public bool IsRunning()
        {
            return running;
        }


        #endregion "Public methods"

    }
}

﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using Auth.utility;
using log4net;
using Models.Constants;

namespace Auth.Service.Chat
{
    public class ChatBackgroundDownloader
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatBackgroundDownloader).Name);

        private WebClient _Downloader = null;
        private string _ImageUrl = null;
        private string _RootPath = null;

        public delegate void OnProgressHandler(string imageUrl, int precentage);
        public event OnProgressHandler OnProgress;

        public delegate void OnCompleteHandler(string imageUrl, bool status);
        public event OnCompleteHandler OnComplete;

        public ChatBackgroundDownloader(string rootPath, string imageUrl)
        {
            this._RootPath = rootPath;
            this._ImageUrl = imageUrl;
            this._Downloader = new WebClient();
            this._Downloader.Headers.Set("x-app-version", AppConfig.VERSION_PC);
            this._Downloader.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            this._Downloader.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
        }

        public void Start()
        {
            try
            {
                string directoryPath = _RootPath + Path.DirectorySeparatorChar + _ImageUrl;
                string downloadUrl = ServerAndPortSettings.ChatBackgroundImageDownloadUrl + Path.AltDirectorySeparatorChar + ServerAndPortSettings.D_FULL + Path.AltDirectorySeparatorChar + _ImageUrl;

                ImageFile imageFile = new ImageFile(directoryPath);
                if (!imageFile.IsComplete)
                {
                    _Downloader.DownloadFileAsync(new Uri(downloadUrl), directoryPath);
                }
                else
                {
                    if (OnComplete != null)
                    {
                        OnComplete(_ImageUrl, true);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Downloading Failed => " + ex.Message + "\n" + ex.StackTrace);
                _Downloader.Dispose();

                if (OnComplete != null)
                {
                    OnComplete(_ImageUrl, false);
                }
            }
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            if (OnProgress != null)
            {
                OnProgress(_ImageUrl, e.ProgressPercentage);
            }
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled == false && e.Error == null)
            {
                _Downloader.Dispose();
                if (OnComplete != null)
                {
                    OnComplete(_ImageUrl, true);
                }
            }
            else
            {
                _Downloader.Dispose();
                if (OnComplete != null)
                {
                    OnComplete(_ImageUrl, false);
                }
            }

            
        }

    }
}

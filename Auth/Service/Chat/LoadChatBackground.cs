﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Auth.Service.Chat
{
    public class LoadChatBackground : BackgroundWorker
    {
        private readonly ILog log = LogManager.GetLogger(typeof(LoadChatBackground).Name);

        private string _RootPath;

        public delegate void FetchSingleBgImageHandler(ChatBgImageDTO imageDTO);
        public event FetchSingleBgImageHandler OnFetchSingleBgImage;

        public delegate void CompleteHandler();
        public event CompleteHandler OnComplete;

        public LoadChatBackground(string rootPath)
        {
            this._RootPath = rootPath;
            WorkerSupportsCancellation = true;
            DoWork += LoadChatBackground_DoWork;
        }

        public void Start()
        {
            RunWorkerAsync();
        }

        void LoadChatBackground_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ChatBgDTO chatBgDTO = HttpRequest.GetChatBackgroundImage();
                if (chatBgDTO == null || chatBgDTO.chatBgImageList == null)
                {
                    OnComplete();
                }
                else
                {
                    foreach (ChatBgImageDTO imageDTO in chatBgDTO.chatBgImageList)
                    {
                        if (CheckForCancel(e)) return;

                        string folderPath = _RootPath + Path.DirectorySeparatorChar + "thumb" + imageDTO.name;
                        string uri = ServerAndPortSettings.ChatBackgroundImageDownloadUrl + Path.AltDirectorySeparatorChar + ServerAndPortSettings.D_FULL + Path.AltDirectorySeparatorChar + "thumb" + imageDTO.name;

                        ImageFile file = new ImageFile(folderPath);
                        if (!file.IsComplete)
                        {
                            if (HttpRequest.DownloadFile(uri, folderPath))
                            {
                                OnFetchSingleBgImage(imageDTO);
                            }
                        }
                        else
                        {
                            OnFetchSingleBgImage(imageDTO);
                        }
                    }

                    OnComplete();
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadChatBackground_DoWork ==> " + ex.Message + "\n" + ex.StackTrace);
                OnComplete();
            }
        }

        private bool CheckForCancel(DoWorkEventArgs e)
        {
            if (CancellationPending)
            {
                e.Cancel = true;
                return true;
            }
            return false;
        }
    }
}

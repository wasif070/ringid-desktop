﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Auth.Service.FriendProfile
{
    //public class AcceptFriendRequest
    //{
    //    private long userIdentity;
    //    private long userTableId;
    //    private readonly ILog log = LogManager.GetLogger(typeof(AcceptFriendRequest).Name);

    //    public AcceptFriendRequest(long userIdentity, long userTableId)
    //    {
    //        this.userIdentity = userIdentity;
    //        this.userTableId = userTableId;
    //    }

    //    public void Run(out bool isSuccess, out UserBasicInfoDTO userBasicInfo, out string msg)
    //    {
    //        msg = "";
    //        isSuccess = false;
    //        userBasicInfo = null;
    //        if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //        {
    //            try
    //            {
    //                JObject pakToSend = new JObject();
    //                String pakId = SendToServer.GetRanDomPacketID();
    //                pakToSend[JsonKeys.PacketId] = pakId;
    //                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACCEPT_FRIEND;
    //                pakToSend[JsonKeys.UserTableId] = userTableId;

    //                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //                SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //                Thread.Sleep(25);
    //                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
    //                {
    //                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
    //                    {
    //                        break;
    //                    }
    //                    Thread.Sleep(DefaultSettings.WAITING_TIME);
    //                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
    //                    {
    //                        if (i % DefaultSettings.SEND_INTERVAL == 0)
    //                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //                    }
    //                    else
    //                    {
    //                        JObject feedbackfields = null;
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
    //                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
    //                        {
    //                            isSuccess = true;
    //                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userIdentity, out userBasicInfo);
    //                            if (userBasicInfo != null)
    //                            {

    //                                if (feedbackfields[JsonKeys.UserIdentity] != null)
    //                                {
    //                                    userBasicInfo.UserIdentity = (long)feedbackfields[JsonKeys.UserIdentity];
    //                                }
    //                                if (feedbackfields[JsonKeys.FullName] != null)
    //                                {
    //                                    userBasicInfo.FullName = (string)feedbackfields[JsonKeys.FullName];
    //                                }
    //                                if (feedbackfields[JsonKeys.ProfileImage] != null)
    //                                {
    //                                    userBasicInfo.ProfileImage = (string)feedbackfields[JsonKeys.ProfileImage];
    //                                }
    //                                if (feedbackfields[JsonKeys.ProfileImageId] != null)
    //                                {
    //                                    userBasicInfo.ProfileImageId = (long)feedbackfields[JsonKeys.ProfileImageId];
    //                                }
    //                                if (feedbackfields[JsonKeys.FriendshipStatus] != null)
    //                                {
    //                                    userBasicInfo.FriendShipStatus = (int)feedbackfields[JsonKeys.FriendshipStatus];
    //                                }
    //                                if (feedbackfields[JsonKeys.UpdateTime] != null)
    //                                {
    //                                    userBasicInfo.UpdateTime = (long)feedbackfields[JsonKeys.UpdateTime];
    //                                }
    //                                if (userBasicInfo.BlockedBy != 1)
    //                                {
    //                                    if (feedbackfields[JsonKeys.CallAccess] != null)
    //                                    {
    //                                        userBasicInfo.CallAccess = (int)feedbackfields[JsonKeys.CallAccess];
    //                                    }
    //                                    if (feedbackfields[JsonKeys.ChatAccess] != null)
    //                                    {
    //                                        userBasicInfo.ChatAccess = (int)feedbackfields[JsonKeys.ChatAccess];
    //                                    }
    //                                    if (feedbackfields[JsonKeys.FeedAccess] != null)
    //                                    {
    //                                        userBasicInfo.FeedAccess = (int)feedbackfields[JsonKeys.FeedAccess];
    //                                    }
    //                                }
    //                                else
    //                                {
    //                                    userBasicInfo.CallAccess = 0;
    //                                    userBasicInfo.ChatAccess = 0;
    //                                    userBasicInfo.FeedAccess = 0;
    //                                }

    //                                //HelperMethodsAuth.AuthHandlerInstance.SetRequestsForFriendViewedandAdded(userBasicInfo);
    //                                //List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
    //                                //contactList.Add(userBasicInfo);
    //                                //new InsertIntoUserBasicInfoTable(contactList).Start();
    //                                //HelperMethodsAuth.AuthHandlerInstance.CheckFriendPresence(userBasicInfo.UserIdentity);

    //                                List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
    //                                userBasicInfoList.Add(userBasicInfo);
    //                                new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();

    //                                HelperMethodsAuth.AuthHandlerInstance.UI_AcceptFriendRequest(userBasicInfo);
    //                            }
    //                        }
    //                        else
    //                        {
    //                            if (feedbackfields[JsonKeys.ReasonCode] != null)
    //                            {
    //                                msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
    //                            }
    //                        }
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
    //                        return;
    //                    }
    //                    PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
    //                }

    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("AcceptFriendRequest ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }
    //        else
    //        {
    //            log.Error("AcceptFriendRequest Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //            // msg = NotificationMessages.INTERNET_UNAVAILABLE;
    //        }
    //    }
    //}
}

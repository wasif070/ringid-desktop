﻿
namespace Auth.Service.FriendProfile
{
    //public class FriendPresenceInfo
    //{
    //    private long friendIdentity;
    //    private List<long> friendIdentityList;
    //    private Func<int, int, bool, bool> _OnComplete = null;
    //    private readonly ILog log = LogManager.GetLogger(typeof(FriendPresenceInfo).Name);

    //    public FriendPresenceInfo(long friendIdentity, Func<int, int, bool, bool> onComplete = null)
    //    {
    //        this.friendIdentity = friendIdentity;
    //        this._OnComplete = onComplete;
    //        Thread th = new Thread(new ThreadStart(run));
    //        th.Start();
    //    }

    //    public FriendPresenceInfo(List<long> friendIdentityList)
    //    {
    //        this.friendIdentityList = friendIdentityList;
    //        Thread th = new Thread(new ThreadStart(run));
    //        th.Start();
    //    }

    //    private void run()
    //    {
    //        if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //        {
    //            try
    //            {
    //                JObject pakToSend = new JObject();
    //                string pakId = SendToServer.GetRanDomPacketID();
    //                pakToSend[JsonKeys.PacketId] = pakId;
    //                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //                if (friendIdentityList != null)
    //                {
    //                    JArray jArray = new JArray();
    //                    foreach (long uID in friendIdentityList)
    //                    {
    //                        jArray.Add((object)uID.ToString());
    //                    }
    //                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_MULTIPLE_FRIEND_PRESENCE_INFO;
    //                    pakToSend[JsonKeys.UIDs] = jArray;
    //                }
    //                else
    //                {
    //                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_SINGLE_FRIEND_PRESENCE_INFO;
    //                    pakToSend[JsonKeys.FriendId] = this.friendIdentity.ToString();
    //                }


    //                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //                SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
    //                Thread.Sleep(25);
    //                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
    //                {
    //                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
    //                    {
    //                        break;
    //                    }
    //                    Thread.Sleep(DefaultSettings.WAITING_TIME);
    //                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
    //                    {
    //                        if (i % DefaultSettings.SEND_INTERVAL == 0)
    //                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
    //                    }
    //                    else
    //                    {
    //                        JObject feedBackFields = null;
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedBackFields);

    //                        if (feedBackFields != null)
    //                        {
    //                            int presence = 0;
    //                            if (feedBackFields[JsonKeys.Presence] != null)
    //                            {
    //                                presence = (int)feedBackFields[JsonKeys.Presence];
    //                            }
    //                            int mood = 0;
    //                            if (feedBackFields[JsonKeys.Mood] != null)
    //                            {
    //                                mood = (int)feedBackFields[JsonKeys.Mood];
    //                            }

    //                            Callback(presence, mood, true);
    //                        }
    //                        else
    //                        {
    //                            Callback(0, 0, false);
    //                        }

    //                        return;
    //                    }
    //                    PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
    //                }

    //                Callback(0, 0, false);
    //            }
    //            catch (Exception e)
    //            {
    //                Callback(0, 0, false);
    //                log.Error("FriendPresenceInfo ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }
    //        else
    //        {
    //            Callback(0, 0, false);
    //            log.Error("FriendPresenceInfo Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //        }
    //    }

    //    private void Callback(int presence, int mood, bool status)
    //    {
    //        try
    //        {
    //            if (_OnComplete != null)
    //            {
    //                _OnComplete(presence, mood, status);
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
    //        }
    //        _OnComplete = null;
    //    }
    //}
}

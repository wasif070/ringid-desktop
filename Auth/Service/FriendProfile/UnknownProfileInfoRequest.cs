﻿namespace Auth.Service.FriendProfile
{
    //public class UnknownProfileInfoRequest
    //{
    //    private long userIdentity;
    //    private long userTableId;
    //    private readonly ILog log = LogManager.GetLogger(typeof(UnknownProfileInfoRequest).Name);
    //    public delegate void OnSuccessHandler(UserBasicInfoDTO userDTO);
    //    public event OnSuccessHandler OnSuccess;
    //    private bool InsertIntoDB;
    //    private bool alreadyFriendRequested;

    //    public UnknownProfileInfoRequest(long userIdentity, long userTableId, bool insertIntoDB = false, bool alreadyFriendRequested = false)
    //    {
    //        this.userIdentity = userIdentity;
    //        this.userTableId = userTableId;
    //        this.InsertIntoDB = insertIntoDB;
    //        this.alreadyFriendRequested = alreadyFriendRequested;
    //    }

    //    public void Start()
    //    {
    //        Thread th = new Thread(new ThreadStart(run));
    //        th.Name = this.GetType().Name;
    //        th.Start();
    //    }

    //    public void run()
    //    {
    //        if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //        {
    //            try
    //            {
    //                JObject pakToSend = new JObject();
    //                string pakId = SendToServer.GetRanDomPacketID();
    //                pakToSend[JsonKeys.PacketId] = pakId;
    //                pakToSend[JsonKeys.Action] = AppConstants.TYPE_UNKNWON_PROFILE_INFO;
    //                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //                if (this.userIdentity > 0)
    //                {
    //                    pakToSend[JsonKeys.UserIdentity] = this.userIdentity;
    //                }
    //                else
    //                {
    //                    pakToSend[JsonKeys.UserTableId] = this.userTableId;
    //                }

    //                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //                SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
    //                Thread.Sleep(25);
    //                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
    //                {
    //                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
    //                    {
    //                        break;
    //                    }
    //                    Thread.Sleep(DefaultSettings.WAITING_TIME);
    //                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
    //                    {
    //                        if (i % DefaultSettings.SEND_INTERVAL == 0)
    //                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
    //                    }
    //                    else
    //                    {
    //                        JObject FeedBackFields = null;
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out FeedBackFields);

    //                        if (FeedBackFields != null && (bool)FeedBackFields[JsonKeys.Success])
    //                        {
    //                            JObject UserDetails = (JObject)FeedBackFields[JsonKeys.UserDetails];

    //                            long uID = 0;
    //                            if (UserDetails[JsonKeys.UserIdentity] != null)
    //                            {
    //                                uID = (long)UserDetails[JsonKeys.UserIdentity];
    //                            }

    //                            UserBasicInfoDTO unknownProfileDTO = null;
    //                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(uID, out unknownProfileDTO);
    //                            if (unknownProfileDTO == null)
    //                            {
    //                                unknownProfileDTO = new UserBasicInfoDTO();
    //                                FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[uID] = unknownProfileDTO;
    //                            }
    //                            unknownProfileDTO.UserIdentity = uID;

    //                            if (UserDetails[JsonKeys.FullName] != null)
    //                            {
    //                                unknownProfileDTO.FullName = (string)UserDetails[JsonKeys.FullName];
    //                            }
    //                            if (UserDetails[JsonKeys.ProfileImage] != null)
    //                            {
    //                                unknownProfileDTO.ProfileImage = (string)UserDetails[JsonKeys.ProfileImage];
    //                            }
    //                            if (UserDetails[JsonKeys.FriendshipStatus] != null)
    //                            {
    //                                unknownProfileDTO.FriendShipStatus = (int)UserDetails[JsonKeys.FriendshipStatus];
    //                            }
    //                            if (UserDetails[JsonKeys.CallAccess] != null)
    //                            {
    //                                unknownProfileDTO.CallAccess = (int)UserDetails[JsonKeys.CallAccess];
    //                            }
    //                            if (UserDetails[JsonKeys.ChatAccess] != null)
    //                            {
    //                                unknownProfileDTO.ChatAccess = (int)UserDetails[JsonKeys.ChatAccess];
    //                            }
    //                            if (UserDetails[JsonKeys.FeedAccess] != null)
    //                            {
    //                                unknownProfileDTO.FeedAccess = (int)UserDetails[JsonKeys.FeedAccess];
    //                            }
    //                            if (UserDetails[JsonKeys.UserTableId] != null)
    //                            {
    //                                unknownProfileDTO.UserTableId = (long)UserDetails[JsonKeys.UserTableId];
    //                            }

    //                            if (this.userTableId == DefaultSettings.RINGID_OFFICIAL_UTID)
    //                            {
    //                                unknownProfileDTO.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_ACCEPTED;
    //                                unknownProfileDTO.ContactType = SettingsConstants.SPECIAL_CONTACT;
    //                                unknownProfileDTO.CallAccess = 1;
    //                                unknownProfileDTO.ChatAccess = 1;
    //                                unknownProfileDTO.FeedAccess = 1;
    //                            }

    //                            unknownProfileDTO.BirthDay = 1;
    //                            unknownProfileDTO.MarriageDay = 1;

    //                            if (InsertIntoDB)
    //                            {
    //                                FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[uID] = unknownProfileDTO;
    //                                List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
    //                                userList.Add(unknownProfileDTO);
    //                                new InsertIntoUserBasicInfoTable(userList).Start();
    //                            }
    //                            if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.ContainsKey(unknownProfileDTO.UserTableId))
    //                                FriendDictionaries.Instance.UTID_UID_DICTIONARY[unknownProfileDTO.UserTableId] = uID;
    //                            else
    //                                FriendDictionaries.Instance.UTID_UID_DICTIONARY.Add(unknownProfileDTO.UserTableId, uID);

    //                            HelperMethodsAuth.AuthHandlerInstance.UI_UnknownProfileInfoRequest(unknownProfileDTO, alreadyFriendRequested);
    //                            //HelperMethodsAuth.AuthHandlerInstance.ChangeFriendProfile(unknownProfileDTO);
    //                            //HelperMethodsAuth.AuthHandlerInstance.ShowFriendBasicInfo(unknownProfileDTO);
    //                            //HelperMethodsAuth.AuthHandlerInstance.ChangeCallChatLogsOnFriendUpdate(unknownProfileDTO.UserIdentity);
    //                            //if (alreadyFriendRequested)
    //                            //{
    //                            //    HelperMethodsAuth.AuthHandlerInstance.AddOrAcceptSingleFriendUI(unknownProfileDTO);
    //                            //}

    //                            Callback(unknownProfileDTO);
    //                        }
    //                        else
    //                        {
    //                            Callback(null);
    //                        }

    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out FeedBackFields);
    //                        return;
    //                    }
    //                    PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("UnknownProfileInfoRequest ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }
    //        else
    //        {
    //            log.Error("UnknownProfileInfoRequest Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //        }

    //        Callback(null);
    //    }

    //    private void Callback(UserBasicInfoDTO unknownProfileDTO)
    //    {
    //        try
    //        {
    //            if (OnSuccess != null)
    //            {
    //                OnSuccess(unknownProfileDTO);
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
    //        }
    //        OnSuccess = null;
    //    }

    //}
}

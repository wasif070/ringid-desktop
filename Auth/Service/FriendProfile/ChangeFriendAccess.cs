﻿
namespace Auth.Service.FriendProfile
{
    //public class ChangeFriendAccess
    //{
    //    private readonly ILog log = LogManager.GetLogger(typeof(ChangeFriendAccess).Name);
    //    private long _UserIdentity;
    //    private long _UserTableId;
    //    private SettingsDTO _Settings;

    //    public delegate void CompleteHandler(SettingsDTO settings, bool status, string errorMsg);
    //    public event CompleteHandler OnComplete;

    //    public ChangeFriendAccess(long userIdentity, long userTableId, SettingsDTO settings)
    //    {
    //        this._UserIdentity = userIdentity;
    //        this._UserTableId = userTableId;
    //        this._Settings = settings;
    //    }

    //    public void Start()
    //    {
    //        Thread th = new Thread(Run);
    //        th.Name = this.GetType().Name;
    //        th.Start();
    //    }

    //    public void Run()
    //    {
    //        if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //        {

    //            try
    //            {
    //                JObject pakToSend = new JObject();
    //                String pakId = SendToServer.GetRanDomPacketID();
    //                pakToSend[JsonKeys.PacketId] = pakId;
    //                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //                pakToSend[JsonKeys.Action] = AppConstants.TYPE_UPDATE_CONTACT_ACCESS;
    //                pakToSend[JsonKeys.SettingsName] = _Settings.SettingsName;
    //                pakToSend[JsonKeys.SettingsValue] = _Settings.SettingsValue;
    //                pakToSend[JsonKeys.UserTableId] = _UserTableId;

    //                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //                SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //                Thread.Sleep(25);
    //                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
    //                {
    //                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
    //                    {
    //                        break;
    //                    }
    //                    Thread.Sleep(DefaultSettings.WAITING_TIME);
    //                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
    //                    {
    //                        if (i % DefaultSettings.SEND_INTERVAL == 0)
    //                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //                    }
    //                    else
    //                    {
    //                        JObject feedbackfields = null;
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);

    //                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
    //                        {
    //                            UserBasicInfoDTO userBasicInfo = null;
    //                            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(_UserIdentity, out userBasicInfo);
    //                            if (userBasicInfo != null)
    //                            {
    //                                if (feedbackfields[JsonKeys.SettingsName] != null && feedbackfields[JsonKeys.SettingsValue] != null)
    //                                {
    //                                    if ((int)feedbackfields[JsonKeys.SettingsName] == StatusConstants.CALL_ACCESS)
    //                                    {
    //                                        userBasicInfo.CallAccess = (int)feedbackfields[JsonKeys.SettingsValue];
    //                                    }
    //                                    else if ((int)feedbackfields[JsonKeys.SettingsName] == StatusConstants.CHAT_ACCESS)
    //                                    {
    //                                        userBasicInfo.ChatAccess = (int)feedbackfields[JsonKeys.SettingsValue];
    //                                    }
    //                                    else if ((int)feedbackfields[JsonKeys.SettingsName] == StatusConstants.FEED_ACCESS)
    //                                    {
    //                                        userBasicInfo.FeedAccess = (int)feedbackfields[JsonKeys.SettingsValue];
    //                                    }

    //                                    List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
    //                                    contactList.Add(userBasicInfo);
    //                                    new InsertIntoUserBasicInfoTable(contactList).Start();
    //                                }
    //                            }
 
    //                            Callback(true, String.Empty);
    //                        }
    //                        else
    //                        {
    //                            String msg = String.Empty;
    //                            if (feedbackfields[JsonKeys.Message] != null)
    //                            {
    //                                msg = (string)feedbackfields[JsonKeys.Message];
    //                            }
    //                            else if (feedbackfields[JsonKeys.ReasonCode] != null)
    //                            {
    //                                msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
    //                            }

    //                            Callback(false, msg);
    //                        }
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
    //                        return;
    //                    }
    //                    PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("ChangeFriendAccess ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }
    //        else
    //        {
    //            log.Error("ChangeFriendAccess Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //        }

    //        Callback(false, String.Empty);
    //    }

    //    private void Callback(bool status, string errorMsg)
    //    {
    //        try
    //        {
    //            if (OnComplete != null)
    //            {
    //                OnComplete(_Settings, status, errorMsg);
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
    //        }
    //        OnComplete = null;
    //    }
    //}
}

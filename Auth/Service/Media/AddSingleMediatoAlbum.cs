﻿
namespace Auth.Service.Media
{
    //public class AddSingleMediatoAlbum
    //{
    //    private readonly ILog log = LogManager.GetLogger(typeof(AddSingleMediatoAlbum).Name);
    //    private int mediaType, th, tw, mpvc;
    //    private string ttl, artist, streamUrl, thumbUrl;
    //    private long albumId, drtn;

    //    //public AddSingleMediatoAlbum(int mediaType, long albumId, string streamUrl, string thumbUrl, long drtn, string ttl, string artist, int tw, int th, int mpvc)
    //    //{
    //    //    this.mpvc = mpvc;
    //    //    this.mediaType = mediaType;
    //    //    this.albumId = albumId;
    //    //    this.streamUrl = streamUrl;
    //    //    this.thumbUrl = thumbUrl;
    //    //    this.drtn = drtn;
    //    //    this.ttl = ttl;
    //    //    this.artist = artist;
    //    //    this.tw = tw;
    //    //    this.th = th;
    //    //}

    //    //public void StartThread()
    //    //{
    //    //    Thread thread = new Thread(new ThreadStart(Run));
    //    //    thread.Start();
    //    //}

    //    //public void Run()
    //    //{
    //    //    if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //    //    {
    //    //        try
    //    //        {
    //    //            JObject pakToSend = new JObject();
    //    //            String pakId = SendToServer.GetRanDomPacketID();
    //    //            pakToSend[JsonKeys.PacketId] = pakId;
    //    //            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //    //            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_MEDIA_CONTENT;
    //    //            pakToSend[JsonKeys.AlbumId] = albumId;
    //    //            pakToSend[JsonKeys.MediaType] = mediaType;
    //    //            //if (mpvc > 0) pakToSend[JsonKeys.MediaPrivacy] = mpvc;

    //    //            JArray mediasToSend = new JArray();
    //    //            JObject obj = new JObject();
    //    //            obj[JsonKeys.StreamUrl] = streamUrl;
    //    //            obj[JsonKeys.Title] = ttl;
    //    //            obj[JsonKeys.MediaDuration] = drtn;
    //    //            if (mpvc > 0) obj[JsonKeys.MediaPrivacy] = mpvc;
    //    //            if (!string.IsNullOrEmpty(thumbUrl))
    //    //            {
    //    //                obj[JsonKeys.ThumbUrl] = thumbUrl;
    //    //                obj[JsonKeys.ThumbImageWidth] = tw;
    //    //                obj[JsonKeys.ThumbImageHeight] = th;
    //    //            }
    //    //            if (!string.IsNullOrEmpty(artist)) obj[JsonKeys.Artist] = artist;
    //    //            mediasToSend.Add(obj);
    //    //            pakToSend[JsonKeys.MediaList] = mediasToSend;

    //    //            string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //    //            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //    //            for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
    //    //            {
    //    //                Thread.Sleep(DefaultSettings.WAITING_TIME);
    //    //                if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
    //    //                {
    //    //                    if (i % 9 == 0)
    //    //                        SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //    //                }
    //    //                else
    //    //                {
    //    //                    JObject feedbackfields = null;
    //    //                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
    //    //                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.MediaIds] != null
    //    //                        && feedbackfields[JsonKeys.AlbumId] != null && (long)feedbackfields[JsonKeys.AlbumId] == albumId)
    //    //                    {
    //    //                        JArray array = (JArray)feedbackfields[JsonKeys.MediaIds];
    //    //                        long contentId = (long)array.ElementAt(0);
    //    //                        Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
    //    //                        if (DICT_BY_UTID.ContainsKey(DefaultSettings.LOGIN_USER_TABLE_ID))
    //    //                        {
    //    //                            MediaContentDTO album = null;
    //    //                            if (DICT_BY_UTID[DefaultSettings.LOGIN_USER_TABLE_ID].TryGetValue(albumId, out album))
    //    //                            {
    //    //                                if (album.MediaList == null) album.MediaList = new List<SingleMediaDTO>();
    //    //                                SingleMediaDTO dto = new SingleMediaDTO { ContentId = contentId, AlbumId = albumId, AlbumName = album.AlbumName, Title = ttl, Duration = drtn, MediaType = mediaType, StreamUrl = streamUrl };
    //    //                                if (!string.IsNullOrEmpty(thumbUrl)) { dto.ThumbUrl = thumbUrl; dto.ThumbImageHeight = th; dto.ThumbImageWidth = tw; };
    //    //                                if (!string.IsNullOrEmpty(artist)) obj[JsonKeys.Artist] = artist;
    //    //                                album.TotalMediaCount += 1;
    //    //                                album.MediaList.Insert(0, dto);
    //    //                                HelperMethodsAuth.NewsFeedHandlerInstance.AddSingleMediatoMyModels(mediaType, album.TotalMediaCount, albumId, dto);
    //    //                                HelperMethodsAuth.AuthHandlerInstance.ShowTimerMessageBox(NotificationMessages.ADDING_MEDIA_CONFIRMATION, "Success!");
    //    //                            }
    //    //                        }
    //    //                    }
    //    //                    else
    //    //                    {
    //    //                        string ms = (feedbackfields[JsonKeys.Message] != null) ? (string)feedbackfields[JsonKeys.Message] : "Failed to Add Media to the Album!";
    //    //                        // HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(ms, "Failed!");
    //    //                        HelperMethodsAuth.AuthHandlerInstance.ShowTimerMessageBox(ms, "Failed!");
    //    //                    }
    //    //                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
    //    //                    HelperMethodsAuth.NewsFeedHandlerInstance.EnableAddtoExistingOrNewAlbumPopup(albumId);
    //    //                    return;
    //    //                }
    //    //                PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
    //    //            }
    //    //        }
    //    //        catch (Exception e)
    //    //        {
    //    //            log.Error("AddSingleMediatoAlbum ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //    //        }
    //    //    }
    //    //    else
    //    //    {
    //    //        log.Error("AddSingleMediatoAlbum Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //    //        //msg = NotificationMessages.INTERNET_UNAVAILABLE;
    //    //    }
    //    //    HelperMethodsAuth.NewsFeedHandlerInstance.EnableAddtoExistingOrNewAlbumPopup(albumId);
    //    //    string msg = (DefaultSettings.IsInternetAvailable) ? "" : ReasonCodeConstants.REASON_MSG_INTERNET_UNAVAILABLE;
    //    //    HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox("Failed to Add Media to the Album!" + msg, "Failed!");
    //    //}
    //}
}

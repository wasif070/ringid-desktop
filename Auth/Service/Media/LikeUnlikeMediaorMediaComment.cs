﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using Newtonsoft.Json;

namespace Auth.Service.Media
{
    //public class LikeUnlikeMediaorMediaComment
    //{
    //    private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(LikeUnlikeMediaorMediaComment).Name);
    //    private long contentId, cmntId, nfId, utId;
    //    private int liked, mediaType;
    //    private string msg = "Failed to Like Media!";
    //    public LikeUnlikeMediaorMediaComment(long nfId, int liked, long contentId, long cmntId, long utId = 0, int mediaType = 0)
    //    {
    //        this.nfId = nfId;
    //        this.contentId = contentId;
    //        this.cmntId = cmntId;
    //        this.liked = liked;
    //        this.utId = utId;
    //        this.mediaType = mediaType;
    //        Thread thd = new Thread(Run);
    //        thd.Name = this.GetType().Name;
    //        thd.Start();
    //    }
    //    private void Run()
    //    {
    //        bool success = false;
    //        if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //        {
    //            try
    //            {
    //                JObject pakToSend = new JObject();
    //                string pakId = SendToServer.GetRanDomPacketID();
    //                pakToSend[JsonKeys.PacketId] = pakId;
    //                if (cmntId > 0)
    //                {
    //                    pakToSend[JsonKeys.CommentId] = cmntId;
    //                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_LIKE_UNLIKE_MEDIA_COMMENT;
    //                }
    //                else
    //                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_LIKE_UNLIKE_MEDIA;
    //                if (nfId > 0)
    //                    pakToSend[JsonKeys.NewsfeedId] = nfId;
    //                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //                pakToSend[JsonKeys.ContentId] = this.contentId;
    //                pakToSend[JsonKeys.Liked] = this.liked;

    //                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //                SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //                Thread.Sleep(25);
    //                for (int pakSendingAttempt = 1; pakSendingAttempt <= DefaultSettings.TRYING_TIME; pakSendingAttempt++)
    //                {
    //                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
    //                    {
    //                        break;
    //                    }
    //                    Thread.Sleep(DefaultSettings.WAITING_TIME);
    //                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
    //                    {
    //                        if (pakSendingAttempt % 9 == 0)
    //                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //                    }
    //                    else
    //                    {
    //                        JObject feedbackfields = null;
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
    //                        if (feedbackfields[JsonKeys.Success] != null)
    //                        {
    //                            if ((bool)feedbackfields[JsonKeys.Success]
    //                                || (feedbackfields[JsonKeys.ReasonCode] != null && ((int)feedbackfields[JsonKeys.ReasonCode]) == 34))
    //                            {
    //                                success = true;
    //                                if (cmntId > 0)
    //                                {
    //                                    Dictionary<long, CommentDTO> CommentsOfThisItem = null;
    //                                    if (NewsFeedDictionaries.Instance.TEMP_MEDIA_COMMENT_LIST.TryGetValue(contentId, out CommentsOfThisItem)
    //                                        && CommentsOfThisItem.ContainsKey(cmntId))
    //                                    {
    //                                        if (liked == 1)
    //                                        {
    //                                            CommentsOfThisItem[cmntId].TotalLikeComment = CommentsOfThisItem[cmntId].TotalLikeComment + 1;
    //                                            CommentsOfThisItem[cmntId].ILikeComment = 1;
    //                                        }
    //                                        else if (CommentsOfThisItem[cmntId].TotalLikeComment > 0)
    //                                        {
    //                                            CommentsOfThisItem[cmntId].TotalLikeComment = CommentsOfThisItem[cmntId].TotalLikeComment - 1;
    //                                            CommentsOfThisItem[cmntId].ILikeComment = 0;
    //                                        }
    //                                    }
    //                                }
    //                                else if (utId > 0 && mediaType > 0)
    //                                {
    //                                    Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
    //                                    Dictionary<long, MediaContentDTO> albumsOfaUtId = null;
    //                                    if (DICT_BY_UTID.TryGetValue(utId, out albumsOfaUtId))
    //                                    {
    //                                        for (int i = 0; i < albumsOfaUtId.Count; i++)
    //                                        {
    //                                            //albumsOfaUtId.ElementAt(i).Value.MediaList
    //                                            if (albumsOfaUtId.ElementAt(i).Value.MediaList != null && albumsOfaUtId.ElementAt(i).Value.MediaList.Count > 0)
    //                                            {
    //                                                SingleMediaDTO singleMedia = albumsOfaUtId.ElementAt(i).Value.MediaList.Where(P => P.ContentId == contentId).FirstOrDefault();
    //                                                if (singleMedia != null)
    //                                                {
    //                                                    if (liked == 1) { singleMedia.LikeCount++; singleMedia.ILike = 1; }
    //                                                    else if (singleMedia.LikeCount > 0)
    //                                                    { singleMedia.LikeCount--; singleMedia.ILike = 0; }
    //                                                    break;
    //                                                }
    //                                            }
    //                                        }
    //                                    }
    //                                }
    //                            }
    //                            if (feedbackfields[JsonKeys.Message] != null)
    //                            {
    //                                msg = (string)feedbackfields[JsonKeys.Message];
    //                            }
    //                        }
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
    //                        break;
    //                    }
    //                    PingInServer.StartThread(pakSendingAttempt, DefaultSettings.TRYING_TIME);
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Error in Packet type ==>" + AppConstants.REQUEST_TYPE_UPDATE + "==>" + e.Message + "\n" + e.StackTrace);
    //            }
    //        }
    //        else
    //        {
    //            log.Info("AuthRequestNoResult Failed => Packet type ==>" + AppConstants.REQUEST_TYPE_UPDATE + " ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //        }
    //        if (!success) HelperMethodsAuth.NewsFeedHandlerInstance.OnMediaorMediaCommentLikeUnlikeFailed(liked, nfId, contentId, cmntId, msg);
    //    }
    //}
}
﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Auth.Service.NewsPortal
{
    //public class SaveUnsaveNewsPortalFeed
    //{
    //    private readonly ILog log = LogManager.GetLogger(typeof(SaveUnsaveNewsPortalFeed).Name);
    //    private List<long> nfIdLst;
    //    private string sb;
    //    private int option; //fieldName - option value - 1/2/3 1 => Save ids 2 => Remove selected Ids3 => Remove All
    //    private int ProfileType;
    //    public SaveUnsaveNewsPortalFeed(int option, List<long> nfIdLst, int ProfileType)
    //    {
    //        this.nfIdLst = nfIdLst;
    //        this.option = option;
    //        this.sb = (option == 1) ? "save" : "remove";
    //        this.ProfileType = ProfileType;
    //        //ThreadStart childref = new ThreadStart(Run);
    //        //Thread childThread = new Thread(childref);
    //        //childThread.Start();
    //    }

    //    //public void Run()
    //    //{
    //    //    if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //    //    {
    //    //        try
    //    //        {
    //    //            JObject FeedBackFields = null;
    //    //            JObject pakToSend = new JObject();
    //    //            string pakId = SendToServer.GetRanDomPacketID();
    //    //            pakToSend[JsonKeys.PacketId] = pakId;
    //    //            pakToSend[JsonKeys.Action] = AppConstants.TYPE_SAVE_UNSAVE_NEWSPORTAL_FEED;
    //    //            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //    //            pakToSend[JsonKeys.Option] = option;

    //    //            if (option != 3)
    //    //            {
    //    //                JArray Arr = new JArray();
    //    //                foreach (var item in nfIdLst)
    //    //                {
    //    //                    Arr.Add(item);
    //    //                }
    //    //                pakToSend[JsonKeys.FeedIdsList] = Arr;
    //    //            }
    //    //            //pakToSend[JsonKeys.ProfileType] = ProfileType;
    //    //            string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //    //            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //    //            Thread.Sleep(25);
    //    //            for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
    //    //            {
    //    //                if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
    //    //                {
    //    //                    break;
    //    //                }
    //    //                Thread.Sleep(DefaultSettings.WAITING_TIME);
    //    //                if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out FeedBackFields))
    //    //                {
    //    //                    if (i % DefaultSettings.SEND_INTERVAL == 0)
    //    //                        SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
    //    //                }
    //    //                else
    //    //                {
    //    //                    if ((bool)FeedBackFields[JsonKeys.Success])
    //    //                    {
    //    //                        HelperMethodsAuth.NewsFeedHandlerInstance.SaveUnsaveNewsPortalFeed(option, nfIdLst);
    //    //                    }
    //    //                    else
    //    //                    {
    //    //                        HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox("Failed to " + sb + " this feed! Please Try later!", "Failed");
    //    //                    }
    //    //                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out FeedBackFields);
    //    //                    return;
    //    //                }
    //    //                PingInServer.StartThread(1, DefaultSettings.TRYING_TIME);
    //    //            }
    //    //        }
    //    //        catch (Exception e)
    //    //        {
    //    //            log.Error("SubscribeOrUnsubscribe ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //    //        }
    //    //    }
    //    //    else
    //    //    {
    //    //        log.Error("SubscribeOrUnsubscribe Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //    //    }
    //    //    HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox("Failed to " + sb + " this feed! Please Check your Inernet Connection or Try later!", "Failed");
    //    //}
    //}
}

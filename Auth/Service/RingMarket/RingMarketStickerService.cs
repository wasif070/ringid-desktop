<<<<<<< HEAD
﻿using Auth.utility;
using Models.Entity;
using Models.Utility;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Models.Constants;
using System.IO;

namespace Auth.Service.RingMarket
{
    public class RingMarketStickerService
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(RingMarketStickerService).Name);

        private string _RootPath;
        private IRingMarkerSticker _Listener = null;
        private static RingMarketStickerService _Instance;

        public const int _Sticker_Icon = 1;
        public const int _DetailImage = 2;
        public const int _BannerImage = 3;
        public const int _ThumbDetail = 4;

        public const int _Market_Icon_Image = 5;
        public const int _MarketBannerImage = 6;
        public const int _MarketDetailImage = 7;
        public const int _MarketBannerImageMini = 8;
        public const int _Market_Thumb_DetailImage = 9;

        public RingMarketStickerService() { }

        public RingMarketStickerService(string countryCode, string rootPath, IRingMarkerSticker listener)
        {
            this._RootPath = rootPath;
            this._Listener = listener;

            Thread t = new Thread(() => InitSticker(countryCode));
            t.Name = this.GetType().Name;
            t.Start();
        }

        public RingMarketStickerService(int categoryID, string rootPath, IRingMarkerSticker listener, int imageType = 0, Func<bool, int> isNoInternet = null)
        {
            this._RootPath = rootPath;
            this._Listener = listener;

            MarketStickerCategoryDTO marketCtgDto = null;
            if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(categoryID, out marketCtgDto))
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    ThreadStart ts = null;
                    if (imageType == RingMarketStickerService._Market_Icon_Image || imageType == RingMarketStickerService._Sticker_Icon)
                    {
                        ts = new ThreadStart(() => DownloadMarketStickerIcon(marketCtgDto, imageType));
                    }

                    else if (imageType == RingMarketStickerService._MarketBannerImage || imageType == RingMarketStickerService._MarketBannerImageMini)
                    {
                        ts = new ThreadStart(() => DownloadMarketBannerImage(marketCtgDto, imageType));
                    }

                    else if (imageType == RingMarketStickerService._MarketDetailImage || imageType == RingMarketStickerService._Market_Thumb_DetailImage)
                    {
                        ts = new ThreadStart(() => DownloadMarketStickerDetailImage(marketCtgDto, imageType));
                    }

                    else if (marketCtgDto.ImagesList.Count > 0) // ImageList
                    {
                        ts = new ThreadStart(() => DownloadMarketImagList(marketCtgDto));
                    }

                    if (ts != null)
                    {
                        Thread t = new Thread(ts);
                        t.Name = this.GetType().Name;
                        t.Start();
                    }
                    else
                    {
                        log.Error("Thread not start =>  RingMarketStickerService ");
                    }
                }
                else if (isNoInternet != null)
                {
                    isNoInternet(true);
                }
            }
        }

        public static RingMarketStickerService Instance
        {
            get
            {
                return _Instance = _Instance ?? new RingMarketStickerService();
            }
        }

        private void InitSticker(string countryCode)
        {
            int startIndex = 0, categoryLimit = 4, collectionLimit = 0, stickerType = StickerMarketConstants.STICKER_TYPE_ALL; //stickerType(1 = all, 2 = top, 3 = new)
            List<MarketStickerCategoryDTO> tempCtgList = null;
            try
            {
                this._Listener.OnInit();
                this.FetchNewStickerCategoryIds();
                this.FetchDefaultSticker();

                List<MarketStickerCategoryDTO> recentCtgDTOs = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.ToList();
                List<MarketStickerCategoryDTO> chatLowerPanelCategoryDTOs = new List<MarketStickerCategoryDTO>();
                chatLowerPanelCategoryDTOs.AddRange(recentCtgDTOs.Where(P => P.IsDefault).Take(4).ToList());
                if (chatLowerPanelCategoryDTOs.Count < 4)
                {
                    if (recentCtgDTOs.Count >= 4)
                    {
                        int limit = 4 - chatLowerPanelCategoryDTOs.Count;
                        chatLowerPanelCategoryDTOs.AddRange(recentCtgDTOs.Where(P => P.IsDefault == false && P.Downloaded).Take(limit).ToList());
                    }
                    else
                    {
                        tempCtgList = GetMoreStickerCategoriesOfSameType(startIndex, categoryLimit, collectionLimit, stickerType, true);
                        if (tempCtgList != null && tempCtgList.Count > 0)
                        {
                            int limit = 4 - chatLowerPanelCategoryDTOs.Count;
                            tempCtgList = tempCtgList.Where(P => !chatLowerPanelCategoryDTOs.Any(Q => Q.sCtId == P.sCtId)).ToList();
                            chatLowerPanelCategoryDTOs.AddRange(tempCtgList.OrderBy(P => P.sCtId).Take(limit).ToList());
                        }
                    }
                }

                this._Listener.OnMarketStickerCategoryInfo(chatLowerPanelCategoryDTOs, true);

                MarketStickerDTO marketStickerDTO = this.FetchInitMarketSticker(); // MenuStickerMarket
                if (marketStickerDTO != null && marketStickerDTO.sucs)
                {
                    if (marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                    {
                        this._Listener.OnMarketStickerCategoryInfo(marketStickerDTO.catList, false);
                    }
                }

                this._Listener.OnMarketStickerCollectionInfo(StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY.Values.ToList());
                this._Listener.OnMarketLaguageInfo(StickerDictionaries.Instance.STICKER_COUNTRYLIST.ToList());
                this._Listener.OnComplete();

                List<MarketStickerCategoryDTO> defaultCtgDTOs = recentCtgDTOs.Where(P => P.IsDefault).OrderBy(P => P.sCtId).ToList();
                foreach (MarketStickerCategoryDTO marketCtgDTO in defaultCtgDTOs)
                {
                    this.GetMarketStickerImagesOfACategory(marketCtgDTO, this._Listener);
                    this.DownloadMarketImagList(marketCtgDTO);
                }
            }

            catch (Exception ex)
            {
                this._Listener.OnComplete();
                log.Error("InitStickerMarketRequest() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void FetchNewStickerCategoryIds()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    StickerDTO stickerDTO = HttpRequest.GetNewStickerCategoryIds();
                    if (stickerDTO != null && stickerDTO.ids != null)
                    {
                        foreach (int categoryId in stickerDTO.ids)
                        {
                            StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Add(categoryId);
                        }

                        List<MarketStickerCategoryDTO> newCtgSeenList = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.cgNw && P.IsNewStickerSeen).ToList();
                        foreach (MarketStickerCategoryDTO ctgDTO in newCtgSeenList)
                        {
                            ctgDTO.IsNewStickerSeen = true;
                        }

                        DefaultSettings.NEW_STICKER_UNSEEN_COUNT = StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Where(P => !newCtgSeenList.Any(Q => Q.sCtId == P)).ToList().Count();
                        this._Listener.OnOnMarketStickerNewCount(StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Count, DefaultSettings.NEW_STICKER_UNSEEN_COUNT);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FetchNewStickerCategoryIds() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void FetchDefaultSticker()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    StickerDTO stickerDTO = HttpRequest.GetDefaultStickers();
                    if (stickerDTO != null && stickerDTO.Sucs && stickerDTO.CategoriesList != null && stickerDTO.CategoriesList.Count > 0)
                    {
                        List<MarketStickerCategoryDTO> tempList = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.IsDefault && !stickerDTO.CategoriesList.Any(Q => Q.SCtId == P.sCtId)).ToList();
                        foreach (MarketStickerCategoryDTO marketCtgDTO in tempList)
                        {
                            marketCtgDTO.IsDefault = false;
                        }

                        foreach (StickerCategoryDTO categoryDTO in stickerDTO.CategoriesList)
                        {
                            MarketStickerCategoryDTO marketCtgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(categoryDTO.SCtId);
                            if (marketCtgDTO == null)
                            {
                                marketCtgDTO = new MarketStickerCategoryDTO();
                            }
                            marketCtgDTO.IsDefault = true;
                            marketCtgDTO.UpdateMarketDTOByCategoryDTO(categoryDTO);
                            StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = marketCtgDTO;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FetchDefaultSticker() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private MarketStickerDTO FetchInitMarketSticker()
        {
            MarketStickerDTO marketStickerDTO = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    int categoryLimit = 4, collectionLimit = 5;
                    marketStickerDTO = HttpRequest.GetAllStickersForNewAPI(categoryLimit, collectionLimit);

                    if (marketStickerDTO != null && marketStickerDTO.sucs == true)
                    {
                        if (marketStickerDTO.pCat != null && marketStickerDTO.pCat.Count > 0)
                        {
                            foreach (int ctgId in marketStickerDTO.pCat)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_POPULAR_LIST.Add(ctgId);
                            }
                        }
                        if (marketStickerDTO.nCat != null && marketStickerDTO.nCat.Count > 0)
                        {
                            foreach (int ctgId in marketStickerDTO.nCat)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_NEW_LIST.Add(ctgId);
                            }
                        }

                        if (marketStickerDTO.tCat != null && marketStickerDTO.tCat.Count > 0)
                        {
                            foreach (int ctgId in marketStickerDTO.tCat)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_TOP_LIST.Add(ctgId);
                            }
                        }
                        if (marketStickerDTO.allCat != null && marketStickerDTO.allCat.Count > 0)
                        {
                            foreach (int ctgId in marketStickerDTO.allCat)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST.Add(ctgId);
                            }
                        }
                        if (marketStickerDTO.plrBnr != null && marketStickerDTO.plrBnr.Count > 0)
                        {
                            List<MarketStickerCategoryDTO> templist = new List<MarketStickerCategoryDTO>();
                            foreach (MarketStickerCategoryDTO ctgDTO in marketStickerDTO.plrBnr)
                            {
                                templist.Add(ctgDTO);
                            }

                            StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[StickerMarketConstants.STICKER_TYPE_POPULAR] = templist;
                        }
                        if (marketStickerDTO.nwBnr != null && marketStickerDTO.nwBnr.Count > 0)
                        {
                            List<MarketStickerCategoryDTO> templist = new List<MarketStickerCategoryDTO>();
                            foreach (MarketStickerCategoryDTO ctgDTO in marketStickerDTO.nwBnr)
                            {
                                templist.Add(ctgDTO);
                            }

                            StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[StickerMarketConstants.STICKER_TYPE_NEW] = templist;
                        }
                        if (marketStickerDTO.allBnr != null && marketStickerDTO.allBnr.Count > 0)
                        {
                            List<MarketStickerCategoryDTO> templist = new List<MarketStickerCategoryDTO>();
                            foreach (MarketStickerCategoryDTO ctgDTO in marketStickerDTO.allBnr)
                            {
                                templist.Add(ctgDTO);
                            }

                            StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[StickerMarketConstants.STICKER_TYPE_ALL] = templist;
                        }
                        if (marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                        {
                            foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                            {
                                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                                if (ctgDTO != null)
                                {
                                    ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                                }
                                else
                                {
                                    ctgDTO = marketCtgDTO;
                                }

                                StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                            }
                        }
                        if (marketStickerDTO.colList != null && marketStickerDTO.colList.Count > 0)
                        {
                            foreach (MarketStickerCollectionDTO collDTO in marketStickerDTO.colList)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY[collDTO.sClId] = collDTO;
                            }
                        }
                        if (marketStickerDTO.langLst != null && marketStickerDTO.langLst.Count > 0)
                        {
                            foreach (MarketStickerLanguageyDTO langDto in marketStickerDTO.langLst)
                            {
                                StickerDictionaries.Instance.STICKER_COUNTRYLIST.Add(langDto);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FetchInitMarketSticker.... => " + ex.Message + ex.StackTrace);
            }
            return marketStickerDTO;
        }

        public MarketStickerDTO GetMoreStickerCollections(int startIndex, int categoryLimit, int collectionLimit, int stickerType)
        {
            MarketStickerDTO marketStickerDTO = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    marketStickerDTO = HttpRequest.GetStickerOfParticularType(startIndex, categoryLimit, collectionLimit, stickerType);
                    if (marketStickerDTO != null && marketStickerDTO.sucs == true && marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                    {
                        foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                        {
                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                            if (ctgDTO != null)
                            {
                                ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                            }
                            else
                            {
                                ctgDTO = marketCtgDTO;
                            }

                            StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                        }

                        if (marketStickerDTO.colList != null && marketStickerDTO.colList.Count > 0)
                        {
                            foreach (MarketStickerCollectionDTO collDTO in marketStickerDTO.colList)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY[collDTO.sClId] = collDTO;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerCollections.... => " + ex.Message + ex.StackTrace);
            }
            return marketStickerDTO;
        }

        public MarketStickerDTO GetMoreStickerLanguages(int startIndex, int categoryLimit, int collectionLimit, int stickerType)
        {
            MarketStickerDTO marketStickerDTO = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    marketStickerDTO = HttpRequest.GetStickerOfParticularType(startIndex, categoryLimit, collectionLimit, stickerType);
                    if (marketStickerDTO != null && marketStickerDTO.sucs == true && marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                    {
                        foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                        {
                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                            if (ctgDTO != null)
                            {
                                ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                            }
                            else
                            {
                                ctgDTO = marketCtgDTO;
                            }

                            StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                        }

                        if (marketStickerDTO.langLst != null && marketStickerDTO.langLst.Count > 0)
                        {
                            foreach (MarketStickerLanguageyDTO collDTO in marketStickerDTO.langLst)
                            {
                                StickerDictionaries.Instance.STICKER_COUNTRYLIST.Add(collDTO);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerLanguages() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return marketStickerDTO;
        }

        public List<MarketStickerCategoryDTO> GetMoreStickerCategoriesOfSameType(int startIndex, int categoryLimit, int collectionLimit, int stickerType, bool initialRequest = false)
        {
            List<MarketStickerCategoryDTO> ctgList = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    MarketStickerDTO marketStickerDTO = HttpRequest.GetStickerOfParticularType(startIndex, categoryLimit, collectionLimit, stickerType);
                    if (marketStickerDTO != null && marketStickerDTO.sucs == true && marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                    {
                        ctgList = new List<MarketStickerCategoryDTO>();
                        foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                        {
                            if (stickerType == StickerMarketConstants.STICKER_TYPE_POPULAR)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_POPULAR_LIST.Add(marketCtgDTO.sCtId);
                            }

                            else if (stickerType == StickerMarketConstants.STICKER_TYPE_ALL)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST.Add(marketCtgDTO.sCtId);
                            }

                            else if (stickerType == StickerMarketConstants.STICKER_TYPE_NEW)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_NEW_LIST.Add(marketCtgDTO.sCtId);
                            }

                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                            if (ctgDTO != null)
                            {
                                ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                            }
                            else
                            {
                                ctgDTO = marketCtgDTO;
                            }

                            StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                            if (ctgList.Where(P => P.sCtId == ctgDTO.sCtId).FirstOrDefault() == null)
                            {
                                ctgList.Add(marketCtgDTO);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerCategoriesOfSameType() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return ctgList;
        }

        public List<MarketStickerCategoryDTO> GetMoreStickerCategoriesInACollection(int startIndex, int categoryLimit, int stickerType, int collectionId)
        {
            List<MarketStickerCategoryDTO> categoriesDTOs = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    MarketStickerDTO marketStickerDTO = HttpRequest.GetStickersOfParticularCollection(startIndex, categoryLimit, stickerType, collectionId);

                    if (marketStickerDTO != null && marketStickerDTO.sucs == true)
                    {
                        if (marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                        {
                            categoriesDTOs = new List<MarketStickerCategoryDTO>();
                            foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                            {
                                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                                if (ctgDTO != null)
                                {
                                    ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                                }
                                else
                                {
                                    ctgDTO = marketCtgDTO;
                                }

                                categoriesDTOs.Add(ctgDTO);
                                StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[ctgDTO.sCtId] = ctgDTO;
                            }
                        }

                        if (marketStickerDTO.colList != null && marketStickerDTO.colList.Count > 0)
                        {
                            foreach (MarketStickerCollectionDTO collDTO in marketStickerDTO.colList) // 1 Collection
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY[collDTO.sClId] = collDTO;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerCategoriesInACollection() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return categoriesDTOs;
        }

        public List<MarketStickerCategoryDTO> GetMoreStickerCategoriesInALanguage(int startIndex, int categoryLimit, int stickerType, string languageName)
        {
            List<MarketStickerCategoryDTO> categoriesDTOs = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    MarketStickerDTO marketStickerDTO = HttpRequest.GetStickersOfParticularLanguage(startIndex, categoryLimit, stickerType, languageName);

                    if (marketStickerDTO != null && marketStickerDTO.sucs == true)
                    {
                        if (marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                        {
                            categoriesDTOs = new List<MarketStickerCategoryDTO>();
                            foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                            {
                                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                                if (ctgDTO != null)
                                {
                                    ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                                }
                                else
                                {
                                    ctgDTO = marketCtgDTO;
                                }

                                categoriesDTOs.Add(ctgDTO);
                                StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[ctgDTO.sCtId] = ctgDTO;
                            }
                        }

                        if (marketStickerDTO.colList != null && marketStickerDTO.colList.Count > 0)
                        {
                            foreach (MarketStickerCollectionDTO collDTO in marketStickerDTO.colList) // 1 Collection
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY[collDTO.sClId] = collDTO;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerCategoriesInALanguage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            return categoriesDTOs;
        }

        public void GetMarketStickerImagesOfACategory(MarketStickerCategoryDTO categoryDTO, IRingMarkerSticker listener)
        {
            try
            {
                this._Listener = listener;
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    MarketStickerDTO stickerDTO = HttpRequest.GetMarketStickerImagesByCategoryID(categoryDTO.sCtId);
                    if (stickerDTO != null && stickerDTO.ImagesList != null && stickerDTO.ImagesList.Count > 0)
                    {
                        List<MarkertStickerImagesDTO> imageList = new List<MarkertStickerImagesDTO>();
                        foreach (MarkertStickerImagesDTO imageDTO in stickerDTO.ImagesList)
                        {
                            MarkertStickerImagesDTO tempImageDTO = categoryDTO.ImagesList.Where(P => P.imId == imageDTO.imId).FirstOrDefault();
                            if (tempImageDTO != null)
                            {
                                if (tempImageDTO.sClId <= 0)
                                {
                                    tempImageDTO.sClId = categoryDTO.sClId;
                                    imageList.Add(tempImageDTO);
                                }
                            }
                            else
                            {
                                imageDTO.sClId = categoryDTO.sClId;
                                imageList.Add(imageDTO);
                            }
                        }

                        if (imageList.Count > 0)
                        {
                            categoryDTO.ImagesList.AddRange(imageList);
                            if (this._Listener != null)
                            {
                                _Listener.OnMarketStickerImageInfo(categoryDTO.sCtId, imageList);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMarketStickerImagesOfACategory() ==> " + ex.StackTrace);
            }
        }

        public void DownloadMarketStickerIcon(MarketStickerCategoryDTO categoryDTO, int imageType)
        {
            try
            {
                string folderPath = _RootPath + Path.DirectorySeparatorChar + categoryDTO.sClId + Path.DirectorySeparatorChar + categoryDTO.sCtId;
                HelperMethodsAuth.CreateDirectory(folderPath);

                string extension = imageType == RingMarketStickerService._Market_Icon_Image ? "market_" : string.Empty;
                string iconFilePath = folderPath + Path.DirectorySeparatorChar + extension + categoryDTO.Icon;

                string imgPath = imageType == RingMarketStickerService._Market_Icon_Image ? ServerAndPortSettings.D_FULL : ServerAndPortSettings.D_MINI;
                string iconUri = ServerAndPortSettings.StickerMarketDownloadUrl + "/" + imgPath + "/" + categoryDTO.sClId + "/" + categoryDTO.sCtId + "/" + categoryDTO.Icon;
                ImageFile file = new ImageFile(iconFilePath);
                if (!file.IsComplete)
                {
                    if (HttpRequest.DownloadFile(iconUri, iconFilePath))
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerIconDownloaded(categoryDTO.sCtId, imageType, true);
                        }
                    }
                    else
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerIconDownloaded(categoryDTO.sCtId, imageType, false);
                        }
                    }
                }
                else
                {
                    if (this._Listener != null)
                    {
                        this._Listener.OnMarketStickerIconDownloaded(categoryDTO.sCtId, imageType, false);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("DownloadMarketStickerIcon() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DownloadMarketStickerDetailImage(MarketStickerCategoryDTO categoryDTO, int imageType)
        {
            try
            {
                string folderPath = this._RootPath + Path.DirectorySeparatorChar + categoryDTO.sClId + Path.DirectorySeparatorChar + categoryDTO.sCtId;
                HelperMethodsAuth.CreateDirectory(folderPath);

                string extension = imageType == RingMarketStickerService._MarketDetailImage ? categoryDTO.DetailImg : categoryDTO.ThumbDetailImage;

                string detailFilePath = folderPath + Path.DirectorySeparatorChar + extension;
                string detailUri = ServerAndPortSettings.StickerMarketDownloadUrl + "/" + ServerAndPortSettings.D_FULL + "/" + categoryDTO.sClId + "/" + categoryDTO.sCtId + "/" + extension;

                ImageFile file = new ImageFile(detailFilePath);
                if (!file.IsComplete)
                {
                    if (HttpRequest.DownloadFile(detailUri, detailFilePath))
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerDetailImageDownloaded(categoryDTO.sCtId, imageType, true);
                        }
                    }
                    else
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerDetailImageDownloaded(categoryDTO.sCtId, imageType, false);
                        }
                    }
                }
                else
                {
                    if (this._Listener != null)
                    {
                        this._Listener.OnMarketStickerDetailImageDownloaded(categoryDTO.sCtId, imageType, false);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("DownloadMarketStickerDetailImage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DownloadMarketBannerImage(MarketStickerCategoryDTO categoryDTO, int imageType)
        {
            try
            {
                string folderPath = this._RootPath + Path.DirectorySeparatorChar + categoryDTO.sClId + Path.DirectorySeparatorChar + categoryDTO.sCtId;
                HelperMethodsAuth.CreateDirectory(folderPath);

                string extension = imageType == RingMarketStickerService._MarketBannerImageMini ? "s" : string.Empty;
                string detailFilePath = folderPath + Path.DirectorySeparatorChar + extension + categoryDTO.cgBnrImg;

                //string imgPath = imageType == RingMarketStickerService._MarketBannerImageMini ? ServerAndPortSettings.D_MINI : ServerAndPortSettings.D_FULL;
                string detailUri = ServerAndPortSettings.StickerMarketDownloadUrl + "/" + ServerAndPortSettings.D_FULL + "/" + categoryDTO.sClId + "/" + categoryDTO.sCtId + "/" + extension + categoryDTO.cgBnrImg;

                ImageFile file = new ImageFile(detailFilePath);
                if (!file.IsComplete)
                {
                    if (HttpRequest.DownloadFile(detailUri, detailFilePath))
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerBannerImageDownloaded(categoryDTO.sCtId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("DownloadMarketBannerImage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DownloadMarketImagList(MarketStickerCategoryDTO categoryDTO)
        {
            try
            {
                bool isDownloaded = true;
                List<MarkertStickerImagesDTO> imageList = null;
                if ((DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null) || categoryDTO.Downloaded)
                {
                    imageList = categoryDTO.ImagesList.ToList();
                    foreach (MarkertStickerImagesDTO imageDTO in imageList)
                    {
                        string folderPath = this._RootPath + Path.DirectorySeparatorChar + imageDTO.sClId + Path.DirectorySeparatorChar + imageDTO.sCtId;
                        string filePath = folderPath + Path.DirectorySeparatorChar + imageDTO.imUrl;
                        string uri = ServerAndPortSettings.StickerMarketDownloadUrl + "/" + ServerAndPortSettings.D_FULL + "/" + imageDTO.sClId + "/" + imageDTO.sCtId + "/" + imageDTO.imUrl;

                        ImageFile file = new ImageFile(filePath);
                        if (file.IsComplete || HttpRequest.DownloadFile(uri, filePath))
                        {
                            if (this._Listener != null)
                            {
                                this._Listener.OnMarketStickerImageDownloaded(categoryDTO.sCtId, imageDTO.imId);
                            }
                        }
                        else
                        {
                            isDownloaded = false;
                        }
                    }
                }
                else
                {
                    isDownloaded = false;
                }

                bool status = isDownloaded && imageList != null && imageList.Count > 0;
                this._Listener.OnMarketStickerImageListDownloaded(categoryDTO.sCtId, status);
            }
            catch (Exception ex)
            {
                log.Error("DownloadMarketImagList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            finally
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null && categoryDTO != null)
                {
                    HttpRequest.IncreaseDownloadCount(categoryDTO.sCtId);
                }
            }
        }
    }
}
=======
﻿using Auth.utility;
using Models.Entity;
using Models.Utility;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Models.Constants;
using System.IO;

namespace Auth.Service.RingMarket
{
    public class RingMarketStickerService
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(RingMarketStickerService).Name);

        private string _RootPath;
        private IRingMarkerSticker _Listener = null;
        private static RingMarketStickerService _Instance;

        public const int _Sticker_Icon = 1;
        public const int _DetailImage = 2;
        public const int _BannerImage = 3;
        public const int _ThumbDetail = 4;

        public const int _Market_Icon_Image = 5;
        public const int _MarketBannerImage = 6;
        public const int _MarketDetailImage = 7;
        public const int _MarketBannerImageMini = 8;
        public const int _Market_Thumb_DetailImage = 9;

        public RingMarketStickerService() { }

        public RingMarketStickerService(string countryCode, string rootPath, IRingMarkerSticker listener)
        {
            this._RootPath = rootPath;
            this._Listener = listener;

            Thread t = new Thread(() => InitSticker(countryCode));
            t.Name = this.GetType().Name;
            t.Start();
        }

        public RingMarketStickerService(int categoryID, string rootPath, IRingMarkerSticker listener, int imageType = 0, Func<bool, int> isNoInternet = null)
        {
            this._RootPath = rootPath;
            this._Listener = listener;

            MarketStickerCategoryDTO marketCtgDto = null;
            if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(categoryID, out marketCtgDto))
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    ThreadStart ts = null;
                    if (imageType == RingMarketStickerService._Market_Icon_Image || imageType == RingMarketStickerService._Sticker_Icon)
                    {
                        ts = new ThreadStart(() => DownloadMarketStickerIcon(marketCtgDto, imageType));
                    }

                    else if (imageType == RingMarketStickerService._MarketBannerImage || imageType == RingMarketStickerService._MarketBannerImageMini)
                    {
                        ts = new ThreadStart(() => DownloadMarketBannerImage(marketCtgDto, imageType));
                    }

                    else if (imageType == RingMarketStickerService._MarketDetailImage || imageType == RingMarketStickerService._Market_Thumb_DetailImage)
                    {
                        ts = new ThreadStart(() => DownloadMarketStickerDetailImage(marketCtgDto, imageType));
                    }

                    else if (marketCtgDto.ImagesList.Count > 0) // ImageList
                    {
                        ts = new ThreadStart(() => DownloadMarketImagList(marketCtgDto));
                    }

                    if (ts != null)
                    {
                        Thread t = new Thread(ts);
                        t.Name = this.GetType().Name;
                        t.Start();
                    }
                    else
                    {
                        log.Error("Thread not start =>  RingMarketStickerService ");
                    }
                }
                else if (isNoInternet != null)
                {
                    isNoInternet(true);
                }
            }
        }

        public static RingMarketStickerService Instance
        {
            get
            {
                return _Instance = _Instance ?? new RingMarketStickerService();
            }
        }

        private void InitSticker(string countryCode)
        {
            int startIndex = 0, categoryLimit = 4, collectionLimit = 0, stickerType = StickerMarketConstants.STICKER_TYPE_ALL; //stickerType(1 = all, 2 = top, 3 = new)
            List<MarketStickerCategoryDTO> tempCtgList = null;
            try
            {
                this._Listener.OnInit();
                this.FetchNewStickerCategoryIds();
                this.FetchDefaultSticker();

                List<MarketStickerCategoryDTO> recentCtgDTOs = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.ToList();
                List<MarketStickerCategoryDTO> chatLowerPanelCategoryDTOs = new List<MarketStickerCategoryDTO>();
                chatLowerPanelCategoryDTOs.AddRange(recentCtgDTOs.Where(P => P.IsDefault).Take(4).ToList());
                if (chatLowerPanelCategoryDTOs.Count < 4)
                {
                    if (recentCtgDTOs.Count >= 4)
                    {
                        int limit = 4 - chatLowerPanelCategoryDTOs.Count;
                        chatLowerPanelCategoryDTOs.AddRange(recentCtgDTOs.Where(P => P.IsDefault == false && P.Downloaded).Take(limit).ToList());
                    }
                    else
                    {
                        tempCtgList = GetMoreStickerCategoriesOfSameType(startIndex, categoryLimit, collectionLimit, stickerType, true);
                        if (tempCtgList != null && tempCtgList.Count > 0)
                        {
                            int limit = 4 - chatLowerPanelCategoryDTOs.Count;
                            tempCtgList = tempCtgList.Where(P => !chatLowerPanelCategoryDTOs.Any(Q => Q.sCtId == P.sCtId)).ToList();
                            chatLowerPanelCategoryDTOs.AddRange(tempCtgList.OrderBy(P => P.sCtId).Take(limit).ToList());
                        }
                    }
                }

                this._Listener.OnMarketStickerCategoryInfo(chatLowerPanelCategoryDTOs, true);

                MarketStickerDTO marketStickerDTO = this.FetchInitMarketSticker(); // MenuStickerMarket
                if (marketStickerDTO != null && marketStickerDTO.sucs)
                {
                    if (marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                    {
                        this._Listener.OnMarketStickerCategoryInfo(marketStickerDTO.catList, false);
                    }
                }

                this._Listener.OnMarketStickerCollectionInfo(StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY.Values.ToList());
                this._Listener.OnMarketLaguageInfo(StickerDictionaries.Instance.STICKER_COUNTRYLIST.ToList());
                this._Listener.OnComplete();

                List<MarketStickerCategoryDTO> defaultCtgDTOs = recentCtgDTOs.Where(P => P.IsDefault).OrderBy(P => P.sCtId).ToList();
                foreach (MarketStickerCategoryDTO marketCtgDTO in defaultCtgDTOs)
                {
                    this.GetMarketStickerImagesOfACategory(marketCtgDTO, this._Listener);
                    this.DownloadMarketImagList(marketCtgDTO);
                }
            }

            catch (Exception ex)
            {
                this._Listener.OnComplete();
                log.Error("InitStickerMarketRequest() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void FetchNewStickerCategoryIds()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    StickerDTO stickerDTO = HttpRequest.GetNewStickerCategoryIds();
                    if (stickerDTO != null && stickerDTO.ids != null)
                    {
                        foreach (int categoryId in stickerDTO.ids)
                        {
                            StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Add(categoryId);
                        }

                        List<MarketStickerCategoryDTO> newCtgSeenList = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.cgNw && P.IsNewStickerSeen).ToList();
                        foreach (MarketStickerCategoryDTO ctgDTO in newCtgSeenList)
                        {
                            ctgDTO.IsNewStickerSeen = true;
                        }

                        DefaultSettings.NEW_STICKER_UNSEEN_COUNT = StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Where(P => !newCtgSeenList.Any(Q => Q.sCtId == P)).ToList().Count();
                        this._Listener.OnOnMarketStickerNewCount(StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Count, DefaultSettings.NEW_STICKER_UNSEEN_COUNT);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FetchNewStickerCategoryIds() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void FetchDefaultSticker()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    StickerDTO stickerDTO = HttpRequest.GetDefaultStickers();
                    if (stickerDTO != null && stickerDTO.Sucs && stickerDTO.CategoriesList != null && stickerDTO.CategoriesList.Count > 0)
                    {
                        List<MarketStickerCategoryDTO> tempList = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.IsDefault && !stickerDTO.CategoriesList.Any(Q => Q.SCtId == P.sCtId)).ToList();
                        foreach (MarketStickerCategoryDTO marketCtgDTO in tempList)
                        {
                            marketCtgDTO.IsDefault = false;
                        }

                        foreach (StickerCategoryDTO categoryDTO in stickerDTO.CategoriesList)
                        {
                            MarketStickerCategoryDTO marketCtgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(categoryDTO.SCtId);
                            if (marketCtgDTO == null)
                            {
                                marketCtgDTO = new MarketStickerCategoryDTO();
                            }
                            marketCtgDTO.IsDefault = true;
                            marketCtgDTO.UpdateMarketDTOByCategoryDTO(categoryDTO);
                            StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = marketCtgDTO;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FetchDefaultSticker() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private MarketStickerDTO FetchInitMarketSticker()
        {
            MarketStickerDTO marketStickerDTO = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    int categoryLimit = 4, collectionLimit = 5;
                    marketStickerDTO = HttpRequest.GetAllStickersForNewAPI(categoryLimit, collectionLimit);

                    if (marketStickerDTO != null && marketStickerDTO.sucs == true)
                    {
                        if (marketStickerDTO.dCatList != null && marketStickerDTO.dCatList.Count > 0)
                        {
                            foreach (MarketStickerDynamicCategoryDTO dynamicCtgDTO in marketStickerDTO.dCatList)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_DYNAMIC_CATEGORY_DICTIONARY.Add(dynamicCtgDTO);
                                if (dynamicCtgDTO.banner != null)
                                {
                                    StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[dynamicCtgDTO.id] = dynamicCtgDTO.banner;
                                    foreach (MarketStickerCategoryDTO marketCtgDTO in dynamicCtgDTO.banner)
                                    {
                                        MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                                        if (ctgDTO != null)
                                        {
                                            ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                                        }
                                        else
                                        {
                                            ctgDTO = marketCtgDTO;
                                        }

                                        StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                                    }
                                }
                            }
                        }

                        if (marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                        {
                            foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                            {
                                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                                if (ctgDTO != null)
                                {
                                    ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                                }
                                else
                                {
                                    ctgDTO = marketCtgDTO;
                                }

                                StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                            }
                        }
                        if (marketStickerDTO.colList != null && marketStickerDTO.colList.Count > 0)
                        {
                            foreach (MarketStickerCollectionDTO collDTO in marketStickerDTO.colList)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY[collDTO.sClId] = collDTO;
                            }
                        }
                        if (marketStickerDTO.langLst != null && marketStickerDTO.langLst.Count > 0)
                        {
                            foreach (MarketStickerLanguageyDTO langDto in marketStickerDTO.langLst)
                            {
                                StickerDictionaries.Instance.STICKER_COUNTRYLIST.Add(langDto);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FetchInitMarketSticker.... => " + ex.Message + ex.StackTrace);
            }
            return marketStickerDTO;
        }

        public MarketStickerDTO GetMoreStickerCollections(int startIndex, int categoryLimit, int collectionLimit, int stickerType)
        {
            MarketStickerDTO marketStickerDTO = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    marketStickerDTO = HttpRequest.GetStickerOfParticularType(startIndex, categoryLimit, collectionLimit, stickerType);
                    if (marketStickerDTO != null && marketStickerDTO.sucs == true && marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                    {
                        foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                        {
                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                            if (ctgDTO != null)
                            {
                                ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                            }
                            else
                            {
                                ctgDTO = marketCtgDTO;
                            }

                            StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                        }

                        if (marketStickerDTO.colList != null && marketStickerDTO.colList.Count > 0)
                        {
                            foreach (MarketStickerCollectionDTO collDTO in marketStickerDTO.colList)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY[collDTO.sClId] = collDTO;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerCollections.... => " + ex.Message + ex.StackTrace);
            }
            return marketStickerDTO;
        }

        public MarketStickerDTO GetMoreStickerLanguages(int startIndex, int categoryLimit, int collectionLimit, int stickerType)
        {
            MarketStickerDTO marketStickerDTO = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    marketStickerDTO = HttpRequest.GetStickerOfParticularType(startIndex, categoryLimit, collectionLimit, stickerType);
                    if (marketStickerDTO != null && marketStickerDTO.sucs == true && marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                    {
                        foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                        {
                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                            if (ctgDTO != null)
                            {
                                ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                            }
                            else
                            {
                                ctgDTO = marketCtgDTO;
                            }

                            StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                        }

                        if (marketStickerDTO.langLst != null && marketStickerDTO.langLst.Count > 0)
                        {
                            foreach (MarketStickerLanguageyDTO collDTO in marketStickerDTO.langLst)
                            {
                                StickerDictionaries.Instance.STICKER_COUNTRYLIST.Add(collDTO);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerLanguages() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return marketStickerDTO;
        }

        public List<MarketStickerCategoryDTO> GetMoreDynamicStickerCategories(int startIndex, int categoryLimit, int collectionLimit, int stickerType, int dynamiCategoryId)
        {
            List<MarketStickerCategoryDTO> ctgList = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    MarketStickerDTO marketStickerDTO = HttpRequest.GetMoreDynamicSticker(startIndex, categoryLimit, collectionLimit, stickerType, dynamiCategoryId);
                    if (marketStickerDTO != null && marketStickerDTO.sucs == true && marketStickerDTO.catList != null)
                    {
                        ctgList = new List<MarketStickerCategoryDTO>();
                        MarketStickerDynamicCategoryDTO dynamicCategoryDTO = StickerDictionaries.Instance.MARKET_STICKER_DYNAMIC_CATEGORY_DICTIONARY.Where(P => P.id == dynamiCategoryId).FirstOrDefault();
                        if (dynamicCategoryDTO != null)
                        {
                            foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                            {
                                if (dynamiCategoryId == StickerMarketConstants.STICKER_TYPE_ALL)
                                {
                                    StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST.Add(marketCtgDTO.sCtId);
                                }

                                dynamicCategoryDTO.catIds.Add(marketCtgDTO.sCtId);
                                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                                if (ctgDTO != null)
                                {
                                    ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                                }
                                else
                                {
                                    ctgDTO = marketCtgDTO;
                                }

                                StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                                if (ctgList.Where(P => P.sCtId == ctgDTO.sCtId).FirstOrDefault() == null)
                                {
                                    ctgList.Add(marketCtgDTO);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerCategoriesOfSameType() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return ctgList;
        }

        public List<MarketStickerCategoryDTO> GetMoreStickerCategoriesOfSameType(int startIndex, int categoryLimit, int collectionLimit, int stickerType, bool initialRequest = false)
        {
            List<MarketStickerCategoryDTO> ctgList = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    MarketStickerDTO marketStickerDTO = HttpRequest.GetStickerOfParticularType(startIndex, categoryLimit, collectionLimit, stickerType);
                    if (marketStickerDTO != null && marketStickerDTO.sucs == true && marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                    {
                        ctgList = new List<MarketStickerCategoryDTO>();
                        foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                        {
                            if (stickerType == StickerMarketConstants.STICKER_TYPE_ALL)
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST.Add(marketCtgDTO.sCtId);
                            }

                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                            if (ctgDTO != null)
                            {
                                ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                            }
                            else
                            {
                                ctgDTO = marketCtgDTO;
                            }

                            StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[marketCtgDTO.sCtId] = ctgDTO;
                            if (ctgList.Where(P => P.sCtId == ctgDTO.sCtId).FirstOrDefault() == null)
                            {
                                ctgList.Add(marketCtgDTO);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerCategoriesOfSameType() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return ctgList;
        }

        public List<MarketStickerCategoryDTO> GetMoreStickerCategoriesInACollection(int startIndex, int categoryLimit, int stickerType, int collectionId)
        {
            List<MarketStickerCategoryDTO> categoriesDTOs = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    MarketStickerDTO marketStickerDTO = HttpRequest.GetStickersOfParticularCollection(startIndex, categoryLimit, stickerType, collectionId);

                    if (marketStickerDTO != null && marketStickerDTO.sucs == true)
                    {
                        if (marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                        {
                            categoriesDTOs = new List<MarketStickerCategoryDTO>();
                            foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                            {
                                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                                if (ctgDTO != null)
                                {
                                    ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                                }
                                else
                                {
                                    ctgDTO = marketCtgDTO;
                                }

                                categoriesDTOs.Add(ctgDTO);
                                StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[ctgDTO.sCtId] = ctgDTO;
                            }
                        }

                        if (marketStickerDTO.colList != null && marketStickerDTO.colList.Count > 0)
                        {
                            foreach (MarketStickerCollectionDTO collDTO in marketStickerDTO.colList) // 1 Collection
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY[collDTO.sClId] = collDTO;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerCategoriesInACollection() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return categoriesDTOs;
        }

        public List<MarketStickerCategoryDTO> GetMoreStickerCategoriesInALanguage(int startIndex, int categoryLimit, int stickerType, string languageName)
        {
            List<MarketStickerCategoryDTO> categoriesDTOs = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    MarketStickerDTO marketStickerDTO = HttpRequest.GetStickersOfParticularLanguage(startIndex, categoryLimit, stickerType, languageName);

                    if (marketStickerDTO != null && marketStickerDTO.sucs == true)
                    {
                        if (marketStickerDTO.catList != null && marketStickerDTO.catList.Count > 0)
                        {
                            categoriesDTOs = new List<MarketStickerCategoryDTO>();
                            foreach (MarketStickerCategoryDTO marketCtgDTO in marketStickerDTO.catList)
                            {
                                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgDTO.sCtId);
                                if (ctgDTO != null)
                                {
                                    ctgDTO.UpdateNewStickerCategoryDTO(marketCtgDTO);
                                }
                                else
                                {
                                    ctgDTO = marketCtgDTO;
                                }

                                categoriesDTOs.Add(ctgDTO);
                                StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[ctgDTO.sCtId] = ctgDTO;
                            }
                        }

                        if (marketStickerDTO.colList != null && marketStickerDTO.colList.Count > 0)
                        {
                            foreach (MarketStickerCollectionDTO collDTO in marketStickerDTO.colList) // 1 Collection
                            {
                                StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY[collDTO.sClId] = collDTO;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMoreStickerCategoriesInALanguage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            return categoriesDTOs;
        }

        public void GetMarketStickerImagesOfACategory(MarketStickerCategoryDTO categoryDTO, IRingMarkerSticker listener)
        {
            try
            {
                this._Listener = listener;
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    MarketStickerDTO stickerDTO = HttpRequest.GetMarketStickerImagesByCategoryID(categoryDTO.sCtId);
                    if (stickerDTO != null && stickerDTO.ImagesList != null && stickerDTO.ImagesList.Count > 0)
                    {
                        List<MarkertStickerImagesDTO> imageList = new List<MarkertStickerImagesDTO>();
                        foreach (MarkertStickerImagesDTO imageDTO in stickerDTO.ImagesList)
                        {
                            MarkertStickerImagesDTO tempImageDTO = categoryDTO.ImagesList.Where(P => P.imId == imageDTO.imId).FirstOrDefault();
                            if (tempImageDTO != null)
                            {
                                if (tempImageDTO.sClId <= 0)
                                {
                                    tempImageDTO.sClId = categoryDTO.sClId;
                                    imageList.Add(tempImageDTO);
                                }
                            }
                            else
                            {
                                imageDTO.sClId = categoryDTO.sClId;
                                imageList.Add(imageDTO);
                            }
                        }

                        if (imageList.Count > 0)
                        {
                            categoryDTO.ImagesList.AddRange(imageList);
                            if (this._Listener != null)
                            {
                                _Listener.OnMarketStickerImageInfo(categoryDTO.sCtId, imageList);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetMarketStickerImagesOfACategory() ==> " + ex.StackTrace);
            }
        }

        public void DownloadMarketStickerIcon(MarketStickerCategoryDTO categoryDTO, int imageType)
        {
            try
            {
                string folderPath = _RootPath + Path.DirectorySeparatorChar + categoryDTO.sClId + Path.DirectorySeparatorChar + categoryDTO.sCtId;
                HelperMethodsAuth.CreateDirectory(folderPath);

                string extension = imageType == RingMarketStickerService._Market_Icon_Image ? "market_" : string.Empty;
                string iconFilePath = folderPath + Path.DirectorySeparatorChar + extension + categoryDTO.Icon;

                string imgPath = imageType == RingMarketStickerService._Market_Icon_Image ? ServerAndPortSettings.D_FULL : ServerAndPortSettings.D_MINI;
                string iconUri = ServerAndPortSettings.StickerMarketDownloadUrl + "/" + imgPath + "/" + categoryDTO.sClId + "/" + categoryDTO.sCtId + "/" + categoryDTO.Icon;
                ImageFile file = new ImageFile(iconFilePath);
                if (!file.IsComplete)
                {
                    if (HttpRequest.DownloadFile(iconUri, iconFilePath))
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerIconDownloaded(categoryDTO.sCtId, imageType, true);
                        }
                    }
                    else
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerIconDownloaded(categoryDTO.sCtId, imageType, false);
                        }
                    }
                }
                else
                {
                    if (this._Listener != null)
                    {
                        this._Listener.OnMarketStickerIconDownloaded(categoryDTO.sCtId, imageType, false);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("DownloadMarketStickerIcon() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DownloadMarketStickerDetailImage(MarketStickerCategoryDTO categoryDTO, int imageType)
        {
            try
            {
                string folderPath = this._RootPath + Path.DirectorySeparatorChar + categoryDTO.sClId + Path.DirectorySeparatorChar + categoryDTO.sCtId;
                HelperMethodsAuth.CreateDirectory(folderPath);

                string extension = imageType == RingMarketStickerService._MarketDetailImage ? categoryDTO.DetailImg : categoryDTO.ThumbDetailImage;

                string detailFilePath = folderPath + Path.DirectorySeparatorChar + extension;
                string detailUri = ServerAndPortSettings.StickerMarketDownloadUrl + "/" + ServerAndPortSettings.D_FULL + "/" + categoryDTO.sClId + "/" + categoryDTO.sCtId + "/" + extension;

                ImageFile file = new ImageFile(detailFilePath);
                if (!file.IsComplete)
                {
                    if (HttpRequest.DownloadFile(detailUri, detailFilePath))
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerDetailImageDownloaded(categoryDTO.sCtId, imageType, true);
                        }
                    }
                    else
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerDetailImageDownloaded(categoryDTO.sCtId, imageType, false);
                        }
                    }
                }
                else
                {
                    if (this._Listener != null)
                    {
                        this._Listener.OnMarketStickerDetailImageDownloaded(categoryDTO.sCtId, imageType, false);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("DownloadMarketStickerDetailImage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DownloadMarketBannerImage(MarketStickerCategoryDTO categoryDTO, int imageType)
        {
            try
            {
                string folderPath = this._RootPath + Path.DirectorySeparatorChar + categoryDTO.sClId + Path.DirectorySeparatorChar + categoryDTO.sCtId;
                HelperMethodsAuth.CreateDirectory(folderPath);

                string extension = imageType == RingMarketStickerService._MarketBannerImageMini ? "s" : string.Empty;
                string detailFilePath = folderPath + Path.DirectorySeparatorChar + extension + categoryDTO.cgBnrImg;

                //string imgPath = imageType == RingMarketStickerService._MarketBannerImageMini ? ServerAndPortSettings.D_MINI : ServerAndPortSettings.D_FULL;
                string detailUri = ServerAndPortSettings.StickerMarketDownloadUrl + "/" + ServerAndPortSettings.D_FULL + "/" + categoryDTO.sClId + "/" + categoryDTO.sCtId + "/" + extension + categoryDTO.cgBnrImg;

                ImageFile file = new ImageFile(detailFilePath);
                if (!file.IsComplete)
                {
                    if (HttpRequest.DownloadFile(detailUri, detailFilePath))
                    {
                        if (this._Listener != null)
                        {
                            this._Listener.OnMarketStickerBannerImageDownloaded(categoryDTO.sCtId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("DownloadMarketBannerImage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DownloadMarketImagList(MarketStickerCategoryDTO categoryDTO)
        {
            try
            {
                bool isDownloaded = true;
                List<MarkertStickerImagesDTO> imageList = null;
                if ((DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null) || categoryDTO.Downloaded)
                {
                    imageList = categoryDTO.ImagesList.ToList();
                    foreach (MarkertStickerImagesDTO imageDTO in imageList)
                    {
                        string folderPath = this._RootPath + Path.DirectorySeparatorChar + imageDTO.sClId + Path.DirectorySeparatorChar + imageDTO.sCtId;
                        string filePath = folderPath + Path.DirectorySeparatorChar + imageDTO.imUrl;
                        string uri = ServerAndPortSettings.StickerMarketDownloadUrl + "/" + ServerAndPortSettings.D_FULL + "/" + imageDTO.sClId + "/" + imageDTO.sCtId + "/" + imageDTO.imUrl;

                        ImageFile file = new ImageFile(filePath);
                        if (file.IsComplete || HttpRequest.DownloadFile(uri, filePath))
                        {
                            if (this._Listener != null)
                            {
                                this._Listener.OnMarketStickerImageDownloaded(categoryDTO.sCtId, imageDTO.imId);
                            }
                        }
                        else
                        {
                            isDownloaded = false;
                        }
                    }
                }
                else
                {
                    isDownloaded = false;
                }

                bool status = isDownloaded && imageList != null && imageList.Count > 0;
                this._Listener.OnMarketStickerImageListDownloaded(categoryDTO.sCtId, status);
            }
            catch (Exception ex)
            {
                log.Error("DownloadMarketImagList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            finally
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null && categoryDTO != null)
                {
                    HttpRequest.IncreaseDownloadCount(categoryDTO.sCtId);
                }
            }
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

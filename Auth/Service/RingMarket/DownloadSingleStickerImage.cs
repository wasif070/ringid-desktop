﻿using Auth.utility;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Auth.Service.RingMarket
{
    public class DownloadSingleStickerImage
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(DownloadSingleStickerImage).Name);

        private string _FilePath;
        private string _Message;

        public delegate void CompleteHandler(bool status);
        public event CompleteHandler OnComplete;

        public DownloadSingleStickerImage(string filePath, string message)
        {
            this._FilePath = filePath;
            this._Message = message;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                string[] splits = _Message.Split('/');
                int clID = int.Parse(splits[0].Trim());
                int ctID = int.Parse(splits[1].Trim());
                string imageName = splits[2].Trim();

                HelperMethodsAuth.CreateDirectory(Path.GetDirectoryName(_FilePath));
                string imageUri = ServerAndPortSettings.StickerMarketDownloadUrl + Path.AltDirectorySeparatorChar + ServerAndPortSettings.D_FULL + Path.AltDirectorySeparatorChar + clID + Path.AltDirectorySeparatorChar + ctID + Path.AltDirectorySeparatorChar + imageName;
                if (HttpRequest.DownloadImageFile(imageUri, _FilePath))
                {
                    if (OnComplete != null)
                    {
                        OnComplete(true);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception in DownloadSingleStickerImage ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }

            if (OnComplete != null)
            {
                OnComplete(false);
            }

        }

    }
}

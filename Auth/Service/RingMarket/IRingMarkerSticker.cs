﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auth.Service.RingMarket
{
    public interface IRingMarkerSticker
    {
        void OnInit();
        void OnOnMarketStickerNewCount(int totalNewCount, int unSeenNewCount);
        void OnMarketStickerCategoryInfo(List<MarketStickerCategoryDTO> ctgDTos, bool isReecnt);
        void OnMarketStickerCollectionInfo(List<MarketStickerCollectionDTO> ctgDTos);
        void OnMarketLaguageInfo(List<MarketStickerLanguageyDTO> langDTOs);
        
        void OnMarketStickerImageInfo(int categoryId, List<MarkertStickerImagesDTO> imgDTos);
        void OnMarketStickerIconDownloaded(int categoryID, int imageType, bool status);
        void OnMarketStickerBannerImageDownloaded(int categoryID);
        void OnMarketStickerDetailImageDownloaded(int categoryID, int imageType, bool isSucces);
        void OnMarketStickerImageDownloaded(int categoryID, int imageID);
        void OnMarketStickerImageListDownloaded(int categoryID, bool status);
        void OnComplete();
    }
}

﻿using System;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;

namespace Auth.Service.SigninSignup
{
    public class SocketPorts
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(SocketPorts).Name);
        #endregion "Fields"

        #region "Public Methods"

        public CommunicationPortsDTO SetSocketPortsWithNewRingId()
        {
            if (SetBaseRacURLs())
            {
                CommunicationPortsDTO dto = HttpRequest.GetNewRingID();
                return setIPPortIfSuccess(0, dto);
            }
            else return null;
        }

        public CommunicationPortsDTO SetSocketPortsByUnusedRingId(long ringid)
        {
            if (SetBaseRacURLs())
            {
                int loginType = SettingsConstants.RINGID_LOGIN;
                CommunicationPortsDTO dto = ringid == 0 ? null : HttpRequest.GetCommunicationPortsByRingID(ringid.ToString());
                if (dto == null || !dto.IsSuccess) loginType = 0;
                dto = HttpRequest.GetNewRingID();
                return setIPPortIfSuccess(loginType, dto);
            }
            else return null;
        }

        public CommunicationPortsDTO SetSocketPortsByRingId(string ringid)
        {
            if (SetBaseRacURLs())
            {
                CommunicationPortsDTO dto = HttpRequest.GetCommunicationPortsByRingID(ringid);
                return setIPPortIfSuccess(SettingsConstants.RINGID_LOGIN, dto);
            }
            else return null;
        }

        public CommunicationPortsDTO SetSocketPortsByMobile(string mbl, string mbldc)
        {
            if (mbldc.Equals("+880") && mbl[0] == '0') mbl = mbl.Substring(1);
            if (SetBaseRacURLs())
            {
                CommunicationPortsDTO comPorts = HttpRequest.GetCommunicationPortsByMobile(mbl, mbldc);
                return setIPPortIfSuccess(SettingsConstants.MOBILE_LOGIN, comPorts);
            }
            else return null;
        }

        public CommunicationPortsDTO SetSocketPortsByEmail(string email)
        {
            if (SetBaseRacURLs())
            {
                CommunicationPortsDTO comPorts = HttpRequest.GetCommunicationPortsByEmail(email);
                return setIPPortIfSuccess(SettingsConstants.EMAIL_LOGIN, comPorts);
            }
            else return null;
        }

        public CommunicationPortsDTO SetSocketPortsBySocialMedia(int loginType, string socialMediaID)
        {
            if (SetBaseRacURLs())
            {
                CommunicationPortsDTO comPorts = HttpRequest.GetCommunicationPortsSocialMedia(loginType, socialMediaID);
                return setIPPortIfSuccess(loginType, comPorts);
            }
            else return null;
        }

        #endregion //"Public Methods"

        #region "Private Methods"

        private bool SetBaseRacURLs()
        {
            try
            {
                CommunicationServerDTO communicationServerDTO = HttpRequest.GetBaseRacURLs();
                if (communicationServerDTO != null && communicationServerDTO.IsSuccess)
                {
                    ServerAndPortSettings.AUTHENTICATION_CONTROLLER = communicationServerDTO.AuthServer;
                    ServerAndPortSettings.IMAGE_SERVER_UPLOAD_API = communicationServerDTO.ImageServer;
                    ServerAndPortSettings.IMAGE_SERVER_RESOURCE = communicationServerDTO.ImageServerResource;
                    ServerAndPortSettings.STICKER_MARKET_API = communicationServerDTO.StickerServer;
                    ServerAndPortSettings.STICKER_MARKET_RESOURCE = communicationServerDTO.StickerServerResource;
                    ServerAndPortSettings.VOD_SERVER_UPLOAD_API = communicationServerDTO.VodServer;
                    ServerAndPortSettings.VOD_SERVER_RESOURCE = communicationServerDTO.VodServerResource;
                    ServerAndPortSettings.BASE_WEB_URL = communicationServerDTO.BaseWebURL;
                    ServerAndPortSettings.WALLET_PAYMENT_GATEWAY_URL = communicationServerDTO.WalletPaymentGateway;

                    if (!string.IsNullOrEmpty(ServerAndPortSettings.AUTHENTICATION_CONTROLLER)
                         && !string.IsNullOrEmpty(ServerAndPortSettings.IMAGE_SERVER_UPLOAD_API)
                         && !string.IsNullOrEmpty(ServerAndPortSettings.IMAGE_SERVER_RESOURCE)
                         && !string.IsNullOrEmpty(ServerAndPortSettings.STICKER_MARKET_API)
                         && !string.IsNullOrEmpty(ServerAndPortSettings.STICKER_MARKET_RESOURCE)
                         && !string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_UPLOAD_API)
                         && !string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE)
                         && !string.IsNullOrEmpty(ServerAndPortSettings.BASE_WEB_URL)
                         && !string.IsNullOrEmpty(ServerAndPortSettings.WALLET_PAYMENT_GATEWAY_URL))
                    {
                        return true;
                    }
                }
                //if (!string.IsNullOrEmpty(ServerAndPortSettings.AUTHENTICATION_CONTROLLER)
                //    && !string.IsNullOrEmpty(ServerAndPortSettings.IMAGE_SERVER_UPLOAD_API)
                //    && !string.IsNullOrEmpty(ServerAndPortSettings.IMAGE_SERVER_RESOURCE)
                //    && !string.IsNullOrEmpty(ServerAndPortSettings.STICKER_MARKET_API)
                //    && !string.IsNullOrEmpty(ServerAndPortSettings.STICKER_MARKET_RESOURCE)
                //    && !string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_UPLOAD_API)
                //    && !string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE)
                //    && !string.IsNullOrEmpty(ServerAndPortSettings.BASE_WEB_URL)
                //    && !string.IsNullOrEmpty(ServerAndPortSettings.WALLET_PAYMENT_GATEWAY_URL))
                //{
                //    return true;
                //}
                //else
                //{
                //    CommunicationServerDTO communicationServerDTO = HttpRequest.GetBaseRacURLs();
                //    if (communicationServerDTO != null && communicationServerDTO.IsSuccess)
                //    {
                //        ServerAndPortSettings.AUTHENTICATION_CONTROLLER = communicationServerDTO.AuthServer;
                //        ServerAndPortSettings.IMAGE_SERVER_UPLOAD_API = communicationServerDTO.ImageServer;
                //        ServerAndPortSettings.IMAGE_SERVER_RESOURCE = communicationServerDTO.ImageServerResource;
                //        ServerAndPortSettings.STICKER_MARKET_API = communicationServerDTO.StickerServer;
                //        ServerAndPortSettings.STICKER_MARKET_RESOURCE = communicationServerDTO.StickerServerResource;
                //        ServerAndPortSettings.VOD_SERVER_UPLOAD_API = communicationServerDTO.VodServer;
                //        ServerAndPortSettings.VOD_SERVER_RESOURCE = communicationServerDTO.VodServerResource;
                //        ServerAndPortSettings.BASE_WEB_URL = communicationServerDTO.BaseWebURL;
                //        ServerAndPortSettings.WALLET_PAYMENT_GATEWAY_URL = communicationServerDTO.WalletPaymentGateway;

                //        if (!string.IsNullOrEmpty(ServerAndPortSettings.AUTHENTICATION_CONTROLLER)
                //             && !string.IsNullOrEmpty(ServerAndPortSettings.IMAGE_SERVER_UPLOAD_API)
                //             && !string.IsNullOrEmpty(ServerAndPortSettings.IMAGE_SERVER_RESOURCE)
                //             && !string.IsNullOrEmpty(ServerAndPortSettings.STICKER_MARKET_API)
                //             && !string.IsNullOrEmpty(ServerAndPortSettings.STICKER_MARKET_RESOURCE)
                //             && !string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_UPLOAD_API)
                //             && !string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE)
                //             && !string.IsNullOrEmpty(ServerAndPortSettings.BASE_WEB_URL)
                //             && !string.IsNullOrEmpty(ServerAndPortSettings.WALLET_PAYMENT_GATEWAY_URL))
                //        {
                //            return true;
                //        }
                //    }
                //}
            }
            catch (Exception e) { log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message); }
            return false;
        }

        private CommunicationPortsDTO setIPPortIfSuccess(int loginType, CommunicationPortsDTO communicationPortsDTO)
        {
            if (communicationPortsDTO != null
                && communicationPortsDTO.IsSuccess
                && !string.IsNullOrEmpty(communicationPortsDTO.AuthServerIP)
                && communicationPortsDTO.ComPort > 0)
            {
                ServerAndPortSettings.AUTH_SERVER_IP = communicationPortsDTO.AuthServerIP;
                ServerAndPortSettings.COMMUNICATION_PORT = communicationPortsDTO.ComPort;
                if (loginType == 0
                    && !string.IsNullOrEmpty(communicationPortsDTO.RingID)
                    && long.Parse(communicationPortsDTO.RingID) > 0)
                {
                    DefaultSettings.VALUE_NEW_USER_NAME = long.Parse(communicationPortsDTO.RingID);
                    new DatabaseActivityDAO().SaveNewUserIDintoDB();
                }
                else if (loginType == 0) DefaultSettings.VALUE_NEW_USER_NAME = 0;
                return communicationPortsDTO;
            }
            return null;
        }

        #endregion //"Private Methods"
    }
}

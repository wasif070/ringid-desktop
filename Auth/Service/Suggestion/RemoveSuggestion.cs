﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Auth.Service.Suggestion
{
    //public class RemoveSuggestion
    //{
    //    private readonly ILog log = LogManager.GetLogger(typeof(RemoveSuggestion).Name);
    //    private long utid;
    //    private long userIdentity;

    //    public RemoveSuggestion(long userIdentity, long utid)
    //    {
    //        this.userIdentity = userIdentity;
    //        this.utid = utid;
    //        // Thread th = new Thread(new ThreadStart(run));
    //        // th.Start();
    //    }

    //    public void Run(out bool isSuccess, out UserBasicInfoDTO userBasicInfo, out string msg)
    //    {
    //        msg = "";
    //        isSuccess = false;
    //        userBasicInfo = null;
    //        if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //        {
    //            try
    //            {
    //                JObject pakToSend = new JObject();
    //                string pakId = SendToServer.GetRanDomPacketID();
    //                pakToSend[JsonKeys.PacketId] = pakId;
    //                pakToSend[JsonKeys.Action] = AppConstants.TYPE_REMOVE_SUGGESTION;
    //                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //                pakToSend[JsonKeys.UserTableId] = utid;


    //                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //                SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
    //                Thread.Sleep(25);
    //                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
    //                {
    //                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
    //                    {
    //                        break;
    //                    }
    //                    Thread.Sleep(DefaultSettings.WAITING_TIME);
    //                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
    //                    {
    //                        if (i % DefaultSettings.SEND_INTERVAL == 0)
    //                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
    //                    }
    //                    else
    //                    {
    //                        JObject feedbackfields = null;
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
    //                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
    //                        {

    //                            isSuccess = true;
    //                            FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.TryGetValue(userIdentity, out userBasicInfo);

    //                            FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(utid);
    //                            FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userIdentity);

    //                        }
    //                        else
    //                        {
    //                            if (feedbackfields[JsonKeys.Message] != null)
    //                            {
    //                                msg = (string)feedbackfields[JsonKeys.Message];
    //                            }
    //                            else if (feedbackfields[JsonKeys.ReasonCode] != null)
    //                            {
    //                                msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
    //                            }
    //                        }
    //                        break;
    //                    }

    //                }
    //                JObject obj;
    //                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out obj);
    //                return;
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("RemoveSuggestion ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }
    //        else
    //        {
    //            log.Error("RemoveSuggestion Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //        }
    //    }
    //}
}

﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Auth.Service.Images
{
    public class FeedImagesUpload
    {
        private readonly ILog log = LogManager.GetLogger(typeof(FeedImagesUpload).Name);
        private string _lineEnd = "\r\n";
        private string _twoHyphens = "--";
        private string _boundary = "*****";      
        private byte[] _boundarybytes, _trailerbytes;
        private HttpWebRequest httpWebRequest;
        private  string _serverResponse;

        private static FeedImagesUpload _instance;
        public static FeedImagesUpload Instance
        {
            get
            {
                _instance = _instance ?? new FeedImagesUpload();
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        private void AddDefaultDataWithImage(Dictionary<string, object> _headerContents)
        {
            _headerContents.Add("sId", DefaultSettings.LOGIN_SESSIONID);
            _headerContents.Add("uId", DefaultSettings.LOGIN_RING_ID);
            _headerContents.Add("authServer", ServerAndPortSettings.AUTH_SERVER_IP);
            _headerContents.Add("comPort", ServerAndPortSettings.COMMUNICATION_PORT);
            _headerContents.Add("x-app-version", AppConfig.VERSION_PC);
        }

        public async Task<byte[]> UploadThumbImageToImageServerWebRequest(byte[] imageData, int cropW, int cropH)
        {
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                var bytes = default(byte[]);
                try
                {
                    _boundarybytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _lineEnd);
                    _trailerbytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _twoHyphens + _lineEnd);

                    Dictionary<string, object> _headerContents = new Dictionary<string, object>();
                    AddDefaultDataWithImage(_headerContents);
                    //_headerContents.Add("cimX", cropX);
                    //_headerContents.Add("cimY", cropY);
                    _headerContents.Add("iw", cropW);
                    _headerContents.Add("ih", cropH);


                    httpWebRequest = (HttpWebRequest)WebRequest.Create(ServerAndPortSettings.GetVODThumbImageUploadingURL);
                    httpWebRequest.ContentType = "multipart/form-data; boundary=" + _boundary;
                    httpWebRequest.UserAgent = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Headers["access-control-allow-origin"] = "*";
                    httpWebRequest.Proxy = null;

                    using (var stream = await Task.Factory.FromAsync<Stream>(httpWebRequest.BeginGetRequestStream, httpWebRequest.EndGetRequestStream, null))
                    {
                        SetRequestHeader("pcupload.jpg", imageData, stream, _headerContents);

                        using (var webResponse = await Task.Factory.FromAsync<WebResponse>(httpWebRequest.BeginGetResponse, httpWebRequest.EndGetResponse, null))
                        using (Stream responseStream = webResponse.GetResponseStream())
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        using (var memstream = new MemoryStream())
                        {
                            streamReader.BaseStream.CopyTo(memstream);
                            bytes = memstream.ToArray();
                        }
                    }

                }
                catch (Exception e)
                {
                    log.Error("Exception in UploadThumbImageToImageServerWebRequest() ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
                finally
                {
                    if (httpWebRequest != null)
                    {
                        httpWebRequest.Abort();
                        httpWebRequest = null;
                    }
                }
                return bytes;
            }
            else
            {
                log.Error("UploadThumbImageToImageServerWebRequest() Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                return null;
            }
        }
        public async Task<string> UploadToImageServerWebRequest(byte[] imageData, int imageType, int cropW, int cropH)
        {
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                _serverResponse = String.Empty;
                try
                {
                    _boundarybytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _lineEnd);
                    _trailerbytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _twoHyphens + _lineEnd);

                    Dictionary<string, object> _headerContents = new Dictionary<string, object>();
                    AddDefaultDataWithImage(_headerContents);
                    //_headerContents.Add("cimX", cropX);
                    //_headerContents.Add("cimY", cropY);
                    _headerContents.Add("iw", cropW);
                    _headerContents.Add("ih", cropH);
                    _headerContents.Add("imT", imageType);


                    httpWebRequest = (HttpWebRequest)WebRequest.Create(ServerAndPortSettings.GetAlbumImageUploadingURL);
                    httpWebRequest.ContentType = "multipart/form-data; boundary=" + _boundary;
                    httpWebRequest.Method = "POST";

                    using (var stream = await Task.Factory.FromAsync<Stream>(httpWebRequest.BeginGetRequestStream, httpWebRequest.EndGetRequestStream, null))
                    {
                        SetRequestHeader("pcupload.jpg", imageData, stream, _headerContents);

                        using (var webResponse = await Task.Factory.FromAsync<WebResponse>(httpWebRequest.BeginGetResponse, httpWebRequest.EndGetResponse, null))
                        using (Stream responseStream = webResponse.GetResponseStream())
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        {
                            _serverResponse = streamReader.ReadToEnd();
                        }
                    }

                }
                catch (Exception e)
                {
                    log.Error("Exception in UploadToImageServerWebRequest() ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
                finally
                {
                    if (httpWebRequest != null)
                    {
                        httpWebRequest.Abort();
                        httpWebRequest = null;
                    }
                }
                return _serverResponse;
            }
            else
            {
                log.Error("UploadToImageServerWebRequest() Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                return string.Empty;
            }
        }

        private void SetRequestHeader(string fileName, byte[] fileBytes, Stream stream, Dictionary<string, object> headerContents)
        {
            try
            {
                /// the form-data file upload, properly formatted
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;

                //WriteToStream Method for Dictionary Add
                foreach (var headerContent in headerContents)
                {
                    WriteToStream(stream, _boundarybytes);
                    string a = string.Format(fileheaderTemplate, headerContent.Key, headerContent.Value);
                    WriteToStream(stream, a);
                }

                WriteToStream(stream, _boundarybytes);
                WriteToStream(stream, "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + fileName + "\"" + _lineEnd);
                WriteToStream(stream, _lineEnd);
                WriteToStream(stream, fileBytes);
                WriteToStream(stream, _lineEnd);
                WriteToStream(stream, _trailerbytes);

                stream.Close();
            }
            catch (Exception ex)
            {
                log.Error("Exception in SetRequestHeader() ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        private static void WriteToStream(Stream s, string txt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(txt);
            s.Write(bytes, 0, bytes.Length);
        }

        private static void WriteToStream(Stream s, byte[] bytes)
        {
            s.Write(bytes, 0, bytes.Length);
        }
    }
}

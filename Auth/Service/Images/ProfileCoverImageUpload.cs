﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;

namespace Auth.Service.Images
{
    public class ProfileCoverImageUpload
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ProfileCoverImageUpload).Name);
        string _lineEnd = "\r\n";
        string _twoHyphens = "--";
        string _boundary = "*****";
        byte[] _boundarybytes, _trailerbytes;
        HttpWebRequest httpWebRequest;
        string _serverResponse;

        private static ProfileCoverImageUpload _instance;
        public static ProfileCoverImageUpload Instance
        {
            get
            {
                _instance = _instance ?? new ProfileCoverImageUpload();
                return _instance;
            }
            set { _instance = value; }
        }

        private void AddDefaultDataWithImage(Dictionary<string, object> _headerContents)
        {
            _headerContents.Add("sId", DefaultSettings.LOGIN_SESSIONID);
            _headerContents.Add("uId", DefaultSettings.LOGIN_RING_ID);
            _headerContents.Add("authServer", ServerAndPortSettings.AUTH_SERVER_IP);
            _headerContents.Add("comPort", ServerAndPortSettings.COMMUNICATION_PORT);
            _headerContents.Add("x-app-version", AppConfig.VERSION_PC);
        }

        public string UploadToImageServerWebRequest(byte[] imageData, int imageType, double cropX = 0, double cropY = 0, int cropW = 0, int cropH = 0)
        {
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                _serverResponse = String.Empty;
                try
                {
                    _boundarybytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _lineEnd);
                    _trailerbytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _twoHyphens + _lineEnd);

                    Dictionary<string, object> _headerContents = new Dictionary<string, object>();
                    AddDefaultDataWithImage(_headerContents);

                    if (imageType == SettingsConstants.TYPE_GROUP_IMAGE) httpWebRequest = (HttpWebRequest)WebRequest.Create(ServerAndPortSettings.GetGroupImageUploadingURL);
                    else
                    {
                        _headerContents.Add("cimX", cropX);
                        _headerContents.Add("cimY", cropY);
                        _headerContents.Add("iw", cropW);
                        _headerContents.Add("ih", cropH);
                        _headerContents.Add("imT", imageType);
                        httpWebRequest = (HttpWebRequest)WebRequest.Create(ServerAndPortSettings.GetProfileImageUploadingURL);
                    }
                    httpWebRequest.ContentType = "multipart/form-data; boundary=" + _boundary;
                    httpWebRequest.UserAgent = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Headers["access-control-allow-origin"] = "*";
                    httpWebRequest.Proxy = null;

                    using (var stream = httpWebRequest.GetRequestStream()/*await Task.Factory.FromAsync<Stream>(httpWebRequest.BeginGetRequestStream, httpWebRequest.EndGetRequestStream, null)*/)
                    {
                        SetRequestHeader("pcupload.jpg", imageData, stream, _headerContents);
                        using (var webResponse = httpWebRequest.GetResponse()/*await Task.Factory.FromAsync<WebResponse>(httpWebRequest.BeginGetResponse, httpWebRequest.EndGetResponse, null)*/)
                        using (Stream responseStream = webResponse.GetResponseStream())
                        using (StreamReader streamReader = new StreamReader(responseStream))
                            _serverResponse = imageType == SettingsConstants.TYPE_GROUP_IMAGE ? GetImageUploadResponse(streamReader.ReadToEnd()) : streamReader.ReadToEnd();
                    }
                }
                catch (Exception e) { log.Error("Exception in UploadToImageServerWebRequest()==>" + e.StackTrace); }
                finally
                {
                    if (httpWebRequest != null)
                    {
                        httpWebRequest.Abort();
                        httpWebRequest = null;
                    }
                }
                return _serverResponse;
            }
            else
            {
                log.Error("UploadToImageServerWebRequest() Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                return string.Empty;
            }
        }

        private void SetRequestHeader(string fileName, byte[] fileBytes, Stream stream, Dictionary<string, object> headerContents)
        {
            try
            {
                /// the form-data file upload, properly formatted
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;
                foreach (var headerContent in headerContents)
                {
                    WriteToStream(stream, _boundarybytes);
                    string a = string.Format(fileheaderTemplate, headerContent.Key, headerContent.Value);
                    WriteToStream(stream, a);
                }
                WriteToStream(stream, _boundarybytes);
                WriteToStream(stream, "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + fileName + "\"" + _lineEnd);
                WriteToStream(stream, _lineEnd);
                WriteToStream(stream, fileBytes);
                WriteToStream(stream, _lineEnd);
                WriteToStream(stream, _trailerbytes);
                stream.Close();
            }
            catch (Exception ex) { log.Error("Exception in SetRequestHeader() ==>" + ex.StackTrace); }
        }

        private static void WriteToStream(Stream s, string txt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(txt);
            s.Write(bytes, 0, bytes.Length);
        }

        private static void WriteToStream(Stream s, byte[] bytes)
        {
            s.Write(bytes, 0, bytes.Length);
        }

        public string UpdateCropImageToImageServerWebRequest(double cropX, double cropY, int cropW, int cropH, string imgUrl)
        {
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                string _response = string.Empty;
                try
                {
                    _boundarybytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _lineEnd);
                    _trailerbytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _twoHyphens + _lineEnd);

                    Dictionary<string, object> _headerContents = new Dictionary<string, object>();
                    AddDefaultDataWithImage(_headerContents);

                    _headerContents.Add("cimX", cropX);
                    _headerContents.Add("cimY", cropY);
                    _headerContents.Add("iw", cropW);
                    _headerContents.Add("ih", cropH);
                    _headerContents.Add("origFile", imgUrl);

                    httpWebRequest = (HttpWebRequest)WebRequest.Create(ServerAndPortSettings.GetCropImageUploadingURL);
                    httpWebRequest.ContentType = "multipart/form-data; boundary=" + _boundary;
                    httpWebRequest.UserAgent = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Headers["access-control-allow-origin"] = "*";

                    using (var stream = httpWebRequest.GetRequestStream())
                    {
                        string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;
                        foreach (var headerContent in _headerContents)
                        {
                            WriteToStream(stream, _boundarybytes);
                            string a = string.Format(fileheaderTemplate, headerContent.Key, headerContent.Value);
                            WriteToStream(stream, a);
                        }
                        WriteToStream(stream, _trailerbytes);
                        stream.Close();
                        using (var webResponse = httpWebRequest.GetResponse()/*await Task.Factory.FromAsync<WebResponse>(httpWebRequest.BeginGetResponse, httpWebRequest.EndGetResponse, null)*/)
                        using (Stream responseStream = webResponse.GetResponseStream())
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            if (responseStream != null)
                            {
                                responseStream.CopyTo(memoryStream);
                                byte[] responseBytes = memoryStream.ToArray();
                                if (responseBytes.Length > 0 && responseBytes[0] == 1) _response = Encoding.UTF8.GetString(responseBytes, 2, responseBytes[1]);
                            }
                            memoryStream.Close();
                        }
                    }
                }
                catch (Exception e) { log.Error("UpdateCropImageToImageServerWebRequest()==>" + e.StackTrace); }
                finally
                {
                    if (httpWebRequest != null)
                    {
                        httpWebRequest.Abort();
                        httpWebRequest = null;
                    }
                }
                return _response;
            }
            else
            {
                log.Error("UpdateCropImageToImageServerWebRequest() ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                return string.Empty;
            }
        }

        private string GetImageUploadResponse(string response)
        {
            string imageUrl = String.Empty;
            try
            {
                JObject responseObj = JObject.Parse(response);
                if (responseObj != null && (bool)responseObj[JsonKeys.Success]) imageUrl = (string)responseObj[JsonKeys.ImageUrl];
            }
            catch (Exception ex) { log.Error("Exception in GetGroupImageUploadResponse()==>" + ex.StackTrace); }
            return imageUrl;
        }

        private string GetAnonymousImageUploadResponse(byte[] response)
        {
            string imageUrl = String.Empty;
            try
            {
                int totalRead = 0;
                int status = response[0];
                totalRead++;
                int msgLength = response[totalRead];
                totalRead++;
                if (status == 1) imageUrl = Encoding.UTF8.GetString(response, totalRead, msgLength);
            }
            catch (Exception ex) { log.Error("Exception in GetAnonymousImageUploadResponse()==>" + ex.StackTrace + "==>" + ex.Message); }
            return imageUrl;
        }
    }
}

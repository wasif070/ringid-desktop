﻿
namespace Auth.Service.Feed
{
    //    public class AddMultipleImages
    //    {
    //        private readonly ILog log = LogManager.GetLogger(typeof(AddMultipleImages).Name);
    //        private int type; //1 for feed,2 on friends wall,3 on circle
    //        private JObject pakToSend;
    //        private long frndid_or_circleId;
    //        private LocationDTO locationDTO;
    //        private List<long> taggedUtids;
    //        private long DoingId;
    //        private bool NoResponse = true;
    //        private int pvc;

    //        public AddMultipleImages(int pvc, JObject Jobj, int type, long frndid_or_circleId, LocationDTO locationDTO, List<long> taggedUtids, long DoingId)
    //        {
    //            this.pvc = pvc;
    //            this.pakToSend = Jobj;
    //            this.type = type;
    //            this.frndid_or_circleId = frndid_or_circleId;
    //            this.locationDTO = locationDTO;
    //            this.taggedUtids = taggedUtids;
    //            this.DoingId = DoingId;
    //            Thread th = new Thread(new ThreadStart(Run));
    //            th.Start();
    //        }

    //        public void Run()
    //        {
    //            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //            {
    //                try
    //                {
    //                    string pakId = SendToServer.GetRanDomPacketID();
    //                    pakToSend[JsonKeys.PacketId] = pakId;
    //                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_MULTIPLE_IMAGE_POST;
    //                    //if (pvc > 0) pakToSend[JsonKeys.FeedPrivacy] = pvc;
    //                    pakToSend[JsonKeys.Type] = SettingsConstants.NEWS_FEED_TYPE_MULTIPLE_IMAGE;

    //                    if (type == 2) { pakToSend[JsonKeys.FriendId] = frndid_or_circleId; pakToSend[JsonKeys.FeedPrivacy] = 2; }
    //                    else if (type == 3) { pakToSend[JsonKeys.GroupId] = frndid_or_circleId; pakToSend[JsonKeys.FeedPrivacy] = 2; }
    //                    else if (pvc > 0) pakToSend[JsonKeys.FeedPrivacy] = pvc;

    //                    if (locationDTO != null)
    //                    {
    //                        pakToSend[JsonKeys.Location] = locationDTO.loc;
    //                        pakToSend[JsonKeys.Latitude] = locationDTO.lat;
    //                        pakToSend[JsonKeys.Longitude] = locationDTO.lon;
    //                    }
    //                    if (taggedUtids != null)
    //                    {
    //                        JArray array = new JArray();
    //                        foreach (long item in taggedUtids)
    //                        {
    //                            array.Add(item);
    //                        }
    //                        pakToSend[JsonKeys.TaggedFriendUtIds] = array;
    //                    }
    //                    if (this.DoingId > 0)
    //                    {
    //                        JArray array = new JArray();
    //                        array.Add(DoingId);
    //                        pakToSend[JsonKeys.MoodIds] = array;
    //                    }

    //                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //                    Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, AppConstants.TYPE_MULTIPLE_IMAGE_POST, pakId);
    //                    int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
    //                    List<string> packetIds = new List<string>(packets.Keys);
    //                    SendToServer.SendBrokenPacket(packetType, packets);
    //                    Thread.Sleep(25);
    //                    string makeResponsepak = pakId + "_" + AppConstants.TYPE_MULTIPLE_IMAGE_POST;
    //                    for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
    //                    {
    //                        if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
    //                        {
    //                            break;
    //                        }
    //                        Thread.Sleep(DefaultSettings.WAITING_TIME);
    //                        if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsepak))
    //                        {
    //                            if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
    //                            {
    //                                if (i % DefaultSettings.SEND_INTERVAL == 0)
    //                                {
    //                                    SendToServer.SendBrokenPacket(packetType, packets);
    //                                }
    //                            }
    //                        }
    //                        else
    //                        {
    //                            JObject feedbackfields;
    //                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsepak, out feedbackfields);
    //                            if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
    //                            {
    //                            }
    //                            else if (feedbackfields[JsonKeys.Message] != null)
    //                            {
    //                            }
    //                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(makeResponsepak, out feedbackfields);
    //                            NoResponse = false;
    //                            break;
    //                        }
    //                        PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
    //                    }
    //                    SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
    //                }
    //                catch (Exception e)
    //                {

    //                    log.Error("AddMultipleImages ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

    //                }
    //            }
    //            else
    //            {
    //#if AUTH_LOG
    //                log.Error("AddMultipleImages Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //#endif
    //                HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(NotificationMessages.INTERNET_UNAVAILABLE, "Failed!");
    //            }
    //            HelperMethodsAuth.NewsFeedHandlerInstance.EnablePostBtn(type, frndid_or_circleId, !NoResponse);
    //        }
    //    }
}

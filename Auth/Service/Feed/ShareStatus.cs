﻿
namespace Auth.Service.Feed
{
    //public class ShareStatus
    //{
    //    private readonly ILog log = LogManager.GetLogger(typeof(ShareStatus).Name);
    //    private string status;
    //    private long shareFeedId;
    //    private LocationDTO locationDTO;
    //    private List<long> taggedUtids;
    //    private long FActivityId;
    //    private JArray stsTags;

    //    public ShareStatus(string status, JArray stsTags, long shareFeedId, LocationDTO locationDTO, List<long> taggedUtids, long FActivityId)
    //    {
    //        this.status = status;
    //        this.stsTags = stsTags;
    //        this.shareFeedId = shareFeedId;
    //        this.locationDTO = locationDTO;
    //        this.taggedUtids = taggedUtids;
    //        this.FActivityId = FActivityId;
    //        Thread th = new Thread(new ThreadStart(Run));
    //        th.Start();
    //    }

    //    public void Run()
    //    {
    //        if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
    //        {
    //            try
    //            {
    //                JObject pakToSend = new JObject();
    //                string pakId = SendToServer.GetRanDomPacketID();
    //                pakToSend[JsonKeys.PacketId] = pakId;
    //                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
    //                pakToSend[JsonKeys.Status] = status;
    //                if (stsTags != null) pakToSend[JsonKeys.StatusTags] = stsTags;
    //                pakToSend[JsonKeys.Type] = SettingsConstants.NEWS_FEED_TYPE_TEXT_STATUS;
    //                pakToSend[JsonKeys.SharedFeedId] = shareFeedId;
    //                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SHARE_STATUS;
    //                if (locationDTO != null)
    //                {
    //                    pakToSend[JsonKeys.Location] = locationDTO.loc;
    //                    pakToSend[JsonKeys.Latitude] = locationDTO.lat;
    //                    pakToSend[JsonKeys.Longitude] = locationDTO.lon;
    //                }
    //                if (taggedUtids != null)
    //                {
    //                    JArray array = new JArray();
    //                    foreach (long item in taggedUtids)
    //                    {
    //                        array.Add(item);
    //                    }
    //                    pakToSend[JsonKeys.TaggedFriendUtIds] = array;
    //                }
    //                if (this.FActivityId > 0)
    //                {
    //                    JArray array = new JArray();
    //                    array.Add(FActivityId);
    //                    pakToSend[JsonKeys.MoodIds] = array;
    //                }

    //                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
    //                System.Diagnostics.Debug.WriteLine(data);
    //                Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, AppConstants.TYPE_SHARE_STATUS, pakId);
    //                int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
    //                List<string> packetIds = new List<string>(packets.Keys);
    //                SendToServer.SendBrokenPacket(packetType, packets);
    //                Thread.Sleep(25);
    //                string makeResponsepak = pakId + "_" + AppConstants.TYPE_SHARE_STATUS;
    //                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
    //                {
    //                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
    //                    {
    //                        break;
    //                    }
    //                    Thread.Sleep(DefaultSettings.WAITING_TIME);
    //                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsepak))
    //                    {
    //                        if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
    //                        {
    //                            if (i % DefaultSettings.SEND_INTERVAL == 0)
    //                            {
    //                                SendToServer.SendBrokenPacket(packetType, packets);
    //                            }
    //                        }
    //                    }
    //                    else
    //                    {
    //                        JObject feedbackfields = null;
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsepak, out feedbackfields);
    //                        bool succ = false;
    //                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
    //                        {
    //                            succ = true;
    //                            FeedDTO WhoShare = HelperMethodsModel.BindFeedDetails((JObject)feedbackfields[JsonKeys.NewsFeed]);
    //                            FeedDTO ParentFeed = WhoShare.ParentFeed;
    //                            ParentFeed.IShare = 1;
    //                            ParentFeed.WhoShare = WhoShare;

    //                            if (ParentFeed.NewsfeedId > 0 && WhoShare.NewsfeedId > 0)
    //                            {
    //                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
    //                                {
    //                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[ParentFeed.NewsfeedId] = ParentFeed;

    //                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[WhoShare.NewsfeedId] = WhoShare;
    //                                }



    //                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
    //                                {
    //                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertData(WhoShare.NewsfeedId, WhoShare.Time, WhoShare.FeedCategory);
    //                                }


    //                                lock (NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME)
    //                                {
    //                                    NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertData(WhoShare.NewsfeedId, WhoShare.ActualTime, WhoShare.FeedCategory);
    //                                }

    //                                lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
    //                                {
    //                                    NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[WhoShare.NewsfeedId] = ParentFeed.NewsfeedId;
    //                                }


    //                                if (ParentFeed.UserIdentity != DefaultSettings.LOGIN_USER_ID)
    //                                {
    //                                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(ParentFeed.UserIdentity))
    //                                    {
    //                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[ParentFeed.UserIdentity].InsertData(WhoShare.NewsfeedId, WhoShare.ActualTime, WhoShare.FeedCategory);
    //                                    }
    //                                    else
    //                                    {
    //                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Add(ParentFeed.UserIdentity, new CustomSortedList());
    //                                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[ParentFeed.UserIdentity].InsertData(WhoShare.NewsfeedId, WhoShare.ActualTime, WhoShare.FeedCategory);
    //                                    }

    //                                }

    //                                HelperMethodsAuth.NewsFeedHandlerInstance.ShareStatus(ParentFeed);
    //                            }
    //                        }

    //                        if (!succ)
    //                        {
    //                            string mg = "Failed to share feed! ";
    //                            if (feedbackfields[JsonKeys.ReasonCode] != null)
    //                            {
    //                                int rc = (int)feedbackfields[JsonKeys.ReasonCode];
    //                                if (rc != 0)
    //                                    mg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
    //                            }
    //                            HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(mg, "Failed!");
    //                        }
    //                        break;
    //                    }
    //                    PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
    //                }
    //                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(makeResponsepak);
    //                SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("ShareStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }

    //        }
    //        else
    //        {
    //            log.Error("ShareStatus Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
    //            HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(NotificationMessages.INTERNET_UNAVAILABLE, "Failed!");
    //        }
    //        HelperMethodsAuth.NewsFeedHandlerInstance.EnableSharePopup();
    //    }
    //}
}

﻿
namespace Auth.Service.Feed
{
//    public class EditFeed
//    {
//        private readonly ILog log = LogManager.GetLogger(typeof(EditFeed).Name);
//        private long NfId;
//        private string sts;
//        private long FriendIdentity;
//        private long CircleId;
//        private List<long> AddedTags, RemovedTags;
//        private LocationDTO locationDTO;
//        private string msg;
//        private bool success = false;
//        private JArray JobjStsTags;
//        private int pvc;

//        public EditFeed(int pvc, long NfId, long FriendIdentity, long CircleId, string sts, JArray JobjStsTags, List<long> AddedTags, List<long> RemovedTags, LocationDTO locationDTO)
//        {
//            this.FriendIdentity = FriendIdentity;
//            this.CircleId = CircleId;
//            this.NfId = NfId;
//            this.pvc = pvc;
//            this.sts = sts;
//            this.JobjStsTags = JobjStsTags;
//            this.AddedTags = AddedTags;
//            this.RemovedTags = RemovedTags;
//            this.locationDTO = locationDTO;
//            Thread th = new Thread(new ThreadStart(Run));
//            th.Start();
//        }

//        public EditFeed(int pvc, long NfId, string sts, JArray JobjStsTags, List<long> AddedTags, List<long> RemovedTags, LocationDTO locationDTO)
//        {
//            this.NfId = NfId;
//            this.sts = sts;
//            this.pvc = pvc;
//            this.JobjStsTags = JobjStsTags;
//            this.AddedTags = AddedTags;
//            this.RemovedTags = RemovedTags;
//            this.locationDTO = locationDTO;
//            Thread th = new Thread(new ThreadStart(Run));
//            th.Start();
//        }
//        public EditFeed(int pvc, long NfId, string sts, JArray JobjStsTags, long FriendIdentity, long CircleId, LocationDTO locationDTO)
//        {
//            this.NfId = NfId;
//            this.sts = sts;
//            this.pvc = pvc;
//            this.JobjStsTags = JobjStsTags;
//            this.FriendIdentity = FriendIdentity;
//            this.CircleId = CircleId;
//            this.locationDTO = locationDTO;
//            Thread th = new Thread(new ThreadStart(Run));
//            th.Start();
//        }
//        public void Run()
//        {
//            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
//            {
//                try
//                {
//                    JObject pakToSend = new JObject();
//                    string pakId = SendToServer.GetRanDomPacketID();
//                    pakToSend[JsonKeys.PacketId] = pakId;
//                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
//                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_STATUS;
//                    pakToSend[JsonKeys.NewsfeedId] = NfId;
//                    if (sts != null) pakToSend[JsonKeys.Status] = sts;
//                    if (pvc > 0) pakToSend[JsonKeys.FeedPrivacy] = pvc;
//                    if (JobjStsTags != null) pakToSend[JsonKeys.StatusTags] = JobjStsTags;
//                    if (FriendIdentity > 0) pakToSend[JsonKeys.FriendId] = FriendIdentity;
//                    if (CircleId > 0) pakToSend[JsonKeys.GroupId] = CircleId;
//                    if (AddedTags != null)
//                    {
//                        JArray array = new JArray();
//                        foreach (long item in AddedTags)
//                        {
//                            array.Add(item);
//                        }
//                        pakToSend[JsonKeys.TaggedFriendUtIds] = array;
//                    }
//                    if (RemovedTags != null)
//                    {
//                        JArray array = new JArray();
//                        foreach (long item in RemovedTags)
//                        {
//                            array.Add(item);
//                        }
//                        pakToSend[JsonKeys.RemovedTagsFriendUtIds] = array;
//                    }
//                    if (locationDTO != null)
//                    {
//                        pakToSend[JsonKeys.Location] = locationDTO.loc;
//                        pakToSend[JsonKeys.Latitude] = locationDTO.lat;
//                        pakToSend[JsonKeys.Longitude] = locationDTO.lon;
//                    }

//                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
//                    Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, AppConstants.TYPE_EDIT_STATUS, pakId);
//                    int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
//                    List<string> packetIds = new List<string>(packets.Keys);
//                    SendToServer.SendBrokenPacket(packetType, packets);
//                    Thread.Sleep(25);
//                    string makeResponsepak = pakId + "_" + AppConstants.TYPE_EDIT_STATUS;
//                    for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
//                    {
//                        if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
//                        {
//                            break;
//                        }
//                        Thread.Sleep(DefaultSettings.WAITING_TIME);
//                        if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsepak))
//                        {
//                            if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
//                            {
//                                if (i % DefaultSettings.SEND_INTERVAL == 0)
//                                    SendToServer.SendBrokenPacket(packetType, packets);
//                            }
//                        }
//                        else
//                        {
//                            JObject feedbackfields = null;
//                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsepak, out feedbackfields);
//                            if (feedbackfields[JsonKeys.Success] != null)
//                            {
//                                success = (bool)feedbackfields[JsonKeys.Success];
//                                List<StatusTagDTO> tagLst = (JobjStsTags != null) ? HelperMethodsModel.GetStatusTagListFromJArray(JobjStsTags) : null;
//                                HelperMethodsAuth.NewsFeedHandlerInstance.EditNewsFeed(success, NfId, sts, tagLst, FriendIdentity, CircleId, AddedTags, RemovedTags, locationDTO, pvc);
//                                //FeedDTO feed = (success) ? HelperMethodsModel.BindFeedDetails(feedbackfields) : null;
//                                //HelperMethodsAuth.NewsFeedHandlerInstance.EditFeed(feed);
//                            }
//                            if (feedbackfields[JsonKeys.Message] != null)
//                            {
//                                msg = (string)feedbackfields[JsonKeys.Message];
//                            }
//                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
//                            break;
//                        }
//                        PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
//                    }
//                    SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
//                }
//                catch (Exception e)
//                {

//                    log.Error("EditFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//                }
//            }
//            else
//            {
//#if AUTH_LOG
//                log.Error("EditFeed Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
//#endif
//                msg = NotificationMessages.INTERNET_UNAVAILABLE;
//            }
//            if (!success)
//            {
//                if (string.IsNullOrEmpty(msg)) msg = "Failed to Edit the Feed!";
//                HelperMethodsAuth.NewsFeedHandlerInstance.OnEditFeedFailed(msg);
//            }
//        }
//    }
}

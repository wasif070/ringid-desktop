﻿
namespace Auth.Service.Feed
{
//    public class AddFeed
//    {
//        private readonly ILog log = LogManager.GetLogger(typeof(AddFeed).Name);
//        private int type; //1 for feed,2 on friends wall,3 on circle
//        private int validity, w, h, mediatype;
//        private string sts, imgUrl, cptn;
//        private long frndid_or_circleId;
//        private string lnkTtl, lnkURL, lnkDsc, lnlImgURL, lnkDmn;
//        private int lnkTyp;
//        private LocationDTO locationDTO;
//        private List<long> taggedUtids;
//        private long DoingId;
//        private bool NoResponse = true;
//        //private bool succ = false;
//        private JObject JobjMediaContent;
//        private JArray JobjStsTags, SharingPostContentIds;
//        private int pvc;

//        public AddFeed(int pvc, string sts, JArray JobjStsTags, int validity, int type, long frndid_or_circleId, string lnkTtl, string lnkURL, string lnkDsc, string lnlImgURL, string lnkDmn, int lnkTyp, LocationDTO locationDTO, List<long> taggedUtids, long DoingId)
//        {
//            this.pvc = pvc;
//            this.type = type;
//            this.sts = sts;
//            this.JobjStsTags = JobjStsTags;
//            this.validity = validity;
//            this.frndid_or_circleId = frndid_or_circleId;
//            this.lnkDmn = lnkDmn;
//            this.lnkDsc = lnkDsc;
//            this.lnkTtl = lnkTtl;
//            this.lnkURL = lnkURL;
//            this.lnlImgURL = lnlImgURL;
//            this.lnkTyp = lnkTyp;
//            this.locationDTO = locationDTO;
//            this.taggedUtids = taggedUtids;
//            this.DoingId = DoingId;
//            Thread th = new Thread(new ThreadStart(Run));
//            th.Start();
//        }

//        public AddFeed(int pvc, string imgUrl, JObject JobjMediaContent, int mediatype, string cptn, int w, int h, string sts, JArray JobjStsTags, int validity, int type, long frndid_or_circleId, LocationDTO locationDTO, List<long> taggedUtids, long DoingId, JArray SharingPostContentIds = null)
//        {
//            this.pvc = pvc;
//            this.imgUrl = imgUrl;
//            this.JobjMediaContent = JobjMediaContent;
//            this.mediatype = mediatype;
//            this.cptn = cptn;
//            this.w = w;
//            this.h = h;
//            this.type = type;
//            this.sts = sts;
//            this.JobjStsTags = JobjStsTags;
//            this.validity = validity;
//            this.frndid_or_circleId = frndid_or_circleId;
//            this.locationDTO = locationDTO;
//            this.taggedUtids = taggedUtids;
//            this.DoingId = DoingId;
//            this.SharingPostContentIds = SharingPostContentIds;
//            Thread th = new Thread(new ThreadStart(Run));
//            th.Start();
//        }
//        public void Run()
//        {
//            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
//            {
//                try
//                {
//                    JObject pakToSend = new JObject();
//                    string pakId = SendToServer.GetRanDomPacketID();
//                    pakToSend[JsonKeys.PacketId] = pakId;
//                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
//                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_STATUS;
//                    pakToSend[JsonKeys.Status] = sts;
//                    //if (pvc > 0) pakToSend[JsonKeys.FeedPrivacy] = pvc;

//                    if (type == 2) { pakToSend[JsonKeys.FriendId] = frndid_or_circleId; pakToSend[JsonKeys.FeedPrivacy] = 2; }
//                    else if (type == 3) { pakToSend[JsonKeys.GroupId] = frndid_or_circleId; pakToSend[JsonKeys.FeedPrivacy] = 2; }
//                    else if (pvc > 0) pakToSend[JsonKeys.FeedPrivacy] = pvc;

//                    if (JobjStsTags != null) pakToSend[JsonKeys.StatusTags] = JobjStsTags;
//                    pakToSend[JsonKeys.Validity] = validity;
//                    if (JobjMediaContent != null)
//                    {
//                        pakToSend[JsonKeys.Type] = mediatype;
//                        pakToSend[JsonKeys.MediaContentDTO] = JobjMediaContent;
//                    }
//                    else if (SharingPostContentIds != null)
//                    {
//                        pakToSend[JsonKeys.Type] = mediatype;
//                        pakToSend[JsonKeys.SharingPostContentIds] = SharingPostContentIds;
//                    }
//                    else if (string.IsNullOrEmpty(imgUrl))
//                    {
//                        pakToSend[JsonKeys.Type] = SettingsConstants.NEWS_FEED_TYPE_TEXT_STATUS;
//                        if (!string.IsNullOrEmpty(lnkTtl)) pakToSend[JsonKeys.LinkTitle] = lnkTtl;
//                        if (!string.IsNullOrEmpty(lnkDsc)) pakToSend[JsonKeys.LinkDescription] = lnkDsc;
//                        if (!string.IsNullOrEmpty(lnkDmn)) pakToSend[JsonKeys.LinkDomain] = lnkDmn;
//                        if (!string.IsNullOrEmpty(lnlImgURL) && lnlImgURL.Length < 350) pakToSend[JsonKeys.LinkImageURL] = lnlImgURL;
//                        if (!string.IsNullOrEmpty(lnkURL)) pakToSend[JsonKeys.LinkURL] = lnkURL;
//                        if (lnkTyp > 0) pakToSend[JsonKeys.LinkType] = lnkTyp;
//                    }
//                    else
//                    {
//                        pakToSend[JsonKeys.Type] = SettingsConstants.NEWS_FEED_TYPE_SINGLE_IMAGE;
//                        pakToSend[JsonKeys.ImageUrl] = imgUrl;
//                        pakToSend[JsonKeys.ImageCaption] = cptn;
//                        pakToSend[JsonKeys.ImageHeight] = h;
//                        pakToSend[JsonKeys.ImageWidth] = w;
//                    }
//                    if (locationDTO != null)
//                    {
//                        pakToSend[JsonKeys.Location] = locationDTO.loc;
//                        pakToSend[JsonKeys.Latitude] = locationDTO.lat;
//                        pakToSend[JsonKeys.Longitude] = locationDTO.lon;
//                    }
//                    if (taggedUtids != null)
//                    {
//                        JArray array = new JArray();
//                        foreach (long item in taggedUtids)
//                        {
//                            array.Add(item);
//                        }
//                        pakToSend[JsonKeys.TaggedFriendUtIds] = array;
//                    }
//                    if (this.DoingId > 0)
//                    {
//                        JArray array = new JArray();
//                        array.Add(DoingId);
//                        pakToSend[JsonKeys.MoodIds] = array;
//                    }
//                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
//                    Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, AppConstants.TYPE_ADD_STATUS, pakId);
//                    int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
//                    List<string> packetIds = new List<string>(packets.Keys);
//                    SendToServer.SendBrokenPacket(packetType, packets);
//                    Thread.Sleep(25);
//                    string makeResponsepak = pakId + "_" + AppConstants.TYPE_ADD_STATUS;
//                    for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
//                    {
//                        //log.Info("********SendToServer.CheckAllBrokenPacketConfirmation(packets)..>" + sts);
//                        if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
//                        {
//                            break;
//                        }
//                        Thread.Sleep(DefaultSettings.WAITING_TIME);
//                        if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsepak))
//                        {
//                            //log.Info("SendToServer.CheckAllBrokenPacketConfirmation(packets)..>" + sts);
//                            if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
//                            {
//                                //log.Info(" SendToServer.SendBrokenPacket..>" + sts);
//                                SendToServer.SendBrokenPacket(packetType, packets);
//                            }
//                        }
//                        else
//                        {
//                            JObject feedbackfields;
//                            //log.Info("Before RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove..>" + sts);
//                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(makeResponsepak, out feedbackfields);
//                            //log.Info("Before NoResponse = false..>" + sts);
//                            NoResponse = false;
//                            //log.Info("Before break..>" + sts);
//                            break;
//                        }
//                        //   log.Info("Before PingInServer.StartThread..>" + sts);
//                        PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
//                    }
//                    // log.Info("Before SendToServer.RemoveAllBrokenPacketConfirmation..>" + sts);
//                    SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
//                }
//                catch (Exception e)
//                {

//                    log.Error("AddMultipleImages ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

//                }
//            }
//            else
//            {
//#if AUTH_LOG
//                log.Info("AddMultipleImages Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
//#endif
//                HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox(NotificationMessages.INTERNET_UNAVAILABLE, "Failed!");
//            }
//            //log.Info("Before Post Btn Enabled!..>" + sts);
//            HelperMethodsAuth.NewsFeedHandlerInstance.EnablePostBtn(type, frndid_or_circleId, !NoResponse);
//        }
//    }
}

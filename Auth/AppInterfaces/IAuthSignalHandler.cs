<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.Parser;
using Newtonsoft.Json.Linq;

namespace Auth.AppInterfaces
{
    public interface IAuthSignalHandler
    {
        void Process_INVALID_LOGIN_SESSION_19(JObject jObject);

        void Process_CONTACT_LIST_23(CommonPacketAttributes commonPacketAttributes);

        void Process_CONTACT_UTIDS_29(CommonPacketAttributes commonPacketAttributes);

        void Process_SUGGESTION_IDS_31(JObject jObject);

        void Process_USERS_DETAILS_32(JObject jObject);

        void Process_CONTACT_SEARCH_34(JObject jObject, string client_packet_id);

        void Process_NEW_CIRCLE_51(JObject jObject);

        void Process_CIRCLE_DETAILS_52(JObject jObject);

        void Process_LEAVE_CIRCLE_53(JObject jObject);

        void Process_CIRCLE_LIST_70(JObject jObject);

        void Process_MULTIPLE_SESSION_WARNING_75(CommonPacketAttributes commonPacketAttributes);

        void Process_PRESENCE_78(JObject jObject);

        void Process_UNWANTED_LOGIN_79(CommonPacketAttributes commonPacketAttributes);

        void Process_COMMENTS_FOR_STATUS_84(JObject jObject);

        void Process_MEDIA_FEED_87(JObject jObject, string client_packet_id);

        void Process_NEWS_FEED_88(JObject jObject, string client_packet_id);

        void Process_COMMENTS_FOR_IMAGE_89(JObject jObject);

        void Process_ACTION_MERGED_LIKES_LIST_OF_COMMENT_1116(JObject jObject);

        void Process_ACTION_MERGED_COMMENTS_LIST_1084(JObject jObject);

        void Process_LIKES_FOR_STATUS_92(JObject jObject);

        void Process_LIKES_FOR_IMAGE_93(JObject jObject);

        void Process_MY_BOOK_94(JObject jObject, string client_packet_id);

        void Process_IMAGE_ALBUM_LIST_96(JObject jObject);

        void Process_MY_ALBUM_IMAGES_97(JObject jObject);

        void Process_CIRCLE_MEMBERS_LIST_99(JObject jObject);

        void Process_SEARCH_CIRCLE_MEMBER_101(JObject jObject);

        void Process_FRIEND_CONTACT_LIST_107(JObject jObject);

        void Process_FRIEND_ALBUM_IMAGES_109(JObject jObject);

        void Process_FRIEND_NEWSFEED_110(JObject jObject, string clientpacketid);

        void Process_MY_NOTIFICATIONS_111(JObject jObject);

        void Process_SINGLE_NOTIFICATION_113(JObject jObject);

        void Process_SINGLE_FEED_SHARE_LIST_115(JObject jObject);

        void Process_LIST_LIKES_OF_COMMENT_116(JObject jObject);

        void Process_MULTIPLE_IMAGE_POST_117(JObject jObject);

        void Process_MUTUAL_FRIENDS_118(JObject jObject);

        void Process_ADD_FRIEND_127(JObject jObject, long server_packet_id);

        void Process_DELETE_FRIEND_128(JObject jObject, int action, long server_packet_id);

        void Process_ACCEPT_FRIEND_129(JObject jObject, long server_packet_id);

        void Process_START_GROUP_CHAT_134(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_MORE_FEED_IMAGES_139(JObject jObject);

        void Process_DELETE_CIRCLE_152(JObject jObject);

        void Process_REMOVE_CIRCLE_MEMBER_154(JObject jObject);

        void Process_START_FRIEND_CHAT_175(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_ADD_STATUS_177(JObject jObject);

        //void Process_ADD_IMAGE_COMMENT_180(JObject jObject);

        //void Process_LIKE_STATUS_184(JObject jObject);

        //void Process_UNLIKE_STATUS_186(JObject jObject);

        //void Process_EDIT_IMAGE_COMMENT_194(JObject jObject);

        void Process_IMAGE_COMMENT_LIKES_196(JObject jObject);

        void Process_LIKE_UNLIKE_IMAGE_COMMENT_197(JObject jObject);

        void Process_CIRCLE_NEWSFEED_198(JObject jObject, string client_packet_id);

        void Process_SINGLE_FRIEND_PRESENCE_INFO_199(JObject jObject);


        void Process_UPDATE_DIGITS_VERIFY_209(JObject jObject);

        void Process_FRIEND_CONTACT_LIST_211(JObject jObject);

        void Process_MISS_CALL_LIST_224(JObject jObject);

        void Process_ADD_WORK_227(JObject jObject);

        void Process_ADD_EDUCATION_231(JObject jObject);

        void Process_LIST_WORK_234(JObject jObject);

        void Process_LIST_EDUCATION_235(JObject jObject);

        void Process_LIST_SKILL_236(JObject jObject);

        void Process_ADD_SKILL_237(JObject jObject);

        void Process_WHO_SHARES_LIST_249(JObject jObject);

        void Process_MEDIA_SHARE_LIST_250(JObject jObject);

        void Process_MEDIA_ALBUM_LIST_256(JObject jObject);

        void Process_MEDIA_ALBUM_CONTENT_LIST_261(JObject jObject);

        void Process_MEDIA_LIKE_LIST_269(JObject jObject);

        void Process_MEDIA_COMMENT_LIST_270(JObject jObject);

        void Process_MEDIACOMMENT_LIKE_LIST_271(JObject jObject);

        void Process_VIEW_DOING_LIST_273(JObject jObject);

        void Process_VIEW_TAGGED_LIST_274(JObject jObject);

        void Process_MEDIA_SUGGESTION_277(JObject jObject);

        void Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(JObject jObject);

        void Process_HASHTAG_MEDIA_CONTENTS_279(JObject jObject);

        void Process_HASHTAG_SUGGESTION_280(JObject jObject);

        void Process_SEARCH_TRENDS_281(JObject jObject);

        void Process_STORE_CONTACT_LIST_284(JObject jObject);

        void Process_CELEBRITIES_CATEGORIES_LIST_285(JObject jObject);

        void Process_CELEBRITY_DISCOVER_LIST_286(JObject jObject);

        void Process_NEWSPORTAL_CATEGORIES_LIST_294(JObject jObject);

        void Process_TYPE_CELEBRITY_FEED_288(JObject jObject, string client_packet_id);

        void Process_NEWSPORTAL_FEED_295(JObject jObject, string client_packet_id);

        void Process_NEWSPORTAL_LIST_299(JObject jObject);

        void Process_NEWSPORTAL_BREAKING_FEED_302(JObject jObject);

        void Process_BUSINESSPAGE_BREAKING_FEED_303(JObject jObject);

        void Process_BUSINESS_PAGE_FEED_306(JObject jObject, string client_packet_id);

        //void Process_MEDIA_PAGE_FEED_307(JObject jObject, string client_packet_id);

        void Process_MEDIA_PAGE_TRENDING_FEED_308(JObject jObject);

        void Process_ALL_SAVED_FEEDS_309(JObject jObject, string client_packet_id);

        void Process_ACTION_UPDATE_LIKE_UNLIKE_COMMENT_1323(JObject jObject);

        void Process_UPDATE_LIKE_COMMENT_323(JObject jObject);

        void Process_UPDATE_UNLIKE_COMMENT_325(JObject jObject);

        void Process_UPDATE_ADD_FRIEND_327(JObject jObject);

        void Process_UPDATE_DELETE_FRIEND_328(JObject jObject, int action, long server_packet_id);

        void Process_UPDATE_ACCEPT_FRIEND_329(JObject jObject);

        void Process_UPDATE_START_GROUP_CHAT_334(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_UPDATE_ADD_GROUP_MEMBER_335(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO_336(JObject jObject);

        //void Process_UPDATE_DELETE_CIRCLET_352(JObject jObject);

        //void Process_UPDATE_REMOVE_CIRCLE_MEMBER_354(JObject jObject);

        //void Process_UPDATE_ADD_CIRCLE_MEMBER_356(JObject jObject);

        //void Process_UPDATE_EDIT_CIRCLE_MEMBER_358(JObject jObject);

        void Process_UPDATE_SEND_REGISTER_374(JObject jObject);

        void Process_UPDATE_START_FRIEND_CHAT_375(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_UPDATE_ADD_STATUS_377(JObject jObject);

        void Process_UPDATE_EDIT_STATUS_378(JObject jObject);

        void Process_UPDATE_DELETE_STATUS_379(JObject jObject);

        void Process_UPDATE_ADD_IMAGE_COMMENT_380(JObject jObject);

        void Process_UPDATE_ADD_STATUS_COMMENT_381(JObject jObject);

        void Process_ACTION_MERGED_UPDATE_ADD_COMMENT_1381(JObject jObject);

        void Process_UPDATE_DELETE_IMAGE_COMMENT_382(JObject jObject);

        void Process_ACTION_MERGED_UPDATE_DELETE_COMMENT_1383(JObject jObject);

        void Process_UPDATE_DELETE_STATUS_COMMENT_383(JObject jObject);

        void Process_UPDATE_LIKE_STATUS_384(JObject jObject);

        void Process_MERGED_UPDATE_LIKE_UNLIKE_1384(JObject jObject);

        void Process_UPDATE_LIKE_UNLIKE_IMAGE_385(JObject jObject);

        void Process_UPDATE_UNLIKE_STATUS_386(JObject jObject);

        void Process_UPDATE_EDIT_STATUS_COMMENT_389(JObject jObject);

        void Process_UPDATE_SHARE_STATUS_391(JObject jObject);

        void Process_UPDATE_EDIT_IMAGE_COMMENT_394(JObject jObject);

        void Process_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT_397(JObject jObject);

        void Process_UPDATE_LIKEUNLIKE_MEDIA_464(JObject jObject);

        void Process_UPDATE_COMMENT_MEDIA_465(JObject jObject);

        void Process_UPDATE_EDITCOMMENT_MEDIA_466(JObject jObject);

        void Process_UPDATE_DELETECOMMENT_MEDIA_467(JObject jObject);

        //void Process_UPDATE_LIKEUNLIKECOMMENT_MEDIA_468(JObject jObject);

        void Process_UPDATE_STORE_CONTACT_LIST_484(JObject jObject);


        void Process_SPAM_REASON_LIST_1001(JObject jObject);

        void Process_TYPE_GET_WALLET_INFORMATION_1026(JObject jObject);

        void Process_TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION_1027(JObject jObject);

        void Process_TYPE_GET_TRANSACTION_HISTORY_1031(JObject jObject);

        void Process_NEWSFEED_EDIT_HISTORY_LIST_1016(JObject _JobjFromResponse);

        void Process_COMMENT_EDIT_HISTORY_LIST_1021(JObject _JobjFromResponse);

        void PROCESS_TYPE_UPDATE_REFERRAL_REQUEST_1037(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_WALLET_GIFT_1039(JObject __jObjFromResponse);

        void PROCESS_TYPE_GET_WALLET_COIN_BUNDLE_LIST_1038(JObject _jObjFromResponse);

        void PROCESS_TYPE_GET_COIN_EARNING_RULE_LIST_1045(JObject _jObjFromResponse);

        void PROCESS_TYPE_PAYMENT_RECEIVED_1046(JObject _jObjFromResponse);

        void PROCESS_TYPE_GIFT_RECEIVED_1052(JObject _jObjFromResponse);

        void PROCESS_UPDATE_FOLLOW_UNFOLLOW_USER(JObject _jObjFromResponse);

        //void PROCESS_TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST_1039(JObject _JobjFromResponse);

        void PROCESS_TYPE_LIVE_STREAM_REQUEST_2001(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_FEATURED_LIVE_STREAMS_REQUEST_2004(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_STREAM_CATEGORY_LIST_REQUEST_2007(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_MOST_VIEWED_STREAMS_2008(JObject _JobjFromResponse);

        void PROCESS_TYPE_SEARCH_LIVE_STREAMS_REQUEST_2010(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_RECENT_STREAMS_REQUEST_2005(JObject _JobjFromResponse);

        void PROCESS_UPDATE_LIVE_STREAM_2006(JObject _JobjFromResponse);

        void PROCESS_UPDATE_STREAMING_LIKE_COUNT_2012(JObject _JobjFromResponse);

        void PROCESS_GET_FOLLOWING_STREAMS_REQUEST_2013(JObject _JobjFromResponse);

        void PROCESS_GET_CATEGORY_WISE_STREAM_COUNT_REQUEST_2014(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_NEAREST_STREAMS_2009(JObject _JobjFromResponse);

        void PROCESS_TYPE_REFERRAL_NETWORK_SUMMARY_1041(JObject _jObjFromResponse);

        void PROCESS_TYPE_MY_REFERRAL_LIST_1042(JObject _jObjFromResponse);

        void PROCESS_TYPE_GET_DAILY_CHECKIN_HISTORY_1054(JObject _jObjFromResponse);

        void PROCESS_CREATE_CHANNEL_REQUEST_2015(JObject _JobjFromResponse);

        void PROCESS_GET_CHANNEL_CATEGORY_LIST_REQUEST_2016(JObject _JobjFromResponse);

        void PROCESS_UPDATE_CHANNEL_INFO_REQUEST_2017(JObject _JobjFromResponse);

        void PROCESS_GET_FOLLOWING_CHANNEL_LIST_REQUEST_2019(JObject _JobjFromResponse);

        void PROCESS_GET_MOST_VIEWED_CHANNEL_LIST_REQUEST_2020(JObject _JobjFromResponse);

        void PROCESS_GET_OWN_CHANNEL_LIST_REQUEST_2021(JObject _JobjFromResponse);

        void PROCESS_ADD_CHANNEL_PROFILE_IMAGE_REQUEST_2024(JObject _JobjFromResponse);

        void PROCESS_ADD_CHANNEL_COVER_IMAGE_REQUEST_2025(JObject _JobjFromResponse);

        void PROCESS_ADD_MEDIA_TO_CHANNEL_REQUEST_2026(JObject _JobjFromResponse);

        void PROCESS_GET_CHANNEL_MEDIA_LIST_REQUEST_2028(JObject _JobjFromResponse);

        void PROCESS_UPDATE_CHANNEL_MEDIA_STATUS_2029(JObject _JobjFromResponse);

        void PROCESS_GET_FEATURED_CHANNEL_LIST_REQUEST_2030(JObject _JobjFromResponse);

        void PROCESS_UPDATE_CHANNEL_MEDIA_INFO_REQUEST_2031(JObject _JobjFromResponse);

        void PROCESS_SEARCH_CHANNEL_LIST_REQUEST_2032(JObject _JobjFromResponse);

        void PROCESS_GET_CHANNEL_PLAYLIST_REQUEST_2033(JObject _JobjFromResponse);
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.Parser;
using Newtonsoft.Json.Linq;

namespace Auth.AppInterfaces
{
    public interface IAuthSignalHandler
    {
        void Process_INVALID_LOGIN_SESSION_19(JObject jObject);

        void Process_CONTACT_LIST_23(CommonPacketAttributes commonPacketAttributes);

        void Process_CONTACT_UTIDS_29(CommonPacketAttributes commonPacketAttributes);

        void Process_SUGGESTION_IDS_31(JObject jObject);

        void Process_USERS_DETAILS_32(JObject jObject);

        void Process_CONTACT_SEARCH_34(JObject jObject, string client_packet_id);

        void Process_NEW_CIRCLE_51(JObject jObject);

        void Process_CIRCLE_DETAILS_52(JObject jObject);

        void Process_LEAVE_CIRCLE_53(JObject jObject);

        void Process_CIRCLE_LIST_70(JObject jObject);

        void Process_MULTIPLE_SESSION_WARNING_75(CommonPacketAttributes commonPacketAttributes);

        void Process_PRESENCE_78(JObject jObject);

        void Process_UNWANTED_LOGIN_79(CommonPacketAttributes commonPacketAttributes);

        void Process_COMMENTS_FOR_STATUS_84(JObject jObject);

        void Process_MEDIA_FEED_87(JObject jObject, string client_packet_id);

        void Process_NEWS_FEED_88(JObject jObject, string client_packet_id);

        void Process_COMMENTS_FOR_IMAGE_89(JObject jObject);

        void Process_ACTION_MERGED_LIKES_LIST_OF_COMMENT_1116(JObject jObject);

        void Process_ACTION_MERGED_COMMENTS_LIST_1084(JObject jObject);

        void Process_LIKES_FOR_STATUS_92(JObject jObject);

        void Process_LIKES_FOR_IMAGE_93(JObject jObject);

        void Process_MY_BOOK_94(JObject jObject, string client_packet_id);

        void Process_IMAGE_ALBUM_LIST_96(JObject jObject);

        void Process_MY_ALBUM_IMAGES_97(JObject jObject);

        void Process_CIRCLE_MEMBERS_LIST_99(JObject jObject);

        void Process_SEARCH_CIRCLE_MEMBER_101(JObject jObject);

        void Process_FRIEND_CONTACT_LIST_107(JObject jObject);

        void Process_FRIEND_ALBUM_IMAGES_109(JObject jObject);

        void Process_FRIEND_NEWSFEED_110(JObject jObject, string clientpacketid);

        void Process_MY_NOTIFICATIONS_111(JObject jObject);

        void Process_SINGLE_NOTIFICATION_113(JObject jObject);

        void Process_SINGLE_FEED_SHARE_LIST_115(JObject jObject);

        void Process_LIST_LIKES_OF_COMMENT_116(JObject jObject);

        void Process_MULTIPLE_IMAGE_POST_117(JObject jObject);

        void Process_MUTUAL_FRIENDS_118(JObject jObject);

        void Process_ADD_FRIEND_127(JObject jObject, long server_packet_id);

        void Process_DELETE_FRIEND_128(JObject jObject, int action, long server_packet_id);

        void Process_ACCEPT_FRIEND_129(JObject jObject, long server_packet_id);

        void Process_START_GROUP_CHAT_134(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_MORE_FEED_IMAGES_139(JObject jObject);

        void Process_DELETE_CIRCLE_152(JObject jObject);

        void Process_REMOVE_CIRCLE_MEMBER_154(JObject jObject);

        void Process_START_FRIEND_CHAT_175(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_ADD_STATUS_177(JObject jObject);

        //void Process_ADD_IMAGE_COMMENT_180(JObject jObject);

        //void Process_LIKE_STATUS_184(JObject jObject);

        //void Process_UNLIKE_STATUS_186(JObject jObject);

        //void Process_EDIT_IMAGE_COMMENT_194(JObject jObject);

        void Process_IMAGE_COMMENT_LIKES_196(JObject jObject);

        void Process_LIKE_UNLIKE_IMAGE_COMMENT_197(JObject jObject);

        void Process_CIRCLE_NEWSFEED_198(JObject jObject, string client_packet_id);

        void Process_SINGLE_FRIEND_PRESENCE_INFO_199(JObject jObject);


        void Process_UPDATE_DIGITS_VERIFY_209(JObject jObject);

        void Process_FRIEND_CONTACT_LIST_211(JObject jObject);

        void Process_MISS_CALL_LIST_224(JObject jObject);

        void Process_ADD_WORK_227(JObject jObject);

        void Process_ADD_EDUCATION_231(JObject jObject);

        void Process_LIST_WORK_234(JObject jObject);

        void Process_LIST_EDUCATION_235(JObject jObject);

        void Process_LIST_SKILL_236(JObject jObject);

        void Process_ADD_SKILL_237(JObject jObject);

        void Process_WHO_SHARES_LIST_249(JObject jObject);

        void Process_MEDIA_SHARE_LIST_250(JObject jObject);

        void Process_MEDIA_ALBUM_LIST_256(JObject jObject);

        void Process_MEDIA_ALBUM_CONTENT_LIST_261(JObject jObject);

        void Process_MEDIA_LIKE_LIST_269(JObject jObject);

        void Process_MEDIA_COMMENT_LIST_270(JObject jObject);

        void Process_MEDIACOMMENT_LIKE_LIST_271(JObject jObject);

        void Process_VIEW_DOING_LIST_273(JObject jObject);

        void Process_VIEW_TAGGED_LIST_274(JObject jObject);

        void Process_MEDIA_SUGGESTION_277(JObject jObject);

        void Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(JObject jObject);

        void Process_HASHTAG_MEDIA_CONTENTS_279(JObject jObject);

        void Process_HASHTAG_SUGGESTION_280(JObject jObject);

        void Process_SEARCH_TRENDS_281(JObject jObject);

        void Process_STORE_CONTACT_LIST_284(JObject jObject);

        void Process_CELEBRITIES_CATEGORIES_LIST_285(JObject jObject);

        void Process_CELEBRITY_DISCOVER_LIST_286(JObject jObject);

        void Process_NEWSPORTAL_CATEGORIES_LIST_294(JObject jObject);

        void Process_TYPE_CELEBRITY_FEED_288(JObject jObject, string client_packet_id);

        void Process_NEWSPORTAL_FEED_295(JObject jObject, string client_packet_id);

        void Process_NEWSPORTAL_LIST_299(JObject jObject);

        void Process_NEWSPORTAL_BREAKING_FEED_302(JObject jObject);

        void Process_BUSINESSPAGE_BREAKING_FEED_303(JObject jObject);

        void Process_BUSINESS_PAGE_FEED_306(JObject jObject, string client_packet_id);

        //void Process_MEDIA_PAGE_FEED_307(JObject jObject, string client_packet_id);

        void Process_MEDIA_PAGE_TRENDING_FEED_308(JObject jObject);

        void Process_ALL_SAVED_FEEDS_309(JObject jObject, string client_packet_id);

        void Process_ACTION_UPDATE_LIKE_UNLIKE_COMMENT_1323(JObject jObject);

        void Process_UPDATE_LIKE_COMMENT_323(JObject jObject);

        void Process_UPDATE_UNLIKE_COMMENT_325(JObject jObject);

        void Process_UPDATE_ADD_FRIEND_327(JObject jObject);

        void Process_UPDATE_DELETE_FRIEND_328(JObject jObject, int action, long server_packet_id);

        void Process_UPDATE_ACCEPT_FRIEND_329(JObject jObject);

        void Process_UPDATE_START_GROUP_CHAT_334(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_UPDATE_ADD_GROUP_MEMBER_335(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO_336(JObject jObject);

        //void Process_UPDATE_DELETE_CIRCLET_352(JObject jObject);

        //void Process_UPDATE_REMOVE_CIRCLE_MEMBER_354(JObject jObject);

        //void Process_UPDATE_ADD_CIRCLE_MEMBER_356(JObject jObject);

        //void Process_UPDATE_EDIT_CIRCLE_MEMBER_358(JObject jObject);

        void Process_UPDATE_SEND_REGISTER_374(JObject jObject);

        void Process_UPDATE_START_FRIEND_CHAT_375(JObject jObject, int action, long server_packet_id, string client_packet_id);

        void Process_UPDATE_ADD_STATUS_377(JObject jObject);

        void Process_UPDATE_EDIT_STATUS_378(JObject jObject);

        void Process_UPDATE_DELETE_STATUS_379(JObject jObject);

        void Process_UPDATE_ADD_IMAGE_COMMENT_380(JObject jObject);

        void Process_UPDATE_ADD_STATUS_COMMENT_381(JObject jObject);

        void Process_ACTION_MERGED_UPDATE_ADD_COMMENT_1381(JObject jObject);

        void Process_UPDATE_DELETE_IMAGE_COMMENT_382(JObject jObject);

        void Process_ACTION_MERGED_UPDATE_DELETE_COMMENT_1383(JObject jObject);

        void Process_UPDATE_DELETE_STATUS_COMMENT_383(JObject jObject);

        void Process_UPDATE_LIKE_STATUS_384(JObject jObject);

        void Process_MERGED_UPDATE_LIKE_UNLIKE_1384(JObject jObject);

        void Process_UPDATE_LIKE_UNLIKE_IMAGE_385(JObject jObject);

        void Process_UPDATE_UNLIKE_STATUS_386(JObject jObject);

        void Process_UPDATE_EDIT_STATUS_COMMENT_389(JObject jObject);

        void Process_UPDATE_SHARE_STATUS_391(JObject jObject);

        void Process_UPDATE_EDIT_IMAGE_COMMENT_394(JObject jObject);

        void Process_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT_397(JObject jObject);

        void Process_UPDATE_LIKEUNLIKE_MEDIA_464(JObject jObject);

        void Process_UPDATE_COMMENT_MEDIA_465(JObject jObject);

        void Process_UPDATE_EDITCOMMENT_MEDIA_466(JObject jObject);

        void Process_UPDATE_DELETECOMMENT_MEDIA_467(JObject jObject);

        //void Process_UPDATE_LIKEUNLIKECOMMENT_MEDIA_468(JObject jObject);

        void Process_UPDATE_STORE_CONTACT_LIST_484(JObject jObject);


        void Process_SPAM_REASON_LIST_1001(JObject jObject);

        void Process_TYPE_GET_WALLET_INFORMATION_1026(JObject jObject);

        void Process_TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION_1027(JObject jObject);

        void Process_TYPE_GET_TRANSACTION_HISTORY_1031(JObject jObject);

        void Process_NEWSFEED_EDIT_HISTORY_LIST_1016(JObject _JobjFromResponse);

        void Process_COMMENT_EDIT_HISTORY_LIST_1021(JObject _JobjFromResponse);

        void PROCESS_TYPE_UPDATE_REFERRAL_REQUEST_1037(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_WALLET_GIFT_1039(JObject __jObjFromResponse);

        void PROCESS_TYPE_GET_WALLET_COIN_BUNDLE_LIST_1038(JObject _jObjFromResponse);

        void PROCESS_TYPE_GET_COIN_EARNING_RULE_LIST_1045(JObject _jObjFromResponse);

        void PROCESS_TYPE_PAYMENT_RECEIVED_1046(JObject _jObjFromResponse);

        void PROCESS_TYPE_GIFT_RECEIVED_1052(JObject _jObjFromResponse);

        void PROCESS_UPDATE_FOLLOW_UNFOLLOW_USER(JObject _jObjFromResponse);

        //void PROCESS_TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST_1039(JObject _JobjFromResponse);

        void PROCESS_TYPE_LIVE_STREAM_REQUEST_2001(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_FEATURED_LIVE_STREAMS_REQUEST_2004(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_STREAM_CATEGORY_LIST_REQUEST_2007(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_MOST_VIEWED_STREAMS_2008(JObject _JobjFromResponse);

        void PROCESS_TYPE_SEARCH_LIVE_STREAMS_REQUEST_2010(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_RECENT_STREAMS_REQUEST_2005(JObject _JobjFromResponse);

        void PROCESS_UPDATE_LIVE_STREAM_2006(JObject _JobjFromResponse);

        void PROCESS_UPDATE_STREAMING_LIKE_COUNT_2012(JObject _JobjFromResponse);

        void PROCESS_GET_FOLLOWING_STREAMS_REQUEST_2013(JObject _JobjFromResponse);

        void PROCESS_GET_CATEGORY_WISE_STREAM_COUNT_REQUEST_2014(JObject _JobjFromResponse);

        void PROCESS_TYPE_GET_NEAREST_STREAMS_2009(JObject _JobjFromResponse);

        void PROCESS_TYPE_REFERRAL_NETWORK_SUMMARY_1041(JObject _jObjFromResponse);

        void PROCESS_TYPE_MY_REFERRAL_LIST_1042(JObject _jObjFromResponse);

        void PROCESS_TYPE_GET_DAILY_CHECKIN_HISTORY_1054(JObject _jObjFromResponse);

        void PROCESS_CREATE_CHANNEL_REQUEST_2015(JObject _JobjFromResponse);

        void PROCESS_GET_CHANNEL_CATEGORY_LIST_REQUEST_2016(JObject _JobjFromResponse);

        void PROCESS_UPDATE_CHANNEL_INFO_REQUEST_2017(JObject _JobjFromResponse);

        void PROCESS_GET_FOLLOWING_CHANNEL_LIST_REQUEST_2019(JObject _JobjFromResponse);

        void PROCESS_GET_MOST_VIEWED_CHANNEL_LIST_REQUEST_2020(JObject _JobjFromResponse);

        void PROCESS_GET_OWN_CHANNEL_LIST_REQUEST_2021(JObject _JobjFromResponse);

        void PROCESS_ADD_CHANNEL_PROFILE_IMAGE_REQUEST_2024(JObject _JobjFromResponse);

        void PROCESS_ADD_CHANNEL_COVER_IMAGE_REQUEST_2025(JObject _JobjFromResponse);

        void PROCESS_ADD_UPLOADED_CHANNEL_MEDIA_2026(JObject _JobjFromResponse);

        void PROCESS_GET_UPLOADED_CHANNEL_MEDIA_2027(JObject _JobjFromResponse);

        void PROCESS_GET_CHANNEL_MEDIA_LIST_REQUEST_2028(JObject _JobjFromResponse);

        void PROCESS_UPDATE_CHANNEL_MEDIA_STATUS_2029(JObject _JobjFromResponse);

        void PROCESS_GET_FEATURED_CHANNEL_LIST_REQUEST_2030(JObject _JobjFromResponse);

        void PROCESS_UPDATE_CHANNEL_MEDIA_INFO_REQUEST_2031(JObject _JobjFromResponse);

        void PROCESS_SEARCH_CHANNEL_LIST_REQUEST_2032(JObject _JobjFromResponse);

        void PROCESS_GET_CHANNEL_PLAYLIST_REQUEST_2033(JObject _JobjFromResponse);
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

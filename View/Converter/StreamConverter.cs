﻿using Models.Constants;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Images;
using View.Utility.Stream;
using View.Utility.Stream.Utils;
using View.Utility.Wallet;

namespace View.Converter
{
    public class StreamProfileImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] is StreamModel)
            {
                StreamModel streamModel = value[0] != null && value[0] != DependencyProperty.UnsetValue ? (StreamModel)value[0] : null;
                string imageUrl = value[1] != null && value[1] != DependencyProperty.UnsetValue ? (string)value[1] : string.Empty;
                int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

                if (streamModel != null && !String.IsNullOrWhiteSpace(imageUrl))
                {
                    string convertedPUrl = HelperMethods.ConvertUrlToPurl(imageUrl, imgType);
                    string convertedName = HelperMethods.ConvertUrlToName(imageUrl, imgType);
                    string imagePath = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedName;
                    BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);

                    if (image != null)
                    {
                        return image;
                    }
                    else
                    {
                        DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedName, convertedPUrl, streamModel, SettingsConstants.IMAGE_TYPE_STREAM);
                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                    }
                }

                if (imgType == ImageUtility.IMG_THUMB || imgType == ImageUtility.IMG_CROP)
                {
                    return ImageUtility.GetDetaultProfileImage(streamModel.UserTableID, streamModel.UserName, 150, 150, 50);
                }
                else
                {
                    return ImageObjects.DEFAULT_COVER_IMAGE;
                }
            }
            else if (value[0] is StreamUserModel)
            {

                StreamUserModel streamUserModel = value[0] != null && value[0] != DependencyProperty.UnsetValue ? (StreamUserModel)value[0] : null;
                string imageUrl = value[1] != null && value[1] != DependencyProperty.UnsetValue ? (string)value[1] : string.Empty;
                int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

                if (streamUserModel != null && !String.IsNullOrWhiteSpace(imageUrl))
                {
                    string convertedPUrl = HelperMethods.ConvertUrlToPurl(imageUrl, imgType);
                    string convertedName = HelperMethods.ConvertUrlToName(imageUrl, imgType);
                    string imagePath = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedName;
                    BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);

                    if (image != null)
                    {
                        return image;
                    }
                    else
                    {
                        DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedName, convertedPUrl, streamUserModel, SettingsConstants.IMAGE_TYPE_STREAM);
                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                    }
                }

                if (imgType == ImageUtility.IMG_THUMB || imgType == ImageUtility.IMG_CROP)
                {
                    return ImageUtility.GetDetaultProfileImage(streamUserModel.UserTableID, streamUserModel.UserName, 150, 150, 50);
                }
                else
                {
                    return ImageObjects.DEFAULT_COVER_IMAGE;
                }
            }
            else if (value[0] is WalletGiftProductModel)
            {
                WalletGiftProductModel giftModel = (WalletGiftProductModel)value[0];
                int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

                if (giftModel != null && !String.IsNullOrWhiteSpace(giftModel.GiftSenderProfileImage))
                {
                    string convertedPUrl = HelperMethods.ConvertUrlToPurl(giftModel.GiftSenderProfileImage, imgType);
                    string convertedName = HelperMethods.ConvertUrlToName(giftModel.GiftSenderProfileImage, imgType);
                    string imagePath = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedName;
                    BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);

                    if (image != null)
                    {
                        return image;
                    }
                    else
                    {
                        DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedName, convertedPUrl, giftModel, SettingsConstants.IMAGE_TYPE_STREAM);
                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                    }
                }

                if (imgType == ImageUtility.IMG_THUMB || imgType == ImageUtility.IMG_CROP)
                {
                    return ImageUtility.GetDetaultProfileImage(giftModel.GiftSenderTableId, giftModel.GiftSenderName, 150, 150, 50);
                }
                else
                {
                    return ImageObjects.DEFAULT_COVER_IMAGE;
                }
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class LiveDurationConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long startTime = value[0] != DependencyProperty.UnsetValue && value[0] is long ? (long)value[0] : 0;
            long endTime = startTime > 0 && value[1] != DependencyProperty.UnsetValue && value[1] is long ? (long)value[1] : 0;
            return endTime > startTime ? TimeSpan.FromMilliseconds(endTime - startTime).ToString(@"hh\:mm\:ss") : TimeSpan.FromMilliseconds(0).ToString(@"hh\:mm\:ss");
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StreamFollowingCountConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long count = long.Parse(value.ToString());
            if (count > 999 && count < 1000000)
            {
                double normalizedValue = (count) / 1000;
                return normalizedValue.ToString() + "k";
            }
            else if (count > 1000000 && count < 1000000000)
            {
                double normalizedValue = (count) / 1000000;
                return normalizedValue.ToString() + "m";
            }
            return count;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class AppIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IntPtr appHandler = (IntPtr)((int)value);
            IntPtr iconHandle = IntPtr.Zero;
            Icon icn = null;

            try
            {
                iconHandle = NativeLibrary.SendMessage(appHandler, NativeLibrary.WM_GETICON, NativeLibrary.ICON_BIG, 0);
                if (iconHandle == IntPtr.Zero)
                    iconHandle = NativeLibrary.SendMessage(appHandler, NativeLibrary.WM_GETICON, NativeLibrary.ICON_SMALL2, 0);
                if (iconHandle == IntPtr.Zero)
                    iconHandle = NativeLibrary.SendMessage(appHandler, NativeLibrary.WM_GETICON, NativeLibrary.ICON_SMALL, 0);
                if (iconHandle == IntPtr.Zero)
                    iconHandle = NativeLibrary.GetClassLongPtr(appHandler, NativeLibrary.GCL_HICON);
                if (iconHandle == IntPtr.Zero)
                    iconHandle = NativeLibrary.GetClassLongPtr(appHandler, NativeLibrary.GCL_HICONSM);
                if (iconHandle != IntPtr.Zero)
                    icn = System.Drawing.Icon.FromHandle(iconHandle);

                if (icn != null)
                {
                    return (ImageSource)IconManager.ToImageSource(icn);
                }
            }
            catch
            {
            }
            finally
            {
                NativeLibrary.DeleteObject(appHandler);
                NativeLibrary.DeleteObject(iconHandle);
            }
            return ImageObjects.DEFAULT_APP_ICON;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IsAppSelectConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int currentID = (int)value[0];
            int selectedID = (int)value[1];
            return currentID == selectedID;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StreamContributionConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long utId = long.Parse(value.ToString());
            StreamUserModel userModel = StreamViewModel.Instance.SreamContributorList.Where(P => P.UserTableID == utId).FirstOrDefault();
            if (userModel != null)
            {
                if (userModel.ContributionType == 2)
                {
                    return userModel.ContributionType + "nd";
                }
                else if (userModel.ContributionType == 3)
                {
                    return userModel.ContributionType + "rd";
                }
                else
                {
                    return userModel.ContributionType.ToString();
                }
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StreamGiftImageConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            try
            {
                WalletGiftProductModel giftModel = null;
                string extension = parameter != null ? parameter.ToString() : ".png";
            
                if (value[0] != null && value[0] is MessageModel) 
                {
                    MessageModel msgModel = (MessageModel)value[0];
                    int productId = value[1] != null ? Int32.Parse(value[1].ToString()) : 0;
                    giftModel = WalletViewModel.Instance.GiftProductsList.Where(P => P.ProductID == productId).FirstOrDefault();
                    if (giftModel != null)
                    {
                        string imageName = giftModel.ProductName + extension;
                        string imagePath = RingIDSettings.STREAM_GIFT_FOLDER + Path.DirectorySeparatorChar + imageName;

                        bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);
                        if (bitmapImage == null)
                        {
                            new ThrdDownloadGift(msgModel,giftModel, RingIDSettings.STREAM_GIFT_FOLDER, extension).StartThread();
                        }
                    }
                }
                else if (value[0] != null && value[0] is WalletGiftProductModel) 
                {
                    giftModel = (WalletGiftProductModel)value[0];
                    string imageName = giftModel.ProductName + extension;
                    string imagePath = RingIDSettings.STREAM_GIFT_FOLDER + Path.DirectorySeparatorChar + imageName;

                    bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);
                    if (bitmapImage == null)
                    {
                        new ThrdDownloadGift(giftModel, RingIDSettings.STREAM_GIFT_FOLDER, extension).StartThread();
                    }
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(StreamGiftImageConverter).Name).Error(ex.Message + ex.StackTrace);
            }

            return bitmapImage;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StreamingStatusTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int status = (int)value;
            switch (status)
            {
                case StreamConstants.STREAMING_FINISHED:
                case StreamConstants.STREAMING_CONNECTED:
                    return null;
                case StreamConstants.STREAMING_CONNECTING:
                    return "Loading...";
                case StreamConstants.STREAMING_POOR_NERWORK:
                case StreamConstants.STREAMING_NO_DATA:
                    return "Poor Network Connection";
                case StreamConstants.STREAMING_INTERRUPTED:
                    return "Streaming Interrupted";
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}

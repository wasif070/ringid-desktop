﻿using log4net;
using Models.Constants;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.Images;

namespace View.Converter
{
    public class StatusTimeTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                return FeedUtils.GetShowableDate((long)value);
            }
            return "Time Unknown";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class SelectedLocationModelReadOnlyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null && value is LocationModel)
            {
                return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class VisibilityAddAdminButton : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel userBasicModel = (UserBasicInfoModel)value;

                if (userBasicModel.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID) return Visibility.Collapsed;

                CircleModel circle = null;
                if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(userBasicModel.CircleId, out circle))
                {
                    if (circle.SuperAdmin == DefaultSettings.LOGIN_RING_ID && !userBasicModel.IsAdmin)
                    {
                        return Visibility.Visible;
                    }
                }
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class VisibilityRemoveMemberButton : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel userBasicModel = (UserBasicInfoModel)value;
                if (userBasicModel.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID) return Visibility.Collapsed;
                CircleModel circle = null;
                if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(userBasicModel.CircleId, out circle))
                {
                    if (circle.SuperAdmin == DefaultSettings.LOGIN_RING_ID) return Visibility.Visible;
                    else if (circle.SuperAdmin == userBasicModel.ShortInfoModel.UserIdentity) return Visibility.Collapsed;
                    else
                    {
                        if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(userBasicModel.CircleId, out circle))
                        {
                            if (!userBasicModel.IsAdmin && StatusConstants.CIRCLE_MEMBER != CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[userBasicModel.CircleId].MembershipStatus)//DefaultSettings.userProfile.Admin)
                            {
                                return Visibility.Visible;
                            }
                        }
                    }
                }
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class VisibilityRemoveAdminButton : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel userBasicModel = (UserBasicInfoModel)value;
                if (userBasicModel.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID) return Visibility.Collapsed;
                CircleModel circle = null;
                if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(userBasicModel.CircleId, out circle))
                {
                    if (circle.SuperAdmin == DefaultSettings.LOGIN_RING_ID && userBasicModel.IsAdmin)
                        return Visibility.Visible;
                }
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class MembershipTagConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel userBasicModel = (UserBasicInfoModel)value;
                CircleModel circle = null;
                if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(userBasicModel.CircleId, out circle))
                {
                    if (circle.SuperAdmin == userBasicModel.ShortInfoModel.UserIdentity) return "Super Admin";
                    else if (userBasicModel.IsAdmin)
                        return "Admin";
                    else return "Member";
                }
            }
            return "Member";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class MembershipTagGroupConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string memberType = "Member";
            if (value is int)
            {
                int mType = ((int)value);

                switch (mType)
                {
                    case StatusConstants.CIRCLE_SUPER_ADMIN:
                        memberType = "Super Admin";
                        break;
                    case StatusConstants.CIRCLE_ADMIN:
                        memberType = "Admin";
                        break;
                    case StatusConstants.CIRCLE_MEMBER:
                        memberType = "Member";
                        break;
                    default:
                        break;
                }
            }
            return memberType;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NumbertoTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                int count = System.Convert.ToInt32(value);
                if (count > 0) return count;
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /*public class NumberofCounttoTextConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (values != null && values.Length > 0 && values[0] is string)
                {
                    int type = System.Convert.ToInt32(values[0]);
                    ImageModel singleImgModel = (values[1] != null && values[1] is ImageModel) ? (ImageModel)values[1] : null;
                    SingleMediaModel singleMdaModel = (values[2] != null && values[2] is SingleMediaModel) ? (SingleMediaModel)values[2] : null;
                    long NumberOfLikes = (values[3] != null && values[3] is long) ? (long)values[3] : 0;
                    long NumberOfComments = (values[4] != null && values[4] is long) ? (long)values[4] : 0;
                    long NumberOfShares = (values[5] != null && values[5] is long) ? (long)values[5] : 0;
                    switch (type)
                    {
                        case 1:
                            if (singleImgModel != null) return (singleImgModel.NumberOfLikes > 0) ? singleImgModel.NumberOfLikes + "" : "";
                            else if (singleMdaModel != null) return (singleMdaModel.LikeCount > 0) ? singleMdaModel.LikeCount + "" : "";
                            else return (NumberOfLikes > 0) ? NumberOfLikes + "" : "";
                        case 2:
                            if (singleImgModel != null) return (singleImgModel.NumberOfComments > 0) ? singleImgModel.NumberOfComments + "" : "";
                            else if (singleMdaModel != null) return (singleMdaModel.CommentCount > 0) ? singleMdaModel.CommentCount + "" : "";
                            else return (NumberOfComments > 0) ? NumberOfComments + "" : "";
                        case 3:
                            if (singleMdaModel != null) return (singleMdaModel.ShareCount > 0) ? singleMdaModel.ShareCount + "" : "";
                            else return (NumberOfShares > 0) ? NumberOfShares + "" : "";
                        default:
                            break;
                    }
                }
            }
            catch (Exception) { }
            return "";
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }*/

    //public class ShowMoreVisibilityConverter : IMultiValueConverter
    //{
    //    public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        int count = 0;
    //        int total = 0;
    //        if (value[0] is int)
    //        {
    //            count = (int)value[0];
    //        }
    //        if (value[1] is int)
    //        {
    //            total = (int)value[1];
    //        }
    //        if (count != 0 && total != 0 && count < total)
    //            return Visibility.Visible;
    //        else
    //            return Visibility.Collapsed;
    //    }

    //    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
    public class FeedSecondNameStyleConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value != null && value.Length > 1)
                {
                    if (value[0] is long && value[1] is long
                        && (long)value[0] != (long)value[1])
                    {
                        return false;
                    }
                    else return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            return true;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class FeedFriendNameStyleConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value != null && value.Length > 0)
                {
                    if (value[6] != null && value[6] is string)
                    {
                        string portalNm = (string)value[6];
                        return portalNm;
                    }
                    else if (value[7] != null && value[7] is string)
                    {
                        string pageNm = (string)value[7];
                        return pageNm;
                    }
                    else if (value[8] != null && value[8] is string)
                    {
                        string celebrityNm = (string)value[8];
                        return celebrityNm;
                    }
                    else if (value[9] != null && value[9] is string)
                    {
                        string musicPgNm = (string)value[9];
                        return musicPgNm;
                    }
                    else if (value[0] != null && value[0] is UserShortInfoModel) //FriendShortInfoModel
                    {
                        UserShortInfoModel userShortInfoModel = (UserShortInfoModel)value[0];
                        return (userShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID) ? "your" : userShortInfoModel.FullName;
                    }
                    else if (value[1] != null && value[1] is string) //model.ParentFeed.UserShortInfoModel.FullName
                    {
                        return (string)value[1];
                    }
                    else if (value[2] != null && value[2] is string) //circleName
                    {
                        return (string)value[2];
                    }
                    else if (value[3] != null && value[3] is long && ((long)value[3]) > 0) //circleId
                    {
                        CircleModel circleModel;
                        if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue((long)value[3], out circleModel))
                        {
                            return circleModel.FullName;
                        }
                    }
                    else if (value[4] != null && value[4] is string) //model.SingleMediaFeedModel.FullName
                    {
                        int subtype = (value[5] != null && value[5] is short) ? (short)value[5] : 0;
                        if (subtype != 0)
                        {
                            string nm = (string)value[4];
                            //if (nm.Equals(DefaultSettings.userProfile.FullName)) return "";
                            return (string)value[4];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            ////
            //if (value[0] is FeedModel)
            //{
            //    FeedModel model = (FeedModel)value[0];

            //    if (model.FriendShortInfoModel != null && model.FriendShortInfoModel.UserIdentity > 0)
            //    {
            //        if (model.FriendShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID)
            //        {
            //            return "your";
            //        }
            //        else
            //        {
            //            return model.FriendShortInfoModel.FullName;
            //        }
            //    }
            //    else if (model.ParentFeed != null && model.ParentFeed.UserShortInfoModel != null && model.ParentFeed.UserShortInfoModel.UserIdentity > 0)
            //    {
            //        //if (model.ParentFeed.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID)
            //        //{
            //        //    return "Your";
            //        //}
            //        //else
            //        //{
            //        return model.ParentFeed.UserShortInfoModel.FullName;
            //        //}
            //    }
            //    else if (model.GroupId > 0)
            //    {
            //        if (model.GroupName != null)
            //        {
            //            return model.GroupName;
            //        }
            //        else
            //        {
            //            CircleDTO circleDTO;
            //            if (RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY.TryGetValue(model.GroupId, out circleDTO))
            //            {
            //                return circleDTO.CircleName;
            //            }
            //        }
            //    }
            //    else if (model.SingleMediaFeedModel != null && model.FeedSubType > 0 && model.SingleMediaFeedModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
            //    {
            //        return model.SingleMediaFeedModel.FullName;
            //    }

            //}
            return "";
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class StatusWithTagConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value[0] is string && value[2] is RichTextFeedView)
            {
                string feedStatus = (string)value[0];
                ObservableCollection<TaggedUserModel> feedStatusTags = null;
                if (value[1] is ObservableCollection<TaggedUserModel>) feedStatusTags = (ObservableCollection<TaggedUserModel>)value[1];
                //FeedModel model = (FeedModel)value[0];
                RichTextFeedView richTextBoxFeedView = (RichTextFeedView)value[2];
                richTextBoxFeedView.TextHandle(feedStatus, feedStatusTags);
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class StatusGridVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] is string)
            {
                string str = (string)value[0];
                if (string.IsNullOrEmpty(str))
                {
                    if (value[1] == null) return Visibility.Collapsed;
                    else if (value[1] is ObservableCollection<TaggedUserModel>)
                    {
                        ObservableCollection<TaggedUserModel> tagList = (ObservableCollection<TaggedUserModel>)value[1];
                        if (tagList.Count == 0) return Visibility.Collapsed;
                    }
                }
            }
            return Visibility.Visible;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class FeedFriendNameToolTipStyleConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value[0] is FeedModel)
            {
                FeedModel model = (FeedModel)value[0];
                return model.PostOwner.FullName;
                //if (model.FriendShortInfoModel != null && model.FriendShortInfoModel.UserIdentity > 0)
                //{
                //    return model.FriendShortInfoModel.FullName;
                //}
                //else if (model.ParentFeed != null && model.ParentFeed.UserShortInfoModel != null && model.ParentFeed.UserShortInfoModel.UserIdentity > 0)
                //{
                //    return model.ParentFeed.UserShortInfoModel.FullName;
                //}
                //else if (model.GroupId > 0)
                //{
                //    if (model.GroupName != null)
                //    {
                //        return model.GroupName;
                //    }
                //    else
                //    {
                //        CircleModel circleModel;
                //        if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(model.GroupId, out circleModel))
                //        {
                //            return circleModel.FullName;
                //        }
                //    }
                //}
                //else if (model.SingleMediaFeedModel != null && model.FeedSubType > 0 && model.SingleMediaFeedModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
                //{
                //    return model.SingleMediaFeedModel.FullName;
                //}
            }
            return "";
        }



        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //public class FeedNameStyleConverter : IMultiValueConverter
    //{
    //    public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (values[0] is long && values[1] is string)
    //        {
    //            long uId = (long)values[0];
    //            if (uId == DefaultSettings.LOGIN_USER_ID)
    //            {
    //                return "You";
    //            }
    //            else
    //            {
    //                return values[1].ToString();
    //            }
    //        }
    //        return "";
    //    }


    //    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class FeedModelLastUpdateStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null && value is FeedModel)
            {
                FeedModel model = (FeedModel)value;
                //if (model.Status.Equals("S8I"))
                //{

                //}
                //if (model.Status.Equals("J"))
                //{

                //}
                string updatedText = "";

                if (!(model.PostOwner.UserTableID == model.WallOrContentOwner.UserTableID
                    || model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE))
                {
                    if (model.ParentFeed != null)
                    {
                        //if (model.ParentFeed.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID)
                        //{
                        //    updatedText = " feed";
                        //}
                        //else
                        //{
                        updatedText = "'s status";
                        //}                   
                    }
                    else if (model.WallOrContentOwner.FullName.Length > 0)
                    {
                        //if (model.WallOrContentOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                        //{
                        //    updatedText = " timeline";
                        //}
                        //else
                        //{
                        updatedText = "'s timeline";
                        //}
                    }
                    else if (model.SingleMediaFeedModel != null && model.FeedSubType == SettingsConstants.MEDIA_TYPE_AUDIO)
                    {
                        updatedText = "'s music";
                        //if (model.SingleMediaFeedModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                        //{
                        //    updatedText = "a music";
                        //}
                        //else
                        //{
                        //    updatedText = "'s music";
                        //}
                    }
                    else if (model.SingleMediaFeedModel != null && model.FeedSubType == SettingsConstants.MEDIA_TYPE_VIDEO)
                    {
                        updatedText = "'s video";
                        //if (model.SingleMediaFeedModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                        //{
                        //    updatedText = "a video";
                        //}
                        //else
                        //{
                        //    updatedText = "'s video";
                        //}
                    }
                }
                //else if (model.GroupId > 0)
                //{
                //    updatedText = " ring circle";
                //}
                return updatedText;
            }
            return "";
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class FeedModeltoUpdateStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value != null && value is FeedModel)
                {
                    FeedModel model = (FeedModel)value;
                    string updatedText = "";
                    //if (model.ImageList != null && model.ImageList[0].AlbumName == "test")
                    //{
                    //}
                    if (model.ParentFeed != null)
                    {
                        //updatedText = ((model.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID) ? " have" : " has") + " shared ";
                        updatedText = " shared ";
                    }
                    else if (model.PostOwner.UserTableID == model.WallOrContentOwner.UserTableID && model.ImageList != null && model.ImageList.Count == 1
                        && model.PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_GENERAL)
                    {
                        updatedText = "";
                        ImageModel firstImgModel = model.ImageList.First();
                        if (firstImgModel != null && firstImgModel.ImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
                        {
                            updatedText = "";
                        }
                        else if (firstImgModel != null && (firstImgModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE || firstImgModel.ImageType == SettingsConstants.TYPE_COVER_IMAGE))
                        {
                            updatedText = " updated " + ((firstImgModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE) ? "profile picture" : "cover photo");
                        }
                        else
                        {
                            updatedText = !model.IsStaticAlbum ? " added 1 photo to the album: " : "";
                        }
                    }
                    else if (model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE)
                    {
                        if (model.ImageList != null)
                        {
                            int numberofPhotos = 0;
                            if (model.TotalImage > 0)
                            {
                                numberofPhotos = model.TotalImage;
                            }
                            else if (model.ImageList != null && model.ImageList.Count > 0)
                            {
                                numberofPhotos = model.ImageList.Count;
                            }
                            if (numberofPhotos > 1 || !model.IsStaticAlbum)
                                updatedText = " added " + numberofPhotos + (numberofPhotos > 1 ? " photos" : " photo") + (!model.IsStaticAlbum ? " to the album: " : "");
                            else
                            {
                                updatedText = " posted";
                            }
                            //if (numberofPhotos > 1)
                            //    updatedText = " added " + numberofPhotos + "photos in ";
                        }
                        else if (model.MediaContent != null && model.MediaContent.MediaList != null)
                        {
                            //if (model.MediaContent.MediaList.Count > 1)
                            //{
                            if (model.MediaContent.TotalMediaCount == 1 && !model.IsStaticAlbum)
                            {
                                updatedText = " added a" + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" : " video") + " to the album: ";
                            }
                            else if (model.MediaContent.TotalMediaCount > 1)
                            {
                                updatedText = " added " + model.MediaContent.TotalMediaCount + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" : " videos") + (!model.IsStaticAlbum ? " to the album: " : "");
                            }
                            else
                            {
                                updatedText = " posted";
                            }
                        }
                        else
                        {
                            if (model.LinkModel != null)//!string.IsNullOrEmpty(model.LinkModel.PreviewUrl))
                            {
                                updatedText = " shared a link";
                            }
                            else
                            {
                                updatedText = " posted";
                            }
                        }
                    }
                    else if (model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_GENERAL || model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_MUSICPAGE
                        || model.PostOwner.UserTableID != model.WallOrContentOwner.UserTableID)
                    {
                        if (model.ImageList != null && model.ImageList.Count > 0)
                        {
                            int numberofPhotos = 0;
                            if (model.TotalImage > 0)
                            {
                                numberofPhotos = model.TotalImage;
                            }
                            else if (model.ImageList != null && model.ImageList.Count > 0)
                            {
                                numberofPhotos = model.ImageList.Count;
                            }
                            if (numberofPhotos > 1 || !model.IsStaticAlbum)
                                updatedText = " added " + numberofPhotos + (numberofPhotos > 1 ? " photos" : " photo") + (!model.IsStaticAlbum ? " to the album: " : "");
                            //if (numberofPhotos > 1)
                            //    updatedText = " added " + numberofPhotos + "photos on ";
                        }
                        else if (model.MediaContent != null && model.MediaContent.MediaList != null)
                        {
                            if (model.MediaContent.TotalMediaCount == 1)
                            {
                                //if (model.SingleMediaFeedModel != null)
                                //{
                                if (!model.IsStaticAlbum)
                                    updatedText = " added a" + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" : " video") + (!model.IsStaticAlbum ? " to the album: " : "");
                                //}
                                //else
                                //{
                                //    updatedText = " shared ";
                                //}
                            }
                            else if (model.MediaContent.TotalMediaCount > 1)
                            {
                                updatedText = " added " + model.MediaContent.TotalMediaCount + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" :
                                    " videos") + (!model.IsStaticAlbum ? " to the album: " : "");
                            }
                        }
                        else
                        {
                            if (model.LinkModel != null && !string.IsNullOrEmpty(model.LinkModel.PreviewUrl))
                            {
                                updatedText = " shared a link";
                            }
                            else if (model.PostOwner.UserTableID != model.WallOrContentOwner.UserTableID)
                            {
                                updatedText = " posted";
                            }
                            else updatedText = "";
                        }
                    }
                    else if (model.LinkModel != null && !string.IsNullOrEmpty(model.LinkModel.PreviewUrl))
                    {
                        updatedText = " shared a link";
                    }
                    return updatedText;
                }
            }
            catch (Exception)
            {

            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class FeedModeltoUpdateStringConverter1 : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null && value is FeedModel)
            {
                FeedModel model = (FeedModel)value;
                string updatedText = "";
                //if (model.ImageList != null && model.ImageList[0].AlbumName == "test")
                //{
                //}
                if (model.ParentFeed != null)
                {
                    //updatedText = ((model.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID) ? " have" : " has") + " shared ";
                    updatedText = " shared ";
                }
                else if ((model.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID && model.WallOrContentOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                    && (model.ImageList != null && model.ImageList.Count == 1))
                {
                    updatedText = "";
                    ImageModel firstImgModel = model.ImageList.First();
                    if (firstImgModel != null && firstImgModel.ImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
                    {
                        updatedText = "";
                    }
                    else if (firstImgModel != null && (firstImgModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE || firstImgModel.ImageType == SettingsConstants.TYPE_COVER_IMAGE))
                    {
                        updatedText = " updated " + ((firstImgModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE) ? "profile picture" : "cover photo");
                    }
                    else
                    {
                        updatedText = " added 1 photo" + (!model.IsStaticAlbum ? " to the album: " : "");
                    }
                }
                else if (model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE)
                {
                    if (model.ImageList != null)
                    {
                        int numberofPhotos = 0;
                        if (model.TotalImage > 0)
                        {
                            numberofPhotos = model.TotalImage;
                        }
                        else if (model.ImageList != null && model.ImageList.Count > 0)
                        {
                            numberofPhotos = model.ImageList.Count;
                        }

                        updatedText = " added " + numberofPhotos + (numberofPhotos > 1 ? " photos" : " photo") + (!model.IsStaticAlbum ? " to the album: " : "");
                        //if (numberofPhotos > 1)
                        //    updatedText = " added " + numberofPhotos + "photos in ";
                    }
                    else if (model.MediaContent != null && model.MediaContent.MediaList != null)
                    {
                        //if (model.MediaContent.MediaList.Count > 1)
                        //{
                        if (model.MediaContent.TotalMediaCount == 1)
                        {
                            updatedText = " added a" + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" : " video") + (!model.IsStaticAlbum ? " to the album: " : "");
                        }
                        else if (model.MediaContent.TotalMediaCount > 1)
                        {
                            updatedText = " added " + model.MediaContent.TotalMediaCount + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" : " videos") + (!model.IsStaticAlbum ? " to the album: " : "");
                        }
                        //}
                        //else
                        //{
                        //    updatedText = " posted in ";
                        //}
                    }
                    else
                    {
                        if (model.LinkModel != null)//!string.IsNullOrEmpty(model.LinkModel.PreviewUrl))
                        {
                            updatedText = " shared a link";
                        }
                        else
                        {
                            updatedText = " posted";
                        }
                    }
                }
                else if (!(model.PostOwner.UserTableID == model.WallOrContentOwner.UserTableID)
                    && model.PostOwner.ProfileType != SettingsConstants.PROFILE_TYPE_NEWSPORTAL && model.PostOwner.ProfileType != SettingsConstants.PROFILE_TYPE_PAGES
                    && model.PostOwner.ProfileType != SettingsConstants.PROFILE_TYPE_MUSICPAGE)
                {
                    if (model.ImageList != null && model.ImageList.Count > 0)
                    {
                        int numberofPhotos = 0;
                        if (model.TotalImage > 0)
                        {
                            numberofPhotos = model.TotalImage;
                        }
                        else if (model.ImageList != null && model.ImageList.Count > 0)
                        {
                            numberofPhotos = model.ImageList.Count;
                        }
                        if (numberofPhotos > 0)
                            updatedText = " added " + numberofPhotos + (numberofPhotos > 1 ? " photos" : " photo") + (!model.IsStaticAlbum ? " to the album: " : "");
                        //if (numberofPhotos > 1)
                        //    updatedText = " added " + numberofPhotos + "photos on ";
                    }
                    else if (model.MediaContent != null && model.MediaContent.MediaList != null)
                    {
                        if (model.MediaContent.TotalMediaCount == 1)
                        {
                            updatedText = " added a" + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" : " video") + (!model.IsStaticAlbum ? " to the album: " : "");
                        }
                        else if (model.MediaContent.TotalMediaCount > 1)
                        {
                            updatedText = " added " + model.MediaContent.TotalMediaCount + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" :
                                " videos") + (!model.IsStaticAlbum ? " to the album: " : "");
                        }
                    }
                    else
                    {
                        if (model.LinkModel != null && !string.IsNullOrEmpty(model.LinkModel.PreviewUrl))
                        {
                            updatedText = " shared a link";
                        }
                        else
                        {
                            updatedText = " posted";
                        }
                    }
                }
                else if (model.LinkModel != null && !string.IsNullOrEmpty(model.LinkModel.PreviewUrl))
                {
                    updatedText = " shared a link";
                }
                else if (model.ImageList != null && model.ImageList.Count > 0 && model.PostOwner.ProfileType != SettingsConstants.PROFILE_TYPE_NEWSPORTAL
                    && model.PostOwner.ProfileType != SettingsConstants.PROFILE_TYPE_PAGES && model.PostOwner.ProfileType != SettingsConstants.PROFILE_TYPE_MUSICPAGE)
                {
                    int numberofPhotos = 0;
                    if (model.TotalImage > 0)
                    {
                        numberofPhotos = model.TotalImage;
                    }
                    else if (model.ImageList != null && model.ImageList.Count > 0)
                    {
                        numberofPhotos = model.ImageList.Count;
                    }
                    if (numberofPhotos > 0)
                        updatedText = " added " + numberofPhotos + (numberofPhotos > 1 ? " photos" : " photo") + (!model.IsStaticAlbum ? " to the album: " : "");
                    //if (numberofPhotos > 1)
                    //    updatedText = " added " + numberofPhotos + " photos";
                }
                else if (model.MediaContent != null && model.MediaContent.MediaList != null && model.PostOwner.ProfileType != SettingsConstants.PROFILE_TYPE_NEWSPORTAL
                    && model.PostOwner.ProfileType != SettingsConstants.PROFILE_TYPE_PAGES && model.PostOwner.ProfileType != SettingsConstants.PROFILE_TYPE_MUSICPAGE)
                {
                    if (model.MediaContent.TotalMediaCount == 1)
                    {
                        if (model.SingleMediaFeedModel != null)
                        {
                            updatedText = " added a" + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" : " video") + (!model.IsStaticAlbum ? " to the album: " : "");
                        }
                        else
                        {
                            updatedText = " shared ";
                        }
                    }
                    else if (model.MediaContent.TotalMediaCount > 1)
                    {
                        updatedText = " added " + model.MediaContent.TotalMediaCount + ((model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? " music" : " videos") + (!model.IsStaticAlbum ? " to the album: " : "");
                    }

                }

                return updatedText;
            }
            //if (value != null && value is FeedModel)
            //{
            //    FeedModel model = (FeedModel)value;
            //    string updatedText = "";

            //    if (model.ParentFeed != null)
            //    {
            //        //updatedText = ((model.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID) ? " have" : " has") + " shared ";
            //        updatedText = " shared ";
            //    }
            //    else if ((model.FriendShortInfoModel == null || (model.FriendShortInfoModel != null && model.FriendShortInfoModel.UserIdentity == 0))
            //        && model.GroupId == 0 && model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_IMAGE && (model.ImageList != null && model.ImageList.Count == 1))
            //    {
            //        if (model.ImageList != null)
            //        {
            //            updatedText = "";
            //            ImageModel firstImgModel = model.ImageList.First();
            //            if (firstImgModel != null && firstImgModel.ImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
            //            {
            //                updatedText = "";
            //            }
            //            else if (firstImgModel != null && (firstImgModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE || firstImgModel.ImageType == SettingsConstants.TYPE_COVER_IMAGE))
            //            {
            //                updatedText = " updated " + ((firstImgModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE) ? "profile picture" : "cover photo");
            //            }
            //        }
            //    }
            //    else if ((model.FriendShortInfoModel == null || (model.FriendShortInfoModel != null && model.FriendShortInfoModel.UserIdentity == 0)) && model.GroupId == 0 && model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_TEXT_STATUS)
            //    {
            //        if (model.ImageList != null)
            //        {
            //            updatedText = "";
            //        }
            //        else if (!string.IsNullOrEmpty(model.PreviewUrl))
            //        {
            //            updatedText = " shared a link";
            //        }
            //    }
            //    else if ((model.FriendShortInfoModel == null || (model.FriendShortInfoModel != null && model.FriendShortInfoModel.UserIdentity == 0)) && model.GroupId == 0 && model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_IMAGE)
            //    {
            //        int numberofPhotos = 0;
            //        if (model.TotalImage > 0)
            //        {
            //            numberofPhotos = model.TotalImage;
            //        }
            //        else if (model.ImageList != null && model.ImageList.Count > 0)
            //        {
            //            numberofPhotos = model.ImageList.Count;
            //        }
            //        if (numberofPhotos > 0)
            //            updatedText = " added " + numberofPhotos + (numberofPhotos > 1 ? " photos" : " photo");
            //        //if (numberofPhotos > 1)
            //        //    updatedText = " added " + numberofPhotos + " photos";
            //    }
            //    else if ((model.FriendShortInfoModel == null || (model.FriendShortInfoModel != null && model.FriendShortInfoModel.UserIdentity == 0)) && model.GroupId == 0 &&
            //        (model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO || model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_VIDEO))
            //    {
            //        if (model.MediaContent != null && model.MediaContent.MediaList != null)
            //        {
            //            if (model.MediaContent.MediaList.Count == 1)
            //            {
            //                if (model.FeedSubType == 0)
            //                {
            //                    updatedText = " added a" + ((model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO) ? " music" : " video");
            //                }
            //                else
            //                {
            //                    updatedText = " shared ";
            //                }
            //            }
            //            else if (model.MediaContent.MediaList.Count > 1)
            //            {
            //                updatedText = " added " + model.MediaContent.MediaList.Count + ((model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO) ? " music files" : " videos");
            //            }

            //        }
            //        else
            //        {
            //            updatedText = "";
            //        }
            //    }
            //    else if (model.FriendShortInfoModel != null && model.FriendShortInfoModel.UserIdentity > 0 && model.FriendShortInfoModel.FullName.Length > 0)
            //    {
            //        if (model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_IMAGE)
            //        {
            //            int numberofPhotos = 0;
            //            if (model.TotalImage > 0)
            //            {
            //                numberofPhotos = model.TotalImage;
            //            }
            //            else if (model.ImageList != null && model.ImageList.Count > 0)
            //            {
            //                numberofPhotos = model.ImageList.Count;
            //            }
            //            if (numberofPhotos > 0)
            //                updatedText = " added " + numberofPhotos + (numberofPhotos > 1 ? " photos" : " photo") + " on ";
            //            //if (numberofPhotos > 1)
            //            //    updatedText = " added " + numberofPhotos + "photos on ";
            //        }
            //        else if (model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO || model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_VIDEO)
            //        {
            //            if (model.MediaContent != null && model.MediaContent.MediaList != null)
            //            {
            //                if (model.MediaContent.MediaList.Count == 1)
            //                {
            //                    updatedText = " added a" + ((model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO) ? " music" : " video") + " on ";
            //                }
            //                else if (model.MediaContent.MediaList.Count > 1)
            //                {
            //                    updatedText = " added " + model.MediaContent.MediaList.Count + ((model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO) ? " music files" : " videos") + " on ";
            //                }
            //            }
            //            else
            //            {
            //                //updatedText = "";
            //                updatedText = " posted on ";
            //            }
            //        }
            //        else
            //        {
            //            if (!string.IsNullOrEmpty(model.PreviewUrl))
            //            {
            //                updatedText = " shared a link on ";
            //            }
            //            else
            //            {
            //                updatedText = " posted on ";
            //            }
            //        }

            //    }
            //    else if (model.GroupId > 0)
            //    {
            //        if (model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_IMAGE)
            //        {
            //            int numberofPhotos = 0;
            //            if (model.TotalImage > 0)
            //            {
            //                numberofPhotos = model.TotalImage;
            //            }
            //            else if (model.ImageList != null && model.ImageList.Count > 0)
            //            {
            //                numberofPhotos = model.ImageList.Count;
            //            }

            //            updatedText = " added " + numberofPhotos + (numberofPhotos > 1 ? " photos" : " photo") + " in ";
            //            //if (numberofPhotos > 1)
            //            //    updatedText = " added " + numberofPhotos + "photos in ";
            //        }
            //        else if (model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO || model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_VIDEO)
            //        {
            //            if (model.MediaContent != null && model.MediaContent.MediaList != null && model.MediaContent.MediaList.Count > 1)
            //            {
            //                if (model.MediaContent.MediaList.Count == 1)
            //                {
            //                    updatedText = " added a" + ((model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO) ? " music" : " video") + " in ";
            //                }
            //                else if (model.MediaContent.MediaList.Count > 1)
            //                {
            //                    updatedText = " added " + model.MediaContent.MediaList.Count + ((model.BookPostType == SettingsConstants.NEWS_FEED_TYPE_AUDIO) ? " music files" : " videos") + " in ";
            //                }
            //            }
            //            else
            //            {
            //                updatedText = " posted in ";
            //            }
            //        }
            //        else
            //        {
            //            if (!string.IsNullOrEmpty(model.PreviewUrl))
            //            {
            //                updatedText = " shared a link in ";
            //            }
            //            else
            //            {
            //                updatedText = " posted in ";
            //            }
            //        }
            //    }

            //    return updatedText;
            //}
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FeedModeltoUpdateStringOnConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value != null && value is FeedModel)
                {
                    FeedModel model = (FeedModel)value;
                    string updatedText = "";

                    if (model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE)
                    {
                        updatedText = " in ";
                    }
                    else if (!(model.PostOwner.UserTableID == model.WallOrContentOwner.UserTableID)
                        && model.ParentFeed == null)
                    {
                        updatedText = " on ";
                    }

                    return updatedText;
                }
            }
            catch (Exception)
            {

            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //public class TagFriendNameStringConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        try
    //        {
    //            int type = System.Convert.ToInt32(parameter);
    //            switch (type)
    //            {
    //                case 0:
    //                    if (value is ObservableCollection<UserShortInfoModel>)
    //                    {
    //                        ObservableCollection<UserShortInfoModel> tgList = (ObservableCollection<UserShortInfoModel>)value;
    //                        if (tgList.Count > 0) return tgList[0].FullName;
    //                    }
    //                    break;
    //                case 1:
    //                    if (value is short)
    //                    {
    //                        short totaltags = (short)value;
    //                        if (totaltags > 1) return (totaltags - 1) + " others";
    //                    }
    //                    break;
    //                case 2:
    //                    if (value is ObservableCollection<UserShortInfoModel>)
    //                    {
    //                        ObservableCollection<UserShortInfoModel> tgList = (ObservableCollection<UserShortInfoModel>)value;
    //                        if (tgList.Count > 1) return tgList[1].FullName;
    //                    }
    //                    break;
    //                default:
    //                    break;
    //            }
    //        }
    //        catch (Exception)
    //        {
    //        }
    //        return "";
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class NewStatusTagFriendNameStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is ObservableCollection<UserShortInfoModel>)
            {
                ObservableCollection<UserShortInfoModel> tagList = (ObservableCollection<UserShortInfoModel>)value;
                if (tagList != null && tagList.Count > 0 && parameter is string)
                {
                    int type = System.Convert.ToInt32(parameter);
                    switch (type)
                    {
                        case 0:
                            return tagList[0].FullName;
                        case 1:
                            if (tagList.Count == 2) return tagList[1].FullName;
                            else if (tagList.Count > 2) return (tagList.Count - 1) + " others";
                            break;
                        default:
                            break;
                    }
                }
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    //public class ShareTagFriendNameStringConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (value is UCShareSingleFeedView)
    //        {
    //            UCShareSingleFeedView ucShareSingleFeedView = (UCShareSingleFeedView)value;
    //            ObservableCollection<UserShortInfoModel> tagList = ucShareSingleFeedView.TaggedFriends;
    //            if (tagList != null && tagList.Count > 0 && parameter is string)
    //            {
    //                int type = System.Convert.ToInt32(parameter);
    //                switch (type)
    //                {
    //                    case 0:
    //                        return tagList[0].FullName;
    //                    case 1:
    //                        if (tagList.Count == 2) return tagList[1].FullName;
    //                        else if (tagList.Count > 2) return (tagList.Count - 1) + " others";
    //                        break;
    //                    default:
    //                        break;
    //                }
    //            }
    //        }
    //        return "";
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
    public class FeedModeltoActivistNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null && value is UserShortInfoModel)
            {
                UserShortInfoModel ActivistShortInfoModel = (UserShortInfoModel)value;
                if (ActivistShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID) return "You";
                else return ActivistShortInfoModel.FullName;
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class FeedPanelsVisbilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return Visibility.Collapsed;
            //else if (value is string)
            //{
            //    string str = (string)value;
            //    return string.IsNullOrEmpty(str) ? Visibility.Collapsed : Visibility.Visible;
            //}
            else if (value is List<ImageModel>)
            {
                List<ImageModel> list = (List<ImageModel>)value;
                return (list != null && list.Count > 0) ? Visibility.Visible : Visibility.Collapsed;
            }
            //else if (value is UserShortInfoModel)
            //{
            //    UserShortInfoModel aid = (UserShortInfoModel)value;
            //    return ((aid.UserIdentity > 0) || (aid.UserTableID > 0)) ? Visibility.Visible : Visibility.Collapsed;
            //}
            else if (value is UserShortInfoModel)
            {
                UserShortInfoModel ActivistShortInfoModel = (UserShortInfoModel)value;
                return ((ActivistShortInfoModel.UserIdentity > 0) || (ActivistShortInfoModel.UserTableID > 0)) ? Visibility.Visible : Visibility.Collapsed;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ParentFeedShareVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return false;
            else if (value is FeedModel)
            {
                FeedModel parentModel = (FeedModel)value;
                if (parentModel == null)
                    return false;
            }
            //if (value[0] is FeedModel && value[1] is int)
            //{
            //    FeedModel parentModel = (FeedModel)value[0];
            //    int i = (int)value[1];

            //    if (parentModel != null && i == NewsFeedPanel.SHARE_FEED)
            //        return true;
            //    else
            //        return false;
            //}

            return true;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SingleFeedNameStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return false;
            else if (value is long)
            {
                long utId = (long)value;
                return HelperMethods.IsRingIDOfficialAccount(utId);
            }
            return false;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class FeedPanelTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return false;
            else if (value is short)
            {
                short feedPanelType = (short)value;
                if (feedPanelType == 7 || feedPanelType == 11)
                    return true;
            }
            return false;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class LocationModelToImageConverter : IValueConverter
    {
        public class AsyncTask : INotifyPropertyChanged
        {
            public AsyncTask(Func<object> valueFunc)
            {
                AsyncValue = "loading async value"; //temp value for demo
                LoadValue(valueFunc);
            }

            private async Task LoadValue(Func<object> valueFunc)
            {
                AsyncValue = await Task.Factory.StartNew<object>(() =>
                {
                    return valueFunc();
                });
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("AsyncValue"));
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public object AsyncValue { get; set; }
        }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is LocationModel)
            {
                try
                {
                    LocationModel model = (LocationModel)value;
                    if (!string.IsNullOrEmpty(model.ImageUrl) && (model.Latitude != 0 || model.Longitude != 0))
                    {
                        string mapFilePath = RingIDSettings.TEMP_MAP_FOLDER + System.IO.Path.DirectorySeparatorChar + Math.Round(model.Latitude, 3) + "_" + Math.Round(model.Longitude, 3) + ".png";
                        if (File.Exists(mapFilePath))
                        {
                            /*byte[] imageBytes = File.ReadAllBytes(mapFilePath);
                            BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                            imageBytes = null;
                            return BmpImg;*/
                            BitmapImage bi = new BitmapImage();
                            bi.BeginInit();
                            bi.DecodePixelHeight = 170;
                            bi.DecodePixelWidth = 600;
                            bi.CacheOption = BitmapCacheOption.OnLoad;
                            bi.UriSource = new Uri(mapFilePath);
                            bi.EndInit();
                            GC.SuppressFinalize(bi);
                            return bi;
                        }
                        else
                        {
                            DefaultSettings.FEED_STATIC_MAP_HIT++;
                            System.Diagnostics.Debug.WriteLine("DefaultSettings.FEED_STATIC_MAP_HIT==>" + DefaultSettings.FEED_STATIC_MAP_HIT);
                            new MapImage(model, mapFilePath).Start();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            else if (value is SingleMediaModel)
            {
                try
                {
                    SingleMediaModel singleMediaModel = (SingleMediaModel)value;
                    if (string.IsNullOrEmpty(singleMediaModel.ThumbUrl) || singleMediaModel.NoImageFound)
                    {
                        return (singleMediaModel.MediaType == 1) ? ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE : ImageObjects.DEFAULT_VIDEO_ALBUM_IMAGE;
                    }
                    else
                    {
                        string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + singleMediaModel.ThumbUrl.Replace("/", "_");
                        if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                        if (File.Exists(filePathImage))
                        {
                            //ImageUploaderModel imgUpModel = (ImageUploaderModel)value;
                            //BitmapImage bitmapsource = null;
                            //if (ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.TryGetValue(filePathImage, out bitmapsource))
                            //{
                            //    GC.SuppressFinalize(bitmapsource);
                            //    GC.SuppressFinalize(ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[filePathImage]);
                            //    ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[filePathImage] = null;
                            //    ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.Remove(filePathImage);
                            //    return bitmapsource;
                            //}
                            //else
                            //{
                            //    if (FeedBaseUserImageLoader.Instance == null)
                            //    {
                            //        FeedBaseUserImageLoader.Instance = new FeedBaseUserImageLoader();
                            //    }
                            //    FeedBaseUserImageLoader.Instance.LoadSingleImageUploaderModel(singleMediaModel);
                            //}

                            //approach 1 byte array
                            byte[] oFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                            if (oFileBytes != null && oFileBytes.Length > 1)
                            {
                                BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(oFileBytes);
                                GC.SuppressFinalize(BmpImg);
                                return BmpImg;
                            }

                            /*//approach 2 failed queue backgroundthread
                            BitmapImage image = new BitmapImage();
                            image.Freeze();
                            BitmapImagesLoader.Instance.EnqueueImages(image.Clone(), filePathImage);
                            return image;
                            byte[] imageBytes = File.ReadAllBytes(filePathImage);
                            BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                            imageBytes = null;
                            GC.SuppressFinalize(BmpImg);
                            return BmpImg;*/

                            /*//approach 3 failed task
                            return new AsyncTask(() =>
                            {
                                BitmapImage img = new BitmapImage();
                                img.BeginInit();
                                img.CacheOption = BitmapCacheOption.OnLoad;
                                img.UriSource = new Uri(filePathImage);
                                img.EndInit();
                                return img;
                            });*/
                        }
                        else if (ServerAndPortSettings.GetVODThumbImageUpResourceURL != null && DefaultSettings.IsInternetAvailable)
                        {
                            new MediaImage(singleMediaModel, filePathImage).Start();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            else if (value is NewsPortalModel)
            {
                try
                {
                    NewsPortalModel newsPortalModel = (NewsPortalModel)value;
                    if (string.IsNullOrEmpty(newsPortalModel.ProfileImage))
                    {
                        return ImageLocation.NEWSPORTAL_DEFAULT_IMAGE;
                    }
                    else
                    {
                        string filePathImage = RingIDSettings.TEMP_NEWSPORTAL_IMAGES_FOLDER + Path.DirectorySeparatorChar + newsPortalModel.ProfileImage.Replace("/", "_");
                        if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                        if (File.Exists(filePathImage))
                        {
                            byte[] oFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                            if (oFileBytes != null && oFileBytes.Length > 1)
                            {
                                BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(oFileBytes);
                                GC.SuppressFinalize(BmpImg);
                                return BmpImg;
                            }

                            //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                            //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                            //imageBytes = null;
                            //GC.SuppressFinalize(BmpImg);
                            //return BmpImg;
                        }
                        else
                        {
                            new MediaImage(newsPortalModel, filePathImage).Start();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            else if (value is PageInfoModel)
            {
                try
                {
                    PageInfoModel pageInfoModel = (PageInfoModel)value;
                    if (string.IsNullOrEmpty(pageInfoModel.ProfileImage))
                    {
                        return ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE;
                    }
                    else
                    {
                        string filePathImage = RingIDSettings.TEMP_PAGES_IMAGES_FOLDER + Path.DirectorySeparatorChar + pageInfoModel.ProfileImage.Replace("/", "_");
                        if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                        if (File.Exists(filePathImage))
                        {
                            byte[] imageFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                            if (imageFileBytes != null && imageFileBytes.Length > 1)
                            {
                                BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(imageFileBytes);
                                GC.SuppressFinalize(BmpImg);
                                return BmpImg;
                            }

                            //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                            //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                            //imageBytes = null;
                            //GC.SuppressFinalize(BmpImg);
                            //return BmpImg;
                        }
                        else
                        {
                            new MediaImage(pageInfoModel, filePathImage).Start();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            else if (value is MusicPageModel)
            {
                try
                {
                    MusicPageModel musicPageModel = (MusicPageModel)value;
                    if (string.IsNullOrEmpty(musicPageModel.ProfileImage))
                    {
                        return ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE;
                    }
                    else
                    {
                        string filePathImage = RingIDSettings.TEMP_MEDIAPAGE_IMAGES_FOLDER + Path.DirectorySeparatorChar + musicPageModel.ProfileImage.Replace("/", "_");
                        if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                        if (File.Exists(filePathImage))
                        {
                            byte[] imageFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                            if (imageFileBytes != null && imageFileBytes.Length > 1)
                            {
                                BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(imageFileBytes);
                                GC.SuppressFinalize(BmpImg);
                                return BmpImg;
                            }

                            //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                            //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                            //imageBytes = null;
                            //GC.SuppressFinalize(BmpImg);
                            //return BmpImg;
                        }
                        else
                        {
                            new MediaImage(musicPageModel, filePathImage).Start();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            else if (value is CelebrityModel)
            {
                try
                {
                    CelebrityModel celebrityModel = (CelebrityModel)value;
                    if (string.IsNullOrEmpty(celebrityModel.ProfileImage))
                    {
                        return ImageObjects.CELEBRITY_DEFAULT_IMAGE;
                    }
                    else
                    {
                        string filePathImage = RingIDSettings.TEMP_CELEBRITY_IMAGES_FOLDER + Path.DirectorySeparatorChar + celebrityModel.ProfileImage.Replace("/", "_");
                        if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                        if (File.Exists(filePathImage))
                        {
                            byte[] imageFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                            if (imageFileBytes != null && imageFileBytes.Length > 1)
                            {
                                BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(imageFileBytes);
                                GC.SuppressFinalize(BmpImg);
                                return BmpImg;
                            }

                            //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                            //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                            //imageBytes = null;
                            //GC.SuppressFinalize(BmpImg);
                            //return BmpImg;
                        }
                        else
                        {
                            new MediaImage(celebrityModel, filePathImage).Start();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
<<<<<<< HEAD

=======
    public class BookImageConverter : IValueConverter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FeedImageConverter).Name);
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value is ImageModel)
                {
                    ImageModel imageModel = (ImageModel)value;
                    BitmapImage bitmapsource = null;
                    if (imageModel.NoImageFound)
                    {
                        bitmapsource = ImageObjects.NO_IMAGE_FOUND;
                    }
                    else if (ImageDictionaries.Instance.TMP_BOOK_IMAGES.TryGetValue(imageModel.ImageId, out bitmapsource))
                    {
                        GC.SuppressFinalize(bitmapsource);
                        GC.SuppressFinalize(ImageDictionaries.Instance.TMP_BOOK_IMAGES[imageModel.ImageId]);
                        ImageDictionaries.Instance.TMP_BOOK_IMAGES[imageModel.ImageId] = null;
                        ImageDictionaries.Instance.TMP_BOOK_IMAGES.Remove(imageModel.ImageId);
                    }
                    else
                    {
                        bitmapsource = ImageObjects.PHOTO_LOADING;
                        ImageQueueLoader.Instance.LoadImage(imageModel);
                    }
                    return bitmapsource;
                    /*else if (!string.IsNullOrEmpty(imageModel.ImageLocation) && File.Exists(imageModel.ImageLocation))
                    {
                        double mw = 0, mh = 0;
                        ImageUtility.GetResizeImageParameters(imageModel.FeedImageInfoModel.Width, imageModel.FeedImageInfoModel.Height, imageModel.FeedImageInfoModel.ReWidth, imageModel.FeedImageInfoModel.ReHeight, out mw, out mh);
                        BitmapImage bitmap = new BitmapImage();
                        var stream = new FileStream(imageModel.ImageLocation, FileMode.Open);
                        bitmap.BeginInit();
                        bitmap.CacheOption = BitmapCacheOption.OnLoad;
                        bitmap.StreamSource = stream;
                        bitmap.DecodePixelHeight = (int)mh;
                        bitmap.DecodePixelWidth = (int)mw;
                        bitmap.EndInit();
                        stream.Close();
                        stream.Dispose();
                        bitmap.Freeze();
                        return bitmap;
                    }
                    else if (!string.IsNullOrEmpty(imageModel.ImageUrl))
                    {
                        if (ServerAndPortSettings.GetImageServerResourceURL != null)
                        {
                            string imgUrl = HelperMethods.GetUrlWithSize(imageModel.ImageUrl, ImageUtility.IMG_600);
                            if (!string.IsNullOrWhiteSpace(imgUrl))
                            {
                                BackgroundWorker bgworker = new BackgroundWorker();
                                bgworker.DoWork += (s, e) =>
                                {
                                    string image_url_with_base = ServerAndPortSettings.GetImageServerResourceURL + imgUrl;
                                    e.Result = ImageUtility.DownloadRemoteImageFile(image_url_with_base, imageModel.ImageLocation);
                                };
                                bgworker.RunWorkerCompleted += (s, e) =>
                                {
                                    if (e.Result != null && e.Result is bool && (bool)e.Result)
                                    {
                                        imageModel.OnPropertyChanged("CurrentInstance");
                                    }
                                };
                                bgworker.RunWorkerAsync();
                            }
                        }
                        return ImageObjects.PHOTO_LOADING;
                    } */
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
            return ImageObjects.PHOTO_LOADING;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
    public class FeedImageConverter : IMultiValueConverter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FeedImageConverter).Name);
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            try
            {
                if (value[0] is ImageModel)
                {
                    ImageModel imageModel = (ImageModel)value[0];
                    if (imageModel.NoImageFound)
                    {
                        bitmapImage = ImageObjects.NO_IMAGE_FOUND;
                        GC.SuppressFinalize(bitmapImage);
                    }
                    else if (!string.IsNullOrEmpty(imageModel.ImageLocation))
                    {
                        Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageModel.ImageLocation);
                        if (bitmap != null)
                        {
                            double mw = 0, mh = 0;
                            ImageUtility.GetResizeImageParameters(bitmap.Width, bitmap.Height, imageModel.FeedImageInfoModel.ReWidth, imageModel.FeedImageInfoModel.ReHeight, out mw, out mh);
                            MemoryStream ms = new MemoryStream();
                            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            bitmapImage = new BitmapImage();
                            bitmapImage.BeginInit();
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapImage.StreamSource = ms;
                            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                            bitmapImage.DecodePixelHeight = (int)mh;
                            bitmapImage.DecodePixelWidth = (int)mw;
                            bitmapImage.EndInit();
                            GC.SuppressFinalize(bitmapImage);
                            ms = null;
                            bitmap.Dispose();
                            bitmap = null;
                            if (!string.IsNullOrEmpty(imageModel.MoreImagesCount))
                            {
                                imageModel.OnPropertyChanged("MoreImagesCount");
                            }
                        }
                        else if (!string.IsNullOrEmpty(imageModel.ImageUrl))
                        {
                            if (ServerAndPortSettings.GetImageServerResourceURL != null)
                            {
                                string imgUrl = HelperMethods.ConvertUrlToName(imageModel.ImageUrl, ImageUtility.IMG_600);
                                FeedImage obj = new FeedImage(imageModel.ImageId, imgUrl, imageModel.NewsFeedId, imageModel.ImageType);
                                obj.Start();
                            }
                            bitmapImage = ImageObjects.PHOTO_LOADING;
                            GC.SuppressFinalize(bitmapImage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
            return bitmapImage;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //public class FeedImageConverter : IMultiValueConverter
    //{
    //    private static readonly ILog log = LogManager.GetLogger(typeof(FeedImageConverter).Name);
    //    public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        BitmapImage bitmapImage = null;
    //        try
    //        {
    //            if (value[0] is ImageModel && value[1] is FeedModel && parameter is int)
    //            {
    //                ImageModel imageModel = (ImageModel)value[0];
    //                string imageUrl = imageModel.ImageUrl;
    //                FeedModel feedModel = (FeedModel)value[1];
    //                int width = (int)parameter;

    //                if (imageModel.NoImageFound)
    //                {
    //                    bitmapImage = ImageObjects.NO_IMAGE_FOUND;
    //                }
    //                else
    //                {

    //                    if (!String.IsNullOrEmpty(imageUrl) && imageUrl.Contains("/"))
    //                    {
    //                        string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_600);

    //                        convertedUrl = convertedUrl.Replace("/", "_");

    //                        if (imageModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
    //                        {
    //                            convertedUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
    //                        }
    //                        else if (imageModel.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
    //                        {
    //                            convertedUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
    //                        }
    //                        else
    //                        {
    //                            convertedUrl = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
    //                        }

    //                        Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(convertedUrl);

    //                        if (bitmap != null)
    //                        {
    //                            if (imageModel.FeedImageInfoModel.Width < (bitmap.Width + 10) || imageModel.FeedImageInfoModel.Height < (bitmap.Height + 10))
    //                            {
    //                                imageModel.FeedImageInfoModel.Width = bitmap.Width;
    //                                imageModel.FeedImageInfoModel.Height = bitmap.Height;
    //                                ScaleFeedImages scaler = new ScaleFeedImages(feedModel.FeedImageList, feedModel.ActualTime, width);
    //                                if (feedModel.ImageViewHeight == 0 || feedModel.FeedImageList.Count == 1)
    //                                    feedModel.ImageViewHeight = scaler.ImageViewHeight;

    //                                imageModel.OnPropertyChanged("FeedImageInfoModel");
    //                            }
    //                            //bitmapImage = ImageUtility.GetResizedImageForFeed(bitmap, imageModel.FeedImageInfoModel.ReWidth, imageModel.FeedImageInfoModel.ReHeight);  //ImageUtility.ConvertBitmapToBitmapImage(bitmap);
    //                            int mw = 0, mh = 0;
    //                            ImageUtility.GetResizeImageParameters(bitmap.Width, bitmap.Height, imageModel.FeedImageInfoModel.ReWidth, imageModel.FeedImageInfoModel.ReHeight, out mw, out mh);
    //                            MemoryStream ms = new MemoryStream();
    //                            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
    //                            bitmapImage = new BitmapImage();
    //                            bitmapImage.BeginInit();
    //                            ms.Seek(0, SeekOrigin.Begin);
    //                            bitmapImage.StreamSource = ms;
    //                            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
    //                            bitmapImage.DecodePixelHeight = mh;
    //                            bitmapImage.DecodePixelWidth = mw;
    //                            bitmapImage.EndInit();
    //                            ms = null;
    //                            bitmap.Dispose();
    //                            bitmap = null;
    //                            if (!string.IsNullOrEmpty(imageModel.MoreImagesCount))
    //                            {
    //                                imageModel.OnPropertyChanged("MoreImagesCount");
    //                            }
    //                        }

    //                        if (bitmapImage == null)
    //                        {
    //                            if (!String.IsNullOrEmpty(imageModel.ImageUrl))
    //                            {
    //                                imageUrl = HelperMethods.ConvertUrlToName(imageModel.ImageUrl, ImageUtility.IMG_600);
    //                                if (ServerAndPortSettings.GetImageServerResourceURL != null)
    //                                {
    //                                    FeedImage obj = new FeedImage(imageModel.ImageId, imageUrl, imageModel.NewsFeedId, imageModel.ImageType);
    //                                    obj.Start();
    //                                }
    //                                //bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
    //                                bitmapImage = ImageObjects.PHOTO_LOADING;
    //                                GC.SuppressFinalize(bitmapImage);
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
    //        }
    //        return bitmapImage;
    //    }

    //    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class BreakingNewsFeedImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            if (value is FeedModel)
            {
                FeedModel feedModel = (FeedModel)value;
                //if (feedModel.Im.NoImageFound)
                //{
                //    bitmapImage = ImageObjects.NO_IMAGE_FOUND;
                //}
                //else
                //{
                //}
                int width = 180;
                int height = 176;

                string imageUrl = feedModel.PostOwner.ProfileImage;//PreviewImgUrl;
                if (string.IsNullOrEmpty(imageUrl)) return null;

                int index = imageUrl.LastIndexOf('/');
                imageUrl = imageUrl.Insert(index + 1, "p600");
                string filePathImage = RingIDSettings.TEMP_NEWSPORTAL_IMAGES_FOLDER + System.IO.Path.DirectorySeparatorChar + imageUrl.Replace("/", "_");
                if (File.Exists(filePathImage))
                {
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    bi.DecodePixelHeight = height;
                    bi.DecodePixelWidth = width;
                    bi.CacheOption = BitmapCacheOption.OnLoad;
                    bi.UriSource = new Uri(filePathImage);
                    bi.EndInit();
                    GC.SuppressFinalize(bi);
                    return bi;
                }
                else new FeedImage(feedModel, imageUrl, filePathImage, true).Start();
                //return ImageLocation.NEWSPORTAL_DEFAULT_IMAGE;
            }
            else if (value is ImageModel)
            {
                ImageModel imageModel = (ImageModel)value;
                double width = 600;
                double height = 320;
                double ratio = 0;
                double resizedWidth = 0;
                double resizedHeight = 0;
                string imageUrl = imageModel.ImageUrl;
                if (parameter != null)
                {
                    width = height = 600;
                }
                if (!string.IsNullOrEmpty(imageUrl))
                {
                    int index = imageUrl.LastIndexOf('/');
                    imageUrl = imageUrl.Insert(index + 1, "p600");
                    string filePathImage = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + imageUrl.Replace("/", "_");
                    if (File.Exists(filePathImage))
                    {
                        //BitmapImage bi = new BitmapImage();
                        //bi.BeginInit();
                        // bi.DecodePixelHeight = height;
                        //bi.DecodePixelWidth = width;
                        Bitmap bi = ImageUtility.GetBitmapOfDynamicResource(filePathImage);
                        if (bi.Height >= height)
                        {
                            resizedHeight = height;
                            ratio = bi.Height / height;
                            resizedWidth = bi.Width / ratio;
                        }
                        else if (bi.Width >= width)
                        {
                            resizedWidth = width;
                            ratio = bi.Width / width;
                            resizedHeight = bi.Height / ratio;
                        }
                        else if (bi.Height < height && bi.Width < width)
                        {
                            resizedWidth = bi.Width;
                            resizedHeight = bi.Height;
                        }
                        MemoryStream ms = new MemoryStream();
                        bi.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        bitmapImage = new BitmapImage();
                        bitmapImage.BeginInit();
                        ms.Seek(0, SeekOrigin.Begin);
                        bitmapImage.StreamSource = ms;
                        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                        bitmapImage.DecodePixelHeight = System.Convert.ToInt32(resizedHeight);
                        bitmapImage.DecodePixelWidth = System.Convert.ToInt32(resizedWidth);
                        bitmapImage.EndInit();
                        ms = null;
                        bi.Dispose();
                        bi = null;
                        /*Bitmap newImage = new Bitmap((int)resizedWidth, (int)resizedHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                        using (Graphics graphics = Graphics.FromImage(newImage))
                        {
                            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                            graphics.DrawImage(bi, 0, 0, (int)resizedWidth, (int)resizedHeight);
                        }
                        bi.Dispose();
                        return ImageUtility.ConvertBitmapToBitmapImage(newImage);*/

                        /*bi.CacheOption = BitmapCacheOption.OnLoad;
                        bi.UriSource = new Uri(filePathImage);
                        bi.EndInit();
                        GC.SuppressFinalize(bi);
                        return bi;*/
                    }
                    else new FeedImage(imageModel.ImageId, imageUrl.Replace("/", "_"), imageModel.NewsFeedId, imageModel.ImageType).Start();
                }
                else return ImageObjects.NO_IMAGE_FOUND;
            }
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //public class FeedImagesVisibilityConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (value == null) return Visibility.Collapsed;
    //        else if (value is List<ImageModel>)
    //        {
    //            List<ImageModel> list = (List<ImageModel>)value;
    //            int num = System.Convert.ToInt32(parameter);
    //            if (num <= list.Count)
    //            {
    //                return Visibility.Visible;
    //                //
    //            }
    //            else
    //            {
    //                return Visibility.Collapsed;
    //            }
    //        }
    //        return Visibility.Collapsed;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
    public class EditDeleteVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value == null || value.Length == 0) return Visibility.Collapsed;
                if (value[0] is string && value[1] is long)
                {
                    long posterUtid = System.Convert.ToInt64(value[1]);
                    long placeOwnerUtId = (value[2] is long) ? (long)value[2] : 0;
                    if (posterUtid == DefaultSettings.LOGIN_TABLE_ID || placeOwnerUtId == DefaultSettings.LOGIN_TABLE_ID) return Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        //public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        //{
        //    if (value == null) return Visibility.Collapsed;
        //    else if (value is FeedModel)
        //    {
        //        FeedModel model = (FeedModel)value;
        //        if (model.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID || (model.FriendShortInfoModel != null && model.FriendShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID))
        //            return Visibility.Visible;
        //    }
        //    else if (value is CommentModel)
        //    {
        //        CommentModel model = (CommentModel)value;
        //        if (model.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID) return Visibility.Visible;
        //        if (model.PostOwnerUid == DefaultSettings.LOGIN_USER_ID) return Visibility.Visible;
        //        else return Visibility.Collapsed;
        //    }
        //    else if (value is bool)
        //    {
        //        bool isEditMode = (bool)value;
        //        int num = System.Convert.ToInt32(parameter);
        //        if (num == 1) //showpanel
        //        {
        //            return isEditMode ? Visibility.Collapsed : Visibility.Visible;
        //        }
        //        else //editpanel
        //        {
        //            return isEditMode ? Visibility.Visible : Visibility.Collapsed;
        //        }

        //    }
        //    return Visibility.Collapsed;
        //}

        //public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        //{
        //    throw new NotImplementedException();
        //}
    }
    public class PrevCommentsVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value != null && value.Length > 0)
                {
                    int feedCC = (value[2] != null && value[2] is int) ? (int)value[2] : 0;

                    if (value[3] != null && value[3] is bool && (bool)value[3])
                    {
                        return Visibility.Collapsed;
                    }
                    if (value[0] != null && value[0] is long) //model.SingleImageFeedModel.NumberOfComments
                    {
                        long singleCC = (long)value[0];
                        return (singleCC != feedCC) ? Visibility.Visible : Visibility.Collapsed;
                    }
                    else if (value[1] != null && value[1] is short) //model.SingleMDaFeedModel.NumberOfComments
                    {
                        short singleCC = (short)value[1];
                        return (singleCC != feedCC) ? Visibility.Visible : Visibility.Collapsed;
                    }
                    long mainCC = (value[4] != null && value[4] is long) ? (long)value[4] : 0;
                    return (mainCC != feedCC) ? Visibility.Visible : Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return Visibility.Visible;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    /* public class PrevCommentsVisibilityConverter : IValueConverter
     {
         public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
         {
             if (value == null) return Visibility.Collapsed;
             else if (value is FeedModel)
             {
                 FeedModel model = (FeedModel)value;
                 if (model.SingleMediaFeedModel != null)
                 {
                     if (model.CommentList.Count != model.SingleMediaFeedModel.CommentCount) return Visibility.Visible;
                     else return Visibility.Collapsed;
                 }
                 else if (model.SingleImageFeedModel != null)
                 {
                     if (model.CommentList.Count != model.SingleImageFeedModel.NumberOfComments) return Visibility.Visible;
                     else return Visibility.Collapsed;
                 }
                 if (model.NoMoreComments) return Visibility.Collapsed;
                 if (model.CommentList.Count != model.NumberOfComments) return Visibility.Visible;
             }
             return Visibility.Collapsed;
         }

         public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
         {
             throw new NotImplementedException();
         }
     } */
    public class LoaderVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return Visibility.Collapsed;
            else if (value is int)
            {
                int LoadState = (int)value;
                int num = System.Convert.ToInt32(parameter);
                // return (LoadState==num)? 
                if (num == 0)
                {
                    return (LoadState == 0) ? Visibility.Collapsed : Visibility.Visible;
                }
                else if (num == 1)
                {
                    return (LoadState == 1) ? Visibility.Visible : Visibility.Collapsed;
                }
                else if (num == 2)
                {
                    return (LoadState == 2) ? Visibility.Visible : Visibility.Collapsed;
                }
                else
                {
                    return (LoadState == 3) ? Visibility.Visible : Visibility.Collapsed;
                }

            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class PathToFeedCommentImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            if (value is CommentModel)
            {
                CommentModel commentModel = (CommentModel)value;
                string imageUrl = commentModel.MediaOrImageUrl;

                if (!String.IsNullOrWhiteSpace(imageUrl))
                {
                    string newImageUrl, convertedUrl;
                    if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_STICKER)
                    {
                        convertedUrl = imageUrl;
                        newImageUrl = RingIDSettings.STICKER_FOLDER + Path.DirectorySeparatorChar + convertedUrl.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                    }
                    else
                    {
                        convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_300);
                        newImageUrl = RingIDSettings.TEMP_COMMENT_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
                    }
                    bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);
                    if (bitmapImage == null)
                    {
                        CommentImage obj = new CommentImage(commentModel, convertedUrl);
                        obj.Start();
                    }
                }
                else
                {
                    bitmapImage = ImageObjects.UNKNOWN_IMAGE_LARGE;
                }
            }

            if (bitmapImage != null) GC.SuppressFinalize(bitmapImage);
            return bitmapImage;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StickerUrlToFilePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                string stickerPath = @RingIDSettings.STICKER_FOLDER + Path.DirectorySeparatorChar + ((string)value).Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                return stickerPath;
            }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PathToMediaCommentImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            if (value is CommentModel)
            {
                CommentModel commentModel = (CommentModel)value;

                if (string.IsNullOrEmpty(commentModel.ThumbUrl))
                {
                    return commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC ? ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE : ImageObjects.DEFAULT_VIDEO_ALBUM_IMAGE;
                }
                else
                {
                    string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + commentModel.ThumbUrl.Replace("/", "_");
                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                    if (File.Exists(filePathImage))
                    {
                        byte[] imageFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                        if (imageFileBytes != null && imageFileBytes.Length > 1)
                        {
                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(imageFileBytes);
                            GC.SuppressFinalize(BmpImg);
                            return BmpImg;
                        }

                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                        //imageBytes = null;
                        //GC.SuppressFinalize(BmpImg);
                        //return BmpImg;
                    }
                    else
                    {
                        new MediaImage(commentModel, filePathImage).Start();
                    }
                }
            }
            else if (value is string)
            {
                if (!string.IsNullOrEmpty(value.ToString()))
                {
                    string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + value.ToString().Replace("/", "_");
                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                    if (File.Exists(filePathImage))
                    {
                        byte[] oFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                        if (oFileBytes != null && oFileBytes.Length > 1)
                        {
                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(oFileBytes);
                            GC.SuppressFinalize(BmpImg);
                            return BmpImg;
                        }
                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                        //imageBytes = null;
                        //GC.SuppressFinalize(BmpImg);
                        //return BmpImg;
                    }
                }
            }
            if (bitmapImage != null) GC.SuppressFinalize(bitmapImage);
            return bitmapImage;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FeedUploadImageSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is ImageUploaderModel)
            {
                ImageUploaderModel imgUpModel = (ImageUploaderModel)value;
                BitmapImage bitmapsource = null;
                if (ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.TryGetValue(imgUpModel.FilePath, out bitmapsource))
                {
                    GC.SuppressFinalize(bitmapsource);
                    GC.SuppressFinalize(ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[imgUpModel.FilePath]);
                    ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[imgUpModel.FilePath] = null;
                    ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.Remove(imgUpModel.FilePath);
                    return bitmapsource;
                }
                else
                {
                    if (PreviewImageLoadUtility.Instance == null)
                    {
                        PreviewImageLoadUtility.Instance = new PreviewImageLoadUtility();
                    }
                    PreviewImageLoadUtility.Instance.LoadSingleImageUploaderModel(imgUpModel);
                    //return loader;
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class FeedUploadAudioImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is MusicUploaderModel)
            {
                MusicUploaderModel model = (MusicUploaderModel)value;
                if (!string.IsNullOrEmpty(model.ThumbUrl))
                {
                    string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + model.ThumbUrl.Replace("/", "_");
                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                    if (File.Exists(filePathImage))
                    {
                        byte[] oFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                        if (oFileBytes != null && oFileBytes.Length > 1)
                        {
                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(oFileBytes);
                            GC.SuppressFinalize(BmpImg);
                            return BmpImg;
                        }
                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                        //imageBytes = null;
                        //GC.SuppressFinalize(BmpImg);
                        //return BmpImg;
                    }
                }
                else if (model.ContentId == Guid.Empty)
                {
                    if (model.IsImageFound)
                    {
                        TagLib.File file = TagLib.File.Create(model.FilePath);
                        BitmapImage bmp = ImageUtility.ConvertByteArrayToBitmapImage(file.Tag.Pictures[0].Data.Data);
                        file.Dispose();
                        GC.SuppressFinalize(bmp);
                        model.ImageHeight = (int)bmp.Height;
                        model.ImageWidth = (int)bmp.Width;
                        return bmp;
                    }
                }
            }
            else if (value is FeedVideoUploaderModel)
            {
                FeedVideoUploaderModel model = (FeedVideoUploaderModel)value;
                //if (model.ContentId == 0)
                //{
                //    if (model.IsImageFound)
                //    {
                //        TagLib.File file = TagLib.File.Create(model.FilePath);
                //        BitmapImage bmp = ImageUtility.convertByteArrayToBitmapImage(file.Tag.Pictures[0].Data.Data);
                //        file.Dispose();
                //        GC.SuppressFinalize(bmp);
                //        model.ImageHeight = (int)bmp.Height;
                //        model.ImageWidth = (int)bmp.Width;
                //        return bmp;
                //    }
                //}
                //if (model.VideoThumbnail == null)
                //{
                //}
                if (!string.IsNullOrEmpty(model.ThumbUrl))
                {
                    string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + model.ThumbUrl.Replace("/", "_");
                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                    if (File.Exists(filePathImage))
                    {
                        byte[] oFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                        if (oFileBytes != null && oFileBytes.Length > 1)
                        {
                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(oFileBytes);
                            GC.SuppressFinalize(BmpImg);
                            return BmpImg;
                        }
                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                        //imageBytes = null;
                        //GC.SuppressFinalize(BmpImg);
                        //return BmpImg;
                    }
                }
                else
                    return ImageLocation.DEFAULT_VIDEO_IMAGE;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class UploadImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is CommonUploaderModel)
            {
                CommonUploaderModel model = (CommonUploaderModel)value;
                if (!string.IsNullOrEmpty(model.UploadedThumbUrl))
                {
                    string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + model.UploadedThumbUrl.Replace("/", "_");
                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                    if (File.Exists(filePathImage))
                    {
                        byte[] oFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                        if (oFileBytes != null && oFileBytes.Length > 1)
                        {
                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(oFileBytes);
                            GC.SuppressFinalize(BmpImg);
                            return BmpImg;
                        }
                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                        //imageBytes = null;
                        //GC.SuppressFinalize(BmpImg);
                        //return BmpImg;
                    }
                }
                else if (model.IsThumbImageFound)
                {
                    TagLib.File file = TagLib.File.Create(model.FilePath);
                    BitmapImage bmp = ImageUtility.ConvertByteArrayToBitmapImage(file.Tag.Pictures[0].Data.Data);
                    file.Dispose();
                    GC.SuppressFinalize(bmp);
                    model.FileHeight = (int)bmp.Height;
                    model.FileWidth = (int)bmp.Width;
                    return bmp;
                }
                else
                {
                    if (model.UploadType == SettingsConstants.UPLOADTYPE_MUSIC)
                    {
                        return ImageLocation.DEFAULT_AUDIO_IMAGE;
                    }
                    else if (model.UploadType == SettingsConstants.UPLOADTYPE_VIDEO)
                    {
                        return ImageLocation.DEFAULT_VIDEO_IMAGE;
                    }
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class DurationTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = "";
            int durationType = parameter != null ? Int32.Parse(parameter.ToString()) : 0;
            if (value is long)
            {
                long d = durationType == 0 ? (long)value : ((long)value) / 1000;
                int seconds = (int)d % 60;
                int minutes = (int)d / 60;
                int hours = (int)d / (60 * 60);
                minutes = minutes - (hours * 60);
                string s = (seconds < 10) ? ("0" + seconds) : ("" + seconds);
                string m = (minutes < 10) ? ("0" + minutes) : ("" + minutes);
                string h = (hours < 10) ? ("0" + hours) : ("" + hours);
                if (hours > 0)
                {
                    str = h + ":" + m + ":" + s;
                }
                else
                {
                    str = m + ":" + s;
                }
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TimeTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = "";
            if (value is double)
            {
                bool isNegative = false;
                double d = (double)value;
                if (d < 0)
                {
                    isNegative = true;
                    d = -1 * d;
                }

                int seconds = (int)d % 60;
                int minutes = (int)d / 60;
                int hours = (int)d / (60 * 60);
                minutes = minutes - (hours * 60);
                string s = (seconds < 10) ? ("0" + seconds) : ("" + seconds);
                string m = (minutes < 10) ? ("0" + minutes) : ("" + minutes);
                string h = (hours < 10) ? ("0" + hours) : ("" + hours);

                if (hours > 0)
                {
                    str = h + ":" + m + ":" + s;
                }
                else
                {
                    str = m + ":" + s;
                }
                if (isNegative)
                {
                    str = "-" + str;
                }
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NewStatusUploadLinkImageSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value is string && !string.IsNullOrEmpty((string)value))
                {
                    var fullFilePath = (string)value;
                    if (!fullFilePath.StartsWith("http"))
                    {
                        fullFilePath = "http:" + fullFilePath;
                    }
                    //int cntTp = 0;
                    //HelperMethods.GetURLData(fullFilePath, out cntTp);
                    //////int imgType = ImageUtility.GetFileImageTypeFromHeader(null, ImageUtility.ConvertBitmapImageToByteArray(bitmap));
                    //if (cntTp == 2)
                    //{
                    BitmapImage bitmap = new BitmapImage();
                    GC.SuppressFinalize(bitmap);
                    bitmap.BeginInit();
                    //bitmap.DecodePixelHeight = 150;
                    //bitmap.DecodePixelWidth = 150;
                    bitmap.UriSource = new Uri(fullFilePath, UriKind.Absolute);
                    bitmap.CacheOption = BitmapCacheOption.OnLoad;
                    bitmap.EndInit();
                    return bitmap;// ImageUtility.GetResizedImage(bitmap, 150, 150);
                    //}
                    //else
                    //{
                    //    BitmapSource img = BitmapImage.Create(
                    //    2,
                    //    2,
                    //    96,
                    //    96,
                    //    System.Windows.Media.PixelFormats.Indexed1,
                    //    new BitmapPalette(new List<System.Windows.Media.Color> { System.Windows.Media.Colors.BlanchedAlmond }),
                    //    new byte[] { 0, 0, 0, 0 },
                    //    1);
                    //    return img;
                    //}
                }
                else if (value is LinkInformationModel)
                {
                    LinkInformationModel model = (LinkInformationModel)value;
                    if (!string.IsNullOrEmpty(model.PreviewImgUrl))
                    {
                        string UrlasfileName = null;
                        if (model.PreviewImgUrl.StartsWith("http://"))
                        {
                            UrlasfileName = model.PreviewImgUrl.Substring(7);
                        }
                        else if (model.PreviewImgUrl.StartsWith("https://"))
                        {
                            UrlasfileName = model.PreviewImgUrl.Substring(8);
                        }
                        else UrlasfileName = model.PreviewImgUrl;
                        if (UrlasfileName.StartsWith("www."))
                        {
                            UrlasfileName = UrlasfileName.Substring(4);
                        }
                        UrlasfileName = UrlasfileName.Replace("/", "_");
                        string invalid = new string(Path.GetInvalidFileNameChars());
                        foreach (char c in invalid)
                        {
                            UrlasfileName = UrlasfileName.Replace(c.ToString(), "");
                        }
                        if (UrlasfileName.Length > 120) UrlasfileName = UrlasfileName.Substring(0, 110);
                        string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + UrlasfileName;
                        if (File.Exists(filePathImage))
                        {
                            /*byte[] imageBytes = File.ReadAllBytes(filePathImage);
                            BitmapImage bitmapImage = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                            imageBytes = null;
                            return bitmapImage;*/
                            BitmapImage bi = new BitmapImage();
                            bi.BeginInit();
                            //bi.DecodePixelHeight = 150;
                            //bi.DecodePixelWidth = 150;
                            bi.CacheOption = BitmapCacheOption.OnLoad;
                            bi.UriSource = new Uri(filePathImage);
                            bi.EndInit();
                            GC.SuppressFinalize(bi);
                            return bi;
                        }
                        else
                        {
                            //int cntTp = 0;
                            //string st = (model.PreviewImgUrl.StartsWith("http")) ? model.PreviewImgUrl : "http://" + model.PreviewImgUrl;
                            //HelperMethods.GetURLData(st, out cntTp);
                            //if (cntTp == 2)
                            new MediaImage(model, filePathImage).Start();
                            //else return BitmapImage.Create(
                            //       2,
                            //       2,
                            //       96,
                            //       96,
                            //       System.Windows.Media.PixelFormats.Indexed1,
                            //       new BitmapPalette(new List<System.Windows.Media.Color> { System.Windows.Media.Colors.BlanchedAlmond }),
                            //       new byte[] { 0, 0, 0, 0 },
                            //       1);
                        }
                    }
                    else
                    {
                        return (model.PreviewLinkType == 1) ? ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE : ImageObjects.DEFAULT_VIDEO_ALBUM_IMAGE;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //public class LocationModelToImageConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (value is LocationModel)
    //        {
    //            try
    //            {
    //                LocationModel model = (LocationModel)value;
    //                if (!string.IsNullOrEmpty(model.imgUrl) && (model.lat != 0 || model.lon != 0))
    //                {
    //                    string mapFilePath = RingIDSettings.TEMP_MAP_FOLDER + System.IO.Path.DirectorySeparatorChar + Math.Round(model.lat, 3) + "_" + Math.Round(model.lon, 3) + ".png";
    //                    if (File.Exists(mapFilePath))
    //                    {
    //                        /*byte[] imageBytes = File.ReadAllBytes(mapFilePath);
    //                        BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
    //                        imageBytes = null;
    //                        return BmpImg;*/
    //                        BitmapImage bi = new BitmapImage();
    //                        bi.BeginInit();
    //                        bi.DecodePixelHeight = 170;
    //                        bi.DecodePixelWidth = 600;
    //                        bi.CacheOption = BitmapCacheOption.OnLoad;
    //                        bi.UriSource = new Uri(mapFilePath);
    //                        bi.EndInit();
    //                        GC.SuppressFinalize(bi);
    //                        return bi;
    //                    }
    //                    else
    //                    {
    //                        DefaultSettings.FEED_STATIC_MAP_HIT++;
    //                        System.Diagnostics.Debug.WriteLine("DefaultSettings.FEED_STATIC_MAP_HIT==>" + DefaultSettings.FEED_STATIC_MAP_HIT);
    //                        new MapImage(model, mapFilePath).Start();
    //                    }
    //                }
    //            }
    //            catch (Exception)
    //            {
    //            }
    //        }
    //        else if (value is SingleMediaModel)
    //        {
    //            try
    //            {
    //                SingleMediaModel singleMediaModel = (SingleMediaModel)value;
    //                if (string.IsNullOrEmpty(singleMediaModel.ThumbUrl) || singleMediaModel.NoImageFound)
    //                {
    //                    return (singleMediaModel.MediaType == 1) ? ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE : ImageObjects.DEFAULT_VIDEO_ALBUM_IMAGE;
    //                }
    //                else
    //                {
    //                    string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + singleMediaModel.ThumbUrl.Replace("/", "_");
    //                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
    //                    if (File.Exists(filePathImage))
    //                    {
    //                        byte[] oFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
    //                        if (oFileBytes != null && oFileBytes.Length > 1)
    //                        {
    //                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(oFileBytes);
    //                            GC.SuppressFinalize(BmpImg);
    //                            return BmpImg;
    //                        }
    //                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
    //                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
    //                        //imageBytes = null;
    //                        //GC.SuppressFinalize(BmpImg);
    //                        //return BmpImg;
    //                    }
    //                    else
    //                    {
    //                        new MediaImage(singleMediaModel, filePathImage).Start();
    //                    }
    //                }
    //            }
    //            catch (Exception)
    //            {
    //            }
    //        }
    //        else if (value is NewsPortalModel)
    //        {
    //            try
    //            {
    //                NewsPortalModel newsPortalModel = (NewsPortalModel)value;
    //                if (string.IsNullOrEmpty(newsPortalModel.ProfileImage))
    //                {
    //                    return ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE;
    //                }
    //                else
    //                {
    //                    string filePathImage = RingIDSettings.TEMP_NEWSPORTAL_IMAGES_FOLDER + Path.DirectorySeparatorChar + newsPortalModel.ProfileImage.Replace("/", "_");
    //                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
    //                    if (File.Exists(filePathImage))
    //                    {
    //                        byte[] oFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
    //                        if (oFileBytes != null && oFileBytes.Length > 1)
    //                        {
    //                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(oFileBytes);
    //                            GC.SuppressFinalize(BmpImg);
    //                            return BmpImg;
    //                        }

    //                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
    //                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
    //                        //imageBytes = null;
    //                        //GC.SuppressFinalize(BmpImg);
    //                        //return BmpImg;
    //                    }
    //                    else
    //                    {
    //                        new MediaImage(newsPortalModel, filePathImage).Start();
    //                    }
    //                }
    //            }
    //            catch (Exception)
    //            {
    //            }
    //        }
    //        else if (value is PageInfoModel)
    //        {
    //            try
    //            {
    //                PageInfoModel pageInfoModel = (PageInfoModel)value;
    //                if (string.IsNullOrEmpty(pageInfoModel.ProfileImage))
    //                {
    //                    return ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE;
    //                }
    //                else
    //                {
    //                    string filePathImage = RingIDSettings.TEMP_PAGES_IMAGES_FOLDER + Path.DirectorySeparatorChar + pageInfoModel.ProfileImage.Replace("/", "_");
    //                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
    //                    if (File.Exists(filePathImage))
    //                    {
    //                        byte[] imageFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
    //                        if (imageFileBytes != null && imageFileBytes.Length > 1)
    //                        {
    //                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(imageFileBytes);
    //                            GC.SuppressFinalize(BmpImg);
    //                            return BmpImg;
    //                        }

    //                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
    //                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
    //                        //imageBytes = null;
    //                        //GC.SuppressFinalize(BmpImg);
    //                        //return BmpImg;
    //                    }
    //                    else
    //                    {
    //                        new MediaImage(pageInfoModel, filePathImage).Start();
    //                    }
    //                }
    //            }
    //            catch (Exception)
    //            {
    //            }
    //        }
    //        else if (value is MusicPageModel)
    //        {
    //            try
    //            {
    //                MusicPageModel musicPageModel = (MusicPageModel)value;
    //                if (string.IsNullOrEmpty(musicPageModel.ProfileImage))
    //                {
    //                    return ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE;
    //                }
    //                else
    //                {
    //                    string filePathImage = RingIDSettings.TEMP_MEDIAPAGE_IMAGES_FOLDER + Path.DirectorySeparatorChar + musicPageModel.ProfileImage.Replace("/", "_");
    //                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
    //                    if (File.Exists(filePathImage))
    //                    {
    //                        byte[] imageFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
    //                        if (imageFileBytes != null && imageFileBytes.Length > 1)
    //                        {
    //                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(imageFileBytes);
    //                            GC.SuppressFinalize(BmpImg);
    //                            return BmpImg;
    //                        }

    //                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
    //                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
    //                        //imageBytes = null;
    //                        //GC.SuppressFinalize(BmpImg);
    //                        //return BmpImg;
    //                    }
    //                    else
    //                    {
    //                        new MediaImage(musicPageModel, filePathImage).Start();
    //                    }
    //                }
    //            }
    //            catch (Exception)
    //            {
    //            }
    //        }
    //        else if (value is CelebrityModel)
    //        {
    //            try
    //            {
    //                CelebrityModel celebrityModel = (CelebrityModel)value;
    //                if (string.IsNullOrEmpty(celebrityModel.ProfileImage))
    //                {
    //                    return ImageObjects.CELEBRITY_DEFAULT_IMAGE;
    //                }
    //                else
    //                {
    //                    string filePathImage = RingIDSettings.TEMP_CELEBRITY_IMAGES_FOLDER + Path.DirectorySeparatorChar + celebrityModel.ProfileImage.Replace("/", "_");
    //                    if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
    //                    if (File.Exists(filePathImage))
    //                    {
    //                        byte[] imageFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
    //                        if (imageFileBytes != null && imageFileBytes.Length > 1)
    //                        {
    //                            BitmapImage BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(imageFileBytes);
    //                            GC.SuppressFinalize(BmpImg);
    //                            return BmpImg;
    //                        }

    //                        //byte[] imageBytes = File.ReadAllBytes(filePathImage);
    //                        //BitmapImage BmpImg = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
    //                        //imageBytes = null;
    //                        //GC.SuppressFinalize(BmpImg);
    //                        //return BmpImg;
    //                    }
    //                    else
    //                    {
    //                        new MediaImage(celebrityModel, filePathImage).Start();
    //                    }
    //                }
    //            }
    //            catch (Exception)
    //            {
    //            }
    //        }
    //        return null;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class ImageViewerCaptionVisibilityConverter : IValueConverter
    {

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                string caption = value.ToString();

                return string.IsNullOrEmpty(caption) ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }

    public class VisibilityConverterForSingleFeedDetails : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            if (value == null)
            {
                return Visibility.Collapsed;
            }
            else
            {
                return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MediaModelTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //if (value is MediaContentModel)
            //{
            //    MediaContentModel model = (MediaContentModel)value;
            //    if (model.TotalMediaCount != 0)
            //    {
            //        if (model.MediaType == 1)
            //        {
            //            if (model.TotalMediaCount == 1)
            //            {
            //                return "Music";
            //            }
            //            else
            //            {
            //                return "Musics";
            //            }
            //        }
            //        else if (model.MediaType == 2)
            //        {
            //            if (model.TotalMediaCount == 1)
            //            {
            //                return "Video";
            //            }
            //            else
            //            {
            //                return "Videos";
            //            }
            //        }

            //    }
            //}
            //else 
            if (value is int)
            {
                int count = (int)value;
                if (count != 0)
                {
                    if (count == 1)
                    {
                        return "Item";
                    }
                    else
                    {
                        return "Items";
                    }

                }
            }
            //else if (value is MediaContentModel)
            //{
            //    MediaContentModel model = (MediaContentModel)value;
            //    if (model.TotalMediaCount != 0)
            //    {
            //        if (model.MediaType == 1)
            //        {
            //            if (model.TotalMediaCount == 1 || model.TotalMediaCount == 0)
            //            {
            //                return NotificationMessages.MEDIA_MUSIC;
            //            }
            //            else
            //            {
            //                return NotificationMessages.MEDIA_MUSICS;
            //            }
            //        }
            //        else if (model.MediaType == 2)
            //        {
            //            if (model.TotalMediaCount == 1 || model.TotalMediaCount == 0)
            //            {
            //                return NotificationMessages.MEDIA_VIDEO;
            //            }
            //            else
            //            {
            //                return NotificationMessages.MEDIA_VIDEOS;
            //            }
            //        }

            //    }
            //}
            return null;
        }
        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }

    //public class PrivacyEditConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (value == null) return Visibility.Collapsed;
    //        else if (value is FeedModel)
    //        {
    //            FeedModel model = (FeedModel)value;
    //            if ((model.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID && model.ParentFeed == null && model.FeedSubType == 0)
    //                /*|| (model.FriendShortInfoModel != null && model.FriendShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID)*/)
    //                return Visibility.Visible;
    //        }

    //        return Visibility.Collapsed;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    //public class LikeCommentShareColorConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (value is FeedModel && parameter is string) //like 1,comment 2 ,share 3
    //        {
    //            FeedModel model = (FeedModel)value;
    //            int type = System.Convert.ToInt32(parameter);
    //            switch (type)
    //            {
    //                case 1:
    //                    if (model.SingleImageFeedModel != null) return model.SingleImageFeedModel.ILike;
    //                    else if (model.SingleMediaFeedModel != null) return model.SingleMediaFeedModel.ILike;
    //                    else return model.ILike;
    //                case 2:
    //                    if (model.SingleImageFeedModel != null) return model.SingleImageFeedModel.IComment;
    //                    else if (model.SingleMediaFeedModel != null) return model.SingleMediaFeedModel.IComment;
    //                    else return model.IComment;
    //                case 3:
    //                    if (model.SingleMediaFeedModel != null) return model.SingleMediaFeedModel.IShare;
    //                    else return model.IShare;
    //                default:
    //                    break;
    //            }
    //        }
    //        return 0;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class LinkDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string val = (string)value;

            if (!string.IsNullOrEmpty(val))
            {
                val = val.Replace('\n', ' ');
                if (val.Length > 200)
                {
                    return val.Trim().Remove(200);
                }
                else
                    return val.Trim();
            }
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MultipleWrapPanelCommandParameterConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values.Clone();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NewsportalDomainNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                string uriString = (string)value;

                if (!string.IsNullOrEmpty(uriString))
                {
                    if (!uriString.StartsWith("http://", StringComparison.CurrentCultureIgnoreCase) && !uriString.StartsWith("https://", StringComparison.CurrentCultureIgnoreCase))
                    {
                        uriString = "http://" + uriString;
                    }
                    uriString = new Uri(uriString).Host;
                    return uriString;
                }
                else
                    return null;
            }
            catch (Exception)
            {

            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GreaterThanValueConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int numberOfElement = Int32.Parse((string)parameter);
            return (int)value > numberOfElement;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Dynamic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.AddFriend;
using View.UI.Profile.FriendProfile;
using View.UI.Settings;
using View.ViewModel;
using View.Utility.Circle;
using View.Utility.Chat;
using View.Utility;
using System.Windows.Media;

namespace View.Converter
{
    public class StatusTagConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long id = (long)value;
            int type = System.Convert.ToInt32(parameter);
            dynamic obj = new ExpandoObject();
            obj.id = id;
            obj.type = type;
            return obj;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TextToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return Visibility.Collapsed;
            if (value is string)
            {
                string str = (string)value;
                return string.IsNullOrEmpty(str) ? Visibility.Collapsed : Visibility.Visible;
            }
            else if (value is long)
            {
                long v = (long)value;
                return (v == 1) ? Visibility.Collapsed : Visibility.Visible;
            }
            else if (value is LocationModel)
            {
                LocationModel locationModel = (LocationModel)value;
                return string.IsNullOrEmpty(locationModel.LocationName) ? Visibility.Collapsed : Visibility.Visible;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class StringNotEmptyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return false;
            if (value is string)
            {
                string str = (string)value;
                return !string.IsNullOrEmpty(str);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class LinkToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return Visibility.Collapsed;
            if (value is string)
            {
                string str = (string)value;
                return string.IsNullOrEmpty(str) ? Visibility.Collapsed : Visibility.Visible;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CircleMembersConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NoCirclesVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return Visibility.Collapsed;
            int num = (int)value;
            return (num > 0) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CustomizedMsgBorderVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Thickness thickness = new Thickness(0, 0, 0, 1);

            if (value == null) return thickness;
            else if (value is InstantMsgModel)
            {
                InstantMsgModel model = (InstantMsgModel)value;
                if (UCCustomizeMessageSettings.Instance != null)
                {
                    InstantMsgModel lastModel = UCCustomizeMessageSettings.Instance.MyInstantMsgModelList.LastOrDefault();
                    if (lastModel.InstantMessage.Equals(model.InstantMessage))
                    {
                        thickness = new Thickness(0, 0, 0, 0);
                    }
                }
            }
            else if (value is CircleModel)
            {
                CircleModel CircleModel = (CircleModel)value;

                if ((VMCircle.Instance.CircleListYouManage.Count > 0 && CircleModel.UserTableID == VMCircle.Instance.CircleListYouManage.LastOrDefault().UserTableID)
                      || (VMCircle.Instance.CircleListYouAreIn.Count > 0 && CircleModel.UserTableID == VMCircle.Instance.CircleListYouAreIn.LastOrDefault().UserTableID))
                {
                    thickness = new Thickness(0, 0, 0, 0);
                }
            }
            else if (value is EditHistoryModel)
            {
                EditHistoryModel editHistoryModel = (EditHistoryModel)value;

                if ((MainSwitcher.PopupController.ucEditedHistoryView != null &&
                    MainSwitcher.PopupController.ucEditedHistoryView.EditHistories.Count > 0
                    && editHistoryModel.EditedId == MainSwitcher.PopupController.ucEditedHistoryView.EditHistories.LastOrDefault().EditedId))
                {
                    thickness = new Thickness(0, 0, 0, 0);
                }
            }

            return thickness;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class GroupListCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return RingIDViewModel.Instance != null && RingIDViewModel.Instance.GroupList != null ? RingIDViewModel.Instance.GroupList.Values.Where(P => !(P.IsPartial == false && P.AccessType == ChatConstants.MEMBER_TYPE_NOT_MEMBER)).ToList().Count : 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PendingBorderVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Thickness thickness = new Thickness(0, 1, 0, 0);
            if (value == null) return thickness;
            long utId = long.Parse(value.ToString());
            if (UCAddFriendPending.Instance != null)
            {
                if ((UCAddFriendPending.Instance.PendingListIncoming.Count > 0 && utId == UCAddFriendPending.Instance.PendingListIncoming.FirstOrDefault().ShortInfoModel.UserTableID)
                     || (UCAddFriendPending.Instance.PendingListOutgoing.Count > 0 && utId == UCAddFriendPending.Instance.PendingListOutgoing.FirstOrDefault().ShortInfoModel.UserTableID)
                     || (UCAddFriendPending.Instance.SearchPendingList.Count > 0 && utId == UCAddFriendPending.Instance.SearchPendingList.FirstOrDefault().ShortInfoModel.UserTableID))
                {
                    thickness = new Thickness(0, 0, 0, 0);
                }
            }
            return thickness;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GenderToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return "Select a Gender";
            string str = (string)value;
            if (string.IsNullOrEmpty(str)) return "Select a Gender";
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string)value;
        }
    }

    public class TextToLblConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return "";
            string str = (string)value;
            return string.IsNullOrEmpty(str) ? "N/A" : str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DateTimeToViewStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime dt = (DateTime)value;
            if (dt == DateTime.MinValue) return "N/A"; 
            else return dt.ToString("dd-MMM-yyyy");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DateTimeToDatePickerStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime dt = (DateTime)value;
            string str = (dt == DateTime.MinValue) ? null : dt.ToString("dd-MMM-yyyy");
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return DateTime.MinValue;
            else return value;
        }
    }

    //public class LongToViewStringConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        long v = (long)value;
    //        if (v == -1) { return ""; }
    //        if (v == 1 || v == 0) { return "N/A"; }
    //        else
    //        {
    //            DateTime dt = ModelUtility.DateTimeFromMillisSince1970(v);
    //            return dt.ToLongDateString().Replace(dt.DayOfWeek.ToString() + ", ", "");
    //        }
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    //public class LongToDatePickerStringConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        long v = (long)value;
    //        string str = (v == 1 || v == 0) ? null : ModelUtility.DateTimeFromMillisSince1970(v).ToShortDateString();
    //        return str;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (value == null) return 1;
    //        DateTime dt = (DateTime)value;
    //        return ModelUtility.MillisFromDateTimeSince1970(dt);
    //    }
    //}

    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility closeMode = Visibility.Hidden;
            if (parameter != null && parameter.ToString() == "1")
            {
                closeMode = Visibility.Collapsed;
            }
            return (bool)value ? Visibility.Visible : closeMode;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class InverseBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class CallFriendIDtoFullNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long utId = (long)value;
            //UserBasicInfoDTO dto = null;
            //if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(utId, out dto))
            //{
            UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(utId);
            if (dto != null)
            {
                return dto.FullName;
            }
            return utId.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GreaterThanOrLessThanZeroConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter != null && parameter.ToString() == "1")
            {
                return value is long ? (long)value < 0 : (int)value < 0;
            }
            else
            {
                return value is long ? (long)value > 0 : (int)value > 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GreaterThanOrLessThanOneConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (parameter != null && parameter.ToString() == "1")
            {
                return (double)value < 1;
            }
            else
            {
                return (double)value > 1;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IsNullOrWhiteSpaceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return String.IsNullOrWhiteSpace(value as String);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IsNotNullOrWhiteSpaceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !String.IsNullOrWhiteSpace(value as String);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MilisecondsToTimeSpanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long miliseconds = (long)value;
            return TimeSpan.FromMilliseconds(miliseconds);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ImageCursorVisibilityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((ImageModel)value).NoImageFound == false ? Cursors.Hand : Cursors.Arrow;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PrivacySettingsLastValueConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Thickness thickness = new Thickness(0, 0, 0, 1);
            int CurrentIndex = Int32.Parse(parameter.ToString());
            int LastIndex = RingIDViewModel.Instance._WNRingIDSettings._ComboboxList.LastOrDefault();
            if (LastIndex > 0 && CurrentIndex > 0)
            {
                if (CurrentIndex == LastIndex)
                {
                    thickness = new Thickness(0, 0, 0, 0);
                }
            }
            return thickness;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ErrorLabelVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (string.IsNullOrEmpty((string)value)) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class AlbumBorderConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Thickness thickness = new Thickness(0, 0, 0, 1);
            return thickness;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }

    public class ComapareBetweenString : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == null || values[1] == null)
                return false;

            string value1 = values[0].ToString();
            string value2 = values[1].ToString();

            if (String.IsNullOrWhiteSpace(value1) || String.IsNullOrWhiteSpace(value2))
                return false;

            return value1.Equals(value2);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IsLoginUserIDConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (long)value == DefaultSettings.LOGIN_TABLE_ID;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class UploadDownloadContentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = "";
            if (value is int && (int)value > 0)
            {
                int upCount = 0, downCount = 0;
                foreach (var model in RingIDViewModel.Instance.UploadingModelList)
                {
                    if (model.IsDownload)
                        downCount++;
                    else
                        upCount++;
                }
                if (downCount != 0 && upCount != 0 ) str = "Total Uploading: " + upCount + ", Total Downloading: " + downCount;
                else if (downCount == 0) str = "Total Uploading: " + upCount;
                else if (upCount == 0) str = "Total Downloading: " + downCount;

            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class UploadDownloadGifConverter : IValueConverter 
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int type = 0;
            if (value is int && (int)value > 0)
            {
                int upCount = 0, downCount = 0;
                foreach (var model in RingIDViewModel.Instance.UploadingModelList)
                {
                    if (model.IsDownload)
                        downCount++;
                    else
                        upCount++;
                }
                if (downCount != 0 && upCount != 0) type = 1 ;
                else if (downCount == 0) type=2 ;
                else if (upCount == 0) type = 3 ;
            }
            return type;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class WalletLongToDateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long ms = (long)value;
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dt2 = dt.AddSeconds(ms).ToLocalTime();

            return dt2.ToShortDateString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class WalletUtIDToRingIDConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //string ringID = string.Empty;
            string ringID = value.ToString();
            //long utID = (long)value;
            //if (utID <1)
            //{
            //    ringID = "My Coins";
            //}
            //else
            //{
            //    UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(utID);
            //    if (dto != null)
            //    {
            //        ringID = dto.RingID.ToString().Substring(2).Insert(4, " ");
            //    }
            //}
            return ringID.Substring(2).Insert(4, " ");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class WalletEarnCoinStyleConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            int ruleItemID = 0;
            ruleItemID = int.Parse(values[0].ToString());

            Style inviteStyle = values[1] as Style;
            Style checkInStyle = values[2] as Style;
            Style moreShareStyle = values[3] as Style;
            Style postButtonStyle = values[4] as Style;
            Style selectedStyle;
            switch (ruleItemID)
            {
                case WalletConstants.COIN_EARNING_RULE_INVITE_FRIENDS:
                    selectedStyle = inviteStyle;
                    break;
                case WalletConstants.COIN_EARNING_RULE_DAILY_CHECK_IN:
                    selectedStyle = checkInStyle;
                    break;
                case WalletConstants.COIN_EARNING_RULE_MORE_SHARE:
                    selectedStyle = moreShareStyle;
                    break;
                default:
                    selectedStyle = postButtonStyle;
                    break;
            }

            return selectedStyle;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //public class SelectedMediaConverter : IValueConverter
    //{

    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        string url = (string)value;
    //        bool isSelected = url == Utility.MainSwitcher.MediaPlayerController.VMMediaPlayer.SingleMediaModel.StreamUrl;
    //        //Utility.MainSwitcher.MediaPlayerController.VMMediaPlayer.OnPropertyChanged("CommentList");
    //        return isSelected;            
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    [ValueConversion(typeof(int), typeof(SolidColorBrush))]
    public class IntToBackgroundColor : IValueConverter
    {

        private static SolidColorBrush BRUSH_0 = new BrushConverter().ConvertFromString("#b54ef7") as SolidColorBrush;
        private static SolidColorBrush BRUSH_1 = new BrushConverter().ConvertFromString("#bad511") as SolidColorBrush;
        private static SolidColorBrush BRUSH_2 = new BrushConverter().ConvertFromString("#da449f") as SolidColorBrush;
        private static SolidColorBrush BRUSH_3 = new BrushConverter().ConvertFromString("#86c447") as SolidColorBrush;
        private static SolidColorBrush BRUSH_4 = new BrushConverter().ConvertFromString("#da4444") as SolidColorBrush;
        private static SolidColorBrush BRUSH_5 = new BrushConverter().ConvertFromString("#2eb081") as SolidColorBrush;
        private static SolidColorBrush BRUSH_6 = new BrushConverter().ConvertFromString("#ee762d") as SolidColorBrush;
        private static SolidColorBrush BRUSH_7 = new BrushConverter().ConvertFromString("#2eb0ae") as SolidColorBrush;
        private static SolidColorBrush BRUSH_8 = new BrushConverter().ConvertFromString("#6563ea") as SolidColorBrush;
        private static SolidColorBrush BRUSH_9 = new BrushConverter().ConvertFromString("#3496d6") as SolidColorBrush;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch ((int)(value ?? -1) % 10)
            {
                case 0:
                    return BRUSH_0;
                case 1:
                    return BRUSH_1;
                case 2:
                    return BRUSH_2;
                case 3:
                    return BRUSH_3;
                case 4:
                    return BRUSH_4;
                case 5:
                    return BRUSH_5;
                case 6:
                    return BRUSH_6;
                case 7:
                    return BRUSH_7;
                case 8:
                    return BRUSH_8;
                case 9:
                    return BRUSH_9;
                default:
                    return Brushes.LightGray;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public class LoadingStatusConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int status = StatusConstants.NO_DATA;

            if (values != null)
            {
                bool hasData = false;
                bool isLoading = false;

                foreach (object value in values)
                {
                    if (value is bool && (bool)value)
                    {
                        isLoading = true;
                    }
                    else if (value is int && (int)value > 0)
                    {
                        hasData = true;
                    }
                }

                if (hasData && isLoading) 
                {
                    status = StatusConstants.HAS_DATA_AND_LOADING;
                }
                else if (hasData)
                {
                    status = StatusConstants.HAS_DATA;
                }
                else if (isLoading)
                {
                    status = StatusConstants.NO_DATA_AND_LOADING;
                }
            }

            return status;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}

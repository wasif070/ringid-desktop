﻿using Models.Constants;
using Models.Stores;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.AddFriend;
using View.UI.Call;
using View.UI.Chat;
using View.UI.Dialer;
using View.UI.FriendList;
using View.UI.PopUp;
using View.Utility;
using View.Utility.FriendList;
using View.ViewModel;
namespace View.Converter
{
    class FriendBlockVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isVisible = false;
            if (value[0] is int)
            {
                int friendShipShatus = (int)value[0];
                int chatAccess = (int)value[1];
                int callAccess = (int)value[2];
                int feedAccess = (int)value[3];
                bool isBlockedByMe = (bool)value[4];

                if (friendShipShatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                {
                    isVisible = (callAccess == StatusConstants.TYPE_ACCESS_BLOCKED && chatAccess == StatusConstants.TYPE_ACCESS_BLOCKED && feedAccess == StatusConstants.TYPE_ACCESS_BLOCKED);
                }
                else
                {
                    isVisible = isBlockedByMe;
                }
            }
            return isVisible;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class FriendBlockTextConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = "Block";

            int friendShipShatus = (int)value[0];
            int chatAccess = (int)value[1];
            int callAccess = (int)value[2];
            int feedAccess = (int)value[3];
            bool isBlockedByMe = (bool)value[4];

            if (friendShipShatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            {
                if (callAccess == StatusConstants.TYPE_ACCESS_BLOCKED && chatAccess == StatusConstants.TYPE_ACCESS_BLOCKED && feedAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
                {
                    str = "Unblock";
                }
            }
            else
            {
                if (isBlockedByMe)
                {
                    str = "Unblock";
                }
            }

            return str;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class IsFriendSelectedinFriendListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Control ctrl = (Control)value;
            UserBasicInfoModel model = (UserBasicInfoModel)ctrl.DataContext;
            long selected = ctrl.Tag != null ? (long)ctrl.Tag : 0;
            return model.ShortInfoModel.UserTableID == selected;
            //if (value == null || parameter == null) return false;
            //long id = (long)value;
            //long selectedid = (long)parameter;
            //return (id == selectedid);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class FriendListSelectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long friendUserTableId = (long)value;
            int friendType = Int32.Parse(parameter.ToString());

            long selectedFriendUserTableId = 0;
            int selectedFriendType = 0;

            if (UCFriendList.Instance != null)
            {
                selectedFriendUserTableId = UCFriendList.Instance.SelectedFriendUserTableId;
                selectedFriendType = UCFriendList.Instance.SelectedFriendType;
            }

            return friendUserTableId == selectedFriendUserTableId && friendType == selectedFriendType;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class IsChatLogSelectedConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value[0].ToString().Equals(value[1].ToString());
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class IsCallLogSelectedConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value[0].Equals(value[1]) && value[2].Equals(value[3]);
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class DialedFriendListSelectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long selectedItem = UCDialer.Instance != null ? UCDialer.Instance.SelectedFriendUserTableId : 0;
            return (long)value == selectedItem;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class AddFriendListSelectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long selectedItem = UCAddFriendSearchPanel.Instance != null ? UCAddFriendSearchPanel.Instance.SelectedFriendUserTableId : 0;
            return (long)value == selectedItem;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FriendSearchListSelectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long selectedItem = UCGuiRingID.Instance.ucFriendSearchListPanel != null ? UCGuiRingID.Instance.ucFriendSearchListPanel.SelectedFriendUserTableId : 0;
            return (long)value == selectedItem;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class AddFriendSuggestionListSelectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long selectedItem = UCSuggestions.Instance != null ? UCSuggestions.Instance.SelectedFriendUserTableId : 0;
            return (long)value == selectedItem;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class AddFriendSuggestionListShowMoreVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visible = Visibility.Visible;

            if (RingIDViewModel.Instance.SuggestionFriendList.Count == 0)
            {
                visible = Visibility.Collapsed;
            }
            else
            {
                foreach (bool isVisible in FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Values)
                {
                    if (isVisible == false)
                    {
                        visible = Visibility.Visible;
                        break;
                    }
                    else
                    {
                        visible = Visibility.Collapsed;
                    }
                }
            }
            return visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    class AddFriendPendingListSelectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long selectedItem = UCAddFriendPending.Instance != null ? UCAddFriendPending.Instance.SelectedFriendUserTableId : 0;
            return (long)value == selectedItem;

            //int selectedItem = 0;
            //if (UCAddFriendPending.Instance.SelectedType == 0)
            //{
            //    selectedItem = 0;
            //}
            //else if (UCAddFriendPending.Instance.SelectedType == 1)
            //{
            //    selectedItem = 1;
            //}
            //else if (UCAddFriendPending.Instance.SelectedType == 2)
            //{
            //    selectedItem = 2;
            //}

            //return selectedItem;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class MutualFriendPopupTitleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                string str = (string)value;
                return str;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class AddFriendNumberOfMutualFriends : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = "";
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)value;

                if (model.NumberOfMutualFriends > 1)
                {
                    str = model.NumberOfMutualFriends + " mutual friends";
                }
                else
                {
                    str = model.NumberOfMutualFriends + " mutual friend";
                }
            }

            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class FriendNumberOfMutualFriends : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int mf = (int)value;
            string str = string.Empty;
            if (mf > 1)
            {
                str = mf + " mutual friends";
            }
            else
            {
                str = mf + " mutual friend";
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class FriendNameFirstPart : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = (string)value;
            string extra = parameter != null ? " " + parameter.ToString() + " " : " ";
            if (!string.IsNullOrEmpty(str))
            {
                string[] ssize = str.Split(null);
                str = ssize[0] + "'s" + extra + "Permission Settings";
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class FriendNameFirstPartChat : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = (string)value;
            if (!string.IsNullOrEmpty(str))
            {
                string[] ssize = str.Split(null);
                str = ssize[0] + "'s Chat Settings";
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class FriendContactTypeIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage image = null;
            UserBasicInfoModel model = (UserBasicInfoModel)value;
            bool isOpened = Boolean.Parse(parameter.ToString());
            if (model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            {
                image = isOpened ? ImageObjects.ID_SELECTED : ImageObjects.ID_DEFAULT;
            }
            else
            {
                image = isOpened ? ImageObjects.ID_SELECTED : ImageObjects.ADD_ICON_CONTACT_LIST;
            }
            return image;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class EducationCommaVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return Visibility.Collapsed;
            SingleEducationModel model = null;
            model = (SingleEducationModel)value;
            if (model == null) return Visibility.Collapsed;
            if (string.IsNullOrEmpty(model.EDegree) && string.IsNullOrEmpty(model.EConcentration)) return Visibility.Collapsed;
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FriendFavouriteCheckBoxConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double favouriteRank = (double)value;
            return favouriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE ? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //double favouriteRank = (double)value;
            return (bool)value ? -1 : 0;
        }
    }

    class FriendOnlineStatusIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage image = null;
            if (value is UserShortInfoModel)
            {
                UserShortInfoModel model = (UserShortInfoModel)value;
                image = ImageUtility.GetBitmapImage(HelperMethods.GetStatusIconLocation(model.Presence, model.Device, model.Mood, model.UserTableID));
            }
            else
            {
                image = ImageUtility.GetBitmapImage(ImageLocation.OFFLINE);
            }
            return image;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class FriendOnlineStatusTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string text = String.Empty;
            if (value is UserShortInfoModel)
            {
                UserShortInfoModel model = (UserShortInfoModel)value;
                text = HelperMethods.GetStatusText(model.Presence, model.Mood, model.LastOnlineTime);
            }
            return text;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FriendOnlineStatusIconExceptOfflineIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int type = parameter != null ? Int32.Parse(parameter.ToString()) : 0; // 1= online/away image, 2 = lastOnlineTime 
            if (value is UserShortInfoModel)
            {
                UserShortInfoModel model = (UserShortInfoModel)value;
                string status = HelperMethods.GetStatusIconLocation(model.Presence, model.Device, model.Mood, model.UserTableID);
                if (model.ContactType != SettingsConstants.SPECIAL_CONTACT)
                {
                    if (status != ImageLocation.OFFLINE && type == 1)
                    {
                        return ImageUtility.GetBitmapImage(status);
                    }
                    else if (status == ImageLocation.OFFLINE && type == 2)
                    {
                        return FeedUtils.GetLastOnlineTime(model.LastOnlineTime, View.Utility.Chat.Service.ChatService.GetServerTime());
                    }
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class CursorFromUserBasicModel : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)value;
                bool isProfileImg = Boolean.Parse(parameter.ToString());
                if (isProfileImg)
                {
                    if ((model.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID || model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED) && !string.IsNullOrEmpty(model.ShortInfoModel.ProfileImage) && model.ProfileImagePrivacy != 3)
                        return true;
                }
                else
                {
                    if ((model.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID || model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED) && !string.IsNullOrEmpty(model.CoverImage) && model.CoverImagePrivacy != 3)
                        return true;
                }
            }
            //else if (value is NewsPortalModel)
            //{
            //    NewsPortalModel model = (NewsPortalModel)value;
            //    bool isProfileImg = Boolean.Parse(parameter.ToString());
            //    if (isProfileImg)
            //    {
            //        if ((model.ShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID || model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED) && !string.IsNullOrEmpty(model.ShortInfoModel.ProfileImage) && model.ProfileImagePrivacy != 3)
            //            return true;
            //    }
            //    else
            //    {
            //        if ((model.ShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID || model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED) && !string.IsNullOrEmpty(model.CoverImage) && model.CoverImagePrivacy != 3)
            //            return true;
            //    }
            //}
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NoFriendFoundVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double height = (double)value;
            return height > 50 ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class FriendListGroupVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int FilterType = Int32.Parse(parameter.ToString());
            if (UCFriendList.Instance.SelectedFilterType == 0) return Visibility.Visible;
            else if (FilterType == UCFriendList.Instance.SelectedFilterType) return Visibility.Visible;
            else if (FilterType == UCFriendList.Instance.SelectedFilterType) return Visibility.Visible;
            else if (FilterType == UCFriendList.Instance.SelectedFilterType) return Visibility.Visible;
            else if (FilterType == UCFriendList.Instance.SelectedFilterType) return Visibility.Visible;
            else return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FriendListFilterSelectionConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string text = String.Empty;
            FriendFilterEnum param = (FriendFilterEnum)value;
            if (param == FriendFilterEnum.TypeAllFriends) text = NotificationMessages.MNU_ALL_FRIENDS;
            else if (param == FriendFilterEnum.TypeFav) text = NotificationMessages.MNU_FAV_FRIENDS;
            else if (param == FriendFilterEnum.TypeTop) text = NotificationMessages.MNU_TOP_FRIENDS;
            else if (param == FriendFilterEnum.TypeRecentlyAdded) text = NotificationMessages.MNU_RECENTLY_ADDED;
            else if (param == FriendFilterEnum.Official) text = NotificationMessages.MNU_OFFICIAL;
            UCFriendList.Instance.GetFriendCount();
            return text;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class UserIdentityToDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                string v = (string)value;
                if (!string.IsNullOrEmpty(v)) value = long.Parse(v);
                else value = (long)0;
            }
            long id = (long)value;
            return HelperMethods.GetDisplayRingIDfromUserID(id);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class TagFriendUserIdentityToDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                int type = System.Convert.ToInt32(parameter);
                switch (type)
                {
                    case 0:
                        if (value is ObservableCollection<UserShortInfoModel>)
                        {
                            ObservableCollection<UserShortInfoModel> tgList = (ObservableCollection<UserShortInfoModel>)value;
                            if (tgList.Count > 0) return HelperMethods.GetDisplayRingIDfromUserID(tgList[0].UserIdentity);
                        }
                        break;
                    case 1:
                        break;
                    case 2:
                        if (value is ObservableCollection<UserShortInfoModel>)
                        {
                            ObservableCollection<UserShortInfoModel> tgList = (ObservableCollection<UserShortInfoModel>)value;
                            if (tgList.Count > 1) return HelperMethods.GetDisplayRingIDfromUserID(tgList[1].UserIdentity);
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class RightPanelViewConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            if (value is bool)
            {
                if (!(bool)value)
                {
                    bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.LOADING_RINGID_FEED);
                    GC.SuppressFinalize(bitmapImage);
                    return bitmapImage;
                }
                else return null;
            }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class AddFriendPhoneNumber : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = "";
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)value;
                if (!string.IsNullOrEmpty(model.MobilePhoneDialingCode) && !string.IsNullOrEmpty(model.MobilePhone))
                    str = model.MobilePhoneDialingCode + " " + model.MobilePhone.Insert(4, " ");
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class AddFriendEmail : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = "";
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)value;
                if (!string.IsNullOrEmpty(model.Email)) str = model.Email;
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class AddFriendCity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = "";
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)value;
                if (!string.IsNullOrEmpty(model.HomeCity) && !string.IsNullOrEmpty(model.CurrentCity)) str = "HomeCity : " + model.HomeCity + " " + "," + " " + "CurrentCity : " + model.CurrentCity;
                else
                {
                    if (!string.IsNullOrEmpty(model.HomeCity)) str = "HomeCity : " + model.HomeCity;
                    else if (!string.IsNullOrEmpty(model.CurrentCity)) str = "CurrentCity : " + model.CurrentCity;
                }
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class FriendCurrentCity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string str = "Location not saved ";
            if (value is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)value;
                if (!string.IsNullOrEmpty(model.CurrentCity)) str = "Lives in " + model.CurrentCity;
                else if (!string.IsNullOrEmpty(model.HomeCity)) str = "Lives in " + model.HomeCity;
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class UserIDEqualLoggedInUserIDConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (long)value == DefaultSettings.LOGIN_TABLE_ID;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class LoginUserMemberAccessType : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            GroupInfoModel model = (GroupInfoModel)value;
            if (model != null && model.MemberInfoDictionary != null)
            {
                GroupMemberInfoModel member = model.MemberInfoDictionary.TryGetValue(DefaultSettings.LOGIN_TABLE_ID);
                return member != null ? member.MemberAccessType : ChatConstants.MEMBER_TYPE_NOT_MEMBER;
            }
            else return ChatConstants.MEMBER_TYPE_NOT_MEMBER;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FriendListSearchConverter : IMultiValueConverter
    {
        string prevString = string.Empty;
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = Visibility.Visible;
            try
            {
                int type = Int32.Parse(parameter.ToString()); //0 = special 1 = fav, 2 = top, 3 = more 
                FriendFilterEnum FilterType = (FriendFilterEnum)value[2];
                if (FilterType == FriendFilterEnum.TypeAllFriends) { }
                else if (FilterType == FriendFilterEnum.TypeFav) { }
                else if (FilterType == FriendFilterEnum.TypeTop) { }
                else if (FilterType == FriendFilterEnum.TypeRecentlyAdded) { }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(FriendListSearchConverter).Name).Error(ex.StackTrace + " " + ex.Message);
            }
            return visibility;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class CallLogSearchConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = Visibility.Visible;
            try
            {
                string fullName = value[0] == null ? String.Empty : value[0].ToString().ToLower().Trim();
                string userIdentity = value[1] == null ? String.Empty : value[1].ToString().ToLower().Trim();
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(FriendListSearchConverter).Name).Error(ex.StackTrace + " " + ex.Message);
            }
            return visibility;
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class SearchByFullNameConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = Visibility.Visible;
            string fullName = value[0] == null ? String.Empty : value[0].ToString().ToLower().Trim();
            string ringId = value[1] == null ? string.Empty : value[1].ToString().ToLower().Trim();
            string searchString = value[2] == null ? string.Empty : value[2].ToString().ToLower().Trim();
            if (searchString.Length > 0 && (!fullName.Contains(searchString) && !ringId.Contains(searchString))) visibility = Visibility.Collapsed;
            return visibility;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class SearchByAlbumNameConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = Visibility.Visible;

            string AlbumName = value[0] == null ? String.Empty : value[0].ToString().ToLower().Trim();
            string searchString = value[1] == null ? string.Empty : value[1].ToString().ToLower().Trim();
            if (searchString.Length > 0 && (!AlbumName.Contains(searchString))) visibility = Visibility.Collapsed;
            return visibility;
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class CircleLoadingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value == true ? Visibility.Visible : Visibility.Collapsed;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FriendCountConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int Fav = value[0] == null ? 0 : Int32.Parse(value[0].ToString());
            int Top = value[1] == null ? 0 : Int32.Parse(value[1].ToString());
            int NonFav = value[2] == null ? 0 : Int32.Parse(value[2].ToString());
            FriendFilterEnum FilterType = (FriendFilterEnum)Enum.Parse(typeof(FriendFilterEnum), value[3].ToString());
            if (UCFriendList.Instance != null)
            {
                if (FilterType == FriendFilterEnum.TypeFav) return Fav.ToString();
                else if (FilterType == FriendFilterEnum.TypeTop) return Top.ToString();
            }
            return (Fav + Top + NonFav).ToString();
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FriendAddToTemplateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visible = Visibility.Visible;
            if (value is int)
            {
                if ((int)value == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED || (int)value == StatusConstants.FRIENDSHIP_STATUS_INCOMING || (int)value == StatusConstants.FRIENDSHIP_STATUS_PENDING) visible = Visibility.Visible;
                else visible = Visibility.Collapsed;
            }
            return visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class SearchByGroupNameOrFrndNameConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = Visibility.Visible;
            string groupOrFriendName = (value[0] == null || value[0] == DependencyProperty.UnsetValue) ? String.Empty : value[0].ToString().ToLower().Trim();
            string searchString = value[1] == null ? string.Empty : value[1].ToString().ToLower().Trim();
            if (searchString.Length > 0 && (!groupOrFriendName.Contains(searchString))) visibility = Visibility.Collapsed;
            return visibility;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IsUserExistsInDictionaryConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] is long && value[1] is ObservableDictionary<long, UserBasicInfoModel>)
            {
                ObservableDictionary<long, UserBasicInfoModel> dic = (ObservableDictionary<long, UserBasicInfoModel>)value[1];
                return dic.ContainsKey((long)value[0]);
            }
            else return false;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

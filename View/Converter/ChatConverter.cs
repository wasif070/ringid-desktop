﻿using Auth.Service.RingMarket;
using Auth.utility;
using imsdkwrapper;
using MediaToolkit;
using MediaToolkit.Model;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using System;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI.Chat;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Images;
using View.ViewModel;

namespace View.Converter
{
    public class ChatTempleteConverter : IValueConverter
    {
        private static ControlTemplate TYPE_TEXT_MESSAGE = null;
        private static ControlTemplate TYPE_STICKER_MESSAGE = null;
        private static ControlTemplate TYPE_LOCATION_MESSAGE = null;
        private static ControlTemplate TYPE_IMAGE_MESSAGE = null;
        private static ControlTemplate TYPE_AUDIO_MESSAGE = null;
        private static ControlTemplate TYPE_VIDEO_MESSAGE = null;
        private static ControlTemplate TYPE_FILE_MESSAGE = null;
        private static ControlTemplate TYPE_LINK_MESSAGE = null;
        private static ControlTemplate TYPE_RING_MEDIA_MESSAGE = null;
        private static ControlTemplate TYPE_CONTACT_MESSAGE = null;
        private static ControlTemplate TYPE_DELETE_MESSAGE = null;

        static ChatTempleteConverter()
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                TYPE_TEXT_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_TEXT_MESSAGE") as ControlTemplate;
                TYPE_STICKER_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_STICKER_MESSAGE") as ControlTemplate;
                TYPE_LOCATION_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_LOCATION_MESSAGE") as ControlTemplate;
                TYPE_IMAGE_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_IMAGE_MESSAGE") as ControlTemplate;
                TYPE_AUDIO_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_AUDIO_MESSAGE") as ControlTemplate;
                TYPE_VIDEO_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_VIDEO_MESSAGE") as ControlTemplate;
                TYPE_FILE_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_FILE_MESSAGE") as ControlTemplate;
                TYPE_LINK_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_LINK_MESSAGE") as ControlTemplate;
                TYPE_RING_MEDIA_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_RING_MEDIA_MESSAGE") as ControlTemplate;
                TYPE_CONTACT_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_CONTACT_MESSAGE") as ControlTemplate;
                TYPE_DELETE_MESSAGE = Application.Current.MainWindow.FindResource("TYPE_DELETE_MESSAGE") as ControlTemplate;
            }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int viewType = (int)value;
            switch (viewType)
            {
                case ChatConstants.CHAT_PLAIN_EMOTICON_VIEW:
                    return TYPE_TEXT_MESSAGE;
                case ChatConstants.CHAT_STICKER_VIEW:
                    return TYPE_STICKER_MESSAGE;
                case ChatConstants.CHAT_LOCATION_VIEW:
                    return TYPE_LOCATION_MESSAGE;
                case ChatConstants.CHAT_IMAGE_VIEW:
                    return TYPE_IMAGE_MESSAGE;
                case ChatConstants.CHAT_AUDIO_VIEW:
                    return TYPE_AUDIO_MESSAGE;
                case ChatConstants.CHAT_VIDEO_VIEW:
                    return TYPE_VIDEO_MESSAGE;
                case ChatConstants.CHAT_FILE_VIEW:
                    return TYPE_FILE_MESSAGE;
                case ChatConstants.CHAT_LINK_VIEW:
                    return TYPE_LINK_MESSAGE;
                case ChatConstants.CHAT_RING_MEDIA_VIEW:
                    return TYPE_RING_MEDIA_MESSAGE;
                case ChatConstants.CHAT_CONTACT_VIEW:
                    return TYPE_CONTACT_MESSAGE;
                case ChatConstants.CHAT_DELETE_VIEW:
                    return TYPE_DELETE_MESSAGE;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime dt = ModelUtility.GetLocalDateTime((long)value);
            string timeText = String.Format(ChatHelpers.CHAT_TIME_FORMAT, dt);
            return timeText;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatStatusAndTimeConverter : IMultiValueConverter
    {
        private const String SEPERATOR = ", ";

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long time = (long)value[0];
            int status = (int)value[1];
            int messageType = (int)value[2];
            int packetType = (int)value[3];
            bool fromFriend = (bool)value[4];
            bool isRoomChat = (bool)value[5];
            StringBuilder builder = new StringBuilder();

            switch ((MessageType)messageType)
            {
                case MessageType.PLAIN_MESSAGE:
                case MessageType.EMOTICON_MESSAGE:
                case MessageType.LINK_MESSAGE:
                    switch (packetType)
                    {
                        case ChatConstants.PACKET_TYPE_FRIEND_MESSAGE_EDIT:
                        case ChatConstants.PACKET_TYPE_GROUP_MESSAGE_EDIT:
                        case ChatConstants.PACKET_TYPE_ROOM_MESSAGE_EDIT:
                            {
                                builder.Append(ChatHelpers.STATUS_TXT_EDITED);
                                builder.Append(ChatStatusAndTimeConverter.SEPERATOR);
                            }
                            break;
                    }
                    break;
            }

            if (!fromFriend && !isRoomChat)
            {
                switch (status)
                {
                    case ChatConstants.STATUS_VIEWED_PLAYED:
                        builder.Append(ChatHelpers.STATUS_TXT_VIEWED);
                        builder.Append(ChatStatusAndTimeConverter.SEPERATOR);
                        break;
                    case ChatConstants.STATUS_PENDING:
                        builder.Append(ChatHelpers.STATUS_TXT_PENDING);
                        builder.Append(ChatStatusAndTimeConverter.SEPERATOR);
                        break;
                    case ChatConstants.STATUS_FAILED:
                        builder.Append(ChatHelpers.STATUS_TXT_FAILED);
                        builder.Append(ChatStatusAndTimeConverter.SEPERATOR);
                        break;
                    case ChatConstants.STATUS_DISAPPEARED:
                        switch ((MessageType)messageType)
                        {
                            case MessageType.VIDEO_FILE:
                            case MessageType.IMAGE_FILE_FROM_GALLERY:
                            case MessageType.IMAGE_FILE_FROM_CAMERA:
                                {
                                    builder.Append(ChatHelpers.STATUS_TXT_DISAPPEARED_IMAGE_VIDEO);
                                    builder.Append(ChatStatusAndTimeConverter.SEPERATOR);
                                }
                                break;
                            case MessageType.AUDIO_FILE:
                                {
                                    builder.Append(ChatHelpers.STATUS_TXT_DISAPPEARED_VOICE);
                                    builder.Append(ChatStatusAndTimeConverter.SEPERATOR);
                                }
                                break;
                            default:
                                {
                                    builder.Append(ChatHelpers.STATUS_TXT_DISAPPEARED_DEFAULT);
                                    builder.Append(ChatStatusAndTimeConverter.SEPERATOR);
                                }
                                break;
                        }
                        break;
                }
            }

            builder.Append(String.Format(ChatHelpers.CHAT_TIME_FORMAT, ModelUtility.GetLocalDateTime(time)));
            return builder.ToString();
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatLastStatusConverter : IMultiValueConverter
    {
        private const String SEPERATOR = ", ";

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long messageDate = (long)value[0];
            long lastDeliveredDate = (long)value[1];
            long lastSeenDate = (long)value[2];
            return (messageDate == lastSeenDate ? 1 : (messageDate == lastDeliveredDate ? 2 : 0));
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatWideTextLimitConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int status = (int)value[0];
            int messageType = (int)value[1];
            int packetType = (int)value[2];

            switch ((MessageType)messageType)
            {
                case MessageType.PLAIN_MESSAGE:
                case MessageType.EMOTICON_MESSAGE:
                case MessageType.LINK_MESSAGE:
                    switch (packetType)
                    {
                        case ChatConstants.PACKET_TYPE_FRIEND_MESSAGE_EDIT:
                        case ChatConstants.PACKET_TYPE_GROUP_MESSAGE_EDIT:
                        case ChatConstants.PACKET_TYPE_ROOM_MESSAGE_EDIT:
                            {
                                return 270.00;
                            }
                    }
                    break;
            }

            switch (status)
            {
                case ChatConstants.STATUS_VIEWED_PLAYED:
                case ChatConstants.STATUS_PENDING:
                case ChatConstants.STATUS_FAILED:
                case ChatConstants.STATUS_DISAPPEARED:
                    return 270.00;
            }

            return 308.00;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatDateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime dt = ModelUtility.GetLocalDateTime((long)value);
            DateTime currDt = DateTime.Now;
            string timeText = String.Format(ChatHelpers.CHAT_DATE_FORMAT, dt);

            if (dt.IsSameDateOf(currDt))
            {
                timeText = "Today, " + timeText;
            }
            else if (dt.Year == currDt.Year && dt.DayOfYear == (currDt.DayOfYear - 1))
            {
                timeText = "Yesterday, " + timeText;
            }

            return timeText;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatShortDateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime dt = ModelUtility.GetLocalDateTime((long)value);
            DateTime currDt = DateTime.Now;
            string f = dt.ToShortDateString() + ", "; //"8/9/2015"
            string l = dt.ToShortTimeString();//"7:38 PM"

            if (dt.IsSameDateOf(currDt))
            {
                f = "Today, ";
            }
            else if (dt.Year == currDt.Year && dt.DayOfYear == (currDt.DayOfYear - 1))
            {
                f = "Yesterday, ";
            }

            return (f + l);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatStickerConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string message = (string)value[0];
            bool IsPreviewOpened = (bool)value[1];

            if (IsPreviewOpened)
            {
                string filePath = ChatHelpers.GetChatPreviewPath(message, MessageType.DOWNLOAD_STICKER_MESSAGE, "", "", 0, 0, "");
                if (File.Exists(filePath))
                {
                    try
                    {
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.UriSource = new Uri(filePath);
                        image.EndInit();
                        image.Freeze();
                        return image;
                    }
                    catch
                    {
                    }
                }

            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class LocationImageConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            float lat = (float)value[0];
            float lng = (float)value[1];
            bool IsPreviewOpened = (bool)value[2];

            if (IsPreviewOpened)
            {
                string filePath = ChatHelpers.GetChatPreviewPath("", MessageType.LOCATION_MESSAGE, "", "", lat, lng, "");
                if (File.Exists(filePath))
                {
                    try
                    {
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.UriSource = new Uri(filePath);
                        image.EndInit();
                        image.Freeze();
                        return image;
                    }
                    catch
                    {
                    }
                }
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
    public class SharedImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isExists = false;
            MessageModel model = (MessageModel)value[0];
            int width = (int)value[1];
            int height = (int)value[2];
            bool IsPreviewOpened = (bool)value[3];

            if (IsPreviewOpened)
            {
                string filePath = ChatHelpers.GetChatFilePath(model.Message, model.MessageType, model.PacketID, "", 0);
                if (!(isExists = File.Exists(filePath)))
                {
                    filePath = ChatHelpers.GetChatPreviewPath(model.Message, model.MessageType, model.PacketID, "", 0, 0, "");
                    isExists = File.Exists(filePath);
                }

                if (isExists)
                {
                    try
                    {
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        //image.DecodePixelWidth = width;
                        image.DecodePixelHeight = height;
                        image.UriSource = new Uri(filePath, UriKind.Absolute);
                        image.EndInit();
                        image.Freeze();
                        return image;
                    }
                    catch
                    {
                    }
                }
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
    public class SharedContactImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long tableId = (long)value[0];
            string profileImage = (string)value[1];
            string fullName = (string)value[2];
            bool IsPreviewOpened = (bool)value[3];

            if (IsPreviewOpened)
            {
                string filePath = ChatHelpers.GetChatPreviewPath("", MessageType.CONTACT_SHARE, "", "", 0, 0, profileImage);
                if (File.Exists(filePath))
                {
                    try
                    {
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.DecodePixelWidth = 120;
                        image.UriSource = new Uri(filePath, UriKind.Absolute);
                        image.EndInit();
                        image.Freeze();
                        return image;
                    }
                    catch
                    {
                        return ImageUtility.GetDetaultProfileImage(tableId, fullName, 120, 120, 25);
                    }
                }
            }

            return ImageUtility.GetDetaultProfileImage(tableId, fullName, 120, 120, 25);
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
    public class SharedVideoThumbConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string packetID = (string)value[0];
            int width = (int)value[1];
            int height = (int)value[2];
            bool IsPreviewOpened = (bool)value[3];

            if (IsPreviewOpened)
            {
                string filePath = ChatHelpers.GetChatPreviewPath("", MessageType.VIDEO_FILE, packetID, "", 0, 0, "");
                if (File.Exists(filePath))
                {
                    try
                    {
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        //image.DecodePixelWidth = width;
                        image.DecodePixelHeight = height;
                        image.UriSource = new Uri(filePath, UriKind.Absolute);
                        image.EndInit();
                        image.Freeze();
                        return image;
                    }
                    catch
                    {
                    }
                }
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
    public class RingMediaImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string imageUrl = (string)value[0];
            int width = (int)value[1];
            int height = (int)value[2];
            bool IsPreviewOpened = (bool)value[3];

            if (IsPreviewOpened)
            {
                string filePath = ChatHelpers.GetChatPreviewPath("", MessageType.RING_MEDIA_MESSAGE, "", imageUrl, 0, 0, "");
                if (File.Exists(filePath))
                {
                    try
                    {
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.DecodePixelHeight = height;
                        image.UriSource = new Uri(filePath, UriKind.Absolute);
                        image.EndInit();
                        image.Freeze();
                        return image;
                    }
                    catch
                    {
                    }
                }
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
    public class ChatLinkImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string linkImageUrl = (string)value[0];
            bool IsPreviewOpened = (bool)value[1];

            if (IsPreviewOpened)
            {
                string filePath = ChatHelpers.GetChatPreviewPath("", MessageType.LINK_MESSAGE, "", linkImageUrl, 0, 0, "");
                if (File.Exists(filePath))
                {
                    try
                    {
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.DecodePixelWidth = 365;
                        image.UriSource = new Uri(filePath, UriKind.Absolute);
                        image.EndInit();
                        image.Freeze();
                        return image;
                    }
                    catch
                    {
                    }
                }
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
    public class ImagePreviewConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            MessageModel model = (MessageModel)value[0];
            double width = (double)value[1];
            double height = (double)value[2];
            bool IsFileOpened = (bool)value[3];

            if (IsFileOpened && width > 0 && height > 0)
            {
                string filePath = ChatHelpers.GetChatFilePath(model.Message, model.MessageType, model.PacketID, model.LinkImageUrl, 0);
                if (File.Exists(filePath))
                {
                    try
                    {
                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.DecodePixelWidth = (int)width;
                        image.DecodePixelHeight = (int)height;
                        image.UriSource = new Uri(filePath, UriKind.Absolute);
                        image.EndInit();
                        image.Freeze();
                        return image;
                    }
                    catch
                    {
                    }
                }
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
    public class VideoPreviewConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            MessageModel model = (MessageModel)value[0];
            bool IsFileOpened = (bool)value[1];

            if (IsFileOpened)
            {
                string filePath = ChatHelpers.GetChatFilePath(model.Message, model.MessageType, model.PacketID, "", 0);
                if (File.Exists(filePath))
                {
                    Metadata metaData = MediaService.GetMetadata(RingIDSettings.TEMP_MEDIA_TOOLKIT, filePath);
                    if (metaData != null && metaData.VideoData != null)
                    {
                        if (metaData.VideoData.Rotate > 0)
                        {
                            model.ViewModel.RotateAngle = metaData.VideoData.Rotate;
                        }
                        if (model.Duration != metaData.Duration.Seconds)
                        {
                            model.Duration = metaData.Duration.Seconds;
                        }
                    }
                    return new Uri(filePath);
                }
            }

            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
    public class ChatShortFileNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ChatHelpers.GetShortStreamFileName((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatLogVisibilityBySearchConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = Visibility.Visible;
            string fullName = value[0] == null ? String.Empty : value[0].ToString().ToLower();
            RecentModel model = (RecentModel)value[1];

            if (UCChatLogPanel.Instance != null && UCChatLogPanel.Instance.IsDeletePanelVisible)
            {
                model.IsChecked = visibility == Visibility.Visible && UCChatLogPanel.Instance.IsSelectAllMode == false ? true : (visibility == Visibility.Collapsed ? false : model.IsChecked);
            }

            return visibility;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class PathToChatBackgroundSettingsImageConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string imagePath = value as String;
            if (!String.IsNullOrEmpty(imagePath))
            {
                imagePath = RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER + Path.DirectorySeparatorChar + "thumb" + imagePath;
                ImageFile imageFile = new ImageFile(imagePath);
                if (imageFile.IsComplete)
                {
                    try
                    {
                        BitmapImage bitmapImage = new BitmapImage();
                        bitmapImage.BeginInit();
                        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                        bitmapImage.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                        bitmapImage.DecodePixelWidth = 100;
                        bitmapImage.UriSource = new Uri(imagePath, UriKind.Absolute);
                        bitmapImage.EndInit();
                        bitmapImage.Freeze();
                        return bitmapImage;
                    }
                    catch
                    {
                        return null;
                    }
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class PathToChatBackgroundConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string chatBg = (string)value[0];
            string eventChatBg = (string)value[1];

            string chatBgPath = RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER + Path.DirectorySeparatorChar + chatBg;
            string eventChatBgPath = RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER + Path.DirectorySeparatorChar + eventChatBg;

            Uri url = null;
            if (!String.IsNullOrWhiteSpace(eventChatBg) && new ImageFile(eventChatBgPath).IsComplete)
            {
                url = new Uri(eventChatBgPath);
            }
            else if (!String.IsNullOrWhiteSpace(chatBg) && new ImageFile(chatBgPath).IsComplete)
            {
                url = new Uri(chatBgPath);
            }
            else
            {
                url = ImageUtility.GetUri(ImageLocation.DEFAULT_CHAT_BG);
            }

            try
            {
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                bitmapImage.DecodePixelHeight = DefaultSettings.MIN_CHAT_BG_HEIGHT;
                bitmapImage.UriSource = url;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
                return bitmapImage;
            }
            catch
            {
                return null;
            }
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatPermissionConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //HAS FRINED CHAT PERMISSION && (BLOCKED BY FRIEND && NO ANONYMOUS CHAT PERMISSION)
            int friendShipShatus = (int)value[0];
            int chatAccess = (int)value[1];
            bool isFriendRegDenied = (bool)value[2];
            bool isBlockedByMe = (bool)value[3];
            bool isBlockedFriend = (bool)value[4];
            int anonymousChatAccess = (int)value[5];

            if (friendShipShatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            {
                return chatAccess == StatusConstants.TYPE_ACCESS_UNBLOCKED && isFriendRegDenied;
            }
            else
            {
                return !isBlockedByMe && anonymousChatAccess == StatusConstants.CHAT_CALL_EVERYONE && isBlockedFriend;
            }
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatImageZoomPercentageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double zoom = (double)value - 1;
            zoom = ((zoom * 100) / (UCChatMediaImagePreview.MAX_ZOOM_LIMIT - 1));
            return (int)zoom;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatBubbolConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double ActualWidth = (double)value[0];
            double ActualHeight = (double)value[1];
            bool FromFriend = (bool)value[2];
            bool IsSamePrevAndCurrID = (bool)value[3];
            Thickness Padding = new Thickness(13, 0, 13, 0);//17
            int RadiusX = 10;//14
            int RadiusY = 10;//14
            Arrow arrow = IsSamePrevAndCurrID ? Arrow.None : (FromFriend ? Arrow.Left : Arrow.Right);

            Geometry result = new RectangleGeometry(new Rect(0 + Padding.Left, 0 + Padding.Top, Math.Max(ActualWidth - Padding.Left - Padding.Right, 0), Math.Max(ActualHeight - Padding.Top - Padding.Bottom, 0)), RadiusX, RadiusY);
            double halfWidth = (result.Bounds.Width) / 2;
            double halfHeight = (result.Bounds.Height) / 2;

            if (arrow == Arrow.Left)
            {
                if (Padding.Left > 0)
                {
                    PathGeometry pathGeometry = new PathGeometry();
                    PathFigure figure = new PathFigure { StartPoint = new System.Windows.Point(0, 0 + Padding.Top - 2), IsClosed = true };
                    figure.Segments.Add(new ArcSegment { Point = new System.Windows.Point(0, Padding.Left * 2 - 2), Size = new System.Windows.Size(1, 1), SweepDirection = SweepDirection.Clockwise });
                    pathGeometry.Figures.Add(figure);

                    CombinedGeometry temp = new CombinedGeometry(GeometryCombineMode.Union, new RectangleGeometry(new Rect(0, Padding.Left - 2, Padding.Left, Padding.Left)), pathGeometry);
                    temp = new CombinedGeometry(GeometryCombineMode.Exclude, new RectangleGeometry(new Rect(0, 0 + Padding.Top, Padding.Left, Padding.Left * 2 - 2)), temp);
                    temp = new CombinedGeometry(GeometryCombineMode.Union, new RectangleGeometry(new Rect(0 + Padding.Left, 0 + Padding.Top, halfWidth, halfHeight)), temp);
                    result = new CombinedGeometry(GeometryCombineMode.Union, result, temp);
                }
            }
            else if (arrow == Arrow.Right)
            {
                if (Padding.Right > 0)
                {
                    PathGeometry pathGeometry = new PathGeometry();
                    PathFigure figure = new PathFigure { StartPoint = new System.Windows.Point(ActualWidth, 0 + Padding.Top - 2), IsClosed = true };
                    figure.Segments.Add(new ArcSegment { Point = new System.Windows.Point(ActualWidth, Padding.Right * 2 - 2), Size = new System.Windows.Size(1, 1), SweepDirection = SweepDirection.Counterclockwise });
                    pathGeometry.Figures.Add(figure);

                    CombinedGeometry temp = new CombinedGeometry(GeometryCombineMode.Union, new RectangleGeometry(new Rect(ActualWidth - Padding.Right, Padding.Right - 2, Padding.Right, Padding.Right)), pathGeometry);
                    temp = new CombinedGeometry(GeometryCombineMode.Exclude, new RectangleGeometry(new Rect(ActualWidth - Padding.Right, 0 + Padding.Top, Padding.Right, Math.Max(Padding.Right * 2 - 2, 0))), temp);
                    temp = new CombinedGeometry(GeometryCombineMode.Union, new RectangleGeometry(new Rect(halfWidth + Padding.Left, 0 + Padding.Top, halfWidth, halfHeight)), temp);
                    result = new CombinedGeometry(GeometryCombineMode.Union, result, temp);
                }
            }

            return result;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatFileIconConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string message = value[0].ToString();
            string packetID = value[1].ToString();
            long userIdentity = (long)value[2];
            return (ImageSource)IconManager.FindIconForFilename(ChatHelpers.GetChatFilePath(message, MessageType.FILE_STREAM, packetID, "", userIdentity), true);
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatFileStatusConverter : IMultiValueConverter
    {
        static string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" }; //Longs run out around EB

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string statusText = String.Empty;

            int fileStatus = (int)value[0];
            long fileSize = (long)value[1];
            bool fromFriend = (bool)value[2];
            long fileProgress = (long)value[3];
            bool isFileQueued = (bool)value[4];

            if (isFileQueued)
            {
                return ChatHelpers.STATUS_TXT_FILE_QUEUED;
            }

            switch (fileStatus)
            {
                case ChatConstants.FILE_DEFAULT:
                    statusText = ChatHelpers.GetFileSizeInText(fileSize);
                    if (fileProgress > 0)
                    {
                        statusText = ChatHelpers.GetFileSizeInText(fileProgress) + " of " + statusText;
                    }
                    break;
                case ChatConstants.FILE_TRANSFER_CANCELLED:
                    if (fromFriend)
                    {
                        statusText = ChatHelpers.STATUS_TXT_FILE_RECEIVING_CANCELLED;
                    }
                    else
                    {
                        statusText = ChatHelpers.STATUS_TXT_FILE_SENDING_CANCELLED;
                    }
                    break;
                case ChatConstants.FILE_TRANSFER_COMPLETED:
                    if (fromFriend)
                    {
                        statusText = ChatHelpers.STATUS_TXT_FILE_RECEIVED;
                    }
                    else
                    {
                        statusText = ChatHelpers.STATUS_TXT_FILE_SENT;
                    }
                    break;
                case ChatConstants.FILE_MOVED:
                    statusText = ChatHelpers.STATUS_TXT_FILE_MOVED;
                    break;
            }

            return statusText;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatFileActionConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int fileStatus = (int)value[0];
            bool fromFriend = (bool)value[1];
            bool isDownloadRunning = (bool)value[2];
            int action = 0;

            switch (fileStatus)
            {
                case ChatConstants.FILE_DEFAULT:
                    if (fromFriend && isDownloadRunning)
                    {
                        action = 1;//Downloading
                    }
                    else if (fromFriend && !isDownloadRunning)
                    {
                        action = 2;//Message Recieved
                    }
                    else if (!fromFriend)
                    {
                        action = 3;//Message Send
                    }
                    break;
                case ChatConstants.FILE_TRANSFER_COMPLETED:
                    action = 4;//Send/Receive Completed
                    break;
                case ChatConstants.FILE_TRANSFER_CANCELLED:
                    action = 5;//Send/Receive Cancelled
                    break;
                case ChatConstants.FILE_MOVED:
                    action = 6;//File Moved
                    break;
            }
            return action;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatAudioActionConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isFileOpened = (bool)value[0];
            bool isAudioOpened = (bool)value[1];
            int state = (int)value[2];
            bool isDownloadRunning = (bool)value[3];
            bool isUploadRunning = (bool)value[4];
            int action = 0;

            if (isFileOpened)
            {
                if (isAudioOpened)
                {
                    switch (state)
                    {
                        case StatusConstants.MEDIA_INIT_STATE:
                            action = 2;//Init->Play
                            break;
                        case StatusConstants.MEDIA_PAUSE_STATE:
                            action = 3;//Pause->play
                            break;
                        case StatusConstants.MEDIA_PLAY_STATE:
                            action = 4;//Play->Pause
                            break;
                    }
                }
            }
            else
            {
                if (!isDownloadRunning && !isUploadRunning)
                {
                    action = 1;//Download
                }
            }
            return action;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatAudioActionCommandParameterConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            MessageModel model = (MessageModel)value[0];
            int state = (int)value[1];

            dynamic obj = new ExpandoObject();
            obj.Model = model;
            switch (state)
            {
                case StatusConstants.MEDIA_INIT_STATE:
                case StatusConstants.MEDIA_PAUSE_STATE:
                    obj.State = StatusConstants.MEDIA_PLAY_STATE;
                    break;
                case StatusConstants.MEDIA_PLAY_STATE:
                    obj.State = StatusConstants.MEDIA_PAUSE_STATE;
                    break;
                default:
                    obj.State = StatusConstants.MEDIA_INIT_STATE;
                    break;
            }
            return obj;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatAudioPlayerProgressTimeConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int state = (int)value[0];
            int offset = (int)value[1];
            int duration = (int)value[2];

            return TimeSpan.FromSeconds(state == StatusConstants.MEDIA_INIT_STATE ? duration : offset);
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatPlayerTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int duration = (int)value;
            return TimeSpan.FromSeconds(duration);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ChatPlayerProgressConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int offset = (int)value[0];
            int duration = (int)value[1];
            return duration > 0 ? ((double)offset * 100) / duration : 0d;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class IsGroupMemberActionVisibleConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int accessType = (int)value[0];
            int memberAccessType = value[1] is int ? (int)value[1] : ChatConstants.MEMBER_TYPE_NOT_MEMBER;
            long userTableId = (long)value[2];
            long memberAddedBy = (long)value[3];
            return (memberAccessType < accessType || userTableId == DefaultSettings.LOGIN_TABLE_ID || (accessType == ChatConstants.MEMBER_TYPE_MEMBER && memberAccessType == ChatConstants.MEMBER_TYPE_MEMBER && memberAddedBy == DefaultSettings.LOGIN_TABLE_ID));
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public enum Arrow
    {
        None = 0,
        Left = 1,
        Right = 2,
    }
}

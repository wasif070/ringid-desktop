<<<<<<< HEAD
﻿using Models.Constants;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Circle;
using View.Utility.DataContainer;
using View.Utility.Images;

namespace View.Converter
{
    class NotificationFrienNameConverter : IValueConverter
    {
        //public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        //{
        //    string txt = "";
        //    NotificationModel model = null;
        //    UserShortInfoModel userShortInfoModel = null;

        //    if (value[0] is UserShortInfoModel && value[1] is NotificationModel)
        //    {
        //        userShortInfoModel = (UserShortInfoModel)value[0];
        //        model = (NotificationModel)value[1];

        //        //if (userShortInfoModel.UserTableID > 0 && !string.IsNullOrEmpty(userShortInfoModel.FullName))
        //        if (!string.IsNullOrEmpty(model.UserShortInfoModel.FullName))
        //        {
        //            //string modifiedFriendName = (userShortInfoModel.FullName.Length > 30) ? userShortInfoModel.FullName.Substring(0, 26) + "...\n" : userShortInfoModel.FullName;
        //            string modifiedFriendName = (model.UserShortInfoModel.FullName.Length > 30) ? model.UserShortInfoModel.FullName.Substring(0, 26) + "...\n" : model.UserShortInfoModel.FullName;
        //            if (model.NumberOfLikeOrComment > 1)
        //            {
        //                if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG || model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG || model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG)
        //                {
        //                    txt = modifiedFriendName;
        //                }
        //                else
        //                {
        //                    //if (model.NumberOfLikeOrComment == 2)
        //                    //{
        //                    //    txt = model.UserShortInfoModel.FullName.Length > 28 ? modifiedFriendName + "and " + (model.NumberOfLikeOrComment - 1) + " other" : modifiedFriendName + " and " + (model.NumberOfLikeOrComment - 1) + " other";
        //                    //}
        //                    //else
        //                    //{
        //                    //    txt = model.UserShortInfoModel.FullName.Length > 28 ? modifiedFriendName + "and " + (model.NumberOfLikeOrComment - 1) + " others" : modifiedFriendName + " and " + (model.NumberOfLikeOrComment - 1) + " others";
        //                    //}
        //                    txt = string.Format("{0} and {1} {2}", model.UserShortInfoModel.FullName.Length > 28 ? model.UserShortInfoModel.FullName : modifiedFriendName, model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other");
        //                }
        //            }
        //            else
        //            {
        //                txt = modifiedFriendName;
        //            }
        //            // txt = modifiedFriendName + " ";
        //        }

        //    }
        //    return txt;
        //}


        //public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        //{
        //    throw new NotImplementedException();
        //}

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string txt = "";
            NotificationModel model = null;
            UserShortInfoModel userShortInfoModel = null;

            if (value is NotificationModel)
            {
                model = (NotificationModel)value;
                userShortInfoModel = model.UserShortInfoModel;
                if (!string.IsNullOrEmpty(model.UserShortInfoModel.FullName))
                {
                    //string modifiedFriendName = (userShortInfoModel.FullName.Length > 30) ? userShortInfoModel.FullName.Substring(0, 26) + "...\n" : userShortInfoModel.FullName;
                    string modifiedFriendName = (model.UserShortInfoModel.FullName.Length > 30) ? model.UserShortInfoModel.FullName.Substring(0, 26) + "...\n" : model.UserShortInfoModel.FullName;
                    if (model.NumberOfLikeOrComment > 1)
                    {
                        if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG || model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG || model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG)
                        {
                            txt = modifiedFriendName;
                        }
                        else
                        {
                            //if (model.NumberOfLikeOrComment == 2)
                            //{
                            //    txt = model.UserShortInfoModel.FullName.Length > 28 ? modifiedFriendName + "and " + (model.NumberOfLikeOrComment - 1) + " other" : modifiedFriendName + " and " + (model.NumberOfLikeOrComment - 1) + " other";
                            //}
                            //else
                            //{
                            //    txt = model.UserShortInfoModel.FullName.Length > 28 ? modifiedFriendName + "and " + (model.NumberOfLikeOrComment - 1) + " others" : modifiedFriendName + " and " + (model.NumberOfLikeOrComment - 1) + " others";
                            //}
                            txt = string.Format("{0} and {1} {2}", model.UserShortInfoModel.FullName.Length > 28 ? model.UserShortInfoModel.FullName : modifiedFriendName, model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other");
                        }
                    }
                    else
                    {
                        txt = modifiedFriendName;
                    }
                    // txt = modifiedFriendName + " ";
                }
            }

            return txt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    class NotificationTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string txt = "";
            //int messageType = (int)value;

            NotificationModel model = null;

            //if (value is NotificationModel)
            //{
            model = (NotificationModel)value;
            switch (model.MessageType)
            {
                case StatusConstants.MESSAGE_UPDATE_PROFILE_IMAGE:
                    txt = NotificationMessages.MSG_UPDATE_PROFILE_IMAGE;
                    break;
                case StatusConstants.MESSAGE_UPDATE_COVER_IMAGE:
                    txt = NotificationMessages.MSG_UPDATE_COVER_IMAGE;
                    break;
                case StatusConstants.MESSAGE_ADD_FRIEND:
                    txt = NotificationMessages.MSG_ADD_FRIEND;
                    break;
                case StatusConstants.MESSAGE_ACCEPT_FRIEND:
                    txt = NotificationMessages.MSG_ACCEPT_FRIEND;
                    break;
                case StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER:
                    string circleName = (!CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.ContainsKey(model.ActivityID)) ? "a Circle" : CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[model.ActivityID].FullName;
                    circleName = circleName.Length > 25 ? string.Format("{0}...", circleName.Substring(0, 20)) : circleName;
                    txt = string.Format(NotificationMessages.MSG_ADD_CIRCLE_MEMBER, circleName);
                    break;
                case StatusConstants.MESSAGE_ADD_STATUS_COMMENT:
                    txt = NotificationMessages.MSG_ADD_STATUS_COMMENT;
                    break;
                case StatusConstants.MESSAGE_LIKE_STATUS:
                    txt = NotificationMessages.MSG_LIKE_STATUS;
                    break;
                case StatusConstants.MESSAGE_LIKE_COMMENT:
                    txt = NotificationMessages.MSG_LIKE_COMMENT;
                    break;
                case StatusConstants.MESSAGE_ADD_COMMENT_ON_COMMENT:
                    txt = NotificationMessages.MSG_ADD_COMMENT_ON_COMMENT;
                    break;
                case StatusConstants.MESSAGE_SHARE_STATUS:
                    txt = NotificationMessages.MSG_SHARE_STATUS;
                    break;
                case StatusConstants.MESSAGE_LIKE_IMAGE:
                    txt = NotificationMessages.MSG_LIKE_IMAGE;
                    break;
                case StatusConstants.MESSAGE_IMAGE_COMMENT:
                    txt = NotificationMessages.MSG_IMAGE_COMMENT;
                    break;
                case StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT:
                    txt = NotificationMessages.MSG_LIKE_IMAGE_COMMENT;
                    break;
                case StatusConstants.MESSAGE_ACCEPT_FRIEND_ACCESS:
                    txt = NotificationMessages.MSG_ACCEPT_FRIEND_ACCESS;
                    break;
                case StatusConstants.MESSAGE_UPGRADE_FRIEND_ACCESS:
                    txt = NotificationMessages.MSG_UPGRADE_FRIEND_ACCESS;
                    break;
                case StatusConstants.MESSAGE_DOWNGRADE_FRIEND_ACCESS:
                    txt = NotificationMessages.MSG_DOWNGRADE_FRIEND_ACCESS;
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG:
                    txt = string.Format(" was with you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
                    break;
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA:
                    txt = NotificationMessages.MESSAGE_LIKE_AUDIO_MEDIA;
                    break;
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT:
                    txt = NotificationMessages.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT;
                    break;
                case StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT:
                    txt = NotificationMessages.MESSAGE_AUDIO_MEDIA_COMMENT;
                    break;
                case StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW:
                    txt = NotificationMessages.MESSAGE_AUDIO_MEDIA_VIEW;
                    break;
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA:
                    txt = NotificationMessages.MESSAGE_LIKE_VIDEO_MEDIA;
                    break;
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT:
                    txt = NotificationMessages.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT;
                    break;
                case StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT:
                    txt = NotificationMessages.MESSAGE_VIDEO_MEDIA_COMMENT;
                    break;
                case StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW:
                    txt = NotificationMessages.MESSAGE_VIDEO_MEDIA_VIEW;
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG:
                    txt = string.Format("tagged you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG:
                    txt = string.Format("tagged you{0} in a comment", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_LIVE:
                    txt = "is live now";
                    break;
                default:
                    break;
            }

            //if (model.MessageType == StatusConstants.MESSAGE_UPDATE_PROFILE_IMAGE)
            //{
            //    txt = NotificationMessages.MSG_UPDATE_PROFILE_IMAGE;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_UPDATE_COVER_IMAGE)
            //{
            //    txt = NotificationMessages.MSG_UPDATE_COVER_IMAGE;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ADD_FRIEND)
            //{
            //    txt = NotificationMessages.MSG_ADD_FRIEND;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ACCEPT_FRIEND)
            //{
            //    txt = NotificationMessages.MSG_ACCEPT_FRIEND;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER)
            //{
            //    //string circleName = "a Circle";
            //    string circleName = (!CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.ContainsKey(model.ActivityID)) ? "a Circle" : CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[model.ActivityID].FullName;
            //    circleName = circleName.Length > 25 ? string.Format("{0}...", circleName.Substring(0, 20)) : circleName;
            //    txt = string.Format(NotificationMessages.MSG_ADD_CIRCLE_MEMBER, circleName);
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ADD_STATUS_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_ADD_STATUS_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_STATUS)
            //{
            //    txt = NotificationMessages.MSG_LIKE_STATUS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_LIKE_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ADD_COMMENT_ON_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_ADD_COMMENT_ON_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_SHARE_STATUS)
            //{
            //    txt = NotificationMessages.MSG_SHARE_STATUS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_IMAGE)
            //{
            //    txt = NotificationMessages.MSG_LIKE_IMAGE;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_IMAGE_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_IMAGE_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_LIKE_IMAGE_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ACCEPT_FRIEND_ACCESS)
            //{
            //    txt = NotificationMessages.MSG_ACCEPT_FRIEND_ACCESS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_UPGRADE_FRIEND_ACCESS)
            //{
            //    txt = NotificationMessages.MSG_UPGRADE_FRIEND_ACCESS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_DOWNGRADE_FRIEND_ACCESS)
            //{
            //    txt = NotificationMessages.MSG_DOWNGRADE_FRIEND_ACCESS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG)
            //{
            //    //txt = NotificationMessages.MESSAGE_YOU_HAVE_BEEN_TAGGED;
            //    txt = string.Format(" was with you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA)
            //{
            //    txt = NotificationMessages.MESSAGE_LIKE_AUDIO_MEDIA;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT)
            //{
            //    txt = NotificationMessages.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT)
            //{
            //    txt = NotificationMessages.MESSAGE_AUDIO_MEDIA_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW)
            //{
            //    txt = NotificationMessages.MESSAGE_AUDIO_MEDIA_VIEW;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA)
            //{
            //    txt = NotificationMessages.MESSAGE_LIKE_VIDEO_MEDIA;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT)
            //{
            //    txt = NotificationMessages.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT)
            //{
            //    txt = NotificationMessages.MESSAGE_VIDEO_MEDIA_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW)
            //{
            //    txt = NotificationMessages.MESSAGE_VIDEO_MEDIA_VIEW;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG)
            //{
            //    //txt = string.Format("tagged you{0} in a post", model.NumberOfLikeOrComment > 1 ? " and " + (model.NumberOfLikeOrComment - 1).ToString() + " others" : string.Empty);
            //    txt = string.Format("tagged you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG)
            //{
            //    //txt = string.Format("tagged you{0} in a comment", model.NumberOfLikeOrComment > 1 ? " and " + (model.NumberOfLikeOrComment - 1).ToString() + " others" : string.Empty);
            //    txt = string.Format("tagged you{0} in a comment", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_NOW_FRIEND)
            //{
            //    txt = NotificationMessages.MSG_NOW_FRIEND;
            //}

            //}
            return txt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationAllReadConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // NotificationModel model = null;
            string time = "";
            long notificationTime = (long)value;
            //if (value is NotificationModel)
            //{
            //model = (NotificationModel)value;
            //time = FeedUtils.GetShowableDate(model.UpdateTime);

            DateTime dt = ModelUtility.GetLocalDateTime(notificationTime);
            DateTime currDt = DateTime.UtcNow;
            string f = dt.ToShortDateString() + ", "; //"8/9/2015"
            string l = dt.ToShortTimeString();//"7:38 PM"

            if (dt.IsSameDateOf(currDt))
            {
                f = "Today, ";
            }
            else if (dt.Year == currDt.Year && dt.DayOfYear == (currDt.DayOfYear - 1))
            {
                f = "Yesterday, ";
            }

            time = (f + l);
            //}

            return time;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //class PathToNotificationImageConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (value == null) return ImageObjects.UNKNOWN_IMAGE;
    //        int imgType = (int)parameter;
    //        string imageUrl = null;
    //        string fullName = String.Empty;
    //        NotificationModel model = null;

    //        if (value is NotificationModel)
    //        {
    //            model = (NotificationModel)value;
    //            imageUrl = model.ImageUrl;
    //            fullName = imgType == ImageUtility.IMG_CROP ? String.Empty : HelperMethods.GetShortName(model.FriendName);
    //        }

    //        if (String.IsNullOrEmpty(imageUrl))
    //        {
    //            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(model.FriendID) && !string.IsNullOrWhiteSpace(FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[model.FriendID].ProfileImage)) imageUrl = FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[model.FriendID].ProfileImage;
    //            else return ImageUtility.GetDetaultProfileImage(fullName);
    //        }

    //        string convertedUrl = HelperMethods.ConvertUrlTo(imageUrl, imgType);
    //        string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

    //        BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);
    //        if (image == null && model != null && model.FriendID > 0 && convertedUrl != null)
    //        {
    //            ProfileImage obj = new ProfileImage(model.FriendID, convertedUrl);
    //            ImageDictionaries.Instance.FRIENDLIST_PROFILE_IMAGE_QUEUE.AddItem(obj);
    //            if (!String.IsNullOrEmpty(fullName))
    //            {
    //                image = ImageUtility.GetDetaultProfileImage(fullName);
    //            }


    //        }
    //        return image;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    class NotificationCounterTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            int param = parameter != null ? Int32.Parse((string)parameter) : 0;

            string text = count.ToString();

            if (count > 99)
            {
                text = 99 + "+";
            }

            if (param < 0)
            {
                return count > 1 ? count + " " + "New Stickers" : count == 0 ? "Sticker Market" : count + " " + "New Sticker";
            }

            return text;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationEllipseSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            int size = 0;
            if (0 <= count && count <= 9)
            {
                size = 16;
            }
            else if (10 <= count && count <= 99)
            {
                size = 18;
            }
            // else if (100 <= count && count <= 999)
            else
            {
                size = 22;
            }
            return size;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationEllipseLeftConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            int leftPosition = 0;
            if (parameter != null)
            {
                if (0 <= count && count <= 9)
                {
                    leftPosition = -26;
                }
                else if (10 <= count && count <= 99)
                {
                    leftPosition = -24;
                }
                //else if (100 <= count && count <= 999)
                else
                {
                    leftPosition = -22;
                }
            }
            else
            {
                if (0 <= count && count <= 9)
                {
                    leftPosition = -20;
                }
                else if (10 <= count && count <= 99)
                {
                    leftPosition = -18;
                }
                else
                {
                    leftPosition = -16;
                }
            }
            return leftPosition;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationCounterMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            string margin = "";
            if (parameter != null)
            {
                bool isTopTabNotifications = Boolean.Parse(parameter.ToString());
                if (0 <= count && count <= 9)
                {
                    margin = (isTopTabNotifications) ? "-15,5,0,0" : "41,5,0,0";
                }
                else if (10 <= count && count <= 99)
                {
                    margin = (isTopTabNotifications) ? "-14,6,0,0" : "39,6,0,0";
                }
                //else if (100 <= count && count <= 999)
                else
                {
                    margin = (isTopTabNotifications) ? "-13,8,0,0" : "38,7,0,0";
                }
            }
            else
            {
                if (0 <= count && count <= 9)
                {
                    margin = "-21,5,0,0";
                }
                else if (10 <= count && count <= 99)
                {
                    margin = "-22,6,0,0";
                }
                else
                {
                    margin = "-24,8,0,0";
                }
            }
            return margin;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FeedUnreadNotificationCounterMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            string margin = "";
            if (0 <= count && count <= 9)
            {
                margin = "28,5,0,0";
            }
            else if (10 <= count && count <= 99)
            {
                margin = "27,5,0,0";
            }
            //else if (100 <= count && count <= 999)
            else
            {
                margin = "25,5,0,0";
            }
            return margin;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FeedUnreadNotificationEllipseSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            int size = 0;
            if (0 <= count && count <= 9)
            {
                size = 17;//15;
            }
            else if (10 <= count && count <= 99)
            {
                size = 20;//18;
            }
            // else if (100 <= count && count <= 999)
            else
            {
                size = 21;//22;
            }
            return size;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    //class AddFriendNotificationEllipseSizeConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        int count = (int)value;
    //        int size = 0;
    //        if (0 <= count && count <= 9)
    //        {
    //            size = 20;//15;
    //        }
    //        else if (10 <= count && count <= 99)
    //        {
    //            size = 25;//18;
    //        }
    //        // else if (100 <= count && count <= 999)
    //        else
    //        {
    //            size = 29;//22;
    //        }
    //        return size;
    //    }
    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    class CallHistoryTextConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((int)value[0] == 0 || (int)value[1] == 0) return false;

            ObservableDictionary<string, long> unreadList = (ObservableDictionary<string, long>)value[2];
            ObservableDictionary<string, long> callIDs = (ObservableDictionary<string, long>)value[3];

            return unreadList.Where(P => callIDs.Any(Q => Q.Key.Equals(P.Key))).ToArray().Length > 0;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToCallDivertConver : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isDivert = (bool)value;
            return isDivert;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
=======
﻿using Models.Constants;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Circle;
using View.Utility.DataContainer;
using View.Utility.Images;

namespace View.Converter
{
    class NotificationFrienNameConverter : IValueConverter
    {
        //public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        //{
        //    string txt = "";
        //    NotificationModel model = null;
        //    UserShortInfoModel userShortInfoModel = null;

        //    if (value[0] is UserShortInfoModel && value[1] is NotificationModel)
        //    {
        //        userShortInfoModel = (UserShortInfoModel)value[0];
        //        model = (NotificationModel)value[1];

        //        //if (userShortInfoModel.UserTableID > 0 && !string.IsNullOrEmpty(userShortInfoModel.FullName))
        //        if (!string.IsNullOrEmpty(model.UserShortInfoModel.FullName))
        //        {
        //            //string modifiedFriendName = (userShortInfoModel.FullName.Length > 30) ? userShortInfoModel.FullName.Substring(0, 26) + "...\n" : userShortInfoModel.FullName;
        //            string modifiedFriendName = (model.UserShortInfoModel.FullName.Length > 30) ? model.UserShortInfoModel.FullName.Substring(0, 26) + "...\n" : model.UserShortInfoModel.FullName;
        //            if (model.NumberOfLikeOrComment > 1)
        //            {
        //                if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG || model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG || model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG)
        //                {
        //                    txt = modifiedFriendName;
        //                }
        //                else
        //                {
        //                    //if (model.NumberOfLikeOrComment == 2)
        //                    //{
        //                    //    txt = model.UserShortInfoModel.FullName.Length > 28 ? modifiedFriendName + "and " + (model.NumberOfLikeOrComment - 1) + " other" : modifiedFriendName + " and " + (model.NumberOfLikeOrComment - 1) + " other";
        //                    //}
        //                    //else
        //                    //{
        //                    //    txt = model.UserShortInfoModel.FullName.Length > 28 ? modifiedFriendName + "and " + (model.NumberOfLikeOrComment - 1) + " others" : modifiedFriendName + " and " + (model.NumberOfLikeOrComment - 1) + " others";
        //                    //}
        //                    txt = string.Format("{0} and {1} {2}", model.UserShortInfoModel.FullName.Length > 28 ? model.UserShortInfoModel.FullName : modifiedFriendName, model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other");
        //                }
        //            }
        //            else
        //            {
        //                txt = modifiedFriendName;
        //            }
        //            // txt = modifiedFriendName + " ";
        //        }

        //    }
        //    return txt;
        //}


        //public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        //{
        //    throw new NotImplementedException();
        //}

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string txt = "";
            NotificationModel model = null;
            UserShortInfoModel userShortInfoModel = null;

            if (value is NotificationModel)
            {
                model = (NotificationModel)value;
                userShortInfoModel = model.UserShortInfoModel;
                if (!string.IsNullOrEmpty(model.UserShortInfoModel.FullName))
                {
                    //string modifiedFriendName = (userShortInfoModel.FullName.Length > 30) ? userShortInfoModel.FullName.Substring(0, 26) + "...\n" : userShortInfoModel.FullName;
                    string modifiedFriendName = (model.UserShortInfoModel.FullName.Length > 30) ? model.UserShortInfoModel.FullName.Substring(0, 26) + "...\n" : model.UserShortInfoModel.FullName;
                    if (model.NumberOfLikeOrComment > 1)
                    {
                        if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG || model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG || model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG)
                        {
                            txt = modifiedFriendName;
                        }
                        else
                        {
                            //if (model.NumberOfLikeOrComment == 2)
                            //{
                            //    txt = model.UserShortInfoModel.FullName.Length > 28 ? modifiedFriendName + "and " + (model.NumberOfLikeOrComment - 1) + " other" : modifiedFriendName + " and " + (model.NumberOfLikeOrComment - 1) + " other";
                            //}
                            //else
                            //{
                            //    txt = model.UserShortInfoModel.FullName.Length > 28 ? modifiedFriendName + "and " + (model.NumberOfLikeOrComment - 1) + " others" : modifiedFriendName + " and " + (model.NumberOfLikeOrComment - 1) + " others";
                            //}
                            txt = string.Format("{0} and {1} {2}", model.UserShortInfoModel.FullName.Length > 28 ? model.UserShortInfoModel.FullName : modifiedFriendName, model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other");
                        }
                    }
                    else
                    {
                        txt = modifiedFriendName;
                    }
                    // txt = modifiedFriendName + " ";
                }
            }

            return txt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    class NotificationTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string txt = "";
            //int messageType = (int)value;

            NotificationModel model = null;

            //if (value is NotificationModel)
            //{
            model = (NotificationModel)value;
            switch (model.MessageType)
            {
                case StatusConstants.MESSAGE_UPDATE_PROFILE_IMAGE:
                    txt = NotificationMessages.MSG_UPDATE_PROFILE_IMAGE;
                    break;
                case StatusConstants.MESSAGE_UPDATE_COVER_IMAGE:
                    txt = NotificationMessages.MSG_UPDATE_COVER_IMAGE;
                    break;
                case StatusConstants.MESSAGE_ADD_FRIEND:
                    txt = NotificationMessages.MSG_ADD_FRIEND;
                    break;
                case StatusConstants.MESSAGE_ACCEPT_FRIEND:
                    txt = NotificationMessages.MSG_ACCEPT_FRIEND;
                    break;
                case StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER:
                    string circleName = (!CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.ContainsKey(model.ActivityID)) ? "a Circle" : CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[model.ActivityID].FullName;
                    circleName = circleName.Length > 25 ? string.Format("{0}...", circleName.Substring(0, 20)) : circleName;
                    txt = string.Format(NotificationMessages.MSG_ADD_CIRCLE_MEMBER, circleName);
                    break;
                case StatusConstants.MESSAGE_ADD_STATUS_COMMENT:
                    txt = NotificationMessages.MSG_ADD_STATUS_COMMENT;
                    break;
                case StatusConstants.MESSAGE_LIKE_STATUS:
                    txt = NotificationMessages.MSG_LIKE_STATUS;
                    break;
                case StatusConstants.MESSAGE_LIKE_COMMENT:
                    txt = NotificationMessages.MSG_LIKE_COMMENT;
                    break;
                case StatusConstants.MESSAGE_ADD_COMMENT_ON_COMMENT:
                    txt = NotificationMessages.MSG_ADD_COMMENT_ON_COMMENT;
                    break;
                case StatusConstants.MESSAGE_SHARE_STATUS:
                    txt = NotificationMessages.MSG_SHARE_STATUS;
                    break;
                case StatusConstants.MESSAGE_LIKE_IMAGE:
                    txt = NotificationMessages.MSG_LIKE_IMAGE;
                    break;
                case StatusConstants.MESSAGE_IMAGE_COMMENT:
                    txt = NotificationMessages.MSG_IMAGE_COMMENT;
                    break;
                case StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT:
                    txt = NotificationMessages.MSG_LIKE_IMAGE_COMMENT;
                    break;
                case StatusConstants.MESSAGE_ACCEPT_FRIEND_ACCESS:
                    txt = NotificationMessages.MSG_ACCEPT_FRIEND_ACCESS;
                    break;
                case StatusConstants.MESSAGE_UPGRADE_FRIEND_ACCESS:
                    txt = NotificationMessages.MSG_UPGRADE_FRIEND_ACCESS;
                    break;
                case StatusConstants.MESSAGE_DOWNGRADE_FRIEND_ACCESS:
                    txt = NotificationMessages.MSG_DOWNGRADE_FRIEND_ACCESS;
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG:
                    txt = string.Format("tagged you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
                    //txt = string.Format(" was with you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
                    break;
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA:
                    txt = NotificationMessages.MESSAGE_LIKE_AUDIO_MEDIA;
                    break;
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT:
                    txt = NotificationMessages.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT;
                    break;
                case StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT:
                    txt = NotificationMessages.MESSAGE_AUDIO_MEDIA_COMMENT;
                    break;
                case StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW:
                    txt = NotificationMessages.MESSAGE_AUDIO_MEDIA_VIEW;
                    break;
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA:
                    txt = NotificationMessages.MESSAGE_LIKE_VIDEO_MEDIA;
                    break;
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT:
                    txt = NotificationMessages.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT;
                    break;
                case StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT:
                    txt = NotificationMessages.MESSAGE_VIDEO_MEDIA_COMMENT;
                    break;
                case StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW:
                    txt = NotificationMessages.MESSAGE_VIDEO_MEDIA_VIEW;
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG:
                    txt = string.Format(" was with you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
                    //txt = string.Format("tagged you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG:
                    txt = string.Format("tagged you{0} in a comment", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_LIVE:
                    txt = "is live now";
                    break;
                default:
                    break;
            }

            //if (model.MessageType == StatusConstants.MESSAGE_UPDATE_PROFILE_IMAGE)
            //{
            //    txt = NotificationMessages.MSG_UPDATE_PROFILE_IMAGE;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_UPDATE_COVER_IMAGE)
            //{
            //    txt = NotificationMessages.MSG_UPDATE_COVER_IMAGE;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ADD_FRIEND)
            //{
            //    txt = NotificationMessages.MSG_ADD_FRIEND;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ACCEPT_FRIEND)
            //{
            //    txt = NotificationMessages.MSG_ACCEPT_FRIEND;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER)
            //{
            //    //string circleName = "a Circle";
            //    string circleName = (!CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.ContainsKey(model.ActivityID)) ? "a Circle" : CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[model.ActivityID].FullName;
            //    circleName = circleName.Length > 25 ? string.Format("{0}...", circleName.Substring(0, 20)) : circleName;
            //    txt = string.Format(NotificationMessages.MSG_ADD_CIRCLE_MEMBER, circleName);
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ADD_STATUS_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_ADD_STATUS_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_STATUS)
            //{
            //    txt = NotificationMessages.MSG_LIKE_STATUS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_LIKE_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ADD_COMMENT_ON_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_ADD_COMMENT_ON_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_SHARE_STATUS)
            //{
            //    txt = NotificationMessages.MSG_SHARE_STATUS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_IMAGE)
            //{
            //    txt = NotificationMessages.MSG_LIKE_IMAGE;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_IMAGE_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_IMAGE_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT)
            //{
            //    txt = NotificationMessages.MSG_LIKE_IMAGE_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_ACCEPT_FRIEND_ACCESS)
            //{
            //    txt = NotificationMessages.MSG_ACCEPT_FRIEND_ACCESS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_UPGRADE_FRIEND_ACCESS)
            //{
            //    txt = NotificationMessages.MSG_UPGRADE_FRIEND_ACCESS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_DOWNGRADE_FRIEND_ACCESS)
            //{
            //    txt = NotificationMessages.MSG_DOWNGRADE_FRIEND_ACCESS;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG)
            //{
            //    //txt = NotificationMessages.MESSAGE_YOU_HAVE_BEEN_TAGGED;
            //    txt = string.Format(" was with you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA)
            //{
            //    txt = NotificationMessages.MESSAGE_LIKE_AUDIO_MEDIA;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT)
            //{
            //    txt = NotificationMessages.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT)
            //{
            //    txt = NotificationMessages.MESSAGE_AUDIO_MEDIA_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW)
            //{
            //    txt = NotificationMessages.MESSAGE_AUDIO_MEDIA_VIEW;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA)
            //{
            //    txt = NotificationMessages.MESSAGE_LIKE_VIDEO_MEDIA;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT)
            //{
            //    txt = NotificationMessages.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT)
            //{
            //    txt = NotificationMessages.MESSAGE_VIDEO_MEDIA_COMMENT;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW)
            //{
            //    txt = NotificationMessages.MESSAGE_VIDEO_MEDIA_VIEW;
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG)
            //{
            //    //txt = string.Format("tagged you{0} in a post", model.NumberOfLikeOrComment > 1 ? " and " + (model.NumberOfLikeOrComment - 1).ToString() + " others" : string.Empty);
            //    txt = string.Format("tagged you{0} in a post", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG)
            //{
            //    //txt = string.Format("tagged you{0} in a comment", model.NumberOfLikeOrComment > 1 ? " and " + (model.NumberOfLikeOrComment - 1).ToString() + " others" : string.Empty);
            //    txt = string.Format("tagged you{0} in a comment", model.NumberOfLikeOrComment > 1 ? string.Format(" and {0} {1}", model.NumberOfLikeOrComment - 1, model.NumberOfLikeOrComment > 2 ? "others" : "other") : string.Empty);
            //}
            //else if (model.MessageType == StatusConstants.MESSAGE_NOW_FRIEND)
            //{
            //    txt = NotificationMessages.MSG_NOW_FRIEND;
            //}

            //}
            return txt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationAllReadConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // NotificationModel model = null;
            string time = "";
            long notificationTime = (long)value;
            //if (value is NotificationModel)
            //{
            //model = (NotificationModel)value;
            //time = FeedUtils.GetShowableDate(model.UpdateTime);

            DateTime dt = ModelUtility.GetLocalDateTime(notificationTime);
            DateTime currDt = DateTime.UtcNow;
            string f = dt.ToShortDateString() + ", "; //"8/9/2015"
            string l = dt.ToShortTimeString();//"7:38 PM"

            if (dt.IsSameDateOf(currDt))
            {
                f = "Today, ";
            }
            else if (dt.Year == currDt.Year && dt.DayOfYear == (currDt.DayOfYear - 1))
            {
                f = "Yesterday, ";
            }

            time = (f + l);
            //}

            return time;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    //class PathToNotificationImageConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        if (value == null) return ImageObjects.UNKNOWN_IMAGE;
    //        int imgType = (int)parameter;
    //        string imageUrl = null;
    //        string fullName = String.Empty;
    //        NotificationModel model = null;

    //        if (value is NotificationModel)
    //        {
    //            model = (NotificationModel)value;
    //            imageUrl = model.ImageUrl;
    //            fullName = imgType == ImageUtility.IMG_CROP ? String.Empty : HelperMethods.GetShortName(model.FriendName);
    //        }

    //        if (String.IsNullOrEmpty(imageUrl))
    //        {
    //            if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(model.FriendID) && !string.IsNullOrWhiteSpace(FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[model.FriendID].ProfileImage)) imageUrl = FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[model.FriendID].ProfileImage;
    //            else return ImageUtility.GetDetaultProfileImage(fullName);
    //        }

    //        string convertedUrl = HelperMethods.ConvertUrlTo(imageUrl, imgType);
    //        string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

    //        BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);
    //        if (image == null && model != null && model.FriendID > 0 && convertedUrl != null)
    //        {
    //            ProfileImage obj = new ProfileImage(model.FriendID, convertedUrl);
    //            ImageDictionaries.Instance.FRIENDLIST_PROFILE_IMAGE_QUEUE.AddItem(obj);
    //            if (!String.IsNullOrEmpty(fullName))
    //            {
    //                image = ImageUtility.GetDetaultProfileImage(fullName);
    //            }


    //        }
    //        return image;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    class NotificationCounterTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            int param = parameter != null ? Int32.Parse((string)parameter) : 0;

            string text = count.ToString();

            if (count > 99)
            {
                text = 99 + "+";
            }

            if (param < 0)
            {
                return count > 1 ? count + " " + "New Stickers" : count == 0 ? "Sticker Market" : count + " " + "New Sticker";
            }

            return text;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationEllipseSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            int size = 0;
            if (0 <= count && count <= 9)
            {
                size = 16;
            }
            else if (10 <= count && count <= 99)
            {
                size = 18;
            }
            // else if (100 <= count && count <= 999)
            else
            {
                size = 22;
            }
            return size;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationEllipseLeftConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            int leftPosition = 0;
            if (parameter != null)
            {
                if (0 <= count && count <= 9)
                {
                    leftPosition = -26;
                }
                else if (10 <= count && count <= 99)
                {
                    leftPosition = -24;
                }
                //else if (100 <= count && count <= 999)
                else
                {
                    leftPosition = -22;
                }
            }
            else
            {
                if (0 <= count && count <= 9)
                {
                    leftPosition = -20;
                }
                else if (10 <= count && count <= 99)
                {
                    leftPosition = -18;
                }
                else
                {
                    leftPosition = -16;
                }
            }
            return leftPosition;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class NotificationCounterMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            string margin = "";
            if (parameter != null)
            {
                bool isTopTabNotifications = Boolean.Parse(parameter.ToString());
                if (0 <= count && count <= 9)
                {
                    margin = (isTopTabNotifications) ? "-15,5,0,0" : "41,5,0,0";
                }
                else if (10 <= count && count <= 99)
                {
                    margin = (isTopTabNotifications) ? "-14,6,0,0" : "39,6,0,0";
                }
                //else if (100 <= count && count <= 999)
                else
                {
                    margin = (isTopTabNotifications) ? "-13,8,0,0" : "38,7,0,0";
                }
            }
            else
            {
                if (0 <= count && count <= 9)
                {
                    margin = "-21,5,0,0";
                }
                else if (10 <= count && count <= 99)
                {
                    margin = "-22,6,0,0";
                }
                else
                {
                    margin = "-24,8,0,0";
                }
            }
            return margin;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FeedUnreadNotificationCounterMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            string margin = "";
            if (0 <= count && count <= 9)
            {
                margin = "28,5,0,0";
            }
            else if (10 <= count && count <= 99)
            {
                margin = "27,5,0,0";
            }
            //else if (100 <= count && count <= 999)
            else
            {
                margin = "25,5,0,0";
            }
            return margin;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FeedUnreadNotificationEllipseSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = (int)value;
            int size = 0;
            if (0 <= count && count <= 9)
            {
                size = 17;//15;
            }
            else if (10 <= count && count <= 99)
            {
                size = 20;//18;
            }
            // else if (100 <= count && count <= 999)
            else
            {
                size = 21;//22;
            }
            return size;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    //class AddFriendNotificationEllipseSizeConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        int count = (int)value;
    //        int size = 0;
    //        if (0 <= count && count <= 9)
    //        {
    //            size = 20;//15;
    //        }
    //        else if (10 <= count && count <= 99)
    //        {
    //            size = 25;//18;
    //        }
    //        // else if (100 <= count && count <= 999)
    //        else
    //        {
    //            size = 29;//22;
    //        }
    //        return size;
    //    }
    //    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    class CallHistoryTextConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((int)value[0] == 0 || (int)value[1] == 0) return false;

            ObservableDictionary<string, long> unreadList = (ObservableDictionary<string, long>)value[2];
            ObservableDictionary<string, long> callIDs = (ObservableDictionary<string, long>)value[3];

            return unreadList.Where(P => callIDs.Any(Q => Q.Key.Equals(P.Key))).ToArray().Length > 0;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToCallDivertConver : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isDivert = (bool)value;
            return isDivert;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

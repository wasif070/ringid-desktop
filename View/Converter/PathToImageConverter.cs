﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI;
using View.UI.Profile.FriendProfile;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Images;
using View.ViewModel;

namespace View.Converter
{

    class PathToProfileImageConverter : IMultiValueConverter
    {
        int imageWidth = 115;
        int font = 35;
        private static readonly ILog logger = LogManager.GetLogger(typeof(PathToProfileImageConverter).Name);
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value[0] == null)
                {
                    if (value.Length > 1 && value[1] is string)
                    {
                        string fn = (string)value[1];
                        if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
                    }
                    return ImageObjects.UNKNOWN_IMAGE;
                }
                bool isOpened = value.Length <= 1 || value[1] == null || value[1] is string || (value[1] is bool && (bool)value[1]);

                if (value[0] is BaseUserProfileModel)
                {
                    BaseUserProfileModel model = (BaseUserProfileModel)value[0];
                    int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;
                    string imageUrl = model.ProfileImage;

                    if (isOpened == false || string.IsNullOrWhiteSpace(imageUrl))
                    {
                        if (string.IsNullOrWhiteSpace(model.FullName))
                        {
                            if (value.Length > 1 && value[1] is string)
                            {
                                string fn = (string)value[1];
                                if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
                            }
                            return ImageObjects.UNKNOWN_IMAGE;
                        }
                        if (imgType == ImageUtility.IMG_CROP || imgType == ImageUtility.IMG_THUMB_LARGE)
                        {
                            return ImageUtility.GetDetaultProfileImage(model, imageWidth, imageWidth, font);
                        }
                        return ImageUtility.GetDetaultProfileImage(model);
                    }
                    else
                    {
                        string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, imgType == ImageUtility.IMG_THUMB_LARGE ? ImageUtility.IMG_THUMB : imgType);
                        string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

                        BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl, true);

                        if (image == null && model != null && convertedUrl != null)
                        {
                            if (imgType == ImageUtility.IMG_CROP)
                            {
                                new ThreadDownloadImage(convertedUrl, SettingsConstants.IMAGE_TYPE_PROFILE, model);
                                convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_THUMB);
                                newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
                                image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl, true);

                                if (image == null)
                                {
                                    return ImageUtility.GetDetaultProfileImage(model, imageWidth, imageWidth, font);
                                }
                                else if (image != null)
                                {
                                    return image;
                                }
                            }
                            else
                            {
                                DownloadImageShortInfo obj = new DownloadImageShortInfo(model.UserTableID, convertedUrl, model, SettingsConstants.IMAGE_TYPE_PROFILE);
                                ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                                if (imgType == ImageUtility.IMG_THUMB_LARGE)
                                {
                                    return ImageUtility.GetDetaultProfileImage(model, imageWidth, imageWidth, font);
                                }
                                return ImageUtility.GetDetaultProfileImage(model);
                            }
                        }
                        else
                        {
                            return image;
                        }
                    }
                }
                else if (value[0] is GroupInfoModel)
                {
                    GroupInfoModel model = (GroupInfoModel)value[0];
                    int imgType = parameter != null ? (int)parameter : ImageUtility.IMG_SMALL_LARGE;

                    BitmapImage image = null;
                    if (isOpened == true && model != null && !String.IsNullOrWhiteSpace(model.GroupProfileImage))
                    {
                        string convertedUrl = model.GroupProfileImage.Replace('/', '_');
                        string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
                        image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);
                        if (image == null)
                        {
                            GroupImage obj = new GroupImage(model.GroupID, convertedUrl);
                            obj.Start();
                        }
                    }
                    return image ?? (imgType == ImageUtility.IMG_GROUP_LARGE ? ImageObjects.UNKNOWN_GROUP_LARGE_IMAGE : ImageObjects.UNKNOWN_GROUP_IMAGE);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
            return null;
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToCoverImageConverter : IValueConverter
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PathToCoverImageConverter).Name);
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            try
            {
                if (value is UserBasicInfoModel)
                {
                    UserBasicInfoModel model = (UserBasicInfoModel)value;
                    string imageUrl = model.CoverImage;
                    //int cropX = Math.Abs(model.CropImageX);
                    //int cropY = Math.Abs(model.CropImageY);

                    if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
                    {
                        imageUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_CROP);
                        imageUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
                        /*Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

                        if (bitmap != null)
                        {
                            bitmapImage = ImageUtility.GetResizedOrCroppedCoverImage(bitmap, cropX, cropY);
                        }*/
                        bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(imageUrl, true);
                    }

                    if (bitmapImage == null)
                    {
                        if (!String.IsNullOrWhiteSpace(model.CoverImage))
                        {
                            imageUrl = HelperMethods.ConvertUrlToName(model.CoverImage, ImageUtility.IMG_CROP);
                            CoverImage obj = new CoverImage(model.ShortInfoModel.UserTableID, imageUrl);
                            obj.Start();
                        }

                        bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
                    }
                }
                else if (value is GroupInfoModel)
                {
                    GroupInfoModel model = (GroupInfoModel)value;
                    string imageUrl = model.GroupProfileImage;

                    if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
                    {
                        imageUrl = model.GroupProfileImage.Replace('/', '_');
                        imageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
                        Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

                        if (bitmap != null)
                        {
                            bitmapImage = ImageUtility.GetResizedImageToFit(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);
                        }
                    }

                    if (bitmapImage == null)
                    {
                        if (!String.IsNullOrWhiteSpace(model.GroupProfileImage))
                        {
                            imageUrl = model.GroupProfileImage.Replace('/', '_');
                            GroupImage obj = new GroupImage(model.GroupID, imageUrl);
                            obj.Start();
                        }

                        bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
                    }
                }
                else if (value is NewsPortalModel)
                {
                    NewsPortalModel model = (NewsPortalModel)value;
                    string imageUrl = model.CoverImage;

                    if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
                    {
                        imageUrl = model.CoverImage.Replace('/', '_');
                        imageUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
                        Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

                        if (bitmap != null)
                        {
                            bitmapImage = ImageUtility.GetResizedImageToFit(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);
                        }
                    }

                    if (bitmapImage == null)
                    {
                        if (!String.IsNullOrWhiteSpace(model.CoverImage))
                        {
                            imageUrl = model.CoverImage.Replace('/', '_');
                            CoverImage obj = new CoverImage(model, imageUrl);
                            obj.Start();
                        }

                        bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
                    }
                    //bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
                }
                else if (value is PageInfoModel)
                {
                    PageInfoModel model = (PageInfoModel)value;
                    string imageUrl = model.CoverImage;

                    if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
                    {
                        imageUrl = model.CoverImage.Replace('/', '_');
                        imageUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
                        Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

                        if (bitmap != null)
                        {
                            bitmapImage = ImageUtility.GetResizedImageToFit(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);
                        }
                    }

                    if (bitmapImage == null)
                    {
                        if (!String.IsNullOrWhiteSpace(model.CoverImage))
                        {
                            imageUrl = model.CoverImage.Replace('/', '_');
                            CoverImage obj = new CoverImage(model, imageUrl);
                            obj.Start();
                        }

                        bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
                    }
                    //bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
                }
                else if (value is MusicPageModel)
                {
                    MusicPageModel model = (MusicPageModel)value;
                    string imageUrl = model.CoverImage;

                    if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
                    {
                        imageUrl = model.CoverImage.Replace('/', '_');
                        imageUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
                        Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

                        if (bitmap != null)
                        {
                            bitmapImage = ImageUtility.GetResizedImageToFit(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);
                        }
                    }

                    if (bitmapImage == null)
                    {
                        if (!String.IsNullOrWhiteSpace(model.CoverImage))
                        {
                            imageUrl = model.CoverImage.Replace('/', '_');
                            CoverImage obj = new CoverImage(model, imageUrl);
                            obj.Start();
                        }

                        bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
                    }
                    //bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToAlbumImageConverter : IValueConverter
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PathToAlbumImageConverter).Name);
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            try
            {
                ImageModel model = (ImageModel)value;
                string imageUrl = model.ImageUrl;

                int size = (int)parameter;

                if (model.NoImageFound)
                {
                    bitmapImage = ImageObjects.NO_IMAGE_FOUND;
                }
                else
                {

                    if (!String.IsNullOrEmpty(imageUrl) && imageUrl.Contains("/"))
                    {
                        string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_300);

                        convertedUrl = convertedUrl.Replace("/", "_");

                        //if (model.ImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
                        //{
                        //    convertedUrl = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                        //}
                        if (model.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                        {
                            convertedUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                        }
                        else if (model.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
                        {
                            convertedUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                        }
                        else
                        {
                            convertedUrl = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                        }

                        /*Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(convertedUrl);

                        if (bitmap != null)
                        {
                            bitmapImage = ImageUtility.GetResizedImage(bitmap, size, size);
                        }*/
                        bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(convertedUrl);
                    }

                    if (bitmapImage == null)
                    {
                        if (!String.IsNullOrEmpty(model.ImageUrl))
                        {
                            imageUrl = HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_300);
                            if (!model.IsIMG300Downloading)
                            {
                                model.IsIMG300Downloading = true;
                                AlbumImage obj = new AlbumImage(model.FriendUserTableID, model.ImageId, imageUrl, model.ImageType);
                                obj.Start();
                            }
                            bitmapImage = ImageObjects.PHOTO_LOADING;//bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
                            //GC.SuppressFinalize(bitmapImage);
                        }
                    }
                    else
                    {
                        model.StrechValue = Stretch.UniformToFill;
                    }
                }
            }
            catch (Exception ex) { logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToAlbumCoverImageConverter : IValueConverter
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PathToAlbumImageConverter).Name);
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            try
            {
                AlbumModel model = (AlbumModel)value;
                string imageUrl = model.AlbumCoverImageUrl;

                int size = (int)parameter;

                if (model.NoImageFound)
                {
                    bitmapImage = ImageObjects.NO_IMAGE_FOUND;
                }
                else
                {

                    if (!String.IsNullOrEmpty(imageUrl) && imageUrl.Contains("/"))
                    {
                        string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_300);

                        convertedUrl = convertedUrl.Replace("/", "_");
                        convertedUrl = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;

                        bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(convertedUrl);
                    }

                    if (bitmapImage == null)
                    {
                        if (!String.IsNullOrEmpty(model.AlbumCoverImageUrl))
                        {
                            imageUrl = HelperMethods.ConvertUrlToName(model.AlbumCoverImageUrl, ImageUtility.IMG_300);
                            AlbumCoverImage obj = new AlbumCoverImage(model.UserTableID == DefaultSettings.LOGIN_TABLE_ID ? 0 : model.UserTableID, model.AlbumId, imageUrl);
                            obj.Start();
                        }
                        bitmapImage = ImageObjects.PHOTO_ALBUM_COVER_IMAGE;
                    }
                }
            }
            catch (Exception ex) { logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToMediaAlbumImageConverter : IValueConverter
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PathToMediaAlbumImageConverter).Name);
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value is MediaContentModel)
                {
                    MediaContentModel model = (MediaContentModel)value;
                    string imageUrl = model.AlbumImageUrl;
                    if (string.IsNullOrEmpty(imageUrl) || model.DownloadFailed)
                    {
                        return (model.MediaType == 1) ? ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE : ImageObjects.DEFAULT_VIDEO_ALBUM_IMAGE;
                    }
                    else
                    {
                        string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + imageUrl.Replace("/", "_");
                        if (File.Exists(filePathImage))
                        {
                            byte[] imageFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                            if (imageFileBytes != null && imageFileBytes.Length > 1)
                            {
                                BitmapImage bitmapImage = ImageUtility.ConvertByteArrayToBitmapImage(imageFileBytes);
                                GC.SuppressFinalize(bitmapImage);
                                return bitmapImage;
                            }

                            //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                            //bitmapImage = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                            //imageBytes = null;
                            //GC.SuppressFinalize(bitmapImage);
                            //return bitmapImage;
                        }
                        else
                        {
                            new MediaImage(model, filePathImage).Start();
                        }
                    }
                }
            }
            catch (Exception ex) { logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToSingleAlbumImageConverter : IValueConverter
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PathToSingleAlbumImageConverter).Name);
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value is SingleMediaModel)
                {
                    SingleMediaModel model = (SingleMediaModel)value;
                    string imageUrl = model.ThumbUrl;
                    if (string.IsNullOrEmpty(imageUrl) || model.NoImageFound)
                    {
                        return (model.MediaType == 1) ? ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE : ImageObjects.DEFAULT_VIDEO_ALBUM_IMAGE;
                    }
                    else
                    {
                        string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + imageUrl.Replace("/", "_");
                        if (!(filePathImage.EndsWith(".jpg") || filePathImage.EndsWith(".png"))) filePathImage = filePathImage + ".jpg";
                        if (File.Exists(filePathImage))
                        {
                            byte[] oFileBytes = ImageUtility.GetByteArrayFromFilePath(filePathImage);
                            if (oFileBytes != null && oFileBytes.Length > 1)
                            {
                                BitmapImage bitmapImage = ImageUtility.ConvertByteArrayToBitmapImage(oFileBytes);
                                GC.SuppressFinalize(bitmapImage);
                                return bitmapImage;
                            }

                            //byte[] imageBytes = File.ReadAllBytes(filePathImage);
                            //bitmapImage = ImageUtility.convertByteArrayToBitmapImage(imageBytes);
                            //imageBytes = null;
                            //GC.SuppressFinalize(bitmapImage);
                            //return bitmapImage;
                        }
                        else
                        {
                            new MediaImage(model, filePathImage).Start();
                        }
                        //else if (ServerAndPortSettings.GetVODThumbImageUpResourceURL != null && DefaultSettings.IsInternetAvailable)
                        //{
                        //    new MediaImage(model, filePathImage).Start();
                        //}
                    }
                }
            }
            catch (Exception ex) { logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class SingleMediaContentCheckConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                string str = (string)value;
                return string.IsNullOrEmpty(str) ? Visibility.Collapsed : Visibility.Visible;
            }
            else if (value is long && parameter != null)
            {
                long mediaPublishTime = long.Parse(value.ToString());
                int param = Int32.Parse(parameter.ToString());
                if (param == 1)
                {
                    if (View.Utility.Chat.Service.ChatService.GetServerTime() < mediaPublishTime)
                    {
                        return Visibility.Visible;
                    }
                    else
                    {
                        return Visibility.Collapsed;
                    }
                }
                else if (param == 2)
                {
                    if (View.Utility.Chat.Service.ChatService.GetServerTime() > mediaPublishTime)
                    {
                        return Visibility.Visible;
                    }
                    else
                    {
                        return Visibility.Collapsed;
                    }
                }
            }
            return Visibility.Collapsed;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class SymbolToEmoticonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bmpImage = ImageObjects.EMOTICON_ICON.TryGetValue(value.ToString());
            if (bmpImage == null)
            {
                EmoticonDTO entry = DefaultDictionaries.Instance.EMOTICON_DICTIONARY[value.ToString()];
                if (entry != null)
                {
                    bmpImage = ImageUtility.GetBitmapImageOfDynamicResource(RingIDSettings.EMOTICON_FOLDER + Path.DirectorySeparatorChar + "dmid" + Path.DirectorySeparatorChar + entry.Url);
                    if (bmpImage != null)
                    {
                        ImageObjects.EMOTICON_ICON[entry.Symbol] = bmpImage;
                    }
                }
            }
            return bmpImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToDoingImageConverter : IValueConverter
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PathToDoingImageConverter).Name);
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value is string && parameter is string)
                {
                    int size = System.Convert.ToInt32((string)parameter);
                    string imgUrl = (string)value;
                    if (!string.IsNullOrEmpty(imgUrl))
                    {
                        string filePath = (size == 0) ? (RingIDSettings.TEMP_DOING_MINI_FOLDER + "/" + imgUrl) : (RingIDSettings.TEMP_DOING_FULL_FOLDER + "/" + imgUrl);
                        if (File.Exists(filePath) && HelperMethods.IsValidImageFile(filePath, size))
                        {
                            Bitmap bmp = (Bitmap)Image.FromFile(filePath);
                            BitmapImage bi = ImageUtility.ConvertBitmapToBitmapImageAsPNG(bmp);
                            if (bmp != null)
                            {
                                bmp.Dispose();
                            }
                            return bi;
                        }
                        else
                        {
                            BitmapImage bitmap = new BitmapImage();
                            bitmap.BeginInit();
                            bitmap.CacheOption = BitmapCacheOption.OnLoad;
                            string url = (size == 0) ? (ServerAndPortSettings.GetFeelingMiniURL + imgUrl) : (ServerAndPortSettings.GetFeelingFullURL + imgUrl);
                            bitmap.UriSource = new Uri(url, UriKind.Absolute);
                            bitmap.EndInit();
                            if (bitmap != null && !ImageDictionaries.Instance.IMAGES_IN_DOWNLOADING_STATE.Contains(url))
                            {
                                Bitmap bmp = ImageUtility.ConvertBitmapImageToBitmap(bitmap);
                                if (bmp != null) //ImageUtility.SaveBitMapInLocalFolderAsPNG(filePath, bmp);
                                {
                                    if (File.Exists(filePath)) File.Delete(filePath);
                                    bmp.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);
                                }
                            }
                            return bitmap;
                        }
                    }
                }
            }
            catch (Exception ex) { logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToMdediaAlbumListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ImageLocation.DEFAULT_VIDEO_SMALL;
            //if (value is string)
            //{
            //    string imgUrl = (string)value;
            //    string imgNm = RingIDSettings.TEMP_DOING_FOLDER + Path.DirectorySeparatorChar + imgUrl.Replace("/", "_");
            //    if (File.Exists(imgNm))
            //    {
            //        Bitmap bmp = (Bitmap)Image.FromFile(@imgNm);
            //        BitmapImage bi = ImageUtility.ConvertBitmapToBitmapImageAsPNG(bmp);
            //        if (bmp != null)
            //        {
            //            bmp.Dispose();
            //        }
            //        return bi;
            //    }
            //    else
            //    {
            //        BitmapImage bitmap = new BitmapImage();
            //        bitmap.BeginInit();
            //        bitmap.UriSource = new Uri(ServerAndPortSettings.GetImageServer() + imgUrl, UriKind.Absolute);
            //        //bitmap.DecodePixelHeight = 150;
            //        //bitmap.DecodePixelWidth = 150;
            //        bitmap.EndInit();
            //        return bitmap;
            //    }
            //}
            //return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class DirectPathToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return null;
            try
            {
                string imageUrl = value.ToString().Trim();
                if (!String.IsNullOrEmpty(imageUrl) && File.Exists(imageUrl))
                {
                    var bi = new BitmapImage();
                    bi.BeginInit();
                    bi.CacheOption = BitmapCacheOption.OnLoad;
                    //bi.CacheOption = BitmapCacheOption.None;
                    bi.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                    bi.DecodePixelWidth = 131;
                    //bi.DecodePixelHeight = 100;
                    bi.UriSource = new Uri(imageUrl, UriKind.Absolute);
                    bi.EndInit();
                    bi.Freeze();
                    return bi;
                }
            }
            catch (Exception) { }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class FriendFeedImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            try
            {
                if (value is Guid)
                {
                    Guid previewPhotoId = (Guid)value;
                    ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(previewPhotoId);
                    if (previewPhotoId != Guid.Empty && model != null)
                    {
                        UCFriendProfile _UCFriendProfile = null;
                        if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == model.UserShortInfoModel.UserTableID)
                        {
                            _UCFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                            if (model.NoImageFound)
                            {
                                _UCFriendProfile.NoImageFound = true;
                                bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.PHOTOS);
                                GC.SuppressFinalize(bitmapImage);
                                return bitmapImage;
                                //return ImageObjects.PHOTOS;
                            }
                            else
                            {
                                string imageUrl = model.ImageUrl;
                                if (!String.IsNullOrEmpty(imageUrl) && imageUrl.Contains("/"))
                                {
                                    string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_300);

                                    convertedUrl = convertedUrl.Replace("/", "_");

                                    if (model.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                                    {
                                        convertedUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                                    }
                                    else if (model.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
                                    {
                                        convertedUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                                    }
                                    else
                                    {
                                        convertedUrl = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                                    }

                                    bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(convertedUrl);

                                    if (bitmapImage != null)
                                    {
                                        _UCFriendProfile.NoImageFound = false;
                                    }
                                    else
                                    {

                                        imageUrl = HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_300);
                                        if (!model.IsIMG300Downloading)
                                        {
                                            model.IsIMG300Downloading = true;
                                            AlbumImage obj = new AlbumImage(model.FriendUserTableID, model.ImageId, imageUrl, model.ImageType);
                                            obj.Start();
                                        }
                                        _UCFriendProfile.NoImageFound = true;
                                        bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.PHOTOS);
                                        GC.SuppressFinalize(bitmapImage);
                                        return bitmapImage;
                                        //return ImageObjects.PHOTOS;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.PHOTOS);
                        GC.SuppressFinalize(bitmapImage);
                        return bitmapImage;
                        //return ImageObjects.PHOTOS;
                    }
                }
            }
            catch (Exception) { }
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToFriendProfileImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            try
            {
                if (value is UserShortInfoModel)
                {
                    UserShortInfoModel model = (UserShortInfoModel)value;
                    if (model != null)
                    {
                        if (!String.IsNullOrEmpty(model.ProfileImage))
                        {
                            string convertedUrl = HelperMethods.ConvertUrlToName(model.ProfileImage, ImageUtility.IMG_THUMB);
                            string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
                            bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

                            if (bitmapImage == null && convertedUrl != null)
                            {
                                ProfileImage obj = new ProfileImage(model.UserTableID, convertedUrl, model);
                                obj.Start();
                                bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE_LARGE);
                                GC.SuppressFinalize(bitmapImage);
                                return bitmapImage;
                                //return ImageObjects.UNKNOWN_IMAGE_LARGE;
                            }
                            else
                            {
                                GC.SuppressFinalize(bitmapImage);
                                return bitmapImage;
                            }
                        }
                        else
                        {
                            bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE_LARGE);
                            GC.SuppressFinalize(bitmapImage);
                            return bitmapImage;
                            //return ImageObjects.UNKNOWN_IMAGE_LARGE;
                        }
                    }
                    else
                    {
                        bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE_LARGE);
                        GC.SuppressFinalize(bitmapImage);
                        return bitmapImage;
                        //return ImageObjects.UNKNOWN_IMAGE_LARGE;
                    }
                }
            }
            catch (Exception) { }
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToMutualFriendProfileImageConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            try
            {
                if (value[0] is UserShortInfoModel && value[1] is long)
                {
                    UserShortInfoModel model = (UserShortInfoModel)value[0];
                    long friendUserTableId = (long)value[1];
                    UCFriendProfile _UCFriendProfile = null;
                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == friendUserTableId)
                    {
                        _UCFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                    }

                    if (model != null)
                    {
                        if (!String.IsNullOrEmpty(model.ProfileImage))
                        {
                            string convertedUrl = HelperMethods.ConvertUrlToName(model.ProfileImage, ImageUtility.IMG_THUMB);
                            string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
                            bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

                            if (bitmapImage == null && convertedUrl != null && model.UserTableID > 0)
                            {
                                ProfileImage obj = new ProfileImage(model.UserTableID, convertedUrl, model, friendUserTableId);
                                obj.Start();
                                bitmapImage = ImageUtility.GetDetaultProfileImage(model);
                                GC.SuppressFinalize(bitmapImage);
                            }
                            if (_UCFriendProfile != null)
                            {
                                if (_UCFriendProfile.UserShortInfoModelOfMutualFriendCircle2 != null && _UCFriendProfile.UserShortInfoModelOfMutualFriendCircle2.ProfileImage == model.ProfileImage)
                                {
                                    _UCFriendProfile.IsCircle2Visible = true;
                                    _UCFriendProfile.MutualFriendGap = 72;
                                }
                                else if (_UCFriendProfile.UserShortInfoModelOfMutualFriendCircle3 != null && _UCFriendProfile.UserShortInfoModelOfMutualFriendCircle3.ProfileImage == model.ProfileImage)
                                {
                                    _UCFriendProfile.IsCircle3Visible = true;
                                    _UCFriendProfile.MutualFriendGap = 88;
                                }
                            }
                            return bitmapImage;
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(model.FullName))
                            {
                                string fullName = HelperMethods.GetShortName(model.FullName);
                                if (_UCFriendProfile != null)
                                {
                                    if (_UCFriendProfile.ProfileImageVisibilityValue == 1)
                                    {
                                        _UCFriendProfile.IsCircle2Visible = true;
                                        _UCFriendProfile.MutualFriendGap = 72;
                                    }
                                    else if (_UCFriendProfile.ProfileImageVisibilityValue == 2)
                                    {
                                        _UCFriendProfile.IsCircle3Visible = true;
                                        _UCFriendProfile.MutualFriendGap = 88;
                                    }
                                    _UCFriendProfile.ProfileImageVisibilityValue++;
                                }
                                bitmapImage = ImageUtility.GetDetaultProfileImage(model);
                                GC.SuppressFinalize(bitmapImage);
                                return bitmapImage;
                            }
                        }
                        return bitmapImage;
                    }
                    else
                    {
                        bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND);
                        GC.SuppressFinalize(bitmapImage);
                        return bitmapImage;
                        //return ImageObjects.UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND;
                    }
                }
            }
            catch (Exception) { }
            bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE_LARGE);
            GC.SuppressFinalize(bitmapImage);
            return bitmapImage;
            //return ImageObjects.UNKNOWN_IMAGE_LARGE;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToUnknownProfileImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bmp = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE_LARGE);
            GC.SuppressFinalize(bmp);
            return bmp;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class MyProfileFeedImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = null;
            try
            {
                if (value is Guid)
                {
                    Guid previewPhotoId = (Guid)value;
                    ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(previewPhotoId);
                    if (previewPhotoId != Guid.Empty && model != null)
                    {
                        if (model.NoImageFound)
                        {
                            RingIDViewModel.Instance.MyFeedNoImageFound = true;
                            BitmapImage bmp = ImageUtility.GetBitmapImage(ImageLocation.PHOTOS);
                            GC.SuppressFinalize(bmp);
                            return bmp;
                            //return ImageObjects.PHOTOS;
                        }
                        else
                        {
                            string imageUrl = model.ImageUrl;
                            if (!String.IsNullOrEmpty(imageUrl) && imageUrl.Contains("/"))
                            {
                                string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_300);

                                convertedUrl = convertedUrl.Replace("/", "_");

                                if (model.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                                {
                                    convertedUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                                }
                                else if (model.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
                                {
                                    convertedUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                                }
                                else
                                {
                                    convertedUrl = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                                }

                                bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(convertedUrl);

                                if (bitmapImage != null)
                                {
                                    RingIDViewModel.Instance.MyFeedNoImageFound = false;
                                }
                                else
                                {
                                    imageUrl = HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_300);
                                    if (!model.IsIMG300Downloading)
                                    {
                                        model.IsIMG300Downloading = true;
                                        AlbumImage obj = new AlbumImage(0, model.ImageId, imageUrl, model.ImageType);
                                        obj.Start();
                                    }
                                    RingIDViewModel.Instance.MyFeedNoImageFound = true;
                                    BitmapImage bmp = ImageUtility.GetBitmapImage(ImageLocation.PHOTOS);
                                    GC.SuppressFinalize(bmp);
                                    return bmp;
                                    //return ImageObjects.PHOTOS;
                                }
                            }
                        }
                    }
                    else
                    {
                        RingIDViewModel.Instance.MyFeedNoImageFound = true;
                        BitmapImage bmp = ImageUtility.GetBitmapImage(ImageLocation.PHOTOS);
                        GC.SuppressFinalize(bmp);
                        return bmp;
                        //return ImageObjects.PHOTOS;
                    }
                }
            }
            catch (Exception) { }
            return bitmapImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToChatProfileImageConverter : IValueConverter
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PathToChatProfileImageConverter).Name);
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                long userTableID = 0;
                string profileImage = String.Empty;
                string fullName = String.Empty;
                UserShortInfoModel friendModel = null;
                MessageModel messageModel = null;

                if (value is UserShortInfoModel)
                {
                    friendModel = (UserShortInfoModel)value;
                    userTableID = friendModel.UserTableID;
                    fullName = friendModel.FullName;
                    profileImage = friendModel.ProfileImage ?? String.Empty;
                }
                else
                {
                    messageModel = (MessageModel)value;
                    userTableID = messageModel.SenderTableID;
                    fullName = messageModel.FullName;
                    profileImage = messageModel.ProfileImage != null && Path.HasExtension(messageModel.ProfileImage) ? messageModel.ProfileImage : String.Empty;
                }

                KeyValuePair<string, BitmapImage> pair = new KeyValuePair<string, BitmapImage>();

                if (UIDictionaries.Instance.CHAT_PROFILE_IMAGE_DICTIONARY.TryGetValue(userTableID, out pair) && pair.Key.Equals(profileImage))
                {
                    return pair.Value;
                }
                else
                {
                    BitmapImage bitmapImage = null;
                    if (String.IsNullOrWhiteSpace(profileImage))
                    {
                        if (string.IsNullOrWhiteSpace(fullName))
                        {
                            bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE);
                        }
                        else
                        {
                            if (friendModel != null)
                            {
                                bitmapImage = ImageUtility.GetDetaultProfileImage(friendModel);
                            }
                            else
                            {
                                bitmapImage = ImageUtility.GetDetaultProfileImage(messageModel);
                            }
                        }
                    }
                    else
                    {
                        string convertedUrl = HelperMethods.ConvertUrlToName(profileImage, ImageUtility.IMG_THUMB);
                        string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

                        try
                        {
                            bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);
                        }
                        catch (Exception ex)
                        {
                        }

                        if (bitmapImage == null)
                        {
                            if (friendModel != null)
                            {
                                DownloadImageShortInfo obj = new DownloadImageShortInfo(userTableID, convertedUrl, friendModel, SettingsConstants.IMAGE_TYPE_PROFILE);
                                ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                            }
                            else
                            {
                                DownloadImageShortInfo obj = new DownloadImageShortInfo(userTableID, convertedUrl, profileImage, messageModel, SettingsConstants.IMAGE_TYPE_CHAT_PROFILE);
                                ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                            }

                            if (string.IsNullOrWhiteSpace(fullName))
                            {
                                bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE);
                            }
                            else
                            {
                                if (friendModel != null)
                                {
                                    bitmapImage = ImageUtility.GetDetaultProfileImage(friendModel);
                                }
                                else
                                {
                                    bitmapImage = ImageUtility.GetDetaultProfileImage(messageModel);
                                }
                            }
                            return bitmapImage;
                        }
                    }

                    bitmapImage.Freeze();
                    UIDictionaries.Instance.CHAT_PROFILE_IMAGE_DICTIONARY[userTableID] = new KeyValuePair<string, BitmapImage>(profileImage, bitmapImage);

                    return bitmapImage;
                }
            }

            return ImageObjects.UNKNOWN_IMAGE;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToRoomImageConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            string roomId = value[0] as String;
            string profileImage = value[1] as String;
            bool isOpened = value.Length <= 2 || value[2] == null || value[2] is string || (value[2] is bool && (bool)value[2]);

            if (isOpened && !String.IsNullOrWhiteSpace(roomId))
            {
                BitmapImage bitmapImage = null;
                if (!String.IsNullOrWhiteSpace(profileImage))
                {
                    string convertedUrl = HelperMethods.ConvertUrlToName(profileImage, ImageUtility.IMG_FULL);
                    string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

                    if (new ImageFile(newImageUrl).IsComplete)
                    {
                        bitmapImage = new BitmapImage();
                        bitmapImage.BeginInit();
                        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                        bitmapImage.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
                        bitmapImage.DecodePixelHeight = parameter == null ? 400 : (bool)parameter ? 100 : 150;
                        bitmapImage.UriSource = new Uri(newImageUrl, UriKind.Absolute);
                        bitmapImage.EndInit();
                        bitmapImage.Freeze();
                        return bitmapImage;
                    }
                    else
                    {
                        DownloadImageShortInfo obj = new DownloadImageShortInfo(roomId, convertedUrl, profileImage, SettingsConstants.IMAGE_TYPE_ROOM);
                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                    }
                }
            }

            return parameter == null ? null : (bool)parameter ? ImageObjects.UNKNOWN_ROOM_IMAGE : ImageObjects.ROOM_DEFAULT_IMAGE;
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToRoomMemberImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            RoomMemberModel memberModel = value as RoomMemberModel;
            BitmapImage bitmapImage = null;

            if (memberModel != null)
            {
                string fullName = memberModel.FullName;
                string profileImage = memberModel.ProfileImageUrl;
                bool showUnkhownDefault = parameter != null && (bool)parameter;

                if (!String.IsNullOrWhiteSpace(profileImage))
                {
                    string convertedUrl = HelperMethods.ConvertUrlToName(profileImage, ImageUtility.IMG_FULL);
                    string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

                    bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);
                    if (bitmapImage == null)
                    {
                        DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedUrl, profileImage, memberModel, SettingsConstants.IMAGE_TYPE_ROOM_MEMBER);
                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                        if (showUnkhownDefault)
                        {
                            bitmapImage = ImageObjects.UNKNOWN_IMAGE_LARGE;
                        }
                        else
                        {
                            bitmapImage = ImageUtility.GetDetaultProfileImage(memberModel.MemberID, fullName);
                        }
                    }
                }
                else
                {
                    if (showUnkhownDefault)
                    {
                        bitmapImage = ImageObjects.UNKNOWN_IMAGE_LARGE;
                    }
                    else
                    {
                        bitmapImage = ImageUtility.GetDetaultProfileImage(memberModel.MemberID, fullName);
                    }
                }
            }
            else
            {
                bitmapImage = ImageObjects.UNKNOWN_IMAGE_LARGE;
            }

            return bitmapImage;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToLoaderClockImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                Visibility val = (Visibility)value;
                if (val == Visibility.Visible)
                {
                    BitmapImage bmp = ImageUtility.GetBitmapImage(ImageLocation.LOADER_CLOCK);
                    GC.SuppressFinalize(bmp);
                    return bmp;
                }
            }
            catch (Exception) { }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class LinkToBaseUrlConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                string linkUrl = (string)value;
                if (!string.IsNullOrWhiteSpace(linkUrl))
                {
                    var uri = new Uri(linkUrl);
                    string baseUri = uri.GetLeftPart(System.UriPartial.Authority);
                    return baseUri;
                }
            }
            catch (Exception) { }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class PathToWalletReferralImageConverter : IMultiValueConverter
    {
        int imageWidth = 115;
        int font = 35;
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value[0] == null)
                {
                    if (value.Length > 1 && value[1] is string)
                    {
                        string fn = (string)value[1];
                        if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
                    }
                    return ImageObjects.UNKNOWN_IMAGE;
                }
                bool isOpened = value.Length <= 1 || value[1] == null || value[1] is string || (value[1] is bool && (bool)value[1]);

                #region UserShortInfoModel
                if (value[0] is UserBasicInfoModel)
                {
                    //WalletReferralModel wModel = (WalletReferralModel)value[0];
                    UserBasicInfoModel wModel = (UserBasicInfoModel)value[0];
                    //UserShortInfoModel model = new UserShortInfoModel
                    //{
                    //    UserIdentity = wModel.UserIdentity,
                    //    UserTableID = wModel.UserTableID,
                    //    FullName = wModel.FullName,
                    //    ProfileImage = wModel.ProfileImage
                    //};
                    UserShortInfoModel model = wModel.ShortInfoModel;
                    //UserShortInfoModel model = (UserShortInfoModel)value[0];

                    int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

                    string imageUrl = model.ProfileImage;
                    //string fullName = HelperMethods.GetShortName(model.FullName); /*imgType == ImageUtility.IMG_CROP ? String.Empty : HelperMethods.GetShortName(model.FullName);*/

                    if (isOpened == false || String.IsNullOrWhiteSpace(imageUrl))
                    {
                        if (string.IsNullOrWhiteSpace(model.FullName))
                        {
                            if (value.Length > 1 && value[1] is string)
                            {
                                string fn = (string)value[1];
                                if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
                            }
                            return ImageObjects.UNKNOWN_IMAGE;
                        }
                        if (imgType == ImageUtility.IMG_CROP || imgType == ImageUtility.IMG_THUMB_LARGE)
                        {
                            return ImageUtility.GetDetaultProfileImage(model, imageWidth, imageWidth, font);
                        }
                        return ImageUtility.GetDetaultProfileImage(model);
                    }
                    else
                    {
                        string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, imgType == ImageUtility.IMG_THUMB_LARGE ? ImageUtility.IMG_THUMB : imgType);
                        string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

                        BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

                        if (image == null && model != null && convertedUrl != null)
                        {
                            if (imgType == ImageUtility.IMG_CROP)
                            {
                                ProfileImage obj = new ProfileImage(model.UserTableID, convertedUrl, model);
                                obj.Start();
                                convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_THUMB);
                                newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
                                image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

                                if (image == null)
                                {
                                    return ImageUtility.GetDetaultProfileImage(model, imageWidth, imageWidth, font);
                                }
                                else if (image != null)
                                {
                                    return image;
                                }
                            }
                            else
                            {
                                DownloadImageShortInfo obj = new DownloadImageShortInfo(model.UserTableID, convertedUrl, model, SettingsConstants.IMAGE_TYPE_PROFILE);
                                ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                                // if (!String.IsNullOrEmpty(model.FullName))
                                //{
                                if (imgType == ImageUtility.IMG_THUMB_LARGE)
                                {
                                    return ImageUtility.GetDetaultProfileImage(model, imageWidth, imageWidth, font);
                                }
                                return ImageUtility.GetDetaultProfileImage(model);
                                // }   
                            }
                        }
                        else
                        {
                            return image;
                        }
                    }
                }
                #endregion


            }
            catch (Exception ex)
            {
                // logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
//public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
//{
//    try
//    {
//        if (value[0] == null)
//        {
//            if (value.Length > 1 && value[1] is string)
//            {
//                string fn = (string)value[1];
//                if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
//            }
//            return ImageObjects.UNKNOWN_IMAGE;
//        }
//        bool isOpened = value.Length <= 1 || value[1] == null || value[1] is string || (value[1] is bool && (bool)value[1]);

//        #region UserShortInfoModel
//        if (value[0] is UserShortInfoModel)
//        {
//            UserShortInfoModel model = (UserShortInfoModel)value[0];

//            int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

//            string imageUrl = model.ProfileImage;
//            //string fullName = HelperMethods.GetShortName(model.FullName); /*imgType == ImageUtility.IMG_CROP ? String.Empty : HelperMethods.GetShortName(model.FullName);*/

//            if (isOpened == false || String.IsNullOrWhiteSpace(imageUrl))
//            {
//                if (string.IsNullOrWhiteSpace(model.FullName))
//                {
//                    if (value.Length > 1 && value[1] is string)
//                    {
//                        string fn = (string)value[1];
//                        if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
//                    }
//                    return ImageObjects.UNKNOWN_IMAGE;
//                }
//                if (imgType == ImageUtility.IMG_CROP || imgType == ImageUtility.IMG_THUMB_LARGE)
//                {
//                    return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                }
//                return ImageUtility.GetDetaultProfileImage(model);
//            }
//            else
//            {
//                string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, imgType == ImageUtility.IMG_THUMB_LARGE ? ImageUtility.IMG_THUMB : imgType);
//                string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

//                BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                if (image == null && model != null && convertedUrl != null)
//                {
//                    if (imgType == ImageUtility.IMG_CROP)
//                    {
//                        ProfileImage obj = new ProfileImage(model.UserTableID, convertedUrl, model);
//                        obj.Start();
//                        convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_THUMB);
//                        newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
//                        image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                        if (image == null)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        else if (image != null)
//                        {
//                            return image;
//                        }
//                    }
//                    else
//                    {
//                        DownloadImageShortInfo obj = new DownloadImageShortInfo(model.UserTableID, convertedUrl, model, SettingsConstants.IMAGE_TYPE_PROFILE);
//                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
//                        // if (!String.IsNullOrEmpty(model.FullName))
//                        //{
//                        if (imgType == ImageUtility.IMG_THUMB_LARGE)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        return ImageUtility.GetDetaultProfileImage(model);
//                        // }   
//                    }
//                }
//                else
//                {
//                    return image;
//                }
//            }
//        }
//        #endregion

//        #region GroupInfoModel
//        else if (value[0] is GroupInfoModel)
//        {
//            GroupInfoModel model = (GroupInfoModel)value[0];
//            int imgType = parameter != null ? (int)parameter : ImageUtility.IMG_SMALL_LARGE;

//            BitmapImage image = null;
//            if (isOpened == true && model != null && !String.IsNullOrWhiteSpace(model.GroupProfileImage))
//            {
//                string convertedUrl = model.GroupProfileImage.Replace('/', '_');
//                string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
//                image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);
//                if (image == null)
//                {
//                    GroupImage obj = new GroupImage(model.GroupID, convertedUrl);
//                    obj.Start();
//                }
//            }
//            return image ?? (imgType == ImageUtility.IMG_GROUP_LARGE ? ImageObjects.UNKNOWN_GROUP_LARGE_IMAGE : ImageObjects.UNKNOWN_GROUP_IMAGE);
//        }
//        #endregion

//        #region NewsPortalModel
//        else if (value[0] is NewsPortalModel)
//        {
//            NewsPortalModel model = (NewsPortalModel)value[0];
//            int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

//            string imageUrl = model.ProfileImage;
//            //string fullName = HelperMethods.GetShortName(model.FullName); /*imgType == ImageUtility.IMG_CROP ? String.Empty : HelperMethods.GetShortName(model.FullName);*/

//            if (isOpened == false || String.IsNullOrWhiteSpace(imageUrl))
//            {
//                if (string.IsNullOrWhiteSpace(model.FullName))
//                {
//                    if (value.Length > 1 && value[1] is string)
//                    {
//                        string fn = (string)value[1];
//                        if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
//                    }
//                    return ImageObjects.UNKNOWN_IMAGE;
//                }
//                if (imgType == ImageUtility.IMG_CROP || imgType == ImageUtility.IMG_THUMB_LARGE)
//                {
//                    return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                }
//                return ImageUtility.GetDetaultProfileImage(model);
//            }
//            else
//            {
//                string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, imgType == ImageUtility.IMG_THUMB_LARGE ? ImageUtility.IMG_THUMB : imgType);
//                string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

//                BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                if (image == null && model != null && convertedUrl != null)
//                {
//                    if (imgType == ImageUtility.IMG_CROP)
//                    {
//                        ProfileImage obj = new ProfileImage(model.UserTableID, convertedUrl, model);
//                        obj.Start();
//                        convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_THUMB);
//                        newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
//                        image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                        if (image == null)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        else if (image != null)
//                        {
//                            return image;
//                        }
//                    }
//                    else
//                    {
//                        DownloadImageShortInfo obj = new DownloadImageShortInfo(model.UserTableID, convertedUrl, model, SettingsConstants.IMAGE_TYPE_PROFILE);
//                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
//                        // if (!String.IsNullOrEmpty(model.FullName))
//                        //{
//                        if (imgType == ImageUtility.IMG_THUMB_LARGE)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        return ImageUtility.GetDetaultProfileImage(model);
//                        // }   
//                    }
//                }
//                else
//                {
//                    return image;
//                }
//            }
//        }
//        #endregion

//        #region PageInfoModel
//        else if (value[0] is PageInfoModel)
//        {
//            PageInfoModel model = (PageInfoModel)value[0];
//            int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

//            string imageUrl = model.ProfileImage;
//            //string fullName = HelperMethods.GetShortName(model.FullName); /*imgType == ImageUtility.IMG_CROP ? String.Empty : HelperMethods.GetShortName(model.FullName);*/

//            if (isOpened == false || String.IsNullOrWhiteSpace(imageUrl))
//            {
//                if (string.IsNullOrWhiteSpace(model.FullName))
//                {
//                    if (value.Length > 1 && value[1] is string)
//                    {
//                        string fn = (string)value[1];
//                        if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
//                    }
//                    return ImageObjects.UNKNOWN_IMAGE;
//                }
//                if (imgType == ImageUtility.IMG_CROP || imgType == ImageUtility.IMG_THUMB_LARGE)
//                {
//                    return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                }
//                return ImageUtility.GetDetaultProfileImage(model);
//            }
//            else
//            {
//                string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, imgType == ImageUtility.IMG_THUMB_LARGE ? ImageUtility.IMG_THUMB : imgType);
//                string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

//                BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                if (image == null && model != null && convertedUrl != null)
//                {
//                    if (imgType == ImageUtility.IMG_CROP)
//                    {
//                        ProfileImage obj = new ProfileImage(model.UserTableID, convertedUrl, model);
//                        obj.Start();
//                        convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_THUMB);
//                        newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
//                        image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                        if (image == null)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        else if (image != null)
//                        {
//                            return image;
//                        }
//                    }
//                    else
//                    {
//                        DownloadImageShortInfo obj = new DownloadImageShortInfo(model.UserTableID, convertedUrl, model, SettingsConstants.IMAGE_TYPE_PROFILE);
//                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
//                        // if (!String.IsNullOrEmpty(model.FullName))
//                        //{
//                        if (imgType == ImageUtility.IMG_THUMB_LARGE)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        return ImageUtility.GetDetaultProfileImage(model);
//                        // }   
//                    }
//                }
//                else
//                {
//                    return image;
//                }
//            }
//        }
//        #endregion

//        #region CelebrityModel
//        else if (value[0] is CelebrityModel)
//        {
//            CelebrityModel model = (CelebrityModel)value[0];
//            int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

//            string imageUrl = model.ProfileImage;
//            //string fullName = HelperMethods.GetShortName(model.FullName); /*imgType == ImageUtility.IMG_CROP ? String.Empty : HelperMethods.GetShortName(model.FullName);*/

//            if (isOpened == false || String.IsNullOrWhiteSpace(imageUrl))
//            {
//                if (string.IsNullOrWhiteSpace(model.FullName))
//                {
//                    if (value.Length > 1 && value[1] is string)
//                    {
//                        string fn = (string)value[1];
//                        if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
//                    }
//                    return ImageObjects.UNKNOWN_IMAGE;
//                }
//                if (imgType == ImageUtility.IMG_CROP || imgType == ImageUtility.IMG_THUMB_LARGE)
//                {
//                    return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                }
//                return ImageUtility.GetDetaultProfileImage(model);
//            }
//            else
//            {
//                string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, imgType == ImageUtility.IMG_THUMB_LARGE ? ImageUtility.IMG_THUMB : imgType);
//                string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

//                BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                if (image == null && model != null && convertedUrl != null)
//                {
//                    if (imgType == ImageUtility.IMG_CROP)
//                    {
//                        ProfileImage obj = new ProfileImage(model.UserTableID, convertedUrl, model);
//                        obj.Start();
//                        convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_THUMB);
//                        newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
//                        image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                        if (image == null)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        else if (image != null)
//                        {
//                            return image;
//                        }
//                    }
//                    else
//                    {
//                        DownloadImageShortInfo obj = new DownloadImageShortInfo(model.UserTableID, convertedUrl, model, SettingsConstants.IMAGE_TYPE_PROFILE);
//                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
//                        // if (!String.IsNullOrEmpty(model.FullName))
//                        //{
//                        if (imgType == ImageUtility.IMG_THUMB_LARGE)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        return ImageUtility.GetDetaultProfileImage(model);
//                        // }   
//                    }
//                }
//                else
//                {
//                    return image;
//                }
//            }
//        }
//        #endregion

//        #region "MusicPageModel"
//        else if (value[0] is MusicPageModel)
//        {
//            MusicPageModel model = (MusicPageModel)value[0];
//            int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

//            string imageUrl = model.ProfileImage;
//            //string fullName = HelperMethods.GetShortName(model.FullName); /*imgType == ImageUtility.IMG_CROP ? String.Empty : HelperMethods.GetShortName(model.FullName);*/

//            if (isOpened == false || String.IsNullOrWhiteSpace(imageUrl))
//            {
//                if (string.IsNullOrWhiteSpace(model.FullName))
//                {
//                    if (value.Length > 1 && value[1] is string)
//                    {
//                        string fn = (string)value[1];
//                        if (!string.IsNullOrEmpty(fn)) return ImageUtility.GetDetaultProfileImage(HelperMethods.GetShortName(fn));
//                    }
//                    return ImageObjects.UNKNOWN_IMAGE;
//                }
//                if (imgType == ImageUtility.IMG_CROP || imgType == ImageUtility.IMG_THUMB_LARGE)
//                {
//                    return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                }
//                return ImageUtility.GetDetaultProfileImage(model);
//            }
//            else
//            {
//                string convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, imgType == ImageUtility.IMG_THUMB_LARGE ? ImageUtility.IMG_THUMB : imgType);
//                string newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;

//                BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                if (image == null && model != null && convertedUrl != null)
//                {

//                    if (imgType == ImageUtility.IMG_CROP)
//                    {
//                        ProfileImage obj = new ProfileImage(model.UserTableID, convertedUrl, model);
//                        obj.Start();
//                        convertedUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_THUMB);
//                        newImageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedUrl;
//                        image = ImageUtility.GetBitmapImageOfDynamicResource(newImageUrl);

//                        if (image == null)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        else if (image != null)
//                        {
//                            return image;
//                        }
//                    }
//                    else
//                    {
//                        DownloadImageShortInfo obj = new DownloadImageShortInfo(model.UserTableID, convertedUrl, model, SettingsConstants.IMAGE_TYPE_PROFILE);
//                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
//                        // if (!String.IsNullOrEmpty(model.FullName))
//                        //{
//                        if (imgType == ImageUtility.IMG_THUMB_LARGE)
//                        {
//                            return ImageUtility.GetDetaultProfileImage(model, ImageWidth, ImageWidth, font);
//                        }
//                        return ImageUtility.GetDetaultProfileImage(model);
//                        // }   
//                    }
//                }
//                else
//                {
//                    return image;
//                }
//            }
//        }
//        #endregion
//    }
//    catch (Exception ex)
//    {
//        logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
//    }
//    return null;
//}
//public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
//{
//    BitmapImage bitmapImage = null;
//    try
//    {
//        if (value is UserBasicInfoModel)
//        {
//            UserBasicInfoModel model = (UserBasicInfoModel)value;
//            string imageUrl = model.CoverImage;
//            int cropX = Math.Abs(model.CropImageX);
//            int cropY = Math.Abs(model.CropImageY);

//            if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
//            {
//                imageUrl = HelperMethods.ConvertUrlToName(imageUrl, ImageUtility.IMG_FULL);
//                imageUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
//                Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

//                if (bitmap != null)
//                {
//                    bitmapImage = ImageUtility.GetResizedOrCroppedCoverImage(bitmap, cropX, cropY);
//                }
//            }

//            if (bitmapImage == null)
//            {
//                if (!String.IsNullOrWhiteSpace(model.CoverImage))
//                {
//                    imageUrl = HelperMethods.ConvertUrlToName(model.CoverImage, ImageUtility.IMG_FULL);
//                    CoverImage obj = new CoverImage(model.ShortInfoModel.UserTableID, imageUrl);
//                    obj.Start();
//                }

//                bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
//            }
//        }
//        else if (value is GroupInfoModel)
//        {
//            GroupInfoModel model = (GroupInfoModel)value;
//            string imageUrl = model.GroupProfileImage;

//            if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
//            {
//                imageUrl = model.GroupProfileImage.Replace('/', '_');
//                imageUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
//                Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

//                if (bitmap != null)
//                {
//                    bitmapImage = ImageUtility.GetResizedImageToFit(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);
//                }
//            }

//            if (bitmapImage == null)
//            {
//                if (!String.IsNullOrWhiteSpace(model.GroupProfileImage))
//                {
//                    imageUrl = model.GroupProfileImage.Replace('/', '_');
//                    GroupImage obj = new GroupImage(model.GroupID, imageUrl);
//                    obj.Start();
//                }

//                bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
//            }
//        }
//        else if (value is NewsPortalModel)
//        {
//            NewsPortalModel model = (NewsPortalModel)value;
//            string imageUrl = model.CoverImage;

//            if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
//            {
//                imageUrl = model.CoverImage.Replace('/', '_');
//                imageUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
//                Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

//                if (bitmap != null)
//                {
//                    bitmapImage = ImageUtility.GetResizedImageToFit(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);
//                }
//            }

//            if (bitmapImage == null)
//            {
//                if (!String.IsNullOrWhiteSpace(model.CoverImage))
//                {
//                    imageUrl = model.CoverImage.Replace('/', '_');
//                    CoverImage obj = new CoverImage(model, imageUrl);
//                    obj.Start();
//                }

//                bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
//            }
//            bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
//        }
//        else if (value is PageInfoModel)
//        {
//            PageInfoModel model = (PageInfoModel)value;
//            string imageUrl = model.CoverImage;

//            if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
//            {
//                imageUrl = model.CoverImage.Replace('/', '_');
//                imageUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
//                Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

//                if (bitmap != null)
//                {
//                    bitmapImage = ImageUtility.GetResizedImageToFit(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);
//                }
//            }

//            if (bitmapImage == null)
//            {
//                if (!String.IsNullOrWhiteSpace(model.CoverImage))
//                {
//                    imageUrl = model.CoverImage.Replace('/', '_');
//                    CoverImage obj = new CoverImage(model, imageUrl);
//                    obj.Start();
//                }

//                bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
//            }
//            bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
//        }
//        else if (value is MusicPageModel)
//        {
//            MusicPageModel model = (MusicPageModel)value;
//            string imageUrl = model.CoverImage;

//            if (!String.IsNullOrWhiteSpace(imageUrl) && imageUrl.Contains("/"))
//            {
//                imageUrl = model.CoverImage.Replace('/', '_');
//                imageUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + imageUrl;
//                Bitmap bitmap = ImageUtility.GetBitmapOfDynamicResource(imageUrl);

//                if (bitmap != null)
//                {
//                    bitmapImage = ImageUtility.GetResizedImageToFit(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);
//                }
//            }

//            if (bitmapImage == null)
//            {
//                if (!String.IsNullOrWhiteSpace(model.CoverImage))
//                {
//                    imageUrl = model.CoverImage.Replace('/', '_');
//                    CoverImage obj = new CoverImage(model, imageUrl);
//                    obj.Start();
//                }

//                bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
//            }
//            bitmapImage = ImageObjects.DEFAULT_COVER_IMAGE;
//        }
//    }
//    catch (Exception ex) { logger.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
//    return bitmapImage;
//}
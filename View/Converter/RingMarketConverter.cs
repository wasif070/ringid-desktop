﻿using View.Constants;
using System;
using System.IO;
using System.Linq;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using View.Utility;
using View.BindingModels;
using View.ViewModel;
using Auth.Service.RingMarket;
using View.Utility.RingMarket;
using Auth.utility;
using System.Windows;
using Models.Constants;

namespace View.Converter
{
    class StickerImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            string imageName = value[0] != null ? value[0].ToString() : string.Empty;
            imageName = parameter != null && parameter.ToString().Equals("MARKET_STICKER_ICON") ? "market_" + imageName : imageName;
            imageName = parameter != null && parameter.ToString().Equals("MARKET_BANNER_IMAGE_MINI") ? "s" + imageName : imageName; // sbanner.png

            int collectionId = 0;
            int categoryId = 0;
            if (value[1] != null && Int32.TryParse(value[1].ToString(), out collectionId)) { }
            if (value[2] != null && Int32.TryParse(value[2].ToString(), out categoryId)) { }

            BitmapImage bitmapImage = null;
            if (categoryId == 0 && collectionId == 0)
            {
                bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.STICKER_RECENT_ICON);
                return bitmapImage;
            }

            else if (categoryId == -1 && collectionId == -1)
            {
                bitmapImage = ImageUtility.GetBitmapImage(ImageLocation.CHAT_EMOTICON_LARGE);
                return bitmapImage;
            }

            string imagePath = RingIDSettings.STICKER_FOLDER + Path.DirectorySeparatorChar + collectionId.ToString() + Path.DirectorySeparatorChar + categoryId.ToString() + Path.DirectorySeparatorChar + imageName;
            MarketStickerCategoryModel categoryModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCollectionID == collectionId && P.StickerCategoryID == categoryId).FirstOrDefault();

            if (categoryModel != null && !(parameter != null && ((parameter.ToString().Equals("DETAIL_IMAGE") && categoryModel.IsDetailImageDownloading == true)
                                         || (parameter.ToString().Equals("THUMB_DETAIL_IMAGE") && categoryModel.IsThumbDetailImageDownloading == true)
                                         || (parameter.ToString().Equals("STICKER_ICON") && categoryModel.IsIconDownloading == true)
                                         || (parameter.ToString().Equals("MARKET_STICKER_ICON") && categoryModel.IsMarketIconDownloading == true))))
            {
                bitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);
                if (bitmapImage == null && DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    if (parameter != null && parameter.ToString().Equals("STICKER_ICON")) // Right sided icon
                    {
                        categoryModel.IsIconDownloading = true;
                        new RingMarketStickerService(categoryId, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), RingMarketStickerService._Sticker_Icon);
                    }

                    else if (parameter != null && parameter.ToString().Equals("MARKET_STICKER_ICON"))
                    {
                        categoryModel.IsMarketIconDownloading = true;
                        new RingMarketStickerService(categoryId, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), RingMarketStickerService._Market_Icon_Image);
                    }

                    else if (parameter != null && parameter.ToString().Equals("MARKET_BANNER_IMAGE_MINI"))
                    {
                        new RingMarketStickerService(categoryId, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), RingMarketStickerService._MarketBannerImageMini);
                    }

                    else if (parameter != null && parameter.ToString().Equals("MARKET_BANNER_IMAGE"))
                    {
                        new RingMarketStickerService(categoryId, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), RingMarketStickerService._MarketBannerImage);
                    }

                    else if (parameter != null && parameter.ToString().Equals("DETAIL_IMAGE"))
                    {
                        categoryModel.IsDetailImageDownloading = true;
                        new RingMarketStickerService(categoryId, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), RingMarketStickerService._MarketDetailImage);
                    }

                    else if (parameter != null && parameter.ToString().Equals("THUMB_DETAIL_IMAGE"))
                    {
                        categoryModel.IsThumbDetailImageDownloading = true;
                        new RingMarketStickerService(categoryId, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), RingMarketStickerService._Market_Thumb_DetailImage);
                    }
                }
            }
            return bitmapImage;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    class CollectionTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int selectedCtg = value != null ? Int32.Parse(value.ToString()) : 0;
            int collectionCount = parameter != null ? Int32.Parse(parameter.ToString()) : 0;
            if (selectedCtg == 4 && collectionCount > 0)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

<<<<<<< HEAD
﻿using log4net;
using Models.Constants;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Chat.Service;
using View.Utility.Images;

namespace View.Converter
{
    public class ChannelProfileImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] is ChannelModel)
            {
                ChannelModel channelModel = value[0] != null && value[0] != DependencyProperty.UnsetValue ? (ChannelModel)value[0] : null;
                string imageUrl = value[1] != null && value[1] != DependencyProperty.UnsetValue ? (string)value[1] : string.Empty;
                int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

                if (channelModel != null && !String.IsNullOrWhiteSpace(imageUrl))
                {
                    string convertedPUrl = HelperMethods.ConvertUrlToPurl(imageUrl, imgType);
                    string convertedName = HelperMethods.ConvertUrlToName(imageUrl, imgType);
                    string imagePath = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedName;
                    BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);

                    if (image != null)
                    {
                        return image;
                    }
                    else
                    {
                        DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedName, convertedPUrl, channelModel, SettingsConstants.IMAGE_TYPE_CHANNEL, true);
                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                    }
                }

                return imgType == ImageUtility.IMG_THUMB || imgType == ImageUtility.IMG_CROP ? ImageObjects.CHANNEL_PROFILE_UNKNOWN : ImageObjects.DEFAULT_COVER_IMAGE;
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ChannelCoverImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] is ChannelModel)
            {
                ChannelModel channelModel = value[0] != null && value[0] != DependencyProperty.UnsetValue ? (ChannelModel)value[0] : null;
                string imageUrl = value[1] != null && value[1] != DependencyProperty.UnsetValue ? (string)value[1] : string.Empty;
                int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

                if (channelModel != null && !String.IsNullOrWhiteSpace(imageUrl))
                {
                    string convertedPUrl = HelperMethods.ConvertUrlToPurl(imageUrl, imgType);
                    string convertedName = HelperMethods.ConvertUrlToName(imageUrl, imgType);
                    string imagePath = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedName;
                    BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);

                    if (image != null)
                    {
                        return image;
                    }
                    else
                    {
                        DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedName, convertedPUrl, channelModel, SettingsConstants.IMAGE_TYPE_CHANNEL, false);
                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                    }
                }

                return ImageObjects.DEFAULT_COVER_IMAGE;
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ChannelMediaImageConverter : IMultiValueConverter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChannelMediaImageConverter).Name);

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value[0] is ChannelMediaModel)
                {
                    ChannelMediaModel mediaModel = value[0] != null && value[0] != DependencyProperty.UnsetValue ? (ChannelMediaModel)value[0] : null;
                    string imageUrl = value[1] != null && value[1] != DependencyProperty.UnsetValue ? (string)value[1] : string.Empty;

                    if (!String.IsNullOrWhiteSpace(imageUrl))
                    {
                        if (!(imageUrl.EndsWith(".jpg") || imageUrl.EndsWith(".png"))) imageUrl = imageUrl + ".jpg";
                        string imagePath = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + imageUrl.Replace("/", "_");

                        BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);
                        if (image != null)
                        {
                            return image;
                        }
                        else
                        {
                            new MediaImage(mediaModel, imagePath).Start();
                        }
                    }

                    return mediaModel.MediaType == 1 ? ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE : ImageObjects.DEFAULT_VIDEO_ALBUM_IMAGE;
                }
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
            return null;
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ArrowVisibilityConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] == DependencyProperty.UnsetValue || value[1] == DependencyProperty.UnsetValue || value[2] == DependencyProperty.UnsetValue)
            {
                return Visibility.Collapsed;
            }
            else
            {
                double leftMargin = ((Thickness)value[0]).Left;
                if (parameter != null && (bool)parameter)
                {
                    double bdrWidth = (double)value[1];
                    double itcWidth = (double)value[2];

                    return Math.Abs(leftMargin) + bdrWidth < itcWidth ? Visibility.Visible : Visibility.Collapsed;
                }
                else
                {
                    return leftMargin < 0 ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MediaPublishingDateConverter : IValueConverter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChannelMediaImageConverter).Name);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                long scheduleTime = value != null ? long.Parse(value.ToString()) : 0;
                int param = parameter != null ? Int32.Parse(parameter.ToString()) : 0;

                DateTime dt = ModelUtility.GetLocalDateTime(scheduleTime);
                dt.GetDateTimeFormats();
                //DateTime currDt = DateTime.Now;
                string date = dt.ToString("dd-MM-yyyy");
                string time = dt.ToShortTimeString();//"7:38 PM"

                //if (dt.IsSameDateOf(currDt))
                //{
                //    f = "Today, ";
                //}
                //else if (dt.Year == currDt.Year && dt.DayOfYear == (currDt.DayOfYear - 1))
                //{
                //    f = "Yesterday, ";
                //}

                //string aa = f + "   " + l;

                if (param == 1)
                {
                    return date;
                }
                else if (param == 2)
                {
                    return time;
                }
                else if (param == 3)
                {
                    if (ModelUtility.CurrentTimeMillisLocal() > scheduleTime)
                    {
                        DateTime currDt = DateTime.Now;
                        if (dt.IsSameDateOf(currDt))
                        {
                            return " Played Today at" + " " + time;
                        }
                        else if (dt.Year == currDt.Year && dt.DayOfYear == (currDt.DayOfYear - 1))
                        {
                            return "Played Yesterday at" + " " + time;
                        }
                        else
                        {
                            return "Played on" + " " + dt.ToString("dd MMMM, yyyy");
                        }
                    }
                    else
                    {
                        return "Play Time :" + " " + dt.ToString("dd MMMM") + " " + time;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
            }
            return string.Empty;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }



    }

    public class ChannelCallDurationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return TimeSpan.FromSeconds((int)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
=======
﻿using log4net;
using Models.Constants;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Chat.Service;
using View.Utility.Images;

namespace View.Converter
{
    public class ChannelProfileImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] is ChannelModel)
            {
                ChannelModel channelModel = value[0] != null && value[0] != DependencyProperty.UnsetValue ? (ChannelModel)value[0] : null;
                string imageUrl = value[1] != null && value[1] != DependencyProperty.UnsetValue ? (string)value[1] : string.Empty;
                int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

                if (channelModel != null && !String.IsNullOrWhiteSpace(imageUrl))
                {
                    string convertedPUrl = HelperMethods.ConvertUrlToPurl(imageUrl, imgType);
                    string convertedName = HelperMethods.ConvertUrlToName(imageUrl, imgType);
                    string imagePath = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedName;
                    BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);

                    if (image != null)
                    {
                        return image;
                    }
                    else
                    {
                        DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedName, convertedPUrl, channelModel, SettingsConstants.IMAGE_TYPE_CHANNEL, true);
                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                    }
                }

                return imgType == ImageUtility.IMG_THUMB || imgType == ImageUtility.IMG_CROP ? ImageObjects.CHANNEL_PROFILE_UNKNOWN : ImageObjects.DEFAULT_COVER_IMAGE;
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ChannelCoverImageConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] is ChannelModel)
            {
                ChannelModel channelModel = value[0] != null && value[0] != DependencyProperty.UnsetValue ? (ChannelModel)value[0] : null;
                string imageUrl = value[1] != null && value[1] != DependencyProperty.UnsetValue ? (string)value[1] : string.Empty;
                int imgType = (parameter != null) ? (int)parameter : ImageUtility.IMG_THUMB;

                if (channelModel != null && !String.IsNullOrWhiteSpace(imageUrl))
                {
                    string convertedPUrl = HelperMethods.ConvertUrlToPurl(imageUrl, imgType);
                    string convertedName = HelperMethods.ConvertUrlToName(imageUrl, imgType);
                    string imagePath = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + convertedName;
                    BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);

                    if (image != null)
                    {
                        return image;
                    }
                    else
                    {
                        DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedName, convertedPUrl, channelModel, SettingsConstants.IMAGE_TYPE_CHANNEL, false);
                        ImageDictionaries.Instance.RING_IMAGE_QUEUE.AddItem(obj);
                    }
                }

                return ImageObjects.DEFAULT_COVER_IMAGE;
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ChannelMediaImageConverter : IMultiValueConverter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChannelMediaImageConverter).Name);

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value[0] is ChannelMediaModel)
                {
                    ChannelMediaModel mediaModel = value[0] != null && value[0] != DependencyProperty.UnsetValue ? (ChannelMediaModel)value[0] : null;
                    string imageUrl = value[1] != null && value[1] != DependencyProperty.UnsetValue ? (string)value[1] : string.Empty;

                    if (!String.IsNullOrWhiteSpace(imageUrl))
                    {
                        if (!(imageUrl.EndsWith(".jpg") || imageUrl.EndsWith(".png"))) imageUrl = imageUrl + ".jpg";
                        string imagePath = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + imageUrl.Replace("/", "_");

                        BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);
                        if (image != null)
                        {
                            return image;
                        }
                        else
                        {
                            new MediaImage(mediaModel, imagePath).Start();
                        }
                    }

                    return mediaModel.MediaType == ChannelConstants.MEDIA_TYPE_AUDIO ? ImageObjects.DEFAULT_MEDIA_ALBUM_IMAGE : ImageObjects.DEFAULT_VIDEO_ALBUM_IMAGE;
                }
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
            return null;
        }
        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ArrowVisibilityConverter : IMultiValueConverter
    {

        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value[0] == DependencyProperty.UnsetValue || value[1] == DependencyProperty.UnsetValue || value[2] == DependencyProperty.UnsetValue)
            {
                return Visibility.Collapsed;
            }
            else
            {
                double leftMargin = ((Thickness)value[0]).Left;
                if (parameter != null && (bool)parameter)
                {
                    double bdrWidth = (double)value[1];
                    double itcWidth = (double)value[2];

                    return Math.Abs(leftMargin) + bdrWidth < itcWidth ? Visibility.Visible : Visibility.Collapsed;
                }
                else
                {
                    return leftMargin < 0 ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MediaPublishingDateConverter : IValueConverter
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChannelMediaImageConverter).Name);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                long scheduleTime = value != null ? long.Parse(value.ToString()) : 0;
                int param = parameter != null ? Int32.Parse(parameter.ToString()) : 0;

                DateTime dt = ModelUtility.GetLocalDateTime(scheduleTime);
                dt.GetDateTimeFormats();
                //DateTime currDt = DateTime.Now;
                string date = dt.ToString("dd-MM-yyyy");
                string time = dt.ToShortTimeString();//"7:38 PM"

                //if (dt.IsSameDateOf(currDt))
                //{
                //    f = "Today, ";
                //}
                //else if (dt.Year == currDt.Year && dt.DayOfYear == (currDt.DayOfYear - 1))
                //{
                //    f = "Yesterday, ";
                //}

                //string aa = f + "   " + l;

                if (param == 1)
                {
                    return date;
                }
                else if (param == 2)
                {
                    return time;
                }
                else if (param == 3)
                {
                    if (ModelUtility.CurrentTimeMillisLocal() > scheduleTime)
                    {
                        DateTime currDt = DateTime.Now;
                        if (dt.IsSameDateOf(currDt))
                        {
                            return " Played Today at" + " " + time;
                        }
                        else if (dt.Year == currDt.Year && dt.DayOfYear == (currDt.DayOfYear - 1))
                        {
                            return "Played Yesterday at" + " " + time;
                        }
                        else
                        {
                            return "Played on" + " " + dt.ToString("dd MMMM, yyyy");
                        }
                    }
                    else
                    {
                        return "Play Time :" + " " + dt.ToString("dd MMMM") + " " + time;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
            }
            return string.Empty;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }



    }

    public class ChannelCallDurationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return TimeSpan.FromSeconds((int)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using MediaInfoDotNet;
using Microsoft.Win32;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.Feed;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.ViewModel.NewStatus;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMediaCloudNewStatusPopup.xaml
    /// </summary>
    public partial class UCMediaCloudUploadPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        public UCMediaCloudUploadPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 0.0);//background Color Transparent Black
            SetParent(motherGrid);
        }

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"

        private string _AlbumNm = string.Empty;
        public string AlbumNm
        {
            get { return _AlbumNm; }
            set
            {
                if (value == _AlbumNm)
                    return;
                _AlbumNm = value;
                this.OnPropertyChanged("AlbumNm");
            }
        }

        private NewStatusView _newStatusView = null;
        public NewStatusView NewStatusView
        {
            get { return _newStatusView; }
            set
            {
                if (value == _newStatusView)
                    return;
                _newStatusView = value;
                this.OnPropertyChanged("NewStatusView");
            }
        }

        private NewStatusViewModel _newStatusViewModel;
        public NewStatusViewModel NewStatusViewModel
        {
            get
            {
                return _newStatusViewModel;
            }
            set
            {
                if (value == _newStatusViewModel)
                {
                    return;
                }
                _newStatusViewModel = value;
                this.OnPropertyChanged("NewStatusViewModel");
            }
        }

        private OpenFileDialog _openFileDialog = null;
        public OpenFileDialog OpenFileDialog
        {
            get { return _openFileDialog; }
            set
            {
                if (value == _openFileDialog)
                    return;
                _openFileDialog = value;
                this.OnPropertyChanged("OpenFileDialog");
            }
        }

        //MainSwitcher.PopupController.MediaListFromAlbumClickWrapper

        #endregion

        public void ShowPopup(OpenFileDialog openFileDialog, bool IsVideo)
        {
            this.OpenFileDialog = openFileDialog;

            if (NewStatusView == null) InitializeNewStatusInstance();
            AlbumNm = string.Empty;

            if (!IsVideo)
            {
                UploadAudio();
            }
            else
            {
                UploadVideo();
            }
        }

        public void ShowRecordVideo(string fileName, int duration)
        {
            if (NewStatusViewModel != null && !NewStatusViewModel.IsPostButtonEnabled && (NewStatusViewModel.NewStatusVideoUpload.Count > 0 || NewStatusViewModel.NewStatusMusicUpload.Count > 0))
            {
                SetEvents();
                this.Visibility = Visibility.Visible;
            }
            else
            {
                if (NewStatusViewModel != null && NewStatusViewModel.NewStatusVideoUpload.Count == 5)
                {
                    //MessageBoxResult result = CustomMessageBox.ShowError("Can't upload more than 5 videos at a time !");
                    //if (result.Equals(MessageBoxResult.OK) && NewStatusViewModel.NewStatusVideoUpload.Count == 5)
                    //{
                    //    SetEvents();
                    //    this.Visibility = Visibility.Visible;
                    //}
                    bool isTrue = UIHelperMethods.ShowOKCancel("Upload video", "Can't upload more than 5 videos at a time !");
                    if (isTrue && NewStatusViewModel.NewStatusVideoUpload.Count == 5)
                    {
                        SetEvents();
                        this.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    if (NewStatusViewModel == null) InitializeNewStatusInstance();
                    AlbumNm = string.Empty;
                    if (!containerGrid.Children.Contains(NewStatusView)) containerGrid.Children.Add(NewStatusView);
                    NewStatusViewModel.AudioAlbumTitleTextBoxText = "";
                    if (NewStatusViewModel.NewStatusMusicUpload.Count > 0) NewStatusViewModel.NewStatusMusicUpload.Clear();
                    FeedVideoUploaderModel model = new FeedVideoUploaderModel { IsRecorded = true, FilePath = fileName, VideoDuration = duration, VideoTitle = System.IO.Path.GetFileNameWithoutExtension(fileName) };
                    new SetArtImageFromVideo(model);
                    NewStatusViewModel.NewStatusVideoUpload.Add(model);
                    NewStatusViewModel.AudioPanelVisibility = Visibility.Collapsed;
                    NewStatusViewModel.VideoPanelVisibility = Visibility.Visible;
                    NewStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                    NewStatusViewModel.ActionRemoveLinkPreview();
                    SetEvents();
                    this.Visibility = Visibility.Visible;
                }
            }
        }

        public void HidePopup()
        {
            //BtnExpand.Click -= BtnExpand_Click;
            //cancel.Click -= CancelButton_Click;
            //this.IsVisibleChanged -= UserControl_IsVisibleChanged;
            //this.SizeChanged -= UserControl_SizeChanged;
            //PopupViewWrapper._parent.IsEnabled = true;
            scroll.PreviewKeyDown -= Scroll_PreviewKeyDown;
            NewStatusView.RtbNewStatusArea.Document.Blocks.Clear();
            Visibility = Visibility.Hidden;
            Hide();
        }
        public void LoadStatesInUI(int state)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                if (state == 2) //new status post to upload percantage
                {
                    if (NewStatusViewModel.NewStatusMusicUpload.Count > 0) { string st = NewStatusViewModel.AudioAlbumTitleTextBoxText.Trim(); AlbumNm = string.IsNullOrEmpty(st) ? "Feed Audios" : st; }
                    else if (NewStatusViewModel.NewStatusVideoUpload.Count > 0) { string st = NewStatusViewModel.VideoAlbumTitleTextBoxText.Trim(); AlbumNm = string.IsNullOrEmpty(st) ? "Feed Videos" : st; }

                    //if (containerGrid.Children.Contains(ucNewStatus))
                    //    containerGrid.Children.Remove(ucNewStatus);
                }
                else if (state == 1)//new status mode
                {
                    if (!containerGrid.Children.Contains(NewStatusView))
                        containerGrid.Children.Add(NewStatusView);

                }
                else if (state == 0)//popup hide
                {
                    if (containerGrid.Children.Contains(NewStatusView))
                        containerGrid.Children.Remove(NewStatusView);
                    HidePopup();
                }
            }));
        }

        private void SetEvents()
        {
            scroll.PreviewKeyDown -= Scroll_PreviewKeyDown;
            scroll.PreviewKeyDown += Scroll_PreviewKeyDown;
        }
        private void InitializeNewStatusInstance()
        {
            NewStatusView = new NewStatusView();
            NewStatusViewModel = (NewStatusViewModel)NewStatusView.DataContext;
            NewStatusView.Tag = "4";
            NewStatusViewModel.ucMediaCloudUploadPopup = this;
            NewStatusViewModel.BottomLeftStackPanelVisibility = Visibility.Hidden;
            NewStatusViewModel.OptionsButtonVisibility = Visibility.Hidden;
            NewStatusViewModel.WriteOnText = "Update your feed...";
            NewStatusViewModel.WritePostText = "Upload Music or Video";
        }

        private void UploadAudio()
        {
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;

            if (OpenFileDialog.FileNames.Length <= 5 && (OpenFileDialog.FileNames.Length + NewStatusViewModel.NewStatusMusicUpload.Count) <= 5) // When 5 audio select with cntrl--> openFileDialog.FileNames.Length <= 5
            {
                if (!NewStatusViewModel.IsPostButtonEnabled && (NewStatusViewModel.NewStatusVideoUpload.Count > 0 || NewStatusViewModel.NewStatusMusicUpload.Count > 0))
                {
                    SetEvents();
                    this.Visibility = Visibility.Visible;
                }
                else
                {
                    foreach (string filename in OpenFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);
                            if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit250MBinBytes) { anyMorethan500MB = true; continue; }
                            if (mf.Audio == null || mf.Audio.Count == 0 || mf.Video.Count > 0 || !mf.format.Equals("MPEG Audio")) { anyCorrupted = true; continue; }
                            MusicUploaderModel model = new MusicUploaderModel();
                            model.FileSize = mf.size;
                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                                if (file.Tag != null && file.Tag.Pictures.Any())
                                {
                                    if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null)
                                    {
                                        model.IsImageFound = true;
                                    }
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                    {
                                        model.AudioArtist = file.Tag.Performers[0];
                                    }
                                    else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                    {
                                        model.AudioArtist = file.Tag.AlbumArtists[0];
                                    }
                                    model.AudioTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception) { }
                            if (string.IsNullOrEmpty(model.AudioTitle))
                            {
                                model.AudioTitle = System.IO.Path.GetFileNameWithoutExtension(filename);
                            }
                            model.AudioDuration = (long)(mf.duration / 1000);
                            if (model.AudioDuration == 0)
                                model.AudioDuration = HelperMethods.MediaPlayerDuration(filename);
                            model.FilePath = filename;
                            NewStatusViewModel.NewStatusMusicUpload.Add(model);
                            NewStatusViewModel.UploadingText = "";
                        }
                        catch (Exception) { anyCorrupted = true; }
                    }
                    if (NewStatusViewModel.NewStatusMusicUpload.Count > 0)
                    {
                        if (!containerGrid.Children.Contains(NewStatusView))
                            containerGrid.Children.Add(NewStatusView);
                        NewStatusViewModel.VideoAlbumTitleTextBoxText = "";
                        //if (ucNewStatus.NewStatusImageUpload.Count > 0) ucNewStatus.NewStatusImageUpload.Clear();
                        if (NewStatusViewModel.NewStatusVideoUpload.Count > 0) NewStatusViewModel.NewStatusVideoUpload.Clear();
                        NewStatusViewModel.AudioPanelVisibility = Visibility.Visible;
                        //ucNewStatus.stsImagePanel.Visibility = Visibility.Collapsed;
                        NewStatusViewModel.VideoPanelVisibility = Visibility.Collapsed;
                        NewStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                        NewStatusViewModel.ActionRemoveLinkPreview();
                    }
                    SetEvents();
                    this.Visibility = Visibility.Visible;
                }
            }
            else
            {
                //MessageBoxResult result = CustomMessageBox.ShowError("Can't upload more than 5 music at a time !");
                //if (result.Equals(MessageBoxResult.OK) && NewStatusViewModel.NewStatusMusicUpload.Count == 5)
                //{
                //    SetEvents();
                //    this.Visibility = Visibility.Visible;
                //}
                bool isTrue = UIHelperMethods.ShowOKCancel("Upload music", "Can't upload more than 5 music at a time !");
                if (isTrue && NewStatusViewModel.NewStatusMusicUpload.Count == 5)
                {
                    SetEvents();
                    this.Visibility = Visibility.Visible;
                }
            }
            if (anyCorrupted) UIHelperMethods.ShowWarning("One or more files are corrupted or invalid format!");
            if (anyMorethan500MB) UIHelperMethods.ShowWarning("One or more files are greater than maximum file Limit (250MB) !");
        }

        private void UploadVideo()
        {
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;

            if (OpenFileDialog.FileNames.Length <= 5 && (OpenFileDialog.FileNames.Length + NewStatusViewModel.NewStatusVideoUpload.Count) <= 5) // When 5 video select with cntrl--> openFileDialog.FileNames.Length <= 5
            {
                if (!NewStatusViewModel.IsPostButtonEnabled && (NewStatusViewModel.NewStatusVideoUpload.Count > 0 || NewStatusViewModel.NewStatusMusicUpload.Count > 0))
                {
                    SetEvents();
                    this.Visibility = Visibility.Visible;
                }
                else
                {
                    foreach (string filename in OpenFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);
                            if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit250MBinBytes) { anyMorethan500MB = true; continue; }
                            if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264")) { anyCorrupted = true; continue; }
                            FeedVideoUploaderModel model = new FeedVideoUploaderModel();
                            model.FileSize = mf.size;
                            model.FilePath = filename;
                            NewStatusViewModel.NewStatusVideoUpload.Add(model);
                            NewStatusViewModel.UploadingText = "";
                            model.VideoTitle = System.IO.Path.GetFileNameWithoutExtension(filename);
                            model.VideoDuration = (long)(mf.duration / 1000);
                            if (model.VideoDuration == 0)
                                model.VideoDuration = HelperMethods.MediaPlayerDuration(filename);
                            new SetArtImageFromVideo(model);
                        }
                        catch (Exception) { anyCorrupted = true; }
                    }
                    if (NewStatusViewModel.NewStatusVideoUpload.Count > 0)
                    {
                        if (!containerGrid.Children.Contains(NewStatusView)) containerGrid.Children.Add(NewStatusView);
                        NewStatusViewModel.AudioAlbumTitleTextBoxText = "";

                        if (NewStatusViewModel.NewStatusMusicUpload.Count > 0) NewStatusViewModel.NewStatusMusicUpload.Clear();
                        NewStatusViewModel.VideoPanelVisibility = Visibility.Visible;
                        //this.stsImagePanel.Visibility = Visibility.Collapsed;
                        NewStatusViewModel.AudioPanelVisibility = Visibility.Collapsed;
                        NewStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                        NewStatusViewModel.ActionRemoveLinkPreview();
                    }
                    SetEvents();
                    this.Visibility = Visibility.Visible;
                }
            }
            else
            {
                ////CustomMessageBox.ShowError("Can't upload more than 5 videos at a time !");
                //MessageBoxResult result = CustomMessageBox.ShowError("Can't upload more than 5 videos at a time !");
                //if (result.Equals(MessageBoxResult.OK) && NewStatusViewModel.NewStatusVideoUpload.Count == 5)
                //{
                //    SetEvents();
                //    this.Visibility = Visibility.Visible;
                //}
                bool isTrue = UIHelperMethods.ShowOKCancel("Media upload!", "Can't upload more than 5 videos at a time !");
                if (isTrue & NewStatusViewModel.NewStatusVideoUpload.Count == 5)
                {
                    SetEvents();
                    this.Visibility = Visibility.Visible;
                }
            }
            if (anyCorrupted) UIHelperMethods.ShowWarning("One or more files are corrupted or invalid format!");
            if (anyMorethan500MB) UIHelperMethods.ShowWarning("One or more files are greater than Maximum file Limit (250MB) !");
        }

        #region "Icommands"

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }
        ICommand _blackSpaceClick;
        public ICommand BlackSpaceClick
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }
        private void OnClickBlackSpace(object param)
        {
            HidePopup();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {

        }

        #endregion

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                scroll.ScrollToEnd();
                e.Handled = true;
            }
        }
    }
}

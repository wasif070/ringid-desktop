﻿using Models.Constants;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using View.Utility;
using View.Utility.Feed;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCDiscoverSearchView.xaml
    /// </summary>
    public partial class UCDiscoverSearchView : PopUpBaseControl, INotifyPropertyChanged
    {
        public static UCDiscoverSearchView Instance;
        bool fromPortal;

        public UCDiscoverSearchView(Grid motherPanel)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, null, 0.0);//background Color Transparent Black
            SetParent(motherPanel);
        }

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private string _SelectedCountryName = "All";
        public string SelectedCountryName
        {
            get { return _SelectedCountryName; }
            set
            {
                if (value == _SelectedCountryName)
                    return;
                _SelectedCountryName = value;
                this.OnPropertyChanged("SelectedCountryName");
            }
        }
        private string _SelectedSearchParam = string.Empty;
        public string SelectedSearchParam
        {
            get { return _SelectedSearchParam; }
            set
            {
                if (value == _SelectedSearchParam)
                    return;
                _SelectedSearchParam = value;
                this.OnPropertyChanged("SelectedSearchParam");
            }
        }
        #endregion

        public void SetDiscoverSettings(bool fromPortal, string searchParam, string countryName)
        {
            this.fromPortal = fromPortal;
            this.SelectedCountryName = string.IsNullOrEmpty(countryName) ? "All" : countryName;
            this.SelectedSearchParam = searchParam;
        }

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }

        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }
        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => OnCloseCommand());
                }
                return _CloseCommand;
            }
        }

        public void OnCloseCommand()
        {
            //Visibility = Visibility.Hidden;
            Hide();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }
        private void OnCommentSpaceClicked(object param)
        {
            OnLoadedControl();
        }

        private ICommand _TxtBoxPanelCommand;
        public ICommand TxtBoxPanelCommand
        {
            get
            {
                if (_TxtBoxPanelCommand == null)
                {
                    _TxtBoxPanelCommand = new RelayCommand((param) => OnTxtBoxPanelCommand());
                }
                return _TxtBoxPanelCommand;
            }
        }
        public void OnTxtBoxPanelCommand()
        {
            SearchTermTextBox.Focusable = true;
            SearchTermTextBox.Focus();
        }

        private ICommand _PanelCommand;
        public ICommand PanelCommand
        {
            get
            {
                if (_PanelCommand == null)
                {
                    _PanelCommand = new RelayCommand((param) => OnPanelCommand());
                }
                return _PanelCommand;
            }
        }
        public void OnPanelCommand()
        {
            if (UCDiscoverSearchCountryView.Instance != null)
                _PopUpWrapperMotherGrid.Children.Remove(UCDiscoverSearchCountryView.Instance);
            UCDiscoverSearchCountryView.Instance = new UCDiscoverSearchCountryView(_PopUpWrapperMotherGrid);
            UCDiscoverSearchCountryView.Instance.Show();
            UCDiscoverSearchCountryView.Instance.LoadMailMobileCountryCodeList(SelectedCountryName);
            UCDiscoverSearchCountryView.Instance.OnRemovedUserControl += () =>
            {
                if (UCDiscoverSearchCountryView.Instance.SelectedCountryName != null)
                    SelectedCountryName = UCDiscoverSearchCountryView.Instance.SelectedCountryName;
                UCDiscoverSearchCountryView.Instance = null;
                if (this.IsVisible)
                    OnLoadedControl();
            };
        }

        ICommand _DoneBtnClicked;
        public ICommand DoneBtnClicked
        {
            get
            {
                if (_DoneBtnClicked == null)
                {
                    _DoneBtnClicked = new RelayCommand(param => OnDoneBtnClicked());
                }
                return _DoneBtnClicked;
            }
        }
        private void OnDoneBtnClicked()
        {
            ThradDiscoverOrFollowingChannelsList thrd = new ThradDiscoverOrFollowingChannelsList();
            thrd.CallBack += (success) =>
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                   {
                       if (fromPortal)
                       {
                           //UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Clear();
                           UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel.SelectedSearchParam = SelectedSearchParam;
                           UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel.SelectedCountryName = SelectedCountryName;
                           UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.showMoreBtn.Visibility = (success == SettingsConstants.RESPONSE_SUCCESS) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                           UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.showMorePanel.Visibility = (success == SettingsConstants.RESPONSE_SUCCESS) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                       }
                       else
                       {
                           //RingIDViewModel.Instance.MusicPagesDiscovered.Clear();
                           UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.SelectedSearchParam = SelectedSearchParam;
                           UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.SelectedCountryName = SelectedCountryName;
                           UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudDiscoverPanel.showMoreBtn.Visibility = (success == SettingsConstants.RESPONSE_SUCCESS) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                           UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudDiscoverPanel.showMorePanel.Visibility = (success == SettingsConstants.RESPONSE_SUCCESS) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                       }
                   });
            };
            if (fromPortal)
                UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Clear();
            else
                RingIDViewModel.Instance.MusicPagesDiscovered.Clear();
            string countryName = !SelectedCountryName.Equals("All") ? SelectedCountryName : "";
            thrd.StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, (fromPortal) ? SettingsConstants.PROFILE_TYPE_NEWSPORTAL : SettingsConstants.PROFILE_TYPE_MUSICPAGE, SearchTermTextBox.Text, countryName);
            OnCloseCommand();
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAddMemberToGroupPopupWrapper.xaml
    /// </summary>
    public partial class UCAddMemberToGroupPopupWrapper : UserControl
    {

        public UCAddMemberToGroupPopup _UCAddMemberToGroupPopup;

        public UCAddMemberToGroupPopupWrapper()
        {
            InitializeComponent();
            //      Instance = this;
        }


        public void Show(UIElement target, List<GroupMemberInfoModel> memberList, Func<List<UserBasicInfoModel>, int> onComplete = null)
        {
            if (this.Content == null)
            {
                _UCAddMemberToGroupPopup = new UCAddMemberToGroupPopup();
                this.Content = _UCAddMemberToGroupPopup;
            }
            ((UCAddMemberToGroupPopup)this.Content).Show(target, memberList, onComplete);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCEditHistoryPopupStyle.xaml
    /// </summary>
    public partial class UCEditHistoryPopupStyle : UserControl
    {
        public UCEditHistoryPopupStyle()
        {
            InitializeComponent();
            this.DataContext = this;
            
        }
        public void Show(UIElement target)
        {
            PopupEdited.IsOpen = true;
            PopupEdited.PlacementTarget = target;
        }
    }
}

﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility;
using View.Utility.Call;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCSignOutLoader.xaml
    /// </summary>
    public partial class UCSignOutLoader : PopUpBaseControl, INotifyPropertyChanged
    {
        #region "Fieldes"
        bool needToExitApplciation = false;
        private string restartWithMessage = null;
        private Grid motherPanel = null;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(UCSignOutLoader).Name);
        #endregion

        #region "Ctors"

        public UCSignOutLoader()
        {
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, null, 0.0);
        }
        #endregion "ctors"

        #region"Utility Methods"
        public void ShowThis(bool isExtit, Grid parent, string restartMessage = null)
        {
            motherPanel = parent;
            SetParent(motherPanel);
            this.needToExitApplciation = isExtit;
            this.restartWithMessage = restartMessage;
            Show();
            ThreadStart sendSignoutReqeust = delegate
            { resetAllWhileApplicaitonClosed(); };
            new Thread(sendSignoutReqeust).Start();
        }

        private void resetAllWhileApplicaitonClosed()
        {
            try
            {
                #region "Chat Unregister & stop chat secret chat & other timer"

                if (ChatHelpers.ForcefullyStopSecretTimer()) Thread.Sleep(1200);
                FileTransferSession.CancelAllTransferFile(false);
                ChatService.Close();

                if (MainSwitcher.CallController.ViewModelCallInfo != null)
                {
                    MainSwitcher.CallController.IsCallEnded = true;
                    MainSwitcher.CallController.ViewModelCallInfo.IsNeedToSendEndSignal = true;
                }
                else if (callsdkwrapper.CallProperty.isCallSdkLoaded())
                {
                    CallHelperMethods.unRegisterEventHandler();
                    System.Threading.Thread.Sleep(600); // 400-600ms sleep
                    CallHelperMethods.VoiceChatReset();
                }
                #endregion "stop chat secret chat & other timer"

                lock (RingIDViewModel.Instance.MyDownloads)
                {
                    foreach (SingleMediaModel model in RingIDViewModel.Instance.MyDownloads)
                    {
                        CustomFileDownloader fileDownloader = null;
                        if (!MediaDataContainer.Instance.CurrentDownloads.TryGetValue(model.ContentId, out fileDownloader))
                        {
                            fileDownloader = new CustomFileDownloader(model);
                            MediaDataContainer.Instance.CurrentDownloads[model.ContentId] = fileDownloader;
                        }
                        if (model.DownloadState == StatusConstants.MEDIA_DOWNLOADING_STATE) fileDownloader.Pause();
                    }
                }
                Utility.Wallet.WalletViewModel.Instance.ProcessWalletDailyDwellingOnClose();
                StorageFeeds.SaveLastTwentyFeeds();

                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                {
                    String pakId = DefaultSettings.LOGIN_RING_ID.ToString() + AppConstants.TYPE_SIGN_OUT.ToString() + DateTime.Now.Millisecond.ToString();
                    JObject pakToSend = new JObject();
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_SIGN_OUT;
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_AUTH, data);
                    Thread.Sleep(50);
                    for (int i = 1; i <= 12; i++)
                    {
                        if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                        {
                            if (i % 3 == 0) SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_AUTH, data);
                            else break;
                        }
                        Thread.Sleep(250);
                    }
                }
                HideTray();
                if (needToExitApplciation) System.Diagnostics.Process.GetCurrentProcess().Kill();
                else
                {
                    HideTray();
                    if (!string.IsNullOrEmpty(restartWithMessage)) System.Diagnostics.Process.Start(Application.ResourceAssembly.Location, StartUpConstatns.ARGUMENT_RESTART + " " + restartWithMessage);
                    else System.Diagnostics.Process.Start(Application.ResourceAssembly.Location, StartUpConstatns.ARGUMENT_RESTART);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }
            catch (Exception ex)
            {
                log.Error("Signout Exception==>" + ex.Message);
                HideTray();
                System.Diagnostics.Process.Start(Application.ResourceAssembly.Location, StartUpConstatns.ARGUMENT_RESTART);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        private WNRingIDMain RingIDMainWindow
        {
            get
            {
                var obj = motherPanel.Parent;
                if (obj != null && obj is WNRingIDMain) return (WNRingIDMain)obj;
                return null;
            }
        }
        private void HideTray()
        {
            if (RingIDMainWindow != null) RingIDMainWindow.HideTrayIcon();
        }

        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

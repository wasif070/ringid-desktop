﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.Feed;
using View.ViewModel.NewStatus;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCHashTagPopUp.xaml
    /// </summary>
    public partial class UCHashTagPopUp : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCHashTagPopUp).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private ObservableCollection<HashTagModel> _searchHashTag;
        public ObservableCollection<HashTagModel> SearchHashTag
        {
            get
            {
                return _searchHashTag;
            }
            set
            {
                _searchHashTag = value;
                this.OnPropertyChanged("SearchHashTag");
            }
        }


        public UCHashTagPopUp()
        {
            InitializeComponent();
            SearchHashTag = new ObservableCollection<HashTagModel>();
        }

        Func<int> _OnClosing = null;
        NewStatusViewModel newStatusViewModel;
        TextBox textBox;

        public void Show(NewStatusViewModel newStatusViewModel, TextBox textBox, UIElement target)
        {
            this.newStatusViewModel = newStatusViewModel;
            this.singleHashInstance = null;
            this.textBox = textBox;
            textBox.LostFocus -= textBox_LostFocus;
            textBox.LostFocus += textBox_LostFocus;
            this.searchParameter = textBox.Text.Trim();
            processingList = false;
            popupHashTag.PlacementTarget = target;
            if (string.IsNullOrWhiteSpace(searchParameter))
            {
                runningSearchHashTagThread = false;
                CloseHashTagPopUp();
            }
            else
            {
                StartThread();
            }
        }

        void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            textBox.Text = string.Empty;
        }

        UCSingleHashTagPopup singleHashInstance;
        public void Show(UCSingleHashTagPopup instance, TextBox textBox)
        {
            this.singleHashInstance = instance;
            this.newStatusViewModel = null;
            this.textBox = textBox;
            textBox.LostFocus -= textBox_LostFocus;
            textBox.LostFocus += textBox_LostFocus;
            this.searchParameter = textBox.Text.Trim();
            StartThread();
        }

        # region Search Thread

        private bool runningSearchHashTagThread = false;
        private string searchParameter;
        private bool processingList = false;
        private int waiting = 0;
        private Int32 SLEEP_TIME = 5;//ms
        private string previousString;
        private Thread hashTagRequestThread = null;

        public void StartThread()
        {
            if (!runningSearchHashTagThread)
            {
                runningSearchHashTagThread = true;
                if (hashTagRequestThread == null || !hashTagRequestThread.IsAlive)
                {
                    hashTagRequestThread = new Thread(Run);
                    hashTagRequestThread.Name = "SearchHashTag";
                    hashTagRequestThread.Start();
                }
            }
        }

        public void Run()
        {
            while (runningSearchHashTagThread)
            {
                try
                {
                    if (!string.IsNullOrEmpty(searchParameter))
                    {
                        if (string.IsNullOrEmpty(previousString) || !previousString.Equals(searchParameter))
                        {
                            if (searchParameter.Trim().Length > 0)
                            {
                                ClearSearchHashTagList();
                                previousString = searchParameter;
                                if (SearchHashTag.Count < 1 && searchParameter.Trim().Length > 0)
                                {
                                    runningSearchHashTagThread = false;
                                    sendSearchRequest();
                                }
                                AddIntoSearchHashTagListBySearchResult();
                            }
                            else
                            {
                                ClearSearchHashTagList();
                            }
                        }

                    }
                    else
                    {
                        ClearSearchHashTagList();
                    }
                    if (waiting != 0 && waiting % 15 == 0)
                    {
                        runningSearchHashTagThread = false;// stop thread if 5 sec idle
                    }
                    waiting++;
                    Thread.Sleep(300);
                }
                catch (Exception ex)
                {
                    log.Error("SearchHashTag==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);
                }
                finally
                {
                    runningSearchHashTagThread = false;
                }
            }
        }

        private void sendSearchRequest()
        {
            JObject packet = new JObject();
            packet[JsonKeys.Action] = AppConstants.TYPE_HASHTAG_SUGGESTION;
            String pakId = SendToServer.GetRanDomPacketID();
            packet[JsonKeys.PacketId] = pakId;
            packet[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            packet[JsonKeys.SearchParam] = searchParameter;
            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, packet.ToString());
            for (int pakSendingAttempt = 1; pakSendingAttempt <= DefaultSettings.TRYING_TIME; pakSendingAttempt++)
            {
                if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && runningSearchHashTagThread)
                {
                    return;
                }
                Thread.Sleep(DefaultSettings.WAITING_TIME);
                if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                {
                    if (pakSendingAttempt % 9 == 0)
                        SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, packet.ToString());
                }
                else
                {
                    JObject feedbackfields;
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                    break;
                }
            }
        }

        private void AddIntoSearchHashTagListBySearchResult()
        {
            if (!processingList)
            {
                try
                {
                    processingList = true;
                    lock (MediaDictionaries.Instance.HashTagSuggestions)
                    {
                        if (MediaDictionaries.Instance.HashTagSuggestions.Count > 0)
                        {
                            for (int dtoCount = 0; dtoCount < MediaDictionaries.Instance.HashTagSuggestions.Count; dtoCount++ )
                            {
                                HashTagDTO dto = MediaDictionaries.Instance.HashTagSuggestions[dtoCount];
                                if (dto.HashTagSearchKey.Contains(previousString))
                                {
                                    if ((dto.HashTagInsertionTime + (60 * 60 * 100)) < (ModelUtility.CurrentTimeMillis()))
                                    {
                                        MediaDictionaries.Instance.HashTagSuggestions.Remove(dto);
                                        Application.Current.Dispatcher.Invoke((Action)delegate
                                        {
                                            lock (SearchHashTag)
                                            {
                                                SearchHashTag.Remove(SearchHashTag.Where(x => x.HashTagSearchKey.Equals(dto.HashTagSearchKey, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault());
                                            }
                                        });
                                    }
                                    else
                                    {
                                        lock (SearchHashTag)
                                        {
                                            if (!SearchHashTag.Any(x => x.HashTagSearchKey.Equals(dto.HashTagSearchKey, StringComparison.InvariantCultureIgnoreCase))
                                                && (newStatusViewModel == null || newStatusViewModel.NewStatusHashTag.Count == 0 || !newStatusViewModel.NewStatusHashTag.Any(x => x.HashTagSearchKey.Equals(dto.HashTagSearchKey, StringComparison.InvariantCultureIgnoreCase))))
                                            {
                                                HashTagModel model = new HashTagModel();
                                                model.LoadData(dto);
                                                Application.Current.Dispatcher.Invoke((Action)delegate
                                                {
                                                    SearchHashTag.Add(model);
                                                    OpenHashTagPopUp();
                                                });
                                            }
                                        }
                                        addASleep();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("AddIntoSearchHashTagListBySearchResult==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);
                }
                finally
                {
                    processingList = false;
                }
            }
        }

        private void addASleep()
        {
            System.Threading.Thread.Sleep(SLEEP_TIME);
        }

        private void ClearSearchHashTagList()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                SearchHashTag.Clear();
            }, System.Windows.Threading.DispatcherPriority.Send);

            CloseHashTagPopUp();
        }

        #endregion

        private void popupHashTag_Closed(object sender, EventArgs e)
        {
            if (_OnClosing != null)
            {
                _OnClosing();
            }
        }

        private void OpenHashTagPopUp()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                popupHashTag.IsOpen = true;
                TagList.SelectedIndex = 0;
            });
        }

        public void CloseHashTagPopUp()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                popupHashTag.IsOpen = false;
                runningSearchHashTagThread = false;
                previousString = string.Empty;
            });
        }

        private void grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DockPanel dp = sender as DockPanel;
            if (dp.DataContext is HashTagModel)
            {
                if (newStatusViewModel != null)
                {
                    if (newStatusViewModel.NewStatusHashTag.Count - 2 >= 0)
                        newStatusViewModel.NewStatusHashTag.Insert(newStatusViewModel.NewStatusHashTag.Count - 1, ((HashTagModel)dp.DataContext));
                    else
                        newStatusViewModel.NewStatusHashTag.Insert(0, ((HashTagModel)dp.DataContext));
                    textBox.Text = string.Empty;
                    textBox.Focus();
                }
                if (singleHashInstance != null)
                {
                    singleHashInstance.SingleHashTag.Add((HashTagModel)dp.DataContext);
                    textBox.Text = string.Empty;
                }
                CloseHashTagPopUp();
            }
        }

        private void grid_MouseEnter(object sender, MouseEventArgs e)
        {
            DockPanel dock = (DockPanel)sender;
            if (dock.DataContext is HashTagModel)
            {
                TagList.SelectedItem = (HashTagModel)dock.DataContext;
            }
        }
    }
}

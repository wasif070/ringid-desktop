<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using View.BindingModels;
using View.UI.PopUp.MediaSendViaChat;
using View.UI.SocialMedia;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Channel;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMediaShareMoreOptions.xaml
    /// </summary>
    public partial class UCMediaShareMoreOptions : PopUpBaseControl
    {
        #region "Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMediaShareMoreOptions).Name);

        private const int POPUP_HEIGHT = 160;
        public SingleMediaModel PlayingMedia = null;
        UCFBShare fbShare;
        private int Type;

        #endregion "Fields"

        #region "Constructors"
        public UCMediaShareMoreOptions(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            //InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
        #endregion "End Constuctors"

        #region Properties
        private UCAlbumMediaEditPopup ucAlbumMediaEditPopup { get; set; }
        #endregion "Properties"

        #region "Event Triggers"

        private void addToAlbum_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                popupMoreOptions.IsOpen = false;
                if (UCDownloadOrAddToAlbumPopUp.Instance != null)
                {
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(UCDownloadOrAddToAlbumPopUp.Instance);
                    else UCGuiRingID.Instance.MotherPanel.Children.Remove(UCDownloadOrAddToAlbumPopUp.Instance);
                }
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                else UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(UCGuiRingID.Instance.MotherPanel);

                UCDownloadOrAddToAlbumPopUp.Instance.OnRemovedUserControl += () =>
                {
                    UCDownloadOrAddToAlbumPopUp.Instance = null;
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                };
                if (PlayingMedia.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                {
                    if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                    }
                    else if (DefaultSettings.IsInternetAvailable)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                        SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_AUDIO, Guid.Empty);
                    }
                }
                else if (PlayingMedia.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                {
                    if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                    }
                    else if (DefaultSettings.IsInternetAvailable)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                        SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_VIDEO, Guid.Empty);
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace + ex.Message);

            }
        }

        //private void share_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    popupMoreOptions.IsOpen = false;
        //    MainSwitcher.PopupController.ShareSingleFeedViewWrapper.Show(PlayingMedia);
        //}

        private void sendViaChat_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCSingleFeedDetailsView.Instance.Hide();
            popupMoreOptions.IsOpen = false;
            if (UCAlbumContentView.Instance != null && UCAlbumContentView.Instance.IsVisible)
            {
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCAlbumContentView.Instance.mainPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetSingleMedia(this.PlayingMedia);
                ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
                {
                    ucMediaShareToFriendPopup = null;
                    if (UCAlbumContentView.Instance != null && UCAlbumContentView.Instance.IsVisible) UCAlbumContentView.Instance.OnLoadedControl();
                };
            }
            else
            {
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetSingleMedia(this.PlayingMedia);
                ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
                {
                    ucMediaShareToFriendPopup = null;
                };
            }
            //DownloadOrAddToAlbumPopUpWrapper.Show(this.PlayingMedia);
        }

        private void shareOnFb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && !string.IsNullOrEmpty(this.PlayingMedia.StreamUrl))
            {
                //try
                //{
                //    if (!UI.Wallet.WnMediaShareViaWallet.Instance.IsOpen)
                //    {
                //        RingIDViewModel.Instance.SetMediaShareInfo(MediaUtility.MakeEncriptedURL(this.PlayingMedia.MediaOwner.UserTableID, this.PlayingMedia.ContentId), this.PlayingMedia.MediaType, WalletConstants.SOCIAL_MEDIA_FACEBOOK);
                //        UI.Wallet.WnMediaShareViaWallet.Instance.Show();
                //    }
                //    else
                //    {
                //        UI.Wallet.WnMediaShareViaWallet.Instance.Focusable = false;
                //        CustomMessageBox.ShowInformation("Media Share already opened!");
                //        UI.Wallet.WnMediaShareViaWallet.Instance.Focus();
                //    }
                //}
                //catch (Exception ex)
                //{
                //    log.Error(ex.StackTrace + "==>" + ex.Message);
                //}

                if (fbShare == null)
                {
                    string encyptedUrl = MediaUtility.MakeEncriptedURL(this.PlayingMedia.MediaOwner.UserTableID, this.PlayingMedia.ContentId);
                    fbShare = new UCFBShare(encyptedUrl, PlayingMedia.MediaType, true);
                    fbShare.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                    fbShare.SetParent(UCGuiRingID.Instance.MotherPanel);
                    fbShare.OnRemovedUserControl += () =>
                    {
                        fbShare = null;
                    };
                    fbShare.Show();
                }
                popupMoreOptions.IsOpen = false;
            }
            else
            {
                popupMoreOptions.IsOpen = false;
                UIHelperMethods.ShowFailed(NotificationMessages.FAILED_MEDIA_SHARE_FACEBOOK_MESSAGE, NotificationMessages.FAILED_MEDIA_SHARE_FACEBOOK_MESSAGE_TITLE);
            }
        }

        private void copyLink_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && !string.IsNullOrEmpty(this.PlayingMedia.StreamUrl))
            {
                System.Windows.Clipboard.SetText(MediaUtility.MakeEncriptedURL(this.PlayingMedia.MediaOwner.UserTableID, PlayingMedia.ContentId));
                popupMoreOptions.IsOpen = false;
                UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.COPY_MEDIA_LINK_SUCCESS_MESSAGE);
            }
            else
            {
                popupMoreOptions.IsOpen = false;
                UIHelperMethods.ShowWarning(NotificationMessages.COPY_MEDIA_LINK_FAILED_MESSAGE, NotificationMessages.COPY_MEDIA_LINK_MESSAGE_TITLE);
            }
        }

        private void reportMedia_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupMoreOptions.IsOpen = false;
            MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(this.PlayingMedia.ContentId, StatusConstants.SPAM_MEDIA_CONTENT, 0);
        }

        private void renameMedia_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popupMoreOptions.IsOpen = false;
            if (ucAlbumMediaEditPopup != null) UCGuiRingID.Instance.MotherPanel.Children.Remove(ucAlbumMediaEditPopup);
            ucAlbumMediaEditPopup = new UCAlbumMediaEditPopup(UCGuiRingID.Instance.MotherPanel);
            ucAlbumMediaEditPopup.Show();
            ucAlbumMediaEditPopup.ShowView(this.PlayingMedia);
            ucAlbumMediaEditPopup.OnRemovedUserControl += () =>
            {
                ucAlbumMediaEditPopup.ClosePopUp();
                ucAlbumMediaEditPopup = null;
            };
        }

        private void deleteMedia_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popupMoreOptions.IsOpen = false;
            bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this media"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.DELETE_MEDIA_MESSAGE_TITLE));
            if (isTrue)
            {
                if (Type == 2)//D
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (RingIDViewModel.Instance.MyDownloads.Count > 0 && RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == PlayingMedia.ContentId))
                            RingIDViewModel.Instance.MyDownloads.Remove(PlayingMedia);
                    });
                    MediaDAO.Instance.DeleteFromDownloadedMediasTable(PlayingMedia.ContentId);
                }
                else if (Type == 3)//r
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (RingIDViewModel.Instance.MyRecentPlayList.Count > 0 && RingIDViewModel.Instance.MyRecentPlayList.Any(P => P.ContentId == PlayingMedia.ContentId))
                            RingIDViewModel.Instance.MyRecentPlayList.Remove(PlayingMedia);
                    });
                    MediaDAO.Instance.DeleteFromRecentMediasTable(PlayingMedia.ContentId);
                }
                else
                {
                    ThreadDeleteMediaOrAlbum thrd = new ThreadDeleteMediaOrAlbum(PlayingMedia.ContentId, PlayingMedia.AlbumId, PlayingMedia.MediaType);
                    thrd.callBackEvent += (success) =>
                    {
                        if (success == SettingsConstants.RESPONSE_SUCCESS)
                        {
                            if (PlayingMedia.AlbumId != Guid.Empty)
                            {
                                MediaContentModel albumModel = RingIDViewModel.Instance.GetMyAlbumModelFromAlbumID(PlayingMedia.AlbumId, PlayingMedia.MediaType);
                                if (albumModel != null)
                                {
                                    albumModel.TotalMediaCount -= 1;
                                    Application.Current.Dispatcher.Invoke(() =>
                                    {
                                        if (albumModel.MediaList != null && albumModel.MediaList.Any(P => P.ContentId == PlayingMedia.ContentId))
                                            albumModel.MediaList.Remove(PlayingMedia);
                                    });
                                }
                                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.DELETE_MEDIA_SUCCESS_MESSAGE);
                            }
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, NotificationMessages.NETWORK_MESSAGE_TITLE);
                            });
                        }
                    };
                    thrd.StartThread();
                }
            }
        }

        private void addToMyChannel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popupMoreOptions.IsOpen = false;
            if (UCAddToMyChannelView.Instance != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCAddToMyChannelView.Instance);
            if (!(UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId != Guid.Empty))
            {
                UCAddToMyChannelView.Instance = new UCAddToMyChannelView(UCGuiRingID.Instance.MotherPanel);
                UCAddToMyChannelView.Instance.Show();
                UCAddToMyChannelView.Instance.ShowChannelView(PlayingMedia);
                UCAddToMyChannelView.Instance.OnRemovedUserControl += () =>
                    {
                        UCAddToMyChannelView.Instance = null;
                        //UCAlbumContentView.Instance.OnLoadedControl();
                    };
            }
            else if (PlayingMedia != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null)
            {
                List<ChannelMediaDTO> mediaDTOList = new List<ChannelMediaDTO>();
                ChannelMediaDTO channelMediaDTO = ChannelHelpers.GetChannelMediaDtoFromSingleMediaModel(PlayingMedia);
                channelMediaDTO.ChannelID = UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId;
                channelMediaDTO.ChannelType = SettingsConstants.MEDIA_TYPE_VIDEO;
                channelMediaDTO.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                channelMediaDTO.OwnerID = DefaultSettings.LOGIN_TABLE_ID;
                mediaDTOList.Add(channelMediaDTO);
                new ThrdAddMediaToChannel(UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId, SettingsConstants.MEDIA_TYPE_VIDEO, mediaDTOList, (status, message) =>
                {
                    if (status)
                    {
                        UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.MEDIA_ADDED_SUCCESS_MESSAGE, 1000);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(message)) UIHelperMethods.ShowFailed(message, NotificationMessages.MEDIA_ADDED_MESSAGE_TITLE);
                        else UIHelperMethods.ShowFailed(NotificationMessages.MEDIA_ADDED_FAILED_MESSAGE, NotificationMessages.MEDIA_ADDED_MESSAGE_TITLE);
                    }
                    return 0;
                }).Start();
            }
        }
        #endregion "Event Triggers"

        #region "Private methods"
        private void SetEvents()
        {
            addToAlbum.MouseLeftButtonDown -= addToAlbum_MouseLeftButtonDown;
            addToAlbum.MouseLeftButtonDown += addToAlbum_MouseLeftButtonDown;
            //share.MouseLeftButtonDown -= share_MouseLeftButtonDown;
            //share.MouseLeftButtonDown += share_MouseLeftButtonDown;
            sendViaChat.MouseLeftButtonDown -= sendViaChat_MouseLeftButtonDown;
            sendViaChat.MouseLeftButtonDown += sendViaChat_MouseLeftButtonDown;
            shareOnFb.MouseLeftButtonDown -= shareOnFb_MouseLeftButtonDown;
            shareOnFb.MouseLeftButtonDown += shareOnFb_MouseLeftButtonDown;
            copyLink.MouseLeftButtonDown -= copyLink_MouseLeftButtonDown;
            copyLink.MouseLeftButtonDown += copyLink_MouseLeftButtonDown;
            reportMedia.MouseLeftButtonDown -= reportMedia_MouseLeftButtonDown;
            reportMedia.MouseLeftButtonDown += reportMedia_MouseLeftButtonDown;
            renameMedia.MouseLeftButtonUp -= renameMedia_MouseLeftButtonUp;
            renameMedia.MouseLeftButtonUp += renameMedia_MouseLeftButtonUp;
            deleteMedia.MouseLeftButtonUp -= deleteMedia_MouseLeftButtonUp;
            deleteMedia.MouseLeftButtonUp += deleteMedia_MouseLeftButtonUp;
            //addToMyChannel.MouseLeftButtonUp -= addToMyChannel_MouseLeftButtonUp;
            //addToMyChannel.MouseLeftButtonUp += addToMyChannel_MouseLeftButtonUp;
        }
        private void ClosePopUp()
        {
            addToAlbum.MouseLeftButtonDown -= addToAlbum_MouseLeftButtonDown;
            //share.MouseLeftButtonDown -= share_MouseLeftButtonDown;
            sendViaChat.MouseLeftButtonDown -= sendViaChat_MouseLeftButtonDown;
            shareOnFb.MouseLeftButtonDown -= shareOnFb_MouseLeftButtonDown;
            copyLink.MouseLeftButtonDown -= copyLink_MouseLeftButtonDown;
            reportMedia.MouseLeftButtonDown -= reportMedia_MouseLeftButtonDown;
            renameMedia.MouseLeftButtonUp -= renameMedia_MouseLeftButtonUp;
            deleteMedia.MouseLeftButtonUp -= deleteMedia_MouseLeftButtonUp;
            //addToMyChannel.MouseLeftButtonUp -= addToMyChannel_MouseLeftButtonUp;
            //DownloadOrAddToAlbumPopUpWrapper.Visibility = Visibility.Collapsed;
        }

        #endregion "Private methods"

        #region "public methods"

        public void ShowOptionsPopup(SingleMediaModel PlayingMedia, UIElement target, int MediaType, int type)
        {
            //_OnClosing = onClosing;
            this.PlayingMedia = PlayingMedia;
            this.Type = type;
            //this.MediaType = MediaType;
            if (PlayingMedia.MediaOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
            {
                ReportDock.Visibility = Visibility.Collapsed;
                RenameDock.Visibility = Visibility.Visible;
                //DeleteDock.Visibility = Visibility.Visible;
            }
            else
            {
                ReportDock.Visibility = Visibility.Visible;
                RenameDock.Visibility = Visibility.Collapsed;
                //DeleteDock.Visibility = Visibility.Collapsed;
            }
            if (Type == 1 || Type == 2 || Type == 3)
                DeleteDock.Visibility = Visibility.Visible;
            else
                DeleteDock.Visibility = Visibility.Collapsed;

            //if (PlayingMedia.Privacy == SettingsConstants.PRIVACY_PUBLIC)
            //    addToAlbumDock.Visibility = Visibility.Visible;
            //else addToAlbumDock.Visibility = Visibility.Collapsed;

            //if (PlayingMedia.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
            //    addToMyChannelDock.Visibility = Visibility.Visible;
            //else addToMyChannelDock.Visibility = Visibility.Collapsed;

            int buttonPosition = (int)target.PointToScreen(new System.Windows.Point(0, 0)).Y;
            int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;

            if ((buttonPosition < POPUP_HEIGHT && bottomSpace >= POPUP_HEIGHT) || bottomSpace >= POPUP_HEIGHT)
            {
                popupMoreOptions.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                //popupMoreOptions.VerticalOffset = -12;
            }
            else
            {
                popupMoreOptions.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
                //popupMoreOptions.VerticalOffset = 12;
            }
            popupMoreOptions.PlacementTarget = target;
            popupMoreOptions.IsOpen = true;

            SetEvents();
        }

        #endregion "public methods"

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupMoreOptions.IsOpen = false;
        }

        private ICommand _PopupOptionClosed;
        public ICommand PopupOptionClosed
        {
            get
            {
                if (_PopupOptionClosed == null) _PopupOptionClosed = new RelayCommand(param => OnPopupOptionsClosed());
                return _PopupOptionClosed;
            }
        }
        private void OnPopupOptionsClosed()
        {
            ClosePopUp();
            Hide();
        }
        #endregion
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using View.BindingModels;
using View.UI.PopUp.MediaSendViaChat;
using View.UI.SocialMedia;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Channel;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMediaShareMoreOptions.xaml
    /// </summary>
    public partial class UCMediaShareMoreOptions : PopUpBaseControl
    {
        #region "Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMediaShareMoreOptions).Name);

        private const int POPUP_HEIGHT = 160;
        public SingleMediaModel PlayingMedia = null;
        UCFBShare fbShare;
        private int Type;

        #endregion "Fields"

        #region "Constructors"
        public UCMediaShareMoreOptions(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            //InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
        #endregion "End Constuctors"

        #region Properties
        private UCAlbumMediaEditPopup ucAlbumMediaEditPopup { get; set; }
        #endregion "Properties"

        #region "Event Triggers"

        private void addToAlbum_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                popupMoreOptions.IsOpen = false;
                if (UCDownloadOrAddToAlbumPopUp.Instance != null)
                {
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(UCDownloadOrAddToAlbumPopUp.Instance);
                    else UCGuiRingID.Instance.MotherPanel.Children.Remove(UCDownloadOrAddToAlbumPopUp.Instance);
                }
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                else UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(UCGuiRingID.Instance.MotherPanel);

                UCDownloadOrAddToAlbumPopUp.Instance.OnRemovedUserControl += () =>
                {
                    UCDownloadOrAddToAlbumPopUp.Instance = null;
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                };
                if (PlayingMedia.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                {
                    if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                    }
                    else if (DefaultSettings.IsInternetAvailable)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                        SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_AUDIO, Guid.Empty);
                    }
                }
                else if (PlayingMedia.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                {
                    if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                    }
                    else if (DefaultSettings.IsInternetAvailable)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                        SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_VIDEO, Guid.Empty);
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace + ex.Message);

            }
        }

        //private void share_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    popupMoreOptions.IsOpen = false;
        //    MainSwitcher.PopupController.ShareSingleFeedViewWrapper.Show(PlayingMedia);
        //}

        private void sendViaChat_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCSingleFeedDetailsView.Instance.Hide();
            popupMoreOptions.IsOpen = false;
            if (UCAlbumContentView.Instance != null && UCAlbumContentView.Instance.IsVisible)
            {
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCAlbumContentView.Instance.mainPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetSingleMedia(this.PlayingMedia);
                ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
                {
                    ucMediaShareToFriendPopup = null;
                    if (UCAlbumContentView.Instance != null && UCAlbumContentView.Instance.IsVisible) UCAlbumContentView.Instance.OnLoadedControl();
                };
            }
            else
            {
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetSingleMedia(this.PlayingMedia);
                ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
                {
                    ucMediaShareToFriendPopup = null;
                };
            }
            //DownloadOrAddToAlbumPopUpWrapper.Show(this.PlayingMedia);
        }

        private void shareOnFb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && !string.IsNullOrEmpty(this.PlayingMedia.StreamUrl))
            {
                //try
                //{
                //    if (!UI.Wallet.WnMediaShareViaWallet.Instance.IsOpen)
                //    {
                //        RingIDViewModel.Instance.SetMediaShareInfo(MediaUtility.MakeEncriptedURL(this.PlayingMedia.MediaOwner.UserTableID, this.PlayingMedia.ContentId), this.PlayingMedia.MediaType, WalletConstants.SOCIAL_MEDIA_FACEBOOK);
                //        UI.Wallet.WnMediaShareViaWallet.Instance.Show();
                //    }
                //    else
                //    {
                //        UI.Wallet.WnMediaShareViaWallet.Instance.Focusable = false;
                //        CustomMessageBox.ShowInformation("Media Share already opened!");
                //        UI.Wallet.WnMediaShareViaWallet.Instance.Focus();
                //    }
                //}
                //catch (Exception ex)
                //{
                //    log.Error(ex.StackTrace + "==>" + ex.Message);
                //}

                if (fbShare == null)
                {
                    string encyptedUrl = MediaUtility.MakeEncriptedURL(this.PlayingMedia.MediaOwner.UserTableID, this.PlayingMedia.ContentId);
                    fbShare = new UCFBShare(encyptedUrl, PlayingMedia.MediaType, true);
                    fbShare.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                    fbShare.SetParent(UCGuiRingID.Instance.MotherPanel);
                    fbShare.OnRemovedUserControl += () =>
                    {
                        fbShare = null;
                    };
                    fbShare.Show();
                }
                popupMoreOptions.IsOpen = false;
            }
            else
            {
                popupMoreOptions.IsOpen = false;
                UIHelperMethods.ShowFailed(NotificationMessages.FAILED_MEDIA_SHARE_FACEBOOK_MESSAGE, NotificationMessages.FAILED_MEDIA_SHARE_FACEBOOK_MESSAGE_TITLE);
            }
        }

        private void copyLink_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && !string.IsNullOrEmpty(this.PlayingMedia.StreamUrl))
            {
                System.Windows.Clipboard.SetText(MediaUtility.MakeEncriptedURL(this.PlayingMedia.MediaOwner.UserTableID, PlayingMedia.ContentId));
                popupMoreOptions.IsOpen = false;
                UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.COPY_MEDIA_LINK_SUCCESS_MESSAGE);
            }
            else
            {
                popupMoreOptions.IsOpen = false;
                UIHelperMethods.ShowWarning(NotificationMessages.COPY_MEDIA_LINK_FAILED_MESSAGE, NotificationMessages.COPY_MEDIA_LINK_MESSAGE_TITLE);
            }
        }

        private void reportMedia_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupMoreOptions.IsOpen = false;
            MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(this.PlayingMedia.ContentId, StatusConstants.SPAM_MEDIA_CONTENT, 0);
        }

        private void renameMedia_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popupMoreOptions.IsOpen = false;
            if (ucAlbumMediaEditPopup != null) UCGuiRingID.Instance.MotherPanel.Children.Remove(ucAlbumMediaEditPopup);
            ucAlbumMediaEditPopup = new UCAlbumMediaEditPopup(UCGuiRingID.Instance.MotherPanel);
            ucAlbumMediaEditPopup.Show();
            ucAlbumMediaEditPopup.ShowView(this.PlayingMedia);
            ucAlbumMediaEditPopup.OnRemovedUserControl += () =>
            {
                ucAlbumMediaEditPopup.ClosePopUp();
                ucAlbumMediaEditPopup = null;
            };
        }

        private void deleteMedia_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popupMoreOptions.IsOpen = false;
            bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this media"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.DELETE_MEDIA_MESSAGE_TITLE));
            if (isTrue)
            {
                if (Type == 2)//D
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (RingIDViewModel.Instance.MyDownloads.Count > 0 && RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == PlayingMedia.ContentId))
                            RingIDViewModel.Instance.MyDownloads.Remove(PlayingMedia);
                    });
                    MediaDAO.Instance.DeleteFromDownloadedMediasTable(PlayingMedia.ContentId);
                }
                else if (Type == 3)//r
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (RingIDViewModel.Instance.MyRecentPlayList.Count > 0 && RingIDViewModel.Instance.MyRecentPlayList.Any(P => P.ContentId == PlayingMedia.ContentId))
                            RingIDViewModel.Instance.MyRecentPlayList.Remove(PlayingMedia);
                    });
                    MediaDAO.Instance.DeleteFromRecentMediasTable(PlayingMedia.ContentId);
                }
                else
                {
                    ThreadDeleteMediaOrAlbum thrd = new ThreadDeleteMediaOrAlbum(PlayingMedia.ContentId, PlayingMedia.AlbumId, PlayingMedia.MediaType);
                    thrd.callBackEvent += (success) =>
                    {
                        if (success == SettingsConstants.RESPONSE_SUCCESS)
                        {
                            if (PlayingMedia.AlbumId != Guid.Empty)
                            {
                                MediaContentModel albumModel = RingIDViewModel.Instance.GetMyAlbumModelFromAlbumID(PlayingMedia.AlbumId, PlayingMedia.MediaType);
                                if (albumModel != null)
                                {
                                    albumModel.TotalMediaCount -= 1;
                                    Application.Current.Dispatcher.Invoke(() =>
                                    {
                                        if (albumModel.MediaList != null && albumModel.MediaList.Any(P => P.ContentId == PlayingMedia.ContentId))
                                            albumModel.MediaList.Remove(PlayingMedia);
                                    });
                                }
                                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.DELETE_MEDIA_SUCCESS_MESSAGE);
                            }
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, NotificationMessages.NETWORK_MESSAGE_TITLE);
                            });
                        }
                    };
                    thrd.StartThread();
                }
            }
        }

        private void addToMyChannel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            popupMoreOptions.IsOpen = false;
            if (UCAddToMyChannelView.Instance != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCAddToMyChannelView.Instance);
            if (!(UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId != Guid.Empty))
            {
                UCAddToMyChannelView.Instance = new UCAddToMyChannelView(UCGuiRingID.Instance.MotherPanel);
                UCAddToMyChannelView.Instance.Show();
                UCAddToMyChannelView.Instance.ShowChannelView(PlayingMedia);
                UCAddToMyChannelView.Instance.OnRemovedUserControl += () =>
                    {
                        UCAddToMyChannelView.Instance = null;
                        //UCAlbumContentView.Instance.OnLoadedControl();
                    };
            }
            else if (PlayingMedia != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null)
            {
                List<ChannelMediaDTO> mediaDTOList = new List<ChannelMediaDTO>();
                ChannelMediaDTO channelMediaDTO = ChannelHelpers.GetChannelMediaDtoFromSingleMediaModel(PlayingMedia);
                channelMediaDTO.ChannelID = UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId;
                channelMediaDTO.ChannelType = SettingsConstants.MEDIA_TYPE_VIDEO;
                channelMediaDTO.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                channelMediaDTO.OwnerID = DefaultSettings.LOGIN_TABLE_ID;
                mediaDTOList.Add(channelMediaDTO);
                new ThrdAddUploadedChannelMedia(mediaDTOList, (status, message) =>
                {
                    if (status)
                    {
                        UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.MEDIA_ADDED_SUCCESS_MESSAGE, 1000);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(message)) UIHelperMethods.ShowFailed(message, NotificationMessages.MEDIA_ADDED_MESSAGE_TITLE);
                        else UIHelperMethods.ShowFailed(NotificationMessages.MEDIA_ADDED_FAILED_MESSAGE, NotificationMessages.MEDIA_ADDED_MESSAGE_TITLE);
                    }
                    return 0;
                }).Start();
            }
        }
        #endregion "Event Triggers"

        #region "Private methods"
        private void SetEvents()
        {
            addToAlbum.MouseLeftButtonDown -= addToAlbum_MouseLeftButtonDown;
            addToAlbum.MouseLeftButtonDown += addToAlbum_MouseLeftButtonDown;
            //share.MouseLeftButtonDown -= share_MouseLeftButtonDown;
            //share.MouseLeftButtonDown += share_MouseLeftButtonDown;
            sendViaChat.MouseLeftButtonDown -= sendViaChat_MouseLeftButtonDown;
            sendViaChat.MouseLeftButtonDown += sendViaChat_MouseLeftButtonDown;
            shareOnFb.MouseLeftButtonDown -= shareOnFb_MouseLeftButtonDown;
            shareOnFb.MouseLeftButtonDown += shareOnFb_MouseLeftButtonDown;
            copyLink.MouseLeftButtonDown -= copyLink_MouseLeftButtonDown;
            copyLink.MouseLeftButtonDown += copyLink_MouseLeftButtonDown;
            reportMedia.MouseLeftButtonDown -= reportMedia_MouseLeftButtonDown;
            reportMedia.MouseLeftButtonDown += reportMedia_MouseLeftButtonDown;
            renameMedia.MouseLeftButtonUp -= renameMedia_MouseLeftButtonUp;
            renameMedia.MouseLeftButtonUp += renameMedia_MouseLeftButtonUp;
            deleteMedia.MouseLeftButtonUp -= deleteMedia_MouseLeftButtonUp;
            deleteMedia.MouseLeftButtonUp += deleteMedia_MouseLeftButtonUp;
            //addToMyChannel.MouseLeftButtonUp -= addToMyChannel_MouseLeftButtonUp;
            //addToMyChannel.MouseLeftButtonUp += addToMyChannel_MouseLeftButtonUp;
        }
        private void ClosePopUp()
        {
            addToAlbum.MouseLeftButtonDown -= addToAlbum_MouseLeftButtonDown;
            //share.MouseLeftButtonDown -= share_MouseLeftButtonDown;
            sendViaChat.MouseLeftButtonDown -= sendViaChat_MouseLeftButtonDown;
            shareOnFb.MouseLeftButtonDown -= shareOnFb_MouseLeftButtonDown;
            copyLink.MouseLeftButtonDown -= copyLink_MouseLeftButtonDown;
            reportMedia.MouseLeftButtonDown -= reportMedia_MouseLeftButtonDown;
            renameMedia.MouseLeftButtonUp -= renameMedia_MouseLeftButtonUp;
            deleteMedia.MouseLeftButtonUp -= deleteMedia_MouseLeftButtonUp;
            //addToMyChannel.MouseLeftButtonUp -= addToMyChannel_MouseLeftButtonUp;
            //DownloadOrAddToAlbumPopUpWrapper.Visibility = Visibility.Collapsed;
        }

        #endregion "Private methods"

        #region "public methods"

        public void ShowOptionsPopup(SingleMediaModel PlayingMedia, UIElement target, int MediaType, int type)
        {
            //_OnClosing = onClosing;
            this.PlayingMedia = PlayingMedia;
            this.Type = type;
            //this.MediaType = MediaType;
            if (PlayingMedia.MediaOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
            {
                ReportDock.Visibility = Visibility.Collapsed;
                RenameDock.Visibility = Visibility.Visible;
                //DeleteDock.Visibility = Visibility.Visible;
            }
            else
            {
                ReportDock.Visibility = Visibility.Visible;
                RenameDock.Visibility = Visibility.Collapsed;
                //DeleteDock.Visibility = Visibility.Collapsed;
            }
            if (Type == 1 || Type == 2 || Type == 3)
                DeleteDock.Visibility = Visibility.Visible;
            else
                DeleteDock.Visibility = Visibility.Collapsed;

            //if (PlayingMedia.Privacy == SettingsConstants.PRIVACY_PUBLIC)
            //    addToAlbumDock.Visibility = Visibility.Visible;
            //else addToAlbumDock.Visibility = Visibility.Collapsed;

            //if (PlayingMedia.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
            //    addToMyChannelDock.Visibility = Visibility.Visible;
            //else addToMyChannelDock.Visibility = Visibility.Collapsed;

            int buttonPosition = (int)target.PointToScreen(new System.Windows.Point(0, 0)).Y;
            int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;

            if ((buttonPosition < POPUP_HEIGHT && bottomSpace >= POPUP_HEIGHT) || bottomSpace >= POPUP_HEIGHT)
            {
                popupMoreOptions.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                //popupMoreOptions.VerticalOffset = -12;
            }
            else
            {
                popupMoreOptions.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
                //popupMoreOptions.VerticalOffset = 12;
            }
            popupMoreOptions.PlacementTarget = target;
            popupMoreOptions.IsOpen = true;

            SetEvents();
        }

        #endregion "public methods"

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupMoreOptions.IsOpen = false;
        }

        private ICommand _PopupOptionClosed;
        public ICommand PopupOptionClosed
        {
            get
            {
                if (_PopupOptionClosed == null) _PopupOptionClosed = new RelayCommand(param => OnPopupOptionsClosed());
                return _PopupOptionClosed;
            }
        }
        private void OnPopupOptionsClosed()
        {
            ClosePopUp();
            Hide();
        }
        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

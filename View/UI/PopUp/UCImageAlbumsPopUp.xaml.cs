﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCImageAlbumsPopUp.xaml
    /// </summary>
    public partial class UCImageAlbumsPopUp : PopUpBaseControl, INotifyPropertyChanged
    {
        public static UCImageAlbumsPopUp Instance;
        private NewStatusViewModel newStatusViewModel;

        #region "Constructor"
        public UCImageAlbumsPopUp(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            SetParent(motherGrid);
        }
        #endregion

        #region "Property"

        private ObservableCollection<AlbumModel> _imageAlbumList;
        public ObservableCollection<AlbumModel> ImageAlbumList
        {
            get
            {
                return _imageAlbumList;
            }
            set
            {
                if(_imageAlbumList == value)
                {
                    return;
                }
                _imageAlbumList = value;
                this.OnPropertyChanged("ImageAlbumList");
            }
        }


        private ICollectionView _imageListView;
        public ICollectionView ImageListView
        {
            get { return this._imageListView; }
            private set
            {
                if (value == this._imageListView)
                {
                    return;
                }

                this._imageListView = value;
                this.OnPropertyChanged("ImageListView");
            }
        }

        private bool _isNoAlbum = true;
        public bool IsNoAlbum
        {
            get
            {
                return _isNoAlbum;
            }
            set
            {
                if(value == this._isNoAlbum)
                {
                    return;
                }

                this._isNoAlbum = value;
                this.OnPropertyChanged("IsAnyAlbum");
            }
        }

        private Visibility _noAlbumToAddTextBlockVisibility = Visibility.Hidden;
        public Visibility NoAlbumToAddTextBlockVisibility
        {
            get
            {
                return _noAlbumToAddTextBlockVisibility;
            }
            set
            {
                if(_noAlbumToAddTextBlockVisibility == value)
                {
                    return;
                }
                _noAlbumToAddTextBlockVisibility = value;
                this.OnPropertyChanged("NoAlbumToAddTextBlockVisibility");
            }
        }

        private Visibility _imageAlbumListBoxVisibility = Visibility.Hidden;
        public Visibility ImageAlbumListBoxVisibility
        {
            get
            {
                return _imageAlbumListBoxVisibility;
            }
            set
            {
                if(_imageAlbumListBoxVisibility == value)
                {
                    return;
                }
                _imageAlbumListBoxVisibility = value;
                this.OnPropertyChanged("ImageAlbumListBoxVisibility");
            }
        }

        private Visibility _loaderVisibility = Visibility.Visible;
        public Visibility LoaderVisibility
        {
            get
            {
                return _loaderVisibility;
            }
            set
            {
                if(_loaderVisibility == value)
                {
                    return;
                }
                _loaderVisibility = value;
                this.OnPropertyChanged("LoaderVisibility");
            }
        }
        #endregion

        #region "Public Method"
        public void ShowImagePopup(UIElement target, NewStatusViewModel newStatusViewModel, bool MakeOpen = true)
        {
            this.newStatusViewModel = newStatusViewModel;
            popupMediaContentAlbum.PlacementTarget = target;
            popupMediaContentAlbum.IsOpen = MakeOpen;
            AlbumMainBorder.Width = 630;
            ImageListView = new ListCollectionView(RingIDViewModel.Instance.MyImageAlubms);
            ImageListView.Filter = new Predicate<object>(this.FilterImageAlbumList);
            ImageAlbumList = RingIDViewModel.Instance.MyImageAlubms;
        }
        #endregion

        #region "Private Method"
        private bool FilterImageAlbumList(object item)
        {
            AlbumModel albumMoel = (AlbumModel)item;
            if(albumMoel.ImageAlbumType == 1 || string.IsNullOrEmpty(albumMoel.AlbumName))
            {
                return false;
            }
            return true;
        }
        #endregion

        #region "OnPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupMediaContentAlbum.IsOpen = false;
        }

        private ICommand _imageAlbumsListBoxScrollChangedEventCommand;
        public ICommand ImageAlbumsListBoxScrollChangedEventCommand
        {
            get
            {
                _imageAlbumsListBoxScrollChangedEventCommand = _imageAlbumsListBoxScrollChangedEventCommand ?? new RelayCommand(param => onImageAlbumsListBoxScrollChangedEvent(param));
                return _imageAlbumsListBoxScrollChangedEventCommand;
            }
        }

        private void onImageAlbumsListBoxScrollChangedEvent(object param)
        {
            if (RingIDViewModel.Instance.MyImageAlubms.Count < DefaultSettings.MY_IMAGE_ALBUMS_COUNT)
            {
                Task.Factory.StartNew(() =>
                {
                    SendDataToServer.ListOfImageAlbumsOfUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_IMAGE, RingIDViewModel.Instance.ImageAlbumNpUUId);
                });
            }
        }

        private ICommand _AlbumMouseSelected;
        public ICommand AlbumMouseSelected
        {
            get
            {
                if (_AlbumMouseSelected == null) _AlbumMouseSelected = new RelayCommand(param => OnAlbumMouseSelected(param));
                return _AlbumMouseSelected;
            }
        }
        private void OnAlbumMouseSelected(object sender)
        {
            if (sender is Border)
            {
                Border bd = (Border)sender;
                AlbumModel model = (AlbumModel)bd.DataContext;
                popupMediaContentAlbum.IsOpen = false;
                newStatusViewModel.ImageAlbumTitleTextBoxText = model.AlbumName;
                newStatusViewModel.PrivacyValue = model.AlbumPrivacy;
                newStatusViewModel.IsPrivacyButtonEnabled = false;
                newStatusViewModel.AddToImageAlbumButtonVisibility = Visibility.Collapsed;
                newStatusViewModel.SelectedImageAlbumCrossButtonVisibility = Visibility.Visible;
                newStatusViewModel.IsImageAlbumTitleTextBoxEnabled = false;
                newStatusViewModel.IsImageAlbumSelectedFromExisting = true;
            }
        }

        private ICommand _popupImageAlbumClosed;
        public ICommand popupImageAlbumClosed
        {
            get
            {
                if (_popupImageAlbumClosed == null) _popupImageAlbumClosed = new RelayCommand(param => OnpopupImageAlbumClosed());
                return _popupImageAlbumClosed;
            }
        }
        private void OnpopupImageAlbumClosed()
        {
            Hide();
        }
        #endregion
    }
}

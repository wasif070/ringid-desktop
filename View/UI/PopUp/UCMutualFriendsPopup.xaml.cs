﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.ViewModel;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMutualFriendsPopup.xaml
    /// </summary>
    public partial class UCMutualFriendsPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMutualFriendsPopup).Name);

        public static UCMutualFriendsPopup Instance;
        public long utId;

        #region "Constructor"
        public UCMutualFriendsPopup()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Prperty"
        private ObservableCollection<UserBasicInfoModel> _MutualList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> MutualList
        {
            get
            {
                return _MutualList;
            }
            set
            {
                _MutualList = value;
                this.OnPropertyChanged("MutualList");
            }
        }

        private string _FullName;
        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                _FullName = value;
                this.OnPropertyChanged("FullName");
            }
        }

        public int Total { get; set; }
        #endregion

        #region "OnPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Public Method"
        public void SetVisibilities()
        {
            try
            {
                if (topLoader.Visibility != Visibility.Collapsed)
                    topLoader.Visibility = Visibility.Collapsed;
                seeMore.Visibility = (MutualList.Count < Total) ? Visibility.Visible : Visibility.Collapsed;
                if (bottomLoader.Visibility != Visibility.Collapsed)
                    bottomLoader.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public void HideLoaderandSeeMore()
        {
            try
            {
                topLoader.Visibility = Visibility.Collapsed;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void ShowPopup(UIElement target, UserBasicInfoModel model)
        {
            try
            {
                Instance = this;
                this.utId = model.ShortInfoModel.UserTableID;
                this.FullName = model.ShortInfoModel.FullName;

                FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.Remove(this.utId);
                topLoader.Visibility = Visibility.Visible;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Collapsed;

                if (popupMutual.IsOpen)
                {
                    popupMutual.IsOpen = false;
                }
                else
                {
                    popupMutual.PlacementTarget = target;

                    popupMutual.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void ShowPopup(UIElement target, long utId, string Name)
        {
            try
            {
                Instance = this;
                this.utId = utId;
                this.FullName = Name;

                FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.Remove(this.utId);
                topLoader.Visibility = Visibility.Visible;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Collapsed;

                if (popupMutual.IsOpen)
                {
                    popupMutual.IsOpen = false;
                }
                else
                {
                    popupMutual.PlacementTarget = target;

                    popupMutual.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        #endregion

        private void HidePopup()
        {
            if (popupMutual.IsOpen)
            {
                popupMutual.IsOpen = false;
            }
        }
        private void SeeMore_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                topLoader.Visibility = Visibility.Collapsed;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Visible;
                LoadMutualContactFromDictionary();
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        private void LoadMutualContactFromDictionary()
        {
            try
            {
                FriendInfoDTO fiDTO;
                FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.TryGetValue(utId, out fiDTO);
                if (fiDTO != null && MutualList.Count < fiDTO.FriendList.Count)
                {
                    int i = 0;
                    while (i < DefaultSettings.CONTENT_SHOW_LIMIT && (MutualList.Count + i) < fiDTO.FriendList.Count)
                    {
                        long id = fiDTO.FriendList[MutualList.Count + i];
                        i++;
                        FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Add(id);
                    }
                    MainSwitcher.AuthSignalHandler().contactListHandler.ShowFriendMutualContactList(utId, FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: LoadMutualContactFromDictionary() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        private void txtBL_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock control = (TextBlock)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            RingIDViewModel.Instance.OnFriendProfileButtonClicked(model.ShortInfoModel.UserTableID);
        }


        public UIElement center { get; set; }

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            HidePopup();
        }
        private ICommand popupClosedCommand;
        public ICommand PopupClosedCommand
        {
            get
            {
                if (popupClosedCommand == null) popupClosedCommand = new RelayCommand(param => OnPopupClosedCommand());
                return popupClosedCommand;
            }
        }
        private void OnPopupClosedCommand()
        {
            Hide();
        }
        #endregion

    }
}

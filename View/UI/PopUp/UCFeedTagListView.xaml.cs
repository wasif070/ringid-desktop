﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCFeedTagListView.xaml
    /// </summary>
    public partial class UCFeedTagListView : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFeedTagListView).Name);

        public Guid nfid;
        public static UCFeedTagListView Instance;

        #region "Constructor"
        public UCFeedTagListView(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
            //Visibility = Visibility.Hidden;
        }
        #endregion

        #region "Property"
        private ObservableCollection<BaseUserProfileModel> _TagList = new ObservableCollection<BaseUserProfileModel>();
        public ObservableCollection<BaseUserProfileModel> TagList
        {
            get
            {
                return _TagList;
            }
            set
            {
                _TagList = value;
                this.OnPropertyChanged("TagList");
            }
        }
        #endregion

        #region "OnPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Public Method"
        public void ShowTagView(Guid nfid)
        {
            try
            {
                TagList.Clear();

                this.nfid = nfid;

                OnLoadedControl();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public void ShowSharersView(ObservableCollection<BaseUserProfileModel> sharerList = null)
        {
            try
            {
                TagList.Clear();

                this.TagList = sharerList;

                titleBlck.Text = "Shared this Feed";
                OnLoadedControl();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public void ShowTagView(ObservableCollection<BaseUserProfileModel> frndTagList)
        {
            try
            {
                TagList = frndTagList;

                OnLoadedControl();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        private void DoCancel()
        {
            Visibility = Visibility.Hidden;
            TagList = null;
            Hide();
        }

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        ICommand _blackSpaceClick;
        public ICommand CloseWindowCommand
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }
        private void OnClickBlackSpace(object param)
        {
            DoCancel();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {
            OnLoadedControl();
        }

        ICommand _txtBLMouseLeftButtonUp;
        public ICommand txtBLMouseLeftButtonUp
        {
            get
            {
                if (_txtBLMouseLeftButtonUp == null)
                {
                    _txtBLMouseLeftButtonUp = new RelayCommand(param => OntxtBLMouseLeftButtonUp(param));
                }
                return _txtBLMouseLeftButtonUp;
            }
        }

        private void OntxtBLMouseLeftButtonUp(object sender)
        {
            if (sender is BaseUserProfileModel && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
            {
                try
                {
                    DoCancel();
                    BaseUserProfileModel PostOwner = (BaseUserProfileModel)sender;

                    RingIDViewModel.Instance.OnUserProfileClicked(PostOwner);
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
        #endregion
    }
}

﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCUploadingPopup.xaml
    /// </summary>
    public partial class UCUploadingPopup : PopUpBaseControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCUploadingPopup).Name);

        public UCUploadingPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            //InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
        public void ShowUploadingPopup(UIElement target)
        {
            try
            {
                lock (RingIDViewModel.Instance.UploadingModelList)
                {
                    foreach(var model in RingIDViewModel.Instance.UploadingModelList)
                    {
                        if (model.IsUploadFailed)
                            RingIDViewModel.Instance.UploadingModelList.Remove(model);
                    }
                }

                if (popupUploading.IsOpen)
                {
                    popupUploading.IsOpen = false;
                }
                else
                {
                    popupUploading.PlacementTarget = target;

                    popupUploading.IsOpen = true;
                }
                popupUploading.Closed += popupUploading_Closed;
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public void ClosePopUp()
        {
            popupUploading.Closed -= popupUploading_Closed;
        }

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupUploading.IsOpen = false;
        }
        #endregion

        private void popupUploading_Closed(object sender, EventArgs e)
        {
            Hide();
            ClosePopUp();
        }
    }
}

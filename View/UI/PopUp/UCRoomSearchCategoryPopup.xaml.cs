﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Chat.Service;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCCelebrityDiscoverCategoryPopup.xaml
    /// </summary>
    public partial class UCRoomSearchCategoryPopup : UserControl, INotifyPropertyChanged
    {
        public static UCRoomSearchCategoryPopup Instance;
        Func<int> _OnClosing = null;
        private bool _IsRoomCategorySync = false;

        #region "Constructor"
        public UCRoomSearchCategoryPopup()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion 

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region "Property"

        private int _HeightPopup = 300;  
        public int HeightPopup
        {
            get
            {
                return _HeightPopup;
            }
            set
            {
                if (value == _HeightPopup)
                    return;
                _HeightPopup = value;
                OnPropertyChanged("HeightPopup");
            }
        }

        private string _catName = "";  
        public string CatName
        {
            get
            {
                return _catName;
            }
            set
            {
                if (value == _catName)
                    return;
                _catName = value;
                OnPropertyChanged("CatName");
            }
        }

        private RoomCategoryModel _RoomCategoryModel;
        public RoomCategoryModel RoomCategoryModel
        {
            get
            {
                return _RoomCategoryModel;
            }
            set
            {
                if (value == _RoomCategoryModel)
                    return;
                _RoomCategoryModel = value;
                OnPropertyChanged("RoomCategoryModel");
            }
        }

        private int _IsReset = 0;  // 1=reset, 2=done
        public int IsReset
        {
            get
            {
                return _IsReset;
            }
            set
            {
                if (value == _IsReset)
                    return;
                _IsReset = value;
                OnPropertyChanged("IsReset");
            }
        }
        #endregion 

        #region "Public Method"
        public void Show(UIElement target, Func<int> onClosing)
        {
            if (popupCategory.IsOpen)
                popupCategory.IsOpen = false;
            else
            {
                if (_IsRoomCategorySync == false)
                {
                    RoomCategoryModel lastCategoryModel = RingIDViewModel.Instance.RoomCategoryList.OrderByDescending(P => P.CategoryName).FirstOrDefault();
                    String lastCategory = lastCategoryModel != null ? lastCategoryModel.CategoryName : String.Empty;
                    ChatService.GetPublicChatRoomCategoryList(lastCategory, 50, (args) => 
                    {
                        _IsRoomCategorySync = args.Status;
                    });
                }

                IsReset = 0;
                _OnClosing = onClosing;
                popupCategory.Closed += popupCategory_Closed;
                CategorySearchTextBox.TextChanged += CategorySearchTextBox_TextChanged;
                popupCategory.PlacementTarget = target;
                popupCategory.IsOpen = true;
                this.Focusable = true;
                this.Focus();
            }
        }
        #endregion

        #region "Event Trigger"
        private void popupCategory_Closed(object sender, EventArgs e)
        {
            if (_OnClosing != null)
            {
                if (IsReset == 1)
                {
                    CatName = "";
                    List<RoomCategoryModel> list = RingIDViewModel.Instance.RoomCategoryList.ToList();
                    foreach (var item in list)
                    {
                        item.TempSelected = true;
                        item.VisibilityInCountrySearch = Visibility.Visible;
                    }
                    CategorySearchTextBox.Text = "";
                }
                else if(IsReset == 0)
                {
                    List<RoomCategoryModel> list = RingIDViewModel.Instance.RoomCategoryList.ToList();
                    foreach (var item in list)
                    {
                        if (item.CategoryName == CatName)
                            item.TempSelected = false;
                        else
                            item.TempSelected = true;
                    }
                }
                popupCategory.Closed -= popupCategory_Closed;
                CategorySearchTextBox.TextChanged -= CategorySearchTextBox_TextChanged;
                _OnClosing();
            }
        }

        private void categoryBtn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.DataContext is RoomCategoryModel)
            {
                RoomCategoryModel model = (RoomCategoryModel)btn.DataContext;
                //CatID = model.CategoryId;
                //CatName = model.CategoryName;
                RoomCategoryModel = model;
                List<RoomCategoryModel> list = RingIDViewModel.Instance.RoomCategoryList.ToList();
                    foreach (var item in list)
                    {
                    if (item.CategoryName == model.CategoryName)
                        item.TempSelected = false;
                    else
                        item.TempSelected = true;
                }
            }
        }

        private void CategorySearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = CategorySearchTextBox.Text;
            int countVisible = 0;

            List<RoomCategoryModel> list = RingIDViewModel.Instance.RoomCategoryList.ToList();
                    foreach (var item in list)
                    {
                //if (item.CategoryName.ToLower().Contains(SearchText.ToLower())) { item.VisibilityInCountrySearch = Visibility.Visible; countVisible++; }
                if (item.CategoryName.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0) { item.VisibilityInCountrySearch = Visibility.Visible; countVisible++; }
                else item.VisibilityInCountrySearch = Visibility.Collapsed;
            }
            HeightPopup = (countVisible < 6) ? (countVisible * 30) + 50 + 70 : 300;
        }

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                if (Instance != null && popupCategory.IsOpen)
                    popupCategory.IsOpen = false;
            }
        }
        #endregion 

        #region "ICommand"
        ICommand _resetDoneClick;
        public ICommand ResetDoneClick
        {
            get
            {
                if (_resetDoneClick == null)
                {
                    _resetDoneClick = new RelayCommand(param => OnResetDoneCommand(param));
                }
                return _resetDoneClick;
            }
        }
        private void OnResetDoneCommand(object param)
        {
            IsReset = int.Parse(param.ToString());
            if (IsReset == 1)
            {
                CatName = "";
            }
            else if(IsReset == 2)
            {
                CatName = RoomCategoryModel.CategoryName;
            }

            if (popupCategory.IsOpen)
                popupCategory.IsOpen = false;
        }
        #endregion "ICommand"
    }
}

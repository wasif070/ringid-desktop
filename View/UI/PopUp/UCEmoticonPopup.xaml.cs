﻿using log4net;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCFeedEmoticonPopup.xaml
    /// </summary>
    public partial class UCEmoticonPopup : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCEmoticonPopup).Name);

        public const int TYPE_DEFAULT = 0;
        public const int TYPE_NEW_STATUS = 1;
        public const int TYPE_CALL_CHAT_PANEL = 2;
        public const int TYPE_CHAT_EDIT_PANEL = 3;

        private const int EMO_POPUP_HEIGHT = 344;
        private int _ShowUpArrowPopup = 1;//0 - Default, 1 - UP, 2 - Down
        private ICommand _EmoticonClickCommand;
        private string _EmoticonFocused = String.Empty;
        private string _EmoticonSelected = String.Empty;
        private ObservableCollection<EmoticonModel> _EmoticonModelList = new ObservableCollection<EmoticonModel>();
        public int _Type = UCEmoticonPopup.TYPE_DEFAULT;
        public bool _IsWindowExpanded = false;

        public UCEmoticonPopup()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        void popupEmoticon_Closed(object sender, EventArgs e)
        {
            Type = UCEmoticonPopup.TYPE_DEFAULT;
            popupEmoticon.Closed -= popupEmoticon_Closed;
            if (_OnClosing != null)
            {
                _OnClosing(_EmoticonSelected);
            }
        }

        public string EmoticonFocused
        {
            get { return _EmoticonFocused; }
            set
            {
                _EmoticonFocused = value;
                this.OnPropertyChanged("EmoticonFocused");
            }
        }

        public int Type
        {
            get { return _Type; }
            set
            {
                _Type = value;
                this.OnPropertyChanged("Type");
            }
        }

        public ObservableCollection<EmoticonModel> EmoticonModelList
        {
            get { return _EmoticonModelList; }
            set
            {
                _EmoticonModelList = value;
                this.OnPropertyChanged("EmoticonModelList");
            }
        }

        public ICommand EmoticonClickCommand
        {
            get
            {
                if (_EmoticonClickCommand == null)
                {
                    _EmoticonClickCommand = new RelayCommand((param) => OnEmoticonClick(param));
                }
                return _EmoticonClickCommand;
            }
        }

        public int ShowUpArrowPopup
        {
            get { return _ShowUpArrowPopup; }
            set
            {
                if (_ShowUpArrowPopup == value)
                {
                    return;
                }
                _ShowUpArrowPopup = value;
                this.OnPropertyChanged("ShowUpArrowPopup");
            }
        }

        private void EmoList_MouseEnter(object sender, MouseEventArgs e)
        {
            EmoticonFocused = ((FrameworkElement)sender).Tag.ToString();
        }

        private void EmoList_MouseLeave(object sender, MouseEventArgs e)
        {
            EmoticonFocused = String.Empty;
        }

        public void ClosePopUp()
        {
            popupEmoticon.IsOpen = false;
        }
        private void OnEmoticonClick(object param)
        {
            _EmoticonSelected = param.ToString();
            ClosePopUp();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        Func<string, int> _OnClosing = null;

        public void Show(UIElement target, int type, Func<string, int> onClosing)
        {
            try
            {
                Type = type;
                _EmoticonSelected = String.Empty;
                _OnClosing = onClosing;

                if (popupEmoticon.IsOpen)
                {
                    popupEmoticon.IsOpen = false;
                }
                else
                {
                    popupEmoticon.Closed -= popupEmoticon_Closed;
                    popupEmoticon.Closed += popupEmoticon_Closed;
                    popupEmoticon.PlacementTarget = target;

                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;
                    if (buttonPosition < EMO_POPUP_HEIGHT && bottomSpace >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 1;
                    }
                    else if (buttonPosition >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 2;
                    }
                    else
                    {
                        ShowUpArrowPopup = 0;
                    }

                    if (type == UCEmoticonPopup.TYPE_NEW_STATUS)
                    {
                        if (ShowUpArrowPopup == 1)
                        {
                            popupEmoticon.HorizontalOffset = -239;
                            popupEmoticon.VerticalOffset = -3;
                        }
                        else if (ShowUpArrowPopup == 2)
                        {
                            popupEmoticon.HorizontalOffset = -239;
                            popupEmoticon.VerticalOffset = 5;
                        }
                        else
                        {
                            popupEmoticon.HorizontalOffset = -239;
                            popupEmoticon.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                        }
                    }
                    else if (type == UCEmoticonPopup.TYPE_CALL_CHAT_PANEL || type == UCEmoticonPopup.TYPE_CHAT_EDIT_PANEL)
                    {
                        if (ShowUpArrowPopup == 1)
                        {
                            popupEmoticon.HorizontalOffset = -252;
                            popupEmoticon.VerticalOffset = -10;
                        }
                        else if (ShowUpArrowPopup == 2)
                        {
                            popupEmoticon.HorizontalOffset = -252;
                            popupEmoticon.VerticalOffset = 0;
                        }
                        else
                        {
                            popupEmoticon.HorizontalOffset = -252;
                            popupEmoticon.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                        }
                    }
                    else
                    {
                        if (ShowUpArrowPopup == 1)
                        {
                            popupEmoticon.HorizontalOffset = -250;
                            popupEmoticon.VerticalOffset = -3;
                        }
                        else if (ShowUpArrowPopup == 2)
                        {
                            popupEmoticon.HorizontalOffset = -250;
                            popupEmoticon.VerticalOffset = 5;
                        }
                        else
                        {
                            popupEmoticon.HorizontalOffset = -250;
                            popupEmoticon.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                        }
                    }

                    if (EmoticonModelList.Count <= 0)
                    {
                        foreach (EmoticonDTO emoticonDTO in DefaultDictionaries.Instance.EMOTICON_DICTIONARY.Values)
                        {
                            EmoticonModelList.Add(new EmoticonModel(emoticonDTO));
                        }
                    }

                    popupEmoticon.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: Show() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
    }
}
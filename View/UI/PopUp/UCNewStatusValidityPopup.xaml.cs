﻿using log4net;
using Models.Constants;
using Models.DAO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.Feed;
using View.ViewModel;
using View.Utility;
using View.ViewModel.NewStatus;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCNewStatusValidityPopup.xaml
    /// </summary>
    public partial class UCNewStatusValidityPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCNewStatusValidityPopup).Name);

        /// <summary>
        /// false = UP
        /// true = Down
        /// </summary>
        private bool _ShowUpArrowPopup = false;
        private const int VALIDITY_POPUP_HEIGHT = 179;
        private bool isFromEditPopup = false;
        private NewStatusViewModel newStatusViewModel;

        #region Properties

        private string _ValidityString = "";
        public string ValidityString
        {
            get { return _ValidityString; }
            set
            {
                _ValidityString = value;
                this.OnPropertyChanged("ValidityString");
            }
        }
        public bool ShowUpArrowPopup
        {
            get { return _ShowUpArrowPopup; }
            set
            {
                if (_ShowUpArrowPopup == value)
                {
                    return;
                }
                _ShowUpArrowPopup = value;
                this.OnPropertyChanged("ShowUpArrowPopup");
            }
        }

        private ObservableCollection<ValidityModel> _ValidityCollection = new ObservableCollection<ValidityModel>();
        public ObservableCollection<ValidityModel> ValidityCollection
        {
            get
            {
                return _ValidityCollection;
            }
            set
            {
                _ValidityCollection = value;
                this.OnPropertyChanged("ValidityCollection");
            }
        }

        #endregion

        #region Constructor

        public UCNewStatusValidityPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            SetParent(motherGrid);
            LoadValidityArray();
        }

        #endregion

        #region Private Method

        private void LoadValidityArray()
        {
            ValidityCollection.Clear();
            ValidityModel validityList;
            for (int i = 1; i <= 30; i++)
            {
                validityList = new ValidityModel(i.ToString(), false);
                ValidityCollection.Add(validityList);
            }
            validityList = new ValidityModel(AppConstants.UNLIMITED_VALIDITY_STRING, false);
            ValidityCollection.Add(validityList);
        }
        #endregion

        #region Public Method

        public void ShowPopup(UIElement target, NewStatusViewModel newStatusViewModel, int editPopUpValidity = -2)
        {
            try
            {
                this.newStatusViewModel = newStatusViewModel;
                switch (editPopUpValidity)
                {
                    case -2: //newstat def
                        isFromEditPopup = false;
                        foreach (var model in ValidityCollection)
                        {
                            if (!model.ValidityString.Equals(AppConstants.UNLIMITED_VALIDITY_STRING) && SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY == Convert.ToInt32(model.ValidityString))
                            {
                                model.IsSelected = true;
                            }
                            else model.IsSelected = false;
                        }
                        break;
                    case AppConstants.DEFAULT_VALIDITY:
                        isFromEditPopup = true;
                        foreach (var model in ValidityCollection)
                        {
                            if (model.ValidityString.Equals(AppConstants.UNLIMITED_VALIDITY_STRING))
                            {
                                model.IsSelected = true;
                            }
                            else model.IsSelected = false;
                        }
                        break;
                    default: //edit others
                        isFromEditPopup = true;
                        foreach (var model in ValidityCollection)
                        {
                            if (!model.ValidityString.Equals(AppConstants.UNLIMITED_VALIDITY_STRING) && editPopUpValidity == Convert.ToInt32(model.ValidityString))
                            {
                                model.IsSelected = true;
                            }
                            else model.IsSelected = false;
                        }
                        break;
                }
                if (popupValidity.IsOpen == false)
                {
                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;
                    if ((buttonPosition < VALIDITY_POPUP_HEIGHT && bottomSpace >= VALIDITY_POPUP_HEIGHT) || bottomSpace >= VALIDITY_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = true;
                    }
                    else
                    {
                        ShowUpArrowPopup = false;
                    }
                    popupValidity.PlacementTarget = target;
                    popupValidity.IsOpen = true;
                }
                else
                {
                    popupValidity.IsOpen = false;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region ICommands

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupValidity.IsOpen = false;
        }

        private ICommand _popupValidityClosed;
        public ICommand popupValidityClosed
        {
            get
            {
                if (_popupValidityClosed == null) _popupValidityClosed = new RelayCommand(param => OnpopupValidityClosed());
                return _popupValidityClosed;
            }
        }

        private void OnpopupValidityClosed()
        {
            Hide();
        }

        private ICommand _BdMouseLeftButtonUp;
        public ICommand BdMouseLeftButtonUp
        {
            get
            {
                if (_BdMouseLeftButtonUp == null) _BdMouseLeftButtonUp = new RelayCommand(param => OnBdMouseLeftButtonUp(param));
                return _BdMouseLeftButtonUp;
            }
        }
        private void OnBdMouseLeftButtonUp(object parameter)
        {
            try
            {
                if(parameter is Border)
                {
                    popupValidity.IsOpen = false;
                    Border validitySelectionBorder = (Border)parameter;
                    foreach (var model in ValidityCollection)
                    {
                        model.IsSelected = false;
                    }
                    ValidityModel validityModel = (ValidityModel)validitySelectionBorder.DataContext;

                    if (!isFromEditPopup)
                    {
                        if (validityModel.ValidityString == AppConstants.UNLIMITED_VALIDITY_STRING)
                            SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY = AppConstants.DEFAULT_VALIDITY;
                        else
                        {
                            SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY = Convert.ToInt32(validityModel.ValidityString);
                            validityModel.IsSelected = true;
                        }

                        new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_NEWS_FEED_EXPIRY, SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY.ToString());
                        if (RingIDViewModel.Instance._WNRingIDSettings != null && RingIDViewModel.Instance._WNRingIDSettings._UCGeneralSettings != null)
                        {
                            RingIDViewModel.Instance._WNRingIDSettings._UCGeneralSettings.SetSelectedModelValuesForFeedValidityPopup();
                        }
                    }
                    newStatusViewModel.SetSelectedModelValuesForValidityPopup(isFromEditPopup, validityModel.ValidityString == AppConstants.UNLIMITED_VALIDITY_STRING ? 0 : Convert.ToInt32(validityModel.ValidityString));
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Feed;
using View.ViewModel;
using View.ViewModel.NewStatus;
using System.Windows.Data;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCShareSingleFeedView.xaml
    /// </summary>
    public partial class UCShareSingleFeedView : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCShareSingleFeedView).Name);
        public static UCShareSingleFeedView Instance;

        #region Constructor

        public UCShareSingleFeedView(Grid motherGrid)
        {
            Instance = this;
            UCShareSingleFeedViewInstance = Instance;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
            Visibility = Visibility.Hidden;
        }

        #endregion

        #region Property

        private UCLocationPopUp LocationPopUp { get; set; }
        private UCNewStatusTagPopup ucNewStatusTagPopup { get; set; }
        private UCNewStatusFeelingPopup ucNewStatusFeelingPopup { get; set; }

        #endregion

        #region Property Change

        private UCShareSingleFeedView _UCShareSingleFeedViewInstance;
        public UCShareSingleFeedView UCShareSingleFeedViewInstance
        {
            get { return _UCShareSingleFeedViewInstance; }
            set
            {
                if (value == _UCShareSingleFeedViewInstance)
                    return;
                _UCShareSingleFeedViewInstance = value;
                this.OnPropertyChanged("UCShareSingleFeedViewInstance");
            }
        }

        private FeedModel _feedModel;
        public FeedModel FeedModel
        {
            get
            {
                return _feedModel;
            }
            set
            {
                _feedModel = value;
                this.OnPropertyChanged("FeedModel");
            }
        }

        private SingleMediaModel _singleMediaModel;
        public SingleMediaModel SingleMediaModel
        {
            get
            {
                return _singleMediaModel;
            }
            set
            {
                _singleMediaModel = value;
                this.OnPropertyChanged("SingleMediaModel");
            }
        }

        private bool _isSingleMedia;
        public bool IsSingleMedia
        {
            get 
            {
                return _isSingleMedia; 
            }
            set
            {
                if (value == _isSingleMedia)
                    return;
                _isSingleMedia = value;
                this.OnPropertyChanged("IsSingleMedia");
            }
        }

        private bool _emoPopup;
        public bool EmoPopup
        {
            get
            { 
                return _emoPopup; 
            }
            set
            {
                if (value == _emoPopup)
                    return;
                _emoPopup = value;
                this.OnPropertyChanged("EmoPopup");
            }
        }

        public long SelectedDoingId;
        private DoingModel _selectedDoingModel = null;
        public DoingModel SelectedDoingModel
        {
            get 
            { 
                return _selectedDoingModel; 
            }
            set
            {
                if (value == _selectedDoingModel)
                    return;
                _selectedDoingModel = value;
                this.OnPropertyChanged("SelectedDoingModel");
            }
        }

        private bool _tagPopup;
        public bool TagPopup
        {
            get 
            { 
                return _tagPopup; 
            }
            set
            {
                if (value == _tagPopup)
                    return;
                _tagPopup = value;
                this.OnPropertyChanged("TagPopup");
            }
        }

        private ObservableCollection<LocationModel> _locationSuggestions = new ObservableCollection<LocationModel>();
        public ObservableCollection<LocationModel> LocationSuggestions
        {
            get
            {
                return _locationSuggestions;
            }
            set
            {
                _locationSuggestions = value;
                this.OnPropertyChanged("LocationSuggestions");
            }
        }

        private string _selectedText = null;
        public string SelectedText
        {
            get 
            { 
                return _selectedText; 
            }
            set
            {
                if (value == _selectedText)
                    return;
                _selectedText = value;
                this.OnPropertyChanged("SelectedText");
            }
        }

        private LocationModel _selectedLocationModel = null;
        public LocationModel SelectedLocationModel
        {
            get 
            { 
                return _selectedLocationModel; 
            }
            set
            {
                if (value == _selectedLocationModel)
                    return;
                _selectedLocationModel = value;
                this.OnPropertyChanged("SelectedLocationModel");
            }
        }

        private Visibility _mapPopupVisibility = Visibility.Collapsed;
        public Visibility MapPopupVisibility
        {
            get 
            { 
                return _mapPopupVisibility; 
            }
            set
            {
                if (value == _mapPopupVisibility)
                    return;
                _mapPopupVisibility = value;
                this.OnPropertyChanged("MapPopupVisibility");
            }
        }

        private BitmapImage _bitmapImage;
        public BitmapImage BitmapImage
        {
            get 
            { 
                return _bitmapImage; 
            }
            set
            {
                if (value == _bitmapImage)
                    return;
                _bitmapImage = value;
                this.OnPropertyChanged("BitmapImage");
            }
        }

        public List<long> TaggedUtids = null;
        private ObservableCollection<UserShortInfoModel> _taggedFriends;
        public ObservableCollection<UserShortInfoModel> TaggedFriends
        {
            get
            {
                return _taggedFriends;
            }
            set
            {
                _taggedFriends = value;
                this.OnPropertyChanged("TaggedFriends");
            }
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region "Icommands"

        private ICommand _loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (_loadedControl == null) _loadedControl = new RelayCommand(param => onLoadedControl());
                return _loadedControl;
            }
        }

        private void onLoadedControl()
        {
            rtbFeedEdit.Focusable = true;
            rtbFeedEdit.Focus();
        }

        private ICommand _blackSpaceClick;
        public ICommand BlackSpaceClick
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => onClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }

        private void onClickBlackSpace(object param)
        {
            if(param is string)
            {
                bool FromTextBx = Convert.ToBoolean(param);

                if(FromTextBx)
                {
                    if (LocationPopUp != null && LocationPopUp.popupLocation.IsOpen == true)
                    {
                        LocationPopUp.popupLocation.StaysOpen = false;
                        LocationPopUp.popupLocation.IsOpen = false;
                    }
                    else
                    {
                        Hide();
                        HideShareView();
                    }                    
                }
                else
                {
                    if (LocationPopUp != null && LocationPopUp.popupLocation.IsOpen == true)
                    {
                        LocationPopUp.popupLocation.StaysOpen = false;
                        LocationPopUp.popupLocation.IsOpen = false;
                    }
                    Hide();
                    HideShareView();
                }
            }
        }

        private ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => onCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void onCommentSpaceClicked(object param)
        {
            if (LocationPopUp != null
                && LocationPopUp.popupLocation.IsOpen == true)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
                LocationPopUp.popupLocation.IsOpen = false;
            }
        }

        private ICommand _cancelTagFriendClicked;
        public ICommand CancelTagFriendClicked
        {
            get
            {
                if (_cancelTagFriendClicked == null)
                {
                    _cancelTagFriendClicked = new RelayCommand(param => onCancelTagFriendClicked(param));
                }
                return _cancelTagFriendClicked;
            }
        }

        private void onCancelTagFriendClicked(object param)
        {
            if (param is UserShortInfoModel)
            {
                UserShortInfoModel model = (UserShortInfoModel)param;
                model.IsVisibleInTagList = false;
                TaggedFriends.Remove(model);
                OnPropertyChanged("UCShareSingleFeedViewInstance");
                TaggedUtids.Remove(model.UserTableID);
                if (TaggedFriends.Count == 0 && TagListHolder.Visibility == Visibility.Visible)
                {
                    TagListHolder.Visibility = Visibility.Collapsed;
                }
            }
        }

        private ICommand _locationCrossButtonClicked;
        public ICommand LocationCrossButtonClicked
        {
            get
            {
                if (_locationCrossButtonClicked == null)
                {
                    _locationCrossButtonClicked = new RelayCommand(param => onLocationCrossButtonClicked());
                }
                return _locationCrossButtonClicked;
            }
        }

        private void onLocationCrossButtonClicked()
        {
            SelectedLocationModel = null;
            lctnTextBox.Text = "";
            lctnTextBox.Focus();
        }

        private ICommand _ShareFeelingBtnClicked;
        public ICommand ShareFeelingBtnClicked
        {
            get
            {
                if (_ShareFeelingBtnClicked == null)
                {
                    _ShareFeelingBtnClicked = new RelayCommand(param => onShareFeelingBtnClicked());
                }
                return _ShareFeelingBtnClicked;
            }
        }

        private void onShareFeelingBtnClicked()
        {
            if (lctnMainBorderPanel.Visibility == Visibility.Visible)
            {
                lctnMainBorderPanel.Visibility = Visibility.Collapsed;
            }
            if (TagListHolder.Visibility == Visibility.Visible)
            {
                TagListHolder.Visibility = Visibility.Collapsed;
            }
            if (FeelingDoingBtnContainer.Visibility == Visibility.Visible)
            {
                FeelingDoingBtnContainer.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (SelectedDoingModel != null)
                    FeelingDoingBtnContainer.Visibility = Visibility.Visible;
            }
            EmoPopup = true;
            RingIDViewModel.Instance.LoadDoingModels();
            if (ucNewStatusFeelingPopup != null)
                gridPanel.Children.Remove(ucNewStatusFeelingPopup);
            if (ucNewStatusFeelingPopup == null)
                ucNewStatusFeelingPopup = new UCNewStatusFeelingPopup(gridPanel);
            ucNewStatusFeelingPopup.Show();
            ucNewStatusFeelingPopup.ShowFeelingPopup(feelingBtn, this);
            ucNewStatusFeelingPopup.OnRemovedUserControl += () =>
            {
                EmoPopup = false;
                if (this.IsVisible)
                {
                    rtbFeedEdit.Focusable = true;
                    rtbFeedEdit.Focus();
                }
            };
        }

        private ICommand _shareTagFriendsClicked;
        public ICommand ShareTagFriendsClicked
        {
            get
            {
                if (_shareTagFriendsClicked == null)
                {
                    _shareTagFriendsClicked = new RelayCommand(param => onShareTagFriendsClicked());
                }
                return _shareTagFriendsClicked;
            }
        }

        private void onShareTagFriendsClicked()
        {
            if (lctnMainBorderPanel.Visibility == Visibility.Visible)
            {
                lctnMainBorderPanel.Visibility = Visibility.Collapsed;
            }
            if (FeelingDoingBtnContainer.Visibility == Visibility.Visible)
            {
                FeelingDoingBtnContainer.Visibility = Visibility.Collapsed;
            }
            if (TaggedFriends != null && TaggedFriends.Count > 0 && TagListHolder.Visibility == Visibility.Collapsed)
            {
                TagListHolder.Visibility = Visibility.Visible;
            }
            else
            {
                TagListHolder.Visibility = Visibility.Collapsed;
            }

            TagPopup = true;
            if (ucNewStatusTagPopup != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNewStatusTagPopup);
            ucNewStatusTagPopup = new UCNewStatusTagPopup(UCGuiRingID.Instance.MotherPanel);
            ucNewStatusTagPopup.Show();
            ucNewStatusTagPopup.ShowTagPopup(shareTagFrnd, this);
            ucNewStatusTagPopup.OnRemovedUserControl += () =>
            {
                ucNewStatusTagPopup = null;
                TagPopup = false;
            };
        }

        private ICommand _shareLocatinClicked;
        public ICommand ShareLocatinClicked
        {
            get
            {
                if (_shareLocatinClicked == null)
                {
                    _shareLocatinClicked = new RelayCommand(param => onShareLocatinClicked());
                }
                return _shareLocatinClicked;
            }
        }

        private void onShareLocatinClicked()
        {
            if (TagListHolder.Visibility == Visibility.Visible)
            {
                TagListHolder.Visibility = Visibility.Collapsed;
            }
            if (FeelingDoingBtnContainer.Visibility == Visibility.Visible)
            {
                FeelingDoingBtnContainer.Visibility = Visibility.Collapsed;
            }
            if (lctnMainBorderPanel.Visibility == Visibility.Visible)
            {
                lctnMainBorderPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                lctnMainBorderPanel.Visibility = Visibility.Visible;
                lctnTextBox.Focus();
            }
        }

        private ICommand _shareButtonClicked;
        public ICommand ShareButtonClicked
        {
            get
            {
                if (_shareButtonClicked == null)
                {
                    _shareButtonClicked = new RelayCommand(param => onShareButtonClicked());
                }
                return _shareButtonClicked;
            }
        }

        private void onShareButtonClicked()
        {
            EnableButtonsandLoader(false);
            rtbFeedEdit.SetStatusandTags();

            string StatusTextWithoutTags = (rtbFeedEdit.TagsJArray != null) ? rtbFeedEdit.StringWithoutTags : rtbFeedEdit.Text.Trim();
            NewStatusViewModel newStatusViewModel = new NewStatusViewModel();
            newStatusViewModel.PrivacyValue = 0;
            newStatusViewModel.StatusTextWithoutTags = StatusTextWithoutTags;
            newStatusViewModel.StatusTagsJArray = rtbFeedEdit.TagsJArray;
            newStatusViewModel.ValidityValue = -1;
            if (FeedModel != null && FeedModel.LinkModel != null)
            {
                newStatusViewModel.PreviewTitle = FeedModel.LinkModel.PreviewTitle;
                newStatusViewModel.PreviewUrl = FeedModel.LinkModel.PreviewUrl;
                newStatusViewModel.PreviewDesc = FeedModel.LinkModel.PreviewDesc;
                newStatusViewModel.PreviewImgUrl = FeedModel.LinkModel.PreviewImgUrl;
                newStatusViewModel.PreviewDomain = FeedModel.LinkModel.PreviewDomain;
                newStatusViewModel.PreviewLnkType = FeedModel.LinkModel.PreviewLinkType;
            }
            newStatusViewModel.SelectedLocationModel = SelectedLocationModel;
            newStatusViewModel.TaggedUtids = TaggedUtids;
            newStatusViewModel.SelectedDoingId = SelectedDoingId;
            newStatusViewModel.FriendUtIdOrCircleId = 0;
            newStatusViewModel.FeedWallType = 5;
            if (SingleMediaModel != null)
            {
                JArray singleMediaModelContentIdsArray = new JArray();
                singleMediaModelContentIdsArray.Add(SingleMediaModel.ContentId);
                new ThreadAddFeed().StartThread(newStatusViewModel, (SingleMediaModel.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? SettingsConstants.FEED_TYPE_AUDIO : SettingsConstants.FEED_TYPE_VIDEO, null, singleMediaModelContentIdsArray);
            }
            else if (FeedModel != null && FeedModel.LinkModel != null && !string.IsNullOrEmpty(FeedModel.LinkModel.PreviewUrl))
            {
                new ThreadAddFeed().StartThread(newStatusViewModel);
            }
            else if (FeedModel != null)
            {
                new ThreadShareStatus().StartThread(StatusTextWithoutTags, rtbFeedEdit.TagsJArray, this.FeedModel.NewsfeedId, (SelectedLocationModel == null) ? null : SelectedLocationModel.GetLocationDTOFromModel(), TaggedUtids, SelectedDoingId, FeedModel.Privacy);
            }
        }

        private ICommand _shareCancelButtonClicked;
        public ICommand ShareCancelButtonClicked
        {
            get
            {
                if (_shareCancelButtonClicked == null)
                {
                    _shareCancelButtonClicked = new RelayCommand(param => onShareCancelButtonClicked());
                }
                return _shareCancelButtonClicked;
            }
        }

        private void onShareCancelButtonClicked()
        {
            Hide();
            HideShareView();
        }

        #endregion

        public void LocationDetailsFromAddress(LocationModel model)
        {
            try
            {
                string latLong = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + model.LocationName;
                var request = WebRequest.Create(new Uri(latLong));
                var response = request.GetResponse();
                var xdoc = XDocument.Load(response.GetResponseStream());
                var result = xdoc.Element("GeocodeResponse").Element("result");
                if (result == null) return;
                var locationElement = result.Element("geometry").Element("location");
                if (locationElement == null) return;
                var _Latitude = locationElement.Element("lat");
                if (_Latitude.Value != null) model.Latitude = Convert.ToDouble(_Latitude.Value);
                var _Longitude = locationElement.Element("lng");
                if (_Longitude.Value != null) model.Longitude = Convert.ToDouble(_Longitude.Value);
                if (_Latitude.Value != null && _Longitude.Value != null)
                {
                    string mapURL = "http://maps.googleapis.com/maps/api/staticmap?"
                        + "center=" + _Latitude.Value + "," + _Longitude.Value
                        + "key=" + SocialMediaConstants.GOOGLE_MAP_API_KEY
                        + "&size=600x170"
                        + "&markers=size:mid%7Ccolor:red%7C" + _Latitude.Value + "," + _Longitude.Value
                        + "&zoom=15"
                        + "&maptype=roadmap"
                        + "&sensor=false";
                    model.ImageUrl = mapURL;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideShareView()
        {
            Visibility = Visibility.Hidden;
            removeEvents();
            ClearRichBoxText();
            lctnMainBorderPanel.Visibility = Visibility.Collapsed;
            SelectedLocationModel = null;
            lctnTextBox.Text = "";
            if (TaggedFriends != null) TaggedFriends.Clear();
            if (TaggedUtids != null) TaggedUtids.Clear();
            SelectedDoingModel = null;
            SelectedDoingId = 0;
            FeelingDoingBtnContainer.Visibility = Visibility.Collapsed;
            TagListHolder.Visibility = Visibility.Collapsed;
            if (LocationPopUp != null)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
                LocationPopUp.popupLocation.IsOpen = false;
            }
            if (ucNewStatusFeelingPopup != null)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ucNewStatusFeelingPopup = null;
        }

        public void ShowShareView(FeedModel _FeedModel)
        {
            this.FeedModel = _FeedModel;
            SingleMediaModel = null;
            this.IsSingleMedia = false;
            Visibility = Visibility.Visible;
            SetEvents();
        }

        public void ShowShareView(SingleMediaModel _SingleMediaModel)
        {
            this.SingleMediaModel = _SingleMediaModel;
            this.FeedModel = null;
            this.IsSingleMedia = true;
            Visibility = Visibility.Visible;
            SetEvents();
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        public void EnableButtonsandLoader(bool flag)
        {
            rtbFeedEdit.IsEnabled = flag;
            feelingBtn.IsEnabled = flag;
            shareTagFrnd.IsEnabled = flag;
            shareLocatin.IsEnabled = flag;
            shareBtn.IsEnabled = flag;
            cancelBtn.IsEnabled = flag;
            feelingDoingCross.IsEnabled = flag;
            TagListHolder.IsEnabled = flag;
            lctnMainBorderPanel.IsEnabled = flag;
            TextLocationCrossBtn.IsEnabled = flag;
            lctnTextBox.IsEnabled = flag;
            if (!flag && TaggedFriends != null && TaggedFriends.Count > 0 && TagListHolder.Visibility == Visibility.Visible)
            {
                TagListHolder.Visibility = Visibility.Collapsed;
            }

            if (flag)
            {
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            }
            else
            {
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_SMALL));
            }
        }

        public void ClearRichBoxText()
        {
            rtbFeedEdit.Document.Blocks.Clear();
        }

        private void FeelingDoingCrossButton_Click(object sender, RoutedEventArgs e)
        {
            SelectedDoingModel = null;
            SelectedDoingId = 0;
            FeelingDoingBtnContainer.Visibility = Visibility.Collapsed;
        }

        private void rtBoxShare_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (lctnMainBorderPanel.Visibility == Visibility.Visible)
            {
                lctnMainBorderPanel.Visibility = Visibility.Collapsed;
            }
            if (TagListHolder.Visibility == Visibility.Visible)
            {
                TagListHolder.Visibility = Visibility.Collapsed;
            }
        }

        private void lctnTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (lctnTextBox.Text.Trim().Length > 0)
                {
                    if (LocationPopUp != null)
                        LocationPopUp.LocationSuggestions.Clear();
                    try
                    {
                        var address = lctnTextBox.Text;
                        var requestUri = string.Format("https://maps.googleapis.com/maps/api/place/autocomplete/xml?input={0}&key={1}", Uri.EscapeDataString(address), Uri.EscapeDataString(SocialMediaConstants.GOOGLE_MAP_API_KEY));
                        var request = WebRequest.Create(requestUri);
                        var response = request.GetResponse();
                        var xdoc = XDocument.Load(response.GetResponseStream());
                        if (LocationPopUp == null)
                            locationPopupView();
                        else UCGuiRingID.Instance.MotherPanel.Children.Remove(LocationPopUp);
                        LocationPopUp.Show();
                        LocationPopUp.ShowLocationPopup(lctnTextBox, this);
                        foreach (XElement xe in xdoc.Descendants("prediction"))
                        {
                            string locationName = xe.Element("description").Value;
                            string locationId = xe.Element("place_id").Value;
                            System.Diagnostics.Debug.WriteLine(locationName + "  " + locationId);
                            if (!LocationPopUp.LocationSuggestions.Any(x => x.LocationId == locationId))
                            {
                                LocationPopUp.LocationSuggestions.Add(new LocationModel { LocationName = locationName, LocationId = locationId, Type = 0 });
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

                    }
                }
                else
                {
                    if (LocationPopUp != null)
                        LocationPopUp.SelectedLocationModel = null;
                    LocationPopUp.popupLocation.IsOpen = false;
                }

                if (LocationPopUp.LocationSuggestions.Count == 0)
                {
                    LocationPopUp.popupLocation.StaysOpen = false;
                    LocationPopUp.popupLocation.IsOpen = false;
                }
                else if (lctnTextBox.Text.Length == 0)
                {
                    LocationPopUp.popupLocation.StaysOpen = false;
                    LocationPopUp.popupLocation.IsOpen = false;
                }
                else
                {
                    LocationPopUp.popupLocation.StaysOpen = true;
                    LocationPopUp.popupLocation.IsOpen = true;
                }
                lctnTextBox.Focus();
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void rtbFeedEdit_RichTextChanged(string text)
        {
            if (text.Length > 0)
            {
                writeSmthngTxtBlock.Visibility = Visibility.Collapsed;
                if (text.EndsWith("@"))
                {
                    rtbFeedEdit.AlphaStarts = rtbFeedEdit.Text.Length;
                    if (UCAlphaTagPopUp.Instance == null)
                    {
                        UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
                    }
                    if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
                    {
                        Grid g = (Grid)UCAlphaTagPopUp.Instance.Parent;
                        g.Children.Remove(UCAlphaTagPopUp.Instance);
                    }
                    richGrid.Children.Add(UCAlphaTagPopUp.Instance);
                    UCAlphaTagPopUp.Instance.InitializePopUpLocation(rtbFeedEdit);
                }
                else if (rtbFeedEdit.AlphaStarts >= 0 && rtbFeedEdit.Text.Length > rtbFeedEdit.AlphaStarts)
                {
                    string searchStr = rtbFeedEdit.Text.Substring(rtbFeedEdit.AlphaStarts);
                    UCAlphaTagPopUp.Instance.ViewSearchFriends(searchStr);
                }
            }
            else
            {
                writeSmthngTxtBlock.Visibility = Visibility.Visible;
                rtbFeedEdit.AlphaStarts = -1;
            }
        }

        private void rtbFeedEdit_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && (UCAlphaTagPopUp.Instance == null || (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen == false)))
            {
                rtbFeedEdit.AppendText("\n");
                e.Handled = true;
            }
            if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
            {
                if (e.Key == Key.Down)
                {
                    e.Handled = true;
                    if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
                    {
                        UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
                    }
                    else
                    {
                        UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
                    }
                }
                if (e.Key == Key.Up)
                {
                    e.Handled = true;

                    if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
                    {
                        UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
                    }
                    else
                    {
                        UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
                    }
                }
                if (e.Key == Key.Enter || e.Key == Key.Tab)
                {
                    e.Handled = true;
                    UCAlphaTagPopUp.Instance.SelectFriendTag((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
                }
            }
        }

        void lctnTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (LocationPopUp != null)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
                LocationPopUp.popupLocation.IsOpen = false;
            }
        }

        private void removeEvents()
        {
            rtbFeedEdit.RichTextChanged -= rtbFeedEdit_RichTextChanged;
            rtbFeedEdit.PreviewKeyDown -= rtbFeedEdit_PreviewKeyDown;
            feelingDoingCross.Click -= FeelingDoingCrossButton_Click;
            lctnTextBox.LostFocus -= lctnTextBox_LostFocus;
            lctnTextBox.TextChanged -= lctnTextBox_TextChanged;
            Scroll.PreviewKeyUp -= Scroll_PreviewKeyDown;
        }

        private void SetEvents()
        {
            rtbFeedEdit.RichTextChanged += rtbFeedEdit_RichTextChanged;
            rtbFeedEdit.PreviewKeyDown += rtbFeedEdit_PreviewKeyDown;
            feelingDoingCross.Click += FeelingDoingCrossButton_Click;
            lctnTextBox.TextChanged += lctnTextBox_TextChanged;
            lctnTextBox.LostFocus += lctnTextBox_LostFocus;
            Scroll.PreviewKeyUp += Scroll_PreviewKeyDown;
        }

        #region "location Popup"

        private void locationPopupView()
        {
            if (LocationPopUp != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(LocationPopUp);
            LocationPopUp = new UCLocationPopUp(UCGuiRingID.Instance.MotherPanel);
            //LikeListView.ContentId = contentID;

            //LocationPopUp.Show();

            LocationPopUp.OnRemovedUserControl += () =>
            {
                LocationPopUp = null;
            };
        }
        #endregion
    }
}

﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.Feed;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCDiscoverSearchCountryView.xaml
    /// </summary>
    public partial class UCDiscoverSearchCountryView : PopUpBaseControl, INotifyPropertyChanged
    {
        public static UCDiscoverSearchCountryView Instance;
        public UCDiscoverSearchCountryView(Grid motherPanel)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, null, 0.0);//background Color Transparent Black
            SetParent(motherPanel);
        }

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private string _SelectedCountryName;
        public string SelectedCountryName
        {
            get
            {
                return _SelectedCountryName;
            }
            set
            {
                if (value == _SelectedCountryName)
                    return;
                _SelectedCountryName = value;
                OnPropertyChanged("SelectedCountryName");
            }
        }
        private ObservableCollection<CountryCodeModel> _CountryModelList;
        public ObservableCollection<CountryCodeModel> CountryModelList
        {
            get
            {
                return _CountryModelList;
            }
            set
            {
                _CountryModelList = value;
                this.OnPropertyChanged("CountryModelList");
            }
        }
        #endregion

        private void ShowDiscoverSearchCountryView(string CountryName)
        {
            //this.SelectedCountryName = CountryName;

            foreach (var item in CountryModelList)
            {
                if (item.CountryName == CountryName)
                    item.TempSelected = true;
                else
                    item.TempSelected = false;
            }
        }

        public void LoadMailMobileCountryCodeList(string CountryName)
        {
            CountryModelList = new ObservableCollection<CountryCodeModel>();
            CountryModelList.Add(new CountryCodeModel("All", ""));
            for (int i = 2; i < DefaultSettings.COUNTRY_MOBILE_CODE.Length / 2; i++)
            {
                string name = DefaultSettings.COUNTRY_MOBILE_CODE[i, 0];
                string code = DefaultSettings.COUNTRY_MOBILE_CODE[i, 1];
                CountryModelList.Add(new CountryCodeModel(name, code));
            }
            ShowDiscoverSearchCountryView(CountryName);
        }
        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }

        private void OnLoadedControl()
        {
            //LoadMailMobileCountryCodeList();
            this.Focusable = true;
            Keyboard.Focus(this);
        }
        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => OnCloseCommand());
                }
                return _CloseCommand;
            }
        }

        public void OnCloseCommand()
        {
            Hide();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }
        private void OnCommentSpaceClicked(object param)
        {
            OnLoadedControl();
        }

        private ICommand _PanelCommand;
        public ICommand PanelCommand
        {
            get
            {
                if (_PanelCommand == null)
                {
                    _PanelCommand = new RelayCommand((param) => OnPanelCommand());
                }
                return _PanelCommand;
            }
        }
        public void OnPanelCommand()
        {

        }

        ICommand _DoneBtnClicked;
        public ICommand DoneBtnClicked
        {
            get
            {
                if (_DoneBtnClicked == null)
                {
                    _DoneBtnClicked = new RelayCommand(param => OnDoneBtnClicked());
                }
                return _DoneBtnClicked;
            }
        }
        private void OnDoneBtnClicked()
        {
            //            ThradDiscoverOrFollowingChannelsList thrd = new ThradDiscoverOrFollowingChannelsList();
            //            thrd.CallBack += (success) =>
            //            {
            ////po
            //            };
            //            RingIDViewModel.Instance.MusicPagesDiscovered.Clear();
            //            thrd.StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_MUSICPAGE, ,SelectedCountryName);
        }
        #endregion

        private void counrtyBtn_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.DataContext is CountryCodeModel)
                {
                    CountryCodeModel model = (CountryCodeModel)btn.DataContext;
                    SelectedCountryName = model.CountryName;

                    foreach (var item in CountryModelList)
                    {
                        if (item.CountryCode == model.CountryCode)
                            item.TempSelected = true;
                        else
                            item.TempSelected = false;
                    }
                    OnCloseCommand();
                }
            }
        }

        private string searchText = string.Empty;
        public string SearchText
        {
            get { return searchText; }
            set
            {
                if (value == searchText) return;
                searchText = value;
                filterCountryList(searchText);
                OnPropertyChanged("SearchText");
            }
        }
        private void filterCountryList(string searchText1)
        {
            foreach (var item in CountryModelList)
            {
                if (item.CountryName.IndexOf(searchText1, StringComparison.OrdinalIgnoreCase) >= 0) { item.VisibilityInCountrySearch = Visibility.Visible; }
                else item.VisibilityInCountrySearch = Visibility.Collapsed;
            }
        }
    }
}

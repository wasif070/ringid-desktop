﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAboutRingID.xaml
    /// </summary>
    public partial class UCAboutRingID : PopUpBaseControl, INotifyPropertyChanged
    {
        public UCAboutRingID()
        {
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, "#FFd8e1e6", 1.0, null, false);
            this.DataContext = this;
        }
        private ICommand exit;
        public ICommand Exit
        {
            get
            {
                if (exit == null) exit = new RelayCommand(param => OnExit());
                return exit;
            }
        }

        public void OnExit()
        {
            this.Hide();
        }

        public void ShowThis()
        {
            Show();
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

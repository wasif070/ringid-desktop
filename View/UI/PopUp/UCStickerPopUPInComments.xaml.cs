﻿using log4net;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.Feed;
using View.Utility;
using View.ViewModel;
using System.Linq;
using Auth.Service.RingMarket;
using View.Constants;
using View.Utility.RingMarket;
using System.Windows.Media.Animation;
using System.Collections.Specialized;
using System.Threading;
using Models.Constants;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCStickerPopUPInComments.xaml
    /// </summary>
    public partial class UCStickerPopUPInComments : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCEmoticonPopup).Name);

        public const int TYPE_DEFAULT = 0;
        public const int TYPE_NEW_STATUS = 1;
        public const int TYPE_CALL_CHAT_PANEL = 2;
        public const int TYPE_CHAT_EDIT_PANEL = 3;

        private const int EMO_POPUP_HEIGHT = 344;
        private int _ShowUpArrowPopup = 1;//0 - Default, 1 - UP, 2 - Down   

        private string _EmoticonSelected = String.Empty;
        public bool _IsWindowExpanded = false;
        private bool _IsAdditionalWidthAdded = false;
        private Func<object, int> _OnClosing = null;

        private VirtualizingStackPanel _ImageItemContainer = null;
        private Grid _MotherPanel = null;
        private int CurrentLeftMargin = 0;
        private double SingleIconGridWidth = 45;

        private ICommand _StickerIconClickCommand;
        private ICommand _MoreStickerCommand;
        private ICommand _RightArrowCommand;
        private ICommand _EmoticonClickCommand;
        private ICommand _LeftArrowCommand;

        public UCStickerPopUPInComments()
        {
            InitializeComponent();
            this.DataContext = this;
            ImagesListBox.Loaded += ImagesListBox_Loaded;

            ((INotifyCollectionChanged)ImagesListBox.Items).CollectionChanged += ImagesListBox_CollectionChanged;
            popupEmoticon.Child.IsVisibleChanged += Child_IsVisibleChanged;
        }

        private void Child_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                Keyboard.Focus(popupEmoticon.Child);
                popupEmoticon.Closed += popupEmoticon_Closed;
                popupEmoticon.PreviewKeyDown += popupEmoticon_PreviewKeyDown;
            }
            else
            {
                popupEmoticon.Closed -= popupEmoticon_Closed;
                popupEmoticon.PreviewKeyDown -= popupEmoticon_PreviewKeyDown;
            }
        }

        #region Utility

        public void Show(UIElement target, int type, Grid motherPanel = null, Func<object, int> onClosing = null)
        {
            try
            {
                Type = type;
                this._MotherPanel = motherPanel;
                _EmoticonSelected = String.Empty;
                _OnClosing = onClosing;

                if (popupEmoticon.IsOpen)
                {
                    popupEmoticon.IsOpen = false;
                }
                else
                {

                    popupEmoticon.PlacementTarget = target;

                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;
                    if (buttonPosition < EMO_POPUP_HEIGHT && bottomSpace >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 1;
                    }
                    else if (buttonPosition >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 2;
                    }
                    else
                    {
                        ShowUpArrowPopup = 0;
                    }

                    if (type == UCEmoticonPopup.TYPE_NEW_STATUS)
                    {
                        if (ShowUpArrowPopup == 1)
                        {
                            popupEmoticon.HorizontalOffset = -284;
                            //popupEmoticon.VerticalOffset = -3;
                        }
                        else if (ShowUpArrowPopup == 2)
                        {
                            popupEmoticon.HorizontalOffset = -284;
                            //popupEmoticon.VerticalOffset = 5;
                        }
                        else
                        {
                            popupEmoticon.HorizontalOffset = -284;
                            popupEmoticon.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                        }
                    }
                    else if (type == UCEmoticonPopup.TYPE_CALL_CHAT_PANEL || type == UCEmoticonPopup.TYPE_CHAT_EDIT_PANEL)
                    {
                        if (ShowUpArrowPopup == 1)
                        {
                            popupEmoticon.HorizontalOffset = -297;
                            popupEmoticon.VerticalOffset = -10;
                        }
                        else if (ShowUpArrowPopup == 2)
                        {
                            popupEmoticon.HorizontalOffset = -297;
                            popupEmoticon.VerticalOffset = 0;
                        }
                        else
                        {
                            popupEmoticon.HorizontalOffset = -297;
                            popupEmoticon.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                        }
                    }
                    else
                    {
                        if (ShowUpArrowPopup == 1)
                        {
                            popupEmoticon.HorizontalOffset = -215;
                            popupEmoticon.VerticalOffset = 5;
                        }
                        else if (ShowUpArrowPopup == 2)
                        {
                            popupEmoticon.HorizontalOffset = -215;
                            popupEmoticon.VerticalOffset = -3;
                        }
                        else
                        {
                            popupEmoticon.HorizontalOffset = -215;
                            popupEmoticon.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                        }
                    }

                    if (EmoticonModelList.Count <= 0)
                    {
                        foreach (EmoticonDTO emoticonDTO in DefaultDictionaries.Instance.EMOTICON_DICTIONARY.Values)
                        {
                            EmoticonModelList.Add(new EmoticonModel(emoticonDTO));
                        }
                    }

                    popupEmoticon.IsOpen = true;
                    popupEmoticon.Loaded += (sender, args) =>
                    {
                        popupEmoticon.Child.Focusable = true;
                        popupEmoticon.Child.IsVisibleChanged += (o, e) =>
                        {
                            if (popupEmoticon.Child.IsVisible)
                            {
                                Keyboard.Focus(popupEmoticon.Child);
                            }
                            else
                            {
                                ClosePopUp();
                            }
                        };
                    };

                    this.LoadStickerIcons();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Show() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LoadStickerIcons()
        {
            MarketStickerCategoryDTO ctgDTO = new MarketStickerCategoryDTO();
            ctgDTO.sCtId = -1;
            ctgDTO.sClId = -1;
            ctgDTO.sctName = "Emoticon";
            MarketStickerCategoryModel ctgModel = new MarketStickerCategoryModel(ctgDTO);
            if (!MarketStickerCategoryList.Any(P => P.StickerCategoryID == ctgModel.StickerCategoryID))
            {
                MarketStickerCategoryList.InvokeAdd(ctgModel);
                ctgModel.IsSelectedInComment = true;
                SelectedCategoryModel = ctgModel;
            }

            MarketStickerCategoryModel recentModel = RingIDViewModel.Instance.RecentStickerCategoryList.Where(P => P.StickerCategoryID == 0).FirstOrDefault();
            if (recentModel != null)
            {
                if (!MarketStickerCategoryList.Any(P => P.StickerCategoryID == recentModel.StickerCategoryID))
                {
                    recentModel.IsSelectedInComment = false;
                    MarketStickerCategoryList.InvokeAdd(recentModel);
                }
            }

            List<MarketStickerCategoryModel> ctgList;
            ctgList = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.Downloaded).ToList();
            if (ctgList != null && ctgList.Count > 0)
            {
                foreach (MarketStickerCategoryModel model in ctgList)
                {
                    if (!MarketStickerCategoryList.Any(P => P.StickerCategoryID == model.StickerCategoryID))
                    {
                        model.IsSelectedInComment = false;
                        MarketStickerCategoryList.InvokeAdd(model);
                    }
                    if (MarketStickerCategoryList.Count >= 10) // Next Stickers would be loaded in Next Button actions
                    {
                        break;
                    }
                }
            }
        }

        public void ClosePopUp()
        {
            popupEmoticon.IsOpen = false;
            ImagesListBox.Loaded -= ImagesListBox_Loaded;
            AnimationOnArrowClicked(0, 0);
            CurrentLeftMargin = 0;
            IsStickerSelected = false;
            _IsAdditionalWidthAdded = false;
            ImagesListBox_Loaded(null, null);
            MarketStickerCategoryList.Clear();
            GC.SuppressFinalize(MarketStickerCategoryList);
            GC.Collect();
        }
        private void OnEmoticonClick(object param)
        {
            _EmoticonSelected = param.ToString();
            ClosePopUp();
            if (_OnClosing != null)
            {
                _OnClosing(_EmoticonSelected);
                _OnClosing = null;
            }
        }

        private void OnMoreStickerClickedCommand()
        {
            if (MainSwitcher.PopupController.stickerPopUPInComments != null && MainSwitcher.PopupController.stickerPopUPInComments.popupEmoticon.IsOpen)
            {
                MainSwitcher.PopupController.stickerPopUPInComments.popupEmoticon.IsOpen = false;
            }
            UCMoreStickersPopUpInComments moreStickersPopUpInComments = new UCMoreStickersPopUpInComments(_MotherPanel ?? UCGuiRingID.Instance.MotherPanel);
            moreStickersPopUpInComments.Show();
            moreStickersPopUpInComments.LoadData();
        }

        private void OnPrevious(object param)
        {
            try
            {
                int target = CurrentLeftMargin + (int)pnlImageContainerWrapper.ActualWidth - (int)(2 * SingleIconGridWidth);
                if (CurrentLeftMargin == -180)
                {
                    target = 0;
                }
                else if (_IsAdditionalWidthAdded)
                {
                    target -= 45;
                    _IsAdditionalWidthAdded = false;
                }
                AnimationOnArrowClicked(target, 0.35);
                CurrentLeftMargin = target;
                ImagesListBox_Loaded(null, null);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNext(object param)
        {
            try
            {
                this.LoadNextFiveCategories();
                int target = CurrentLeftMargin - (int)(4 * SingleIconGridWidth);
                if (target <= -(((float)ImagesListBox.Items.Count / 5) - 1) * pnlImageContainerWrapper.ActualWidth)// that means at the end of list
                {
                    target += 45; // SingleIconGridWidth = 45, additional 45 added to view at least one item in viewport;
                    _IsAdditionalWidthAdded = true;
                }
                AnimationOnArrowClicked(target, 0.35);
                CurrentLeftMargin = target;
                ImagesListBox_Loaded(null, null);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LoadNextFiveCategories()
        {
            int currentIndex = MarketStickerCategoryList.Count;
            List<MarketStickerCategoryModel> ctgList;
            ctgList = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.Downloaded).ToList();
            foreach (MarketStickerCategoryModel ctgModel in ctgList)
            {
                if (!MarketStickerCategoryList.Any(P => P.StickerCategoryID == ctgModel.StickerCategoryID))
                {
                    ctgModel.IsSelectedInComment = false;
                    MarketStickerCategoryList.InvokeAdd(ctgModel);
                }
                if (MarketStickerCategoryList.Count >= currentIndex + 5)
                {
                    break;
                }
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(CurrentLeftMargin, 0, 0, 0);
            MarginAnimation.To = new Thickness(target, 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
            storyboard.Begin(_ImageItemContainer);
        }

        private void ImagesListBox_Loaded(object sender, RoutedEventArgs e)
        {
            IsLeftArrowVisible = CurrentLeftMargin < 0 ? Visibility.Visible : Visibility.Collapsed;
            // IsRightArrowVisible = CurrentLeftMargin > -(((float)ImagesListBox.Items.Count / 5) - 1) * pnlImageContainerWrapper.ActualWidth ? Visibility.Visible : Visibility.Collapsed;
            double offset = -((ImagesListBox.Items.Count - 5) * SingleIconGridWidth);
            IsRightArrowVisible = CurrentLeftMargin > offset ? Visibility.Visible : Visibility.Collapsed;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region EventHandler

        private void popupEmoticon_Closed(object sender, EventArgs e)
        {
            Type = UCEmoticonPopup.TYPE_DEFAULT;
            ClosePopUp();
            if (_OnClosing != null)
            {
                _OnClosing(_EmoticonSelected);
            }
        }

        private void popupEmoticon_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                ClosePopUp();
            }
        }

        private void HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as VirtualizingStackPanel;
        }

        private void OnStickerIconClickCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)param;
            if (ctgModel.StickerCategoryID >= 0)
            {
                if (SelectedCategoryModel != null)
                {
                    if (SelectedCategoryModel.StickerCategoryID == ctgModel.StickerCategoryID)
                    {
                        return;
                    }
                    SelectedCategoryModel.IsSelectedInComment = false;
                }

                IsStickerSelected = true;
                ctgModel.IsSelectedInComment = true;
                SelectedCategoryModel = ctgModel;
                scrollViewer.ScrollToVerticalOffset(0);

                if (ctgModel.Downloaded == true)
                {
                    MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgModel.StickerCategoryID).FirstOrDefault();
                    if (ctgDTO != null)
                    {
                        if (ctgModel.ImageList.Count < ctgDTO.ImagesList.Count)
                        {
                            ctgDTO.ImagesList.ForEach(i =>
                            {
                                MarkertStickerImagesModel imgModel = ctgModel.ImageList.Where(P => P.ImageID == i.imId).FirstOrDefault();
                                if (imgModel == null)
                                {
                                    imgModel = new MarkertStickerImagesModel(i);
                                    SelectedCategoryModel.ImageList.InvokeAdd(imgModel);
                                }
                            });
                        }
                    }
                    if (SelectedCategoryModel.StickerCategoryID > 0)
                    {
                        new RingMarketStickerService(SelectedCategoryModel.StickerCategoryID, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), 0);
                    }
                }
            }
            else
            {
                if (SelectedCategoryModel != null)
                {
                    if (SelectedCategoryModel.StickerCategoryID == ctgModel.StickerCategoryID)
                    {
                        return;
                    }

                    SelectedCategoryModel.IsSelectedInComment = false;
                    IsStickerSelected = false;
                    ctgModel.IsSelectedInComment = true;
                    SelectedCategoryModel = ctgModel;
                    scrollViewer.ScrollToVerticalOffset(0);
                }
            }
        }

        private void StickerImage_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.DataContext is MarkertStickerImagesModel)
            {
                //MarkertStickerImagesModel sticker = (MarkertStickerImagesModel)btn.DataContext;
                ClosePopUp();
                if (_OnClosing != null)
                {
                    _OnClosing(btn.DataContext);
                    _OnClosing = null;
                }
            }
        }

        private void ImagesListBox_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                ImagesListBox_Loaded(null, null);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                ImagesListBox_Loaded(null, null);
            }
        }

        #endregion

        #region Property

        private MarketStickerCategoryModel _SelectedCategoryModel;
        public MarketStickerCategoryModel SelectedCategoryModel
        {
            get { return _SelectedCategoryModel; }
            set
            {
                _SelectedCategoryModel = value;
                OnPropertyChanged("SelectedCategoryModel");
            }
        }

        public int _Type = UCEmoticonPopup.TYPE_DEFAULT;
        public int Type
        {
            get { return _Type; }
            set
            {
                _Type = value;
                this.OnPropertyChanged("Type");
            }
        }

        private bool _IsStickerSelected = false;
        public bool IsStickerSelected
        {
            get { return _IsStickerSelected; }
            set
            {
                _IsStickerSelected = value;
                this.OnPropertyChanged("IsStickerSelected");
            }
        }

        private ObservableCollection<EmoticonModel> _EmoticonModelList = new ObservableCollection<EmoticonModel>();
        public ObservableCollection<EmoticonModel> EmoticonModelList
        {
            get { return _EmoticonModelList; }
            set
            {
                _EmoticonModelList = value;
                this.OnPropertyChanged("EmoticonModelList");
            }
        }

        private ObservableCollection<MarketStickerCategoryModel> _MarketStickerCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> MarketStickerCategoryList
        {
            get { return _MarketStickerCategoryList; }
            set
            {
                _MarketStickerCategoryList = value;
                this.OnPropertyChanged("MarketStickerCategoryList");
            }
        }

        public int ShowUpArrowPopup
        {
            get { return _ShowUpArrowPopup; }
            set
            {
                if (_ShowUpArrowPopup == value)
                {
                    return;
                }
                _ShowUpArrowPopup = value;
                this.OnPropertyChanged("ShowUpArrowPopup");
            }
        }

        private Visibility _IsLeftArrowVisible = Visibility.Visible;
        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        private Visibility _IsRightArrowVisible = Visibility.Visible;
        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }

        #endregion

        #region Command

        public ICommand StickerIconClickCommand
        {
            get
            {
                if (_StickerIconClickCommand == null)
                {
                    _StickerIconClickCommand = new RelayCommand((param) => OnStickerIconClickCommand(param));
                }
                return _StickerIconClickCommand;
            }
        }

        public ICommand MoreStickerCommand
        {
            get
            {
                if (_MoreStickerCommand == null) _MoreStickerCommand = new RelayCommand(param => OnMoreStickerClickedCommand());
                return _MoreStickerCommand;
            }
        }

        public ICommand EmoticonClickCommand
        {
            get
            {
                if (_EmoticonClickCommand == null)
                {
                    _EmoticonClickCommand = new RelayCommand((param) => OnEmoticonClick(param));
                }
                return _EmoticonClickCommand;
            }
        }

        public ICommand LeftArrowCommand
        {
            get
            {
                if (_LeftArrowCommand == null)
                {
                    _LeftArrowCommand = new RelayCommand(param => OnPrevious(param));
                }
                return _LeftArrowCommand;
            }
        }

        public ICommand RightArrowCommand
        {
            get
            {
                if (_RightArrowCommand == null)
                {
                    _RightArrowCommand = new RelayCommand(param => OnNext(param));

                }
                return _RightArrowCommand;
            }
        }

        #endregion
    }
}

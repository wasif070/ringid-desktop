﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Chat.Service;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCChatRoomMembersPopUp.xaml
    /// </summary>
    public partial class UCChatRoomMembersPopUp : UserControl, INotifyPropertyChanged
    {
        private ScrollViewer _Scroll = null;
        public delegate void OnCompleted();
        public event OnCompleted CompletedHandler;

        public UCChatRoomMembersPopUp(RoomModel roomModel)
        {
            InitializeComponent();
            this.DataContext = this;
            SelectedModel = roomModel;
            this.Loaded += UCChatRoomMembersPopUp_Loaded;
        }

        private RoomModel _SelectedModel;
        public RoomModel SelectedModel
        {
            get { return _SelectedModel; }
            set
            {
                _SelectedModel = value;
                OnPropertyChanged("SelectedModel");
            }
        }

        private ImageSource _LOADER_SMALL;
        public ImageSource LOADER_SMALL
        {
            get { return _LOADER_SMALL; }
            set
            {
                _LOADER_SMALL = value;
                OnPropertyChanged("LOADER_SMALL");
            }
        }

        private bool _IS_LOADING;
        public bool IS_LOADING
        {
            get { return _IS_LOADING; }
            set
            {
                _IS_LOADING = value;
                OnPropertyChanged("IS_LOADING");
            }
        }

        public void InitData()
        {
            HideMoreLoading();
            //if (SelectedModel.ChatMemberNotFound == false)
            //{
            ShowInitLoader();


            string prevPagingState = SelectedModel.PagingState ?? String.Empty;
            ChatService.GetRoomMemberList(SelectedModel.RoomID, SelectedModel.PagingState, 15, (args) =>
            {
                for (int i = 0; prevPagingState.Equals(SelectedModel.PagingState ?? String.Empty) && i < 5000; i += 100)
                {
                    Thread.Sleep(100);
                }
                HideInitLoader();
            });
            //}

            this.Focusable = true;
            Keyboard.Focus(this);
            this.KeyDown += UCCreateGroupPopUp_KeyDown;
        }

        public void ShowInitLoader()
        {
            _IS_LOADING = true;
            LOADER_SMALL = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
            GC.SuppressFinalize(LOADER_SMALL);
        }

        public void HideInitLoader()
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                _IS_LOADING = false;
                LOADER_SMALL = null;
                ImgInitLoader.Visibility = Visibility.Collapsed;
            }, DispatcherPriority.Send);
        }

        public void ShowMoreLoading()
        {
            try
            {
                showMorePanel.Visibility = Visibility.Visible;
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            }
            catch (System.Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCChatRoomMembersPopUp).Name).Error(ex.StackTrace + ex.Message);
            }
        }

        public void HideMoreLoading()
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                showMorePanel.Visibility = Visibility.Collapsed;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            }, DispatcherPriority.Send);
        }

        private void OnClickBlackSpace(object param)
        {
            Cancel_Click(null, null);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void Cancel_Click(object sender, RoutedEventArgs e)
        {
            HideMoreLoading();
            HideInitLoader();
            Visibility = Visibility.Hidden;
            itemsControl.ItemsSource = null;
            this.DataContext = null;
            this.KeyDown -= UCCreateGroupPopUp_KeyDown;

            if (CompletedHandler != null)
            {
                CompletedHandler();
            }
        }

        private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            _Scroll = (ScrollViewer)sender;
            _Scroll.ScrollChanged += _Scroll_ScrollChanged;
        }


        private void _Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            if (_IS_LOADING == false && SelectedModel.ChatMemberNotFound == false && e.VerticalChange > 0 && e.VerticalOffset >= (_Scroll.ScrollableHeight - 106.0))
            {
                IS_LOADING = true;
                ShowMoreLoading();
                if (!SelectedModel.ChatMemberNotFound)
                {
                    string prevPagingState = SelectedModel.PagingState ?? String.Empty;
                    ChatService.GetRoomMemberList(SelectedModel.RoomID, SelectedModel.PagingState, 10, (args) =>
                    {
                        for (int i = 0; prevPagingState.Equals(SelectedModel.PagingState ?? String.Empty) && i < 5000; i += 100)
                        {
                            Thread.Sleep(100);
                        }

                        HideMoreLoading();
                        IS_LOADING = false;
                    });
                }
                else
                {
                    HideMoreLoading();
                    IS_LOADING = false;
                }
            }
        }

        private void UCChatRoomMembersPopUp_Loaded(object sender, RoutedEventArgs e)
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            this.KeyDown += UCCreateGroupPopUp_KeyDown;
        }

        private void UCCreateGroupPopUp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Cancel_Click(null, null);
            }
        }

        private ICommand _blackSpaceClick;
        public ICommand BlackSpaceClick
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }

        private ICommand _EmptyCommand;
        public ICommand EmptyCommand
        {
            get
            {
                if (_EmptyCommand == null)
                {
                    _EmptyCommand = new RelayCommand(param => OnEmptyCommandClicked(param));
                }
                return _EmptyCommand;
            }
        }

        private void OnEmptyCommandClicked(object param)
        {

        }
    }
}

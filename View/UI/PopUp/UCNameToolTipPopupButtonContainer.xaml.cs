﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCNameToolTipPopupButtonContainer.xaml
    /// </summary>
    public partial class UCNameToolTipPopupButtonContainer : UserControl, INotifyPropertyChanged
    {
        public static UCNameToolTipPopupButtonContainer Instance = null;
        public UCNameToolTipPopupButtonContainer()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region "Property"

        private int _type;//0=user, 1=frndwall, 2=newsportal, 3=page, 4=musicpage, 5=celebrity, 6=circle
        public int Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                OnPropertyChanged("Type");
            }
        }

        private int _FriendShipStatus;//0=unknown,1=frnd, 2=incoming, 3=outgoing, 4=musicpage
        public int FriendShipStatus
        {
            get
            {
                return _FriendShipStatus;
            }
            set
            {
                _FriendShipStatus = value;
                OnPropertyChanged("FriendShipStatus");
            }
        }

        private long _User_Table_ID;//0=unknown,1=frnd, 2=incoming, 3=outgoing, 4=musicpage
        public long User_Table_ID
        {
            get
            {
                return _User_Table_ID;
            }
            set
            {
                _User_Table_ID = value;
                OnPropertyChanged("User_Table_ID");
            }
        }
        private bool _IsSubscribed;
        public bool IsSubscribed
        {
            get { return _IsSubscribed; }
            set
            {
                if (value == _IsSubscribed)
                    return;
                _IsSubscribed = value;
                this.OnPropertyChanged("IsSubscribed");
            }
        }
        #endregion

        #region "ICommand"
        private ICommand _AcceptCommand;
        public ICommand AcceptCommand
        {
            get
            {
                if (_AcceptCommand == null)
                {
                    _AcceptCommand = new RelayCommand(param => OnAcceptButtonClicked(param));
                }
                return _AcceptCommand;
            }
        }
        private void OnAcceptButtonClicked(object parameter)
        {
            try
            {
                //Button btn = (Button)parameter;

                UserBasicInfoModel model = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable((long)parameter);

                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(model.ShortInfoModel.UserTableID);

                int accessType = -1;
                if (!HelperMethods.HasFriendChatPermission(model, blockModel, out accessType))
                {
                    if (HelperMethods.ShowBlockWarning(model, accessType))
                    {
                        return;
                    }
                }
                HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, model, blockModel, (status) =>
                {
                    if (status == false) return 0;

                    if (MainSwitcher.ThreadManager().thradAcceptFriendRequest == null)
                    {
                        MainSwitcher.ThreadManager().thradAcceptFriendRequest = new ThradAcceptFriendRequest();
                    }
                    MainSwitcher.ThreadManager().thradAcceptFriendRequest.StartThread(model);

                    return 1;
                }, true);
            }
            finally { }
        }

        private ICommand _RejectCommand;
        public ICommand RejectCommand
        {
            get
            {
                if (_RejectCommand == null)
                {
                    _RejectCommand = new RelayCommand(param => OnRejectButtonClicked(param));
                }
                return _RejectCommand;
            }
        }
        private void OnRejectButtonClicked(object parameter)
        {
            UserBasicInfoModel model = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable((long)parameter); ;

            //MessageBoxResult result;
            //if (FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            //{
            //    result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.REMOVE_FROM_FRIEND, model.ShortInfoModel.FullName));
            //}
            //else
            //{
            //    string msg = FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING ? NotificationMessages.CANCEL_OUTGOING_REQUEST : NotificationMessages.CANCEL_INCOMING_REQUEST;
            //    result = CustomMessageBox.ShowQuestion(msg);
            //}
            // MessageBoxResult result;
            string dialog = string.Empty;
            if (FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                dialog = string.Format(NotificationMessages.REMOVE_FROM_FRIEND, model.ShortInfoModel.FullName);
            else
                dialog = FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING ? NotificationMessages.CANCEL_OUTGOING_REQUEST : NotificationMessages.CANCEL_INCOMING_REQUEST;
            bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"), dialog);
            if (isTrue)
            {
                (new DeleteFriend()).StartProcess(model);
            }
        }

        private ICommand _AddFriendButtonCommand;
        public ICommand AddFriendButtonCommand
        {
            get
            {
                if (_AddFriendButtonCommand == null)
                {
                    _AddFriendButtonCommand = new RelayCommand(param => OnAddFriendButtonClicked(param));
                }
                return _AddFriendButtonCommand;
            }
        }
        public void OnAddFriendButtonClicked(object parameter)
        {
            UserBasicInfoModel model = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable((long)parameter);

            BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(model.ShortInfoModel.UserTableID);

            int accessType = -1;
            if (!HelperMethods.HasFriendChatPermission(model, blockModel, out accessType))
            {
                if (HelperMethods.ShowBlockWarning(model, accessType))
                {
                    return;
                }
            }
            HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, model, blockModel, (status) =>
            {
                if (status == false) return 0;

                //SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                new ThrdAddFriend().StartProcess(model);
                return 1;
            }, true);
        }

        private ICommand _OpenPopupCommand;
        public ICommand OpenPopupCommand
        {
            get
            {
                if (_OpenPopupCommand == null)
                    _OpenPopupCommand = new RelayCommand(param => OpenPopupExecute(param));
                return _OpenPopupCommand;
            }
            set
            {
                _OpenPopupCommand = value;
            }
        }

        public void OpenPopupExecute(object parameter)
        {
            UserBasicInfoModel model = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable((long)parameter);
            if (model.NumberOfMutualFriends > 0)
            {
                //ucMutual.MutualList.Clear();
                //ucMutual.Show(control, model);
                //SendDataToServer.SendMutualFreindRequest(model.ShortInfoModel.UserTableID);
            }
        }

        private ICommand _FollowUnFollowCommand;
        public ICommand FollowUnFollowCommand
        {
            get
            {
                if (_FollowUnFollowCommand == null)
                    _FollowUnFollowCommand = new RelayCommand(param => OnFollowUnFollowClick(param));
                return _FollowUnFollowCommand;
            }
            set
            {
                _FollowUnFollowCommand = value;
            }
        }

        public void OnFollowUnFollowClick(object parameter)
        {
            if (IsSubscribed)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "this News Portal"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                if (isTrue)
                    new ThradSubscribeOrUnsubscribe(Type, User_Table_ID, 1, null, null, 0).StartThread();
            }
            else
            {
                if (UCFollowingUnfollowingView.Instance == null)
                {
                    //UCFollowingUnfollowingView.Instance = new UCFollowingUnfollowingView(UCGuiRingID.Instance.MotherPanel);
                    //UCFollowingUnfollowingView.Instance.Show();
                    //UCFollowingUnfollowingView.Instance.ShowFollowUnFollow(portalModel, true);
                    //UCFollowingUnfollowingView.Instance.OnRemovedUserControl += () =>
                    //{
                    //    UCFollowingUnfollowingView.Instance = null;
                    //};
                }
            }
        }
        #endregion
    }
}

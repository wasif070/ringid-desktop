﻿using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.Feed;
using View.Utility;
using View.Utility.Auth;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMediaContentAlbumPopUp.xaml
    /// </summary>
    public partial class UCMediaContentAlbumPopUp : PopUpBaseControl, INotifyPropertyChanged
    {
        public static UCMediaContentAlbumPopUp Instance;
        public UCMediaContentAlbumPopUp(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            SetParent(motherGrid);
        }
        #region "Property"
        private int _MediaTypePopUp;
        public int MediaTypePopUp
        {
            get
            {
                return _MediaTypePopUp;
            }
            set
            {
                _MediaTypePopUp = value;
                this.OnPropertyChanged("MediaTypePopUp");
            }
        }
        private ObservableCollection<MediaContentModel> _MyMediaList = new ObservableCollection<MediaContentModel>();
        public ObservableCollection<MediaContentModel> MyMediaList
        {
            get
            {
                return _MyMediaList;
            }
            set
            {
                _MyMediaList = value;
                this.OnPropertyChanged("MyMediaList");
            }
        }
        #endregion

        private NewStatusViewModel newStatusViewModel;
        public void ShowMediaPopup(UIElement target, int myMediaType, NewStatusViewModel newStatusViewModel, bool MakeOpen = true)
        {
            this.MediaTypePopUp = myMediaType;
            MyMediaList = myMediaType == 1 ?  RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
            this.newStatusViewModel = newStatusViewModel;
            popupMediaContentAlbum.PlacementTarget = target;
            popupMediaContentAlbum.IsOpen = MakeOpen;
            AlbumMainBorder.Width = 630;
        }

        #region "OnPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupMediaContentAlbum.IsOpen = false;
        }

        private ICommand _mediaAlbumsListBoxScrollChangedEventCommand;
        public ICommand MediaAlbumsListBoxScrollChangedEventCommand
        {
            get
            {
                _mediaAlbumsListBoxScrollChangedEventCommand = _mediaAlbumsListBoxScrollChangedEventCommand ?? new RelayCommand(param => onMediaAlbumsListBoxScrollChangedEvent(param));
                return _mediaAlbumsListBoxScrollChangedEventCommand;
            }
        }

        private void onMediaAlbumsListBoxScrollChangedEvent(object param)
        {
            int albumCount = MediaTypePopUp == SettingsConstants.MEDIA_TYPE_AUDIO ? RingIDViewModel.Instance.MyAudioAlbums.Count : RingIDViewModel.Instance.MyVideoAlbums.Count;
            int storedCount = MediaTypePopUp == SettingsConstants.MEDIA_TYPE_AUDIO ? DefaultSettings.MY_AUDIO_ALBUMS_COUNT : DefaultSettings.MY_VIDEO_ALBUMS_COUNT;
            if (albumCount < storedCount)
            {
                Task.Factory.StartNew(() =>
                {
                    SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, MediaTypePopUp, MediaTypePopUp == SettingsConstants.MEDIA_TYPE_AUDIO ? RingIDViewModel.Instance.AudioAlbumNpUUId : RingIDViewModel.Instance.VideoAlbumNpUUId);
                });
            }
        }

        private ICommand _popupMediaContentAlbumClosed;
        public ICommand popupMediaContentAlbumClosed
        {
            get
            {
                if (_popupMediaContentAlbumClosed == null) _popupMediaContentAlbumClosed = new RelayCommand(param => OnpopupMediaContentAlbumClosed());
                return _popupMediaContentAlbumClosed;
            }
        }
        private void OnpopupMediaContentAlbumClosed()
        {
            Hide();
        }

        private ICommand _AlbumMouseSelected;
        public ICommand AlbumMouseSelected
        {
            get
            {
                if (_AlbumMouseSelected == null) _AlbumMouseSelected = new RelayCommand(param => OnAlbumMouseSelected(param));
                return _AlbumMouseSelected;
            }
        }
        private void OnAlbumMouseSelected(object sender)
        {
            if(sender is Border)
            {
                Border bd = (Border)sender;
                MediaContentModel model = (MediaContentModel)bd.DataContext;
                popupMediaContentAlbum.IsOpen = false;

                if (model.MediaType == 1)
                {
                    newStatusViewModel.AudioAlbumTitleTextBoxText = model.AlbumName;
                    newStatusViewModel.AddtoAudioAlbumButtonVisibility = Visibility.Collapsed;
                    newStatusViewModel.SelectedAudioAlbumCrossButtonVisibility = Visibility.Visible;
                    newStatusViewModel.IsAudioAlbumTitleTextBoxEnabled = false;
                    newStatusViewModel.IsAudioAlbumSelectedFromExisting = true;
                }
                else if (model.MediaType == 2)
                {
                    newStatusViewModel.VideoAlbumTitleTextBoxText = model.AlbumName;
                    newStatusViewModel.AddtoVideoAlbumButtonVisibility = Visibility.Collapsed;
                    newStatusViewModel.SelectedVideoAlbumCrossButtonVisibility = Visibility.Visible;
                    newStatusViewModel.IsVideoAlbumTitleTextBoxEnabled = false;
                    newStatusViewModel.IsVideoAlbumSelectedFromExisting = true;
                }
                newStatusViewModel.PrivacyValue = model.MediaPrivacy;
                newStatusViewModel.IsPrivacyButtonEnabled = false;
            }
        }
        #endregion
    }
}

<<<<<<< HEAD
﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.Channel;
using View.Utility;
using View.Utility.Channel;
using View.Utility.WPFMessageBox;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAddToMyChannelView.xaml
    /// </summary>
    public partial class UCAddToMyChannelView : PopUpBaseControl, INotifyPropertyChanged
    {
        private static ILog log = LogManager.GetLogger(typeof(UCAddToMyChannelView).Name);
        public static UCAddToMyChannelView Instance;
        public event PropertyChangedEventHandler PropertyChanged;
        private static readonly object _PROCESS_SYNC_LOCK = new object();

        private ICommand loadedControl;
        private ICommand _blackSpaceClick;
        private ICommand _commentSpaceClicked;
        private ICommand _CraeteAlbumDockCommand;
        private ICommand _AddToChannelCommand;
        private bool _IsProcessing = false;
        private bool _IS_LOADING = false;

        private SingleMediaModel _SingleMediaModel;
        private ObservableCollection<ChannelModel> _ChannelList = new ObservableCollection<ChannelModel>();

        public UCAddToMyChannelView(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 0.0);//background Color Transparent Black
            SetParent(motherGrid);
        }

        #region "Utility Methods"

        public void ShowChannelView(SingleMediaModel singleMediaModel = null)
        {
            this.DataContext = this;
            this._SingleMediaModel = singleMediaModel;
            this.itcChannelList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelList") });
            scrlViewr.ScrollChanged += scrlViewr_ScrollChanged;
            if (_SingleMediaModel != null)
            {
                List<ChannelModel> chnlList = ChannelViewModel.Instance.MyChannelList.Where(P => P.ChannelType == _SingleMediaModel.MediaType).ToList();
                foreach (ChannelModel model in chnlList)
                {
                    ChannelList.InvokeAdd(model);
                }
            }

            if (ChannelList.Count < 20)
            {
                new ThrdGetOwnChannelList(0, ChannelList.Count, ChannelList.Count + 10).Start();
            }
        }

        public void HidePopup()
        {
            this.DataContext = null;
            ChannelList.Clear();
            this.itcChannelList.ClearValue(ItemsControl.ItemsSourceProperty);
            Hide();
        }

        private void OnLoadedControl()
        {
            this.Focusable = true;
            this.Focus();
            //this.SearchAlbumTextBox.Focusable = true;
            //this.SearchAlbumTextBox.Focus();
        }

        private void OnClickBlackSpace(object param)
        {
            HidePopup();
        }

        private void OnAddToChannelCommand(object param)
        {
            if (string.IsNullOrWhiteSpace(tbChannelTitle.Text))
            {
                UIHelperMethods.ShowWarning("Please give a title", "Media title");
                return;
            }
            if (param is ChannelModel && _SingleMediaModel != null)
            {
                if (_SingleMediaModel.Duration <= 0)
                {
                    UIHelperMethods.ShowWarning("Media duration must greater than zero", "Media duration");
                    return;
                }
                ChannelModel model = (ChannelModel)param;
                List<ChannelMediaDTO> mediaList = new List<ChannelMediaDTO>();
                ChannelMediaDTO channelMediaDTO = ChannelHelpers.GetChannelMediaDtoFromChannelModel(model);
                channelMediaDTO.Title = tbChannelTitle.Text;
                channelMediaDTO.Artist = _SingleMediaModel.Artist;
                channelMediaDTO.MediaID = _SingleMediaModel.ContentId;
                channelMediaDTO.MediaType = _SingleMediaModel.MediaType;
                channelMediaDTO.MediaUrl = _SingleMediaModel.StreamUrl;
                channelMediaDTO.ThumbImageUrl = _SingleMediaModel.ThumbUrl;
                channelMediaDTO.ThumbImageWidth = _SingleMediaModel.ThumbImageWidth;
                channelMediaDTO.ThumbImageHeight = _SingleMediaModel.ThumbImageHeight;
                channelMediaDTO.Duration = 1000 * _SingleMediaModel.Duration;// in ms
                channelMediaDTO.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                mediaList.Add(channelMediaDTO);
                new ThrdAddMediaToChannel(model.ChannelID, _SingleMediaModel.MediaType, mediaList, (status, message) =>
                {
                    if (status)
                    {
                        UIHelperMethods.ShowMessageWithTimerFromThread(gridContainer, "Media added Successfully!", 1000);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(message)) UIHelperMethods.ShowWarning(message, "Add media");
                        else UIHelperMethods.ShowWarning("Failed to add media", "Add media");
                    }
                    return 0;
                }).Start();
            }
        }

        public void AddIntoMyChannelListPopUp(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelList;
                lock (_PROCESS_SYNC_LOCK)
                {

                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();

                    ChannelModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.CreationTime > firstModel.CreationTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.CreationTime < lastModel.CreationTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotCount = viewerModelList.ElementAt(pivot).CreationTime;
                                    if (newModel.CreationTime > pivotCount)
                                    {
                                        max = pivot;
                                    }
                                    else
                                    {
                                        min = pivot + 1;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.CreationTime > firstModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.CreationTime > newModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.CreationTime > viewerModelList.ElementAt(pivot).CreationTime)
                                {
                                    max = pivot;
                                }
                                else
                                {
                                    min = pivot + 1;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoMyChannelListPopUp() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }


        private void OnCommentSpaceClicked(object param)
        {

        }

        private void OnCraeteChannnelDockCommand()
        {
            //HidePopup();
            //MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyChannel, true);
            //MyChannelSwitcher.Switch(MyChannelConstants.TypeMyChannelAndFollowingPanel);
            //UCMiddlePanelSwitcher.View_UCMyChannel.MyChannelListAndFollowingMainPanel.ScrlViewer.ScrollToVerticalOffset(0);
        }

        private void LoadChannelOnScroll()
        {
            _IS_LOADING = true;
            if (_SingleMediaModel != null)
            {
                int limit = ChannelList.Count + 8;
                List<ChannelModel> chnlList = ChannelViewModel.Instance.MyChannelList.Where(P => P.ChannelType == _SingleMediaModel.MediaType).ToList();
                foreach (ChannelModel chnlModel in chnlList)
                {
                    if (!ChannelList.Any(P => P.ChannelID == chnlModel.ChannelID))
                    {
                        ChannelList.InvokeAdd(chnlModel);
                        if (ChannelList.Count >= limit)
                        {
                            _IS_LOADING = false;
                            break;
                        }
                    }
                }

                if (ChannelList.Count < limit)
                {
                    LoadMoreChannelFromServer();
                }
            }
        }

        private void LoadMoreChannelFromServer()
        {
            int startIndex = ChannelViewModel.Instance.MyChannelList.Count;
            new ThrdGetOwnChannelList(0, startIndex, startIndex + 8, (status) =>
            {
                _IS_LOADING = false;
                return 0;
            }).Start();
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void scrlViewr_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.VerticalChange > 0 && scrlViewr.VerticalOffset > (scrlViewr.ScrollableHeight - 200) && _IS_LOADING == false)
            {
                this.LoadChannelOnScroll();
            }
        }

        #endregion

        #region "ICommand"

        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }

        public ICommand BlackSpaceClicked
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }

        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        public ICommand CraeteChannnelDockCommand
        {
            get
            {
                if (_CraeteAlbumDockCommand == null)
                {
                    _CraeteAlbumDockCommand = new RelayCommand(param => OnCraeteChannnelDockCommand());
                }
                return _CraeteAlbumDockCommand;
            }
        }

        public ICommand AddToChannelCommand
        {
            get
            {
                if (_AddToChannelCommand == null)
                {
                    _AddToChannelCommand = new RelayCommand(param => OnAddToChannelCommand(param));
                }
                return _AddToChannelCommand;
            }
        }

        #endregion

        #region Property

        public bool IsProcessing
        {
            get { return _IsProcessing; }
            set
            {
                if (value == _IsProcessing) return;
                _IsProcessing = value;
                this.OnPropertyChanged("IsProcessing");
            }
        }

        public ObservableCollection<ChannelModel> ChannelList
        {
            get { return _ChannelList; }
            set
            {
                _ChannelList = value;
                this.OnPropertyChanged("ChannelList");
            }
        }

        #endregion

    }
}
=======
﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.Channel;
using View.Utility;
using View.Utility.Channel;
using View.Utility.WPFMessageBox;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAddToMyChannelView.xaml
    /// </summary>
    public partial class UCAddToMyChannelView : PopUpBaseControl, INotifyPropertyChanged
    {
        private static ILog log = LogManager.GetLogger(typeof(UCAddToMyChannelView).Name);
        public static UCAddToMyChannelView Instance;
        public event PropertyChangedEventHandler PropertyChanged;
        private static readonly object _PROCESS_SYNC_LOCK = new object();

        private ICommand loadedControl;
        private ICommand _blackSpaceClick;
        private ICommand _commentSpaceClicked;
        private ICommand _CraeteAlbumDockCommand;
        private ICommand _AddToChannelCommand;
        private bool _IsProcessing = false;
        private bool _IS_LOADING = false;

        private SingleMediaModel _SingleMediaModel;
        private ObservableCollection<ChannelModel> _ChannelList = new ObservableCollection<ChannelModel>();

        public UCAddToMyChannelView(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 0.0);//background Color Transparent Black
            SetParent(motherGrid);
        }

        #region "Utility Methods"

        public void ShowChannelView(SingleMediaModel singleMediaModel = null)
        {
            this.DataContext = this;
            this._SingleMediaModel = singleMediaModel;
            this.itcChannelList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelList") });
            scrlViewr.ScrollChanged += scrlViewr_ScrollChanged;
            if (_SingleMediaModel != null)
            {
                List<ChannelModel> chnlList = ChannelViewModel.Instance.MyChannelList.Where(P => P.ChannelType == _SingleMediaModel.MediaType).ToList();
                foreach (ChannelModel model in chnlList)
                {
                    ChannelList.InvokeAdd(model);
                }
            }

            if (ChannelList.Count < 20)
            {
                //new ThrdGetOwnChannelList(0, ChannelList.Count, ChannelList.Count + 10).Start();
            }
        }

        public void HidePopup()
        {
            this.DataContext = null;
            ChannelList.Clear();
            this.itcChannelList.ClearValue(ItemsControl.ItemsSourceProperty);
            Hide();
        }

        private void OnLoadedControl()
        {
            this.Focusable = true;
            this.Focus();
            //this.SearchAlbumTextBox.Focusable = true;
            //this.SearchAlbumTextBox.Focus();
        }

        private void OnClickBlackSpace(object param)
        {
            HidePopup();
        }

        private void OnAddToChannelCommand(object param)
        {
            if (string.IsNullOrWhiteSpace(tbChannelTitle.Text))
            {
                UIHelperMethods.ShowWarning("Please give a title", "Media title");
                return;
            }
            if (param is ChannelModel && _SingleMediaModel != null)
            {
                if (_SingleMediaModel.Duration <= 0)
                {
                    UIHelperMethods.ShowWarning("Media duration must greater than zero", "Media duration");
                    return;
                }
                ChannelModel model = (ChannelModel)param;
                List<ChannelMediaDTO> mediaList = new List<ChannelMediaDTO>();
                ChannelMediaDTO channelMediaDTO = ChannelHelpers.GetChannelMediaDtoFromChannelModel(model);
                channelMediaDTO.Title = tbChannelTitle.Text;
                channelMediaDTO.Artist = _SingleMediaModel.Artist;
                channelMediaDTO.MediaID = _SingleMediaModel.ContentId;
                channelMediaDTO.MediaType = _SingleMediaModel.MediaType;
                channelMediaDTO.MediaUrl = _SingleMediaModel.StreamUrl;
                channelMediaDTO.ThumbImageUrl = _SingleMediaModel.ThumbUrl;
                channelMediaDTO.ThumbImageWidth = _SingleMediaModel.ThumbImageWidth;
                channelMediaDTO.ThumbImageHeight = _SingleMediaModel.ThumbImageHeight;
                channelMediaDTO.Duration = 1000 * _SingleMediaModel.Duration;// in ms
                channelMediaDTO.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                mediaList.Add(channelMediaDTO);
                new ThrdAddUploadedChannelMedia(mediaList, (status, message) =>
                {
                    if (status)
                    {
                        UIHelperMethods.ShowMessageWithTimerFromThread(gridContainer, "Media added Successfully!", 1000);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(message)) UIHelperMethods.ShowWarning(message, "Add media");
                        else UIHelperMethods.ShowWarning("Failed to add media", "Add media");
                    }
                    return 0;
                }).Start();
            }
        }

        public void AddIntoMyChannelListPopUp(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelList;
                lock (_PROCESS_SYNC_LOCK)
                {

                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();

                    ChannelModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.CreationTime > firstModel.CreationTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.CreationTime < lastModel.CreationTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotCount = viewerModelList.ElementAt(pivot).CreationTime;
                                    if (newModel.CreationTime > pivotCount)
                                    {
                                        max = pivot;
                                    }
                                    else
                                    {
                                        min = pivot + 1;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.CreationTime > firstModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.CreationTime > newModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.CreationTime > viewerModelList.ElementAt(pivot).CreationTime)
                                {
                                    max = pivot;
                                }
                                else
                                {
                                    min = pivot + 1;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoMyChannelListPopUp() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }


        private void OnCommentSpaceClicked(object param)
        {

        }

        private void OnCraeteChannnelDockCommand()
        {
            //HidePopup();
            //MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyChannel, true);
            //MyChannelSwitcher.Switch(MyChannelConstants.TypeMyChannelAndFollowingPanel);
            //UCMiddlePanelSwitcher.View_UCMyChannel.MyChannelListAndFollowingMainPanel.ScrlViewer.ScrollToVerticalOffset(0);
        }

        private void LoadChannelOnScroll()
        {
            _IS_LOADING = true;
            if (_SingleMediaModel != null)
            {
                int limit = ChannelList.Count + 8;
                List<ChannelModel> chnlList = ChannelViewModel.Instance.MyChannelList.Where(P => P.ChannelType == _SingleMediaModel.MediaType).ToList();
                foreach (ChannelModel chnlModel in chnlList)
                {
                    if (!ChannelList.Any(P => P.ChannelID == chnlModel.ChannelID))
                    {
                        ChannelList.InvokeAdd(chnlModel);
                        if (ChannelList.Count >= limit)
                        {
                            _IS_LOADING = false;
                            break;
                        }
                    }
                }

                if (ChannelList.Count < limit)
                {
                    LoadMoreChannelFromServer();
                }
            }
        }

        private void LoadMoreChannelFromServer()
        {
            //int startIndex = ChannelViewModel.Instance.MyChannelList.Count;
            //new ThrdGetOwnChannelList(0, startIndex, startIndex + 8, (status) =>
            //{
            //    _IS_LOADING = false;
            //    return 0;
            //}).Start();
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void scrlViewr_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.VerticalChange > 0 && scrlViewr.VerticalOffset > (scrlViewr.ScrollableHeight - 200) && _IS_LOADING == false)
            {
                this.LoadChannelOnScroll();
            }
        }

        #endregion

        #region "ICommand"

        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }

        public ICommand BlackSpaceClicked
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }

        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        public ICommand CraeteChannnelDockCommand
        {
            get
            {
                if (_CraeteAlbumDockCommand == null)
                {
                    _CraeteAlbumDockCommand = new RelayCommand(param => OnCraeteChannnelDockCommand());
                }
                return _CraeteAlbumDockCommand;
            }
        }

        public ICommand AddToChannelCommand
        {
            get
            {
                if (_AddToChannelCommand == null)
                {
                    _AddToChannelCommand = new RelayCommand(param => OnAddToChannelCommand(param));
                }
                return _AddToChannelCommand;
            }
        }

        #endregion

        #region Property

        public bool IsProcessing
        {
            get { return _IsProcessing; }
            set
            {
                if (value == _IsProcessing) return;
                _IsProcessing = value;
                this.OnPropertyChanged("IsProcessing");
            }
        }

        public ObservableCollection<ChannelModel> ChannelList
        {
            get { return _ChannelList; }
            set
            {
                _ChannelList = value;
                this.OnPropertyChanged("ChannelList");
            }
        }

        #endregion

    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.UI.MediaCloud;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMeidaCloudDownloadPopup.xaml
    /// </summary>
    public partial class UCMeidaCloudDownloadPopup : PopUpBaseControl
    {
        public static UCMeidaCloudDownloadPopup Instance;
        #region "Constructor"
        public UCMeidaCloudDownloadPopup(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
        #endregion

        #region "Icommands"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        ICommand _blackSpaceClick;
        public ICommand BlackSpaceClick
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace());
                }
                return _blackSpaceClick;
            }
        }
        private void OnClickBlackSpace()
        {
            HideMediaDowload();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {
            OnLoadedControl();
        }

        #endregion

        #region "Public Methods"
        public void HideMediaDowload()
        { 
            Visibility = Visibility.Hidden;
            Hide();
        }

        /*private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                scroll.ScrollToEnd();
                e.Handled = true;
            }
        }*/

        public void ShowMediaDownload()
        {
            if (UCMediaContentsDownloadView.Instance == null) UCMediaContentsDownloadView.Instance = new UCMediaContentsDownloadView();
            else if (UCMediaContentsDownloadView.Instance.Parent is Border) ((Border)UCMediaContentsDownloadView.Instance.Parent).Child = null;
            containerBorder.Child = UCMediaContentsDownloadView.Instance;
            UCMediaContentsDownloadView.Instance.ShowDownloadedMedias(null);
            UCMediaContentsDownloadView.Instance.scroll.ScrollToTop();
            this.Visibility = Visibility.Visible;
        }
        #endregion
    }
}

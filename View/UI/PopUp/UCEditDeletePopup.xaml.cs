﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Utility;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.Utility.DataContainer;
using View.Utility.RingPlayer;
using Newtonsoft.Json.Linq;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCEditDeletePopup.xaml
    /// </summary>
    public partial class UCEditDeletePopup : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCEditDeletePopup).Name);

        private bool isFeed = true;

        public UCEditDeletePopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            SetParent(motherGrid);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private FeedModel feedModel;
        private CommentModel commentModel;

        #region "Properties"

        private UCAddLocationView ucAddLocationView
        {
            get
            {
                return MainSwitcher.PopupController.ucAddLocationView;
            }
        }

        private int _NumberOfMenus = 1;
        public int NumberOfMenus
        {
            get { return _NumberOfMenus; }
            set
            {
                if (value == _NumberOfMenus)
                    return;
                _NumberOfMenus = value;
                this.OnPropertyChanged("NumberOfMenus");
            }
        }

        private bool _EditVisible = false;
        public bool EditVisible
        {
            get { return _EditVisible; }
            set
            {
                if (value == _EditVisible)
                    return;
                _EditVisible = value;
                this.OnPropertyChanged("EditVisible");
            }
        }

        private bool _AddLocationVisible = false;
        public bool AddLocationVisible
        {
            get { return _AddLocationVisible; }
            set
            {
                if (value == _AddLocationVisible)
                    return;
                _AddLocationVisible = value;
                this.OnPropertyChanged("AddLocationVisible");
            }
        }

        #endregion ""

        public void ShowPopup(UIElement target, bool EditOff, object model)
        {
            try
            {
                if (model is FeedModel)
                {
                    this.feedModel = (FeedModel)model;
                    isFeed = true;
                }
                else if (model is CommentModel)
                {
                    this.commentModel = (CommentModel)model;
                    isFeed = false;
                }
                AddLocationVisible = false;
                NumberOfMenus = 1;
                //if (commentModel.TotalLikeComment > 0)
                //    EditVisible = EditOff;
                //else 
                EditVisible = !EditOff;
                if (EditVisible)
                {
                    NumberOfMenus = 2;
                }
                if (isFeed)
                {
                    if (feedModel.PostOwner.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
                        EditVisible = false;
                    else if (feedModel.LocationModel == null)
                        AddLocationVisible = true;
                    if (EditVisible && AddLocationVisible) NumberOfMenus = 3;
                    else if ((EditVisible && !AddLocationVisible) || (!EditVisible && AddLocationVisible)) NumberOfMenus = 2;
                    else NumberOfMenus = 1;
                }

                if (popupEditDelete.IsOpen)
                {
                    popupEditDelete.IsOpen = false;
                }
                else
                {
                    popupEditDelete.PlacementTarget = target;

                    popupEditDelete.IsOpen = true;
                    addLocation.MouseLeftButtonDown += addLocation_Click;
                    edit.MouseLeftButtonDown += edit_Click;
                    delete.MouseLeftButtonDown += delete_Click;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public void ClosePopUp()
        {
            popupEditDelete.IsOpen = false;
        }

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            ClosePopUp();
        }

        private ICommand _PopupEditDeleteClosed;
        public ICommand PopupEditDeleteClosed
        {
            get
            {
                if (_PopupEditDeleteClosed == null) _PopupEditDeleteClosed = new RelayCommand(param => OnPopupEditDeleteClosed());
                return _PopupEditDeleteClosed;
            }
        }
        private void OnPopupEditDeleteClosed()
        {
            Hide();
            addLocation.MouseLeftButtonDown -= addLocation_Click;
            edit.MouseLeftButtonDown -= edit_Click;
            delete.MouseLeftButtonDown -= delete_Click;
        }
        #endregion

        private void edit_Click(object sender, MouseButtonEventArgs e)
        {
            popupEditDelete.IsOpen = false;
            if (isFeed)
            {
                if (feedModel.LinkModel != null && !string.IsNullOrEmpty(feedModel.LinkModel.PreviewUrl) && string.IsNullOrEmpty(feedModel.Status))
                {
                    feedModel.Status = feedModel.LinkModel.PreviewUrl;
                }
                feedModel.IsEditMode = true;
            }
            else
            {
                commentModel.IsEditMode = true;
            }
        }


        private void delete_Click(object sender, MouseButtonEventArgs e)
        {
            popupEditDelete.IsOpen = false;
            if (isFeed)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this post"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
                if (isTrue)
                    new ThradDeleteFeed().StartThread(feedModel.NewsfeedId);
            }
            else
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this comment"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
                if (isTrue)
                {
                    JObject pakToSend = new JObject();
                    FeedModel feedModel = null;
                    bool ImgToContentID = false;
                    pakToSend[JsonKeys.Action] = AppConstants.ACTION_MERGED_DELETE_COMMENT;
                    pakToSend[JsonKeys.CommentId] = commentModel.CommentId;

                    if (commentModel.ImageId != Guid.Empty)     //// DELETE COMMENT OF IMAGE
                    {
                        ImageModel imageModel = null;
                        ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(commentModel.ImageId, out imageModel);
                        bool fromFeed = (ViewConstants.NavigateFrom == StatusConstants.NAVIGATE_FROM_FEED) ? true : false;

                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_IMAGE;
                        pakToSend[JsonKeys.OwnerId] = imageModel.UserShortInfoModel.UserTableID;
                        pakToSend[JsonKeys.ContentId] = commentModel.ImageId;
                        ImgToContentID = true;
                        if (fromFeed)
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            if (imageModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(imageModel.NewsFeedId, out feedModel))
                            {
                                pakToSend[JsonKeys.NewsfeedId] = imageModel.NewsFeedId;
                                pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                                pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            }
                        }
                        else
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                            pakToSend[JsonKeys.AlbumId] = imageModel.AlbumId;
                            pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                        }
                    }
                    else if (commentModel.ContentId != Guid.Empty)     //// DELETE COMMENT OF MULTIMEDIA
                    {
                        SingleMediaModel singleMediaModel = null;
                        MediaDataContainer.Instance.ContentModels.TryGetValue(commentModel.ContentId, out singleMediaModel);
                        bool fromFeed = singleMediaModel.PlayedFromFeed ? true : false;

                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_MULTIMEDIA;
                        pakToSend[JsonKeys.OwnerId] = singleMediaModel.MediaOwner.UserTableID;
                        pakToSend[JsonKeys.ContentId] = singleMediaModel.ContentId;
                        if (fromFeed)
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            if (singleMediaModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(singleMediaModel.NewsFeedId, out feedModel))
                            {
                                pakToSend[JsonKeys.NewsfeedId] = singleMediaModel.NewsFeedId;
                                pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                                pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            }
                        }
                        else
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                            pakToSend[JsonKeys.AlbumId] = singleMediaModel.AlbumId;
                            pakToSend[JsonKeys.MediaType] = singleMediaModel.MediaType;
                        }
                    }
                    else      //// DELETE COMMENT OF STATUS
                    {
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_STATUS;
                        if (commentModel.NewsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(commentModel.NewsfeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            pakToSend[JsonKeys.NewsfeedId] = commentModel.NewsfeedId;
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            pakToSend[JsonKeys.OwnerId] = feedModel.PostOwner.UserTableID;

                            if (feedModel.BookPostType == 2 || feedModel.BookPostType == 5 || feedModel.BookPostType == 8)
                            {
                                if (feedModel.SingleMediaFeedModel != null)
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleMediaFeedModel.ContentId;

                                else if (feedModel.SingleImageFeedModel != null)
                                {
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleImageFeedModel.ImageId;
                                    ImgToContentID = true;
                                }
                            }
                        }
                    }
                    new ThreadDeleteAnyComment(pakToSend, ImgToContentID).StartThread();
                }
            }
        }

        private void addLocation_Click(object sender, MouseButtonEventArgs e)
        {
            ucAddLocationView.Show();
            ucAddLocationView.ShowAddLoation(feedModel.NewsfeedId, feedModel.Status, feedModel.StatusTags, feedModel.WallOrContentOwner.UserTableID, (feedModel.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE) ? feedModel.WallOrContentOwner.UserTableID : 0);
            popupEditDelete.IsOpen = false;
        }
        #region "Utility Methods"

        #endregion "Utility Methods"
    }
}

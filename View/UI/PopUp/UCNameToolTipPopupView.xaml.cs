﻿using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.ImageViewer;
using View.UI.MediaPlayer;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCNameToolTipPopupView.xaml
    /// </summary>
    public partial class UCNameToolTipPopupView : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCNameToolTipPopupView).Name);
        #region "Fields"
        int POPUP_HEIGHT;
        Grid motherPanel;
        public static UCNameToolTipPopupView Instance;
        #endregion

        #region "Constructor"
        public UCNameToolTipPopupView(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            SetParent(motherGrid);
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region "Properties"
        private bool _IsUpper;
        public bool IsUpper
        {
            get
            {
                return _IsUpper;
            }
            set
            {
                _IsUpper = value;
                OnPropertyChanged("IsUpper");
            }
        }

        private int _type;//0=userBaseModel, 1=userShortInfoModel
        public int Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                OnPropertyChanged("Type");
            }
        }

        //private int _pType;//0=user, 1=mediaPage, 2=newsPortal, 3=page, 4=celebrity
        //public int PType
        //{
        //    get
        //    {
        //        return _pType;
        //    }
        //    set
        //    {
        //        _pType = value;
        //        OnPropertyChanged("PType");
        //    }
        //}
        private BaseUserProfileModel _BaseModel;
        public BaseUserProfileModel BaseModel
        {
            get
            {
                return _BaseModel;
            }
            set
            {
                if (_BaseModel == value)
                {
                    return;
                }
                _BaseModel = value;
                OnPropertyChanged("BaseModel");
            }
        }

        private bool _IsFrnd;
        public bool IsFrnd
        {
            get
            {
                return _IsFrnd;
            }
            set
            {
                _IsFrnd = value;
                OnPropertyChanged("IsFrnd");
            }
        }
        private int _FriendMutualCount;
        public int FriendMutualCount
        {
            get
            {
                return _FriendMutualCount;
            }
            set
            {
                _FriendMutualCount = value;
                OnPropertyChanged("FriendMutualCount");
            }
        }

        private long _UserIdentity;
        public long UserIdentity
        {
            get
            {
                return _UserIdentity;
            }
            set
            {
                _UserIdentity = value;
                OnPropertyChanged("UserIdentity");
            }
        }
        private string _PageSlogan = "";
        public string PageSlogan
        {
            get { return _PageSlogan; }
            set
            {
                if (value == _PageSlogan)
                    return;
                _PageSlogan = value;
                this.OnPropertyChanged("PageSlogan");
            }
        }

        private string _Loader;
        public string Loader
        {
            get
            {
                return _Loader;
            }
            set
            {
                if (value == _Loader)
                    return;
                _Loader = value;
                OnPropertyChanged("Loader");
            }
        }
        #endregion

        #region "Private Method"
        private void setButtonControl()
        {
            if (UCNameToolTipPopupButtonContainer.Instance == null) UCNameToolTipPopupButtonContainer.Instance = new UCNameToolTipPopupButtonContainer();
            else if (UCNameToolTipPopupButtonContainer.Instance.Parent is Border) ((Border)UCNameToolTipPopupButtonContainer.Instance.Parent).Child = null;

            UCNameToolTipPopupButtonContainer.Instance.Type = BaseModel.ProfileType;
            BtnContentsBdr.Child = UCNameToolTipPopupButtonContainer.Instance;
        }

        private void SetEvents()
        {
            contentContainer.MouseEnter += contentContainer_MouseEnter;
            contentContainer.MouseLeave += contentContainer_MouseLeave;
        }

        private void setPopupOpen(UIElement target)
        {
            int buttonPosition = (int)target.PointToScreen(new System.Windows.Point(0, 0)).Y;
            int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;

            if ((buttonPosition < POPUP_HEIGHT && bottomSpace >= POPUP_HEIGHT) || bottomSpace >= POPUP_HEIGHT)
            {
                IsUpper = false;
            }
            else
            {
                IsUpper = true;
            }
            SetEvents();
            popupMainPanel.Height = POPUP_HEIGHT;
            PopupToolTip.PlacementTarget = target;
            PopupToolTip.IsOpen = true;
        }

        private void setFrndShipStatus(int frndShipStatus, long utId)
        {
            if (UCNameToolTipPopupButtonContainer.Instance != null)
            {
                if (frndShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                    UCNameToolTipPopupButtonContainer.Instance.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_ACCEPTED;
                else if (frndShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
                    UCNameToolTipPopupButtonContainer.Instance.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_INCOMING;
                else if (frndShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
                    UCNameToolTipPopupButtonContainer.Instance.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_PENDING;
                else if (frndShipStatus == StatusConstants.FRIENDSHIP_STATUS_UNKNOWN)
                    UCNameToolTipPopupButtonContainer.Instance.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_UNKNOWN;

                UCNameToolTipPopupButtonContainer.Instance.User_Table_ID = utId;
            }
        }
        private void setIsSubscribed(bool IsSubscribe)
        {
            if (UCNameToolTipPopupButtonContainer.Instance != null)
                UCNameToolTipPopupButtonContainer.Instance.IsSubscribed = IsSubscribe;
        }
        #endregion

        #region "Public Method"

        public void ShowPopUp(UIElement target, BaseUserProfileModel model, Grid parentPanel = null)
        {
            if (parentPanel != null)
                motherPanel = parentPanel;
            Type = 0;
            this.BaseModel = model;
            if (!HelperMethods.IsRingIDOfficialAccount(BaseModel.UserTableID))
            {
                if (BaseModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
                {
                    Loader = ImageLocation.LOADER_FEED_CYCLE;
                    if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                        setButtonControl();
                    ThreadChannelInfoRequest request;
                    if (BaseModel.ProfileType == SettingsConstants.PROFILE_TYPE_GENERAL)
                    {
                        request = new ThreadChannelInfoRequest(0, BaseModel.UserTableID, 0, 0, true);
                    }
                    else
                    {
                        request = new ThreadChannelInfoRequest(0, BaseModel.UserTableID, 0, BaseModel.ProfileType);
                    }
                    request.OnSuccess += (jObj) =>
                    {
                        if (jObj != null)
                        {
                            if (BaseModel.ProfileType == SettingsConstants.PROFILE_TYPE_GENERAL)
                            {
                                if (jObj[JsonKeys.UserIdentity] != null)
                                    UserIdentity = (long)jObj[JsonKeys.UserIdentity];
                                if (jObj[JsonKeys.NumberOfMutualFriendCount] != null)
                                {
                                    FriendMutualCount = (int)jObj[JsonKeys.NumberOfMutualFriendCount];
                                }
                                if (jObj[JsonKeys.FriendshipStatus] != null)
                                {
                                    int frndShipStatus = (int)jObj[JsonKeys.FriendshipStatus];
                                    setFrndShipStatus(frndShipStatus, BaseModel.UserTableID);
                                }
                                if(jObj[JsonKeys.ProfileImage] != null)
                                {
                                    BaseModel.ProfileImage = (string)jObj[JsonKeys.ProfileImage];
                                    OnPropertyChanged("BaseModel");
                                }
                            }
                            else 
                            {
                                if (jObj[JsonKeys.NewsPortalDTO] != null)
                                {
                                    JObject jobjt = (JObject)jObj[JsonKeys.NewsPortalDTO];
                                    if (jobjt[JsonKeys.NewsPortalSlogan] != null)
                                        PageSlogan = (string)jobjt[JsonKeys.NewsPortalSlogan];
                                    if (jobjt[JsonKeys.SubscriberCount] != null)
                                        FriendMutualCount = (int)jobjt[JsonKeys.SubscriberCount];
                                    if (jobjt[JsonKeys.Subscribe] != null)
                                    {
                                        bool isSubscribe = (bool)jobjt[JsonKeys.Subscribe];
                                        setIsSubscribed(isSubscribe);
                                    }
                                }
                            }
                        }
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            try
                            {
                                Loader = null;
                                ContentPanel.Visibility = Visibility.Visible;
                            }
                            catch (Exception ex)
                            {
                                //log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
                            }
                        }, System.Windows.Threading.DispatcherPriority.Send);
                    };
                    request.Start();
                    POPUP_HEIGHT = 160;
                }
                else
                {
                    ContentPanel.Visibility = Visibility.Visible;
                    UserIdentity = DefaultSettings.LOGIN_RING_ID;
                    POPUP_HEIGHT = 130;
                }
            }
            //if (type == 0)
            //{
            //    Type = type;
            //    if (BaseModel.ProfileType == SettingsConstants.PROFILE_TYPE_GENERAL)
            //    {
            //        if (!HelperMethods.IsRingIDOfficialAccount(BaseModel.UserTableID))
            //        {
            //            if (BaseModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
            //            {
            //                setButtonControl(true);

            //                ThreadChannelInfoRequest request = new ThreadChannelInfoRequest(0, BaseModel.UserTableID, 0, 0, true);
            //                request.OnSuccess += (jObj) =>
            //                {
            //                    if (jObj != null)
            //                    {
            //                        if (BaseModel.ProfileType == SettingsConstants.PROFILE_TYPE_GENERAL)
            //                        {
            //                            if (jObj[JsonKeys.UserIdentity] != null)
            //                                UserIdentity = (long)jObj[JsonKeys.UserIdentity];
            //                            if (jObj[JsonKeys.NumberOfMutualFriendCount] != null)
            //                            {
            //                                FriendMutualCount = (int)jObj[JsonKeys.NumberOfMutualFriendCount];
            //                            }
            //                            if (jObj[JsonKeys.FriendshipStatus] != null)
            //                            {
            //                                int frndShipStatus = (int)jObj[JsonKeys.FriendshipStatus];
            //                                setFrndShipStatus(frndShipStatus, BaseModel.UserTableID);
            //                            }
            //                        }
            //                        else
            //                        {

            //                        }
            //                    }
            //                };
            //                request.Start();
            //                POPUP_HEIGHT = 180;
            //            }
            //            else
            //            {
            //                UserIdentity = DefaultSettings.LOGIN_RING_ID;
            //                POPUP_HEIGHT = 140;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        IsFrnd = false;
            //        setButtonControl(false);
            //        ThreadChannelInfoRequest request = new ThreadChannelInfoRequest(0, BaseModel.UserTableID, 0, BaseModel.ProfileType);
            //        request.OnSuccess += (jObj) =>
            //        {
            //            if (jObj != null)
            //            {
                             
            //            }
            //        };
            //        request.Start();
            //        if (UCNameToolTipPopupButtonContainer.Instance != null)
            //        {
            //            UCNameToolTipPopupButtonContainer.Instance.Type = BaseModel.ProfileType;
            //        }
            //        POPUP_HEIGHT = 180;
            //    }
               
            //}
            setPopupOpen(target);
        }

        public void HidePopUp(bool isNameClick)
        {
            if (motherPanel != null)
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                //Hide();
                if (obj != null && obj is UCImageViewInMain)
                {
                    UCImageViewInMain imageViewInMain1 = (UCImageViewInMain)obj;
                    if (isNameClick)
                    {
                        imageViewInMain1.Hide();
                        imageViewInMain1 = null;
                    }
                    else
                    {
                        imageViewInMain1.Focus();
                        imageViewInMain1.Focusable = true;
                    }
                }
                else if (obj != null && obj is UCMediaPlayerInMain)
                {
                    UCMediaPlayerInMain mediaViewInMain = (UCMediaPlayerInMain)obj;
                    if (isNameClick)
                    {
                        mediaViewInMain.OnlyPlayerView.OnGoToSmallScreen();
                    }
                    else
                    {
                        mediaViewInMain.Focus();
                        mediaViewInMain.Focusable = true;
                    }
                }
                obj = null;
            }

            PopupToolTip.IsOpen = false;
            //if (_parentGrid != null)
            //{
            //    _parentGrid.Children.Remove(this);
            //}
            //if (OnRemovedUserControl != null)
            //    OnRemovedUserControl();
            //GC.Collect();
        }
        #endregion

        private void contentContainer_MouseEnter(object sender, MouseEventArgs e)
        {
            PopupToolTip.IsOpen = true;
        }
        private void contentContainer_MouseLeave(object sender, MouseEventArgs e)
        {
            if(!(UCMutualFriendsPopup.Instance != null && UCMutualFriendsPopup.Instance.popupMutual.IsOpen))
                HidePopUp(false);
        }

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            HidePopUp(false);
        }

        private ICommand _PopupToolTipClosed;
        public ICommand PopupToolTipClosed
        {
            get
            {
                if (_PopupToolTipClosed == null) _PopupToolTipClosed = new RelayCommand(param => OnPopupToolTipClosed());
                return _PopupToolTipClosed;
            }
        }
        private void OnPopupToolTipClosed()
        {
            contentContainer.MouseEnter -= contentContainer_MouseEnter;
            contentContainer.MouseLeave -= contentContainer_MouseLeave;
            PageSlogan = "";
            if (Loader != null)
                Loader = null;
            Hide();
        }
        private ICommand _contentContainerMouseEnter;
        public ICommand contentContainerMouseEnter
        {
            get
            {
                if (_contentContainerMouseEnter == null) _contentContainerMouseEnter = new RelayCommand(param => OncontentContainerMouseEnter());
                return _contentContainerMouseEnter;
            }
        }
        private void OncontentContainerMouseEnter()
        {
            PopupToolTip.IsOpen = true;
        }

        private ICommand _contentContainerMouseLeave;
        public ICommand contentContainerMouseLeave
        {
            get
            {
                if (_contentContainerMouseLeave == null) _contentContainerMouseLeave = new RelayCommand(param => OncontentContainerMouseLeave());
                return _contentContainerMouseLeave;
            }
        }
        private void OncontentContainerMouseLeave()
        {
            HidePopUp(false);
        }

        private ICommand _OpenMutualFrndPopupCommand;
        public ICommand OpenMutualFrndPopupCommand
        {
            get
            {
                if (_OpenMutualFrndPopupCommand == null)
                {
                    _OpenMutualFrndPopupCommand = new RelayCommand(param => OnOpenMutualFrndPopupCommand(param));
                }
                return _OpenMutualFrndPopupCommand;
            }
        }
        private void OnOpenMutualFrndPopupCommand(object parameter)
        {
            if (parameter is Button)
            {
                Button control = (Button)parameter;

                if (FriendMutualCount > 0)
                {
                    if (UCMutualFriendsPopup.Instance != null)
                        mainGrid.Children.Remove(UCMutualFriendsPopup.Instance);
                    UCMutualFriendsPopup.Instance = new UCMutualFriendsPopup();
                    UCMutualFriendsPopup.Instance.SetParent(mainGrid);
                    UCMutualFriendsPopup.Instance.Show();
                    UCMutualFriendsPopup.Instance.ShowPopup(control, BaseModel.UserTableID, BaseModel.FullName);
                    UCMutualFriendsPopup.Instance.OnRemovedUserControl += () =>
                    {
                        UCMutualFriendsPopup.Instance.MutualList.Clear();
                        UCMutualFriendsPopup.Instance = null;
                        if (Instance != null)
                            OnLoadedControl();
                    };
                    
                    SendDataToServer.SendMutualFreindRequest(BaseModel.UserTableID);
                }
            }
        }
        #endregion
    }
}

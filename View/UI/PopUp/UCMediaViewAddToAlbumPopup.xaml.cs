﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Models.Entity;
using View.BindingModels;
using View.Utility;
using View.Utility.Feed;
using View.ViewModel;
using View.Utility.RingPlayer;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMediaViewAddToAlbumPopup.xaml
    /// </summary>
    public partial class UCMediaViewAddToAlbumPopup : UserControl, INotifyPropertyChanged
    {
        public static UCMediaViewAddToAlbumPopup Instance;
        public UCMediaViewAddToAlbumPopup()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            this.popupAddToAlbum.Closed += popupAddToAlbum_Closed;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //private ObservableCollection<MediaContentModel> _MyMediaList;
        //public ObservableCollection<MediaContentModel> MyMediaList
        //{
        //    get
        //    {
        //        return _MyMediaList;
        //    }
        //    set
        //    {
        //        _MyMediaList = value;
        //        this.OnPropertyChanged("MyMediaList");
        //    }
        //}
        private bool _IsAllEnabled = true;
        public bool IsAllEnabled
        {
            get { return _IsAllEnabled; }
            set
            {
                if (_IsAllEnabled == value) return;
                _IsAllEnabled = value;
                OnPropertyChanged("IsAllEnabled");
            }
        }
        private int _MediaType;
        public int MediaType
        {
            get
            {
                return _MediaType;
            }
            set
            {
                _MediaType = value;
                this.OnPropertyChanged("MediaType");
            }
        }
        private ImageSource _LoaderAddToNewAlbum;
        public ImageSource LoaderAddToNewAlbum
        {
            get
            {
                return _LoaderAddToNewAlbum;
            }
            set
            {
                if (value == _LoaderAddToNewAlbum)
                    return;
                _LoaderAddToNewAlbum = value;
                OnPropertyChanged("LoaderAddToNewAlbum");
            }
        }

        //private ImageSource _LoaderAddToExistingAlbum;
        //public ImageSource LoaderAddToExistingAlbum
        //{
        //    get
        //    {
        //        return _LoaderAddToExistingAlbum;
        //    }
        //    set
        //    {
        //        if (value == _LoaderAddToExistingAlbum)
        //            return;
        //        _LoaderAddToExistingAlbum = value;
        //        OnPropertyChanged("LoaderAddToExistingAlbum");
        //    }
        //}


        Func<int> _OnClosing = null;
        SingleMediaModel PlayingMedia = null;

        public void Show(SingleMediaModel PlayingMedia, UIElement target, int MediaType, Func<int> onClosing)
        {
            this.PlayingMedia = PlayingMedia;
            this.MediaType = MediaType;
            // MyMediaList = (MediaType == 1) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
            _OnClosing = onClosing;

            if (popupAddToAlbum.IsOpen)
            {
                popupAddToAlbum.IsOpen = false;
            }
            else
            {
                craeteAlbumDock.MouseLeftButtonDown += craeteAlbumDock_MouseLeftButtonDown;
                addToAlbum.Click += addToAlbum_Click;
                cancelBtn.Click += cancelBtn_Click;
                CrossButton.Click += CrossButton_Click;
                SearchAlbumTextBox.TextChanged += SearchAlbumTextBox_TextChanged;

                popupAddToAlbum.PlacementTarget = target;
                popupAddToAlbum.IsOpen = true;
                ///RingPlayerViewModel.Instance.VisibilityBorderTopPanel = RingPlayerViewModel.VISIBLE;//UCGuiRingID.Instance.ucBasicMediaViewWrapper.ucBasicMediaView.borderTopPanelVisibility = Visibility.Visible;
            }
        }

        void popupAddToAlbum_Closed(object sender, EventArgs e)
        {
            if (_OnClosing != null)
            {
                _OnClosing();
                craeteAlbumDock.MouseLeftButtonDown -= craeteAlbumDock_MouseLeftButtonDown;
                addToAlbum.Click -= addToAlbum_Click;
                cancelBtn.Click -= cancelBtn_Click;
                CrossButton.Click -= CrossButton_Click;
                SearchAlbumTextBox.TextChanged -= SearchAlbumTextBox_TextChanged;
                NewAlbumTxtBox.Text = "";
                SearchAlbumTextBox.Text = "";
                createAlbumGridPanel.Visibility = Visibility.Collapsed;
                craeteAlbumDock.Visibility = Visibility.Visible;
            }
        }

        public bool IsItemAlreadyinAlbum(ObservableCollection<SingleMediaModel> ItemsOfAnAlbum, SingleMediaModel itemToAdd)
        {
            if (string.IsNullOrEmpty(itemToAdd.StreamUrl)) return false;
            foreach (var item in ItemsOfAnAlbum)
            {
                if (item.StreamUrl.Equals(itemToAdd.StreamUrl)) return true;
            }
            return false;
        }
        public bool IsAlbumNameExists(string name)
        {
            ObservableCollection<MediaContentModel> MyMediaList = (MediaType == 1) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
            foreach (var item in MyMediaList)
            {
                if (item.AlbumName.Trim().Equals(name)) return true;
            }
            return false;
        }

        private void CrossButton_Click(object sender, RoutedEventArgs e)
        {
            popupAddToAlbum.IsOpen = false;
        }

        private void addToAlbum_Click(object sender, RoutedEventArgs e)
        {
            //if (string.IsNullOrEmpty(newAlbnm)) { }
            if (!String.IsNullOrEmpty(NewAlbumTxtBox.Text))
            {
                string newAlbnm = NewAlbumTxtBox.Text.Trim();
                if (IsAlbumNameExists(newAlbnm)) UIHelperMethods.ShowWarning("An album already exists in this Name!", "Album exists");
                else
                {
                    LoaderAddToNewAlbum = ImageObjects.LOADING_WATCH;
                    IsAllEnabled = false;
                    //SingleMediaModel singleMediaModel = new SingleMediaModel { Artist = PlayingMedia.Artist, Title = PlayingMedia.Title, Duration = PlayingMedia.Duration, StreamUrl = PlayingMedia.StreamUrl, ThumbUrl = PlayingMedia.ThumbUrl, ThumbImageWidth = PlayingMedia.ThumbImageWidth, ThumbImageHeight = PlayingMedia.ThumbImageHeight };
                    //string thumburl = (!string.IsNullOrEmpty(PlayingMedia.ThumbUrl)) ? PlayingMedia.ThumbUrl : "";
                    //new ThradAddMediaAlbum(thumburl, MediaType, newAlbnm, singleMediaModel).StartThread();
                    //new ThreadAddMediaToNewOrExistingAlbum(null, PlayingMedia, newAlbnm).StartThread();
                    ThreadAddMediaToNewOrExistingAlbum thrd = new ThreadAddMediaToNewOrExistingAlbum(null, PlayingMedia, newAlbnm);
                    thrd.CallBack += (albumId) =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (UCMediaViewAddToAlbumPopup.Instance != null)
                            {
                                UCMediaViewAddToAlbumPopup.Instance.EnableButtons();
                            }
                            //if (MainSwitcher.PopupController.ucDownloadOrAddToAlbumPopUp != null && albumId != Guid.Empty)
                            //{
                            //    MainSwitcher.PopupController.ucDownloadOrAddToAlbumPopUp.EnableButtons(albumId);
                            //}
                        });
                    };
                    thrd.StartThread();
                }

            }
        }
        //private void addToExistingAlbum_Click(object sender, RoutedEventArgs e)
        //{
        //    Button btn = (Button)sender;
        //    MediaContentModel model = (MediaContentModel)btn.DataContext;
        //    if (model.MediaList != null && model.MediaList.Count > 0 && IsItemAlreadyinAlbum(model.MediaList, PlayingMedia))
        //    {
        //        View.Utility.WPFMessageBox.CustomMessageBox.ShowError("This Item already exists in this Album!", "Item Exists!");
        //    }
        //    else
        //    {
        //        model.IsLoaderOn = true;
        //        IsAllEnabled = false;
        //        new AddSingleMediatoAlbum(MediaType, model.AlbumId, PlayingMedia.StreamUrl, PlayingMedia.ThumbUrl, PlayingMedia.Duration, PlayingMedia.Title, PlayingMedia.Artist, PlayingMedia.ThumbImageWidth, PlayingMedia.ThumbImageHeight, PlayingMedia.Privacy).StartThread();
        //    }
        //    //  MakeDownloaderStart();
        //}

        public void EnableButtons()
        {
            IsAllEnabled = true;
            LoaderAddToNewAlbum = null;
            NewAlbumTxtBox.Text = "";
            createAlbumGridPanel.Visibility = Visibility.Collapsed;
            craeteAlbumDock.Visibility = Visibility.Visible;
            ObservableCollection<MediaContentModel> MyMediaList = (MediaType == 1) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
            foreach (var item in MyMediaList)
            {
                item.IsLoaderOn = false;
            }
        }

        private void craeteAlbumDock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            createAlbumGridPanel.Visibility = Visibility.Visible;
            craeteAlbumDock.Visibility = Visibility.Collapsed;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            NewAlbumTxtBox.Text = "";
            createAlbumGridPanel.Visibility = Visibility.Collapsed;
            craeteAlbumDock.Visibility = Visibility.Visible;
        }

        private void MainPanel_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Grid btn = (Grid)sender;
            MediaContentModel model = (MediaContentModel)btn.DataContext;
            if (model.MediaList != null && model.MediaList.Count > 0 && IsItemAlreadyinAlbum(model.MediaList, PlayingMedia))
                UIHelperMethods.ShowWarning("This item already exists in this album!", "Item exists!");
            else
            {
                model.IsLoaderOn = true;
                IsAllEnabled = false;
                ThreadAddMediaToNewOrExistingAlbum thrd = new ThreadAddMediaToNewOrExistingAlbum(model, PlayingMedia, null);
                thrd.CallBack += (albumId) =>
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (UCMediaViewAddToAlbumPopup.Instance != null) UCMediaViewAddToAlbumPopup.Instance.EnableButtons();
                    });
                };
                thrd.StartThread();
            }
        }

        private void SearchAlbumTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                string searchText = SearchAlbumTextBox.Text.ToLower();
                if (this.MediaType == 1)
                {
                    foreach (MediaContentModel model in RingIDViewModel.Instance.MyAudioAlbums)
                    {
                        string AlbumName = model.AlbumName.ToLower();
                        if (AlbumName.Contains(searchText) || model.AlbumName.ToString().Contains(searchText)) model.PanelVisibility = Visibility.Visible;
                        else model.PanelVisibility = Visibility.Collapsed;
                    }
                }
                else if (this.MediaType == 2)
                {
                    foreach (MediaContentModel model in RingIDViewModel.Instance.MyVideoAlbums)
                    {
                        string AlbumName = model.AlbumName.ToLower();
                        if (AlbumName.Contains(searchText) || model.AlbumName.ToString().Contains(searchText)) model.PanelVisibility = Visibility.Visible;
                        else model.PanelVisibility = Visibility.Collapsed;
                    }
                }
            }
            catch (Exception)
            {

            }
        }
    }
}

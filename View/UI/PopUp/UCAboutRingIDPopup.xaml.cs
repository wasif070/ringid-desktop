﻿using log4net;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.Utility;
namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAboutRingIDPopup.xaml
    /// </summary>
    public partial class UCAboutRingIDPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCAboutRingIDPopup).Name);

        public UCAboutRingIDPopup()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupAboutRIngID.IsOpen = false;
        }

        private ICommand popupClosedCommand;
        public ICommand PopupClosedCommand
        {
            get
            {
                if (popupClosedCommand == null) popupClosedCommand = new RelayCommand(param => OnPopupClosedCommand());
                return popupClosedCommand;
            }
        }
        private void OnPopupClosedCommand()
        {
            Hide();
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public void ShowPopup()
        {
            try
            {
                if (popupAboutRIngID.IsOpen)
                {
                    popupAboutRIngID.IsOpen = false;
                }
                else
                {
                    Show();
                    popupAboutRIngID.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
    }
}

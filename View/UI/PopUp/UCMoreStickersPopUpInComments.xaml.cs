<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.ViewModel;
using View.Utility;
using Models.Entity;
using Auth.Service.RingMarket;
using System.Threading;
using Models.Stores;
using View.Utility.WPFMessageBox;
using View.Utility.RingMarket;
using Models.DAO;
using View.Constants;
using View.UI.PopUp.MediaSendViaChat;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMoreStickersPopUpInComments.xaml
    /// </summary>
    public partial class UCMoreStickersPopUpInComments : PopUpBaseControl, INotifyPropertyChanged
    {
        private int StartIndex = 0;
        private bool _NoMoreDataFound = false;
        private bool _IS_LOADING = false;

        public UCMoreStickersPopUpInComments(Grid motherGrid)
        {
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, ViewConstants.POP_UP_BG_BORDER2, 1.0);
            SetParent(motherGrid);
            this.DataContext = this;
            this.Loaded += UCMoreStickersPopUpInComments_Loaded;
            this.Unloaded += UCMoreStickersPopUpInComments_Unloaded;
        }

        #region Utility

        public void LoadData()
        {
            IsNoMoreStickersToDownload = false;
            int limit = StickerNonDownloadedCategoryList.Count + 8;
            List<MarketStickerCategoryModel> nonDownloadedList = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.Downloaded == false).ToList();
            foreach (MarketStickerCategoryModel ctgModel in nonDownloadedList)
            {
                if (!StickerNonDownloadedCategoryList.Any(P => P.StickerCategoryID == ctgModel.StickerCategoryID))
                {
                    StickerNonDownloadedCategoryList.InvokeAdd(ctgModel);
                    if (StickerNonDownloadedCategoryList.Count >= limit)
                    {
                        _IS_LOADING = false;
                        break;
                    }
                }
            }

            if (StickerNonDownloadedCategoryList.Count < limit)
            {
                LoadMoreStickersFromServer();
            }
        }

        private void LoadMoreStickersFromServer()
        {
            bool flag = true;
            new Thread(() =>
            {
                while (flag)
                {
                    int CategoryLimit = 8, CollectionLimit = 0, StickerType = 2;
                    List<MarketStickerCategoryDTO> categoryDTOs = RingMarketStickerService.Instance.GetMoreStickerCategoriesOfSameType(StartIndex, CategoryLimit, CollectionLimit, StickerType);
                    if (categoryDTOs != null && categoryDTOs.Count > 0)
                    {
                        categoryDTOs = categoryDTOs.OrderBy(P => P.Rnk).ToList();
                        List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                        foreach (MarketStickerCategoryDTO ctgDTO in categoryDTOs)
                        {
                            MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                            if (ctgModel == null)
                            {
                                ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }

                            if (ctgModel.Downloaded == false)
                            {
                                if (StickerNonDownloadedCategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                                {
                                    StickerNonDownloadedCategoryList.InvokeAdd(ctgModel);
                                    Thread.Sleep(10);
                                    flag = false;
                                }
                            }
                        }

                        StartIndex += 8;
                    }
                    else
                    {
                        flag = false;
                        _NoMoreDataFound = true;
                    }
                }
                _IS_LOADING = false;
                if (StickerNonDownloadedCategoryList.Count == 0)
                {
                    IsNoMoreStickersToDownload = true;
                }

            }).Start();

        }

        private void OnBackToPreviousUI()
        {
            IsDetailsView = false;
        }

        private void OnDetailsViewCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)param;
            IsDetailsView = true;
            this.SelectedCategoryModel = ctgModel;

            if (ctgModel.Downloaded == true)
            {
                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgModel.StickerCategoryID).FirstOrDefault();
                if (ctgDTO != null)
                {
                    if (ctgModel.ImageList.Count < ctgDTO.ImagesList.Count)
                    {
                        ctgDTO.ImagesList.ForEach(i =>
                        {
                            MarkertStickerImagesModel imgModel = ctgModel.ImageList.Where(P => P.ImageID == i.imId).FirstOrDefault();
                            if (imgModel == null)
                            {
                                imgModel = new MarkertStickerImagesModel(i);
                                ctgModel.ImageList.InvokeAdd(imgModel);
                            }
                        });
                    }
                }

                new RingMarketStickerService(SelectedCategoryModel.StickerCategoryID, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), 0, (isNointerNet) =>
                {
                    if (isNointerNet)
                    {
                        UIHelperMethods.ShowFailed(Models.Constants.NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                    }
                    return 0;
                });
            }

            if (ctgModel.IsNew && ctgModel.IsNewStickerSeen == false && StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Contains(ctgModel.StickerCategoryID))
            {
                ctgModel.IsNewStickerSeen = true;
                RingIDViewModel.Instance.NewStickerNotificationCounter = RingIDViewModel.Instance.NewStickerNotificationCounter - 1;

                List<MarketStickerCategoryDTO> ctgDTOList = new List<MarketStickerCategoryDTO>();
                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.IsNewStickerSeen = true;
                    ctgDTOList.Add(ctgDTO);
                    new InsertIntoRingMarketStickerTable(ctgDTOList);
                }
            }
        }

        private void OnDownloadCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = null;
            if (param != null && param is MarketStickerCategoryModel)
            {
                ctgModel = (MarketStickerCategoryModel)param;
            }
            else
            {
                ctgModel = SelectedCategoryModel;
            }

            if (ctgModel.Downloaded == false) // do download
            {
                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgModel.DownloadPercentage = 0;
                    ctgModel.IsDownloadRunning = true;
                    RingMarketStickerLoadUtility.DownLoadStickerImages(ctgDTO, (isNointernet) =>
                    {
                        if (isNointernet)
                        {
                            ctgModel.DownloadPercentage = 0;
                            ctgModel.IsDownloadRunning = false;
                            UIHelperMethods.ShowFailed(Models.Constants.NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                        }
                        return 0;
                    });
                }
            }
            else   // Remove Sticker
            {
                if (ctgModel.IsDefault)
                {
                    UIHelperMethods.ShowWarning("Can't remove default sticker!", "Remove sticker");
                    return;
                }

                MarketStickerCategoryDTO ctgDTO = null;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.Downloaded = false;
                    List<MarketStickerCategoryDTO> ctgDTOs = new List<MarketStickerCategoryDTO>();
                    ctgDTOs.Add(ctgDTO);
                    RingMarketStickerLoadUtility.LoadMarketStickerRecentCategorys(ctgDTOs);
                    RingMarkerStickerDAO.UpdateStickerCategoryDownloadStatus(ctgDTO.sCtId, ctgDTO.Downloaded);
                }

                ctgModel.Downloaded = false;
                ctgModel.IsDownloadRunning = false;
                ctgModel.DownloadPercentage = 0;
                ctgModel.OnPropertyChanged("DetailImage");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region EventHandler

        private void UCMoreStickersPopUpInComments_Unloaded(object sender, RoutedEventArgs e)
        {
            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
        }

        private void UCMoreStickersPopUpInComments_Loaded(object sender, RoutedEventArgs e)
        {
            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
        }

        private void myScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (_IS_LOADING == false && _NoMoreDataFound == false && e.VerticalOffset >= (myScrlViewer.ScrollableHeight - 100))
            {
                _IS_LOADING = true;
                this.LoadData();
            }
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)((Grid)sender).DataContext;
            IsDetailsView = true;
            this.SelectedCategoryModel = ctgModel;

            if (ctgModel.Downloaded == true)
            {
                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgModel.StickerCategoryID).FirstOrDefault();
                if (ctgDTO != null)
                {
                    if (ctgModel.ImageList.Count < ctgDTO.ImagesList.Count)
                    {
                        ctgDTO.ImagesList.ForEach(i =>
                        {
                            MarkertStickerImagesModel imgModel = ctgModel.ImageList.Where(P => P.ImageID == i.imId).FirstOrDefault();
                            if (imgModel == null)
                            {
                                imgModel = new MarkertStickerImagesModel(i);
                                ctgModel.ImageList.InvokeAdd(imgModel);
                            }
                        });
                    }
                }

                new RingMarketStickerService(SelectedCategoryModel.StickerCategoryID, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), 0, (isNointerNet) =>
                {
                    if (isNointerNet)
                    {
                        UIHelperMethods.ShowFailed(Models.Constants.NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                    }
                    return 0;
                });
            }

            if (ctgModel.IsNew && ctgModel.IsNewStickerSeen == false && StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Contains(ctgModel.StickerCategoryID))
            {
                ctgModel.IsNewStickerSeen = true;
                RingIDViewModel.Instance.NewStickerNotificationCounter = RingIDViewModel.Instance.NewStickerNotificationCounter - 1;

                List<MarketStickerCategoryDTO> ctgDTOList = new List<MarketStickerCategoryDTO>();
                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.IsNewStickerSeen = true;
                    ctgDTOList.Add(ctgDTO);
                    new InsertIntoRingMarketStickerTable(ctgDTOList);
                }
            }
        }

        private void BtnBackToMain_Click(object sender, RoutedEventArgs e)
        {
            IsDetailsView = false;
        }

        #endregion

        #region Property

        private ObservableCollection<MarketStickerCategoryModel> _StickerNonDownloadedCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> StickerNonDownloadedCategoryList
        {
            get
            {
                return _StickerNonDownloadedCategoryList;
            }
            set
            {
                _StickerNonDownloadedCategoryList = value;
                this.OnPropertyChanged("StickerNonDownloadedCategoryList");
            }
        }

        private MarketStickerCategoryModel _SelectedCategoryModel;
        public MarketStickerCategoryModel SelectedCategoryModel
        {
            get { return _SelectedCategoryModel; }
            set
            {
                _SelectedCategoryModel = value;
                OnPropertyChanged("SelectedCategoryModel");
            }
        }

        private bool _IsDetailsView = false;
        public bool IsDetailsView
        {
            get { return _IsDetailsView; }
            set
            {
                if (value == _IsDetailsView) return;
                _IsDetailsView = value;
                myScrlViewer.ScrollToVerticalOffset(0);
                OnPropertyChanged("IsDetailsView");
            }
        }

        private bool _IsContextMenuOpening;
        public bool IsContextMenuOpening
        {
            get { return _IsContextMenuOpening; }
            set
            {
                _IsContextMenuOpening = value;
                OnPropertyChanged("IsContextMenuOpening");
            }
        }

        private bool _IsNoMoreStickersToDownload;
        public bool IsNoMoreStickersToDownload
        {
            get { return _IsNoMoreStickersToDownload; }
            set
            {
                _IsNoMoreStickersToDownload = value;
                OnPropertyChanged("IsNoMoreStickersToDownload");
            }
        }

        #endregion

        #region "Icommands"

        private ICommand _DownloadCommand;
        public ICommand DownloadCommand
        {
            get
            {
                if (_DownloadCommand == null)
                {
                    _DownloadCommand = new RelayCommand(param => OnDownloadCommand(param));
                }
                return _DownloadCommand;
            }
        }

        private ICommand _DetailsCommand;
        public ICommand DetailsCommand
        {
            get
            {
                if (_DetailsCommand == null)
                {
                    _DetailsCommand = new RelayCommand(param => OnDetailsViewCommand(param));
                }
                return _DetailsCommand;
            }
        }

        private ICommand _BackToPrevoiousUI;
        public ICommand BackToPrevoiousUI
        {
            get
            {
                if (_BackToPrevoiousUI == null)
                {
                    _BackToPrevoiousUI = new RelayCommand(param => OnBackToPreviousUI());
                }
                return _BackToPrevoiousUI;
            }
        }

        ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(param => OnCloseCommand(param));
                }
                return _CloseCommand;
            }
        }

        private void OnCloseCommand(object param)
        {
            IsDetailsView = false;
            _StickerNonDownloadedCategoryList.Clear();
            GC.SuppressFinalize(StickerNonDownloadedCategoryList);
            GC.Collect();
            base.Hide();
        }

        #endregion


        #region EventHandler

        private void MenuSendviaChat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)((MenuItem)sender).DataContext;
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetStickerImage(imgModel);
                OnCloseCommand(null);
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCMoreStickersPopUpInComments).Name).Error(ex.Message + " " + ex.StackTrace);
            }
        }

        private void Image_Button_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            Button button = (Button)sender;
            MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)(button.DataContext);
            imgModel.SingleStickerImageFocusable = true;
        }

        private void Image_Button_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            Button button = (Button)sender;
            MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)(button.DataContext);
            imgModel.SingleStickerImageFocusable = false;
        }

        private void bdrContextMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            IsContextMenuOpening = true;
        }

        private void bdrContextMenu_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            IsContextMenuOpening = false;
        }

        #endregion
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.ViewModel;
using View.Utility;
using Models.Entity;
using Auth.Service.RingMarket;
using System.Threading;
using Models.Stores;
using View.Utility.WPFMessageBox;
using View.Utility.RingMarket;
using Models.DAO;
using View.Constants;
using View.UI.PopUp.MediaSendViaChat;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMoreStickersPopUpInComments.xaml
    /// </summary>
    public partial class UCMoreStickersPopUpInComments : PopUpBaseControl, INotifyPropertyChanged
    {
        private int StartIndex = 0;
        private bool _NoMoreDataFound = false;
        private bool _IS_LOADING = false;

        public UCMoreStickersPopUpInComments(Grid motherGrid)
        {
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, ViewConstants.POP_UP_BG_BORDER2, 1.0);
            SetParent(motherGrid);
            this.DataContext = this;
            this.Loaded += UCMoreStickersPopUpInComments_Loaded;
            this.Unloaded += UCMoreStickersPopUpInComments_Unloaded;
        }

        #region Utility

        public void LoadData()
        {
            IsNoMoreStickersToDownload = false;
            int limit = StickerNonDownloadedCategoryList.Count + 8;
            List<MarketStickerCategoryModel> nonDownloadedList = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.Downloaded == false).ToList();
            foreach (MarketStickerCategoryModel ctgModel in nonDownloadedList)
            {
                if (!StickerNonDownloadedCategoryList.Any(P => P.StickerCategoryID == ctgModel.StickerCategoryID))
                {
                    StickerNonDownloadedCategoryList.InvokeAdd(ctgModel);
                    if (StickerNonDownloadedCategoryList.Count >= limit)
                    {
                        _IS_LOADING = false;
                        break;
                    }
                }
            }

            if (StickerNonDownloadedCategoryList.Count < limit)
            {
                LoadMoreStickersFromServer();
            }
        }

        private void LoadMoreStickersFromServer()
        {
            bool flag = true;
            new Thread(() =>
            {
                while (flag)
                {
                    int CategoryLimit = 8, CollectionLimit = 0, StickerType = 2;
                    List<MarketStickerCategoryDTO> categoryDTOs = RingMarketStickerService.Instance.GetMoreStickerCategoriesOfSameType(StartIndex, CategoryLimit, CollectionLimit, StickerType);
                    if (categoryDTOs != null && categoryDTOs.Count > 0)
                    {
                        categoryDTOs = categoryDTOs.OrderBy(P => P.Rnk).ToList();
                        List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                        foreach (MarketStickerCategoryDTO ctgDTO in categoryDTOs)
                        {
                            MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                            if (ctgModel == null)
                            {
                                ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }

                            if (ctgModel.Downloaded == false)
                            {
                                if (StickerNonDownloadedCategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                                {
                                    StickerNonDownloadedCategoryList.InvokeAdd(ctgModel);
                                    Thread.Sleep(10);
                                    flag = false;
                                }
                            }
                        }

                        StartIndex += 8;
                    }
                    else
                    {
                        flag = false;
                        _NoMoreDataFound = true;
                    }
                }
                _IS_LOADING = false;
                if (StickerNonDownloadedCategoryList.Count == 0)
                {
                    IsNoMoreStickersToDownload = true;
                }

            }).Start();

        }

        private void OnBackToPreviousUI()
        {
            IsDetailsView = false;
        }

        private void OnDetailsViewCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)param;
            IsDetailsView = true;
            this.SelectedCategoryModel = ctgModel;

            if (ctgModel.Downloaded == true)
            {
                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgModel.StickerCategoryID).FirstOrDefault();
                if (ctgDTO != null)
                {
                    if (ctgModel.ImageList.Count < ctgDTO.ImagesList.Count)
                    {
                        ctgDTO.ImagesList.ForEach(i =>
                        {
                            MarkertStickerImagesModel imgModel = ctgModel.ImageList.Where(P => P.ImageID == i.imId).FirstOrDefault();
                            if (imgModel == null)
                            {
                                imgModel = new MarkertStickerImagesModel(i);
                                ctgModel.ImageList.InvokeAdd(imgModel);
                            }
                        });
                    }
                }

                new RingMarketStickerService(SelectedCategoryModel.StickerCategoryID, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), 0, (isNointerNet) =>
                {
                    if (isNointerNet)
                    {
                        UIHelperMethods.ShowFailed(Models.Constants.NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                    }
                    return 0;
                });
            }

            if (ctgModel.IsNew && ctgModel.IsNewStickerSeen == false && StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Contains(ctgModel.StickerCategoryID))
            {
                ctgModel.IsNewStickerSeen = true;
                RingIDViewModel.Instance.NewStickerNotificationCounter = RingIDViewModel.Instance.NewStickerNotificationCounter - 1;

                List<MarketStickerCategoryDTO> ctgDTOList = new List<MarketStickerCategoryDTO>();
                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.IsNewStickerSeen = true;
                    ctgDTOList.Add(ctgDTO);
                    new InsertIntoRingMarketStickerTable(ctgDTOList);
                }
            }
        }

        private void OnDownloadCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = null;
            if (param != null && param is MarketStickerCategoryModel)
            {
                ctgModel = (MarketStickerCategoryModel)param;
            }
            else
            {
                ctgModel = SelectedCategoryModel;
            }

            if (ctgModel.Downloaded == false) // do download
            {
                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgModel.DownloadPercentage = 0;
                    ctgModel.IsDownloadRunning = true;
                    RingMarketStickerLoadUtility.DownLoadStickerImages(ctgModel, ctgDTO, (isNointernet) =>
                    {
                        if (isNointernet)
                        {
                            ctgModel.DownloadPercentage = 0;
                            ctgModel.IsDownloadRunning = false;
                            UIHelperMethods.ShowFailed(Models.Constants.NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                        }
                        return 0;
                    });
                }
            }
            else   // Remove Sticker
            {
                if (ctgModel.IsDefault)
                {
                    UIHelperMethods.ShowWarning("Can't remove default sticker!", "Remove sticker");
                    return;
                }

                MarketStickerCategoryDTO ctgDTO = null;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.Downloaded = false;
                    List<MarketStickerCategoryDTO> ctgDTOs = new List<MarketStickerCategoryDTO>();
                    ctgDTOs.Add(ctgDTO);
                    RingMarketStickerLoadUtility.LoadMarketStickerRecentCategorys(ctgDTOs);
                    RingMarkerStickerDAO.UpdateStickerCategoryDownloadStatus(ctgDTO.sCtId, ctgDTO.Downloaded);
                }

                ctgModel.Downloaded = false;
                ctgModel.IsDownloadRunning = false;
                ctgModel.DownloadPercentage = 0;
                ctgModel.OnPropertyChanged("DetailImage");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region EventHandler

        private void UCMoreStickersPopUpInComments_Unloaded(object sender, RoutedEventArgs e)
        {
            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
        }

        private void UCMoreStickersPopUpInComments_Loaded(object sender, RoutedEventArgs e)
        {
            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
        }

        private void myScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (_IS_LOADING == false && _NoMoreDataFound == false && e.VerticalOffset >= (myScrlViewer.ScrollableHeight - 100))
            {
                _IS_LOADING = true;
                this.LoadData();
            }
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)((Grid)sender).DataContext;
            IsDetailsView = true;
            this.SelectedCategoryModel = ctgModel;

            if (ctgModel.Downloaded == true)
            {
                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgModel.StickerCategoryID).FirstOrDefault();
                if (ctgDTO != null)
                {
                    if (ctgModel.ImageList.Count < ctgDTO.ImagesList.Count)
                    {
                        ctgDTO.ImagesList.ForEach(i =>
                        {
                            MarkertStickerImagesModel imgModel = ctgModel.ImageList.Where(P => P.ImageID == i.imId).FirstOrDefault();
                            if (imgModel == null)
                            {
                                imgModel = new MarkertStickerImagesModel(i);
                                ctgModel.ImageList.InvokeAdd(imgModel);
                            }
                        });
                    }
                }

                new RingMarketStickerService(SelectedCategoryModel.StickerCategoryID, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), 0, (isNointerNet) =>
                {
                    if (isNointerNet)
                    {
                        UIHelperMethods.ShowFailed(Models.Constants.NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                    }
                    return 0;
                });
            }

            if (ctgModel.IsNew && ctgModel.IsNewStickerSeen == false && StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Contains(ctgModel.StickerCategoryID))
            {
                ctgModel.IsNewStickerSeen = true;
                RingIDViewModel.Instance.NewStickerNotificationCounter = RingIDViewModel.Instance.NewStickerNotificationCounter - 1;

                List<MarketStickerCategoryDTO> ctgDTOList = new List<MarketStickerCategoryDTO>();
                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.IsNewStickerSeen = true;
                    ctgDTOList.Add(ctgDTO);
                    new InsertIntoRingMarketStickerTable(ctgDTOList);
                }
            }
        }

        private void BtnBackToMain_Click(object sender, RoutedEventArgs e)
        {
            IsDetailsView = false;
        }

        #endregion

        #region Property

        private ObservableCollection<MarketStickerCategoryModel> _StickerNonDownloadedCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> StickerNonDownloadedCategoryList
        {
            get
            {
                return _StickerNonDownloadedCategoryList;
            }
            set
            {
                _StickerNonDownloadedCategoryList = value;
                this.OnPropertyChanged("StickerNonDownloadedCategoryList");
            }
        }

        private MarketStickerCategoryModel _SelectedCategoryModel;
        public MarketStickerCategoryModel SelectedCategoryModel
        {
            get { return _SelectedCategoryModel; }
            set
            {
                _SelectedCategoryModel = value;
                OnPropertyChanged("SelectedCategoryModel");
            }
        }

        private bool _IsDetailsView = false;
        public bool IsDetailsView
        {
            get { return _IsDetailsView; }
            set
            {
                if (value == _IsDetailsView) return;
                _IsDetailsView = value;
                myScrlViewer.ScrollToVerticalOffset(0);
                OnPropertyChanged("IsDetailsView");
            }
        }

        private bool _IsContextMenuOpening;
        public bool IsContextMenuOpening
        {
            get { return _IsContextMenuOpening; }
            set
            {
                _IsContextMenuOpening = value;
                OnPropertyChanged("IsContextMenuOpening");
            }
        }

        private bool _IsNoMoreStickersToDownload;
        public bool IsNoMoreStickersToDownload
        {
            get { return _IsNoMoreStickersToDownload; }
            set
            {
                _IsNoMoreStickersToDownload = value;
                OnPropertyChanged("IsNoMoreStickersToDownload");
            }
        }

        #endregion

        #region "Icommands"

        private ICommand _DownloadCommand;
        public ICommand DownloadCommand
        {
            get
            {
                if (_DownloadCommand == null)
                {
                    _DownloadCommand = new RelayCommand(param => OnDownloadCommand(param));
                }
                return _DownloadCommand;
            }
        }

        private ICommand _DetailsCommand;
        public ICommand DetailsCommand
        {
            get
            {
                if (_DetailsCommand == null)
                {
                    _DetailsCommand = new RelayCommand(param => OnDetailsViewCommand(param));
                }
                return _DetailsCommand;
            }
        }

        private ICommand _BackToPrevoiousUI;
        public ICommand BackToPrevoiousUI
        {
            get
            {
                if (_BackToPrevoiousUI == null)
                {
                    _BackToPrevoiousUI = new RelayCommand(param => OnBackToPreviousUI());
                }
                return _BackToPrevoiousUI;
            }
        }

        ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(param => OnCloseCommand(param));
                }
                return _CloseCommand;
            }
        }

        private void OnCloseCommand(object param)
        {
            IsDetailsView = false;
            _StickerNonDownloadedCategoryList.Clear();
            GC.SuppressFinalize(StickerNonDownloadedCategoryList);
            GC.Collect();
            base.Hide();
        }

        #endregion


        #region EventHandler

        private void MenuSendviaChat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)((MenuItem)sender).DataContext;
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetStickerImage(imgModel);
                OnCloseCommand(null);
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCMoreStickersPopUpInComments).Name).Error(ex.Message + " " + ex.StackTrace);
            }
        }

        private void Image_Button_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            Button button = (Button)sender;
            MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)(button.DataContext);
            imgModel.SingleStickerImageFocusable = true;
        }

        private void Image_Button_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            Button button = (Button)sender;
            MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)(button.DataContext);
            imgModel.SingleStickerImageFocusable = false;
        }

        private void bdrContextMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            IsContextMenuOpening = true;
        }

        private void bdrContextMenu_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            IsContextMenuOpening = false;
        }

        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

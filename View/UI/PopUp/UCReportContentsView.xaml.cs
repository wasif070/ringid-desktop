﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.Utility.Auth;
using View.BindingModels;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCReportContentsView.xaml
    /// </summary>
    public partial class UCReportContentsView : PopUpBaseControl, INotifyPropertyChanged
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(UCReportContentsView).Name);
        private Guid _spamId;
        private int spamType;
        private long spamUserTableId;
        private int _selectedSpamReasonId;
        private Func<int> _Oncomplete;

        #region"Properties"

        private ObservableDictionary<int, string> _ReasonList = new ObservableDictionary<int, string>();
        public ObservableDictionary<int, string> ReasonList
        {
            get
            {
                return _ReasonList;
            }
            set
            {
                _ReasonList = value;
                this.OnPropertyChanged("ReasonList");
            }
        }

        public int selectedSpamReasonId
        {
            get { return _selectedSpamReasonId; }
            set
            {
                if (value == _selectedSpamReasonId)
                    return;
                _selectedSpamReasonId = value;
                this.OnPropertyChanged("selectedSpamReasonId");
            }
        }

        #endregion"Properties"

        #region "Constructor"
        public UCReportContentsView(Grid motherGrid, Func<int> onComplete = null)
        {
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, ViewConstants.POP_UP_BG_BORDER2, 1.0);
            SetParent(motherGrid);
            this.DataContext = this;
            this._Oncomplete = onComplete;
        }
        #endregion

        #region "Public Method"
        public void SetSpamIdAndSpamType(Guid spamId, int spam_type, long utId)
        {
            this._spamId = spamId;
            this.spamType = spam_type;
            this.spamUserTableId = utId;
        }

        public void ShowReportContents()
        {
            try
            {
                ReasonList = RingIDViewModel.Instance.GetSpamReasonListByType(spamType);
            }
            catch (Exception e)
            {
                log.Error(e.StackTrace + "==>" + e.Message);
            }
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Icommands"

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null) _CloseCommand = new RelayCommand(param => OnCloseCommand());
                return _CloseCommand;
            }
        }
        private void OnCloseCommand()
        {
            ReasonList = null;
            this.selectedSpamReasonId = 0;
            base.Hide();
            if (_Oncomplete != null)
            {
                _Oncomplete();
                if (UCAlbumContentView.Instance != null)
                    UCAlbumContentView.Instance.OnLoadedControl();
            }
        }

        private ICommand _ReportBtnClicked;
        public ICommand ReportBtnClicked
        {
            get
            {
                if (_ReportBtnClicked == null) _ReportBtnClicked = new RelayCommand(param => OnReportBtnClicked());
                return _ReportBtnClicked;
            }
        }
        private void OnReportBtnClicked()
        {
            if (selectedSpamReasonId > 0 && (this._spamId != Guid.Empty || this.spamUserTableId > 0))
            {
                new Thread(() =>
                {
                    bool isSucc = false;
                    string msg = string.Empty;
                    if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                    {
                        try
                        {
                            JObject pakToSend = new JObject();
                            String pakId = SendToServer.GetRanDomPacketID();
                            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                            pakToSend[JsonKeys.PacketId] = pakId;
                            pakToSend[JsonKeys.SpamType] = this.spamType;
                            if (_spamId != Guid.Empty)
                                pakToSend[JsonKeys.SpamId] = this._spamId;
                            else if (spamUserTableId > 0)
                                pakToSend[JsonKeys.SpamId] = this.spamUserTableId;
                            pakToSend[JsonKeys.SpamReasonId] = this.selectedSpamReasonId;

                            spamUserTableId = 0;
                            this._spamId = Guid.Empty;
                            //this.spamType = this.selectedSpamReasonId = 0;

                            new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_SPAM_REPORT, AppConstants.REQUEST_TYPE_UPDATE).Run(out isSucc, out msg);
                            Application.Current.Dispatcher.Invoke(delegate
                            {
                                if (!string.IsNullOrEmpty(msg))
                                {
                                    if (spamType == 4)
                                        UIHelperMethods.ShowInformation(msg, NotificationMessages.REPORT_MEDIA_MESSAGE_TITLE);
                                    else if (spamType == 2)
                                        UIHelperMethods.ShowInformation(msg, NotificationMessages.REPORT_FEED_MESSAGE_TITLE);
                                }
                                else
                                {
                                    if (isSucc)
                                        UIHelperMethods.ShowInformation(NotificationMessages.REPORT_SUCCESSFUL_MESSAGE, NotificationMessages.REPORT_SUCCESSFUL_MESSAGE_TITLE);
                                    else
                                        UIHelperMethods.ShowInformation(NotificationMessages.REPORT_FAILED_MESSAGE, NotificationMessages.REPORT_FAILED_MESSAGE_TITILE);
                                }
                                this.spamType = this.selectedSpamReasonId = 0;
                            });                          
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.Message + "\n" + ex.StackTrace);
                        }
                    }
                }).Start();

                this.OnCloseCommand();
            }
            else
            {
                if (spamType == 4)
                    UIHelperMethods.ShowWarning(NotificationMessages.REPORT_TYPE_SELECT_ALERT_MESSAGE, NotificationMessages.REPORT_MEDIA_MESSAGE_TITLE);
                else if (spamType == 2)
                    UIHelperMethods.ShowWarning(NotificationMessages.REPORT_TYPE_SELECT_ALERT_MESSAGE, NotificationMessages.REPORT_FEED_MESSAGE_TITLE);
            }
        }
        #endregion "Icommands"

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButton rdBtn = (RadioButton)sender;
            this.selectedSpamReasonId = ((KeyValuePair<int, string>)rdBtn.DataContext).Key;
        }
    }
}

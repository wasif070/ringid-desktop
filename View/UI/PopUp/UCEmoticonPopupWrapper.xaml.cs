﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCEmoticonPopupWrapper.xaml
    /// </summary>
    public partial class UCEmoticonPopupWrapper : UserControl
    {
        public UCEmoticonPopupWrapper()
        {
            InitializeComponent();
          
        }

        public void Show(UIElement target, int type, Func<string, int> onClosing)
        {
            if (this.Content == null)
            {
                this.Content = new UCEmoticonPopup();
            }
            ((UCEmoticonPopup)this.Content).Show(target, type, onClosing);
        }
    }
}

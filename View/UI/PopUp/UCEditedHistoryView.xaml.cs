﻿using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Feed;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCEditedHistoryView.xaml
    /// </summary>
    public partial class UCEditedHistoryView : PopUpBaseControl, INotifyPropertyChanged
    {
        public FeedModel FeedModel;
        public CommentModel CommentModel;

        public UCEditedHistoryView(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private ObservableCollection<EditHistoryModel> _EditHistories = new ObservableCollection<EditHistoryModel>();
        public ObservableCollection<EditHistoryModel> EditHistories
        {
            get
            {
                return _EditHistories;
            }
            set
            {
                if (_EditHistories == value)
                {
                    return;
                }
                _EditHistories = value;
                OnPropertyChanged("EditHistories");
            }
        }
        #endregion

        public void HideEditHistoryView()
        {
            Visibility = Visibility.Hidden;
            reloadBtn.Visibility = Visibility.Collapsed;
            noContent.Visibility = Visibility.Collapsed;
            if (GIFCtrl.IsRunning()) GIFCtrl.StopAnimate();
            //_parent.IsEnabled = true;
        }

        private void RequestToGetResult()
        {
            itemContainer.Visibility = Visibility.Collapsed;
            if (FeedModel != null)
            {
                GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                MainSwitcher.ThreadManager().ListEditHistories.OnSuccess += (response) =>
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (GIFCtrl.IsRunning()) GIFCtrl.StopAnimate();
                        switch (response)
                        {
                            case SettingsConstants.RESPONSE_SUCCESS:
                                itemContainer.Visibility = Visibility.Visible;
                                break;
                            case SettingsConstants.RESPONSE_NOTSUCCESS:
                                noContent.Visibility = Visibility.Visible;
                                break;
                            default: //no response
                                reloadBtn.Visibility = Visibility.Visible;
                                break;
                        }
                    });
                };
                MainSwitcher.ThreadManager().ListEditHistories.StartThread(FeedModel.NewsfeedId, Guid.Empty, 0);
            }
            else if (CommentModel != null)
            {
                GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                MainSwitcher.ThreadManager().ListEditHistories.OnSuccess += (response) =>
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (GIFCtrl.IsRunning()) GIFCtrl.StopAnimate();
                        switch (response)
                        {
                            case SettingsConstants.RESPONSE_SUCCESS:
                                itemContainer.Visibility = Visibility.Visible;
                                break;
                            case SettingsConstants.RESPONSE_NOTSUCCESS:
                                noContent.Visibility = Visibility.Visible;
                                break;
                            default: //no response
                                reloadBtn.Visibility = Visibility.Visible;
                                break;
                        }
                    });
                };
                MainSwitcher.ThreadManager().ListEditHistories.StartThread(CommentModel.NewsfeedId, CommentModel.CommentId, commentType);
            }
        }

        int commentType;

        public void ShowEditHistoryView(Object model)
        {
            if (model is FeedModel)
            {
                if (this.FeedModel != null && this.FeedModel.NewsfeedId == ((FeedModel)model).NewsfeedId && EditHistories.Count > 0)
                {
                }
                else
                {
                    this.FeedModel = (FeedModel)model;
                    this.CommentModel = null;
                    EditHistories.Clear();

                    RequestToGetResult();
                }
            }
            else if (model is CommentModel)
            {
                if (this.CommentModel != null && this.CommentModel.CommentId == ((CommentModel)model).CommentId && EditHistories.Count > 0)
                {
                }
                else
                {
                    this.CommentModel = (CommentModel)model;
                    commentType = SettingsConstants.TYPE_FEED_COMMENT;
                    if (CommentModel.ImageId != Guid.Empty) commentType = SettingsConstants.TYPE_IMAGE_COMMENT;
                    else if (CommentModel.ContentId != Guid.Empty) commentType = SettingsConstants.TYPE_MEDIA_COMMENT;
                    this.FeedModel = null;
                    EditHistories.Clear();

                    RequestToGetResult();
                }
            }
            Visibility = Visibility.Visible;
        }

        #region Command

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        ICommand _blackSpaceClick;
        public ICommand BlackSpaceClick
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }
        private void OnClickBlackSpace(object param)
        {
            Hide();
            HideEditHistoryView();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {

        }
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            reloadBtn.Visibility = Visibility.Collapsed;
            RequestToGetResult();
        }
        #endregion

        private void txtBL_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Hide();
            HideEditHistoryView();

            TextBlock tb = (TextBlock)sender;
            EditHistoryModel model = (EditHistoryModel)tb.DataContext;

            if (model.ShortInfoModel != null && model.ShortInfoModel.UserTableID > 0)
            {
                long utId = model.ShortInfoModel.UserTableID;
                if (!HelperMethods.IsRingIDOfficialAccount(utId))
                {
                    if (utId == DefaultSettings.LOGIN_TABLE_ID)
                        RingIDViewModel.Instance.OnMyProfileClicked(utId);
                    else
                        RingIDViewModel.Instance.OnFriendProfileButtonClicked(utId);
                }
            }
        }
    }
}

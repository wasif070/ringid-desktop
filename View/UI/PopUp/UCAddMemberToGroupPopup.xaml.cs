﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using log4net;
using Models.Entity;
using View.BindingModels;
using View.Utility;
using View.Utility.FriendList;
using View.ViewModel;
using Models.Constants;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAddMemberToGroupPopup.xaml
    /// </summary>
    public partial class UCAddMemberToGroupPopup : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCAddMemberToGroupPopup).Name);

        private ObservableCollection<UserBasicInfoModel> _FriendList = new ObservableCollection<UserBasicInfoModel>();
        ObservableCollection<UserBasicInfoModel> _TempList;
        public ObservableCollection<UserBasicInfoModel> SelectedFriendList = new ObservableCollection<UserBasicInfoModel>();
        private int _TotalSelectedFriend = 0;
        private ICommand _AddMemberInGroupCommand;
        private Func<List<UserBasicInfoModel>, int> _OnComplete = null;

        private const int EMO_POPUP_HEIGHT = 464;
        private int _ShowUpArrowPopup = 1;//0 - Default, 1 - UP, 2 - Down

        public UCAddMemberToGroupPopup()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Event Handler

        private void popupAddMember_Closed(object sender, EventArgs e)
        {
            RingIDViewModel.Instance.GroupButtonSelection = false;
            Dispose();
        }

        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SearchText = SearchTermTextBox.Text.ToLower();
        }

        #endregion Event Handler

        #region Utility Methods

        public void Show(UIElement target, List<GroupMemberInfoModel> memberList, Func<List<UserBasicInfoModel>, int> onComplete = null)
        {
            try
            {
                TotalSelectedFriend = 0;
                SearchTermTextBox.TextChanged += SearchTermTextBox_TextChanged;
                if (popupAddMember.IsOpen)
                {
                    popupAddMember.IsOpen = false;
                }
                else
                {
                    popupAddMember.PlacementTarget = target;

                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 35;
                    if (buttonPosition < EMO_POPUP_HEIGHT && bottomSpace >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 1;
                        popupAddMember.HorizontalOffset = -239;
                        popupAddMember.VerticalOffset = -3;
                    }
                    else if (buttonPosition >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 2;
                        popupAddMember.HorizontalOffset = -239;
                        popupAddMember.VerticalOffset = 5;
                    }
                    else
                    {
                        ShowUpArrowPopup = 0;
                        popupAddMember.HorizontalOffset = -239;
                        popupAddMember.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                    }
                    _OnComplete = onComplete;

                    FriendList.Clear();
                    SelectedFriendList.Clear();
                    popupAddMember.PlacementTarget = target;
                    popupAddMember.IsOpen = true;
                    LoadFriendList(memberList);

                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Show() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void LoadFriendList(List<GroupMemberInfoModel> memberList)
        {
            List<UserBasicInfoModel> allModels = FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values.ToList();
            this._TempList = new ObservableCollection<UserBasicInfoModel>(
                allModels.Where(P => P.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && !memberList.Any(Q => Q.UserTableID == P.ShortInfoModel.UserTableID && Q.MemberAccessType != ChatConstants.MEMBER_TYPE_NOT_MEMBER)).OrderBy(P => P.ShortInfoModel.FullName).ToList()
            );
            FriendList = this._TempList;
        }

        private void ShowSearchList()
        {
            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                List<UserBasicInfoModel> modelList = _TempList.Where(P => (P.ShortInfoModel.FullName.ToLower().Contains(SearchText) || P.ShortInfoModel.UserIdentity.ToString().Contains(SearchText))).ToList();

                ObservableCollection<UserBasicInfoModel> tempList1 = new ObservableCollection<UserBasicInfoModel>();
                foreach (UserBasicInfoModel model in modelList)
                {
                    tempList1.Add(model);
                }
                FriendList = tempList1;
            }
            else
            {
                FriendList = _TempList;
            }
        }

        private void OnAddMember(object param)
        {
            if (_OnComplete != null)
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    _OnComplete(SelectedFriendList.ToList());
                });
            }
            popupAddMember.IsOpen = false;
        }

        public void Dispose()
        {
            List<UserBasicInfoModel> deletedList = SelectedFriendList.ToList();
            foreach (UserBasicInfoModel model in deletedList)
            {
                model.VisibilityModel.IsAddGroupMember = false;
            }
            TotalSelectedFriend = 0;
            SelectedFriendList.Clear();
            _TempList.Clear();
            FriendList.Clear();
            TotalSelectedFriend = 0;

            SearchTermTextBox.Text = string.Empty;
            SearchText = string.Empty;
            SearchTermTextBox.TextChanged -= SearchTermTextBox_TextChanged;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility methods

        #region Property

        public ICommand AddMemberInGroupCommand
        {
            get
            {
                if (_AddMemberInGroupCommand == null)
                {
                    _AddMemberInGroupCommand = new RelayCommand((param) => OnAddMember(param));
                }
                return _AddMemberInGroupCommand;
            }
        }

        public ObservableCollection<UserBasicInfoModel> FriendList
        {
            get
            {
                return _FriendList;
            }
            set
            {
                _FriendList = value;
                this.OnPropertyChanged("FriendList");
            }
        }

        public int TotalSelectedFriend
        {
            get
            {
                return _TotalSelectedFriend;
            }
            set
            {
                _TotalSelectedFriend = value;
                this.OnPropertyChanged("TotalSelectedFriend");
            }
        }

        public int ShowUpArrowPopup
        {
            get { return _ShowUpArrowPopup; }
            set
            {
                if (_ShowUpArrowPopup == value)
                {
                    return;
                }
                _ShowUpArrowPopup = value;
                this.OnPropertyChanged("ShowUpArrowPopup");
            }
        }

        private string _SearchText = string.Empty;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                _SearchText = value;
                ShowSearchList();
            }
        }

        #endregion Property

    }
}

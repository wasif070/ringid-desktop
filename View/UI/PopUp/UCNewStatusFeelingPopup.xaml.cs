﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.Feed;
using View.Utility;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCNewStatusFeelingPopup.xaml
    /// </summary>
    public partial class UCNewStatusFeelingPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        private BackgroundWorker bgworker;

        public UCNewStatusFeelingPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            SetParent(motherGrid);
        }

        #region Property

        private int _ShowUpArrowPopup = 1;//0 - Default, 1 - UP, 2 - Down
        public int ShowUpArrowPopup
        {
            get { return _ShowUpArrowPopup; }
            set
            {
                if (_ShowUpArrowPopup == value)
                {
                    return;
                }
                _ShowUpArrowPopup = value;
                this.OnPropertyChanged("ShowUpArrowPopup");
            }
        }
        #endregion Property

        #region Utility Methods

        private const int EMO_POPUP_HEIGHT = 464;
        private NewStatusViewModel newStatusViewModel;
        public void ShowFeelingPopup(UIElement target, NewStatusViewModel newStatusViewModel)
        {

            this.newStatusViewModel = newStatusViewModel;
            this.ucShareSingleFeedView = null;
            if (popupFeeling.IsOpen == false)
            {
                scvFriendList.ScrollToVerticalOffset(0);
                popupFeeling.PlacementTarget = target;

                int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;
                if (buttonPosition < EMO_POPUP_HEIGHT && bottomSpace >= EMO_POPUP_HEIGHT)
                {
                    ShowUpArrowPopup = 1;
                    popupFeeling.HorizontalOffset = -239;
                    popupFeeling.VerticalOffset = -3;
                }
                else if (buttonPosition >= EMO_POPUP_HEIGHT)
                {
                    ShowUpArrowPopup = 2;
                    popupFeeling.HorizontalOffset = -239;
                    popupFeeling.VerticalOffset = 5;
                }
                else
                {
                    ShowUpArrowPopup = 0;
                    popupFeeling.HorizontalOffset = -239;
                    popupFeeling.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                }

                popupFeeling.IsOpen = true;
                //if (!IsFriendListLoaded)
                //    LoadFriendListIntoPopup();
            }
            else
            {
                popupFeeling.IsOpen = false;
            }

        }
        private UCShareSingleFeedView ucShareSingleFeedView;
        public void ShowFeelingPopup(UIElement target, UCShareSingleFeedView UCShareSingleFeedViewInstance)
        {
            this.ucShareSingleFeedView = UCShareSingleFeedViewInstance;
            this.newStatusViewModel = null;
            if (popupFeeling.IsOpen == false)
            {
                scvFriendList.ScrollToVerticalOffset(0);
                popupFeeling.PlacementTarget = target;

                int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;
                if (buttonPosition < EMO_POPUP_HEIGHT && bottomSpace >= EMO_POPUP_HEIGHT)
                {
                    ShowUpArrowPopup = 1;
                    popupFeeling.HorizontalOffset = -239;
                    popupFeeling.VerticalOffset = -3;
                }
                else if (buttonPosition >= EMO_POPUP_HEIGHT)
                {
                    ShowUpArrowPopup = 2;
                    popupFeeling.HorizontalOffset = -239;
                    popupFeeling.VerticalOffset = 5;
                }
                else
                {
                    ShowUpArrowPopup = 0;
                    popupFeeling.HorizontalOffset = -239;
                    popupFeeling.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                }


                popupFeeling.IsOpen = true;
                //if (!IsFriendListLoaded)
                //    LoadFriendListIntoPopup();
            }
            else
            {
                popupFeeling.IsOpen = false;
            }

        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility methods

        #region Event Handler

        void popupFeeling_Closed(object sender, EventArgs e)
        {

            this.popupFeeling.Closed -= popupFeeling_Closed;
            Dispose();
            Hide();

        }

        public void Dispose()
        {
            try
            {
                if (bgworker != null && bgworker.IsBusy)
                {
                    bgworker.CancelAsync();
                }
                else
                {
                    textBoxSearch.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {

                //System.Diagnostics.Debug.WriteLine("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                log4net.LogManager.GetLogger(typeof(UCNewStatusFeelingPopup).Name).Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Border bd = (Border)sender;
            DoingModel model = (DoingModel)bd.DataContext;
            if (newStatusViewModel != null)
            {
                newStatusViewModel.SelectedDoingModel = model;
                newStatusViewModel.SelectedDoingId = model.ID;
            }
            else if (ucShareSingleFeedView != null)
            {
                ucShareSingleFeedView.SelectedDoingModel = model;
                ucShareSingleFeedView.SelectedDoingId = model.ID;
            }
            popupFeeling.IsOpen = false;
        }

        #endregion Event Handler

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.popupFeeling.Closed += popupFeeling_Closed;
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupFeeling.IsOpen = false;
        }

        private ICommand txtChangeCommand;
        public ICommand TxtChangeCommand
        {
            get
            {
                if (txtChangeCommand == null) txtChangeCommand = new RelayCommand(param => OnTxtChangeCommand());
                return txtChangeCommand;
            }
        }

        private void OnTxtChangeCommand()
        {
            string searchText = textBoxSearch.Text.Trim();
            int c = 0;
            if (bgworker == null)
            {
                bgworker = new BackgroundWorker();
                bgworker.WorkerReportsProgress = true;
                bgworker.WorkerSupportsCancellation = true;
                bgworker.DoWork += (s, ee) =>
                {
                    string sText = (string)ee.Argument;
                    if (RingIDViewModel.Instance.DoingListForNewStatus != null && RingIDViewModel.Instance.DoingListForNewStatus.Count > 0)
                    {
                        if (sText.Length == 0) foreach (var item in RingIDViewModel.Instance.DoingListForNewStatus) item.VisibilityInSearch = Visibility.Visible;
                        else
                        {
                            foreach (var item in RingIDViewModel.Instance.DoingListForNewStatus)
                            {
                                if (item.Name.IndexOf(sText, StringComparison.OrdinalIgnoreCase) >= 0)
                                {
                                    item.VisibilityInSearch = Visibility.Visible;
                                    c++;
                                }
                                else item.VisibilityInSearch = Visibility.Collapsed;
                                // item.VisibilityInSearch = (item.Name.IndexOf(SearchText, StringComparison.OrdinalIgnoreCase) >= 0) ? Visibility.Visible : Visibility.Collapsed;
                            }
#if FEED_LOG
                            System.Diagnostics.Debug.WriteLine(sText + " ==> " + c);
#endif
                        }
                    }

                    ee.Result = null;
                };
                bgworker.RunWorkerCompleted += (s, ee) =>
                {
                    if (ee.Result == null) { }
                };
            }
            if (!bgworker.IsBusy)
                bgworker.RunWorkerAsync(searchText);
        }
        #endregion
    }
}

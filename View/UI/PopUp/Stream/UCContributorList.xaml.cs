﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.Stream;
using View.Utility;
using View.Utility.Stream;

namespace View.UI.PopUp.Stream
{
    /// <summary>
    /// Interaction logic for UCContributorList.xaml
    /// </summary>
    public partial class UCContributorList : UserControl, INotifyPropertyChanged
    {
        private Grid _Parent;
        public UCContributorList()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Utility

        public void ShowProfileInfo(Grid parent)
        {
            parent.Children.Add(this);
            this._Parent = parent;
        }

        private void OnLoadedControl()
        {
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
        }

        private void OnCloseCommand()
        {
            _Parent.Children.Remove(this);
        }
        

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }        

        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null) _CloseCommand = new RelayCommand(param => OnCloseCommand());
                return _CloseCommand;
            }
        }
    }
}

<<<<<<< HEAD
﻿using log4net;
using MediaInfoDotNet;
using MediaToolkit.Model;
using Microsoft.Win32;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.Channel;
using View.Utility;
using View.Utility.Channel;
using View.Utility.GIF;
using View.Utility.WPFMessageBox;

namespace View.UI.PopUp.Stream
{
    /// <summary>
    /// Interaction logic for UCUploadMediaToChannelPopUp.xaml
    /// </summary>
    public partial class UCUploadMediaToChannelPopUp : PopUpBaseControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCUploadMediaToChannelPopUp).Name);
        public static UCUploadMediaToChannelPopUp Instance;
        public delegate void OnCompleteHandler(List<ChannelMediaDTO> mediaList);
        public event OnCompleteHandler _OnCompleteEvent;
        private Guid _ChannelId;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<FeedVideoUploaderModel> _ChannelVideoUploadList;//= new ObservableCollection<FeedVideoUploaderModel>();
        private ICommand _LoadedControl;
        private ICommand _BlackSpaceClick;
        private ICommand _UploadClickCommand;
        private ICommand _EmptyCommand;
        private bool _IsUploadButtonEnabled = true;
        private string TEMP_MEDIA_UPLOAD_FILE_NAME = "channel_video_upload_";

        public UCUploadMediaToChannelPopUp(Grid motherGrid, Guid channelId)
        {
            InitializeComponent();
            Instance = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 0.0);//background Color Transparent Black
            SetParent(motherGrid);
            this._ChannelId = channelId;
        }

        #region Utility Methods

        public void InilializeViewer()
        {
            this.DataContext = this;
            itcntrlChannelMediaUpload.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelVideoUploadList") });
        }

        public void Dispose()
        {
            if (IsUploadButtonEnabled)
            {
                this.DataContext = null;
                itcntrlChannelMediaUpload.ClearValue(ItemsControl.ItemsSourceProperty);
                itcntrlChannelMediaUpload.ItemsSource = null;
                ChannelVideoUploadList.Clear();
                base.Hide();
            }
            else
            {
                this.Visibility = Visibility.Collapsed;
            }
        }

        public void ShowPopup(int channelType)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "All supported Video files|*.mp4|MP4 files(*.mp4)|*.mp4";
            if (openFileDialog.ShowDialog() == true)
            {
                if (_ChannelVideoUploadList == null)
                {
                    ChannelVideoUploadList = new ObservableCollection<FeedVideoUploaderModel>();
                    base.Show();
                }
                string tempFilePath = string.Empty;
                if (openFileDialog.FileNames.Length <= 5) // When 5 video select with cntrl--> openFileDialog.FileNames.Length <= 5
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaInfoDotNet.MediaFile mf = new MediaInfoDotNet.MediaFile(filename);
                            if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit2GB)
                            {
                                continue;
                            }
                            if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264"))
                            {
                                continue;
                            }
                            FeedVideoUploaderModel model = new FeedVideoUploaderModel();
                            model.FileSize = mf.size;
                            model.FilePath = filename;
                            model.VideoTitle = System.IO.Path.GetFileNameWithoutExtension(filename);
                            //model.VideoDuration = (long)(mf.duration / 1000);
                            model.VideoDuration = (long)(mf.duration);
                            if (model.VideoDuration == 0)
                            {
                                model.VideoDuration = HelperMethods.MediaPlayerDuration(filename);
                            }
                            tempFilePath = RingIDSettings.TEMP_CAM_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + TEMP_MEDIA_UPLOAD_FILE_NAME + System.IO.Path.GetFileNameWithoutExtension(filename) + ".jpg";
                            double offset = model.VideoDuration > 5 ? 5 : -1;

                            try
                            {
                                MediaToolkit.MediaService.GetThumbnail(RingIDSettings.TEMP_MEDIA_TOOLKIT, filename, tempFilePath, offset);
                                BitmapImage bmpImage = ImageUtility.GetBitmapImageOfDynamicResource(tempFilePath);
                                model.VideoThumbnail = bmpImage;
                                model.ThumbImageWidth = bmpImage != null ? (int)model.VideoThumbnail.Width : 0;
                                model.ThumbImageHeight = bmpImage != null ? (int)model.VideoThumbnail.Height : 0;
                                bmpImage = null;

                                int width = 0, height = 0;
                                Metadata mediaData = MediaToolkit.MediaService.GetMetadata(RingIDSettings.TEMP_MEDIA_TOOLKIT, filename);
                                if (channelType == SettingsConstants.MEDIA_TYPE_VIDEO)
                                {

                                    if (mediaData.VideoData.FrameSize != null)
                                    {
                                        string frameSize = mediaData.VideoData.FrameSize;
                                        string[] values = frameSize.Split('x');
                                        width = Int32.Parse(values[0]);
                                        height = Int32.Parse(values[1]);
                                    }
                                }

                                int bitRate = mediaData.AudioData.BitRateKbs * 1000;
                                int sampleRate = 0;
                                if (mediaData.AudioData.SampleRate != null)
                                {
                                    string[] values = mediaData.AudioData.SampleRate.Split(' ');
                                    sampleRate = values.Length > 1 ? values[1].ToLower().Equals("hz") ? Int32.Parse(values[0]) : values[1].ToLower().Equals("khz") ? Int32.Parse(values[0]) * 1000 : 0 : 0;
                                }

                                if ((width >= ChannelConstants.CHANNEL_VIDEO_UPLOAD_MINIMUM_RESOLUTION || height >= ChannelConstants.CHANNEL_VIDEO_UPLOAD_MINIMUM_RESOLUTION)
                                    && sampleRate >= ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_SAMPLE_RATE
                                    && bitRate >= ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE)
                                {
                                    ChannelVideoUploadList.Add(model);
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("ShowPopup" + ex.Message + ex.StackTrace);
                            }

                        }
                        catch (Exception ex)
                        {
                            log.Error("ShowPopup" + ex.Message + ex.StackTrace);
                        }
                        finally
                        {
                            try
                            {
                                if (!string.IsNullOrEmpty(tempFilePath) && File.Exists(tempFilePath))
                                {
                                    File.Delete(tempFilePath);
                                }
                            }
                            catch (Exception e)
                            {
                                log.Error("Failed to delete Uploaded media => " + e.Message + e.StackTrace);
                            }
                        }
                    }
                    if (ChannelVideoUploadList.Count == 0)
                    {
                        UIHelperMethods.ShowWarning(
                                       string.Format("All videos failed to fulfill minimum requirements of video resolution {0}, audio bit rate {1} kbps and sample rate {2}",
                                       ChannelConstants.CHANNEL_VIDEO_UPLOAD_MINIMUM_RESOLUTION,
                                       ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE / 1000,
                                       ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE), "Video upload"
                                       );
                        Dispose();
                        return;
                    }
                    else if (openFileDialog.FileNames.Length != ChannelVideoUploadList.Count)
                    {
                        UIHelperMethods.ShowWarning(
                                        string.Format("Some videos failed to fulfill minimum requirements of video resolution {0}, audio bit rate {1} kbps and sample rate {2}",
                                        ChannelConstants.CHANNEL_VIDEO_UPLOAD_MINIMUM_RESOLUTION,
                                        ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE / 1000,
                                        ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE), "Video upload"
                                        );
                        return;
                    }
                }
                else
                {
                    Dispose();
                    UIHelperMethods.ShowWarning("Can't upload more than 5 videos at a time !", "Video upload");
                }
            }
        }

        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private void OnUploadClickCommand()
        {
            if (ChannelVideoUploadList.Count > 0)
            {
                bool isAllVideoHaveTitle = true;
                foreach (FeedVideoUploaderModel model in ChannelVideoUploadList)
                {
                    if (string.IsNullOrEmpty(model.VideoTitle))
                    {
                        isAllVideoHaveTitle = false;
                        break;
                    }
                }
                if (!isAllVideoHaveTitle) UIHelperMethods.ShowWarning("Please give title for Every Video!", "Media title");
                else UploadMedia();
            }
            //else ShowPopup();
        }

        private void OnEmptyCommand()
        {

        }

        private void UploadMedia()
        {
            try
            {
                BitmapImage _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                imgLoader.Source = _LoadingIcon;
                ImageBehavior.SetAnimatedSource(imgLoader, _LoadingIcon);
                IsUploadButtonEnabled = false;
                new VideoUploader(this, (status) =>
                {
                    if (status)
                    {
                        List<ChannelMediaDTO> mediaList = new List<ChannelMediaDTO>();
                        foreach (FeedVideoUploaderModel model in ChannelVideoUploadList)
                        {
                            ChannelMediaDTO channelMediaDTO = new ChannelMediaDTO();
                            channelMediaDTO.ChannelID = this._ChannelId;
                            channelMediaDTO.Title = model.VideoTitle;
                            channelMediaDTO.Artist = model.VideoArtist;
                            channelMediaDTO.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;
                            channelMediaDTO.ChannelType = SettingsConstants.MEDIA_TYPE_VIDEO;
                            channelMediaDTO.MediaUrl = model.StreamUrl;
                            channelMediaDTO.ThumbImageUrl = model.ThumbUrl;
                            channelMediaDTO.ThumbImageWidth = model.ThumbImageWidth;
                            channelMediaDTO.ThumbImageHeight = model.ThumbImageHeight;
                            channelMediaDTO.Duration = model.VideoDuration;
                            channelMediaDTO.MediaStatus = ChannelConstants.MEDIA_STATUS_UPLOADED;
                            channelMediaDTO.OwnerID = DefaultSettings.LOGIN_TABLE_ID;
                            mediaList.Add(channelMediaDTO);
                        }
                        new ThrdAddMediaToChannel(this._ChannelId, SettingsConstants.MEDIA_TYPE_VIDEO, mediaList, (isSucces, message) =>
                        {
                            if (isSucces)
                            {
                                HideLoader();
                                Application.Current.Dispatcher.BeginInvoke(delegate
                                {
                                    Dispose();
                                    if (_OnCompleteEvent != null)
                                    {
                                        this._OnCompleteEvent(mediaList);
                                    }
                                    UIHelperMethods.ShowMessageWithTimerFromNonThread(UCMyChannelMainPanel.MyChannelUploadsPanel._parentGrid, "Media added Successfully!", 1000);
                                    return;
                                });
                            }
                            else
                            {
                                HideLoader();
                                if (!string.IsNullOrEmpty(message)) UIHelperMethods.ShowFailed(message, "Add media");
                                else UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to add media", "Add media");
                            }
                            return 0;
                        }).Start();
                    }
                    else
                    {
                        HideLoader();
                    }
                    return 0;
                });
            }
            catch (Exception ex)
            {
                HideLoader();
                log.Error("UploadMedia =>" + ex.Message + ex.StackTrace);
            }
        }

        private void HideLoader()
        {
            IsUploadButtonEnabled = true;
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                ImageBehavior.SetAnimatedSource(imgLoader, null);
                imgLoader.Source = null;
            });
        }

        private bool isSelectedMediaFilesLessThan500MB(int mediaType)
        {
            long count = 0;
            if (mediaType == 2)
            {
                foreach (var item in ChannelVideoUploadList)
                {
                    count += new FileInfo(item.FilePath).Length;
                }
                return (count < DefaultSettings.MaxFileLimit500MBinBytes);
            }
            return false;
        }

        private void OnBlackSpaceClick()
        {
            Dispose();
        }

        private void OnCancelClickCommand()
        {
            Dispose();
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion


        #region Command

        public ICommand LoadedControl
        {
            get
            {
                if (_LoadedControl == null)
                {
                    _LoadedControl = new RelayCommand(param => OnLoadedControl());
                }
                return _LoadedControl;
            }

        }

        public ICommand BlackSpaceClick
        {
            get
            {
                if (_BlackSpaceClick == null)
                {
                    _BlackSpaceClick = new RelayCommand(param => OnBlackSpaceClick());
                }
                return _BlackSpaceClick;
            }

        }

        public ICommand UploadClickCommand
        {
            get
            {
                if (_UploadClickCommand == null)
                {
                    _UploadClickCommand = new RelayCommand(param => OnUploadClickCommand());
                }
                return _UploadClickCommand;
            }

        }
      
        public ICommand EmptyCommand
        {
            get
            {
                if (_EmptyCommand == null)
                {
                    _EmptyCommand = new RelayCommand(param => OnEmptyCommand());
                }
                return _EmptyCommand;
            }
        }
        

        #endregion

        #region Property


        public ObservableCollection<FeedVideoUploaderModel> ChannelVideoUploadList
        {
            get { return _ChannelVideoUploadList; }
            set
            {
                _ChannelVideoUploadList = value;
                this.OnPropertyChanged("ChannelVideoUploadList");
            }
        }

        public bool IsUploadButtonEnabled
        {
            get { return _IsUploadButtonEnabled; }
            set
            {
                if (value == _IsUploadButtonEnabled) return;
                _IsUploadButtonEnabled = value;
                this.OnPropertyChanged("IsUploadButtonEnabled");
            }
        }


        #endregion

    }
}
=======
﻿using log4net;
using MediaInfoDotNet;
using MediaToolkit.Model;
using Microsoft.Win32;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.Channel;
using View.Utility;
using View.Utility.Channel;
using View.Utility.GIF;
using View.Utility.WPFMessageBox;

namespace View.UI.PopUp.Stream
{
    /// <summary>
    /// Interaction logic for UCUploadMediaToChannelPopUp.xaml
    /// </summary>
    public partial class UCUploadMediaToChannelPopUp : PopUpBaseControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCUploadMediaToChannelPopUp).Name);
        public static UCUploadMediaToChannelPopUp Instance;
        public delegate void OnCompleteHandler(List<ChannelMediaDTO> mediaList);
        public event OnCompleteHandler _OnCompleteEvent;
        private Guid _ChannelId;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<FeedVideoUploaderModel> _ChannelVideoUploadList;//= new ObservableCollection<FeedVideoUploaderModel>();
        private ICommand _LoadedControl;
        private ICommand _BlackSpaceClick;
        private ICommand _UploadClickCommand;
        private ICommand _EmptyCommand;
        private bool _IsUploadButtonEnabled = true;
        private string TEMP_MEDIA_UPLOAD_FILE_NAME = "channel_video_upload_";

        public UCUploadMediaToChannelPopUp(Grid motherGrid, Guid channelId)
        {
            InitializeComponent();
            Instance = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 0.0);//background Color Transparent Black
            SetParent(motherGrid);
            this._ChannelId = channelId;
        }

        #region Utility Methods

        public void InilializeViewer()
        {
            this.DataContext = this;
            itcntrlChannelMediaUpload.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelVideoUploadList") });
        }

        public void Dispose()
        {
            if (IsUploadButtonEnabled)
            {
                this.DataContext = null;
                itcntrlChannelMediaUpload.ClearValue(ItemsControl.ItemsSourceProperty);
                itcntrlChannelMediaUpload.ItemsSource = null;
                ChannelVideoUploadList.Clear();
                base.Hide();
            }
            else
            {
                this.Visibility = Visibility.Collapsed;
            }
        }

        public void ShowPopup(int channelType)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "All supported Video files|*.mp4|MP4 files(*.mp4)|*.mp4";
            if (openFileDialog.ShowDialog() == true)
            {
                if (_ChannelVideoUploadList == null)
                {
                    ChannelVideoUploadList = new ObservableCollection<FeedVideoUploaderModel>();
                    base.Show();
                }
                string tempFilePath = string.Empty;
                if (openFileDialog.FileNames.Length <= 5) // When 5 video select with cntrl--> openFileDialog.FileNames.Length <= 5
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaInfoDotNet.MediaFile mf = new MediaInfoDotNet.MediaFile(filename);
                            if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit2GB)
                            {
                                continue;
                            }
                            if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264"))
                            {
                                continue;
                            }
                            FeedVideoUploaderModel model = new FeedVideoUploaderModel();
                            model.FileSize = mf.size;
                            model.FilePath = filename;
                            model.VideoTitle = System.IO.Path.GetFileNameWithoutExtension(filename);
                            //model.VideoDuration = (long)(mf.duration / 1000);
                            model.VideoDuration = (long)(mf.duration);
                            if (model.VideoDuration == 0)
                            {
                                model.VideoDuration = HelperMethods.MediaPlayerDuration(filename);
                            }
                            tempFilePath = RingIDSettings.TEMP_CAM_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + TEMP_MEDIA_UPLOAD_FILE_NAME + System.IO.Path.GetFileNameWithoutExtension(filename) + ".jpg";
                            double offset = model.VideoDuration > 10 ? 10 : (model.VideoDuration > 5 ? 5 : -1);

                            try
                            {
                                MediaToolkit.MediaService.GetThumbnail(RingIDSettings.TEMP_MEDIA_TOOLKIT, filename, tempFilePath, offset);
                                BitmapImage bmpImage = ImageUtility.GetBitmapImageOfDynamicResource(tempFilePath);
                                model.VideoThumbnail = bmpImage;
                                model.ThumbImageWidth = bmpImage != null ? (int)model.VideoThumbnail.Width : 0;
                                model.ThumbImageHeight = bmpImage != null ? (int)model.VideoThumbnail.Height : 0;
                                bmpImage = null;

                                int width = 0, height = 0;
                                Metadata mediaData = MediaToolkit.MediaService.GetMetadata(RingIDSettings.TEMP_MEDIA_TOOLKIT, filename);
                                if (channelType == SettingsConstants.MEDIA_TYPE_VIDEO)
                                {

                                    if (mediaData.VideoData.FrameSize != null)
                                    {
                                        string frameSize = mediaData.VideoData.FrameSize;
                                        string[] values = frameSize.Split('x');
                                        width = Int32.Parse(values[0]);
                                        height = Int32.Parse(values[1]);
                                    }
                                }

                                int bitRate = mediaData.AudioData.BitRateKbs * 1000;
                                int sampleRate = 0;
                                if (mediaData.AudioData.SampleRate != null)
                                {
                                    string[] values = mediaData.AudioData.SampleRate.Split(' ');
                                    sampleRate = values.Length > 1 ? values[1].ToLower().Equals("hz") ? Int32.Parse(values[0]) : values[1].ToLower().Equals("khz") ? Int32.Parse(values[0]) * 1000 : 0 : 0;
                                }

                                if ((width >= ChannelConstants.CHANNEL_VIDEO_UPLOAD_MINIMUM_RESOLUTION || height >= ChannelConstants.CHANNEL_VIDEO_UPLOAD_MINIMUM_RESOLUTION)
                                    && sampleRate >= ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_SAMPLE_RATE
                                    && bitRate >= ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE)
                                {
                                    ChannelVideoUploadList.Add(model);
                                }
                            }
                            catch (Exception ex)
                            {
                                log.Error("ShowPopup" + ex.Message + ex.StackTrace);
                            }

                        }
                        catch (Exception ex)
                        {
                            log.Error("ShowPopup" + ex.Message + ex.StackTrace);
                        }
                        finally
                        {
                            try
                            {
                                if (!string.IsNullOrEmpty(tempFilePath) && File.Exists(tempFilePath))
                                {
                                    File.Delete(tempFilePath);
                                }
                            }
                            catch (Exception e)
                            {
                                log.Error("Failed to delete Uploaded media => " + e.Message + e.StackTrace);
                            }
                        }
                    }
                    if (ChannelVideoUploadList.Count == 0)
                    {
                        UIHelperMethods.ShowWarning(
                                       string.Format("All videos failed to fulfill minimum requirements of video resolution {0}, audio bit rate {1} kbps and sample rate {2}",
                                       ChannelConstants.CHANNEL_VIDEO_UPLOAD_MINIMUM_RESOLUTION,
                                       ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE / 1000,
                                       ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE), "Video upload"
                                       );
                        Dispose();
                        return;
                    }
                    else if (openFileDialog.FileNames.Length != ChannelVideoUploadList.Count)
                    {
                        UIHelperMethods.ShowWarning(
                                        string.Format("Some videos failed to fulfill minimum requirements of video resolution {0}, audio bit rate {1} kbps and sample rate {2}",
                                        ChannelConstants.CHANNEL_VIDEO_UPLOAD_MINIMUM_RESOLUTION,
                                        ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE / 1000,
                                        ChannelConstants.CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE), "Video upload"
                                        );
                        return;
                    }
                }
                else
                {
                    Dispose();
                    UIHelperMethods.ShowWarning("Can't upload more than 5 videos at a time !", "Video upload");
                }
            }
        }

        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private void OnUploadClickCommand()
        {
            if (ChannelVideoUploadList.Count > 0)
            {
                bool isAllVideoHaveTitle = true;
                foreach (FeedVideoUploaderModel model in ChannelVideoUploadList)
                {
                    if (string.IsNullOrEmpty(model.VideoTitle))
                    {
                        isAllVideoHaveTitle = false;
                        break;
                    }
                }
                if (!isAllVideoHaveTitle) UIHelperMethods.ShowWarning("Please give title for Every Video!", "Media title");
                else UploadMedia();
            }
            //else ShowPopup();
        }

        private void OnEmptyCommand()
        {

        }

        private void UploadMedia()
        {
            try
            {
                BitmapImage _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                imgLoader.Source = _LoadingIcon;
                ImageBehavior.SetAnimatedSource(imgLoader, _LoadingIcon);
                IsUploadButtonEnabled = false;
                new VideoUploader(this, (status) =>
                {
                    if (status)
                    {
                        List<ChannelMediaDTO> mediaList = new List<ChannelMediaDTO>();
                        foreach (FeedVideoUploaderModel model in ChannelVideoUploadList)
                        {
                            ChannelMediaDTO channelMediaDTO = new ChannelMediaDTO();
                            channelMediaDTO.Title = model.VideoTitle;
                            channelMediaDTO.Artist = model.VideoArtist;
                            channelMediaDTO.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;
                            channelMediaDTO.MediaUrl = model.StreamUrl;
                            channelMediaDTO.ThumbImageUrl = model.ThumbUrl;
                            channelMediaDTO.ThumbImageWidth = model.ThumbImageWidth;
                            channelMediaDTO.ThumbImageHeight = model.ThumbImageHeight;
                            channelMediaDTO.Duration = model.VideoDuration;
                            channelMediaDTO.Description = model.VideoTitle;
                            mediaList.Add(channelMediaDTO);
                        }

                        new ThrdAddUploadedChannelMedia(mediaList, (isSucces, message) =>
                        {
                            if (isSucces)
                            {
                                HideLoader();
                                Application.Current.Dispatcher.BeginInvoke(delegate
                                {
                                    Dispose();
                                    if (_OnCompleteEvent != null)
                                    {
                                        this._OnCompleteEvent(mediaList);
                                    }
                                    //UIHelperMethods.ShowMessageWithTimerFromNonThread(UCMyChannelMainPanel.MyChannelUploadsPanel._parentGrid, "Media added Successfully!", 1000);
                                    return;
                                });
                            }
                            else
                            {
                                HideLoader();
                                if (!string.IsNullOrEmpty(message)) UIHelperMethods.ShowFailed(message, "Add media");
                                else UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to upload media", "Add media");
                            }
                            return 0;
                        }).Start();
                    }
                    else
                    {
                        HideLoader();
                    }
                    return 0;
                });
            }
            catch (Exception ex)
            {
                HideLoader();
                log.Error("UploadMedia =>" + ex.Message + ex.StackTrace);
            }
        }

        private void HideLoader()
        {
            IsUploadButtonEnabled = true;
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                ImageBehavior.SetAnimatedSource(imgLoader, null);
                imgLoader.Source = null;
            });
        }

        private bool isSelectedMediaFilesLessThan500MB(int mediaType)
        {
            long count = 0;
            if (mediaType == 2)
            {
                foreach (var item in ChannelVideoUploadList)
                {
                    count += new FileInfo(item.FilePath).Length;
                }
                return (count < DefaultSettings.MaxFileLimit500MBinBytes);
            }
            return false;
        }

        private void OnBlackSpaceClick()
        {
            Dispose();
        }

        private void OnCancelClickCommand()
        {
            Dispose();
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion


        #region Command

        public ICommand LoadedControl
        {
            get
            {
                if (_LoadedControl == null)
                {
                    _LoadedControl = new RelayCommand(param => OnLoadedControl());
                }
                return _LoadedControl;
            }

        }

        public ICommand BlackSpaceClick
        {
            get
            {
                if (_BlackSpaceClick == null)
                {
                    _BlackSpaceClick = new RelayCommand(param => OnBlackSpaceClick());
                }
                return _BlackSpaceClick;
            }

        }

        public ICommand UploadClickCommand
        {
            get
            {
                if (_UploadClickCommand == null)
                {
                    _UploadClickCommand = new RelayCommand(param => OnUploadClickCommand());
                }
                return _UploadClickCommand;
            }

        }
      
        public ICommand EmptyCommand
        {
            get
            {
                if (_EmptyCommand == null)
                {
                    _EmptyCommand = new RelayCommand(param => OnEmptyCommand());
                }
                return _EmptyCommand;
            }
        }
        

        #endregion

        #region Property


        public ObservableCollection<FeedVideoUploaderModel> ChannelVideoUploadList
        {
            get { return _ChannelVideoUploadList; }
            set
            {
                _ChannelVideoUploadList = value;
                this.OnPropertyChanged("ChannelVideoUploadList");
            }
        }

        public bool IsUploadButtonEnabled
        {
            get { return _IsUploadButtonEnabled; }
            set
            {
                if (value == _IsUploadButtonEnabled) return;
                _IsUploadButtonEnabled = value;
                this.OnPropertyChanged("IsUploadButtonEnabled");
            }
        }


        #endregion

    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

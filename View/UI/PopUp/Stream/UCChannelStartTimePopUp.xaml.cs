﻿using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;
using View.Utility.Chat.Service;
namespace View.UI.PopUp.Stream
{
    /// <summary>
    /// Interaction logic for UCChannelStartTimePopUp.xaml
    /// </summary>
    public partial class UCChannelStartTimePopUp : PopUpBaseControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ICommand _BlackSpaceClick;
        public ICommand _EmptyCommand;
        private ICommand _SaveCommand;
        public ICommand _LoadedControl;
        private Func<long, int> _Oncomplete;
        private ObservableCollection<HourSelection> _HoursList = new ObservableCollection<HourSelection>();
        private ObservableCollection<MinuteSelection> _MinutesList = new ObservableCollection<MinuteSelection>();
        //private ObservableCollection<string> _SeceondsList = new ObservableCollection<string>();
        View.UI.Channel.DateTimePicker.Controls.DateTimePicker dtPicker;

        public UCChannelStartTimePopUp(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
            LoadDaysHoursAndMinutes();
        }

        #region Utility Methods

        public void Dispose(long publishTime = 0)
        {
            if (_Oncomplete != null)
            {
                this._Oncomplete(publishTime);
            }
            base.Hide();
        }

        public void ShowPopup(Func<long, int> onComplete)
        {
            base.Show();
            this._Oncomplete = onComplete;
            //  bdrContentPanel.Child = host;
        }

        private void LoadDaysHoursAndMinutes()
        {
            for (int i = 0; i < 24; i++)
            {
                HourSelection hourSelection = new HourSelection();
                hourSelection.Hour = i < 10 ? "0" + i.ToString() : i.ToString();
                HoursList.Add(hourSelection);
            }

            for (int i = 0; i < 60; i++)
            {
                MinuteSelection minSelection = new MinuteSelection();
                minSelection.Minute = i < 10 ? "0" + i.ToString() : i.ToString();
                MinutesList.Add(minSelection);
                //SeceondsList.Add(ms);
            }
        }

        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private void OnBlackSpaceClick()
        {
            this.Dispose();
        }

        private void OnSaveCommand()
        {
            if (dtPicker != null)
            {
                DateTime dateAndTime = dtPicker.DateTimeSelected;
                DateTime date = dateAndTime.Date;
                HourSelection hours = (HourSelection)cmbHour.SelectedValue;
                DateTime modifiedHours = date.AddHours(Int32.Parse(hours.Hour));
                MinuteSelection mimutes = (MinuteSelection)cmbMin.SelectedValue;
                DateTime modifiedHourMin = modifiedHours.AddMinutes(Int32.Parse(mimutes.Minute));
                
                long publishingTime = ModelUtility.CurrentTimeMillisLocal(modifiedHourMin);
                long mimRequiredTime = ModelUtility.CurrentTimeMillisLocal() + ChannelConstants.CHANNEL_MEDIA_DURATION_OFFSET_10_MIN;

                DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime t = epoch.AddMilliseconds(mimRequiredTime);
                Debug.WriteLine(publishingTime + ", " + mimRequiredTime);

                if (publishingTime < mimRequiredTime)
                {
                    UIHelperMethods.ShowInformation("Channel starting time must 10 minutes higher than current time!", "Channel");
                }
                else
                {
                    long timeDifference = publishingTime - ModelUtility.CurrentTimeMillisLocal();
                    Dispose(timeDifference);
                }
            }
        }

        private void OnEmptyCommand()
        {

        }

        public ObservableCollection<HourSelection> HoursList
        {
            get
            {
                return _HoursList;
            }
            set
            {
                _HoursList = value;
                this.OnPropertyChanged("HoursList");
            }
        }

        public ObservableCollection<MinuteSelection> MinutesList
        {
            get
            {
                return _MinutesList;
            }
            set
            {
                _MinutesList = value;
                this.OnPropertyChanged("MinutesList");
            }
        }

        //public ObservableCollection<string> SeceondsList
        //{
        //    get
        //    {
        //        return _SeceondsList;
        //    }
        //    set
        //    {
        //        _SeceondsList = value;
        //        this.OnPropertyChanged("SeceondsList");
        //    }
        //}

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility

        #region Command

        public ICommand LoadedControl
        {
            get
            {
                if (_LoadedControl == null)
                {
                    _LoadedControl = new RelayCommand(param => OnLoadedControl());
                }
                return _LoadedControl;
            }

        }

        public ICommand BlackSpaceClick
        {
            get
            {
                if (_BlackSpaceClick == null)
                {
                    _BlackSpaceClick = new RelayCommand(param => OnBlackSpaceClick());
                }
                return _BlackSpaceClick;
            }
        }

        public ICommand EmptyCommand
        {
            get
            {
                if (_EmptyCommand == null)
                {
                    _EmptyCommand = new RelayCommand(param => OnEmptyCommand());
                }
                return _EmptyCommand;
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (_SaveCommand == null)
                {
                    _SaveCommand = new RelayCommand(param => OnSaveCommand());
                }
                return _SaveCommand;
            }
        }


        #endregion

        #region EventHandler

        private void dateTimePicker_Loaded(object sender, RoutedEventArgs e)
        {
            dtPicker = (View.UI.Channel.DateTimePicker.Controls.DateTimePicker)sender;
        }

        private void cmbHour_DropDownOpened(object sender, EventArgs e)
        {
            foreach (HourSelection hrSelection in ((System.Windows.Controls.ComboBox)sender).Items)
            {
                if (Int32.Parse(hrSelection.Hour) < DateTime.Now.Hour)
                {
                    hrSelection.IsValid = false;
                }
            }
        }

        private void cmbMin_DropDownOpened(object sender, EventArgs e)
        {
            foreach (MinuteSelection minSelection in ((System.Windows.Controls.ComboBox)sender).Items)
            {
                if (Int32.Parse(minSelection.Minute) < DateTime.Now.Minute)
                {
                    minSelection.IsValid = false;
                }
            }
        }

        #endregion

        public class HourSelection : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;
            private string _Hour;
            private bool _IsValid = true;

            public string Hour
            {
                get
                {
                    return _Hour;
                }
                set
                {
                    if (value == _Hour) return;
                    _Hour = value;
                    this.OnPropertyChanged("Hour");
                }
            }

            public bool IsValid
            {
                get
                {
                    return _IsValid;
                }
                set
                {
                    if (value == _IsValid) return;
                    _IsValid = value;
                    this.OnPropertyChanged("IsValid");
                }
            }

            public void OnPropertyChanged(string propertyName)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        public class MinuteSelection : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;
            private string _Minute;
            private bool _IsValid = true;

            public string Minute
            {
                get
                {
                    return _Minute;
                }
                set
                {
                    if (value == _Minute) return;
                    _Minute = value;
                    this.OnPropertyChanged("Minute");
                }
            }

            public bool IsValid
            {
                get
                {
                    return _IsValid;
                }
                set
                {
                    if (value == _IsValid) return;
                    _IsValid = value;
                    this.OnPropertyChanged("IsValid");
                }
            }

            public void OnPropertyChanged(string propertyName)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;

namespace View.UI.PopUp.Stream
{
    /// <summary>
    /// Interaction logic for UCRechargePopUp.xaml
    /// </summary>
    public partial class UCRechargePopUp : UserControl, INotifyPropertyChanged
    {
        public delegate void OnCloseing(bool isRecharge);
        public event OnCloseing onClosing;

        public UCRechargePopUp()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private ICommand _RechargeCommand;
        public ICommand RechargeCommand
        {
            get
            {
                if (_RechargeCommand == null)
                {
                    _RechargeCommand = new RelayCommand(parm => OnRechargeCommand());
                }
                return _RechargeCommand;
            }
        }

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand(parm => OnCancelCommand());
                }
                return _CancelCommand;
            }
        }

        private void OnCancelCommand()
        {
            onClosing(false);
        }

        private void OnRechargeCommand()
        {
            onClosing(true);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

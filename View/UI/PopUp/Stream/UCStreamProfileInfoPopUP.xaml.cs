﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.Call;
using View.Utility.Feed;
using View.Utility.Stream;
using View.ViewModel;

namespace View.UI.PopUp.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamProfileInfoPopUP.xaml
    /// </summary>
    public partial class UCStreamProfileInfoPopUP : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCStreamProfileInfoPopUP).Name);
        private Grid _parent;

        public UCStreamProfileInfoPopUP()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Utility

        public void ShowProfileInfo(Grid parent)
        {
            parent.Children.Add(this);
            this._parent = parent;
        }

        private void OnLoadedControl()
        {
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
        }

        private void OnCallCommand(object param)
        {
            try
            {
                if (StreamViewModel.Instance.InitCallInfo(StreamConstants.STREAM_CALL_OUTGOING, SelectedStreamUserModel))
                {
                    if (!CallHelperMethods.StartLiveCall(SelectedStreamUserModel.UserTableID))
                    {
                        StreamViewModel.Instance.DestroyCallInfo();
                    }
                }
                _parent.Children.Remove(this);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnStreamCallCommand() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool CanCallCommand(object param)
        {
            return StreamViewModel.Instance.CallStatus == 0 && SelectedStreamModel != null && SelectedStreamUserModel != null;
        }

        private void OnCloseCommand()
        {
            _parent.Children.Remove(this);
        }

        public void OnReportCommand()
        {
            MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(Guid.Empty, StatusConstants.SPAM_USER, SelectedStreamModel.UserTableID, _parent);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region ICommands

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }

        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null) _CloseCommand = new RelayCommand(param => OnCloseCommand());
                return _CloseCommand;
            }
        }

        private ICommand _ReportCommand;
        public ICommand ReportCommand
        {
            get
            {
                if (_ReportCommand == null) _ReportCommand = new RelayCommand(param => OnReportCommand());
                return _ReportCommand;
            }
        }

        private ICommand _CallCommand;
        public ICommand CallCommand
        {
            get
            {
                if (_CallCommand == null) _CallCommand = new RelayCommand(param => OnCallCommand(param), param => CanCallCommand(param));
                return _CallCommand;
            }
        }

        #endregion

        #region Property

        private StreamModel _SelectedStreamModel;
        public StreamModel SelectedStreamModel
        {
            get { return _SelectedStreamModel; }
            set
            {
                if (value == _SelectedStreamModel) return;
                _SelectedStreamModel = value;
                this.OnPropertyChanged("SelectedStreamModel");
            }
        }

        private StreamUserModel _SelectedStreamUserModel;
        public StreamUserModel SelectedStreamUserModel
        {
            get { return _SelectedStreamUserModel; }
            set
            {
                if (value == _SelectedStreamUserModel) return;
                _SelectedStreamUserModel = value;
                this.OnPropertyChanged("SelectedStreamUserModel");
            }
        }

        #endregion

    }
}

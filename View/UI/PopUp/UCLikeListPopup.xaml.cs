﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.UI.ImageViews;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCLikeListPopup.xaml
    /// </summary>
    public partial class UCLikeListPopup : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCLikeListPopup).Name);

        private const int POPUP_HEIGHT = 720;
        private bool _ShowLikeListPopup;
        private int action;
        private long total;
        public Guid NfId, CommentId, ImageId, ContentId;
        public int type; //0 status,1 statuscomment,2 image,3 imagecomment,4 media,5 mediacomment
        private ObservableCollection<UserBasicInfoModel> _LikeList = new ObservableCollection<UserBasicInfoModel>();

        public ObservableCollection<UserBasicInfoModel> LikeList
        {
            get
            {
                return _LikeList;
            }
            set
            {
                _LikeList = value;
                this.OnPropertyChanged("LikeList");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public UCLikeListPopup()
        {
            this.DataContext = this;
            InitializeComponent();
        }
        public void ClosePopUp()
        {
            popupLikeList.IsOpen = false;
        }

        public bool ShowLikeListPopup
        {
            get { return _ShowLikeListPopup; }
            set
            {
                if (_ShowLikeListPopup == value)
                {
                    return;
                }
                _ShowLikeListPopup = value;
                this.OnPropertyChanged("ShowLikeListPopup");
            }
        }

        public void SetVisibilities()
        {
            try
            {
                if (topLoader.Visibility != Visibility.Collapsed)
                    topLoader.Visibility = Visibility.Collapsed;
                seeMore.Visibility = (_LikeList.Count < total) ? Visibility.Visible : Visibility.Collapsed;
                if (bottomLoader.Visibility != Visibility.Collapsed)
                    bottomLoader.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public void HideLoaderandSeeMore()
        {
            try
            {
                topLoader.Visibility = Visibility.Collapsed;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void Show(UIElement target, long total, Guid nfid, int action) //likes for status
        {
            try
            {
                // CurrentInstance = this;
                type = 0;
                this.NfId = nfid;
                this.action = action;
                this.total = total;
                LikeList.Clear();
                topLoader.Visibility = Visibility.Visible;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Collapsed;
                new ThradListLikesOrComments().StartThread(nfid, action, 0);
                if (popupLikeList.IsOpen)
                {
                    popupLikeList.IsOpen = false;
                }
                else
                {
                    popupLikeList.PlacementTarget = target;


                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    if (buttonPosition < POPUP_HEIGHT)
                    {
                        ShowLikeListPopup = true;
                        //popupLikeList.VerticalOffset = -30;
                    }
                    else
                    {
                        ShowLikeListPopup = false;
                        //popupLikeList.VerticalOffset = -35;
                    }


                    popupLikeList.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public void Show(UIElement target, long total, Guid nfid, Guid cmntid, int action) //likes for status comment
        {
            try
            {
                //CurrentInstance = this;
                type = 1;
                this.NfId = nfid;
                this.CommentId = cmntid;
                this.action = action;
                this.total = total;
                LikeList.Clear();
                topLoader.Visibility = Visibility.Visible;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Collapsed;
                new ThradListLikesOrComments().StartThread(nfid, cmntid, action, 0);
                if (popupLikeList.IsOpen)
                {
                    popupLikeList.IsOpen = false;
                }
                else
                {
                    popupLikeList.PlacementTarget = target;


                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    if (buttonPosition < POPUP_HEIGHT)
                    {
                        ShowLikeListPopup = true;
                        //popupLikeList.VerticalOffset = -30;
                    }
                    else
                    {
                        ShowLikeListPopup = false;
                        //popupLikeList.VerticalOffset = -35;
                    }


                    popupLikeList.IsOpen = true;

                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public void Show(UIElement target, long total, Guid imageId)//likes for image
        {
            try
            {
                type = 2;
                this.total = total;
                this.ImageId = imageId;
                LikeList.Clear();
                topLoader.Visibility = Visibility.Visible;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Collapsed;
                // new ImageLikersOrImageCommentLikersList(imageId, startLimit: LikeList.Count);
                SendDataToServer.ImageLikersOrImageCommentLikersList(imageId, Guid.Empty, startLimit: LikeList.Count);
                if (popupLikeList.IsOpen)
                {
                    popupLikeList.IsOpen = false;
                }
                else
                {
                    popupLikeList.PlacementTarget = target;

                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    if (buttonPosition < POPUP_HEIGHT)
                    {
                        ShowLikeListPopup = true;
                    }
                    else
                    {
                        ShowLikeListPopup = false;
                    }
                    popupLikeList.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public void Show(UIElement target, Guid imageId, Guid commentId, long total) //likes for image comments
        {
            try
            {
                type = 3;
                this.ImageId = imageId;
                this.CommentId = commentId;
                this.total = total;
                LikeList.Clear();
                topLoader.Visibility = Visibility.Visible;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Collapsed;
                SendDataToServer.ImageLikersOrImageCommentLikersList(ImageId, CommentId, 0);

                if (popupLikeList.IsOpen)
                {
                    popupLikeList.IsOpen = false;
                }
                else
                {
                    popupLikeList.PlacementTarget = target;

                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    if (buttonPosition < POPUP_HEIGHT)
                    {
                        ShowLikeListPopup = true;
                    }
                    else
                    {
                        ShowLikeListPopup = false;
                    }
                    popupLikeList.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            popupLikeList.IsOpen = false;
        }

        private void SeeMore_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                topLoader.Visibility = Visibility.Collapsed;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Visible;
                switch (type)
                {
                    case 0:
                        new ThradListLikesOrComments().StartThread(NfId, action, _LikeList.Count);
                        break;
                    case 1:
                        new ThradListLikesOrComments().StartThread(NfId, CommentId, action, 0);
                        break;
                    case 2:
                        SendDataToServer.ImageLikersOrImageCommentLikersList(ImageId, Guid.Empty, startLimit: _LikeList.Count);
                        break;
                    case 3:
                        SendDataToServer.ImageLikersOrImageCommentLikersList(ImageId, CommentId, _LikeList.Count);
                        break;
                    case 4:
                        SendDataToServer.ListMediaLikes(ContentId, NfId, _LikeList.Count);
                        break;
                    case 5:
                        SendDataToServer.ListMediaCommentLikes(ContentId, CommentId, NfId, _LikeList.Count);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private UCBasicImageViewWrapper basicImageViewWrapper()
        {
            return MainSwitcher.PopupController.BasicImageViewWrapper;
        }

        private void txtBL_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (popupLikeList.IsOpen) ClosePopUp();
                if (basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.Visibility == Visibility.Visible) basicImageViewWrapper().ucBasicImageView.DoCancel();

                if (MainSwitcher.PopupController.BasicMediaViewWrapper.Visibility == Visibility.Visible) RingPlayerViewModel.Instance.OnDoCancelRequested();//UCGuiRingID.Instance.ucBasicMediaViewWrapper.ucBasicMediaView.DoCancel();

                TextBlock control = (TextBlock)sender;
                UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
                if (model.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID) RingIDViewModel.Instance.OnMyProfileClicked(model.ShortInfoModel.UserTableID);
                else RingIDViewModel.Instance.OnFriendProfileButtonClicked(model.ShortInfoModel.UserTableID);
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public void Show(UIElement target, long total, Guid nfId, Guid imageId, Guid contentId, Guid commentId)
        {
            try
            {
                this.ImageId = imageId;
                this.CommentId = commentId;
                this.ContentId = contentId;
                this.NfId = nfId;
                this.total = total;
                LikeList.Clear();
                topLoader.Visibility = Visibility.Visible;
                seeMore.Visibility = Visibility.Collapsed;
                bottomLoader.Visibility = Visibility.Collapsed;
                if (commentId != Guid.Empty)
                {
                    if (NfId != Guid.Empty) { }
                    else if (ImageId != Guid.Empty) { }
                    if (ContentId != Guid.Empty) { type = 5; SendDataToServer.ListMediaCommentLikes(ContentId, CommentId, nfId); }
                }
                else
                {
                    if (NfId != Guid.Empty) { }
                    else if (ImageId != Guid.Empty) { }
                    if (ContentId != Guid.Empty) { type = 4; SendDataToServer.ListMediaLikes(ContentId, NfId); }
                }

                if (popupLikeList.IsOpen)
                {
                    popupLikeList.IsOpen = false;
                }
                else
                {
                    popupLikeList.PlacementTarget = target;

                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    if (buttonPosition < POPUP_HEIGHT)
                    {
                        ShowLikeListPopup = true;
                    }
                    else
                    {
                        ShowLikeListPopup = false;
                    }
                    popupLikeList.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public bool IsSameRequest(Guid nfId, Guid imageId, Guid contentId, Guid commentId)
        {
            return (nfId == NfId && imageId == ImageId && contentId == ContentId && commentId == CommentId);
        }
        private void sendRequest_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Image control = (Image)sender;
            UserBasicInfoModel _model = (UserBasicInfoModel)control.DataContext;
            new ThrdAddFriend().StartProcess(_model);
        }
        #region "Utility Methods"

        //public void ListLikesOrComments(long nfId, int action, int comment_like_number)
        //{
        //    new ThradListLikesOrComments().StartThread(nfId, action, comment_like_number);
        //}

        //public void ListLikesOrComments(long nfId, long cmntId, int action, int comment_like_number)
        //{
        //    new ThradListLikesOrComments().StartThread(nfId, cmntId, action, comment_like_number);
        //}

        #endregion "Utility Methods"
    }
}

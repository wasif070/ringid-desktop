﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using View.UI.Feed;
using View.Utility;
using View.ViewModel.NewStatus;
using View.BindingModels;
using System.Collections.ObjectModel;
using View.ViewModel.PopUp;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCNewStatusPrivacyPopup.xaml
    /// </summary>
    public partial class UCNewStatusPrivacyPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        ILog log = LogManager.GetLogger(typeof(UCNewStatusPrivacyPopup).Name);

        #region "Constructor"
        public UCNewStatusPrivacyPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            SetParent(motherGrid);
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region Properties
        private int _PrivacyVal;//default=2(friends),1=only me,3=public
        public int PrivacyVal
        {
            get { return _PrivacyVal; }
            set
            {
                _PrivacyVal = value;
                this.OnPropertyChanged("PrivacyVal");
            }
        }

        private ObservableCollection<UserShortInfoModel> _friendToShowOrHide = new ObservableCollection<UserShortInfoModel>();
        public ObservableCollection<UserShortInfoModel> FriendToShowOrHide
        {
            get
            {
                return _friendToShowOrHide;
            }
            set
            {
                _friendToShowOrHide = value;
                this.OnPropertyChanged("FriendToShowOrHide");
            }
        }

        public CustomPrivacyPopupView customPrivacyPopupView
        {
            get
            {
                return MainSwitcher.PopupController.customPrivacyPopupView;
            }
        }

        #endregion "Properties"

        private NewStatusViewModel newStatusViewModel;
        private UCSingleFeed ucSingleFeed;

        #region "Public Method"

        public void ShowPopUp(UIElement target, NewStatusViewModel newStatusViewModel)
        {
            this.newStatusViewModel = newStatusViewModel;
            ucSingleFeed = null;
            this.PrivacyVal = this.newStatusViewModel.PrivacyValue;

            popupPrivacy.PlacementTarget = target;
            popupPrivacy.IsOpen = true;
        }

        public void ShowPopUp(UIElement target, UCSingleFeed ucSingleFeed)
        {
            this.ucSingleFeed = ucSingleFeed;
            this.newStatusViewModel = null;
            //this.PrivacyVal = this.ucSingleFeed.FeedModel.Privacy;

            popupPrivacy.PlacementTarget = target;
            popupPrivacy.IsOpen = true;
        }

        public void PopupClose()
        {
                //UCNewStatusPrivacyPopup.Instance = null;
                //UCNewStatusPrivacyPopupWrapper.Instance.wrapperBorder.Child = null;
                popupPrivacy.IsOpen = false;
        }
        #endregion

        private void onlyMe_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                PrivacyVal = 1;
                PopupClose();
                if (newStatusViewModel != null)
                    this.newStatusViewModel.PrivacyValue = PrivacyVal;
                //else if (ucSingleFeed != null && ucSingleFeed.FeedModel.Privacy != PrivacyVal)
                //{
                //    //EditFeed(PrivacyVal, ucSingleFeed.FeedModel.NewsfeedId, null, null, null, null);
                //    //ucSingleFeed.action_edit_feed(PrivacyVal);
                //}
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CustomMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                PopupClose();
                if(newStatusViewModel != null)
                {
                    this.newStatusViewModel.PrivacyValue = PrivacyVal;
                }
                if (customPrivacyPopupView != null)
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(customPrivacyPopupView);
                //customPrivacyPopupView = new CustomPrivacyPopupView(UCGuiRingID.Instance.MotherPanel);
                //friendPrivacyPopupViewModel = (CustomPrivacyPopupViewModel)friendPrivacyPopupView.DataContext;
                customPrivacyPopupView.Show();
                ((CustomPrivacyPopupViewModel)customPrivacyPopupView.DataContext).ShowCustomPrivacy(newStatusViewModel);
                //LikeListViewWrapper.ShowPrivacyFriends(newStatusViewModel, PrivacyVal);
            }
            catch(Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void friends_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                PrivacyVal = 15;
                PopupClose();
                if (newStatusViewModel != null)
                    this.newStatusViewModel.PrivacyValue = PrivacyVal;
                //else if (ucSingleFeed != null && ucSingleFeed.FeedModel.Privacy != PrivacyVal)
                //{
                //    //EditFeed(PrivacyVal, ucSingleFeed.FeedModel.NewsfeedId, null, null, null, null);
                //    //ucSingleFeed.action_edit_feed(PrivacyVal);
                //}
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void public_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                PrivacyVal = 25;
                PopupClose();
                if (newStatusViewModel != null)
                    this.newStatusViewModel.PrivacyValue = PrivacyVal;
                //else if (ucSingleFeed != null && ucSingleFeed.FeedModel.Privacy != PrivacyVal)
                //{
                //    //EditFeed(PrivacyVal, ucSingleFeed.FeedModel.NewsfeedId, null, null, null, null);
                //    //ucSingleFeed.action_edit_feed(PrivacyVal);
                //}
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            PopupClose();
        }

        private ICommand _popupPrivacyClosed;
        public ICommand popupPrivacyClosed
        {
            get
            {
                if (_popupPrivacyClosed == null) _popupPrivacyClosed = new RelayCommand(param => OnpopupPrivacyClosed());
                return _popupPrivacyClosed;
            }
        }
        private void OnpopupPrivacyClosed()
        {
            PopupClose();
            Hide();
        }
        #endregion
    }
}

<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Auth.Service.Feed;
using log4net;
using Models.Constants;
using Models.Entity;
using View.BindingModels;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.Utility;
using View.Utility.Circle;
using View.Utility.DataContainer;
using View.Utility.RingPlayer;
using Newtonsoft.Json.Linq;
using View.Utility.Auth;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCEditDeleteReportOptionsPopup.xaml
    /// </summary>
    public partial class UCEditDeleteReportOptionsPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCEditDeleteReportOptionsPopup).Name);

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private FeedModel feedModel;
        private CommentModel commentModel;
        private bool isFeed = true;

        #region "Constructor"
        public UCEditDeleteReportOptionsPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            //InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
        #endregion

        #region "Property"
        //private Visibility _AddLctnVisibility = Visibility.Collapsed;
        //public Visibility AddLctnVisibility
        //{
        //    get { return _AddLctnVisibility; }
        //    set
        //    {
        //        if (value == _AddLctnVisibility)
        //            return;
        //        _AddLctnVisibility = value;
        //        this.OnPropertyChanged("AddLctnVisibility");
        //    }
        //}

        private Visibility _EditVisibility = Visibility.Collapsed;
        public Visibility EditVisibility
        {
            get { return _EditVisibility; }
            set
            {
                if (value == _EditVisibility)
                    return;
                _EditVisibility = value;
                this.OnPropertyChanged("EditVisibility");
            }
        }
        private Visibility _DeletetVisibility = Visibility.Collapsed;
        public Visibility DeletetVisibility
        {
            get { return _DeletetVisibility; }
            set
            {
                if (value == _DeletetVisibility)
                    return;
                _DeletetVisibility = value;
                this.OnPropertyChanged("DeletetVisibility");
            }
        }


        private Visibility _HideThisorAll = Visibility.Collapsed;
        public Visibility HideThisorAll
        {
            get { return _HideThisorAll; }
            set
            {
                if (value == _HideThisorAll)
                    return;
                _HideThisorAll = value;
                this.OnPropertyChanged("HideThisorAll");
            }
        }

        private Visibility _HideThisVisibility = Visibility.Collapsed;
        public Visibility HideThisVisibility
        {
            get { return _HideThisVisibility; }
            set
            {
                if (value == _HideThisVisibility)
                    return;
                _HideThisVisibility = value;
                this.OnPropertyChanged("HideThisVisibility");
            }
        }

        private Visibility _SaveUnsaveVisibility = Visibility.Collapsed;
        public Visibility SaveUnsaveVisibility
        {
            get { return _SaveUnsaveVisibility; }
            set
            {
                if (value == _SaveUnsaveVisibility)
                    return;
                _SaveUnsaveVisibility = value;
                this.OnPropertyChanged("SaveUnsaveVisibility");
            }
        }

        private Visibility _followUnfollowVisibility = Visibility.Collapsed;
        public Visibility FollowUnfollowVisibility
        {
            get { return _followUnfollowVisibility; }
            set
            {
                if (value == _followUnfollowVisibility)
                    return;
                _followUnfollowVisibility = value;
                this.OnPropertyChanged("FollowUnfollowVisibility");
            }
        }

        private Visibility _ReportVisibility = Visibility.Collapsed;
        public Visibility ReportVisibility
        {
            get { return _ReportVisibility; }
            set
            {
                if (value == _ReportVisibility)
                    return;
                _ReportVisibility = value;
                this.OnPropertyChanged("ReportVisibility");
            }
        }

        private bool _IsSaved = false;
        public bool IsSaved
        {
            get { return _IsSaved; }
            set
            {
                if (value == _IsSaved)
                    return;
                _IsSaved = value;
                this.OnPropertyChanged("IsSaved");
            }
        }

        private bool _IsFollowed = false;
        public bool IsFollowed
        {
            get { return _IsFollowed; }
            set
            {
                if (value == _IsFollowed)
                    return;
                _IsFollowed = value;
                this.OnPropertyChanged("IsFollowed");
            }
        }

        public UCAddLocationView ucAddLocationView
        {
            get
            {
                return MainSwitcher.PopupController.ucAddLocationView;
            }
        }

        private UCEditStatusCommentView EditStatusCommentView
        {
            get
            {
                return MainSwitcher.PopupController.EditStatusCommentView;
            }
        }
        #endregion"Properties"

        public void ShowEditDeletePopup(UIElement target, bool HideOff, bool hidethis, bool saveUnsave, bool followUnfollow, bool ReportOn, object model, bool IsFromShareList = false)
        {
            try
            {
                if (model is FeedModel)
                {
                    this.feedModel = (FeedModel)model;
                    isFeed = true;
                    //if (this.feedModel.NewsPortalModel.IsSubscribed)
                    //{
                    //    IsFollowed = true;
                    //}
                    //else IsFollowed = false;
                }
                else if (model is CommentModel)
                {
                    this.commentModel = (CommentModel)model;
                    isFeed = false;
                }
                if (popupOptions.IsOpen == false)
                {
                    if (isFeed)
                    {
                        if (saveUnsave)
                        {
                            if (this.feedModel != null && this.feedModel.IsSaved)
                            {
                                IsSaved = true;
                            }
                            else
                            {
                                IsSaved = false;
                                if (!hidethis) HideThisVisibility = Visibility.Visible;
                                if (!HideOff) HideThisorAll = Visibility.Visible;
                            }

                            SaveUnsaveVisibility = Visibility.Visible;
                        }

                        //if (followUnfollow) FollowUnfollowVisibility = Visibility.Visible;

                        if (ReportOn) ReportVisibility = Visibility.Visible;

                        if (this.feedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                        {
                            //if (feedModel.NumberOfComments == 0 && feedModel.NumberOfLikes == 0 && feedModel.NumberOfShares == 0)
                            if (feedModel.SingleImageFeedModel != null && (feedModel.SingleImageFeedModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE || feedModel.SingleImageFeedModel.ImageType == SettingsConstants.TYPE_COVER_IMAGE))
                            {
                                EditVisibility = Visibility.Collapsed;
                                DeletetVisibility = Visibility.Collapsed;
                            }
                            else
                            {
                                EditVisibility = Visibility.Visible;
                                DeletetVisibility = Visibility.Visible;
                            }

                            //if (feedModel.locationModel == null)
                            //    AddLctnVisibility = Visibility.Visible;
                        }
                        else if (this.feedModel.WallOrContentOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID && !IsFromShareList)
                            DeletetVisibility = Visibility.Visible;
                    }
                    else
                    {
                        if (this.commentModel.UserShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                            EditVisibility = Visibility.Visible;
                        else EditVisibility = Visibility.Collapsed;

                        DeletetVisibility = Visibility.Visible;
                    }

                    popupOptions.PlacementTarget = target;
                    popupOptions.IsOpen = true;
                    SetEvents();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetEvents()
        {
            //addLctnDock.MouseLeftButtonDown += addLctnDock_MouseLeftButtonDown;
            editDock.MouseLeftButtonDown += editDock_MouseLeftButtonDown;
            deleteDock.MouseLeftButtonDown += deleteDock_MouseLeftButtonDown;
            hideFeedDock.MouseLeftButtonDown += hideFeedDock_MouseLeftButtonDown;
            hideAllFeedDock.MouseLeftButtonDown += hideAllFeedDock_MouseLeftButtonDown;
            reportDock.MouseLeftButtonDown += reportDock_MouseLeftButtonDown;
            saveUnsaveDock.MouseLeftButtonDown += saveUnsaveDock_MouseLeftButtonDown;
            //followUnfollowDock.MouseLeftButtonDown += followUnfollowDock_MouseLeftButtonDown;
        }

        public void ClosePopup()
        {
            HideThisorAll = Visibility.Collapsed;
            HideThisVisibility = Visibility.Collapsed;
            SaveUnsaveVisibility = Visibility.Collapsed;
            FollowUnfollowVisibility = Visibility.Collapsed;
            ReportVisibility = Visibility.Collapsed;
            EditVisibility = Visibility.Collapsed;
            //AddLctnVisibility = Visibility.Collapsed;
            DeletetVisibility = Visibility.Collapsed;
            //addLctnDock.MouseLeftButtonDown -= addLctnDock_MouseLeftButtonDown;
            editDock.MouseLeftButtonDown -= editDock_MouseLeftButtonDown;
            deleteDock.MouseLeftButtonDown -= deleteDock_MouseLeftButtonDown;
            hideFeedDock.MouseLeftButtonDown -= hideFeedDock_MouseLeftButtonDown;
            hideAllFeedDock.MouseLeftButtonDown -= hideAllFeedDock_MouseLeftButtonDown;
            reportDock.MouseLeftButtonDown -= reportDock_MouseLeftButtonDown;
            saveUnsaveDock.MouseLeftButtonDown -= saveUnsaveDock_MouseLeftButtonDown;
            //followUnfollowDock.MouseLeftButtonDown -= followUnfollowDock_MouseLeftButtonDown;
            //DownloadOrAddToAlbumPopUpWrapper.Visibility = Visibility.Hidden;
        }

        private void addLctnDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            ucAddLocationView.Show();
            ucAddLocationView.ShowAddLoation(feedModel.NewsfeedId, feedModel.Status, feedModel.StatusTags, feedModel.WallOrContentOwner.UserTableID, (feedModel.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE) ? feedModel.WallOrContentOwner.UserTableID : 0);
        }

        private void editDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            if (isFeed)
            {
                //if (feedModel.ShowContinue || (feedModel.TaggedFriendsList != null && feedModel.TotalTaggedFriends != feedModel.TaggedFriendsList.Count)
                //    || feedModel.TotalImage > 3 || (feedModel.MediaContent != null && feedModel.MediaContent.TotalMediaCount > 3))
                //{
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    UCSingleFeedDetailsView.Instance.Hide();
                EditStatusCommentView.Show();
                EditStatusCommentView.ShowEditStatus(feedModel, true);
                EditStatusCommentView.OnRemovedUserControl += () =>
                {
                    EditStatusCommentView.HideEditStatusView();
                    //if (ImageViewInMain != null)
                    //{
                    //    ImageViewInMain.GrabKeyboardFocus();
                    //}
                };
                MainSwitcher.ThreadManager().DetailsOfaFeed.CallBackEvent += (dto) =>
                {
                    if (dto != null)
                    {
                        feedModel.LoadData(dto);
                        if (feedModel.ImageList != null && feedModel.ImageList.Count < feedModel.TotalImage)
                        {
                            SendDataToServer.SendMoreFeedImagesRequest(feedModel.NewsfeedId, feedModel.ImageList.Count - 1, feedModel.TotalImage - feedModel.ImageList.Count);
                            Thread.Sleep(2000);
                        }
                        //if (feedModel.TaggedFriendsList != null && feedModel.TaggedFriendsList.Count > 0)
                        //    feedModel.OnPropertyChanged("TaggedFriendsList");
                        Application.Current.Dispatcher.Invoke((Action)(() =>
                        {
                            EditStatusCommentView.LoadNewStatusFromFeedModel();
                        }));
                    }
                };
                MainSwitcher.ThreadManager().DetailsOfaFeed.StartThread(feedModel.NewsfeedId);
                //}
                //else LikeListViewWrapper.Show(feedModel);
            }
            else
                this.commentModel.IsEditMode = true;
        }

        //private UCBasicImageViewWrapper basicImageViewWrapper()
        //{
        //    return MainSwitcher.PopupController.BasicImageViewWrapper;
        //}

        private void deleteDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            if (isFeed)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this Post"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.DELETE_MEDIA_MESSAGE_TITLE));
                if (isTrue)
                {
                    //if (feedModel.GroupId > 0)
                    //{
                    //    new ThradDeleteFeed().StartThread(0, feedModel.GroupId, feedModel.NewsfeedId);
                    //}
                    //else if (feedModel.FriendShortInfoModel != null && feedModel.FriendShortInfoModel.UserTableID > 0)
                    //{
                    //    new ThradDeleteFeed().StartThread(feedModel.FriendShortInfoModel.UserTableID, 0, feedModel.NewsfeedId);
                    //}
                    //else
                    //{
                    //    new ThradDeleteFeed().StartThread(feedModel.NewsfeedId);
                    //}
                    new ThradDeleteFeed().StartThread(feedModel.NewsfeedId);
                }
            }
            else
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this comment"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.DELETE_MEDIA_MESSAGE_TITLE));
                if (isTrue)
                {
                    JObject pakToSend = new JObject();
                    FeedModel fm = null;
                    bool ImgToContentID = false;
                    pakToSend[JsonKeys.Action] = AppConstants.ACTION_MERGED_DELETE_COMMENT;
                    pakToSend[JsonKeys.CommentId] = commentModel.CommentId;

                    if (commentModel.ImageId != Guid.Empty)     //// DELETE COMMENT OF IMAGE
                    {
                        ImageModel imageModel = null;
                        ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(commentModel.ImageId, out imageModel);
                        bool fromFeed = (ViewConstants.NavigateFrom == StatusConstants.NAVIGATE_FROM_FEED) ? true : false;

                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_IMAGE;
                        pakToSend[JsonKeys.OwnerId] = imageModel.UserShortInfoModel.UserTableID;
                        pakToSend[JsonKeys.ContentId] = commentModel.ImageId;
                        ImgToContentID = true;
                        if (fromFeed)
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            if (imageModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(imageModel.NewsFeedId, out fm))
                            {
                                pakToSend[JsonKeys.NewsfeedId] = imageModel.NewsFeedId;
                                pakToSend[JsonKeys.Type] = fm.BookPostType;
                                pakToSend[JsonKeys.Privacy] = fm.Privacy;
                            }
                        }
                        else
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                            pakToSend[JsonKeys.AlbumId] = imageModel.AlbumId;
                            pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                        }
                    }
                    else if (commentModel.ContentId != Guid.Empty)     //// DELETE COMMENT OF MULTIMEDIA
                    {
                        SingleMediaModel singleMediaModel = null;
                        MediaDataContainer.Instance.ContentModels.TryGetValue(commentModel.ContentId, out singleMediaModel);
                        bool fromFeed = singleMediaModel.PlayedFromFeed ? true : false;

                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_MULTIMEDIA;
                        pakToSend[JsonKeys.OwnerId] = singleMediaModel.MediaOwner.UserTableID;
                        pakToSend[JsonKeys.ContentId] = singleMediaModel.ContentId;
                        if (fromFeed)
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            if (singleMediaModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(singleMediaModel.NewsFeedId, out fm))
                            {
                                pakToSend[JsonKeys.NewsfeedId] = singleMediaModel.NewsFeedId;
                                pakToSend[JsonKeys.Type] = fm.BookPostType;
                                pakToSend[JsonKeys.Privacy] = fm.Privacy;
                            }
                        }
                        else
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                            pakToSend[JsonKeys.AlbumId] = singleMediaModel.AlbumId;
                            pakToSend[JsonKeys.MediaType] = singleMediaModel.MediaType;
                        }
                    }
                    else      //// DELETE COMMENT OF STATUS
                    {
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_STATUS;
                        if (commentModel.NewsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(commentModel.NewsfeedId, out fm))
                        {
                            pakToSend[JsonKeys.Privacy] = fm.Privacy;
                            pakToSend[JsonKeys.NewsfeedId] = commentModel.NewsfeedId;
                            pakToSend[JsonKeys.Type] = fm.BookPostType;
                            pakToSend[JsonKeys.OwnerId] = fm.PostOwner.UserTableID;

                            if (fm.BookPostType == 2 || fm.BookPostType == 5 || fm.BookPostType == 8)
                            {
                                if (fm.SingleMediaFeedModel != null)
                                    pakToSend[JsonKeys.ContentId] = fm.SingleMediaFeedModel.ContentId;

                                else if (fm.SingleImageFeedModel != null)
                                {
                                    pakToSend[JsonKeys.ContentId] = fm.SingleImageFeedModel.ImageId;
                                    ImgToContentID = true;
                                }
                            }
                        }
                    }
                    new ThreadDeleteAnyComment(pakToSend, ImgToContentID).StartThread();
                }
            }
        }

        private void hideFeedDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            bool isTrue = UIHelperMethods.ShowQuestion(NotificationMessages.SINGLE_FEED_HIDE_MESSAGE, String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.SINGLE_FEED_HIDE_MESSAGE_TITLE));
            if (isTrue)
            {
                MainSwitcher.ThreadManager().HideFeed.StartThread(feedModel);
            }
            popupOptions.IsOpen = false;
        }

        private void hideAllFeedDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string fullName = feedModel.PostOwner.FullName;
            bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.SINGLE_FEED_HIDE_USER_MESSAGE, fullName), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.SINGLE_FEED_HIDE_MESSAGE_TITLE));
            if (isTrue)
            {
                MainSwitcher.ThreadManager().HideUser.StartThread(feedModel.PostOwner.UserTableID, feedModel.PostOwner);
            }
            popupOptions.IsOpen = false;
        }

        private void reportDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(this.feedModel.NewsfeedId, StatusConstants.SPAM_FEED, 0);
        }

        private void saveUnsaveDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            if (IsSaved)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.SURE_WANT_TO, NotificationMessages.SINGLE_FEED_UNSAVE_MESSAGE), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages. SINGLE_FEED_UNSAVE_MESSAGE_TITLE));
                if (isTrue)
                {
                    List<Guid> lst = new List<Guid>();
                    lst.Add(feedModel.NewsfeedId);
                    //int ptype = 0;
                    //if (feedModel.BookPostType == SettingsConstants.NEWS_FEED_TYPE_NEWSPORTAL) ptype = SettingsConstants.PROFILE_TYPE_NEWSPORTAL;
                    //else if (feedModel.BookPostType == SettingsConstants.NEWS_FEED_TYPE_PAGES) ptype = SettingsConstants.PROFILE_TYPE_PAGES;
                    new ThradSaveUnsaveNewsPortalFeed(2, lst, 0, feedModel).StartThread();
                }
            }
            else
            {
                List<Guid> lst = new List<Guid>();
                lst.Add(feedModel.NewsfeedId);
                //int ptype = 0;
                //if (feedModel.BookPostType == SettingsConstants.NEWS_FEED_TYPE_NEWSPORTAL) ptype = SettingsConstants.PROFILE_TYPE_NEWSPORTAL;
                //else if (feedModel.BookPostType == SettingsConstants.NEWS_FEED_TYPE_PAGES) ptype = SettingsConstants.PROFILE_TYPE_PAGES;
                new ThradSaveUnsaveNewsPortalFeed(1, lst, 0, feedModel).StartThread();
            }
            //ClosePopup();
        }

        private void followUnfollowDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupOptions.IsOpen = false;
        }

        private ICommand _PopupOptionClosed;
        public ICommand PopupOptionClosed
        {
            get
            {
                if (_PopupOptionClosed == null) _PopupOptionClosed = new RelayCommand(param => OnPopupOptionsClosed());
                return _PopupOptionClosed;
            }
        }
        private void OnPopupOptionsClosed()
        {
            ClosePopup();
            Hide();
        }
        #endregion

        #region "Utility Methods"

        //private void DeleteComment(long NfId, long ImageId, long ContentId, long cmntId)
        //{
        //    new ThreadDeleteAnyComment(NfId, ImageId, ContentId, cmntId).StartThread();
        //}

        //private void DeleteFeed(long FriendIdentity, long CircleId, long NfId)
        //{
        //    new ThradDeleteFeed().StartThread(FriendIdentity, CircleId, NfId);
        //}
        //private void DeleteFeed(long NfId)
        //{
        //    new ThradDeleteFeed().StartThread(NfId);
        //}

        #endregion "Utility Methods"

    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Auth.Service.Feed;
using log4net;
using Models.Constants;
using Models.Entity;
using View.BindingModels;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.Utility;
using View.Utility.Circle;
using View.Utility.DataContainer;
using View.Utility.RingPlayer;
using Newtonsoft.Json.Linq;
using View.Utility.Auth;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCEditDeleteReportOptionsPopup.xaml
    /// </summary>
    public partial class UCEditDeleteReportOptionsPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCEditDeleteReportOptionsPopup).Name);

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private FeedModel feedModel;
        private CommentModel commentModel;
        private bool isFeed = true;

        #region "Constructor"
        public UCEditDeleteReportOptionsPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            //InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
        #endregion

        #region "Property"
        //private Visibility _AddLctnVisibility = Visibility.Collapsed;
        //public Visibility AddLctnVisibility
        //{
        //    get { return _AddLctnVisibility; }
        //    set
        //    {
        //        if (value == _AddLctnVisibility)
        //            return;
        //        _AddLctnVisibility = value;
        //        this.OnPropertyChanged("AddLctnVisibility");
        //    }
        //}

        private Visibility _EditVisibility = Visibility.Collapsed;
        public Visibility EditVisibility
        {
            get { return _EditVisibility; }
            set
            {
                if (value == _EditVisibility)
                    return;
                _EditVisibility = value;
                this.OnPropertyChanged("EditVisibility");
            }
        }
        private Visibility _DeletetVisibility = Visibility.Collapsed;
        public Visibility DeletetVisibility
        {
            get { return _DeletetVisibility; }
            set
            {
                if (value == _DeletetVisibility)
                    return;
                _DeletetVisibility = value;
                this.OnPropertyChanged("DeletetVisibility");
            }
        }


        private Visibility _HideThisorAll = Visibility.Collapsed;
        public Visibility HideThisorAll
        {
            get { return _HideThisorAll; }
            set
            {
                if (value == _HideThisorAll)
                    return;
                _HideThisorAll = value;
                this.OnPropertyChanged("HideThisorAll");
            }
        }

        private Visibility _HideThisVisibility = Visibility.Collapsed;
        public Visibility HideThisVisibility
        {
            get { return _HideThisVisibility; }
            set
            {
                if (value == _HideThisVisibility)
                    return;
                _HideThisVisibility = value;
                this.OnPropertyChanged("HideThisVisibility");
            }
        }

        private Visibility _SaveUnsaveVisibility = Visibility.Collapsed;
        public Visibility SaveUnsaveVisibility
        {
            get { return _SaveUnsaveVisibility; }
            set
            {
                if (value == _SaveUnsaveVisibility)
                    return;
                _SaveUnsaveVisibility = value;
                this.OnPropertyChanged("SaveUnsaveVisibility");
            }
        }

        private Visibility _followUnfollowVisibility = Visibility.Collapsed;
        public Visibility FollowUnfollowVisibility
        {
            get { return _followUnfollowVisibility; }
            set
            {
                if (value == _followUnfollowVisibility)
                    return;
                _followUnfollowVisibility = value;
                this.OnPropertyChanged("FollowUnfollowVisibility");
            }
        }

        private Visibility _ReportVisibility = Visibility.Collapsed;
        public Visibility ReportVisibility
        {
            get { return _ReportVisibility; }
            set
            {
                if (value == _ReportVisibility)
                    return;
                _ReportVisibility = value;
                this.OnPropertyChanged("ReportVisibility");
            }
        }

        private bool _IsSaved = false;
        public bool IsSaved
        {
            get { return _IsSaved; }
            set
            {
                if (value == _IsSaved)
                    return;
                _IsSaved = value;
                this.OnPropertyChanged("IsSaved");
            }
        }

        private bool _IsFollowed = false;
        public bool IsFollowed
        {
            get { return _IsFollowed; }
            set
            {
                if (value == _IsFollowed)
                    return;
                _IsFollowed = value;
                this.OnPropertyChanged("IsFollowed");
            }
        }

        public UCAddLocationView ucAddLocationView
        {
            get
            {
                return MainSwitcher.PopupController.ucAddLocationView;
            }
        }

        private UCEditStatusCommentView EditStatusCommentView
        {
            get
            {
                return MainSwitcher.PopupController.EditStatusCommentView;
            }
        }
        #endregion"Properties"

        public void ShowEditDeletePopup(UIElement target, bool HideOff, bool hidethis, bool saveUnsave, bool followUnfollow, bool ReportOn, object model, bool IsFromShareList = false)
        {
            try
            {
                if (model is FeedModel)
                {
                    this.feedModel = (FeedModel)model;
                    isFeed = true;
                    //if (this.feedModel.NewsPortalModel.IsSubscribed)
                    //{
                    //    IsFollowed = true;
                    //}
                    //else IsFollowed = false;
                }
                else if (model is CommentModel)
                {
                    this.commentModel = (CommentModel)model;
                    isFeed = false;
                }
                if (popupOptions.IsOpen == false)
                {
                    if (isFeed)
                    {
                        if (saveUnsave)
                        {
                            if (this.feedModel != null && this.feedModel.IsSaved)
                            {
                                IsSaved = true;
                            }
                            else
                            {
                                IsSaved = false;
                                if (!hidethis) HideThisVisibility = Visibility.Visible;
                                if (!HideOff) HideThisorAll = Visibility.Visible;
                            }

                            SaveUnsaveVisibility = Visibility.Visible;
                        }

                        //if (followUnfollow) FollowUnfollowVisibility = Visibility.Visible;

                        if (ReportOn) ReportVisibility = Visibility.Visible;

                        if (this.feedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                        {
                            //if (feedModel.NumberOfComments == 0 && feedModel.NumberOfLikes == 0 && feedModel.NumberOfShares == 0)
                            if (feedModel.SingleImageFeedModel != null && (feedModel.SingleImageFeedModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE || feedModel.SingleImageFeedModel.ImageType == SettingsConstants.TYPE_COVER_IMAGE))
                            {
                                EditVisibility = Visibility.Collapsed;
                                DeletetVisibility = Visibility.Collapsed;
                            }
                            else
                            {
                                EditVisibility = Visibility.Visible;
                                DeletetVisibility = Visibility.Visible;
                            }

                            //if (feedModel.locationModel == null)
                            //    AddLctnVisibility = Visibility.Visible;
                        }
                        else if (this.feedModel.WallOrContentOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID && !IsFromShareList)
                            DeletetVisibility = Visibility.Visible;
                    }
                    else
                    {
                        if (this.commentModel.UserShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                            EditVisibility = Visibility.Visible;
                        else EditVisibility = Visibility.Collapsed;

                        DeletetVisibility = Visibility.Visible;
                    }

                    popupOptions.PlacementTarget = target;
                    popupOptions.IsOpen = true;
                    SetEvents();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetEvents()
        {
            //addLctnDock.MouseLeftButtonDown += addLctnDock_MouseLeftButtonDown;
            editDock.MouseLeftButtonDown += editDock_MouseLeftButtonDown;
            deleteDock.MouseLeftButtonDown += deleteDock_MouseLeftButtonDown;
            hideFeedDock.MouseLeftButtonDown += hideFeedDock_MouseLeftButtonDown;
            hideAllFeedDock.MouseLeftButtonDown += hideAllFeedDock_MouseLeftButtonDown;
            reportDock.MouseLeftButtonDown += reportDock_MouseLeftButtonDown;
            saveUnsaveDock.MouseLeftButtonDown += saveUnsaveDock_MouseLeftButtonDown;
            //followUnfollowDock.MouseLeftButtonDown += followUnfollowDock_MouseLeftButtonDown;
        }

        public void ClosePopup()
        {
            HideThisorAll = Visibility.Collapsed;
            HideThisVisibility = Visibility.Collapsed;
            SaveUnsaveVisibility = Visibility.Collapsed;
            FollowUnfollowVisibility = Visibility.Collapsed;
            ReportVisibility = Visibility.Collapsed;
            EditVisibility = Visibility.Collapsed;
            //AddLctnVisibility = Visibility.Collapsed;
            DeletetVisibility = Visibility.Collapsed;
            //addLctnDock.MouseLeftButtonDown -= addLctnDock_MouseLeftButtonDown;
            editDock.MouseLeftButtonDown -= editDock_MouseLeftButtonDown;
            deleteDock.MouseLeftButtonDown -= deleteDock_MouseLeftButtonDown;
            hideFeedDock.MouseLeftButtonDown -= hideFeedDock_MouseLeftButtonDown;
            hideAllFeedDock.MouseLeftButtonDown -= hideAllFeedDock_MouseLeftButtonDown;
            reportDock.MouseLeftButtonDown -= reportDock_MouseLeftButtonDown;
            saveUnsaveDock.MouseLeftButtonDown -= saveUnsaveDock_MouseLeftButtonDown;
            //followUnfollowDock.MouseLeftButtonDown -= followUnfollowDock_MouseLeftButtonDown;
            //DownloadOrAddToAlbumPopUpWrapper.Visibility = Visibility.Hidden;
        }

        private void addLctnDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            ucAddLocationView.Show();
            ucAddLocationView.ShowAddLoation(feedModel.NewsfeedId, feedModel.Status, feedModel.StatusTags, feedModel.WallOrContentOwner.UserTableID, (feedModel.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE) ? feedModel.WallOrContentOwner.UserTableID : 0);
        }

        private void editDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            if (isFeed)
            {
                //if (feedModel.ShowContinue || (feedModel.TaggedFriendsList != null && feedModel.TotalTaggedFriends != feedModel.TaggedFriendsList.Count)
                //    || feedModel.TotalImage > 3 || (feedModel.MediaContent != null && feedModel.MediaContent.TotalMediaCount > 3))
                //{
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    UCSingleFeedDetailsView.Instance.Hide();
                EditStatusCommentView.Show();
                EditStatusCommentView.ShowEditStatus(feedModel, true);
                EditStatusCommentView.OnRemovedUserControl += () =>
                {
                    EditStatusCommentView.HideEditStatusView();
                    //if (ImageViewInMain != null)
                    //{
                    //    ImageViewInMain.GrabKeyboardFocus();
                    //}
                };
                MainSwitcher.ThreadManager().DetailsOfaFeed.CallBackEvent += (dto) =>
                {
                    if (dto != null)
                    {
                        feedModel.LoadData(dto);
                        if (feedModel.ImageList != null && feedModel.ImageList.Count < feedModel.TotalImage)
                        {
                            SendDataToServer.SendMoreFeedImagesRequest(feedModel.NewsfeedId, feedModel.ImageList.Count - 1, feedModel.TotalImage - feedModel.ImageList.Count);
                            Thread.Sleep(2000);
                        }
                        //if (feedModel.TaggedFriendsList != null && feedModel.TaggedFriendsList.Count > 0)
                        //    feedModel.OnPropertyChanged("TaggedFriendsList");
                        Application.Current.Dispatcher.Invoke((Action)(() =>
                        {
                            EditStatusCommentView.LoadNewStatusFromFeedModel();
                        }));
                    }
                };
                MainSwitcher.ThreadManager().DetailsOfaFeed.StartThread(feedModel.NewsfeedId);
                //}
                //else LikeListViewWrapper.Show(feedModel);
            }
            else
                this.commentModel.IsEditMode = true;
        }

        //private UCBasicImageViewWrapper basicImageViewWrapper()
        //{
        //    return MainSwitcher.PopupController.BasicImageViewWrapper;
        //}

        private void deleteDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            if (isFeed)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this Post"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.DELETE_MEDIA_MESSAGE_TITLE));
                if (isTrue)
                {
                    //if (feedModel.GroupId > 0)
                    //{
                    //    new ThradDeleteFeed().StartThread(0, feedModel.GroupId, feedModel.NewsfeedId);
                    //}
                    //else if (feedModel.FriendShortInfoModel != null && feedModel.FriendShortInfoModel.UserTableID > 0)
                    //{
                    //    new ThradDeleteFeed().StartThread(feedModel.FriendShortInfoModel.UserTableID, 0, feedModel.NewsfeedId);
                    //}
                    //else
                    //{
                    //    new ThradDeleteFeed().StartThread(feedModel.NewsfeedId);
                    //}
                    new ThradDeleteFeed().StartThread(feedModel.NewsfeedId);
                }
            }
            else
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this comment"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.DELETE_MEDIA_MESSAGE_TITLE));
                if (isTrue)
                {
                    JObject pakToSend = new JObject();
                    FeedModel feedModel = null;
                    bool ImageIdToContentId = false;
                    pakToSend[JsonKeys.Action] = AppConstants.ACTION_MERGED_DELETE_COMMENT;
                    pakToSend[JsonKeys.CommentId] = commentModel.CommentId;

                    if (commentModel.ImageId != Guid.Empty)     //// DELETE COMMENT OF IMAGE
                    {
                        ImageModel imageModel = null;
                        ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(commentModel.ImageId, out imageModel);
                        bool fromFeed = (ViewConstants.NavigateFrom == StatusConstants.NAVIGATE_FROM_FEED) ? true : false;

                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_IMAGE;
                        pakToSend[JsonKeys.OwnerId] = imageModel.UserShortInfoModel.UserTableID;
                        pakToSend[JsonKeys.ContentId] = commentModel.ImageId;
                        ImageIdToContentId = true;
                        if (fromFeed)
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            if (imageModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(imageModel.NewsFeedId, out feedModel))
                            {
                                pakToSend[JsonKeys.NewsfeedId] = imageModel.NewsFeedId;
                                pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                                pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            }
                        }
                        else
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                            pakToSend[JsonKeys.AlbumId] = imageModel.AlbumId;
                            pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                        }
                    }
                    else if (commentModel.ContentId != Guid.Empty)     //// DELETE COMMENT OF MULTIMEDIA
                    {
                        SingleMediaModel singleMediaModel = null;
                        MediaDataContainer.Instance.ContentModels.TryGetValue(commentModel.ContentId, out singleMediaModel);
                        bool fromFeed = singleMediaModel.PlayedFromFeed ? true : false;

                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_MULTIMEDIA;
                        pakToSend[JsonKeys.OwnerId] = singleMediaModel.MediaOwner.UserTableID;
                        pakToSend[JsonKeys.ContentId] = singleMediaModel.ContentId;
                        if (fromFeed)
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            if (singleMediaModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(singleMediaModel.NewsFeedId, out feedModel))
                            {
                                pakToSend[JsonKeys.NewsfeedId] = singleMediaModel.NewsFeedId;
                                pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                                pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            }
                        }
                        else
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                            pakToSend[JsonKeys.AlbumId] = singleMediaModel.AlbumId;
                            pakToSend[JsonKeys.MediaType] = singleMediaModel.MediaType;
                        }
                    }
                    else      //// DELETE COMMENT OF STATUS
                    {
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_STATUS;
                        if (commentModel.NewsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(commentModel.NewsfeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            pakToSend[JsonKeys.NewsfeedId] = commentModel.NewsfeedId;
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            pakToSend[JsonKeys.OwnerId] = feedModel.PostOwner.UserTableID;

                            if (feedModel.BookPostType == 2 || feedModel.BookPostType == 5 || feedModel.BookPostType == 8)
                            {
                                if (feedModel.SingleMediaFeedModel != null)
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleMediaFeedModel.ContentId;

                                else if (feedModel.SingleImageFeedModel != null)
                                {
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleImageFeedModel.ImageId;
                                    ImageIdToContentId = true;
                                }
                            }
                        }
                    }
                    new ThreadDeleteAnyComment(pakToSend, ImageIdToContentId).StartThread();
                }
            }
        }

        private void hideFeedDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            bool isTrue = UIHelperMethods.ShowQuestion(NotificationMessages.SINGLE_FEED_HIDE_MESSAGE, String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.SINGLE_FEED_HIDE_MESSAGE_TITLE));
            if (isTrue)
            {
                MainSwitcher.ThreadManager().HideFeed.StartThread(feedModel);
            }
            popupOptions.IsOpen = false;
        }

        private void hideAllFeedDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string fullName = feedModel.PostOwner.FullName;
            bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.SINGLE_FEED_HIDE_USER_MESSAGE, fullName), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.SINGLE_FEED_HIDE_MESSAGE_TITLE));
            if (isTrue)
            {
                MainSwitcher.ThreadManager().HideUser.StartThread(feedModel.PostOwner.UserTableID, feedModel.PostOwner);
            }
            popupOptions.IsOpen = false;
        }

        private void reportDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(this.feedModel.NewsfeedId, StatusConstants.SPAM_FEED, 0);
        }

        private void saveUnsaveDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            if (IsSaved)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.SURE_WANT_TO, NotificationMessages.SINGLE_FEED_UNSAVE_MESSAGE), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages. SINGLE_FEED_UNSAVE_MESSAGE_TITLE));
                if (isTrue)
                {
                    List<Guid> lst = new List<Guid>();
                    lst.Add(feedModel.NewsfeedId);
                    //int ptype = 0;
                    //if (feedModel.BookPostType == SettingsConstants.NEWS_FEED_TYPE_NEWSPORTAL) ptype = SettingsConstants.PROFILE_TYPE_NEWSPORTAL;
                    //else if (feedModel.BookPostType == SettingsConstants.NEWS_FEED_TYPE_PAGES) ptype = SettingsConstants.PROFILE_TYPE_PAGES;
                    new ThradSaveUnsaveNewsPortalFeed(2, lst, 0, feedModel).StartThread();
                }
            }
            else
            {
                List<Guid> lst = new List<Guid>();
                lst.Add(feedModel.NewsfeedId);
                //int ptype = 0;
                //if (feedModel.BookPostType == SettingsConstants.NEWS_FEED_TYPE_NEWSPORTAL) ptype = SettingsConstants.PROFILE_TYPE_NEWSPORTAL;
                //else if (feedModel.BookPostType == SettingsConstants.NEWS_FEED_TYPE_PAGES) ptype = SettingsConstants.PROFILE_TYPE_PAGES;
                new ThradSaveUnsaveNewsPortalFeed(1, lst, 0, feedModel).StartThread();
            }
            //ClosePopup();
        }

        private void followUnfollowDock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupOptions.IsOpen = false;
        }

        private ICommand _PopupOptionClosed;
        public ICommand PopupOptionClosed
        {
            get
            {
                if (_PopupOptionClosed == null) _PopupOptionClosed = new RelayCommand(param => OnPopupOptionsClosed());
                return _PopupOptionClosed;
            }
        }
        private void OnPopupOptionsClosed()
        {
            ClosePopup();
            Hide();
        }
        #endregion

        #region "Utility Methods"

        //private void DeleteComment(long NfId, long ImageId, long ContentId, long cmntId)
        //{
        //    new ThreadDeleteAnyComment(NfId, ImageId, ContentId, cmntId).StartThread();
        //}

        //private void DeleteFeed(long FriendIdentity, long CircleId, long NfId)
        //{
        //    new ThradDeleteFeed().StartThread(FriendIdentity, CircleId, NfId);
        //}
        //private void DeleteFeed(long NfId)
        //{
        //    new ThradDeleteFeed().StartThread(NfId);
        //}

        #endregion "Utility Methods"

    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

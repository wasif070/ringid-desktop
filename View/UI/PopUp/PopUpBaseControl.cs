﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using View.Utility;

namespace View.UI.PopUp
{
    public abstract class PopUpBaseControl : UserControl
    {
        #region "Fields"
        private Grid _parentGrid;
        private Button ExitButton;
        //public delegate void RemovedUserControl();
        public event DelegateNoParam OnRemovedUserControl;
        #endregion "Fields"

        #region "Utility Methods"

        public void InitPopUpBaseControl(string backGroundColor, string dataGridBG, double dataGridBorderThikness, string borderbrush = null, bool setVerticalAlingmentCenter = true)
        {
            var _MainBorder = this.FindName("_MainBorder") as Border;
            if (!string.IsNullOrEmpty(backGroundColor) && _MainBorder != null)
            {
                _MainBorder.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(backGroundColor));
            }
            var _MainBorder2 = this.FindName("_MainBorder2") as Border;
            if (_MainBorder2 != null)
            {
                if (borderbrush != null)
                {
                    _MainBorder2.BorderBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom(borderbrush));
                }
                else _MainBorder2.BorderBrush = Brushes.Black;
                if (setVerticalAlingmentCenter)
                    _MainBorder2.VerticalAlignment = VerticalAlignment.Center;
                _MainBorder2.HorizontalAlignment = HorizontalAlignment.Center;
                _MainBorder2.BorderThickness = new Thickness(dataGridBorderThikness);
                if (!string.IsNullOrEmpty(dataGridBG))
                    _MainBorder2.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(dataGridBG));
                _MainBorder2.MinHeight = 80;
                _MainBorder2.MinWidth = 100;
            }
            this.ExitButton = this.FindName("_ExitButton") as Button;
        }

        #region EventTriggers

        void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        #endregion

        public void SetParent(Grid parentGrid)
        {
            this._parentGrid = parentGrid;
        }

        public Grid GetParent
        {
            get { return this._parentGrid; }
        }

        public void Show()
        {
            this.IsEnabled = true;
            if (_parentGrid != null)
            {
                _parentGrid.Children.Add(this);
            }
            if (ExitButton != null)
                ExitButton.Click += ExitButton_Click;
        }

        public void Hide()
        {
            this.Focusable = false;
            if (ExitButton != null)
                ExitButton.Click -= ExitButton_Click;
            if (_parentGrid != null)
            {
                _parentGrid.Children.Remove(this);
            }
            if (OnRemovedUserControl != null)
                OnRemovedUserControl();
            GC.Collect();
        }
        #endregion "Utility methods"
    }
}

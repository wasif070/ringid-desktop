﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Constants;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCFeedLocationView.xaml
    /// </summary>
    public partial class UCFeedLocationView : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFeedLocationView).Name);

        public static UCFeedLocationView Instance;
        private double _Latitude;
        private double _Longitude;
        private string loc;

        #region "Constructor"
        public UCFeedLocationView(Grid motherPanel)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, null, 0.0);//background Color Transparent Black
            SetParent(motherPanel);
        }
        #endregion
        #region "Property"
        private string _Loader;
        public string Loader
        {
            get
            {
                return _Loader;
            }
            set
            {
                if (value == _Loader)
                    return;
                _Loader = value;
                OnPropertyChanged("Loader");
            }
        }

        private string _LocationName;
        public string LocationName
        {
            get
            {
                return _LocationName;
            }
            set
            {
                if (value == _LocationName)
                    return;
                _LocationName = value;
                OnPropertyChanged("LocationName");
            }
        }
        #endregion
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region "Public Methods"
        public void ShowLocationView(double latitude, double longitude, string loc)
        {
            _Latitude = latitude;
            _Longitude = longitude;
            this.loc = loc;
            LocationName = loc;

            if (Loader == null)
            {
                Loader = ImageLocation.LOADER_FEED_CYCLE;
            }
            ShowMapUsingLatLng();
            mapBrowser.DocumentCompleted += Browser_DocumentCompleted;
        }
        #endregion

        #region "Private Method"
        private void ShowMapUsingLatLng()
        {
            try
            {
                string html = String.Empty;
                using (System.IO.Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("View.UI.Feed.Location.Templete"))
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }

                //html = html.Replace("[zoom]", _Zoom.ToString());
                html = html.Replace("[geoCode]", _Latitude.ToString() + ", " + _Longitude.ToString());
                html = html.Replace("[loctn]", loc);
                mapBrowser.DocumentText = html;
            }
            catch (Exception ex)
            {

                log.Error("Error: ShowMapUsingLatLng() => " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        private void Dispose()
        {
            mapBrowser.DocumentCompleted -= Browser_DocumentCompleted;
            mapBrowser.Dispose();
            Instance = null;
        }
        #endregion

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }

        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }
        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => OnCloseCommand());
                }
                return _CloseCommand;
            }
        }

        public void OnCloseCommand()
        {
            Dispose();
            Hide();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {

        }

        #endregion

        private void Browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (Loader != null)
            {
                Loader = null;
                //_FeedModel.PopupLikeLoaderVisibility = Visibility.Visible;
            }
            FormContainer.Visibility = Visibility.Visible;
            mapBrowser.ObjectForScripting = new HtmlInteropInternal();
        }

    }

    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public class HtmlInteropInternal
    {
        public void endDragMarkerCS(float Lat, float Lng)
        {

        }
    }
}

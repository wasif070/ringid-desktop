﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;
using View.UI.Feed;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCEditStatusCommentView.xaml
    /// </summary>
    public partial class UCEditStatusCommentView : PopUpBaseControl, INotifyPropertyChanged
    {
        #region Private Variables

        private int editFeedWallType = AppConstants.EDIT_WALL_TYPE;
        private bool isShared = false;

        #endregion

        #region Constructor

        public UCEditStatusCommentView(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, null, 0.0);//background Color Transparent Black
            SetParent(motherGrid);
        }

        #endregion

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property

        private FeedModel _feedModel;
        public FeedModel FeedModel
        {
            get
            {
                return _feedModel;
            }
            set
            {
                if (_feedModel == value)
                {
                    return;
                }
                _feedModel = value;
                OnPropertyChanged("FeedModel");
            }
        }

        private NewStatusView _newStatusView = null;
        public NewStatusView NewStatusView
        {
            get { return _newStatusView; }
            set
            {
                if (_newStatusView == value)
                    return;
                _newStatusView = value;
                this.OnPropertyChanged("NewStatusView");
            }
        }

        private NewStatusViewModel _newStatusViewModel = null;
        public NewStatusViewModel NewStatusViewModel
        {
            get { return _newStatusViewModel; }
            set
            {
                if (_newStatusViewModel == value)
                {
                    return;
                }
                _newStatusViewModel = value;
                this.OnPropertyChanged("NewStatusViewModel");
            }
        }

        private Visibility _loaderVisibility = Visibility.Visible;
        public Visibility LoaderVisibility
        {
            get
            {
                return _loaderVisibility;
            }
            set
            {
                if (_loaderVisibility == value)
                {
                    return;
                }
                _loaderVisibility = value;
                this.OnPropertyChanged("IsLoaderVisible");
            }
        }

        private Visibility _editScrollVisibility = Visibility.Collapsed;
        public Visibility EditScrollVisibility
        {
            get
            {
                return _editScrollVisibility;
            }
            set
            {
                if (_editScrollVisibility == value)
                {
                    return;
                }
                _editScrollVisibility = value;
                this.OnPropertyChanged("EditScrollVisibility");
            }
        }

        #endregion

        #region Public Methods

        public void ActionEditFeed()
        {
            try
            {
                int privacy = 0, validity = 0;
                bool isChanged = false;
                LocationModel locationModel = null;
                string status = null;
                JArray innerTags = null;
                JArray AddedOuterTagsJArray = null, RemovedOuterTagsJArray = null;

                JArray EditedImagesJArray = null, RemovedImagesJArray = null;
                JArray EditedMediasJArray = null, RemovedMediasJArray = null, AddedHashTagsJArray = null;
                JArray moodId = null;
                List<ImageUploaderModel> AddedImages = null;
                List<MusicUploaderModel> AddedMusics = null;
                List<FeedVideoUploaderModel> AddedVideos = null;

                //Status Text Edit
                NewStatusView.RtbNewStatusArea.SetStatusandTags();
                if (!NewStatusView.RtbNewStatusArea.StringWithoutTags.Trim().Equals(FeedModel.Status) || !HelperMethods.IsTagDTOsSameasJArray(NewStatusView.RtbNewStatusArea.TagsJArray, FeedModel.StatusTags))
                {
                    isChanged = true;
                    status = NewStatusView.RtbNewStatusArea.StringWithoutTags;
                    innerTags = NewStatusView.RtbNewStatusArea.TagsJArray;
                }

                //Tagged Friends Edit
                if (NewStatusViewModel.TaggedFriends != null && NewStatusViewModel.TaggedFriends.Count > 0)
                {
                    foreach (var item in NewStatusViewModel.TaggedFriends)
                    {
                        if (FeedModel.TaggedFriendsList == null || FeedModel.TaggedFriendsList.Count >= 0 || (FeedModel.TaggedFriendsList.Count > 0 && !FeedModel.TaggedFriendsList.Any(P => P.UserTableID == item.UserTableID)))
                        {
                            if (AddedOuterTagsJArray == null) AddedOuterTagsJArray = new JArray();
                            AddedOuterTagsJArray.Add(item.UserTableID);
                            isChanged = true;
                        }
                    }
                }
                if (FeedModel.TaggedFriendsList != null && FeedModel.TaggedFriendsList.Count > 0)
                {
                    foreach (var item in FeedModel.TaggedFriendsList)
                    {
                        if (!NewStatusViewModel.TaggedFriends.Any(P => P.UserTableID == item.UserTableID))
                        {
                            if (RemovedOuterTagsJArray == null) RemovedOuterTagsJArray = new JArray();
                            RemovedOuterTagsJArray.Add(item.UserTableID);
                            isChanged = true;
                        }
                    }
                }

                if (RemovedOuterTagsJArray != null && RemovedOuterTagsJArray.Count > 0)
                {
                    if (AddedOuterTagsJArray == null)
                    {
                        AddedOuterTagsJArray = new JArray();
                        foreach (var item in NewStatusViewModel.TaggedFriends)
                        {
                            AddedOuterTagsJArray.Add(item.UserTableID);
                        }
                    }
                }

                //Location Edit
                if (FeedModel.LocationModel == null && NewStatusViewModel.SelectedLocationModel != null)
                {
                    isChanged = true;
                    locationModel = NewStatusViewModel.SelectedLocationModel;
                }
                else if (FeedModel.LocationModel != null && NewStatusViewModel.SelectedLocationModel != null && FeedModel.LocationModel != NewStatusViewModel.SelectedLocationModel)
                {
                    isChanged = true;
                    locationModel = NewStatusViewModel.SelectedLocationModel;
                }
                else if (FeedModel.LocationModel != null && NewStatusViewModel.SelectedLocationModel == null)
                {
                    isChanged = true;
                    if (locationModel == null)
                    {
                        locationModel = new LocationModel();
                    }
                    locationModel.LocationName = string.Empty;
                    locationModel.Latitude = 0;
                    locationModel.Longitude = 0;
                }

                //Mood Edit
                if (FeedModel != null && NewStatusViewModel.SelectedDoingModel != null && FeedModel.DoingId != NewStatusViewModel.SelectedDoingModel.ID)
                {
                    moodId = new JArray();
                    moodId.Add(NewStatusViewModel.SelectedDoingModel.ID);
                    isChanged = true;
                }
                else if (FeedModel.DoingId > 0 && NewStatusViewModel.SelectedDoingModel == null)
                {
                    moodId = new JArray();
                    isChanged = true;
                }

                //Validity
                if (FeedModel.Validity != NewStatusViewModel.ValidityValue)
                {
                    isChanged = true;
                }
                validity = NewStatusViewModel.ValidityValue;

                if ((moodId == null || moodId.Count == 0)
                    && string.IsNullOrEmpty(NewStatusView.RtbNewStatusArea.Text.Trim())
                    && NewStatusViewModel.SelectedLocationModel == null
                    && (NewStatusViewModel.TaggedFriends == null || NewStatusViewModel.TaggedFriends.Count == 0)
                    && (NewStatusViewModel.NewStatusImageUpload == null || NewStatusViewModel.NewStatusImageUpload.Count == 0)
                    && (NewStatusViewModel.NewStatusMusicUpload == null || NewStatusViewModel.NewStatusMusicUpload.Count == 0)
                    && (NewStatusViewModel.NewStatusVideoUpload == null || NewStatusViewModel.NewStatusVideoUpload.Count == 0)
                    && !isShared)
                {
                    UIHelperMethods.ShowWarning("No Content In The Feed.", "You May Want To Delete The Status.", "Edit failed!");
                }
                else if (!isChanged)
                {
                    UIHelperMethods.ShowWarning(NotificationMessages.ABOUT_NO_CHANGE, "Edit failed!");
                }
                else
                {
                    if (string.IsNullOrEmpty(status))
                    {
                        status = NewStatusView.RtbNewStatusArea.StringWithoutTags;
                    }
                    NewStatusViewModel.EnablePostbutton(false);
                    MainSwitcher.ThreadManager().EditFeed.SetEditedFields(FeedModel.NewsfeedId, status, innerTags, AddedOuterTagsJArray, RemovedOuterTagsJArray, privacy, locationModel, validity, AddedImages, EditedImagesJArray, RemovedImagesJArray, AddedMusics, AddedVideos, EditedMediasJArray, RemovedMediasJArray, AddedHashTagsJArray, moodId);
                    MainSwitcher.ThreadManager().EditFeed.callBackEvent += (success, msg) =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NewStatusViewModel.EnablePostbutton(true);
                            if (success)
                            {
                                HideEditStatusView();
                                Hide();
                            }
                            else
                            {
                                UIHelperMethods.ShowWarning(msg, "Edit failed!");
                            }
                        });
                    };
                    MainSwitcher.ThreadManager().EditFeed.StartThread();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message : " + ex.Message + "StackTrace : " + ex.StackTrace);
            }
        }

        public void HideEditStatusView()
        {
            Visibility = Visibility.Hidden;
            NewStatusView.RtbNewStatusArea.Document.Blocks.Clear();
            if (NewStatusViewModel.NewStatusImageUpload != null && NewStatusViewModel.NewStatusImageUpload.Count > 0)
            {
                NewStatusViewModel.NewStatusImageUpload.Clear();
                NewStatusViewModel.StatusImagePanelVisibility = Visibility.Collapsed;
            }
            if (NewStatusViewModel.NewStatusMusicUpload != null && NewStatusViewModel.NewStatusMusicUpload.Count > 0)
            {
                NewStatusViewModel.AudioPanelVisibility = Visibility.Collapsed;
                NewStatusViewModel.NewStatusMusicUpload.Clear();
            }
            if (NewStatusViewModel.NewStatusVideoUpload != null && NewStatusViewModel.NewStatusVideoUpload.Count > 0)
            {
                NewStatusViewModel.NewStatusVideoUpload.Clear();
                NewStatusViewModel.VideoPanelVisibility = Visibility.Collapsed;
            }
            if (NewStatusViewModel.TaggedFriends != null && NewStatusViewModel.TaggedFriends.Count > 0)
            {
                NewStatusViewModel.TaggedFriends.Clear();
                NewStatusViewModel.TagListHolderVisibility = Visibility.Collapsed;
            }
            if (NewStatusViewModel.SelectedLocationModel != null)
            {
                NewStatusViewModel.SelectedLocationModel = null;
                NewStatusViewModel.LocationMainBorderPanelVisibility = Visibility.Collapsed;
            }
            if (NewStatusViewModel.SelectedDoingModel != null && NewStatusViewModel.SelectedDoingModel.ID > 0)
            {
                NewStatusViewModel.SelectedDoingModel = null;
                NewStatusViewModel.FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
            }
            LoaderVisibility = Visibility.Visible;
            EditScrollVisibility = Visibility.Collapsed;
        }

        public void LoadNewStatusFromFeedModel()
        {
            NewStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
            isShared = FeedModel.ParentFeed == null ? false : true;
            if (FeedModel.Status != null && FeedModel.Status.Length > 0)
            {
                NewStatusView.RtbNewStatusArea.AppendText(FeedModel.Status);
            }
            if (FeedModel.StatusTags != null && FeedModel.StatusTags.Count > 0)
            {
                NewStatusView.RtbNewStatusArea.TagsHandle(FeedModel.StatusTags);
                foreach (var item in FeedModel.StatusTags)
                {
                    NewStatusView.RtbNewStatusArea._AddedFriendsUtid.Add(item.UserTableID);
                }
            }
            NewStatusViewModel.WriteSomethingTextBlockVisibility = (NewStatusView.RtbNewStatusArea.Text.Length > 0) ? Visibility.Collapsed : Visibility.Visible;

            if (FeedModel.DoingImageUrl != null && FeedModel.DoingId > 0)
            {
                DoingModel doingModel = new DoingModel();
                doingModel.ID = FeedModel.DoingId;
                doingModel.ImgUrl = FeedModel.DoingImageUrl;
                doingModel.Name = FeedModel.DoingActivity;
                NewStatusViewModel.SelectedDoingModel = doingModel;
            }

            if (FeedModel.TaggedFriendsList != null && FeedModel.TaggedFriendsList.Count > 0)
            {
                if (NewStatusViewModel.TaggedFriends == null)
                {
                    NewStatusViewModel.TaggedFriends = new ObservableCollection<UserShortInfoModel>();
                }
                foreach (var item in FeedModel.TaggedFriendsList)
                {
                    UserShortInfoModel userModel = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(item.UserTableID, item.UserIdentity, item.FullName, item.ProfileImage);
                    NewStatusViewModel.TaggedFriends.Add(userModel);
                }
                NewStatusViewModel.OnPropertyChanged("TaggedFriends");
            }
            NewStatusViewModel.PrivacyValue = FeedModel.Privacy;

            NewStatusViewModel.ValidityValue = FeedModel.Validity;
            switch (FeedModel.Validity)
            {
                case AppConstants.DEFAULT_VALIDITY:
                    NewStatusViewModel.ValidityToolTipText = "Post Validity Unlimited";
                    break;
                case 1:
                    NewStatusViewModel.ValidityToolTipText = "Post Validity 1 Day";
                    break;
                default:
                    NewStatusViewModel.ValidityToolTipText = "Post Validity " + FeedModel.Validity + " Days";
                    break;
            }

            if (FeedModel.LocationModel != null)
            {
                NewStatusViewModel.IsLocationSet = true;
                NewStatusViewModel.SelectedLocationModel = FeedModel.LocationModel;
                NewStatusViewModel.LocationTextBoxText = FeedModel.LocationModel.LocationName;
            }

            NewStatusViewModel.NewStatusMusicButtonVisibility = Visibility.Collapsed;
            NewStatusViewModel.NewStatusVideoButtonVisibility = Visibility.Collapsed;
            NewStatusViewModel.UploadPhotoButtonVisibility = Visibility.Collapsed;

            if (FeedModel.ImageList != null && FeedModel.ImageList.Count > 0)
            {
                foreach (ImageModel model in FeedModel.ImageList)
                {
                    string convertedUrl = HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_600);
                    convertedUrl = convertedUrl.Replace("/", "_");

                    if (model.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                    {
                        convertedUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    }
                    else if (model.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
                    {
                        convertedUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    }
                    else
                    {
                        convertedUrl = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    }
                    ImageUploaderModel imageUploaderModel = new ImageUploaderModel
                    {
                        FilePath = convertedUrl,
                        IsFromUpload = false,
                        ImageCaption = model.Caption,
                        CaptionEnabled = true,
                        ImageUrl = model.ImageUrl,
                        ImageId = model.ImageId,
                        FileSize = FeedModel.ImageList.Count
                    };
                    NewStatusViewModel.NewStatusImageUpload.Add(imageUploaderModel);
                    NewStatusViewModel.SetStatusImagePanelVisibility();
                    NewStatusViewModel.UploadingText = "Selected Image(s): " + NewStatusViewModel.NewStatusImageUpload.Count;
                    NewStatusViewModel.ImageTitlePanelVisibility = Visibility.Collapsed;
                }
            }

            if (FeedModel.MediaContent != null && FeedModel.MediaContent.MediaList != null && FeedModel.MediaContent.MediaList.Count > 0 && FeedModel.FeedSubType == 0)
            {
                if (FeedModel.MediaContent.TotalMediaCount > FeedModel.MediaContent.MediaList.Count)
                {
                    ThreadFetchExtraMedia threadFetchExtraMedia = new ThreadFetchExtraMedia();
                    threadFetchExtraMedia.CallBack += (response, mediaArray) =>
                    {
                        if (response == SettingsConstants.RESPONSE_SUCCESS)
                        {
                            foreach (JObject singleMediaObj in mediaArray)
                            {
                                Guid contentId = (Guid)singleMediaObj[JsonKeys.ContentId];
                                SingleMediaModel singleMediaModel = null;
                                if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel))
                                {
                                    singleMediaModel = new SingleMediaModel();
                                    MediaDataContainer.Instance.ContentModels[contentId] = singleMediaModel;
                                }
                                singleMediaModel.LoadData(singleMediaObj, singleMediaModel.MediaType);
                                if (singleMediaModel.MediaOwner == null)
                                {
                                    singleMediaModel.MediaOwner = FeedModel.PostOwner;
                                }
                                singleMediaModel.NewsFeedId = FeedModel.NewsfeedId;
                                singleMediaModel.MediaType = FeedModel.MediaContent.MediaType;
                                FeedModel.MediaContent.MediaList.InvokeAddNoDuplicate(singleMediaModel);
                                FeedModel.MediaContent.FeedMediaList.InvokeAddNoDuplicate(singleMediaModel);
                                Application.Current.Dispatcher.Invoke((Action)(() =>
                                {
                                    if (singleMediaModel.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                                    {
                                        MusicUploaderModel musicUploaderModel = NewStatusViewModel.NewStatusMusicUpload.Where(x => string.IsNullOrEmpty(x.FilePath)).FirstOrDefault();
                                        if (musicUploaderModel != null)
                                        {
                                            musicUploaderModel.AudioTitle = singleMediaModel.Title;
                                            musicUploaderModel.AudioArtist = singleMediaModel.Artist;
                                            musicUploaderModel.AudioDuration = singleMediaModel.Duration;
                                            musicUploaderModel.FilePath = singleMediaModel.StreamUrl;
                                            musicUploaderModel.ContentId = singleMediaModel.ContentId;
                                            musicUploaderModel.ThumbUrl = singleMediaModel.ThumbUrl;
                                            musicUploaderModel.IsImageFound = !string.IsNullOrEmpty(singleMediaModel.ThumbUrl) ? true : false;

                                        }
                                    }
                                    else
                                    {
                                        FeedVideoUploaderModel feedVideoUploaderModel = NewStatusViewModel.NewStatusVideoUpload.Where(x => string.IsNullOrEmpty(x.FilePath)).FirstOrDefault();
                                        if (feedVideoUploaderModel != null)
                                        {
                                            feedVideoUploaderModel.VideoTitle = singleMediaModel.Title;
                                            feedVideoUploaderModel.VideoArtist = singleMediaModel.Artist;
                                            feedVideoUploaderModel.VideoDuration = singleMediaModel.Duration;
                                            feedVideoUploaderModel.FilePath = singleMediaModel.StreamUrl;
                                            feedVideoUploaderModel.ContentId = singleMediaModel.ContentId;
                                            feedVideoUploaderModel.ThumbUrl = singleMediaModel.ThumbUrl;
                                            var synchronizationContext = SynchronizationContext.Current;
                                            new Thread(() =>
                                            {
                                                var imageFilePath = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + feedVideoUploaderModel.ThumbUrl.Replace("/", "_");
                                                var stream = HelperMethods.DownloadImage(new Uri(ServerAndPortSettings.IMAGE_SERVER_RESOURCE + feedVideoUploaderModel.ThumbUrl, UriKind.Absolute), imageFilePath);
                                                synchronizationContext.Send(p =>
                                                {
                                                    BitmapImage bitmapImage = new BitmapImage();
                                                    bitmapImage.BeginInit();
                                                    bitmapImage.StreamSource = (System.IO.Stream)p;
                                                    bitmapImage.EndInit();
                                                    feedVideoUploaderModel.VideoThumbnail = bitmapImage;
                                                }, stream);
                                            }).Start();
                                        }
                                    }
                                }));
                            }
                        }
                    };
                    threadFetchExtraMedia.StartThread(FeedModel.NewsfeedId, 2, 0);
                }
                if (FeedModel.MediaContent.MediaType == 1)
                {
                    foreach (SingleMediaModel model in FeedModel.MediaContent.MediaList)
                    {
                        MusicUploaderModel musicUploaderModel = new MusicUploaderModel
                        {
                            AudioTitle = model.Title,
                            AudioArtist = model.Artist,
                            AudioDuration = model.Duration,
                            FilePath = model.StreamUrl,
                            ContentId = model.ContentId,
                            ThumbUrl = model.ThumbUrl,
                            IsImageFound = !string.IsNullOrEmpty(model.ThumbUrl) ? true : false
                        };
                        NewStatusViewModel.NewStatusMusicUpload.Add(musicUploaderModel);
                    }
                    for (int i = NewStatusViewModel.NewStatusMusicUpload.Count; i < FeedModel.MediaContent.TotalMediaCount; i++)
                    {
                        NewStatusViewModel.NewStatusMusicUpload.Add(new MusicUploaderModel());
                    }
                    NewStatusViewModel.AudioPanelVisibility = Visibility.Visible;
                    NewStatusViewModel.AudioTitlePanelVisibility = Visibility.Collapsed;
                    NewStatusViewModel.AudioHashTagGridPanelVisibility = Visibility.Collapsed;
                }
                else if (FeedModel.MediaContent.MediaType == 2)
                {
                    foreach (SingleMediaModel model in FeedModel.MediaContent.MediaList)
                    {
                        FeedVideoUploaderModel feedVideoUploaderModel = new FeedVideoUploaderModel
                        {
                            VideoTitle = model.Title,
                            VideoArtist = model.Artist,
                            VideoDuration = model.Duration,
                            FilePath = model.StreamUrl,
                            ContentId = model.ContentId,
                            ThumbUrl = model.ThumbUrl
                        };
                        var sc = SynchronizationContext.Current;
                        new Thread(() =>
                        {
                            var imageFilePath = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + feedVideoUploaderModel.ThumbUrl.Replace("/", "_");
                            var stream = HelperMethods.DownloadImage(new Uri(ServerAndPortSettings.IMAGE_SERVER_RESOURCE + feedVideoUploaderModel.ThumbUrl, UriKind.Absolute), imageFilePath);
                            sc.Send(p =>
                            {
                                BitmapImage bitmapImage = new BitmapImage();
                                bitmapImage.BeginInit();
                                bitmapImage.StreamSource = (System.IO.Stream)p;
                                bitmapImage.EndInit();
                                feedVideoUploaderModel.VideoThumbnail = bitmapImage;
                            }, stream);
                        }).Start();
                        NewStatusViewModel.NewStatusVideoUpload.Add(feedVideoUploaderModel);
                    }
                    for (int i = NewStatusViewModel.NewStatusVideoUpload.Count; i < FeedModel.MediaContent.TotalMediaCount; i++)
                    {
                        NewStatusViewModel.NewStatusVideoUpload.Add(new FeedVideoUploaderModel());
                    }
                    NewStatusViewModel.VideoPanelVisibility = Visibility.Visible;
                    NewStatusViewModel.VideoTitlePanelVisibility = Visibility.Collapsed;
                    NewStatusViewModel.VideoAddHAshTagGridPanelVisibility = Visibility.Collapsed;
                }
            }
            EditScrollVisibility = Visibility.Visible;
            LoaderVisibility = Visibility.Hidden;
        }

        public void ShowEditStatus(FeedModel model, bool DetailsRequest)
        {
            if (NewStatusView == null)
            {
                InitializeNewStatusInstance();
            }

            if (!containerGrid.Children.Contains(NewStatusView))
            {
                containerGrid.Children.Add(NewStatusView);
            }

            this.FeedModel = (FeedModel)model;
            if (!DetailsRequest)
            {
                LoadNewStatusFromFeedModel();
            }
            Visibility = Visibility.Visible;
        }

        #endregion

        #region Private Methods

        private void InitializeNewStatusInstance()
        {
            NewStatusView = new NewStatusView();
            NewStatusViewModel = (NewStatusViewModel)NewStatusView.DataContext;
            NewStatusViewModel.OptionsButtonVisibility = Visibility.Hidden;
            NewStatusViewModel.PrivacyButtonVisibility = Visibility.Collapsed;
            NewStatusViewModel.UploadPhotoButtonVisibility = Visibility.Collapsed;
            NewStatusViewModel.NewStatusEmoticonVisibility = Visibility.Visible;
            NewStatusViewModel.NewStatusTagFriendButtonVisibility = Visibility.Visible;
            NewStatusViewModel.NewStatusMusicButtonVisibility = Visibility.Collapsed;
            NewStatusViewModel.NewStatusVideoButtonVisibility = Visibility.Collapsed;
            NewStatusViewModel.NewStatusLocationButtonVisibility = Visibility.Visible;
            NewStatusViewModel.FeedWallType = editFeedWallType;
            NewStatusViewModel.WriteOnText = "Update your feed...";
            NewStatusViewModel.WritePostText = "Edit Post";
            NewStatusView.PostButton.Content = "Save";
        }

        #endregion

        #region ICommands

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                loadedControl = loadedControl ?? new RelayCommand(param => onLoadedControl());
                return loadedControl;
            }
        }

        private void onLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapeCommand;
        public ICommand EscapeCommand
        {
            get
            {
                escapeCommand = escapeCommand ?? new RelayCommand(param => onEscapeCommand());
                return escapeCommand;
            }
        }

        private void onEscapeCommand()
        {
            Hide();
        }

        #endregion
    }
}


/*************************************Commented Out Code, May be we will need them later***************************************/
// In action_edit_feed method 
//image
//if (NewStatusViewModel.NewStatusImageUpload != null && NewStatusViewModel.NewStatusImageUpload.Count > 0)
//{
//    foreach (var item in NewStatusViewModel.NewStatusImageUpload)
//    {
//        if (item.ImageId == Guid.Empty)
//        {
//            if (AddedImages == null) AddedImages = new List<ImageUploaderModel>();
//            AddedImages.Add(item);
//        }
//        else //supposedly not exception
//        {
//            ImageModel originalModel = FeedModel.ImageList.Where(P => P.ImageId == item.ImageId).FirstOrDefault();
//            if (originalModel != null && item.ImageCaption != null && originalModel.Caption != null && !originalModel.Caption.Trim().Equals(item.ImageCaption.Trim()))
//            {
//                if (EditedImagesJArray == null) EditedImagesJArray = new JArray();
//                JObject jobj = new JObject();
//                jobj[JsonKeys.ImageCaption] = item.ImageCaption.Trim();
//                jobj[JsonKeys.ImageId] = item.ImageId;
//                EditedImagesJArray.Add(jobj);
//            }
//        }
//    }
//}
//if (FeedModel.ImageList != null && FeedModel.ImageList.Count > 0)
//{
//    foreach (var item in FeedModel.ImageList)
//    {
//        if (!NewStatusViewModel.NewStatusImageUpload.Any(P => P.ImageId == item.ImageId))
//        {
//            if (RemovedImagesJArray == null) RemovedImagesJArray = new JArray();
//            RemovedImagesJArray.Add(item.ImageId);
//        }
//    }
//}
//music
//if (NewStatusViewModel.NewStatusMusicUpload != null && NewStatusViewModel.NewStatusMusicUpload.Count > 0)
//{
//    foreach (var item in NewStatusViewModel.NewStatusMusicUpload)
//    {
//        if (item.ContentId == Guid.Empty)
//        {
//            if (AddedMusics == null) AddedMusics = new List<MusicUploaderModel>();
//            AddedMusics.Add(item);
//        }
//        else //supposedly not exception
//        {
//            SingleMediaModel originalModel = FeedModel.MediaContent.MediaList.Where(P => P.ContentId == item.ContentId).FirstOrDefault();
//            if (originalModel != null && !originalModel.Title.Trim().Equals(item.AudioTitle.Trim()))
//            {
//                if (EditedMediasJArray == null) EditedMediasJArray = new JArray();
//                JObject jobj = new JObject();
//                jobj[JsonKeys.Title] = item.AudioTitle.Trim();
//                jobj[JsonKeys.ContentId] = item.ContentId;
//                EditedMediasJArray.Add(jobj);
//            }
//        }
//    }
//    if (NewStatusViewModel.NewStatusHashTag != null && NewStatusViewModel.NewStatusHashTag.Count > 0)
//    {
//        if (AddedHashTagsJArray == null) AddedHashTagsJArray = new JArray();
//        foreach (var item in NewStatusViewModel.NewStatusHashTag)
//        {
//            AddedHashTagsJArray.Add(item.HashTagId);
//        }
//    }
//}
//video
//if (NewStatusViewModel.NewStatusVideoUpload != null && NewStatusViewModel.NewStatusVideoUpload.Count > 0)
//{
//    foreach (var item in NewStatusViewModel.NewStatusVideoUpload)
//    {
//        if (item.ContentId == Guid.Empty)
//        {
//            if (AddedVideos == null) AddedVideos = new List<FeedVideoUploaderModel>();
//            AddedVideos.Add(item);
//        }
//        else //supposedly not exception
//        {
//            SingleMediaModel originalModel = FeedModel.MediaContent.MediaList.Where(P => P.ContentId == item.ContentId).FirstOrDefault();
//            if (originalModel != null && !originalModel.Title.Trim().Equals(item.VideoTitle.Trim()))
//            {
//                if (EditedMediasJArray == null) EditedMediasJArray = new JArray();
//                JObject jobj = new JObject();
//                jobj[JsonKeys.Title] = item.VideoTitle.Trim();
//                jobj[JsonKeys.ContentId] = item.ContentId;
//                EditedMediasJArray.Add(jobj);
//            }
//        }
//    }
//    if (NewStatusViewModel.NewStatusHashTag != null && NewStatusViewModel.NewStatusHashTag.Count > 0)
//    {
//        if (AddedHashTagsJArray == null) AddedHashTagsJArray = new JArray();
//        foreach (var item in NewStatusViewModel.NewStatusHashTag)
//        {
//            AddedHashTagsJArray.Add(item.HashTagId);
//        }
//    }
//}
////
//if (FeedModel.MediaContent != null && FeedModel.MediaContent.MediaList != null && FeedModel.MediaContent.MediaList.Count > 0)
//{
//    if (FeedModel.MediaContent.MediaType == 1)
//        foreach (var item in FeedModel.MediaContent.MediaList)
//        {
//            if (!NewStatusViewModel.NewStatusMusicUpload.Any(P => P.ContentId == item.ContentId))
//            {
//                if (RemovedMediasJArray == null) RemovedMediasJArray = new JArray();
//                RemovedMediasJArray.Add(item.ContentId);
//            }
//        }
//    else if (FeedModel.MediaContent.MediaType == 2)
//        foreach (var item in FeedModel.MediaContent.MediaList)
//        {
//            if (!NewStatusViewModel.NewStatusVideoUpload.Any(P => P.ContentId == item.ContentId))
//            {
//                if (RemovedMediasJArray == null) RemovedMediasJArray = new JArray();
//                RemovedMediasJArray.Add(item.ContentId);
//            }
//        }
//}
/**************************************************************************************************************************/

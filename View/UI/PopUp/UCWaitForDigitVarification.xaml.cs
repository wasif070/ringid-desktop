﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Auth.Service.SigninSignup;
using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using View.Constants;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCWaitForDigitVarification.xaml
    /// </summary>
    public partial class UCWaitForDigitVarification : PopUpBaseControl, INotifyPropertyChanged
    {
        #region "Fields"
        string defaultWatiText = "There is a mobile number verify page in your default web browser. Please verify your{0}number there within";
        EventHandler handler;
        public System.Windows.Forms.Timer remainingTimeCounter = null;
        int ticksCount = 0;
        public bool stopThread = false;
        long ringID;
        int digitVerifyType;//1=signup,2=recover,3=profile
        public event DelegateBoolThreeString OnCompletedDigitsVarifications;
        bool successfullyVerified = false;
        string mbl = null, mblDc = null;
        string previousRingId = null;

        public int DigitsVerify_Signup = 1;
        public int DigitsVerify_Recovery = 2;
        public int DigitsVerify_Profile = 3;
        #endregion

        #region"Ctors"
        public UCWaitForDigitVarification(int type = 1, long ringID = 0, string phoneNumber1 = null)
        {
            InitializeComponent();
            this.DataContext = this;
            this.digitVerifyType = type;
            this.ringID = ringID;
            if (!string.IsNullOrEmpty(phoneNumber1))
            {
                phoneNumber1 = HelperMethodsModel.PhoneNumberStarred(phoneNumber1);
                WaitText = String.Format(defaultWatiText, " " + phoneNumber1 + " ");
            }
            else WaitText = String.Format(defaultWatiText, " ");
            PhoneNumber = phoneNumber1;
        }
        #endregion"Ctors"

        #region "Properties"

        private string title = "Waiting for phone number verification";
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }

        private bool pleaseWaitLoader = true;
        public bool PleaseWaitLoader
        {
            get { return this.pleaseWaitLoader; }
            set
            {
                if (this.pleaseWaitLoader == value) return;
                this.pleaseWaitLoader = value; this.OnPropertyChanged("PleaseWaitLoader");
            }
        }

        private int remainingTime;
        public int RemainingTime
        {
            get { return remainingTime; }
            set { remainingTime = value; OnPropertyChanged("RemainingTime"); }
        }

        private int totalSeconds = 120;
        public int TotalSeconds
        {
            get { return totalSeconds; }
            set { totalSeconds = value; OnPropertyChanged("TotalSeconds"); }
        }

        private string waitText = " ";
        public string WaitText
        {
            get { return waitText; }
            set { waitText = value; OnPropertyChanged("WaitText"); }
        }

        private string phoneNumber;
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; OnPropertyChanged("PhoneNumber"); }
        }

        //There is a mobile number verify page in your default web browser. Please verify your{0}number there within
        #endregion

        #region "ICommands and Command Methods"

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            startCounter();
            startDisgitVerificationThread();
        }

        private ICommand escapeCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapeCommand == null) escapeCommand = new RelayCommand(param => OnEscapeCommand(param));
                return escapeCommand;
            }
        }
        public void OnEscapeCommand(object param)
        {
            disposeThis();
            Application.Current.Dispatcher.Invoke(() =>
            {
                Hide();
                onCompleteThread();
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        #endregion"ICommands and Command Methods"

        #region "Utility Methods"

        private void startDisgitVerificationThread()
        {
            Task taskForServercommunications = new Task(() => verifyDigits(null));
            taskForServercommunications.Start();
        }

        private void startCounter()
        {
            ticksCount = 0;
            RemainingTime = totalSeconds;
            if (remainingTimeCounter == null) remainingTimeCounter = new System.Windows.Forms.Timer();
            if (remainingTimeCounter.Enabled) remainingTimeCounter.Stop();
            if (handler == null) handler = new EventHandler(TimerEventProcessor);
            remainingTimeCounter.Tick -= handler;
            remainingTimeCounter.Tick += handler;
            remainingTimeCounter.Interval = 1000;
            remainingTimeCounter.Start();
        }

        private void disposeThis()
        {
            stopThread = true;
            if (remainingTimeCounter != null && remainingTimeCounter.Enabled)
            {
                remainingTimeCounter.Tick -= handler;
                remainingTimeCounter.Stop();
            }
        }

        private void ChangeTime()
        {
            RemainingTime = totalSeconds - ticksCount;
        }

        private void onCompleteThread()
        {
            if (OnCompletedDigitsVarifications != null) OnCompletedDigitsVarifications(successfullyVerified, mbl, mblDc, previousRingId);
        }

        #endregion

        #region"Event Handler"

        private void TimerEventProcessor(object sender, EventArgs e)
        {
            ticksCount++;
            ChangeTime();
            if (ticksCount == totalSeconds) OnEscapeCommand(null);
        }
        /// <summary>
        /// This will open digits api into browser 
        /// if phonNumber is not null or empty a default mobile number will appear into browser mobile number field
        /// i.e if phonNumber = +880-16464646546
        /// </summary>
        /// <param name="phonNumber"></param>
        private void verifyDigits(string phonNumber)
        {
            try
            {
                ViewConstants.DigitsDatas = null;
                if (ringID == 0)
                {
                    CommunicationPortsDTO comports = new SocketPorts().SetSocketPortsWithNewRingId();
                    if (comports != null && !string.IsNullOrEmpty(comports.RingID)) ringID = long.Parse(comports.RingID);
                }
                if (ringID > 0)
                {
                    string access_token = HttpRequest.GetAccessTokenForDigitsVerification(ringID);
                    string number_url = string.IsNullOrEmpty(phonNumber) ? "" : "&x_auth_phone_number=%2B" + phonNumber;
                    string browserUrl = "https://www.digits.com/login?consumer_key=" + SocialMediaConstants.DIGITS_CONSUMER_KEY + "&host=https://www.ringid.com"
                        + number_url
                        + "&callback_url="
                        + ServerAndPortSettings.BASE_WEB_URL  //https%3A%2F%2Fwww.ringid.com%2F
                        + "digits%3Ftoken%3D" + access_token;
                    HelperMethodsAuth.GoToSite(browserUrl);
                    JObject pakToSend = new JObject();
                    JObject feedbackfields = null;
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_DIGITS_VERIFY;
                    pakToSend[JsonKeys.RID] = ringID;
                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_AUTH, data);
                    for (int pakSendingAttempt = 1; pakSendingAttempt <= DefaultSettings.TRYING_TIME; pakSendingAttempt++)
                    {
                        if (stopThread) break;
                        if (ViewConstants.DigitsDatas != null)
                        {
                            if (ViewConstants.DigitsDatas.IsSccess)
                            {
                                successfullyVerified = true;
                                mblDc = ViewConstants.DigitsDatas.MobileDialingCode;
                                mbl = ViewConstants.DigitsDatas.PhoneNumber;
                            }
                            previousRingId = HttpRequest.GetRingIDFromPhone(mbl, mblDc);
                            break;
                        }
                        else if (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields)
                            && feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                            mbl = (feedbackfields[JsonKeys.MobilePhone] != null) ? (string)feedbackfields[JsonKeys.MobilePhone] : "";
                            mblDc = (feedbackfields[JsonKeys.DialingCode] != null) ? (string)feedbackfields[JsonKeys.DialingCode] : "";
                            successfullyVerified = true;
                            previousRingId = HttpRequest.GetRingIDFromPhone(mbl, mblDc);
                            break;
                        }
                        else
                        {
                            if (stopThread) break;
                            Thread.Sleep(5000);
                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_AUTH, data);
                        }
                    }
                    OnEscapeCommand(null);
                }
            }
            finally { ViewConstants.DigitsDatas = null; }
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
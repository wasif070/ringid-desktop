﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCLikeListPopupWrapper.xaml
    /// </summary>
    public partial class UCLikeListPopupWrapper : UserControl
    {
        public static UCLikeListPopupWrapper CurrentInstance;
        public UCLikeListPopup ucLikeListPopup = null;

        public UCLikeListPopupWrapper()
        {
            CurrentInstance = this;
            InitializeComponent();
        }

        public void Show(UIElement target, long total, Guid nfid, int action) //likes for status
        {
            if (ucLikeListPopup == null)
            {
                ucLikeListPopup = new UCLikeListPopup();
                wrapperLikeBorder.Child = ucLikeListPopup;
            }
            ucLikeListPopup.Show(target, total, nfid, action);
        }

        public void Show(UIElement target, long total, Guid nfid, Guid cmntid, int action) //likes for status comment
        {
            if (ucLikeListPopup == null)
            {
                ucLikeListPopup = new UCLikeListPopup();
                wrapperLikeBorder.Child = ucLikeListPopup;
            }
            ucLikeListPopup.Show(target, total, nfid, cmntid, action);
        }

        public void Show(UIElement target, long total, Guid imageId)//likes for image
        {
            CurrentInstance = this;
            if (ucLikeListPopup == null)
            {
                ucLikeListPopup = new UCLikeListPopup();
                wrapperLikeBorder.Child = ucLikeListPopup;
            }
            ucLikeListPopup.Show(target, total, imageId);
        }

        public void Show(UIElement target, Guid imageId, Guid commentId, long total) //likes for image comments
        {
            CurrentInstance = this;
            if (ucLikeListPopup == null)
            {
                ucLikeListPopup = new UCLikeListPopup();
                wrapperLikeBorder.Child = ucLikeListPopup;
            }
            ucLikeListPopup.Show(target, imageId, commentId, total);
        }

        public void Show(UIElement target, long total, Guid nfId, Guid imageId, Guid contentId, Guid commentId)
        {
            CurrentInstance = this;
            if (ucLikeListPopup == null)
            {
                ucLikeListPopup = new UCLikeListPopup();
                wrapperLikeBorder.Child = ucLikeListPopup;
            }
            ucLikeListPopup.Show(target, total, nfId, imageId, contentId, commentId);
        }
    }
}

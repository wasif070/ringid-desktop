﻿using Auth.Service.Feed;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using View.BindingModels;
using View.Utility;
using View.Utility.Feed;
using View.Constants;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAddLocationView.xaml
    /// </summary>
    public partial class UCAddLocationView : PopUpBaseControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCAddLocationView).Name);
        //public static UCAddLocationView Instance;
        public UCAddLocationView(Grid motherGrid)
        {
            //Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
            Visibility = Visibility.Hidden;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region "Property"

        //public UCLocationPopUp LocationPopUp
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.LocationPopUp;
        //    }
        //}
        private UCLocationPopUp LocationPopUp { get; set; }
        #endregion "Property"

        #region "Property change"


        #endregion "Property change"

        public long frnId, circleId;
        public Guid Nfid;
        private string sts;
        private ObservableCollection<TaggedUserModel> StatusTags;

        public void ShowAddLoation(Guid Nfid, string sts, ObservableCollection<TaggedUserModel> StatusTags, long frnId, long circleId)
        {
            Visibility = Visibility.Visible;
            TextBox.Focus();
            this.sts = sts;
            this.StatusTags = StatusTags;
            this.Nfid = Nfid;
            this.frnId = frnId;
            this.circleId = circleId;
            if (LocationPopUp != null) LocationPopUp.SelectedLocationModel = null;
            TextBox.Text = "";
            TextBox.IsReadOnly = false;
            TextLocationCrossBtn.Visibility = Visibility.Collapsed;
        }

        public void HideAddLocation()
        {
            Visibility = Visibility.Hidden;
            if (LocationPopUp != null) LocationPopUp.SelectedLocationModel = null;
            TextBox.Text = "";
            TextBox.IsReadOnly = false;
            TextLocationCrossBtn.Visibility = Visibility.Collapsed;
            if (LocationPopUp != null
                && LocationPopUp.popupLocation.IsOpen == true)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
                LocationPopUp.popupLocation.IsOpen = false;
            }
        }

        public void EnableButtonsandLoader(bool flag)
        {
            addBtn.IsEnabled = flag;
            cancelBtn.IsEnabled = flag;
            if (flag)
            {
                postLoader.Visibility = Visibility.Collapsed;
            }
            else
            {
                postLoader.Visibility = Visibility.Visible;
            }
        }

        #region "Icommands"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        ICommand _blackSpaceClick;
        public ICommand BlackSpaceClick
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }
        private void OnClickBlackSpace(object param)
        {
            if (LocationPopUp != null
                && LocationPopUp.popupLocation.IsOpen == true)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
                LocationPopUp.popupLocation.IsOpen = false;
            }

            Hide();
            HideAddLocation();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {
            if (LocationPopUp != null
                && LocationPopUp.popupLocation.IsOpen == true)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
                LocationPopUp.popupLocation.IsOpen = false;
            }
        }

        ICommand _AddLocationClicked;
        public ICommand AddLocationClicked
        {
            get
            {
                if (_AddLocationClicked == null)
                {
                    _AddLocationClicked = new RelayCommand(param => OnAddLocationClicked(param));
                }
                return _AddLocationClicked;
            }
        }

        private void OnAddLocationClicked(object param)
        {
            if (LocationPopUp != null && LocationPopUp.SelectedLocationModel != null)
            {
                EnableButtonsandLoader(false);
                JArray statusTagsArray = HelperMethods.GetStatusTagJArrayFromModels(StatusTags);
                new ThradEditFeed().StartThread(0, Nfid, sts, statusTagsArray, frnId, circleId, LocationPopUp.SelectedLocationModel.GetLocationDTOFromModel());
            }
        }

        ICommand _LocationCrossButtonClick;
        public ICommand LocationCrossButtonClick
        {
            get
            {
                if (_LocationCrossButtonClick == null)
                {
                    _LocationCrossButtonClick = new RelayCommand(param => OnLocationCrossButtonClick());
                }
                return _LocationCrossButtonClick;
            }
        }
        private void OnLocationCrossButtonClick()
        {
            TextBox.Text = "";
            TextBox.IsReadOnly = false;
            TextBox.Focus();
            if (LocationPopUp != null)
                LocationPopUp.SelectedLocationModel = null;
            TextLocationCrossBtn.Visibility = Visibility.Collapsed;
            if (LocationPopUp != null)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
                LocationPopUp.popupLocation.IsOpen = false;
            }
        }

        ICommand _TextBoxTextChanged;
        public ICommand TextBoxTextChanged
        {
            get
            {
                if (_TextBoxTextChanged == null)
                {
                    _TextBoxTextChanged = new RelayCommand(param => OnTextBoxTextChanged());
                }
                return _TextBoxTextChanged;
            }
        }
        private void OnTextBoxTextChanged()
        {
            try
            {
                if (TextBox.Text.Trim().Length > 0)
                {
                    if (LocationPopUp != null)
                        LocationPopUp.LocationSuggestions.Clear();
                    try
                    {
                        var address = TextBox.Text;
                        var requestUri = string.Format("https://maps.googleapis.com/maps/api/place/autocomplete/xml?input={0}&key={1}", Uri.EscapeDataString(address), Uri.EscapeDataString(SocialMediaConstants.GOOGLE_MAP_API_KEY));
                        var request = WebRequest.Create(requestUri);
                        var response = request.GetResponse();
                        var xdoc = XDocument.Load(response.GetResponseStream());
                        if (LocationPopUp == null)
                            locationPopupView();
                        LocationPopUp.Show();
                        LocationPopUp.ShowLocationPopup(TextBox, this);
                        foreach (XElement xe in xdoc.Descendants("prediction"))
                        {
                            string locationName = xe.Element("description").Value;
                            string locationId = xe.Element("place_id").Value;
                            System.Diagnostics.Debug.WriteLine(locationName + "  " + locationId);
                            if (!LocationPopUp.LocationSuggestions.Any(x => x.LocationId == locationId))
                            {
                                LocationPopUp.LocationSuggestions.Add(new LocationModel { LocationName = locationName, LocationId = locationId, Type = 0 });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message + "\n" + ex.StackTrace);
                    }
                }
                else
                {
                    if (LocationPopUp != null)
                        LocationPopUp.SelectedLocationModel = null;
                    LocationPopUp.popupLocation.IsOpen = false;
                }

                if (LocationPopUp.LocationSuggestions.Count == 0)
                {
                    LocationPopUp.popupLocation.StaysOpen = false;
                    LocationPopUp.popupLocation.IsOpen = false;
                }
                else
                {
                    LocationPopUp.popupLocation.StaysOpen = true;
                    LocationPopUp.popupLocation.IsOpen = true;
                }
                TextBox.Focus();
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }

        }
        #endregion

        #region "location Popup"

        private void locationPopupView()
        {
            if (LocationPopUp != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(LocationPopUp);
            LocationPopUp = new UCLocationPopUp(UCGuiRingID.Instance.MotherPanel);
            //LikeListView.ContentId = contentID;

            //LocationPopUp.Show();

            LocationPopUp.OnRemovedUserControl += () =>
            {
                LocationPopUp = null;
            };
        }
        #endregion
    }
}

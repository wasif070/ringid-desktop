﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.BindingModels;
using View.UI.ImageViewer;
using View.UI.MediaPlayer;
using View.Utility;
using View.Utility.Auth;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCLikeList.xaml
    /// This UserControl will use to show like list of image,media,feed etc.
    /// </summary>
    public partial class UCLikeList : PopUpBaseControl, INotifyPropertyChanged
    {
        #region "Fields"
        public System.Guid NewsfeedId, CommentId, ImageId, ContentId;
        private ThreadLikeRequest likeRequest;
        #endregion"Fields"

        #region "Properties"
        private ObservableCollection<UserBasicInfoModel> likeList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> LikeList
        {
            get { return likeList; }
            set { likeList = value; this.OnPropertyChanged("LikeList"); }
        }

        private string titleBarText = "Total Likes";
        public string TitleBarText
        {
            get { return titleBarText; }
            set { titleBarText = value; OnPropertyChanged("TitleBarText"); }
        }

        private long numberOfLike;
        public long NumberOfLike
        {
            get { return numberOfLike; }
            set { numberOfLike = value; OnPropertyChanged("NumberOfLike"); }
        }

        private bool showLoadMore = true;
        public bool ShowLoadMore
        {
            get { return showLoadMore; }
            set
            {
                if (value == showLoadMore) return;
                showLoadMore = value; OnPropertyChanged("ShowLoadMore");
            }
        }

        private bool isLoadingMoreLikes = false;
        public bool IsLoadingMoreLikes
        {
            get { return isLoadingMoreLikes; }
            set
            {
                if (value == isLoadingMoreLikes) return;
                isLoadingMoreLikes = value; OnPropertyChanged("IsLoadingMoreLikes");
            }
        }
        #endregion

        #region "Constructors"
        public UCLikeList(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
            RingIDViewModel.Instance.LikeList.Clear();
            LikeList = RingIDViewModel.Instance.LikeList;
        }
        #endregion "Constructor"

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            Hide();
        }

        private ICommand gotoUserProfile;
        public ICommand GotoUserProfile
        {
            get
            {
                if (gotoUserProfile == null) gotoUserProfile = new RelayCommand(param => OnGotoUserProfile(param));
                return gotoUserProfile;
            }
        }

        private ICommand sendFreindReuest;
        public ICommand SendFreindReuest
        {
            get
            {
                if (sendFreindReuest == null) sendFreindReuest = new RelayCommand(param => OnSendFriendRequest(param));
                return sendFreindReuest;
            }
        }

        private ICommand showMore;
        public ICommand ShowMore
        {
            get
            {
                if (showMore == null) showMore = new RelayCommand(param => OnShowMore(param));
                return showMore;
            }
        }

        #endregion

        #region "Utilty Methods"
        public void OnGotoUserProfile(object param)
        {
            var obj1 = GetParent.Parent;
            Hide();
            if (obj1 is Border) {
                var obj2 = ((Border)(obj1)).Parent;
                if (obj2 != null && obj2 is UCImageViewInMain)
                {
                    UCImageViewInMain imageViewInMain1 = (UCImageViewInMain)obj2;
                    imageViewInMain1.Hide();
                }
                else if (obj2 != null && obj2 is UCMediaPlayerInMain)
                {
                    UCMediaPlayerInMain imageViewInMain1 = (UCMediaPlayerInMain)obj2;
                    if (imageViewInMain1.OnlyPlayerView != null)
                        imageViewInMain1.OnlyPlayerView.OnGoToSmallScreen();
                    else  imageViewInMain1.CloseMediaPlayer(); 
                }
                else if (obj2 != null && obj2 is UCSingleFeedDetailsView)
                {
                    UCSingleFeedDetailsView SingleFeedDetailsView = (UCSingleFeedDetailsView)obj2;
                    SingleFeedDetailsView.Hide();
                }
                
            } else if (obj1 is UCGuiRingID) {
            }

            if (param != null && param is long)
            {
                long control = (long)param;
                if (control == DefaultSettings.LOGIN_TABLE_ID) RingIDViewModel.Instance.OnMyProfileClicked(control);
                else RingIDViewModel.Instance.OnFriendProfileButtonClicked(control);
            }
            obj1 = null;
        }

        public void OnSendFriendRequest(object param)
        {
            if (param != null && param is UserBasicInfoModel)
            {
                UserBasicInfoModel _model = (UserBasicInfoModel)param;
                if (_model.ShortInfoModel.FriendShipStatus == 0)
                    RingIDViewModel.Instance.OnAddFriendClicked(_model);
            }
        }

        private void OnShowMore(object param)
        {
            SendLikeListRequest();
        }

        public void SendLikeListRequest()
        {
            TitleBarText = string.Format("Total likes ({0})", NumberOfLike);
            if (likeRequest == null)
                likeRequest = new ThreadLikeRequest(this);
            likeRequest.StartThread();
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}

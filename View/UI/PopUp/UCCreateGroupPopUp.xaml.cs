﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.Chat;
using View.Utility.FriendList;
using Models.Utility;
using View.Constants;
using View.UI.Chat;
namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCCreateGroupPopUp.xaml
    /// </summary>
    public partial class UCCreateGroupPopUp : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCreateGroupPopUp).Name);
        public ObservableCollection<UserBasicInfoModel> SelectedFriendList = new ObservableCollection<UserBasicInfoModel>();
        private ObservableCollection<UserBasicInfoModel> _TempList = new ObservableCollection<UserBasicInfoModel>();

        public UCCreateGroupPopUp(Grid motherGrid)
        {
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, ViewConstants.POP_UP_BG_BORDER2, 1.0);
            SetParent(motherGrid);
            this.DataContext = this;
            this.Loaded += UCCreateGroupPopUp_Loaded;
        }

        #region Utility

        private void LoadFriendList()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                List<UserBasicInfoModel> modelList = FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values.
                     Where(
                         P => P.ShortInfoModel != null && P.ShortInfoModel.UserTableID > 0
                         && P.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED
                         && P.ShortInfoModel.ContactType != SettingsConstants.SPECIAL_CONTACT
                         ).ToList();

                foreach (UserBasicInfoModel model in modelList)
                {
                    int max = _TempList.Count;
                    int min = 0;
                    int pivot;

                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (String.CompareOrdinal(model.ShortInfoModel.FullName, _TempList.ElementAt(pivot).ShortInfoModel.FullName) > 0)
                        {
                            min = pivot + 1;
                        }
                        else
                        {
                            max = pivot;
                        }
                    }

                    _TempList.Insert(min, model);
                }
                FriendList = _TempList;
            }, DispatcherPriority.ApplicationIdle);
        }

        public void ShowFriendList()
        {
            try
            {
                SearchTermTextBox.Text = string.Empty;
                GroupNameTextBox.Text = string.Empty;
                TotalSelectedFriend = 0;
                LoadFriendList();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnCloseCommand()
        {
            base.Hide();
            ShowAddLoader = null;
            TotalSelectedFriend = 0;
            List<UserBasicInfoModel> removeList = SelectedFriendList.ToList();
            foreach (UserBasicInfoModel model in removeList)
            {
                model.ShortInfoModel.IsVisibleInTagList = false;
            }
            SelectedFriendList.Clear();
            _TempList.Clear();
            FriendList.Clear();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region EventHandler

        private void UCCreateGroupPopUp_Loaded(object sender, RoutedEventArgs e)
        {
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
        }

        private void OnGroupCreateCommand()
        {
            if (SelectedFriendList.Count == 0)
            {
                ErrorMessage = "No friend selected !";
                return;
            }

            ShowAddLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_CLOCK);
            GC.SuppressFinalize(ShowAddLoader);

            string groupName = string.IsNullOrWhiteSpace(GroupNameTextBox.Text) ? "Group" : GroupNameTextBox.Text;
            ChatHelpers.CreateGroup(SelectedFriendList.ToList(), groupName, false);
            this.OnCloseCommand();
        }

        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SearchText = SearchTermTextBox.Text.ToLower();
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (!string.IsNullOrWhiteSpace(SearchText))
                {
                    List<UserBasicInfoModel> modelList = _TempList.Where(P => (P.ShortInfoModel.FullName.ToLower().Contains(SearchText) || P.ShortInfoModel.UserIdentity.ToString().Contains(SearchText))).ToList();

                    ObservableCollection<UserBasicInfoModel> tempList1 = new ObservableCollection<UserBasicInfoModel>();
                    foreach (UserBasicInfoModel model in modelList)
                    {
                        tempList1.Add(model);
                    }
                    FriendList = tempList1;
                }
                else
                {
                    FriendList = _TempList;
                }
            }, DispatcherPriority.ApplicationIdle);
        }

        #endregion

        #region "Icommands"

        private ICommand _GroupCreateCommand;
        public ICommand GroupCreateCommand
        {
            get
            {
                if (_GroupCreateCommand == null)
                {
                    _GroupCreateCommand = new RelayCommand(param => OnGroupCreateCommand());
                }
                return _GroupCreateCommand;
            }
        }

        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(param => OnCloseCommand());
                }
                return _CloseCommand;
            }
        }

        #endregion "Icommands"

        #region Property

        private ObservableCollection<UserBasicInfoModel> _FriendList;
        public ObservableCollection<UserBasicInfoModel> FriendList
        {
            get
            {
                return _FriendList;
            }
            set
            {
                _FriendList = value;
                this.OnPropertyChanged("FriendList");
            }
        }

        private int _TotalSelectedFriend;
        public int TotalSelectedFriend
        {
            get { return _TotalSelectedFriend; }
            set
            {
                if (_TotalSelectedFriend == value) return;
                _TotalSelectedFriend = value;
                if (_TotalSelectedFriend > 0)
                {
                    ErrorMessage = string.Empty;
                }
                this.OnPropertyChanged("TotalSelectedFriend");
            }
        }

        private string _SearchText = string.Empty;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                _SearchText = value;
                OnPropertyChanged("SearchText");
            }
        }

        private string _ErrorMessage = string.Empty;
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set
            {
                _ErrorMessage = value;
                OnPropertyChanged("ErrorMessage");
            }
        }

        private ImageSource _ShowAddLoader;
        public ImageSource ShowAddLoader
        {
            get { return _ShowAddLoader; }
            set
            {
                _ShowAddLoader = value;
                OnPropertyChanged("ShowAddLoader");
            }
        }

        #endregion
    }
}

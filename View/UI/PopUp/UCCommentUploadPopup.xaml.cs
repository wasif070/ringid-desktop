﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCCommentUploadPopup.xaml
    /// </summary>
    public partial class UCCommentUploadPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        Func<int, int> _OnClosing = null;
        private int type;//1=image, 2= audio, 3= video
        private const int POPUP_HEIGHT = 120;

        #region "Constructor"
        public UCCommentUploadPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            SetParent(motherGrid);
        }
        #endregion
        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion 

        #region "Properties"
        private bool _IsUpper;
        public bool IsUpper
        {
            get
            {
                return _IsUpper;
            }
            set
            {
                _IsUpper = value;
                OnPropertyChanged("IsUpper");
            }
        }
        #endregion "Properties"

        #region "Public Method"
        public void ShowPopup(UIElement target, Func<int, int> onClosing)
        {
            type = 0;
            _OnClosing = onClosing;
            if (PopupUpload.IsOpen)
            {
                PopupUpload.IsOpen = false;
            }
            else
            {
                int buttonPosition = (int)target.PointToScreen(new System.Windows.Point(0, 0)).Y;
                int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;

                if ((buttonPosition < POPUP_HEIGHT && bottomSpace >= POPUP_HEIGHT) || bottomSpace >= POPUP_HEIGHT)
                {
                    IsUpper = false;

                   // VerOffset = -14.0;
                   // popupPlayList.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                }
                else
                {
                    IsUpper = true;
                    //VerOffset = 14.0;
                    //popupPlayList.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
                }
                SetEvents();
                PopupUpload.PlacementTarget = target;
                PopupUpload.IsOpen = true;
            }
        }

        public void HidePopUp()
        {
            PopupUpload.IsOpen = false;
        }
        #endregion

        #region "Private Method"
        private void SetEvents()
        {
            imgUploadBorder.MouseLeftButtonUp += imgUploadBorder_MouseLeftButtonUp;
            uploadAudioBorder.MouseLeftButtonUp += uploadAudioBorder_MouseLeftButtonUp;
            uploadVideoBorder.MouseLeftButtonUp += uploadVideoBorder_MouseLeftButtonUp;
            uploadWebCamBorder.MouseLeftButtonUp += uploadWebCamBorder_MouseLeftButtonUp;
            //uploadRecordBorder.MouseLeftButtonUp += uploadRecordBorder_MouseLeftButtonUp;
            PopupUpload.Closed += PopupUpload_Closed;
        }
        #endregion

        #region "Event Trigger"
        private void PopupUpload_Closed(object sender, EventArgs e)
        {
            if (_OnClosing != null)
            {
                imgUploadBorder.MouseLeftButtonUp -= imgUploadBorder_MouseLeftButtonUp;
                uploadAudioBorder.MouseLeftButtonUp -= uploadAudioBorder_MouseLeftButtonUp;
                uploadVideoBorder.MouseLeftButtonUp -= uploadVideoBorder_MouseLeftButtonUp;
                uploadWebCamBorder.MouseLeftButtonUp -= uploadWebCamBorder_MouseLeftButtonUp;
                //uploadRecordBorder.MouseLeftButtonUp -= uploadRecordBorder_MouseLeftButtonUp;
                PopupUpload.Closed -= PopupUpload_Closed;
                _OnClosing(type);
                Hide();
            }
        }

        private void imgUploadBorder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            HidePopUp();
            type = 1;
        }

        private void uploadAudioBorder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            HidePopUp();
            type = 2;
        }

        private void uploadVideoBorder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            HidePopUp();
            type = 3;
        }
        private void uploadWebCamBorder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            HidePopUp();
            type = 4;
        }
        private void uploadRecordBorder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) 
        {
            HidePopUp();
            type = 5;
        }
        #endregion

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            HidePopUp();
        }
        #endregion
    }
}

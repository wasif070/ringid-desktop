﻿using log4net;
using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMutualFriendsPopup.xaml
    /// </summary>
    public partial class UCFeedTagListPopup : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFeedTagListPopup).Name);

        public long nfid;
        private ObservableCollection<UserBasicInfoModel> _TagList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> TagList
        {
            get
            {
                return _TagList;
            }
            set
            {
                _TagList = value;
                this.OnPropertyChanged("CategoryList");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public UCFeedTagListPopup()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        public void Show(long nfid, UIElement target)
        {
            try
            {
                TagList.Clear();
           
                this.nfid = nfid;

                if (popupTagList.IsOpen)
                {
                    popupTagList.IsOpen = false;
                }
                else
                {
                    popupTagList.PlacementTarget = target;

                    popupTagList.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            if (popupTagList.IsOpen)
            {
                popupTagList.IsOpen = false;
            }
        }
        private void txtBL_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (popupTagList.IsOpen) popupTagList.IsOpen = false;
            TextBlock control = (TextBlock)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            if (model.ShortInfoModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
                RingIDViewModel.Instance.OnFriendProfileButtonClicked(model.ShortInfoModel.UserTableID);                
            else
                RingIDViewModel.Instance.OnMyProfileClicked(model.ShortInfoModel.UserTableID);
        }


        public UIElement center { get; set; }
    }
}

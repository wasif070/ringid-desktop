﻿using Models.Constants;
using Models.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.WPFMessageBox;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCFeedDownloadPauseResumePopup.xaml
    /// </summary>
    public partial class UCFeedDownloadPauseCancelPopup : UserControl, INotifyPropertyChanged
    {
        private static UCFeedDownloadPauseCancelPopup _Instance;
        private const int MEDIA_PAUSE_RESUME_POPUP_HEIGHT = 60;
        SingleMediaModel singleMediaModel;


        public UCFeedDownloadPauseCancelPopup()
        {
            //Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private bool _IsResume = false;
        public bool IsResume
        {
            get
            {
                return _IsResume;
            }
            set
            {
                if (value == _IsResume)
                    return;
                _IsResume = value;
                OnPropertyChanged("IsResume");
            }
        }

        private bool _PopupPosition = false;
        public bool PopupPosition
        {
            get
            {
                return _PopupPosition;
            }
            set
            {
                if (value == _PopupPosition)
                    return;
                _PopupPosition = value;
                OnPropertyChanged("PopupPosition");
            }
        }
        CustomFileDownloader fileDownloader = null;
        public void Show(SingleMediaModel singleMedia, UIElement target)
        {
            this.singleMediaModel = singleMedia;

            if (!MediaDataContainer.Instance.CurrentDownloads.TryGetValue(singleMediaModel.ContentId, out fileDownloader))
            {
                fileDownloader = new CustomFileDownloader(singleMediaModel);
                MediaDataContainer.Instance.CurrentDownloads[singleMediaModel.ContentId] = fileDownloader;
            }
            ShowUpperOrLowerPopUp(target); // Determine whether the popup placement is lower or upper.
            if (popupPausedResume.IsOpen)
            {
                popupPausedResume.IsOpen = false;
            }
            else
            {
                popupPausedResume.PlacementTarget = target;
                popupPausedResume.IsOpen = true;
            }
        }

        private void ShowUpperOrLowerPopUp(UIElement target)
        {
            int buttonPosition = (int)target.PointToScreen(new System.Windows.Point(0, 0)).Y;
            int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;

            if ((buttonPosition < MEDIA_PAUSE_RESUME_POPUP_HEIGHT && bottomSpace >= MEDIA_PAUSE_RESUME_POPUP_HEIGHT) || bottomSpace >= MEDIA_PAUSE_RESUME_POPUP_HEIGHT)
            {
                PopupPosition = true;
                popupPausedResume.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            }
            else
            {
                PopupPosition = false;
                popupPausedResume.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
            }
        }

        private void pause_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (fileDownloader.CanPause)
            {
                fileDownloader.Pause();
                popupPausedResume.IsOpen = false;
            }
            else
            {
                UIHelperMethods.ShowWarning("Paused Media file not found", "Not found");
                //singleMediaModel.DeleteDownloadMedia(singleMediaModel.ContentId);
                singleMediaModel.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                singleMediaModel.DownloadProgress = 0;
                MediaDAO.Instance.DeleteFromDownloadedMediasTable(singleMediaModel.ContentId);
                //TODO delete downloaded files from PC
            }
        }

        private void cancel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (fileDownloader.CanStop)
            {
                fileDownloader.Stop();
                popupPausedResume.IsOpen = false;
            }
            else
            {
                //singleMediaModel.DeleteDownloadMedia(singleMediaModel.ContentId);
                singleMediaModel.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                singleMediaModel.DownloadProgress = 0;
                MediaDAO.Instance.DeleteFromDownloadedMediasTable(singleMediaModel.ContentId);
                //TODO delete downloaded files from PC
            }
        }

    }
}

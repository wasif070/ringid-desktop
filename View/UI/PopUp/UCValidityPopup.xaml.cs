﻿using log4net;
using Models.Constants;
using Models.DAO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Dictonary;
using View.UI.Feed;
using View.UI.Profile.FriendProfile;
using View.UI.Settings;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Auth;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCValidityPopup.xaml
    /// </summary>
    public partial class UCValidityPopup : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCValidityPopup).Name);

        private JObject pakToSend;
        private bool _ShowUpArrowPopup = false;// false - UP, true - Down
        private const int VALIDITY_POPUP_HEIGHT = 179;

        public bool isFeed;

        #region Property
        private string _ValidityString = "";
        public string ValidityString
        {
            get { return _ValidityString; }
            set
            {
                _ValidityString = value;
                this.OnPropertyChanged("ValidityString");
            }
        }
        public bool ShowUpArrowPopup
        {
            get { return _ShowUpArrowPopup; }
            set
            {
                if (_ShowUpArrowPopup == value)
                {
                    return;
                }
                _ShowUpArrowPopup = value;
                this.OnPropertyChanged("ShowUpArrowPopup");
            }
        }

        private ObservableCollection<ValidityModel> _ValidityCollection = new ObservableCollection<ValidityModel>();
        public ObservableCollection<ValidityModel> ValidityCollection
        {
            get
            {
                return _ValidityCollection;
            }
            set
            {
                _ValidityCollection = value;
                this.OnPropertyChanged("ValidityCollection");
            }
        }
        #endregion

        #region Constructor
        public UCValidityPopup()
        {
            this.DataContext = this;
            InitializeComponent();
            //LoadValidityArray();
            //popupValidity.Closed += popupValidity_Closed;
        }
        #endregion

        private void LoadValidityArray()
        {
            ValidityCollection.Clear();
            ValidityModel vldtList;
            for (int i = 1; i <= 30; i++)
            {
                vldtList = new ValidityModel(i.ToString(), false);
                ValidityCollection.Add(vldtList);
            }
            /* vldtList = new ValidityModel("Unlimited", false);
             ValidityCollection.Add(vldtList);*/
        }
        private UCGeneralSettings ucGeneralSettings;
        public void Show(UIElement target, UCGeneralSettings ucGeneralSettings, bool isFeed = false)
        {
            try
            {
                this.ucGeneralSettings = ucGeneralSettings;
                this.isFeed = isFeed;
                ValidityCollection.Clear();

                if (isFeed)
                {
                    ValidityModel vldtList;
                    for (int i = 1; i <= 30; i++)
                    {
                        vldtList = new ValidityModel(i.ToString(), SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY == Convert.ToInt32(i.ToString()) ? true : false);
                        ValidityCollection.Add(vldtList);
                    }
                    vldtList = new ValidityModel(AppConstants.UNLIMITED_VALIDITY_STRING, false);
                    ValidityCollection.Add(vldtList);
                }
                else
                {
                    ValidityModel vldtList;
                    for (int i = 1; i <= 30; i++)
                    {
                        vldtList = new ValidityModel(i.ToString(), DefaultSettings.userProfile.NotificationValidity == Convert.ToInt32(i.ToString()) ? true : false);
                        ValidityCollection.Add(vldtList);
                    }
                    //foreach (var model in ValidityCollection)
                    //{
                    //    if (DefaultSettings.userProfile.NotificationValidity == Convert.ToInt32(model.ValidityString))
                    //    {
                    //        model.IsSelected = true;
                    //    }
                    //}
                }

                if (popupValidity.IsOpen == false)
                {
                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;
                    if ((buttonPosition < VALIDITY_POPUP_HEIGHT && bottomSpace >= VALIDITY_POPUP_HEIGHT) || bottomSpace >= VALIDITY_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = true;
                        //popupValidity.VerticalOffset = -10;
                    }
                    else
                    {
                        //popupValidity.VerticalOffset = 0;
                        ShowUpArrowPopup = false;
                    }
                    popupValidity.PlacementTarget = target;
                    popupValidity.IsOpen = true;
                    this.Focusable = true;
                    this.Focus();
                    /*DownloadOrAddToAlbumPopUpWrapper.Visibility = Visibility.Visible;
                    DownloadOrAddToAlbumPopUpWrapper.Focusable = true;
                    DownloadOrAddToAlbumPopUpWrapper.Focus();*/
                }
                else
                {
                    popupValidity.IsOpen = false;
                    //DownloadOrAddToAlbumPopUpWrapper.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void ValidityList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //try
            //{
            //    popupValidity.IsOpen = false;
            //    ListBox lb = (ListBox)sender;
            //    string str = lb.SelectedValue.ToString();
            //    //ucNewStatus.validityTextBlock.Text = str;
            //    if (lb.SelectedIndex == 30) ValidityVal = -1;
            //    else ValidityVal = System.Convert.ToInt32(str);
            //    _UCGeneralSettings.SetSelectedModelValuesForValidityPopup();
            //    /*SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY = ValidityVal;
            //    new InsertIntoRingUserSettings();
            //    ucNewStatus.SetSelectedModelValuesForValidityPopup();*/
            //}
            //catch (Exception ex)
            //{

            //    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            //}
        }

        private void Bd_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                popupValidity.IsOpen = false;
                Border bd = (Border)sender;
                foreach (var model in ValidityCollection)
                {
                    model.IsSelected = false;
                }
                ValidityModel vldtyModel = (ValidityModel)bd.DataContext;
                if (isFeed)
                    SetFeedValidty(vldtyModel);
                else
                    SetNotificationValidity(vldtyModel);
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void SetFeedValidty(ValidityModel vldtyModel)
        {
            if (vldtyModel.ValidityString == AppConstants.UNLIMITED_VALIDITY_STRING)
                SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY = AppConstants.DEFAULT_VALIDITY;
            else
            {
                SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY = Convert.ToInt32(vldtyModel.ValidityString);
                vldtyModel.IsSelected = true;
            }

            new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_NEWS_FEED_EXPIRY, SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY.ToString());

            ucGeneralSettings.SetSelectedModelValuesForFeedValidityPopup();
            SetValueForOtherUIFeedValidityPopUp();
        }
        private void SetValueForOtherUIFeedValidityPopUp()
        {
            if (UCAllFeeds.Instance != null && UCAllFeeds.Instance.newStatusViewModel != null)
                UCAllFeeds.Instance.newStatusViewModel.SetSelectedModelValuesForValidityPopup();

            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.NewStatusView != null &&
                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.NewStatusView.IsVisible)
            {
                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.NewStatusViewModel.SetSelectedModelValuesForValidityPopup();
            }

            //foreach (long friendTableId in UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.Keys)
            //{
            //    UCFriendProfile _UCFriendProfile = UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY[friendTableId];
            //    if (_UCFriendProfile != null && _UCFriendProfile._UCFriendNewsFeeds != null && _UCFriendProfile._UCFriendNewsFeeds.ucNewStatus != null && _UCFriendProfile._UCFriendNewsFeeds.ucNewStatus.IsVisible)
            //    {
            //        _UCFriendProfile._UCFriendNewsFeeds.ucNewStatus.SetSelectedModelValuesForValidityPopup();
            //        break;
            //    }
            //}
            if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null)
            {
                UCFriendProfile _UCFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                if (_UCFriendProfile != null && _UCFriendProfile._UCFriendNewsFeeds != null && _UCFriendProfile._UCFriendNewsFeeds.NewStatusView != null && _UCFriendProfile._UCFriendNewsFeeds.NewStatusView.IsVisible)
                {
                    _UCFriendProfile._UCFriendNewsFeeds.NewStatusViewModel.SetSelectedModelValuesForValidityPopup();
                }
            }

            if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.NewStatusView != null &&
                UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.NewStatusView.IsVisible)
            {
                UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.NewStatusViewModel.SetSelectedModelValuesForValidityPopup();
            }
        }

        private void SetNotificationValidity(ValidityModel vldtyModel)
        {
            pakToSend = new JObject();
            pakToSend[JsonKeys.ProfileType] = 1;
            pakToSend[JsonKeys.NotificationValidity] = Convert.ToInt32(vldtyModel.ValidityString);
            pakToSend[JsonKeys.NumberOfHeaders] = (pakToSend.Count - 1) + "";

            bool _SuccessBasic = false;
            string _Msg = string.Empty;

            (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_USER_PROFILE, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _SuccessBasic, out _Msg);
            if (_SuccessBasic)
            {
                if (pakToSend[JsonKeys.NotificationValidity] != null)
                {
                    DefaultSettings.userProfile.NotificationValidity = (int)pakToSend[JsonKeys.NotificationValidity];
                }
                vldtyModel.IsSelected = true;
                ucGeneralSettings.SetSelectedModelValuesForNotificationValidityPopup();
            }
            else
            {
                WNConfirmationView cv = new WNConfirmationView("Change validity", _Msg, CustomConfirmationDialogButtonOptions.OK, RingIDViewModel.Instance._WNRingIDSettings);
                var result = cv.ShowCustomDialog();
            }
            //{
            //    CustomMessageBox.ShowError(owner: RingIDViewModel.Instance._WNRingIDSettings, exception: new Exception(""), message: _Msg);
            //}
        }

        //private void popupValidity_Closed(object sender, EventArgs e)
        //{
        //    DownloadOrAddToAlbumPopUpWrapper.Visibility = Visibility.Hidden;
        //    popupValidity.Closed -= popupValidity_Closed;
        //}

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                popupValidity.IsOpen = false;
            }
        }
    }
}

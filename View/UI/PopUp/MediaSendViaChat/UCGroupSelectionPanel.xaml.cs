﻿using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.ViewModel;
using View.Utility;
using imsdkwrapper;
using Models.Constants;

namespace View.UI.PopUp.MediaSendViaChat
{
    /// <summary>
    /// Interaction logic for UCGroupPanel.xaml
    /// </summary>
    public partial class UCGroupSelectionPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCGroupSelectionPanel).Name);
        public ObservableCollection<GroupInfoModel> _TempList = new ObservableCollection<GroupInfoModel>();
        public delegate void OnSelectingComplete(long frindID, long groupID, string roomId);
        public event OnSelectingComplete _OnComplete = null;

        public UCGroupSelectionPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Property

        private ObservableCollection<GroupInfoModel> _GroupList = new ObservableCollection<GroupInfoModel>();
        public ObservableCollection<GroupInfoModel> GroupList
        {
            get
            {
                return _GroupList;
            }
            set
            {
                _GroupList = value;
                this.OnPropertyChanged("GroupList");
            }
        }

        private string _SearchText = string.Empty;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                _SearchText = value;
                ShowSearchView();
            }
        }

        private ICommand _OnSelectCommand;
        public ICommand OnSelectCommand
        {
            get
            {
                if (_OnSelectCommand == null)
                {
                    _OnSelectCommand = new RelayCommand((param) => OnSelect(param));
                }
                return _OnSelectCommand;
            }
        }

        private void OnSelect(object param)
        {
            //if(param)
            GroupInfoModel model = (GroupInfoModel)param;
            if (model != null && _OnComplete != null)
            {
                this._OnComplete(0, model.GroupID, null);
                //MainSwitcher.PopupController.ucMediaShareToFriendPopup.OnClick(0, model.GroupID, null);
            }
        }

        #endregion

        #region Utility

        public void LoadGroupList()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                List<GroupInfoModel> modelList = RingIDViewModel.Instance.GroupList.Values.Where(P => !(P.IsPartial == false && P.AccessType == ChatConstants.MEMBER_TYPE_NOT_MEMBER)).ToList();
                ObservableCollection<GroupInfoModel> TempList = new ObservableCollection<GroupInfoModel>();
                foreach (GroupInfoModel model in modelList)
                {

                    int max = _TempList.Count;
                    int min = 0;
                    int pivot;

                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (String.CompareOrdinal(model.GroupName, _TempList.ElementAt(pivot).GroupName) > 0)
                        {
                            min = pivot + 1;
                        }
                        else
                        {
                            max = pivot;
                        }
                    }

                    _TempList.InvokeInsert(min, model);
                }
                GroupList = _TempList;
                GC.SuppressFinalize(modelList);
            });
        }

        private void ShowSearchView()
        {
            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                List<GroupInfoModel> modelList = _TempList.Where(P => (P.GroupName.ToLower().Contains(SearchText))).ToList();
                ObservableCollection<GroupInfoModel> tempList1 = new ObservableCollection<GroupInfoModel>();
                foreach (GroupInfoModel model in modelList)
                {
                    tempList1.Add(model);
                }
                GroupList = tempList1;
            }
            else
            {
                GroupList = _TempList;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}

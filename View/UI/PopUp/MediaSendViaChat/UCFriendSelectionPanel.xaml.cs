﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.FriendList;

namespace View.UI.PopUp.MediaSendViaChat
{
    /// <summary>
    /// Interaction logic for FriendlistSelectionPanel.xaml
    /// </summary>
    public partial class UCFriendSelectionPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendSelectionPanel).Name);
        public ObservableCollection<UserBasicInfoModel> _TempList = new ObservableCollection<UserBasicInfoModel>();
        public delegate void OnSelectingComplete(long frindID, long groupID, string roomId);
        public event OnSelectingComplete _OnComplete = null;

        public UCFriendSelectionPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Property

        private ObservableCollection<UserBasicInfoModel> _FriendList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> FriendList
        {
            get
            {
                return _FriendList;
            }
            set
            {
                _FriendList = value;
                this.OnPropertyChanged("FriendList");
            }
        }

        private string _SearchText = string.Empty;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                _SearchText = value;
                ShowSearchView();
            }
        }

        private ICommand _OnSelectCommand;
        public ICommand OnSelectCommand
        {
            get
            {
                if (_OnSelectCommand == null)
                {
                    _OnSelectCommand = new RelayCommand((param) => OnSelect(param));
                }
                return _OnSelectCommand;
            }
        }

        private void OnSelect(object param)
        {
            UserBasicInfoModel model = (UserBasicInfoModel)param;
            if (model != null && _OnComplete != null)
            {
                this._OnComplete(model.ShortInfoModel.UserTableID, 0, null);
                //MainSwitcher.PopupController.ucMediaShareToFriendPopup.OnClick(model.ShortInfoModel.UserTableID, 0, null);
            }
        }

        #endregion

        #region Utility

        public void LoadFriendList()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                List<UserBasicInfoModel> modelList = FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values.
                    Where(
                        P => P.ShortInfoModel != null && P.ShortInfoModel.UserTableID > 0
                        && P.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED
                        && P.ShortInfoModel.ContactType != SettingsConstants.SPECIAL_CONTACT
                        ).ToList();
                foreach (UserBasicInfoModel model in modelList)
                {
                    int max = _TempList.Count;
                    int min = 0;
                    int pivot;

                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (String.CompareOrdinal(model.ShortInfoModel.FullName, _TempList.ElementAt(pivot).ShortInfoModel.FullName) > 0)
                        {
                            min = pivot + 1;
                        }
                        else
                        {
                            max = pivot;
                        }
                    }

                    _TempList.Insert(min, model);
                }
                FriendList = _TempList;
                GC.SuppressFinalize(_TempList);
            }, DispatcherPriority.ApplicationIdle);
        }

        private void ShowSearchView()
        {
            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                List<UserBasicInfoModel> modelList = _TempList.Where(P => (P.ShortInfoModel.FullName.ToLower().Contains(SearchText)) || P.ShortInfoModel.UserIdentity.ToString().Contains(SearchText)).ToList();
                ObservableCollection<UserBasicInfoModel> tempList1 = new ObservableCollection<UserBasicInfoModel>();
                foreach (UserBasicInfoModel model in modelList)
                {
                    tempList1.Add(model);
                }
                FriendList = tempList1;
            }
            else
            {
                FriendList = _TempList;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }
}

﻿using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.Utility.Chat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.UI.Chat;
using View.UI.Feed;
using View.Utility;
using View.Utility.Chat;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using Models.Entity;
using System.IO;
using View.Utility.Chat.Service;

namespace View.UI.PopUp.MediaSendViaChat
{
    /// <summary>
    /// Interaction logic for UCMediaShareToFriendPopup.xaml
    /// </summary>
    public partial class UCMediaShareToFriendPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMediaShareToFriendPopup).Name);
        private SingleMediaModel _SingleMediaModel;
        private ImageModel _ImageModel;
        private MarkertStickerImagesModel _StickerImageModel;
        private MessageModel _MessageModel;
        private ChatFileDTO _chatFileDTO;
        private UCFriendSelectionPanel _UCFriendSelectionPanel = null;
        private UCGroupSelectionPanel _UCGroupSelectionPanel = null;
        private UCRecentSelectionPanel _UCRecentSelectionPanel = null;

        public UCMediaShareToFriendPopup(Grid motherGrid)
        {
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, ViewConstants.POP_UP_BG_BORDER2, 1.0);
            SetParent(motherGrid);
            this.DataContext = this;
            this.Loaded += UCMediaShareToFriendPopup_Loaded;
        }

        void UCMediaShareToFriendPopup_Loaded(object sender, RoutedEventArgs e)
        {
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
            SwitchToTab(0);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region "Properties"

        #endregion "Properties"

        #region Utility

        public void SetSingleMedia(SingleMediaModel singleMediaModel)
        {
            try
            {
                this._SingleMediaModel = singleMediaModel;
                this._MessageModel = null;
                this._ImageModel = null;
                this._StickerImageModel = null;
                this._chatFileDTO = null;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void SetSingleImage(ImageModel imageModel)
        {
            try
            {
                this._ImageModel = imageModel;
                this._MessageModel = null;
                this._SingleMediaModel = null;
                this._StickerImageModel = null;
                this._chatFileDTO = null;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void SetStickerImage(MarkertStickerImagesModel stickerImageModel)
        {
            try
            {
                this._StickerImageModel = stickerImageModel;
                this._MessageModel = null;
                this._SingleMediaModel = null;
                this._ImageModel = null;
                this._chatFileDTO = null;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void SetSingleChatFile(ChatFileDTO fileDTO)
        {
            try
            {
                this._chatFileDTO = fileDTO;
                this._StickerImageModel = null;
                this._MessageModel = null;
                this._SingleMediaModel = null;
                this._ImageModel = null;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void SetMessage(MessageModel messageModel)
        {
            try
            {
                this._MessageModel = messageModel;
                this._StickerImageModel = null;
                this._SingleMediaModel = null;
                this._ImageModel = null;
                this._chatFileDTO = null;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //private UCBasicImageViewWrapper basicImageViewWrapper()
        //{
        //    return MainSwitcher.PopupController.BasicImageViewWrapper;
        //}

        public void OnClick(long frindID, long groupID, string roomId)
        {
            if (_SingleMediaModel != null)
            {
                string message = JsonConvert.SerializeObject(BindSingleMediaObject(_SingleMediaModel), Formatting.None);
                ChatHelpers.AddForwardOrShareMessage(frindID, groupID, roomId, MessageType.RING_MEDIA_MESSAGE, message, false, null);
                OnCloseCommand();
            }
            else if (_ImageModel != null)
            {
                string message = ChatJSONParser.MakeImageMessage(_ImageModel.ImageUrl, _ImageModel.Caption, _ImageModel.ImageWidth, _ImageModel.ImageHeight);
                ChatHelpers.AddForwardOrShareMessage(frindID, groupID, roomId, MessageType.IMAGE_FILE_FROM_GALLERY, message, false, null);
                //if (basicImageViewWrapper().ucBasicImageView != null) basicImageViewWrapper().ucBasicImageView.DoCancel();
                OnCloseCommand();
            }
            else if (_StickerImageModel != null)
            {
                string message = _StickerImageModel.StickerCollectionID.ToString() + System.IO.Path.AltDirectorySeparatorChar + _StickerImageModel.StickerCategoryID.ToString() + System.IO.Path.AltDirectorySeparatorChar + _StickerImageModel.ImageUrl;
                ChatHelpers.AddForwardOrShareMessage(frindID, groupID, roomId, MessageType.DOWNLOAD_STICKER_MESSAGE, _StickerImageModel, true, null);
                OnCloseCommand();
            }
            else if (_MessageModel != null)
            {
                ChatHelpers.AddForwardOrShareMessage(frindID, groupID, roomId, _MessageModel.MessageType, _MessageModel.OriginalMessage, true, _MessageModel);
                OnCloseCommand();
            }
            else if (_chatFileDTO != null)
            {
                FileInfo fi = new FileInfo(_chatFileDTO.File);
                if (fi.Length <= DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE)
                {
                    if (!String.IsNullOrWhiteSpace(roomId))
                    {
                        UIHelperMethods.ShowWarning("File transfer is currently not supported on public room chat.", "File transfer");
                        return;
                    }
                    string message = ChatJSONParser.MakeStreamMessage(_chatFileDTO.File, fi.Length, PacketIDGenerator.GetUnixTimestamp(ChatService.GeneratePacketID().PacketID));
                    ChatHelpers.AddForwardOrShareMessage(frindID, groupID, roomId, MessageType.FILE_STREAM, message, false, null);
                }
                else UIHelperMethods.ShowFailed(String.Format(NotificationMessages.MAX_FILE_TRANSFER_SIZE, DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE / (1024 * 1024 * 1024)), "Media share");
                OnCloseCommand();
            }
            else UIHelperMethods.ShowFailed("Must select a friend to share the media link!", "Media share");
        }

        private JObject BindSingleMediaObject(SingleMediaModel dto)
        {
            try
            {
                JObject jObject = new JObject();
                jObject[JsonKeys.ContentId] = dto.ContentId;
                jObject[JsonKeys.Title] = dto.Title;
                jObject[JsonKeys.Artist] = dto.Artist;
                jObject[JsonKeys.MediaType] = dto.MediaType;
                jObject[JsonKeys.StreamUrl] = dto.StreamUrl;
                jObject[JsonKeys.ThumbUrl] = dto.ThumbUrl;
                jObject[JsonKeys.MediaDuration] = dto.Duration;
                jObject[JsonKeys.ThumbImageWidth] = dto.ThumbImageWidth;
                jObject[JsonKeys.ThumbImageHeight] = dto.ThumbImageHeight;
                jObject[JsonKeys.MediaPrivacy] = dto.Privacy;
                jObject[JsonKeys.AlbumName] = dto.AlbumName;
                jObject[JsonKeys.AlbumId] = dto.AlbumId;
                jObject[JsonKeys.AccessCount] = dto.AccessCount;
                if (dto.LikeCommentShare != null)
                {
                    jObject[JsonKeys.LikeCount] = dto.LikeCommentShare.NumberOfLikes;
                    jObject[JsonKeys.CommentCount] = dto.LikeCommentShare.NumberOfComments;
                    jObject[JsonKeys.UserTableID] = dto.MediaOwner.UserTableID;
                    jObject[JsonKeys.ILike] = dto.LikeCommentShare.ILike;
                    jObject[JsonKeys.IComment] = dto.LikeCommentShare.IComment;
                    jObject[JsonKeys.IShare] = dto.LikeCommentShare.IShare;
                    jObject[JsonKeys.NumberOfShares] = dto.LikeCommentShare.NumberOfShares;
                }
                jObject[JsonKeys.FullName] = dto.MediaOwner.FullName;
                jObject[JsonKeys.ProfileImage] = dto.MediaOwner.ProfileImage;
                jObject[JsonKeys.NewsfeedId] = dto.NewsFeedId;

                return jObject;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in BindSingleMediaObject => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        private void OnCloseCommand()
        {
            if (_UCRecentSelectionPanel != null)
            {
                _UCRecentSelectionPanel._TempList.Clear();
                GC.SuppressFinalize(_UCRecentSelectionPanel._TempList);
                _UCRecentSelectionPanel.RecentList.Clear();
                GC.SuppressFinalize(_UCRecentSelectionPanel.RecentList);
                _UCRecentSelectionPanel = null;
            }

            if (_UCFriendSelectionPanel != null)
            {
                _UCFriendSelectionPanel._TempList.Clear();
                GC.SuppressFinalize(_UCFriendSelectionPanel._TempList);
                _UCFriendSelectionPanel.FriendList.Clear();
                GC.SuppressFinalize(_UCFriendSelectionPanel.FriendList);
                _UCFriendSelectionPanel = null;
            }

            if (_UCGroupSelectionPanel != null)
            {
                _UCGroupSelectionPanel._TempList.Clear();
                GC.SuppressFinalize(_UCGroupSelectionPanel._TempList);
                _UCGroupSelectionPanel.GroupList.Clear();
                GC.SuppressFinalize(_UCGroupSelectionPanel.GroupList);
                _UCGroupSelectionPanel = null;
            }

            base.Hide();
            SearchTermTextBox.Text = string.Empty;
            this._SingleMediaModel = null;
            this._MessageModel = null;
            this._ImageModel = null;
            this._StickerImageModel = null;
            this._chatFileDTO = null;
            GC.Collect();
        }

        public void OnBtnTabCommandClicked(object parameter)
        {
            TabType = Convert.ToInt32(parameter);
            SwitchToTab(_TabType);
        }

        #endregion

        #region "Icommands"

        ICommand _blackSpaceClick;
        public ICommand BlackSpaceClick
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }

        private void OnClickBlackSpace(object param)
        {
            Hide();
        }

        private ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {

        }

        private ICommand _emptyCommand;
        public ICommand EmptyCommand
        {
            get
            {
                if (_emptyCommand == null)
                {
                    _emptyCommand = new RelayCommand(param => ActionEmpty(param));
                }
                return _emptyCommand;
            }
        }

        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(param => OnCloseCommand());
                }
                return _CloseCommand;
            }
        }

        private ICommand _BtnTabCommand;
        public ICommand BtnTabCommand
        {
            get
            {
                if (_BtnTabCommand == null)
                {
                    _BtnTabCommand = new RelayCommand(param => OnBtnTabCommandClicked(param));
                }
                return _BtnTabCommand;
            }
        }

        #endregion "Icommands"

        #region EventHandler

        private void SwitchToTab(int SelectedIndex)
        {
            switch (SelectedIndex)
            {
                case 0:
                    if (_UCRecentSelectionPanel == null)
                    {
                        _UCRecentSelectionPanel = new UCRecentSelectionPanel();
                        _UCRecentSelectionPanel.LoadRecentList();
                        _UCRecentSelectionPanel._OnComplete += (frindID, groupID, roomId) =>
                        {
                            this.OnClick(frindID, groupID, roomId);
                        };
                    }
                    bdrSendViaChat.Child = _UCRecentSelectionPanel;
                    break;

                case 1:
                    if (_UCFriendSelectionPanel == null)
                    {
                        _UCFriendSelectionPanel = new UCFriendSelectionPanel();
                        _UCFriendSelectionPanel.SearchText = SearchTermTextBox.Text;
                        _UCFriendSelectionPanel.LoadFriendList();
                        _UCFriendSelectionPanel._OnComplete += (frindID, groupID, roomId) =>
                        {
                            this.OnClick(frindID, groupID, roomId);
                        };
                    }
                    bdrSendViaChat.Child = _UCFriendSelectionPanel;
                    break;

                case 2:
                    if (_UCGroupSelectionPanel == null)
                    {
                        _UCGroupSelectionPanel = new UCGroupSelectionPanel();
                        _UCGroupSelectionPanel.LoadGroupList();
                        _UCGroupSelectionPanel.SearchText = SearchTermTextBox.Text;
                        _UCGroupSelectionPanel._OnComplete += (frindID, groupID, roomId) =>
                        {
                            this.OnClick(frindID, groupID, roomId);
                        };
                    }
                    bdrSendViaChat.Child = _UCGroupSelectionPanel;
                    break;
            }
        }

        private int _TabType = 0;
        public int TabType
        {
            get { return _TabType; }
            set
            {
                _TabType = value;
                OnPropertyChanged("TabType");
            }
        }

        private void ActionEmpty(object param)
        {

        }
        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_UCRecentSelectionPanel != null)
            {
                _UCRecentSelectionPanel.SearchText = SearchTermTextBox.Text.Trim().ToLower();
            }
            if (_UCFriendSelectionPanel != null)
            {
                _UCFriendSelectionPanel.SearchText = SearchTermTextBox.Text.Trim().ToLower();
            }
            if (_UCGroupSelectionPanel != null)
            {
                _UCGroupSelectionPanel.SearchText = SearchTermTextBox.Text.Trim().ToLower();
            }
        }

        #endregion
    }
}

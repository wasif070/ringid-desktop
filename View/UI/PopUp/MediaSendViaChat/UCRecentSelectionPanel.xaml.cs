﻿using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.UI.Chat;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Recent;
using View.ViewModel;

namespace View.UI.PopUp.MediaSendViaChat
{
    /// <summary>
    /// Interaction logic for RecentChatPanel.xaml
    /// </summary>
    public partial class UCRecentSelectionPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCRecentSelectionPanel).Name);
        public ObservableCollection<RecentModel> _TempList = new ObservableCollection<RecentModel>();
        public delegate void OnSelectingComplete(long frindID, long groupID, string roomId);
        public event OnSelectingComplete _OnComplete = null;

        public UCRecentSelectionPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Property

        private ObservableCollection<RecentModel> _RecentList = new ObservableCollection<RecentModel>();
        public ObservableCollection<RecentModel> RecentList
        {
            get
            {
                return _RecentList;
            }
            set
            {
                _RecentList = value;
                this.OnPropertyChanged("RecentList");
            }
        }

        private string _SearchText = string.Empty;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                _SearchText = value;
                ShowSearchView();
            }
        }

        private ICommand _OnSelectCommand;
        public ICommand OnSelectCommand
        {
            get
            {
                if (_OnSelectCommand == null)
                {
                    _OnSelectCommand = new RelayCommand((param) => OnSelect(param));
                }
                return _OnSelectCommand;
            }
        }

        private void OnSelect(object param)
        {
            RecentModel model = (RecentModel)param;
            if (model != null && _OnComplete != null)
            {
                _OnComplete(model.FriendTableID, model.GroupID, model.RoomID);
                //MainSwitcher.PopupController.ucMediaShareToFriendPopup.OnClick(model.FriendTableID, model.GroupID, model.RoomID);
            }
        }

        #endregion

        #region Utility

        Thread _Thread = null;
        public void LoadRecentList()
        {
            try
            {
                if (UCChatLogPanel.Instance == null)
                {
                    if (_Thread != null && _Thread.IsAlive)
                    {
                        return;
                    }

                    _Thread = new Thread(() =>
                    {
                        Thread.Sleep(100);
                        List<RecentDTO> list = RecentChatCallActivityDAO.LoadRecentChatContactList(0, 0, null, ChatConstants.DAY_365_DAYS, 0, 50);
                        int count = 0;
                        foreach (RecentDTO newDTO in list)
                        {
                            if (count == 25) break;

                            if (!String.IsNullOrWhiteSpace(newDTO.RoomID)) continue;

                            RecentModel firstModel = _TempList.FirstOrDefault();
                            RecentModel newModel = new RecentModel(newDTO);
                            if (newDTO.FriendTableID > 0)
                            {
                                newModel.FriendInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(newDTO.FriendTableID);
                            }
                            else if (newDTO.GroupID > 0)
                            {
                                newModel.GroupInfoModel = RingIDViewModel.Instance.GetGroupInfoModelByGroupID(newDTO.GroupID);
                            }

                            if (firstModel == null || newDTO.Message.MessageDate >= firstModel.Time)
                            {
                                _TempList.InvokeInsert(0, newModel);
                            }
                            else
                            {
                                int max = _TempList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    if (newDTO.Message.MessageDate < _TempList.ElementAt(pivot).Time)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                _TempList.InvokeInsert(min, newModel);
                            }
                            count++;
                        }

                        RecentList = _TempList;
                        GC.SuppressFinalize(_TempList);
                    });
                    _Thread.Start();
                }
                else
                {
                    foreach (RecentModel recentModel in RingIDViewModel.Instance.ChatLogModelsList)
                    {
                        RecentList.InvokeAdd(recentModel);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadRecentList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowSearchView()
        {
            if (_Thread != null && _Thread.IsAlive)
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(SearchText))
            {
                List<RecentModel> modelList = _TempList.Where(P =>
                {
                    switch (P.Type)
                    {
                        case ChatConstants.SUBTYPE_FRIEND_CHAT:
                            return (P.FriendInfoModel.FullName.ToLower().Contains(SearchText)) || P.FriendInfoModel.UserIdentity.ToString().Contains(SearchText);
                        case ChatConstants.SUBTYPE_GROUP_CHAT:
                            return (P.GroupInfoModel.GroupName.ToLower().Contains(SearchText));
                        default:
                            return false;
                    }
                }).ToList();

                ObservableCollection<RecentModel> tempList1 = new ObservableCollection<RecentModel>();
                foreach (RecentModel model in modelList)
                {
                    tempList1.Add(model);
                }
                RecentList = tempList1;
            }
            else
            {
                RecentList = _TempList;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }
}

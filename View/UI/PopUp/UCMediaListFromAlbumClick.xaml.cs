﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCMediaListFromAlbumClick.xaml
    /// </summary>
    public partial class UCMediaListFromAlbumClick : PopUpBaseControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCMediaListFromAlbumClick).Name);
        public MediaContentDTO mediaDto;

        public MediaContentModel albumModel;
        public static UCMediaListFromAlbumClick Instance;

        public UCMediaListFromAlbumClick(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 0.0);//background Color Transparent Black
            SetParent(motherGrid);
            Visibility = Visibility.Hidden;
        }

        #region"Property "
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private ObservableCollection<SingleMediaModel> _SongsOfanAlbum;
        public ObservableCollection<SingleMediaModel> SongsOfanAlbum
        {
            get
            {
                return _SongsOfanAlbum;
            }
            set
            {
                _SongsOfanAlbum = value;
                this.OnPropertyChanged("SongsOfanAlbum");
            }
        }

        private int _Total;
        public int Total
        {
            get { return _Total; }
            set
            {
                if (_Total == value) return;
                _Total = value;
                OnPropertyChanged("Total");
            }
        }

        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (_MediaType == value) return;
                _MediaType = value;
                OnPropertyChanged("MediaType");
            }
        }

        private void SetEvent()
        {
            showMoreText.Click -= showMoreText_Click;
            showMoreText.Click += showMoreText_Click;
        }
        #endregion"Properties"

        public void ShowPopup(Object obj)
        {
            try
            {
                if (Visibility == Visibility.Visible)
                {
                    Visibility = Visibility.Hidden;
                }
                else
                {
                    if (obj is MediaContentModel)
                    {
                        albumModel = (MediaContentModel)obj;
                        albumNameTxt.Text = albumModel.AlbumName;
                        if (albumModel.MediaList == null) albumModel.MediaList = new ObservableCollection<SingleMediaModel>();
                        SongsOfanAlbum = albumModel.MediaList;
                        Total = albumModel.TotalMediaCount;
                        MediaType = albumModel.MediaType;
                        if (albumModel.MediaList == null || albumModel.MediaList.Count == 0)
                            MediaAlbumContentList(0, albumModel.AlbumId, albumModel.UserTableID, albumModel.MediaType);
                    }
                    Visibility = Visibility.Visible;
                    SetEvent();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideOrShowMoreVisibility()
        {
            try
            {
                if (albumModel.MediaList.Count != 0 && albumModel.MediaList.Count < albumModel.TotalMediaCount)
                {
                    ShowMore();
                }
                else
                {
                    HideShowMoreLoading();
                }
                this.Focusable = true;
                this.Focus();
            }
            catch (System.Exception ex)
            {
                log.Error("Exception => " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);
            }
        }

        public void ShowMore()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Visible;
                this.showMoreLoader.Visibility = Visibility.Collapsed;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            }
            catch (System.Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowLoading()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Collapsed;
                this.showMoreLoader.Visibility = Visibility.Visible;
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            }
            catch (System.Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideShowMoreLoading()
        {
            this.showMorePanel.Visibility = Visibility.Collapsed;
            this.showMoreText.Visibility = Visibility.Collapsed;
            this.showMoreLoader.Visibility = Visibility.Collapsed;
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        }

        private void ShowMore_PanelClick(object sender, RoutedEventArgs e)
        {
            ShowLoading();
            if (albumModel != null && albumModel.MediaList != null)
                MediaAlbumContentList(albumModel.MediaList.Count, albumModel.AlbumId, albumModel.UserTableID, albumModel.MediaType);
        }

        public void ClosePopUp()
        {
            //NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.Clear();
            //if (albumModel != null && albumModel.MediaList != null)
            //    albumModel.MediaList.Clear();
            Visibility = Visibility.Hidden;
            Hide();
            showMoreText.Click -= showMoreText_Click;

            //if (ucDownloadOrAddToAlbumPopUp != null
            //    && ucDownloadOrAddToAlbumPopUp.IsVisible)
            //{
            //    ucDownloadOrAddToAlbumPopUp.HidePopup();
            //}
        }

        #region "ICommand"

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        ICommand _blackSpaceClick;
        public ICommand CloseWindowCommand
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }

        private void OnClickBlackSpace(object param)
        {
            ClosePopUp();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {
        }
        #endregion

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            SingleMediaModel model = (SingleMediaModel)btn.DataContext;
            if (SongsOfanAlbum.Count > 0)
            {
                int idx = SongsOfanAlbum.IndexOf(model);
                if (idx >= 0)
                    MediaUtility.RunPlayList(false, Guid.Empty, SongsOfanAlbum, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, idx);
            }
            ClosePopUp();
        }

        private void playAllBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SongsOfanAlbum.Count > 0) MediaUtility.RunPlayList(false, Guid.Empty, SongsOfanAlbum);
                ClosePopUp();
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }

        public void stopShowMoreLoader()
        {
            showMoreLoader.Visibility = Visibility.Collapsed;
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        }

        private void showMoreText_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                showMoreText.Visibility = Visibility.Collapsed;
                showMoreLoader.Visibility = Visibility.Visible;
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));

                if (albumModel != null && albumModel.MediaList != null)
                    MediaAlbumContentList(albumModel.MediaList.Count, albumModel.AlbumId, albumModel.UserTableID, albumModel.MediaType);

            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }
        #region "Utility Methods"

        public void MediaAlbumContentList(int StartLimit, Guid albmId, long UtId, int mediaType, long userId = 0)
        {
            new ThradMediaAlbumContentList().StartThread(StartLimit, albmId, UtId, mediaType, userId);
        }

        #endregion "Utility Methods"

    }
}

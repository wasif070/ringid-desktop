﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using log4net;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI.Feed;
using View.Utility;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCNewStatusTagPopup.xaml
    /// </summary>
    public partial class UCNewStatusTagPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCNewStatusTagPopup).Name);

        private int _TotalSelectedFriend = 0;
        private ObservableCollection<UserBasicInfoModel> _FriendList = new ObservableCollection<UserBasicInfoModel>();
        //private ConcurrentQueue<UserBasicInfoModel> _ModelQueue = new ConcurrentQueue<UserBasicInfoModel>();
        //private BackgroundWorker _FrindListLoader = new BackgroundWorker();
        // private Func<List<UserBasicInfoModel>, int> _BcakToSearch = null;

        private const int EMO_POPUP_HEIGHT = 464;
        private int _ShowUpArrowPopup = 1;//0 - Default, 1 - UP, 2 - Down
        //private DispatcherTimer _FriendListResizeTimer = null;
        private ObservableCollection<UserBasicInfoModel> MainFriendList = new ObservableCollection<UserBasicInfoModel>();

        public UCNewStatusTagPopup(Grid motherGrid)
        {
            InitializeComponent();
            //_FrindListLoader.WorkerSupportsCancellation = true;
            //_FrindListLoader.WorkerReportsProgress = true;
            //_FrindListLoader.DoWork += FriendListLoad_DowWork;
            //_FrindListLoader.RunWorkerCompleted += FriendListLoad_RunWorkerCompleted;
            //_FrindListLoader.ProgressChanged += FriendListLoad_ProgressChanged;
            GC.SuppressFinalize(FriendList);
            this.DataContext = this;
            SetParent(motherGrid);
        }
        #region Property
        public ObservableCollection<UserBasicInfoModel> FriendList
        {
            get
            {
                return _FriendList;
            }
            set
            {
                _FriendList = value;
                this.OnPropertyChanged("FriendList");
            }
        }

        public int TotalSelectedFriend
        {
            get
            {
                return _TotalSelectedFriend;
            }
            set
            {
                _TotalSelectedFriend = value;
                this.OnPropertyChanged("TotalSelectedFriend");
            }
        }

        public int ShowUpArrowPopup
        {
            get { return _ShowUpArrowPopup; }
            set
            {
                if (_ShowUpArrowPopup == value)
                {
                    return;
                }
                _ShowUpArrowPopup = value;
                this.OnPropertyChanged("ShowUpArrowPopup");
            }
        }
        #endregion"Properties"

        #region Utility Methods

        private void OnSelect(object param)
        {
            UserBasicInfoModel model = param as UserBasicInfoModel;
            model.ShortInfoModel.IsVisibleInTagList = !model.ShortInfoModel.IsVisibleInTagList;
            OnCheck(model.ShortInfoModel.IsVisibleInTagList);
        }

        #region OldCode
        //public void LoadShowMoreData(List<UserBasicInfoModel> list)
        //{
        //    try
        //    {
        //        if (list != null && list.Count > 0)
        //        {
        //            foreach (UserBasicInfoModel model in list)
        //            {
        //                model.VisibilityModel.IsAddGroupMember = false;
        //                LoadShowMoreData(model);
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}

        //private void LoadShowMoreData(UserBasicInfoModel model)
        //{
        //    try
        //    {
        //        _ModelQueue.Enqueue(model);
        //        if (_FrindListLoader.IsBusy == false)
        //        {
        //            _FrindListLoader.RunWorkerAsync();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}


        //private void scvFriendList_ScrollChanged(object sender, ScrollChangedEventArgs e)
        //{
        //    try
        //    {
        //        if (Math.Abs(e.VerticalChange) > 0)
        //        {
        //            ChangeFriendOpenStatus();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: scvFriendList_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}

        //private void itemsControl_SizeChanged(object sender, SizeChangedEventArgs e)
        //{
        //    try
        //    {
        //        ChangeFriendOpenStatus();
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: itemsControl_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}

        //private void ChangeFriendOpenStatus()
        //{
        //    try
        //    {
        //        if (_FriendListResizeTimer == null)
        //        {
        //            _FriendListResizeTimer = new DispatcherTimer();
        //            _FriendListResizeTimer.Interval = TimeSpan.FromSeconds(1);
        //            _FriendListResizeTimer.Tick += (o, e) =>
        //            {
        //                if (popupTagFrnd != null && popupTagFrnd.IsOpen)
        //                {
        //                    List<object> infoList = HelperMethods.ChangeViewPortOpenedProperty(scvFriendList, itemsControl);
        //                    //if (infoList.Count > 0)
        //                    //{
        //                    //   new ThreadFriendPresenceInfo(infoList.Select(P => ((UserBasicInfoModel)P).ShortInfoModel.UserIdentity).ToList());
        //                    //}
        //                }

        //                _FriendListResizeTimer.Stop();
        //                _FriendListResizeTimer.Interval = TimeSpan.FromSeconds(0.2);
        //            };
        //        }

        //        _FriendListResizeTimer.Stop();
        //        _FriendListResizeTimer.Start();
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}

        //private void FriendListLoad_DowWork(object sender, DoWorkEventArgs e)
        //{
        //    try
        //    {
        //        UserBasicInfoModel model = null;
        //        while (_ModelQueue.TryDequeue(out model))
        //        {
        //            System.Threading.Thread.Sleep(10);
        //            _FrindListLoader.ReportProgress(0, model);
        //            if (_FrindListLoader.CancellationPending)
        //            {
        //                e.Cancel = true;
        //                return;
        //            }
        //        }

        //        if (_FrindListLoader.CancellationPending)
        //        {
        //            e.Cancel = true;
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //    e.Result = true;
        //}

        //private void FriendListLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //{
        //    UserBasicInfoModel newModel = ((UserBasicInfoModel)e.UserState);
        //    AddRecentModel(newModel);
        //}

        //private void FriendListLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Cancelled)
        //        {
        //            TotalSelectedFriend = 0;
        //            UserBasicInfoModel model = null;
        //            while (!_ModelQueue.IsEmpty)
        //            {
        //                _ModelQueue.TryDequeue(out model);
        //            }
        //            FriendList.Clear();
        //            SearchTermTextBox.Text = string.Empty;
        //        }
        //        else if (!e.Cancelled && e.Error == null)
        //        {
        //            IsFriendListLoaded = true;
        //            if (feedModel != null && feedModel.TaggedFriendsList != null && feedModel.TaggedFriendsList.Count > 0)
        //            {
        //                foreach (UserBasicInfoModel model in FriendList)
        //                {
        //                    model.ShortInfoModel.IsVisibleInTagList = (feedModel.TaggedFriendsList.Any(P => P.UserIdentity == model.ShortInfoModel.UserIdentity));
        //                }
        //            }
        //            else if (feedModel != null && feedModel.TaggedFriendsList == null)
        //            {
        //                foreach (UserBasicInfoModel model in FriendList)
        //                {
        //                    model.ShortInfoModel.IsVisibleInTagList = false;
        //                }
        //            }
        //            if (ucNewStatus != null && ucNewStatus.TaggedFriends != null && ucNewStatus.TaggedFriends.Count > 0)
        //            {
        //                foreach (UserBasicInfoModel model in FriendList)
        //                {
        //                    model.ShortInfoModel.IsVisibleInTagList = (ucNewStatus.TaggedUtids.Any(P => P == model.ShortInfoModel.UserTableID));
        //                }
        //            }
        //            else if (ucNewStatus.TaggedFriends == null || ucNewStatus.TaggedFriends.Count == 0)
        //            {
        //                foreach (UserBasicInfoModel model in FriendList)
        //                {
        //                    model.ShortInfoModel.IsVisibleInTagList = false;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}

        //private void AddRecentModel(UserBasicInfoModel model)
        //{
        //    try
        //    {
        //        int max = FriendList.Count;
        //        int min = 0;
        //        int pivot;

        //        while (max > min)
        //        {
        //            pivot = (min + max) / 2;
        //            if (String.CompareOrdinal(model.ShortInfoModel.FullName, FriendList.ElementAt(pivot).ShortInfoModel.FullName) > 0)
        //            {
        //                min = pivot + 1;
        //            }
        //            else
        //            {
        //                max = pivot;
        //            }
        //        }

        //        FriendList.Insert(min, model);
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}
        #endregion

        private NewStatusViewModel newStatusViewModel;
        private FeedModel feedModel;
        private List<long> AddedTags, RemovedTags;
        private string sts;
        private JArray StatusTagsJArray;
        //private bool IsFriendListLoaded = false;
        public void ShowPopup(UIElement target, FeedModel feedModel, string sts, JArray StatusTagsJArray)
        {
            try
            {
                SearchTermTextBox.Text = string.Empty;
                this.newStatusViewModel = null;
                this.ucShareSingleFeedView = null;
                this.feedModel = feedModel;
                AddedTags = null;
                RemovedTags = null;
                this.sts = sts;
                this.StatusTagsJArray = StatusTagsJArray;
                if (popupTagFrnd.IsOpen == false)
                {
                    //scvFriendList.ScrollToVerticalOffset(0);
                    //scvFriendList.ScrollChanged -= scvFriendList_ScrollChanged;
                    //itemsControl.SizeChanged -= itemsControl_SizeChanged;
                    //scvFriendList.ScrollChanged += scvFriendList_ScrollChanged;
                    //itemsControl.SizeChanged += itemsControl_SizeChanged;

                    popupTagFrnd.PlacementTarget = target;

                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 35;
                    if (buttonPosition < EMO_POPUP_HEIGHT && bottomSpace >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 1;
                        popupTagFrnd.HorizontalOffset = -239;
                        popupTagFrnd.VerticalOffset = -3;
                    }
                    else if (buttonPosition >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 2;
                        popupTagFrnd.HorizontalOffset = -239;
                        popupTagFrnd.VerticalOffset = 5;
                    }
                    else
                    {
                        ShowUpArrowPopup = 0;
                        popupTagFrnd.HorizontalOffset = -239;
                        popupTagFrnd.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                    }
                    //UserBasicInfoModel umodel = null;
                    //while (!_ModelQueue.IsEmpty)
                    //{
                    //    _ModelQueue.TryDequeue(out umodel);
                    //}
                    TotalSelectedFriend = 0;
                    FriendList.Clear();
                    //FriendList = null;
                    if (feedModel != null)

                        // if (!IsFriendListLoaded)
                        //   {
                        LoadFriendListIntoPopup();
                    // while (!IsFriendListLoaded) { }
                    //Thread.Sleep(2000);
                    //    }
                    //if (/*IsFriendListLoaded && */feedModel != null && feedModel.TaggedFriendsList != null && feedModel.TaggedFriendsList.Count > 0)
                    //{
                    //    foreach (UserBasicInfoModel model in MainFriendList)
                    //    {
                    //        model.ShortInfoModel.IsVisibleInTagList = (feedModel.TaggedFriendsList.Any(P => P.UserTableID == model.ShortInfoModel.UserTableID));
                    //        if (model.ShortInfoModel.IsVisibleInTagList)
                    //            TotalSelectedFriend += 1;
                    //    }
                    //}
                    //else if (/*IsFriendListLoaded && */feedModel.TaggedFriendsList == null)
                    //{
                    //    foreach (UserBasicInfoModel model in MainFriendList)
                    //    {
                    //        model.ShortInfoModel.IsVisibleInTagList = false;
                    //    }
                    //}
                    popupTagFrnd.IsOpen = true;
                    SearchTermTextBox.Focus();
                    //if (!IsFriendListLoaded)
                    //    LoadFriendListIntoPopup();
                }
                else
                {
                    popupTagFrnd.IsOpen = false;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
       
        public void ShowTagPopup(UIElement target, NewStatusViewModel newStatusViewModel)
        {
            try
            {
                this.newStatusViewModel = newStatusViewModel;
                this.feedModel = null;
                this.ucShareSingleFeedView = null;
                AddedTags = null;
                RemovedTags = null;
                this.sts = null;
                if (popupTagFrnd.IsOpen == false)
                {
                    //scvFriendList.ScrollToVerticalOffset(0);
                    //scvFriendList.ScrollChanged -= scvFriendList_ScrollChanged;
                    //itemsControl.SizeChanged -= itemsControl_SizeChanged;
                    //scvFriendList.ScrollChanged += scvFriendList_ScrollChanged;
                    //itemsControl.SizeChanged += itemsControl_SizeChanged;
                    popupTagFrnd.PlacementTarget = target;

                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 35;
                    if (buttonPosition < EMO_POPUP_HEIGHT && bottomSpace >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 1;
                        popupTagFrnd.HorizontalOffset = -239;
                        popupTagFrnd.VerticalOffset = -3;
                    }
                    else if (buttonPosition >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 2;
                        popupTagFrnd.HorizontalOffset = -239;
                        popupTagFrnd.VerticalOffset = 5;
                    }
                    else
                    {
                        ShowUpArrowPopup = 0;
                        popupTagFrnd.HorizontalOffset = -239;
                        popupTagFrnd.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                    }
                    if (FriendList == null) FriendList = new ObservableCollection<UserBasicInfoModel>();
                    //UserBasicInfoModel umodel = null;
                    //while (!_ModelQueue.IsEmpty)
                    //{
                    //    _ModelQueue.TryDequeue(out umodel);
                    //}
                    TotalSelectedFriend = 0;
                    FriendList.Clear();
                    // if (!IsFriendListLoaded)
                    //   {
                    LoadFriendListIntoPopup();
                    // while (!IsFriendListLoaded) { }
                    //Thread.Sleep(2000);
                    //   }

                    if (newStatusViewModel != null && newStatusViewModel.TaggedFriends != null && newStatusViewModel.TaggedFriends.Count > 0)
                    {
                        foreach (UserBasicInfoModel model in MainFriendList)
                        {
                            model.ShortInfoModel.IsVisibleInTagList = (newStatusViewModel.TaggedFriends.Any(P => P.UserTableID == model.ShortInfoModel.UserTableID));
                            if (model.ShortInfoModel.IsVisibleInTagList)
                                TotalSelectedFriend += 1;
                        }
                    }
                    else if (newStatusViewModel.TaggedFriends == null || newStatusViewModel.TaggedFriends.Count == 0)
                    {
                        foreach (UserBasicInfoModel model in MainFriendList)
                        {
                            model.ShortInfoModel.IsVisibleInTagList = false;
                        }
                    }
                    popupTagFrnd.IsOpen = true;
                    SearchTermTextBox.Focus();
                    //if (!IsFriendListLoaded)
                    //    LoadFriendListIntoPopup();
                }
                else
                {
                    popupTagFrnd.IsOpen = false;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        private UCShareSingleFeedView ucShareSingleFeedView;
        public void ShowTagPopup(UIElement target, UCShareSingleFeedView UCShareSingleFeedViewInstance)
        {
            try
            {
                this.ucShareSingleFeedView = UCShareSingleFeedViewInstance;
                this.feedModel = null;
                this.newStatusViewModel = null;
                AddedTags = null;
                RemovedTags = null;
                this.sts = null;
                if (popupTagFrnd.IsOpen == false)
                {
                    //scvFriendList.ScrollToVerticalOffset(0);
                    //scvFriendList.ScrollChanged -= scvFriendList_ScrollChanged;
                    //itemsControl.SizeChanged -= itemsControl_SizeChanged;
                    //scvFriendList.ScrollChanged += scvFriendList_ScrollChanged;
                    //itemsControl.SizeChanged += itemsControl_SizeChanged;
                    popupTagFrnd.PlacementTarget = target;

                    int buttonPosition = (int)target.PointToScreen(new Point(0, 0)).Y;
                    int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 35;
                    if (buttonPosition < EMO_POPUP_HEIGHT && bottomSpace >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 1;
                        popupTagFrnd.HorizontalOffset = -239;
                        popupTagFrnd.VerticalOffset = -3;
                    }
                    else if (buttonPosition >= EMO_POPUP_HEIGHT)
                    {
                        ShowUpArrowPopup = 2;
                        popupTagFrnd.HorizontalOffset = -239;
                        popupTagFrnd.VerticalOffset = 5;
                    }
                    else
                    {
                        ShowUpArrowPopup = 0;
                        popupTagFrnd.HorizontalOffset = -239;
                        popupTagFrnd.VerticalOffset = -(EMO_POPUP_HEIGHT - bottomSpace);
                    }
                    //UserBasicInfoModel umodel = null;
                    //while (!_ModelQueue.IsEmpty)
                    //{
                    //    _ModelQueue.TryDequeue(out umodel);
                    //}
                    TotalSelectedFriend = 0;
                    FriendList.Clear();
                    //if (!IsFriendListLoaded)
                    //{
                    LoadFriendListIntoPopup();
                    // while (!IsFriendListLoaded) { }
                    //Thread.Sleep(2000);
                    // }

                    if (ucShareSingleFeedView != null && ucShareSingleFeedView.TaggedFriends != null && ucShareSingleFeedView.TaggedFriends.Count > 0)
                    {
                        foreach (UserBasicInfoModel model in MainFriendList)
                        {
                            model.ShortInfoModel.IsVisibleInTagList = (ucShareSingleFeedView.TaggedUtids.Any(P => P == model.ShortInfoModel.UserTableID));
                            if (model.ShortInfoModel.IsVisibleInTagList)
                                TotalSelectedFriend += 1;
                        }
                    }
                    else if (ucShareSingleFeedView.TaggedFriends == null || ucShareSingleFeedView.TaggedFriends.Count == 0)
                    {
                        foreach (UserBasicInfoModel model in MainFriendList)
                        {
                            model.ShortInfoModel.IsVisibleInTagList = false;
                        }
                    }
                    popupTagFrnd.IsOpen = true;
                    SearchTermTextBox.Focus();
                    //if (!IsFriendListLoaded)
                    //    LoadFriendListIntoPopup();
                }
                else
                {
                    popupTagFrnd.IsOpen = false;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LoadFriendListIntoPopup()
        {
            GC.SuppressFinalize(MainFriendList);
            foreach (var model in RingIDViewModel.Instance.FavouriteFriendList)
            {
                model.VisibilityModel.IsAddGroupMember = false;
                MainFriendList.Add(model);
            }
            foreach (var model in RingIDViewModel.Instance.TopFriendList)
            {
                model.VisibilityModel.IsAddGroupMember = false;
                MainFriendList.Add(model);
            }
            foreach (UserBasicInfoDTO userDTO in FriendListController.Instance.FriendDataContainer.NonFavoriteList)
            {
                UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                model.VisibilityModel.IsAddGroupMember = false;
                MainFriendList.Add(model);
            }
            MainFriendList = new ObservableCollection<UserBasicInfoModel>(from i in MainFriendList orderby i.ShortInfoModel.FullName select i);

            FriendList = MainFriendList;

            //Application.Current.Dispatcher.BeginInvoke(() =>
            //{
            //    List<UserBasicInfoModel> modelList = new List<UserBasicInfoModel>();
            //    GC.SuppressFinalize(modelList);

            //    /*modelList.AddRange(RingIDViewModel.Instance.FavouriteFriendList.ToList());
            //    modelList.AddRange(RingIDViewModel.Instance.TopFriendList.Where(P => !modelList.Any(Q => Q.ShortInfoModel.UserIdentity == P.ShortInfoModel.UserIdentity)).ToList());
            //    modelList.AddRange(RingIDViewModel.Instance.NonFavouriteFriendList.Where(P => !modelList.Any(Q => Q.ShortInfoModel.UserIdentity == P.ShortInfoModel.UserIdentity)).ToList());*/

            //    foreach (UserBasicInfoModel model in FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values)
            //    {
            //        if (model.ShortInfoModel != null && model.ShortInfoModel.UserIdentity > 0 && model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && model.ShortInfoModel.ContactType != SettingsConstants.SPECIAL_CONTACT)
            //        {
            //            modelList.Add(model);
            //        }
            //    }
            //    List<long> idList = new List<long>();
            //    foreach (UserBasicInfoModel temp in modelList)
            //    {
            //        idList.Add(temp.ShortInfoModel.UserIdentity);
            //    }
            //    LoadShowMoreData(modelList);
            //}, DispatcherPriority.ApplicationIdle);
        }

        public void Dispose()
        {
            try
            {
                //if (_FrindListLoader.IsBusy)
                //{
                //    _FrindListLoader.CancelAsync();
                //    ClearAllSelectedFriends();
                //}
                //else
                {
                    SearchTermTextBox.Text = string.Empty;
                    TotalSelectedFriend = 0;
                    //UserBasicInfoModel model = null;
                    //while (!_ModelQueue.IsEmpty)
                    //{
                    //    _ModelQueue.TryDequeue(out model);
                    //    GC.SuppressFinalize(model);
                    //    model = null;
                    //}
                    ClearAllSelectedFriends();
                    MainFriendList.Clear();
                    FriendList.Clear();
                    //FriendList = null;
                    //scvFriendList.ScrollChanged -= scvFriendList_ScrollChanged;
                    //itemsControl.SizeChanged -= itemsControl_SizeChanged;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility methods

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        private void OnEscapeCommand()
        {
            popupTagFrnd.IsOpen = false;
        }

        private ICommand _PopupTagFrndClosed;
        public ICommand PopupTagFrndClosed
        {
            get
            {
                if (_PopupTagFrndClosed == null) _PopupTagFrndClosed = new RelayCommand(param => OnPopupTagFrndClosed());
                return _PopupTagFrndClosed;
            }
        }
        private void OnPopupTagFrndClosed()
        {
            Hide();
            Dispose();
        }

        private ICommand _OnSelectCommand;
        public ICommand OnSelectCommand
        {
            get
            {
                if (_OnSelectCommand == null)
                {
                    _OnSelectCommand = new RelayCommand((param) => OnSelect(param));
                }
                return _OnSelectCommand;
            }
        }
        private ICommand _OnCheckCommand;
        public ICommand OnCheckCommand
        {
            get
            {
                if (_OnCheckCommand == null)
                {
                    _OnCheckCommand = new RelayCommand((param) => OnCheck(param));
                }
                return _OnCheckCommand;
            }
        }

        private void OnCheck(object param)
        {
            if (param is bool)
            {
                bool chk = (bool)param;
                if (chk)
                    TotalSelectedFriend += 1;
                else
                    TotalSelectedFriend -= 1;
            }
        }

        private ICommand _SubmitTagClick;
        public ICommand SubmitTagClick
        {
            get
            {
                if (_SubmitTagClick == null)
                {
                    _SubmitTagClick = new RelayCommand((param) => OnSubmitTagClick());
                }
                return _SubmitTagClick;
            }
        }
        private void OnSubmitTagClick()
        {
            if (newStatusViewModel != null)
            {
                if (newStatusViewModel.TaggedFriends == null) newStatusViewModel.TaggedFriends = new ObservableCollection<UserShortInfoModel>();
                if (newStatusViewModel.TaggedUtids == null) newStatusViewModel.TaggedUtids = new List<long>();
                foreach (UserBasicInfoModel item in MainFriendList)
                {
                    if (item.ShortInfoModel.IsVisibleInTagList)
                    {
                        if (!newStatusViewModel.TaggedFriends.Any(P => P.UserTableID == item.ShortInfoModel.UserTableID))
                        {
                            newStatusViewModel.TaggedFriends.Add(item.ShortInfoModel);
                            newStatusViewModel.OnPropertyChanged("TaggedFriends");
                            newStatusViewModel.TaggedUtids.Add(item.ShortInfoModel.UserTableID);
                        }
                    }
                    else
                    {
                        if (newStatusViewModel.TaggedFriends.Any(P => P.UserTableID == item.ShortInfoModel.UserTableID))
                        {
                            newStatusViewModel.TaggedFriends.Remove(item.ShortInfoModel);
                            if (newStatusViewModel.TaggedFriends.Count == 0) newStatusViewModel.TagListHolderVisibility = Visibility.Collapsed;
                            newStatusViewModel.OnPropertyChanged("TaggedFriends");
                            newStatusViewModel.TaggedUtids.Remove(item.ShortInfoModel.UserTableID);
                        }
                    }
                }
                //newStatusViewModel.OnPropertyChanged("TaggedFriends");
                //ucNewStatus.SetTagPanelVisbility(Visibility.Visible);
                popupTagFrnd.IsOpen = false;
            }
            else if (ucShareSingleFeedView != null)
            {
                if (ucShareSingleFeedView.TaggedFriends == null) ucShareSingleFeedView.TaggedFriends = new ObservableCollection<UserShortInfoModel>();
                if (ucShareSingleFeedView.TaggedUtids == null) ucShareSingleFeedView.TaggedUtids = new List<long>();
                foreach (UserBasicInfoModel item in MainFriendList)
                {
                    if (item.ShortInfoModel.IsVisibleInTagList)
                    {
                        if (!ucShareSingleFeedView.TaggedFriends.Any(P => P.UserTableID == item.ShortInfoModel.UserTableID))
                        {
                            ucShareSingleFeedView.TaggedFriends.Add(item.ShortInfoModel);
                            ucShareSingleFeedView.OnPropertyChanged("UCShareSingleFeedViewInstance");
                            ucShareSingleFeedView.TaggedUtids.Add(item.ShortInfoModel.UserTableID);
                        }
                    }
                    else
                    {
                        if (ucShareSingleFeedView.TaggedFriends.Any(P => P.UserTableID == item.ShortInfoModel.UserTableID))
                        {
                            ucShareSingleFeedView.TaggedFriends.Remove(item.ShortInfoModel);
                            ucShareSingleFeedView.OnPropertyChanged("UCShareSingleFeedViewInstance");
                            ucShareSingleFeedView.TaggedUtids.Remove(item.ShortInfoModel.UserTableID);
                        }
                    }
                }
                popupTagFrnd.IsOpen = false;
            }
            //else if (feedModel != null)
            //{
            //    foreach (UserBasicInfoModel item in MainFriendList)
            //    {
            //        if (item.ShortInfoModel.IsVisibleInTagList) // (feedModel.TaggedFriendsList.Any(P => P.UserIdentity == model.ShortInfoModel.UserIdentity))
            //        {
            //            if (feedModel.TaggedFriendsList == null || (feedModel.TaggedFriendsList != null && !feedModel.TaggedFriendsList.Any(P => P.UserIdentity == item.ShortInfoModel.UserIdentity)))
            //            {
            //                if (AddedTags == null) AddedTags = new List<long>();
            //                AddedTags.Add(item.ShortInfoModel.UserTableID);
            //            }
            //        }
            //        else
            //        {
            //            if (feedModel.TaggedFriendsList != null && feedModel.TaggedFriendsList.Any(P => P.UserTableID == item.ShortInfoModel.UserTableID))
            //            {
            //                if (RemovedTags == null) RemovedTags = new List<long>();
            //                RemovedTags.Add(item.ShortInfoModel.UserTableID);
            //            }
            //        }
            //    }
            //    popupTagFrnd.IsOpen = false;
            //    //edit req
            //    if (AddedTags != null || RemovedTags != null || !sts.Equals(feedModel.Status) || !HelperMethods.IsTagDTOsSameasJArray(StatusTagsJArray, feedModel.StatusTags))
            //    {
            //        if (feedModel.GroupId > 0)
            //        {
            //            LocationDTO location = (feedModel.locationModel != null) ? feedModel.locationModel.GetLocationDTOFromModel() : null;
            //            EditFeed(0, feedModel.NewsfeedId, 0, feedModel.GroupId, sts, StatusTagsJArray, AddedTags, RemovedTags, location);
            //        }
            //        else if (feedModel.FriendShortInfoModel != null && feedModel.FriendShortInfoModel.UserTableID > 0)
            //        {
            //            LocationDTO location = (feedModel.locationModel != null) ? feedModel.locationModel.GetLocationDTOFromModel() : null;
            //            EditFeed(0, feedModel.NewsfeedId, feedModel.FriendShortInfoModel.UserIdentity, 0, sts, StatusTagsJArray, AddedTags, RemovedTags, location);
            //        }
            //        else
            //        {
            //            if (feedModel.PreviewUrl != null)
            //            {
            //                if (feedModel.PreviewUrl.Equals(sts) || feedModel.PreviewUrl.Equals("http://" + sts) || feedModel.PreviewUrl.Equals("https://" + sts) || feedModel.PreviewUrl.Equals("http://" + sts + "/") || feedModel.PreviewUrl.Equals("https://" + sts + "/"))
            //                    sts = "";
            //            }
            //            LocationDTO location = (feedModel.locationModel != null) ? feedModel.locationModel.GetLocationDTOFromModel() : null;
            //            EditFeed(0, feedModel.NewsfeedId, sts, StatusTagsJArray, AddedTags, RemovedTags, location);
            //        }
            //    }
            //}
        }

        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchTermTextBox.Text.Trim() != string.Empty)
            {
                string txt = SearchTermTextBox.Text.Trim();
                List<UserBasicInfoModel> list = (from j in MainFriendList where j.ShortInfoModel.FullName.ToLower().Contains(txt.ToLower()) || j.ShortInfoModel.UserIdentity.ToString().Contains(txt) select j).ToList();
                ObservableCollection<UserBasicInfoModel> tempList = new ObservableCollection<UserBasicInfoModel>();
                foreach (UserBasicInfoModel model in list)
                {
                    tempList.Add(model);
                }
                FriendList = tempList;
            }
            else
            {
                FriendList = MainFriendList;
            }
        }
        #endregion

        #region Event Handler

        
        //private void CheckBox_Checked(object sender, RoutedEventArgs e)
        //{            
        //    TotalSelectedFriend += 1;
        //}

        //private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        //{            
        //    TotalSelectedFriend -= 1;
        //}

        public void ClearAllSelectedFriends()
        {
            foreach (var item in MainFriendList)
            {
                item.ShortInfoModel.IsVisibleInTagList = false;
            }
            TotalSelectedFriend = 0;
        }

        #endregion Event Handler

        #region "Utility Methods"


        //public void EditFeed(int pvc, long NfId, long FriendIdentity, long CircleId, string sts, JArray JobjStsTags, List<long> AddedTags, List<long> RemovedTags, LocationDTO locationDTO)
        //{
        //    new ThradEditFeed().StartThread(pvc, NfId, FriendIdentity, CircleId, sts, JobjStsTags, AddedTags, RemovedTags, locationDTO);
        //}

        //public void EditFeed(int pvc, long NfId, string sts, JArray JobjStsTags, List<long> AddedTags, List<long> RemovedTags, LocationDTO locationDTO)
        //{
        //    new ThradEditFeed().StartThread(pvc, NfId, sts, JobjStsTags, AddedTags, RemovedTags, locationDTO);
        //}

        //public void EditFeed(int pvc, long NfId, string sts, JArray JobjStsTags, long FriendIdentity, long CircleId, LocationDTO locationDTO)
        //{
        //    new ThradEditFeed().StartThread(pvc, NfId, sts, JobjStsTags, FriendIdentity, CircleId, locationDTO);
        //}

        #endregion "Utility Methods"
    }
}

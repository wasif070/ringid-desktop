﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Models.Constants;
using Models.Entity;
using View.BindingModels;
using View.Utility;
using View.Utility.Feed;
using View.ViewModel;
using Models.DAO;
using View.Utility.DataContainer;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCDownloadOrAddToAlbumPopUp.xaml
    /// </summary>
    public partial class UCDownloadOrAddToAlbumPopUp : PopUpBaseControl, INotifyPropertyChanged
    {
        MediaContentModel model;
        Func<int> _OnClosing = null;
        public SingleMediaModel PlayingMedia = null;
        public bool IsOpenCreateNewAlbum;
        public static UCDownloadOrAddToAlbumPopUp Instance;

        #region "Constructor"
        public UCDownloadOrAddToAlbumPopUp(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 0.0);//background Color Transparent Black
            SetParent(motherGrid);
            Visibility = Visibility.Hidden;
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Properties"
        private ObservableCollection<MediaContentModel> _MyMediaList;
        public ObservableCollection<MediaContentModel> MyMediaList
        {
            get
            {
                return _MyMediaList;
            }
            set
            {
                _MyMediaList = value;
                this.OnPropertyChanged("MyMediaList");
            }
        }
        //private bool _IsAddBtnEnabled = true;
        //public bool IsAddBtnEnabled
        //{
        //    get { return _IsAddBtnEnabled; }
        //    set
        //    {
        //        if (_IsAddBtnEnabled == value) return;
        //        _IsAddBtnEnabled = value;
        //        OnPropertyChanged("IsAddBtnEnabled");
        //    }
        //}
        private int _MediaType;
        public int MediaType
        {
            get
            {
                return _MediaType;
            }
            set
            {
                _MediaType = value;
                this.OnPropertyChanged("MediaType");
            }
        }
        private ImageSource _LoaderAddToNewAlbum;
        public ImageSource LoaderAddToNewAlbum
        {
            get
            {
                return _LoaderAddToNewAlbum;
            }
            set
            {
                if (value == _LoaderAddToNewAlbum)
                    return;
                _LoaderAddToNewAlbum = value;
                OnPropertyChanged("LoaderAddToNewAlbum");
            }
        }

        private bool _basicMedia;
        public bool BasicMedia
        {
            get
            {
                return _basicMedia;
            }
            set
            {
                _basicMedia = value;
                this.OnPropertyChanged("BasicMedia");
            }
        }
        //private ImageSource _LoaderAddToExistingAlbum;
        //public ImageSource LoaderAddToExistingAlbum
        //{
        //    get
        //    {
        //        return _LoaderAddToExistingAlbum;
        //    }
        //    set
        //    {
        //        if (value == _LoaderAddToExistingAlbum)
        //            return;
        //        _LoaderAddToExistingAlbum = value;
        //        OnPropertyChanged("LoaderAddToExistingAlbum");
        //    }
        //}
        private string _AddOrDownloadToNewAlbum = string.Empty;
        public string AddOrDownloadToNewAlbum
        {
            get
            {
                return _AddOrDownloadToNewAlbum;
            }
            set
            {
                if (value == _AddOrDownloadToNewAlbum)
                    return;
                _AddOrDownloadToNewAlbum = value;
                OnPropertyChanged("AddOrDownloadToNewAlbum");
            }
        }
        private string _MusicOrVideoText = string.Empty;
        public string MusicOrVideoText
        {
            get
            {
                return _MusicOrVideoText;
            }
            set
            {
                if (value == _MusicOrVideoText)
                    return;
                _MusicOrVideoText = value;
                OnPropertyChanged("MusicOrVideoText");
            }
        }
        private bool _IsDownload = false;
        public bool IsDownload
        {
            get
            {
                return _IsDownload;
            }
            set
            {
                if (value == _IsDownload)
                    return;
                _IsDownload = value;
                OnPropertyChanged("IsDownload");
            }
        }

        private bool _IsAllEnabled = true;
        public bool IsAllEnabled
        {
            get { return _IsAllEnabled; }
            set
            {
                if (_IsAllEnabled == value) return;
                _IsAllEnabled = value;
                OnPropertyChanged("IsAllEnabled");
            }
        }
        #endregion"Properties"

        #region "Public Method"
        public void ShowPopup(SingleMediaModel PlayingMedia, int MediaType, bool isDownload, bool basicMedia, Func<int> onClosing)
        {
            _OnClosing = onClosing;
            this.PlayingMedia = PlayingMedia;
            this.MediaType = MediaType;
            this.IsDownload = isDownload;
            BasicMedia = basicMedia;
            MusicOrVideoText = MediaType == SettingsConstants.MEDIA_TYPE_AUDIO ? "music" : "video";
            //AlbumCount = MediaType == 1 ? RingIDViewModel.Instance.MyAudioAlbums.Count : RingIDViewModel.Instance.MyVideoAlbums.Count;
            // MyMediaList = (MediaType == 1) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;

            if (!isDownload)
            {
                if (MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                {
                    AddOrDownloadToNewAlbum = NotificationMessages.ADD_TO_MUSIC_ALBUM;
                }
                else
                {
                    AddOrDownloadToNewAlbum = NotificationMessages.ADD_TO_VIDEO_ALBUM;
                }
            }
            else
            {
                if (MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                {
                    AddOrDownloadToNewAlbum = NotificationMessages.DOWNLOAD_TO_MUSIC_ALBUM;
                }
                else
                {
                    AddOrDownloadToNewAlbum = NotificationMessages.DOWNLOAD_TO_VIDEO_ALBUM;
                }
            }
            Visibility = Visibility.Visible;
        }

        public bool IsItemAlreadyinAlbum(ObservableCollection<SingleMediaModel> ItemsOfAnAlbum, SingleMediaModel itemToAdd)
        {
            if (string.IsNullOrEmpty(itemToAdd.StreamUrl)) return false;
            foreach (var item in ItemsOfAnAlbum)
            {
                if (item.StreamUrl.Equals(itemToAdd.StreamUrl)) return true;
            }
            return false;
        }
        public bool IsAlbumNameExists(string name)
        {
            ObservableCollection<MediaContentModel> MyMediaList = (MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
            foreach (var item in MyMediaList)
            {
                if (item.AlbumName.Trim().Equals(name)) return true;
            }
            return false;
        }
        CustomFileDownloader fileDownloader = null;
        public void MakeDownloaderStart()
        {
            //if (AddOrDownloadToNewAlbum.Trim().Equals(NotificationMessages.DOWNLOAD_TO_NEW_ALBUM)
            //    || AddOrDownloadToExistingAlbum.Trim().Equals(NotificationMessages.DOWNLOAD_TO_EXISTING_ALBUM))
            //{
            //popupAddToAlbum.IsOpen = false;

            UploadingModel _uploadingModel = new UploadingModel();
            _uploadingModel.IsDownload = true;
            _uploadingModel.ContentId = this.PlayingMedia.ContentId;
            _uploadingModel.UploadingContent = this.PlayingMedia.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO ? "1 audio is downloading..." : "1 video is downloading...";
            RingIDViewModel.Instance.UploadingModelList.Add(_uploadingModel);

            if (System.IO.File.Exists(HelperMethods.GetDownloadedFilePathFromStreamURL(PlayingMedia.StreamUrl, PlayingMedia.ContentId)))
            {
                //this.PlayingMedia.DeleteDownloadMedia(PlayingMedia.ContentId);
                PlayingMedia.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                PlayingMedia.DownloadProgress = 0;
                MediaDAO.Instance.DeleteFromDownloadedMediasTable(PlayingMedia.ContentId);
                //TODO delete downloaded files from PC
            }

            if (!MediaDataContainer.Instance.CurrentDownloads.TryGetValue(PlayingMedia.ContentId, out fileDownloader))
            {
                fileDownloader = new CustomFileDownloader(PlayingMedia);
                MediaDataContainer.Instance.CurrentDownloads[PlayingMedia.ContentId] = fileDownloader;
            }
            if (this.fileDownloader.Files != null && this.fileDownloader.Files.Count > 0)
            {
                //<<<<<<< .working
                this.fileDownloader.Files.Add(new FileDownloader.FileInfo(ServerAndPortSettings.GetVODServerResourceURL + this.PlayingMedia.StreamUrl, this.PlayingMedia.ContentId.ToString()));
                //=======
                //                this.fileDownloader.Files.Clear();
                //>>>>>>> .merge-right.r2138
            }

            //if (fileDownloader.Files == null || (this.fileDownloader.Files != null && this.fileDownloader.Files.Count == 0))
            //{
            this.fileDownloader.Files.Add(new FileDownloader.FileInfo(ServerAndPortSettings.GetVODServerResourceURL + this.PlayingMedia.StreamUrl, this.PlayingMedia.ContentId.ToString()));
            //}
            this.fileDownloader.Start();
            //}
        }

        public void EnableButtons(Guid albumId)
        {
            IsAllEnabled = true;
            LoaderAddToNewAlbum = null;
            NewAlbumTxtBox.Text = "";
            model = (MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? RingIDViewModel.Instance.MyAudioAlbums.Where(P => P.AlbumId == albumId).FirstOrDefault() : RingIDViewModel.Instance.MyVideoAlbums.Where(P => P.AlbumId == albumId).FirstOrDefault();
            if (model != null) model.IsLoaderOn = false;
            //ObservableCollection<MediaContentModel> MyMediaList = (MediaType == 1) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
            //foreach (var item in MyMediaList)
            //{
            //    item.IsLoaderOn = false;
            //}
        }

        public void HidePopup()
        {
            if (_OnClosing != null)
            {
                if (model != null && model.IsLoaderOn == true)
                    model.IsLoaderOn = false;

                //SearchAlbumTextBox.Loaded -= SearchAlbumTextBox_Loaded;
                NewAlbumTxtBox.Text = "";
                SearchAlbumTextBox.Text = "";
                createAlbumGridPanel.Visibility = Visibility.Collapsed;
                craeteAlbumDock.Visibility = Visibility.Visible;
                _OnClosing();
                Visibility = Visibility.Hidden;

                if (LoaderAddToNewAlbum != null)
                    LoaderAddToNewAlbum = null;
                IsAllEnabled = true;
                Hide();
                if (UCAlbumContentView.Instance != null)
                    UCAlbumContentView.Instance.OnLoadedControl();
            }
        }
        #endregion

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            if (IsOpenCreateNewAlbum)
            {
                createAlbumGridPanel.Visibility = Visibility.Visible;
                craeteAlbumDock.Visibility = Visibility.Collapsed;
                this.NewAlbumTxtBox.Focusable = true;
                this.NewAlbumTxtBox.Focus();
            }
            else
            {
                this.SearchAlbumTextBox.Focusable = true;
                this.SearchAlbumTextBox.Focus();
            }
        }

        ICommand _blackSpaceClick;
        public ICommand BlackSpaceClicked
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }

        private void OnClickBlackSpace(object param)
        {
            HidePopup();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {
        }
        private ICommand _AddOrDownloadCommand;
        public ICommand AddOrDownloadCommand
        {
            get
            {
                if (_AddOrDownloadCommand == null)
                {
                    _AddOrDownloadCommand = new RelayCommand((param) => OnAddOrDownloadCommand(param));
                }
                return _AddOrDownloadCommand;
            }
        }

        public void OnAddOrDownloadCommand(object param)
        {
            model = (MediaContentModel)param;
            if (model.MediaList != null && model.MediaList.Count > 0 && IsItemAlreadyinAlbum(model.MediaList, PlayingMedia))
            {
                UIHelperMethods.ShowWarning("This Item already exists in this Album!", "Add media");
            }
            else
            {
                model.IsLoaderOn = true;
                IsAllEnabled = false;
                new ThradAddSingleMediatoAlbum(model, MediaType, PlayingMedia.StreamUrl, PlayingMedia.ThumbUrl, PlayingMedia.Duration, PlayingMedia.Title, PlayingMedia.Artist, PlayingMedia.ThumbImageWidth, PlayingMedia.ThumbImageHeight, PlayingMedia.Privacy).StartThread();
                //new ThradAddSingleMediatoAlbum(model.AlbumId, PlayingMedia).StartThread();
            }
            if (AddOrDownloadToNewAlbum.Trim().Equals(NotificationMessages.DOWNLOAD_TO_MUSIC_ALBUM) || AddOrDownloadToNewAlbum.Trim().Equals(NotificationMessages.DOWNLOAD_TO_VIDEO_ALBUM))
            {
                MakeDownloaderStart();
            }
        }

        ICommand _TextBlockMouseLeftButtonUp;
        public ICommand TextBlockMouseLeftButtonUp
        {
            get
            {
                if (_TextBlockMouseLeftButtonUp == null)
                {
                    _TextBlockMouseLeftButtonUp = new RelayCommand(param => OnTextBlockMouseLeftButtonUp(param));
                }
                return _TextBlockMouseLeftButtonUp;
            }
        }
        private void OnTextBlockMouseLeftButtonUp(object sender)
        {
            try
            {
                if (sender is MediaContentModel)
                {
                    model = (MediaContentModel)sender;
                    //HidePopup();
                    if (UCMediaListFromAlbumClick.Instance == null)
                        UCMediaListFromAlbumClick.Instance = new UCMediaListFromAlbumClick(UCGuiRingID.Instance.MotherPanel);
                    UCMediaListFromAlbumClick.Instance.Show();
                    UCMediaListFromAlbumClick.Instance.ShowPopup(model);
                    UCMediaListFromAlbumClick.Instance.OnRemovedUserControl += () =>
                    {
                        UCMediaListFromAlbumClick.Instance = null;
                        this.SearchAlbumTextBox.Focusable = true;
                        this.SearchAlbumTextBox.Focus();
                    };
                }
            }
            catch (Exception)
            {

            }
        }

        ICommand _AddToExistingAlbumCommand;
        public ICommand AddToExistingAlbumCommand
        {
            get
            {
                if (_AddToExistingAlbumCommand == null)
                {
                    _AddToExistingAlbumCommand = new RelayCommand(param => OnAddToExistingAlbumCommand(param));
                }
                return _AddToExistingAlbumCommand;
            }
        }
        private void OnAddToExistingAlbumCommand(object sender)
        {
            if (sender is MediaContentModel)
            {
                model = (MediaContentModel)sender;
                if (model.MediaList != null && model.MediaList.Count > 0 && IsItemAlreadyinAlbum(model.MediaList, PlayingMedia))
                {
                    UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, "This Item already exists in this Album!");
                }
                else
                {
                    model.IsLoaderOn = true;
                    IsAllEnabled = false;
                    //new ThreadAddMediaToNewOrExistingAlbum(model, PlayingMedia, null).StartThread();
                    ThreadAddMediaToNewOrExistingAlbum thrd = new ThreadAddMediaToNewOrExistingAlbum(model, PlayingMedia, null);
                    thrd.CallBack += (albumId) =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (UCMediaViewAddToAlbumPopup.Instance != null)
                            {
                                UCMediaViewAddToAlbumPopup.Instance.EnableButtons();
                            }
                            if (albumId != Guid.Empty)
                            {
                                EnableButtons(albumId);
                            }
                        });
                    };
                    thrd.StartThread();
                }
                if (IsDownload)
                {
                    MakeDownloaderStart();
                }
                HidePopup();
            }
        }

        ICommand _AddToAlbumCommand;
        public ICommand AddToAlbumCommand
        {
            get
            {
                if (_AddToAlbumCommand == null)
                {
                    _AddToAlbumCommand = new RelayCommand(param => OnAddToAlbumCommand());
                }
                return _AddToAlbumCommand;
            }
        }
        private void OnAddToAlbumCommand()
        {
            if (!String.IsNullOrEmpty(NewAlbumTxtBox.Text))
            {
                string newAlbnm = NewAlbumTxtBox.Text.Trim();
                if (IsAlbumNameExists(newAlbnm)) UIHelperMethods.ShowWarning("An album already exists in this Name!", "Album exists");
                else
                {
                    LoaderAddToNewAlbum = ImageObjects.LOADING_WATCH;
                    IsAllEnabled = false;

                    ThreadAddMediaToNewOrExistingAlbum thrd = new ThreadAddMediaToNewOrExistingAlbum(null, PlayingMedia, newAlbnm);
                    thrd.CallBack += (albumId) =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (UCMediaViewAddToAlbumPopup.Instance != null)
                            {
                                UCMediaViewAddToAlbumPopup.Instance.EnableButtons();
                            }
                            if (albumId != Guid.Empty)
                            {
                                EnableButtons(albumId);
                            }
                        });
                    };
                    thrd.StartThread();
                }
                if (IsDownload)
                {
                    MakeDownloaderStart();
                }
                HidePopup();
            }
        }

        ICommand _AddToAlbumCreateOrCancelCommand;
        public ICommand AddToAlbumCreateOrCancelCommand
        {
            get
            {
                if (_AddToAlbumCreateOrCancelCommand == null)
                {
                    _AddToAlbumCreateOrCancelCommand = new RelayCommand(param => OnAddToAlbumCreateOrCancelCommand(param));
                }
                return _AddToAlbumCreateOrCancelCommand;
            }
        }
        private void OnAddToAlbumCreateOrCancelCommand(Object parameter)
        {
            if (parameter is string)
            {
                bool IsCreate = Convert.ToBoolean(parameter);
                if (IsCreate)
                {
                    createAlbumGridPanel.Visibility = Visibility.Visible;
                    craeteAlbumDock.Visibility = Visibility.Collapsed;
                    this.NewAlbumTxtBox.Focusable = true;
                    this.NewAlbumTxtBox.Focus();
                }
                else
                {
                    NewAlbumTxtBox.Text = "";
                    createAlbumGridPanel.Visibility = Visibility.Collapsed;
                    craeteAlbumDock.Visibility = Visibility.Visible;
                    IsOpenCreateNewAlbum = false;
                    OnLoadedControl();
                }
            }
        }

        ICommand _SearchAlbumTextBoxTextChangedCommand;
        public ICommand SearchAlbumTextBoxTextChangedCommand
        {
            get
            {
                if (_SearchAlbumTextBoxTextChangedCommand == null)
                {
                    _SearchAlbumTextBoxTextChangedCommand = new RelayCommand(param => OnSearchAlbumTextBoxTextChangedCommand());
                }
                return _SearchAlbumTextBoxTextChangedCommand;
            }
        }
        private void OnSearchAlbumTextBoxTextChangedCommand()
        {
            try
            {
                string searchText = SearchAlbumTextBox.Text.ToLower();
                if (this.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                {
                    foreach (MediaContentModel model in RingIDViewModel.Instance.MyAudioAlbums)
                    {
                        string AlbumName = model.AlbumName.ToLower();
                        if (AlbumName.Contains(searchText) || model.AlbumName.ToString().Contains(searchText))
                        {
                            model.PanelVisibility = Visibility.Visible;
                        }
                        else
                        {
                            model.PanelVisibility = Visibility.Collapsed;
                        }
                    }
                }
                else if (this.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                {
                    foreach (MediaContentModel model in RingIDViewModel.Instance.MyVideoAlbums)
                    {
                        string AlbumName = model.AlbumName.ToLower();
                        if (AlbumName.Contains(searchText) || model.AlbumName.ToString().Contains(searchText))
                        {
                            model.PanelVisibility = Visibility.Visible;
                        }
                        else
                        {
                            model.PanelVisibility = Visibility.Collapsed;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion "ICommand"
    }
}


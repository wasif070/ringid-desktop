﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAlbumContentView.xaml
    /// </summary>
    public partial class UCAlbumContentView : PopUpBaseControl
    {
        public static UCAlbumContentView Instance = null;
        #region "Constructor"
        public UCAlbumContentView(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 0.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
        #endregion

        #region "Icommands"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        public void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        ICommand _blackSpaceClick;
        public ICommand BlackSpaceClick
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }
        private void OnClickBlackSpace(object param)
        {
            Hide();
            HideMediaAlbumContent();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {

        }

        #endregion
        public void HideMediaAlbumContent()
        {
            Scroll.PreviewKeyDown -= Scroll_PreviewKeyDown;            
            Visibility = Visibility.Hidden;
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        public void ShowMediaAlbumContent(MediaContentModel mediaContentModel)
        {
            if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
            else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
            containerBorder.Child = UCMediaContentsView.Instance;

            UCMediaContentsView.Instance.ShowAlbumContents(mediaContentModel, () =>
            {
                HideMediaAlbumContent();
                return 0;
            });
            Scroll.PreviewKeyDown += Scroll_PreviewKeyDown; 
            //UCMediaContentsDownloadView.Instance.scroll.ScrollToTop();
            this.Visibility = Visibility.Visible;
        }
    }
}

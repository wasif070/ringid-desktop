﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using View.BindingModels;
using View.Constants;
using View.UI.Feed;
using View.Utility;
using View.ViewModel.NewStatus;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCLocationPopUp.xaml
    /// </summary>
    public partial class UCLocationPopUp : PopUpBaseControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCLocationPopUp).Name);
        private const int LOCATION_POPUP_HEIGHT = 340;

        public UCLocationPopUp(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            //InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void location_details_from_address(LocationModel model)
        {
            try
            {
                string latLong = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + model.LocationName;
                var request = WebRequest.Create(new Uri(latLong));
                var response = request.GetResponse();
                var xdoc = XDocument.Load(response.GetResponseStream());
                var result = xdoc.Element("GeocodeResponse").Element("result");
                if (result == null) return;
                var locationElement = result.Element("geometry").Element("location");
                if (locationElement == null) return;
                var _Latitude = locationElement.Element("lat");
                if (_Latitude.Value != null) model.Latitude = Convert.ToDouble(_Latitude.Value);
                var _Longitude = locationElement.Element("lng");
                if (_Longitude.Value != null) model.Longitude = Convert.ToDouble(_Longitude.Value);
                if (_Latitude.Value != null && _Longitude.Value != null)
                {
                    string mapURL = "http://maps.googleapis.com/maps/api/staticmap?"
                        + "center=" + _Latitude.Value + "," + _Longitude.Value
                        + "&key=" + SocialMediaConstants.GOOGLE_MAP_API_KEY
                        + "&size=600x170"
                        + "&markers=size:mid%7Ccolor:red%7C" + _Latitude.Value + "," + _Longitude.Value
                        + "&zoom=15"
                        + "&maptype=roadmap"
                        + "&sensor=false";
                    model.ImageUrl = mapURL;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private BackgroundWorker ViewStaticMapWorker;
        private string CurrentMapUrl = null, CurrentLat_Long = null;

        public void ViewStaticMap()
        {
            if (ViewStaticMapWorker == null)
            {
                ViewStaticMapWorker = new BackgroundWorker();
                ViewStaticMapWorker.DoWork += new DoWorkEventHandler(ViewStaticMap_DoWork);
                ViewStaticMapWorker.ProgressChanged += new ProgressChangedEventHandler(ViewStaticMap_ProgressChanged);
                ViewStaticMapWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ViewStaticMap_RunWorkerCompleted);
            }
            ViewStaticMapWorker.WorkerReportsProgress = true;
            ViewStaticMapWorker.WorkerSupportsCancellation = true;

            if (ViewStaticMapWorker.IsBusy)
            {
                ViewStaticMapWorker.CancelAsync();
                BmpImg = null;
            }
            else
            {
                ViewStaticMapWorker.RunWorkerAsync();
            }
        }

        private void ViewStaticMap_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                byte[] imageBytes = null;
                if (!File.Exists(CurrentLat_Long))
                {
                    var webClient = new WebClient();
                    imageBytes = webClient.DownloadData(new Uri(CurrentMapUrl));
                    //File.WriteAllBytes(@CurrentLat_Long, imageBytes); //hover images save not now
                }
                else
                {
                    imageBytes = File.ReadAllBytes(CurrentLat_Long);
                }
                e.Result = imageBytes;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message + "\n" + ex.StackTrace);
                e.Result = null;
            }
        }
        private void ViewStaticMap_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }
        private void ViewStaticMap_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null && e.Result is byte[])
            {
                byte[] imageBytes = (byte[])e.Result;
                BmpImg = ImageUtility.ConvertByteArrayToBitmapImage(imageBytes);
                imageBytes = null;
            }
            else if (e.Cancelled)
            {
                ViewStaticMapWorker.RunWorkerAsync();
            }
        }


        #region "Property"

        //public UCLocationPopUpWrapper LocationPopUpWrapper
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.LocationPopUpWrapper;
        //    }
        //}

        private ObservableCollection<LocationModel> _LocationSuggestions = new ObservableCollection<LocationModel>();
        public ObservableCollection<LocationModel> LocationSuggestions
        {
            get
            {
                return _LocationSuggestions;
            }
            set
            {
                _LocationSuggestions = value;
                this.OnPropertyChanged("LocationSuggestions");
            }
        }
        private BitmapImage _BmpImg;
        public BitmapImage BmpImg
        {
            get { return _BmpImg; }
            set
            {
                if (value == _BmpImg)
                    return;
                _BmpImg = value;
                this.OnPropertyChanged("BmpImg");
            }
        }

        private LocationModel _SelectedLocationModel = null;
        public LocationModel SelectedLocationModel
        {
            get { return _SelectedLocationModel; }
            set
            {
                if (value == _SelectedLocationModel)
                    return;
                _SelectedLocationModel = value;
                this.OnPropertyChanged("SelectedLocationModel");
            }
        }
        private string _SelectedLocationTxt = null;
        public string SelectedLocationTxt
        {
            get { return _SelectedLocationTxt; }
            set
            {
                if (value == _SelectedLocationTxt)
                    return;
                _SelectedLocationTxt = value;
                this.OnPropertyChanged("SelectedTxt");
            }
        }

        private bool _gridRow;
        public bool GridRow
        {
            get { return _gridRow; }
            set
            {
                if (value == _gridRow)
                    return;
                _gridRow = value;
                this.OnPropertyChanged("GridRow");
            }
        }
        #endregion "Property"

        private NewStatusViewModel newStatusViewModel;
        private UCShareSingleFeedView ucShareSingleFeedView;
        private UCAddLocationView ucAddLocationView;

        public void ShowLocationPopup(UIElement target, Object obj)
        {
            int buttonPosition = (int)target.PointToScreen(new System.Windows.Point(0, 0)).Y;
            int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;

            if ((buttonPosition < LOCATION_POPUP_HEIGHT && bottomSpace >= LOCATION_POPUP_HEIGHT) || bottomSpace >= LOCATION_POPUP_HEIGHT)
            {
                GridRow = true;
                popupLocation.VerticalOffset = 7;
                popupLocation.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            }
            else
            {
                GridRow = false;
                popupLocation.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
                popupLocation.VerticalOffset = -7;
                //popupLocation.VerticalOffset = -(LOCATION_POPUP_HEIGHT - bottomSpace);
            }

            popupLocation.PlacementTarget = target;
            popupLocation.IsOpen = true;

            if (obj is NewStatusViewModel)
            {
                locationMainBorder.Width = 630;
                this.newStatusViewModel = (NewStatusViewModel)obj;
                this.ucShareSingleFeedView = null;
                this.ucAddLocationView = null;
            }
            else if (obj is UCShareSingleFeedView)
            {
                locationMainBorder.Width = 633;
                this.newStatusViewModel = null;
                this.ucShareSingleFeedView = (UCShareSingleFeedView)obj;
                this.ucAddLocationView = null;
            }
            else if (obj is UCAddLocationView)
            {
                locationMainBorder.Width = 632;
                this.newStatusViewModel = null;
                this.ucShareSingleFeedView = null;
                this.ucAddLocationView = (UCAddLocationView)obj;
            }
        }
        public bool LoadNewLocationFromMap(string address)
        {
            var requestUri = string.Format("https://maps.googleapis.com/maps/api/place/autocomplete/xml?input={0}&key={1}", Uri.EscapeDataString(address), Uri.EscapeDataString(SocialMediaConstants.GOOGLE_MAP_API_KEY));
            var request = WebRequest.Create(requestUri);
            request.Timeout = 3000;
            var response = request.GetResponse();
            if (response == null) return false;
            var xdoc = XDocument.Load(response.GetResponseStream());
            IEnumerable<XElement> results = xdoc.Descendants("prediction");
            LocationSuggestions.Clear();
            if (results != null)
            {
                List<LocationDTO> locationListByAddress = new List<LocationDTO>();
                foreach (XElement xe in results)
                {
                    string locationName = xe.Element("description").Value;
                    string locationId = xe.Element("place_id").Value;
                    System.Diagnostics.Debug.WriteLine(locationName + "  " + locationId);
                    LocationModel model = new LocationModel { LocationName = locationName, LocationId = locationId, Type = 0 };
                    LocationSuggestions.Add(model);
                    locationListByAddress.Add(model.GetLocationDTOFromModel());
                }
                if (locationListByAddress.Count > 0)
                {
                    NewsFeedDictionaries.Instance.MAP_SEARCH_HISTORY.Add(address, locationListByAddress);
                    return true;
                }
            }
            return false;
        }
        public void LoadLocationsFromHistory(string addressInMap)
        {
            List<LocationDTO> locationListByAddress = NewsFeedDictionaries.Instance.MAP_SEARCH_HISTORY[addressInMap];
            LocationSuggestions.Clear();
            foreach (var item in locationListByAddress)
            {
                LocationModel model = new LocationModel();
                model.LoadData(item);
                LocationSuggestions.Add(model);
            }
        }
        private void Selector_MouseEnter(object sender, MouseEventArgs e)
        {
            Border lb = (Border)sender;
            LocationModel model = (LocationModel)lb.DataContext;
            if (string.IsNullOrEmpty(model.ImageUrl))
                location_details_from_address(model);
            if (!string.IsNullOrEmpty(model.ImageUrl))
            {
                CurrentMapUrl = model.ImageUrl;
                CurrentLat_Long = RingIDSettings.TEMP_MAP_FOLDER + System.IO.Path.DirectorySeparatorChar + Math.Round(model.Latitude, 3) + "_" + Math.Round(model.Longitude, 3) + ".png";
                ViewStaticMap();
            }
        }

        private void lctn_MouseSelected(object sender, MouseButtonEventArgs e)
        {
            Border sp = (Border)sender;
            if (sp.DataContext is LocationModel)
            {
                LocationModel model = (LocationModel)sp.DataContext;
                SelectedLocationModel = model;
                SelectedLocationTxt = model.LocationName;
                if (newStatusViewModel != null)
                {
                    newStatusViewModel.LocationTextBoxText = SelectedLocationTxt;
                    newStatusViewModel.SelectedLocationModel = SelectedLocationModel;
                    newStatusViewModel.IsLocationSet = true;
                }
                else if (ucShareSingleFeedView != null)
                {
                    ucShareSingleFeedView.lctnTextBox.Text = SelectedLocationTxt;
                    ucShareSingleFeedView.SelectedLocationModel = SelectedLocationModel;
                }
                else if (ucAddLocationView != null)
                {
                    ucAddLocationView.TextBox.Text = SelectedLocationTxt;
                    ucAddLocationView.TextBox.IsReadOnly = true;
                    ucAddLocationView.TextLocationCrossBtn.Visibility = Visibility.Visible;
                }
            }
            popupLocation.IsOpen = false;
        }

        #region ICommands
        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupLocation.IsOpen = false;
        }

        private ICommand popupLocationClosed;
        public ICommand PopupLocationClosed
        {
            get
            {
                if (popupLocationClosed == null) popupLocationClosed = new RelayCommand(param => OnPopupLocationClosed());
                return popupLocationClosed;
            }
        }
        private void OnPopupLocationClosed()
        {
            Hide();
        }

        #endregion

    }
}

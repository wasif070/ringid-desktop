﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCCelebrityDiscoverCategoryPopup.xaml
    /// </summary>
    public partial class UCCelebrityDiscoverCategoryPopup : UserControl, INotifyPropertyChanged
    {
        public static UCCelebrityDiscoverCategoryPopup Instance;
        Func<int> _OnClosing = null;

        #region "Constructor"
        public UCCelebrityDiscoverCategoryPopup()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region "Property"
        private ObservableCollection<NewsCategoryModel> _CategoriesofCelebrities = new ObservableCollection<NewsCategoryModel>();
        public ObservableCollection<NewsCategoryModel> CategoriesofCelebrities
        {
            get
            {
                return _CategoriesofCelebrities;
            }
            set
            {
                _CategoriesofCelebrities = value;
                this.OnPropertyChanged("CategoriesofCelebrities");
            }
        }
        private int _HeightPopup = 300;
        public int HeightPopup
        {
            get
            {
                return _HeightPopup;
            }
            set
            {
                if (value == _HeightPopup)
                    return;
                _HeightPopup = value;
                OnPropertyChanged("HeightPopup");
            }
        }

        private Guid _catID;
        public Guid CatID
        {
            get
            {
                return _catID;
            }
            set
            {
                if (value == _catID)
                    return;
                _catID = value;
                OnPropertyChanged("CatID");
            }
        }

        private string _catName = "";
        public string CatName
        {
            get
            {
                return _catName;
            }
            set
            {
                if (value == _catName)
                    return;
                _catName = value;
                OnPropertyChanged("CatName");
            }
        }

        private NewsCategoryModel _NewsCategoryModel;
        public NewsCategoryModel NewsCategoryModel
        {
            get
            {
                return _NewsCategoryModel;
            }
            set
            {
                if (value == _NewsCategoryModel)
                    return;
                _NewsCategoryModel = value;
                OnPropertyChanged("NewsCategoryModel");
            }
        }

        private int _IsReset = 0;  // 1=reset, 2=done
        public int IsReset
        {
            get
            {
                return _IsReset;
            }
            set
            {
                if (value == _IsReset)
                    return;
                _IsReset = value;
                OnPropertyChanged("IsReset");
            }
        }
        #endregion

        #region "Public Method"
        public void Show(UIElement target, Func<int> onClosing)
        {
            if (popupCategory.IsOpen)
                popupCategory.IsOpen = false;
            else
            {
                if (CategoriesofCelebrities.Count == 0)
                {
                    JObject pakToSend = new JObject();
                    (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_CELEBRITY_CATEGORY_LIST, AppConstants.REQUEST_TYPE_REQUEST);
                }
                IsReset = 0;
                _OnClosing = onClosing;
                popupCategory.Closed += popupCategory_Closed;
                CategorySearchTextBox.TextChanged += CategorySearchTextBox_TextChanged;
                popupCategory.PlacementTarget = target;
                popupCategory.IsOpen = true;
                this.Focusable = true;
                this.Focus();
            }
        }
        #endregion

        #region "Event Trigger"
        private void popupCategory_Closed(object sender, EventArgs e)
        {
            if (_OnClosing != null)
            {
                if (IsReset == 1)
                {
                    CatID = Guid.Empty;
                    CatName = "";
                    foreach (var item in CategoriesofCelebrities)
                    {
                        item.TempSelected = true;
                        item.VisibilityInCountrySearch = Visibility.Visible;
                    }
                    CategorySearchTextBox.Text = "";
                }
                else if (IsReset == 0)
                {
                    foreach (var item in CategoriesofCelebrities)
                    {
                        if (item.CategoryId == CatID)
                            item.TempSelected = false;
                        else
                            item.TempSelected = true;
                    }
                }
                popupCategory.Closed -= popupCategory_Closed;
                CategorySearchTextBox.TextChanged -= CategorySearchTextBox_TextChanged;
                _OnClosing();
            }
        }

        private void categoryBtn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.DataContext is NewsCategoryModel)
            {
                NewsCategoryModel model = (NewsCategoryModel)btn.DataContext;
                //CatID = model.CategoryId;
                //CatName = model.CategoryName;
                NewsCategoryModel = model;
                foreach (var item in CategoriesofCelebrities)
                {
                    if (item.CategoryId == model.CategoryId)
                        item.TempSelected = false;
                    else
                        item.TempSelected = true;
                }
            }
        }

        private void CategorySearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = CategorySearchTextBox.Text;
            int countVisible = 0;

            foreach (var item in CategoriesofCelebrities)
            {
                //if (item.CategoryName.ToLower().Contains(SearchText.ToLower())) { item.VisibilityInCountrySearch = Visibility.Visible; countVisible++; }
                if (item.CategoryName.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0) { item.VisibilityInCountrySearch = Visibility.Visible; countVisible++; }
                else item.VisibilityInCountrySearch = Visibility.Collapsed;
            }
            HeightPopup = (countVisible < 6) ? (countVisible * 30) + 50 + 70 : 300;
        }

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                if (Instance != null && popupCategory.IsOpen)
                    popupCategory.IsOpen = false;
            }
        }
        #endregion

        #region "ICommand"
        ICommand _resetDoneClick;
        public ICommand ResetDoneClick
        {
            get
            {
                if (_resetDoneClick == null)
                {
                    _resetDoneClick = new RelayCommand(param => OnResetDoneCommand(param));
                }
                return _resetDoneClick;
            }
        }
        private void OnResetDoneCommand(object param)
        {
            IsReset = int.Parse(param.ToString());
            if (IsReset == 1)
            {
                CatID = Guid.Empty;
                CatName = "";
            }
            else if (IsReset == 2)
            {
                CatID = NewsCategoryModel.CategoryId;
                CatName = NewsCategoryModel.CategoryName;
            }

            if (popupCategory.IsOpen)
                popupCategory.IsOpen = false;
        }
        #endregion "ICommand"
    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Auth;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCSingleFeedDetailsView.xaml
    /// </summary>
    public partial class UCSingleFeedDetailsView : PopUpBaseControl
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCSingleFeedDetailsView).Name);

        public FeedModel FeedModel;
        public static UCSingleFeedDetailsView Instance;

        #region "Constructor"
        public UCSingleFeedDetailsView(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0, ViewConstants.POP_UP_BORDER_BRUSH);//background Color Transparent Black
            SetParent(motherGrid);
        }
        #endregion

        #region "Public Methods"

        public void LoadFeedModel(FeedModel feedModel)
        {
            //if (FeedModel != null && feedModel != null && FeedModel.NewsfeedId != feedModel.NewsfeedId) this.FeedModel.IsDetilsView = false;
            this.FeedModel = feedModel;
            if (this.FeedModel != null)
            {
                //this.FeedModel.IsDetilsView = true;
                FeedHolderModel holder = new FeedHolderModel(2);
                holder.Feed = FeedModel;
                holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                holder.FeedPanelType = holder.Feed.FeedPanelType;
                //holder.ShortModel = false;
                ObservableCollection<FeedHolderModel> list = new ObservableCollection<FeedHolderModel>();
                list.Add(holder);
                feedContainer.ClearValue(ItemsControl.ItemsSourceProperty);
                Binding itemSourceBinding = new Binding
                {
                    Source = list
                };
                feedContainer.SetBinding(ItemsControl.ItemsSourceProperty, itemSourceBinding);

                if (feedModel.TotalImage > 0 && feedModel.ImageList != null && feedModel.ImageList.Count > 0)
                {
                    int currentImageCount = feedModel.ImageList.Count;
                    if (currentImageCount < feedModel.TotalImage)
                    {
                        SendDataToServer.SendMoreFeedImagesRequest(feedModel.NewsfeedId, currentImageCount - 1, feedModel.TotalImage - currentImageCount);
                    }
                }
            }
            else
            {
                feedContainer.ClearValue(ItemsControl.ItemsSourceProperty);
            }
        }
        public void ShowDetailsView(FeedModel model = null)
        {
            //_MainBorder2.MinHeight = 300;
            notFound.Visibility = Visibility.Collapsed;
            LoadFeedModel(model);
            //if (model == null)
            //{
            //    GIFCtrlPanel.Visibility = Visibility.Visible;
            //    GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
            //}
            ScrlViewer.ScrollToVerticalOffset(0);
            this.IsVisibleChanged += UserControl_IsVisibleChanged;
            this.SizeChanged += UserControl_SizeChanged;
            Visibility = Visibility.Visible;
        }
        public void HideDetailsView()
        {
            //if (FeedModel != null) this.FeedModel.IsDetilsView = false;
            this.IsVisibleChanged -= UserControl_IsVisibleChanged;
            this.SizeChanged -= UserControl_SizeChanged;

            GIFCtrlPanel.Visibility = Visibility.Collapsed;
            if (GIFCtrlLoader.IsRunning()) GIFCtrlLoader.StopAnimate();
            Visibility = Visibility.Hidden;

            if (FeedModel != null) FeedModel.ShowNextCommentsVisible = 0;
        }

        public void GrabKeyboardFocus()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }
        #endregion

        #region "Private Method"
        private void SetButtonExpandVisibility()
        {
            //try
            //{
            //    //if (MainSwitcher.MainController().MainUIController().MainWindow.WindowState == WindowState.Maximized)
            //    //{
            //    //    this.BtnExpand.Visibility = Visibility.Hidden;
            //    //}
            //    //else if (MainSwitcher.MainController().MainUIController().MainWindow.WindowState == WindowState.Normal)
            //    //{
            //    //    this.BtnExpand.Visibility = Visibility.Visible;
            //    //}
            //}
            //catch (Exception ex)
            //{

            //    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            //}
            if (WNRingIDMain.WinRingIDMain != null) WNRingIDMain.WinRingIDMain.SetButtonExpandVisibility(this.BtnExpand);
        }
        #endregion

        #region EventTrigger
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue == false)
                {
                    HideDetailsView();

                }
                SetButtonExpandVisibility();
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetButtonExpandVisibility();
        }
        #endregion

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            GrabKeyboardFocus();
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        private void OnEscapeCommand()
        {
            Hide();
        }

        private ICommand _BtnExpandClickClicked;
        public ICommand BtnExpandClickClicked
        {
            get
            {
                if (_BtnExpandClickClicked == null)
                {
                    _BtnExpandClickClicked = new RelayCommand(param => OnBtnExpandClickClicked(param));
                }
                return _BtnExpandClickClicked;
            }
        }

        public void OnBtnExpandClickClicked(object parameter)
        {
            //MainSwitcher.MainController().MainUIController().MainWindow.WindowState = WindowState.Maximized;
            if (WNRingIDMain.WinRingIDMain != null) WNRingIDMain.WinRingIDMain.MaximizeTheWindow(true);
            GrabKeyboardFocus();
        }

        private ICommand _DetailsDoubleCommand;
        public ICommand DetailsDoubleCommand
        {
            get
            {
                if (_DetailsDoubleCommand == null)
                {
                    _DetailsDoubleCommand = new RelayCommand(param => OnDetailsDoubleClicked(param));
                }
                return _DetailsDoubleCommand;
            }
        }

        public void OnDetailsDoubleClicked(object parameter)
        {
            //if (MainSwitcher.MainController().MainUIController().MainWindow.WindowState == WindowState.Maximized)
            //{
            //    MainSwitcher.MainController().MainUIController().MainWindow.WindowState = WindowState.Normal;
            //}
            //else if (MainSwitcher.MainController().MainUIController().MainWindow.WindowState == WindowState.Normal)
            //{
            //    MainSwitcher.MainController().MainUIController().MainWindow.WindowState = WindowState.Maximized;
            //}

            if (WNRingIDMain.WinRingIDMain != null) WNRingIDMain.WinRingIDMain.MaximizeTheWindow();
            GrabKeyboardFocus();
        }

        private ICommand _NoActionCommand;
        public ICommand NoActionCommand
        {
            get
            {
                if (_NoActionCommand == null) _NoActionCommand = new RelayCommand(param => OnNoActionCommand());
                return _NoActionCommand;
            }
        }
        public void OnNoActionCommand()
        {
            GrabKeyboardFocus();
        }
        #endregion
    }
}

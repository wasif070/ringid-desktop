﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCCelebrityDiscoverCountryPopup.xaml
    /// </summary>
    public partial class UCCelebrityDiscoverCountryPopup : UserControl, INotifyPropertyChanged
    {
        public static UCCelebrityDiscoverCountryPopup Instance;
        Func<int> _OnClosing = null;

        #region "Constructor"
        public UCCelebrityDiscoverCountryPopup()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion 

        #region "Property"
        private CountryCodeModel _SelectedCountryModel;
        public CountryCodeModel SelectedCountryModel
        {
            get
            {
                return _SelectedCountryModel;
            }
            set
            {
                if (value == _SelectedCountryModel)
                    return;
                _SelectedCountryModel = value;
                OnPropertyChanged("SelectedCountryModel");
            }
        }
        private int _HeightPopup = 300;  
        public int HeightPopup
        {
            get
            {
                return _HeightPopup;
            }
            set
            {
                if (value == _HeightPopup)
                    return;
                _HeightPopup = value;
                OnPropertyChanged("HeightPopup");
            }
        }

        private int _IsReset = 0;  // 1=reset, 2=done
        public int IsReset
        {
            get
            {
                return _IsReset;
            }
            set
            {
                if (value == _IsReset)
                    return;
                _IsReset = value;
                OnPropertyChanged("IsReset");
            }
        }

        private string _cntryName = "";  
        public string CntryName
        {
            get
            {
                return _cntryName;
            }
            set
            {
                if (value == _cntryName)
                    return;
                _cntryName = value;
                OnPropertyChanged("CntryName");
            }
        }
        #endregion 

        #region "Public Method"
        public void Show(UIElement target, Func<int> onClosing)
        {
            if (popupCountry.IsOpen)
                popupCountry.IsOpen = false;
            else
            {
                IsReset = 0;
                if (RingIDViewModel.Instance.CountryModelList == null)
                    RingIDViewModel.Instance.LoadMailMobileCountryCodeList();
                _OnClosing = onClosing;

                popupCountry.Closed += popupCountry_Closed;
                CountrySearchTextBox.TextChanged += CountrySearchTextBox_TextChanged;

                popupCountry.PlacementTarget = target;
                popupCountry.IsOpen = true;
                this.Focusable = true;
                this.Focus();
            }
        }
        #endregion
        
        #region "Event Trigger"

        private void counrtyBtn_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button)
            {
                Button btn = (Button)sender;
                if (btn.DataContext is CountryCodeModel)
                {
                    CountryCodeModel model = (CountryCodeModel)btn.DataContext;
                    SelectedCountryModel = model;

                    foreach (var item in RingIDViewModel.Instance.CountryModelList)
                    {
                        if (item.CountryCode == model.CountryCode)
                            item.TempSelected = true;
                        else
                            item.TempSelected = false;
                    }
                }
            }
        }

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                if (Instance != null && popupCountry.IsOpen)
                    popupCountry.IsOpen = false;
            }
        }

        private void CountrySearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchText = CountrySearchTextBox.Text;
            int countVisible = 0;

            foreach (var item in RingIDViewModel.Instance.CountryModelList)
            {
                if (item.CountryName.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0) { item.VisibilityInCountrySearch = Visibility.Visible; countVisible++; }
                else item.VisibilityInCountrySearch = Visibility.Collapsed;
            }
            HeightPopup = (countVisible < 6) ? (countVisible * 30) + 50 + 70 : 300;
        }

        private void popupCountry_Closed(object sender, EventArgs e)
        {
            if (_OnClosing != null)
            {
                if (IsReset == 1)
                {
                    CntryName = "";
                    foreach (var item in RingIDViewModel.Instance.CountryModelList)
                    {
                        item.TempSelected = false;
                        item.VisibilityInCountrySearch = Visibility.Visible;
                    }
                    CountrySearchTextBox.Text = "";
                }
                else if (IsReset == 0)
                {
                    foreach (var item in RingIDViewModel.Instance.CountryModelList)
                    {
                        if (item.CountryName == CntryName)
                            item.TempSelected = true;
                        else
                            item.TempSelected = false;
                    }
                }
                popupCountry.Closed -= popupCountry_Closed;
                CountrySearchTextBox.TextChanged -= CountrySearchTextBox_TextChanged;
                _OnClosing();
            }
        }
        #endregion 

        #region "ICommand"
        ICommand _resetDoneClick;
        public ICommand ResetDoneClick
        {
            get
            {
                if (_resetDoneClick == null)
                {
                    _resetDoneClick = new RelayCommand(param => OnResetDoneCommand(param));
                }
                return _resetDoneClick;
            }
        }
        private void OnResetDoneCommand(object param)
        {
            IsReset = int.Parse(param.ToString());
            if (IsReset == 1)
            {
                CntryName = "";
                HeightPopup = 300;
            }
            else if (IsReset == 2)
            {
                CntryName = SelectedCountryModel.CountryName;
            }

            if (popupCountry.IsOpen)
                popupCountry.IsOpen = false;
        }
        #endregion "ICommand"
    }
}

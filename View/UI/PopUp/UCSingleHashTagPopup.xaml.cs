﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCSingleHashTagPopup.xaml
    /// </summary>
    public partial class UCSingleHashTagPopup : UserControl, INotifyPropertyChanged
    {
        public static UCSingleHashTagPopup Instance;

        public UCSingleHashTagPopup()
        {
            Instance = this;
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private ObservableCollection<HashTagModel> _SingleHashTag = new ObservableCollection<HashTagModel>();
        public ObservableCollection<HashTagModel> SingleHashTag
        {
            get { return _SingleHashTag; }
            set
            {
                _SingleHashTag = value;
                this.OnPropertyChanged("SingleHashTag");
            }
        }

        public void Show(UIElement target)
        {
            popupHashTag.PlacementTarget = target;
            popupHashTag.IsOpen = true;
        }

        public UCHashTagPopUp HashTagPopUp
        {
            get
            {
                return MainSwitcher.PopupController.HashTagPopUp;
            }
        }

        private void AddHashTagTxtBx_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if (HashTagPopUp == null)
            //{
            //    HashTagPopUp = new UCHashTagPopUp();
            //}
            if (HashTagPopUp.Parent != null && HashTagPopUp.Parent is Grid)
            {
                Grid g = (Grid)HashTagPopUp.Parent;
                g.Children.Remove(HashTagPopUp);
            }
            AddHashTagTxtBoxContainerGrid.Children.Add(HashTagPopUp);
            HashTagPopUp.Show(this, AddHashTagTxtBx);
        }

        private void removeHashTag_Click(object sender, RoutedEventArgs e)
        {

        }

        private void okBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

﻿using System.Windows.Controls;
using View.Constants;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAuthenticationLoaderPopup.xaml
    /// </summary>
    public partial class UCAuthenticationLoaderPopup : UserControl
    {
        public static UCAuthenticationLoaderPopup Instance;
        public UCAuthenticationLoaderPopup()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }
        public void Hide()
        {
            if (authLoaderPopup.IsOpen)
            {
                authLoaderPopup.IsOpen = false;
                if (GIFCtrl != null && GIFCtrl.IsRunning())
                    this.GIFCtrl.StopAnimate();
            }
        }

        public void Show()
        {

            //popupCountry.PlacementTarget = target;
            //popupCountry.VerticalOffset = -30;
            if (!authLoaderPopup.IsOpen)
            {
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_100));
                authLoaderPopup.IsOpen = true;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCTimerInfomessage.xaml
    /// </summary>
    public partial class UCTimerInfomessage : UserControl, INotifyPropertyChanged
    {

        #region "Private Fields"
        private Grid _parentGrid;
        #endregion "Private Fields"

        #region "Constructors"
        public UCTimerInfomessage(Grid parentGrid)
        {
            InitializeComponent();
            setParent(parentGrid);
            this.DataContext = this;
        }
        #endregion "Constructors"

        #region

        private ICommand exitThisPopup;
        public ICommand ExitThisPopup
        {
            get
            {
                if (exitThisPopup == null)
                {
                    exitThisPopup = new RelayCommand(param => OnExit(param));
                }
                return exitThisPopup;
            }

        }
        #endregion

        #region "Utility Methods"


        private void setParent(Grid parentGrid)
        {
            this._parentGrid = parentGrid;
        }

        public void Show()
        {
            if (_parentGrid != null)
            {
                _parentGrid.IsEnabled = false;
            }
        }

        public void Hide()
        {
            if (_parentGrid != null)
            {
                _parentGrid.IsEnabled = true;
            }
        }


        public void OnExit(object param)
        {
            Hide();
        }

        #endregion "Utility methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        public virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                this.PropertyChanged(this, e);
            }
        }
        #endregion
    }
    //private class VMTimerInfomessage : ObservableObject
    //{
    //    #region "Private Fields"
    //    #endregion "Private Fields"

    //    #region "Constructors"
    //    public VMTimerInfomessage()
    //    {
    //    }
    //    #endregion "Constructors"

    //    #region "Data Properties"

    //    #endregion "Data Properties"

    //    #region "Utility Methods"

    //    public void OnExit(object param)
    //    {
    //    }

    //    #endregion "Utility methods"
    //}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.UI.Feed;
using View.Utility;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCFeedOptionsPopup.xaml
    /// </summary>
    public partial class UCFeedOptionsPopup : PopUpBaseControl, INotifyPropertyChanged
    {
        public UCFeedOptionsPopup()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private int _OptionVal;//1=feed 1, 2=feed 2, 3=feed 3
        public int OptionVal
        {
            get { return _OptionVal; }
            set
            {
                _OptionVal = value;
                this.OnPropertyChanged("OptionVal");
            }
        }

        private NewStatusViewModel newStatusViewModel = new NewStatusViewModel();

        public void ShowPopup(UIElement target,NewStatusViewModel newStatusViewModel)
        {
            this.newStatusViewModel = newStatusViewModel;
            if (popupOptions.IsOpen)
            {
                popupOptions.IsOpen = false;
            }
            else
            {
                if (newStatusViewModel.OptionBool == true && RingIDViewModel.Instance.NumberOfFeedColumn == 2)
                {
                    pathStyle.Margin = new Thickness(0, 0, 9, 0);
                }
                else if (newStatusViewModel.OptionBool == false && RingIDViewModel.Instance.NumberOfFeedColumn == 2)
                {
                    pathStyle.Margin = new Thickness(0, 0, 11, 0);
                }
                else if (newStatusViewModel.OptionBool == false && RingIDViewModel.Instance.NumberOfFeedColumn == 3)
                {
                    pathStyle.Margin = new Thickness(0, 0, 13, 0);
                }
                popupOptions.PlacementTarget = target;
                popupOptions.IsOpen = true;
            }
        }
        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupOptions.IsOpen = false;
        }

        private ICommand popupClosedCommand;
        public ICommand PopupClosedCommand
        {
            get
            {
                if (popupClosedCommand == null) popupClosedCommand = new RelayCommand(param => OnPopupClosedCommand());
                return popupClosedCommand;
            }
        }
        private void OnPopupClosedCommand()
        {
            Hide();
        }
        #endregion
        private void feed1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            RingIDViewModel.Instance.MaxFeedColumn = 1;
            this.OptionVal = RingIDViewModel.Instance.NumberOfFeedColumn;
            this.newStatusViewModel.OptionBool = true;
        }

        private void feed2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            RingIDViewModel.Instance.MaxFeedColumn = 2;
            this.newStatusViewModel.OptionBool = false;
        }

        private void feed3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupOptions.IsOpen = false;
            RingIDViewModel.Instance.MaxFeedColumn = 3;
            this.newStatusViewModel.OptionBool = false;
        }
    }
}

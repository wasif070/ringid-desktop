﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Data;
//using System.Windows.Documents;
//using System.Windows.Input;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
//using System.Windows.Navigation;
//using System.Windows.Shapes;

//namespace View.UI.PopUp
//{
//    /// <summary>
//    /// Interaction logic for UCHDPopup.xaml
//    /// </summary>
//    public partial class UCHDPopup : UserControl
//    {
//        public UCHDPopup()
//        {
//            InitializeComponent();
//        }
//    }
//}

using System.Windows;
using System.Windows.Controls;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCHDPopup.xaml
    /// </summary>
    public partial class UCHDPopup : UserControl
    {
        public UCHDPopup()
        {
            InitializeComponent();
            //DataContext = View.Utility.RingPlayer.RingPlayerViewModel.Instance;
        }

        public void Show(UIElement target)
        {
            //if (popupHdSelection)
            //{

            //}
            if (popupHD.IsOpen)
            {
                popupHD.IsOpen = false;
            }
            else
            {
                popupHD.PlacementTarget = target;
                popupHD.IsOpen = true;
            }
            popupHD.Placement = System.Windows.Controls.Primitives.PlacementMode.RelativePoint;
            //ShowUpperOrLowerPopUp(target);
        }
        public void ClosePopup()
        {
            popupHD.IsOpen = false;
        }
    }
}

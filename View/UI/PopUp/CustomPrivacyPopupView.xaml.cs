﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;
using View.ViewModel.PopUp;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCFriendPrivacyPopup.xaml
    /// </summary>
    public partial class CustomPrivacyPopupView : PopUpBaseControl
    {
        public CustomPrivacyPopupView(Grid motherGrid)
        {
            CustomPrivacyPopupViewModel friendPrivacyPopupViewModel = new CustomPrivacyPopupViewModel();
            DataContext = friendPrivacyPopupViewModel;
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
    }
}

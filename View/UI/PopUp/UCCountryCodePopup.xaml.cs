﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Utility;


namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCCountryCodePopup.xaml
    /// </summary>
    public partial class UCCountryCodePopup : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCCountryCodePopup).Name);
        public event DelegateNoParam OnHide;
        public delegate void CountryModelChanged(CountryCodeModel selectedModel);
        public event CountryModelChanged OnCountryModelChanged;
        #endregion

        #region"Ctors"
        public UCCountryCodePopup()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion"Ctors"

        #region "Properties"
        private ObservableCollection<CountryCodeModel> countryModelList = new ObservableCollection<CountryCodeModel>();
        public ObservableCollection<CountryCodeModel> CountryModelList
        {
            get { return countryModelList; }
            set { countryModelList = value; this.OnPropertyChanged("CountryModelList"); }
        }

        private CountryCodeModel selectedCountryModel;
        public CountryCodeModel SelectedCountryModel
        {
            get { return selectedCountryModel; }
            set
            {
                if (value == selectedCountryModel) return;
                selectedCountryModel = value;
                //SearchText = selectedCountryModel.CountryName;
                if (OnCountryModelChanged != null) OnCountryModelChanged(selectedCountryModel);
                OnPropertyChanged("SelectedCountryModel");
            }
        }
        private int heightPopup = 200;  // 5*20=100
        public int HeightPopup
        {
            get { return heightPopup; }
            set
            {
                if (value == heightPopup) return;
                heightPopup = value; OnPropertyChanged("HeightPopup");
            }
        }

        private string searchText = string.Empty;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                if (value == searchText) return;
                searchText = value;
                filterCountryList(searchText);
                OnPropertyChanged("SearchText");
            }
        }

        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            getFocusOnUserControl();
        }

        private ICommand backButtonCommand;
        public ICommand BackButtonCommand
        {
            get
            {
                if (backButtonCommand == null) backButtonCommand = new RelayCommand(param => OnBackButton(param));
                return backButtonCommand;
            }
        }
        private void OnBackButton(object param)
        {
            DisposeThis();
        }

        private ICommand countryButtonCommand;
        public ICommand CountryButtonCommand
        {
            get
            {
                if (countryButtonCommand == null) countryButtonCommand = new RelayCommand(param => OnCountryButtonCommand(param));
                return countryButtonCommand;
            }
        }
        private void OnCountryButtonCommand(object param)
        {
            if (param is Button)
            {
                Button btn = (Button)param;
                if (btn.DataContext is CountryCodeModel)
                {
                    CountryCodeModel model = (CountryCodeModel)btn.DataContext;
                    SelectedCountryModel = model;
                    DisposeThis();
                }
            }
        }

        private ICommand onTempFlagImageCommand;
        public ICommand TempFlagImageCommand
        {
            get
            {
                if (onTempFlagImageCommand == null) onTempFlagImageCommand = new RelayCommand(param => OnTempFlagImageCommand(param));
                return onTempFlagImageCommand;
            }
        }
        private void OnTempFlagImageCommand(object param)
        {
            OnBackButton(null);
        }

        #endregion"ICommands and Command Methods"

        #region "Utility Methods"

        public void ShowThis(UIElement target)
        {
            if (CountryModelList.Count == 0) loadMailMobileCountryCodeList();
            _PopupCountry.PlacementTarget = target;
            _PopupCountry.IsOpen = true;
        }

        public void DisposeThis()
        {
            if (OnHide != null) OnHide();
            SearchText = string.Empty;
            if (_PopupCountry.IsOpen) _PopupCountry.IsOpen = false;
        }

        private void getFocusOnUserControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            this.FocusVisualStyle = null;
        }

        private void loadMailMobileCountryCodeList()
        {
            for (int i = 2; i < DefaultSettings.COUNTRY_MOBILE_CODE.Length / 2; i++)
            {
                string name = DefaultSettings.COUNTRY_MOBILE_CODE[i, 0];
                string code = DefaultSettings.COUNTRY_MOBILE_CODE[i, 1];
                CountryModelList.Add(new CountryCodeModel(name, code));
            }
        }

        private void filterCountryList(string searchText1)
        {
            int countVisible = 0;
            foreach (var item in CountryModelList)
            {
                if (item.CountryName.IndexOf(searchText1, StringComparison.OrdinalIgnoreCase) >= 0) { item.VisibilityInCountrySearch = Visibility.Visible; countVisible++; }
                else item.VisibilityInCountrySearch = Visibility.Collapsed;
            }
            HeightPopup = (countVisible < 5) ? (countVisible * 25) + 40 : 200;
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
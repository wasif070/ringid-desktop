﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCFollowingUnfollowingView.xaml
    /// </summary>
    public partial class UCFollowingUnfollowingView : PopUpBaseControl, INotifyPropertyChanged
    {
        public BaseUserProfileModel UserModel;
        public List<Guid> SelectedUtids = new List<Guid>();
        public List<Guid> UnselectedUtids = new List<Guid>();
        public Dictionary<Guid, bool> StartStatesUtids = new Dictionary<Guid, bool>();
        public static UCFollowingUnfollowingView Instance;

        public UCFollowingUnfollowingView(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
            Visibility = Visibility.Hidden;
        }

        #region "Property Changed"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private bool _IsEdit;
        public bool IsEdit
        {
            get { return _IsEdit; }
            set
            {
                if (value == _IsEdit)
                    return;

                _IsEdit = value;
                this.OnPropertyChanged("IsEdit");
            }
        }
        private ObservableCollection<NewsCategoryModel> _CategoriesofthePortal = new ObservableCollection<NewsCategoryModel>();
        public ObservableCollection<NewsCategoryModel> CategoriesofthePortal
        {
            get
            {
                return _CategoriesofthePortal;
            }
            set
            {
                _CategoriesofthePortal = value;
                this.OnPropertyChanged("CategoriesofthePortal");
            }
        }
        #endregion "Properties"

        #region "public Method"
        public void ShowFollowUnFollow(BaseUserProfileModel model, bool IsEdit)
        {
            this.IsEdit = IsEdit;
            this.UserModel = model;
            this.newsCategortyTtl.Text = this.UserModel.FullName;
            CategoriesofthePortal.Clear();
            SelectedUtids.Clear();
            UnselectedUtids.Clear();
            StartStatesUtids.Clear();
            SendDataToServer.ListNewsPortalInfoCategories(UserModel.UserTableID, UserModel.ProfileType);
            int i = CategoriesofthePortal.Count;
            SetEvent();
        }

        public void SetEvent()
        {
            //SelectDeselect();

            Visibility = Visibility.Visible;
            //<<<<<<< .mine
            //            doneButton.Click -= doneButton_Click;
            //            doneButton.Click += doneButton_Click;
            //            cancelButton.Click -= cancelButton_Click;
            //            cancelButton.Click += cancelButton_Click;
            //=======
            //>>>>>>> .r2337
        }

        public void HideView()
        {
            Visibility = Visibility.Hidden;
        }

        #endregion

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            Hide();
        }

        ICommand _selectDesectClicked;
        public ICommand SelectDeselectClicked
        {
            get
            {
                if (_selectDesectClicked == null)
                {
                    _selectDesectClicked = new RelayCommand(param => OnSelectDeselectClicked(param));
                }
                return _selectDesectClicked;
            }
        }

        private void OnSelectDeselectClicked(object param)
        {
            if (selectCheckBox.IsChecked == true)
            {
                foreach (var item in CategoriesofthePortal)
                {
                    item.TempSelected = true;
                }
            }
            else
            {
                foreach (var item in CategoriesofthePortal)
                {
                    item.TempSelected = false;
                }
            }
        }

        ICommand _DoneButtonClicked;
        public ICommand DoneButtonClicked
        {
            get
            {
                if (_DoneButtonClicked == null)
                {
                    _DoneButtonClicked = new RelayCommand(param => OnDoneButtonClickedCommand(param));
                }
                return _DoneButtonClicked;
            }
        }

        private void OnDoneButtonClickedCommand(object param)
        {
            int totalSel = 0;
            foreach (var item in CategoriesofthePortal)
            {
                if (StartStatesUtids.ContainsKey(item.CategoryId))
                {
                    if (StartStatesUtids[item.CategoryId])
                    {
                        if (!item.TempSelected && !UnselectedUtids.Contains(item.CategoryId)) UnselectedUtids.Add(item.CategoryId);
                    }
                    else
                    {
                        if (item.TempSelected && !SelectedUtids.Contains(item.CategoryId)) SelectedUtids.Add(item.CategoryId);
                    }
                }
                if (item.TempSelected) totalSel++;
            }
            if (totalSel == 0) UIHelperMethods.ShowWarning("Please select one or more categories to follow", "Select category");
            else if (totalSel == CategoriesofthePortal.Count)
            {

                Hide();
                if (!IsEdit)   //new subscription with all categories, default
                    SubscribeOrUnsubscribe(UserModel.ProfileType, UserModel.UserTableID, 2, null, null);
                else                   //select all categories of a subscribed portal  
                    if (SelectedUtids.Count != 0 || UnselectedUtids.Count != 0) SubscribeOrUnsubscribe(UserModel.ProfileType, UserModel.UserTableID, 0, SelectedUtids, UnselectedUtids);
            }
            else
            {
                Hide();
                if (!IsEdit) //new subscription with selected category
                    SubscribeOrUnsubscribe(UserModel.ProfileType, UserModel.UserTableID, 0, SelectedUtids, UnselectedUtids, true);
                else                 //edit in caterogy of a subscribed portal    
                    if (SelectedUtids.Count != 0 || UnselectedUtids.Count != 0) SubscribeOrUnsubscribe(UserModel.ProfileType, UserModel.UserTableID, 0, SelectedUtids, UnselectedUtids, false);
            }
        }

        private ICommand cancelCommand;
        public ICommand CancelCommand
        {
            get
            {
                if (cancelCommand == null) cancelCommand = new RelayCommand(param => OnCancelClickedCommand());
                return cancelCommand;
            }
        }
        public void OnCancelClickedCommand()
        {
            Hide();
            //if (IsEdit)
            //{
            //    foreach (var item in CategoriesofthePortal)
            //    {
            //        if (StartStatesUtids.ContainsKey(item.CategoryId))
            //        {
            //            SelectedUtids.Add(item.CategoryId);
            //        }
            //    }
            //    SubscribeOrUnsubscribe(UserModel.ProfileType, UserModel.UserTableID, 0, SelectedUtids, null);
            //}
        }

        private ICommand selectDesectCommand;
        public ICommand SelectDeselectCommand
        {
            get
            {
                if (selectDesectCommand == null) selectDesectCommand = new RelayCommand(param => OnSelectDeselectClickedCommand());
                return selectDesectCommand;
            }
        }
        public void OnSelectDeselectClickedCommand()
        {
            SelectDeselect();
        }
        #endregion "ICommand"

        #region "Event Trigger"

        private void tmpSelected_Checked(object sender, RoutedEventArgs e)
        {
            SelectDeselect();
        }

        private void tmpSelected_Unchecked(object sender, RoutedEventArgs e)
        {
            SelectDeselect();
        }

        #endregion

        #region "Utility Methods"
        /*
         *    private void MyNewsFeedsRequest(long time, short scl, Object obj)
        {
            new ThradMyNewsFeedsRequest().StartThread(time, scl, obj);
        }
        */

        private void SubscribeOrUnsubscribe(int profileType, long utId, int SubUnsubscrbType, List<Guid> subscrbeLst, List<Guid> unsubscrbeLst, bool IsEditInNewSubscription = false)
        {
            new ThradSubscribeOrUnsubscribe(profileType, utId, SubUnsubscrbType, subscrbeLst, unsubscrbeLst, 0, IsEditInNewSubscription).StartThread();
        }

        public void SelectDeselect()
        {
            bool flag = false;
            foreach (var item in CategoriesofthePortal)
            {
                if (item.TempSelected == false)
                    flag = true;
            }
            if (flag == true)
                selectCheckBox.IsChecked = false;
            else selectCheckBox.IsChecked = true;
        }

        #endregion
    }
}

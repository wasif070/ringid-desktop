﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.MediaPlayer;
using View.Utility;
using View.Utility.Auth;
using View.ViewModel;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCDownloadPopup.xaml
    /// </summary>
    public partial class UCDownloadPopup : PopUpBaseControl
    {
        #region "Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(UCDownloadPopup).Name);

        private const int POPUP_HEIGHT = 100;
        public SingleMediaModel PlayingMedia = null;
        public static UCDownloadPopup Instance;
        Grid motherPanel;
        #endregion "Fields"

        #region "Constructors"
        public UCDownloadPopup(Grid motherGrid)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            //InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
            motherPanel = motherGrid;
        }
        #endregion

        #region "Private methods"
        private UCMediaPlayerInMain MediaPlayerInMain
        {
            get
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                if (obj != null && obj is UCMediaPlayerInMain)
                {
                    return (UCMediaPlayerInMain)obj;
                }
                return null;
            }
        }

        private UCSingleFeedDetailsView SingleFeedDetailsView
        {
            get
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                if (obj != null && obj is UCSingleFeedDetailsView)
                {
                    return (UCSingleFeedDetailsView)obj;
                }
                return null;
            }
        }
        #endregion

        #region "public methods"
        Func<int> _OnBackToPrevious;
        public void ShowPopup(SingleMediaModel PlayingMedia, UIElement target, Func<int> _OnBackToPrevious)
        {
            this.PlayingMedia = PlayingMedia;
            this._OnBackToPrevious = _OnBackToPrevious;
            int buttonPosition = (int)target.PointToScreen(new System.Windows.Point(0, 0)).Y;
            int bottomSpace = (int)(SystemParameters.PrimaryScreenHeight - buttonPosition) - 20;

            if ((buttonPosition < POPUP_HEIGHT && bottomSpace >= POPUP_HEIGHT) || bottomSpace >= POPUP_HEIGHT)
            {
                popupMoreOptions.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                //popupMoreOptions.VerticalOffset = -12;
            }
            else
            {
                popupMoreOptions.Placement = System.Windows.Controls.Primitives.PlacementMode.Top;
                //popupMoreOptions.VerticalOffset = 12;
            }
            popupMoreOptions.PlacementTarget = target;
            popupMoreOptions.IsOpen = true;
        }
        #endregion

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            popupMoreOptions.IsOpen = false;
        }

        private ICommand _PopupOptionClosed;
        public ICommand PopupOptionClosed
        {
            get
            {
                if (_PopupOptionClosed == null) _PopupOptionClosed = new RelayCommand(param => OnPopupOptionsClosed());
                return _PopupOptionClosed;
            }
        }
        private void OnPopupOptionsClosed()
        {
            //ClosePopUp();
            Hide();
        }

        private ICommand _AddToAlbumCommand;
        public ICommand AddToAlbumCommand
        {
            get
            {
                if (_AddToAlbumCommand == null) _AddToAlbumCommand = new RelayCommand(param => OnAddToAlbumCommand(param));
                return _AddToAlbumCommand;
            }
        }
        private void OnAddToAlbumCommand(object param)
        {
            if(param is string)
            {
                bool IsCreate = Convert.ToBoolean(param);
                if (PlayingMedia != null && PlayingMedia.ContentId != Guid.Empty)
                {
                    Hide();
                    if (UCDownloadOrAddToAlbumPopUp.Instance != null)
                        motherPanel.Children.Remove(UCDownloadOrAddToAlbumPopUp.Instance);
                    UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(motherPanel);
                    if (IsCreate)
                        UCDownloadOrAddToAlbumPopUp.Instance.IsOpenCreateNewAlbum = true;
                    else UCDownloadOrAddToAlbumPopUp.Instance.IsOpenCreateNewAlbum = false;
                    UCDownloadOrAddToAlbumPopUp.Instance.OnRemovedUserControl += () =>
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance = null;
                        if (_OnBackToPrevious != null) _OnBackToPrevious();
                        if (!(motherPanel == UCGuiRingID.Instance.MotherPanel))
                        {
                            if (MediaPlayerInMain != null) MediaPlayerInMain.GrabKeyBoardFocus();
                            else if (SingleFeedDetailsView != null) SingleFeedDetailsView.GrabKeyboardFocus();
                        }
                    };
                    if (PlayingMedia.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                    {
                        if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                        {
                            UCDownloadOrAddToAlbumPopUp.Instance.Show();
                            UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_AUDIO, true, false, () => { return 0; });
                        }
                        else if (DefaultSettings.IsInternetAvailable)
                        {
                            UCDownloadOrAddToAlbumPopUp.Instance.Show();
                            UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_AUDIO, true, false, () => { return 0; });
                            SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_AUDIO, Guid.Empty);
                        }
                    }
                    else if (PlayingMedia.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                    {
                        if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                        {
                            UCDownloadOrAddToAlbumPopUp.Instance.Show();
                            UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_VIDEO, true, false, () => { return 0; });
                        }
                        else if (DefaultSettings.IsInternetAvailable)
                        {
                            UCDownloadOrAddToAlbumPopUp.Instance.Show();
                            UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(PlayingMedia, SettingsConstants.MEDIA_TYPE_VIDEO, true, false, () => { return 0; });
                            SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_VIDEO, Guid.Empty);
                        }
                    }
                }
            }

        }
        #endregion
    }
}

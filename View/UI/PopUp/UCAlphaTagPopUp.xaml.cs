﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.FriendList;
using View.ViewModel;
using View.ViewModel.PopUp;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAlphaTagPopUp.xaml
    /// </summary>
    public partial class UCAlphaTagPopUp : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCAlphaTagPopUp).Name);

        private CustomPrivacyPopupViewModel friendPrivacyPopupViewModel;

        int ParentType;

        public static UCAlphaTagPopUp Instance;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private ObservableCollection<UserBasicInfoModel> _SearchTagFriends;
        public ObservableCollection<UserBasicInfoModel> SearchTagFriends
        {
            get
            {
                return _SearchTagFriends;
            }
            set
            {
                _SearchTagFriends = value;
                this.OnPropertyChanged("SearchTagFriends");
            }
        }

        public UCAlphaTagPopUp()
        {
            InitializeComponent();
            this.SearchTagFriends = new ObservableCollection<UserBasicInfoModel>();
        }

        RichTextFeedEdit richTextBox;
        public void InitializePopUpLocation(RichTextFeedEdit richTextBox)
        {
            this.richTextBox = richTextBox;
            ParentType = 0;
            popupTag.PlacementRectangle = richTextBox.CaretPosition.GetCharacterRect(LogicalDirection.Forward);
            CloseFriendTagPopUp();
        }

        TextBox addFriendTextBox;
        public void InitializePopUpLocation(TextBox addFriendTextBox, CustomPrivacyPopupViewModel friendPrivacyPopupViewModel)
        {
            this.friendPrivacyPopupViewModel = friendPrivacyPopupViewModel;
            this.addFriendTextBox = addFriendTextBox;
            this.ParentType = 1;
            popupTag.PlacementTarget = this.addFriendTextBox;
            CloseFriendTagPopUp();
        }

        public void ViewSearchFriends(string searchStr)
        {
            searchParm = searchStr;
            processingList = false;
            StartThread();
        }

        # region Search Thread
        private bool runningSearchTagFriendThread = false;
        private string searchParm;
        private bool processingList = false;
        private int waiting = 0;
        private Int32 SLEEP_TIME = 5;//ms
        private string prevString;
        private Thread trd = null;

        private void StartThread()
        {
            if (!runningSearchTagFriendThread)
            {
                runningSearchTagFriendThread = true;
                if (trd == null || !trd.IsAlive)
                {
                    trd = new Thread(Run);
                    trd.Name = "SearchTagFriend";
                    trd.Start();
                }
            }
        }

        private void Run()
        {
            while (runningSearchTagFriendThread)
            {
                try
                {
                    if (!string.IsNullOrEmpty(searchParm))
                    {
                        if (string.IsNullOrEmpty(prevString) || !prevString.Equals(searchParm))
                        {
                            if (searchParm.Trim().Length > 0)
                            {
                                ClearSearchTagFriendList();
                                prevString = searchParm;
                                AddIntoSearchTagFriendListBySearchResult();
                            }
                            else
                            {
                                ClearSearchTagFriendList();
                            }
                        }

                    }
                    else
                    {
                        ClearSearchTagFriendList();
                    }
                    if (waiting != 0 && waiting % 15 == 0)
                    {
                        runningSearchTagFriendThread = false;// stop thread if 5 sec idle
                    }
                    waiting++;
                    Thread.Sleep(300);
                }
                catch (Exception ex)
                {

                    log.Error("SearchTagfreind==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);

                }
            }
        }

        private void AddIntoSearchTagFriendListBySearchResult()
        {
            if (!processingList)
            {
                try
                {
                    processingList = true;

                    foreach (UserBasicInfoModel model in RingIDViewModel.Instance.FavouriteFriendList)
                    {
                        if (FriendListLoadUtility.CheckIfMatch(prevString, model, StatusConstants.SEARCH_BY_NAME))
                        {
                            if (!processingList)
                            {
                                return;
                            }
                            AddIntoSearchTagFriendListWithChecking(model, model.ShortInfoModel.UserTableID);
                            addASleep();
                        }
                    }

                    foreach (UserBasicInfoModel model in RingIDViewModel.Instance.TopFriendList)
                    {
                        if (FriendListLoadUtility.CheckIfMatch(prevString, model, StatusConstants.SEARCH_BY_NAME))
                        {
                            if (!processingList)
                            {
                                return;
                            }
                            AddIntoSearchTagFriendListWithChecking(model, model.ShortInfoModel.UserTableID);
                            addASleep();
                        }
                    }

                    foreach (UserBasicInfoModel model in RingIDViewModel.Instance.NonFavouriteFriendList)
                    {
                        if (!processingList)
                        {
                            return;
                        }
                        if (FriendListLoadUtility.CheckIfMatch(prevString, model, StatusConstants.SEARCH_BY_NAME))
                        {
                            AddIntoSearchTagFriendListWithChecking(model, model.ShortInfoModel.UserTableID);
                        }
                        addASleep();
                    }
                }
                catch (Exception ex)
                {

                    log.Error("AddIntoSearchTagFriendListBySearchResult==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);

                }
                finally
                {
                    processingList = false;
                }
            }
        }

        private void OpenFriendTagPopUp()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                popupTag.IsOpen = true;
                TagList.SelectedIndex = 0;
            });
        }
        public void CloseFriendTagPopUp()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                popupTag.IsOpen = false;
                runningSearchTagFriendThread = false;
                prevString = string.Empty;
            });
        }

        private void addASleep()
        {
            System.Threading.Thread.Sleep(SLEEP_TIME);
        }

        private void AddIntoSearchTagFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null)
            {
                if (userbasicinforModel.ShortInfoModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID && SearchTagFriends.Count < 5)
                {
                    SearchTagFriends.Add(userbasicinforModel);
                    OpenFriendTagPopUp();
                }
            }
        }
        private bool checkIfSearchTagFriendList(long utId)
        {
            if(ParentType == 0)
            {
                return SearchTagFriends.Any(p => p.ShortInfoModel.UserTableID == utId) || richTextBox._AddedFriendsUtid.Contains(utId);
            }
            else
            {
                return friendPrivacyPopupViewModel.FriendListToShowOrHide.Any(p => p.ShortInfoModel.UserTableID == utId);
            }
            
        }
        private void ClearSearchTagFriendList()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                SearchTagFriends.Clear();
            }, System.Windows.Threading.DispatcherPriority.Send);

            CloseFriendTagPopUp();
        }

        private void AddIntoSearchTagFriendListWithChecking(UserBasicInfoModel model, long utId)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                lock (SearchTagFriends)
                {
                    if (!checkIfSearchTagFriendList(utId))
                    {
                        AddIntoSearchTagFriendList(model);
                    }
                }
            });

        }
        #endregion

        private void grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DockPanel dp = sender as DockPanel;
            if (dp.DataContext is UserBasicInfoModel)
            {
                if (ParentType == 1)
                {
                    friendPrivacyPopupViewModel.AddFriendToCollection((UserBasicInfoModel)dp.DataContext);
                    this.addFriendTextBox.Clear();
                    this.addFriendTextBox.Focusable = true;
                    this.addFriendTextBox.Focus();
                    CloseFriendTagPopUp();
                }
                else
                {
                    SelectFriendTag((UserBasicInfoModel)dp.DataContext);
                }
            }

        }
        public void SelectFriendTag(UserBasicInfoModel selectedModel)
        {
            richTextBox.AlphaStarts = -1;
            richTextBox.ReplaceText("@" + searchParm, selectedModel.ShortInfoModel.FullName, selectedModel.ShortInfoModel.UserTableID);
            CloseFriendTagPopUp();
        }

        private void grid_MouseEnter(object sender, MouseEventArgs e)
        {
            DockPanel dock = (DockPanel)sender;
            //dock.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFF47727"));
            if (dock.DataContext is UserBasicInfoModel)
            {
                TagList.SelectedItem = (UserBasicInfoModel)dock.DataContext;
            }
        }

        private void grid_MouseLeave(object sender, MouseEventArgs e)
        {
            // DockPanel dock = (DockPanel)sender;
            // dock.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFFFFFF"));
        }

    }
}

﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.Utility.Myprofile;
using View.Utility;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCStatusPopup.xaml
    /// </summary>
    public partial class UCStatusPopup : PopUpBaseControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStatusPopup).Name);

        public UCStatusPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            //InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }

        #region"Property"

        
        #endregion"Property"

        public void ShowStatusPopup(UIElement target)
        {
            try
            {
                if (popupStatus.IsOpen)
                {
                    popupStatus.IsOpen = false;
                }
                else
                {
                    popupStatus.PlacementTarget = target;
                    popupStatus.IsOpen = true;
                    if (DefaultSettings.userProfile.Mood == StatusConstants.MOOD_ALIVE)
                    {
                        online.IsEnabled = false;
                        disturb.IsEnabled = true;
                    }
                    else if (DefaultSettings.userProfile.Mood == StatusConstants.MOOD_DONT_DISTURB)
                    {
                        disturb.IsEnabled = false;
                        online.IsEnabled = true;
                    }
                    popupStatus.Closed += popupStatus_Closed;
                    online.MouseLeftButtonUp += onlineMode_Click;
                    disturb.MouseLeftButtonUp += donotDstrbMode_Clic;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void onlineMode_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (DefaultSettings.userProfile.Mood != StatusConstants.MOOD_ALIVE)
                {
                    StartChangeMoodThread(StatusConstants.MOOD_ALIVE);
                    online.IsEnabled = false;
                    disturb.IsEnabled = true;
                    popupStatus.IsOpen = false;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void donotDstrbMode_Clic(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (DefaultSettings.userProfile.Mood != StatusConstants.MOOD_DONT_DISTURB)
                {
                    StartChangeMoodThread(StatusConstants.MOOD_DONT_DISTURB);
                    disturb.IsEnabled = false;
                    online.IsEnabled = true;
                    popupStatus.IsOpen = false;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void popupStatus_Closed(object sender, EventArgs e)
        {
            //Hide();
            popupStatus.Closed -= popupStatus_Closed;
            online.MouseLeftButtonUp -= onlineMode_Click;
            disturb.MouseLeftButtonUp -= donotDstrbMode_Clic;
        }

        private void StartChangeMoodThread(int mood)
        {
            MainSwitcher.ThreadManager().ChangeMood().StartThread(mood);
        }

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            Hide();
        }
        #endregion
    }
}

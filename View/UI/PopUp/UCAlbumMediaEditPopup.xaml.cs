﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.WPFMessageBox;

namespace View.UI.PopUp
{
    /// <summary>
    /// Interaction logic for UCAlbumMediaEditPopup.xaml
    /// </summary>
    public partial class UCAlbumMediaEditPopup : PopUpBaseControl//, INotifyPropertyChanged
    {
        public SingleMediaModel PlayingMedia = null;
        public MediaContentModel mediaContentmodel = null;

        #region "Constructor"
        public UCAlbumMediaEditPopup(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            //InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BORDER_BRUSH, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }
        #endregion

        //#region "PropertyChanged"
        //public event PropertyChangedEventHandler PropertyChanged;
        //public void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //}
        //#endregion

        #region "Property"
        //private string _songoralbumTxt;
        //public string SongorAlbumTxt
        //{
        //    get
        //    {
        //        return this._songoralbumTxt;
        //    }
        //    set
        //    {
        //        this._songoralbumTxt = value;
        //        this.OnPropertyChanged("SongorAlbumTxt");
        //    }
        //}

        //private string _artistTxt;
        //public string ArtistTxt
        //{
        //    get
        //    {
        //        return this._artistTxt;
        //    }
        //    set
        //    {
        //        this._artistTxt = value;
        //        this.OnPropertyChanged("ArtistTxt");
        //    }
        //}

        //private bool _isComponentEnabled;
        //public bool IsComponentEnabled
        //{
        //    get
        //    {
        //        return _isComponentEnabled;
        //    }
        //    set
        //    {
        //        if (value == _isComponentEnabled)
        //            return;
        //        _isComponentEnabled = value;
        //        OnPropertyChanged("IsComponentEnabled");
        //    }
        //}
        #endregion

        //#region Properties

        //public UCMediaListFromAlbumClickWrapper MediaListFromAlbumClickWrapper
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.MediaListFromAlbumClickWrapper;
        //    }
        //}

        //#endregion "Properties"

        public void ShowView(SingleMediaModel PlayingMedia)
        {
            this.mediaContentmodel = null;
            this.PlayingMedia = PlayingMedia;
            songoralbumTxtBox.Text = PlayingMedia.Title;
            artistTxtBox.Text = PlayingMedia.Artist;

            songoralbumRename.Text = "Rename Song";
            artistRename.Visibility = Visibility.Visible;
            artistTxtBoxContainer.Visibility = Visibility.Visible;
            setEvents();
        }

        public void ShowView(MediaContentModel mediaContentmodel)
        {
            this.PlayingMedia = null;
            this.mediaContentmodel = mediaContentmodel;
            songoralbumTxtBox.Text = mediaContentmodel.AlbumName;

            songoralbumRename.Text = "Rename Album";
            artistRename.Visibility = Visibility.Collapsed;
            artistTxtBoxContainer.Visibility = Visibility.Collapsed;

            setEvents();
        }

        public void ClosePopUp()
        {
            songoralbumTxtBox.PreviewKeyDown -= songoralbumTxtBox_PreviewKeyDown;
            artistTxtBox.PreviewKeyDown -= songoralbumTxtBox_PreviewKeyDown;
            saveBtn.Click -= saveBtn_Click;
            //Hide();
            //StopAnimation();
        }

        private void setEvents()
        {
            songoralbumTxtBox.PreviewKeyDown += songoralbumTxtBox_PreviewKeyDown;
            artistTxtBox.PreviewKeyDown += songoralbumTxtBox_PreviewKeyDown;
            saveBtn.Click += saveBtn_Click;
        }

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        ICommand _blackSpaceClick;
        public ICommand CloseWindowCommand
        {
            get
            {
                if (_blackSpaceClick == null)
                {
                    _blackSpaceClick = new RelayCommand(param => OnClickBlackSpace(param));
                }
                return _blackSpaceClick;
            }
        }
        private void OnClickBlackSpace(object param)
        {
            Hide();
        }

        ICommand _commentSpaceClicked;
        public ICommand CommentSpaceClicked
        {
            get
            {
                if (_commentSpaceClicked == null)
                {
                    _commentSpaceClicked = new RelayCommand(param => OnCommentSpaceClicked(param));
                }
                return _commentSpaceClicked;
            }
        }

        private void OnCommentSpaceClicked(object param)
        {
        }
        #endregion

        private void saveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (PlayingMedia != null)
            {
                string nmTxt = songoralbumTxtBox.Text.Trim();
                string artstTxt = artistTxtBox.Text.Trim();
                if (nmTxt.Equals(PlayingMedia.Title) && artstTxt.Equals(PlayingMedia.Artist))
                {
                    //CustomMessageBox.ShowWarning("No Change!");
                    UIHelperMethods.ShowWarning(NotificationMessages.ABOUT_NO_CHANGE, "Media edit failed!");
                }
                else
                {
                    songoralbumTxtBox.IsEnabled = false;
                    artistTxtBox.IsEnabled = false;
                    cancelBtn.IsEnabled = false;
                    //Cancel.IsEnabled = false;
                    saveBtn.IsEnabled = false;
                    MainSwitcher.ThreadManager().EditMediaOrAlbumInfo.callBackEvent += (response) =>
                    {
                        if (response == SettingsConstants.RESPONSE_SUCCESS)
                        {
                            PlayingMedia.Title = nmTxt;
                            PlayingMedia.Artist = artstTxt;
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                songoralbumTxtBox.IsEnabled = true;
                                artistTxtBox.IsEnabled = true;
                                cancelBtn.IsEnabled = true;
                                //Cancel.IsEnabled = true;
                                saveBtn.IsEnabled = true;
                                Hide();
                            });
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                songoralbumTxtBox.IsEnabled = true;
                                artistTxtBox.IsEnabled = true;
                                cancelBtn.IsEnabled = true;
                                //Cancel.IsEnabled = true;
                                saveBtn.IsEnabled = true;
                                UIHelperMethods.ShowWarning(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Media edit failed!");
                                //CustomMessageBox.ShowWarning("Failed to Edit! Please Check your internet connection or try later!");
                            });
                        }
                    };
                    MainSwitcher.ThreadManager().EditMediaOrAlbumInfo.StartThread(PlayingMedia, nmTxt, artstTxt);
                }
            }
            else if (mediaContentmodel != null)
            {
                string nmTxt = songoralbumTxtBox.Text.Trim();
                if (nmTxt.Equals(mediaContentmodel.AlbumName))
                {
                    UIHelperMethods.ShowWarning(NotificationMessages.ABOUT_NO_CHANGE, "Media edit failed!");
                    // CustomMessageBox.ShowWarning("No Change!");
                }
                else
                {
                    songoralbumTxtBox.IsEnabled = false;
                    cancelBtn.IsEnabled = false;
                    saveBtn.IsEnabled = false;
                    MainSwitcher.ThreadManager().EditMediaOrAlbumInfo.callBackEvent += (response) =>
                    {
                        if (response == SettingsConstants.RESPONSE_SUCCESS)
                        {
                            mediaContentmodel.AlbumName = nmTxt;
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                songoralbumTxtBox.IsEnabled = true;
                                cancelBtn.IsEnabled = true;
                                saveBtn.IsEnabled = true;
                                Hide();
                            });
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                songoralbumTxtBox.IsEnabled = true;
                                cancelBtn.IsEnabled = true;
                                saveBtn.IsEnabled = true;
                                UIHelperMethods.ShowWarning(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Media edit failed!");
                                //  CustomMessageBox.ShowWarning("Failed to Edit! Please Check your internet connection or try later!");
                            });
                        }
                    };
                    MainSwitcher.ThreadManager().EditMediaOrAlbumInfo.StartThread(mediaContentmodel, nmTxt, null, 0, 0);
                }
            }
        }

        private void songoralbumTxtBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                saveBtn_Click(null, null);
        }
    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using View.Utility;
//using Outlook = Microsoft.Office.Interop.Outlook;

namespace View.UI.AddFriend
{
    /// <summary>
    /// Interaction logic for UCInvite.xaml
    /// </summary>
    public partial class UCInvite : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCInvite).Name);
        //public static UCInvite Instance; 
        private const string fbShareLink = "https://www.facebook.com/sharer/sharer.php?u=www.ringid.com&t=TITLE";
        private const string twShareLink = "https://twitter.com/share?url=www.ringid.com&via=ringidapp&text=Don't%20miss%20the%20free%20calling,%20hottest%20messaging%20features,%20fun%20stickers,%20secret%20chat%20and%20many%20more!%20http://www.ringid.com";
        private const string gplsSharelink = "https://plus.google.com/share?url=www.ringid.com";
        private const string emilSharelink = "mailto:id@receivermailid.com?subject=RingID&body=Don't%20miss%20the%20free%20calling%20hottest%20messaging%20features%20of%20ringID%20with%20fun%20stickers%2C%20secret%20chat%2C%20multimedia%20sharing%2C%20newsfeed%20%3B%20more.%20Get%20your%20ringID%20Now!%0Ahttp%3A%2F%2Fwww.ringid.com";
        public UCInvite()
        {
            InitializeComponent();
            this.DataContext = this;
            //Instance = this;
        }

        public void GoToSite(string url)
        {
            Process myProcess = new Process();

            try
            {
                // true is the default, but it is important not to set it to false
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = url;
                myProcess.Start();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }

        }

        private void FacebookBtn_Click(object sender, RoutedEventArgs e)
        {
            GoToSite(fbShareLink);
        }

        private void TwitterBtn_Click(object sender, RoutedEventArgs e)
        {
            GoToSite(twShareLink);
        }
        private void GooglePlusBtn_Click(object sender, RoutedEventArgs e)
        {
            GoToSite(gplsSharelink);
        }

        private void EmailBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenOutlookandSendMail();
        }

        public void OpenOutlookandSendMail()
        {
            try
            {
                string protocol = "mailto";
                string to = String.Empty;//"info@ringid.com";

                string subject = "Your invitation to join ringID!";
                string body = "Hi " + DefaultSettings.userProfile.FullName + " here!%0A%0A";
                body += "Join me on ringID. It's a revolutionary ad-free integrated social network, where you can make Free Calls, write Instant Messages ";
                body += "with Secret Chat and get Newsfeed updates in one place. It's time to forget the stress of managing several different apps because ";
                body += "ringID has all the features that we are looking for and the good news is it's free!%0AMy ringID is ";
                body += DefaultSettings.LOGIN_RING_ID + ". Get your's now from: http%3A%2F%2Fwww.ringid.com.%0A";

                var content = protocol + ":" + to + "?" + "subject=" + subject + "&body=" + body;
                System.Diagnostics.Process.Start(content);
            }//end of try block
            catch (Exception ex)
            {
                MessageBox.Show(null, "Please, Install mail client first to send mail!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                log.Error("Error at OpenOutlookandSendMail() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //public void OpenOutlookandSendMail()
        //{
        //    try
        //    {
        //        List<string> lstAllRecipients = new List<string>();
        //        //Below is hardcoded - can be replaced with db data
        //        lstAllRecipients.Add("info@ringid.com");

        //        Outlook.Application outlookApp = new Outlook.Application();
        //        Outlook.Application application = null;

        //        // Check whether there is an Outlook process running.
        //        //if (StartProcess.GetProcessesByName("OUTLOOK").Count() > 0)
        //        //{

        //        //    // If so, use the GetActiveObject method to obtain the process and cast it to an Application object.
        //        //    application = Marshal.GetActiveObject("Outlook.Application") as Outlook.Application;
        //        //}
        //        //else
        //        //{
        //        Type officeType = Type.GetTypeFromProgID("Outlook.Application");

        //        if (officeType == null)
        //        {
        //            // Outlook is not installed.   
        //            // Show message or alert that Outlook is not installed.
        //            MessageBox.Show(MainSwitcher.MainController().MainUIController().MainWindow, "Please Install Outlook first to Send mail!", "Information!", MessageBoxButton.OK, MessageBoxImage.Information);
        //            return;
        //        }
        //        else
        //        {
        //            // Outlook is installed.    
        //            // Continue your work.
        //            application = new Outlook.Application();
        //        }

        //        // If not, create a new instance of Outlook and log on to the default profile.
        //        // application = new Outlook.Application();
        //        //   }
        //        Outlook._MailItem oMailItem = (Outlook._MailItem)application.CreateItem(Outlook.OlItemType.olMailItem);
        //        Outlook.Inspector oInspector = oMailItem.GetInspector;
        //        // Thread.Sleep(10000);

        //        // Recipient
        //        Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;
        //        foreach (String recipient in lstAllRecipients)
        //        {
        //            Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(recipient);
        //            oRecip.Resolve();
        //        }

        //        //Add CC
        //        //Outlook.Recipient oCCRecip = oRecips.Add("someone@gmail.com");
        //        //oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
        //        //oCCRecip.Resolve();

        //        //Add Subject
        //        oMailItem.Subject = "Your invitation to join ringID!";
        //        // oMailItem.Body = "Device: Desktop, Please Describe your Problem or Question!";
        //        string str = "Hi " + DefaultSettings.userProfile.FullName + " here!";
        //        str += "Join me on ringID. It's a revolutionary ad-free integrated social network, where you can make Free Calls, write Instant Messages with Secret Chat and get Newsfeed updates in one place.\n It's time to forget the stress of managing several different apps because ringID has all the features that we are looking for and the good news is it's free!\n My ringID is ";
        //        str += DefaultSettings.LOGIN_USER_ID + ". Get your's now from: www.ringid.com.\n";
        //        oMailItem.Body = str;
        //        // body, bcc etc...
        //        //Display the mailbox
        //        oMailItem.Display(true);
        //    }//end of try block
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(MainSwitcher.MainController().MainUIController().MainWindow, "Please, Install mail client first to send mail!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
        //        log.Error("Error at OpenOutlookandSendMail() => " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

    }
}

﻿using Auth.utility;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.UI.Settings;
using View.Utility.FriendList;
using View.ViewModel;
using log4net;

namespace View.UI.AddFriend
{
    /// <summary>
    /// Interaction logic for UCAddFriendMainPanel.xaml
    /// </summary>
    public partial class UCAddFriendMainPanel : UserControl, INotifyPropertyChanged
    {
        #region "Private Fields"
        private string tabItem;
        public bool isLoaded = false;
        private static readonly ILog log = LogManager.GetLogger(typeof(UCAddFriendMainPanel).Name);
        public UCBlockedContacts _BlockedContactsInstance = null;
        private ThrdLoadIncommingListInView thrdLoadIncommingListInView;
        private ThrdLoadPendingListInView thrdLoadPendingListInView;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"

        public UCAddFriendMainPanel()
        {
            InitializeComponent();
            this.IsVisibleChanged += UCAddFriendMainPanel_IsVisibleChanged;
            this.DataContext = this;
            //Instance = this;
        }

        #endregion "Constructors"

        #region "Properties"
        private int _IncomingRequestCount;
        public int IncomingRequestCount
        {
            get { return _IncomingRequestCount; }
            set
            {
                if (value == _IncomingRequestCount)
                    return;

                _IncomingRequestCount = value;
                this.OnPropertyChanged("IncomingRequestCount");
            }
        }

        private int _IncomingRequestCounterSize;
        public int IncomingRequestCounterSize
        {
            get
            {
                return _IncomingRequestCounterSize;
            }
            set
            {
                if (_IncomingRequestCounterSize == value)
                {
                    return;
                }
                _IncomingRequestCounterSize = value;
                OnPropertyChanged("IncomingRequestCounterSize");
            }
        }
        #endregion "Properties"

        #region "Event Trigger"
        private void UCAddFriendMainPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                RingIDViewModel.Instance.AvatarButtonSelection = true;
            }
            else if ((bool)e.NewValue == false)
            {
                RingIDViewModel.Instance.AvatarButtonSelection = false;
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tabItem = ((sender as TabControl).SelectedItem as TabItem).Name as string;
            switch (tabItem)
            {
                case "FriendSearchTab":
                    if (UCAddFriendSearchPanel.Instance == null)
                    {
                        FriendSearchPanel.Child = new UCAddFriendSearchPanel();
                    }
                    break;
                case "FriendSuggestionTab":
                    if (UCSuggestions.Instance == null)
                    {
                        _AddFriendSuggestion.Children.Add(new UCSuggestions());
                    }
                    break;
                case "FriendInviteTab":
                    break;
                case "FriendPendingTab":
                    LoadPendingData();
                    break;
                case "FriendBlockListTab":
                    if (_BlockedContactsInstance == null)
                    {
                        _BlockedContactsInstance = new UCBlockedContacts();
                        BlockListPanel.Child = _BlockedContactsInstance;
                    }
                    break;
            }
        }
        #endregion "Event Trigger"

        #region "Private methods"
        private void LoadPendingList(int incomingIndex, int outgoingIndex, bool isNeedToLoadIncoming = false, bool isNeedToLoadOutgoing = false)
        {
            if (isNeedToLoadIncoming && FriendListController.Instance.FriendDataContainer.IncommingListCount() > 0)
            {
                if (thrdLoadIncommingListInView == null) thrdLoadIncommingListInView = new ThrdLoadIncommingListInView();
                thrdLoadIncommingListInView.StartThread(10);
            }
            if (isNeedToLoadOutgoing && FriendListController.Instance.FriendDataContainer.PendingListcount() > 0)
            {
                if (thrdLoadPendingListInView == null) thrdLoadPendingListInView = new ThrdLoadPendingListInView();
                thrdLoadPendingListInView.StartThread(10);
                // FriendListController.Instance.StartThrdLoadPendingListInView();
            }
        }
        #endregion "Private methods"

        #region "Public Methods"

        public void LoadPendingData()
        {
            if (!isLoaded)
            {
                //PendingListLoadUtility.Instance.LoadPendingList(0, 0, true, true);
                LoadPendingList(0, 0, true, true);
                isLoaded = true;
            }
            if (UCAddFriendPending.Instance != null)
            {
                UCAddFriendPending.Instance.ShowIncommingAndPendingCounts();
            }
        }
        #endregion "Public methods
        //public static UCAddFriendMainPanel Instance;

        #region Utility Methods

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}

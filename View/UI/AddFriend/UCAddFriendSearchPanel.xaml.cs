﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.BindingModels;
using View.Utility;
using View.Utility.FriendList;
using View.ViewModel;
using View.Utility.WPFMessageBox;
using System.Windows.Media;
using System.Timers;
using View.Utility.Auth;
using View.UI.PopUp;

namespace View.UI.AddFriend
{
    /// <summary>
    /// Interaction logic for UCAddFriendSearchPanel.xaml
    /// </summary>
    public partial class UCAddFriendSearchPanel : UserControl, INotifyPropertyChanged
    {
        public static UCAddFriendSearchPanel Instance;
        public ThrdSearchFriend _SearchFriend;
        private long _SelectedFriendUserTableId = 0;
        private string _SearchString = String.Empty;
        private ICommand _AddFriendButtonCommand;
        private ICommand _OpenPopupCommand;
        private ICommand _CommonExpandCommand;

        private Timer SearchTextBoxTimer;
        private int ThreadMaxRunningTime = 0;

        #region Constructor
        public UCAddFriendSearchPanel()
        {
            InitializeComponent();
            Instance = this;
            this.DataContext = this;
            this.Loaded += UCAddFriend_Loaded;

        }
        #endregion Constructor

        void UCAddFriend_Loaded(object sender, RoutedEventArgs e)
        {
            this.Unloaded += UCAddFriend_Unloaded;
            cancel.Click += cancel_Click;
            Scroll.PreviewKeyDown += Scroll_PreviewKeyDown;
            SearchTermTextBox.PreviewKeyDown += SearchTermTextBox_PreviewKeyDown;
            SearchTermTextBox.LostFocus += SearchTermTextBox_LostFocus;
            showMoreButtonPanel.Click += ShowMore_PanelClick;
            SearchTermTextBox.GotKeyboardFocus += SearchTermTextBox_GotKeyboardFocus;
            SearchTermTextBox.Focus();
            SearchTextBoxTimer = new Timer();
        }

        private void SearchTermTextBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(SearchTermTextBox.Text) && RingIDViewModel.Instance.TempMyFriendList.Count > 0)
            {
                ClearTempList();
            }
        }
        public void UCAddFriend_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SetDefaultParameter();
                if (SearchTextBoxTimer != null)
                {
                    SearchTextBoxTimer.Stop();
                    SearchTextBoxTimer.Enabled = false;
                }
                SearchTextBoxTimer = null;
                SearchTermTextBox.Text = string.Empty;
                SelectedFriendUserTableId = 0;
                ThrdSearchFriend.searchParm = null;
                AddRemoveInCollections.ClearTempFriendList();
                cancel.Click -= cancel_Click;
                Scroll.PreviewKeyDown -= Scroll_PreviewKeyDown;
                SearchTermTextBox.PreviewKeyDown -= SearchTermTextBox_PreviewKeyDown;
                SearchTermTextBox.LostFocus -= SearchTermTextBox_LostFocus;
                SearchTermTextBox.GotKeyboardFocus -= SearchTermTextBox_GotKeyboardFocus;
                showMoreButtonPanel.Click -= ShowMore_PanelClick;
                this.Unloaded -= UCAddFriend_Unloaded;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCAddFriendSearchPanel).Name).Error(ex.StackTrace + ex.Message);
            }
            finally
            {
                ThrdSearchFriend.runningSearchFriendThread = false;
            }
        }

        #region "Event Listner"

        void SearchTermTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            //  SearchFriend.runningSearchFriendThread = false;
        }
        public void OpenPopupExecute(object parameter)
        {
            Button control = (Button)parameter;
            UserBasicInfoModel model = (UserBasicInfoModel)control.Tag;
            if (model.NumberOfMutualFriends > 0)
            {
                if (UCMutualFriendsPopup.Instance != null)
                    mainPanel.Children.Remove(UCMutualFriendsPopup.Instance);
                UCMutualFriendsPopup.Instance = new UCMutualFriendsPopup();
                UCMutualFriendsPopup.Instance.SetParent(mainPanel);
                UCMutualFriendsPopup.Instance.Show();
                UCMutualFriendsPopup.Instance.ShowPopup(control, model.ShortInfoModel.UserTableID, model.ShortInfoModel.FullName);
                UCMutualFriendsPopup.Instance.OnRemovedUserControl += () =>
                {
                    UCMutualFriendsPopup.Instance.MutualList.Clear();
                    UCMutualFriendsPopup.Instance = null;
                };
                SendDataToServer.SendMutualFreindRequest(model.ShortInfoModel.UserTableID);
            }
        }
        #endregion "Event"
        #region ICommands
        public ICommand OpenPopupCommand
        {
            get
            {
                if (_OpenPopupCommand == null)
                    _OpenPopupCommand = new RelayCommand(param => OpenPopupExecute(param));
                return _OpenPopupCommand;
            }
            set
            {
                _OpenPopupCommand = value;
            }
        }
        public ICommand AddFriendButtonCommand
        {
            get
            {
                if (_AddFriendButtonCommand == null)
                {
                    _AddFriendButtonCommand = new RelayCommand(param => OnAddFriendButtonClicked(param));
                }
                return _AddFriendButtonCommand;
            }
        }

        public ICommand CommonExpandCommand
        {
            get
            {
                if (_CommonExpandCommand == null)
                {
                    _CommonExpandCommand = new RelayCommand(param => OnExpandViewClicked(param));
                }
                return _CommonExpandCommand;
            }
        }

        #endregion "ICommands"
        #region "Property Change"
        public long SelectedFriendUserTableId
        {
            get { return _SelectedFriendUserTableId; }
            set { _SelectedFriendUserTableId = value; }
        }
        public string SearchString
        {
            get { return _SearchString; }
            set
            {
                _SearchString = value;
                SetDefaultParameter();
                if (!string.IsNullOrWhiteSpace(SearchString))
                {
                    DoSearch();
                }
                else
                {
                    ThrdSearchFriend.searchParm = null;
                    NoResultsFound = false;
                    ClearTempList();
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
        #region "Button Events"
        private void Button_Add_Click(object sender, RoutedEventArgs e)
        {
            Control control = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            if (SelectedFriendUserTableId.Equals(model.ShortInfoModel.UserTableID))
            {
                SelectedFriendUserTableId = 0;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
            }
            else
            {
                long tempUTId = SelectedFriendUserTableId;
                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");

                UserBasicInfoModel tempModel = AddRemoveInCollections.getSingleTempFriend(tempUTId);
                if (tempModel != null)
                {
                    tempModel.ShortInfoModel.OnPropertyChanged("UserTableID");
                }

            }
        }
        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }
        #endregion "Buttons Events"
        #region "Utility Methods"
        public void OnAddFriendButtonClicked(object parameter)
        {
            RingIDViewModel.Instance.OnAddFriendClicked(parameter);
        }
        private void OnExpandViewClicked(object parameter)
        {
            ShowDefaultOrExpandView((UserBasicInfoModel)parameter);
        }
        private void ClearTempList()
        {
            RingIDViewModel.Instance.TempFriendList.Clear();
        }


        private void DoSearch()
        {
            try
            {
                if (string.IsNullOrEmpty(ThrdSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThrdSearchFriend.prevString) && !ThrdSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    ThreadMaxRunningTime = 0;
                    SearchTextBoxTimer.Elapsed -= SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Elapsed += SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Interval = 400;
                    SearchTextBoxTimer.AutoReset = true;
                    SearchTextBoxTimer.Enabled = true;
                    SearchTextBoxTimer.Stop();
                    SearchTextBoxTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCAddFriendSearchPanel).Name).Error(ex.StackTrace + " " + ex.Message);
            }
        }
        void SearchTextBoxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                if (string.IsNullOrEmpty(ThrdSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThrdSearchFriend.prevString) && !ThrdSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    HideNoResultsFound();
                    ClearTempListFromTimer();
                    OnSearch();
                }
            }
            else
            {
                ClearTempListFromTimer();
            }

            ThreadMaxRunningTime += 400;
            if (ThreadMaxRunningTime >= 3000)
            {
                ThreadMaxRunningTime = 0;
                SearchTextBoxTimer.Enabled = false;
                SearchTextBoxTimer.Stop();
            }
        }
        private void ClearTempListFromTimer()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                lock (RingIDViewModel.Instance.TempFriendList)
                {
                    RingIDViewModel.Instance.TempFriendList.Clear();
                }
            });
        }

        private void OnSearch()
        {
            ThrdSearchFriend.searchParm = SearchString.ToLower();
            if (!ThrdSearchFriend.runningSearchFriendThread)
            {
                (new ThrdSearchFriend(ThrdSearchFriend.SearchFromAddFriend, StatusConstants.SEARCH_BY_ALL)).StartThread();
            }
        }
        public Control sender { get; set; }
        #endregion "Utility Methods"

        private ImageSource _LoaderSmall;
        public ImageSource LoaderSmall
        {
            get
            {
                return _LoaderSmall;
            }
            set
            {
                if (value == _LoaderSmall)
                    return;
                _LoaderSmall = value;
                OnPropertyChanged("LoaderSmall");
            }
        }

        private ImageSource _Loader;
        public ImageSource Loader
        {
            get
            {
                return _Loader;
            }
            set
            {
                if (value == _Loader)
                    return;
                _Loader = value;
                OnPropertyChanged("Loader");
            }
        }

        private bool _NoResultsFound = false;
        public bool NoResultsFound
        {
            get
            {
                return _NoResultsFound;
            }
            set
            {
                if (value == _NoResultsFound)
                    return;
                _NoResultsFound = value;
                OnPropertyChanged("NoResultsFound");
            }
        }

        #region Visibility
        private void SetDefaultParameter()
        {
            ThrdSearchFriend.processingList = false;
            ThrdSearchFriend.SearchForShowMore = false;
            ThrdSearchFriend.totalSearchCount = 0;
            ThrdSearchFriend.friendCountInOneRequest = 0;
            ShowSearchingLoader(false);
            HideShowMorePanel();           
        }

        public void HideNoResultsFound()
        {
            if (!string.IsNullOrWhiteSpace(ThrdSearchFriend.searchParm))
            {
                NoResultsFound = false;
            }
        }
        public void HideShowMorePanel()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                showMorePanel.Visibility = Visibility.Collapsed;
            }, System.Windows.Threading.DispatcherPriority.Send);
        }
        public void ShowLoader(bool show)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (!string.IsNullOrWhiteSpace(ThrdSearchFriend.searchParm) && RingIDViewModel.Instance.TempFriendList.Count > 0)
                {
                    if (show)
                    {
                        showMoreLoaderBorder.Visibility = Visibility.Visible;
                        Loader = ImageObjects.LOADER_FEED;
                    }
                    else
                    {
                        showMoreLoaderBorder.Visibility = Visibility.Collapsed;
                        showMorePanel.Visibility = Visibility.Visible;
                        showMoreButtonPanel.Visibility = Visibility.Visible;
                        if (ImageObjects.LOADER_FEED != null)
                            GC.SuppressFinalize(ImageObjects.LOADER_FEED);
                        if (Loader != null)
                            GC.SuppressFinalize(Loader);
                        ImageObjects.LOADER_FEED = null;
                        Loader = null;
                    }
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public void ShowSearchingLoader(bool isSearching)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (isSearching)
                {
                    LoaderSmall = ImageObjects.LOADER_SMALL;
                }
                else
                {
                    if (ImageObjects.LOADER_SMALL != null)
                        GC.SuppressFinalize(ImageObjects.LOADER_SMALL);
                    if (LoaderSmall != null)
                        GC.SuppressFinalize(LoaderSmall);
                    ImageObjects.LOADER_SMALL = null;
                    LoaderSmall = null;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void ShowMore_PanelClick(object sender, RoutedEventArgs e)
        {
            ShowLoader(true);
            showMoreButtonPanel.Visibility = Visibility.Collapsed;
            ThrdSearchFriend.SearchForShowMore = true;
            ThrdSearchFriend.friendCountInOneRequest = 0;
            //SearchFriend.processingList = false;
            ThrdSearchFriend.searchParm = SearchString.ToLower();
            //  SearchFriend.sendSearchRequest();
            if (!ThrdSearchFriend.runningSearchFriendThread)
            {
                (new ThrdSearchFriend(ThrdSearchFriend.SearchFromAddFriend, StatusConstants.SEARCH_BY_ALL)).StartThread();
            }
        }
        #endregion
        private void SearchTermTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Back && (string.IsNullOrWhiteSpace(SearchTermTextBox.Text) || SearchTermTextBox.Text.Length == 0))
            {
                RingIDViewModel.Instance.TempFriendList.Clear();
                showMorePanel.Visibility = Visibility.Collapsed;
            }
        }

        private void Control_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Control control = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            ShowDefaultOrExpandView(model);
        }
        private void ShowDefaultOrExpandView(UserBasicInfoModel model)
        {
            if (SelectedFriendUserTableId.Equals(model.ShortInfoModel.UserTableID))
            {
                SelectedFriendUserTableId = 0;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
            }
            else
            {
                long tempUTId = SelectedFriendUserTableId;
                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
                UserBasicInfoModel tempModel = AddRemoveInCollections.getSingleTempFriend(tempUTId);
                if (tempModel != null)
                {
                    tempModel.ShortInfoModel.OnPropertyChanged("UserTableID");
                }
            }
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.Text = null;
            RingIDViewModel.Instance.TempFriendList.Clear();
            showMorePanel.Visibility = Visibility.Collapsed;
        }
    }
}

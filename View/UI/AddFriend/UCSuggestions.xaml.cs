﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Models.Constants;
using Models.Stores;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;
using View.Utility.FriendList;
using View.Utility.Suggestion;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Constants;
using View.UI.PopUp;

namespace View.UI.AddFriend
{
    /// <summary>
    /// Interaction logic for UCSuggestions.xaml
    /// </summary>
    public partial class UCSuggestions : UserControl, INotifyPropertyChanged
    {
        #region "Private Member"
        private bool IsNoSearchResultsFound = true;
        //private BackgroundWorker addFriendButtonClickedWorker, removeButtonWorker;
        #endregion

        #region "Public Member"
        public static UCSuggestions Instance;
        #endregion

        #region "Property"
        private long _SelectedFriendUserTableId = 0;
        public long SelectedFriendUserTableId
        {
            get { return _SelectedFriendUserTableId; }
            set { _SelectedFriendUserTableId = value; }
        }

        private string _SearchString = String.Empty;
        public string SearchString
        {
            get { return _SearchString; }
            set
            {
                _SearchString = value;
                OnSearch();
            }
        }

        private ImageSource _Loader;
        public ImageSource Loader
        {
            get
            {
                return _Loader;
            }
            set
            {
                if (value == _Loader)
                    return;
                _Loader = value;
                OnPropertyChanged("Loader");
            }
        }

        private Visibility _ShowMorePanel = Visibility.Collapsed;
        public Visibility ShowMorePanel
        {
            get
            {
                return _ShowMorePanel;
            }
            set
            {
                if (value == _ShowMorePanel) return;
                _ShowMorePanel = value;
                OnPropertyChanged("ShowMorePanel");
            }
        }

        private Visibility _ShowMoreButtonPanel;
        public Visibility ShowMoreButtonPanel
        {
            get
            {
                return _ShowMoreButtonPanel;
            }
            set
            {
                if (value == _ShowMoreButtonPanel) return;
                _ShowMoreButtonPanel = value;
                OnPropertyChanged("ShowMoreButtonPanel");
            }
        }

        private Visibility _ShowMoreLoaderBorder = Visibility.Collapsed;
        public Visibility ShowMoreLoaderBorder
        {
            get
            {
                return _ShowMoreLoaderBorder;
            }
            set
            {
                if (value == _ShowMoreLoaderBorder) return;
                _ShowMoreLoaderBorder = value;
                OnPropertyChanged("ShowMoreLoaderBorder");
            }
        }

        private Visibility _NoResultsFound;
        public Visibility NoResultsFound
        {
            get
            {
                return _NoResultsFound;
            }
            set
            {
                if (value == _NoResultsFound) return;
                _NoResultsFound = value;
                OnPropertyChanged("NoResultsFound");
            }
        }

        #endregion

        #region "Constructor"
        public UCSuggestions()
        {
            InitializeComponent();
            Instance = this;
            this.DataContext = this;
            this.Unloaded += ((s, e) =>
            {
                SearchTermTextBox.Text = string.Empty;
            });

            this.Loaded += ((s, e) =>
            {
                NoResultsFound = Visibility.Collapsed;
            });

            lock (RingIDViewModel.Instance.SuggestionFriendList)
            {
                if (!RingIDViewModel.Instance.SuggestionFriendList.Any(P => P.VisibilityModel.IsVisibleInSuggestion == Visibility.Visible))
                {
                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); ;
                    this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                    FetchTenSuggestionDetails((onComplete) =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (GIFCtrl != null && GIFCtrl.IsRunning())
                            {
                                this.GIFCtrl.StopAnimate();
                            }
                        });
                        if (RingIDViewModel.Instance.SuggestionFriendList.Count > 0) ShowMorePanel = Visibility.Visible;
                        return 0;
                    });
                }
            }
        }
        #endregion

        #region "Command"
        private ICommand _AddFriendButtonCommand;
        public ICommand AddFriendButtonCommand
        {
            get
            {
                if (_AddFriendButtonCommand == null)
                {
                    _AddFriendButtonCommand = new RelayCommand(param => OnAddFriendButtonClicked(param));
                }
                return _AddFriendButtonCommand;
            }
        }

        private ICommand _OpenPopupCommand;
        public ICommand OpenPopupCommand
        {
            get
            {
                if (_OpenPopupCommand == null)
                    _OpenPopupCommand = new RelayCommand(param => OpenPopupExecute(param));
                return _OpenPopupCommand;
            }
            set
            {
                _OpenPopupCommand = value;
            }
        }

        private ICommand _RemoveButtonCommand;
        public ICommand RemoveButtonCommand
        {
            get
            {
                if (_RemoveButtonCommand == null)
                {
                    _RemoveButtonCommand = new RelayCommand(param => OnRemoveButtonClicked(param));
                }
                return _RemoveButtonCommand;
            }
        }

        private ICommand _CommonExpandCommand;
        public ICommand AcceptCommand
        {
            get
            {
                if (_CommonExpandCommand == null)
                {
                    _CommonExpandCommand = new RelayCommand(param => OnExpandViewClicked(param));
                }
                return _CommonExpandCommand;
            }
        }

        public ICommand RejectCommand
        {
            get
            {
                if (_CommonExpandCommand == null)
                {
                    _CommonExpandCommand = new RelayCommand(param => OnExpandViewClicked(param));
                }
                return _CommonExpandCommand;
            }
        }

        public ICommand CommonExpandCommand
        {
            get
            {
                if (_CommonExpandCommand == null)
                {
                    _CommonExpandCommand = new RelayCommand(param => OnExpandViewClicked(param));
                }
                return _CommonExpandCommand;
            }
        }

        private ICommand _LoadMoreButtonCommand;
        public ICommand LoadMoreButtonCommand
        {
            get
            {
                if (_LoadMoreButtonCommand == null)
                {
                    _LoadMoreButtonCommand = new RelayCommand(param => OnLoadMoreButtonClicked());
                }
                return _LoadMoreButtonCommand;
            }
        }
        #endregion

        #region "Event Handler"
        //private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        //{
        //    //showMoreText.Visibility = Visibility.Collapsed;
        //    ShowMoreButtonPanel = Visibility.Collapsed;
        //    //showMoreLoader.Visibility = Visibility.Visible;
        //    ShowLoader(true);
        //    FetchTenSuggestionDetails();
        //}

        private void Control_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Control control = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            ShowDefaultOrExpandView(model);
        }

        //private void LoadMore_ButtonClick(object sender, RoutedEventArgs e)
        //{
        //    OnLoadMoreButtonClicked();
        //}       

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.Text = string.Empty;
        }
        #endregion

        #region "Private Method"
        private void FetchTenSuggestionDetails(Func<bool, int> onComplete = null)
        {
            List<long> ids = new List<long>();
            foreach (KeyValuePair<long, bool> id in FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
            {
                if (id.Value == false)
                {
                    ids.Add(id.Key);
                    if (ids.Count == 10)
                    {
                        break;
                    }
                }
            }
            if (ids.Count > 0)
            {
                (new ThreadSuggestionUsersDetailsRequest()).StartThread(ids, (status) =>
                {
                    if (onComplete != null && status)
                    {
                        onComplete(true);
                    }
                    return 0;
                });
            }
            else
            {
                ShowLoader(false);
                //UCSuggestions.Instance.showMoreText.Visibility = Visibility.Visible;
                ShowMoreButtonPanel = Visibility.Visible;
                ShowMorePanel = Visibility.Collapsed;
                if (onComplete != null)
                {
                    onComplete(true);
                }
            }
        }

        private void OnSearch()
        {
            IsNoSearchResultsFound = true;
            lock (RingIDViewModel.Instance.SuggestionFriendList)
            {
                RingIDViewModel.Instance.SuggestionFriendList.ToList().ForEach(P => P.VisibilityModel.IsVisibleInSuggestion = IsFriendVisibility(P));
                if (!string.IsNullOrWhiteSpace(_SearchString) && IsNoSearchResultsFound == true)
                {
                    ShowMorePanel = Visibility.Collapsed;
                    NoResultsFound = Visibility.Visible;
                }
                else
                {
                    if (!RingIDViewModel.Instance.SuggestionFriendList.Any(P => P.VisibilityModel.IsVisibleInSuggestion == Visibility.Visible) &&
                        RingIDViewModel.Instance.SuggestionFriendList.Count > 0)
                        ShowMorePanel = Visibility.Visible;
                    NoResultsFound = Visibility.Collapsed;
                }
            }
        }

        private void OnExpandViewClicked(object parameter)
        {
            ShowDefaultOrExpandView((UserBasicInfoModel)parameter);
        }

        private void RemoveFromSuggesion(UserBasicInfoModel _model, bool isAccept)
        {
            _model.VisibilityModel.ShowActionButton = false;
            _model.VisibilityModel.ShowLoading = Visibility.Visible;
            new RemoveFromSuggestion(_model, false);
        }

        private void ShowDefaultOrExpandView(UserBasicInfoModel model)
        {
            if (SelectedFriendUserTableId.Equals(model.ShortInfoModel.UserTableID))
            {
                SelectedFriendUserTableId = 0;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
            }
            else
            {
                long tempUTId = SelectedFriendUserTableId;
                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");

                //   UserBasicInfoModel tempModel = RingIDViewModel.Instance.SuggestionFriendList.Where(P => P.ShortInfoModel.UserIdentity == tempIdentity).FirstOrDefault();
                //UserBasicInfoModel tempModel = AddRemoveInCollections.getSingleSuggestionFriend(tempIdentity);
                UserBasicInfoModel tempModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(tempUTId);
                if (tempModel != null)
                {
                    tempModel.ShortInfoModel.OnPropertyChanged("UserTableID");
                }
            }
        }

        private void OnLoadMoreButtonClicked()
        {
            ShowMoreButtonPanel = Visibility.Collapsed;
            //showMoreLoader.Visibility = Visibility.Visible;
            ShowLoader(true);
            FetchTenSuggestionDetails();
        }
        #endregion

        #region "Public Method"
        public void ShowLoader(bool show)
        {
            if (show)
            {
                ShowMoreLoaderBorder = Visibility.Visible;
                Loader = ImageObjects.LOADER_FEED;
            }
            else
            {
                ShowMoreLoaderBorder = Visibility.Collapsed;
                if (ImageObjects.LOADER_FEED != null)
                    GC.SuppressFinalize(ImageObjects.LOADER_FEED);
                if (Loader != null)
                    GC.SuppressFinalize(Loader);
                ImageObjects.LOADER_FEED = null;
                Loader = null;
            }
        }

        public void ShowMorePanelHideShow()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Count == 0)
                {
                    ShowMorePanel = Visibility.Collapsed;
                }
                else if (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Count > 0)
                {
                    lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Any(P => P.Value == false))
                            {
                                ShowMorePanel = Visibility.Visible;
                            }
                            else
                            {
                                ShowMorePanel = Visibility.Collapsed;
                            }
                        }, System.Windows.Threading.DispatcherPriority.Send);
                    }
                }
                else
                {
                    ShowMorePanel = Visibility.Collapsed;
                }
            });
        }

        public Visibility IsFriendVisibility(UserBasicInfoModel model)
        {
            Visibility result = Visibility.Collapsed;
            try
            {
                if (model.VisibilityModel.IsVisibleInPeopleUmayKnow == Visibility.Visible)
                {
                    return result;
                }

                string searchText = SearchString.ToLower();
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    if (!string.IsNullOrWhiteSpace(model.ShortInfoModel.FullName) && model.ShortInfoModel.FullName.ToLower().Trim().IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        result = Visibility.Visible;
                    }
                    else if (!string.IsNullOrWhiteSpace(model.MobilePhoneDialingCode)
                        && !string.IsNullOrWhiteSpace(model.MobilePhone)
                        && (model.MobilePhoneDialingCode.Trim() + " " + model.MobilePhone.Trim()).IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        result = Visibility.Visible;
                    }
                    else if (!string.IsNullOrWhiteSpace(model.Email) && model.Email.Trim().IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        result = Visibility.Visible;
                    }
                    else if (model.ShortInfoModel.UserIdentity > 0 && model.ShortInfoModel.UserIdentity.ToString().Trim().IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        result = Visibility.Visible;
                    }
                    else if (!string.IsNullOrEmpty(model.HomeCity) && !string.IsNullOrEmpty(model.CurrentCity))
                    {
                        string location = model.HomeCity.ToLower().Trim() + model.CurrentCity.ToLower().Trim();

                        if (location.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            result = Visibility.Visible;
                        }
                    }
                }
                else
                {
                    result = Visibility.Visible;
                }

                if (result == Visibility.Visible)
                {
                    IsNoSearchResultsFound = false;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("UpdateVisibility ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return result;
        }

        public void OnRemoveButtonClicked(object parameter)
        {
            UserBasicInfoModel model = (UserBasicInfoModel)parameter;
            string msg = string.Format(NotificationMessages.REMOVE_FROM_SUGGESTION, model.ShortInfoModel.FullName);
            //MessageBoxResult result = CustomMessageBox.ShowQuestion(msg);
            //if (result == MessageBoxResult.Yes)
            //{
            bool isTrue = UIHelperMethods.ShowQuestion(msg, "Remove confirmation");
            if (isTrue)
            {
                RemoveFromSuggesion(model, false);
            }
        }

        public void OnAddFriendButtonClicked(object parameter)
        {

            UserBasicInfoModel model = (UserBasicInfoModel)parameter;
            BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(model.ShortInfoModel.UserTableID);

            int accessType = -1;
            if (!HelperMethods.HasFriendChatPermission(model, blockModel, out accessType))
            {
                if (HelperMethods.ShowBlockWarning(model, accessType))
                {
                    return;
                }
            }
            HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, model, blockModel, (status) =>
            {
                if (status == false) return 0;
                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                new ThrdAddFriend().StartProcess(model, true, (complete) =>
                {
                    if (complete)
                    {
                        if ((RingIDViewModel.Instance.IsNeedToShowInPeopleYouMayKnow && RingIDViewModel.Instance.SuggestionFriendList.Count <= 10) || (RingIDViewModel.Instance.IsNeedToShowInPeopleYouMayKnow == false && RingIDViewModel.Instance.SuggestionFriendList.Count <= 5))
                        {
                            this.FetchTenSuggestionDetails();
                        }
                    }
                    return 0;
                });
                return 1;
            }, true);
        }

        public void OpenPopupExecute(object parameter)
        {
            Button control = (Button)parameter;
            UserBasicInfoModel model = (UserBasicInfoModel)control.Tag;
            if (model.NumberOfMutualFriends > 0)
            {
                if (UCMutualFriendsPopup.Instance != null)
                    mainPanel.Children.Remove(UCMutualFriendsPopup.Instance);
                UCMutualFriendsPopup.Instance = new UCMutualFriendsPopup();
                UCMutualFriendsPopup.Instance.SetParent(mainPanel);
                UCMutualFriendsPopup.Instance.Show();
                UCMutualFriendsPopup.Instance.ShowPopup(control, model.ShortInfoModel.UserTableID, model.ShortInfoModel.FullName);
                UCMutualFriendsPopup.Instance.OnRemovedUserControl += () =>
                {
                    UCMutualFriendsPopup.Instance.MutualList.Clear();
                    UCMutualFriendsPopup.Instance = null;
                };
                //   new MutualFriendsRequest(model.UserIdentity);
                SendDataToServer.SendMutualFreindRequest(model.ShortInfoModel.UserTableID);
            }
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Loaded_Unloaded Events"
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using System.Timers;
using View.UI.PopUp;

namespace View.UI.AddFriend
{
    /// <summary>
    /// Interaction logic for UCAddFriendPending.xaml
    /// </summary>
    public partial class UCAddFriendPending : UserControl, INotifyPropertyChanged
    {
        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(AddRemoveInCollections).Name);
        private int threadMaxRunningTime = 0;
        private string prevSerchString = null;
        private Timer searchTextBoxTimer;
        private System.Threading.Thread searchThread;
        private ThrdLoadIncommingListInView thrdLoadIncommingListInView;
        private ThrdLoadPendingListInView thrdLoadPendingListInView;
        #endregion "Private Fields"

        #region "Public Fields"
        public static UCAddFriendPending Instance;
        #endregion "Public Fields"

        #region "Constructors"
        public UCAddFriendPending()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            this.IsVisibleChanged += UCAddFriendPending_IsVisibleChanged;
            PendingListIncoming = new ObservableCollection<UserBasicInfoModel>();
            PendingListOutgoing = new ObservableCollection<UserBasicInfoModel>();
            SearchPendingList = new ObservableCollection<UserBasicInfoModel>();
        }
        #endregion "Constructors"

        #region "Properties"

        private int outgoingRequestCount = 0;
        public int OutgoingRequestCount
        {
            get { return outgoingRequestCount; }
            set
            {
                if (value == outgoingRequestCount) return;
                outgoingRequestCount = value; this.OnPropertyChanged("OutgoingRequestCount");
            }
        }

        private int incomingRequestCount = 0;
        public int IncomingRequestCount
        {
            get { return incomingRequestCount; }
            set
            {
                if (value == incomingRequestCount) return;
                incomingRequestCount = value; this.OnPropertyChanged("IncomingRequestCount");
            }
        }

        public long SelectedFriendUserTableId { get; set; }

        private ObservableCollection<UserBasicInfoModel> pendingListIncoming;
        public ObservableCollection<UserBasicInfoModel> PendingListIncoming
        {
            get { return pendingListIncoming; }
            set
            {
                pendingListIncoming = value;
                pendingListIncoming.CollectionChanged += PendingListIncoming_CollectionChanged;
                this.OnPropertyChanged("PendingListIncoming");
            }
        }

        private ObservableCollection<UserBasicInfoModel> pendingListOutgoing;
        public ObservableCollection<UserBasicInfoModel> PendingListOutgoing
        {
            get { return pendingListOutgoing; }
            set
            {
                pendingListOutgoing = value;
                pendingListOutgoing.CollectionChanged += PendingListOutgoing_CollectionChanged;
                this.OnPropertyChanged("PendingListOutgoing");
            }
        }

        private ObservableCollection<UserBasicInfoModel> searchPendingList;
        public ObservableCollection<UserBasicInfoModel> SearchPendingList
        {
            get { return searchPendingList; }
            set { searchPendingList = value; this.OnPropertyChanged("SearchPendingList"); }
        }

        private string searchString;
        public string SearchString
        {
            get { return searchString; }
            set
            {
                IsSearching = true;
                searchString = value.ToLower().Trim();
                prevSerchString = null;
                SearchPendingList.Clear();
                IsNoResultsFound = false;

                if (!string.IsNullOrWhiteSpace(searchString)) onSearch();
                else
                {
                    Scroll.ScrollToVerticalOffset(0);
                    IsSearching = false;
                    SearchPendingList.Clear();
                }
            }
        }

        private ImageSource loader;
        public ImageSource Loader
        {
            get { return loader; }
            set
            {
                if (value == loader) return;
                loader = value; OnPropertyChanged("Loader");
            }
        }

        private bool isSearching = false;
        public bool IsSearching
        {
            get { return isSearching; }
            set
            {
                if (value == isSearching) return;
                isSearching = value; OnPropertyChanged("IsSearching");
            }
        }

        private bool isNoResultsFound = false;
        public bool IsNoResultsFound
        {
            get { return isNoResultsFound; }
            set
            {
                if (value == isNoResultsFound) return;
                isNoResultsFound = value; OnPropertyChanged("IsNoResultsFound");
            }
        }

        private Visibility isNoIncomingVisibility = Visibility.Visible;
        public Visibility IsNoIncomingVisibility
        {
            get { return isNoIncomingVisibility; }
            set
            {
                if (value == isNoIncomingVisibility) return;
                isNoIncomingVisibility = value;
                this.OnPropertyChanged("IsNoIncomingVisibility");
            }
        }

        private Visibility isNoOutgoingVisibility = Visibility.Visible;
        public Visibility IsNoOutgoingVisibility
        {
            get { return isNoOutgoingVisibility; }
            set
            {
                if (value == isNoOutgoingVisibility) return;
                isNoOutgoingVisibility = value;
                this.OnPropertyChanged("IsNoOutgoingVisibility");
            }
        }

        #endregion "Properties"

        #region"Commands and Methods"

        private ICommand openPopupCommand;
        public ICommand OpenPopupCommand
        {
            get
            {
                if (openPopupCommand == null) openPopupCommand = new RelayCommand(param => openPopupExecute(param));
                return openPopupCommand;
            }
            set { openPopupCommand = value; }
        }
        private void openPopupExecute(object parameter)
        {
            Button control = (Button)parameter;
            UserBasicInfoModel model = (UserBasicInfoModel)control.Tag;
            if (model.NumberOfMutualFriends > 0)
            {
                if (UCMutualFriendsPopup.Instance != null) mainPanel.Children.Remove(UCMutualFriendsPopup.Instance);
                UCMutualFriendsPopup.Instance = new UCMutualFriendsPopup();
                UCMutualFriendsPopup.Instance.SetParent(mainPanel);
                UCMutualFriendsPopup.Instance.Show();
                UCMutualFriendsPopup.Instance.ShowPopup(control, model.ShortInfoModel.UserTableID, model.ShortInfoModel.FullName);
                UCMutualFriendsPopup.Instance.OnRemovedUserControl += () =>
                {
                    UCMutualFriendsPopup.Instance.MutualList.Clear();
                    UCMutualFriendsPopup.Instance = null;
                };
                SendDataToServer.SendMutualFreindRequest(model.ShortInfoModel.UserTableID);
            }
        }

        private ICommand commonExpandCommand;
        public ICommand CommonExpandCommand
        {
            get
            {
                if (commonExpandCommand == null) commonExpandCommand = new RelayCommand(param => OnExpandViewClicked(param));
                return commonExpandCommand;
            }
        }
        private void OnExpandViewClicked(object parameter)
        {
            showDefaultOrExpandView((UserBasicInfoModel)parameter);
        }

        private ICommand addFriendButtonCommand;
        public ICommand AddFriendButtonCommand
        {
            get
            {
                if (addFriendButtonCommand == null) addFriendButtonCommand = new RelayCommand(param => ActionEmpty(param));
                return addFriendButtonCommand;
            }
        }
        private void ActionEmpty(object param)
        {
        }

        #endregion //"Commands and methods"

        #region "Event Trigger"

        void PendingListOutgoing_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                OutgoingRequestCount = FriendListController.Instance.FriendDataContainer.PendingListcount();
                if (OutgoingRequestCount > 0) IsNoOutgoingVisibility = Visibility.Collapsed;
                else IsNoOutgoingVisibility = Visibility.Visible;
            }
        }

        void UCAddFriendPending_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                searchTextBoxTimer = new Timer();
                UCMiddlePanelSwitcher.View_UCAddFriendMainPanel.LoadPendingData();
            }
            else
            {
                IsSearching = false;
                searchTextBoxTimer = null;
                SearchTermTextBox.Text = string.Empty;
                UCMiddlePanelSwitcher.View_UCAddFriendMainPanel.isLoaded = false;
                DispatcherTimer _RemoveMembers = new DispatcherTimer();
                _RemoveMembers.Interval = TimeSpan.FromMilliseconds(100);
                _RemoveMembers.Tick += (s, ex) =>
                {
                    lock (PendingListIncoming)
                    {
                        foreach (var x in PendingListIncoming.ToList())
                        {
                            PendingListIncoming.Remove(x);
                            GC.SuppressFinalize(x);
                        }
                    }
                    lock (PendingListOutgoing)
                    {
                        foreach (var x in PendingListOutgoing.ToList())
                        {
                            PendingListOutgoing.Remove(x);
                            GC.SuppressFinalize(x);
                        }
                    }
                    _RemoveMembers.Stop();
                };
                _RemoveMembers.Stop();
                _RemoveMembers.Start();
            }
        }

        private void PendingListIncoming_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                IncomingRequestCount = FriendListController.Instance.FriendDataContainer.IncommingListCount();
                UCMiddlePanelSwitcher.View_UCAddFriendMainPanel.IncomingRequestCount = IncomingRequestCount;
                UCMiddlePanelSwitcher.View_UCAddFriendMainPanel.IncomingRequestCounterSize = HelperMethods.CalculateCircleSizeOfAddFriendNotifactionCounter(IncomingRequestCount);

                if (IncomingRequestCount == 0)
                {
                    IsNoIncomingVisibility = Visibility.Visible;
                }
                else
                {
                    IsNoIncomingVisibility = Visibility.Collapsed;
                }
            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void showMoreIncomingButton_Click(object sender, RoutedEventArgs e)
        {
            if (thrdLoadIncommingListInView == null) thrdLoadIncommingListInView = new ThrdLoadIncommingListInView();
            thrdLoadIncommingListInView.StartThread(10);
        }

        private void showMoreOutgoingButton_Click(object sender, RoutedEventArgs e)
        {
            //FriendListController.Instance.StartThrdLoadPendingListInView();
            if (thrdLoadPendingListInView == null) thrdLoadPendingListInView = new ThrdLoadPendingListInView();
            thrdLoadPendingListInView.StartThread(10);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Control control = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;

            if (SelectedFriendUserTableId.Equals(model.ShortInfoModel.UserTableID))
            {
                SelectedFriendUserTableId = 0;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
            }
            else
            {
                long tempUTID = SelectedFriendUserTableId;
                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");

                UserBasicInfoModel tempModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(tempUTID);
                if (tempModel != null) tempModel.ShortInfoModel.OnPropertyChanged("UserTableID");
            }
        }

        private void Control_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Control control = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            model.IncomingStatus = StatusConstants.INCOMING_REQUEST_READ;
            UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(model.ShortInfoModel.UserTableID);
            if (dto != null)
            {
                dto.IncomingStatus = StatusConstants.INCOMING_REQUEST_READ;
                List<UserBasicInfoDTO> basicInfoDTOs = new List<UserBasicInfoDTO>();
                basicInfoDTOs.Add(dto);
                new InsertIntoUserBasicInfoTable(basicInfoDTOs).Start();
            }
            showDefaultOrExpandView(model);
        }

        private void SearchTextBoxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(searchString) && !(prevSerchString != null && prevSerchString.ToLower().Trim().Equals(searchString)))
            {
                if (!(searchThread != null && searchThread.IsAlive))
                {
                    IsSearching = true;
                    searchThread = new System.Threading.Thread(run);
                    searchThread.Start();
                }
            }

            threadMaxRunningTime += 200;
            if (threadMaxRunningTime >= 3000)
            {
                threadMaxRunningTime = 0;
                if (searchTextBoxTimer != null)
                {
                    searchTextBoxTimer.Enabled = false;
                    searchTextBoxTimer.Stop();
                }
            }
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.Text = string.Empty;
        }

        #endregion "Event Trigger"

        #region "Private methods"

        private void onSearch()
        {
            try
            {
                threadMaxRunningTime = 0;
                searchTextBoxTimer.Elapsed -= SearchTextBoxTimer_Elapsed;
                searchTextBoxTimer.Elapsed += SearchTextBoxTimer_Elapsed;
                searchTextBoxTimer.Interval = 200;
                searchTextBoxTimer.AutoReset = true;
                searchTextBoxTimer.Enabled = true;
                searchTextBoxTimer.Stop();
                searchTextBoxTimer.Start();
            }
            catch (Exception ex) { log.Error(ex.StackTrace + " " + ex.Message); }
        }

        private void run()
        {
            prevSerchString = searchString;
            clearSearchList();

            List<UserBasicInfoDTO> tempList = new List<UserBasicInfoDTO>();
            tempList = FriendListController.Instance.FriendDataContainer.IncomingList.Where(P => P.FullName.ToLower().Trim().Contains(SearchString) || P.RingID.ToString().Contains(SearchString)).ToList();
            foreach (UserBasicInfoDTO userDTO in tempList)
            {
                UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                if (model != null && !SearchPendingList.Contains(model))
                {
                    SearchPendingList.InvokeAdd(model);
                    System.Threading.Thread.Sleep(10);
                }
                if (!(prevSerchString != null && prevSerchString.ToLower().Trim().Equals(searchString)))
                {
                    clearSearchList();
                    break;
                }
            }

            tempList = FriendListController.Instance.FriendDataContainer.PendingList.Where(P => P.FullName.ToLower().Trim().Contains(SearchString) || P.RingID.ToString().Contains(SearchString)).ToList();
            foreach (UserBasicInfoDTO userDTO in tempList)
            {
                UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                if (model != null && !SearchPendingList.Contains(model))
                {
                    SearchPendingList.InvokeAdd(model);
                    System.Threading.Thread.Sleep(10);
                }
                if (!(prevSerchString != null && prevSerchString.ToLower().Trim().Equals(searchString)))
                {
                    clearSearchList();
                    break;
                }
            }
            if (SearchPendingList.Count == 0) IsNoResultsFound = true;
        }

        private void clearSearchList()
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                SearchPendingList.Clear();
            }, DispatcherPriority.Send);
        }

        private void showDefaultOrExpandView(UserBasicInfoModel model)
        {
            if (SelectedFriendUserTableId.Equals(model.ShortInfoModel.UserTableID))
            {
                SelectedFriendUserTableId = 0;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
            }
            else
            {
                long tempUTID = SelectedFriendUserTableId;
                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
                UserBasicInfoModel tempModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(tempUTID);
                if (tempModel != null) tempModel.ShortInfoModel.OnPropertyChanged("UserTableID");
            }
        }

        #endregion "Private methods"

        #region "Public Methods"

        public void InsertUIInPending(UserBasicInfoModel userModel, bool IsIncoming)
        {
            if (IsIncoming)
            {
                lock (PendingListIncoming)
                {
                    if (!PendingListIncoming.Any(P => P.ShortInfoModel.UserTableID == userModel.ShortInfoModel.UserTableID))
                    {
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            PendingListIncoming.Insert(0, userModel);
                        }, System.Windows.Threading.DispatcherPriority.Send);
                    }
                }
            }
            else
            {
                lock (PendingListOutgoing)
                {
                    if (!PendingListOutgoing.Any(P => P.ShortInfoModel.UserTableID == userModel.ShortInfoModel.UserTableID))
                    {
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            PendingListOutgoing.Insert(0, userModel);
                        }, System.Windows.Threading.DispatcherPriority.Send);
                    }
                }
            }
        }

        public void HideShowMorePanel(bool isIncoimg)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (isIncoimg) { showMoreIncomingPanel.Visibility = Visibility.Collapsed; }
                else { showMoreOutgoingPanel.Visibility = Visibility.Collapsed; }
            });
        }

        public void ShowMorePanel(bool isIncoimg)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (isIncoimg) { showMoreIncomingPanel.Visibility = Visibility.Visible; }
                else { showMoreOutgoingPanel.Visibility = Visibility.Visible; }
            });
        }

        #region "IncomingRequestFriendList"
        public void AddIntoIncomingRequestFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null)
            {
                try
                {
                    int max = PendingListIncoming.Count;
                    int min = 0, pivot;
                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (userbasicinforModel.UpdateTime < PendingListIncoming.ElementAt(pivot).UpdateTime) min = pivot + 1;
                        else max = pivot;
                    }
                    PendingListIncoming.InvokeInsert(min, userbasicinforModel);
                }
                catch (Exception) { PendingListIncoming.InvokeAdd(userbasicinforModel); }
            }
            else log.Debug("IncomingRequestFriendList model=null*************");
        }

        public void RemoveFromIncomingRequestFriendList(UserBasicInfoModel userbasicinforModel)
        {
            PendingListIncoming.InvokeRemove(userbasicinforModel);
        }

        public void InvokeInsertIncomingRequestFriendList(int index, UserBasicInfoModel userbasicinforModel)
        {
            PendingListIncoming.InvokeInsert(index, userbasicinforModel);
        }

        #endregion "IncomingRequestFriendList"

        #region "OutgoingRequestFriendList"
        public void AddIntoOutgoingRequestFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null)
            {
                try
                {
                    int max = PendingListOutgoing.Count;
                    int min = 0, pivot;
                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (userbasicinforModel.UpdateTime < PendingListOutgoing.ElementAt(pivot).UpdateTime) min = pivot + 1;
                        else max = pivot;
                    }
                    PendingListOutgoing.InvokeInsert(min, userbasicinforModel);
                }
                catch (Exception) { PendingListOutgoing.InvokeAdd(userbasicinforModel); }
            }
            else log.Debug("OutgoingRequestFriendList model=null*************");
        }

        public void RemoveFromOutgoingRequestFriendList(UserBasicInfoModel userbasicinforModel)
        {
            PendingListOutgoing.InvokeRemove(userbasicinforModel);
        }

        public void InvokeInsertIPendingListOutgoing(int index, UserBasicInfoModel userbasicinforModel)
        {
            PendingListOutgoing.InvokeInsert(index, userbasicinforModel);
        }
        #endregion "OutgoingRequestFriendList"

        public void ShowIncommingAndPendingCounts()
        {
            this.OutgoingRequestCount = FriendListController.Instance.FriendDataContainer.PendingListcount();
            this.IncomingRequestCount = FriendListController.Instance.FriendDataContainer.IncommingListCount();
        }

        #endregion "Public Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}

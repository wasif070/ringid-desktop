﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Utility;
using View.Utility.Chat.Service;
using View.Utility.FriendProfile;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.AddFriend
{
    /// <summary>
    /// Interaction logic for UCBlockedContacts.xaml
    /// </summary>
    public partial class UCBlockedContacts : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCBlockedContacts).Name);        

        public UCBlockedContacts()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Unloaded += UCBlockedContacts_Unloaded;
            LoadData();
        }

        #region Property

        private bool _NoBlockedUserVisibility = false;
        public bool NoBlockedUserVisibility
        {
            get { return _NoBlockedUserVisibility; }
            set
            {
                _NoBlockedUserVisibility = value;
                OnPropertyChanged("NoBlockedUserVisibility");
            }
        }

        private bool _IsNoContactsFound = false;
        public bool IsNoContactsFound
        {
            get
            {
                return _IsNoContactsFound;
            }
            set
            {
                if (_IsNoContactsFound == value) return;
                _IsNoContactsFound = value;
                OnPropertyChanged("IsNoContactsFound");
            }
        }

        #endregion Property

        #region Utility Methods

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadData()
        {
            new Thread(() =>
            {
                List<UserBasicInfoDTO> listBC = ContactListDAO.FetchBlockedContactList();
                if ((listBC.Count + RingIDViewModel.Instance.BlockedContactList.Count) == 0)
                {
                    NoBlockedUserVisibility = true;
                }
                foreach (var user in listBC)
                {
                    UserBasicInfoModel model = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(user.UserTableID, user.RingID, user.FullName, user.ProfileImage);
                    if (!RingIDViewModel.Instance.BlockedContactList.Any(u => u.ShortInfoModel.UserTableID == user.UserTableID))
                    {
                        Thread.Sleep(5);
                        RingIDViewModel.Instance.BlockedContactList.InvokeAdd(model);
                    }
                }
            }).Start();
        }        

        private void OnAccessChangeClicked(object param)
        {
            try
            {
                UserBasicInfoModel friendInfoModel = ((UserBasicInfoModel)param);
                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(friendInfoModel.ShortInfoModel.UserTableID);

                friendInfoModel.VisibilityModel.ShowLoading = Visibility.Visible;
                HelperMethods.ChangeBlockUnblockSettingsWrapper(StatusConstants.FULL_ACCESS, friendInfoModel, blockModel, (status) =>
                {
                    if (status)
                    {
                        RingIDViewModel.Instance.BlockedContactList.InvokeRemove(friendInfoModel);
                    }
                    friendInfoModel.VisibilityModel.ShowLoading = Visibility.Collapsed;

                    return 0;
                }, true);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnAccessChangeClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void AddRemoveEffectIntoBlockedContactUI(UserBasicInfoModel BasicInfoModel)
        {
            try
            {
                if (BasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                {
                    if (BasicInfoModel.CallAccess == StatusConstants.TYPE_ACCESS_BLOCKED && BasicInfoModel.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED && BasicInfoModel.FeedAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
                    {
                        RingIDViewModel.Instance.BlockedContactList.InvokeAdd(BasicInfoModel);
                    }
                    else
                    {
                        var model = RingIDViewModel.Instance.BlockedContactList.FirstOrDefault(u => u.ShortInfoModel.UserTableID == BasicInfoModel.ShortInfoModel.UserTableID);
                        if (model != null)
                        {
                            RingIDViewModel.Instance.BlockedContactList.InvokeRemove(model);
                        }
                    }
                }
                else if (BasicInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                {
                    BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(BasicInfoModel.ShortInfoModel.UserTableID);
                    if (blockModel.IsBlockedByMe)
                    {
                        RingIDViewModel.Instance.BlockedContactList.InvokeAdd(BasicInfoModel);
                    }
                    else
                    {
                        var model = RingIDViewModel.Instance.BlockedContactList.FirstOrDefault(u => u.ShortInfoModel.UserTableID == BasicInfoModel.ShortInfoModel.UserTableID);
                        if (model != null)
                        {
                            RingIDViewModel.Instance.BlockedContactList.InvokeRemove(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error at AddRemoveEffectIntoBlockedContactUI() => " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        #endregion

        #region EventHandler

        private void UCBlockedContacts_Unloaded(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.Text = string.Empty;
            IsNoContactsFound = false;
        }

        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string text = SearchTermTextBox.Text.Trim().ToLower();
            if (!string.IsNullOrWhiteSpace(text) && RingIDViewModel.Instance.BlockedContactList.Count > 0)
            {
                if (RingIDViewModel.Instance.BlockedContactList.Any(P => (P.ShortInfoModel.FullName.Trim().ToLower().Contains(text) || P.ShortInfoModel.UserIdentity.ToString().Contains(text))))
                {
                    IsNoContactsFound = false;
                }
                else
                {
                    IsNoContactsFound = true;
                }
            }
            else
            {
                IsNoContactsFound = false;
            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.Text = string.Empty;
        }
        #endregion

        #region Command

        private ICommand _AccessChangeCommand;
        public ICommand AccessChangeCommand
        {
            get
            {
                if (_AccessChangeCommand == null)
                {
                    _AccessChangeCommand = new RelayCommand(param => OnAccessChangeClicked(param));
                }
                return _AccessChangeCommand;
            }
        }

        #endregion
    }
}

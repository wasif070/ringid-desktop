﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.ViewModel;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for WNChatPopupMessage.xaml
    /// </summary>
    public partial class WNChatPopupMessage : Window, INotifyPropertyChanged
    {
        private ICommand _NotifyWindowCloseCommand = null;
        private ICommand _SingleNotifyMessageClickCommand = null;
        private ObservableCollection<RecentModel> _NotifyPopUpMessageList;
        private DispatcherTimer stayOpenTimer = null;
        private Storyboard storyboard;
        private DoubleAnimation animation;
        private double hiddenTop;
        private double openedTop;
        private EventHandler arrivedHidden;
        private EventHandler arrivedOpened;
        private static WNChatPopupMessage Instance;
        private object locker = new object();
        private long VisualizeTime = 3000;
        private double SinglePanelHeight = 80;
        private double TopPanelHeight = 25;
        private int IntervalOfTimer = 500;
        private double _WindowHeight;
        private enum DisplayStates
        {
            Opening,
            Opened,
            Hiding,
            Hidden,
            Close
        }

        public WNChatPopupMessage()
        {
            InitializeComponent();
            DataContext = this;
            WindowHeight = (SinglePanelHeight + TopPanelHeight + 10);
            Init();
        }

        #region PropertyChanged

        public ObservableCollection<RecentModel> NotifyPopUpMessageList
        {
            get
            {
                if (_NotifyPopUpMessageList == null)
                {
                    _NotifyPopUpMessageList = new ObservableCollection<RecentModel>();
                }
                return _NotifyPopUpMessageList;
            }
            set
            {
                if (_NotifyPopUpMessageList == value) return;
                _NotifyPopUpMessageList = value;
                OnPropertyChanged("NotifyPopUpMessageList");
            }
        }

        public double WindowHeight
        {
            get
            {
                return _WindowHeight;
            }
            set
            {
                if (_WindowHeight == value) return;
                _WindowHeight = value;
                OnPropertyChanged("WindowHeight");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion


        #region Utility

        public static void ShowNotification(RecentDTO recentDTO)
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (Instance == null)
                {
                    Instance = new WNChatPopupMessage();
                    //Instance.WindowState = WindowState.Minimized;
                    Instance.Show();
                    Instance.ChatPopUpMessage.IsOpen = true;
                }
                Instance.LoadChatPopupMessage(recentDTO);
            }, DispatcherPriority.ApplicationIdle);
        }

        public void LoadChatPopupMessage(RecentDTO recentDTO)
        {
            try
            {
                RecentModel recentModel = null;
                bool isfound = true;
                if (recentDTO.Message.FriendTableID > 0)
                {
                    recentModel = NotifyPopUpMessageList.Where(P => (P.FriendInfoModel != null ? P.FriendInfoModel.UserTableID : 0) == recentDTO.Message.FriendTableID).FirstOrDefault();
                }
                if (recentModel == null)
                {
                    if (recentDTO.Message.GroupID > 0)
                    {
                        recentModel = NotifyPopUpMessageList.Where(P => (P.GroupInfoModel != null ? P.GroupInfoModel.GroupID : 0) == recentDTO.Message.GroupID).FirstOrDefault();
                    }
                    if (recentModel == null)
                    {
                        isfound = false;
                        recentModel = new RecentModel();
                    }
                }
                recentModel.LoadData(recentDTO);
                recentModel.PopupLifeTime = VisualizeTime;
                if (recentModel.Message.GroupID > 0)
                {
                    recentModel.GroupInfoModel = RingIDViewModel.Instance.GetGroupInfoModelByGroupID(recentDTO.Message.GroupID);
                }
                else
                {
                    recentModel.FriendInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(recentDTO.Message.FriendTableID);
                }
                if (!isfound)
                {
                    NotifyPopUpMessageList.InvokeInsert(0, recentModel);
                    WindowHeight = (NotifyPopUpMessageList.Count * SinglePanelHeight) + TopPanelHeight + 10;
                    SetInitialLocations(true, WindowHeight);
                }
                Notify();
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(WNChatPopupMessage).Name).Error(ex.StackTrace + " " + ex.Message);
            }
        }

        private int openingMilliseconds = 1000;
        /// <summary>
        /// The time the TaskbarNotifier window should take to open in milliseconds.
        /// </summary>
        public int OpeningMilliseconds
        {
            get { return this.openingMilliseconds; }
            set
            {
                this.openingMilliseconds = value;
                this.OnPropertyChanged("OpeningMilliseconds");
            }
        }

        private int hidingMilliseconds = 500;
        /// <summary>
        /// The time the TaskbarNotifier window should take to hide in milliseconds.
        /// </summary>
        public int HidingMilliseconds
        {
            get { return this.hidingMilliseconds; }
            set
            {
                this.hidingMilliseconds = value;
                this.OnPropertyChanged("HidingMilliseconds");
            }
        }


        private int leftOffset = 20;
        /// <summary>
        /// The space, if any, between the left side of the TaskNotifer window and the right side of the screen.
        /// </summary>
        public int LeftOffset
        {
            get { return this.leftOffset; }
            set
            {
                this.leftOffset = value;
                this.OnPropertyChanged("LeftOffset");
            }
        }

        private DisplayStates displayState;
        /// <summary>
        /// The current DisplayState
        /// </summary>
        private DisplayStates DisplayState
        {
            get
            {
                return this.displayState;
            }
            set
            {
                if (value != this.displayState)
                {
                    this.displayState = value;

                    // Handle the new state.
                    this.OnDisplayStateChanged();
                }
            }
        }

        private void SetInitialLocations(bool showOpened, double winHeight)
        {
            // Determine screen working area.
            System.Drawing.Rectangle workingArea = new System.Drawing.Rectangle((int)this.Left, (int)this.Top, (int)this.ActualWidth, (int)this.ActualHeight);
            workingArea = Screen.GetWorkingArea(workingArea);

            // Initialize the window location to the bottom right corner.
            this.Left = workingArea.Right - this.ActualWidth - this.leftOffset;

            // Set the opened and hidden locations.
            this.hiddenTop = workingArea.Bottom;
            this.openedTop = workingArea.Bottom - winHeight - 20;

            // Set Top based on whether opened or hidden is desired
            if (showOpened)
                this.Top = openedTop;
            else
                this.Top = hiddenTop;
        }

        private void BringToTop()
        {
            // Bring this window to the top without making it active.
            try
            {
                Instance.ChatPopUpMessage.IsOpen = true;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(WNChatPopupMessage).Name).Error(ex.StackTrace + " " + ex.Message);
            }
        }

        private void OnDisplayStateChanged()
        {
            try
            {
                // The display state has changed.

                // Unless the stortboard as already been created, nothing can be done yet.

                if (this.storyboard == null)
                    return;

                // Stop the current animation.
                if (this.displayState != DisplayStates.Opening)
                {
                    this.storyboard.Stop(this);
                }

                // Since the storyboard is reused for opening and closing, both possible
                // completed event handlers need to be removed.  It is not a problem if
                // either of them was not previously set.
                this.storyboard.Completed -= arrivedHidden;
                this.storyboard.Completed -= arrivedOpened;

                if (this.displayState == DisplayStates.Close)
                {
                    return;
                }
                if (this.displayState != DisplayStates.Hidden)
                {
                    // Unless the window has just arrived at the hidden state, it must be
                    // moving, and should be shown.
                    this.BringToTop();
                }

                if (this.displayState == DisplayStates.Opened)
                {
                    // The window has just arrived at the opened state.

                    // Because the inital settings of this TaskNotifier depend on the screen's working area,
                    // it is best to reset these occasionally in case the screen size has been adjusted.
                    this.SetInitialLocations(true, WindowHeight);

                    if (!ChatPopUpMessage.IsMouseOver)
                    {
                        // The mouse is not within the window, so start the countdown to hide it.
                        this.stayOpenTimer.Stop();
                        this.stayOpenTimer.Start();
                    }
                }
                else if (this.displayState == DisplayStates.Opening)
                {

                    this.BringToTop();

                    // Because the window may already be partially open, the rate at which
                    // it opens may be a fraction of the normal rate.
                    // This must be calculated.


                    // Reconfigure the animation.
                    this.animation.To = this.openedTop;
                    this.animation.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 1000));

                    // Set the specific completed event handler.
                    this.storyboard.Completed += arrivedOpened;

                    // Start the animation.
                    this.storyboard.Begin(this, true);

                    int milliseconds = this.CalculateMillseconds(this.openingMilliseconds, this.openedTop);

                    if (milliseconds < 1)
                    {
                        // This window must already be open.
                        this.DisplayState = DisplayStates.Opened;
                        return;
                    }
                }
                else if (this.displayState == DisplayStates.Hiding)
                {
                    // The window should start hiding.

                    // Because the window may already be partially hidden, the rate at which
                    // it hides may be a fraction of the normal rate.
                    // This must be calculated.
                    int milliseconds = this.CalculateMillseconds(this.hidingMilliseconds, this.hiddenTop);

                    if (milliseconds < 1)
                    {
                        // This window must already be hidden.
                        this.DisplayState = DisplayStates.Hidden;
                        return;
                    }

                    // Reconfigure the animation.
                    this.animation.To = this.hiddenTop;
                    this.animation.Duration = new Duration(new TimeSpan(0, 0, 0, 0, milliseconds));

                    // Set the specific completed event handler.
                    this.storyboard.Completed += arrivedHidden;

                    // Start the animation.
                    this.storyboard.Begin(this, true);
                }
                else if (this.displayState == DisplayStates.Hidden)
                {
                    // Ensure the window is in the hidden position.
                    // Hide the window.
                    Instance.ChatPopUpMessage.IsOpen = false;
                    NotifyPopUpMessageList.Clear();
                    Instance.Close();
                    Instance = null;
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(WNChatPopupMessage).Name).Error(ex.StackTrace + " " + ex.Message);
            }
        }

        private int CalculateMillseconds(int totalMillsecondsNormally, double destination)
        {
            if (this.Top == destination)
            {
                // The window is already at its destination.  Nothing to do.
                return 0;
            }

            double distanceRemaining = Math.Abs(this.Top - destination);
            double percentDone = distanceRemaining / this.ActualHeight;

            // Determine the percentage of normal milliseconds that are actually required.
            return (int)(totalMillsecondsNormally * percentDone);
        }

        protected virtual void Storyboard_ArrivedHidden(object sender, EventArgs e)
        {
            // Setting the display state will result in any needed actions.
            this.DisplayState = DisplayStates.Hidden;
        }

        protected virtual void Storyboard_ArrivedOpened(object sender, EventArgs e)
        {
            // Setting the display state will result in any needed actions.
            this.DisplayState = DisplayStates.Opened;
        }

        private void stayOpenTimer_Elapsed(Object sender, EventArgs args)
        {
            try
            {
                List<RecentModel> deletelist = new List<RecentModel>();
                List<RecentModel> tempList = NotifyPopUpMessageList.ToList();
                foreach (RecentModel model in tempList)
                {
                    model.PopupLifeTime -= 500;
                    if (model.PopupLifeTime <= 0)
                    {
                        deletelist.Add(model);
                    }
                }
                if (NotifyPopUpMessageList.Count > 1)
                {
                    lock (NotifyPopUpMessageList)
                    {
                        foreach (RecentModel model in deletelist)
                        {
                            NotifyPopUpMessageList.InvokeRemove(model);
                        }
                    }
                }
                if (NotifyPopUpMessageList.Count > 9)
                {
                    WindowHeight = (9 * SinglePanelHeight + TopPanelHeight + 10);
                }
                else
                {
                    WindowHeight = NotifyPopUpMessageList.Count > 1 ? (NotifyPopUpMessageList.Count * SinglePanelHeight + TopPanelHeight + 10) : (SinglePanelHeight + TopPanelHeight + 10);
                }

                SetInitialLocations(true, WindowHeight);
                if (NotifyPopUpMessageList.Count == 1 && deletelist.Count > 0 && NotifyPopUpMessageList.ElementAt(0).PopupLifeTime <= 0)
                {
                    this.stayOpenTimer.Stop();
                    // Only start closing the window if the mouse is not over it.
                    this.DisplayState = DisplayStates.Hiding;
                }
                else if (NotifyPopUpMessageList.Count == 0 && Instance.ChatPopUpMessage.IsOpen)
                {
                    this.stayOpenTimer.Stop();
                    // Only start closing the window if the mouse is not over it.
                    Instance.ChatPopUpMessage.IsOpen = false;
                    Instance.Close();
                    Instance = null;
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(WNChatPopupMessage).Name).Error(ex.StackTrace + " " + ex.Message);
            }
        }

        protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {
            if (this.DisplayState == DisplayStates.Opened)
            {
                // When the user mouses over and the window is already open, it should stay open.
                // Stop the timer that would have otherwise hidden it.
                this.stayOpenTimer.Stop();
            }
            else if ((this.DisplayState == DisplayStates.Hidden) ||
                     (this.DisplayState == DisplayStates.Hiding))
            {
                // When the user mouses over and the window is hidden or hiding, it should open. 
                this.DisplayState = DisplayStates.Opening;
            }

            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
        {
            if (this.DisplayState == DisplayStates.Opened)
            {
                // When the user moves the mouse out of the window, the timer to hide the window
                // should be started.
                this.stayOpenTimer.Stop();
                this.stayOpenTimer.Start();
            }

            base.OnMouseEnter(e);
        }

        private void Notify()
        {
            if (this.DisplayState == DisplayStates.Opened)
            {
                // The window is already open, and should now remain open for another count.
                this.stayOpenTimer.Stop();
                this.stayOpenTimer.Start();
            }
            else
            {
                this.DisplayState = DisplayStates.Opening;
            }
        }

        /// <summary>
        /// Force the window to immediately move to the hidden state.
        /// </summary>
        public void ForceHidden()
        {
            this.DisplayState = DisplayStates.Hidden;
        }
        #endregion

        #region Command
        public ICommand NotifyWindowCloseCommand
        {
            get
            {
                if (_NotifyWindowCloseCommand == null)
                {
                    _NotifyWindowCloseCommand = new RelayCommand((param) => OnNotifyWindowCloseCommand(param));
                }
                return _NotifyWindowCloseCommand;
            }
        }
        public ICommand SingleNotifyMessageClickCommand
        {
            get
            {
                if (_SingleNotifyMessageClickCommand == null)
                {
                    _SingleNotifyMessageClickCommand = new RelayCommand(param => OnSingleNotifyMessageClickCommand(param));
                }
                return _SingleNotifyMessageClickCommand;
            }
        }


        private void OnNotifyWindowCloseCommand(object param)
        {
            try
            {
                stayOpenTimer.Stop();
                stayOpenTimer.IsEnabled = false;
                NotifyPopUpMessageList.Clear();
                base.Close();
                Instance = null;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(WNChatPopupMessage).Name).Error(ex.StackTrace + " " + ex.Message);
            }
        }

        private void OnSingleNotifyMessageClickCommand(object param)
        {
            try
            {
                FocusMainWindow();
                RecentModel model = (RecentModel)param;
                if (model.Message.GroupID > 0)
                {
                    RingIDViewModel.Instance.OnGroupCallChatButtonClicked(model.Message.GroupID);
                }
                else
                {
                    RingIDViewModel.Instance.OnFriendCallChatButtonClicked(model.Message.FriendTableID);
                }
                model.PopupLifeTime = 0;
                stayOpenTimer_Elapsed(null, null);
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(WNChatPopupMessage).Name).Error(ex.StackTrace + " " + ex.Message);
            }
        }

        private void FocusMainWindow()
        {
            if (WNRingIDMain.WinRingIDMain != null) WNRingIDMain.WinRingIDMain.FocusWindow();
            //MainSwitcher.MainController().MainUIController().MainWindow.FocusWindow();
        }

        #endregion

        #region EventHandler

        private void Init()
        {
            // Set initial settings based on the current screen working area.

            try
            {
                this.SetInitialLocations(false, WindowHeight);
                // Start the window in the Hidden state.
                this.DisplayState = DisplayStates.Hidden;

                // Prepare the timer for how long the window should stay open.
                this.stayOpenTimer = new DispatcherTimer();
                this.stayOpenTimer.Interval = TimeSpan.FromMilliseconds(this.IntervalOfTimer);
                this.stayOpenTimer.Tick += new EventHandler(this.stayOpenTimer_Elapsed);

                // Prepare the animation to change the Top property.
                this.animation = new DoubleAnimation();
                Storyboard.SetTargetProperty(this.animation, new PropertyPath(Window.TopProperty));
                this.storyboard = new Storyboard();
                this.storyboard.Children.Add(this.animation);
                this.storyboard.FillBehavior = FillBehavior.Stop;

                // Create the event handlers for when the animation finishes.
                this.arrivedHidden = new EventHandler(this.Storyboard_ArrivedHidden);
                this.arrivedOpened = new EventHandler(this.Storyboard_ArrivedOpened);
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(WNChatPopupMessage).Name).Error(ex.StackTrace + " " + ex.Message);
            }
        }
        #endregion
    }
}

﻿using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.Recent;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using System.Threading;
using View.Utility.Chat;
using imsdkwrapper;
using View.Utility.Chat.Service;
using View.Constants;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatLogPanel.xaml
    /// </summary>
    public partial class UCChatLogPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatLogPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        public static UCChatLogPanel Instance;
        private Thread _Thread = null;
        private Thread _ScrollThread = null;
        private bool _IS_LOADING = false;
        public int _DB_OFFSET = 0;
        private int _LIMIT = 15;
        private bool _IsDeletePanelVisible = false;
        private bool _IsSelectAllMode = true;
        private string _SelectedContactID = String.Empty;
        private bool _IsChatLogEnabled;

        private ICommand _OnSelectContactIDCommand;
        private ICommand _ShowDeletePanelCommand;
        private ICommand _SelectAllCommand;
        private ICommand _ChatDeleteCommand;
        private ICommand _ChatDeleteCancelCommand;

        private DispatcherTimer _ContactListResizeTimer = null;

        public UCChatLogPanel()
        {
            InitializeComponent();
            Instance = this;
            if (UCMiddlePanelSwitcher.View_UCFriendChatCallPanel != null && UCMiddlePanelSwitcher.View_UCFriendChatCallPanel.IsVisible)
            {
                this.SelectedContactID = UCMiddlePanelSwitcher.View_UCFriendChatCallPanel._FriendTableID.ToString();
            }
            else if (UCMiddlePanelSwitcher.View_UCGroupChatCallPanel != null && UCMiddlePanelSwitcher.View_UCGroupChatCallPanel.IsVisible)
            {
                this.SelectedContactID = UCMiddlePanelSwitcher.View_UCGroupChatCallPanel._GroupID.ToString();
            }
            else if (UCMiddlePanelSwitcher.View_UCRoomChatCallPanel != null && UCMiddlePanelSwitcher.View_UCRoomChatCallPanel.IsVisible)
            {
                this.SelectedContactID = UCMiddlePanelSwitcher.View_UCRoomChatCallPanel._RoomID.ToString();
            }

            this.DataContext = this;
        }

        #region Event Handler

        private void itemsControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                this.ChangeFriendOpenStatus();
            }
            catch (Exception ex)
            {
                log.Error("Error: itemsControl_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CheckBox_CheckedUncheck(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox chkBox = (CheckBox)sender;
                if (chkBox.IsChecked != null)
                {
                    if ((bool)chkBox.IsChecked)
                    {
                        IsSelectAllMode = false;
                        for (int i = 0; i < itemsControl.Items.Count; i++)
                        {
                            ContentPresenter child = (ContentPresenter)itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                            RecentModel model = (RecentModel)child.Content;
                            child.ApplyTemplate();

                            Control presenter = (Control)child.ContentTemplate.FindName("container", child);
                            presenter.ApplyTemplate();

                            CustomBorder border = (CustomBorder)presenter.Template.FindName("customBorder", presenter);
                            if (border.Visibility == Visibility.Visible && model.IsChecked == false)
                            {
                                IsSelectAllMode = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        IsSelectAllMode = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CheckBox_CheckedUncheck() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mnuDeleteConversation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RecentModel model = (RecentModel)((Control)sender).DataContext;
                List<RecentModel> modelList = new List<RecentModel>();
                modelList.Add(model);
                DeletChatLog(modelList);
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuDeleteConversation_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mnuChat_Click(object sender, RoutedEventArgs e)
        {
            RecentModel model = (RecentModel)((Control)sender).DataContext;
            ChangeCallChatUI(model);
        }

        private void Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                if (e.VerticalChange > 0 && e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0 && e.VerticalOffset >= (Scroll.ScrollableHeight - 100))
                {
                    this.LoadNextChatLogs();
                }

                if (Math.Abs(e.VerticalChange) > 0)
                {
                    this.ChangeFriendOpenStatus();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Scroll_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void LoadChatLogs()
        {
            try
            {
                this._IS_LOADING = true;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); ;
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

                _Thread = new Thread(() =>
                {
                    Thread.Sleep(200);

                    List<RecentDTO> list = RecentChatCallActivityDAO.LoadRecentChatContactList(0, 0, null, ChatConstants.DAY_365_DAYS, this._DB_OFFSET, this._LIMIT);
                    ChatLogLoadUtility.LoadRecentData(list);
                    this._DB_OFFSET = this._DB_OFFSET + list.Count;

                    if (list.Count < _LIMIT && SettingsConstants.VALUE_RINGID_IS_NO_MORE_CHAT_LOG == false)
                    {
                        GetConversationListFromServer();
                    }
                    else
                    {
                        Thread.Sleep(100);
                        this.IsChatLogEnabled = true;
                        this._IS_LOADING = false;
                        Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                    }
                });
                _Thread.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadChatLogs() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void GetConversationListFromServer()
        {
            ChatService.GetConversationList(ConversationType.ALL, SettingsConstants.VALUE_RINGID_CHAT_LOG_OFFSET, this._LIMIT, 0, (args) =>
            {
                if (args.Status)
                {
                    SettingsConstants.VALUE_RINGID_CHAT_LOG_OFFSET = SettingsConstants.VALUE_RINGID_CHAT_LOG_OFFSET + this._LIMIT;
                    new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_CHAT_LOG_OFFSET, SettingsConstants.VALUE_RINGID_CHAT_LOG_OFFSET.ToString());

                    for (int idx = 0; idx < 2500 && RingIDViewModel.Instance.ChatLogModelsList.Count == 0; idx += 250)
                    {
                        Thread.Sleep(250);
                    }
                }

                this.IsChatLogEnabled = true;
                this._IS_LOADING = false;

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (Scroll.VerticalOffset > 0 && Scroll.VerticalOffset >= (Scroll.ScrollableHeight - 25))
                    {
                        Scroll.ScrollToVerticalOffset(Scroll.VerticalOffset - 25);
                    }
                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                }, DispatcherPriority.Send);
            });
        }

        public void LoadNextChatLogs()
        {
            if (this._IS_LOADING == false && (this._ScrollThread == null || this._ScrollThread.IsAlive == false))
            {
                this._ScrollThread = new Thread(new ThreadStart(LoadChatLogByScroll));
                this._ScrollThread.Start();
            }
        }

        private void LoadChatLogByScroll()
        {
            this._IS_LOADING = true;
            List<RecentDTO> list = RecentChatCallActivityDAO.LoadRecentChatContactList(0, 0, null, ChatConstants.DAY_365_DAYS, this._DB_OFFSET, this._LIMIT);
            this._DB_OFFSET = this._DB_OFFSET + list.Count;
            list = list.Where(P => !RingIDViewModel.Instance.ChatLogModelsList.Any(R => R.ContactID == P.ContactID)).ToList();
            ChatLogLoadUtility.LoadRecentData(list);

            if (list.Count <= 0 && SettingsConstants.VALUE_RINGID_IS_NO_MORE_CHAT_LOG == false)
            {
                this.GetConversationListFromServer();
            }
            else
            {
                this._IS_LOADING = false;
            }
        }

        public bool LoadChatLogByContactID(string contactID)
        {
            bool status = false;
            try
            {
                RecentModel recentModel = RingIDViewModel.Instance.ChatLogModelsList.Where(P => P.ContactID.Equals(contactID)).FirstOrDefault();
                if (recentModel != null)
                {
                    RecentDTO recentDTO = RecentChatCallActivityDAO.LoadRecentChatContactList(recentModel.Message.FriendTableID, recentModel.Message.GroupID, recentModel.Message.RoomID).FirstOrDefault();
                    if (recentDTO != null)
                    {
                        recentDTO.NeedForcelyLoad = true;
                        ChatLogLoadUtility.LoadRecentData(recentDTO);
                    }
                    else
                    {
                        ChatLogLoadUtility.Remove(recentModel);
                        status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadChatLogByContactID() => " + ex.Message + "\n" + ex.StackTrace);
            }
            return status;
        }

        private void OnSelectContactID(object param)
        {
            RecentModel model = (RecentModel)param;
            ChangeCallChatUI(model);
        }

        private void ChangeCallChatUI(RecentModel model)
        {
            try
            {
                if (model.FriendTableID > 0)
                {
                    RingIDViewModel.Instance.OnFriendCallChatButtonClicked(model.FriendTableID);
                }
                else if (model.GroupID > 0)
                {
                    RingIDViewModel.Instance.OnGroupCallChatButtonClicked(model.GroupID);
                }
                else if (!String.IsNullOrWhiteSpace(model.RoomID))
                {
                    RingIDViewModel.Instance.OnRoomCallChatButtonClicked(model.RoomID);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSelectContactID() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeFriendOpenStatus()
        {
            try
            {
                if (_ContactListResizeTimer == null)
                {
                    _ContactListResizeTimer = new DispatcherTimer();
                    _ContactListResizeTimer.Interval = TimeSpan.FromSeconds(1);
                    _ContactListResizeTimer.Tick += (o, e) =>
                    {
                        HelperMethods.ChangeViewPortOpenedProperty(Scroll, itemsControl);
                        _ContactListResizeTimer.Stop();
                        _ContactListResizeTimer.Interval = TimeSpan.FromSeconds(0.2);
                    };
                }

                _ContactListResizeTimer.Stop();
                _ContactListResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnSelectAll(object param)
        {
            try
            {
                IsSelectAllMode = !IsSelectAllMode;

                if (IsSelectAllMode == false)
                {
                    for (int i = 0; i < itemsControl.Items.Count; i++)
                    {
                        ContentPresenter child = (ContentPresenter)itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                        RecentModel model = (RecentModel)child.Content;
                        child.ApplyTemplate();

                        Control presenter = (Control)child.ContentTemplate.FindName("container", child);
                        presenter.ApplyTemplate();

                        CustomBorder border = (CustomBorder)presenter.Template.FindName("customBorder", presenter);
                        if (border.Visibility == Visibility.Visible)
                        {
                            model.IsChecked = !IsSelectAllMode;
                        }
                    }
                }
                else
                {
                    RingIDViewModel.Instance.ChatLogModelsList.ToList().ForEach(P => P.IsChecked = !IsSelectAllMode);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSelectAll() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnShowChatDelete(object param)
        {
            IsDeletePanelVisible = true;
            IsSelectAllMode = true;
        }

        private void OnChatDelete(object param)
        {
            try
            {
                List<RecentModel> modelList = new List<RecentModel>();
                for (int i = 0; i < itemsControl.Items.Count; i++)
                {
                    ContentPresenter child = (ContentPresenter)itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                    RecentModel model = (RecentModel)child.Content;
                    child.ApplyTemplate();

                    Control presenter = (Control)child.ContentTemplate.FindName("container", child);
                    presenter.ApplyTemplate();

                    CustomBorder border = (CustomBorder)presenter.Template.FindName("customBorder", presenter);
                    if (border.Visibility == Visibility.Visible && model.IsChecked == true)
                    {
                        modelList.Add(model);
                    }
                }

                if (modelList.Count == 0)
                {
                    UIHelperMethods.ShowWarning("Please Select Chat Log First!");
                    return;
                }

                DeletChatLog(modelList);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChatDelete() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DeletChatLog(List<RecentModel> modelList)
        {
            //WNConfirmationView cv = new WNConfirmationView("Delete confirmation!", NotificationMessages.DELETE_NOTIFICAITON, CustomConfirmationDialogButtonOptions.YesNo, "Deleting chat log will delete your selected logs forever.");
            //var result = cv.ShowCustomDialog();
            //if (result == ConfirmationDialogResult.Yes)
            //{
            bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.SURE_WANT_TO, "delete"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"), "Deleting will delete your selected logs forever.");
            if (isTrue)
            {
                new Thread(() =>
                {
                    try
                    {
                        List<BaseConversationDTO> convList = new List<BaseConversationDTO>();
                        ChatLogLoadUtility.RemoveRange(modelList);
                        foreach (RecentModel model in modelList)
                        {
                            if (model.FriendTableID > 0)
                            {
                                convList.Add(new BaseConversationDTO { ConversationType = (int)ConversationType.FRIEND_ID, FriendID = model.FriendTableID });
                            }
                            else if (model.GroupID > 0)
                            {
                                convList.Add(new BaseConversationDTO { ConversationType = (int)ConversationType.GROUP_ID, FriendID = model.GroupID });
                            }

                            RecentChatCallActivityDAO.DeleteChatHistory(model.Message.FriendTableID, model.Message.GroupID);
                            ObservableDictionary<string, long> unreadList = ChatViewModel.Instance.GetUnreadChatByID(model.ContactID);
                            ChatHelpers.RemoveUnreadChat(model.ContactID, unreadList.Keys.ToList(), unreadList);

                            List<RecentModel> tempList = RingIDViewModel.Instance.GetRecentModelListByID(model.ContactID)
                                    .Where(P => P.Type == ChatConstants.SUBTYPE_GROUP_CHAT || P.Type == ChatConstants.SUBTYPE_FRIEND_CHAT || P.Type == ChatConstants.SUBTYPE_ROOM_CHAT).ToList();
                            foreach (RecentModel temp in tempList)
                            {
                                string filePath = ChatHelpers.GetChatFileDeleteFile(temp.Message.Message, temp.Message.PrevMessageType, temp.Message.PacketID, temp.Message.SenderTableID);
                                if (!String.IsNullOrWhiteSpace(filePath) && System.IO.File.Exists(filePath))
                                {
                                    try { System.IO.File.Delete(filePath); }
                                    catch { }
                                }

                                filePath = ChatHelpers.GetChatPreviewDeletePath(temp.Message.Message, temp.Message.PrevMessageType, temp.Message.PacketID);
                                if (!String.IsNullOrWhiteSpace(filePath) && System.IO.File.Exists(filePath))
                                {
                                    try { System.IO.File.Delete(filePath); }
                                    catch { }
                                }
                            }

                            List<string> packetIds = tempList.Select(P => P.Message.PacketID).ToList();
                            RecentLoadUtility.RemoveRange(model.ContactID, packetIds, true);
                        }

                        if (convList.Count > 0)
                        {
                            ChatService.DeleteConversation(convList, null);
                        }
                        OnChatDeleteCancel(null);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error: OnChatDelete()::Invoke() => " + ex.Message + "\n" + ex.StackTrace);
                    }
                }).Start();
            }
        }

        private void OnChatDeleteCancel(object param)
        {
            try
            {
                IsSelectAllMode = true;
                IsDeletePanelVisible = false;
                RingIDViewModel.Instance.ChatLogModelsList.ToList().ForEach(P => P.IsChecked = !IsSelectAllMode);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChatDeleteCancel() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility method

        #region Property

        public ICommand OnSelectContactIDCommand
        {
            get
            {
                if (_OnSelectContactIDCommand == null)
                {
                    _OnSelectContactIDCommand = new RelayCommand((param) => OnSelectContactID(param));
                }
                return _OnSelectContactIDCommand;
            }
        }

        public ICommand ShowDeletePanelCommand
        {
            get
            {
                if (_ShowDeletePanelCommand == null)
                {
                    _ShowDeletePanelCommand = new RelayCommand((param) => OnShowChatDelete(param));
                }
                return _ShowDeletePanelCommand;
            }
        }

        public ICommand SelectAllCommand
        {
            get
            {
                if (_SelectAllCommand == null)
                {
                    _SelectAllCommand = new RelayCommand((param) => OnSelectAll(param));
                }
                return _SelectAllCommand;
            }
        }

        public ICommand ChatDeleteCommand
        {
            get
            {
                if (_ChatDeleteCommand == null)
                {
                    _ChatDeleteCommand = new RelayCommand((param) => OnChatDelete(param));
                }
                return _ChatDeleteCommand;
            }
        }

        public ICommand ChatDeleteCancelCommand
        {
            get
            {
                if (_ChatDeleteCancelCommand == null)
                {
                    _ChatDeleteCancelCommand = new RelayCommand((param) => OnChatDeleteCancel(param));
                }
                return _ChatDeleteCancelCommand;
            }
        }

        public string SelectedContactID
        {
            get { return _SelectedContactID; }
            set
            {
                if (value == _SelectedContactID)
                    return;

                _SelectedContactID = value;
                this.OnPropertyChanged("SelectedContactID");
            }
        }

        public bool IsDeletePanelVisible
        {
            get { return _IsDeletePanelVisible; }
            set
            {
                _IsDeletePanelVisible = value;
                this.OnPropertyChanged("IsDeletePanelVisible");
            }
        }

        public bool IsSelectAllMode
        {
            get { return _IsSelectAllMode; }
            set
            {
                _IsSelectAllMode = value;
                this.OnPropertyChanged("IsSelectAllMode");
            }
        }

        public bool IsChatLogEnabled
        {
            get { return this._IsChatLogEnabled; }
            set
            {
                if (this._IsChatLogEnabled == value)
                    return;

                this._IsChatLogEnabled = value;
                this.OnPropertyChanged("IsChatLogEnabled");
            }
        }

        #endregion Property


    }
}

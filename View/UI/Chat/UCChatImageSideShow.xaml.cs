﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Animation;
using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Chat;
using View.Utility.WPFMessageBox;
using System.Diagnostics;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatImageSideShow.xaml
    /// </summary>
    public partial class UCChatImageSideShow : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatImageSideShow).Name);
        private ObservableCollection<ImageUploaderModel> _ImageUploaderModelList = new ObservableCollection<ImageUploaderModel>();
        private ImageUploaderModel _AddImageModel = new ImageUploaderModel { ImageType = 0 };
        private ImageUploaderModel _SelectedImageUploadModel = null;

        private ICommand _ChatSendCommand;
        private ICommand _CloseCommand;
        private ICommand _LeftArrowCommand;
        private ICommand _RightArrowCommand;
        private ICommand _AddDirectoryImageCommand;
        private ICommand _AddRingCameraImageCommand;

        private Visibility _IsLeftArrowVisible = Visibility.Visible;
        private Visibility _IsRightArrowVisible = Visibility.Visible;
        private Visibility _IsWaterMarkVisible = Visibility.Visible;

        private int CurrentLeftMargin = 0;
        private VirtualizingStackPanel _ImageItemContainer = null;

        private ICommand _ChatEmoticonCommand;

        private BackgroundWorker _ImageLoader = new BackgroundWorker();

        public delegate void OnCompletedHandler(List<FileShareData> fileShareList);
        public event OnCompletedHandler OnCompleted;

        public UCChatImageSideShow(List<ImageUploaderModel> modelList)
        {
            InitializeComponent();
            ((INotifyCollectionChanged)ImagesListBox.Items).CollectionChanged += ImagesListBox_CollectionChanged;
            ImagesListBox.Loaded += ImagesListBox_Loaded;
            ImagesListBox.SelectionChanged += ImagesListBox_SelectionChanged;
            rtbChatArea.RichTextChanged += rtbChatArea_TextChanged;
            rtbChatArea.PreviewKeyDown += rtbChatArea_PreviewKeyDown;
            _ImageLoader.WorkerSupportsCancellation = true;
            _ImageLoader.WorkerReportsProgress = true;
            _ImageLoader.DoWork += ImageLoader_DowWork;
            _ImageLoader.RunWorkerCompleted += ImageLoader_RunWorkerCompleted;
            this.DataContext = this;
            _ImageLoader.RunWorkerAsync(modelList);
            log.Debug("==> UCChatImageSideShow ");
        }

        #region Event Handler

        private void ImageLoader_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                List<ImageUploaderModel> modelList = (List<ImageUploaderModel>)e.Argument;
                log.Debug("==> ImageLoader_DowWork => Count = " + modelList.Count);
                foreach (ImageUploaderModel model in modelList)
                {
                    ImageUploaderModelList.InvokeAdd(model);
                    System.Threading.Thread.Sleep(15);

                    if (_ImageLoader.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                }

                if (_ImageLoader.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ImageLoader_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
            e.Result = true;
        }

        private void ImageLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == false && e.Result != null && (bool)e.Result)
            {
                ImageUploaderModelList.Add(_AddImageModel);
            }
        }

        private void HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as VirtualizingStackPanel;
        }

        private void ImagesListBox_Loaded(object sender, RoutedEventArgs e)
        {
            IsLeftArrowVisible = CurrentLeftMargin < 0 ? Visibility.Visible : Visibility.Hidden;
            IsRightArrowVisible = CurrentLeftMargin > -(((float)ImagesListBox.Items.Count / 4) - 1) * pnlImageContainerWrapper.ActualWidth ? Visibility.Visible : Visibility.Hidden;
        }

        private void ImagesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ImageUploaderModel model = ImagesListBox.SelectedItem as ImageUploaderModel;
                if (model != null && model.ImageType != 0 && model.ImageType != 1)
                {
                    SelectedImageUploadModel = null;
                    rtbChatArea.Document.Blocks.Clear();
                    rtbChatArea.AppendText(model.ImageCaption);
                    SelectedImageUploadModel = model;
                    if (!String.IsNullOrEmpty(SelectedImageUploadModel.ImageCaption))
                    {
                        IsWaterMarkVisible = Visibility.Collapsed;
                    }
                    else
                    {
                        IsWaterMarkVisible = Visibility.Visible;
                    }
                }
                else
                {
                    SelectedImageUploadModel = null;
                    IsWaterMarkVisible = Visibility.Visible;
                    rtbChatArea.Document.Blocks.Clear();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ImagesListBox_SelectionChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
        }

        private void ImagesListBox_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            log.Debug("==> ImagesListBox_CollectionChanged " + e.Action);
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                ImagesListBox_Loaded(null, null);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                ImagesListBox_Loaded(null, null);
            }
        }

        private void rtbChatArea_TextChanged(string text)
        {
            if (SelectedImageUploadModel != null)
            {
                SelectedImageUploadModel.ImageCaption = text;
                if (SelectedImageUploadModel.ImageCaption.Length > 0)
                {
                    IsWaterMarkVisible = Visibility.Collapsed;
                }
                else
                {
                    IsWaterMarkVisible = Visibility.Visible;
                }
            }
        }

        private void rtbChatArea_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                {
                    e.Handled = true;

                    if (ImageUploaderModelList.Count == 2)
                    {
                        OnChatSend(null);
                    }
                }
            }
        }

        #endregion Event Handler

        #region Property

        public ObservableCollection<ImageUploaderModel> ImageUploaderModelList
        {
            get { return _ImageUploaderModelList; }
            set
            {
                _ImageUploaderModelList = value;
                this.OnPropertyChanged("ImageUploaderModelList");
            }
        }

        public ICommand ChatSendCommand
        {
            get
            {
                if (_ChatSendCommand == null)
                {
                    //  _ChatSendCommand = new RelayCommand((param) => OnChatSend(param), (param) => CanChatSend(param));
                    _ChatSendCommand = new RelayCommand((param) => OnChatSend(param));
                }
                return _ChatSendCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => OnClose(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand LeftArrowCommand
        {
            get
            {
                if (_LeftArrowCommand == null)
                {
                    _LeftArrowCommand = new RelayCommand(param => OnPrevious(param));
                }
                return _LeftArrowCommand;
            }
        }

        public ICommand RightArrowCommand
        {
            get
            {
                if (_RightArrowCommand == null)
                {
                    _RightArrowCommand = new RelayCommand(param => OnNext(param));

                }
                return _RightArrowCommand;
            }
        }

        public ICommand AddDirectoryImageCommand
        {
            get
            {
                if (_AddDirectoryImageCommand == null)
                {
                    _AddDirectoryImageCommand = new RelayCommand(param => OnAddDirectoryImage(param));

                }
                return _AddDirectoryImageCommand;
            }
        }

        public ICommand AddRingCameraImageCommand
        {
            get
            {
                if (_AddRingCameraImageCommand == null)
                {
                    _AddRingCameraImageCommand = new RelayCommand(param => OnAddRingCameraImage(param));

                }
                return _AddRingCameraImageCommand;
            }
        }

        public ICommand ChatEmoticonCommand
        {
            get
            {
                if (_ChatEmoticonCommand == null)
                {
                    _ChatEmoticonCommand = new RelayCommand((param) => OnChatEmoticon(param));
                }
                return _ChatEmoticonCommand;
            }
        }

        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }

        public Visibility IsWaterMarkVisible
        {
            get { return _IsWaterMarkVisible; }
            set
            {
                _IsWaterMarkVisible = value;
                OnPropertyChanged("IsWaterMarkVisible");
            }
        }

        public ImageUploaderModel SelectedImageUploadModel
        {
            get { return _SelectedImageUploadModel; }
            set
            {
                if (_SelectedImageUploadModel == value)
                    return;

                _SelectedImageUploadModel = value;
                this.OnPropertyChanged("SelectedImageUploadModel");
            }
        }

        #endregion Property

        #region Utility Methods

        private void OnPrevious(object param)
        {
            try
            {
                int target = CurrentLeftMargin + (int)pnlImageContainerWrapper.ActualWidth;
                AnimationOnArrowClicked(target, 0.35);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNext(object param)
        {
            try
            {
                int target = CurrentLeftMargin - (int)pnlImageContainerWrapper.ActualWidth;
                AnimationOnArrowClicked(target, 0.35);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnAddDirectoryImage(object param)
        {
            try
            {
                log.Debug("==> OnAddDirectoryImage ");
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Title = "Select picture";
                dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
                dialog.Multiselect = true;
                if (dialog.ShowDialog() == true)
                {
                    foreach (string fileName in dialog.FileNames)
                    {
                        if ((new FileInfo(fileName)).Length > DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE)
                        {
                            UIHelperMethods.ShowWarning(String.Format(NotificationMessages.MAX_FILE_TRANSFER_SIZE, DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE / (1024 * 1024)), "Add image");
                            // CustomMessageBox.ShowWarning(String.Format(NotificationMessages.MAX_FILE_TRANSFER_SIZE, DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE / (1024 * 1024)));
                            return;
                        }
                    }

                    AddImages(dialog.FileNames, false);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnAddDirectoryImage() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnAddRingCameraImage(object param)
        {
            log.Debug("==> OnAddRingCameraImage ");
            WNWebcamCapture.Instance.ShowWindow((f) =>
            {
                try
                {
                    if (ImageUploaderModelList.Count < 11)
                    {
                        ImageUploaderModelList.Insert(ImageUploaderModelList.Count - 1, new ImageUploaderModel { ImageType = (int)MessageType.IMAGE_FILE_FROM_CAMERA, FilePath = f });
                        if (ImageUploaderModelList.Count == 2 && ImagesListBox.SelectedIndex == -1)
                        {
                            ImagesListBox.SelectedIndex = 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: OnAddRingCameraImage() => " + ex.Message + "\n" + ex.StackTrace);
                }
            });
        }

        public void AddImages(string[] fileNames, bool addToFront = true)
        {
            try
            {
                log.Debug("==> AddImages => Count = " + fileNames.Length);
                foreach (string fileName in fileNames)
                {
                    if (ImageUploaderModelList.Count < 11)
                    {
                        ImageUploaderModelList.Insert((addToFront ? 0 : ImageUploaderModelList.Count - 1), new ImageUploaderModel { ImageType = (int)MessageType.IMAGE_FILE_FROM_GALLERY, FilePath = fileName });
                        if (ImageUploaderModelList.Count == 2 && ImagesListBox.SelectedIndex == -1)
                        {
                            ImagesListBox.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AddImages() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnChatSend(object param)
        {
            try
            {
                log.Debug("==> OnChatSend ");
                // if (ImageUploaderModelList.Count > 1)
                //{
                ImageUploaderModelList.Remove(_AddImageModel);

                List<FileShareData> fileShareList = new List<FileShareData>();
                foreach (ImageUploaderModel model in ImageUploaderModelList)
                {
                    FileShareData fileShareData = new FileShareData();
                    fileShareData.MessageType = model.ImageType;
                    fileShareData.FilePath = model.FilePath;
                    fileShareData.Caption = model.ImageCaption;
                    int w = 0;
                    int h = 0;
                    ImageUtility.GetImageDimension(fileShareData.FilePath, out w, out h);
                    fileShareData.Width = w;
                    fileShareData.Height = h;
                    fileShareList.Add(fileShareData);
                }

                if (OnCompleted != null)
                {
                    OnCompleted(fileShareList);
                }
                //}
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChatSend() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //private bool CanChatSend(object param)
        //{
        //    return ImageUploaderModelList.Count > 1;
        //}

        private void OnClose(object param)
        {
            log.Debug("==> OnClose " + (param == null));
            if (param == null)
            {
                if (_ImageLoader.IsBusy)
                {
                    _ImageLoader.CancelAsync();
                }

                if (OnCompleted != null)
                {
                    OnCompleted(null);
                }
            }
            else
            {
                ImageUploaderModel model = (ImageUploaderModel)param;
                ImageUploaderModelList.Remove(model);
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(CurrentLeftMargin, 0, 0, 0);
            MarginAnimation.To = new Thickness(target, 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
            storyboard.Begin(_ImageItemContainer);
        }

        private void OnChatEmoticon(object param)
        {
            MainSwitcher.PopupController.EmoticonPopupWrapper.Show(btnChatEmoticon, UCEmoticonPopup.TYPE_CHAT_EDIT_PANEL, (emo) =>
             {
                 if (!String.IsNullOrWhiteSpace(emo))
                 {
                     rtbChatArea.AppendText(emo);
                 }
                 return 0;
             });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void Dispose()
        {
            log.Debug("==> Dispose");
            ImagesListBox.Loaded -= ImagesListBox_Loaded;
            ImagesListBox.SelectionChanged -= ImagesListBox_SelectionChanged;
            rtbChatArea.RichTextChanged -= rtbChatArea_TextChanged;
            rtbChatArea.PreviewKeyDown -= rtbChatArea_PreviewKeyDown;
            _ImageLoader.DoWork -= ImageLoader_DowWork;
            _ImageLoader.RunWorkerCompleted -= ImageLoader_RunWorkerCompleted;
            if (_ImageLoader.IsBusy)
            {
                _ImageLoader.CancelAsync();
            }
            _ImageLoader = null;
            ImageUploaderModelList.Clear();
        }

        #endregion Utility Methods

    }
}

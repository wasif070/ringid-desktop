﻿using log4net;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Recorder;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for WNRecorderPreview.xaml
    /// </summary>
    public partial class WNRecorderPreview : Window
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WNRecorderPreview).Name);

        public WNRecorderPreview(UCChatAudioRecorder audioRecorder)
        {
            InitializeComponent();
            container.Child = audioRecorder;
            this.MouseDown += delegate { DragMove(); };
        }

        public WNRecorderPreview(UCChatVideoRecorder videoRecorder)
        {
            InitializeComponent();
            container.Child = videoRecorder;
            this.MouseDown += delegate { DragMove(); };
        }

        #region Event Handler

        #endregion

        #region Utility Methods

        public void ShowWindow()
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.Show();
            }, DispatcherPriority.Normal);
        }

        public void CloseWindow(object param = null)
        {
            this.container.Child = null;
            this.Owner = null;
            this.Close();
        }

        private new void Show()
        {
            this.Topmost = true;
            base.Show();

            this.Owner = System.Windows.Application.Current.MainWindow;
            System.Drawing.Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
            this.Left = workingArea.Right - this.ActualWidth - 3;
            this.Top = workingArea.Top + 3; //workingArea.Bottom - this.ActualHeight;
        }

        #endregion

        #region Property

        
        #endregion 
    }
}

﻿using log4net;
using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.Recorder;
using View.Utility.WPFMessageBox;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatAudioRecorder.xaml
    /// </summary>
    public partial class UCChatAudioRecorder : System.Windows.Controls.UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatAudioRecorder).Name);

        public static UCChatAudioRecorder Instance = null;

        private int _Type;
        private int _State = RecorderConstants.STATE_READY;
        private int _RecordingProgress = 0;
        private TimeSpan _TimerValue = TimeSpan.FromSeconds(0);
        private UserBasicInfoModel _FriendBasicInfoModel;
        private GroupInfoModel _GroupInfoModel;
        private RoomModel _RoomModel;
        private ICommand _CloseCommand;
        private ICommand _RecordCommand;
        private AudioCapture audioCapture;
        private string _FileName = String.Empty;

        private bool _IsExpanable = true;
        private bool _IsPlayMode = false;
        private int _TotalSeconds = 0;
        private bool _UserIsDraggingSlider = false;

        private CustomizeTimer PlayerProgressTimer { set; get; }

        public delegate void OnRecordingCompleted(string fileName, int countDown);
        public event OnRecordingCompleted RecordingCompletedHandler;

        public UCChatAudioRecorder(GroupInfoModel groupInfoModel)
        {
            this.GroupInfoModel = groupInfoModel;
            this.Type = RecorderConstants.TYPE_GROUP_VOICE_RECORD;
            this.Init();
        }

        public UCChatAudioRecorder(UserBasicInfoModel friendBasicInfoModel)
        {
            this.FriendBasicInfoModel = friendBasicInfoModel;
            this.Type = RecorderConstants.TYPE_FRIEND_VOICE_RECORD;
            this.Init();
        }

        public UCChatAudioRecorder(RoomModel roomModel)
        {
            this.RoomModel = roomModel;
            this.Type = RecorderConstants.TYPE_ROOM_VOICE_RECORD;
            this.Init();
        }

        private void Init()
        {
            this.InitializeComponent();
            Instance = this;
            this.State = RecorderConstants.STATE_READY;
            PlayerProgressTimer = new CustomizeTimer();
            PlayerProgressTimer.IntervalInSecond = 1;
            PlayerProgressTimer.Tick += PlayerProgressTimer_Tick;
            audioCapture = new AudioCapture(RecorderConstants.MAX_VOICE_RECORD_TIME);
            audioCapture.OnRecordingCountDownChange += audioCapture_OnRecordingCountDownChange;
            audioCapture.OnRecordingCompleted += audioCapture_OnRecordingCompleted;
            this.DataContext = this;
        }

        #region Event Handler

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            ((AnimationClock)sender).Completed -= DoubleAnimationCompleted;
            mainContainer.Triggers.Clear();
        }

        private void audioCapture_OnRecordingCountDownChange(int v)
        {
            TimerValue = TimeSpan.FromSeconds(v);
            RecordingProgress = (v * 100) / RecorderConstants.MAX_VOICE_RECORD_TIME;
            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                sdrPlayerProgress.Value = v;
            }, DispatcherPriority.Send);
        }

        private void audioCapture_OnRecordingCompleted()
        {
            State = RecorderConstants.STATE_COMPLETED;
            _TotalSeconds = TimerValue.Seconds;

            new Thread(() =>
            {
                if (System.IO.File.Exists(_FileName))
                {
                    try
                    {
                        Metadata md = MediaService.GetMetadata(RingIDSettings.TEMP_MEDIA_TOOLKIT, _FileName);
                        if (md.Duration.Seconds > _TotalSeconds)
                        {
                            string newFile = System.IO.Path.GetDirectoryName(_FileName) + System.IO.Path.DirectorySeparatorChar + System.IO.Path.GetFileNameWithoutExtension(_FileName) + "_trim" + System.IO.Path.GetExtension(_FileName);

                            ConversionOptions conversionOptions = new ConversionOptions
                            {
                                AudioEncoder = AudioEncoder.PCM_S16LE,
                                Format = Format.WAV,
                                MaxDuration = TimeSpan.FromSeconds(_TotalSeconds)
                            };
                            Metadata metaData = MediaService.ConvertMedia(RingIDSettings.TEMP_MEDIA_TOOLKIT, _FileName, newFile, conversionOptions);
                            if (System.IO.File.Exists(newFile))
                            {
                                FileInfo fi = new FileInfo(_FileName);
                                while (ChatHelpers.IsFileLocked(fi)) Thread.Sleep(50);
                                System.IO.File.Delete(_FileName);
                                System.IO.File.Move(newFile, _FileName);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error: Trim Audio Record File => " + ex.Message + "\n" + ex.StackTrace);
                    }

                    System.Windows.Application.Current.Dispatcher.Invoke(() =>
                    {
                        _UserIsDraggingSlider = false;

                        sdrPlayerProgress.Minimum = 0;
                        sdrPlayerProgress.Maximum = _TotalSeconds;
                        sdrPlayerProgress.Value = 0;
                        mediaPlayer.Source = new Uri(_FileName);
                        IsPlayMode = true;
                    }, DispatcherPriority.ApplicationIdle);
                }
            }).Start();
        }

        private void PlayerProgressTimer_Tick(int counter, bool initTick, object state)
        {
            try
            {
                if (initTick) return;

                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    bool hasTimeSpan = mediaPlayer.NaturalDuration.HasTimeSpan;
                    if (hasTimeSpan && mediaPlayer.Source != null && _UserIsDraggingSlider == false)
                    {
                        TimeSpan tempTimeSpan = mediaPlayer.Position;
                        double diff = Math.Ceiling(1000 - (tempTimeSpan.TotalMilliseconds % 1000));
                        TimerValue = diff < 100 ? tempTimeSpan.Add(TimeSpan.FromMilliseconds(diff)) : tempTimeSpan;

                        if ((int)TimerValue.TotalSeconds >= _TotalSeconds)
                        {
                            sdrPlayerProgress.Value = _TotalSeconds;
                            PlayerProgressTimer.Stop();
                            mediaPlayer.Stop();
                            State = RecorderConstants.STATE_COMPLETED;
                        }
                        else
                        {
                            sdrPlayerProgress.Value = TimerValue.TotalSeconds;
                        }
                    }
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                if (DefaultSettings.DEBUG)
                    log.Error("Error: PlayerProgressTimer_Tick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void sdrPlayerProgress_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            if (IsPlayMode)
            {
                _UserIsDraggingSlider = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void sdrPlayerProgress_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            if (IsPlayMode)
            {
                _UserIsDraggingSlider = false;
                mediaPlayer.Position = TimeSpan.FromSeconds(sdrPlayerProgress.Value);
            }
            else
            {
                e.Handled = true;
            }
        }

        #endregion

        #region Utility Methods

        private void OnRecordClick(object param)
        {
            try
            {
                int buttonType = (int)param;
                switch (buttonType)
                {
                    case RecorderConstants.BUTTON_RESTART:
                    case RecorderConstants.BUTTON_START:
                        IsPlayMode = false;
                        mediaPlayer.Close();
                        PlayerProgressTimer.Stop();
                        mediaPlayer.Source = null;

                        sdrPlayerProgress.Minimum = 0;
                        sdrPlayerProgress.Maximum = RecorderConstants.MAX_VOICE_RECORD_TIME;

                        try
                        {
                            if (!String.IsNullOrWhiteSpace(_FileName))
                            {
                                if (System.IO.File.Exists(_FileName))
                                {
                                    System.IO.File.Delete(_FileName);
                                }
                            }
                        }
                        catch { }

                        _FileName = RingIDSettings.TEMP_CAM_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + DefaultSettings.LOGIN_RING_ID + "_" + ChatService.GetServerTime() + AudioCapture.AUDIO_FORMAT;
                        if (audioCapture.Record(_FileName))
                        {
                            State = RecorderConstants.STATE_RUNNING;
                        }
                        else
                        {
                            UIHelperMethods.ShowWarning("No microphone found in your system!");
                            OnCloseClick(null);
                        }
                        break;
                    case RecorderConstants.BUTTON_STOP:
                        audioCapture.RequestForStop();
                        break;
                    case RecorderConstants.BUTTON_PLAY:
                        mediaPlayer.Volume = 1;
                        mediaPlayer.Play();
                        _UserIsDraggingSlider = false;
                        if (PlayerProgressTimer.IsAlive)
                        {
                            PlayerProgressTimer.Resume();
                        }
                        else
                        {
                            PlayerProgressTimer.Start();
                        }
                        State = RecorderConstants.STATE_PLAY;
                        break;
                    case RecorderConstants.BUTTON_PAUSE:
                        if (IsPlayMode)
                        {
                            mediaPlayer.Pause();
                            PlayerProgressTimer.Pause();
                            _UserIsDraggingSlider = false;
                        }
                        else
                        {
                            audioCapture.Pause();
                        }
                        State = RecorderConstants.STATE_PAUSED;
                        break;
                    case RecorderConstants.BUTTON_RESUME:
                        audioCapture.Resume();
                        State = RecorderConstants.STATE_RUNNING;
                        break;
                    case RecorderConstants.BUTTON_SEND:
                        State = RecorderConstants.STATE_SENT;
                        if (RecordingCompletedHandler != null)
                        {
                            RecordingCompletedHandler(_FileName, _TotalSeconds);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnRecordClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnCloseClick(object param)
        {
            if (RecordingCompletedHandler != null)
            {
                RecordingCompletedHandler(string.Empty, 0);
            }
        }

        public void Dispose()
        {
            try
            {
                if (audioCapture != null)
                {
                    audioCapture.Dispose();
                    audioCapture = null;
                }
                PlayerProgressTimer.Stop();
                mediaPlayer.Close();
                mediaPlayer.Source = null;
                if (State != RecorderConstants.STATE_SENT && !String.IsNullOrWhiteSpace(_FileName))
                {
                    if (System.IO.File.Exists(_FileName))
                    {
                        System.IO.File.Delete(_FileName);
                    }
                }

            }
            catch (Exception ex)
            {
                if (DefaultSettings.DEBUG)
                    log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
            Instance = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property

        public UserBasicInfoModel FriendBasicInfoModel
        {
            get { return _FriendBasicInfoModel; }
            set { _FriendBasicInfoModel = value; }
        }

        public GroupInfoModel GroupInfoModel
        {
            get { return _GroupInfoModel; }
            set { _GroupInfoModel = value; }
        }

        public RoomModel RoomModel
        {
            get { return _RoomModel; }
            set { _RoomModel = value; }
        }

        public int Type
        {
            get { return _Type; }
            set
            {
                _Type = value;
                this.OnPropertyChanged("Type");
            }
        }

        public int State
        {
            get { return _State; }
            set
            {
                _State = value;
                this.OnPropertyChanged("State");
            }
        }

        public int RecordingProgress
        {
            get { return _RecordingProgress; }
            set
            {
                _RecordingProgress = value;
                this.OnPropertyChanged("RecordingProgress");
            }
        }

        public TimeSpan TimerValue
        {
            get { return _TimerValue; }
            set
            {
                _TimerValue = value;
                this.OnPropertyChanged("TimerValue");
            }
        }

        public bool IsPlayMode
        {
            get
            {
                return _IsPlayMode;
            }
            set
            {
                _IsPlayMode = value;
                this.OnPropertyChanged("IsPlayMode");
            }
        }

        public bool IsExpanable
        {
            get
            {
                return _IsExpanable;
            }
            set
            {
                _IsExpanable = value;
                this.OnPropertyChanged("IsExpanable");
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => OnCloseClick(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand RecordCommand
        {
            get
            {
                if (_RecordCommand == null)
                {
                    _RecordCommand = new RelayCommand((param) => OnRecordClick(param));
                }
                return _RecordCommand;
            }
        }

        #endregion
    }
}

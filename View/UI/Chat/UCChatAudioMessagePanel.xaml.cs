﻿using Auth.utility;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Recent;
using View.Utility.Recorder.DirectX.Capture;
using View.Utility.WPFMessageBox;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatMultimediaMessagePanel.xaml
    /// </summary>
    public partial class UCChatAudioMessagePanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatAudioMessagePanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private bool disposed = false;
        private static object ImageLocation = null;
        private static object RingIDSettings = null;

        static UCChatAudioMessagePanel()
        {
            if (Application.Current != null) 
            { 
                ImageLocation = Application.Current.FindResource("ImageLocation");
                RingIDSettings = Application.Current.FindResource("RingIDSettings"); 
            }
        }

        public UCChatAudioMessagePanel()
        {
            this.MinHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)MessageType.AUDIO_FILE][0] + ChatHelpers.CHAT_VIEW_HEIGHT[(int)MessageType.AUDIO_FILE][1];
            InitializeComponent();
        }

        ~UCChatAudioMessagePanel()
        {
           Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCChatAudioMessagePanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        public static readonly DependencyProperty IsSameSenderProperty = DependencyProperty.Register("IsSameSender", typeof(bool), typeof(UCChatAudioMessagePanel), new PropertyMetadata(true, OnNewSenderChanged));

        public bool IsSameSender
        {
            get { return (bool)GetValue(IsSameSenderProperty); }
            set
            {
                SetValue(IsSameSenderProperty, value);
            }
        }

        public static readonly DependencyProperty IsPreviewOpenedProperty = DependencyProperty.Register("IsPreviewOpened", typeof(bool), typeof(UCChatAudioMessagePanel), new PropertyMetadata(false, OnPreviewChanged));

        public bool IsPreviewOpened
        {
            get { return (bool)GetValue(IsPreviewOpenedProperty); }
            set
            {
                SetValue(IsPreviewOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                UCChatAudioMessagePanel panel = ((UCChatAudioMessagePanel)sender);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewSize.Height > model.PrevViewHeight)
                {
                    model.PrevViewHeight = (int)e.NewSize.Height;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSizeChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatAudioMessagePanel panel = ((UCChatAudioMessagePanel)o);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewValue != null && (bool)e.NewValue)
                {
                    model.PrevViewHeight = model.MinViewHeight;
                    panel.SizeChanged += panel.OnSizeChanged;

                    if (!panel.IsSameSender)
                    {
                        panel.BindNewSender(model.Message.ViewModel.IsRoomChat);
                    }
                    else
                    {
                        panel.ClearNewSender();
                    }

                    panel.BindPreview();
                    panel.BindMsgInfo(model.Message);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnNewSenderChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatAudioMessagePanel panel = ((UCChatAudioMessagePanel)o);
                if (panel.disposed) return;

                if (e.NewValue != null && !(bool)e.NewValue && panel.IsOpened)
                {
                    panel.BindNewSender(((RecentModel)panel.DataContext).Message.ViewModel.IsRoomChat);
                }
                else
                {
                    panel.ClearNewSender();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNewSenderChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnPreviewChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatAudioMessagePanel panel = ((UCChatAudioMessagePanel)o);
                if (panel.disposed) return;

                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewValue != null && (bool)e.NewValue && panel.IsOpened)
                {
                    panel.BindPreview();
                }
                else
                {
                    panel.ClearPreview();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPreviewChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void prgsControl_ActionChanged(int action)
        {
            try
            {
                switch (action)
                {
                    case 1://Download
                        {
                            btnPlayer.Visibility = Visibility.Visible;
                            btnPlayer.ActionCursor = Cursors.Hand;
                            btnPlayer.SetBinding(CircularProgressButton.ActionCommandProperty, new Binding { Path = new PropertyPath("ChatViewModel.OnMediaDownloadClickCommand"), Source = RingIDSettings });
                            btnPlayer.SetBinding(CircularProgressButton.ActionCommandParameterProperty, new Binding { Path = new PropertyPath("Message") });
                            btnPlayer.ActionToolTip = "Click to Download";
                            btnPlayer.ClearValue(CircularProgressButton.PercentageProperty);
                            btnPlayer.Percentage = 100;
                            btnPlayer.SetBinding(CircularProgressButton.IconSourceProperty, new Binding { Path = new PropertyPath("CHAT_BG_DOWNLOAD"), Source = ImageLocation });
                            btnPlayer.SetBinding(CircularProgressButton.IconSourceHoverProperty, new Binding { Path = new PropertyPath("CHAT_BG_DOWNLOAD_H"), Source = ImageLocation });
                        }
                        break;
                    case 2://Init->Play
                    case 3://Pause->play
                        {
                            btnPlayer.Visibility = Visibility.Visible;
                            btnPlayer.ActionCursor = Cursors.Hand;
                            btnPlayer.SetBinding(CircularProgressButton.ActionCommandProperty, new Binding { Path = new PropertyPath("ChatViewModel.OnAudioPlayerActionClickCommand"), Source = RingIDSettings });
                            MultiBinding commandParameterBinding = new MultiBinding { Converter = new ChatAudioActionCommandParameterConverter() };
                            commandParameterBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message")
                            });
                            commandParameterBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message.MultiMediaState")
                            });
                            btnPlayer.SetBinding(CircularProgressButton.ActionCommandParameterProperty, commandParameterBinding);
                            btnPlayer.ActionToolTip = "Click to Play";
                            btnPlayer.ClearValue(CircularProgressButton.PercentageProperty);
                            btnPlayer.Percentage = 100;
                            btnPlayer.SetBinding(CircularProgressButton.IconSourceProperty, new Binding { Path = new PropertyPath("CHAT_PLAY"), Source = ImageLocation });
                            btnPlayer.SetBinding(CircularProgressButton.IconSourceHoverProperty, new Binding { Path = new PropertyPath("CHAT_PLAY_H"), Source = ImageLocation });
                        }
                        break;
                    case 4://Play->Pause
                        {
                            btnPlayer.Visibility = Visibility.Visible;
                            btnPlayer.ActionCursor = Cursors.Hand;
                            btnPlayer.SetBinding(CircularProgressButton.ActionCommandProperty, new Binding { Path = new PropertyPath("ChatViewModel.OnAudioPlayerActionClickCommand"), Source = RingIDSettings });
                            MultiBinding commandParameterBinding = new MultiBinding { Converter = new ChatAudioActionCommandParameterConverter() };
                            commandParameterBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message")
                            });
                            commandParameterBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message.MultiMediaState")
                            });
                            btnPlayer.SetBinding(CircularProgressButton.ActionCommandParameterProperty, commandParameterBinding);
                            btnPlayer.ActionToolTip = "Click to Pause";
                            btnPlayer.ClearValue(CircularProgressButton.PercentageProperty);
                            btnPlayer.Percentage = 100;
                            btnPlayer.SetBinding(CircularProgressButton.IconSourceProperty, new Binding { Path = new PropertyPath("CHAT_PAUSE"), Source = ImageLocation });
                            btnPlayer.SetBinding(CircularProgressButton.IconSourceHoverProperty, new Binding { Path = new PropertyPath("CHAT_PAUSE_H"), Source = ImageLocation });
                        }
                        break;
                    default:
                        {
                            btnPlayer.Visibility = Visibility.Collapsed;
                            btnPlayer.ActionCursor = null;
                            btnPlayer.ClearValue(CircularProgressButton.ActionCommandProperty);
                            btnPlayer.ClearValue(CircularProgressButton.ActionCommandParameterProperty);
                            btnPlayer.ActionToolTip = null;
                            btnPlayer.ClearValue(CircularProgressButton.PercentageProperty);
                            btnPlayer.Percentage = 0;
                            btnPlayer.ClearValue(CircularProgressButton.IconSourceProperty);
                            btnPlayer.IconSource = null;
                            btnPlayer.ClearValue(CircularProgressButton.IconSourceHoverProperty);
                            btnPlayer.IconSourceHover = null;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: prgsControl_ActionChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindMsgInfo(MessageModel messageModel)
        {
            MultiBinding textBinding = new MultiBinding { Converter = new ChatStatusAndTimeConverter() };
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MessageDate")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.Status")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MessageType")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.PacketType")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FromFriend")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsRoomChat")
            });
            txtStatusAndTime.SetBinding(TextBlock.TextProperty, textBinding);

            if (!messageModel.FromFriend && !messageModel.ViewModel.IsRoomChat)
            {
                MultiBinding lastStatusBinding = new MultiBinding { Converter = new ChatLastStatusConverter() };
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.MessageDate")
                });
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.LastStatus.LastDeliveredDate")
                });
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.LastStatus.LastSeenDate")
                });
                icnLastStatus.SetBinding(Image.TagProperty, lastStatusBinding);
            }

            MultiBinding clipBinding = new MultiBinding { Converter = new ChatBubbolConverter() };
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Width"),
                RelativeSource = new RelativeSource(RelativeSourceMode.Self)
            });
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Height"),
                RelativeSource = new RelativeSource(RelativeSourceMode.Self)
            });
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FromFriend")
            });
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsSamePrevAndCurrID")
            });
            grdMessageContainer.SetBinding(Grid.ClipProperty, clipBinding);
        }

        private void ClearMsgInfo()
        {
            txtStatusAndTime.ClearValue(TextBlock.TextProperty);
            icnLastStatus.ClearValue(Image.TagProperty);
            icnLastStatus.Source = null;
            grdMessageContainer.ClearValue(Grid.ClipProperty);
            grdMessageContainer.Clip = null;
        }

        private void BindPreview()
        {
            btnPlayer.ActionChanged += prgsControl_ActionChanged;
            MultiBinding actionBinding = new MultiBinding { Converter = new ChatAudioActionConverter() };
            actionBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.IsFileOpened")
            });
            actionBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsAudioOpened")
            });
            actionBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MultiMediaState")
            });
            actionBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.IsDownloadRunning")
            });
            actionBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsUploadRunning")
            });
            btnPlayer.SetBinding(CircularProgressButton.ActionProperty, actionBinding);

            MultiBinding progressBarBinding = new MultiBinding { Converter = new ChatPlayerProgressConverter() };
            progressBarBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.OffsetPosition")
            });
            progressBarBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.Duration")
            });
            prgsBar.SetBinding(ProgressBar.ValueProperty, progressBarBinding);

            MultiBinding progressTimeBinding = new MultiBinding { Converter = new ChatAudioPlayerProgressTimeConverter(), StringFormat = "hh\\:mm\\:ss" };
            progressTimeBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MultiMediaState")
            });
            progressTimeBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.OffsetPosition")
            });
            progressTimeBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.Duration")
            });
            prgsTime.SetBinding(TextBlock.TextProperty, progressTimeBinding);
        }

        private void ClearPreview()
        {
            btnPlayer.ActionChanged -= prgsControl_ActionChanged;
            btnPlayer.ClearValue(CircularProgressButton.ActionProperty);
            prgsControl_ActionChanged(0);
            prgsBar.ClearValue(ProgressBar.ValueProperty);
            prgsTime.ClearValue(TextBlock.TextProperty);
        }

        private void BindNewSender(bool isRoomChat)
        {
            if (isRoomChat)
            {
                imgProfile.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("Message.CurrentInstance"), Converter = new PathToChatProfileImageConverter() });
                txtFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.FullName") });
            }
            else
            {
                imgProfile.SetBinding(System.Windows.Controls.Image.SourceProperty, new Binding { Path = new PropertyPath("Message.FriendInfoModel.CurrentInstance"), Converter = new PathToChatProfileImageConverter() });
                txtFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.FriendInfoModel.FullName") });
            }
        }

        private void ClearNewSender()
        {
            imgProfile.ClearValue(Image.SourceProperty);
            imgProfile.Source = null;
            txtFullName.ClearValue(TextBlock.TextProperty);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    this.SizeChanged -= OnSizeChanged;
                    if (disposing)
                    {
                        this.ClearNewSender();
                        this.ClearMsgInfo();
                        this.ClearPreview();
                        this.imgProfile = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

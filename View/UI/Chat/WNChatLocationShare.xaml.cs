﻿using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.Utility.Chat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Linq;
using View.BindingModels;
using View.Utility;
using View.Utility.Chat;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for WNChatLocationShare.xaml
    /// </summary>
    public partial class WNChatLocationShare : Window, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WNLocationViewer).Name);

        private static WNChatLocationShare _Instance = null;
        private GeoCoordinateWatcher _Watcher = null;
        private ICommand _CloseCommand;
        private ICommand _ChatSendCommand;
        public float _Latitude;
        public float _Longitude;
        public bool _IsReady;
        private string _LatLong = string.Empty;
        private string _Place = string.Empty;
        private long _FriendTableID;
        private long _GroupID;
        private string _RoomID;

        public WNChatLocationShare(long friendTableID, long groupID, string roomID)
        {
            InitializeComponent();
            this._FriendTableID = friendTableID;
            this._GroupID = groupID;
            this._RoomID = roomID;
            DataContext = this;
            this.MouseDown += delegate { DragMove(); };
        }

        #region Event Handler

        private void Browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            mapBrowser.ObjectForScripting = new HtmlInteropInternal((isReady, lat, lng, place, dms) =>
            {
                this.IsReady = (lat > 0 || lng > 0) && !String.IsNullOrWhiteSpace(place);
                this._Latitude = lat;
                this._Longitude = lng;
                this.Place = place;
                this.LatLong = ChatHelpers.ConvertLatLongToDMS(lat, lng);
                return 0;
            });
            mapBrowser.Document.Focus();
        }

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {

        }

        #endregion Event Handler

        #region Property

        public string Place
        {
            get { return _Place; }
            set
            {
                if (value == _Place) return;
                _Place = value;
                OnPropertyChanged("Place");
            }
        }

        public string LatLong
        {
            get { return _LatLong; }
            set
            {
                if (value == _LatLong) return;
                _LatLong = value;
                OnPropertyChanged("LatLong");
            }
        }

        public bool IsReady
        {
            get { return _IsReady; }
            set
            {
                if (value == _IsReady) return;
                _IsReady = value;
                OnPropertyChanged("IsReady");
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => CloseWindow(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand ChatSendCommand
        {
            get
            {
                if (_ChatSendCommand == null)
                {
                    _ChatSendCommand = new RelayCommand((param) => OnChatSend(param));
                }
                return _ChatSendCommand;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Property

        #region Utility Method

        private new void Show()
        {
            this.Topmost = true;
            base.Show();

            this.Owner = System.Windows.Application.Current.MainWindow;
            System.Drawing.Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
            this.Left = (workingArea.Right - this.ActualWidth) / 2;
            this.Top = (workingArea.Bottom - this.ActualHeight) / 2;

            if (System.Environment.OSVersion.Version.Minor > 1)
            {
                this._Watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
                if (this._Watcher.Permission != GeoPositionPermission.Denied)
                {
                    this._Watcher.PositionChanged += new EventHandler<GeoPositionChangedEventArgs<GeoCoordinate>>(this.watcher_PositionChanged);
                    this._Watcher.Start();
                }
                else
                {
                    this.ShowMapUsingLatLng(0, 0);
                }
            }
            else
            {
                this.ShowMapUsingLatLng(0, 0);
            }
        }

        public static void ShowWindow(long friendTableID, long groupID, string roomID)
        {
            App.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
             {
                 if (_Instance != null)
                 {
                     if ((_Instance._FriendTableID > 0 && friendTableID > 0 && _Instance._FriendTableID == friendTableID)
                         || (_Instance._GroupID > 0 && groupID > 0 && _Instance._GroupID == groupID)
                         || (!String.IsNullOrWhiteSpace(_Instance._RoomID) && !String.IsNullOrWhiteSpace(roomID) && _Instance._RoomID.Equals(roomID)))
                     {
                         _Instance.Focus();
                     }
                     else
                     {
                         CloseWindow();
                     }
                 }

                 if (_Instance == null)
                 {
                     _Instance = new WNChatLocationShare(friendTableID, groupID, roomID);
                     _Instance.Show();
                 }
             }));
        }

        public static void CloseWindow(object param = null)
        {
            if (_Instance != null)
            {
                if (_Instance._Watcher != null)
                {
                    _Instance._Watcher.Stop();
                    _Instance._Watcher.PositionChanged -= _Instance.watcher_PositionChanged;
                    _Instance._Watcher = null;
                }
                _Instance.Owner = null;
                _Instance.Close();
                _Instance.mapBrowser.Dispose();
                _Instance = null;
            }
        }

        private void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            if (this._Watcher != null)
            {
                this.ShowMapUsingLatLng((float)e.Position.Location.Latitude, (float)e.Position.Location.Longitude);
                this._Watcher.Stop();
                this._Watcher.PositionChanged -= watcher_PositionChanged;
            }
        }

        private void OnChatSend(object param)
        {
            ChatHelpers.AddForwardOrShareMessage(this._FriendTableID, this._GroupID, this._RoomID, MessageType.LOCATION_MESSAGE, ChatJSONParser.MakeLocationMessage(_Latitude, _Longitude, Place), true, null);
            CloseWindow();
        }

        private void ShowMapUsingLatLng(float lat, float lng)
        {
            try
            {
                if (lat == 0 && lng == 0)
                {
                    HelperMethods.GetLatLongByCurrentTimeZone(out lat, out lng);
                }

                string html = String.Empty;
                using (System.IO.Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("View.UI.Chat.ShareLocation.Templete"))
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }

                html = html.Replace("[zoom]", "12");
                html = html.Replace("[geoCode]", lat.ToString() + ", " + lng.ToString());
                mapBrowser.DocumentText = html;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMapUsingLatLng() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Utility Method
    }

    // Object used for communication from JS -> WPF
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public class HtmlInteropInternal
    {
        Func<bool, float, float, string, string, int> _DelEvent = null;

        public HtmlInteropInternal(Func<bool, float, float, string, string, int> delEvent)
        {
            this._DelEvent = delEvent;
        }

        public void endDragMarkerCS(float Lat, float Lng, string place)
        {
            if (_DelEvent != null)
            {
                _DelEvent((Lat > 0 || Lng > 0) && !String.IsNullOrWhiteSpace(place), Lat, Lng, place, ChatHelpers.ConvertLatLongToDMS(Lat, Lng));
            }
        }
    }
}

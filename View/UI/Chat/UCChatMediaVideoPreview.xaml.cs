﻿using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Converter;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatMediaVideoPreview.xaml
    /// </summary>
    public partial class UCChatMediaVideoPreview : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatMediaVideoPreview).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private MessageModel _MessageModel = null;
        private Visibility _IsActionVisible = Visibility.Collapsed;
        private Visibility _IsProgressVisible = Visibility.Collapsed;
        private double _RotateAngle = 0;
        private ICommand _OnPlayerActionCommand = null;
        private DispatcherTimer _ProgressTimer = null;

        public UCChatMediaVideoPreview()
        {
            InitializeComponent();
            this._ProgressTimer = new DispatcherTimer();
            this._ProgressTimer.Interval = TimeSpan.FromSeconds(0.25);
        }

        #region Event Handler

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (e.NewValue == null)
                    return;

                UCChatMediaVideoPreview panel = (UCChatMediaVideoPreview)d;
                if ((MessageType)e.NewValue == MessageType.DELETE_MESSAGE)
                {
                    panel.IsActionVisible = Visibility.Collapsed;
                    panel.IsProgressVisible = Visibility.Collapsed;
                    panel._ProgressTimer.Tick -= panel.ProgressTimer_Tick;
                    panel._ProgressTimer.Stop();
                    panel.mePlayer.MediaOpened -= panel.mePlayer_MediaOpened;
                    panel.mePlayer.MediaEnded -= panel.mePlayer_MediaEnded;
                    panel.mePlayer.ClearValue(System.Windows.Controls.MediaElement.SourceProperty);
                    panel.mePlayer.Source = null;
                }
                else
                {
                    panel._ProgressTimer.Tick += panel.ProgressTimer_Tick;
                    panel.mePlayer.MediaOpened += panel.mePlayer_MediaOpened;
                    MultiBinding sourceBinding = new MultiBinding { Converter = new VideoPreviewConverter() };
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("MessageModel"),
                    });
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("MessageModel.IsFileOpened"),
                    });
                    panel.mePlayer.SetBinding(System.Windows.Controls.MediaElement.SourceProperty, sourceBinding);

                    if (!panel.MessageModel.IsFileOpened && !panel.MessageModel.IsDownloadRunning)
                    {
                        panel.MessageModel.ViewModel.IsForceDownload = false;
                        panel.MessageModel.IsDownloadRunning = true;
                        panel.MessageModel.ViewModel.DownloadPercentage = 0;
                        ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.AddItem(new ChatDownloadProcessor(panel.MessageModel));
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: PropertyChangedCallback() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mePlayer_MediaOpened(object sender, EventArgs e)
        {
            try
            {
                this.mePlayer.MediaOpened -= mePlayer_MediaOpened;
                this.mePlayer.LoadedBehavior = MediaState.Manual;

                if (this.MessageModel.IsSecretChat && this.MessageModel.FromFriend)
                {
                    this.IsActionVisible = Visibility.Collapsed;
                    if (this.MessageModel.ViewModel.LastViewDate > 0)
                    {
#if MESSAGE_STATUS_LOG
                        log.Debug("*** mePlayer_MediaOpened => Last View Date = " + this.MessageModel.ViewModel.LastViewDate);
#endif
                        long diff = (ChatService.GetServerTime() - this.MessageModel.ViewModel.LastViewDate) / 1000;
                        long temp = this.MessageModel.OffsetPosition + diff;

                        if (diff > 0 && temp < this.MessageModel.Timeout)
                        {
#if MESSAGE_STATUS_LOG
                            log.Debug("*** mePlayer_MediaOpened => Need To Play = " + temp + " Seconds");
#endif
                            this.MessageModel.OffsetPosition = (int)temp;
                            this.mePlayer.Position = TimeSpan.FromSeconds(this.MessageModel.OffsetPosition);
                        }
                        else
                        {
#if MESSAGE_STATUS_LOG
                            log.Debug("*** mePlayer_MediaOpened => Time out exit ");
#endif
                            this.MessageModel.MultiMediaState = StatusConstants.MEDIA_INIT_STATE;
                            ChatHelpers.DeleteSecretChat(this.MessageModel);
                            return;
                        }
                    }
                    else
                    {
#if MESSAGE_STATUS_LOG
                        log.Debug("*** mePlayer_MediaOpened => First Play ");
#endif
                        this.MessageModel.OffsetPosition = 0;
                        this.mePlayer.Position = TimeSpan.FromSeconds((double)this.MessageModel.OffsetPosition);
                        ChatHelpers.SendViewPlayedSatatus(this.MessageModel);
                    }
                }
                else
                {
                    this.IsActionVisible = Visibility.Visible;
                    this.MessageModel.OffsetPosition = this.MessageModel.OffsetPosition >= this.MessageModel.Duration ? 0 : this.MessageModel.OffsetPosition;
                    this.mePlayer.Position = TimeSpan.FromSeconds((double)this.MessageModel.OffsetPosition);
                }

                this.IsProgressVisible = Visibility.Visible;
                this.mePlayer.MediaEnded += mePlayer_MediaEnded;
                this._ProgressTimer.Start();
                this.mePlayer.Play();
                this.MessageModel.MultiMediaState = StatusConstants.MEDIA_PLAY_STATE;
            }
            catch (Exception ex)
            {
                log.Error("Error: mePlayer_MediaOpened() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mePlayer_MediaEnded(object sender, EventArgs e)
        {
            try
            {
                new Thread(() =>
                {
                    Thread.Sleep(250);
#if MESSAGE_STATUS_LOG
                    log.Debug("*** mePlayer_MediaEnded => Play Completed ");
#endif
                    this._ProgressTimer.Stop();
                    this.MessageModel.MultiMediaState = StatusConstants.MEDIA_INIT_STATE;

                    if (this.MessageModel.IsSecretChat && this.MessageModel.FromFriend)
                    {
                        ChatHelpers.DeleteSecretChat(this.MessageModel);
                    }
                }).Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ProgressTimer_Tick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ProgressTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (!this.mePlayer.NaturalDuration.HasTimeSpan)
                    return;

                this.MessageModel.OffsetPosition = (int)this.mePlayer.Position.TotalSeconds;
            }
            catch (Exception ex)
            {
                log.Error("Error: ProgressTimer_Tick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void InitDataContext(MessageModel model)
        {
            try
            {
                this.SetPauseToPlayingVideo();
                this.StartVideoDeleteTimer();
                this.ClearValue(MsgTypeProperty);
                this.MessageModel = model;
                this.StopVideoDeleteTimer();
                if (this.DataContext == null)
                {
                    this.DataContext = this;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: InitDataContext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void InitView()
        {
            try
            {
                if (MessageModel.MessageType == MessageType.DELETE_MESSAGE)
                    return;

                Binding msgTypeBinding = new Binding
                {
                    Path = new PropertyPath("MessageModel.MessageType"),
                };
                SetBinding(MsgTypeProperty, msgTypeBinding);
            }
            catch (Exception ex)
            {
                log.Error("Error: InitView() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetPauseToPlayingVideo()
        {
            if (MessageModel != null && MessageModel.MessageType != MessageType.DELETE_MESSAGE && this.mePlayer.LoadedBehavior == MediaState.Manual && this.MessageModel.MultiMediaState == StatusConstants.MEDIA_PLAY_STATE)
            {
#if MESSAGE_STATUS_LOG
                log.Debug("*** SetPauseToPlayingVideo => Pause ");
#endif
                this.MessageModel.MultiMediaState = StatusConstants.MEDIA_PAUSE_STATE;
                this._ProgressTimer.Stop();
                this.mePlayer.Stop();
                this.MessageModel.ViewModel.LastViewDate = ChatService.GetServerTime();
                this.mePlayer.LoadedBehavior = MediaState.Play;
            }
        }

        public void StartVideoDeleteTimer()
        {
            try
            {
                if (MessageModel != null && MessageModel.MessageType != MessageType.DELETE_MESSAGE && MessageModel.IsSecretChat && MessageModel.FromFriend)
                {
#if MESSAGE_STATUS_LOG
                    log.Debug("*** Start Video Delete Timer => " + MessageModel.Message);
#endif

                    int diff = MessageModel.Duration - MessageModel.OffsetPosition;
                    if (diff > 0)
                    {
                        MessageModel.ViewModel.CreateDeleteTimer(MessageModel);
                        MessageModel.ViewModel.SecretCount = diff;
                        MessageModel.ViewModel.SecretDeleteTimer.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: StartVideoDeleteTimer() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void StopVideoDeleteTimer()
        {
            try
            {
                if (MessageModel.IsSecretChat && MessageModel.FromFriend)
                {
#if MESSAGE_STATUS_LOG
                    log.Debug("*** Stop Video Delete Timer => " + MessageModel.Message);
#endif
                    MessageModel.ViewModel.DistroyDeleteTimer();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: StopVideoDeleteTimer() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPlayerAction(object param)
        {
            try
            {
                if (this.MessageModel.MultiMediaState == StatusConstants.MEDIA_PLAY_STATE)
                {
                    this.MessageModel.MultiMediaState = StatusConstants.MEDIA_PAUSE_STATE;
                    this._ProgressTimer.Stop();
                    this.mePlayer.Pause();
                    this.MessageModel.ViewModel.LastViewDate = ChatService.GetServerTime();
                }
                else
                {
                    this.MessageModel.MultiMediaState = StatusConstants.MEDIA_PLAY_STATE;
                    this._ProgressTimer.Start();
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        this.MessageModel.OffsetPosition = this.MessageModel.OffsetPosition >= this.MessageModel.Duration ? 0 : this.MessageModel.OffsetPosition;
                        this.mePlayer.Position = TimeSpan.FromSeconds((double)this.MessageModel.OffsetPosition);
                        this.mePlayer.Play();
                    }, DispatcherPriority.ApplicationIdle);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPlayerAction() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            try
            {
#if MESSAGE_STATUS_LOG
                log.Debug("*** Dispose => ");
#endif
                this.SetPauseToPlayingVideo();
                this.StartVideoDeleteTimer();
                this.ClearValue(MsgTypeProperty);
                this.MessageModel = null;
                this.DataContext = null;
                this._ProgressTimer = null;
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Property

        public MessageType MsgType
        {
            get { return (MessageType)GetValue(MsgTypeProperty); }
            set { SetValue(MsgTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MsgType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MsgTypeProperty = DependencyProperty.Register("MsgType", typeof(MessageType), typeof(UCChatMediaVideoPreview), new PropertyMetadata(MessageType.DELETE_MESSAGE, UCChatMediaVideoPreview.PropertyChangedCallback));

        public MessageModel MessageModel
        {
            get { return _MessageModel; }
            set
            {
                _MessageModel = value;
                this.OnPropertyChanged("MessageModel");
            }
        }

        public double RotateAngle
        {
            get { return _RotateAngle; }
            set
            {
                if (value == _RotateAngle)
                    return;

                _RotateAngle = value;
                this.OnPropertyChanged("RotateAngle");
            }
        }

        public Visibility IsActionVisible
        {
            get { return _IsActionVisible; }
            set
            {
                if (value == _IsActionVisible)
                    return;

                _IsActionVisible = value;
                this.OnPropertyChanged("IsActionVisible");
            }
        }

        public Visibility IsProgressVisible
        {
            get { return _IsProgressVisible; }
            set
            {
                if (value == _IsProgressVisible)
                    return;

                _IsProgressVisible = value;
                this.OnPropertyChanged("IsProgressVisible");
            }
        }

        public ICommand OnPlayerActionCommand
        {
            get
            {
                if (_OnPlayerActionCommand == null)
                {
                    _OnPlayerActionCommand = new RelayCommand((param) => OnPlayerAction(param));
                }
                return _OnPlayerActionCommand;
            }
        }

        #endregion Property

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatMediaPreviewWrapper.xaml
    /// </summary>
    public partial class UCChatMediaPreviewWrapper : UserControl
    {
        private static UCChatMediaPreviewWrapper _Instance;
        public static UCChatMediaPreview ChildInstance;

        public static UCChatMediaPreviewWrapper Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new UCChatMediaPreviewWrapper();
                    UCGuiRingID.Instance.AddUserControlInMainGrid(_Instance);
                }
                return _Instance;
            }
        }

        public UCChatMediaPreviewWrapper()
        {
            InitializeComponent();
        }

        public void Show(MessageModel model)
        {
            if (ChildInstance == null)
            {
                ChildInstance = new UCChatMediaPreview();
            }
            this.Content = ChildInstance;
            ChildInstance.Show(model);
        }

        public void Hide()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (ChildInstance != null)
                {
                    ChildInstance.Dispose();
                    ChildInstance = null;
                }
                this.Content = null;
                UCGuiRingID.Instance.RemoveUserControlFromMainGrid(_Instance);
                _Instance = null;
            }, DispatcherPriority.Send);
        }

        public static bool Visible
        {
            get { return ChildInstance != null && ChildInstance.IsVisible; }
        }

        public static string PacketID
        {
            get { return ChildInstance != null && ChildInstance._PacketID != null ? ChildInstance._PacketID : String.Empty; }
        }
    }

}

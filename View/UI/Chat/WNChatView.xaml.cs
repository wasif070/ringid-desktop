﻿using Auth.utility;
using log4net;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for WNChatView.xaml
    /// </summary>
    public partial class WNChatView : Window, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WNChatView).Name);

        private System.Timers.Timer _ActivateTimer = null;
        private bool _IsAlreadyOpened = false;
        private bool _IsFriendChat;
        private bool _IsGroupChat;
        private bool _IsRoomChat;
        private GroupInfoModel _GroupInfoModel;
        private UserBasicInfoModel _FriendBasicInfoModel;
        private RoomModel _RoomModel;

        public WNChatView(UCFriendChatCallPanel friendChatCallPanel)
        {
            this.IsFriendChat = true;
            this.FriendBasicInfoModel = friendChatCallPanel.FriendBasicInfoModel;
            this.Init(friendChatCallPanel);
        }

        public WNChatView(UCGroupChatCallPanel groupChatCallPanel)
        {
            this.IsGroupChat = true;
            this.GroupInfoModel = groupChatCallPanel.GroupInfoModel;
            this.Init(groupChatCallPanel);
        }

        public WNChatView(UCRoomChatCallPanel roomChatCallPanel)
        {
            this.IsRoomChat = true;
            this.RoomModel = roomChatCallPanel.RoomModel;
            this.Init(roomChatCallPanel);
        }

        private void Init(FrameworkElement child)
        {
            InitializeComponent();
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
            this.Content = child;
            this.Closing += WNChatView_Closing;
            this.DataContext = this;
        }

        public static bool ChatViewActivated()
        {
            try
            {
                bool isActivated = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.Values.Where(P => P.Parent != null && P.Parent is WNChatView && ((WNChatView)P.Parent).IsActive).FirstOrDefault() != null;
                if (isActivated) return true;

                isActivated = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.Values.Where(P => P.Parent != null && P.Parent is WNChatView && ((WNChatView)P.Parent).IsActive).FirstOrDefault() != null;
                if (isActivated) return true;

                isActivated = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.Values.Where(P => P.Parent != null && P.Parent is WNChatView && ((WNChatView)P.Parent).IsActive).FirstOrDefault() != null;
                if (isActivated) return true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ChatViewActivated() => " + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        public void AddActivateEvent()
        {
            this.Activated -= WNChatView_Activated;
            this.Activated += WNChatView_Activated;
        }

        public void RemoveActivateEvent()
        {
            this.Activated -= WNChatView_Activated;
        }

        private void WNChatView_Activated(object sender, EventArgs args)
        {
            if (_ActivateTimer == null)
            {
                _ActivateTimer = new System.Timers.Timer { AutoReset = false };
                _ActivateTimer.Interval = 3500;
                _ActivateTimer.Elapsed += (o, e) =>
                {
                    _ActivateTimer.Stop();
                    _ActivateTimer.Interval = 2000;
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        if (this.Content != null && this.IsActive)
                        {
                            if (IsFriendChat)
                            {
                                ChatHelpers.TriggerChatActivateEvent((UCFriendChatCallPanel)this.Content);
                            }
                            else if (IsGroupChat)
                            {
                                ChatHelpers.TriggerChatActivateEvent((UCGroupChatCallPanel)this.Content);
                            }
                            else if (IsRoomChat)
                            {
                                ChatHelpers.TriggerChatActivateEvent((UCRoomChatCallPanel)this.Content);
                            }
                        }
                    });
                };
            }
            _ActivateTimer.Stop();
            _ActivateTimer.Start();
        }

        public void ShowWindow()
        {
            //System.Windows.Application.Current.Dispatcher.BeginInvoke(() =>
            //{
            this.Show();
            //}, DispatcherPriority.Normal);
        }

        private void WNChatView_Closing(object sender, CancelEventArgs e)
        {
            this.Activated -= WNChatView_Activated;
            this.Closing -= WNChatView_Closing;
            if (IsFriendChat)
            {
                ((UCFriendChatCallPanel)this.Content).IsSeperateWindow = false;
                ((UCFriendChatCallPanel)this.Content).HideContactShareView();
                ((UCFriendChatCallPanel)this.Content).HideMediaPreview();
                ((UCFriendChatCallPanel)this.Content).HideChatInformationView();
            }
            else if(IsGroupChat)
            {
                ((UCGroupChatCallPanel)this.Content).IsSeperateWindow = false;
                ((UCGroupChatCallPanel)this.Content).HideContactShareView();
                ((UCGroupChatCallPanel)this.Content).HideMediaPreview();
                ((UCGroupChatCallPanel)this.Content).HideChatInformationView();
            }
            else if (IsRoomChat)
            {
                ((UCRoomChatCallPanel)this.Content).IsSeperateWindow = false;
                ((UCRoomChatCallPanel)this.Content).HideContactShareView();
                ((UCRoomChatCallPanel)this.Content).HideMediaPreview();
                ((UCRoomChatCallPanel)this.Content).HideChatInformationView();
                ((UCRoomChatCallPanel)this.Content).HideChatLikeMemberList();
                ((UCRoomChatCallPanel)this.Content).HideRoomMemberList();
            }
           
            this.Content = null;
            this.Owner = null;
            this.FriendBasicInfoModel = null;
            this.GroupInfoModel = null;
            this.DataContext = null;
        }

        private new void Show()
        {
            if (this._IsAlreadyOpened == false)
            {
                if (IsFriendChat)
                {
                    ((UCFriendChatCallPanel)this.Content).IsSeperateWindow = true;
                    ((UCFriendChatCallPanel)this.Content).HideContactShareView();
                    ((UCFriendChatCallPanel)this.Content).HideMediaPreview();
                    ((UCFriendChatCallPanel)this.Content).HideChatInformationView();
                }
                if (IsGroupChat)
                {
                    ((UCGroupChatCallPanel)this.Content).IsSeperateWindow = true;
                    ((UCGroupChatCallPanel)this.Content).HideContactShareView();
                    ((UCGroupChatCallPanel)this.Content).HideMediaPreview();
                    ((UCGroupChatCallPanel)this.Content).HideChatInformationView();
                }
                else if (IsRoomChat)
                {
                    ((UCRoomChatCallPanel)this.Content).IsSeperateWindow = true;
                    ((UCRoomChatCallPanel)this.Content).HideContactShareView();
                    ((UCRoomChatCallPanel)this.Content).HideMediaPreview();
                    ((UCRoomChatCallPanel)this.Content).HideChatInformationView();
                    ((UCRoomChatCallPanel)this.Content).HideChatLikeMemberList();
                    ((UCRoomChatCallPanel)this.Content).HideRoomMemberList();
                }

                this.Activated += WNChatView_Activated;
                this.Topmost = true;
                base.Show();
                this.Left = (Screen.PrimaryScreen.WorkingArea.Right - this.ActualWidth) / 2;
                this.Top = (Screen.PrimaryScreen.WorkingArea.Bottom - this.ActualHeight) / 2 - 20;
                this.Topmost = false; // important
                this._IsAlreadyOpened = true;
                //this.Focus(); 
            }
            else
            {
                this.WindowState = WindowState == WindowState.Minimized ? WindowState.Normal : WindowState;
                this.Activate();
                this.Topmost = true;  // important
                this.Topmost = false; // important
                //this.Focus(); 
            }
        }

        #region Property

        public bool IsFriendChat
        {
            get { return _IsFriendChat; }
            set
            {
                _IsFriendChat = value;
                this.OnPropertyChanged("IsFriendChat");
            }
        }

        public bool IsGroupChat
        {
            get { return _IsGroupChat; }
            set
            {
                _IsGroupChat = value;
                this.OnPropertyChanged("IsGroupChat");
            }
        }

        public bool IsRoomChat
        {
            get { return _IsRoomChat; }
            set
            {
                _IsRoomChat = value;
                this.OnPropertyChanged("IsRoomChat");
            }
        }

        public UserBasicInfoModel FriendBasicInfoModel
        {
            get { return _FriendBasicInfoModel; }
            set { _FriendBasicInfoModel = value; }
        }

        public GroupInfoModel GroupInfoModel
        {
            get { return _GroupInfoModel; }
            set { _GroupInfoModel = value; }
        }

        public RoomModel RoomModel
        {
            get { return _RoomModel; }
            set { _RoomModel = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Property

    }
}

﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;
using View.Utility.Chat;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCSingleDateTitlePanel.xaml
    /// </summary>
    public partial class UCSingleDateTitlePanel : UserControl, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleDateTitlePanel).Name);
        private bool disposed = false;

        public UCSingleDateTitlePanel()
        {
            this.MinHeight = ChatHelpers.TIME_VIEW_HEIGHT;
            InitializeComponent();
        }

        ~UCSingleDateTitlePanel()
        {
            Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleDateTitlePanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleDateTitlePanel panel = ((UCSingleDateTitlePanel)o);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewValue != null && (bool)e.NewValue)
                {
                    model.PrevViewHeight = model.MinViewHeight;
                    panel.pnlDateTitle.SizeChanged += panel.OnSizeChanged;
                    panel.BindDateTimeInfo();
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                DrawShape(pnlDateTitle.ActualWidth, pnlDateTitle.ActualHeight);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSizeChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindDateTimeInfo()
        {
            txtDateTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Time"), Converter = new ChatDateConverter() });
        }

        private void ClearDateTimeInfo()
        {
            txtDateTitle.ClearValue(TextBlock.TextProperty);
        }

        private void DrawShape(double w, double h)
        {
            double width = (550 - w) / 2;
            leftArrow.Data = Geometry.Parse("M 0,12 L " + width.ToString() + ",11 L " + width.ToString() + ",12 L 0,12");
            rightArrow.Data = Geometry.Parse("M 550,12 L " + (550 - width).ToString() + ",11 L " + (550 - width).ToString() + ",12 L 550,12");
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    this.pnlDateTitle.SizeChanged -= OnSizeChanged;
                    if (disposing)
                    {
                        this.ClearDateTimeInfo();
                        this.leftArrow.Data = null;
                        this.rightArrow.Data = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Utility Method

    }
}

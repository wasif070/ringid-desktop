﻿using log4net;
using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using View.Utility;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for WNLocationViewer.xaml
    /// </summary>
    public partial class WNLocationViewer : Window, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WNLocationViewer).Name);

        private static WNLocationViewer _Instance = null;
        private ICommand _CloseCommand;
        private ICommand _ChatSendCommand;
        private int _Zoom = 15;
        private float _Latitude;
        private float _Longitude;

        public WNLocationViewer()
        {
            InitializeComponent();
            DataContext = this;
            this.Closed += WNWebcamCapture_Closed;
            this.MouseDown += delegate { DragMove(); };
        }

        #region Event Handler

        private void WNWebcamCapture_Closed(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            mapBrowser.Document.Focus();
        }

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            ShowMapUsingLatLng();
        }

        #endregion Event Handler

        #region Property

        public static WNLocationViewer Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new WNLocationViewer();
                }
                return _Instance;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => CloseWindow(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand ChatSendCommand
        {
            get
            {
                if (_ChatSendCommand == null)
                {
                    _ChatSendCommand = new RelayCommand((param) => OnChatSend(param));
                }
                return _ChatSendCommand;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Property

        #region Utility Method

        private new void Show()
        {
            this.Topmost = true;
            base.Show();

            this.Owner = System.Windows.Application.Current.MainWindow;
            System.Drawing.Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
            this.Left = (workingArea.Right - this.ActualWidth) / 2;
            this.Top = (workingArea.Bottom - this.ActualHeight) / 2;
        }

        public void ShowWindow(float latitude, float longitude)
        {
            _Latitude = latitude;
            _Longitude = longitude;

            App.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                this.Show();
            }));
        }

        private void OnChatSend(object param)
        {

        }

        public void CloseWindow(object param = null)
        {
            this.Owner = null;
            this.Close();
        }

        private void ShowMapUsingLatLng()
        {
            try
            {
                string html = String.Empty;
                using (System.IO.Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("View.UI.Chat.Location.Templete"))
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }

                html = html.Replace("[zoom]", _Zoom.ToString());
                html = html.Replace("[geoCode]", _Latitude.ToString() + ", " + _Longitude.ToString());
                mapBrowser.DocumentText = html;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMapUsingLatLng() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            mapBrowser.Dispose();
            _Instance = null;
        }

        #endregion Utility Method
    }

}

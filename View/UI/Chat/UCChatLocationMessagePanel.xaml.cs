﻿using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using View.BindingModels;
using View.Converter;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Recent;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatImageMessagePanel.xaml
    /// </summary>
    /// 

    public partial class UCChatLocationMessagePanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatStickerMessagePanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private bool disposed = false;

        public UCChatLocationMessagePanel()
        {
            this.MinHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)MessageType.LOCATION_MESSAGE][0] + ChatHelpers.CHAT_VIEW_HEIGHT[(int)MessageType.LOCATION_MESSAGE][1];
            InitializeComponent();
        }

        ~UCChatLocationMessagePanel()
        {
            Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCChatLocationMessagePanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        public static readonly DependencyProperty IsSameSenderProperty = DependencyProperty.Register("IsSameSender", typeof(bool), typeof(UCChatLocationMessagePanel), new PropertyMetadata(true, OnNewSenderChanged));

        public bool IsSameSender
        {
            get { return (bool)GetValue(IsSameSenderProperty); }
            set
            {
                SetValue(IsSameSenderProperty, value);
            }
        }

        public static readonly DependencyProperty IsPreviewOpenedProperty = DependencyProperty.Register("IsPreviewOpened", typeof(bool), typeof(UCChatLocationMessagePanel), new PropertyMetadata(false, OnPreviewChanged));

        public bool IsPreviewOpened
        {
            get { return (bool)GetValue(IsPreviewOpenedProperty); }
            set
            {
                SetValue(IsPreviewOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                UCChatLocationMessagePanel panel = ((UCChatLocationMessagePanel)sender);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewSize.Height > model.PrevViewHeight)
                {
                    model.PrevViewHeight = (int)e.NewSize.Height;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSizeChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatLocationMessagePanel panel = ((UCChatLocationMessagePanel)o);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewValue != null && (bool)e.NewValue)
                {
                    model.PrevViewHeight = model.MinViewHeight;
                    panel.SizeChanged += panel.OnSizeChanged;

                    if (!panel.IsSameSender)
                    {
                        panel.BindNewSender(model.Message.ViewModel.IsRoomChat);
                    }
                    else
                    {
                        panel.ClearNewSender();
                    }

                    if (panel.IsPreviewOpened)
                    {
                        panel.BindPreview();
                    }
                    else
                    {
                        panel.ClearPreview();
                    }

                    panel.BindMsgInfo(model.Message);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnNewSenderChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatLocationMessagePanel panel = ((UCChatLocationMessagePanel)o);
                if (panel.disposed) return;

                if (e.NewValue != null && !(bool)e.NewValue && panel.IsOpened)
                {
                    panel.BindNewSender(((RecentModel)panel.DataContext).Message.ViewModel.IsRoomChat);
                }
                else
                {
                    panel.ClearNewSender();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNewSenderChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnPreviewChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatLocationMessagePanel panel = ((UCChatLocationMessagePanel)o);
                if (panel.disposed) return;

                if (e.NewValue != null && (bool)e.NewValue && panel.IsOpened)
                {
                    panel.BindPreview();
                }
                else
                {
                    panel.ClearPreview();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPreviewChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindMsgInfo(MessageModel messageModel)
        {
            MultiBinding textBinding = new MultiBinding { Converter = new ChatStatusAndTimeConverter() };
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MessageDate")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.Status")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MessageType")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.PacketType")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FromFriend")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsRoomChat")
            });
            txtStatusAndTime.SetBinding(TextBlock.TextProperty, textBinding);

            if (!messageModel.FromFriend && !messageModel.ViewModel.IsRoomChat)
            {
                MultiBinding lastStatusBinding = new MultiBinding { Converter = new ChatLastStatusConverter() };
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.MessageDate")
                });
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.LastStatus.LastDeliveredDate")
                });
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.LastStatus.LastSeenDate")
                });
                icnLastStatus.SetBinding(Image.TagProperty, lastStatusBinding);
            }

            MultiBinding clipBinding = new MultiBinding { Converter = new ChatBubbolConverter() };
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Width"),
                RelativeSource = new RelativeSource(RelativeSourceMode.Self)
            });
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Height"),
                RelativeSource = new RelativeSource(RelativeSourceMode.Self)
            });
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FromFriend")
            });
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsSamePrevAndCurrID")
            });
            grdMessageContainer.SetBinding(Grid.ClipProperty, clipBinding);
        }

        private void ClearMsgInfo()
        {
            txtStatusAndTime.ClearValue(TextBlock.TextProperty);
            icnLastStatus.ClearValue(Image.TagProperty);
            icnLastStatus.Source = null;
            grdMessageContainer.ClearValue(Grid.ClipProperty);
            grdMessageContainer.Clip = null;
        }

        private void BindPreview()
        {
            MultiBinding sourceBinding = new MultiBinding { Converter = new LocationImageConverter() };
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.Latitude"),
            });
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.Longitude"),
            });
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.IsPreviewOpened"),
            });
            imgLocation.SetBinding(Image.SourceProperty, sourceBinding);
        }

        private void ClearPreview()
        {
            imgLocation.ClearValue(Image.SourceProperty);
            imgLocation.Source = null;
        }

        private void BindNewSender(bool isRoomChat)
        {
            if (isRoomChat)
            {
                imgProfile.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("Message.CurrentInstance"), Converter = new PathToChatProfileImageConverter() });
                txtFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.FullName") });
            }
            else
            {
                imgProfile.SetBinding(System.Windows.Controls.Image.SourceProperty, new Binding { Path = new PropertyPath("Message.FriendInfoModel.CurrentInstance"), Converter = new PathToChatProfileImageConverter() });
                txtFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.FriendInfoModel.FullName") });
            }
        }

        private void ClearNewSender()
        {
            imgProfile.ClearValue(Image.SourceProperty);
            imgProfile.Source = null;
            txtFullName.ClearValue(TextBlock.TextProperty);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    this.SizeChanged -= OnSizeChanged;
                    if (disposing)
                    {
                        this.ClearNewSender();
                        this.ClearMsgInfo();
                        this.ClearPreview();
                        this.imgProfile = null;
                        this.imgLocation = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

    }
}

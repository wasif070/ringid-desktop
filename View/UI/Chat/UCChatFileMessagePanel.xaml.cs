﻿using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.DAO;
using Models.Utility;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.Chat;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using imsdkwrapper;
using Models.Stores;
using View.UI.Profile.FriendProfile;
using View.Dictonary;
using View.UI.Group;
using View.UI.Room;
using View.Converter;
using System.Windows.Data;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatFileMessagePanel.xaml
    /// </summary>
    public partial class UCChatFileMessagePanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatFileMessagePanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private bool disposed = false;
        private static object ImageLocation = null;
        private static object RingIDSettings = null;

        static UCChatFileMessagePanel()
        {
            if (Application.Current != null) 
            { 
                ImageLocation = Application.Current.FindResource("ImageLocation");
                RingIDSettings = Application.Current.FindResource("RingIDSettings"); 
            }
        }

        public UCChatFileMessagePanel()
        {
            this.MinHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)MessageType.FILE_STREAM][0] + ChatHelpers.CHAT_VIEW_HEIGHT[(int)MessageType.FILE_STREAM][1];
            InitializeComponent();
        }

        ~UCChatFileMessagePanel()
        {
           Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCChatFileMessagePanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        public static readonly DependencyProperty IsSameSenderProperty = DependencyProperty.Register("IsSameSender", typeof(bool), typeof(UCChatFileMessagePanel), new PropertyMetadata(true, OnNewSenderChanged));

        public bool IsSameSender
        {
            get { return (bool)GetValue(IsSameSenderProperty); }
            set
            {
                SetValue(IsSameSenderProperty, value);
            }
        }

        public static readonly DependencyProperty IsPreviewOpenedProperty = DependencyProperty.Register("IsPreviewOpened", typeof(bool), typeof(UCChatFileMessagePanel), new PropertyMetadata(false, OnPreviewChanged));

        public bool IsPreviewOpened
        {
            get { return (bool)GetValue(IsPreviewOpenedProperty); }
            set
            {
                SetValue(IsPreviewOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                UCChatFileMessagePanel panel = ((UCChatFileMessagePanel)sender);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewSize.Height > model.PrevViewHeight)
                {
                    model.PrevViewHeight = (int)e.NewSize.Height;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSizeChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatFileMessagePanel panel = ((UCChatFileMessagePanel)o);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewValue != null && (bool)e.NewValue)
                {
                    model.PrevViewHeight = model.MinViewHeight;
                    panel.SizeChanged += panel.OnSizeChanged;
                    panel.grdMessageContainer.ContextMenuOpening += panel.ctxMenu_ContextMenuOpening;

                    if (!panel.IsSameSender)
                    {
                        panel.BindNewSender(model.Message.ViewModel.IsRoomChat);
                    }
                    else
                    {
                        panel.ClearNewSender();
                    }

                    panel.BindPreview();
                    panel.BindMsgInfo(model.Message);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnNewSenderChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatFileMessagePanel panel = ((UCChatFileMessagePanel)o);
                if (panel.disposed) return;

                if (e.NewValue != null && !(bool)e.NewValue && panel.IsOpened)
                {
                    panel.BindNewSender(((RecentModel)panel.DataContext).Message.ViewModel.IsRoomChat);
                }
                else
                {
                    panel.ClearNewSender();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNewSenderChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnPreviewChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatFileMessagePanel panel = ((UCChatFileMessagePanel)o);
                if (panel.disposed) return;

                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewValue != null && (bool)e.NewValue && panel.IsOpened)
                {
                    panel.BindPreview();
                }
                else
                {
                    panel.ClearPreview();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPreviewChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void prgsControl_ActionChanged(int action)
        {
            try
            {
                switch (action)
                {
                    case 1://Downloading
                        {
                            prgsControl.ToolTip = null;
                            prgsControl.Cursor = null;
                            prgsControl.Opacity = 1;
                            prgsControl.ActionCursor = Cursors.Hand;
                            prgsControl.SetBinding(CircularFileTransferProgressButton.ActionCommandProperty, new Binding { Path = new PropertyPath("ChatViewModel.OnFileActionClickCommand"), Source = RingIDSettings });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.ActionCommandParameterProperty, new Binding { Path = new PropertyPath("Message") });
                            prgsControl.ActionToolTip = "Click to Cancel";
                            prgsControl.SetBinding(CircularFileTransferProgressButton.PercentageProperty, new Binding { Path = new PropertyPath("Message.ViewModel.DownloadPercentage") });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.IconSourceProperty, new Binding { Path = new PropertyPath("FILE_TRANFER_CANCEL"), Source = ImageLocation });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.IconSourceHoverProperty, new Binding { Path = new PropertyPath("FILE_TRANFER_CANCEL_H"), Source = ImageLocation });
                        }
                        break;
                    case 2://Message Recieved
                        {
                            prgsControl.ToolTip = null;
                            prgsControl.Cursor = null;
                            prgsControl.Opacity = 1;
                            prgsControl.ActionCursor = Cursors.Hand;
                            prgsControl.SetBinding(CircularFileTransferProgressButton.ActionCommandProperty, new Binding { Path = new PropertyPath("ChatViewModel.OnFileActionClickCommand"), Source = RingIDSettings });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.ActionCommandParameterProperty, new Binding { Path = new PropertyPath("Message") });
                            prgsControl.ActionToolTip = "Click to Download";
                            prgsControl.ClearValue(CircularFileTransferProgressButton.PercentageProperty);
                            prgsControl.Percentage = 0;
                            prgsControl.SetBinding(CircularFileTransferProgressButton.IconSourceProperty, new Binding { Path = new PropertyPath("CHAT_BG_DOWNLOAD"), Source = ImageLocation });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.IconSourceHoverProperty, new Binding { Path = new PropertyPath("CHAT_BG_DOWNLOAD_H"), Source = ImageLocation });
                        }
                        break;
                    case 3://Message Send
                        {
                            prgsControl.ToolTip = "Click to Open";
                            prgsControl.Cursor = Cursors.Hand;
                            prgsControl.Opacity = 1;
                            prgsControl.ActionCursor = Cursors.Hand;
                            prgsControl.SetBinding(CircularFileTransferProgressButton.ActionCommandProperty, new Binding { Path = new PropertyPath("ChatViewModel.OnFileActionClickCommand"), Source = RingIDSettings });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.ActionCommandParameterProperty, new Binding { Path = new PropertyPath("Message") });
                            prgsControl.ActionToolTip = "Click to Cancel";
                            prgsControl.SetBinding(CircularFileTransferProgressButton.PercentageProperty, new Binding { Path = new PropertyPath("Message.ViewModel.UploadPercentage") });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.IconSourceProperty, new Binding { Path = new PropertyPath("FILE_TRANFER_CANCEL"), Source = ImageLocation });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.IconSourceHoverProperty, new Binding { Path = new PropertyPath("FILE_TRANFER_CANCEL_H"), Source = ImageLocation });
                        }
                        break;
                    case 4://Send/Receive Completed
                        {
                            prgsControl.ToolTip = "Click to Open";
                            prgsControl.Cursor = Cursors.Hand;
                            prgsControl.Opacity = 1;
                            prgsControl.ActionCursor = null;
                            prgsControl.ClearValue(CircularFileTransferProgressButton.ActionCommandProperty);
                            prgsControl.ClearValue(CircularFileTransferProgressButton.ActionCommandParameterProperty);
                            prgsControl.ActionToolTip = null;
                            prgsControl.ClearValue(CircularFileTransferProgressButton.PercentageProperty);
                            prgsControl.Percentage = 100;
                            MultiBinding iconSourceBinding = new MultiBinding { Converter = new ChatFileIconConverter() };
                            iconSourceBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message.Message")
                            });
                            iconSourceBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message.PacketID")
                            });
                            iconSourceBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message.SenderTableID")
                            });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.IconSourceProperty, iconSourceBinding);
                            prgsControl.ClearValue(CircularFileTransferProgressButton.IconSourceHoverProperty);
                            prgsControl.IconSourceHover = null;
                        }
                        break;
                    case 5://Send/Receive Cancelled
                    case 6://File Moved
                        {
                            prgsControl.ToolTip = null;
                            prgsControl.Cursor = null;
                            prgsControl.Opacity = 0.3;
                            prgsControl.ActionCursor = null;
                            prgsControl.ClearValue(CircularFileTransferProgressButton.ActionCommandProperty);
                            prgsControl.ClearValue(CircularFileTransferProgressButton.ActionCommandParameterProperty);
                            prgsControl.ActionToolTip = null;
                            prgsControl.ClearValue(CircularFileTransferProgressButton.PercentageProperty);
                            prgsControl.Percentage = 100;
                            MultiBinding iconSourceBinding = new MultiBinding { Converter = new ChatFileIconConverter() };
                            iconSourceBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message.Message")
                            });
                            iconSourceBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message.PacketID")
                            });
                            iconSourceBinding.Bindings.Add(new Binding
                            {
                                Path = new PropertyPath("Message.SenderTableID")
                            });
                            prgsControl.SetBinding(CircularFileTransferProgressButton.IconSourceProperty, iconSourceBinding);
                            prgsControl.ClearValue(CircularFileTransferProgressButton.IconSourceHoverProperty);
                            prgsControl.IconSourceHover = null;
                        }
                        break;
                    default:
                        {
                            prgsControl.ToolTip = null;
                            prgsControl.Cursor = null;
                            prgsControl.Opacity = 0;
                            prgsControl.ActionCursor = null;
                            prgsControl.ClearValue(CircularFileTransferProgressButton.ActionCommandProperty);
                            prgsControl.ClearValue(CircularFileTransferProgressButton.ActionCommandParameterProperty);
                            prgsControl.ActionToolTip = null;
                            prgsControl.ClearValue(CircularFileTransferProgressButton.PercentageProperty);
                            prgsControl.Percentage = 0;
                            prgsControl.ClearValue(CircularFileTransferProgressButton.IconSourceProperty);
                            prgsControl.IconSource = null;
                            prgsControl.ClearValue(CircularFileTransferProgressButton.IconSourceHoverProperty);
                            prgsControl.IconSourceHover = null;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: prgsControl_ActionChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ctxMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            try
            {
                if (this.DataContext == null) return;
                RecentModel model = (RecentModel)this.DataContext;

                string filePath = ChatHelpers.GetChatFilePath(model.Message.Message, model.Message.MessageType, model.Message.PacketID, model.Message.LinkImageUrl, model.Message.SenderTableID);
                FileInfo fi = new FileInfo(filePath);
                model.Message.IsFileOpened = (fi.Exists && fi.Length == model.Message.FileSize);
            }
            catch (Exception ex)
            {
                log.Error("Error: ctxMenu_ContextMenuOpening() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindMsgInfo(MessageModel messageModel)
        {
            MultiBinding textBinding = new MultiBinding { Converter = new ChatStatusAndTimeConverter() };
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MessageDate")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.Status")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MessageType")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.PacketType")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FromFriend")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsRoomChat")
            });
            txtStatusAndTime.SetBinding(TextBlock.TextProperty, textBinding);

            if (!messageModel.FromFriend && !messageModel.ViewModel.IsRoomChat)
            {
                MultiBinding lastStatusBinding = new MultiBinding { Converter = new ChatLastStatusConverter() };
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.MessageDate")
                });
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.LastStatus.LastDeliveredDate")
                });
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.LastStatus.LastSeenDate")
                });
                icnLastStatus.SetBinding(Image.TagProperty, lastStatusBinding);
            }

            MultiBinding clipBinding = new MultiBinding { Converter = new ChatBubbolConverter() };
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Width"),
                RelativeSource = new RelativeSource(RelativeSourceMode.Self)
            });
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Height"),
                RelativeSource = new RelativeSource(RelativeSourceMode.Self)
            });
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FromFriend")
            });
            clipBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsSamePrevAndCurrID")
            });
            grdMessageContainer.SetBinding(Grid.ClipProperty, clipBinding);

            txtFileName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.Message"), Converter = new ChatShortFileNameConverter() });

            MultiBinding statusTextBinding = new MultiBinding { Converter = new ChatFileStatusConverter() };
            statusTextBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FileStatus")
            });
            statusTextBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FileSize")
            });
            statusTextBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FromFriend")
            });
            statusTextBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.FileProgressInSize")
            });
            statusTextBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsFileQueued")
            });
            txtFileStatus.SetBinding(TextBlock.TextProperty, statusTextBinding);
        }

        private void ClearMsgInfo()
        {
            txtStatusAndTime.ClearValue(TextBlock.TextProperty);
            icnLastStatus.ClearValue(Image.TagProperty);
            icnLastStatus.Source = null;
            grdMessageContainer.ClearValue(Grid.ClipProperty);
            grdMessageContainer.Clip = null;
            txtFileName.ClearValue(TextBlock.TextProperty);
            txtFileStatus.ClearValue(TextBlock.TextProperty);
        }

        private void BindPreview()
        {
            prgsControl.ActionChanged += prgsControl_ActionChanged;
            MultiBinding actionBinding = new MultiBinding { Converter = new ChatFileActionConverter() };
            actionBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FileStatus"),
            });
            actionBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FromFriend"),
            });
            actionBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.IsDownloadRunning"),
            });
            prgsControl.SetBinding(CircularFileTransferProgressButton.ActionProperty, actionBinding);
        }

        private void ClearPreview()
        {
            prgsControl.ActionChanged -= prgsControl_ActionChanged;
            prgsControl.ClearValue(CircularFileTransferProgressButton.ActionProperty);
            prgsControl_ActionChanged(0);
        }

        private void BindNewSender(bool isRoomChat)
        {
            if (isRoomChat)
            {
                imgProfile.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("Message.CurrentInstance"), Converter = new PathToChatProfileImageConverter() });
                txtFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.FullName") });
            }
            else
            {
                imgProfile.SetBinding(System.Windows.Controls.Image.SourceProperty, new Binding { Path = new PropertyPath("Message.FriendInfoModel.CurrentInstance"), Converter = new PathToChatProfileImageConverter() });
                txtFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.FriendInfoModel.FullName") });
            }
        }

        private void ClearNewSender()
        {
            imgProfile.ClearValue(Image.SourceProperty);
            imgProfile.Source = null;
            txtFullName.ClearValue(TextBlock.TextProperty);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    this.SizeChanged -= OnSizeChanged;
                    this.grdMessageContainer.ContextMenuOpening -= this.ctxMenu_ContextMenuOpening;
                    if (disposing)
                    {
                        this.ClearNewSender();
                        this.ClearMsgInfo();
                        this.ClearPreview();
                        this.imgProfile = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

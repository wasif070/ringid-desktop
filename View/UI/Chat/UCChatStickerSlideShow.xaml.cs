<<<<<<< HEAD
﻿using Auth.Service.RingMarket;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.RingMarket.MenuMarketSticker;
using View.Utility;
using View.Utility.RingMarket;
using View.ViewModel;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatStickerSlideShow.xaml
    /// </summary>
    public partial class UCChatStickerSlideShow : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatStickerSlideShow).Name);

        private ICommand _CloseCommand;
        private ICommand _StickerDownloadCommand;
        private ICommand _StickerImageClickedCommand;

        private VirtualizingStackPanel _ImageItemContainer = null;
        private Storyboard _Storyboard = null;

        private bool _IsThreadAlreadyRunning = false;
        int _NextIndex = 0;

        public delegate void OnSelectHandler(MarkertStickerImagesModel imageModel);
        public event OnSelectHandler OnSelect;

        public UCChatStickerSlideShow()
        {
            InitializeComponent();
            ImagesListBox.SelectionChanged += ImagesListBox_SelectionChanged;
            this.DataContext = this;
        }

        #region Event Handler

        private void HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as VirtualizingStackPanel;
        }

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            ImagesListBox.ItemsSource = RingIDViewModel.Instance.MarketStickerCategoryList;
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            ImagesListBox.IsHitTestVisible = true;
            if (_Storyboard != null)
            {
                _Storyboard.Completed -= Storyboard_Completed;
                _Storyboard = null;
            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void ImagesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                MarketStickerCategoryModel model = ImagesListBox.SelectedItem as MarketStickerCategoryModel;
                if (!(CategoryModel != null && model.StickerCategoryID == CategoryModel.StickerCategoryID))
                {
                    if (model.Downloaded == true)
                    {
                        MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == model.StickerCategoryID).FirstOrDefault();
                        RingMarketStickerLoadUtility.LaodAndDownLoadStickerImages(model, ctgDTO);
                    }
                    Scroll.ScrollToVerticalOffset(0);
                    CategoryModel = model;
                }
                else
                {
                    return;
                }

                if (ImagesListBox.SelectedIndex > -1)
                {
                    int prevIndex = (int)Math.Abs(_ImageItemContainer.Margin.Left) / 54 - 1;
                    _NextIndex = (int)Math.Abs(_ImageItemContainer.Margin.Left) / 54 + 12;

                    if (ImagesListBox.SelectedIndex == prevIndex + 1)
                    {
                        if (prevIndex > 2)
                        {
                            AnimationOnArrowClicked(4 * 54, 0.20);
                        }
                        else if (prevIndex > 1)
                        {
                            AnimationOnArrowClicked(3 * 54, 0.15);
                        }
                        else if (prevIndex > 0)
                        {
                            AnimationOnArrowClicked(2 * 54, 0.10);
                        }
                        else if (prevIndex > -1)
                        {
                            AnimationOnArrowClicked(1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == prevIndex + 2)
                    {
                        if (prevIndex > 1)
                        {
                            AnimationOnArrowClicked(3 * 54, 0.15);
                        }
                        else if (prevIndex > 0)
                        {
                            AnimationOnArrowClicked(2 * 54, 0.10);
                        }
                        else if (prevIndex > -1)
                        {
                            AnimationOnArrowClicked(1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == prevIndex + 3)
                    {
                        if (prevIndex > 0)
                        {
                            AnimationOnArrowClicked(2 * 54, 0.10);
                        }
                        else if (prevIndex > -1)
                        {
                            AnimationOnArrowClicked(1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == prevIndex + 4)
                    {
                        if (prevIndex > -1)
                        {
                            AnimationOnArrowClicked(1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == _NextIndex - 1)
                    {
                        if (ImagesListBox.Items.Count - _NextIndex > 3)
                        {
                            AnimationOnArrowClicked(-4 * 54, 0.20);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 2)
                        {
                            AnimationOnArrowClicked(-3 * 54, 0.15);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 1)
                        {
                            AnimationOnArrowClicked(-2 * 54, 0.10);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 0)
                        {
                            AnimationOnArrowClicked(-1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == _NextIndex - 2)
                    {
                        if (ImagesListBox.Items.Count - _NextIndex > 2)
                        {
                            AnimationOnArrowClicked(-3 * 54, 0.15);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 1)
                        {
                            AnimationOnArrowClicked(-2 * 54, 0.10);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 0)
                        {
                            AnimationOnArrowClicked(-1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == _NextIndex - 3)
                    {
                        if (ImagesListBox.Items.Count - _NextIndex > 1)
                        {
                            AnimationOnArrowClicked(-2 * 54, 0.10);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 0)
                        {
                            AnimationOnArrowClicked(-1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == _NextIndex - 4)
                    {
                        if (ImagesListBox.Items.Count - _NextIndex > 0)
                        {
                            AnimationOnArrowClicked(-1 * 54, 0.05);
                        }
                    }

                    if (_IsThreadAlreadyRunning == false && DefaultSettings.HAVE_NO_MORE_STICKERS == false && ImagesListBox.SelectedIndex >= (ImagesListBox.Items.Count - 9))
                    {
                        Thread t = new Thread(LoadMoreStickerCategory);
                        t.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ImagesListBox_SelectionChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        private void LoadMoreStickerCategory()
        {
            int allCount = StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST.Count;
            int startIndex = allCount, categoryLimit = 8, collectionLimit = 0, stickerType = StickerMarketConstants.STICKER_TYPE_ALL;
            List<MarketStickerCategoryDTO> ctgDTOList = RingMarketStickerService.Instance.GetMoreStickerCategoriesOfSameType(startIndex, categoryLimit, collectionLimit, stickerType);
            if (ctgDTOList != null && ctgDTOList.Count > 0)
            {
                List<MarketStickerCategoryModel> ctgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                foreach (MarketStickerCategoryDTO ctgDTO in ctgDTOList)
                {
                    if (ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault() == null)
                    {
                        RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                        Thread.Sleep(10);
                    }
                }
            }
            else
            {
                DefaultSettings.HAVE_NO_MORE_STICKERS = true;
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ImagesListBox.IsHitTestVisible = false;
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(_ImageItemContainer.Margin.Left, 0, 0, 0);
            MarginAnimation.To = new Thickness((_ImageItemContainer.Margin.Left + target), 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            _Storyboard = new Storyboard();
            _Storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(StackPanel.MarginProperty));
            _Storyboard.Completed += Storyboard_Completed;
            _Storyboard.Begin(_ImageItemContainer);
        }

        private void OnClose(object param)
        {
            if (OnSelect != null)
            {
                OnSelect(null);
            }
        }

        private void OnDownloadClicked(object parameter)
        {
            if (parameter != null && parameter is int)
            {
                int categoryID = (int)parameter;
                MarketStickerCategoryModel CategoryModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (CategoryModel != null && CategoryModel.Downloaded == false && CategoryModel.IsDownloadRunning == false)
                {
                    if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                    {
                        CategoryModel.DownloadPercentage = 0;
                        CategoryModel.IsDownloadRunning = true;
                        MarketStickerCategoryDTO ctgDTO;
                        if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(CategoryModel.StickerCategoryID, out ctgDTO))
                        {
                            RingMarketStickerLoadUtility.DownLoadStickerImages(ctgDTO);
                        }
                    }
                    else
                    {
                        CategoryModel.DownloadPercentage = 0;
                        CategoryModel.IsDownloadRunning = false;
                        UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                    }
                }
            }
        }

        public void OnStickerImageClickedClicked(object parameter)
        {
            if (OnSelect != null)
            {
                OnSelect((MarkertStickerImagesModel)parameter);
            }
        }

        public void Dispose()
        {
            if (_Storyboard != null)
            {
                _Storyboard.Completed -= Storyboard_Completed;
                _Storyboard = null;
            }
            ImagesListBox.SelectionChanged -= ImagesListBox_SelectionChanged;
            ImagesListBox.ItemsSource = null;
            CategoryModel = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods


        #region Property

        private MarketStickerCategoryModel _CategoryModel;
        public MarketStickerCategoryModel CategoryModel
        {
            get { return _CategoryModel; }
            set
            {
                if (value == _CategoryModel) return;
                _CategoryModel = value;
                OnPropertyChanged("CategoryModel");
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => OnClose(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand StickerDownloadCommand
        {
            get
            {
                if (_StickerDownloadCommand == null)
                {
                    _StickerDownloadCommand = new RelayCommand(param => OnDownloadClicked(param));
                }
                return _StickerDownloadCommand;
            }
        }

        public ICommand StickerImageClickedCommand
        {
            get
            {
                if (_StickerImageClickedCommand == null)
                {
                    _StickerImageClickedCommand = new RelayCommand(param => OnStickerImageClickedClicked(param));
                }
                return _StickerImageClickedCommand;
            }
        }


        #endregion Property

    }
}
=======
﻿using Auth.Service.RingMarket;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.RingMarket.MenuMarketSticker;
using View.Utility;
using View.Utility.RingMarket;
using View.ViewModel;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatStickerSlideShow.xaml
    /// </summary>
    public partial class UCChatStickerSlideShow : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatStickerSlideShow).Name);

        private ICommand _CloseCommand;
        private ICommand _StickerDownloadCommand;
        private ICommand _StickerImageClickedCommand;

        private VirtualizingStackPanel _ImageItemContainer = null;
        private Storyboard _Storyboard = null;

        private bool _IsThreadAlreadyRunning = false;
        int _NextIndex = 0;

        public delegate void OnSelectHandler(MarkertStickerImagesModel imageModel);
        public event OnSelectHandler OnSelect;

        public UCChatStickerSlideShow()
        {
            InitializeComponent();
            ImagesListBox.SelectionChanged += ImagesListBox_SelectionChanged;
            this.DataContext = this;
        }

        #region Event Handler

        private void HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as VirtualizingStackPanel;
        }

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            ImagesListBox.ItemsSource = RingIDViewModel.Instance.MarketStickerCategoryList;
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            ImagesListBox.IsHitTestVisible = true;
            if (_Storyboard != null)
            {
                _Storyboard.Completed -= Storyboard_Completed;
                _Storyboard = null;
            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void ImagesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                MarketStickerCategoryModel model = ImagesListBox.SelectedItem as MarketStickerCategoryModel;
                if (!(CategoryModel != null && model.StickerCategoryID == CategoryModel.StickerCategoryID))
                {
                    if (model.Downloaded == true)
                    {
                        MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == model.StickerCategoryID).FirstOrDefault();
                        RingMarketStickerLoadUtility.DownLoadStickerImages(model, ctgDTO, (isNointernet) =>
                        {
                            if (isNointernet)
                            {
                                model.DownloadPercentage = 0;
                                model.IsDownloadRunning = false;
                                UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                            }
                            return 0;
                        });
                    }
                    Scroll.ScrollToVerticalOffset(0);
                    CategoryModel = model;
                }
                else
                {
                    return;
                }

                if (ImagesListBox.SelectedIndex > -1)
                {
                    int prevIndex = (int)Math.Abs(_ImageItemContainer.Margin.Left) / 54 - 1;
                    _NextIndex = (int)Math.Abs(_ImageItemContainer.Margin.Left) / 54 + 12;

                    if (ImagesListBox.SelectedIndex == prevIndex + 1)
                    {
                        if (prevIndex > 2)
                        {
                            AnimationOnArrowClicked(4 * 54, 0.20);
                        }
                        else if (prevIndex > 1)
                        {
                            AnimationOnArrowClicked(3 * 54, 0.15);
                        }
                        else if (prevIndex > 0)
                        {
                            AnimationOnArrowClicked(2 * 54, 0.10);
                        }
                        else if (prevIndex > -1)
                        {
                            AnimationOnArrowClicked(1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == prevIndex + 2)
                    {
                        if (prevIndex > 1)
                        {
                            AnimationOnArrowClicked(3 * 54, 0.15);
                        }
                        else if (prevIndex > 0)
                        {
                            AnimationOnArrowClicked(2 * 54, 0.10);
                        }
                        else if (prevIndex > -1)
                        {
                            AnimationOnArrowClicked(1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == prevIndex + 3)
                    {
                        if (prevIndex > 0)
                        {
                            AnimationOnArrowClicked(2 * 54, 0.10);
                        }
                        else if (prevIndex > -1)
                        {
                            AnimationOnArrowClicked(1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == prevIndex + 4)
                    {
                        if (prevIndex > -1)
                        {
                            AnimationOnArrowClicked(1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == _NextIndex - 1)
                    {
                        if (ImagesListBox.Items.Count - _NextIndex > 3)
                        {
                            AnimationOnArrowClicked(-4 * 54, 0.20);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 2)
                        {
                            AnimationOnArrowClicked(-3 * 54, 0.15);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 1)
                        {
                            AnimationOnArrowClicked(-2 * 54, 0.10);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 0)
                        {
                            AnimationOnArrowClicked(-1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == _NextIndex - 2)
                    {
                        if (ImagesListBox.Items.Count - _NextIndex > 2)
                        {
                            AnimationOnArrowClicked(-3 * 54, 0.15);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 1)
                        {
                            AnimationOnArrowClicked(-2 * 54, 0.10);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 0)
                        {
                            AnimationOnArrowClicked(-1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == _NextIndex - 3)
                    {
                        if (ImagesListBox.Items.Count - _NextIndex > 1)
                        {
                            AnimationOnArrowClicked(-2 * 54, 0.10);
                        }
                        else if (ImagesListBox.Items.Count - _NextIndex > 0)
                        {
                            AnimationOnArrowClicked(-1 * 54, 0.05);
                        }
                    }
                    else if (ImagesListBox.SelectedIndex == _NextIndex - 4)
                    {
                        if (ImagesListBox.Items.Count - _NextIndex > 0)
                        {
                            AnimationOnArrowClicked(-1 * 54, 0.05);
                        }
                    }

                    if (_IsThreadAlreadyRunning == false && DefaultSettings.HAVE_NO_MORE_STICKERS == false && ImagesListBox.SelectedIndex >= (ImagesListBox.Items.Count - 9))
                    {
                        Thread t = new Thread(LoadMoreStickerCategory);
                        t.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ImagesListBox_SelectionChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        private void LoadMoreStickerCategory()
        {
            int allCount = StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST.Count;
            int startIndex = allCount, categoryLimit = 8, collectionLimit = 0, stickerType = StickerMarketConstants.STICKER_TYPE_ALL;
            List<MarketStickerCategoryDTO> ctgDTOList = RingMarketStickerService.Instance.GetMoreStickerCategoriesOfSameType(startIndex, categoryLimit, collectionLimit, stickerType);
            if (ctgDTOList != null && ctgDTOList.Count > 0)
            {
                List<MarketStickerCategoryModel> ctgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                foreach (MarketStickerCategoryDTO ctgDTO in ctgDTOList)
                {
                    if (ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault() == null)
                    {
                        RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                        Thread.Sleep(10);
                    }
                }
            }
            else
            {
                DefaultSettings.HAVE_NO_MORE_STICKERS = true;
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ImagesListBox.IsHitTestVisible = false;
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(_ImageItemContainer.Margin.Left, 0, 0, 0);
            MarginAnimation.To = new Thickness((_ImageItemContainer.Margin.Left + target), 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            _Storyboard = new Storyboard();
            _Storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(StackPanel.MarginProperty));
            _Storyboard.Completed += Storyboard_Completed;
            _Storyboard.Begin(_ImageItemContainer);
        }

        private void OnClose(object param)
        {
            if (OnSelect != null)
            {
                OnSelect(null);
            }
        }

        private void OnDownloadClicked(object parameter)
        {
            if (parameter != null && parameter is int)
            {
                int categoryID = (int)parameter;
                MarketStickerCategoryModel CategoryModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (CategoryModel != null && CategoryModel.Downloaded == false && CategoryModel.IsDownloadRunning == false)
                {
                    if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                    {
                        CategoryModel.DownloadPercentage = 0;
                        CategoryModel.IsDownloadRunning = true;
                        MarketStickerCategoryDTO ctgDTO;
                        if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(CategoryModel.StickerCategoryID, out ctgDTO))
                        {
                            RingMarketStickerLoadUtility.DownLoadStickerImages(CategoryModel, ctgDTO, (isNointernet) =>
                            {
                                if (isNointernet)
                                {
                                    CategoryModel.DownloadPercentage = 0;
                                    CategoryModel.IsDownloadRunning = false;
                                    UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                                }
                                return 0;
                            });
                        }
                    }
                    else
                    {
                        CategoryModel.DownloadPercentage = 0;
                        CategoryModel.IsDownloadRunning = false;
                        UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                    }
                }
            }
        }

        public void OnStickerImageClickedClicked(object parameter)
        {
            if (OnSelect != null)
            {
                OnSelect((MarkertStickerImagesModel)parameter);
            }
        }

        public void Dispose()
        {
            if (_Storyboard != null)
            {
                _Storyboard.Completed -= Storyboard_Completed;
                _Storyboard = null;
            }
            ImagesListBox.SelectionChanged -= ImagesListBox_SelectionChanged;
            ImagesListBox.ItemsSource = null;
            CategoryModel = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods


        #region Property

        private MarketStickerCategoryModel _CategoryModel;
        public MarketStickerCategoryModel CategoryModel
        {
            get { return _CategoryModel; }
            set
            {
                if (value == _CategoryModel) return;
                _CategoryModel = value;
                OnPropertyChanged("CategoryModel");
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => OnClose(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand StickerDownloadCommand
        {
            get
            {
                if (_StickerDownloadCommand == null)
                {
                    _StickerDownloadCommand = new RelayCommand(param => OnDownloadClicked(param));
                }
                return _StickerDownloadCommand;
            }
        }

        public ICommand StickerImageClickedCommand
        {
            get
            {
                if (_StickerImageClickedCommand == null)
                {
                    _StickerImageClickedCommand = new RelayCommand(param => OnStickerImageClickedClicked(param));
                }
                return _StickerImageClickedCommand;
            }
        }


        #endregion Property

    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

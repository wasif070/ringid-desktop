﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Markup;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for CircularProgressBar.xaml
    /// </summary>
    public partial class CircularProgressButton : UserControl
    {
        public delegate void ActionChangedHandler(int action);
        public event ActionChangedHandler ActionChanged;

        public CircularProgressButton()
        {
            InitializeComponent();
            Angle = (Percentage * 360) / 100;
            RenderArc();
        }

        public int Action
        {
            get { return (int)GetValue(ActionProperty); }
            set { SetValue(ActionProperty, value); }
        }

        public int Radius
        {
            get { return (int)GetValue(RadiusProperty); }
            set { SetValue(RadiusProperty, value); }
        }

        public Brush SegmentColor
        {
            get { return (Brush)GetValue(SegmentColorProperty); }
            set { SetValue(SegmentColorProperty, value); }
        }

        public int StrokeThickness
        {
            get { return (int)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }

        public double Percentage
        {
            get { return (double)GetValue(PercentageProperty); }
            set { SetValue(PercentageProperty, value); }
        }

        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        public ImageSource IconSource
        {
            get { return (ImageSource)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }

        public ImageSource IconSourceHover
        {
            get { return (ImageSource)GetValue(IconSourceHoverProperty); }
            set { SetValue(IconSourceHoverProperty, value); }
        }

        public double IconWidth
        {
            get { return (double)GetValue(IconWidthProperty); }
            set { SetValue(IconWidthProperty, value); }
        }

        public double IconHeight
        {
            get { return (double)GetValue(IconHeightProperty); }
            set { SetValue(IconHeightProperty, value); }
        }

        public ICommand ActionCommand
        {
            get { return (ICommand)GetValue(ActionCommandProperty); }
            set { SetValue(ActionCommandProperty, value); }
        }

        public object ActionCommandParameter
        {
            get { return (object)GetValue(ActionCommandParameterProperty); }
            set { SetValue(ActionCommandParameterProperty, value); }
        }

        public object ActionToolTip
        {
            get { return (object)GetValue(ActionToolTipProperty); }
            set { SetValue(ActionToolTipProperty, value); }
        }

        public Cursor ActionCursor
        {
            get { return (Cursor)GetValue(ActionCursorProperty); }
            set { SetValue(ActionCursorProperty, value); }
        }

        public bool HasHoverImage
        {
            get { return (bool)GetValue(HasHoverImageProperty); }
            set { SetValue(HasHoverImageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Action.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActionProperty =
            DependencyProperty.Register("Action", typeof(int), typeof(CircularProgressButton), new PropertyMetadata(0, new PropertyChangedCallback(OnActionChanged)));

        // Using a DependencyProperty as the backing store for Percentage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PercentageProperty =
            DependencyProperty.Register("Percentage", typeof(double), typeof(CircularProgressButton), new PropertyMetadata(65d, new PropertyChangedCallback(OnPercentageChanged)));

        // Using a DependencyProperty as the backing store for StrokeThickness.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StrokeThicknessProperty =
            DependencyProperty.Register("StrokeThickness", typeof(int), typeof(CircularProgressButton), new PropertyMetadata(5));

        // Using a DependencyProperty as the backing store for SegmentColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SegmentColorProperty =
            DependencyProperty.Register("SegmentColor", typeof(Brush), typeof(CircularProgressButton), new PropertyMetadata(new SolidColorBrush(Colors.Red)));

        // Using a DependencyProperty as the backing store for Radius.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RadiusProperty =
            DependencyProperty.Register("Radius", typeof(int), typeof(CircularProgressButton), new PropertyMetadata(25, new PropertyChangedCallback(OnPropertyChanged)));

        // Using a DependencyProperty as the backing store for Angle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(CircularProgressButton), new PropertyMetadata(120d, new PropertyChangedCallback(OnPropertyChanged)));

        // Using a DependencyProperty as the backing store for IconSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconSourceProperty =
            DependencyProperty.Register("IconSource", typeof(ImageSource), typeof(CircularProgressButton), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for IconSourceHover.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconSourceHoverProperty =
            DependencyProperty.Register("IconSourceHover", typeof(ImageSource), typeof(CircularProgressButton), new PropertyMetadata(null, (s, e) => { ((CircularProgressButton)s).HasHoverImage = e.NewValue != null; }));

        // Using a DependencyProperty as the backing store for IconWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconWidthProperty =
            DependencyProperty.Register("IconWidth", typeof(double), typeof(CircularProgressButton), new PropertyMetadata(0d));

        // Using a DependencyProperty as the backing store for IconHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconHeightProperty =
            DependencyProperty.Register("IconHeight", typeof(double), typeof(CircularProgressButton), new PropertyMetadata(0d));

        // Using a DependencyProperty as the backing store for ActionCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActionCommandProperty =
            DependencyProperty.Register("ActionCommand", typeof(ICommand), typeof(CircularProgressButton), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for ActionCommandParameter.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActionCommandParameterProperty =
            DependencyProperty.Register("ActionCommandParameter", typeof(object), typeof(CircularProgressButton), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for ActionToolTrip.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActionToolTipProperty =
            DependencyProperty.Register("ActionToolTip", typeof(object), typeof(CircularProgressButton), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for ActionCursor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ActionCursorProperty =
            DependencyProperty.Register("ActionCursor", typeof(Cursor), typeof(CircularProgressButton), new PropertyMetadata(null));
        
        // Using a DependencyProperty as the backing store for HasHoverImage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasHoverImageProperty =
            DependencyProperty.Register("HasHoverImage", typeof(bool), typeof(CircularProgressButton), new PropertyMetadata(false));

        private static void OnActionChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            CircularProgressButton circle = sender as CircularProgressButton;
            if (circle.ActionChanged != null)
            {
                circle.ActionChanged((int)e.NewValue);
            }
        }

        private static void OnPercentageChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            CircularProgressButton circle = sender as CircularProgressButton;
            circle.Angle = (circle.Percentage * 360) / 100;
        }

        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            CircularProgressButton circle = sender as CircularProgressButton;
            circle.RenderArc();
        }

        public void RenderArc()
        {
            Point startPoint = ComputeCartesianCoordinate(Angle, Radius);
            startPoint.X += Radius;
            startPoint.Y += Radius;

            Point endPoint = new Point(Radius, 0);

            pathRoot.Width = Radius * 2 + StrokeThickness;
            pathRoot.Height = Radius * 2 + StrokeThickness;
            pathRoot.Margin = new Thickness(StrokeThickness, StrokeThickness, 0, 0);

            bool largeArc = Angle < 180.0;

            Size outerArcSize = new Size(Radius, Radius);

            if (Math.Round(startPoint.X) == endPoint.X && Math.Round(startPoint.Y) == endPoint.Y)
                endPoint.X -= 0.01;
            pathFigure.StartPoint = startPoint;
            arcSegment.Point = endPoint;
            arcSegment.Size = outerArcSize;
            arcSegment.IsLargeArc = largeArc;
        }

        private Point ComputeCartesianCoordinate(double angle, double radius)
        {
            // convert to radians
            double angleRad = (Math.PI / 180.0) * (angle - 90);

            double x = radius * Math.Cos(angleRad);
            double y = radius * Math.Sin(angleRad);

            return new Point(x, y);
        }
    }
}

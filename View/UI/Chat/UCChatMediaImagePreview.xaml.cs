﻿using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Converter;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatMediaImagePreview.xaml
    /// </summary>
    public partial class UCChatMediaImagePreview : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatMediaImagePreview).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        public const double MIN_ZOOM_LIMIT = 1;
        public const double MAX_ZOOM_LIMIT = 16;
        public const double ZOOM_UNIT = 1.1;

        private MessageModel _MessageModel = null;
        private double _Zoom = MIN_ZOOM_LIMIT;
        private bool _IsZoomSliderOpened = false;
        private bool _IsDrugable = false;
        private double _ImgWidth = 0.0;
        private double _ImgHeight = 0.0;
        private double _OriginalImgWidth = 0.0;
        private double _OriginalImgHeight = 0.0;
        private System.Windows.Point _Origin;
        private System.Windows.Point _Start;

        private ICommand _OnResetCommand;
        private ICommand _OnZoomCommand;
        private ICommand _OnSaveAsCommand;

        public UCChatMediaImagePreview()
        {
            InitializeComponent();
            this.zoomSlider.Minimum = MIN_ZOOM_LIMIT;
            this.zoomSlider.Maximum = MAX_ZOOM_LIMIT;
            this.zoomSlider.TickFrequency = ZOOM_UNIT - 1;
        }

        #region Event Handler

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (e.NewValue == null)
                    return;

                UCChatMediaImagePreview panel = (UCChatMediaImagePreview)d;
                if ((MessageType)e.NewValue == MessageType.DELETE_MESSAGE)
                {
                    panel.SizeChanged -= panel.UCChatMediaImagePreview_SizeChanged;
                    panel.MouseWheel -= panel.UCChatMediaImagePreview_MouseWheel;
                    panel.imageCtrl.MouseMove -= panel.imageCtrl_MouseMove;
                    panel.imageCtrl.MouseLeftButtonUp -= panel.imageCtrl_MouseLeftButtonUp;
                    panel.imageCtrl.MouseLeftButtonDown -= panel.imageCtrl_MouseLeftButtonDown;
                    panel.zoomSlider.ValueChanged -= panel.zoomSlider_ValueChanged;
                    panel.imageCtrl.ClearValue(System.Windows.Controls.Image.SourceProperty);
                    panel.imageCtrl.Source = null;
                    panel.ClearImageScale();
                    panel.Zoom = MIN_ZOOM_LIMIT;
                    panel.IsZoomSliderOpened = false;
                    panel.ImgWidth = 0.0;
                    panel.ImgHeight = 0.0;
                    panel.OriginalImgWidth = 0.0;
                    panel.OriginalImgHeight = 0.0;
                }
                else
                {
                    panel.SetCurrentSize();
                    MultiBinding sourceBinding = new MultiBinding { Converter = new ImagePreviewConverter() };
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("MessageModel"),
                    });
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("OriginalImgWidth"),
                    });
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("OriginalImgHeight"),
                    });
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("MessageModel.IsFileOpened"),
                    });
                    panel.imageCtrl.SetBinding(System.Windows.Controls.Image.SourceProperty, sourceBinding);
                    if (!panel.MessageModel.IsFileOpened && !panel.MessageModel.IsDownloadRunning)
                    {
                        panel.MessageModel.ViewModel.IsForceDownload = false;
                        panel.MessageModel.IsDownloadRunning = true;
                        panel.MessageModel.ViewModel.DownloadPercentage = 0;
                        ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.AddItem(new ChatDownloadProcessor(panel.MessageModel, false, (status, packetId) =>
                        {
                            if (status && panel.MessageModel != null && packetId.Equals(panel.MessageModel.PacketID))
                            {
                                ChatHelpers.SendViewPlayedSatatus(panel.MessageModel);
                                panel.zoomSlider.ValueChanged += panel.zoomSlider_ValueChanged;
                            }
                            return 0;
                        }));
                    }
                    else
                    {
                        ChatHelpers.SendViewPlayedSatatus(panel.MessageModel);
                        panel.zoomSlider.ValueChanged += panel.zoomSlider_ValueChanged;
                    }
                    panel.SizeChanged += panel.UCChatMediaImagePreview_SizeChanged;
                    panel.MouseWheel += panel.UCChatMediaImagePreview_MouseWheel;
                    panel.imageCtrl.MouseMove += panel.imageCtrl_MouseMove;
                    panel.imageCtrl.MouseLeftButtonUp += panel.imageCtrl_MouseLeftButtonUp;
                    panel.imageCtrl.MouseLeftButtonDown += panel.imageCtrl_MouseLeftButtonDown;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: mePlayer_MediaOpened() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UCChatMediaImagePreview_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                if (this.IsVisible && this.Zoom <= MIN_ZOOM_LIMIT && this.imageCtrl.Source != null)
                {
                    this.SetCurrentSize();
                    this.ClearImageScale();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCChatMediaImagePreview_SizeChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UCChatMediaImagePreview_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (this.IsVisible == true && !((this.Zoom <= MIN_ZOOM_LIMIT && e.Delta < 0) || (this.Zoom >= MAX_ZOOM_LIMIT && e.Delta > 0)) && this.imageCtrl.Source != null)
                {
                    double prevZoom = Zoom;
                    System.Windows.Point p = e.MouseDevice.GetPosition(imageCtrl);
                    Matrix m = imageCtrl.RenderTransform.Value;

                    double prevZoomWidth = ImgWidth * Zoom;
                    double prevZoomHeight = ImgHeight * Zoom;
                    double centerX = ImgWidth / 2;
                    double centerY = ImgHeight / 2;
                    double limitX = ImgWidth >= this.ActualWidth ? this.ActualWidth + ImgWidth * (ZOOM_UNIT - 1) * 4 : this.ActualWidth;
                    double limitY = ImgHeight >= this.ActualHeight ? this.ActualHeight + ImgWidth * (ZOOM_UNIT - 1) * 4 : this.ActualHeight;

                    if (prevZoomWidth < limitX)
                    {
                        m.OffsetX = (ImgWidth - prevZoomWidth) / 2;
                    }
                    else
                    {
                        centerX = p.X;
                    }

                    if (prevZoomHeight < limitY)
                    {
                        m.OffsetY = (ImgHeight - prevZoomHeight) / 2;
                    }
                    else
                    {
                        centerY = p.Y;
                    }

                    if (e.Delta > 0)
                    {
                        prevZoom *= ZOOM_UNIT;
                        if (prevZoom > MAX_ZOOM_LIMIT)
                        {
                            Zoom = MAX_ZOOM_LIMIT;
                            m.ScaleAtPrepend(1 + (ZOOM_UNIT - (prevZoom / MAX_ZOOM_LIMIT)), 1 + (ZOOM_UNIT - (prevZoom / MAX_ZOOM_LIMIT)), centerX, centerY);
                        }
                        else
                        {
                            Zoom = prevZoom;
                            m.ScaleAtPrepend(ZOOM_UNIT, ZOOM_UNIT, centerX, centerY);
                        }

                        if (m.M11 > MAX_ZOOM_LIMIT)
                        {
                            m.M11 = MAX_ZOOM_LIMIT;
                            m.M22 = MAX_ZOOM_LIMIT;
                        }
                    }
                    else
                    {
                        prevZoom /= ZOOM_UNIT;
                        if (prevZoom < MIN_ZOOM_LIMIT)
                        {
                            Zoom = MIN_ZOOM_LIMIT;
                        }
                        else
                        {
                            Zoom = prevZoom;
                        }

                        m.ScaleAtPrepend(1 / ZOOM_UNIT, 1 / ZOOM_UNIT, centerX, centerY);

                        if (m.M11 < MIN_ZOOM_LIMIT)
                        {
                            m.M11 = MIN_ZOOM_LIMIT;
                            m.M22 = MIN_ZOOM_LIMIT;
                        }
                    }

                    imageCtrl.RenderTransform = new MatrixTransform(m);
                    IsDrugable = ImgWidth * Zoom > this.ActualWidth || ImgHeight * Zoom > this.ActualHeight;
                    //Debug.WriteLine("Scale => {" + m.M11 + ", " + m.M22 + "} - P => {" + p.X + ", " + p.Y + "} - M => {" + m.OffsetX + ", " + m.OffsetY + "}");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: container_MouseWheel() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void imageCtrl_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (!this.imageCtrl.IsMouseCaptured) { return; }

                if (this.IsVisible == true && this.IsDrugable == true)
                {
                    Matrix m = imageCtrl.RenderTransform.Value;
                    System.Windows.Point point = e.MouseDevice.GetPosition(this);
                    System.Windows.Point screenPoint = imageCtrl.TranslatePoint(new System.Windows.Point(0, 0), this);

                    double zoomWidth = ImgWidth * Zoom;
                    double zoomHeight = ImgHeight * Zoom;
                    bool hasExceeded = false;

                    if (zoomWidth > this.ActualWidth)
                    {
                        double diff = 0;
                        if (point.X > _Start.X && screenPoint.X > 0)
                        {
                            diff = screenPoint.X - 0;
                            point.X -= diff;
                            hasExceeded = true;
                        }
                        else if (point.X < _Start.X && zoomWidth + screenPoint.X < this.ActualWidth)
                        {
                            diff = this.ActualWidth - (zoomWidth + screenPoint.X);
                            point.X += diff;
                            hasExceeded = true;
                        }

                        m.OffsetX = _Origin.X + (point.X - _Start.X);
                    }

                    if (zoomHeight > this.ActualHeight)
                    {
                        double diff = 0;
                        if (point.Y > _Start.Y && screenPoint.Y > 0)
                        {
                            diff = screenPoint.Y - 0;
                            point.Y -= diff;
                            hasExceeded = true;
                        }
                        else if (point.Y < _Start.Y && zoomHeight + screenPoint.Y < this.ActualHeight)
                        {
                            diff = this.ActualHeight - (zoomHeight + screenPoint.Y);
                            point.Y += diff;
                            hasExceeded = true;
                        }
                        m.OffsetY = _Origin.Y + (point.Y - _Start.Y);
                    }

                    imageCtrl.RenderTransform = new MatrixTransform(m);
                    if (hasExceeded == true)
                    {
                        imageCtrl.ReleaseMouseCapture();
                    }

                    //Debug.WriteLine("DDDDDDDD => {" + screenPoint.X + ", " + screenPoint.Y + "}");
                    //Debug.WriteLine("S => {" + _Start.X + ", " + _Start.Y + "} - P => {" + point.X + ", " + point.Y + "} - M => {" + m.OffsetX + ", " + m.OffsetY + "}");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: imageCtrl_MouseMove() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void imageCtrl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.imageCtrl.ReleaseMouseCapture();
            }
            catch (Exception ex)
            {
                log.Error("Error: imageCtrl_MouseLeftButtonUp() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void imageCtrl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (this.imageCtrl.IsMouseCaptured) return;

                if (this.IsVisible == true && this.IsDrugable == true && this.imageCtrl.Source != null)
                {
                    imageCtrl.CaptureMouse();

                    _Start = e.GetPosition(this);
                    _Origin.X = imageCtrl.RenderTransform.Value.OffsetX;
                    _Origin.Y = imageCtrl.RenderTransform.Value.OffsetY;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: imageCtrl_MouseLeftButtonDown() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void zoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                if (Zoom != e.NewValue && imageCtrl.Source != null)
                {
                    double delta = e.NewValue - e.OldValue;
                    double prevZoom = Zoom;

                    double prevZoomWidth = ImgWidth * prevZoom;
                    double prevZoomHeight = ImgHeight * prevZoom;
                    double centerX = ImgWidth / 2;
                    double centerY = ImgHeight / 2;

                    Zoom += delta;

                    Matrix m = imageCtrl.RenderTransform.Value;
                    m.OffsetX = (ImgWidth - prevZoomWidth) / 2;
                    m.OffsetY = (ImgHeight - prevZoomHeight) / 2;
                    m.ScaleAtPrepend(Zoom / prevZoom, Zoom / prevZoom, centerX, centerY);

                    if (m.M11 < MIN_ZOOM_LIMIT)
                    {
                        m.M11 = MIN_ZOOM_LIMIT;
                        m.M22 = MIN_ZOOM_LIMIT;
                        Zoom = MIN_ZOOM_LIMIT;
                    }
                    else if (m.M11 > MAX_ZOOM_LIMIT)
                    {
                        m.M11 = MAX_ZOOM_LIMIT;
                        m.M22 = MAX_ZOOM_LIMIT;
                        Zoom = MAX_ZOOM_LIMIT;
                    }

                    imageCtrl.RenderTransform = new MatrixTransform(m);
                    IsDrugable = ImgWidth * Zoom > this.ActualWidth || ImgHeight * Zoom > this.ActualHeight;
                    //Debug.WriteLine("Scale => {" + m.M11 + ", " + m.M22 + "} - P => {" + p.X + ", " + p.Y + "} - M => {" + m.OffsetX + ", " + m.OffsetY + "}");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: zoomSlider_ValueChanged() => " + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void InitDataContext(MessageModel model)
        {
            try
            {
                this.ClearValue(MsgTypeProperty);
                this.SetOriginalSize(model);

                this.MessageModel = model;
                if (this.DataContext == null)
                {
                    this.DataContext = this;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: InitDataContext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void InitView()
        {
            try
            {
                if (MessageModel.MessageType == MessageType.DELETE_MESSAGE)
                    return;

                Binding msgTypeBinding = new Binding
                {
                    Path = new PropertyPath("MessageModel.MessageType"),
                };
                SetBinding(MsgTypeProperty, msgTypeBinding);
            }
            catch (Exception ex)
            {
                log.Error("Error: InitView() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ResetImageSacle(object param)
        {
            try
            {
                this.Zoom = MIN_ZOOM_LIMIT;
                UCChatMediaImagePreview_SizeChanged(null, null);
            }
            catch (Exception ex)
            {
                log.Error("Error: Reset() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SaveImageAs(object param)
        {
            try
            {
                string filePath = ChatHelpers.GetChatFilePath(this.MessageModel.Message, this.MessageModel.MessageType, this.MessageModel.PacketID, this.MessageModel.LinkImageUrl, this.MessageModel.SenderTableID);
                if (System.IO.File.Exists(filePath))
                {
                    SaveFileDialog dialog = new SaveFileDialog();
                    dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                        "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                        "Portable Network Graphic (*.png)|*.png";
                    dialog.FileName = System.IO.Path.GetFileName(filePath);
                    if (dialog.ShowDialog() == true)
                    {
                        if (System.IO.File.Exists(filePath)) System.IO.File.Copy(filePath, dialog.FileName, true);
                        else UIHelperMethods.ShowWarning(NotificationMessages.FILE_NOT_FOUND);
                    }
                }
                else UIHelperMethods.ShowWarning(NotificationMessages.FILE_NOT_FOUND);

            }
            catch (Exception ex)
            {
                log.Error("Error: SaveImageAs() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetOriginalSize(MessageModel model)
        {
            if (model.MessageType != MessageType.DELETE_MESSAGE)
            {
                int targetW = 0;
                int targetH = 0;
                double ratio = (double)model.Width / (double)model.Height;
                int w = (int)model.Width;
                int h = (int)model.Height;

                if (ratio >= 1)
                {
                    targetW = ChatHelpers.HORIZONTAL_DIMENSION[0];
                    targetH = ChatHelpers.HORIZONTAL_DIMENSION[1];
                }
                else if (ratio < 1)
                {
                    targetW = ChatHelpers.VERTICAL_DIMENSION[0];
                    targetH = ChatHelpers.VERTICAL_DIMENSION[1];
                }

                if (model.Width > targetW || model.Height > targetH)
                {
                    int newW = 0;
                    int newH = 0;
                    int oldW = model.Width;
                    int oldH = model.Height;

                    if (oldW > targetW && oldH > targetH)
                    {
                        double diffW = (double)((oldW - targetW) * 100) / oldW;
                        double diffH = (double)((oldH - targetH) * 100) / oldH;
                        if (diffW > diffH)
                        {
                            newW = targetW;
                            newH = (oldH * newW) / oldW;
                        }
                        else
                        {
                            newH = targetH;
                            newW = (oldW * newH) / oldH;
                        }
                    }
                    else if (oldW > targetW)
                    {
                        newW = targetW;
                        newH = (oldH * newW) / oldW;
                    }
                    else if (oldH > targetH)
                    {
                        newH = targetH;
                        newW = (oldW * newH) / oldH;
                    }

                    w = newW;
                    h = newH;
                }

                OriginalImgWidth = w;
                OriginalImgHeight = h;
            }
        }

        private void SetCurrentSize()
        {
            if (this.ActualWidth > 0 && this.ActualHeight > 0)
            {
                double prevImgWidth = ImgWidth;
                double prevImgHeight = ImgHeight;
                double maxWidth = this.ActualWidth;
                double maxHeight = this.ActualHeight;

                if (OriginalImgWidth > maxWidth || OriginalImgHeight > maxHeight)
                {
                    int mode = 1;
                    if ((OriginalImgWidth / OriginalImgHeight) < (maxWidth / maxHeight)) mode = 1;
                    else mode = 2;

                    if (mode == 1)
                    {
                        ImgHeight = maxHeight;
                        ImgWidth = (OriginalImgWidth * ImgHeight) / OriginalImgHeight;
                    }
                    else
                    {
                        ImgWidth = maxWidth;
                        ImgHeight = (OriginalImgHeight * ImgWidth) / OriginalImgWidth;
                    }
                }
                else
                {
                    ImgWidth = OriginalImgWidth;
                    ImgHeight = OriginalImgHeight;
                }
            }
            else
            {
                ImgWidth = 0;
                ImgHeight = 0;
            }
        }

        private void ClearImageScale()
        {
            IsDrugable = false;
            Matrix m = imageCtrl.RenderTransform.Value;
            m.M11 = MIN_ZOOM_LIMIT;
            m.M12 = 0;
            m.M21 = 0;
            m.M22 = MIN_ZOOM_LIMIT;
            m.OffsetX = 0;
            m.OffsetY = 0;
            imageCtrl.RenderTransform = new MatrixTransform(m);
        }

        public void Dispose()
        {
            try
            {
#if MESSAGE_STATUS_LOG
                log.Debug("*** Dispose => ");
#endif
                this.ClearValue(MsgTypeProperty);
                this.MessageModel = null;
                this.DataContext = null;
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Utility Method

        #region Property

        public MessageType MsgType
        {
            get { return (MessageType)GetValue(MsgTypeProperty); }
            set { SetValue(MsgTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MsgType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MsgTypeProperty = DependencyProperty.Register("MsgType", typeof(MessageType), typeof(UCChatMediaImagePreview), new PropertyMetadata(MessageType.DELETE_MESSAGE, UCChatMediaImagePreview.PropertyChangedCallback));

        public MessageModel MessageModel
        {
            get { return _MessageModel; }
            set
            {
                _MessageModel = value;
                this.OnPropertyChanged("MessageModel");
            }
        }

        public bool IsZoomSliderOpened
        {
            get { return _IsZoomSliderOpened; }
            set
            {
                if (value == _IsZoomSliderOpened)
                    return;

                _IsZoomSliderOpened = value;
                this.OnPropertyChanged("IsZoomSliderOpened");
            }
        }

        public bool IsDrugable
        {
            get { return _IsDrugable; }
            set
            {
                if (value == _IsDrugable)
                    return;

                _IsDrugable = value;
                this.OnPropertyChanged("IsDrugable");
            }
        }

        public double Zoom
        {
            get { return _Zoom; }
            set
            {
                if (value == _Zoom)
                    return;

                _Zoom = value;
                this.OnPropertyChanged("Zoom");
            }
        }

        public double ImgWidth
        {
            get { return _ImgWidth; }
            set
            {
                if (value == _ImgWidth)
                    return;

                _ImgWidth = value;
                this.OnPropertyChanged("ImgWidth");
            }
        }

        public double ImgHeight
        {
            get { return _ImgHeight; }
            set
            {
                if (value == _ImgHeight)
                    return;

                _ImgHeight = value;
                this.OnPropertyChanged("ImgHeight");
            }
        }

        public double OriginalImgWidth
        {
            get { return _OriginalImgWidth; }
            set
            {
                if (value == _OriginalImgWidth)
                    return;

                _OriginalImgWidth = value;
                this.OnPropertyChanged("OriginalImgWidth");
            }
        }

        public double OriginalImgHeight
        {
            get { return _OriginalImgHeight; }
            set
            {
                if (value == _OriginalImgHeight)
                    return;

                _OriginalImgHeight = value;
                this.OnPropertyChanged("OriginalImgHeight");
            }
        }

        public ICommand OnResetCommand
        {
            get
            {
                if (_OnResetCommand == null)
                {
                    _OnResetCommand = new RelayCommand((param) => ResetImageSacle(param));
                }
                return _OnResetCommand;
            }
        }

        public ICommand OnZoomCommand
        {
            get
            {
                if (_OnZoomCommand == null)
                {
                    _OnZoomCommand = new RelayCommand((param) =>
                    {
                        IsZoomSliderOpened = !IsZoomSliderOpened;
                    });
                }
                return _OnZoomCommand;
            }
        }

        public ICommand OnSaveAsCommand
        {
            get
            {
                if (_OnSaveAsCommand == null)
                {
                    _OnSaveAsCommand = new RelayCommand((param) => SaveImageAs(param));
                }
                return _OnSaveAsCommand;
            }
        }

        #endregion Property
    }
}

﻿using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatInformationView.xaml
    /// </summary>
    public partial class UCChatInformationView : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatInformationView).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private bool _Visible = false;
        private Func<int> _OnClose = null;
        private string _ChatBgUrl = null;
        private string _EventChatBgUrl = null;
        private RecentModel _RecentModel = null;
        private ObservableCollection<RecentModel> _RecentModelList = null;
        private DispatcherTimer _ViewTimer = null;
        private int _MinBgHeight = 0;
        private bool _IsSeenListLoading = false;
        private const int FULL_NAME_HEIGHT = 25;
        private const int MARGIN_HEIGHT = 25;
        public string _PacketID = null;

        private ICommand _OnCloseCommand;

        public UCChatInformationView()
        {
            InitializeComponent();
        }

        #region Event Handler

        private static void MessageTextPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCChatInformationView panel = (UCChatInformationView)d;
            if (panel.IsVisible && panel._ViewTimer != null && !panel._ViewTimer.IsEnabled && panel.RecentModel != null)
            {
                panel.MinBgHeight = panel.RecentModel.MinViewHeight + MARGIN_HEIGHT + (panel.RecentModel.Message.ViewModel.IsSamePrevAndCurrID ? 0 : FULL_NAME_HEIGHT);
            }
        }

        private static void MessageTypePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCChatInformationView panel = (UCChatInformationView)d;
            if (panel.IsVisible && panel._ViewTimer != null && !panel._ViewTimer.IsEnabled && e.NewValue != null)
            {
                if (((MessageType)e.NewValue) == MessageType.DELETE_MESSAGE)
                {
                    panel.Hide();
                }
            }
        }

        private void UCChatInformationView_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue == false)
                {
                    IsSeenListLoading = false;
                    Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); }, DispatcherPriority.Send);

                    this.ClearValue(MessageTypeProperty);
                    this.ClearValue(MessageTextProperty);

                    _ViewTimer.Stop();
                    _ViewTimer = null;

                    itcSeenByList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                    itcSeenByList.ItemsSource = null;

                    this.RecentModel.Message.SeenByList.Clear();
                    this.RecentModelList.Clear();

                    tvRecentList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                    tvRecentList.ItemsSource = null;

                    imgBg.ClearValue(System.Windows.Controls.Image.SourceProperty);
                    imgBg.Source = null;

                    this._PacketID = null;
                    this.DataContext = null;
                    this.RecentModel = null;
                    this.RecentModelList = null;
                    this.EventChatBgUrl = null;
                    this.ChatBgUrl = null;
                    this.MinBgHeight = 0; 
                    this.Focusable = false;
                    this._OnClose = null;

                    this.IsVisibleChanged -= UCChatInformationView_IsVisibleChanged;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCChatInformationView_IsVisibleChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void Show(RecentModel model, String chatBgUrl, String eventChatBgUrl, Func<int> onClose = null)
        {
            this.IsVisibleChanged += UCChatInformationView_IsVisibleChanged;

            this._PacketID = model.Message.PacketID;
            if (model.Message.ViewModel.IsGroupChat)
            {
                this.IsSeenListLoading = true;
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
            }
            this.ChatBgUrl = chatBgUrl;
            this.EventChatBgUrl = eventChatBgUrl;
            this._OnClose = onClose;

            this.RecentModelList = new ObservableCollection<RecentModel>();
            this.RecentModel = model;
            this.RecentModel.Message.IsMessageOpened = true;
            this.DataContext = this;

            int initHeight = this.RecentModel.PrevViewHeight + MARGIN_HEIGHT;
            int estimatedheight = this.RecentModel.MinViewHeight + MARGIN_HEIGHT + (this.RecentModel.Message.ViewModel.IsSamePrevAndCurrID ? 0 : FULL_NAME_HEIGHT);
            MinBgHeight = initHeight < estimatedheight ? estimatedheight : (int)initHeight;

            this.BuildMessageList();

            this.Visible = true;
            this.Focusable = true;
            this.Focus();
        }

        public void Hide(object param = null)
        {
            if (_OnClose != null)
            {
                _OnClose();
            }
            else
            {
                this.Visible = false;
            }
        }

        private void BuildMessageList()
        {
            try
            {
                if (_ViewTimer == null)
                {
                    _ViewTimer = new DispatcherTimer();
                    _ViewTimer.Interval = TimeSpan.FromSeconds(0.1);
                    _ViewTimer.Tick += (s, e) =>
                    {
                        Binding messageTypeBinding = new Binding
                        {
                            Path = new PropertyPath("RecentModel.Message.MessageType"),
                        };
                        SetBinding(MessageTypeProperty, messageTypeBinding);
                        Binding msgBinding = new Binding
                        {
                            Path = new PropertyPath("RecentModel.Message.Message"),
                        };
                        SetBinding(MessageTextProperty, msgBinding);

                        tvRecentList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                        {
                            Path = new PropertyPath("RecentModelList"),
                        });

                        this.RecentModelList.Add(this.RecentModel);

                        MultiBinding sourceBinding = new MultiBinding { Converter = new PathToChatBackgroundConverter() };
                        sourceBinding.Bindings.Add(new Binding
                        {
                            Path = new PropertyPath("ChatBgUrl"),
                        });
                        sourceBinding.Bindings.Add(new Binding
                        {
                            Path = new PropertyPath("EventChatBgUrl"),
                        });
                        imgBg.SetBinding(System.Windows.Controls.Image.SourceProperty, sourceBinding);

                        if (this.RecentModel.Message.ViewModel.IsGroupChat)
                        {
                            itcSeenByList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                            {
                                Path = new PropertyPath("RecentModel.Message.SeenByList"),
                            });

                            if (this.RecentModel.Message.SeenByList.Count > 0)
                            {
                                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                                IsSeenListLoading = false;
                            }

                            ChatService.GetGroupMessageSeenMemberListByPacketID(this.RecentModel.Message.GroupID, this.RecentModel.Message.PacketID, (args) =>
                            {
                                if (!String.IsNullOrWhiteSpace(_PacketID))
                                {
                                    if (args.Status)
                                    {
                                        for (int idx = 0; idx < 5000 && this.RecentModel.Message.SeenByList.Count == 0; idx += 250)
                                        {
                                            Thread.Sleep(250);
                                        }
                                        IsSeenListLoading = false;
                                        Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();}, DispatcherPriority.Send);
                                    }
                                    else
                                    {

                                        Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();}, DispatcherPriority.Send);
                                        IsSeenListLoading = false;
                                    }
                                }
                            });
                        }
                        _ViewTimer.Stop();
                    };
                }
                _ViewTimer.Stop();
                _ViewTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: BuildMessageList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.Visible = false;
        }
        
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Property

        public string MessageText
        {
            get { return (string)GetValue(MessageTextProperty); }
            set { SetValue(MessageTextProperty, value); }
        }

        public static readonly DependencyProperty MessageTextProperty = DependencyProperty.Register("MessageText", typeof(string), typeof(UCChatInformationView), new PropertyMetadata(null, UCChatInformationView.MessageTextPropertyChangedCallback));

        public MessageType MessageType
        {
            get { return (MessageType)GetValue(MessageTypeProperty); }
            set { SetValue(MessageTypeProperty, value); }
        }

        public static readonly DependencyProperty MessageTypeProperty = DependencyProperty.Register("MessageType", typeof(MessageType), typeof(UCChatInformationView), new PropertyMetadata(MessageType.DELETE_MESSAGE, UCChatInformationView.MessageTypePropertyChangedCallback));

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (value == _Visible)
                    return;

                _Visible = value;
                this.OnPropertyChanged("Visible");
            }
        }

        public bool IsSeenListLoading
        {
            get { return _IsSeenListLoading; }
            set
            {
                if (value == _IsSeenListLoading)
                    return;

                _IsSeenListLoading = value;
                this.OnPropertyChanged("IsSeenListLoading");
            }
        }

        public RecentModel RecentModel
        {
            get { return _RecentModel; }
            set
            {
                if (value == _RecentModel)
                    return;

                _RecentModel = value;
                this.OnPropertyChanged("RecentModel");
            }
        }

        public string ChatBgUrl
        {
            get { return _ChatBgUrl; }
            set
            {
                if (value == _ChatBgUrl)
                    return;

                _ChatBgUrl = value;
                this.OnPropertyChanged("ChatBgUrl");
            }
        }

        public string EventChatBgUrl
        {
            get { return _EventChatBgUrl; }
            set
            {
                if (value == _EventChatBgUrl)
                    return;

                _EventChatBgUrl = value;
                this.OnPropertyChanged("EventChatBgUrl");
            }
        }

        public int MinBgHeight
        {
            get { return _MinBgHeight; }
            set
            {
                if (value == _MinBgHeight)
                    return;

                _MinBgHeight = value;
                this.OnPropertyChanged("MinBgHeight");
            }
        }

        public ObservableCollection<RecentModel> RecentModelList
        {
            get { return _RecentModelList; }
            set
            {
                if (value == _RecentModelList)
                    return;

                _RecentModelList = value;
                this.OnPropertyChanged("RecentModelList");
            }
        }

        public ICommand OnCloseCommand
        {
            get
            {
                if (_OnCloseCommand == null)
                {
                    _OnCloseCommand = new RelayCommand((param) => Hide(param));
                }
                return _OnCloseCommand;
            }
        }

        #endregion Property


    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using log4net;
using Models.Entity;
using View.BindingModels;
using View.Utility;
using View.Utility.FriendList;
using View.ViewModel;
using Models.Constants;
using System.Windows.Threading;
using System.Windows.Data;
using View.Utility.Chat;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatContactSharePreview.xaml
    /// </summary>
    /// 
    public partial class UCChatContactSharePreview : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatContactSharePreview).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<UserBasicInfoModel> _FriendList = null;
        private ObservableDictionary<long, UserBasicInfoModel> _SelectedContactList = null;
        private ObservableCollection<UserBasicInfoModel> _TempList = null;

        private long _FriendTableID;
        private long _GroupID;
        private string _RoomID;

        private Func<List<UserBasicInfoModel>, int> _OnClose;
        private bool _Visible = false;

        private ICommand _ShareCommand;
        private ICommand _CancelCommand;

        private DispatcherTimer _ViewTimer = null;

        public UCChatContactSharePreview()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Event Handler

        private void UCChatContactSharePreview_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue == false)
                {
                    this._FriendTableID = 0;
                    this._GroupID = 0;
                    this._RoomID = String.Empty;

                    this._ViewTimer.Stop();
                    this._ViewTimer = null;

                    this.itemsControl.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                    this.itemsControl.ItemsSource = null;

                    this.FriendList.Clear();

                    this.DataContext = null;
                    this.FriendList = null;
                    this._TempList = null;
                    this.SelectedContactList = null;

                    this.SearchTermTextBox.Text = "";
                    this.SearchTermTextBox.Focusable = false;
                    this._OnClose = null;

                    this.IsVisibleChanged -= UCChatContactSharePreview_IsVisibleChanged;
                    this.SearchTermTextBox.TextChanged -= SearchTermTextBox_TextChanged;
                    this.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCChatContactSharePreview_IsVisibleChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           this.SearchText = SearchTermTextBox.Text.ToLower();
        }

        #endregion Event Handler

        #region Utility Methods

        public void Show(long friendTableID, long grouID, string roomID, Func<List<UserBasicInfoModel>, int> onClose = null)
        {
            this.IsVisibleChanged += UCChatContactSharePreview_IsVisibleChanged;
            this.SearchTermTextBox.TextChanged += SearchTermTextBox_TextChanged;
            this._OnClose = onClose;

            this._FriendTableID = friendTableID;
            this._GroupID = grouID;
            this._RoomID = roomID;

            this.FriendList = new ObservableCollection<UserBasicInfoModel>();
            this.SelectedContactList = new ObservableDictionary<long, UserBasicInfoModel>();
            this.DataContext = this;

            this.BuildDataList();

            this.Visible = true;
            this.SearchTermTextBox.Focusable = true;
            this.SearchTermTextBox.Focus();
        }

        private void BuildDataList()
        {
            try
            {
                if (this._ViewTimer == null)
                {
                    this._ViewTimer = new DispatcherTimer();
                    this._ViewTimer.Interval = TimeSpan.FromSeconds(0.1);
                    this._ViewTimer.Tick += (s, e) =>
                    {
                        List<UserBasicInfoModel> allModels = FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values.ToList();
                        this._TempList = new ObservableCollection<UserBasicInfoModel>(
                            allModels.Where(P => P.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED).OrderBy(P => P.ShortInfoModel.FullName).ToList()
                        );
                        this.FriendList = this._TempList;

                        itemsControl.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                        {
                            Path = new PropertyPath("FriendList"),
                        });

                        this._ViewTimer.Stop();
                    };
                }
                this._ViewTimer.Stop();
                this._ViewTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: BuildMessageList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowSearchList()
        {
            if (!string.IsNullOrWhiteSpace(SearchText) && this._TempList != null && this._TempList.Count > 0)
            {
                List<UserBasicInfoModel> modelList = this._TempList.Where(P => (P.ShortInfoModel.FullName.ToLower().Contains(SearchText) || P.ShortInfoModel.UserIdentity.ToString().Contains(SearchText))).ToList();

                ObservableCollection<UserBasicInfoModel> tempList1 = new ObservableCollection<UserBasicInfoModel>();
                foreach (UserBasicInfoModel model in modelList)
                {
                    tempList1.Add(model);
                }
                this.FriendList = tempList1;
            }
            else
            {
                this.FriendList = this._TempList;
            }
        }

        private void OnShare(object param)
        {
            try
            {
                if (this._OnClose != null)
                {
                    this._OnClose(SelectedContactList != null ? SelectedContactList.Values.ToList() : null);
                }
                else
                {
                    this.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnShare() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnCancel(object param)
        {
            try
            {
                if (this._OnClose != null)
                {
                    this._OnClose(null);
                }
                else
                {
                    this.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCancel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.Visible = false;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility methods

        #region Property

        public ICommand ShareCommand
        {
            get
            {
                if (_ShareCommand == null)
                {
                    _ShareCommand = new RelayCommand((param) => OnShare(param));
                }
                return _ShareCommand;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand((param) => OnCancel(param));
                }
                return _CancelCommand;
            }
        }

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (value == _Visible)
                    return;

                _Visible = value;
                this.OnPropertyChanged("Visible");
            }
        }

        public ObservableCollection<UserBasicInfoModel> FriendList
        {
            get
            {
                return _FriendList;
            }
            set
            {
                _FriendList = value;
                this.OnPropertyChanged("FriendList");
            }
        }

        public ObservableDictionary<long, UserBasicInfoModel> SelectedContactList
        {
            get
            {
                return _SelectedContactList;
            }
            set
            {
                _SelectedContactList = value;
                this.OnPropertyChanged("SelectedContactList");
            }
        }

        private string _SearchText = string.Empty;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                _SearchText = value;
                ShowSearchList();
            }
        }

        #endregion Property

    }
}

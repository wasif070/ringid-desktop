﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.ViewModel;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatLogNotAvailablePanel.xaml
    /// </summary>
    public partial class UCChatLogNotAvailablePanel : UserControl
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatLogNotAvailablePanel).Name);

        public UCChatLogNotAvailablePanel()
        {
            InitializeComponent();
            this.IsVisibleChanged += UCChatLogNotAvailablePanel_IsVisibleChanged;
        }

        #region Event Handler

        private void UCChatLogNotAvailablePanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue)
                {
                    if (RingIDViewModel.Instance.NoChatCallLogModel.Count == 0)
                    {
                        RingIDViewModel.Instance.NoChatCallLogModel.Add(new UserBasicInfoModel());
                    }

                    int itemsCount = 6 - RingIDViewModel.Instance.NoChatCallLogModel.Count;
                    if(itemsCount > 0)
                    {
                        List<UserBasicInfoModel> tempList = RingIDViewModel.Instance.TopFriendList.ToList();
                        foreach (UserBasicInfoModel model in tempList)
                        {
                            if (RingIDViewModel.Instance.NoChatCallLogModel.FirstOrDefault(P => P.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID) == null)
                            {
                                RingIDViewModel.Instance.NoChatCallLogModel.Insert(0, model);
                                itemsCount--;
                                if (itemsCount == 0) break;
                            }
                        }

                        if (itemsCount > 0)
                        {
                            tempList = RingIDViewModel.Instance.FavouriteFriendList.ToList();
                            foreach (UserBasicInfoModel model in tempList)
                            {
                                if (RingIDViewModel.Instance.NoChatCallLogModel.FirstOrDefault(P => P.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID) == null)
                                {
                                    int index = RingIDViewModel.Instance.NoChatCallLogModel.Count > 1 ? RingIDViewModel.Instance.NoChatCallLogModel.Count - 1 : 0;
                                    RingIDViewModel.Instance.NoChatCallLogModel.Insert(index, model);
                                    itemsCount--;
                                    if (itemsCount == 0) break;
                                }
                            }

                            if (itemsCount > 0)
                            {
                                tempList = RingIDViewModel.Instance.NonFavouriteFriendList.ToList();
                                foreach (UserBasicInfoModel model in tempList)
                                {
                                    if (RingIDViewModel.Instance.NoChatCallLogModel.FirstOrDefault(P => P.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID) == null)
                                    {
                                        int index = RingIDViewModel.Instance.NoChatCallLogModel.Count > 1 ? RingIDViewModel.Instance.NoChatCallLogModel.Count - 1 : 0;
                                        RingIDViewModel.Instance.NoChatCallLogModel.Insert(index, model);
                                        itemsCount--;
                                        if (itemsCount == 0) break;
                                    }
                                }
                            }
                        }
                    }

                    object resource = Application.Current.FindResource("RingIDSettings");
                    this.itcUserinfoList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                    {
                        Path = new PropertyPath("MainViewModel.NoChatCallLogModel"),
                        Source = resource
                    });
                }
                else
                {
                    this.itcUserinfoList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                    this.itcUserinfoList.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UCChatLogNotAvailablePanel_IsVisibleChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler


    }
}

﻿using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.Feed;
using View.Utility.Recent;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatReportView.xaml
    /// </summary>
    public partial class UCChatReportView : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatReportView).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private bool _Visible = false;
        private Func<int> _OnClose = null;
        private MessageModel _MessageModel = null;
        private String _SelectedReason = null;
        private ObservableDictionary<int, string> _ReasonList = null;
        private DispatcherTimer _ViewTimer = null;
        private bool _IsReasonListLoading = false;
        private bool _IsReportSending = false;
        public string _PacketID = null;

        private ICommand _OnCloseCommand;
        private ICommand _OnSendCommand;
        private ICommand _OnSelectCommand;

        public UCChatReportView()
        {
            InitializeComponent();
        }

        #region Event Handler

        private void UCChatReportView_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue == false)
                {
                    IsReportSending = false;
                    IsReasonListLoading = false;
                    Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                    Application.Current.Dispatcher.BeginInvoke(() => { if (ReportGIFCtrl != null && ReportGIFCtrl.IsRunning()) this.ReportGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);

                    _ViewTimer.Stop();
                    _ViewTimer = null;

                    itcReasonList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                    itcReasonList.ItemsSource = null;
                    this.ReasonList = null;

                    this._PacketID = null;
                    this.SelectedReason = null;
                    this._MessageModel = null;
                    this.DataContext = null;
                    this.Focusable = false;
                    this._OnClose = null;

                    this.IsVisibleChanged -= UCChatReportView_IsVisibleChanged;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCChatReportView_IsVisibleChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void Show(MessageModel model, Func<int> onClose = null)
        {
            this.IsVisibleChanged += UCChatReportView_IsVisibleChanged;

            this._MessageModel = model;
            this._PacketID = model.PacketID;
            this._OnClose = onClose;

            this.DataContext = this;
            this.BuildReasonList();

            this.Visible = true;
            this.Focusable = true;
            this.Focus();
        }

        private void OnSendReport(object param)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(SelectedReason) && _MessageModel != null)
                {
                    IsReportSending = true;
                    MessageModel model = _MessageModel;
                    this.ReportGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_CLOCK));

                    ChatService.ReportPublicRoomChat(_MessageModel.PacketID, _MessageModel.RoomID, _MessageModel.SenderTableID, SelectedReason, (args) =>
                    {
                        IsReportSending = false;
                        Application.Current.Dispatcher.BeginInvoke(() => { if (ReportGIFCtrl != null && ReportGIFCtrl.IsRunning()) this.ReportGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);

                        if (args.Status)
                        {
                            model.IReport = true;
                            RecentLoadUtility.RemoveByMessageType(model.RoomID, model.PacketID);
                            UIHelperMethods.ShowInformation("Thank you for submitting report. Your report will be reviewed soon.", "Reported successfully");
                            //  CustomMessageBox.ShowInformation("Thank you for submitting report. Your report will be reviewed soon.");
                            Application.Current.Dispatcher.BeginInvoke(() => { Hide(); }, DispatcherPriority.Send);
                        }
                        else
                        {
                            UIHelperMethods.ShowFailed("Can not report right now, please try again later!", "Report failed");
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSendReport() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnSelect(object param)
        {
            try
            {
                SelectedReason = param != null ? param.ToString() : "";
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSelect() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Hide(object param = null)
        {
            if (_OnClose != null)
            {
                _OnClose();
            }
            else
            {
                this.Visible = false;
            }
        }

        private void BuildReasonList()
        {
            try
            {
                if (_ViewTimer == null)
                {
                    _ViewTimer = new DispatcherTimer();
                    _ViewTimer.Interval = TimeSpan.FromSeconds(0.1);
                    _ViewTimer.Tick += (s, e) =>
                    {
                        this.ReasonList = RingIDViewModel.Instance.GetSpamReasonListByType(StatusConstants.SPAM_FEED);

                        itcReasonList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                        {
                            Path = new PropertyPath("ReasonList")
                        });

                        if (this.ReasonList.Count == 0)
                        {
                            this.IsReasonListLoading = true;
                            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            new ThradSpamReasonList().StartThread(StatusConstants.SPAM_FEED, (status) =>
                            {
                                if (status)
                                {
                                    for (int idx = 0; idx < 5000 && this.ReasonList.Count == 0; idx += 250)
                                    {
                                        Thread.Sleep(250);
                                    }
                                    this.IsReasonListLoading = false;
                                    Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                                }
                                else
                                {

                                    Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                                    this.IsReasonListLoading = false;
                                }
                                return 0;
                            });
                        }

                        _ViewTimer.Stop();
                    };
                }
                _ViewTimer.Stop();
                _ViewTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: BuildReasonList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.Visible = false;
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Property

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (value == _Visible)
                    return;

                _Visible = value;
                this.OnPropertyChanged("Visible");
            }
        }

        public bool IsReasonListLoading
        {
            get { return _IsReasonListLoading; }
            set
            {
                if (value == _IsReasonListLoading)
                    return;

                _IsReasonListLoading = value;
                this.OnPropertyChanged("IsReasonListLoading");
            }
        }

        public bool IsReportSending
        {
            get { return _IsReportSending; }
            set
            {
                if (value == _IsReportSending)
                    return;

                _IsReportSending = value;
                this.OnPropertyChanged("IsReportSending");
            }
        }

        public MessageModel MessageModel
        {
            get { return _MessageModel; }
            set
            {
                if (value == _MessageModel)
                    return;

                _MessageModel = value;
                this.OnPropertyChanged("MessageModel");
            }
        }

        public string SelectedReason
        {
            get { return _SelectedReason; }
            set
            {
                if (value == _SelectedReason)
                    return;

                _SelectedReason = value;
                this.OnPropertyChanged("SelectedReason");
            }
        }

        public ObservableDictionary<int, string> ReasonList
        {
            get { return _ReasonList; }
            set
            {
                if (value == _ReasonList)
                    return;

                _ReasonList = value;
                this.OnPropertyChanged("ReasonList");
            }
        }

        public ICommand OnCloseCommand
        {
            get
            {
                if (_OnCloseCommand == null)
                {
                    _OnCloseCommand = new RelayCommand((param) => Hide(param));
                }
                return _OnCloseCommand;
            }
        }

        public ICommand OnSendCommand
        {
            get
            {
                if (_OnSendCommand == null)
                {
                    _OnSendCommand = new RelayCommand((param) => OnSendReport(param));
                }
                return _OnSendCommand;
            }
        }
        public ICommand OnSelectCommand
        {
            get
            {
                if (_OnSelectCommand == null)
                {
                    _OnSelectCommand = new RelayCommand((param) => OnSelect(param));
                }
                return _OnSelectCommand;
            }
        }

        #endregion Property


    }
}

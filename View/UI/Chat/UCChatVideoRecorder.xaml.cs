﻿using log4net;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.Recorder;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatVideoRecorder.xaml
    /// </summary>
    public partial class UCChatVideoRecorder : System.Windows.Controls.UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatVideoRecorder).Name);
        public static UCChatVideoRecorder Instance = null;

        private int _Type;
        private int _State = RecorderConstants.STATE_READY;
        private int _RecordingProgress = 0;
        private TimeSpan _TimerValue = TimeSpan.FromSeconds(0);
        private UserBasicInfoModel _FriendBasicInfoModel;
        private GroupInfoModel _GroupInfoModel;
        private RoomModel _RoomModel;
        private bool _HasWebCamError = false;
        private string _ErrorMessage = String.Empty;
        private bool _IsExpanable = true;
        private bool _IsExpandView = false;
        private bool _IsPlayMode = false;
        private ICommand _CloseCommand;
        private ICommand _RecordCommand;
        private ICommand _ExpandNormalCommand;
        private string _FileName = String.Empty;
        private int _TotalSeconds = 0;
        private bool _UserIsDraggingSlider = false;

        private CustomizeTimer PlayerProgressTimer { set; get; }

        public delegate void OnRecordingCompleted(string fileName, int countDown);
        public event OnRecordingCompleted RecordingCompletedHandler;

        public UCChatVideoRecorder(GroupInfoModel groupInfoModel)
        {
            this.GroupInfoModel = groupInfoModel;
            this.Type = RecorderConstants.TYPE_GROUP_VIDEO_RECORD;
            this.Init();
        }

        public UCChatVideoRecorder(UserBasicInfoModel friendBasicInfoModel)
        {
            this.FriendBasicInfoModel = friendBasicInfoModel;
            this.Type = RecorderConstants.TYPE_FRIEND_VIDEO_RECORD;
            this.Init();
        }

        public UCChatVideoRecorder(RoomModel roomModel)
        {
            this.RoomModel = roomModel;
            this.Type = RecorderConstants.TYPE_ROOM_VIDEO_RECORD;
            this.Init();
        }

        private void Init()
        {
            this.InitializeComponent();
            Instance = this;
            this.State = RecorderConstants.STATE_READY;
            PlayerProgressTimer = new CustomizeTimer();
            PlayerProgressTimer.IntervalInSecond = 1;
            PlayerProgressTimer.Tick += PlayerProgressTimer_Tick;
            WebcamPreview.OnRecordingCountDownChange += WebcamPreview_OnRecordingCountDownChange;
            WebcamPreview.OnRecordingCompleted += WebcamPreview_OnRecordingCompleted;
            this.DataContext = this;
        }

        #region Event Handler

        private void WebcamPreview_OnRecordingCountDownChange(int v)
        {
            TimerValue = TimeSpan.FromSeconds(v);
            RecordingProgress = (v * 100) / RecorderConstants.MAX_VIDEO_RECORD_TIME;
            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                sdrPlayerProgress.Value = v;
            }, DispatcherPriority.Send);
        }

        private void WebcamPreview_OnRecordingCompleted()
        {
            State = RecorderConstants.STATE_COMPLETED;
            _TotalSeconds = (int)TimerValue.TotalSeconds;

            new Thread(() =>
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    _UserIsDraggingSlider = false;

                    sdrPlayerProgress.Minimum = 0;
                    sdrPlayerProgress.Maximum = _TotalSeconds;
                    sdrPlayerProgress.Value = 0;
                    mediaPlayer.Source = new Uri(_FileName);
                    mediaPlayer.Pause();
                    IsPlayMode = true;
                }, DispatcherPriority.ApplicationIdle);
            }).Start();
        }

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            try
            {
                int errorCode = WebcamPreview.InitWebcamDevices(RecorderConstants.MAX_VIDEO_RECORD_TIME);
                if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND)
                {
                    HasWebCamError = true;
                    ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                }
                else if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING)
                {
                    HasWebCamError = true;
                    ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
                }

                ((AnimationClock)sender).Completed -= DoubleAnimationCompleted;
                mainContainer.Triggers.Clear();
            }
            catch (Exception ex)
            {
                log.Error("Error: DoubleAnimationCompleted() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void PlayerProgressTimer_Tick(int counter, bool initTick, object state)
        {
            try
            {
                if (initTick) return;

                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    bool hasTimeSpan = mediaPlayer.NaturalDuration.HasTimeSpan;
                    if (hasTimeSpan && mediaPlayer.Source != null && _UserIsDraggingSlider == false)
                    {
                        TimeSpan tempTimeSpan = mediaPlayer.Position;
                        double diff = Math.Ceiling(1000 - (tempTimeSpan.TotalMilliseconds % 1000));
                        TimerValue = diff < 100 ? tempTimeSpan.Add(TimeSpan.FromMilliseconds(diff)) : tempTimeSpan;

                        if ((int)TimerValue.TotalSeconds >= _TotalSeconds)
                        {
                            sdrPlayerProgress.Value = _TotalSeconds;
                            PlayerProgressTimer.Stop();
                            mediaPlayer.Stop();
                            State = RecorderConstants.STATE_COMPLETED;
                        }
                        else
                        {
                            sdrPlayerProgress.Value = TimerValue.TotalSeconds;
                        }
                    }
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: PlayerProgressTimer_Tick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void sdrPlayerProgress_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            if (IsPlayMode)
            {
                _UserIsDraggingSlider = true;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void sdrPlayerProgress_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            if (IsPlayMode)
            {
                _UserIsDraggingSlider = false;
                mediaPlayer.Position = TimeSpan.FromSeconds(sdrPlayerProgress.Value);
            }
            else
            {
                e.Handled = true;
            }
        }

        #endregion

        #region Utility Methods

        private void OnRecordClick(object param)
        {
            try
            {
                int buttonType = (int)param;
                switch (buttonType)
                {
                    case RecorderConstants.BUTTON_START:
                    case RecorderConstants.BUTTON_RESTART:
                        IsPlayMode = false;
                        mediaPlayer.Close();
                        PlayerProgressTimer.Stop();
                        mediaPlayer.Source = null;

                        sdrPlayerProgress.Minimum = 0;
                        sdrPlayerProgress.Maximum = RecorderConstants.MAX_VIDEO_RECORD_TIME;

                        _FileName = RingIDSettings.TEMP_CAM_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + DefaultSettings.LOGIN_RING_ID + "_" + ChatService.GetServerTime() + RecorderWebcamPreview.VIDEO_FORMAT;

                        int errorCode = WebcamPreview.StartRecording(_FileName);
                        if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND)
                        {
                            HasWebCamError = true;
                            ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                        }
                        else if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING)
                        {
                            HasWebCamError = true;
                            ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
                        }
                        else
                        {
                            State = RecorderConstants.STATE_RUNNING;
                        }
                        break;
                    case RecorderConstants.BUTTON_STOP:
                        WebcamPreview.RequestForStop();
                        break;
                    case RecorderConstants.BUTTON_PLAY:
                        mediaPlayer.Play();
                        _UserIsDraggingSlider = false;
                        if (PlayerProgressTimer.IsAlive)
                        {
                            PlayerProgressTimer.Resume();
                        }
                        else
                        {
                            PlayerProgressTimer.Start();
                        }
                        State = RecorderConstants.STATE_PLAY;
                        break;
                    case RecorderConstants.BUTTON_PAUSE:
                        mediaPlayer.Pause();
                        PlayerProgressTimer.Pause();
                        _UserIsDraggingSlider = false;
                        State = RecorderConstants.STATE_PAUSED;
                        break;
                    case RecorderConstants.BUTTON_SEND:
                        State = RecorderConstants.STATE_SENT;
                        if (RecordingCompletedHandler != null)
                        {
                            RecordingCompletedHandler(_FileName, _TotalSeconds);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnRecordClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnExpandNormalClick(object param)
        {
            IsExpandView = !IsExpandView;
        }

        private void OnCloseClick(object param)
        {
            if (RecordingCompletedHandler != null)
            {
                RecordingCompletedHandler(string.Empty, 0);
            }
        }

        public void Dispose()
        {
            try
            {
                WebcamPreview.Dispose();
                PlayerProgressTimer.Stop();
                mediaPlayer.Close();
                mediaPlayer.Source = null;
                if (State != RecorderConstants.STATE_SENT && !String.IsNullOrWhiteSpace(_FileName))
                {
                    if (System.IO.File.Exists(_FileName))
                    {
                        System.IO.File.Delete(_FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
            Instance = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property

        public UserBasicInfoModel FriendBasicInfoModel
        {
            get { return _FriendBasicInfoModel; }
            set { _FriendBasicInfoModel = value; }
        }

        public GroupInfoModel GroupInfoModel
        {
            get { return _GroupInfoModel; }
            set { _GroupInfoModel = value; }
        }

        public RoomModel RoomModel
        {
            get { return _RoomModel; }
            set { _RoomModel = value; }
        }

        public int Type
        {
            get { return _Type; }
            set
            {
                _Type = value;
                this.OnPropertyChanged("Type");
            }
        }

        public int State
        {
            get { return _State; }
            set
            {
                _State = value;
                this.OnPropertyChanged("State");
            }
        }

        public int RecordingProgress
        {
            get { return _RecordingProgress; }
            set
            {
                _RecordingProgress = value;
                this.OnPropertyChanged("RecordingProgress");
            }
        }

        public TimeSpan TimerValue
        {
            get { return _TimerValue; }
            set
            {
                _TimerValue = value;
                this.OnPropertyChanged("TimerValue");
            }
        }

        public bool HasWebCamError
        {
            get
            {
                return _HasWebCamError;
            }
            set
            {
                _HasWebCamError = value;
                this.OnPropertyChanged("HasWebCamError");
            }
        }

        public string ErrorMessage
        {
            get
            {
                return _ErrorMessage;
            }
            set
            {
                _ErrorMessage = value;
                this.OnPropertyChanged("ErrorMessage");
            }
        }

        public bool IsExpandView
        {
            get
            {
                return _IsExpandView;
            }
            set
            {
                _IsExpandView = value;
                this.OnPropertyChanged("IsExpandView");
            }
        }

        public bool IsExpanable
        {
            get
            {
                return _IsExpanable;
            }
            set
            {
                _IsExpanable = value;
                this.OnPropertyChanged("IsExpanable");
            }
        }

        public bool IsPlayMode
        {
            get
            {
                return _IsPlayMode;
            }
            set
            {
                _IsPlayMode = value;
                this.OnPropertyChanged("IsPlayMode");
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => OnCloseClick(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand RecordCommand
        {
            get
            {
                if (_RecordCommand == null)
                {
                    _RecordCommand = new RelayCommand((param) => OnRecordClick(param));
                }
                return _RecordCommand;
            }
        }

        public ICommand ExpandNormalCommand
        {
            get
            {
                if (_ExpandNormalCommand == null)
                {
                    _ExpandNormalCommand = new RelayCommand((param) => OnExpandNormalClick(param));
                }
                return _ExpandNormalCommand;
            }
        }

        #endregion

    }
}

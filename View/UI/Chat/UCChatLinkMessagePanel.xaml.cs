﻿using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.Utility;
using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using View.BindingModels;
using View.Converter;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatImageMessagePanel.xaml
    /// </summary>
    public partial class UCChatLinkMessagePanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatRingMediaMessagePanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private bool disposed = false;
        private static object ImageLocation = null;

        static UCChatLinkMessagePanel()
        {
            if (Application.Current != null)
            {
                ImageLocation = Application.Current.FindResource("ImageLocation");
            }
        }

        public UCChatLinkMessagePanel()
        {
            this.MinHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)MessageType.LINK_MESSAGE][0] + ChatHelpers.CHAT_VIEW_HEIGHT[(int)MessageType.LINK_MESSAGE][1];
            InitializeComponent();
        }

        ~UCChatLinkMessagePanel()
        {
           Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCChatLinkMessagePanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        public static readonly DependencyProperty IsSameSenderProperty = DependencyProperty.Register("IsSameSender", typeof(bool), typeof(UCChatLinkMessagePanel), new PropertyMetadata(true, OnNewSenderChanged));

        public bool IsSameSender
        {
            get { return (bool)GetValue(IsSameSenderProperty); }
            set
            {
                SetValue(IsSameSenderProperty, value);
            }
        }

        public static readonly DependencyProperty IsPreviewOpenedProperty = DependencyProperty.Register("IsPreviewOpened", typeof(bool), typeof(UCChatLinkMessagePanel), new PropertyMetadata(false, OnPreviewChanged));

        public bool IsPreviewOpened
        {
            get { return (bool)GetValue(IsPreviewOpenedProperty); }
            set
            {
                SetValue(IsPreviewOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                UCChatLinkMessagePanel panel = ((UCChatLinkMessagePanel)sender);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewSize.Height > model.PrevViewHeight)
                {
                    model.PrevViewHeight = (int)e.NewSize.Height;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSizeChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatLinkMessagePanel panel = ((UCChatLinkMessagePanel)o);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewValue != null && (bool)e.NewValue)
                {
                    model.PrevViewHeight = model.MinViewHeight;
                    panel.SizeChanged += panel.OnSizeChanged;
                    panel.grdMessageContainer.ContextMenuOpening += panel.ctxMenu_ContextMenuOpening;
                    panel.grdMessageContainer.ContextMenuClosing += panel.ctxMenu_ContextMenuClosing;
                    panel.txtMessage.SelectionChanged += panel.txtMessage_SelectionChanged;

                    if (!panel.IsSameSender)
                    {
                        panel.BindNewSender(model.Message.ViewModel.IsRoomChat);
                    }
                    else
                    {
                        panel.ClearNewSender();
                    }

                    panel.BindPreview(model.Message.ViewModel.IsPreviewMessage && panel.IsPreviewOpened);
                    panel.BindMsgInfo(model.Message);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnNewSenderChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatLinkMessagePanel panel = ((UCChatLinkMessagePanel)o);
                if (panel.disposed) return;

                if (e.NewValue != null && !(bool)e.NewValue && panel.IsOpened)
                {
                    panel.BindNewSender(((RecentModel)panel.DataContext).Message.ViewModel.IsRoomChat);
                }
                else
                {
                    panel.ClearNewSender();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNewSenderChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnPreviewChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCChatLinkMessagePanel panel = ((UCChatLinkMessagePanel)o);
                if (panel.disposed) return;

                RecentModel model = ((RecentModel)panel.DataContext);
                if (panel.IsOpened)
                {
                    panel.BindPreview(model.Message.ViewModel.IsPreviewMessage && (e.NewValue != null && (bool)e.NewValue));
                }
                else
                {
                    panel.ClearPreview();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPreviewChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ctxMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            try
            {
                if (this.DataContext == null) return;

                RecentModel model = (RecentModel)this.DataContext;
                model.Message.ViewModel.IsEditable = false;
                if (!model.Message.FromFriend)
                {
                    bool isValidStatus = !(model.Message.Status == ChatConstants.STATUS_PENDING
                        || model.Message.Status == ChatConstants.STATUS_SENDING
                        || model.Message.Status == ChatConstants.STATUS_FAILED);

                    model.Message.ViewModel.IsEditable = model.Message.Timeout <= 0
                        && (ChatService.GetServerTime() - model.Message.MessageDate) < (SettingsConstants.MILISECONDS_IN_DAY / 24)
                        && isValidStatus;
                }
                model.Message.ViewModel.SelectedCharacters = txtMessage.Selection.Text.Trim().Length > 0 ? txtMessage.Selection.Text.Trim() : null;
            }
            catch (Exception ex)
            {
                log.Error("Error: ctxMenu_ContextMenuOpening() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ctxMenu_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            txtMessage.Selection.Select(txtMessage.Selection.Start, txtMessage.Selection.Start);
        }

        private void txtMessage_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext == null) return;

            RecentModel model = (RecentModel)this.DataContext;
            if (txtMessage.Selection.Text.Trim().Length > 0)
            {
                model.Message.ViewModel.SelectedCharacters = txtMessage.Selection.Text.Trim();
            }
            else
            {
                model.Message.ViewModel.SelectedCharacters = null;
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindMsgInfo(MessageModel messageModel)
        {
            MultiBinding textBinding = new MultiBinding { Converter = new ChatStatusAndTimeConverter() };
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MessageDate")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.Status")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.MessageType")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.PacketType")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.FromFriend")
            });
            textBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("Message.ViewModel.IsRoomChat")
            });
            txtStatusAndTime.SetBinding(TextBlock.TextProperty, textBinding);

            if (!messageModel.FromFriend && !messageModel.ViewModel.IsRoomChat)
            {
                MultiBinding lastStatusBinding = new MultiBinding { Converter = new ChatLastStatusConverter() };
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.MessageDate")
                });
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.LastStatus.LastDeliveredDate")
                });
                lastStatusBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.LastStatus.LastSeenDate")
                });
                icnLastStatus.SetBinding(Image.TagProperty, lastStatusBinding);
            }
        }

        private void ClearMsgInfo()
        {
            txtStatusAndTime.ClearValue(TextBlock.TextProperty);
            icnLastStatus.ClearValue(Image.TagProperty);
            icnLastStatus.Source = null;
        }

        private void BindPreview(bool isPreviewMessage)
        {
            txtMessage.SetBinding(ChatRichTextView.TextProperty, new Binding { Path = new PropertyPath("Message.Message") });

            if (isPreviewMessage)
            {
                MultiBinding sourceBinding = new MultiBinding { Converter = new ChatLinkImageConverter() };
                sourceBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.LinkImageUrl"),
                });
                sourceBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message.IsPreviewOpened"),
                });
                imgControl.SetBinding(Image.SourceProperty, sourceBinding);
            }
            else
            {
                imgControl.ClearValue(Image.SourceProperty);
                imgControl.Source = null;
            }

            lblTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.LinkTitle") });
            icnLink.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("LINK_MESSAGE_ICON"), Source = ImageLocation });
            urlLink.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.LinkUrl"), Converter = new LinkToBaseUrlConverter() });
            lblDescription.SetBinding(TextBox.TextProperty, new Binding { Path = new PropertyPath("Message.LinkDescription") });
        }

        private void ClearPreview()
        {
            txtMessage.ClearValue(ChatRichTextView.TextProperty);
            txtMessage.Text = null;
            imgControl.ClearValue(Image.SourceProperty);
            imgControl.Source = null;
            lblTitle.ClearValue(TextBlock.TextProperty);
            icnLink.ClearValue(Image.SourceProperty);
            icnLink.Source = null;
            urlLink.ClearValue(TextBlock.TextProperty);
            lblDescription.ClearValue(TextBox.TextProperty);
        }

        private void BindNewSender(bool isRoomChat)
        {
            if (isRoomChat)
            {
                imgProfile.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("Message.CurrentInstance"), Converter = new PathToChatProfileImageConverter() });
                txtFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.FullName") });
            }
            else
            {
                imgProfile.SetBinding(System.Windows.Controls.Image.SourceProperty, new Binding { Path = new PropertyPath("Message.FriendInfoModel.CurrentInstance"), Converter = new PathToChatProfileImageConverter() });
                txtFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Message.FriendInfoModel.FullName") });
            }
        }

        private void ClearNewSender()
        {
            imgProfile.ClearValue(Image.SourceProperty);
            imgProfile.Source = null;
            txtFullName.ClearValue(TextBlock.TextProperty);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    this.SizeChanged -= OnSizeChanged;
                    this.grdMessageContainer.ContextMenuOpening -= this.ctxMenu_ContextMenuOpening;
                    this.grdMessageContainer.ContextMenuClosing -= this.ctxMenu_ContextMenuClosing;
                    this.txtMessage.SelectionChanged -= this.txtMessage_SelectionChanged;
                    if (disposing)
                    {
                        this.ClearNewSender();
                        this.ClearMsgInfo();
                        this.ClearPreview();
                        this.imgProfile = null;
                        this.txtMessage = null;
                        this.imgControl = null;
                        this.lblDescription = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

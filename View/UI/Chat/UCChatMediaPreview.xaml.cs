﻿using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Chat
{
    /// <summary>
    /// Interaction logic for UCChatMediaPreview.xaml
    /// </summary>
    public partial class UCChatMediaPreview : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCChatMediaPreview).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private List<MessageModel> _MesssageList = new List<MessageModel>();
        private bool _Visible = false;
        private int _CurrIndex = 0;
        private int _SequenceNumber = 0;
        private int _TotalCount = 0;
        private Visibility _LeftArrowVisible = Visibility.Collapsed;
        private Visibility _RightArrowVisible = Visibility.Collapsed;
        private ICommand _OnCloseCommand;
        private ICommand _OnLeftCommand;
        private ICommand _OnRightCommand;
        private Func<int> _OnClose = null;
        public string _PacketID = null;
        public DispatcherTimer _ViewTimer = null;

        private UCChatMediaImagePreview _MediaImagePreview = null;
        private UCChatMediaVideoPreview _MediaVideoPreview = null;

        public UCChatMediaPreview()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Event Handler

        private void UCChatMediaPreview_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue == false)
                {
                    this._CurrIndex = 0;
                    this.SequenceNumber = 0;
                    this.TotalCount = 0;
                    this._PacketID = null;

                    this.Focusable = false;
                    this._OnClose = null;

                    this.IsVisibleChanged -= UCChatMediaPreview_IsVisibleChanged;
                    this.PreviewKeyDown -= UCChatMediaPreview_PreviewKeyDown;

                    if (_MediaImagePreview != null)
                    {
                        _MediaImagePreview.Dispose();
                        _MediaImagePreview = null;
                    }
                    if (_MediaVideoPreview != null)
                    {
                        _MediaVideoPreview.Dispose();
                        _MediaVideoPreview = null;
                    }
                    this._MesssageList.Clear();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCChatMediaPreview_IsVisibleChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UCChatMediaPreview_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                {
                    Hide();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCChatMediaPreview_PreviewKeyDown() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void Show(MessageModel model, Func<int> onClose = null)
        {
            this.IsVisibleChanged += UCChatMediaPreview_IsVisibleChanged;
            this.PreviewKeyDown += UCChatMediaPreview_PreviewKeyDown;
            this._OnClose = onClose;
            this.BuildMessageList(model);
            this.Visible = true;
            this.Focusable = true;
            this.Focus();
        }

        public void Hide(object param = null)
        {
            if (_OnClose != null)
            {
                _OnClose();
            }
            else
            {
                UCChatMediaPreviewWrapper.Instance.Hide();
            }
        }

        private void BuildMessageList(MessageModel model)
        {
            try
            {
                string contactID = model.FriendTableID > 0 ? model.FriendTableID.ToString() : model.GroupID > 0 ? model.GroupID.ToString() : model.RoomID;
                List<MessageModel> tempList = RingIDViewModel.Instance.GetRecentModelListByID(contactID).Where(P => P.Message != null).Select(P => P.Message).ToList();
                
                int idx = 0;
                for (int i = 0; i < tempList.Count; i++)
                {
                    MessageModel msgModel = tempList[i];
                    switch (msgModel.MessageType)
                    {
                        case MessageType.IMAGE_FILE_FROM_GALLERY:
                        case MessageType.IMAGE_FILE_FROM_CAMERA:
                        case MessageType.VIDEO_FILE:
                            {
                                _MesssageList.Add(msgModel);
                                if (model.Equals(msgModel))
                                {
                                    _CurrIndex = idx;
                                }
                                idx += 1;
                            }
                            break;
                    }
                }

                TotalCount = idx;
                RevalidateMediaPreview(model);
            }
            catch (Exception ex)
            {
                log.Error("Error: InitView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void RevalidateMediaPreview(MessageModel model)
        {
            try
            {
                if (TotalCount == 1)
                {
                    LeftArrowVisible = Visibility.Collapsed;
                    RightArrowVisible = Visibility.Collapsed;
                }
                else if (_CurrIndex == TotalCount - 1)
                {
                    LeftArrowVisible = Visibility.Collapsed;
                    RightArrowVisible = Visibility.Visible;
                }
                else if (_CurrIndex == 0)
                {
                    LeftArrowVisible = Visibility.Visible;
                    RightArrowVisible = Visibility.Collapsed;
                }
                else
                {
                    LeftArrowVisible = Visibility.Visible;
                    RightArrowVisible = Visibility.Visible;
                }

                SequenceNumber = TotalCount - _CurrIndex;
                MessageType messageType = model.MessageType == MessageType.DELETE_MESSAGE ? model.PrevMessageType : model.MessageType;

                switch (messageType)
                {
                    case MessageType.IMAGE_FILE_FROM_GALLERY:
                    case MessageType.IMAGE_FILE_FROM_CAMERA:
                    case MessageType.RING_MEDIA_MESSAGE:
                        {
                            this._PacketID = model.PacketID;
                            if (this._MediaImagePreview == null)
                            {
                                this._MediaImagePreview = new UCChatMediaImagePreview();
                            }
                            this._MediaImagePreview.InitDataContext(model);
                            this.pnlMediaPreview.Child = _MediaImagePreview;
                            this.InitMediaView();

                            if (this._MediaVideoPreview != null)
                            {
                                this._MediaVideoPreview.Dispose();
                                this._MediaVideoPreview = null;
                            }
                        }
                        break;
                    case MessageType.VIDEO_FILE:
                        {
                            this._PacketID = model.PacketID;
                            if (this._MediaVideoPreview == null)
                            {
                                this._MediaVideoPreview = new UCChatMediaVideoPreview();
                            }
                            this._MediaVideoPreview.InitDataContext(model);
                            this.pnlMediaPreview.Child = _MediaVideoPreview;
                            this.InitMediaView();

                            if (this._MediaImagePreview != null)
                            {
                                this._MediaImagePreview.Dispose();
                                this._MediaImagePreview = null;
                            }
                        }
                        break;
                }

            }
            catch (Exception ex)
            {
                log.Error("Error: OnRight() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void InitMediaView()
        {
            try
            {
                if (_ViewTimer == null)
                {
                    _ViewTimer = new DispatcherTimer();
                    _ViewTimer.Interval = TimeSpan.FromSeconds(0.1);
                    _ViewTimer.Tick += (s, e) =>
                    {
                        if (pnlMediaPreview.Child != null)
                        {
                            if (pnlMediaPreview.Child is UCChatMediaImagePreview)
                            {
                                ((UCChatMediaImagePreview)pnlMediaPreview.Child).InitView();
                            }
                            else if (pnlMediaPreview.Child is UCChatMediaVideoPreview)
                            {
                                ((UCChatMediaVideoPreview)pnlMediaPreview.Child).InitView();
                            }
                        }
                        _ViewTimer.Stop();
                    };
                }
                _ViewTimer.Stop();
                _ViewTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: InitMediaView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.Visible = false;
        }
        
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Property

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (value == _Visible)
                    return;

                _Visible = value;
                this.OnPropertyChanged("Visible");
            }
        }

        public int TotalCount
        {
            get { return _TotalCount; }
            set
            {
                if (value == _TotalCount)
                    return;

                _TotalCount = value;
                this.OnPropertyChanged("TotalCount");
            }
        }
        
        public int SequenceNumber
        {
            get { return _SequenceNumber; }
            set
            {
                if (value == _SequenceNumber)
                    return;

                _SequenceNumber = value;
                this.OnPropertyChanged("SequenceNumber");
            }
        }

        public Visibility LeftArrowVisible
        {
            get { return _LeftArrowVisible; }
            set
            {
                if (value == _LeftArrowVisible)
                    return;

                _LeftArrowVisible = value;
                this.OnPropertyChanged("LeftArrowVisible");
            }
        }

        public Visibility RightArrowVisible
        {
            get { return _RightArrowVisible; }
            set
            {
                if (value == _RightArrowVisible)
                    return;

                _RightArrowVisible = value;
                this.OnPropertyChanged("RightArrowVisible");
            }
        }

        public ICommand OnCloseCommand
        {
            get
            {
                if (_OnCloseCommand == null)
                {
                    _OnCloseCommand = new RelayCommand((param) => Hide(param));
                }
                return _OnCloseCommand;
            }
        }

        public ICommand OnLeftCommand
        {
            get
            {
                if (_OnLeftCommand == null)
                {
                    _OnLeftCommand = new RelayCommand((param) => { RevalidateMediaPreview(_MesssageList[++_CurrIndex]); });
                }
                return _OnLeftCommand;
            }
        }

        public ICommand OnRightCommand
        {
            get
            {
                if (_OnRightCommand == null)
                {
                    _OnRightCommand = new RelayCommand((param) => { RevalidateMediaPreview(_MesssageList[--_CurrIndex]); });
                }
                return _OnRightCommand;
            }
        }

        #endregion Property

    }
}

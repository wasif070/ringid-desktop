﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.Constants;
using View.Utility;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for WebcamCapture.xaml
    /// </summary>
    public partial class WNWebcamCapture : Window, INotifyPropertyChanged, IDisposable
    {
        private bool _IsTakePictureState = true;
        private bool _IsUsePictureState = false;
        private bool _HasWebCamError = false;
        private string _ErrorMessage = String.Empty;
        private static WNWebcamCapture _Instance = null;
        private ICommand _CloseCommand;
        private ICommand _TakePictureCommand;
        private ICommand _TryAgainCommand;
        private ICommand _UsePictureCommand;
        private ObservableCollection<string> _ImageList = new ObservableCollection<string>();

        public delegate void TakePictureCompleteHandler(string fullName);
        public event TakePictureCompleteHandler OnTakePictureComplete;

        public WNWebcamCapture()
        {
            InitializeComponent();
            DataContext = this;
            this.Closed += WNWebcamCapture_Closed;
            this.MouseDown += delegate { DragMove(); };
        }

        #region Event Handler

        private void WNWebcamCapture_Closed(object sender, EventArgs e)
        {
            WebcamPreview.Dispose();
            _Instance = null;
        }

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            FileInfo[] fInfos = GetAllFileInfo();
            foreach (FileInfo info in fInfos)
            {
                ImageList.Add(info.FullName);
            }

            int errorCode = WebcamPreview.StartWebcam(640, 480);
            if (errorCode == WebcamPreview.ERROR_WEBCAM_NOT_FOUND)
            {
                HasWebCamError = true;
                ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
            }
            else if (errorCode == WebcamPreview.ERROR_WEBCAM_ALREADY_USING)
            {
                HasWebCamError = true;
                ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
            }
        }

        #endregion Event Handler

        #region Property

        public static WNWebcamCapture Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new WNWebcamCapture();
                }
                return _Instance;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => CloseWindow(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand TakePictureCommand
        {
            get
            {
                if (_TakePictureCommand == null)
                {
                    _TakePictureCommand = new RelayCommand((param) => OnTakePicture(param));
                }
                return _TakePictureCommand;
            }
        }

        public ICommand TryAgainCommand
        {
            get
            {
                if (_TryAgainCommand == null)
                {
                    _TryAgainCommand = new RelayCommand((param) => OnTryAgain(param));
                }
                return _TryAgainCommand;
            }
        }

        public ICommand UsePictureCommand
        {
            get
            {
                if (_UsePictureCommand == null)
                {
                    _UsePictureCommand = new RelayCommand((param) => OnUsePicture(param));
                }
                return _UsePictureCommand;
            }
        }

        public bool IsTakePictureState
        {
            get
            {
                return _IsTakePictureState;
            }
            set
            {
                _IsTakePictureState = value;
                this.OnPropertyChanged("IsTakePictureState");
            }
        }

        public bool IsUsePictureState
        {
            get
            {
                return _IsUsePictureState;
            }
            set
            {
                _IsUsePictureState = value;
                this.OnPropertyChanged("IsUsePictureState");
            }
        }

        public ObservableCollection<string> ImageList
        {
            get
            {
                return _ImageList;
            }
            set
            {
                _ImageList = value;
                this.OnPropertyChanged("ImageList");
            }
        }

        public bool HasWebCamError
        {
            get
            {
                return _HasWebCamError;
            }
            set
            {
                _HasWebCamError = value;
                this.OnPropertyChanged("HasWebCamError");
            }
        }

        public string ErrorMessage
        {
            get
            {
                return _ErrorMessage;
            }
            set
            {
                _ErrorMessage = value;
                this.OnPropertyChanged("ErrorMessage");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Property

        #region Utility Method

        private new void Show()
        {
            base.Show();

            this.Owner = System.Windows.Application.Current.MainWindow;
            System.Drawing.Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
            this.Left = (workingArea.Right - this.ActualWidth) / 2;
            this.Top = (workingArea.Bottom - this.ActualHeight) / 2;
        }

        public void ShowWindow(TakePictureCompleteHandler del)
        {
            OnTakePictureComplete += del;
            App.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                this.Show();
            }));
        }

        public void CloseWindow(object param = null)
        {
            this.Owner = null;
            this.Close();
        }

        private void OnTakePicture(object param)
        {
            int errorCode = WebcamPreview.TakePicture();
            if (errorCode == WebcamPreview.ERROR_WEBCAM_NOT_FOUND)
            {
                HasWebCamError = true;
                ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
            }
            else if (errorCode == WebcamPreview.ERROR_WEBCAM_ALREADY_USING)
            {
                HasWebCamError = true;
                ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
            }
            else
            {
                IsTakePictureState = false;
                new Thread(() =>
                {
                    for (int idx = 0; idx < 3000 && WebcamPreview.Bitmap == null; idx += 50) 
                    {
                        Debug.WriteLine(idx);
                        Thread.Sleep(50);
                    }
                    IsUsePictureState = true;
                }).Start();
            }
        }

        private void OnTryAgain(object param)
        {
            int errorCode = WebcamPreview.RestartWebcam(640, 480);
            if (errorCode == WebcamPreview.ERROR_WEBCAM_NOT_FOUND)
            {
                HasWebCamError = true;
                ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
            }
            else if (errorCode == WebcamPreview.ERROR_WEBCAM_ALREADY_USING)
            {
                HasWebCamError = true;
                ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
            }
            else
            {
                IsTakePictureState = true;
                IsUsePictureState = false;
            }
        }

        private void OnUsePicture(object param)
        {
            if (WebcamPreview.Bitmap != null)
            {
                int counter = -1;
                FileInfo[] finfos = GetAllFileInfo();

                foreach (FileInfo fi in finfos)
                {
                    string id = fi.Name.Replace("webcam_" + DefaultSettings.LOGIN_RING_ID.ToString() + "_", "");
                    id = id.Replace(".jpeg", "");
                    try
                    {
                        int value = int.Parse(id);
                        if (value > counter)
                            counter = value;
                    }
                    catch
                    {
                    }
                }

                if (finfos.Length >= 10)
                {
                    for (int idx = 9; idx < finfos.Length; idx++)
                    {
                        ImageList.Remove(finfos[idx].FullName);
                        if (File.Exists(finfos[idx].FullName))
                        {
                            File.Delete(finfos[idx].FullName);
                        }
                    }
                }

                string fileName = RingIDSettings.TEMP_CAM_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + "webcam_" + DefaultSettings.LOGIN_RING_ID + "_" + (++counter).ToString() + ".jpeg";
                WebcamPreview.Bitmap.Save(fileName, ImageFormat.Jpeg);

                if (OnTakePictureComplete != null)
                {
                    OnTakePictureComplete(fileName);
                }

                CloseWindow();
            }
        }

        private FileInfo[] GetAllFileInfo()
        {
            HelperMethods.CreateDirectory(RingIDSettings.TEMP_CAM_IMAGE_FOLDER);
            string exp = "(webcam_" + DefaultSettings.LOGIN_RING_ID.ToString() + "_)(\\d+)\\.(jpeg)";
            Regex regx = new Regex(@exp);//(@"\.(\d+)$");
            DirectoryInfo di = new DirectoryInfo(RingIDSettings.TEMP_CAM_IMAGE_FOLDER);
            FileInfo[] finfos = di.GetFiles("*.*", SearchOption.TopDirectoryOnly).Where(P => regx.IsMatch(P.Name)).OrderByDescending(P => P.CreationTime).ToArray();
            return finfos;
        }

        public void Dispose()
        {

        }

        #endregion Utility Method

    }
}

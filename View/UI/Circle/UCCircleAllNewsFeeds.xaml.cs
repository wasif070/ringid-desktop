﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for UCSavedContentPanel.xaml
    /// </summary>
    public partial class UCCircleAllNewsFeeds : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCircleAllNewsFeeds).Name);
        public CustomScrollViewer scroll;

        #region "Constructor"
        public UCCircleAllNewsFeeds(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            //ViewCollection = RingIDViewModel.Instance.AllCircleFeeds;
            scroll.SetScrollValues(ViewCollection, FeedDataContainer.Instance.AllCircleFeedsSortedIds, SettingsConstants.PROFILE_TYPE_CIRCLE, AppConstants.TYPE_CIRCLE_NEWSFEED);
            DefaultSettings.ALLCIRCLE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.ALLCIRCLE_STARTPKT);
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Property
        private CustomObservableCollection _ViewCollection;
        public CustomObservableCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        #endregion

        #region "Private Methods"
        #endregion

        #region "Public Methods"
        #endregion

        #region "Event Triggers"
        private void UserControl_Loaded(object sender, RoutedEventArgs args)
        {
            scroll.ScrollToHome();
            if (ViewCollection == null)
            {
                if (timer == null)
                {
                    timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromMilliseconds(500);
                    timer.Tick += TimerEventProcessor;
                }
                else timer.Stop();
                timer.Start();
            }
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs args)
        {
            //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
            //{
                scroll.SetScrollEvents(false);
                ViewCollection = null;
            //}
        }
        private DispatcherTimer timer;
        private void TimerEventProcessor(object sender, EventArgs e)
        {
            if (timer != null && timer.IsEnabled)
            {
                //ViewCollection = RingIDViewModel.Instance.AllCircleFeeds;
                scroll.SetScrollEvents(true);
                if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    Keyboard.Focus(scroll);
                timer.Stop();
            }
        }

        #endregion
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }
        private void OnReloadCommandClicked()
        {
            DefaultSettings.ALLCIRCLE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.ALLCIRCLE_STARTPKT);
        }

        #region "Utility Methods"

        #endregion "Utility Methods"
    }
}

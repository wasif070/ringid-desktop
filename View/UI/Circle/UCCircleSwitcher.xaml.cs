﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility.Circle;
using View.ViewModel;

namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for CircleSwitcher.xaml
    /// </summary>
    public partial class UCCircleSwitcher : UserControl
    {
        public UCCircleSwitcher()
        {
            InitializeComponent();
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            UserControl user = (UserControl)sender; 
            if (user.IsVisible)
            {
                RingIDViewModel.Instance.CircleButtonSelection = true;
            }
            else
            {
                if (UCMiddlePanelSwitcher.View_UCCircleInitPanel != null)
                {
                    VMCircle.Instance.OnBtnTabCommandClicked(0);
                }
                RingIDViewModel.Instance.CircleButtonSelection = false;
            }
        }
    }
}

﻿using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.ViewModel;
using System.Linq;
using View.Utility;
using View.Constants;
using Newtonsoft.Json.Linq;
using Models.Constants;
using Auth.utility;
using View.Utility.WPFMessageBox;
using Auth.Service.Feed;
using View.Utility.Circle;

namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for UCCreateCirclePanel.xaml
    /// </summary>
    public partial class UCCreateCirclePanel : UserControl
    {
        public bool _IsCreateCircleContinuing = false;
        public UCCreateCirclePanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCCreateCirclePanel_Loaded;
            this.Unloaded += UCCreateCirclePanel_Unloaded;
        }

        void UCCreateCirclePanel_Unloaded(object sender, RoutedEventArgs e)
        {
            _IsCreateCircleContinuing = false;
        }

        public bool ConfirmCircleCreateLeave()
        {
            //MessageBoxResult result = CustomMessageBox.ShowQuestion("Do you want to stop creating circle ?");
            bool isTrue = UIHelperMethods.ShowQuestion("Do you want to stop creating circle ?", "Stop confirmation.");
            if (isTrue)
            {
                VMCircle.Instance.DragListForNewCircle.Clear();
                return true;
            }
            else return false;
        }

        void UCCreateCirclePanel_Loaded(object sender, RoutedEventArgs e)
        {
            _IsCreateCircleContinuing = false;
        }

        private void RemoveCircleMember_Click(object sender, RoutedEventArgs e)
        {
            errorLabel.Content = string.Empty;
            Control control = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            VMCircle.Instance.DragListForNewCircle.Remove(model);
            if (VMCircle.Instance.DragListForNewCircle.Count < 1)
            {
                _IsCreateCircleContinuing = false;
            }
            SetDragTextVisibility();
        }
        public void ClearCreateCirclePanel()
        {
            errorLabel.Content = string.Empty;
        }

        private void dragFriend_Drop(object sender, DragEventArgs e)
        {
            errorLabel.Content = string.Empty;
            DataObject dobj = (DataObject)e.Data;
            UserBasicInfoModel model = (UserBasicInfoModel)dobj.GetData(typeof(UserBasicInfoModel));
            model.IsAdmin = false;
            if (model.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            {
                errorLabel.Content = NotificationMessages.CIRCLE_FRIEND_ADD_ONLY;
            }
            else if (!VMCircle.Instance.DragListForNewCircle.Any(P => P.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID))
            {
                VMCircle.Instance.DragListForNewCircle.Add(model);
                _IsCreateCircleContinuing = true;
            }
            else
            {
                errorLabel.Content = NotificationMessages.CIRCLE_MEMBER_ALREADY_ADDED;
            }
            SetDragTextVisibility();
        }

        private void SetDragTextVisibility()
        {
            if (VMCircle.Instance.DragListForNewCircle.Count > 0)
            {
                this.dragEmptyTextBlock.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.dragEmptyTextBlock.Visibility = Visibility.Visible;
            }
            memberCount.Text = VMCircle.Instance.DragListForNewCircle.Count + "";
        }

        private void dragFriend_DragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCircleMain);
        }

        private void ResetCircleButton_Click(object sender, RoutedEventArgs e)
        {
            circleErrorLabel.Visibility = Visibility.Collapsed;
            circleNameTextBox.Text = string.Empty;
            errorLabel.Content = string.Empty;
            VMCircle.Instance.DragListForNewCircle.Clear();
            SetDragTextVisibility();
        }

        public void EnableButtons(bool flag)
        {
            CreateBtn.IsEnabled = true;
            ResetBtn.IsEnabled = true;
            if (flag)
            {
                circleNameTextBox.Text = string.Empty;
                VMCircle.Instance.DragListForNewCircle.Clear();
                SetDragTextVisibility();
            }
            else
            {
                errorLabel.Content = string.Empty;
            }
        }

        private void CreateCircleButton_Click(object sender, RoutedEventArgs e)
        {
            errorLabel.Content = string.Empty;
            circleErrorLabel.Visibility = Visibility.Visible;
            if (string.IsNullOrEmpty(circleNameTextBox.Text)) { errorLabel.Content = NotificationMessages.CIRCLE_CREATE_NO_NAME; }
            else if (circleNameTextBox.Text.Length > 60) { errorLabel.Content = NotificationMessages.CIRCLE_CREATE_MAX_NAME_LENGTH; }
            else if (VMCircle.Instance.DragListForNewCircle.Count < 1) { errorLabel.Content = NotificationMessages.CIRCLE_CREATE_REQUIRE_MEMBER_ADD; }

            else if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) || !DefaultSettings.IsInternetAvailable)
            {
                UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE);
            }
            else
            {
                circleErrorLabel.Visibility = Visibility.Collapsed;
                CreateBtn.IsEnabled = false;
                ResetBtn.IsEnabled = false;
                VMCircle.Instance.OnCreateCircleAfterDragCommand(circleNameTextBox.Text);
            }
        }
    }
}

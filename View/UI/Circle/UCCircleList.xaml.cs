﻿using Models.Constants;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;
using View.ViewModel;
using View.Utility.Circle;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System;
using log4net;
using System.Threading;
using View.Utility.DataContainer;

namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for UCCircleList.xaml
    /// </summary>
    public partial class UCCircleList : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCircleList).Name);
        private ScrollViewer _MyScroll;
        private bool _IS_LOADING = false;
        private double _PreVerticalChange = 0;
        private double _SingleItemsMaxHeight = 55;
        private Thread ScrollThread = null;

        public UCCircleList(ScrollViewer scroll)
        {
            InitializeComponent();
            this.DataContext = VMCircle.Instance;
            this._MyScroll = scroll;
            this._MyScroll.ScrollChanged += _MyScroll_ScrollChanged;
            this.Loaded += UCCircleList_Loaded;
        }

        void UCCircleList_Loaded(object sender, RoutedEventArgs e)
        {
            if (!UCGuiRingID.Instance.IsAnyWindowAbove())
            {
                textBoxSearch.Focusable = true;
                textBoxSearch.Focus();
            }
        }

        #region Utility

        public void LoadAllCirclesToUI()
        {
            var screen = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            double screenHeight = screen.Height;
            int itemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight);
            VMCircle.Instance._ItemsNeedToLoad = itemsNeedToLoad;
            CircleUtility.Instance.LoadAllCirclesToUI(VMCircle.Instance._ItemsNeedToLoad);
        }

        private void LoadCircleByScroll()
        {
            int prevCount = VMCircle.Instance.CircleListYouAreIn.Count;
            List<CircleModel> circleModeList = CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.Values.Where(P => CircleDataContainer.Instance.CircleIDs.Any(Q => P.UserTableID == Q && P.SuperAdmin != DefaultSettings.LOGIN_RING_ID)).ToList();
            foreach (CircleModel model in circleModeList)
            {
                if (!VMCircle.Instance.CircleListYouAreIn.Any(P => P.UserTableID == model.UserTableID))
                {
                    VMCircle.Instance.CircleListYouAreIn.InvokeAdd(model);
                    Thread.Sleep(10);
                }
                if (VMCircle.Instance.CircleListYouAreIn.Count >= prevCount + 10)
                {
                    break;
                }
            }

            _IS_LOADING = false;
        }

        #endregion

        #region EventHandler

        private void _MyScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (this.IsVisible)
            {
                if (_IS_LOADING == false && e.VerticalChange > 0 && e.VerticalChange != _PreVerticalChange && e.VerticalOffset >= (_MyScroll.ScrollableHeight - 40))
                {
                    _IS_LOADING = true;
                    if (!(ScrollThread != null && ScrollThread.IsAlive))
                    {
                        ScrollThread = new Thread(new ThreadStart(LoadCircleByScroll));
                        ScrollThread.Start();
                    }

                    _PreVerticalChange = e.VerticalChange;
                }
            }
        }

        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Control control = (Control)sender;
            if (sender != null && control is ContextMenu)
            {
                CircleModel cmodel = (CircleModel)control.DataContext;
                if (control.IsVisible)
                {
                    cmodel.SingleCirclePanelFocusable = true;
                }
                else
                {
                    cmodel.SingleCirclePanelFocusable = false;
                }
            }
        }

        private void BtnCancel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            VMCircle.Instance.CircleSearchString = string.Empty;
        }

        #endregion
    }
}
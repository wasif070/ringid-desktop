﻿using Auth.Service.Feed;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.Utility.Circle;

namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for UCCircleAddMembersPanel.xaml
    /// </summary>
    public partial class UCCircleAddMembersPanel : UserControl, INotifyPropertyChanged
    {
        long circleId = 0;
        public UCCircleAddMembersPanel(long circleId)
        {
            InitializeComponent();
            this.DataContext = this;
            this.circleId = circleId;
            this.Loaded += UserControl_Loaded;
            this.Unloaded += UserControl_Unloaded;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            errorLabel.Content = string.Empty;
            DragListForCircle.Clear();
            SetDragTextVisibility();

            ShowMembersBtn.Click += ShowMembers_Click;
            SaveBtn.Click += SaveBtn_Click;
            ResetBtn.Click += ResetBtn_Click;

            // e.Handled = true;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            errorLabel.Content = string.Empty;
            DragListForCircle.Clear();
            SetDragTextVisibility();

            ShowMembersBtn.Click -= ShowMembers_Click;
            SaveBtn.Click -= SaveBtn_Click;
            ResetBtn.Click -= ResetBtn_Click;

            //e.Handled = true;
        }

        void ResetBtn_Click(object sender, RoutedEventArgs e)
        {
            errorLabel.Content = string.Empty;
            DragListForCircle.Clear();
            SetDragTextVisibility();
        }

        public void EnableAddBtns(bool enable = true)
        {
            ResetBtn.IsEnabled = enable;
            SaveBtn.IsEnabled = enable;
        }

        void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (DragListForCircle.Count > 0)
            {
                //ResetBtn.IsEnabled = false;
                //SaveBtn.IsEnabled = false;
                EnableAddBtns(false);
                List<CircleMemberDTO> circleMembers = new List<CircleMemberDTO>();
                foreach (UserBasicInfoModel userBasicInfoModel in DragListForCircle)
                {
                    CircleMemberDTO circleMember = userBasicInfoModel.GetCircleMemberDTOFromModel(circleId, 0);
                    circleMembers.Add(circleMember);
                }
                new ThradAddCircleMember().StartThread(circleId, circleMembers);
            }
            else
            {
                errorLabel.Content = NotificationMessages.CIRCLE_NO_MEMBER_ADDED;
            }
            e.Handled = true;
        }

        private void ShowMembers_Click(object sender, RoutedEventArgs e)
        {
            if (DragListForCircle.Count > 0)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.SURE_WANT_TO, "discard adding circle members"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Discard"));
                if (!isTrue)
                    return;
            }
            DragListForCircle.Clear();
            UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.ShowAddedMembers();
        }

        private void dragFriend_Drop(object sender, DragEventArgs e)
        {
            errorLabel.Content = string.Empty;
            DataObject dobj = (DataObject)e.Data;
            UserBasicInfoModel model = (UserBasicInfoModel)dobj.GetData(typeof(UserBasicInfoModel));
            model.IsAdmin = false;
            if (model.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            {
                errorLabel.Content = NotificationMessages.CIRCLE_FRIEND_ADD_ONLY;
            }
            else if (!DragListForCircle.Any(P => P.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID) && !View.Utility.Circle.VMCircle.Instance.MembersList.Any(P => P.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID))
            {
                DragListForCircle.Add(model);

                SearchCircleMember._SearchParam = model.ShortInfoModel.FullName.ToLower();
                SearchCircleMember._UserIdentity = model.ShortInfoModel.UserIdentity;
                SearchCircleMember._UserBasicInfoModel = model;
                new SearchCircleMember(circleId, 0).StartThread();
                //DragListForCircle.Add(model);
            }
            else
            {
                errorLabel.Content = NotificationMessages.CIRCLE_MEMBER_ALREADY_ADDED;
            }
            SetDragTextVisibility();
        }

        public void SetErrorLabelExists()
        {
            errorLabel.Content = NotificationMessages.CIRCLE_MEMBER_ALREADY_ADDED;
        }

        private void dragFriend_DragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
        }

        private void RemoveCircleMember_Click(object sender, RoutedEventArgs e)
        {
            errorLabel.Content = string.Empty;
            Control control = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            DragListForCircle.Remove(model);
            SetDragTextVisibility();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void SetDragTextVisibility()
        {
            if (DragListForCircle.Count > 0)
            {
                this.dragEmptyTextBlock.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.dragEmptyTextBlock.Visibility = Visibility.Visible;
            }
            memberCount.Text = DragListForCircle.Count + string.Empty;
        }

        private ObservableCollection<UserBasicInfoModel> _DragListForCircle = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> DragListForCircle
        {
            get
            {
                return _DragListForCircle;
            }
            set
            {
                _DragListForCircle = value;
                this.OnPropertyChanged("DragListForCircle");
            }
        }
    }
}

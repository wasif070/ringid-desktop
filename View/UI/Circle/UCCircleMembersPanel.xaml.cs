﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Utility.Circle;

namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for UCCircleMembersPanel.xaml
    /// </summary>
    public partial class UCCircleMembersPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCircleMembersPanel).Name);
        private ListBoxItem prevItem = null;
        public UCCircleAddMembersPanel _ucCircleAddMembersPanel = null;

        public UCCircleMembersPanel()
        {
            InitializeComponent();
            this.DataContext = View.Utility.Circle.VMCircle.Instance;
            Loaded += UserControl_Loaded;
            Unloaded += UserControl_Unloaded;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region Event Handlers
        private void UserControl_Loaded(object sender, RoutedEventArgs args)
        {
            TabList.SelectedIndex = 0;
            textBoxSearch.Text = string.Empty;
            LoadUIEvents();
            ConstructViewModelData();
            SetViewModelData();
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs args)
        {
            textBoxSearch.Text = string.Empty;
            UnloadUIEvents();

            if (addMembersPanel.Visibility == Visibility.Visible && _ucCircleAddMembersPanel != null)
            {
                ShowAddedMembers();
            }
            ClearViewModelData();
        }

        private void list_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (!e.Handled)
            {
                e.Handled = true;
                var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                eventArg.Source = sender;
                var parent = ((Control)sender).Parent as UIElement;
                parent.RaiseEvent(eventArg);

            }
        }
        private void list_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            Control control = (Control)sender;
            ListBox listbx = (ListBox)control;

            if (listbx.SelectedItem != null)
            {
                object selectedItem = listbx.SelectedItem;
                ListBoxItem selectedListBoxItem = listbx.ItemContainerGenerator.ContainerFromItem(selectedItem) as ListBoxItem;
                if (selectedListBoxItem == prevItem)
                {
                    selectedListBoxItem.IsSelected = (!selectedListBoxItem.IsSelected);
                    prevItem = null;
                }
                else
                {
                    prevItem = selectedListBoxItem;
                }
            }
        }

        public void frndsScroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                UCMiddlePanelSwitcher.View_UCCirclePanel.myScrlViewer.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                UCMiddlePanelSwitcher.View_UCCirclePanel.myScrlViewer.ScrollToEnd();
                e.Handled = true;
            }
        }
        public void frndsScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                var scrollViewer = (ScrollViewer)sender;
                if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                }
                else if ((e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight) &&
                          (VMCircle.Instance.MembersList.Count - VMCircle.Instance.CircleModel.AdminCount != VMCircle.Instance.MemberCount) &&
                          (VMCircle.Instance.ShowMorePanelVisibility == Visibility.Visible))
                {
                    VMCircle.Instance.ScrollAction();
                }
            }
            catch (Exception ex)
            {
                log.Error("ERROR ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void TabList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                VMCircle.Instance.HideShowMore();
                VMCircle.Instance.ScrollMode = StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL;
                ICollectionView view = CollectionViewSource.GetDefaultView(VMCircle.Instance.MembersList);
                string tabHeader = (TabList.SelectedItem as TabItem).Name.ToString().ToLower();
                switch (tabHeader)
                {
                    case "memberstab":
                        VMCircle.Instance.SearchBoxVisibility = Visibility.Visible;

                        Task.Factory.StartNew(delegate()
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                MembersView();
                                VMCircle.Instance.TabActionMembers();
                                VMCircle.Instance.SearchBoxVisibility = Visibility.Visible;
                            });
                        });

                        break;
                    case "adminstab":
                        if (!string.IsNullOrWhiteSpace(textBoxSearch.Text))
                        {
                            textBoxSearch.Text = string.Empty;
                            VMCircle.Instance.ScrollMode = StatusConstants.CIRCLE_MEMBER_ADMIN_SCROLL;
                        }

                        Task.Factory.StartNew(delegate()
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                VMCircle.Instance.TabActionAdmins();
                                AdminsView();
                            });
                        });
                        break;
                }
                SetScrollMode();
                e.Handled = true;
            }
            catch (Exception)
            {
                //log.Error("ERROR CIRCLE MEMBER TAB SELECTION ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        void list_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
        void list_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void SearchTermTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Back && (string.IsNullOrWhiteSpace(textBoxSearch.Text) || textBoxSearch.Text.Length == 0))
            {
                showMorePanel.Visibility = Visibility.Collapsed;
            }
        }
        private void SearchTermTextBox_GotFocus(object sender, RoutedEventArgs e) { }

        private void AddMembers_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            showStack.Visibility = Visibility.Collapsed;

            if (addMembersPanel.Child == null)
            {
                _ucCircleAddMembersPanel = new UCCircleAddMembersPanel(VMCircle.Instance.CircleModel.UserTableID);
                addMembersPanel.Child = _ucCircleAddMembersPanel;
            }
            addMembersPanel.Visibility = Visibility.Visible;
        }

        void ClearSearchButton_Click(object sender, RoutedEventArgs e)
        {
            //SearchString = string.Empty;
            if (!string.IsNullOrEmpty(textBoxSearch.Text))
            {
                textBoxSearch.Text = string.Empty;
                VMCircle.Instance.ClearSearchRecords();
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    MembersView();
                });
                textBoxSearch.Focus();
            }
        }

        private void LoadMore_ButtonClick(object sender, RoutedEventArgs e)
        {
            VMCircle.Instance.ShowMoreButtonVisibility = Visibility.Collapsed;
            VMCircle.Instance.ScrollAction();
        }
        #endregion      
        
        private static void SetViewModelData()
        {
            VMCircle.Instance.TotalMember = VMCircle.Instance.CircleModel.MemberCount;
            VMCircle.Instance.AdminCount = VMCircle.Instance.CircleModel.AdminCount;
            VMCircle.Instance.MemberCount = VMCircle.Instance.CircleModel.MemberCount - VMCircle.Instance.CircleModel.AdminCount;
            VMCircle.Instance.LoadCircleMembers();
        }

        public void ShowAddedMembers()
        {
            _ucCircleAddMembersPanel = null;
            addMembersPanel.Child = null;
            addMembersPanel.Visibility = Visibility.Collapsed;

            showStack.Visibility = Visibility.Visible;
        }
        
        private static void ConstructViewModelData()
        {
            VMCircle.Instance.PivotID = 0;
            VMCircle.Instance.AdminPivotID = 0;
            VMCircle.Instance.ScrollMode = StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL;
            VMCircle.Instance.ClearSearchRecords();
            VMCircle.Instance.MembersList = new ObservableCollection<UserBasicInfoModel>();
            VMCircle.Instance.MembersSearchList = new ObservableCollection<UserBasicInfoModel>();
            VMCircle.Instance.SearchMap = new Dictionary<string, long>();
            VMCircle.Instance.SearchMapFetcher = new Dictionary<string, bool>();
            VMCircle.Instance.SearchBoxVisibility = Visibility.Visible;
        }

        private void LoadUIEvents()
        {
            list.SelectionChanged += list_SelectionChanged;
            list.PreviewMouseWheel += list_PreviewMouseWheel;
            list.MouseLeftButtonDown += list_MouseLeftButtonDown;
            list.MouseLeftButtonUp += list_MouseLeftButtonUp;

            listSearch.SelectionChanged += list_SelectionChanged;
            listSearch.PreviewMouseWheel += list_PreviewMouseWheel;
            listSearch.MouseLeftButtonUp += list_MouseLeftButtonUp;
            listSearch.MouseLeftButtonDown += list_MouseLeftButtonDown;

            TabList.SelectionChanged += TabList_SelectionChanged;
            ClearSearchButton.Click += ClearSearchButton_Click;

            showMoreButtonPanel.Click += LoadMore_ButtonClick;
            addMemberButton.Click += AddMembers_Click;
        }
        private void UnloadUIEvents()
        {
            list.SelectionChanged -= list_SelectionChanged;
            list.PreviewMouseWheel -= list_PreviewMouseWheel;
            list.MouseLeftButtonDown -= list_MouseLeftButtonDown;
            list.MouseLeftButtonUp -= list_MouseLeftButtonUp;

            listSearch.SelectionChanged -= list_SelectionChanged;
            listSearch.PreviewMouseWheel -= list_PreviewMouseWheel;
            listSearch.MouseLeftButtonUp -= list_MouseLeftButtonUp;
            listSearch.MouseLeftButtonDown -= list_MouseLeftButtonDown;

            ClearSearchButton.Click -= ClearSearchButton_Click;

            showMoreButtonPanel.Click -= LoadMore_ButtonClick;
            addMemberButton.Click -= AddMembers_Click;

            TabList.SelectionChanged -= TabList_SelectionChanged;
        }
        
        private static void ClearViewModelData()
        {
            VMCircle.Instance.PivotID = 0;
            VMCircle.Instance.AdminPivotID = 0;
            VMCircle.Instance.ScrollMode = StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL;
            VMCircle.Instance.ClearSearchRecords();
            VMCircle.Instance.HideShowMore();
            VMCircle.Instance.SearchMap.Clear();
            VMCircle.Instance.SearchMapFetcher.Clear();
            VMCircle.Instance.MembersList.Clear();
            VMCircle.Instance.MembersSearchList.Clear();
            //VMCircle.Instance.ReleaseCircleMembeFromMemory();
            //VMCircle.Instance.ReleaseCircleSearchedMemberFromMemory();
        }

        void SetScrollMode()
        {
            if (TabList.SelectedIndex == 0 && !string.IsNullOrWhiteSpace(textBoxSearch.Text))
            {
                //ScrollMode = StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL;
                VMCircle.Instance.ScrollMode = StatusConstants.CIRCLE_MEMBER_NORMAL_SEARCH;
            }
            else if (TabList.SelectedIndex == 0 && string.IsNullOrWhiteSpace(textBoxSearch.Text))
            {
                VMCircle.Instance.ScrollMode = StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL;
            }
            else if (TabList.SelectedIndex == 1)
            {
                VMCircle.Instance.ScrollMode = StatusConstants.CIRCLE_MEMBER_ADMIN_SCROLL;
            }
        }
        
        public void SearchResultView(string searchParam)
        {
            try
            {
                listSearch.ItemsSource = null;
                listSearch.ItemsSource = VMCircle.Instance.GetSearchResult(searchParam);
            }
            catch (Exception)
            {

                //throw;
            }
        }

        private void AdminsView()
        {
            try
            {
                list.ItemsSource = null;
                list.ItemsSource = VMCircle.Instance.GetAdmins();
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void MembersView()
        {
            try
            {
                list.ItemsSource = null;
                list.ItemsSource = View.Utility.Circle.VMCircle.Instance.GetMembers();
            }
            catch (Exception)
            {

                //throw;
            }
        }
    }
}

﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.Utility;
using View.Utility.Circle;

namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for UCCirclePanel.xaml
    /// </summary>
    public partial class UCCirclePanel : UserControl, INotifyPropertyChanged
    {
        //private long circleId;
        //private long membersCount;
        //private long adminCount;
        //private short my_membership_state = 0; //0=i am member,1=i am normal admin,2=i am superadmin
        public UCCircleMembersPanel _UCCircleMembersPanel;
        public UCCircleNewsFeeds _UCCircleNewsFeeds;
        //public bool IS_CIRCLE_MEMBERS_REQUESTED = false;

        public UCCirclePanel(CircleModel model)
        {
            //circleModel = model;
            View.Utility.Circle.VMCircle.Instance.CircleModel = model;
            //this.circleId = model.CircleId;
            //this.membersCount = model.MemberCount;
            //this.adminCount = model.AdminCount <= 0 ? RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY[model.CircleId].AdminCount : model.AdminCount;
            InitializeComponent();
            VMCircle.Instance.TotalMembersLabel = "Participants";
            //this.DataContext = this;
            this.DataContext = VMCircle.Instance;
            //ShowFeedsTab();
            ChangeEventHandler();
            new ThreadFetchCircleDetails(model.UserTableID);
        }

        public void ChangeEventHandler(bool Add = true)
        {
            this.TabList.SelectionChanged -= TabControl_SelectionChanged;
            if (Add)
                this.TabList.SelectionChanged += TabControl_SelectionChanged;
        }

        //private CircleModel _circleModel;
        //public CircleModel circleModel
        //{
        //    get
        //    {
        //        return _circleModel;
        //    }
        //    set
        //    {
        //        _circleModel = value;
        //        this.OnPropertyChanged("circleModel");
        //    }
        //}
        //public void RemoveTopOrBottomModels()
        //{
        //    bool _TopLoad = (myScrlViewer.VerticalOffset == 0 || myScrlViewer.VerticalOffset <= (myScrlViewer.ScrollableHeight / 2));
        //    if (_TopLoad)
        //    {
        //        myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
        //        RingIDViewModel.Instance.NewsFeeds.RemoveBottomModels();
        //        myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
        //    }
        //    else
        //    {
        //        myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
        //        RingIDViewModel.Instance.NewsFeeds.RemoveTopModels();
        //        myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;

        //        Application.Current.Dispatcher.Invoke((Action)delegate
        //        {
        //            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
        //            if (myScrlViewer.ScrollableHeight > 50 && myScrlViewer.VerticalOffset == myScrlViewer.ScrollableHeight)
        //            {
        //                myScrlViewer.ScrollToVerticalOffset(myScrlViewer.ScrollableHeight - 50);
        //            }
        //            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;

        //        }, DispatcherPriority.Send);
        //    }
        //}
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        //private void CircleLeaveMenuItem_Click(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    MessageBoxResult result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.CIRCLE_LEAVE_CONFIRMATION_MESSAGE, VMCircle.Instance.CircleModel.CircleName), NotificationMessages.HEADER_CIRCLE_LEAVE);
        //    if (result == MessageBoxResult.Yes)
        //    {
        //        CircleUtility.Instance.LeaveCircle(VMCircle.Instance.CircleModel.CircleId);
        //    }
        //}

        //private void CircleDeleteMenuItem_Click(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    MessageBoxResult result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.CIRCLE_DELETE_CONFIRMATION_MESSAGE, VMCircle.Instance.CircleModel.CircleName), NotificationMessages.HEADER_CIRCLE_DELETE);
        //    if (result == MessageBoxResult.Yes)
        //    {
        //        CircleUtility.Instance.DeleteCircle(VMCircle.Instance.CircleModel.CircleId);
        //    }
        //}

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //string tabHeader = (TabList.SelectedItem as TabItem).Header.ToString().ToLower();
            int tabHeader = TabList.SelectedIndex;
            switch (tabHeader)
            {
                case 0:
                    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(0, 0, VMCircle.Instance.CircleModel.UserTableID);
                    ShowFeedsTab();
                    break;
                case 1:
                    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(1, 0, VMCircle.Instance.CircleModel.UserTableID);
                    ShowMembersTab();
                    break;
                default:
                    break;
            }
        }

        public void ShowFeedsTab()
        {
            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;

            myScrlViewer.ScrollChanged -= myScrlViewer.ScrollPositionChanged;
            myScrlViewer.ScrollChanged += myScrlViewer.ScrollPositionChanged;
            TabItem tabItemCirclePosts = TabList.SelectedItem as TabItem;
            if (_UCCircleNewsFeeds == null)
            {
                _UCCircleNewsFeeds = new UCCircleNewsFeeds(VMCircle.Instance.CircleModel.UserTableID, myScrlViewer);
                ((Border)tabItemCirclePosts.Content).Child = _UCCircleNewsFeeds;
            }
        }

        public void ShowMembersTab()
        {
            myScrlViewer.ScrollChanged -= myScrlViewer.ScrollPositionChanged;

            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
            TabItem item2 = TabList.SelectedItem as TabItem;
            if (((Border)item2.Content).Child == null)
            {
                if (_UCCircleMembersPanel == null)
                {
                    //_UCCircleMembersPanel = new UCCircleMembersPanel(circleId, membersCount, adminCount);
                    _UCCircleMembersPanel = new UCCircleMembersPanel();
                    ((Border)item2.Content).Child = _UCCircleMembersPanel;
                    //_UCCircleMembersPanel.LoadCircleMemberList();
                }
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            backBtn.Click += backBtn_Click;
            TabList.SelectionChanged += TabControl_SelectionChanged;
            //myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
            //myScrlViewer.PreviewKeyDown += myScrlViewer_PreviewKeyDown;
        }

        //void myScrlViewer_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        //{
        //    var scrollviewer = sender as ScrollViewer;
        //    string tabHeader = (TabList.SelectedItem as TabItem).Header.ToString().ToLower();
        //    switch (tabHeader)
        //    {
        //        case "post":
        //            _UCCircleNewsFeeds.circleFeedScrlViewer_PreviewKeyDown(sender, e);
        //            break;
        //        case "members":
        //            _UCCircleMembersPanel.frndsScroll_PreviewKeyDown(sender, e);
        //            break;
        //    }
        //}

        void myScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                //var scrollviewer = sender as ScrollViewer;
                //switch (TabList.SelectedIndex)
                //{
                //    case 0:
                //        _UCCircleNewsFeeds.circleFeedScrlViewer_ScrollChanged(sender, e);
                //        break;
                //    case 1:
                //        if (_UCCircleMembersPanel == null)
                //        {
                //            _UCCircleMembersPanel = new UCCircleMembersPanel();
                //            ((Border)(TabList.SelectedItem as TabItem).Content).Child = _UCCircleMembersPanel;
                //        }
                //        _UCCircleMembersPanel.frndsScroll_ScrollChanged(sender, e);
                //        break;
                //}
                if (TabList.SelectedIndex == 1)
                {
                    if (_UCCircleMembersPanel == null)
                    {
                        _UCCircleMembersPanel = new UCCircleMembersPanel();
                        ((Border)(TabList.SelectedItem as TabItem).Content).Child = _UCCircleMembersPanel;
                    }
                    _UCCircleMembersPanel.frndsScroll_ScrollChanged(sender, e);
                }
            }
            catch (System.Exception)
            {
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            backBtn.Click -= backBtn_Click;
            TabList.SelectionChanged -= TabControl_SelectionChanged;
            //myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
            //myScrlViewer.PreviewKeyDown -= myScrlViewer_PreviewKeyDown;
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }
    }
}

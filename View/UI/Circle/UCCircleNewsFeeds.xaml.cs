﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.UI.Feed;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for UCCircleNewsFeeds.xaml
    /// </summary>
    public partial class UCCircleNewsFeeds : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCircleNewsFeeds).Name);
        public CustomScrollViewer scroll;
        private long _CircleId;
        public long CircleId
        {
            get
            {
                return _CircleId;
            }
            set
            {
                _CircleId = value;
                this.OnPropertyChanged("CircleId");
            }
        }

        public UCCircleNewsFeeds(long circleId, CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            this.DataContext = this;
            this.CircleId = circleId;
            InitializeComponent();
            //if (RingIDViewModel.Instance.CircleProfileFeeds.Count > 2) RingIDViewModel.Instance.CircleProfileFeeds.RemoveAllModels();
            //if (RingIDViewModel.Instance.CircleProfileFeeds.Count != 2) RingIDViewModel.Instance.CircleProfileFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_CIRCLE, true); //safety check
            FeedDataContainer.Instance.CircleProfileFeedsSortedIds.Clear();

            //RingIDViewModel.Instance.CircleProfileFeeds.UserProfileUtId = circleId;//only for profiles
            //RingIDViewModel.Instance.CircleProfileFeeds.pId = 0;
            //ViewCollection = RingIDViewModel.Instance.CircleProfileFeeds;
            scroll.SetScrollValues(ViewCollection, FeedDataContainer.Instance.CircleProfileFeedsSortedIds, SettingsConstants.PROFILE_TYPE_CIRCLE, AppConstants.TYPE_CIRCLE_NEWSFEED);

            DefaultSettings.PROFILECIRCLE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.PROFILECIRCLE_STARTPKT);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private CustomObservableCollection _ViewCollection;
        public CustomObservableCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        public NewStatusView NewStatusView = null;
        public NewStatusViewModel NewStatusViewModel = null;

        private void newStatusViewInstance_Loaded(object sender, RoutedEventArgs e)
        {
            NewStatusView = (NewStatusView)sender;
            NewStatusViewModel = (NewStatusViewModel)NewStatusView.DataContext;
            NewStatusViewModel.WriteOnText = "Write something . . .";
            NewStatusViewModel.WritePostText = "Write Post";
            NewStatusViewModel.PrivacyButtonVisibility = Visibility.Collapsed;
            NewStatusViewModel.NewStatusTagFriendButtonVisibility = Visibility.Collapsed;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs args)
        {
            scroll.ScrollToHome();
            if (ViewCollection == null)
            {
                if (timer == null)
                {
                    timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromMilliseconds(500);
                    timer.Tick += TimerEventProcessor;
                }
                else timer.Stop();
                timer.Start();
            }
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs args)
        {
            //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
            //{
                scroll.SetScrollEvents(false);
                ViewCollection = null;
            //}
        }
        private DispatcherTimer timer;
        private void TimerEventProcessor(object sender, EventArgs e)
        {
            if (timer != null && timer.IsEnabled)
            {
                //ViewCollection = RingIDViewModel.Instance.CircleProfileFeeds;
                scroll.SetScrollEvents(true);
                if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    Keyboard.Focus(scroll);
                timer.Stop();
            }
        }

        //public bool IsLoadMoreEnabled
        //{
        //    get { return (bool)GetValue(IsLoadMoreEnabledProperty); }
        //    set { SetValue(IsLoadMoreEnabledProperty, value); }
        //}

        //// Using a DependencyProperty as the backing store for IsLoadMoreEnabled.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty IsLoadMoreEnabledProperty =
        //    DependencyProperty.Register("IsLoadMoreEnabled", typeof(bool), typeof(UCCircleNewsFeeds), new PropertyMetadata(false));

        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }
        private void OnReloadCommandClicked()
        {
            DefaultSettings.PROFILECIRCLE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.PROFILECIRCLE_STARTPKT);
        }

        #region "Utility Methods"
        #endregion "Utility Methods"
    }
}

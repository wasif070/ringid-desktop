﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.Constants;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Circle;
using View.Utility.Feed;
using View.ViewModel;


namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for UCCircleInitPanel.xaml
    /// </summary>
    public partial class UCCircleInitPanel : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCircleInitPanel).Name);
        public UCCircleNewsFeeds _UCCircleNewsFeeds;
        public UCCircleList _UCCircleList;
        public UCSavedCircleFeeds _UCSavedFeed;
        public UCCircleAllNewsFeeds _UCCircleAllFeeds;
        private bool _IsAlreadyRequested = false;
        private int _WatingTime = 0;

        public UCCircleInitPanel()
        {
            InitializeComponent();
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeCircleMain, VMCircle.Instance.TabType);
            SwitchToTab(VMCircle.Instance.TabType);
            this.Loaded += UCCircleInitPanel_Loaded;
            this.DataContext = VMCircle.Instance;
        }

        private void UCCircleInitPanel_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_IsAlreadyRequested == false && DefaultSettings.TOTAL_CIRCLES_COUNT == 0 || (DefaultSettings.TOTAL_CIRCLES_COUNT != VMCircle.Instance.TotalCircles)) // This additional request is done because lots of sequences is missing first request.
            {
                new System.Threading.Thread(RunCircleListResender).Start();
            }
        }

        #region Utility

        public void SwitchToTab(int SelectedIndex)
        {
            if (VMCircle.Instance.TabType != SelectedIndex)
                VMCircle.Instance.TabType = SelectedIndex;
            switch (SelectedIndex)
            {
                case 0://"FeedsTab":
                    if (_UCCircleAllFeeds == null)
                    {
                        _UCCircleAllFeeds = new UCCircleAllNewsFeeds(ScrlViewer);
                    }
                    brdChild.Child = _UCCircleAllFeeds;
                    break;

                case 1://"MyCircleTab":
                    if (_UCCircleList == null)
                    {
                        _UCCircleList = new UCCircleList(ScrlViewer);
                        _UCCircleList.LoadAllCirclesToUI();
                    }

                    brdChild.Child = _UCCircleList;
                    break;

                case 2://"SavedTab":
                    if (_UCSavedFeed == null)
                    {
                        _UCSavedFeed = new UCSavedCircleFeeds(ScrlViewer);
                    }

                    brdChild.Child = _UCSavedFeed;
                    break;
            }
        }

        private void RunCircleListResender()
        {
            while (_WatingTime != 5000)
            {
                System.Threading.Thread.Sleep(500);
                _WatingTime += 500;
            }

            _IsAlreadyRequested = true;

            if (DefaultSettings.TOTAL_CIRCLES_COUNT == 0 || (DefaultSettings.TOTAL_CIRCLES_COUNT != VMCircle.Instance.TotalCircles))
            {
                SendCircleListRequest();
            }
        }

        public void SendCircleListRequest()
        {
            Newtonsoft.Json.Linq.JObject pakToSend = new Newtonsoft.Json.Linq.JObject();
            string pakId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = pakId;
            pakToSend[JsonKeys.UpdateTime] = 0; // SettingsConstants.VALUE_RINGID_CIRCLE_UT;
            AuthRequestNoResult(pakToSend, AppConstants.TYPE_CIRCLE_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        private void AuthRequestNoResult(Newtonsoft.Json.Linq.JObject pakToSend2, int action1, int requestType, string message = null)
        {
            new AuthRequestNoResult().StartThread(pakToSend2, action1, requestType, message);
        }

        #endregion

        #region "ICommand"

        #endregion

    }
}

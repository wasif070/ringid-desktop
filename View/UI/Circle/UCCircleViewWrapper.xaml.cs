﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Circle;
using View.Utility.DataContainer;

namespace View.UI.Circle
{
    /// <summary>
    /// Interaction logic for UCCircleViewWrapper.xaml
    /// </summary>
    public partial class UCCircleViewWrapper : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCircleViewWrapper).Name);

        private UserBasicInfoModel _FriendModel;
        private bool _IsThreadAlreadyRunning = false;
        private bool _IS_LOADING = false;
        private Thread ScrollThread = null;

        public UCCircleViewWrapper(Grid motherGrid)
        {
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, ViewConstants.POP_UP_BG_BORDER2, 1.0);
            SetParent(motherGrid);
            this.DataContext = this;
            this.Loaded += UCCircleViewWrapper_Loaded;
        }

        private void UCCircleViewWrapper_Loaded(object sender, RoutedEventArgs e)
        {
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
            this.Scroll.ScrollChanged += Scroll_ScrollChanged;
        }

        #region Utility

        public void LoadCirclesList()
        {
            try
            {
                if (_IsThreadAlreadyRunning == false)
                {
                    new Thread(() =>
                    {
                        _IsThreadAlreadyRunning = true;
                        List<CircleModel> circleModels = CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.Values.Where(P => CircleDataContainer.Instance.CircleIDs.Any(Q => P.UserTableID == Q)).OrderBy(P => P.FullName).ToList();
                        TotalCircles = circleModels.Count;

                        foreach (CircleModel circleModel in circleModels)
                        {
                            if (circleModel.IntegerStatus != StatusConstants.STATUS_DELETED)
                            {
                                if (circleModel.SuperAdmin == DefaultSettings.LOGIN_RING_ID)
                                {
                                    if (!CircleListYouManage.Any(x => x.UserTableID == circleModel.UserTableID))
                                    {
                                        CircleListYouManage.InvokeAdd(circleModel);
                                        Thread.Sleep(10);
                                    }
                                }
                                else
                                {
                                    if (CircleListYouManage.Count + CircleListYouAreIn.Count <= 12 && !CircleListYouAreIn.Any(x => x.UserTableID == circleModel.UserTableID))
                                    {
                                        CircleListYouAreIn.InvokeAdd(circleModel);
                                        Thread.Sleep(10);
                                    }
                                }
                            }
                        }

                        _IsThreadAlreadyRunning = false;

                    }).Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadCircleList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                _IsThreadAlreadyRunning = false;
            }
        }

        private void LoadCircleByScroll()
        {
            int prevCount = CircleListYouAreIn.Count;
            List<CircleModel> circleModeList = CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.Values.Where(P => CircleDataContainer.Instance.CircleIDs.Any(Q => P.UserTableID == Q && P.SuperAdmin != DefaultSettings.LOGIN_RING_ID)).ToList();
            foreach (CircleModel model in circleModeList)
            {
                if (!CircleListYouAreIn.Any(P => P.UserTableID == model.UserTableID))
                {
                    CircleListYouAreIn.InvokeAdd(model);
                    Thread.Sleep(10);
                }
                if (CircleListYouAreIn.Count >= prevCount + 10)
                {
                    break;
                }
            }

            _IS_LOADING = false;
        }

        private void OnSearch()
        {
            List<CircleModel> circleModels = CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.Values.Where(P => CircleDataContainer.Instance.CircleIDs.Any(Q => P.UserTableID == Q)).OrderBy(P => P.FullName).ToList();

            List<CircleModel> tempCircleList = circleModels.Where(P => P.FullName.ToLower().Contains(CircleSearchString.ToLower())).ToList();
            foreach (CircleModel model in tempCircleList)
            {
                CircleListYouSearched.InvokeAdd(model);
            }
        }

        public void SetUserModel(UserBasicInfoModel model)
        {
            this._FriendModel = model;
        }

        public void OnCloseCommand()
        {
            base.Hide();
            IsCircleSearch = false;
            CircleSearchString = string.Empty;

            if (_IsThreadAlreadyRunning == false)
            {
                CircleListYouManage.Clear();
                CircleListYouAreIn.Clear();
                CircleListYouSearched.Clear();
            }

            this.Scroll.ScrollChanged -= Scroll_ScrollChanged;
        }

        public void SetUIControlOfCircleViewWrapper()
        {
            EnablePanel = true;
            IsReadOnlyTextBox = false;
            LoaderVisibility = Visibility.Collapsed;
        }

        private void ActionEmpty(object param)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region EventHandler

        private void Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            if (IsCircleSearch == false && _IS_LOADING == false && e.VerticalChange > 0 && e.VerticalOffset >= (Scroll.ScrollableHeight - 40))
            {
                _IS_LOADING = true;
                if (!(ScrollThread != null && ScrollThread.IsAlive))
                {
                    ScrollThread = new Thread(new ThreadStart(LoadCircleByScroll));
                    ScrollThread.Start();
                }
            }
        }

        private void OnDeatilsCommand(object param)
        {
            CircleModel circleModel = (CircleModel)param;
            EnablePanel = false;
            IsReadOnlyTextBox = true;
            LoaderVisibility = Visibility.Visible;
            List<CircleMemberDTO> circleMembers = new List<CircleMemberDTO>();
            //<<<<<<< .working
            //            CircleMemberDTO circleMember = friendModel.GetCircleMemberDTOFromModel(circleModel.UserTableID, 0);
            //=======
            CircleMemberDTO circleMember = _FriendModel.GetCircleMemberDTOFromModel(circleModel.UserTableID, 0);
            ///>>>>>>> .merge-right.r2138
            circleMembers.Add(circleMember);
            new ThradAddCircleMember().StartThread(circleModel.UserTableID, circleMembers);
        }

        #endregion

        #region Property

        private ObservableCollection<CircleModel> _CircleListYouManage = new ObservableCollection<CircleModel>();
        public ObservableCollection<CircleModel> CircleListYouManage
        {
            get
            {
                return _CircleListYouManage;
            }
            set
            {
                if (_CircleListYouManage == value) return;
                _CircleListYouManage = value;
                OnPropertyChanged("CircleListYouManage");
            }
        }

        private ObservableCollection<CircleModel> _CircleListYouAreIn = new ObservableCollection<CircleModel>();
        public ObservableCollection<CircleModel> CircleListYouAreIn
        {
            get
            {
                return _CircleListYouAreIn;
            }
            set
            {
                if (_CircleListYouAreIn == value) return;
                _CircleListYouAreIn = value;
                OnPropertyChanged("CircleListYouAreIn");
            }
        }

        private ObservableCollection<CircleModel> _CircleListYouSearched = new ObservableCollection<CircleModel>();
        public ObservableCollection<CircleModel> CircleListYouSearched
        {
            get
            {
                return _CircleListYouSearched;
            }
            set
            {
                if (_CircleListYouSearched == value) return;
                _CircleListYouSearched = value;
                OnPropertyChanged("CircleListYouSearched");
            }
        }

        private int _TotalCircles;
        public int TotalCircles
        {
            get
            {
                return _TotalCircles;
            }
            set
            {
                _TotalCircles = value;
                this.OnPropertyChanged("TotalCircles");
            }
        }

        private bool _IsCircleSearch = false;
        public bool IsCircleSearch
        {
            get { return _IsCircleSearch; }
            set
            {
                if (_IsCircleSearch == value) return;
                _IsCircleSearch = value;
                OnPropertyChanged("IsCircleSearch");
            }
        }

        private bool _IsNoCircleFound = false;
        public bool IsNoCircleFound
        {
            get { return _IsNoCircleFound; }
            set
            {
                _IsNoCircleFound = value;
                OnPropertyChanged("IsNoCircleFound");
            }
        }

        private string _CircleSearchString = string.Empty;
        public string CircleSearchString
        {
            get { return _CircleSearchString; }
            set
            {
                _CircleSearchString = value;
                IsNoCircleFound = false;
                CircleListYouSearched.Clear();
                OnPropertyChanged("CircleSearchString");

                if (!string.IsNullOrWhiteSpace(_CircleSearchString) && _CircleSearchString.Trim().Length > 0)
                {
                    IsCircleSearch = true;
                    OnSearch();
                }
                else
                {
                    IsCircleSearch = false;
                }
            }
        }

        private bool _EnablePanel = true;
        public bool EnablePanel
        {
            get
            {
                return _EnablePanel;
            }
            set
            {
                _EnablePanel = value;
                this.OnPropertyChanged("EnablePanel");
            }
        }

        private bool _IsReadOnlyTextBox = false;
        public bool IsReadOnlyTextBox
        {
            get
            {
                return _IsReadOnlyTextBox;
            }
            set
            {
                _IsReadOnlyTextBox = value;
                this.OnPropertyChanged("IsReadOnlyTextBox");
            }
        }

        private Visibility _LoaderVisibility = Visibility.Collapsed;
        public Visibility LoaderVisibility
        {
            get
            {
                return _LoaderVisibility;
            }
            set
            {
                _LoaderVisibility = value;
                this.OnPropertyChanged("LoaderVisibility");
            }
        }

        #endregion

        #region Command

        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(param => OnCloseCommand());
                }
                return _CloseCommand;
            }
        }

        private ICommand _CircleDeatilsCommand;
        public ICommand CircleDeatilsCommand
        {
            get
            {
                if (_CircleDeatilsCommand == null)
                {
                    _CircleDeatilsCommand = new RelayCommand(param => OnDeatilsCommand(param));
                }
                return _CircleDeatilsCommand;
            }
        }

        #endregion
    }
}

﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.UI.Channel;
using View.UI.Stream;
using View.Utility;
using View.ViewModel;

namespace View.UI.StreamAndChannel
{
    /// <summary>
    /// Interaction logic for UCStreamAndChannelMainPanel.xaml
    /// </summary>
    public partial class UCStreamAndChannelMainPanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamAndChannelMainPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        public UCStreamAndChannelHomePanel StreamAndChannelHomePanel = null;
        public UCStreamHomePanel StreamHomePanel = null;
        public UCChannelHomePanel ChannelHomePanel = null;

        private int _TabType = -1;
        private ICommand _TabChangeCommand;

        #region Constructor

        public UCStreamAndChannelMainPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            switch (this.TabType)
            {
                case 0:
                    ShowStreamAndChannelHomeView(true);
                    break;
                case 1:
                    ShowStreamHomeView(true);
                    break;
                case 2:
                    ShowChannelHomeView(true);
                    break;
                default:
                    {
                        this.TabType = 0;
                        ShowStreamAndChannelHomeView(true);
                    }
                    break;
            }
        }

        public void ReleaseViewer()
        {
            Destroy(this.TabType);
        }

        private void OnTabChangeClicked(object param)
        {
            try
            {
                int type = Int32.Parse(param.ToString());
                bool isChanged = this.TabType != type;

                if (isChanged)
                {
                    Destroy(this.TabType);
                }
                this.TabType = type;

                switch (this.TabType)
                {
                    case 0:
                        ShowStreamAndChannelHomeView(isChanged);
                        break;
                    case 1:
                        ShowStreamHomeView(isChanged);
                        break;
                    case 2:
                        ShowChannelHomeView(isChanged);
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnTabChangeClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Destroy(int type)
        {
            switch (type)
            {
                case 0:
                    if (StreamAndChannelHomePanel != null)
                    {
                        ScrlViewer.ScrollChanged -= StreamAndChannelHomePanel.ScrlViewer_ScrollChanged;
                        ScrlViewer.SizeChanged -= StreamAndChannelHomePanel.ScrlViewer_SizeChanged;
                        txtSearch.TextChanged -= StreamAndChannelHomePanel.SearchBox_TextChanged;
                        cmbStreamCategory.SelectionChanged -= StreamAndChannelHomePanel.Category_SelectionChanged;
                        cmbChannelCategory.SelectionChanged -= StreamAndChannelHomePanel.Category_SelectionChanged;
                        btnReload.Click -= StreamAndChannelHomePanel.Reload_Click;
                        StreamAndChannelHomePanel.ReleaseViewer();
                    }
                    break;
                case 1:
                    if (StreamHomePanel != null)
                    {
                        ScrlViewer.ScrollChanged -= StreamHomePanel.ScrlViewer_ScrollChanged;
                        ScrlViewer.SizeChanged -= StreamHomePanel.ScrlViewer_SizeChanged;
                        txtSearch.TextChanged -= StreamHomePanel.SearchBox_TextChanged;
                        cmbStreamCategory.SelectionChanged -= StreamHomePanel.Category_SelectionChanged;
                        btnReload.Click -= StreamHomePanel.Reload_Click;
                        StreamHomePanel.ReleaseViewer();
                    }
                    break;
                case 2:
                    if (ChannelHomePanel != null)
                    {
                        ScrlViewer.ScrollChanged -= ChannelHomePanel.ScrlViewer_ScrollChanged;
                        ScrlViewer.SizeChanged -= ChannelHomePanel.ScrlViewer_SizeChanged;
                        txtSearch.TextChanged -= ChannelHomePanel.SearchBox_TextChanged;
                        cmbChannelCategory.SelectionChanged -= ChannelHomePanel.Category_SelectionChanged;
                        btnReload.Click -= ChannelHomePanel.Reload_Click;
                        ChannelHomePanel.ReleaseViewer();
                    }
                    break;
            }
        }

        public void ShowStreamAndChannelHomeView(bool isChanged)
        {
            try
            {
                if (StreamAndChannelHomePanel == null)
                {
                    StreamAndChannelHomePanel = new UCStreamAndChannelHomePanel();
                }
                if (isChanged)
                {
                    cmbStreamCategory.SelectedIndex = 0;
                    cmbChannelCategory.SelectedIndex = 0;
                    StreamAndChannelHomePanel.InilializeViewer();
                    ScrlViewer.ScrollChanged += StreamAndChannelHomePanel.ScrlViewer_ScrollChanged;
                    ScrlViewer.SizeChanged += StreamAndChannelHomePanel.ScrlViewer_SizeChanged;
                    txtSearch.TextChanged += StreamAndChannelHomePanel.SearchBox_TextChanged;
                    cmbStreamCategory.SelectionChanged += StreamAndChannelHomePanel.Category_SelectionChanged;
                    cmbChannelCategory.SelectionChanged += StreamAndChannelHomePanel.Category_SelectionChanged;
                    btnReload.Click += StreamAndChannelHomePanel.Reload_Click;
                    bdrParent.Child = StreamAndChannelHomePanel;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowStreamAndChannelHomeView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        

        public void ShowStreamHomeView(bool isChanged)
        {
            try
            {
                if (StreamHomePanel == null)
                {
                    StreamHomePanel = new UCStreamHomePanel();
                }
                if (isChanged)
                {
                    StreamHomePanel.InilializeViewer();
                    ScrlViewer.ScrollChanged += StreamHomePanel.ScrlViewer_ScrollChanged;
                    ScrlViewer.SizeChanged += StreamHomePanel.ScrlViewer_SizeChanged;
                    txtSearch.TextChanged += StreamHomePanel.SearchBox_TextChanged;
                    cmbStreamCategory.SelectionChanged += StreamHomePanel.Category_SelectionChanged;
                    btnReload.Click += StreamHomePanel.Reload_Click;
                    bdrParent.Child = StreamHomePanel;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowStreamHomeView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowChannelHomeView(bool isChanged)
        {
            try
            {
                if (ChannelHomePanel == null)
                {
                    ChannelHomePanel = new UCChannelHomePanel();
                }
                if (isChanged)
                {
                    ChannelHomePanel.InilializeViewer();
                    ScrlViewer.ScrollChanged += ChannelHomePanel.ScrlViewer_ScrollChanged;
                    ScrlViewer.SizeChanged += ChannelHomePanel.ScrlViewer_SizeChanged;
                    txtSearch.TextChanged += ChannelHomePanel.SearchBox_TextChanged;
                    cmbChannelCategory.SelectionChanged += ChannelHomePanel.Category_SelectionChanged;
                    btnReload.Click += ChannelHomePanel.Reload_Click;
                    bdrParent.Child = ChannelHomePanel;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowChannelHomeView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand TabChangeCommand
        {
            get
            {
                if (_TabChangeCommand == null)
                {
                    _TabChangeCommand = new RelayCommand(param => OnTabChangeClicked(param));
                }
                return _TabChangeCommand;
            }
        }

        public int TabType
        {
            get { return _TabType; }
            set
            {
                if (_TabType == value)
                    return;

                _TabType = value;
                this.OnPropertyChanged("TabType");
            }
        }

        #endregion Property

    }
}

﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.UI.Channel;
using View.UI.Stream;
using View.Utility;
using View.Utility.Stream;
using View.ViewModel;

namespace View.UI.StreamAndChannel
{
    /// <summary>
    /// Interaction logic for UCStreamAndChannelViewer.xaml
    /// </summary>
    public partial class UCStreamAndChannelViewer : UserControl
    {
        private static UCStreamAndChannelViewer _Instance;
        public static UCStreamLiveWrapper StreamLiveWrapper = null;
        public static UCChannelViewer ChannelViewer = null;

        public UCStreamAndChannelViewer()
        {
            InitializeComponent();
        }

        public void Show(StreamModel streamModel = null)
        {
            if(ChannelViewer != null)
            {
                ChannelViewer.Dispose();
                ChannelViewer = null;
            }

            if (streamModel != null)
            {
                if (StreamLiveWrapper != null && StreamLiveWrapper.IS_INITIALIZED && StreamLiveWrapper.StreamInfoModel.UserTableID != streamModel.UserTableID)
                {
                    if (StreamLiveWrapper.StreamInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID && StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_FINISHED)
                    {
                        if (!UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Quit Live"), string.Format(NotificationMessages.SURE_WANT_TO, "quit live broadcasting")))
                        {
                            return;
                        }
                    }
                    StreamLiveWrapper.Dispose();
                    StreamLiveWrapper = null;
                }
            }
            else
            {
                if (StreamLiveWrapper != null && StreamLiveWrapper.IS_INITIALIZED && StreamLiveWrapper.StreamInfoModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
                {
                    StreamLiveWrapper.Dispose();
                    StreamLiveWrapper = null;
                }
            }

            if (StreamLiveWrapper == null || StreamLiveWrapper.IS_INITIALIZED == false)
            {
                StreamLiveWrapper = new UCStreamLiveWrapper(streamModel);
                StreamLiveWrapper.InilializeViewer();
            }
            else
            {
                StreamLiveWrapper.HideWindowPreview();
            }

            Content = StreamLiveWrapper;
            Visibility = Visibility.Visible;

            RingIDViewModel.Instance.MainUserControlChanged -= HelperMethods.OnMainUserControlChanged;
            RingIDViewModel.Instance.MainUserControlChanged += HelperMethods.OnMainUserControlChanged;
        }

        public void Show(ChannelModel channelModel)
        {
            if (StreamLiveWrapper != null && StreamLiveWrapper.IS_INITIALIZED)
            {
                if (StreamLiveWrapper.StreamInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID && StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_FINISHED)
                {
                    if (!UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Quit Live"), string.Format(NotificationMessages.SURE_WANT_TO, "quit live broadcasting")))
                    {
                        return;
                    }
                }

                StreamLiveWrapper.Dispose();
                StreamLiveWrapper = null;
            }

            if (ChannelViewer != null && ChannelViewer.IS_INITIALIZED && ChannelViewer.ChannelInfoModel.ChannelID != channelModel.ChannelID)
            {
                ChannelViewer.Dispose();
                ChannelViewer = null;
            }

            if (ChannelViewer == null || ChannelViewer.IS_INITIALIZED == false)
            {
                ChannelViewer = new UCChannelViewer(channelModel);
                ChannelViewer.InilializeViewer();
            }
            else
            {
                ChannelViewer.HideWindowPreview();
            }

            Content = ChannelViewer;
            Visibility = Visibility.Visible;

            RingIDViewModel.Instance.MainUserControlChanged -= HelperMethods.OnMainUserControlChanged;
            RingIDViewModel.Instance.MainUserControlChanged += HelperMethods.OnMainUserControlChanged;
        }

        public void ShowWindow()
        {
            RingIDViewModel.Instance.MainUserControlChanged -= HelperMethods.OnMainUserControlChanged;

            if (StreamLiveWrapper != null && StreamLiveWrapper.IS_INITIALIZED)
            {
                StreamLiveWrapper.ShowWindowPreview();
                Visibility = Visibility.Collapsed;
            }

            if (ChannelViewer != null && ChannelViewer.IS_INITIALIZED)
            {
                ChannelViewer.ShowWindowPreview();
                Visibility = Visibility.Collapsed;
            }
        }

        public void Dispose()
        {
            RingIDViewModel.Instance.MainUserControlChanged -= HelperMethods.OnMainUserControlChanged;

            if (StreamLiveWrapper != null)
            {
                StreamLiveWrapper.Dispose();
                StreamLiveWrapper = null;
            }
            if (ChannelViewer != null)
            {
                ChannelViewer.Dispose();
                ChannelViewer = null;
            }
            Content = null;

            UCGuiRingID.Instance.RemoveUserControlFromMiddlePanel(_Instance);
            _Instance = null;
        }

        #region Property

        public static UCStreamAndChannelViewer Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new UCStreamAndChannelViewer();
                    _Instance.Visibility = Visibility.Collapsed;
                    UCGuiRingID.Instance.AddUserControlInMiddlePanel(_Instance);
                }
                return _Instance;
            }
        }

        #endregion Property

    }
}

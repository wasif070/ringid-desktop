﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.StreamAndChannel
{
    /// <summary>
    /// Interaction logic for UCLoadingAnimation.xaml
    /// </summary>
    public partial class UCLoadingAnimation : UserControl
    {
        Storyboard _EllipseAnimation;
        Storyboard _IconAnimation;

        public UCLoadingAnimation()
        {
            InitializeComponent();
            this.Visibility = System.Windows.Visibility.Hidden;
            this._IconAnimation = (Storyboard)FindResource("IconAnimation");
            this._EllipseAnimation = (Storyboard)FindResource("EllipseAnimation");
        }
        
        public void StartAnimation()
        {
            this.Visibility = System.Windows.Visibility.Visible;
            if (_IconAnimation != null)
            {
                _IconAnimation.Begin();
            }
            if (_EllipseAnimation != null)
            {
                _EllipseAnimation.Begin();
            }
        }

        public void StopAnimation()
        {
            this.Visibility = System.Windows.Visibility.Hidden;
            if (_EllipseAnimation != null)
            {
                _EllipseAnimation.Stop();
            }
            if (_EllipseAnimation != null)
            {
                _EllipseAnimation.Stop();
            }
        }

        public ImageSource IconSource
        {
            get { return (ImageSource)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconSourceProperty = DependencyProperty.Register("IconSource", typeof(ImageSource), typeof(UCLoadingAnimation), new PropertyMetadata(null));

        public bool ShowAnimation
        {
            get { return (bool)GetValue(ShowAnimationProperty); }
            set { SetValue(ShowAnimationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowAnimationProperty = DependencyProperty.Register("ShowAnimation", typeof(bool), typeof(UCLoadingAnimation), new PropertyMetadata(false, (s, e) => 
        {
            if (s != null)
            {
                UCLoadingAnimation panel = (UCLoadingAnimation)s;
                bool state = (bool)e.NewValue;
                if (state)
                {
                    panel.StartAnimation();
                }
                else
                {
                    panel.StopAnimation();
                }
            }
        }));
    }
}

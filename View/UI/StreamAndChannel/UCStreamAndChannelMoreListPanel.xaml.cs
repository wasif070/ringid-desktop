﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Utility;
using View.Utility.Channel;
using View.Utility.Stream;
using View.Utility.StreamAndChannel;
using View.ViewModel;

namespace View.UI.StreamAndChannel
{
    /// <summary>
    /// Interaction logic for UCStreamAndChannelMoreListPanel.xaml
    /// </summary>
    public partial class UCStreamAndChannelMoreListPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamAndChannelMoreListPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private ParamModel _ParamInfoModel;
        private ICommand _BackCommand;

        private DispatcherTimer _WindowResizeTimer = null;
        private DispatcherTimer _ScrollChangeTimer = null;

        private bool _IS_AT_TOP = true;
        private bool _IS_AT_BOTTOM = false;

        #region Constructor

        static UCStreamAndChannelMoreListPanel()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamAndChannelMoreListPanel(ParamModel paramModel)
        {
            InitializeComponent();
            this.ParamInfoModel = paramModel;
            this.DataContext = null;
        }

        #endregion Constructor

        #region Event Handler

        private static void OnLoadingStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d != null && e.NewValue != null)
                {
                    UCStreamAndChannelMoreListPanel panel = ((UCStreamAndChannelMoreListPanel)d);
                    int loadingStatus = (int)e.NewValue;

                    switch (loadingStatus)
                    {
                        case StatusConstants.NO_DATA:
                            log.Debug("*********************    NO_DATA                 *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.NO_DATA_AND_LOADING:
                            log.Debug("*********************    NO_DATA_AND_LOADING     *********************");
                            panel.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            break;
                        case StatusConstants.HAS_DATA:
                            log.Debug("*********************    HAS_DATA                *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.HAS_DATA_AND_LOADING:
                            log.Debug("*********************    HAS_DATA_AND_LOADING    *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLoadingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int moreCount = this.itcMoreList.Items.Count;
            int topLimit = moreCount > 5 ? 300 : 150;
            int bottomLimit = moreCount > 5 ? 400 : 100;

            if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
            {
                if (IsDataLoading() == false && e.ExtentHeight > e.ViewportHeight)
                {
                    if (e.VerticalChange < 0 && e.VerticalOffset <= topLimit && this._IS_AT_TOP == false)
                    {
                        //Debug.WriteLine("IS_AT_TOP");
                        this.DataLoadByScroll(true, 6);
                    }
                    else if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_AT_BOTTOM == false)
                    {
                        //Debug.WriteLine("IS_AT_BOTTOM");
                        this.DataLoadByScroll(false, 6);
                    }
                }
                this.ChangeScrollOpenStatusOnScroll();
            }

            if (e.ExtentHeight > e.ViewportHeight)
            {
                this._IS_AT_TOP = e.VerticalOffset <= topLimit;
                this._IS_AT_BOTTOM = (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit);
            }
            else
            {
                this._IS_AT_TOP = true;
                this._IS_AT_BOTTOM = false;
            }
        }

        private void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.ChangeScrollOpenStatusOnResize();
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            this.DataLoadByScroll(true, 10);

            this.DataContext = this;
            this.itcMoreList.SizeChanged += ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged += ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged += ScrlViewer_SizeChanged;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                MultiBinding loadingStatusBinding = new MultiBinding { Converter = new LoadingStatusConverter() };

                switch (this.ParamInfoModel.ActionType)
                {
                    case AppConstants.TYPE_GET_FEATURED_LIVE_STREAMS:
                        this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamFeatureList"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamFeatureList.Count"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsFeaturedLoading"), Source = RingIDSettings });
                        break;
                    case AppConstants.TYPE_GET_FEATURED_CHANNEL_LIST:
                        this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelFeatureList"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.ChannelFeatureList.Count"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsFeaturedLoading"), Source = RingIDSettings });
                        break;
                    case AppConstants.TYPE_GET_RECENT_STREAMS:
                        this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList.Count"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsRecentLoading"), Source = RingIDSettings });
                        break;
                    case AppConstants.TYPE_GET_MOST_VIEWED_CHANNEL_LIST:
                        this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelMostViewList"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.ChannelMostViewList.Count"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsMostViewLoading"), Source = RingIDSettings });
                        break;
                    case AppConstants.TYPE_GET_NEAREST_STREAMS:
                        this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamNearByLiveList"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamNearByLiveList.Count"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsNearByLoading"), Source = RingIDSettings });
                        break;
                    case AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS:
                        if (this.ParamInfoModel.Param is CountryCodeModel)
                        {
                            this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamByCountryList"), Source = RingIDSettings });
                            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamByCountryList.Count"), Source = RingIDSettings });
                            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsSearchLoading"), Source = RingIDSettings });
                        }
                        else if (this.ParamInfoModel.Param is StreamCategoryModel)
                        {
                            this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamByCategoryList"), Source = RingIDSettings });
                            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamByCategoryList.Count"), Source = RingIDSettings });
                            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsSearchLoading"), Source = RingIDSettings });
                        }
                        break;
                    case AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST:
                        if (this.ParamInfoModel.Param is CountryCodeModel)
                        {
                            this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelByCountryList"), Source = RingIDSettings });
                            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.ChannelByCountryList.Count"), Source = RingIDSettings });
                            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsSearchLoading"), Source = RingIDSettings });
                        }
                        else if (this.ParamInfoModel.Param is ChannelCategoryModel)
                        {
                            this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelByCategoryList"), Source = RingIDSettings });
                            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.ChannelByCategoryList.Count"), Source = RingIDSettings });
                            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsSearchLoading"), Source = RingIDSettings });
                        }
                        break;
                    default:
                        this.itcMoreList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("MainViewModel.StreamAndChannelFeatureList"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("MainViewModel.StreamAndChannelFeatureList.Count"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsFeaturedLoading"), Source = RingIDSettings });
                        loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsFeaturedLoading"), Source = RingIDSettings });
                        break;
                }

                this.SetBinding(UCStreamAndChannelMoreListPanel.LoadingStatusProperty, loadingStatusBinding);
            });
        }

        private void ChangeScrollOpenStatusOnResize()
        {
            try
            {
                if (this._WindowResizeTimer == null)
                {
                    this._WindowResizeTimer = new DispatcherTimer();
                    this._WindowResizeTimer.Interval = TimeSpan.FromMilliseconds(350);
                    this._WindowResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._WindowResizeTimer.Stop();
                    };
                }

                if (this._ScrollChangeTimer != null && this._ScrollChangeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Stop();
                }
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnScroll()
        {
            try
            {
                if (this._ScrollChangeTimer == null)
                {
                    this._ScrollChangeTimer = new DispatcherTimer();
                    this._ScrollChangeTimer.Interval = TimeSpan.FromMilliseconds(30);
                    this._ScrollChangeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._ScrollChangeTimer.Stop();
                    };
                }
                this._ScrollChangeTimer.Stop();
                if (this._WindowResizeTimer == null || !this._WindowResizeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnScroll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatus()
        {
            try
            {
                int paddingTop = 51;
                switch (this.ParamInfoModel.ActionType)
                {
                    case AppConstants.TYPE_GET_FEATURED_LIVE_STREAMS:
                    case AppConstants.TYPE_GET_RECENT_STREAMS:
                    case AppConstants.TYPE_GET_NEAREST_STREAMS:
                    case AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS:
                        StreamHelpers.ChangeStreamViewPortOpenedProperty(this.ScrlViewer, this.itcMoreList, paddingTop);
                        break;
                    case AppConstants.TYPE_GET_FEATURED_CHANNEL_LIST:
                    case AppConstants.TYPE_GET_MOST_VIEWED_CHANNEL_LIST:
                    case AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST:
                        ChannelHelpers.ChangeChannelViewPortOpenedProperty(this.ScrlViewer, this.itcMoreList, paddingTop);
                        break;
                    default:
                        HelperMethods.ChangeStreamAndChannelViewPortOpenedProperty(this.ScrlViewer, this.itcMoreList, paddingTop);
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DataLoadByScroll(bool isTopLoad, int limit)
        {
            switch (this.ParamInfoModel.ActionType)
            {
                case AppConstants.TYPE_GET_FEATURED_LIVE_STREAMS:
                    {
                        StreamHelpers.LoadFeaturedStreamList(isTopLoad ? StreamConstants.STREAM_TOP_SCROLL : StreamConstants.STREAM_TOP_SCROLL, limit);
                    }
                    break;
                case AppConstants.TYPE_GET_FEATURED_CHANNEL_LIST:
                    {
                        ChannelHelpers.LoadFeaturedChannelList(isTopLoad, limit);
                    }
                    break;
                case AppConstants.TYPE_GET_RECENT_STREAMS:
                    {
                        StreamHelpers.LoadRecentStreamList(isTopLoad ? StreamConstants.STREAM_TOP_SCROLL : StreamConstants.STREAM_TOP_SCROLL, limit);
                    }
                    break;
                case AppConstants.TYPE_GET_MOST_VIEWED_CHANNEL_LIST:
                    {
                        ChannelHelpers.LoadMostViewChannelList(isTopLoad, limit);
                    }
                    break;
                case AppConstants.TYPE_GET_NEAREST_STREAMS:
                    {
                        StreamHelpers.LoadNearByStreamList(isTopLoad ? StreamConstants.STREAM_TOP_SCROLL : StreamConstants.STREAM_TOP_SCROLL, limit);
                    }
                    break;
                case AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS:
                    {
                        if (this.ParamInfoModel.Param is CountryCodeModel)
                        {
                            StreamHelpers.LoadSearchStreamList(isTopLoad ? StreamConstants.STREAM_TOP_SCROLL : StreamConstants.STREAM_TOP_SCROLL, null, ((CountryCodeModel)this.ParamInfoModel.Param).CountryName, null, limit);
                        }
                        else if (this.ParamInfoModel.Param is StreamCategoryModel)
                        {
                            StreamHelpers.LoadSearchStreamList(isTopLoad ? StreamConstants.STREAM_TOP_SCROLL : StreamConstants.STREAM_TOP_SCROLL, null, null, (StreamCategoryModel)this.ParamInfoModel.Param, limit);
                        }
                    }
                    break;
                case AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST:
                    {
                        if (this.ParamInfoModel.Param is CountryCodeModel)
                        {
                            ChannelHelpers.LoadSearchChannelList(isTopLoad, null, ((CountryCodeModel)this.ParamInfoModel.Param).CountryName, 0, limit);
                        }
                        else if (this.ParamInfoModel.Param is ChannelCategoryModel)
                        {
                            ChannelHelpers.LoadSearchChannelList(isTopLoad, null, null, ((ChannelCategoryModel)this.ParamInfoModel.Param).CategoryID, limit);
                        }
                    }
                    break;
                default:
                    {
                        StreamHelpers.LoadFeaturedStreamList(isTopLoad ? StreamConstants.STREAM_TOP_SCROLL : StreamConstants.STREAM_TOP_SCROLL, limit);
                        ChannelHelpers.LoadFeaturedChannelList(isTopLoad, limit);
                    }
                    break;
            }
        }

        private bool IsDataLoading()
        {
            switch (this.ParamInfoModel.ActionType)
            {
                case AppConstants.TYPE_GET_FEATURED_LIVE_STREAMS:
                    return StreamViewModel.Instance.LoadStatusModel.IsFeaturedLoading;
                case AppConstants.TYPE_GET_FEATURED_CHANNEL_LIST:
                    return ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading;
                case AppConstants.TYPE_GET_RECENT_STREAMS:
                    return StreamViewModel.Instance.LoadStatusModel.IsRecentLoading;
                case AppConstants.TYPE_GET_MOST_VIEWED_CHANNEL_LIST:
                    return ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading;
                case AppConstants.TYPE_GET_NEAREST_STREAMS:
                    return StreamViewModel.Instance.LoadStatusModel.IsNearByLoading;
                case AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS:
                    return StreamViewModel.Instance.LoadStatusModel.IsSearchLoading;
                case AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST:
                    return ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading;
                default:
                    return StreamViewModel.Instance.LoadStatusModel.IsFeaturedLoading && ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading;
            }
        }

        private void OnBackClick(object param)
        {
            try
            {
                StreamAndChannelSwitcher.Switch(ParamInfoModel.PrevViewType, ParamInfoModel.PrevParam);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnBackClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            int actionType = this.ParamInfoModel.ActionType;
            object param = this.ParamInfoModel.Param;

            if (this._WindowResizeTimer != null)
            {
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer = null;
            }
            if (this._ScrollChangeTimer != null)
            {
                this._ScrollChangeTimer.Stop();
                this._ScrollChangeTimer = null;
            }

            this.itcMoreList.SizeChanged -= ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged -= ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged -= ScrlViewer_SizeChanged;
            this.ClearValue(UCStreamAndChannelMoreListPanel.LoadingStatusProperty);

            Task.Factory.StartNew(() =>
            {
                switch (actionType)
                {
                    case AppConstants.TYPE_GET_FEATURED_LIVE_STREAMS:
                        StreamViewModel.Instance.StreamFeatureList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        break;
                    case AppConstants.TYPE_GET_FEATURED_CHANNEL_LIST:
                        ChannelViewModel.Instance.ChannelFeatureList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        break;
                    case AppConstants.TYPE_GET_RECENT_STREAMS:
                        StreamViewModel.Instance.StreamRecentLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        break;
                    case AppConstants.TYPE_GET_MOST_VIEWED_CHANNEL_LIST:
                        ChannelViewModel.Instance.ChannelMostViewList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        break;
                    case AppConstants.TYPE_GET_NEAREST_STREAMS:
                        StreamViewModel.Instance.StreamNearByLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        break;
                    case AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS:
                        if (param is CountryCodeModel)
                        {
                            StreamViewModel.Instance.StreamByCountryList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        }
                        else if (param is StreamCategoryModel)
                        {
                            StreamViewModel.Instance.StreamByCategoryList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        }
                        break;
                    case AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST:
                        if (param is CountryCodeModel)
                        {
                            ChannelViewModel.Instance.ChannelByCountryList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        }
                        else if (param is StreamCategoryModel)
                        {
                            ChannelViewModel.Instance.ChannelByCategoryList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        }
                        break;
                    default:
                        RingIDViewModel.Instance.StreamAndChannelFeatureList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                        break;
                }
            });

            this.itcMoreList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcMoreList.ItemsSource = null;

            switch (this.ParamInfoModel.ActionType)
            {
                case AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS:
                    if (this.ParamInfoModel.Param is CountryCodeModel)
                    {
                        StreamViewModel.Instance.StreamByCountryList.Clear();
                    }
                    else if (this.ParamInfoModel.Param is StreamCategoryModel)
                    {
                        StreamViewModel.Instance.StreamByCategoryList.Clear();
                    }
                    break;
                case AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST:
                    if (this.ParamInfoModel.Param is CountryCodeModel)
                    {
                        ChannelViewModel.Instance.ChannelByCountryList.Clear();
                    }
                    else if (this.ParamInfoModel.Param is ChannelCategoryModel)
                    {
                        ChannelViewModel.Instance.ChannelByCategoryList.Clear();
                    }
                    break;
            }

            this.DataContext = null;
            this.ParamInfoModel = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty LoadingStatusProperty = DependencyProperty.Register("LoadingStatus", typeof(int), typeof(UCStreamAndChannelMoreListPanel), new PropertyMetadata(StatusConstants.NO_DATA, OnLoadingStatusChanged));

        public int LoadingStatus
        {
            get { return (int)GetValue(LoadingStatusProperty); }
            set
            {
                SetValue(LoadingStatusProperty, value);
            }
        }

        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand((param) => OnBackClick(param));
                }
                return _BackCommand;
            }
        }

        public ParamModel ParamInfoModel
        {
            get { return _ParamInfoModel; }
            set
            {
                if (value == _ParamInfoModel) { return; }
                _ParamInfoModel = value;
                this.OnPropertyChanged("ParamInfoModel");
            }
        }

        #endregion Property
    }

}

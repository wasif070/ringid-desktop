﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Utility;
using View.Utility.Channel;
using View.Utility.Stream;
using View.Utility.StreamAndChannel;
using View.ViewModel;

namespace View.UI.StreamAndChannel
{
    /// <summary>
    /// Interaction logic for UCStreamAndChannelHomePanel.xaml
    /// </summary>
    public partial class UCStreamAndChannelHomePanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamAndChannelHomePanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private string _SearchText = String.Empty;
        private bool _IsArrowEnabled = true;
        private ICommand _ShowMoreCommand;
        private ICommand _ArrowClickCommand;
        private Storyboard _SliderStoryboard = null;
        private DispatcherTimer _SliderResizeTimer = null;
        private DispatcherTimer _SliderAnimationTimer = null;
        private DispatcherTimer _FilterTimer = null;

        #region Constructor

        static UCStreamAndChannelHomePanel()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamAndChannelHomePanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.srvFeatureList.PreviewKeyDown += SliderControl_PreviewKeyDown;
            this.srvStreamList.PreviewKeyDown += SliderControl_PreviewKeyDown;
            this.srvChannelList.PreviewKeyDown += SliderControl_PreviewKeyDown;
            this.itcFeatureList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("MainViewModel.StreamAndChannelFeatureList"), Source = RingIDSettings });
            this.itcStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList"), Source = RingIDSettings });
            this.itcChannelList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelMostViewList"), Source = RingIDSettings });
        }

        #endregion Constructor

        #region Event Handler

        private static void OnLoadingStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d != null && e.NewValue != null)
                {
                    UCStreamAndChannelHomePanel panel = ((UCStreamAndChannelHomePanel)d);
                    int loadingStatus = (int)e.NewValue;

                    switch (loadingStatus)
                    {
                        case StatusConstants.NO_DATA:
                            log.Debug("*********************    NO_DATA                 *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.NO_DATA_AND_LOADING:
                            log.Debug("*********************    NO_DATA_AND_LOADING     *********************");
                            panel.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            break;
                        case StatusConstants.HAS_DATA:
                            log.Debug("*********************    HAS_DATA                *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.HAS_DATA_AND_LOADING:
                            log.Debug("*********************    HAS_DATA_AND_LOADING    *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLoadingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

        }

        public void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        public void SearchBox_TextChanged(object sender, TextChangedEventArgs args)
        {
            this.OnFilterData();
        }

        public void Category_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count > 0)
            {
                this.OnFilterData(e.AddedItems[0] is StreamCategoryModel ? SettingsConstants.TYPE_STREAM : SettingsConstants.TYPE_CHANNEL);
            }
        }

        public void Reload_Click(object sender, RoutedEventArgs e)
        {
            this.OnFilterData();
        }

        private void SliderControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home || e.Key == Key.End || e.Key == Key.Left || e.Key == Key.Right)
            {
                e.Handled = true;
            }
        }

        private void SliderStoryboard_Completed(object sender, EventArgs e)
        {
            if (this._SliderStoryboard != null)
            {
                this._SliderStoryboard.Completed -= SliderStoryboard_Completed;
                this._SliderStoryboard = null;
            }
            this.ChangeSliderOpenStatusOnAnimation();
        }

        private void SliderControl_SizeChanged(object sender, SizeChangedEventArgs args)
        {
            try
            {
                if (sender.Equals(this.itcFeatureList) && Math.Abs(this.itcFeatureList.Margin.Left) >= this.itcFeatureList.ActualWidth)
                {
                    this.OnSliderAnimation(this.itcFeatureList, 0, 0.0D);
                    return;
                }
                else if (sender.Equals(this.itcStreamList) && Math.Abs(this.itcStreamList.Margin.Left) >= this.itcStreamList.ActualWidth)
                {
                    this.OnSliderAnimation(this.itcStreamList, 0, 0.0D);
                    return;
                }
                else if (sender.Equals(this.itcChannelList) && Math.Abs(this.itcChannelList.Margin.Left) >= this.itcChannelList.ActualWidth)
                {
                    this.OnSliderAnimation(this.itcChannelList, 0, 0.0D);
                    return;
                }
                this.ChangeSliderOpenStatusOnResize();
            }
            catch (Exception ex)
            {
                log.Error("Error: SliderControl_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_TOP_SCROLL);
            ChannelHelpers.LoadFeaturedChannelList(true);
            StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);
            ChannelHelpers.LoadMostViewChannelList(true);

            this.itcFeatureList.SizeChanged += SliderControl_SizeChanged;
            this.itcStreamList.SizeChanged += SliderControl_SizeChanged;
            this.itcChannelList.SizeChanged += SliderControl_SizeChanged;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.ChangeSliderOpenStatus();
            }, DispatcherPriority.ApplicationIdle);

            MultiBinding loadingStatusBinding = new MultiBinding { Converter = new LoadingStatusConverter() };
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("MainViewModel.StreamAndChannelFeatureList.Count"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList.Count"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.ChannelMostViewList.Count"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsFeaturedLoading"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsFeaturedLoading"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsRecentLoading"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsMostViewLoading"), Source = RingIDSettings });
            this.SetBinding(UCStreamAndChannelHomePanel.LoadingStatusProperty, loadingStatusBinding);
        }

        public void ReleaseViewer()
        {
            if (this._FilterTimer != null) this._FilterTimer.Stop();
            if (this._SliderResizeTimer != null) this._SliderResizeTimer.Stop();
            if (this._SliderAnimationTimer != null) this._SliderAnimationTimer.Stop();

            this.itcFeatureList.SizeChanged -= SliderControl_SizeChanged;
            this.itcStreamList.SizeChanged -= SliderControl_SizeChanged;
            this.itcChannelList.SizeChanged -= SliderControl_SizeChanged;
            this.ClearValue(UCStreamAndChannelHomePanel.LoadingStatusProperty);

            Task.Factory.StartNew(() =>
            {
                RingIDViewModel.Instance.StreamAndChannelFeatureList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                StreamViewModel.Instance.StreamRecentLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                ChannelViewModel.Instance.ChannelMostViewList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
            });

            this.IsArrowEnabled = true;
        }

        private void ResetDataListByFilter(int type = SettingsConstants.TYPE_STREAM_AND_CHANNEL)
        {
            if (type == SettingsConstants.TYPE_STREAM_AND_CHANNEL)
            {
                this.itcFeatureList.ClearValue(ItemsControl.ItemsSourceProperty);
                this.itcStreamList.ClearValue(ItemsControl.ItemsSourceProperty);
                this.itcChannelList.ClearValue(ItemsControl.ItemsSourceProperty);
                HelperMethods.RemoveFromFeaturedStreamAndChannelList();
                StreamViewModel.Instance.StreamFeatureList.Clear();
                ChannelViewModel.Instance.ChannelFeatureList.Clear();
                StreamViewModel.Instance.StreamRecentLiveList.Clear();
                ChannelViewModel.Instance.ChannelMostViewList.Clear();
                OnSliderAnimation(itcFeatureList, 0, 0);
                OnSliderAnimation(itcStreamList, 0, 0);
                OnSliderAnimation(itcChannelList, 0, 0);
                this.itcFeatureList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("MainViewModel.StreamAndChannelFeatureList"), Source = RingIDSettings });
                this.itcStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList"), Source = RingIDSettings });
                this.itcChannelList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelMostViewList"), Source = RingIDSettings });
            }
            else if (type == SettingsConstants.TYPE_STREAM)
            {
                this.itcStreamList.ClearValue(ItemsControl.ItemsSourceProperty);
                HelperMethods.RemoveFromFeaturedStreamAndChannelList(type);
                StreamViewModel.Instance.StreamFeatureList.Clear();
                StreamViewModel.Instance.StreamRecentLiveList.Clear();
                OnSliderAnimation(itcFeatureList, 0, 0);
                OnSliderAnimation(itcStreamList, 0, 0);
                this.itcStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList"), Source = RingIDSettings });
            }
            else if (type == SettingsConstants.TYPE_CHANNEL)
            {
                this.itcChannelList.ClearValue(ItemsControl.ItemsSourceProperty);
                HelperMethods.RemoveFromFeaturedStreamAndChannelList(type);
                ChannelViewModel.Instance.ChannelFeatureList.Clear();
                ChannelViewModel.Instance.ChannelMostViewList.Clear();
                OnSliderAnimation(itcFeatureList, 0, 0);
                OnSliderAnimation(itcChannelList, 0, 0);
                this.itcChannelList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelMostViewList"), Source = RingIDSettings });
            }
        }

        private void OnSliderArrowClick(object param)
        {
            try
            {
                int type = Int32.Parse(param.ToString());
                bool isNext = type % 2 == 0;

                switch (type)
                {
                    case 1:
                    case 2:
                        {
                            double targetValue = 0.0D;
                            if (isNext)
                            {
                                double remaining = this.itcFeatureList.ActualWidth - Math.Abs(this.itcFeatureList.Margin.Left) + this.srvFeatureList.ActualWidth;
                                if (remaining < this.srvFeatureList.ActualWidth)
                                {
                                    targetValue = this.itcFeatureList.Margin.Left - remaining;
                                    StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                    ChannelHelpers.LoadFeaturedChannelList();
                                }
                                else
                                {
                                    targetValue = this.itcFeatureList.Margin.Left - this.srvFeatureList.ActualWidth;
                                    if (Math.Abs(targetValue) + this.srvFeatureList.ActualWidth >= this.itcFeatureList.ActualWidth)
                                    {
                                        StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                        ChannelHelpers.LoadFeaturedChannelList();
                                    }
                                }
                            }
                            else
                            {
                                if (Math.Abs(this.itcFeatureList.Margin.Left) <= this.srvFeatureList.ActualWidth)
                                {
                                    targetValue = 0;
                                    StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_TOP_SCROLL);
                                    ChannelHelpers.LoadFeaturedChannelList(true);
                                }
                                else
                                {
                                    targetValue = this.itcFeatureList.Margin.Left + this.srvFeatureList.ActualWidth;
                                }
                            }
                            OnSliderAnimation(this.itcFeatureList, targetValue, 0.35D);
                        }
                        break;
                    case 3:
                    case 4:
                        {
                            double targetValue = 0.0D;
                            if (isNext)
                            {
                                double remaining = this.itcStreamList.ActualWidth - Math.Abs(this.itcStreamList.Margin.Left) + this.srvStreamList.ActualWidth;
                                if (remaining < this.srvStreamList.ActualWidth)
                                {
                                    targetValue = this.itcStreamList.Margin.Left - remaining;
                                    StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                }
                                else
                                {
                                    targetValue = this.itcStreamList.Margin.Left - this.srvStreamList.ActualWidth;
                                    if (Math.Abs(targetValue) + this.srvStreamList.ActualWidth >= this.itcStreamList.ActualWidth)
                                    {
                                        StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                    }
                                }
                            }
                            else
                            {
                                if (Math.Abs(this.itcStreamList.Margin.Left) <= this.srvStreamList.ActualWidth)
                                {
                                    targetValue = 0;
                                    StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);
                                }
                                else
                                {
                                    targetValue = this.itcStreamList.Margin.Left + this.srvStreamList.ActualWidth;
                                }
                            }
                            OnSliderAnimation(this.itcStreamList, targetValue, 0.35D);
                        }
                        break;
                    case 5:
                    case 6:
                        {
                            double targetValue = 0.0D;
                            if (isNext)
                            {
                                double remaining = this.itcChannelList.ActualWidth - Math.Abs(this.itcChannelList.Margin.Left) + this.srvChannelList.ActualWidth;
                                if (remaining < this.srvChannelList.ActualWidth)
                                {
                                    targetValue = this.itcChannelList.Margin.Left - remaining;
                                    ChannelHelpers.LoadMostViewChannelList();
                                }
                                else
                                {
                                    targetValue = this.itcChannelList.Margin.Left - this.srvChannelList.ActualWidth;
                                    if (Math.Abs(targetValue) + this.srvChannelList.ActualWidth >= this.itcChannelList.ActualWidth)
                                    {
                                        ChannelHelpers.LoadMostViewChannelList();
                                    }
                                }
                            }
                            else
                            {
                                if (Math.Abs(this.itcChannelList.Margin.Left) <= this.srvChannelList.ActualWidth)
                                {
                                    targetValue = 0;
                                    ChannelHelpers.LoadMostViewChannelList(true);
                                }
                                else
                                {
                                    targetValue = this.itcChannelList.Margin.Left + this.srvChannelList.ActualWidth;
                                }
                            }
                            OnSliderAnimation(this.itcChannelList, targetValue, 0.35D);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSliderArrowClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnSliderAnimation(FrameworkElement targetElement, double targetValue, double delay)
        {
            try
            {
                this.IsArrowEnabled = false;
                double currentValue = targetElement.Margin.Left;
                ThicknessAnimation marginAnimation = new ThicknessAnimation();
                marginAnimation.From = new Thickness(currentValue, 0, 0, 0);
                marginAnimation.To = new Thickness(targetValue, 0, 0, 0);
                marginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                this._SliderStoryboard = new Storyboard();
                this._SliderStoryboard.Children.Add(marginAnimation);
                Storyboard.SetTarget(marginAnimation, targetElement);
                Storyboard.SetTargetProperty(marginAnimation, new PropertyPath(ItemsControl.MarginProperty));
                this._SliderStoryboard.Completed += this.SliderStoryboard_Completed;
                this._SliderStoryboard.Begin(targetElement);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSliderAnimation() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeSliderOpenStatusOnResize()
        {
            try
            {
                if (this._SliderResizeTimer == null)
                {
                    this._SliderResizeTimer = new DispatcherTimer();
                    this._SliderResizeTimer.Interval = TimeSpan.FromMilliseconds(400);
                    this._SliderResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeSliderOpenStatus();
                        this._SliderResizeTimer.Stop();
                    };
                }
                this._SliderResizeTimer.Stop();
                if (this._SliderAnimationTimer == null || this._SliderAnimationTimer.IsEnabled == false)
                {
                    this._SliderResizeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeSliderOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeSliderOpenStatusOnAnimation()
        {
            try
            {
                if (this._SliderAnimationTimer == null)
                {
                    this._SliderAnimationTimer = new DispatcherTimer();
                    this._SliderAnimationTimer.Interval = TimeSpan.FromMilliseconds(100);
                    this._SliderAnimationTimer.Tick += (o, e) =>
                    {
                        this.ChangeSliderOpenStatus();
                        this._SliderAnimationTimer.Stop();
                        this.IsArrowEnabled = true;
                    };
                }
                if (this._SliderResizeTimer != null && this._SliderResizeTimer.IsEnabled)
                {
                    this._SliderResizeTimer.Stop();
                }
                this._SliderAnimationTimer.Stop();
                this._SliderAnimationTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeSliderOpenStatusOnAnimation() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeSliderOpenStatus()
        {
            try
            {
                int featureIndex = (int)(Math.Abs(itcFeatureList.Margin.Left) / 155);
                if (featureIndex < this.itcFeatureList.Items.Count)
                {
                    int firstIndex = featureIndex - 8 < 0 ? 0 : featureIndex - 8;
                    int read = featureIndex > 8 ? 0 : 8 - featureIndex;
                    for (int idx = firstIndex; idx < this.itcFeatureList.Items.Count && read < 20; idx++, read++)
                    {
                        if (idx >= (featureIndex - 4) && idx < (featureIndex + 4 + 4))
                        {
                            ((StreamAndChannelModel)this.itcFeatureList.Items[idx]).IsViewOpened = true;
                        }
                        else
                        {
                            ((StreamAndChannelModel)this.itcFeatureList.Items[idx]).IsViewOpened = false;
                        }
                    }
                }

                int streamIndex = (int)(Math.Abs(itcStreamList.Margin.Left) / 206);
                if (streamIndex < this.itcStreamList.Items.Count)
                {
                    int firstIndex = streamIndex - 6 < 0 ? 0 : streamIndex - 6;
                    int read = streamIndex > 6 ? 0 : 6 - streamIndex;
                    for (int idx = firstIndex; idx < this.itcStreamList.Items.Count && read < 15; idx++, read++)
                    {
                        if (idx >= (streamIndex - 3) && idx < (streamIndex + 3 + 3))
                        {
                            ((StreamModel)this.itcStreamList.Items[idx]).IsViewOpened = true;
                        }
                        else
                        {
                            ((StreamModel)this.itcStreamList.Items[idx]).IsViewOpened = false;
                        }
                    }
                }

                int channelIndex = (int)(Math.Abs(itcChannelList.Margin.Left) / 206);
                if (channelIndex < this.itcChannelList.Items.Count)
                {
                    int firstIndex = channelIndex - 6 < 0 ? 0 : channelIndex - 6;
                    int read = channelIndex > 6 ? 0 : 6 - channelIndex;
                    for (int idx = firstIndex; idx < this.itcChannelList.Items.Count && read < 15; idx++, read++)
                    {
                        if (idx >= (channelIndex - 3) && idx < (channelIndex + 3 + 3))
                        {
                            ((ChannelModel)this.itcChannelList.Items[idx]).IsViewOpened = true;
                        }
                        else
                        {
                            ((ChannelModel)this.itcChannelList.Items[idx]).IsViewOpened = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeSliderOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnFilterData(int type = SettingsConstants.TYPE_STREAM_AND_CHANNEL)
        {
            try
            {
                this.ResetDataListByFilter(type);

                if (this._FilterTimer == null)
                {
                    this._FilterTimer = new DispatcherTimer();
                    this._FilterTimer.Interval = TimeSpan.FromMilliseconds(800);
                    this._FilterTimer.Tick += (o, e) =>
                    {
                        StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_TOP_SCROLL);
                        ChannelHelpers.LoadFeaturedChannelList(true);
                        StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);
                        ChannelHelpers.LoadMostViewChannelList(true);
                        this._FilterTimer.Stop();
                    };
                }

                this._FilterTimer.Stop();
                this._FilterTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnFilterData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnShowMoreClick(object param)
        {
            try
            {
                ParamModel paramModel = new ParamModel();
                paramModel.Title = "Featured Lives & Channels";
                paramModel.ActionType = param != null ? (int)param : 0;
                paramModel.Param = null;
                paramModel.PrevParam = null;
                paramModel.PrevViewType = StreamAndChannelConstants.TypeStreamAndChannelMainPanel;
                StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel, paramModel);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnShowMoreClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {

        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty LoadingStatusProperty = DependencyProperty.Register("LoadingStatus", typeof(int), typeof(UCStreamAndChannelHomePanel), new PropertyMetadata(StatusConstants.NO_DATA, OnLoadingStatusChanged));

        public int LoadingStatus
        {
            get { return (int)GetValue(LoadingStatusProperty); }
            set
            {
                SetValue(LoadingStatusProperty, value);
            }
        }

        public ICommand ShowMoreCommand
        {
            get
            {
                if (_ShowMoreCommand == null)
                {
                    _ShowMoreCommand = new RelayCommand((param) => OnShowMoreClick(param));
                }
                return _ShowMoreCommand;
            }
        }

        public ICommand ArrowClickCommand
        {
            get
            {
                if (_ArrowClickCommand == null)
                {
                    _ArrowClickCommand = new RelayCommand((param) => OnSliderArrowClick(param));
                }
                return _ArrowClickCommand;
            }
        }

        public bool IsArrowEnabled
        {
            get { return _IsArrowEnabled; }
            set
            {
                if (_IsArrowEnabled == value)
                    return;

                _IsArrowEnabled = value;
                this.OnPropertyChanged("IsArrowEnabled");
            }
        }

        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                if (_SearchText == value)
                    return;

                _SearchText = value;
                this.OnPropertyChanged("SearchText");
            }
        }

        #endregion Property

    }
}

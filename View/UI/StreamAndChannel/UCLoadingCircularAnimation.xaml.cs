﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace View.UI.StreamAndChannel
{
    /// <summary>
    /// Interaction logic for ProgressAnimation.xaml
    /// </summary>
    public partial class UCLoadingCircularAnimation : UserControl
    {
        Storyboard _LoadingAnimation;

        public UCLoadingCircularAnimation()
        {
            InitializeComponent();
            this.Visibility = System.Windows.Visibility.Collapsed;
            this._LoadingAnimation = (Storyboard)FindResource("LoadingAnimation");
        }
        
        public void StartAnimation()
        {
            this.Visibility = System.Windows.Visibility.Visible;
            if (_LoadingAnimation != null)
            {
                _LoadingAnimation.Begin();
            }
        }

        public void StopAnimation()
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
            if (_LoadingAnimation != null)
            {
                _LoadingAnimation.Stop();
            }
        }

        public bool ShowAnimation
        {
            get { return (bool)GetValue(ShowAnimationProperty); }
            set { SetValue(ShowAnimationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowAnimationProperty = DependencyProperty.Register("ShowAnimation", typeof(bool), typeof(UCLoadingCircularAnimation), new PropertyMetadata(false, (s, e) => 
        {
            if (s != null)
            {
                UCLoadingCircularAnimation panel = (UCLoadingCircularAnimation)s;
                bool state = (bool)e.NewValue;
                if (state)
                {
                    panel.StartAnimation();
                }
                else
                {
                    panel.StopAnimation();
                }
            }
        }));


    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;

namespace View.UI.StreamAndChannel
{
    /// <summary>
    /// Interaction logic for UCStreamAndChannelCountryListPanel.xaml
    /// </summary>
    public partial class UCStreamAndChannelCountryListPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamAndChannelCountryListPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        public delegate void CountrySelectHandler(CountryCodeModel countryModel, string searchParam);
        public event CountrySelectHandler OnCountrySelect;

        private ObservableCollection<CountryCodeModel> _CountryList = new ObservableCollection<CountryCodeModel>();
        private ICommand _CountrySelectCommand;
        private ICommand _CancelCommand;

        #region Constructor

        public UCStreamAndChannelCountryListPanel()
        {
            InitializeComponent();
            this.DataContext = null;
            CountryList.Add(new CountryCodeModel("Canada", "+1"));
            CountryList.Add(new CountryCodeModel("United States", "+1"));
            CountryList.Add(new CountryCodeModel("United Kingdom", "+44"));
            CountryList.Add(new CountryCodeModel("Bangladesh", "+880"));
            CountryList.Add(new CountryCodeModel("India", "+91"));
            CountryList.Add(new CountryCodeModel("Pakistan", "+92"));
            CountryList.Add(new CountryCodeModel("Australia", "+61"));
            CountryList.Add(new CountryCodeModel("Iran", "+98"));
            
            List<String> tempList = CountryList.Select(P => P.CountryCode).ToList();
            for (int idx = 2; idx < DefaultSettings.COUNTRY_MOBILE_CODE.Length/2; idx++)
            {
                string name = DefaultSettings.COUNTRY_MOBILE_CODE[idx, 0];
                string code = DefaultSettings.COUNTRY_MOBILE_CODE[idx, 1];
                if (!tempList.Contains(code))
                {
                    CountryList.Add(new CountryCodeModel(name, code));
                }
                else
                {
                    tempList.Remove(code);
                }
            }
        }

        #endregion Constructor

        #region Event Handler

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer(string searchParam)
        {
            this.TxtSearch.Text = searchParam;
            this.DataContext = this;
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.itcCountryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("CountryList") });
            }, DispatcherPriority.ApplicationIdle);
        }

        private void OnCountrySelectCommand(object param)
        {
            try
            {
                if (this.OnCountrySelect != null)
                {
                    this.OnCountrySelect((CountryCodeModel)param, TxtSearch.Text);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCountrySelectCommand() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnCancelCommand(object param)
        {
            try
            {
                if (this.OnCountrySelect != null)
                {
                    this.OnCountrySelect(null, null);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCancelCommand() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.itcCountryList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcCountryList.ItemsSource = null;
            this.CountryList.Clear();
            this.OnCountrySelect = null;
            this.DataContext = null;
            this.CountryList = null;
            this.TxtSearch.Text = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand CountrySelectCommand
        {
            get
            {
                if (_CountrySelectCommand == null)
                {
                    _CountrySelectCommand = new RelayCommand((param) => OnCountrySelectCommand(param));
                }
                return _CountrySelectCommand;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand((param) => OnCancelCommand(param));
                }
                return _CancelCommand;
            }
        }

        public ObservableCollection<CountryCodeModel> CountryList
        {
            get { return _CountryList; }
            set
            {
                _CountryList = value;
                OnPropertyChanged("CountryList");
            }
        }

        #endregion Property
    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.Channel;
using View.UI.Stream;
using View.Utility;
using View.Utility.Stream;
using View.Utility.StreamAndChannel;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.StreamAndChannel
{
    /// <summary>
    /// Interaction logic for UCStreamAndChannelWrapper.xaml
    /// </summary>
    public partial class UCStreamAndChannelWrapper : UserControl
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamAndChannelWrapper).Name);

        public UCStreamAndChannelMainPanel StreamAndChannelMainPanel = null;
        public UCStreamDiscoveryPanel StreamDiscoveryPanel = null;
        public UCChannelDiscoveryPanel ChannelDiscoveryPanel = null;
        public UCStreamFollowingPanel StreamFollowingPanel = null;
        public UCStreamAndChannelMoreListPanel StreamAndChannelMoreListPanel = null;
        public UCChannelDetailsPanel ChannelDetailsPanel = null;

        public static int ViewType = -1;

        #region Constructor

        public UCStreamAndChannelWrapper()
        {
            InitializeComponent();
            this.DataContext = null;
            this.Loaded += UCStreamAndChannelWrapper_Loaded;
            this.Unloaded += UCStreamAndChannelWrapper_Unloaded;
            StreamAndChannelSwitcher.pageSwitcher = this;
        }

        #endregion Constructor

        #region Event Handler

        private void UCStreamAndChannelWrapper_Unloaded(object sender, RoutedEventArgs e)
        {
            if (RingIDViewModel.Instance != null)
                RingIDViewModel.Instance.StreamAndChannelButtonSelection = false;
        }

        void UCStreamAndChannelWrapper_Loaded(object sender, RoutedEventArgs e)
        {
            if (RingIDViewModel.Instance != null)
                RingIDViewModel.Instance.StreamAndChannelButtonSelection = true;
        }

        #endregion Event Handler

        #region Utility Methods

        public void Navigate(int type, object state = null)
        {
            bool isChanged = ViewType != type;
            if (isChanged)
            {
                Destroy(ViewType);
            }
            ViewType = type;

            switch (ViewType)
            {
                case StreamAndChannelConstants.TypeStreamAndChannelMainPanel:
                    LoadStreamAndChannelMainPanel(state, isChanged);
                    break;
                case StreamAndChannelConstants.TypeStreamDiscoveryPanel:
                    LoadStreamDiscoveryPanel(state, isChanged);
                    break;
                case StreamAndChannelConstants.TypeChannelDiscoveryPanel:
                    LoadChannelDiscoveryPanel(state, isChanged);
                    break;
                case StreamAndChannelConstants.TypeStreamFollowingPanel:
                    LoadStreamFollowingPanel(state, isChanged);
                    break;
                case StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel:
                    LoadStreamAndChannelMoreListPanel(state, isChanged);
                    break;
                case StreamAndChannelConstants.TypeChannelDetailsPanel:
                    LoadChannelDetailsPanel(state, isChanged);
                    break;
            }
        }

        private void Destroy(int type)
        {
            switch (type)
            {
                case StreamAndChannelConstants.TypeStreamAndChannelMainPanel:
                    if (StreamAndChannelMainPanel != null)
                    {
                        StreamAndChannelMainPanel.ReleaseViewer();
                    }
                    break;
                case StreamAndChannelConstants.TypeStreamDiscoveryPanel:
                    if (StreamDiscoveryPanel != null)
                    {
                        StreamDiscoveryPanel.Dispose();
                        StreamDiscoveryPanel = null;
                    }
                    break;
                case StreamAndChannelConstants.TypeChannelDiscoveryPanel:
                    if (ChannelDiscoveryPanel != null)
                    {
                        ChannelDiscoveryPanel.Dispose();
                        ChannelDiscoveryPanel = null;
                    }
                    break;
                case StreamAndChannelConstants.TypeStreamFollowingPanel:
                    if (StreamFollowingPanel != null)
                    {
                        StreamFollowingPanel.Dispose();
                        StreamFollowingPanel = null;
                    }
                    break;
                case StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel:
                    if (StreamAndChannelMoreListPanel != null)
                    {
                        StreamAndChannelMoreListPanel.Dispose();
                        StreamAndChannelMoreListPanel = null;
                    }
                    break;
                case StreamAndChannelConstants.TypeChannelDetailsPanel:
                    if (ChannelDetailsPanel != null)
                    {
                        ChannelDetailsPanel.Dispose();
                        ChannelDetailsPanel = null;
                    }
                    break;
            }
        }

        private void LoadStreamAndChannelMainPanel(object state, bool isChanged)
        {
            if (StreamAndChannelMainPanel == null)
            {
                StreamAndChannelMainPanel = new UCStreamAndChannelMainPanel();
            }
            if (isChanged)
            {
                StreamAndChannelMainPanel.InilializeViewer();
                bdrParent.Child = StreamAndChannelMainPanel;
            }
        }

        private void LoadStreamDiscoveryPanel(object state, bool isChanged)
        {
            if (StreamDiscoveryPanel == null)
            {
                StreamDiscoveryPanel = new UCStreamDiscoveryPanel();
                StreamDiscoveryPanel.InilializeViewer(state);
            }
            if (isChanged)
            {
                bdrParent.Child = StreamDiscoveryPanel;
            }
        }

        private void LoadChannelDiscoveryPanel(object state, bool isChanged)
        {
            if (ChannelDiscoveryPanel == null)
            {
                ChannelDiscoveryPanel = new UCChannelDiscoveryPanel();
                ChannelDiscoveryPanel.InilializeViewer(state);
            }
            if (isChanged)
            {
                bdrParent.Child = ChannelDiscoveryPanel;
            }
        }

        private void LoadStreamFollowingPanel(object state, bool isChanged)
        {
            if (StreamFollowingPanel == null)
            {
                StreamFollowingPanel = new UCStreamFollowingPanel();
                StreamFollowingPanel.InilializeViewer();
            }
            if (isChanged)
            {
                bdrParent.Child = StreamFollowingPanel;
            }
        }

        private void LoadStreamAndChannelMoreListPanel(object state, bool isChanged)
        {
            if (StreamAndChannelMoreListPanel == null)
            {
                StreamAndChannelMoreListPanel = new UCStreamAndChannelMoreListPanel((ParamModel)state);
                StreamAndChannelMoreListPanel.InilializeViewer();
            }
            if (isChanged)
            {
                bdrParent.Child = StreamAndChannelMoreListPanel;
            }
        }

        private void LoadChannelDetailsPanel(object state, bool isChanged)
        {
            if (ChannelDetailsPanel == null)
            {
                ChannelDetailsPanel = new UCChannelDetailsPanel(state);
                ChannelDetailsPanel.InilializeViewer();
            }
            if (isChanged)
            {
                bdrParent.Child = ChannelDetailsPanel;
            }
        }

        #endregion Utility Methods

    }
}

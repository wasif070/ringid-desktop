﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;
using View.Utility;
using View.Utility.Channel;
using View.Utility.Stream;

namespace View.UI.StreamAndChannel
{
    /// <summary>
    /// Interaction logic for UCSingleStreamAndChannelPanel.xaml
    /// </summary>
    public partial class UCSingleStreamAndChannelPanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleStreamAndChannelPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private bool disposed = false;

        public UCSingleStreamAndChannelPanel()
        {
            InitializeComponent();
        }

        ~UCSingleStreamAndChannelPanel()
        {
           Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsThumbViewProperty = DependencyProperty.Register("IsThumbView", typeof(bool), typeof(UCSingleStreamAndChannelPanel), new PropertyMetadata(false));

        public bool IsThumbView
        {
            get { return (bool)GetValue(IsThumbViewProperty); }
            set
            {
                SetValue(IsThumbViewProperty, value);
            }
        }

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleStreamAndChannelPanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleStreamAndChannelPanel panel = ((UCSingleStreamAndChannelPanel)o);
                object data = panel.DataContext;

                if (e.NewValue != null && (bool)e.NewValue)
                {
                    if (data is StreamAndChannelModel)
                    {
                        panel.BindPreview((StreamAndChannelModel)data);
                    }
                    else if (data is StreamModel)
                    {
                        panel.BindPreview((StreamModel)data);
                    }
                    else if (data is ChannelModel)
                    {
                        panel.BindPreview((ChannelModel)data);
                    }
                    else
                    {
                        panel.ClearPreview();
                    }
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindPreview(StreamAndChannelModel model)
        {
            if (model.ViewType == SettingsConstants.TYPE_STREAM)
            {
                MultiBinding sourceBinding = new MultiBinding { Converter = new StreamProfileImageConverter(), ConverterParameter = ImageUtility.IMG_CROP };
                sourceBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Stream"),
                });
                sourceBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Stream.ProfileImage"),
                });
                imgControl.SetBinding(Image.SourceProperty, sourceBinding);
                lblViewCount.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Stream.ViewCount"), StringFormat = "0,0" });
                lblTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Stream.Title") });
                lblFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Stream.UserName") });

                MouseBinding mouseBinding = new MouseBinding();
                mouseBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
                mouseBinding.Command = StreamViewModel.Instance.StreamViewCommand;
                mouseBinding.CommandParameter = model.Stream;
                imgControl.InputBindings.Add(mouseBinding);
            }
            else if (model.ViewType == SettingsConstants.TYPE_CHANNEL)
            {
                MultiBinding sourceBinding = new MultiBinding { Converter = new ChannelProfileImageConverter(), ConverterParameter = ImageUtility.IMG_CROP };
                sourceBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Channel"),
                });
                sourceBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Channel.ProfileImage"),
                });
                imgControl.SetBinding(Image.SourceProperty, sourceBinding);
                lblViewCount.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Channel.ViewCount"), StringFormat = "0,0" });
                lblTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Channel.Title") });
                lblSubcriber.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Channel.SubscriberCount"), StringFormat = "0,0 Followers" });

                MouseBinding mouseBinding = new MouseBinding();
                mouseBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
                mouseBinding.Command = ChannelViewModel.Instance.ChannelViewCommand;
                mouseBinding.CommandParameter = model.Channel;
                imgControl.InputBindings.Add(mouseBinding);

                dynamic param = new ExpandoObject();
                param.IsFromMyChannel = false;
                param.ChannelInfoModel = model.Channel;

                MouseBinding infoBinding = new MouseBinding();
                infoBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
                infoBinding.Command = ChannelViewModel.Instance.ChannelDetailsCommand;
                infoBinding.CommandParameter = param;
                btnChannelInfo.InputBindings.Add(infoBinding);
            }
        }

        private void BindPreview(StreamModel model)
        {
            MultiBinding sourceBinding = new MultiBinding { Converter = new StreamProfileImageConverter(), ConverterParameter = ImageUtility.IMG_CROP };
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath(""),
            });
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ProfileImage"),
            });
            imgControl.SetBinding(Image.SourceProperty, sourceBinding);
            lblViewCount.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("ViewCount"), StringFormat = "0,0" });
            lblTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Title")});
            lblFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("UserName") });

            MouseBinding mouseBinding = new MouseBinding();
            mouseBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
            mouseBinding.Command = StreamViewModel.Instance.StreamViewCommand;
            mouseBinding.CommandParameter = model;
            imgControl.InputBindings.Add(mouseBinding);
        }

        private void BindPreview(ChannelModel model)
        {
            MultiBinding sourceBinding = new MultiBinding { Converter = new ChannelProfileImageConverter(), ConverterParameter = ImageUtility.IMG_CROP };
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath(""),
            });
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ProfileImage"),
            });
            imgControl.SetBinding(Image.SourceProperty, sourceBinding);
            lblViewCount.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("ViewCount"), StringFormat = "0,0" });
            lblTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Title")});
            lblSubcriber.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("SubscriberCount"), StringFormat = "0,0 Followers" });

            MouseBinding mouseBinding = new MouseBinding();
            mouseBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
            mouseBinding.Command = ChannelViewModel.Instance.ChannelViewCommand;
            mouseBinding.CommandParameter = model;
            imgControl.InputBindings.Add(mouseBinding);

            dynamic param = new ExpandoObject();
            param.IsFromMyChannel = false;
            param.ChannelInfoModel = model;

            MouseBinding infoBinding = new MouseBinding();
            infoBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
            infoBinding.Command = ChannelViewModel.Instance.ChannelDetailsCommand;
            infoBinding.CommandParameter = param;
            btnChannelInfo.InputBindings.Add(infoBinding);
        }

        private void ClearPreview()
        {
            imgControl.ClearValue(Image.SourceProperty);
            imgControl.Source = null;
            imgControl.InputBindings.Clear();
            btnChannelInfo.InputBindings.Clear();
            imgLive.ClearValue(Image.SourceProperty);
            imgLive.Source = null;
            imgViewCount.ClearValue(Image.SourceProperty);
            imgViewCount.Source = null;
            lblViewCount.ClearValue(TextBlock.TextProperty);
            lblTitle.ClearValue(TextBlock.TextProperty);
            lblFullName.ClearValue(TextBlock.TextProperty);
            lblSubcriber.ClearValue(TextBlock.TextProperty);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    if (disposing)
                    {
                        this.ClearPreview();
                        this.imgControl = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

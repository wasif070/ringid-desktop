﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;
using View.Utility;
using View.Utility.Chat;

namespace View.UI.Activity
{
    /// <summary>
    /// Interaction logic for UCSingleActivityPanel.xaml
    /// </summary>
    public partial class UCSingleActivityPanel : UserControl, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleActivityPanel).Name);
        private bool disposed = false;

        public UCSingleActivityPanel()
        {
            this.MinHeight = ChatHelpers.ACTIVITY_VIEW_HEIGHT;
            InitializeComponent();
        }

        ~UCSingleActivityPanel()
        {
            Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleActivityPanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleActivityPanel panel = ((UCSingleActivityPanel)o);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewValue != null && (bool)e.NewValue)
                {
                    model.PrevViewHeight = model.MinViewHeight;
                    panel.pnlMessage.SizeChanged += panel.OnSizeChanged;
                    panel.BindPreview();
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                RecentModel model = ((RecentModel)this.DataContext);
                if ((int)this.ActualHeight > model.PrevViewHeight)
                {
                    model.PrevViewHeight = (int)this.ActualHeight;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSizeChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindPreview()
        {
            txtMessage.SetBinding(ActivityRichTextView.DataProperty, new Binding { Path = new PropertyPath("Activity") });
            txtTime.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Activity.UpdateTime"), Converter = new ChatTimeConverter() });
        }

        private void ClearPreview()
        {
            txtMessage.ClearValue(ActivityRichTextView.DataProperty);
            txtMessage.Data = null;
            txtTime.ClearValue(TextBlock.TextProperty);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    this.pnlMessage.SizeChanged -= OnSizeChanged;
                    if (disposing)
                    {
                        this.ClearPreview();
                        this.txtMessage = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Utility Method
    }
}

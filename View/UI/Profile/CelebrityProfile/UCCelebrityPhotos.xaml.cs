﻿using log4net;
using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;

namespace View.UI.Profile.CelebrityProfile
{
    /// <summary>
    /// Interaction logic for UCCelebrityPhotos.xaml
    /// </summary>
    public partial class UCCelebrityPhotos : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCelebrityPhotos).Name);
        #region "Private Member"
        private long CelebrityUserTableID;
        #endregion

        #region "Constructor"
        public UCCelebrityPhotos(long CelebrityUserTableID, ObservableCollection<ImageModel> CelebFeedImageList, Visibility IsVisibleFeedPhotosLoader)
        {
            this.CelebFeedImageList = CelebFeedImageList;
            this.CelebrityUserTableID = CelebrityUserTableID;
            InitializeComponent();
            this.DataContext = this;
            this.IsVisibleFeedPhotosLoader = IsVisibleFeedPhotosLoader;
        }
        #endregion

        #region "Property"
        private ObservableCollection<ImageModel> _CelebFeedImageList;
        public ObservableCollection<ImageModel> CelebFeedImageList
        {
            get
            {
                return _CelebFeedImageList;
            }
            set
            {
                _CelebFeedImageList = value;
                this.OnPropertyChanged("CelebFeedImageList");
            }
        }
        private Visibility _IsVisibleFeedPhotosLoader;
        public Visibility IsVisibleFeedPhotosLoader
        {
            get { return _IsVisibleFeedPhotosLoader; }
            set
            {
                if (value == _IsVisibleFeedPhotosLoader)
                    return;

                _IsVisibleFeedPhotosLoader = value;
                this.OnPropertyChanged("IsVisibleFeedPhotosLoader");
            }
        }
        private int _BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
        public int BOTTOM_LOADING
        {
            get { return _BOTTOM_LOADING; }
            set
            {
                _BOTTOM_LOADING = value;
                this.OnPropertyChanged("BOTTOM_LOADING");
            }
        }
        #endregion

        #region "ICommands"
        private ICommand _ImageClickCommand;
        public ICommand MiniImageClickCommand
        {
            get
            {
                if (_ImageClickCommand == null)
                {
                    _ImageClickCommand = new RelayCommand(param => OnImageClick(param));
                }
                return _ImageClickCommand;
            }
        }        
        #endregion

        #region "Event Handler"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        {
            if (BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                LoadMorePhotos();
            }
            e.Handled = true;
        }        
        #endregion

        #region "Private Method"
        private void OnImageClick(object param)
        {
            try
            {
                ImageModel model = (ImageModel)param;
                if (!model.NoImageFound)
                {                    
                    if (CelebFeedImageList.Count > 0)
                    {
                        ImageUtility.ShowImageViewFromCollections(this.CelebrityUserTableID, CelebFeedImageList, model.ImageId, CelebFeedImageList.IndexOf(model), UCMiddlePanelSwitcher.View_CelebrityProfile.FeedImageCount);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnImageClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }        
        #endregion

        #region "Public Method"
        public void ShowHideShowMoreLoading(bool makeVisible)
        {
            try
            {
                IsVisibleFeedPhotosLoader = Visibility.Collapsed;
                if (makeVisible)
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                }
                else
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowHideShowMoreLoading() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void LoadMorePhotos()
        {
            try
            {
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
                //SendDataToServer.SendAlbumRequest(this.CelebrityUserTableID, Guid.Empty, FeedImageControl.Items.Count - 1);//to do
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadMorePhotos() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion
    }
}

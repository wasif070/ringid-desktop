﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;


namespace View.UI.Profile.CelebrityProfile
{
    /// <summary>
    /// Interaction logic for UCFriendNewsFeeds.xaml
    /// </summary>
    public partial class UCCelebrityProfileFeeds : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCelebrityProfileFeeds).Name);

        private long CelebrityUserIdentity;
        private CelebrityModel _CelebrityModel;
        public CelebrityModel CelebrityModel
        {
            get { return _CelebrityModel; }
            set { _CelebrityModel = value; }
        }
        private CustomObservableCollection _ViewCollection;
        public CustomObservableCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        public CustomScrollViewer scroll;

        public UCCelebrityProfileFeeds(CelebrityModel CelebrityInfoModel, CustomScrollViewer scroll)
        {
            this.CelebrityModel = CelebrityInfoModel;
            this.CelebrityUserIdentity = CelebrityInfoModel.UserIdentity;
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            //
            //if (RingIDViewModel.Instance.CelebrityProfileFeeds.Count > 1) RingIDViewModel.Instance.CelebrityProfileFeeds.RemoveAllModels();
            //if (RingIDViewModel.Instance.CelebrityProfileFeeds.Count != 1) RingIDViewModel.Instance.CelebrityProfileFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_CELEBRITY_PROFILE); //safety check
            FeedDataContainer.Instance.CelebrityProfileFeedsSortedIds.Clear();

            //RingIDViewModel.Instance.CelebrityProfileFeeds.UserProfileUtId = CelebrityModel.UserTableID;//only for profiles
            //RingIDViewModel.Instance.CelebrityProfileFeeds.pId = 0;//only for pageprofiles
            //ViewCollection = RingIDViewModel.Instance.CelebrityProfileFeeds;
            scroll.SetScrollValues(ViewCollection, FeedDataContainer.Instance.CelebrityProfileFeedsSortedIds, SettingsConstants.PROFILE_TYPE_CELEBRITY, AppConstants.TYPE_FRIEND_NEWSFEED);

            DefaultSettings.PROFILECELEBRITY_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.PROFILECELEBRITY_STARTPKT);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs args)
        {
            scroll.ScrollToHome();
            if (ViewCollection == null)
            {
                if (timer == null)
                {
                    timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromMilliseconds(500);
                    timer.Tick += TimerEventProcessor;
                }
                else timer.Stop();
                timer.Start();
            }
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs args)
        {
            //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
            //{
                scroll.SetScrollEvents(false);
                ViewCollection = null;
            //}
        }
        private DispatcherTimer timer;
        private void TimerEventProcessor(object sender, EventArgs e)
        {
            if (timer != null && timer.IsEnabled)
            {
                //ViewCollection = RingIDViewModel.Instance.CelebrityProfileFeeds;
                scroll.SetScrollEvents(true);
                if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    Keyboard.Focus(scroll);
                timer.Stop();
            }
        }

        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            DefaultSettings.PROFILECELEBRITY_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.PROFILECELEBRITY_STARTPKT);
        }

        #region "Utility Methods"
        #endregion "Utility Methods"
    }
}

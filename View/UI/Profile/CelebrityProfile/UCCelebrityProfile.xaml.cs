﻿using log4net;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;
using System.Windows.Threading;
using Models.Constants;

namespace View.UI.Profile.CelebrityProfile
{
    /// <summary>
    /// Interaction logic for UCCelebrityProfile.xaml
    /// </summary>
    public partial class UCCelebrityProfile : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCelebrityProfile).Name);

        public long CelebrityUserTableID = 0;
        public bool isTopLoading = false;
        public bool isBottomLoading = false;
        public UCCelebrityProfileFeeds _UCCelebProfileFeeds;
        public UCCelebrityPhotos _UCCelebrityPhotos;

        #region "Constructor"
        public UCCelebrityProfile(CelebrityModel CelebrityModel)
        {
            this.CelebrityModel = CelebrityModel;
            CelebrityUserTableID = CelebrityModel.UserTableID;
            this.DataContext = this;
            InitializeComponent();
            CelebFeedImageList.Add(new ImageModel() { IsLoadMore = true });
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Property

        private CelebrityModel _CelebrityModel;
        public CelebrityModel CelebrityModel
        {
            get { return _CelebrityModel; }
            set { _CelebrityModel = value; }
        }

        private string followUnfollowTxt = "";
        public string FollowUnfollowTxt
        {
            get { return followUnfollowTxt; }
            set
            {
                followUnfollowTxt = value;
                this.OnPropertyChanged("FollowUnfollowTxt");
            }
        }
        private string followUnfollowPlus = "";
        public string FollowUnfollowPlus
        {
            get { return followUnfollowPlus; }
            set
            {
                followUnfollowPlus = value;
                this.OnPropertyChanged("FollowUnfollowPlus");
            }
        }
        private ObservableCollection<ImageModel> _CelebFeedImageList = new ObservableCollection<ImageModel>();
        public ObservableCollection<ImageModel> CelebFeedImageList
        {
            get
            {
                return _CelebFeedImageList;
            }
            set
            {
                _CelebFeedImageList = value;
                this.OnPropertyChanged("CelebFeedImageList");
            }
        }
        private int _FeedImageCount;
        public int FeedImageCount
        {
            get
            {
                return _FeedImageCount;
            }
            set
            {
                _FeedImageCount = value;
                this.OnPropertyChanged("FeedImageCount");
            }
        }
        private Visibility _IsVisibleFeedPhotosLoader = Visibility.Visible;
        public Visibility IsVisibleFeedPhotosLoader
        {
            get { return _IsVisibleFeedPhotosLoader; }
            set
            {
                if (value == _IsVisibleFeedPhotosLoader)
                    return;

                _IsVisibleFeedPhotosLoader = value;
                this.OnPropertyChanged("IsVisibleFeedPhotosLoader");
            }
        }
        #endregion Property

        void backBtn_Click(object sender, RoutedEventArgs e)
        {
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }

        private void followBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CelebrityModel.IsSubscribed)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "this celebrity"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                if (isTrue)
                    new ThrdCelebrityFollowUnfollow(CelebrityModel.UserTableID, false, CelebrityModel).StartThread();
            }
            else
            {
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.FOLLOW_CONFIRMATION, "this celebrity"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Follow"));
                if (isTrue)
                    new ThrdCelebrityFollowUnfollow(CelebrityModel.UserTableID, true, CelebrityModel).StartThread();
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int index = TabControl.SelectedIndex;
                switch (index)
                {
                    case 0:
                        if (_UCCelebProfileFeeds == null)
                        {
                            _UCCelebProfileFeeds = new UCCelebrityProfileFeeds(CelebrityModel, scroll);
                        }
                        PostsPanel.Child = _UCCelebProfileFeeds;
                        break;
                    case 1:
                        if (_UCCelebrityPhotos == null)
                        {
                            _UCCelebrityPhotos = new UCCelebrityPhotos(CelebrityUserTableID, CelebFeedImageList, IsVisibleFeedPhotosLoader);
                        }
                        PhotosPanel.Child = _UCCelebrityPhotos;
                        break;
                }
            }
            catch (Exception ex)
            {
            }
        }

        #region "Utility Methods"
        public void LoadCelebrityFeedImage(List<ImageModel> sortedList, int count)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    foreach (var image in sortedList)
                    {
                        lock (this.CelebFeedImageList)
                        {
                            if (!this.CelebFeedImageList.Any(p => p.ImageId == image.ImageId))
                            {
                                if (CelebFeedImageList.Count > 0 && CelebFeedImageList[0].Time < image.Time)
                                {
                                    this.CelebFeedImageList.Insert(0, image);
                                }
                                else
                                    this.CelebFeedImageList.Insert(CelebFeedImageList.Count - 1, image);
                            }

                        }
                        Thread.Sleep(5);
                    }
                    if (_UCCelebrityPhotos != null)
                    {
                        _UCCelebrityPhotos.ShowHideShowMoreLoading(count > (this.CelebFeedImageList.Count - 1));
                    }
                    this.OnPropertyChanged("CelebFeedImageList");
                }
                catch (Exception ex)
                {
                    log.Error("Error: LoadCelebrityFeedImage() ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            });
        }
        #endregion "Utility Methods"


        public void RequestPhotosToServer()
        {
            //SendDataToServer.SendAlbumRequest(CelebrityUserTableID, Guid.Empty, 0);//to do
            if (_UCCelebrityPhotos == null)
            {
                _UCCelebrityPhotos = new UCCelebrityPhotos(CelebrityUserTableID, CelebFeedImageList, IsVisibleFeedPhotosLoader);
            }
        }

        public void RemoveAllPhotoModel()
        {
            lock (CelebFeedImageList)
            {
                while (CelebFeedImageList.Count > 1)
                {
                    ImageModel model = CelebFeedImageList.FirstOrDefault();
                    if (!model.IsLoadMore)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            CelebFeedImageList.Remove(model);
                            GC.SuppressFinalize(model);
                        }, DispatcherPriority.Send);
                    }
                }
            }
        }
    }
}

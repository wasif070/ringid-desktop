﻿using System;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Auth.Service.Images;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Images;
using View.Utility.WPFMessageBox;
using WpfAnimatedGif;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCCoverPhotoChange.xaml
    /// </summary>
    public partial class UCMyCoverPhotoChange : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyCoverPhotoChange).Name);

        #region "Private Member"
        private double cropX;
        private double cropY;
        private double imageWidth;
        private double imageHeight;
        private string fileName;
        private Image uiImage;
        private Point mousePosition;
        private byte[] postData;
        private bool isClicked;
        private BackgroundWorker bgworker = null, bgworkerUpload = null;
        //private string imageUrl;
        private string convertedUrl;
        private string imageDirectory;
        private ImageModel objImgModel;

        private double imageWidthOriginal;
        private double imageHeightOriginal;
        private byte[] postDataOriginal;
        private double scalRatio;
        #endregion

        #region "Property"
        public UCPhotoSelectionView PhotoSelectionView
        {
            get
            {
                return MainSwitcher.PopupController.PhotoSelectionView;
            }
        }
        private ImageSource _pstLoader;
        public ImageSource pstLoader
        {
            get
            {
                return _pstLoader;
            }
            set
            {
                if (value == _pstLoader)
                    return;
                _pstLoader = value;
                OnPropertyChanged("pstLoader");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Constructor"
        public UCMyCoverPhotoChange()
        {
            InitializeComponent();
            this.DataContext = this;
            //ResetAll();
        }
        #endregion

        #region "Event Handler"
        private void CanvasMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                uiImage = e.Source as Image;

                if (uiImage != null && _Canvas.CaptureMouse() && this.btnSave.IsEnabled)
                {
                    this.isClicked = true;
                    mousePosition = e.GetPosition(_Canvas);
                    Panel.SetZIndex(uiImage, 1); // in case of multiple images
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CanvasMouseLeftButtonDown() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CanvasMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (uiImage != null)
                {
                    this.isClicked = false;
                    _Canvas.ReleaseMouseCapture();
                    Panel.SetZIndex(uiImage, 0);
                    uiImage = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CanvasMouseLeftButtonUp() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CanvasMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (uiImage != null && this.btnSave.IsEnabled && this.isClicked)
                {
                    var position = e.GetPosition(_Canvas);
                    var offset = position - mousePosition;
                    mousePosition = position;
                    double MinX = 0;
                    double MinY = 0;
                    double MaxX = (-imageWidth + RingIDSettings.COVER_PIC_DISPLAY_WIDTH);
                    double MaxY = (-imageHeight + RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);

                    if ((Canvas.GetLeft(uiImage) + offset.X) <= MinX
                        && (Canvas.GetLeft(uiImage) + offset.X) >= MaxX
                        && (Canvas.GetTop(uiImage) + offset.Y) <= MinY
                        && (Canvas.GetTop(uiImage) + offset.Y) >= MaxY)
                    {
                        cropX = Canvas.GetLeft(uiImage) + offset.X;
                        cropY = Canvas.GetTop(uiImage) + offset.Y;
                        Canvas.SetLeft(uiImage, cropX);
                        Canvas.SetTop(uiImage, cropY);
                        _Canvas.UpdateLayout();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CanvasMouseMove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Click_CancelButton(object sender, RoutedEventArgs e)
        {
            try
            {
                //MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyProfile);
                UCMiddlePanelSwitcher.View_UCMyProfile.HideCoverPicChangePanel();
            }
            catch (Exception ex)
            {
                log.Error("Error: Click_CancelButton() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UploadFromComputer_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                OpenFileDialog op = new OpenFileDialog();
                op.Title = "Select a picture";
                op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                  "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                  "Portable Network Graphic (*.png)|*.png";

                if ((bool)op.ShowDialog())
                {
                    Uri uri = new Uri(op.FileName);
                    System.Drawing.Bitmap bmp = ImageUtility.GetBitmapOfDynamicResource(op.FileName);

                    BitmapImage bitmapImage = new BitmapImage(uri);
                    if (bitmapImage.Width < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH || bitmapImage.Height < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT)
                    {
                        //CustomMessageBox.ShowWarning("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                        UIHelperMethods.ShowWarning("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT, "Uploade image failed");
                    }
                    else
                    {
                        int quality = 75;
                        postData = ImageUtility.ReduceQualityAndSize(bmp, RingIDSettings.COVER_PIC_MAXIMUM_WIDTH, quality, SettingsConstants.TYPE_COVER_IMAGE);  //RingIDSettings.COVER_PIC_MAXIMUM_WIDTH = 1480
                        BitmapImage bmpImg = ImageUtility.ConvertByteArrayToBitmapImage(postData);
                        imageWidth = bmpImg.Width;
                        imageHeight = bmpImg.Height;
                        uiImage = new Image { Source = bmpImg };
                        cropX = 0;
                        cropY = 0;
                        Canvas.SetLeft(uiImage, cropX);
                        Canvas.SetTop(uiImage, cropY);
                        _Canvas.Children.Clear();
                        _Canvas.Children.Add(uiImage);

                        this.btnSave.IsEnabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UploadFromComputer_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Click_SaveButton(object sender, RoutedEventArgs e)
        {
            if (objImgModel != null && objImgModel.ImageUrl == DefaultSettings.userProfile.CoverImage
                && cropX == -DefaultSettings.userProfile.CropImageX && cropY == -DefaultSettings.userProfile.CropImageY)
            {
                // CustomMessageBox.ShowInformation("Nothing to Save.");
                UIHelperMethods.ShowInformation("Nothing to Save.", "Save photo");
            }
            else
            {
                BeginCoverPhotoUpload();
            }
        }
        #endregion

        #region "Private Method"
        private void SetCoverImageToCanvas()
        {
            try
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    SetLoadingGifToCanvas();

                    new Thread(() =>
                    {
                        try
                        {
                            System.Drawing.Bitmap bitmap = (System.Drawing.Bitmap)ImageUtility.GetOrientedImageFromFilePath(fileName);//ImageUtility.GetBitmapOfDynamicResource(fileName);
                            if (bitmap != null)//can't read file
                            {
                                if (bitmap.Width < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH || bitmap.Height < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT)
                                {
                                    if (fileName.Contains("webcam") && bitmap.Width == 640)
                                    {
                                        bitmap = ImageUtility.ResizeBitmap(bitmap, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH, bitmap.Height);
                                        int quality = 100;
                                        postDataOriginal = ImageUtility.ReduceQualityAndSize(bitmap, RingIDSettings.COVER_PIC_MAXIMUM_WIDTH, quality, SettingsConstants.TYPE_COVER_IMAGE);  //RingIDSettings.COVER_PIC_MAXIMUM_WIDTH = 1480

                                        BitmapImage bitmapImageOriginal = ImageUtility.ConvertByteArrayToBitmapImage(postDataOriginal);
                                        imageWidthOriginal = bitmapImageOriginal.Width;
                                        imageHeightOriginal = bitmapImageOriginal.Height;

                                        quality = 100;
                                        postData = ImageUtility.ReduceQualityAndSize(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, quality, SettingsConstants.TYPE_COVER_IMAGE);
                                        bitmap.Dispose();
                                        Application.Current.Dispatcher.Invoke(() =>
                                        {
                                            MainSwitcher.PopupController.PhotoSelectionView.OnCloseCommand();
                                            LoadImageToCanvas();
                                        }, System.Windows.Threading.DispatcherPriority.Send);
                                    }
                                    else
                                    {
                                        bitmap.Dispose();
                                        Application.Current.Dispatcher.Invoke(() =>
                                        {
                                            UCMiddlePanelSwitcher.View_UCMyProfile.HideCoverPicChangePanel();
                                            UIHelperMethods.ShowWarning("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT, "Cover photo");
                                        }, System.Windows.Threading.DispatcherPriority.Send);
                                    }
                                }
                                else
                                {
                                    int quality = 75;
                                    postDataOriginal = ImageUtility.ReduceQualityAndSize(bitmap, RingIDSettings.COVER_PIC_MAXIMUM_WIDTH, quality, SettingsConstants.TYPE_COVER_IMAGE);  //RingIDSettings.COVER_PIC_MAXIMUM_WIDTH = 1480

                                    BitmapImage bitmapImageOriginal = ImageUtility.ConvertByteArrayToBitmapImage(postDataOriginal);
                                    imageWidthOriginal = bitmapImageOriginal.Width;
                                    imageHeightOriginal = bitmapImageOriginal.Height;
                                    quality = 100;
                                    postData = ImageUtility.ReduceQualityAndSize(bitmap, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, quality, SettingsConstants.TYPE_COVER_IMAGE);
                                    bitmap.Dispose();
                                    Application.Current.Dispatcher.Invoke(() =>
                                    {
                                        LoadImageToCanvas();
                                    }, System.Windows.Threading.DispatcherPriority.Send);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error: UpdateCoverImageToCanvas() ==> " + ex.Message + "\n" + ex.StackTrace);
                        }
                    }).Start();
                }
                else
                {
                    SetOriginalCoverImageToCanvas();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UpdateCoverImageToCanvas() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LoadImageToCanvas()
        {
            try
            {
                BitmapImage bmpImg = ImageUtility.ConvertByteArrayToBitmapImage(postData);
                imageWidth = bmpImg.Width;
                imageHeight = bmpImg.Height;
                uiImage = new Image { Source = bmpImg };

                double left = (_Canvas.Width - imageWidth) / 2;
                Canvas.SetLeft(uiImage, left);

                double top = (_Canvas.Height - imageHeight) / 2;
                Canvas.SetTop(uiImage, top);

                //if (objImgModel != null && objImgModel.ImageUrl == ViewModel.RingIDViewModel.Instance.MyBasicInfoModel.CoverImage)
                //{
                //    cropX = -ViewModel.RingIDViewModel.Instance.MyBasicInfoModel.CropImageX;
                //    cropY = -ViewModel.RingIDViewModel.Instance.MyBasicInfoModel.CropImageY;
                //}
                //else
                //{
                cropX = left;
                cropY = top;
                //}
                //Canvas.SetLeft(uiImage, cropX);
                //Canvas.SetTop(uiImage, cropY);
                _Canvas.Children.Clear();
                _Canvas.Children.Add(uiImage);

                txtBlockDrag.Visibility = Visibility.Visible;
                this.btnSave.IsEnabled = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadImageToCanvas() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetUnknownImageToCanvas()
        {
            try
            {
                uiImage = new Image { Source = ImageObjects.COVER_IMAGE_BACKGROUND };
                cropX = 0;
                cropY = 0;
                Canvas.SetLeft(uiImage, cropX);
                Canvas.SetTop(uiImage, cropY);
                _Canvas.Children.Clear();
                _Canvas.Children.Add(uiImage);

                txtBlockDrag.Visibility = Visibility.Hidden;
                this.btnSave.IsEnabled = false;
                this.btnCancel.IsEnabled = true;
                this.Loading_upload.Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                log.Error("Error: SetUnknownImageToCanvas() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetLoadingGifToCanvas()
        {
            try
            {
                BitmapImage image = ImageUtility.GetBitmapImage(ImageLocation.LOADER_CLOCK);
                //image.BeginInit();
                //image.UriSource = new Uri(@"/Resources/Images/loaders/waiting.gif,UriKind.RelativeOrAbsolute");
                //image.EndInit();
                uiImage = new Image { Source = image };

                ImageBehavior.SetAnimatedSource(uiImage, image);

                cropX = 0;
                cropY = 0;
                Canvas.SetLeft(uiImage, RingIDSettings.COVER_PIC_DISPLAY_WIDTH / 2);
                Canvas.SetTop(uiImage, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT / 2);
                _Canvas.Children.Clear();
                _Canvas.Children.Add(uiImage);

                txtBlockDrag.Visibility = Visibility.Hidden;
                this.btnSave.IsEnabled = false;
                this.btnCancel.IsEnabled = true;
                this.Loading_upload.Visibility = Visibility.Hidden;
                GC.SuppressFinalize(image);
            }
            catch (Exception ex)
            {
                log.Error("Error: SetLoadingGifToCanvas() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        private void DownloadPreviousImageToCanvas()
        {
            try
            {
                if (bgworker == null)
                {
                    bgworker = new BackgroundWorker();
                    bgworker.DoWork += (s, e) =>
                    {
                        e.Result = ImageUtility.DownloadImage(convertedUrl.Replace("_", "/"), imageDirectory + System.IO.Path.DirectorySeparatorChar + convertedUrl);
                        //e.Result = _BitmapImage;
                    };
                    bgworker.RunWorkerCompleted += (s, e) =>
                    {
                        try
                        {
                            string newImageUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                            System.Drawing.Bitmap img = ImageUtility.GetBitmapOfDynamicResource(newImageUrl);

                            if (e.Result != null && e.Result is bool && (bool)e.Result && img != null)
                            {
                                //Exception occured in memory stream
                                //System.Drawing.Bitmap img = (System.Drawing.Bitmap)e.Result;
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    postDataOriginal = postData = ms.ToArray();
                                }
                                postDataOriginal = ImageUtility.ReduceQualityAndSize(img, RingIDSettings.COVER_PIC_MAXIMUM_WIDTH, 85, SettingsConstants.TYPE_COVER_IMAGE);

                                BitmapImage bitmapImageOriginal = ImageUtility.ConvertByteArrayToBitmapImage(postDataOriginal);
                                imageWidthOriginal = bitmapImageOriginal.Width;
                                imageHeightOriginal = bitmapImageOriginal.Height;

                                int quality = 100;
                                postData = ImageUtility.ReduceQualityAndSize(img, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, quality, SettingsConstants.TYPE_COVER_IMAGE);
                                img.Dispose();
                                LoadImageToCanvas();
                            }
                            else
                            {
                                SetUnknownImageToCanvas();
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error: DownloadPreviousImageToCanvas() ==> " + ex.Message + "\n" + ex.StackTrace);
                        }
                    };
                }
                bgworker.WorkerReportsProgress = true;
                bgworker.WorkerSupportsCancellation = true;

                if (bgworker.IsBusy != true)
                {
                    bgworker.RunWorkerAsync();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: DownloadPreviousImageToCanvas() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetOriginalCoverImageToCanvas()
        {
            try
            {
                if (objImgModel != null)
                {

                    convertedUrl = HelperMethods.ConvertUrlToName(objImgModel.ImageUrl, ImageUtility.IMG_FULL);
                    string newImageUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    System.Drawing.Bitmap existingImage = ImageUtility.GetBitmapOfDynamicResource(newImageUrl);

                    if (existingImage != null)
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            existingImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            postDataOriginal = postData = ms.ToArray();
                        }
                        postDataOriginal = ImageUtility.ReduceQualityAndSize(existingImage, RingIDSettings.COVER_PIC_MAXIMUM_WIDTH, 85, SettingsConstants.TYPE_COVER_IMAGE);

                        BitmapImage bitmapImageOriginal = ImageUtility.ConvertByteArrayToBitmapImage(postDataOriginal);
                        imageWidthOriginal = bitmapImageOriginal.Width;
                        imageHeightOriginal = bitmapImageOriginal.Height;

                        int quality = 100;
                        postData = ImageUtility.ReduceQualityAndSize(existingImage, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, quality, SettingsConstants.TYPE_COVER_IMAGE);
                        existingImage.Dispose();
                        LoadImageToCanvas();
                    }
                    else
                    {
                        SetLoadingGifToCanvas();
                        DownloadPreviousImageToCanvas();
                    }
                }
                else
                {
                    SetUnknownImageToCanvas();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: SetOriginalCoverImageToCanvas() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void GetImageDirectory(int imageType)
        {
            if (imageType == SettingsConstants.TYPE_COVER_IMAGE)
            {
                imageDirectory = RingIDSettings.TEMP_COVER_IMAGE_FOLDER;
            }
            else if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE)
            {
                imageDirectory = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER;
            }
            else
            {
                imageDirectory = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER;
            }
        }
        #endregion

        #region "Public Method"
        public void ShowCoverPhotoHandler(ImageModel imgModel)
        {
            ResetAll();
            this.objImgModel = imgModel;
            //this.imageUrl = objImgModel.ImageUrl;            
            GetImageDirectory(objImgModel.ImageType);
            SetCoverImageToCanvas();
        }
        public void ShowCoverPhotoHandler(string fileName)
        {
            ResetAll();
            this.fileName = fileName;
            SetCoverImageToCanvas();
        }

        public void ResetAll()
        {
            try
            {
                objImgModel = null;
                this.cropX = 0;
                this.cropY = 0;
                this.imageWidth = 0;
                this.imageHeight = 0;
                this.fileName = null;
                this.uiImage = null;
                this.postData = null;
                imageWidthOriginal = 0;
                imageHeightOriginal = 0;
                scalRatio = 0;
                //this.imageUrl = string.Empty;
                this.convertedUrl = string.Empty;
                this.imageDirectory = null;
                this.isClicked = false;
                txtBlockDrag.Visibility = Visibility.Collapsed;
                this.btnSave.IsEnabled = false;
                this.btnCancel.IsEnabled = true;
                this.Loading_upload.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                log.Error("Error: ResetAll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "BackgroundWorker"
        int responseCode = 0;
        private void BeginCoverPhotoUpload()
        {
            try
            {
                this.btnCancel.IsEnabled = false;
                this.btnSave.IsEnabled = false;
                this.Loading_upload.Visibility = Visibility.Visible;
                pstLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_CLOCK);
                scalRatio = imageWidthOriginal / imageWidth;

                if (bgworkerUpload == null)
                {
                    bgworkerUpload = new BackgroundWorker();
                    bgworkerUpload.DoWork += new DoWorkEventHandler(CoverPictureUpload_DoWork);
                    bgworkerUpload.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CoverPictureUpload_RunWorkerCompleted);
                }
                bgworkerUpload.WorkerReportsProgress = true;
                bgworkerUpload.WorkerSupportsCancellation = true;

                if (bgworkerUpload.IsBusy != true)
                {
                    bgworkerUpload.RunWorkerAsync();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: BeginCoverPhotoUpload() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverPictureUpload_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string response = null;
                Guid imgId = Guid.Empty;
                double cropXOriginal = Math.Round(cropX * scalRatio);
                double cropYOriginal = Math.Round(cropY * scalRatio);

                int width, height, issa = 0;
                width = (int)(RingIDSettings.COVER_PIC_DISPLAY_WIDTH * scalRatio);
                height = (int)(RingIDSettings.COVER_PIC_DISPLAY_HEIGHT * scalRatio);

                if (postDataOriginal != null)
                {
                    if (objImgModel != null && objImgModel.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
                    {
                        //if (objImgModel.AlbumId == DefaultSettings.COVER_IMAGE_ALBUM_ID)//to do
                        imgId = objImgModel.ImageId;
                        string responseUrl = ProfileCoverImageUpload.Instance.UpdateCropImageToImageServerWebRequest(-cropXOriginal, -cropYOriginal, width, height, objImgModel.ImageUrl);
                        if (!string.IsNullOrEmpty(responseUrl))
                        {
                            issa = 1;
                            dynamic state = new ExpandoObject();
                            state.sucs = true;
                            state.iurl = responseUrl;
                            response = Newtonsoft.Json.JsonConvert.SerializeObject(state);
                            /*if (imgId == DefaultSettings.userProfile.CoverImageId)
                            {
                                ImageUploadHelper.GetRepositionCropImageUpdate(responseUrl, RingIDSettings.TEMP_COVER_IMAGE_FOLDER, objImgModel.ImageType);
                                response = responseUrl;
                            }
                            else
                            {
                                dynamic state = new ExpandoObject();
                                state.sucs = true;
                                state.iurl = responseUrl;
                                response = Newtonsoft.Json.JsonConvert.SerializeObject(state);
                            }*/
                        }
                    }
                    else
                        response = ProfileCoverImageUpload.Instance.UploadToImageServerWebRequest(postDataOriginal, SettingsConstants.TYPE_COVER_IMAGE, -cropXOriginal, -cropYOriginal, width, height);//RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);

                    if (!string.IsNullOrEmpty(response)) //&& (imgId == Guid.Empty || imgId != DefaultSettings.userProfile.CoverImageId))
                    {
                        //responseCode = ProfileCoverImageUpload.Instance.SendProfileCoverImagePostRequestToAuth(response, (int)ImageWidth, (int)imageHeight, SettingsConstants.TYPE_COVER_IMAGE, RingIDSettings.TEMP_COVER_IMAGE_FOLDER, -cropX, -cropY, imgId);
                        responseCode = ImageUploadHelper.SendProfileCoverImagePostRequestToAuth(response, (int)imageWidthOriginal, (int)imageHeightOriginal, SettingsConstants.TYPE_COVER_IMAGE, RingIDSettings.TEMP_COVER_IMAGE_FOLDER, -cropXOriginal, -cropYOriginal, imgId, issa);
                    }
                }
                e.Result = response;
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverPictureUpload_DoWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverPictureUpload_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Result is string && !string.IsNullOrEmpty((string)e.Result))
                {
                    if (responseCode > ReasonCodeConstants.REASON_CODE_NONE)
                    {
                        UIHelperMethods.ShowFailed(ReasonCodeConstants.GetReason(responseCode));
                        this.btnCancel.IsEnabled = true;
                        this.btnSave.IsEnabled = true;
                        this.Loading_upload.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        this.Loading_upload.Visibility = Visibility.Hidden;
                        UCMiddlePanelSwitcher.View_UCMyProfile.HideCoverPicChangePanel();
                    }
                }
                else
                {
                    UIHelperMethods.ShowFailed("Failed to upload cover image!", "Upload cover image");
                    this.btnCancel.IsEnabled = true;
                    this.btnSave.IsEnabled = true;
                    this.Loading_upload.Visibility = Visibility.Hidden;
                }
                GC.SuppressFinalize(pstLoader);
                pstLoader = null;
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverPictureUpload_RunWorkerCompleted() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Loaded_Unloaded Events"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            btnCancel.Click += Click_CancelButton;
            btnSave.Click += Click_SaveButton;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            btnCancel.Click -= Click_CancelButton;
            btnSave.Click -= Click_SaveButton;
        }
        #endregion
    }
}


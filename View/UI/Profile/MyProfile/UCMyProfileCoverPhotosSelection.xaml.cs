﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.Utility.DataContainer;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMyProfilePhotosSelectionView.xaml
    /// </summary>
    public partial class UCMyProfileCoverPhotosSelection : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyProfileCoverPhotosSelection).Name);

        public UCMyProfileCoverPhotosSelection()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            TakePhotoTab.MouseLeftButtonUp += Tab_MouseLeftButtonUp;
            UploadFromPcTab.MouseLeftButtonUp += Tab_MouseLeftButtonUp;
            EditPhotoTab.MouseLeftButtonUp += Tab_MouseLeftButtonUp;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            TakePhotoTab.MouseLeftButtonUp -= Tab_MouseLeftButtonUp;
            UploadFromPcTab.MouseLeftButtonUp -= Tab_MouseLeftButtonUp;
            EditPhotoTab.MouseLeftButtonUp -= Tab_MouseLeftButtonUp;
        }

        private void UploadFromcmp()
        {
            if (TYPE == MiddlePanelConstants.TypeMyCoverPictureChange)
            {
                MainSwitcher.PopupController.PhotoSelectionView.OnCloseCommand();
            }
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";

            if ((bool)op.ShowDialog())
            {
                /*Uri uri = new Uri(op.FileName);
                Bitmap bmp = ImageUtility.GetBitmapOfDynamicResource(op.FileName);
                BitmapImage bitmapImage = new BitmapImage(uri);*/

                int formatChecked = ImageUtility.GetFileImageTypeFromHeader(op.FileName);
                if (formatChecked != 1 && formatChecked != 2)
                {
                    UIHelperMethods.ShowFailed("File is corrupted !", "Upload cover image");
                    return;
                }
                ShowImage(op.FileName);
            }
        }

        private void ShowImage(string fileName)
        {
            try
            {
                if (TYPE == MiddlePanelConstants.TypeMyProfilePictureChange)
                {
                    UCProfilePictureUpload profilePictureUpload = new UCProfilePictureUpload(MainSwitcher.PopupController.PhotoSelectionView._MainGrid);
                    profilePictureUpload.Show();
                    profilePictureUpload.ShowHandlerDialog(fileName);
                }
                else if (TYPE == MiddlePanelConstants.TypeMyCoverPictureChange)
                {
                    //PhotoSelectionView.HideHandlerDialog();
                    UCMiddlePanelSwitcher.View_UCMyProfile.ShowCoverPicChangePanel(fileName);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void EditPhoto()
        {
            try
            {
                if (TYPE == MiddlePanelConstants.TypeMyProfilePictureChange)
                {
                    ImageModel objImgModel = ImageDataContainer.Instance.GetFromImageModelDictionary(DefaultSettings.userProfile.ProfileImageId);
                    if (objImgModel == null)
                    {
                        objImgModel = new ImageModel();
                        ImageDataContainer.Instance.AddOrReplaceImageModels(DefaultSettings.userProfile.ProfileImageId, objImgModel);
                    }

                    objImgModel.ImageUrl = DefaultSettings.userProfile.ProfileImage;
                    objImgModel.ImageType = SettingsConstants.TYPE_PROFILE_IMAGE;
                    //objImgModel.AlbumId = DefaultSettings.PROFILE_IMAGE_ALBUM_ID;//to do

                    UCProfilePictureUpload profilePictureUpload = new UCProfilePictureUpload(MainSwitcher.PopupController.PhotoSelectionView._MainGrid);
                    profilePictureUpload.Show();
                    profilePictureUpload.ShowHandlerDialog(objImgModel);
                }
                else if (TYPE == MiddlePanelConstants.TypeMyCoverPictureChange)
                {
                    MainSwitcher.PopupController.PhotoSelectionView.OnCloseCommand();

                    ImageModel objImgModel = ImageDataContainer.Instance.GetFromImageModelDictionary(DefaultSettings.userProfile.CoverImageId);
                    if (objImgModel == null)
                    {
                        objImgModel = new ImageModel();
                        ImageDataContainer.Instance.AddOrReplaceImageModels(DefaultSettings.userProfile.CoverImageId, objImgModel);
                    }

                    objImgModel.ImageUrl = DefaultSettings.userProfile.CoverImage;
                    objImgModel.ImageType = SettingsConstants.TYPE_COVER_IMAGE;
                    //objImgModel.AlbumId = DefaultSettings.COVER_IMAGE_ALBUM_ID;//to do
                    UCMiddlePanelSwitcher.View_UCMyProfile.ShowCoverPicChangePanel(objImgModel);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        private int type = 0;
        public int TYPE
        {
            get { return type; }
            set
            {
                type = value;
                OnPropertyChanged("TYPE");
            }
        }

        public void AddProfileCoverFeedPhotos()
        {
            MyProfilePhotos.Child = UCMiddlePanelSwitcher.View_UCMyProfile.MyProfilePhotosChange;
        }
        public void RemoveProfileCoverFeedPhotos()
        {
            MyProfilePhotos.Child = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void Tab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var tabItem = ((sender as TabItem)).Name as string;
            switch (tabItem)
            {
                case "TakePhotoTab":
                    WNWebcamCapture.Instance.ShowWindow((f) =>
                    {
                        ShowImage(f);
                    });
                    break;
                case "UploadFromPcTab":
                    UploadFromcmp();
                    break;
                case "EditPhotoTab":
                    EditPhoto();
                    break;
            }
        }
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
//using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.FriendList;
using View.ViewModel;
using System.Timers;
using View.Utility.Auth;
using View.UI.PopUp;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMyContactLists.xaml
    /// </summary>
    public partial class UCMyContactLists : UserControl, INotifyPropertyChanged
    {
        //TEST
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyContactLists).Name);       

        #region "Private Member"
        private ListBoxItem prevItem = null;
		private Timer SearchTextBoxTimer;
        private int ThreadMaxRunningTime = 0;
        #endregion

        #region "Public Member"
        public List<UserBasicInfoModel> ListContact = new List<UserBasicInfoModel>();
        #endregion

        #region "Constructor"
        public UCMyContactLists()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UserControlLoaded;
            this.Unloaded += UserControlUnloaded;            
            FriendListInUserProfile.Add(new UserBasicInfoModel());
            //    int totalfrind = RingIDViewModel.Instance.TotalFriendList.Count;
        }
        #endregion
                
        #region "Properties"
        private ObservableCollection<UserBasicInfoModel> _FriendListInUserProfile = new ObservableCollection<UserBasicInfoModel>();        
        public ObservableCollection<UserBasicInfoModel> FriendListInUserProfile
        {
            get
            {
                return _FriendListInUserProfile;
            }
            set
            {
                _FriendListInUserProfile = value;
                _FriendListInUserProfile.CollectionChanged += _FriendListInUserProfile_CollectionChanged;
                this.OnPropertyChanged("FriendListInUserProfile");
            }
        }

        void _FriendListInUserProfile_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (FriendListInUserProfile.Count == (RingIDViewModel.Instance.FavouriteFriendList.Count + RingIDViewModel.Instance.TopFriendList.Count + RingIDViewModel.Instance.NonFavouriteFriendList.Count))
                {
                    HideShowMorePanel();
                }
            }
        }

        private ObservableCollection<UserBasicInfoModel> _SearchListInUserProfile = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> SearchListInUserProfile
        {
            get
            {
                return _SearchListInUserProfile;
            }
            set
            {
                _SearchListInUserProfile = value;
                this.OnPropertyChanged("SearchListInUserProfile");
            }
        }

        private string _SearchString;
        public string SearchString
        {
            get { return _SearchString; }
            set
            {
                if (_SearchString == value) return;
                _SearchString = value;
                IsNoContactFound = false;
                if (!string.IsNullOrWhiteSpace(_SearchString) && _SearchString.Trim().Length > 0)
                {
                    SearchlistBox.Visibility = Visibility.Visible;
                    listTF.Visibility = Visibility.Collapsed;
                    HideShowMorePanel();
                    OnSearch();
                }
                else
                {
                    ThreadLeftSearchFriend.searchParm = null;
                    SearchlistBox.Visibility = Visibility.Collapsed;
                    listTF.Visibility = Visibility.Visible;
                    if (FriendListInUserProfile.Count < (RingIDViewModel.Instance.FavouriteFriendList.Count + RingIDViewModel.Instance.TopFriendList.Count + RingIDViewModel.Instance.NonFavouriteFriendList.Count))
                    {
                        ShowLoader(false);
                    }
                }
            }
        }
        
        private System.Windows.Media.ImageSource _Loader;
        public System.Windows.Media.ImageSource Loader
        {
            get
            {
                return _Loader;
            }
            set
            {
                if (value == _Loader)
                    return;
                _Loader = value;
                OnPropertyChanged("Loader");
            }
        }
        
        private bool _IsNoContactFound = false;
        public bool IsNoContactFound
        {
            get { return _IsNoContactFound; }
            set
            {
                _IsNoContactFound = value;
                OnPropertyChanged("IsNoContactFound");
            }
        }

        /*private ImageSource _LoaderSmall;
        public ImageSource LoaderSmall
        {
            get
            {
                return _LoaderSmall;
            }
            set
            {
                if (value == _LoaderSmall)
                    return;
                _LoaderSmall = value;
                OnPropertyChanged("LoaderSmall");
            }
        }*/
        
        private int _TotalFriend = 0;
        public int TotalFriend
        {
            get { return _TotalFriend; }
            set
            {
                _TotalFriend = value;
                OnPropertyChanged("TotalFriend");
            }
        }
        
        private int _BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
        public int BOTTOM_LOADING
        {
            get { return _BOTTOM_LOADING; }
            set
            {
                _BOTTOM_LOADING = value;
                this.OnPropertyChanged("BOTTOM_LOADING");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Event Handler"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            cancel.Click += cancel_Click;
            TabList.SelectionChanged += TabControl_SelectionChanged;
            //textBoxSearch.TextChanged += SearchTextBox_TextChanged;
            listTF.PreviewMouseWheel += list_PreviewMouseWheel;
            SearchlistBox.PreviewMouseWheel += list_PreviewMouseWheel;
            listTF.MouseLeftButtonUp += list_MouseLeftButtonUp;
            SearchlistBox.MouseLeftButtonUp += list_MouseLeftButtonUp;
            listTF.Visibility = Visibility.Visible;
            SearchlistBox.Visibility = Visibility.Collapsed;
            TabControl_SelectionChanged(null, null);
            LoadFriend();
            FriendListController.Instance.FriendDataContainer.TotalFriends();
            SearchTextBoxTimer = new Timer();
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            cancel.Click -= cancel_Click;
            TabList.SelectionChanged -= TabControl_SelectionChanged;
            //textBoxSearch.TextChanged -= SearchTextBox_TextChanged;
            listTF.PreviewMouseWheel -= list_PreviewMouseWheel;
            SearchlistBox.PreviewMouseWheel -= list_PreviewMouseWheel;
            listTF.MouseLeftButtonUp -= list_MouseLeftButtonUp;
            SearchlistBox.MouseLeftButtonUp -= list_MouseLeftButtonUp;
            textBoxSearch.Text = string.Empty;
            listTF.Visibility = Visibility.Visible;
            SearchlistBox.Visibility = Visibility.Collapsed;
            ThrdSearchFrndInMyProfile.runningSearchFriendThread = false;
            RemoveFrienList();
            BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Check_Search();
        }

        private void BtnMutualFrnd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
                if (model.NumberOfMutualFriends > 0)
                {
                    //Button button = (Button)sender;
                    //UIElement uiel = (UIElement)button;
                    if (UCMutualFriendsPopup.Instance != null)
                        mainPanel.Children.Remove(UCMutualFriendsPopup.Instance);
                    UCMutualFriendsPopup.Instance = new UCMutualFriendsPopup();
                    UCMutualFriendsPopup.Instance.SetParent(mainPanel);
                    UCMutualFriendsPopup.Instance.Show();
                    UCMutualFriendsPopup.Instance.ShowPopup(control, model.ShortInfoModel.UserTableID, model.ShortInfoModel.FullName);
                    UCMutualFriendsPopup.Instance.OnRemovedUserControl += () =>
                    {
                        UCMutualFriendsPopup.Instance.MutualList.Clear();
                        UCMutualFriendsPopup.Instance = null;
                    };
                    //   new MutualFriendsRequest(model.UserIdentity);
                    SendDataToServer.SendMutualFreindRequest(model.ShortInfoModel.UserTableID);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Button_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void btnExpandFrnd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button control = (Button)sender;
                UserBasicInfoModel _model = (UserBasicInfoModel)control.DataContext;
                ListBox listObj = (ListBox)this.FindName(control.Tag.ToString());
                listObj.SelectedItem = _model;
                list_MouseLeftButtonUp(listObj, null);
            }
            catch (Exception ex)
            {
                log.Error("Error: Button_Click_1() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //SearchListInUserProfile.Clear();
            Check_Search();
        }

        private void list_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                ListBox listbx = (ListBox)control;
                //ListBoxItem itm = (ListBoxItem)listbx.SelectedItem;
                if (listbx.SelectedItem != null)
                {
                    object selectedItem = listbx.SelectedItem;
                    ListBoxItem selectedListBoxItem = listbx.ItemContainerGenerator.ContainerFromItem(selectedItem) as ListBoxItem;
                    if (selectedListBoxItem == prevItem)
                    {
                        selectedListBoxItem.IsSelected = (!selectedListBoxItem.IsSelected);
                        prevItem = null;
                    }
                    else
                    {
                        prevItem = selectedListBoxItem;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: list_MouseLeftButtonUp() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void list_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: list_PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        void SearchTextBoxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                if (string.IsNullOrEmpty(ThrdSearchFrndInMyProfile.prevString)
                    || (!string.IsNullOrEmpty(ThrdSearchFrndInMyProfile.prevString) && !ThrdSearchFrndInMyProfile.prevString.Trim().Equals(SearchString.Trim())))
                {
                    ThrdSearchFrndInMyProfile.searchParm = SearchString.ToLower();
                    if (!ThrdSearchFrndInMyProfile.runningSearchFriendThread)
                    {
                        new ThrdSearchFrndInMyProfile().StartThread();
                    }
                }
            }
            else
            {
                ClearSearchFriendList();
            }
            ThreadMaxRunningTime += 300;
            if (ThreadMaxRunningTime >= 1200)
            {
                ThreadMaxRunningTime = 0;
                SearchTextBoxTimer.Enabled = false;
                SearchTextBoxTimer.Stop();
            }
        }

        private void showMorePanel_Click(object sender, MouseButtonEventArgs e)
        {
            if (BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                LoadMoreFriends();
            }
            e.Handled = true;
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            textBoxSearch.Text = string.Empty;
        }
        #endregion

        #region "Private Method"
        private void LoadFriend()
        {
            //new System.Threading.Thread(() =>
            //{
                foreach (var model in RingIDViewModel.Instance.FavouriteFriendList)
                {
                    ListContact.Add(model);
                }
                foreach (var model in RingIDViewModel.Instance.TopFriendList)
                {
                    ListContact.Add(model);
                }
                foreach (UserBasicInfoDTO userDTO in FriendListController.Instance.FriendDataContainer.NonFavoriteList)
                {
                    UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                    ListContact.Add(model);
                }
                ListContact = ListContact.OrderBy(p => p.ShortInfoModel.FullName).ToList();
                LoadTenFriend();
            //}).Start();            
        }

        private void LoadTenFriend()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                int i = 0;
                int start = FriendListInUserProfile.Count - 1;
                while (i < DefaultSettings.CONTENT_SHOW_LIMIT && (FriendListInUserProfile.Count - 1) < ListContact.Count)
                {
                    FriendListInUserProfile.Insert(FriendListInUserProfile.Count - 1, ListContact[start + i]);
                    i++;
                }
                if (ListContact.Count > DefaultSettings.CONTENT_SHOW_LIMIT && ListContact.Count > (FriendListInUserProfile.Count - 1))
                {
                    ShowLoader(false);
                }
                else
                {
                    HideShowMorePanel();
                }
            }, DispatcherPriority.Send);
        }
        
        private void RemoveFrienList()
        {
            DispatcherTimer _RemoveMembers = new DispatcherTimer();
            _RemoveMembers.Interval = TimeSpan.FromMilliseconds(100);
            _RemoveMembers.Tick += (s, e) =>
            {
                lock (FriendListInUserProfile)
                {
                    while (FriendListInUserProfile.Count > 1)
                    {
                        UserBasicInfoModel model = FriendListInUserProfile.FirstOrDefault();
                        if (model.ShortInfoModel.UserTableID != 0)
                        {
                            FriendListInUserProfile.Remove(model);
                            GC.SuppressFinalize(model);
                        }
                    }
                }
                lock (SearchListInUserProfile)
                {
                    while (SearchListInUserProfile.Count > 0)
                    {
                        UserBasicInfoModel model = SearchListInUserProfile.FirstOrDefault();
                        SearchListInUserProfile.Remove(model);
                        GC.SuppressFinalize(model);
                    }
                }
                ListContact.Clear();
                _RemoveMembers.Stop();
            };
            _RemoveMembers.Stop();
            _RemoveMembers.Start();
        }

        private void OnSearch()
        {
            try
            {
                if (string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString) && !ThreadLeftSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    //ClearTempList();
                    ThreadMaxRunningTime = 0;
                    SearchTextBoxTimer.Elapsed -= SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Elapsed += SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Interval = 300;
                    SearchTextBoxTimer.AutoReset = true;
                    SearchTextBoxTimer.Enabled = true;
                    SearchTextBoxTimer.Stop();
                    SearchTextBoxTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + " " + ex.Message);
            }
        }
        #endregion

        #region "Public Method"
        public void ClearSearchFriendList()
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                SearchListInUserProfile.Clear();
            }, DispatcherPriority.Send);
        }

        public void Check_Search()
        {
            //IsNoContactFound = false;
            ///*LoaderSmall = ImageUtility.GetBitmapImage(View.Constants.ImageLocation.LOADER_SMALL);
            //if (LoaderSmall != null)
            //{
            //    GC.SuppressFinalize(LoaderSmall);
            //}*/
            //ThrdSearchFrndInMyProfile.searchParm = textBoxSearch.Text;

            //if (!string.IsNullOrWhiteSpace(ThrdSearchFrndInMyProfile.searchParm) && ThrdSearchFrndInMyProfile.searchParm.Trim().Length > 0)
            //{
            //    SearchlistBox.Visibility = Visibility.Visible;
            //    listTF.Visibility = Visibility.Collapsed;
            //    HideShowMorePanel();
            //}
            //else
            //{
            //    SearchlistBox.Visibility = Visibility.Collapsed;
            //    listTF.Visibility = Visibility.Visible;
            //    if (FriendListInUserProfile.Count < (RingIDViewModel.Instance.FavouriteFriendList.Count + RingIDViewModel.Instance.TopFriendList.Count + RingIDViewModel.Instance.NonFavouriteFriendList.Count))
            //    {
            //        ShowLoader(false);
            //    }
            //}
            //if (!ThrdSearchFrndInMyProfile.runningSearchFriendThread
            //    || (!string.IsNullOrEmpty(ThrdSearchFrndInMyProfile.searchParm) && string.IsNullOrEmpty(ThrdSearchFrndInMyProfile.prevString)) && !ThrdSearchFrndInMyProfile.searchParm.Equals(ThrdSearchFrndInMyProfile.prevString))
            //{
            //    new ThrdSearchFrndInMyProfile().StartThread();
            //}
        }

        public UserBasicInfoModel GetSingleFriendInUserProfile(long utId)
        {
            return FriendListInUserProfile.Where(P => P.ShortInfoModel.UserTableID == utId).FirstOrDefault();
        }

        public void DeleteFromFriendListInMyProfile(long utId)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                FriendListInUserProfile.Remove(GetSingleFriendInUserProfile(utId));
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public void ShowLoader(bool show)
        {
            if (show)
            {
                /*showMorePanel.Visibility = Visibility.Visible;
                showMoreLoaderBorder.Visibility = Visibility.Visible;
                showMoreButtonPanel.Visibility = Visibility.Collapsed;*/
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
                Loader = ImageObjects.LOADER_FEED;
            }
            else
            {
                /*showMorePanel.Visibility = Visibility.Visible;
                showMoreLoaderBorder.Visibility = Visibility.Collapsed;
                showMoreButtonPanel.Visibility = Visibility.Visible;*/
                BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                if (ImageObjects.LOADER_FEED != null)
                    GC.SuppressFinalize(ImageObjects.LOADER_FEED);
                if (Loader != null)
                    GC.SuppressFinalize(Loader);
                ImageObjects.LOADER_FEED = null;
                Loader = null;
            }
        }

        public void HideShowMorePanel()
        {
            BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
            //showMorePanel.Visibility = Visibility.Collapsed;
        }

        public void LoadMoreFriends()
        {
            ShowLoader(true);
            LoadTenFriend();
            //UCMiddlePanelSwitcher.View_UCMyProfile._MyProfileFriendListLoadUtility.AddIntoFriendlist(FriendListInUserProfile.Count);
        }
        #endregion
    }
}
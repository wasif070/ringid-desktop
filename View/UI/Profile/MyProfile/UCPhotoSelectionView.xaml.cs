﻿using log4net;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.Constants;
using View.UI.Feed;
using View.UI.PopUp;
using View.Utility;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for PhotoSelectionView.xaml
    /// </summary>
    public partial class UCPhotoSelectionView : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCPhotoSelectionView).Name);
        public NewStatusViewModel newStatusViewModel = null;

        public UCPhotoSelectionView(Grid motherGrid)
        {
            InitializeComponent();
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, ViewConstants.POP_UP_BG_BORDER2, 1.0);
            SetParent(motherGrid);
            this.DataContext = this;
            this.Loaded += UserControlLoaded;
        }

        #region EventHandler
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
        }
        #endregion

        #region Utility
        public void SetPhotoType(int type, NewStatusViewModel newStatusViewModel = null)
        {
            try
            {                
                this.newStatusViewModel = newStatusViewModel;
                TYPE = type;
                SetTypeOfChange();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnCloseCommand()
        {
            try
            {
                base.Hide();
                if (TYPE == MiddlePanelConstants.TypeMyProfilePictureChange || TYPE == MiddlePanelConstants.TypeMyCoverPictureChange)
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile.MyProfileCoverPhotosSelection.RemoveProfileCoverFeedPhotos();
                    UCMiddlePanelSwitcher.View_UCMyProfile.MyProfileCoverPhotosSelection.TabControl.SelectedIndex = 3;
                }
                mainGrid.Child = null;
                if (newStatusViewModel != null)
                {
                    newStatusViewModel.SetStatusImagePanelVisibility();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public void SetTypeOfChange()
        {
            try
            {
                if (TYPE == MiddlePanelConstants.TypeMyProfilePictureChange || TYPE == MiddlePanelConstants.TypeMyCoverPictureChange)
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile.MyProfileCoverPhotosSelection.TYPE = TYPE;
                    UCMiddlePanelSwitcher.View_UCMyProfile.MyProfileCoverPhotosSelection.AddProfileCoverFeedPhotos();
                    mainGrid.Child = UCMiddlePanelSwitcher.View_UCMyProfile.MyProfileCoverPhotosSelection;
                }
                else if (TYPE == MiddlePanelConstants.TypeMyAlbum)
                {
                    mainGrid.Child = RingIDViewModel.Instance.MyAlbums;
                    RingIDViewModel.Instance.MyAlbums.LoadAllImages(newStatusViewModel);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property

        private int type = 0;
        public int TYPE
        {
            get { return type; }
            set
            {
                if (value == type) return;
                type = value;
                OnPropertyChanged("TYPE");
            }
        }

        #endregion

        #region Command

        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(param => OnCloseCommand());
                }
                return _CloseCommand;
            }
        }

        #endregion
    }
}

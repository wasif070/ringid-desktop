﻿using Auth.Service.Images;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.ViewModel;
using View.Utility;
using View.Utility.Auth;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMyProfilePhotos.xaml
    /// </summary>
    public partial class UCMyProfilePhotos : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyProfilePhotos).Name);
        //public static UCMyProfilePhotos Instance;

        #region "Constructor"
        public UCMyProfilePhotos()
        {
            InitializeComponent();
            this.DataContext = this;
            //SendDataToServer.ListOfImageAlbumsOfUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_IMAGE, RingIDViewModel.Instance.ImageAlbumNpUUId);
            //LoadMyImages(DefaultSettings.FEED_IMAGE_ALBUM_ID);
            //LoadMyImages(DefaultSettings.PROFILE_IMAGE_ALBUM_ID);
            //LoadMyImages(DefaultSettings.COVER_IMAGE_ALBUM_ID);            
        }
        #endregion

        #region "Event Handler"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
           
        }

        //DispatcherTimer _RemovePhotos = null;
        private void UserControlUnloaded(object sender, RoutedEventArgs args)
        {
            //TabList.SelectionChanged -= TabControl_SelectionChanged;

            /*if(_UCMyProfilePhotosAlbum != null)
            {
                _UCMyProfilePhotosAlbum.ProfileImageList = null;
            }
            if (_UCMyCoverPhotosAlbum != null)
            {
                _UCMyCoverPhotosAlbum.CoverImageList = null;
            }
            if (_UCMyFeedPhotosAlbum != null)
            {
                _UCMyFeedPhotosAlbum.FeedImageList = null;
            }*/
            /*if (_RemovePhotos == null)
            {
                _RemovePhotos = new DispatcherTimer();
                _RemovePhotos.Interval = TimeSpan.FromSeconds(1);
                _RemovePhotos.Tick += (s, e) =>
                {
                    if (IsLoaded == false)
                    {
                        UnloadModel();
                    }
                    _RemovePhotos.Stop();
                };
            }
            _RemovePhotos.Stop();
            _RemovePhotos.Start();*/
        }

        private void UnloadModel()
        {
            lock (RingIDViewModel.Instance.ProfileImageList)
            {
                while (RingIDViewModel.Instance.ProfileImageList.Count > 1)
                {
                    ImageModel model = RingIDViewModel.Instance.ProfileImageList.FirstOrDefault();
                    if (!model.IsLoadMore)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            RingIDViewModel.Instance.ProfileImageList.Remove(model);
                            GC.SuppressFinalize(model);
                        }, DispatcherPriority.Send);
                    }
                }
            }

            lock (RingIDViewModel.Instance.CoverImageList)
            {
                while (RingIDViewModel.Instance.CoverImageList.Count > 1)
                {
                    ImageModel model = RingIDViewModel.Instance.CoverImageList.FirstOrDefault();
                    if (!model.IsLoadMore)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            RingIDViewModel.Instance.CoverImageList.Remove(model);
                            GC.SuppressFinalize(model);
                        }, DispatcherPriority.Send);
                    }
                }
            }

            lock (RingIDViewModel.Instance.FeedImageList)
            {
                while (RingIDViewModel.Instance.FeedImageList.Count > 1)
                {
                    ImageModel model = RingIDViewModel.Instance.FeedImageList.FirstOrDefault();
                    if (!model.IsLoadMore)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            RingIDViewModel.Instance.FeedImageList.Remove(model);
                            GC.SuppressFinalize(model);
                        }, DispatcherPriority.Send);
                    }
                }
            }
        }
        
        #endregion

        #region "Private Method"
        private void LoadMyImages(string AlbumId)
        {
            try
            {
                if ((AlbumId == DefaultSettings.PROFILE_IMAGE_ALBUM_ID && RingIDViewModel.Instance.ProfileImageList.Count == 1)
                    || (AlbumId == DefaultSettings.COVER_IMAGE_ALBUM_ID && RingIDViewModel.Instance.CoverImageList.Count == 1)
                    || (AlbumId == DefaultSettings.FEED_IMAGE_ALBUM_ID && RingIDViewModel.Instance.FeedImageList.Count == 1))
                {
                    //SendDataToServer.SendAlbumRequest(0, AlbumId, 0);//to do
                }                
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        private ICommand _ImageAlbumCommand;
        public ICommand ImageAlbumCommand
        {
            get
            {
                if (_ImageAlbumCommand == null)
                {
                    _ImageAlbumCommand = new RelayCommand(param => OnAlbumClick(param));
                }
                return _ImageAlbumCommand;
            }
        }

        private void OnAlbumClick(object param)
        {
            try
            {
                AlbumModel model = (AlbumModel)param;
                ThumbPanel.Visibility = Visibility.Collapsed;
                if (UCImageContentView.Instance == null) UCImageContentView.Instance = new UCImageContentView();
                else if (UCImageContentView.Instance.Parent is Border) ((Border)UCImageContentView.Instance.Parent).Child = null;
                ImageAlbumsDetailsPanel.Child = UCImageContentView.Instance;
                UCImageContentView.Instance.SetValue(model, () =>
                {
                    ThumbPanel.Visibility = Visibility.Visible;
                    ImageAlbumsDetailsPanel.Child = null;
                    return 0;
                });                
            }
            catch (Exception)
            {
            }
        }
        private int _BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
        public int BOTTOM_LOADING
        {
            get { return _BOTTOM_LOADING; }
            set
            {
                _BOTTOM_LOADING = value;
                this.OnPropertyChanged("BOTTOM_LOADING");
            }
        }
        private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        {
            if (BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                LoadMorePhotos();
            }
            e.Handled = true;
        }
        public void ShowHideShowMoreLoading(bool makeVisible)
        {
            try
            {
                if (makeVisible)
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                }
                else
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void LoadMorePhotos()
        {
            try
            {
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
                /*List<ImageModel> tmpList = (from l in albumModel.ImageList where l.ImageId != Guid.Empty select l).ToList();
                SendDataToServer.SendAlbumRequest(0, albumId, tmpList.Min(P => P.ImageId), 10);*/
                SendDataToServer.ListOfImageAlbumsOfUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_IMAGE, RingIDViewModel.Instance.ImageAlbumNpUUId);
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadMorePhotos() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

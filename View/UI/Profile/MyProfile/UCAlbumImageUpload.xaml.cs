﻿using Auth.Service.Images;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.GIF;
using View.Utility.WPFMessageBox;
using Auth.utility;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Threading.Tasks;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCAlbumImageUpload.xaml
    /// </summary>
    public partial class UCAlbumImageUpload : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCAlbumImageUpload).Name);

        #region "Private Member"
        private UIElement _parent;
        private BitmapImage bitmapImage;
        private Image uiImage;
        private double rectPositionX;
        private double rectPositionY;
        private double imageWidth;
        private double imageHeight;
        private double imageWidthOriginal;
        private double imageHeightOriginal;
        private string fileName;
        private string convertedUrl;
        private byte[] postDataOriginal;
        private byte[] postData;
        private string uploadedImgUrl;
        private MediaContentModel albumModel;
        #endregion

        #region "Public Member"
        #endregion

        #region "Property"
        private ImageSource _pstLoader;
        public ImageSource pstLoader
        {
            get
            {
                return _pstLoader;
            }
            set
            {
                if (value == _pstLoader)
                    return;
                _pstLoader = value;
                OnPropertyChanged("pstLoader");
            }
        }
        #endregion

        #region "Constructor"
        public UCAlbumImageUpload()
        {
            InitializeComponent();
            this.DataContext = this;
            Visibility = Visibility.Hidden;
            Focusable = false;
            CalculateRectangleXYPositon();
        }
        #endregion

        #region "Command"
        #endregion

        #region "Event Handler"
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            HideHandlerDialog();
        }

        private void btnSavePicture_Click(object sender, RoutedEventArgs e)
        {
            btnSavePicture.IsEnabled = false;
            btnCancel.IsEnabled = false;
            this.Loading_upload.Visibility = Visibility.Visible;
            pstLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_CLOCK);
            AlbumCoverImageUpload();
        }

        private void root_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            btnSavePicture.Click -= btnSavePicture_Click;
            btnCancel.Click -= btnCancel_Click;
            if ((bool)e.NewValue)
            {
                btnSavePicture.Click += btnSavePicture_Click;
                btnCancel.Click += btnCancel_Click;
            }
        }
        #endregion

        #region "Private Method"
        private async void AlbumCoverImageUpload()
        {
            bool success = false;
            try
            {
                byte[] response = await FeedImagesUpload.Instance.UploadThumbImageToImageServerWebRequest(postData, (int)imageWidth, (int)imageHeight);
                if (response != null && response[0] == 1)
                {
                    int len = response[1];
                    success = true;
                    uploadedImgUrl = Encoding.UTF8.GetString(response, 2, len);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                ToAuthServerandChangeImage(success);
            }
        }
        private void ToAuthServerandChangeImage(bool uploaded)
        {
            if (uploaded)
            {
                MainSwitcher.ThreadManager().EditMediaOrAlbumInfo.callBackEvent += (response) =>
                {
                    if (response == SettingsConstants.RESPONSE_SUCCESS)
                    {
                        albumModel.AlbumImageUrl = uploadedImgUrl;
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            btnSavePicture.IsEnabled = true;
                            btnCancel.IsEnabled = true;
                            this.Loading_upload.Visibility = Visibility.Collapsed;
                            pstLoader = null;
                            HideHandlerDialog();
                            albumModel.OnPropertyChanged("CurrentInstance");
                        });
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            btnSavePicture.IsEnabled = true;
                            btnCancel.IsEnabled = true;
                            this.Loading_upload.Visibility = Visibility.Collapsed;
                            pstLoader = null;
                            UIHelperMethods.ShowWarning(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Edit failed!");
                        });
                    }
                };
                MainSwitcher.ThreadManager().EditMediaOrAlbumInfo.StartThread(albumModel, albumModel.AlbumName, uploadedImgUrl, imageWidth, imageHeight);
            }
            else
            {
                btnSavePicture.IsEnabled = true;
                btnCancel.IsEnabled = true;
                this.Loading_upload.Visibility = Visibility.Collapsed;
                pstLoader = null;
                UIHelperMethods.ShowWarning(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Edit failed!");
            }
        }

        private void ResetAll()
        {
            try
            {
                this.imageWidth = 0;
                this.imageHeight = 0;
                imageWidthOriginal = 0;
                imageHeightOriginal = 0;
                this.bitmapImage = null;
                this.uiImage = null;
                this.fileName = null;
                this.convertedUrl = string.Empty;
                _Canvas.Children.Clear();
                this.btnSavePicture.IsEnabled = true;
                this.btnCancel.IsEnabled = true;
                this.Loading_upload.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CalculateRectangleXYPositon()
        {
            rectPositionX = (RingIDSettings.PROFILE_PIC_SCREEN_WIDTH - RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT) / 2;
            rectPositionY = (RingIDSettings.PROFILE_PIC_SCREEN_WIDTH - RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT) / 2;
        }

        private void SetAlbumImageToCanvas()
        {
            try
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    SetLoadingGifToCanvas();
                    this.btnSavePicture.IsEnabled = true;
                    new Thread(() =>
                    {
                        try
                        {
                            System.Drawing.Bitmap bitmap = (System.Drawing.Bitmap)ImageUtility.GetOrientedImageFromFilePath(fileName);//ImageUtility.GetBitmapOfDynamicResource(fileName);
                            if (bitmap.Width < RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH || bitmap.Height < RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH)
                            {
                                bitmap.Dispose();
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    HideHandlerDialog();
                                    UIHelperMethods.ShowFailed("Album image's minimum resulotion: " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH + " x " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH, "Album image");
                                }, System.Windows.Threading.DispatcherPriority.Send);
                            }
                            else
                            {
                                if (bitmap.Width >= RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH && bitmap.Height >= RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH)
                                    postDataOriginal = ImageUtility.ReduceQualityAndSize(bitmap, RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH, 85, SettingsConstants.TYPE_PROFILE_IMAGE);
                                else
                                    postDataOriginal = ImageUtility.ConvertBitmapToByteArray(bitmap);

                                BitmapImage bitmapImageOriginal = ImageUtility.ConvertByteArrayToBitmapImage(postDataOriginal);
                                imageWidthOriginal = bitmapImageOriginal.Width;
                                imageHeightOriginal = bitmapImageOriginal.Height;

                                int quality = 100;
                                postData = ImageUtility.ReduceQualityAndSize(bitmap, RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT, quality, SettingsConstants.TYPE_PROFILE_IMAGE);
                                bitmap.Dispose();
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    LoadImageToCanvas();
                                }, System.Windows.Threading.DispatcherPriority.Send);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                        }
                    }).Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==>  SetAlbumImageToCanvas " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LoadImageToCanvas()
        {
            try
            {
                bitmapImage = ImageUtility.ConvertByteArrayToBitmapImage(postData);
                uiImage = new Image { Source = bitmapImage };
                imageWidth = bitmapImage.Width;
                imageHeight = bitmapImage.Height;

                double left = (_Canvas.Width - imageWidth) / 2;
                Canvas.SetLeft(uiImage, left);

                double top = (_Canvas.Height - imageHeight) / 2;
                Canvas.SetTop(uiImage, top);

                _Canvas.Children.Clear();
                _Canvas.Children.Add(uiImage);
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetLoadingGifToCanvas()
        {
            try
            {
                BitmapImage image = ImageUtility.GetBitmapImage(ImageLocation.LOADER_CLOCK);
                uiImage = new Image { Source = image };
                ImageBehavior.SetAnimatedSource(uiImage, image);

                Canvas.SetLeft(uiImage, rectPositionX + (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT / 2) - (image.Width / 2));
                Canvas.SetTop(uiImage, rectPositionY + (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT / 2) - (image.Height) / 2);
                _Canvas.Children.Clear();
                _Canvas.Children.Add(uiImage);

                this.btnSavePicture.IsEnabled = true;
                this.btnCancel.IsEnabled = true;
                this.Loading_upload.Visibility = Visibility.Collapsed;
                GC.SuppressFinalize(image);
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Public Method"
        public void ShowHandlerDialog(string fileName, MediaContentModel albumModel)
        {
            try
            {
                Visibility = Visibility.Visible;
                Focusable = true;
                _parent.IsEnabled = true;
                ResetAll();
                this.fileName = fileName;
                this.albumModel = albumModel;
                SetAlbumImageToCanvas();
            }
            catch (Exception ex)
            {
                log.Error("Error:  SetUserModel ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideHandlerDialog()
        {
            Visibility = Visibility.Hidden;
            _parent.IsEnabled = true;
            Focusable = false;
        }

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Loaded_Unloaded Events"
        //private void UserControlLoaded(object sender, RoutedEventArgs e)
        //{
        //    btnSavePicture.Click -= btnSavePicture_Click;
        //    btnCancel.Click -= btnCancel_Click;
        //    btnSavePicture.Click += btnSavePicture_Click;
        //    btnCancel.Click += btnCancel_Click;
        //}

        //private void UserControlUnloaded(object sender, RoutedEventArgs e)
        //{
        //    btnSavePicture.Click -= btnSavePicture_Click;
        //    btnCancel.Click -= btnCancel_Click;
        //}
        #endregion
    }
}

﻿using log4net;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Myprofile;
using View.ViewModel;
using Models.Constants;
namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMyProfile.xaml
    /// </summary>
    public partial class UCMyProfile : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyProfile).Name);

        #region "Private Member"
        private UCMyProfileContentSwitcher _UCMPContentSwitcherInstance = null;
        #endregion

        #region "Public Member"
        public UCMyNewsFeeds _UCMyNewsFeeds;
        public UCMyProfilePhotos _UCMyProfilePhotos;
        public MyProfilePhotosChange _UCMyProfilePhotosChange = null;
        public UCMyProfileAbout _UCMyProfileAbout;
        public UCMyProfileCoverPhotosSelection _UCMyProfileCoverPhotosSelection = null;
        public UCMyProfileMediaAlbum _UCMyProfileMediaAlbum = null;
        public UCMyContactLists _UCMyContactLists;
        public MyProfileFriendListLoadUtility _MyProfileFriendListLoadUtility;
        #endregion

        #region "Property"
        public UCMyProfilePhotos MyProfilePhotos
        {
            get
            {
                if (_UCMyProfilePhotos == null)
                {
                    _UCMyProfilePhotos = new UCMyProfilePhotos();
                }
                return _UCMyProfilePhotos;
            }
        }

        public MyProfilePhotosChange MyProfilePhotosChange
        {
            get
            {
                if (_UCMyProfilePhotosChange == null)
                {
                    _UCMyProfilePhotosChange = new MyProfilePhotosChange();
                }
                return _UCMyProfilePhotosChange;
            }
        }

        public UCMyProfileCoverPhotosSelection MyProfileCoverPhotosSelection
        {
            get
            {
                if (_UCMyProfileCoverPhotosSelection == null)
                {
                    _UCMyProfileCoverPhotosSelection = new UCMyProfileCoverPhotosSelection();
                }
                return _UCMyProfileCoverPhotosSelection;
            }
        }

        private int _selectedBorder = 0;
        public int SelectedBorder
        {
            get { return _selectedBorder; }
            set
            {
                _selectedBorder = value;
                this.OnPropertyChanged("SelectedBorder");
            }
        }

        private bool _SettingsButtonFocusable = false;
        public bool SettingsButtonFocusable
        {
            get
            {
                return _SettingsButtonFocusable;
            }

            set
            {
                _SettingsButtonFocusable = value;
                OnPropertyChanged("SettingsButtonFocusable");
            }
        }

        private Guid _PreviewPhotoID = Guid.Empty;
        public Guid PreviewPhotoID
        {
            get
            {
                return _PreviewPhotoID;
            }
            set
            {
                if (_PreviewPhotoID == value)
                    return;
                _PreviewPhotoID = value;
                OnPropertyChanged("PreviewPhotoID");
            }
        }

        #endregion

        #region "Constructor"
        public UCMyProfile()
        {
            InitializeComponent();
            this.DataContext = this;
            _UCMPContentSwitcherInstance = new UCMyProfileContentSwitcher();
            gridContent.Children.Add(_UCMPContentSwitcherInstance);
            _UCMyProfilePhotos = new UCMyProfilePhotos();
        }
        #endregion

        #region "Command"
        private ICommand _ChangeProfilePictureCommand;
        public ICommand ChangeProfilePictureCommand
        {
            get
            {
                if (_ChangeProfilePictureCommand == null)
                {
                    _ChangeProfilePictureCommand = new RelayCommand(param => OnProfilePictureChangeCommand(param));
                }
                return _ChangeProfilePictureCommand;
            }
        }

        private ICommand _DeleteProfileCoverPictureCommand;
        public ICommand DeleteProfileCoverPictureCommand
        {
            get
            {
                if (_DeleteProfileCoverPictureCommand == null)
                {
                    _DeleteProfileCoverPictureCommand = new RelayCommand(param => OnProfileCoverPictureDeleteCommand(param));
                }
                return _DeleteProfileCoverPictureCommand;
            }
        }

        private ICommand _ChangeCoverPhotoCommand;
        public ICommand ChangeCoverPhotoCommand
        {
            get
            {
                if (_ChangeCoverPhotoCommand == null)
                {
                    _ChangeCoverPhotoCommand = new RelayCommand(param => OnChangeCoverPhotoCommand(param));
                }
                return _ChangeCoverPhotoCommand;
            }

        }

        private ICommand _MyNameClickCommand;
        public ICommand MyNameClickCommand
        {
            get
            {
                if (_MyNameClickCommand == null)
                {
                    _MyNameClickCommand = new RelayCommand(param => OnClickMyName());
                }
                return _MyNameClickCommand;
            }
        }

        private ICommand _AboutPanelClickCommand;
        public ICommand AboutPanelClickCommand
        {
            get
            {
                if (_AboutPanelClickCommand == null)
                {
                    _AboutPanelClickCommand = new RelayCommand(param => OnAboutPanelClick());
                }
                return _AboutPanelClickCommand;
            }
        }

        private ICommand _PhotosPanelClickCommand;
        public ICommand PhotosPanelClickCommand
        {
            get
            {
                if (_PhotosPanelClickCommand == null)
                {
                    _PhotosPanelClickCommand = new RelayCommand(param => OnPhotosPanelClick());
                }
                return _PhotosPanelClickCommand;
            }
        }

        private ICommand _FriendsPanelClickCommand;
        public ICommand FriendsPanelClickCommand
        {
            get
            {
                if (_FriendsPanelClickCommand == null)
                {
                    _FriendsPanelClickCommand = new RelayCommand(param => OnFriendsPanelClick());
                }
                return _FriendsPanelClickCommand;
            }
        }

        private ICommand _MVPanelClickCommand;
        public ICommand MVPanelClickCommand
        {
            get
            {
                if (_MVPanelClickCommand == null)
                {
                    _MVPanelClickCommand = new RelayCommand(param => OnMVPanelClick());
                }
                return _MVPanelClickCommand;
            }
        }

        private ICommand _BackBtnClickCommand;
        public ICommand BackBtnClickCommand
        {
            get
            {
                if (_BackBtnClickCommand == null)
                {
                    _BackBtnClickCommand = new RelayCommand(param => OnBackBtnClick());
                }
                return _BackBtnClickCommand;
            }
        }
        #endregion

        #region "Event Handler"
        //private void txtBlockMyName_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyProfile);
        //}

        //private void txtBlockMyName_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyProfile);
        //}

        private void pnlInfoContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DrawShape(pnlInfoContainer.ActualWidth);
        }

        //private void AboutPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(1, -1, 0);
        //    ShowAbout();
        //}

        //private void PhotosPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(2, -1, 0);
        //    ShowPhotos();
        //}

        //private void FriendsPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(3, -1, 0);
        //    ShowFriends();
        //}

        //private void MVPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(4, -1, 0);
        //    ShowMusicVideos();
        //}

        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Control control = (Control)sender;
            if (sender != null && control is ContextMenu)
            {
                if (control.IsVisible)
                {
                    SettingsButtonFocusable = true;
                }
                else
                {
                    SettingsButtonFocusable = false;
                }
            }
        }

        public void myScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                //if (SelectedBorder == 0 && _UCMyNewsFeeds != null)
                //{
                //    var scrollViewer = (ScrollViewer)sender;
                //    if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                //    {
                //        scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                //    }
                //    else if (e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight && !_UCMyNewsFeeds.isBottomLoading && _UCMyNewsFeeds.BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
                //    {
                //        Task.Factory.StartNew(() =>
                //        {
                //            _UCMyNewsFeeds.BottomLoadMyNewsFeeds();
                //        });

                //    }
                //    else if (e.VerticalChange < 0 && e.VerticalOffset > 0 && _UCMyNewsFeeds.IsNewStatusVisible(myScrlViewer) && !_UCMyNewsFeeds.isTopLoading)
                //    {
                //        Task.Factory.StartNew(() =>
                //        {
                //            _UCMyNewsFeeds.TopLoadMyNewsFeeds(myScrlViewer);
                //        });
                //    }
                //    else if (e.VerticalChange < 0 && e.VerticalOffset == 0 && _UCMyNewsFeeds.IsNewStatusVisible(myScrlViewer) && !_UCMyNewsFeeds.isTopLoading)
                //    {
                //        Task.Factory.StartNew(() =>
                //        {
                //            _UCMyNewsFeeds.TopLoadMyNewsFeeds(myScrlViewer, true);
                //        });
                //    }
                //}
                //else 
                if (SelectedBorder == 2 && _UCMyProfilePhotos != null)
                {
                    var scrollViewer = (ScrollViewer)sender;
                    if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                    {
                        scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                    }
                    else if ((e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight))
                    {
                        if (Models.Constants.DefaultSettings.MY_IMAGE_ALBUMS_COUNT != (RingIDViewModel.Instance.MyImageAlubms.Count - 1) && (UCImageContentView.Instance == null || !UCImageContentView.Instance.IsVisible))
                        {
                            _UCMyProfilePhotos.LoadMorePhotos();
                        }
                        else if (UCImageContentView.Instance != null && UCImageContentView.Instance.IsVisible)
                        {
                            UCImageContentView.Instance.LoadMorePhotos();
                        }
                    }
                }
                else if (SelectedBorder == 3 && _UCMyContactLists != null)
                {
                    var scrollViewer = (ScrollViewer)sender;
                    if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                    {
                        scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                    }
                    else if ((e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight))
                    {
                        _UCMyContactLists.LoadMoreFriends();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: myScrlViewer_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //private void myScrlViewer_PreviewKeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Home)
        //    {
        //        myScrlViewer.ScrollToHome();
        //        e.Handled = true;
        //    }
        //    else if (e.Key == Key.End)
        //    {
        //        myScrlViewer.ScrollToEnd();
        //        e.Handled = true;
        //    }
        //    else if (e.Key == Key.Escape &&  MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView != null
        //        && !RingPlayerViewModel.Instance.SmallPlayerOpen)
        //    {
        //        //UCGuiRingID.Instance.ucBasicMediaViewWrapper.ucBasicMediaView.ExitSmallPlayer();
        //        RingPlayerViewModel.Instance.OnDoCancelRequested();//UCGuiRingID.Instance.ucBasicMediaViewWrapper.ucBasicMediaView.DoCancel();
        //    }
        //}

        //private void backBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    OnBackBtnClick();
        //}

        //private void PostTabClick(object sender, MouseButtonEventArgs e)
        //{
        //    if (_UCMyNewsFeeds == null)
        //    {
        //        _UCMyNewsFeeds = new UCMyNewsFeeds();
        //    }
        //    else
        //    {
        //        if (RingIDViewModel.Instance.MyNewsFeeds.Count < 3)
        //        {
        //            _UCMyNewsFeeds.LoadMyFeeds();
        //        }
        //    }
        //}        
        #endregion

        #region "Private Method"

        private int ProfileImageCheck()
        {
            try
            {
                if (!string.IsNullOrEmpty(ViewModel.RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage))
                {
                    //this.mnuItmAddProfilePhoto.Visibility = Visibility;
                    return 1;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ProfileImageCheck() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return 0;
        }

        private void OnProfileCoverPictureDeleteCommand(object parameter)
        {

            if (UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this photo"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete")))
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    try
                    {
                        bool isSuccess = false;
                        string msg = "";

                        int imageType = (int)parameter;

                        ThradProfileCoverImageRemove obj = new ThradProfileCoverImageRemove(imageType);
                        obj.Run(out isSuccess, out msg);

                        if (!isSuccess && !string.IsNullOrEmpty(msg))
                        {
                            UIHelperMethods.ShowErrorMessageBoxFromThread(msg, "Failed!");

                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error: OnProfileCoverPictureDeleteCommand() ==> " + ex.Message + "\n" + ex.StackTrace);
                    }
                });
            }
        }

        private void OnProfilePictureChangeCommand(object parameter)
        {
            MainSwitcher.PopupController.PhotoSelectionView.Show();
            MainSwitcher.PopupController.PhotoSelectionView.SetPhotoType(MiddlePanelConstants.TypeMyProfilePictureChange);
        }

        private void OnChangeCoverPhotoCommand(object parameter)
        {
            MainSwitcher.PopupController.PhotoSelectionView.Show();
            MainSwitcher.PopupController.PhotoSelectionView.SetPhotoType(MiddlePanelConstants.TypeMyCoverPictureChange);
        }

        private void DrawShape(double width)
        {
            try
            {
                RectangleGeometry recGeometry = new RectangleGeometry();
                recGeometry.Rect = new Rect(0, 0, width, 80);

                EllipseGeometry ellipseGeometry = new EllipseGeometry();
                ellipseGeometry.RadiusX = 53;
                ellipseGeometry.RadiusY = 53;
                ellipseGeometry.Center = new Point(-30, 39);

                CombinedGeometry combineGeometry = new CombinedGeometry();
                combineGeometry.GeometryCombineMode = GeometryCombineMode.Exclude;
                combineGeometry.Geometry1 = recGeometry;
                combineGeometry.Geometry2 = ellipseGeometry;

                pathInfoContainer.Data = combineGeometry;
            }
            catch (Exception ex)
            {
                log.Error("Error: DrawShape() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowAbout()
        {
            //myScrlViewer.ScrollChanged -= myScrlViewer.ScrollPositionChanged;

            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
            SelectedBorder = 1;
            if (_UCMyProfileAbout == null)
            {
                _UCMyProfileAbout = new UCMyProfileAbout();
            }
            _UCMPContentSwitcherInstance.Content = _UCMyProfileAbout;
        }

        private void RemoveFrienList()
        {
            DispatcherTimer _RemoveMembers = new DispatcherTimer();
            _RemoveMembers.Interval = TimeSpan.FromMilliseconds(200);
            _RemoveMembers.Tick += (s, e) =>
            {
                lock (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile)
                {
                    while (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Count > 0)
                    {
                        UserBasicInfoModel model = UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.FirstOrDefault();
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Remove(model);
                        GC.SuppressFinalize(model);
                    }
                }
                _RemoveMembers.Stop();
            };
            _RemoveMembers.Stop();
            _RemoveMembers.Start();
        }

        private void OnClickMyName()
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyProfile);
        }

        private void OnAboutPanelClick()
        {
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(1, -1, 0);
            ShowAbout();
        }

        private void OnPhotosPanelClick()
        {
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(2, -1, 0);
            ShowPhotos();
        }

        private void OnFriendsPanelClick()
        {
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(3, -1, 0);
            ShowFriends();
        }

        private void OnMVPanelClick()
        {
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(4, -1, 0);
            ShowMusicVideos();
        }

        private void OnBackBtnClick()
        {
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }
        #endregion

        #region "Public Method"
        //public void RemoveTopOrBottomModels()
        //{
        //    bool _TopLoad = (myScrlViewer.VerticalOffset == 0 || myScrlViewer.VerticalOffset <= (myScrlViewer.ScrollableHeight / 2));
        //    if (_TopLoad)
        //    {
        //        myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
        //        RingIDViewModel.Instance.MyNewsFeeds.RemoveBottomModels();
        //        myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
        //    }
        //    else
        //    {
        //        myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
        //        RingIDViewModel.Instance.MyNewsFeeds.RemoveTopModels();
        //        myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;

        //        Application.Current.Dispatcher.Invoke((Action)delegate
        //        {
        //            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
        //            if (myScrlViewer.ScrollableHeight > 50 && myScrlViewer.VerticalOffset == myScrlViewer.ScrollableHeight)
        //            {
        //                myScrlViewer.ScrollToVerticalOffset(myScrlViewer.ScrollableHeight - 50);
        //            }
        //            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;

        //        }, DispatcherPriority.Send);
        //    }
        //}

        public void ShowCoverPicChangePanel(string fileName)
        {
            try
            {
                DefaultPanel.Visibility = Visibility.Collapsed;
                CoverPicChangePanel.Visibility = Visibility.Visible;
                CoverPicChangePanel.Children.Add(UCMiddlePanelSwitcher.InstanceOfMyCoverPicture);
                UCMiddlePanelSwitcher.InstanceOfMyCoverPicture.ShowCoverPhotoHandler(fileName);
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowCoverPicChangePanel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowCoverPicChangePanel(ImageModel objImgModel)
        {
            try
            {
                DefaultPanel.Visibility = Visibility.Collapsed;
                CoverPicChangePanel.Visibility = Visibility.Visible;
                CoverPicChangePanel.Children.Add(UCMiddlePanelSwitcher.InstanceOfMyCoverPicture);
                UCMiddlePanelSwitcher.InstanceOfMyCoverPicture.ShowCoverPhotoHandler(objImgModel);
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowCoverPicChangePanel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideCoverPicChangePanel()
        {
            try
            {
                DefaultPanel.Visibility = Visibility.Visible;
                CoverPicChangePanel.Visibility = Visibility.Collapsed;
                CoverPicChangePanel.Children.Remove(UCMiddlePanelSwitcher.InstanceOfMyCoverPicture);
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowCoverPicChangePanel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowPost(object state)
        {
            int TabIdx = 0;
            try
            {
                TabIdx = Convert.ToInt32(state);
            }
            catch (Exception) { }
            SelectedBorder = 0;
            switch (TabIdx)
            {
                case 0:
                    myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;

                    //myScrlViewer.ScrollChanged -= myScrlViewer.ScrollPositionChanged;
                    //myScrlViewer.ScrollChanged += myScrlViewer.ScrollPositionChanged;
                    if (_UCMyNewsFeeds == null)
                    {
                        _UCMyNewsFeeds = new UCMyNewsFeeds(myScrlViewer);
                    }
                    else
                    {
                        myScrlViewer.ScrollToVerticalOffset(0);
                        myScrlViewer.UpdateLayout();
                    }
                    _UCMPContentSwitcherInstance.Content = _UCMyNewsFeeds;
                    break;
                case 1: //about
                    ShowAbout();
                    break;
                case 2://photos
                    ShowPhotos();
                    break;
                case 3://Friends
                    ShowFriends();
                    break;
                case 4://Musics
                    ShowMusicVideos();
                    break;
                default:
                    break;
            }
        }

        public void ShowPhotos()
        {
            //myScrlViewer.ScrollChanged -= myScrlViewer.ScrollPositionChanged;

            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
            SelectedBorder = 2;
            if (_UCMyProfilePhotos == null)
            {
                _UCMyProfilePhotos = new UCMyProfilePhotos();
            }
            _UCMPContentSwitcherInstance.Content = _UCMyProfilePhotos;
        }

        public void ShowFriends()
        {
            //myScrlViewer.ScrollChanged -= myScrlViewer.ScrollPositionChanged;

            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
            SelectedBorder = 3;
            if (_UCMyContactLists == null)
            {
                _UCMyContactLists = new UCMyContactLists();
                _MyProfileFriendListLoadUtility = new MyProfileFriendListLoadUtility();
            }
            //UCMiddlePanelSwitcher.View_UCMyProfile._MyProfileFriendListLoadUtility.AddIntoFriendlist(0);
            //_UCMyContactLists.Check_Search();
            _UCMPContentSwitcherInstance.Content = _UCMyContactLists;
        }

        public void ShowMusicVideos()
        {
            //myScrlViewer.ScrollChanged -= myScrlViewer.ScrollPositionChanged;

            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
            SelectedBorder = 4;
            // CustomMessageBox.ShowInformation("Coming soon...");
            if (_UCMyProfileMediaAlbum == null)
            {
                _UCMyProfileMediaAlbum = new UCMyProfileMediaAlbum();
            }
            _UCMPContentSwitcherInstance.Content = _UCMyProfileMediaAlbum;
        }

        public void ShowMyFriendPreviewImages()//for showing 4 images in friend preview panel
        {
            int count = 0;
            RingIDViewModel.Instance.TempMyFriendList.Clear();
            if (count < 4)
            {
                foreach (var model in RingIDViewModel.Instance.TopFriendList)
                {
                    if (!string.IsNullOrEmpty(model.ShortInfoModel.ProfileImage))
                    {
                        RingIDViewModel.Instance.TempMyFriendList.Add(model);
                        count++;
                        if (count == 4) break;
                    }
                }
            }
            if (count < 4)
            {
                foreach (var model in RingIDViewModel.Instance.FavouriteFriendList)
                {
                    if (!string.IsNullOrEmpty(model.ShortInfoModel.ProfileImage))
                    {
                        RingIDViewModel.Instance.TempMyFriendList.Add(model);
                        count++;
                        if (count == 4) break;
                    }
                }
            }
            if (count < 4)
            {
                foreach (var model in RingIDViewModel.Instance.NonFavouriteFriendList)
                {
                    if (!string.IsNullOrEmpty(model.ShortInfoModel.ProfileImage))
                    {
                        RingIDViewModel.Instance.TempMyFriendList.Add(model);
                        count++;
                        if (count == 4) break;
                    }
                }
            }
            if (count > 0)
            {
                for (int i = count; i < 4; i++)
                {
                    RingIDViewModel.Instance.TempMyFriendList.Add(new UserBasicInfoModel());
                }
                DownLoadedFriendPanel.Visibility = Visibility.Visible;
            }
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Loaded_Unloaded Events"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            //myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
            //myScrlViewer.PreviewKeyDown += myScrlViewer_PreviewKeyDown;
            //txtBlockMyName.MouseLeftButtonDown += txtBlockMyName_MouseLeftButtonDown;
            //txtBlockMyName.PreviewMouseDown += txtBlockMyName_PreviewMouseDown;
            SettingBtn.ContextMenu.IsVisibleChanged += ContextMenu_IsVisibleChanged;
            /*AboutPanel.MouseLeftButtonDown += AboutPanel_MouseLeftButtonDown;
            PhotosPanel.MouseLeftButtonDown += PhotosPanel_MouseLeftButtonDown;
            FriendsPanel.MouseLeftButtonDown += FriendsPanel_MouseLeftButtonDown;
            MVPanel.MouseLeftButtonDown += MVPanel_MouseLeftButtonDown;
            backBtn.Click += backBtn_Click;*/
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            //myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
            //myScrlViewer.PreviewKeyDown -= myScrlViewer_PreviewKeyDown;
            //txtBlockMyName.MouseLeftButtonDown -= txtBlockMyName_MouseLeftButtonDown;
            // txtBlockMyName.PreviewMouseDown -= txtBlockMyName_PreviewMouseDown;
            SettingBtn.ContextMenu.IsVisibleChanged -= ContextMenu_IsVisibleChanged;
            /*AboutPanel.MouseLeftButtonDown -= AboutPanel_MouseLeftButtonDown;
            PhotosPanel.MouseLeftButtonDown -= PhotosPanel_MouseLeftButtonDown;
            FriendsPanel.MouseLeftButtonDown -= FriendsPanel_MouseLeftButtonDown;
            MVPanel.MouseLeftButtonDown -= MVPanel_MouseLeftButtonDown;
            backBtn.Click -= backBtn_Click;*/
        }
        #endregion

    }
}

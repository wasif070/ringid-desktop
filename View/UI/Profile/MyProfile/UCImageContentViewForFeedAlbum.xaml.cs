﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;
using View.ViewModel;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCImageContentViewForFeedAlbum.xaml
    /// </summary>
    public partial class UCImageContentViewForFeedAlbum : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCImageContentViewForFeedAlbum).Name);

        public static UCImageContentViewForFeedAlbum Instance = null;

        public UCImageContentViewForFeedAlbum()
        {
            InitializeComponent();
            this.DataContext = this;
            Instance = this;
        }

        #region "Property"
        private int _BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
        public int BOTTOM_LOADING
        {
            get { return _BOTTOM_LOADING; }
            set
            {
                _BOTTOM_LOADING = value;
                this.OnPropertyChanged("BOTTOM_LOADING");
            }
        }

        private ObservableCollection<ImageModel> _ImageList;
        public ObservableCollection<ImageModel> ImageList
        {
            get
            {
                return _ImageList;
            }
            set
            {
                _ImageList = value;
                this.OnPropertyChanged("ImageList");
            }
        }
        #endregion
        
        #region "Command"
        private ICommand _ImageClickCommand;
        public ICommand ImageClickCommand
        {
            get
            {
                if (_ImageClickCommand == null)
                {
                    _ImageClickCommand = new RelayCommand(param => OnImageClick(param));
                }
                return _ImageClickCommand;
            }
        }
        #endregion

        private void OnImageClick(object param)
        {
            if(RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
            {
                RingIDViewModel.Instance.MyAlbums.OnImageClick(param);
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        {
            if (BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                LoadMorePhotos();
            }
            e.Handled = true;
        }

        #region "Public Method"
        public void ShowHideShowMoreLoading(bool makeVisible)
        {
            try
            {
                if (makeVisible)
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                }
                else
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void LoadMorePhotos()
        {
            try
            {
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
                List<ImageModel> tmpList = (from l in albumModel.ImageList where l.ImageId != Guid.Empty select l).ToList();
                if (albumModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                    SendDataToServer.SendAlbumRequest(0, albumId, tmpList.Min(P => P.ImageId), 10);
                else
                    SendDataToServer.SendAlbumRequest(albumModel.UserTableID, albumId, tmpList.Min(P => P.ImageId), 10);
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadMorePhotos() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        Guid albumId = Guid.Empty;
        public AlbumModel albumModel;
        public void SetValue(AlbumModel model, Func<int> _OnBackToPrevious)
        {
            this._OnBackToPrevious = _OnBackToPrevious;
            titleTxt.Text = model.AlbumName + " (" + model.TotalImagesInAlbum + ")";
            albumModel = model;
            albumId = model.AlbumId;
            ImageList = model.ImageList;
            if (albumModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                SendDataToServer.SendAlbumRequest(0, albumId, Guid.Empty, 10);
            else
                SendDataToServer.SendAlbumRequest(albumModel.UserTableID, albumId, Guid.Empty, 10);
        }

        public Func<int> _OnBackToPrevious = null;
        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_OnBackToPrevious != null)
                {
                    _OnBackToPrevious();
                }
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }
    }
}

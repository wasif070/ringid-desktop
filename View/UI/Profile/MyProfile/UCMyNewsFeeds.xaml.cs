﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.UI.Feed;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMyNewsFeeds.xaml
    /// </summary>
    public partial class UCMyNewsFeeds : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyNewsFeeds).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public static UCMyNewsFeeds Instance;
        public NewStatusView NewStatusView = null;
        public NewStatusViewModel NewStatusViewModel = null;
        public CustomFeedScroll scroll;
        public UCMyNewsFeeds(CustomFeedScroll scroll)
        {
            Instance = this;
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;

            // this.scroll.ScrollChanged += (s, e) => { scroll.OnPreviewMouseWheelScrolled(e.VerticalChange); e.Handled = true; };//this.PreviewMouseWheel += delegate(object sender, MouseWheelEventArgs e) { };
            this.IsVisibleChanged += (s, e) => { scroll.OnIsVisibleChanged((bool)e.NewValue); };

            ViewCollection = RingIDViewModel.Instance.ProfileMyCustomFeeds;
            scroll.SetScrollValues(feeditemControls, ViewCollection, FeedDataContainer.Instance.ProfileMyCurrentIds, FeedDataContainer.Instance.ProfileMyTopIds, FeedDataContainer.Instance.ProfileMyBottomIds, SettingsConstants.PROFILE_TYPE_GENERAL, AppConstants.TYPE_MY_BOOK);
            DefaultSettings.PROFILEMY_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
            {
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.PROFILEMY_STARTPKT);
            }
        }

        private CustomFeedCollection _ViewCollection;
        public CustomFeedCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        private void newStatusViewInstance_Loaded(object sender, RoutedEventArgs e)
        {
            NewStatusView = (NewStatusView)sender;
            NewStatusViewModel = (NewStatusViewModel)NewStatusView.DataContext;
            NewStatusViewModel.WriteOnText = "Share your thoughts...";
            NewStatusViewModel.WritePostText = "Write Status";
            NewStatusViewModel.OptionsButtonVisibility = Visibility.Collapsed;
        }

        //private void UserControl_Loaded(object sender, RoutedEventArgs args)
        //{
        //    scroll.ScrollToHome();
        //    if (ViewCollection == null)
        //    {
        //        if (timer == null)
        //        {
        //            timer = new DispatcherTimer();
        //            timer.Interval = TimeSpan.FromMilliseconds(500);
        //            timer.Tick += TimerEventProcessor;
        //        }
        //        else timer.Stop();
        //        timer.Start();
        //    }
        //}
        //private void UserControl_Unloaded(object sender, RoutedEventArgs args)
        //{
        //    //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
        //    //{
        //        scroll.SetScrollEvents(false);
        //        ViewCollection = null;
        //    //}
        //}
        //private DispatcherTimer timer;
        //private void TimerEventProcessor(object sender, EventArgs e)
        //{
        //    if (timer != null && timer.IsEnabled)
        //    {
        //        ViewCollection = RingIDViewModel.Instance.MyNewsFeeds;
        //        scroll.SetScrollEvents(true);
        //        if (!UCGuiRingID.Instance.IsAnyWindowAbove())
        //            Keyboard.Focus(scroll);
        //        timer.Stop();
        //    }
        //}
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }
        private void OnReloadCommandClicked()
        {
            DefaultSettings.PROFILEMY_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.PROFILEMY_STARTPKT);
        }

        #region "Utility Methods"

        #endregion "Utility Methods"
    }
}

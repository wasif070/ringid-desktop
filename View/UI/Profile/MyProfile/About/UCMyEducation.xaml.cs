﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Auth;

namespace View.UI.Profile.MyProfile.About
{
    /// <summary>
    /// Interaction logic for UCMyEducation.xaml
    /// </summary>
    public partial class UCMyEducation : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyEducation).Name);

        #region "Private Member"
        private Dictionary<Guid, EducationDTO> myEducationsbyId;
        #endregion

        #region "Property"
        private SingleEducationModel _AddEduModel;
        public SingleEducationModel AddEduModel
        {
            get
            {
                return _AddEduModel;
            }
            set
            {
                _AddEduModel = value;
                this.OnPropertyChanged("AddEduModel");
            }
        }
        private Visibility _Absent;
        public Visibility Absent
        {
            get
            {
                return _Absent;
            }
            set
            {
                _Absent = value;
                this.OnPropertyChanged("Absent");
            }
        }
        #endregion

        #region "Constructor"
        public UCMyEducation()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Command"
        private ICommand _SaveNewEductionCommand;
        public ICommand SaveNewEductionCommand
        {
            get
            {
                if (_SaveNewEductionCommand == null)
                {
                    _SaveNewEductionCommand = new RelayCommand(param => SaveNewEduction());
                }
                return _SaveNewEductionCommand;
            }
        }

        private ICommand _CancelNewEducationCommand;
        public ICommand CancelNewEducationCommand
        {
            get
            {
                if (_CancelNewEducationCommand == null)
                {
                    _CancelNewEducationCommand = new RelayCommand(param => CancelNewEducation());
                }
                return _CancelNewEducationCommand;
            }
        }

        /*private ICommand _EditEductionCommand;
        public ICommand EditEductionCommand
        {
            get
            {
                if (_EditEductionCommand == null)
                {
                    _EditEductionCommand = new RelayCommand(param => MyEducationUtility.EditEducationClick(param));
                }
                return _EditEductionCommand;
            }
        }

        private ICommand _DeleteEductionCommand;
        public ICommand DeleteEductionCommand
        {
            get
            {
                if (_DeleteEductionCommand == null)
                {
                    _DeleteEductionCommand = new RelayCommand(param => OnDeleteEducation(param));
                }
                return _DeleteEductionCommand;
            }
        }*/

        private ICommand _SaveEditEductionCommand;
        public ICommand SaveEditEductionCommand
        {
            get
            {
                if (_SaveEditEductionCommand == null)
                {
                    _SaveEditEductionCommand = new RelayCommand(param => SaveEditEducation(param));
                }
                return _SaveEditEductionCommand;
            }
        }

        private ICommand _CancelEditEductionCommand;
        public ICommand CancelEditEductionCommand
        {
            get
            {
                if (_CancelEditEductionCommand == null)
                {
                    _CancelEditEductionCommand = new RelayCommand(param => CancelEditEducation(param));
                }
                return _CancelEditEductionCommand;
            }
        }

        #endregion

        #region "Event Handler"
        //private void SaveEditEdu_Click(object sender, RoutedEventArgs e) //update a Education
        //{
        //    SaveEditEducation(sender);
        //}        

        private void DeleteEducationClick(object sender, RoutedEventArgs e)
        {
            OnDeleteEducation(sender);
        }

        private void CurrentEducation_Click(object sender, RoutedEventArgs e)
        {
            MyEducationUtility.CurrentEducation_Click(sender);
        }

        private void EducationFromTime_Selected(object sender, SelectionChangedEventArgs e)
        {
            MyEducationUtility.EducationFromTime_Selected(sender);
        }
        private void EducationToTime_Selected(object sender, SelectionChangedEventArgs e)
        {
            MyEducationUtility.EducationToTime_Selected(sender);
        }
        //private void CancelEditEducationClick(object sender, RoutedEventArgs e)
        //{
        //    MyEducationUtility.CancelEditEducationClick(sender, myEducationsbyId);
        //}
        private void EditEducationClick(object sender, RoutedEventArgs e)
        {
            MyEducationUtility.EditEducationClick(sender);
        }

        //private void cancelEduButton_Click(object sender, RoutedEventArgs e)
        //{
        //    CancelNewEducation();
        //}        

        //private void saveNewEduClick(object sender, RoutedEventArgs e)
        //{
        //    SaveNewEduction();
        //}       

        private void educationContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            MyEducationUtility.educationContextMenu_IsVisibleChanged(sender);
        }
        #endregion

        #region "Private Method"
        private void TrimInput(SingleEducationModel educationModel)
        {
            educationModel.EInstName = educationModel.EInstName.Trim();
            educationModel.EDescription = educationModel.EDescription.Trim();
            educationModel.EConcentration = educationModel.EConcentration.Trim();
            educationModel.EDegree = !string.IsNullOrEmpty(educationModel.EDegree) ? educationModel.EDegree.Trim() : educationModel.EDegree;
        }

        private void ResetComponent()
        {
            AddEduModel = null;
            EduBox.Text = string.Empty;
            addEduDescriptionBox.Text = string.Empty;
            degreeTextBox.Text = string.Empty;
            instnConcentrationBox.Text = string.Empty;
            addEduComboBox.Text = string.Empty;
            addEduDateToComboBoxTo.Text = string.Empty;
            //addCollegeDateToComboBoxTo.Visibility = Visibility.Collapsed;
            addEduCheckBox.IsChecked = false;
            addEducationStackPanel.Visibility = Visibility.Collapsed;
        }

        private void TakeInputFromControl()
        {
            if (AddEduModel == null)
                AddEduModel = new SingleEducationModel();
            AddEduModel.EInstName = EduBox.Text.Trim();
            AddEduModel.EDescription = addEduDescriptionBox.Text.Trim();
            AddEduModel.EFromTime = addEduComboBox.SelectedDate.HasValue ? ModelUtility.MillisFromDateTimeSince1970(addEduComboBox.SelectedDate.Value) : 1;
            if (addEduCheckBox.IsChecked == true)
                AddEduModel.EToTime = 0;
            else
                AddEduModel.EToTime = addEduDateToComboBoxTo.SelectedDate.HasValue ? ModelUtility.MillisFromDateTimeSince1970(addEduDateToComboBoxTo.SelectedDate.Value) : 1;
            AddEduModel.EConcentration = instnConcentrationBox.Text.Trim();
            AddEduModel.EDegree = degreeTextBox.Text.Trim();
        }

        private void SaveNewEduction()
        {
            try
            {
                TakeInputFromControl();
                if (MyEducationUtility.VerifyEducation(AddEduModel, true))
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        bool _Success = false;
                        string _Msg = string.Empty;
                        DefaultSettings.TEMP_EducationObject = new EducationDTO
                        {
                            SchoolName = AddEduModel.EInstName,
                            Degree = AddEduModel.EDegree,
                            Concentration = AddEduModel.EConcentration,
                            Description = AddEduModel.EDescription
                        };
                        DefaultSettings.TEMP_EducationObject.FromTime = ModelUtility.MillisFromDateTimeSince1970(addEduComboBox.SelectedDate.Value);
                        DefaultSettings.TEMP_EducationObject.ToTime = (addEduCheckBox.IsChecked == true) ? 0 : ModelUtility.MillisFromDateTimeSince1970(addEduDateToComboBoxTo.SelectedDate.Value);


                        JObject educationObject = new JObject();
                        educationObject[JsonKeys.SchoolName] = DefaultSettings.TEMP_EducationObject.SchoolName;
                        if (DefaultSettings.TEMP_EducationObject.Description != null) { educationObject[JsonKeys.Description] = DefaultSettings.TEMP_EducationObject.Description; }
                        educationObject[JsonKeys.FromTime] = DefaultSettings.TEMP_EducationObject.FromTime;
                        educationObject[JsonKeys.ToTime] = DefaultSettings.TEMP_EducationObject.ToTime;
                        educationObject[JsonKeys.Graduated] = DefaultSettings.TEMP_EducationObject.Graduated;
                        educationObject[JsonKeys.IsSchool] = DefaultSettings.TEMP_EducationObject.IsSchool;
                        educationObject[JsonKeys.AttendedFor] = DefaultSettings.TEMP_EducationObject.AttendedFor;
                        if (DefaultSettings.TEMP_EducationObject.Degree != null) { educationObject[JsonKeys.Degree] = DefaultSettings.TEMP_EducationObject.Degree; }
                        if (DefaultSettings.TEMP_EducationObject.Concentration != null) { educationObject[JsonKeys.Concentration] = DefaultSettings.TEMP_EducationObject.Concentration; }
                        //AddEducationInfo addEducationInfo = new AddEducationInfo(DefaultSettings.TEMP_EducationObject);
                        // addEducationInfo.Run(out _Success, out _Msg);

                        JObject pakToSend = new JObject();
                        pakToSend[JsonKeys.EducationObj] = educationObject;
                        (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_ADD_EDUCATION, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _Success, out _Msg);
                        if (_Success)
                        {
                            ResetComponent();
                        }
                        else if (!string.IsNullOrEmpty(_Msg)) UIHelperMethods.ShowFailed(_Msg, "Save education");
                    });
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void CancelNewEducation()
        {
            try
            {
                ResetComponent();
                EducationAbsentPanelVisibility();
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        //Edit
        private void SaveEditEducation(object param)
        {
            try
            {
                //Control control = (Control)sender;
                SingleEducationModel educationModel = (SingleEducationModel)param;
                TrimInput(educationModel);
                if (myEducationsbyId == null) RingDictionaries.Instance.EDUCATION_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out myEducationsbyId);
                EducationDTO education = myEducationsbyId[educationModel.EId];
                if (educationModel.EInstName.Equals(education.SchoolName) && educationModel.EDescription.Equals(education.Description) && educationModel.EDegree.Equals(education.Degree)
                    && educationModel.EConcentration.Equals(education.Concentration) && educationModel.EFromTime == education.FromTime && educationModel.EToTime == education.ToTime)
                {
                    educationModel.EInstituteNameErrorString = NotificationMessages.ABOUT_NO_CHANGE;
                }
                else
                {
                    if (MyEducationUtility.VerifyEducation(educationModel, true))
                    {
                        educationModel.EVisibilityGraducated = (educationModel.EIsGraducated == true) ? Visibility.Visible : Visibility.Collapsed;
                        EducationDTO upDatedEducation = new EducationDTO { Id = educationModel.EId, SchoolName = educationModel.EInstName, Description = educationModel.EDescription, Degree = educationModel.EDegree, Concentration = educationModel.EConcentration, FromTime = educationModel.EFromTime, ToTime = educationModel.EToTime, Graduated = educationModel.EIsGraducated };
                        /*upDatedEducation.IsSchool = false;
                        upDatedEducation.AttendedFor = educationModel.EAttendedForIndex + 1;*/
                        if (!string.IsNullOrEmpty(educationModel.EDegree) && !string.IsNullOrEmpty(educationModel.EConcentration))
                        {
                            educationModel.EVisibilityDegConcBoth = Visibility.Visible;
                        }
                        else
                        {
                            educationModel.EVisibilityDegConcBoth = Visibility.Collapsed;
                        }
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            bool _SuccessEducationUpdate = false;
                            string _MsgEducationUpdate = string.Empty;

                            JObject pakToSend = SendDataToServer.UpdateEducation(upDatedEducation);
                            (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_UPDATE_EDUCATION, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _SuccessEducationUpdate, out _MsgEducationUpdate);
                            if (_SuccessEducationUpdate)
                            {
                                myEducationsbyId[educationModel.EId] = upDatedEducation;
                                //educationModel.EVisibilityEditMode = Visibility.Collapsed;
                                educationModel.EVisibilityViewMode = Visibility.Visible;
                            }
                            else if (!string.IsNullOrEmpty(_MsgEducationUpdate)) UIHelperMethods.ShowFailed(_MsgEducationUpdate, "Edit education");
                        });
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: SaveEditEdu_Click() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void CancelEditEducation(object param)
        {
            MyEducationUtility.CancelEditEducationClick(param, myEducationsbyId);
        }

        private void OnDeleteEducation(object sender)
        {
            MyEducationUtility.DeleteEducation(sender, myEducationsbyId);
            EducationAbsentPanelVisibility();
        }
        #endregion

        #region "Public Method"
        public void ShowMyEducationsList()
        {
            try
            {
                lock (RingDictionaries.Instance.EDUCATION_DICTIONARY)
                {
                    if (RingDictionaries.Instance.EDUCATION_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out myEducationsbyId))
                    {
                        Absent = Visibility.Collapsed;
                        foreach (EducationDTO Education in myEducationsbyId.Values)
                        {
                            SingleEducationModel EducationModel = new SingleEducationModel();
                            EducationModel.LoadData(Education);

                            if (!RingIDViewModel.Instance.MyEducationModelsList.Any(p => p.EId == EducationModel.EId))
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    RingIDViewModel.Instance.MyEducationModelsList.Add(EducationModel);
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ShowMyEducationsList() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void EducationAbsentPanelVisibility()
        {
            Absent = (RingIDViewModel.Instance.MyEducationModelsList.Count > 0 || addEducationStackPanel.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Loaded_Unloaded Events"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            //saveEduButton.Click += saveNewEduClick;
            //cancelEduButton.Click += cancelEduButton_Click;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            //saveEduButton.Click -= saveNewEduClick;
            //cancelEduButton.Click -= cancelEduButton_Click;
        }
        #endregion
    }
}

﻿using log4net;
using Models.Constants;
using Models.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Auth;

namespace View.UI.Profile.MyProfile.About
{
    /// <summary>
    /// Interaction logic for UCMyBasicInfoEdit.xaml
    /// </summary>
    public partial class UCMyBasicInfoEdit : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyBasicInfoEdit).Name);

        #region "Private Member"
        private JObject pakToSend;
        private int minimumBirthdayYear = 13;
        private int maximumMarriageDayYear = 20;
        private int maximumFullNameCharacterLimit = 50;
        #endregion

        #region "Property"
        private SingleBasicModelSet _MyBasicSet = new SingleBasicModelSet();
        public SingleBasicModelSet MyBasicSet { get { return _MyBasicSet; } set { _MyBasicSet = value; this.OnPropertyChanged("MyBasicSet"); } }
        #endregion

        #region ErrorLabel Property
        private string _ErrorName = string.Empty;
        public string ErrorName { get { return _ErrorName; } set { _ErrorName = value; this.OnPropertyChanged("ErrorName"); } }

        private string _ErrorHomeCity = string.Empty;
        public string ErrorHomeCity { get { return _ErrorHomeCity; } set { _ErrorHomeCity = value; this.OnPropertyChanged("ErrorHomeCity"); } }

        private string _ErrorCurrentCity = string.Empty;
        public string ErrorCurrentCity { get { return _ErrorCurrentCity; } set { _ErrorCurrentCity = value; this.OnPropertyChanged("ErrorCurrentCity"); } }

        private string _ErrorBirthday = string.Empty;
        public string ErrorBirthday { get { return _ErrorBirthday; } set { _ErrorBirthday = value; this.OnPropertyChanged("ErrorBirthday"); } }

        private string _ErrorMarriageDay = string.Empty;
        public string ErrorMarriageDay { get { return _ErrorMarriageDay; } set { _ErrorMarriageDay = value; this.OnPropertyChanged("ErrorMarriageDay"); } }

        private string _ErrorAboutMe = string.Empty;
        public string ErrorAboutMe { get { return _ErrorAboutMe; } set { _ErrorAboutMe = value; this.OnPropertyChanged("ErrorAboutMe"); } }

        #endregion ErrorLabel Property

        #region Button Enable Properties
        private bool _OKButton = true;
        public bool OKButton { get { return _OKButton; } set { _OKButton = value; this.OnPropertyChanged("OKButton"); } }

        private bool _CancelButton = true;
        public bool CancelButton { get { return _CancelButton; } set { _CancelButton = value; this.OnPropertyChanged("CancelButton"); } }
        #endregion Button Enable Properties

        #region "Constructor"
        public UCMyBasicInfoEdit(SingleBasicModelSet MyBasicSet)
        {
            InitializeComponent();
            this.DataContext = this;
            this.MyBasicSet = MyBasicSet;
        }
        #endregion

        #region "Command"
        private ICommand _SaveCommand;
        public ICommand SaveCommand { get { _SaveCommand = _SaveCommand ?? new RelayCommand((param) => OnSaveClicked()); return _SaveCommand; } }

        private ICommand _CancelCommand;
        public ICommand CancelCommand { get { _CancelCommand = _CancelCommand ?? new RelayCommand((param) => OnCancelClicked()); return _CancelCommand; } }
        #endregion

        #region Verification
        private void VerifyHomeCity()
        {
            bool HomeCityBlank = !string.IsNullOrEmpty(DefaultSettings.userProfile.HomeCity) && string.IsNullOrWhiteSpace(MyBasicSet.HomeCity.Trim());
            bool HomeCityChanged = !MyBasicSet.HomeCity.Trim().Equals(DefaultSettings.userProfile.HomeCity);
            bool HomeCityNotSet = string.IsNullOrEmpty(DefaultSettings.userProfile.HomeCity) && string.IsNullOrWhiteSpace(MyBasicSet.HomeCity.Trim());
            if (!HomeCityNotSet)
            {
                if (HomeCityBlank) ErrorHomeCity = NotificationMessages.BIE_HOME_CITY_BLANK;
                else if (HomeCityChanged)
                {
                    if (pakToSend == null) pakToSend = new JObject();
                    pakToSend[JsonKeys.HomeCity] = MyBasicSet.HomeCity.Trim();
                    if (!string.IsNullOrEmpty(ErrorHomeCity)) ErrorHomeCity = string.Empty;

                }
            }
        }

        private void VerifyCurrentCity()
        {
            bool CurrentCityBlank = !string.IsNullOrEmpty(DefaultSettings.userProfile.CurrentCity) && string.IsNullOrWhiteSpace(MyBasicSet.CurrentCity.Trim());
            bool CurrentCityChanged = !MyBasicSet.CurrentCity.Trim().Equals(DefaultSettings.userProfile.CurrentCity);
            bool CurrentCityNotSet = string.IsNullOrEmpty(DefaultSettings.userProfile.CurrentCity) && string.IsNullOrWhiteSpace(MyBasicSet.CurrentCity.Trim());
            if (!CurrentCityNotSet)
            {
                if (CurrentCityBlank) ErrorCurrentCity = NotificationMessages.BIE_CURRENT_CITY_BLANK;
                else if (CurrentCityChanged)
                {
                    if (pakToSend == null) pakToSend = new JObject();
                    pakToSend[JsonKeys.CurrentCity] = MyBasicSet.CurrentCity.Trim();
                    if (!string.IsNullOrEmpty(ErrorCurrentCity)) ErrorCurrentCity = string.Empty;

                }
            }
        }

        private void VerifyAbout()
        {
            bool AboutMeNotSet = string.IsNullOrEmpty(DefaultSettings.userProfile.AboutMe) && string.IsNullOrEmpty(MyBasicSet.AboutMe.Trim());
            bool AboutMeBlank = string.IsNullOrEmpty(DefaultSettings.userProfile.AboutMe) && string.IsNullOrWhiteSpace(MyBasicSet.AboutMe.Trim());
            bool AboutMeChanged = !MyBasicSet.AboutMe.Trim().Equals(DefaultSettings.userProfile.AboutMe);
            if (!AboutMeNotSet)
            {
                if (AboutMeBlank) ErrorAboutMe = NotificationMessages.BIE_ABOUT_ME_BLANK;
                else if (AboutMeChanged)
                {
                    if (pakToSend == null) pakToSend = new JObject();
                    pakToSend[JsonKeys.AboutMe] = MyBasicSet.AboutMe.Trim();
                    if (!string.IsNullOrEmpty(ErrorAboutMe)) ErrorAboutMe = string.Empty;

                }
            }
        }

        private void VerifyGender()
        {
            bool GenderNotSet = string.IsNullOrEmpty(DefaultSettings.userProfile.Gender) && string.IsNullOrEmpty(MyBasicSet.Gender);
            bool GenderChanged = !MyBasicSet.Gender.Equals(DefaultSettings.userProfile.Gender);
            if (!GenderNotSet && GenderChanged)
            {
                if (pakToSend == null) pakToSend = new JObject();
                pakToSend[JsonKeys.Gender] = MyBasicSet.Gender;
            }
        }

        private void VerifyFullName()
        {
            //bool FullNameLessThanMinimumLimit = MyBasicSet.FullName.Trim().Length < 2;
            bool FullNameExceededLimit = MyBasicSet.FullName.Trim().Length > maximumFullNameCharacterLimit;
            bool FullNameBlank = string.IsNullOrWhiteSpace(MyBasicSet.FullName.Trim());
            bool FullNameChanged = !MyBasicSet.FullName.Trim().Equals(DefaultSettings.userProfile.FullName);
            //bool FullNameNotStartsWithAlphaNumeric = !(char.IsLetter(MyBasicSet.FullName.Trim()[0])) && (!(char.IsNumber(MyBasicSet.FullName.Trim()[0])));

            if (FullNameBlank) ErrorName = NotificationMessages.BIE_FULL_NAME_BLANK;
            //else if (FullNameLessThanMinimumLimit) ErrorName = NotificationMessages.BIE_FULL_NAME_LESS_THAN_MINIMUM_LIMIT;
            else if (FullNameExceededLimit) ErrorName = string.Format(NotificationMessages.BIE_FULL_NAME_EXCEEDS_LIMIT, maximumFullNameCharacterLimit);
            //else if(FullNameNotStartsWithAlphaNumeric) ErrorName = NotificationMessages.BIE_FULL_NAME_DOES_NOT_START_WITH_ALPHANUMERIC;
            else if (FullNameChanged)
            {
                if (pakToSend == null) pakToSend = new JObject();
                pakToSend[JsonKeys.FullName] = MyBasicSet.FullName.Trim();
                if (!string.IsNullOrEmpty(ErrorName)) ErrorName = string.Empty;
            }

        }

        private void VerifyBirthday()
        {
            bool NoBirthdaySet = MyBasicSet.BirthDate == DateTime.MinValue && DefaultSettings.userProfile.BirthDate == DateTime.MinValue;
            bool BirthdaySetInFuture = MyBasicSet.BirthDate != DateTime.MinValue && MyBasicSet.BirthDate > DateTime.Now;
            bool BirthdayGreaterThan13Years = MyBasicSet.BirthDate != DateTime.MinValue && MyBasicSet.BirthDate.Year < DateTime.Now.Year - minimumBirthdayYear || (MyBasicSet.BirthDate.Year == DateTime.Now.Year - minimumBirthdayYear
                && MyBasicSet.BirthDate.Month < DateTime.Now.Month) || (MyBasicSet.BirthDate.Year == DateTime.Now.Year - minimumBirthdayYear && MyBasicSet.BirthDate.Month == DateTime.Now.Month && MyBasicSet.BirthDate.Day <= DateTime.Now.Day);
            bool BirthdayChanged = MyBasicSet.BirthDate != DefaultSettings.userProfile.BirthDate;

            if (!NoBirthdaySet)
            {
                if (MyBasicSet.BirthDate != DateTime.MinValue && !BirthdayGreaterThan13Years && BirthdayChanged) ErrorBirthday = NotificationMessages.BIE_BIRTHDAY_AGE_LIMIT_EXCEEDED;
                else if (BirthdaySetInFuture) ErrorBirthday = NotificationMessages.BIE_BIRTHDAY_SET_FUTURE;
                else if (BirthdayChanged)
                {
                    DateTime dt = MyBasicSet.BirthDate;
                    if (pakToSend == null) pakToSend = new JObject();
                    pakToSend[JsonKeys.BirthDay] = dt.Day;
                    pakToSend[JsonKeys.BirthMonth] = dt.Month;
                    pakToSend[JsonKeys.BirthYear] = dt.Year;
                    if (!string.IsNullOrEmpty(ErrorBirthday)) ErrorBirthday = string.Empty;
                }
            }

            #region OLD CODE
            //bool birthDayMonth = (ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.BirthDate).Year == DateTime.UtcNow.AddYears(minimumBirthdayYear).Year && ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.BirthDate).Month <= DateTime.UtcNow.Month) ||
            //                     (ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.BirthDate).Year < DateTime.UtcNow.AddYears(minimumBirthdayYear).Year);
            //bool birthDayDay = (ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.BirthDate).Year == DateTime.UtcNow.AddYears(minimumBirthdayYear).Year && ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.BirthDate).Day <= DateTime.UtcNow.Day) ||
            //                    ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.BirthDate).Year < DateTime.UtcNow.AddYears(minimumBirthdayYear).Year;

            //bool NoBirthdaySet = DefaultSettings.userProfile.BirthDay == 1 && MyBasicSet.BirthDate == 1;
            //bool BirthdaySetInFuture = MyBasicSet.BirthDate > ModelUtility.CurrentTimeMillisLocal();
            //bool BirthdayGreaterThan13Years = MyBasicSet.BirthDate < ModelUtility.CurrentTimeMillisLocal() &&
            //                                  (
            //                                    ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.BirthDate).Year <= DateTime.UtcNow.AddYears(minimumBirthdayYear).Year && birthDayMonth && birthDayDay
            //                                  );
            //bool BirthdayChanged = MyBasicSet.BirthDate != DefaultSettings.userProfile.BirthDay;
            //if (!NoBirthdaySet)
            //{
            //    if (!BirthdayGreaterThan13Years && BirthdayChanged) ErrorBirthday = NotificationMessages.BIE_BIRTHDAY_AGE_LIMIT_EXCEEDED;
            //    else if (BirthdaySetInFuture) ErrorBirthday = NotificationMessages.BIE_BIRTHDAY_SET_FUTURE;
            //    else if (BirthdayChanged)
            //    {
            //        DateTime dt = ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.BirthDate);
            //        if (pakToSend == null) pakToSend = new JObject();
            //        pakToSend[JsonKeys.BirthDay] = dt.Day;
            //        pakToSend[JsonKeys.BirthMonth] = dt.Month;
            //        pakToSend[JsonKeys.BirthYear] = dt.Year;
            //        if (!string.IsNullOrEmpty(ErrorBirthday)) ErrorBirthday = string.Empty;

            //    }
            //}
            #endregion
        }

        private void VerifyMarriageDay()
        {
            bool NoMarriageDaySet = MyBasicSet.MarriageDate == DateTime.MinValue && DefaultSettings.userProfile.MarriageDate == DateTime.MinValue;
            bool BirthdayGreaterThanMarriageday = MyBasicSet.MarriageDate != DateTime.MinValue && MyBasicSet.BirthDate != DateTime.MinValue && MyBasicSet.BirthDate > MyBasicSet.MarriageDate;
            bool MarriageDayWithin20Years = MyBasicSet.MarriageDate != DateTime.MinValue && (MyBasicSet.MarriageDate.Year < DateTime.Now.Year + maximumMarriageDayYear || (MyBasicSet.MarriageDate.Year == DateTime.Now.Year + maximumMarriageDayYear
                && MyBasicSet.MarriageDate.Month < DateTime.Now.Month) || (MyBasicSet.MarriageDate.Year == DateTime.Now.Year + maximumMarriageDayYear && MyBasicSet.MarriageDate.Month == DateTime.Now.Month && MyBasicSet.MarriageDate.Day <= DateTime.Now.Day));
            bool MarriageDayChanged = MyBasicSet.MarriageDate != DefaultSettings.userProfile.MarriageDate;

            if (!NoMarriageDaySet)
            {
                if (MyBasicSet.MarriageDate != DateTime.MinValue && !MarriageDayWithin20Years && MarriageDayChanged) ErrorMarriageDay = NotificationMessages.BIE_MARRIAGE_DATE_LIMIT_EXCEEDED;
                else if (BirthdayGreaterThanMarriageday) ErrorMarriageDay = NotificationMessages.BIE_MARRIAGE_DATE_SET_BEFORE_BIRTHDAY;
                else if (MarriageDayChanged)
                {
                    DateTime dt = MyBasicSet.MarriageDate;
                    if (pakToSend == null) pakToSend = new JObject();
                    pakToSend[JsonKeys.MarriageDay] = dt.Day;
                    pakToSend[JsonKeys.MarriageMonth] = dt.Month;
                    pakToSend[JsonKeys.MarriageYear] = dt.Year;
                    if (!string.IsNullOrEmpty(ErrorMarriageDay)) ErrorMarriageDay = string.Empty;
                }
            }

            #region OLD Code
            //bool marriageDayMonth = (ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.MarriageDate).Year == DateTime.UtcNow.AddYears(maximumMarriageDayYear).Year && ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.MarriageDate).Month <= DateTime.UtcNow.Month) ||
            //                        (ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.MarriageDate).Year < DateTime.UtcNow.AddYears(maximumMarriageDayYear).Year);
            //bool marriageDayDay = (ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.MarriageDate).Year == DateTime.UtcNow.AddYears(maximumMarriageDayYear).Year && ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.MarriageDate).Day <= DateTime.UtcNow.Day) ||
            //                        (ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.MarriageDate).Year < DateTime.UtcNow.AddYears(maximumMarriageDayYear).Year);

            //bool NoMarriageDaySet = DefaultSettings.userProfile.MarriageDay == 1 && MyBasicSet.MarriageDate == 1;
            //bool BirthdayGreaterThanMarriageday = MyBasicSet.MarriageDate != 1 && MyBasicSet.BirthDate != 1 && MyBasicSet.BirthDate > MyBasicSet.MarriageDate;
            //bool MarriageDayWithin20Years = ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.MarriageDate).Year <= DateTime.UtcNow.AddYears(maximumMarriageDayYear).Year &&
            //                                marriageDayMonth &&
            //                                marriageDayDay;
            //bool MarriageDayChanged = MyBasicSet.MarriageDate != DefaultSettings.userProfile.MarriageDay;
            //if (!NoMarriageDaySet)
            //{
            //    if (!MarriageDayWithin20Years && !MarriageDayChanged) ErrorMarriageDay = NotificationMessages.BIE_MARRIAGE_DATE_LIMIT_EXCEEDED;
            //    else if (BirthdayGreaterThanMarriageday) ErrorMarriageDay = NotificationMessages.BIE_MARRIAGE_DATE_SET_BEFORE_BIRTHDAY;
            //    else if (MarriageDayChanged)
            //    {
            //        DateTime dt = ModelUtility.DateTimeFromMillisSince1970(MyBasicSet.MarriageDate);
            //        if (pakToSend == null) pakToSend = new JObject();
            //        pakToSend[JsonKeys.MarriageDay] = dt.Day;
            //        pakToSend[JsonKeys.MarriageMonth] = dt.Month;
            //        pakToSend[JsonKeys.MarriageYear] = dt.Year;
            //        if (!string.IsNullOrEmpty(ErrorMarriageDay)) ErrorMarriageDay = string.Empty;
            //    }
            //}
            #endregion
        }

        private void VerifyBasicInfoAll()
        {
            VerifyFullName();
            VerifyGender();
            VerifyHomeCity();
            VerifyCurrentCity();
            VerifyBirthday();
            VerifyMarriageDay();
            VerifyAbout();
        }

        public bool NoError
        {
            get
            {
                return string.IsNullOrEmpty(ErrorName) &&
                       string.IsNullOrEmpty(ErrorHomeCity) &&
                       string.IsNullOrEmpty(ErrorCurrentCity) &&
                       string.IsNullOrEmpty(ErrorBirthday) &&
                       string.IsNullOrEmpty(ErrorMarriageDay) &&
                       string.IsNullOrEmpty(ErrorAboutMe);
            }
        }
        #endregion Verification

        #region Command Methods
        private void OnSaveClicked()
        {
            try
            {
                ClearErrorMessages();
                pakToSend = null;
                VerifyBasicInfoAll();

                if (NoError)
                {
                    if (pakToSend == null) // checked
                    {
                        UIHelperMethods.ShowInformation(NotificationMessages.ABOUT_NO_CHANGE, "Save Info");
                        //CustomMessageBox.ShowInformation(NotificationMessages.ABOUT_NO_CHANGE);
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ResetBasicInfoPanel();
                    }
                    else if (pakToSend != null)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            pakToSend[JsonKeys.ProfileType] = 1;
                            pakToSend[JsonKeys.NumberOfHeaders] = (pakToSend.Count - 1) + "";
                            OKButton = false;
                            CancelButton = false;
                            bool _SuccessBasic = false;
                            string _MsgBasic = string.Empty;

                            (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_USER_PROFILE, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _SuccessBasic, out _MsgBasic);
                            if (_SuccessBasic)
                            {
                                UpdateBasicInfoOnSuccess();

                                ClearErrorMessages();
                                RingIDViewModel.Instance.MyBasicInfoModel.LoadData(DefaultSettings.userProfile);
                                RingIDViewModel.Instance.MyBasicInfoModel.OnPropertyChanged("CurrentInstance");
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.BasicInfoEditButton = Visibility.Visible;
                                MyBasicSet.SetMySingleBasicSetToDefaultSettingsUserProfile();
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ResetBasicInfoPanel();
                            }
                            else if (!string.IsNullOrEmpty(_MsgBasic)) UIHelperMethods.ShowFailed(_MsgBasic, "Save info");
                            OKButton = true;
                            CancelButton = true;
                        });
                    }
                }
                this.DataContext = this;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UpdateBasicInfoOnSuccess()
        {
            if (pakToSend[JsonKeys.MarriageDay] != null && pakToSend[JsonKeys.MarriageMonth] != null && pakToSend[JsonKeys.MarriageYear] != null)
            {
                DefaultSettings.userProfile.MarriageDate = new DateTime((int)pakToSend[JsonKeys.MarriageYear], (int)pakToSend[JsonKeys.MarriageMonth], (int)pakToSend[JsonKeys.MarriageDay]);
            }
            if (pakToSend[JsonKeys.BirthDay] != null && pakToSend[JsonKeys.BirthMonth] != null && pakToSend[JsonKeys.BirthYear] != null)
            {
                DefaultSettings.userProfile.BirthDate = new DateTime((int)pakToSend[JsonKeys.BirthYear], (int)pakToSend[JsonKeys.BirthMonth], (int)pakToSend[JsonKeys.BirthDay]);
            }
            if (pakToSend[JsonKeys.FullName] != null) { DefaultSettings.userProfile.FullName = (string)pakToSend[JsonKeys.FullName]; }
            if (pakToSend[JsonKeys.Gender] != null) { DefaultSettings.userProfile.Gender = (string)pakToSend[JsonKeys.Gender]; }
            if (pakToSend[JsonKeys.HomeCity] != null) { DefaultSettings.userProfile.HomeCity = (string)pakToSend[JsonKeys.HomeCity]; }
            if (pakToSend[JsonKeys.CurrentCity] != null) { DefaultSettings.userProfile.CurrentCity = (string)pakToSend[JsonKeys.CurrentCity]; }
            if (pakToSend[JsonKeys.AboutMe] != null) { DefaultSettings.userProfile.AboutMe = (string)pakToSend[JsonKeys.AboutMe]; }
        }
        private void OnCancelClicked()
        {
            ClearErrorMessages();
            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.BasicInfoEditButton = Visibility.Visible;
            MyBasicSet.SetMySingleBasicSetToDefaultSettingsUserProfile();
            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ResetBasicInfoPanel();
        }
        #endregion Command Methods

        #region UI Methods
        private void ClearErrorMessages()
        {
            ErrorName = string.Empty;
            ErrorHomeCity = string.Empty;
            ErrorCurrentCity = string.Empty;
            ErrorBirthday = string.Empty;
            ErrorMarriageDay = string.Empty;
            ErrorAboutMe = string.Empty;
        }
        #endregion UI Methods

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Auth;

namespace View.UI.Profile.MyProfile.About
{
    /// <summary>
    /// Interaction logic for UCMySkill.xaml
    /// </summary>
    public partial class UCMySkill : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMySkill).Name);

        #region "Private Member"
        private Dictionary<Guid, SkillDTO> mySkillsbyId;
        #endregion

        #region "Property"
        private SingleSkillModel _AddSkillModel;
        public SingleSkillModel AddSkillModel
        {
            get
            {
                return _AddSkillModel;
            }
            set
            {
                _AddSkillModel = value;
                this.OnPropertyChanged("AddSkillModel");
            }
        }
        private Visibility _Absent;
        public Visibility Absent
        {
            get
            {
                return _Absent;
            }
            set
            {
                _Absent = value;
                this.OnPropertyChanged("Absent");
            }
        }
        #endregion

        #region "Constructor"
        public UCMySkill()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Command"
        private ICommand _SaveNewSkillCommand;
        public ICommand SaveNewSkillCommand
        {
            get
            {
                if (_SaveNewSkillCommand == null)
                {
                    _SaveNewSkillCommand = new RelayCommand(param => SaveNewSkill());
                }
                return _SaveNewSkillCommand;
            }
        }

        private ICommand _CancelNewSkillCommand;
        public ICommand CancelNewSkillCommand
        {
            get
            {
                if (_CancelNewSkillCommand == null)
                {
                    _CancelNewSkillCommand = new RelayCommand(param => CancelNewSkill());
                }
                return _CancelNewSkillCommand;
            }
        }

        private ICommand _SaveEditSkillCommand;
        public ICommand SaveEditSkillCommand
        {
            get
            {
                if (_SaveEditSkillCommand == null)
                {
                    _SaveEditSkillCommand = new RelayCommand(param => SaveEditSkill(param));
                }
                return _SaveEditSkillCommand;
            }
        }

        private ICommand _CancelEditSkillCommand;
        public ICommand CancelEditSkillCommand
        {
            get
            {
                if (_CancelEditSkillCommand == null)
                {
                    _CancelEditSkillCommand = new RelayCommand(param => CancelEditSkill(param));
                }
                return _CancelEditSkillCommand;
            }
        }
        #endregion

        #region "Event Handler"

        private void DeleteSkillClick(object sender, RoutedEventArgs e)
        {
            try
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this skill"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
                if (isTrue)
                {
                    if (mySkillsbyId == null) RingDictionaries.Instance.SKILL_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out mySkillsbyId);
                    Control control = (Control)sender;
                    SingleSkillModel skillModel = (SingleSkillModel)control.DataContext;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        bool _Success = false;
                        string _Msg = string.Empty;
                        //  DeleteSkillInfo deleteSkillInfo = new DeleteSkillInfo(skillModel.SKId);
                        //  deleteSkillInfo.Run(out _Success, out _Msg);

                        JObject pakToSend = new JObject();
                        pakToSend[JsonKeys.UUID] = skillModel.SKId;
                        (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_REMOVE_SKILL, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _Success, out _Msg);
                        if (_Success)
                        {
                            if (mySkillsbyId.Remove(skillModel.SKId))
                            {
                                if (RingIDViewModel.Instance.MySkillModelsList.Where(P => P.SKId == skillModel.SKId).FirstOrDefault() != null)
                                {
                                    RingIDViewModel.Instance.MySkillModelsList.Remove(skillModel);
                                }
                                SkillAbsentPanelVisibility();
                            }
                            else
                            {
                                UIHelperMethods.ShowFailed("Can't delete skill info", "Delete skill");
                            }
                            //skillModel.SVisibilityEditMode = Visibility.Collapsed;
                            skillModel.SVisibilityViewMode = Visibility.Visible;
                            skillModel.SErrorString = string.Empty;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_Msg))
                                UIHelperMethods.ShowFailed(_Msg, "Delete skill");
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: DeleteSkillClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void EditSkillClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                SingleSkillModel skillModel = (SingleSkillModel)control.DataContext;
                //skillModel.SVisibilityEditMode = Visibility.Visible;
                skillModel.SVisibilityViewMode = Visibility.Collapsed;
                skillModel.SErrorString = string.Empty;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void skillContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Control control = (Control)sender;
            if (sender != null && control is ContextMenu)
            {
                SingleSkillModel skillModel = (SingleSkillModel)control.DataContext;
                if (control.IsVisible)
                    skillModel.SettingAboutButtonVisibilty = Visibility.Visible;
                else
                    skillModel.SettingAboutButtonVisibilty = Visibility.Hidden;
            }
        }
        #endregion

        #region "Private Method"
        private void ResetComponent()
        {
            AddSkillModel = null;
            addSkillPanel.Visibility = Visibility.Collapsed;
            SkillAbsentPanelVisibility();
        }

        public void SkillAbsentPanelVisibility()
        {
            Absent = (RingIDViewModel.Instance.MySkillModelsList.Count > 0 || addSkillPanel.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void SaveNewSkill()
        {
            try
            {
                if (!string.IsNullOrEmpty(AddSkillModel.SSkill) && !string.IsNullOrWhiteSpace(AddSkillModel.SSkill.Trim()))
                {
                    AddSkillModel.SErrorString = string.Empty;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        bool _Success = false;
                        string _Msg = string.Empty;
                        DefaultSettings.TEMP_SkillObject = new SkillDTO
                        {
                            SkillName = AddSkillModel.SSkill.Trim(),
                            Description = string.IsNullOrEmpty(AddSkillModel.SDescription) ? AddSkillModel.SDescription : AddSkillModel.SDescription.Trim()
                        };
                        //  AddSkillInfo addSkillInfo = new AddSkillInfo(DefaultSettings.TEMP_SkillObject);
                        //addSkillInfo.Run(out _Success, out _Msg);


                        JObject skillObject = new JObject();
                        skillObject[JsonKeys.Skill] = DefaultSettings.TEMP_SkillObject.SkillName;
                        if ((DefaultSettings.TEMP_SkillObject.Description != null)) { skillObject[JsonKeys.Description] = DefaultSettings.TEMP_SkillObject.Description; }
                        JObject pakToSend = new JObject();
                        pakToSend[JsonKeys.SkillObj] = skillObject;
                        (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_ADD_SKILL, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _Success, out _Msg);
                        if (_Success)
                        {
                            ResetComponent();
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_Msg))
                                UIHelperMethods.ShowFailed(_Msg, "Save skill");
                            //CustomMessageBox.ShowError("Failed! " + _Msg);
                        }
                    });
                }
                else
                {
                    AddSkillModel.SErrorString = NotificationMessages.SKILL_CREATE_NO_NAME;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void CancelNewSkill()
        {
            try
            {
                ResetComponent();
                SkillAbsentPanelVisibility();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SaveEditSkill(object param)
        {
            try
            {
                //Control control = (Control)sender;
                SingleSkillModel skillModel = (SingleSkillModel)param;//SingleSkillModel skillModel = (SingleSkillModel)control.DataContext;
                skillModel.SErrorString = string.Empty;
                if (mySkillsbyId == null) RingDictionaries.Instance.SKILL_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out mySkillsbyId);
                SkillDTO skill = mySkillsbyId[skillModel.SKId];
                if (skillModel.SSkill.Equals(skill.SkillName) && skillModel.SDescription.Equals(skill.Description))
                {
                    skillModel.SErrorString = NotificationMessages.ABOUT_NO_CHANGE;
                }
                else
                {
                    if (string.IsNullOrEmpty(skillModel.SSkill) && string.IsNullOrWhiteSpace(skillModel.SSkill.Trim()))
                    {
                        skillModel.SErrorString = NotificationMessages.SKILL_CREATE_NO_NAME;
                    }
                    else
                    {
                        skillModel.SErrorString = string.Empty;
                        SkillDTO upDatedSkill = new SkillDTO
                        {
                            Id = skill.Id,
                            SkillName = skillModel.SSkill.Trim(),
                            Description = string.IsNullOrEmpty(skillModel.SDescription) ? skillModel.SDescription : skillModel.SDescription.Trim()
                        };
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            bool _SuccessSkillUpdate = false;
                            string _MsgSkillUpdate = string.Empty;
                            // UpdateSkillInfo updateSkillInfo = new UpdateSkillInfo(upDatedSkill);
                            // updateSkillInfo.Run(out _SuccessSkillUpdate, out _MsgSkillUpdate);

                            JObject skillObject = new JObject();
                            skillObject[JsonKeys.SkillId] = upDatedSkill.Id;
                            skillObject[JsonKeys.Skill] = upDatedSkill.SkillName;
                            if (upDatedSkill.Description != null) skillObject[JsonKeys.Description] = upDatedSkill.Description;

                            JObject pakToSend = new JObject();
                            pakToSend[JsonKeys.SkillObj] = skillObject;
                            (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_UPDATE_SKILL, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _SuccessSkillUpdate, out _MsgSkillUpdate);
                            if (_SuccessSkillUpdate)
                            {
                                mySkillsbyId[skillModel.SKId] = upDatedSkill;
                                //skillModel.SVisibilityEditMode = Visibility.Collapsed;
                                skillModel.SVisibilityViewMode = Visibility.Visible;
                                skillModel.SErrorString = string.Empty;
                            }
                            else if (!string.IsNullOrEmpty(_MsgSkillUpdate))
                            {      // MessageBox.Show(MainSwitcher.pageSwitcher, "Failed! " + _MsgSkillUpdate, "Failed!", MessageBoxButton.OK, MessageBoxImage.Error);
                                UIHelperMethods.ShowFailed(_MsgSkillUpdate, "Update skill");
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: saveEditSkillButton_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CancelEditSkill(object param)
        {
            try
            {
                //Control control = (Control)sender;
                SingleSkillModel skillModel = (SingleSkillModel)param;//SingleSkillModel skillModel = (SingleSkillModel)control.DataContext;
                if (mySkillsbyId == null) RingDictionaries.Instance.SKILL_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out mySkillsbyId);
                SkillDTO skill = mySkillsbyId[skillModel.SKId];
                skillModel.LoadData(skill);
                //skillModel.SVisibilityEditMode = Visibility.Collapsed;
                skillModel.SVisibilityViewMode = Visibility.Visible;
                skillModel.SErrorString = string.Empty;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Public Method"
        public void ShowMySkillsList()
        {
            try
            {
                lock (RingDictionaries.Instance.SKILL_DICTIONARY)
                {
                    if (RingDictionaries.Instance.SKILL_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out mySkillsbyId))
                    {
                        Absent = Visibility.Collapsed;
                        foreach (SkillDTO skill in mySkillsbyId.Values)
                        {
                            SingleSkillModel skillModel = new SingleSkillModel();
                            skillModel.LoadData(skill);
                            if (!RingIDViewModel.Instance.MySkillModelsList.Any(p => p.SKId == skillModel.SKId))
                            {

                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    RingIDViewModel.Instance.MySkillModelsList.Add(skillModel);
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ShowMySkillsList() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void ShowAddSkillPanel()
        {
            Absent = Visibility.Collapsed;
            addSkillPanel.Visibility = Visibility.Visible;
            AddSkillModel = new SingleSkillModel();
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region "Loaded_Unloaded Events"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            //saveSkillButton.Click += SaveNewSkillClick;
            //cancelSkillButton.Click += cancelSkillButton_Click;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            //saveSkillButton.Click -= SaveNewSkillClick;
            //cancelSkillButton.Click -= cancelSkillButton_Click;
        }
        #endregion
    }
}

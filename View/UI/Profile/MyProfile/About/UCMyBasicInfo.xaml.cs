﻿using System.ComponentModel;
using System.Windows.Controls;
using View.BindingModels;

namespace View.UI.Profile.MyProfile.About
{
    /// <summary>
    /// Interaction logic for UCMyProfileBasicInfo.xaml
    /// </summary>
    public partial class UCMyBasicInfo : UserControl, INotifyPropertyChanged
    {
        #region "Property"
        private SingleBasicModelSet _MyBasicSet = new SingleBasicModelSet();
        public SingleBasicModelSet MyBasicSet
        {
            get { return _MyBasicSet; }
            set
            {
                _MyBasicSet = value;
                this.OnPropertyChanged("MyBasicSet");
            }
        }
        #endregion

        #region "Constructor"
        public UCMyBasicInfo(SingleBasicModelSet MyBasicSet)
        {
            InitializeComponent();
            this.MyBasicSet = MyBasicSet;
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Auth;
using View.Utility;

namespace View.UI.Profile.MyProfile.About
{
    /// <summary>
    /// Interaction logic for UCMyProfessionalCareer.xaml
    /// </summary>
    public partial class UCMyProfessionalCareer : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyProfessionalCareer).Name);

        #region "Private Member"
        private string msg = string.Empty;
        private Dictionary<Guid, WorkDTO> myWorksbyId;
        #endregion

        #region "Property"
        private SingleWorkModel _AddCarrerModel;
        public SingleWorkModel AddCarrerModel
        {
            get
            {
                return _AddCarrerModel;
            }
            set
            {
                _AddCarrerModel = value;
                this.OnPropertyChanged("AddCarrerModel");
            }
        }
        private Visibility _Absent;
        public Visibility Absent
        {
            get
            {
                return _Absent;
            }
            set
            {
                _Absent = value;
                this.OnPropertyChanged("Absent");
            }
        }
        #endregion

        #region "Constructor"
        public UCMyProfessionalCareer()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Command"
        private ICommand _SaveNewCareerCommand;
        public ICommand SaveNewCareerCommand
        {
            get
            {
                if (_SaveNewCareerCommand == null)
                {
                    _SaveNewCareerCommand = new RelayCommand(param => SaveNewCareer());
                }
                return _SaveNewCareerCommand;
            }
        }

        private ICommand _CancelNewCareerCommand;
        public ICommand CancelNewCareerCommand
        {
            get
            {
                if (_CancelNewCareerCommand == null)
                {
                    _CancelNewCareerCommand = new RelayCommand(param => CancelNewCareer());
                }
                return _CancelNewCareerCommand;
            }
        }

        private ICommand _SaveEditCareerCommand;
        public ICommand SaveEditCareerCommand
        {
            get
            {
                if (_SaveEditCareerCommand == null)
                {
                    _SaveEditCareerCommand = new RelayCommand(param => SaveEditCareer(param));
                }
                return _SaveEditCareerCommand;
            }
        }

        private ICommand _CancelEditCareerCommand;
        public ICommand CancelEditCareerCommand
        {
            get
            {
                if (_CancelEditCareerCommand == null)
                {
                    _CancelEditCareerCommand = new RelayCommand(param => CancelEditCareer(param));
                }
                return _CancelEditCareerCommand;
            }
        }
        #endregion

        #region "Event Handler"
        //private void saveEditCareerButton_Click(object sender, RoutedEventArgs e) //update a work
        //{
        //    saveEditCareer(sender);
        //}        

        private void DeleteCareerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (myWorksbyId == null) RingDictionaries.Instance.WORK_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out myWorksbyId);
                //if (ConfirmResultMsgBox(NotificationMessages.PC_ASK_DELETE_PROFESSION))
                //{
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this work"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
                if (isTrue)
                {
                    Control control = (Control)sender;
                    SingleWorkModel workModel = (SingleWorkModel)control.DataContext;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        bool _Success = false;
                        string _Msg = string.Empty;

                        JObject pakToSend = new JObject();
                        pakToSend[JsonKeys.UUID] = workModel.WId;
                        (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_REMOVE_WORK, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _Success, out msg);
                        if (_Success)
                        {
                            if (myWorksbyId.Remove(workModel.WId))
                            {
                                if (RingIDViewModel.Instance.MyWorkModelsList.Where(P => P.WId == workModel.WId).FirstOrDefault() != null)
                                {
                                    RingIDViewModel.Instance.MyWorkModelsList.Remove(workModel);
                                }
                                WorkAbsentPanelVisibility();
                            }
                            else UIHelperMethods.ShowFailed("Can't delete career!", "Delete career");
                            //workModel.WVisibilityEditMode = Visibility.Collapsed;
                            workModel.WVisibilityViewMode = Visibility.Visible;
                            //workModel.WErrorString = "";
                        }
                        else if (!string.IsNullOrEmpty(_Msg)) UIHelperMethods.ShowFailed(_Msg, "Delete professional info");
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: DeleteCareerClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CurrentWork_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                SingleWorkModel workModel = (SingleWorkModel)control.DataContext;
                workModel.WToTime_EditMode = DateTime.Now.ToShortDateString();
                workModel.WToTime_ViewMode = "Present";
                workModel.WToTime = 0;
                workModel.WVisibilityDate = Visibility.Visible;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void WorkFromTime_Selected(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                SingleWorkModel workModel = (SingleWorkModel)control.DataContext;
                if (workModel.WVisibilityViewMode == Visibility.Collapsed)
                {
                    var picker = sender as DatePicker;
                    DateTime? ft = picker.SelectedDate;
                    workModel.WFromTime = ft != null ? ModelUtility.MillisFromDateTimeSince1970(ft) : 1;
                    workModel.WFromTime_ViewMode = ft != null ? ft.Value.ToString("dd-MMM-yyyy") : "";
                    workModel.WVisibilityDate = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: WorkFromTime_Selected() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void WorkToTime_Selected(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                SingleWorkModel workModel = (SingleWorkModel)control.DataContext;
                if (workModel.WVisibilityViewMode == Visibility.Collapsed)
                {
                    var picker = sender as DatePicker;
                    DateTime? tt = picker.SelectedDate;
                    workModel.WToTime = tt != null ? ModelUtility.MillisFromDateTimeSince1970(tt) : 1;
                    workModel.WToTime_ViewMode = tt != null ? tt.Value.ToString("dd-MMM-yyyy") : "";
                    workModel.WVisibilityDate = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: WorkToTime_Selected() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //private void CancelEditCareerClick(object sender, RoutedEventArgs e)
        //{
        //    CancelEditCareer(sender);
        //}

        private void EditCareerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                SingleWorkModel workModel = (SingleWorkModel)control.DataContext;
                //workModel.WVisibilityEditMode = Visibility.Visible;
                workModel.WVisibilityViewMode = Visibility.Collapsed;
                //workModel.WErrorString = string.Empty;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //private void cancelCareerButton_Click(object sender, RoutedEventArgs e)
        //{
        //    CancelNewCareer();
        //}

        //private void SaveNewCareerClick(object sender, RoutedEventArgs e)
        //{
        //    SaveNewCareer();
        //}       

        private void careerContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Control control = (Control)sender;
            if (sender != null && control is ContextMenu)
            {
                SingleWorkModel careerModel = (SingleWorkModel)control.DataContext;
                if (control.IsVisible)
                {
                    careerModel.SettingAboutButtonVisibilty = Visibility.Visible;
                }
                else
                {
                    careerModel.SettingAboutButtonVisibilty = Visibility.Hidden;
                }
            }
        }
        #endregion

        #region "Private Method"
        private void TrimInput(SingleWorkModel workModel)
        {
            workModel.WCompanyName = workModel.WCompanyName.Trim();
            workModel.WPosition = workModel.WPosition.Trim();
            workModel.WCity = workModel.WCity.Trim();
            workModel.WDescription = workModel.WDescription.Trim();
        }

        private void ResetComponent()
        {
            AddCarrerModel = null;
            comapnyNameBox.Text = string.Empty;
            comapnyPositionBox.Text = string.Empty;
            cityBox.Text = string.Empty;
            descriptionBox.Text = string.Empty;
            currentCareerComboBox.Text = string.Empty;
            CareerComboBoxTo.Text = string.Empty;
            CareerComboBoxTo.Visibility = Visibility.Visible;
            careerCheckBox.IsChecked = true;
            careerAddWrapper.Visibility = Visibility.Collapsed;
        }

        private void TakeInputFromControl()
        {
            if (AddCarrerModel == null) AddCarrerModel = new SingleWorkModel();
            AddCarrerModel.WCompanyName = comapnyNameBox.Text.Trim();
            AddCarrerModel.WPosition = comapnyPositionBox.Text.Trim();
            AddCarrerModel.WCity = cityBox.Text.Trim();
            AddCarrerModel.WDescription = descriptionBox.Text.Trim();
            AddCarrerModel.WFromTime = currentCareerComboBox.SelectedDate.HasValue ? ModelUtility.MillisFromDateTimeSince1970(currentCareerComboBox.SelectedDate.Value) : 1;
            if (careerCheckBox.IsChecked == true)
                AddCarrerModel.WToTime = 0;
            else
                AddCarrerModel.WToTime = CareerComboBoxTo.SelectedDate.HasValue ? ModelUtility.MillisFromDateTimeSince1970(CareerComboBoxTo.SelectedDate.Value) : 1;
        }

        private bool VerifyCarrer(SingleWorkModel wModel)
        {
            bool NoError = true;

            //CompanyName
            if (string.IsNullOrEmpty(wModel.WCompanyName))
            {
                wModel.WCompanyErrorString = NotificationMessages.PC_COMAPNY_NAME_REQUIRED;
                NoError = false;
            }
            else
            {
                wModel.WCompanyErrorString = string.Empty;
            }

            //Position
            if (string.IsNullOrEmpty(wModel.WPosition))
            {
                wModel.WPostionErrorString = NotificationMessages.PC_POSITION_REQUIRED;
                NoError = false;
            }
            else
            {
                wModel.WPostionErrorString = string.Empty;
            }

            //FromDate
            if (wModel.WFromTime == 1)
            {
                wModel.WFromDateErrorString = NotificationMessages.ABOUT_START_DATE_REQUIRED;
                NoError = false;
            }
            else
            {
                wModel.WFromDateErrorString = string.Empty;
            }

            //ToDate
            if (wModel.WToTime == 1)
            {
                wModel.WToDateErrorString = NotificationMessages.ABOUT_END_DATE_REQUIRED;
                NoError = false;
            }
            else
            {
                wModel.WToDateErrorString = string.Empty;
            }

            if (wModel.WFromTime != 1)
            {
                DateTime t2 = DateTime.Today;
                if (wModel.WToTime > 1)
                {
                    t2 = ModelUtility.DateTimeFromMillisSince1970(wModel.WToTime).Date;
                }
                if (ModelUtility.DateTimeFromMillisSince1970(wModel.WFromTime).Date.CompareTo(t2) > 0)
                {
                    wModel.WFromDateErrorString = NotificationMessages.ABOUT_START_DAY_MUST_EARLY;
                    NoError = false;
                }
                else
                {
                    wModel.WFromDateErrorString = string.Empty;
                }
            }
            return NoError;
        }

        private string TimesCompare(string ft, string tt)
        {
            if (string.IsNullOrEmpty(ft) && string.IsNullOrEmpty(tt)) return string.Empty;
            else
            {
                if (!string.IsNullOrEmpty(ft) && !string.IsNullOrEmpty(tt))
                {
                    DateTime dt1 = Convert.ToDateTime(ft);
                    DateTime dt2 = Convert.ToDateTime(tt);
                    if (dt1.CompareTo(dt2) < 0) return string.Empty;
                    else return NotificationMessages.ABOUT_END_DATE_MUST_GREATER_THAN_START_DATE;
                }
                else
                {
                    return NotificationMessages.ABOUT_START_DATE_END_DATE_REQUIRED_OR_NONE;
                }
            }
        }

        private void SaveNewCareer()
        {
            try
            {
                TakeInputFromControl();
                if (VerifyCarrer(AddCarrerModel))
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        bool _Success = false;
                        string _Msg = string.Empty;
                        DefaultSettings.TEMP_WorkObject = new WorkDTO { City = AddCarrerModel.WCity, Description = AddCarrerModel.WDescription, CompanyName = AddCarrerModel.WCompanyName, Position = AddCarrerModel.WPosition };
                        DefaultSettings.TEMP_WorkObject.FromTime = AddCarrerModel.WFromTime;
                        DefaultSettings.TEMP_WorkObject.ToTime = AddCarrerModel.WToTime;

                        JObject pakToSend = SendDataToServer.AddWorkInfo(DefaultSettings.TEMP_WorkObject);
                        (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_ADD_WORK, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _Success, out _Msg);
                        if (_Success) ResetComponent();
                        else
                        {
                            if (!string.IsNullOrEmpty(_Msg)) UIHelperMethods.ShowFailed(_Msg, "Save professional info");
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CancelNewCareer()
        {
            try
            {
                ResetComponent();
                WorkAbsentPanelVisibility();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SaveEditCareer(object param)
        {
            try
            {
                //Control control = (Control)sender;
                SingleWorkModel workModel = (SingleWorkModel)param;//SingleWorkModel workModel = (SingleWorkModel)control.DataContext;
                TrimInput(workModel);
                if (myWorksbyId == null) RingDictionaries.Instance.WORK_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out myWorksbyId);
                WorkDTO work = myWorksbyId[workModel.WId];
                if (workModel.WCompanyName.Equals(work.CompanyName) && workModel.WDescription.Equals(work.Description) && workModel.WCity.Equals(work.City)
                    && workModel.WPosition.Equals(work.Position) && workModel.WFromTime == work.FromTime && workModel.WToTime == work.ToTime)
                {
                    workModel.WCompanyErrorString = NotificationMessages.ABOUT_NO_CHANGE;
                }
                else
                {
                    if (VerifyCarrer(workModel))
                    {
                        WorkDTO upDatedWork = new WorkDTO { Id = work.Id, City = workModel.WCity, Position = workModel.WPosition, Description = workModel.WDescription, CompanyName = workModel.WCompanyName, FromTime = workModel.WFromTime, ToTime = workModel.WToTime };
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            bool _SuccessWorkUpdate = false;
                            string _MsgWorkUpdate = string.Empty;

                            JObject workObject = new JObject();
                            workObject[JsonKeys.WorkId] = upDatedWork.Id;
                            workObject[JsonKeys.CompanyName] = upDatedWork.CompanyName;
                            if (upDatedWork.Position != null) workObject[JsonKeys.Position] = upDatedWork.Position;
                            if (upDatedWork.City != null) workObject[JsonKeys.City] = upDatedWork.City;
                            if (upDatedWork.Description != null) workObject[JsonKeys.Description] = upDatedWork.Description;
                            workObject[JsonKeys.FromTime] = upDatedWork.FromTime;
                            workObject[JsonKeys.ToTime] = upDatedWork.ToTime;

                            JObject pakToSend = new JObject();
                            pakToSend[JsonKeys.WorkObj] = workObject;
                            (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_UPDATE_WORK, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _SuccessWorkUpdate, out _MsgWorkUpdate);
                            if (_SuccessWorkUpdate)
                            {
                                myWorksbyId[workModel.WId] = upDatedWork;
                                //workModel.WVisibilityEditMode = Visibility.Collapsed;
                                workModel.WVisibilityViewMode = Visibility.Visible;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(_MsgWorkUpdate)) UIHelperMethods.ShowFailed(_MsgWorkUpdate, "Save professional info");
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: saveEditCareerButton_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CancelEditCareer(object param)
        {
            try
            {
                //Control control = (Control)sender;
                SingleWorkModel workModel = (SingleWorkModel)param;//SingleWorkModel workModel = (SingleWorkModel)control.DataContext;
                if (myWorksbyId == null) RingDictionaries.Instance.WORK_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out myWorksbyId);
                WorkDTO work = myWorksbyId[workModel.WId];
                workModel.LoadData(work);
                //workModel.WVisibilityEditMode = Visibility.Collapsed;
                workModel.WVisibilityViewMode = Visibility.Visible;
                //workModel.WErrorString = string.Empty;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Public Method"
        public void ShowMyWorksList()
        {
            try
            {
                lock (RingDictionaries.Instance.WORK_DICTIONARY)
                {
                    if (RingDictionaries.Instance.WORK_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out myWorksbyId))
                    {
                        Absent = Visibility.Collapsed;
                        foreach (WorkDTO work in myWorksbyId.Values)
                        {
                            SingleWorkModel workModel = new SingleWorkModel();
                            workModel.LoadData(work);
                            if (!RingIDViewModel.Instance.MyWorkModelsList.Any(p => p.WId == workModel.WId))
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    RingIDViewModel.Instance.MyWorkModelsList.Add(workModel);
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ShowMyWorksList() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void WorkAbsentPanelVisibility()
        {
            Absent = (RingIDViewModel.Instance.MyWorkModelsList.Count > 0 || careerAddWrapper.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
        }

        public void ShowAddCareerPanel()
        {
            Absent = Visibility.Collapsed;
            careerAddWrapper.Visibility = Visibility.Visible;
            AddCarrerModel = new SingleWorkModel();
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Loaded_Unloaded Events"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            //saveCareerButton.Click += SaveNewCareerClick;
            //cancelCareerButton.Click += cancelCareerButton_Click;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            //saveCareerButton.Click -= SaveNewCareerClick;
            //cancelCareerButton.Click -= cancelCareerButton_Click;
        }
        #endregion
    }
}

﻿using System;
using System.ComponentModel;
using System.Dynamic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Auth.Service.Images;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Images;
using View.Utility.WPFMessageBox;
using WpfAnimatedGif;
using View.UI.PopUp;
using View.UI.ImageViewer;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCProfilePictureUpload.xaml
    /// </summary>
    public partial class UCProfilePictureUpload : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCProfilePictureUpload).Name);

        #region "Private Member"
        private double cropX;
        private double cropY;
        private double imageWidth;
        private double imageHeight;
        private string fileName;
        private BitmapImage bitmapImage;
        private Image uiImage;
        private Point mousePosition;
        private byte[] postData;
        private bool isClicked;
        private double rectPositionX;
        private double rectPositionY;
        private double zoomLimit;
        private const double zoomValue = 0.2;
        private BackgroundWorker bgworker = null, bgworkerUpload = null;
        //private string imageUrl;
        private string convertedUrl;
        private string imageDirectory;
        private ImageModel objImgModel;

        private double imageWidthOriginal;
        private double imageHeightOriginal;
        private byte[] postDataOriginal;
        private double scalRatio;
        private int responseCode = 0;
        Grid motherGrid;
        #endregion

        #region "Property"
        private bool _EnablePanel = true;
        public bool EnablePanel
        {
            get
            {
                return _EnablePanel;
            }
            set
            {
                if (_EnablePanel == value)
                {
                    return;
                }
                _EnablePanel = value;
                this.OnPropertyChanged("EnablePanel");
            }
        }
        private ImageSource _pstLoader;
        public ImageSource pstLoader
        {
            get
            {
                return _pstLoader;
            }
            set
            {
                if (value == _pstLoader)
                    return;
                _pstLoader = value;
                OnPropertyChanged("pstLoader");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Constructor"
        public UCProfilePictureUpload(Grid motherGrid)
        {
            InitializeComponent();
            this.motherGrid = motherGrid;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, ViewConstants.POP_UP_BG_BORDER2, 1.0);
            SetParent(motherGrid);
            this.DataContext = this;
            this.Loaded += UserControlLoaded;
            this.Unloaded += UserControlUnloaded;
            CalculateRectangleXYPositon();
        }
        #endregion

        #region "Command"
        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(param => OnCloseCommand());
                }
                return _CloseCommand;
            }
        }

        private ICommand _SavePictureCommand;
        public ICommand SavePictureCommand
        {
            get
            {
                if (_SavePictureCommand == null)
                {
                    _SavePictureCommand = new RelayCommand(param => OnSavePictureCommand());
                }
                return _SavePictureCommand;
            }
        }
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            Hide();
        }
        #endregion

        #region "Event Handler"
        private void CanvasMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //uiImage = e.Source as Image;

                if (uiImage != null && _Canvas.CaptureMouse() && this.btnSavePicture.IsEnabled)
                {
                    this.isClicked = true;
                    mousePosition = e.GetPosition(_Canvas);
                    Panel.SetZIndex(uiImage, 1); // in case of multiple images
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CanvasMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (uiImage != null)
                {
                    this.isClicked = false;
                    _Canvas.ReleaseMouseCapture();
                    Panel.SetZIndex(uiImage, 0);
                    //uiImage = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CanvasMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (uiImage != null && this.btnSavePicture.IsEnabled && this.isClicked)
                {
                    var position = e.GetPosition(_Canvas);
                    var offset = position - mousePosition;
                    mousePosition = position;
                    double MinX = rectPositionX;
                    double MinY = rectPositionY;
                    double MaxX = MinX + RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT;
                    double MaxY = MinY + RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT;


                    if ((Canvas.GetLeft(uiImage) + offset.X <= MinX && Canvas.GetLeft(uiImage) + offset.X + imageWidth >= MaxX) &&
                         (Canvas.GetTop(uiImage) + offset.Y <= MinY && Canvas.GetTop(uiImage) + offset.Y + imageHeight >= MaxY))
                    {
                        double left = Canvas.GetLeft(uiImage) + offset.X;
                        double top = Canvas.GetTop(uiImage) + offset.Y;
                        Canvas.SetLeft(uiImage, left);
                        Canvas.SetTop(uiImage, top);
                        GetXYPositionOfCropWindow(left, top);
                        _Canvas.UpdateLayout();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CanvasMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (uiImage != null && this.btnSavePicture.IsEnabled)
                {
                    if (e.Delta > 0 && zoomLimit < 5)
                    {
                        zoomLimit++;
                        sliderZoomControl.Value++;
                    }
                    else if (e.Delta < 0 && zoomLimit > 0)
                    {
                        zoomLimit--;
                        sliderZoomControl.Value--;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void sliderZoomControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            zoomLimit = sliderZoomControl.Value;
            ZoomInOutImage();
        }

        private void btnZoomInOut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var btn = (Button)sender;
                if (btn.Name.Equals("btnZoomIn") && zoomLimit < 5)
                {
                    zoomLimit++;
                    sliderZoomControl.Value++;
                }
                else if (btn.Name.Equals("btnZoomOut") && zoomLimit > 0)
                {
                    zoomLimit--;
                    sliderZoomControl.Value--;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Private Method"
        private void ResetAll()
        {
            try
            {
                this.cropX = 0;
                this.cropY = 0;
                this.imageWidth = 0;
                this.imageHeight = 0;
                this.bitmapImage = null;
                this.uiImage = null;
                this.isClicked = false;
                this.zoomLimit = 0;
                this.fileName = null;
                objImgModel = null;
                imageWidthOriginal = 0;
                imageHeightOriginal = 0;
                scalRatio = 0;
                //this.imageUrl = string.Empty;
                this.convertedUrl = string.Empty;
                this.imageDirectory = null;
                sliderZoomControl.Value = 0;
                _Canvas.Children.Clear();
                zoomDockPanel.IsEnabled = true;
                zoomDockPanel.Visibility = Visibility.Collapsed;
                this.btnSavePicture.IsEnabled = false;
                this.btnCancel.IsEnabled = true;
                this.Loading_upload.Visibility = Visibility.Collapsed;
                EnablePanel = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DisposeLoader()
        {
            if (pstLoader != null)
            {
                GC.SuppressFinalize(pstLoader);
                pstLoader = null;
            }
        }

        private void CalculateRectangleXYPositon()
        {
            rectPositionX = (RingIDSettings.PROFILE_PIC_SCREEN_WIDTH - RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT) / 2;
            rectPositionY = (RingIDSettings.PROFILE_PIC_SCREEN_WIDTH - RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT) / 2;
        }

        private void SetProfileImageToCanvas()
        {
            try
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    SetLoadingGifToCanvas();

                    new Thread(() =>
                    {
                        try
                        {
                            System.Drawing.Bitmap bitmap = (System.Drawing.Bitmap)ImageUtility.GetOrientedImageFromFilePath(fileName);//ImageUtility.GetBitmapOfDynamicResource(fileName);
                            if (bitmap.Width < RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH || bitmap.Height < RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH)
                            {
                                bitmap.Dispose();
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    OnCloseCommand();
                                    UIHelperMethods.ShowFailed("Profile photo's minimum resulotion: " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH + " x " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH, "Set profile image");
                                }, System.Windows.Threading.DispatcherPriority.Send);
                            }
                            else
                            {
                                if (bitmap.Width >= RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH && bitmap.Height >= RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH)
                                    postDataOriginal = ImageUtility.ReduceQualityAndSize(bitmap, RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH, 85, SettingsConstants.TYPE_PROFILE_IMAGE);
                                else
                                    postDataOriginal = ImageUtility.ConvertBitmapToByteArray(bitmap);

                                BitmapImage bitmapImageOriginal = ImageUtility.ConvertByteArrayToBitmapImage(postDataOriginal);
                                imageWidthOriginal = bitmapImageOriginal.Width;
                                imageHeightOriginal = bitmapImageOriginal.Height;

                                int quality = 100;
                                postData = ImageUtility.ReduceQualityAndSize(bitmap, RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT, quality, SettingsConstants.TYPE_PROFILE_IMAGE);
                                bitmap.Dispose();
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    LoadImageToCanvas();
                                }, System.Windows.Threading.DispatcherPriority.Send);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                        }
                    }).Start();
                }
                else
                {
                    SetOriginalProfileImageToCanvas();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LoadImageToCanvas()
        {
            try
            {
                bitmapImage = ImageUtility.ConvertByteArrayToBitmapImage(postData);
                uiImage = new Image { Source = bitmapImage };
                imageWidth = bitmapImage.Width;
                imageHeight = bitmapImage.Height;

                double left = (_Canvas.Width - imageWidth) / 2;
                Canvas.SetLeft(uiImage, left);

                double top = (_Canvas.Height - imageHeight) / 2;
                Canvas.SetTop(uiImage, top);

                GetXYPositionOfCropWindow(left, top);
                _Canvas.Children.Clear();
                _Canvas.Children.Add(uiImage);

                zoomDockPanel.Visibility = Visibility.Visible;
                this.btnSavePicture.IsEnabled = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetUnknownImageToCanvas()
        {
            try
            {
                uiImage = new Image { Source = ImageObjects.PROFILE_IMAGE_BOX_UNKNOWN };
                Canvas.SetLeft(uiImage, rectPositionX + (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT / 2) - (uiImage.Source.Width / 2));
                Canvas.SetTop(uiImage, rectPositionY + (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT / 2) - (uiImage.Source.Height) / 2);
                _Canvas.Children.Clear();
                _Canvas.Children.Add(uiImage);

                this.btnSavePicture.IsEnabled = false;
                this.btnCancel.IsEnabled = true;
                this.Loading_upload.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetLoadingGifToCanvas()
        {
            try
            {
                BitmapImage image = ImageUtility.GetBitmapImage(ImageLocation.LOADER_CLOCK);
                uiImage = new Image { Source = image };
                ImageBehavior.SetAnimatedSource(uiImage, image);

                Canvas.SetLeft(uiImage, rectPositionX + (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT / 2) - (image.Width / 2));
                Canvas.SetTop(uiImage, rectPositionY + (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT / 2) - (image.Height) / 2);
                _Canvas.Children.Clear();
                _Canvas.Children.Add(uiImage);

                this.btnSavePicture.IsEnabled = false;
                this.btnCancel.IsEnabled = true;
                this.Loading_upload.Visibility = Visibility.Collapsed;
                GC.SuppressFinalize(image);
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DownloadPreviousImageToCanvas()
        {
            try
            {
                if (bgworker == null)
                {
                    bgworker = new BackgroundWorker();
                    bgworker.DoWork += (s, e) =>
                    {
                        e.Result = ImageUtility.DownloadImage(convertedUrl.Replace('_', '/'), imageDirectory + System.IO.Path.DirectorySeparatorChar + convertedUrl);
                        //e.Result = _BitmapImage;
                    };
                    bgworker.RunWorkerCompleted += (s, e) =>
                    {
                        try
                        {
                            string newImageUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                            System.Drawing.Bitmap img = ImageUtility.GetBitmapOfDynamicResource(newImageUrl);

                            if (e.Result != null && e.Result is bool && (bool)e.Result && img != null)
                            {
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    postDataOriginal = postData = ms.ToArray();
                                }
                                if (img.Width >= RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH && img.Height >= RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH)
                                    postDataOriginal = ImageUtility.ReduceQualityAndSize(img, RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH, 85, SettingsConstants.TYPE_PROFILE_IMAGE);

                                BitmapImage bitmapImageOriginal = ImageUtility.ConvertByteArrayToBitmapImage(postDataOriginal);
                                imageWidthOriginal = bitmapImageOriginal.Width;
                                imageHeightOriginal = bitmapImageOriginal.Height;

                                int quality = 100;
                                postData = ImageUtility.ReduceQualityAndSize(img, RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT, quality, SettingsConstants.TYPE_PROFILE_IMAGE);
                                img.Dispose();
                                LoadImageToCanvas();
                            }
                            else
                            {
                                SetUnknownImageToCanvas();
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                        }
                    };
                }
                bgworker.WorkerReportsProgress = true;
                bgworker.WorkerSupportsCancellation = true;

                if (bgworker.IsBusy != true)
                {
                    bgworker.RunWorkerAsync();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetOriginalProfileImageToCanvas()
        {
            try
            {
                if (objImgModel != null)
                {
                    convertedUrl = HelperMethods.ConvertUrlToName(objImgModel.ImageUrl, ImageUtility.IMG_FULL);
                    string newImageUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    System.Drawing.Bitmap existingImage = ImageUtility.GetBitmapOfDynamicResource(newImageUrl);

                    if (existingImage != null)
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            existingImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            postDataOriginal = postData = ms.ToArray();
                        }
                        if (existingImage.Width >= RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH && existingImage.Height >= RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH)
                            postDataOriginal = ImageUtility.ReduceQualityAndSize(existingImage, RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH, 85, SettingsConstants.TYPE_PROFILE_IMAGE);

                        BitmapImage bitmapImageOriginal = ImageUtility.ConvertByteArrayToBitmapImage(postDataOriginal);
                        imageWidthOriginal = bitmapImageOriginal.Width;
                        imageHeightOriginal = bitmapImageOriginal.Height;

                        int quality = 100;
                        postData = ImageUtility.ReduceQualityAndSize(existingImage, RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT, quality, SettingsConstants.TYPE_PROFILE_IMAGE);
                        existingImage.Dispose();
                        LoadImageToCanvas();
                    }
                    else
                    {
                        SetLoadingGifToCanvas();
                        DownloadPreviousImageToCanvas();
                    }
                }
                else
                {
                    SetUnknownImageToCanvas();
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in SetOriginalProfileImageToCanvas==>" + e.Message + "\n" + e.StackTrace);
            }
        }

        private void GetImageDirectory(int imageType)
        {
            if (imageType == SettingsConstants.TYPE_COVER_IMAGE)
            {
                imageDirectory = RingIDSettings.TEMP_COVER_IMAGE_FOLDER;
            }
            else if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE)
            {
                imageDirectory = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER;
            }
            else
            {
                imageDirectory = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER;
            }
        }

        private void GetXYPositionOfCropWindow(double left, double top)
        {
            if (left < 0)
                cropX = Math.Abs(left) + rectPositionX;
            else
                cropX = rectPositionX - left;

            if (top < 0)
                cropY = Math.Abs(top) + rectPositionY;
            else
                cropY = rectPositionY - top;

            cropX = Math.Round(cropX / (1 + zoomLimit * zoomValue));
            cropY = Math.Round(cropY / (1 + zoomLimit * zoomValue));
        }
        private int ConvertInttoDouble(double w)
        {
            double val = w * (1 + zoomLimit * zoomValue);
            return (int)(Math.Round(val));
        }

        private void ZoomInOutImage()
        {
            try
            {

                if (bitmapImage != null && zoomLimit >= 0 && zoomLimit < 6)
                {
                    double canvas_left = Canvas.GetLeft(uiImage);
                    double canvas_top = Canvas.GetTop(uiImage);

                    BitmapImage bmpImg = ImageUtility.ConvertBitmapToBitmapImage(ImageUtility.ResizeBitmap(ImageUtility.ConvertBitmapImageToBitmap(bitmapImage),
                        ConvertInttoDouble(bitmapImage.Width), ConvertInttoDouble(bitmapImage.Height)));
                    uiImage = new System.Windows.Controls.Image { Source = bmpImg };

                    double xVal = (bmpImg.Width - imageWidth) / 2;
                    double yVal = (bmpImg.Height - imageHeight) / 2;

                    imageWidth = bmpImg.Width;
                    imageHeight = bmpImg.Height;


                    double new_left = canvas_left - xVal;
                    if (new_left > rectPositionX)//left side boundary
                    {
                        new_left = rectPositionX;
                    }
                    else if (new_left + imageWidth < (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT + rectPositionX))//right side boundary
                    {
                        new_left = (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT + rectPositionX) - imageWidth;
                    }
                    Canvas.SetLeft(uiImage, new_left);

                    double new_top = canvas_top - yVal; ;
                    if (new_top > rectPositionY)//top boundary
                    {
                        new_top = rectPositionY;
                    }
                    else if (new_top + imageHeight < (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT + rectPositionY))//bottom boundary
                    {
                        new_top = (RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT + rectPositionY) - imageHeight;
                    }
                    Canvas.SetTop(uiImage, new_top);

                    GetXYPositionOfCropWindow(Canvas.GetLeft(uiImage), Canvas.GetTop(uiImage));
                    _Canvas.Children.Clear();
                    _Canvas.Children.Add(uiImage);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Public Method"
        public void ShowHandlerDialog(string fileName)
        {
            try
            {
                ResetAll();
                this.fileName = fileName;
                SetProfileImageToCanvas();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public void ShowHandlerDialog(ImageModel imgModel)
        {
            try
            {
                ResetAll();
                this.objImgModel = imgModel;
                //this.imageUrl = imageUrl;
                GetImageDirectory(objImgModel.ImageType);
                SetProfileImageToCanvas();
                /*if (objImgModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                {
                    CustomMessageBox.ShowInformation("Repositioning and Zooming feature is currently off for existing \nprofile picture but you can set it as a profile picture.");
                    EnablePanel = false;
                }*/
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public void OnCloseCommand()
        {
            base.Hide();
            _MainBorder.Focusable = false;
            MainSwitcher.PopupController.PhotoSelectionView._MainBorder.Focusable = true;
            Keyboard.Focus(MainSwitcher.PopupController.PhotoSelectionView._MainBorder);
        }
        #endregion

        #region "BackgroundWorker"
        private void OnSavePictureCommand()
        {
            BeginProfilePictureUpload();
        }


        private void BeginProfilePictureUpload()
        {
            try
            {
                this.zoomDockPanel.IsEnabled = false;
                this.btnCancel.IsEnabled = false;
                this.btnSavePicture.IsEnabled = false;
                this.Loading_upload.Visibility = Visibility.Visible;
                pstLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_CLOCK);
                dynamic state = new ExpandoObject();
                state.width = (int)imageWidthOriginal;//(int)bitmapImage.Width;
                state.height = (int)imageHeightOriginal;//(int)bitmapImage.Height;
                scalRatio = imageWidthOriginal / bitmapImage.Width;
                if (bgworkerUpload == null)
                {
                    bgworkerUpload = new BackgroundWorker();
                    bgworkerUpload.DoWork += new DoWorkEventHandler(ProfilePictureUpload_DoWork);
                    bgworkerUpload.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ProfilePictureUpload_RunWorkerCompleted);
                }
                bgworkerUpload.WorkerReportsProgress = true;
                bgworkerUpload.WorkerSupportsCancellation = true;

                if (bgworkerUpload.IsBusy != true)
                {
                    bgworkerUpload.RunWorkerAsync(state);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ProfilePictureUpload_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                dynamic st = (ExpandoObject)e.Argument;
                string response = null;
                Guid imgId = Guid.Empty;
                double cropXOriginal = Math.Round(cropX * scalRatio);
                double cropYOriginal = Math.Round(cropY * scalRatio);

                int width, height, issa = 0;
                width = (int)((RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT * scalRatio) / (1 + zoomLimit * zoomValue));
                height = (int)((RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT * scalRatio) / (1 + zoomLimit * zoomValue));

                if (postDataOriginal != null)
                {
                    if (objImgModel != null && objImgModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                    {
                        //if (objImgModel.AlbumId == DefaultSettings.PROFILE_IMAGE_ALBUM_ID)//to do
                        imgId = objImgModel.ImageId;
                        string responseUrl = ProfileCoverImageUpload.Instance.UpdateCropImageToImageServerWebRequest(cropXOriginal, cropYOriginal, width, height, objImgModel.ImageUrl);
                        if (!string.IsNullOrEmpty(responseUrl))
                        {
                            issa = 1;
                            if (imgId == DefaultSettings.userProfile.ProfileImageId)
                            {
                                ImageUploadHelper.GetRepositionCropImageUpdate(responseUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER, objImgModel.ImageType);
                                response = responseUrl;
                            }
                            else
                            {
                                dynamic state = new ExpandoObject();
                                state.sucs = true;
                                state.iurl = responseUrl;
                                response = Newtonsoft.Json.JsonConvert.SerializeObject(state);
                            }
                        }
                    }
                    else
                    {
                        response = ProfileCoverImageUpload.Instance.UploadToImageServerWebRequest(postDataOriginal, SettingsConstants.TYPE_PROFILE_IMAGE, cropXOriginal, cropYOriginal, width, height);
                    }
                    //postDataOriginal = null;
                    if (!string.IsNullOrEmpty(response) && (imgId == Guid.Empty || imgId != DefaultSettings.userProfile.ProfileImageId))
                    {
                        //responseCode = ProfileCoverImageUpload.Instance.SendProfileCoverImagePostRequestToAuth(response, st.width, st.height, SettingsConstants.TYPE_PROFILE_IMAGE, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER, cropXOriginal, cropYOriginal, imgId);
                        responseCode = ImageUploadHelper.SendProfileCoverImagePostRequestToAuth(response, st.width, st.height, SettingsConstants.TYPE_PROFILE_IMAGE, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER, cropXOriginal, cropYOriginal, imgId, issa);
                    }
                }
                e.Result = response;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        private void ProfilePictureUpload_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Result is string && !string.IsNullOrEmpty((string)e.Result))
                {
                    if (responseCode > ReasonCodeConstants.REASON_CODE_NONE)
                    {
                        UIHelperMethods.ShowFailed(ReasonCodeConstants.GetReason(responseCode));
                        //   CustomMessageBox.ShowError("Failed! " + ReasonCodeConstants.GetReason(responseCode));
                        this.zoomDockPanel.IsEnabled = true;
                        this.btnCancel.IsEnabled = true;
                        this.btnSavePicture.IsEnabled = true;
                        this.Loading_upload.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        postDataOriginal = null;
                        OnCloseCommand();
                        MainSwitcher.PopupController.PhotoSelectionView.OnCloseCommand();
                        HideMotherPanel();
                    }
                }
                else
                {
                    UIHelperMethods.ShowFailed("Failed to upload profile image! ", "Upload profile image");
                    this.zoomDockPanel.IsEnabled = true;
                    this.btnCancel.IsEnabled = true;
                    this.btnSavePicture.IsEnabled = true;
                    this.Loading_upload.Visibility = Visibility.Collapsed;
                }
                DisposeLoader();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Utility Methods"
        private void HideMotherPanel()
        {
            if (ImageViewInMain != null) ImageViewInMain.Hide();
        }

        private UCImageViewInMain ImageViewInMain
        {
            get
            {
                var obj = ((Border)(motherGrid.Parent)).Parent;
                if (obj != null && obj is UCImageViewInMain) return (UCImageViewInMain)obj;
                return null;
            }
        }
        #endregion

        #region "Loaded_Unloaded Events"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
            _Canvas.MouseMove += CanvasMouseMove;
            _Canvas.MouseLeftButtonUp += CanvasMouseLeftButtonUp;
            _Canvas.MouseLeftButtonDown += CanvasMouseLeftButtonDown;
            _Canvas.MouseWheel += CanvasMouseWheel;
            btnZoomOut.Click += btnZoomInOut_Click;
            btnZoomIn.Click += btnZoomInOut_Click;
            sliderZoomControl.ValueChanged += sliderZoomControl_ValueChanged;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            _Canvas.MouseMove -= CanvasMouseMove;
            _Canvas.MouseLeftButtonUp -= CanvasMouseLeftButtonUp;
            _Canvas.MouseLeftButtonDown -= CanvasMouseLeftButtonDown;
            _Canvas.MouseWheel -= CanvasMouseWheel;
            btnZoomOut.Click -= btnZoomInOut_Click;
            btnZoomIn.Click -= btnZoomInOut_Click;
            sliderZoomControl.ValueChanged -= sliderZoomControl_ValueChanged;
        }
        #endregion
    }
}

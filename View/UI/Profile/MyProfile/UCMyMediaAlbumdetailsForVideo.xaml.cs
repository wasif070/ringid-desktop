﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using Models.Entity;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMediaAlbumDetailsForVideo.xaml
    /// </summary>
    public partial class UCMyMediaAlbumdetailsForVideo : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyMediaAlbumdetailsForVideo).Name);
        public string ButtonView = string.Empty;
        public MediaContentDTO mediaDto;
        public UCMyMediaAlbumdetailsForVideo()
        {
            InitializeComponent();
            this.DataContext = this;
            CategoryAlbum = 2; // abum =1, singlemedia =  2;
            this.FriendIdentity = DefaultSettings.LOGIN_RING_ID;
            this.Loaded += UCMyMediaAlbumdetailsForVideo_Loaded;
            this.Unloaded += UCMyMediaAlbumdetailsForVideo_Unloaded;
        }

        void UCMyMediaAlbumdetailsForVideo_Loaded(object sender, RoutedEventArgs e)
        {
            BtnPlayAll.Click += PlayAll_Click;
            showMoreText.Click += ShowMore_PanelClick;
        }

        void UCMyMediaAlbumdetailsForVideo_Unloaded(object sender, RoutedEventArgs e)
        {
            BtnPlayAll.Click -= PlayAll_Click;
            showMoreText.Click -= ShowMore_PanelClick;
            closeTheWidnow_Click(null, null);
        }

        #region"Property "
        private long _FriendIdentity;
        public long FriendIdentity
        {
            get { return _FriendIdentity; }
            set
            {
                _FriendIdentity = value;
                if (FriendIdentity == value) { return; }
                _FriendIdentity = value;
                OnPropertyChanged("FriendIdentity");
            }
        }
        private int _CategoryAlbum;
        public int CategoryAlbum
        {
            get { return _CategoryAlbum; }
            set
            {
                _CategoryAlbum = value;
                if (_CategoryAlbum == value) { return; }
                _CategoryAlbum = value;
                OnPropertyChanged("CategoryAlbum");
            }
        }
        private ObservableCollection<SingleMediaModel> _MySingleMediaVedios;
        public ObservableCollection<SingleMediaModel> MySingleMediaVedios
        {
            get
            {
                return _MySingleMediaVedios;
            }
            set
            {
                _MySingleMediaVedios = value;
                this.OnPropertyChanged("MySingleMediaVedios");
            }
        }
        private string _AlbumName = string.Empty;
        public string AlbumName
        {
            get { return _AlbumName; }
            set
            {
                _AlbumName = value;
                this.OnPropertyChanged("AlbumName");
            }
        }
        private Guid _AlbumId;
        public Guid AlbumId
        {
            get { return _AlbumId; }
            set
            {
                if (_AlbumId == value) return;
                _AlbumId = value;
                OnPropertyChanged("AlbumId");
            }
        }
        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (_MediaType == value) return;
                _MediaType = value;
                OnPropertyChanged("MediaType");
            }
        }
        private Visibility _IsPlayButtonVisible = Visibility.Hidden;
        public Visibility IsPlayButtonVisible
        {
            get { return _IsPlayButtonVisible; }
            set
            {
                if (_IsPlayButtonVisible == value) return;
                _IsPlayButtonVisible = value;
                OnPropertyChanged("IsPlayButtonVisible");
            }
        }


        private System.Windows.Media.ImageSource _LoaderSmall;
        public System.Windows.Media.ImageSource LoaderSmall
        {
            get
            {
                return _LoaderSmall;
            }
            set
            {
                if (value == _LoaderSmall)
                    return;
                _LoaderSmall = value;
                OnPropertyChanged("LoaderSmall");
            }
        }
        private bool _IsNoAlbumFound = false;
        public bool IsNoAlbumFound
        {
            get
            {
                return _IsNoAlbumFound;
            }
            set
            {
                if (value == _IsNoAlbumFound)
                    return;
                _IsNoAlbumFound = value;
                OnPropertyChanged("IsNoAlbumFound");
            }
        }

        private bool _IsNeeedToLoading = false;
        public bool IsNeeedToLoading
        {
            get
            {
                return _IsNeeedToLoading;
            }
            set
            {
                if (value == _IsNeeedToLoading)
                    return;
                _IsNeeedToLoading = value;
                OnPropertyChanged("IsNeeedToLoading");
            }
        }
        #endregion "Property "
        public void ShowLoader(bool neeedToLoading)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (neeedToLoading)
                {
                    IsNeeedToLoading = true;
                    LoaderSmall = ImageUtility.GetBitmapImage(View.Constants.ImageLocation.LOADER_SMALL);
                    GC.SuppressFinalize(LoaderSmall);
                }
                else
                {
                    IsNeeedToLoading = false;
                    LoaderSmall = null;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }


        //public void LoadData(MediaContentDTO mediaDto)
        //{
        //    MediaContentModel model = RingIDViewModel.Instance.MyVideoAlbums.Where(P => P.AlbumId == mediaDto.AlbumId).FirstOrDefault();
        //    if (model == null) model = new MediaContentModel();
        //    if (model.MediaList == null || (model.MediaList.Count != mediaDto.TotalMediaCount))
        //    {
        //        model.LoadData(mediaDto);
        //    }
        //    MySingleMediaVedios = model.MediaList;
        //    if (mediaDto.TotalMediaCount > 0)
        //    {
        //        IsPlayButtonVisible = Visibility.Visible;
        //    }
        //    HideOrShowMoreVisibility();
        //}
        public void HideOrShowMoreVisibility()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (mediaDto != null && mediaDto.MediaList != null)
                {
                    if (mediaDto.MediaList.Count < mediaDto.TotalMediaCount)
                    {
                        ShowMore();
                    }
                    else
                    {
                        HideShowMoreLoading();
                    }
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private void closeTheWidnow_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyprofileVideoAlbum.ShowAfterVideoAlbumDetailsCancel();
            //UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo = null;
            ClearVideo();
        }

        private void ClearVideo()
        {
            try
            {
                if (MySingleMediaVedios != null && MySingleMediaVedios.Count > 0)
                {
                    DispatcherTimer _RemoveVideo = new DispatcherTimer();
                    _RemoveVideo.Interval = TimeSpan.FromMilliseconds(100);
                    _RemoveVideo.Tick += (s, e) =>
                    {
                        lock (MySingleMediaVedios)
                        {
                            while (MySingleMediaVedios.Count > 0)
                            {
                                SingleMediaModel model = MySingleMediaVedios.FirstOrDefault();
                                MySingleMediaVedios.Remove(model);
                                GC.SuppressFinalize(model);
                            }
                        }
                        _RemoveVideo.Stop();
                    };
                    _RemoveVideo.Stop();
                    _RemoveVideo.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + " " + ex.Message);
            }
        }

        private void PlayAll_Click(object sender, RoutedEventArgs e)
        {
            if (MySingleMediaVedios.Count > 0)
            {
                MediaUtility.RunPlayList(false, Guid.Empty, MySingleMediaVedios, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel);
                //RingPlayerViewModel.Instance.NavigateFrom = "album";
            }
        }
        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            SingleMediaModel model = (SingleMediaModel)btn.DataContext;
            if (MySingleMediaVedios.Count > 0)
            {
                int idx = MySingleMediaVedios.IndexOf(model);
                if (idx >= 0)
                {
                    MediaUtility.RunPlayList(false, Guid.Empty, MySingleMediaVedios, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, idx);
                    //RingPlayerViewModel.Instance.NavigateFrom = "album";
                }
            }
        }

        public void ShowMore()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Visible;
                this.showMoreLoader.Visibility = Visibility.Collapsed;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            }
            catch (System.Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowLoading()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Collapsed;
                this.showMoreLoader.Visibility = Visibility.Visible;
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            }
            catch (System.Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideShowMoreLoading()
        {
            this.showMorePanel.Visibility = Visibility.Collapsed;
            this.showMoreText.Visibility = Visibility.Visible;
            this.showMoreLoader.Visibility = Visibility.Collapsed;
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        }


        private void ShowMore_PanelClick(object sender, RoutedEventArgs e)
        {
            ShowLoading();
            new ThradMediaAlbumContentList().StartThread(MySingleMediaVedios.Count, AlbumId, DefaultSettings.userProfile.UserTableID, MediaType);
        }

        public void ShowThumbView()
        {
            Thumb_View_Panel.Visibility = Visibility.Visible;
            List_View_Panel.Visibility = Visibility.Collapsed;
        }
        public void ShowListView()
        {
            List_View_Panel.Visibility = Visibility.Visible;
            Thumb_View_Panel.Visibility = Visibility.Collapsed;
        }

        public void LoadCurrentView(string ButtonView)
        {
            if (!string.IsNullOrEmpty(ButtonView) && !ButtonView.Equals("Thumbnail_View"))
            {
                ShowListView();
            }
            else
            {
                ShowThumbView();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Auth;
using View.ViewModel;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMyProfileMusicAlbum.xaml
    /// </summary>
    public partial class UCMyProfileMusicAlbum : UserControl, INotifyPropertyChanged
    {

       // public string ButtonView = string.Empty;
        private bool isListView = false;
        public UCMyProfileMusicAlbum()
        {
            InitializeComponent();
            this.DataContext = this;
            if (RingIDViewModel.Instance.MyAudioAlbums.Count == 0)
                this.MusicGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
        }

        private Visibility _NoAudioTxtVisibility = Visibility.Collapsed;
        public Visibility NoAudioTxtVisibility
        {
            get { return _NoAudioTxtVisibility; }
            set
            {
                if (value == _NoAudioTxtVisibility) return;
                _NoAudioTxtVisibility = value;
                this.OnPropertyChanged("NoAudioTxtVisibility");
            }
        }

        private ICommand _AudioAlbumCommand;
        public ICommand AudioAlbumCommand
        {
            get
            {
                if (_AudioAlbumCommand == null)
                {
                    _AudioAlbumCommand = new RelayCommand(param => OnAlbumClick(param));
                }
                return _AudioAlbumCommand;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private void OnAlbumClick(object param)
        {
            try
            {
                MediaContentModel model = (MediaContentModel)param;
                ListViewPanel.Visibility = Visibility.Collapsed;
                ThumbPanel.Visibility = Visibility.Collapsed;

                if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                MusicAlbumsDetailsPanel.Child = UCMediaContentsView.Instance;
                //if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum.IsVisible)
                //{
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum.btnVisibility(true);
                //}
                //UCMediaContentsView.Instance.ParentInstance = this;
                UCMediaContentsView.Instance.ShowAlbumContents(model, () =>
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum.btnVisibility(false);
                    ShowAfterMusicAlbumDetailsCancel();
                    return 0;
                });
            }
            catch (Exception)
            {
            }
        }

        public void ShowAfterMusicAlbumDetailsCancel()
        {
            if (isListView)
            {
                ListViewPanel.Visibility = Visibility.Visible;
                ThumbPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                ListViewPanel.Visibility = Visibility.Collapsed;
                ThumbPanel.Visibility = Visibility.Visible;
            }
            MusicAlbumsDetailsPanel.Child = null;
        }

        public void ShowListOrThumbViewInMusic(bool isListView)
        {
            this.isListView = isListView;
            if (MusicAlbumsDetailsPanel.Child == null)
            {
                if (isListView)
                {
                    ListViewPanel.Visibility = Visibility.Visible;
                    ThumbPanel.Visibility = Visibility.Collapsed;
                }
                else
                {
                    ThumbPanel.Visibility = Visibility.Visible;
                    ListViewPanel.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                //if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio != null)
                //{
                //    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio.LoadCurrentView(ButtonView);
                //}
            }
        }
        public void SetMyAlbumsList()
        {
            if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
            {
                if (MusicGIFCtrl != null && MusicGIFCtrl.IsRunning()) this.MusicGIFCtrl.StopAnimate();
            }
            //else if (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_TABLE_ID) && NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
            //{
            //    NoAudioTxt.Visibility = Visibility.Collapsed;
            //    List<MediaContentDTO> lst = NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Values.ToList();
            //    foreach (var albumDto in lst)
            //    {
            //        if (!RingIDViewModel.Instance.MyAudioAlbums.Any(P => P.AlbumId == albumDto.AlbumId))
            //        {
            //            MediaContentModel albumModel = new MediaContentModel();
            //            albumModel.LoadData(albumDto);
            //            RingIDViewModel.Instance.MyAudioAlbums.Add(albumModel);
            //        }
            //    }
            //    if (MusicGIFCtrl != null && MusicGIFCtrl.IsRunning()) this.MusicGIFCtrl.StopAnimate();
            //}
            else
            {
                NoAudioTxtVisibility = Visibility.Collapsed;
                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    new Thread(() =>
                    {
                        bool success = false;
                        string message = string.Empty;
                        JObject obj = new JObject();
                        obj[JsonKeys.UserTableID] = DefaultSettings.LOGIN_TABLE_ID;
                        obj[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_AUDIO;
                        SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_MEDIA_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                        SetLoaderTextValue(success);                        
                    }).Start();
                }
                else if (MusicGIFCtrl != null && MusicGIFCtrl.IsRunning()) this.MusicGIFCtrl.StopAnimate();
            }
        }

        public void SetLoaderTextValue(bool success)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (MusicGIFCtrl != null && MusicGIFCtrl.IsRunning()) this.MusicGIFCtrl.StopAnimate();
                if (!success && RingIDViewModel.Instance.MyAudioAlbums.Count == 0)
                {
                    NoAudioTxtVisibility = Visibility.Visible;
                    DefaultSettings.MY_AUDIO_ALBUMS_COUNT = 0;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        #region "Event Trigger"
        private void SingleAlbum_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Grid grid = (Grid)sender;
            if (grid.DataContext is MediaContentModel)
            {
                MediaContentModel model = (MediaContentModel)grid.DataContext;
                if (model.IsStaticAlbum == false)
                {
                    if (grid.ContextMenu != ContextMenuAlbumEditOptions.Instance.cntxMenu)
                    {
                        grid.ContextMenu = ContextMenuAlbumEditOptions.Instance.cntxMenu;
                        ContextMenuAlbumEditOptions.Instance.SetupMenuItem();
                    }
                    ContextMenuAlbumEditOptions.Instance.ShowHandler(grid, model);
                    ContextMenuAlbumEditOptions.Instance.cntxMenu.IsOpen = true;
                }
            }
        }
        #endregion
    }
}

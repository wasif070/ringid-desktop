﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using log4net;
using Models.Constants;
using View.Utility.Auth;
using View.ViewModel;
using System.Windows.Input;
using View.Utility;
using View.BindingModels;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for MyProfilePhotosChange.xaml
    /// </summary>
    public partial class MyProfilePhotosChange : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MyProfilePhotosChange).Name);

        #region "Public Member"        
        #endregion

        #region "Constructor"
        public MyProfilePhotosChange()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Event Handler"
        private void ScrollViewer_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }
        #endregion

        #region "Private Method"
        private void LoadMyImageAlbums()
        {
            try
            {
                if((RingIDViewModel.Instance.MyImageAlubms.Count - 1) == DefaultSettings.MY_IMAGE_ALBUMS_COUNT)
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadMyImages() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion
        
        #region "Loaded_Unloaded Events"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            Scroll.PreviewKeyDown += ScrollViewer_PreviewKeyDown;
            LoadMyImageAlbums();
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            Scroll.PreviewKeyDown -= ScrollViewer_PreviewKeyDown;            
        }
        #endregion
        private ICommand _ImageAlbumCommand;
        public ICommand ImageAlbumCommand
        {
            get
            {
                if (_ImageAlbumCommand == null)
                {
                    _ImageAlbumCommand = new RelayCommand(param => OnAlbumClick(param));
                }
                return _ImageAlbumCommand;
            }
        }

        private void OnAlbumClick(object param)
        {
            try
            {
                AlbumModel model = (AlbumModel)param;
                ThumbPanel.Visibility = Visibility.Collapsed;
                if (UCImageContentViewForProfileCoverPhotoChange.Instance == null) UCImageContentViewForProfileCoverPhotoChange.Instance = new UCImageContentViewForProfileCoverPhotoChange();
                else if (UCImageContentViewForProfileCoverPhotoChange.Instance.Parent is Border) ((Border)UCImageContentViewForProfileCoverPhotoChange.Instance.Parent).Child = null;
                ImageAlbumsDetailsPanel.Child = UCImageContentViewForProfileCoverPhotoChange.Instance;
                UCImageContentViewForProfileCoverPhotoChange.Instance.SetValue(model, () =>
                {
                    ThumbPanel.Visibility = Visibility.Visible;
                    ImageAlbumsDetailsPanel.Child = null;
                    return 0;
                });
            }
            catch (Exception)
            {
            }
        }

        private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        {
            if (BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                LoadMorePhotos();
            }
            e.Handled = true;
        }
        public void ShowHideShowMoreLoading(bool makeVisible)
        {
            try
            {
                if (makeVisible)
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                }
                else
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void LoadMorePhotos()
        {
            try
            {
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;                
                SendDataToServer.ListOfImageAlbumsOfUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_IMAGE, RingIDViewModel.Instance.ImageAlbumNpUUId);
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadMorePhotos() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private int _BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
        public int BOTTOM_LOADING
        {
            get { return _BOTTOM_LOADING; }
            set
            {
                _BOTTOM_LOADING = value;
                this.OnPropertyChanged("BOTTOM_LOADING");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

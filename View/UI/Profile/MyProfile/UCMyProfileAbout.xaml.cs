﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using View.BindingModels;
using View.UI.Profile.MyProfile.About;
using View.Utility.Auth;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMyProfileAbout.xaml
    /// </summary>
    public partial class UCMyProfileAbout : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyProfileAbout).Name);
        private string msg = string.Empty;
        #region VARIABLES INIT

        private UCMyBasicInfo ucMyBasicInfo = null;
        private UCMyBasicInfoEdit ucMyBasicInfoEdit = null;
        public UCMyProfessionalCareer ucMyProfessionalCareer = null;
        public UCMyEducation ucMyEducation = null;
        public UCMySkill ucMySkill = null;
        private SingleBasicModelSet _MyBasicSet = new SingleBasicModelSet();
        public SingleBasicModelSet MyBasicSet
        {
            get { return _MyBasicSet; }
            set
            {
                _MyBasicSet = value;
                this.OnPropertyChanged("MyBasicSet");
            }
        }
        private bool isProfileValuesShown = false;
        #endregion VARIABLES INIT

        public UCMyProfileAbout()
        {
            InitializeComponent();
            this.DataContext = this;
            MyBasicSet.SetMySingleBasicSetToDefaultSettingsUserProfile();
            SendDataToServer.WorkEducationSkillRequest(null);

        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            LoadAllChild();
            StackPanelBasic.MouseEnter += basicInfoLbl_MouseEnter;
            StackPanelBasic.MouseLeave += basicInfoLbl_MouseLeave;
            editBasic.Click += editBasicClick;
            StackPanelProCareer.MouseEnter += CareerMouseEnter;
            StackPanelProCareer.MouseLeave += CareerMouseLeave;
            addCareer.Click += AddCareerClick;
            StackPanelEducation.MouseEnter += EducationMouseEnter;
            StackPanelEducation.MouseLeave += EducationMouseLeave;
            addEducation.Click += addEducationClick;
            StackPanelSkill.MouseEnter += Skill_MouseEnter;
            StackPanelSkill.MouseLeave += Skill_MouseLeave;
            addSkill.Click += AddSkillClick;
        }

        private void LoadAllChild()
        {
            if (MyBasicSet.EditModeVisibility == Visibility.Collapsed)
            {
                if (ucMyBasicInfo == null) ucMyBasicInfo = new UCMyBasicInfo(MyBasicSet);
                BasicHolderPanel.Child = ucMyBasicInfo;
            }
            else
            {
                if (ucMyBasicInfoEdit == null) new UCMyBasicInfoEdit(MyBasicSet);
                BasicHolderPanel.Child = ucMyBasicInfoEdit;
            }

            if (ucMyProfessionalCareer == null) ucMyProfessionalCareer = new UCMyProfessionalCareer();
            ucMyProfessionalCareer.ShowMyWorksList();
            ProfessionalCareerHolderPanel.Child = ucMyProfessionalCareer;

            if (ucMyEducation == null) ucMyEducation = new UCMyEducation();
            ucMyEducation.ShowMyEducationsList();
            EducationHolderPanel.Child = ucMyEducation;

            if (ucMySkill == null) ucMySkill = new UCMySkill();
            ucMySkill.ShowMySkillsList();
            SkillHolderPanel.Child = ucMySkill;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            UnloadAllChild();

            StackPanelBasic.MouseEnter -= basicInfoLbl_MouseEnter;
            StackPanelBasic.MouseLeave -= basicInfoLbl_MouseLeave;
            editBasic.Click -= editBasicClick;
            StackPanelProCareer.MouseEnter -= CareerMouseEnter;
            StackPanelProCareer.MouseLeave -= CareerMouseLeave;
            addCareer.Click -= AddCareerClick;
            StackPanelEducation.MouseEnter -= EducationMouseEnter;
            StackPanelEducation.MouseLeave -= EducationMouseLeave;
            addEducation.Click -= addEducationClick;
            StackPanelSkill.MouseEnter -= Skill_MouseEnter;
            StackPanelSkill.MouseLeave -= Skill_MouseLeave;
            addSkill.Click -= AddSkillClick;
        }

        private void UnloadAllChild()
        {
            BasicHolderPanel.Child = null;
            ProfessionalCareerHolderPanel.Child = null;
            EducationHolderPanel.Child = null;
            SkillHolderPanel.Child = null;
        }

        public void ShowMyBasicInfos()
        {
            try
            {
                if (!isProfileValuesShown)
                {
                    MyBasicSet.SetMySingleBasicSetToDefaultSettingsUserProfile();
                    isProfileValuesShown = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMyBasicInfos() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void AddCareerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                addCareer.Visibility = Visibility.Hidden;

                if (ProfessionalCareerHolderPanel.Child == null)
                {
                    if (ucMyProfessionalCareer == null) ucMyProfessionalCareer = new UCMyProfessionalCareer();
                    ucMyProfessionalCareer.ShowMyWorksList();
                    ProfessionalCareerHolderPanel.Child = ucMyProfessionalCareer;
                }
                ucMyProfessionalCareer.ShowAddCareerPanel();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void addEducationClick(object sender, RoutedEventArgs e)
        {
            try
            {
                addEducation.Visibility = Visibility.Collapsed;
                if (EducationHolderPanel.Child == null)
                {
                    if (ucMyEducation == null) ucMyEducation = new UCMyEducation();
                    ucMyEducation.ShowMyEducationsList();
                    EducationHolderPanel.Child = ucMyEducation;
                }

                ucMyEducation.addEducationStackPanel.Visibility = Visibility.Visible;
                ucMyEducation.AddEduModel = new SingleEducationModel();
                ucMyEducation.Absent = Visibility.Collapsed;
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void AddSkillClick(object sender, RoutedEventArgs e)
        {
            try
            {
                addSkill.Visibility = Visibility.Hidden;

                if (SkillHolderPanel.Child == null)
                {
                    if (ucMySkill == null) ucMySkill = new UCMySkill();
                    ucMySkill.ShowMySkillsList();
                    SkillHolderPanel.Child = ucMySkill;
                }
                ucMySkill.ShowAddSkillPanel();
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }


        #region EVENT HANDLERS

        public void ResetBasicInfoPanel()
        {
            MyBasicSet.EditModeVisibility = Visibility.Collapsed;
            ucMyBasicInfo = new UCMyBasicInfo(MyBasicSet);
            BasicHolderPanel.Child = ucMyBasicInfo;
            ucMyBasicInfoEdit = null;
        }
        private Visibility _BasicInfoEditButton = Visibility.Collapsed;
        public Visibility BasicInfoEditButton
        {
            get { return _BasicInfoEditButton; }
            set { _BasicInfoEditButton = value; this.OnPropertyChanged("BasicInfoEditButton"); }
        }
        private void basicInfoLbl_MouseEnter(object sender, MouseEventArgs e)
        {
            if (MyBasicSet.EditModeVisibility == Visibility.Collapsed)
                BasicInfoEditButton = Visibility.Visible;
        }
        private void basicInfoLbl_MouseLeave(object sender, MouseEventArgs e)
        {
            BasicInfoEditButton = Visibility.Collapsed;
        }
        private void editBasicClick(object sender, RoutedEventArgs e)
        {
            try
            {
                BasicInfoEditButton = Visibility.Collapsed;
                ucMyBasicInfoEdit = new UCMyBasicInfoEdit(MyBasicSet);
                BasicHolderPanel.Child = ucMyBasicInfoEdit;
                MyBasicSet.ViewModeVisibility = Visibility.Collapsed;
                MyBasicSet.EditModeVisibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion EVENT HANDLERS

        #region UTILITY METHODS

        //start addINotified
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        //end addINotified
        #endregion UTILITY METHODS

        private void CareerMouseEnter(object sender, MouseEventArgs e)
        {
            if (ucMyProfessionalCareer != null && ucMyProfessionalCareer.careerAddWrapper.Visibility == Visibility.Visible)
            {
                addCareer.Visibility = Visibility.Hidden;
            }
            else
            {
                addCareer.Visibility = Visibility.Visible;
            }
        }

        private void CareerMouseLeave(object sender, MouseEventArgs e)
        {
            addCareer.Visibility = Visibility.Hidden;
        }

        private void EducationMouseEnter(object sender, MouseEventArgs e)
        {
            if (ucMyEducation != null && ucMyEducation.addEducationStackPanel.Visibility == Visibility.Visible)
            {
                addEducation.Visibility = Visibility.Collapsed;
            }
            else
            {
                addEducation.Visibility = Visibility.Visible;
            }
        }
        private void EducationMouseLeave(object sender, MouseEventArgs e)
        {
            addEducation.Visibility = Visibility.Collapsed;
        }

        private void Skill_MouseEnter(object sender, MouseEventArgs e)
        {
            if (ucMySkill != null && ucMySkill.addSkillPanel.Visibility == Visibility.Visible)
            {
                addSkill.Visibility = Visibility.Collapsed;
            }
            else
            {
                addSkill.Visibility = Visibility.Visible;
            }
        }
        private void Skill_MouseLeave(object sender, MouseEventArgs e)
        {
            addSkill.Visibility = Visibility.Collapsed;
        }

    }
}

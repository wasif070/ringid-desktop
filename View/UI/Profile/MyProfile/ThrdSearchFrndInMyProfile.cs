﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.ViewModel;

namespace View.UI.Profile.MyProfile
{
    class ThrdSearchFrndInMyProfile
    {
        public static string searchParm = string.Empty;
        public static string prevString = string.Empty;
        private Int32 SLEEP_TIME = 10;//ms
        private int waiting = 0;
        public static bool runningSearchFriendThread = false;
        private static readonly ILog log = LogManager.GetLogger(typeof(ThrdSearchFrndInMyProfile).Name);

        public void StartThread()
        {
            if (!runningSearchFriendThread)
            {
                runningSearchFriendThread = true;
                Thread trd = new Thread(Run);
                trd.Name = "SearchFriend";
                trd.Start();
            }
        }
        private void Run()
        {
            Search_friends();
        }
        private void addASleep()
        {
            System.Threading.Thread.Sleep(SLEEP_TIME);
        }
        private void Search_friends()
        {
            try
            {
                while (runningSearchFriendThread)
                {
                    if (!string.IsNullOrWhiteSpace(searchParm))
                    {
                        if (string.IsNullOrEmpty(prevString) || !prevString.Equals(searchParm))
                        {
                            waiting = 0;
                            prevString = searchParm;
                            //Thread.Sleep(200);
                            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.ClearSearchFriendList();
                            ProcessSearch();
                        }
                    }
                    else
                    {
                        prevString = null;
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.ClearSearchFriendList();
                    }
                    if (waiting != 0 && waiting % 130 == 0)
                    {
                        break;
                    }
                    waiting++;
                    Thread.Sleep(400);
                }
            }
            catch (Exception ex)
            {
                log.Error("Searchfreind==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                searchParm = string.Empty;
                runningSearchFriendThread = false;
                prevString = string.Empty;
            }
        }

        private void ProcessSearch()
        {
            try
            {
                //Thread.Sleep(100);
                //foreach (UserBasicInfoModel model in RingIDViewModel.Instance.FavouriteFriendList)
                //{

                //    ProcessSearch(model, searchParm);
                //    addASleep();
                //    if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile.Count > 200
                //        || (string.IsNullOrEmpty(prevString) || (!string.IsNullOrWhiteSpace(searchParm) && !prevString.Equals(searchParm))))
                //    {
                //        break;
                //    }
                //}
                //foreach (UserBasicInfoModel model in RingIDViewModel.Instance.TopFriendList)
                //{
                //    ProcessSearch(model, searchParm);
                //    if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile.Count > 200
                //         || (string.IsNullOrEmpty(prevString) || (!string.IsNullOrWhiteSpace(searchParm) && !prevString.Equals(searchParm))))
                //    {
                //        break;
                //    }
                //    addASleep();
                //}
                foreach (UserBasicInfoModel model in UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.ListContact)
                {
                    ProcessSearch(model, searchParm);
                    addASleep();
                    if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile.Count > 200
                        || (string.IsNullOrEmpty(prevString) || (!string.IsNullOrWhiteSpace(searchParm) && !prevString.Equals(searchParm))))
                    {
                        break;
                    }
                }
                if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile.Count == 0)
                {
                    //UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.LoaderSmall = null;
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.IsNoContactFound = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Search_friends() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ProcessSearch(UserBasicInfoModel model, string searchText)
        {
            string fullName = model.ShortInfoModel.FullName.ToLower();
            string ringId = model.ShortInfoModel.UserIdentity.ToString().Substring(2);
            if (!string.IsNullOrWhiteSpace(searchText) && (fullName.Contains(searchText.Trim()) || ringId.Contains(searchText.Trim())))
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (!CheckIfNotExists(model.ShortInfoModel.UserIdentity))
                    {
                        //UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.LoaderSmall = null;
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile.InvokeAdd(model);
                    }
                });
                //model.VisibilityModel.IsVisibleInMyFriendList = Visibility.Visible;
            }
        }
        private bool CheckIfNotExists(long userId)
        {
            return UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile.Any(P => P.ShortInfoModel.UserIdentity == userId);
        }
    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Auth;
using View.Utility.WPFMessageBox;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCImageContentViewForProfileCoverPhotoChange.xaml
    /// </summary>
    public partial class UCImageContentViewForProfileCoverPhotoChange : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCImageContentViewForProfileCoverPhotoChange).Name);

        public static UCImageContentViewForProfileCoverPhotoChange Instance = null;
        #region "Constructor"
        public UCImageContentViewForProfileCoverPhotoChange()
        {
            InitializeComponent();
            this.DataContext = this;
            Instance = this;
        }
        #endregion

        #region "Property"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private ObservableCollection<ImageModel> _ImageList;
        public ObservableCollection<ImageModel> ImageList
        {
            get
            {
                return _ImageList;
            }
            set
            {
                _ImageList = value;
                this.OnPropertyChanged("ImageList");
            }
        }
        #endregion "Property"

        #region "Command"
        private ICommand _ImageClickCommand;
        public ICommand ImageClickCommand
        {
            get
            {
                if (_ImageClickCommand == null)
                {
                    _ImageClickCommand = new RelayCommand(param => OnImageClick(param));
                }
                return _ImageClickCommand;
            }
        }
        #endregion

        #region "Event Handler"
        //private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        //{
        //    ShowLoading();

        //    SendDataToServer.SendAlbumRequest(0, DefaultSettings.PROFILE_IMAGE_ALBUM_ID, RingIDViewModel.Instance.ProfileImageList.Count);
        //}

        private void ShowMore_PanelClick(object sender, RoutedEventArgs e)
        {
            ShowLoading();
            List<ImageModel> tmpList = (from l in albumModel.ImageList where l.ImageId != Guid.Empty select l).ToList();
            SendDataToServer.SendAlbumRequest(0, albumId, tmpList.Min(P => P.ImageId), 10);
        }
        #endregion

        #region "Private Method"
        private void OnImageClick(object param)
        {
            try
            {
                ImageModel objImageModel = (ImageModel)param;
                if (objImageModel != null && !objImageModel.NoImageFound)
                {
                    if (MainSwitcher.PopupController.PhotoSelectionView.TYPE == MiddlePanelConstants.TypeMyProfilePictureChange)
                    {
                        UCProfilePictureUpload profilePictureUpload = new UCProfilePictureUpload(MainSwitcher.PopupController.PhotoSelectionView._MainGrid);
                        profilePictureUpload.Show();
                        profilePictureUpload.ShowHandlerDialog(objImageModel);
                    }
                    else if (MainSwitcher.PopupController.PhotoSelectionView.TYPE == MiddlePanelConstants.TypeMyCoverPictureChange)
                    {
                        if (objImageModel.ImageWidth < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH || objImageModel.ImageHeight < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT)
                        {
                            //CustomMessageBox.ShowError("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                            UIHelperMethods.ShowFailed("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT, "Cover photo");
                        }
                        else
                        {
                            MainSwitcher.PopupController.PhotoSelectionView.OnCloseCommand();
                            UCMiddlePanelSwitcher.View_UCMyProfile.ShowCoverPicChangePanel(objImageModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Public Method"
        public void ShowMore()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Visible;
                this.showMoreLoader.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowLoading()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Collapsed;
                this.showMoreLoader.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideShowMoreLoading()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Collapsed;
                this.showMoreText.Visibility = Visibility.Visible;
                this.showMoreLoader.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Loaded_Unloaded Events"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            showMoreText.Click += ShowMore_PanelClick;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            showMoreText.Click -= ShowMore_PanelClick;
        }
        #endregion

        Guid albumId = Guid.Empty;
        public AlbumModel albumModel;
        public void SetValue(AlbumModel model, Func<int> _OnBackToPrevious)
        {
            this._OnBackToPrevious = _OnBackToPrevious;
            titleTxt.Text = model.AlbumName + " (" + model.TotalImagesInAlbum + ")";
            albumModel = model;
            albumId = model.AlbumId;
            ImageList = model.ImageList;
            if (albumModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                SendDataToServer.SendAlbumRequest(0, albumId, Guid.Empty, 10);
            else
                SendDataToServer.SendAlbumRequest(albumModel.UserTableID, albumId, Guid.Empty, 10);
        }

        public Func<int> _OnBackToPrevious = null;
        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_OnBackToPrevious != null)
                {
                    _OnBackToPrevious();
                }
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }
    }
}

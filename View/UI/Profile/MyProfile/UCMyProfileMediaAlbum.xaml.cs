﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMyProfileMediaAlbum.xaml
    /// </summary>
    public partial class UCMyProfileMediaAlbum : UserControl, INotifyPropertyChanged
    {
        private string tabItem = string.Empty;
        public string ButtonName = string.Empty;
        public UCMyProfileMusicAlbum _UCMyProfileMusicAlbum = null;
        public UCMyprofileVideoAlbum _UCMyprofileVideoAlbum = null;

        public UCMyProfileMediaAlbum()
        {
            InitializeComponent();
            TabList.SelectedIndex = 0;
            this.DataContext = this;
        }

        #region "Property"

        private bool _IsListView = false;
        public bool IsListView
        {
            get { return _IsListView; }
            set
            {
                if (value == _IsListView)
                    return;
                _IsListView = value;
                this.OnPropertyChanged("IsListView");
            }
        }
        #endregion
        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            TabList.SelectionChanged += TabControl_SelectionChanged;
            viewBtn.Click += viewBtn_Click;
            //Thumbnail_View.Click += ThumbnailView_Click;
            TabControl_SelectionChanged(TabList, null);
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            TabList.SelectionChanged -= TabControl_SelectionChanged;
            viewBtn.Click -= viewBtn_Click;
            //List_View.Click -= ListView_Click;
            //Thumbnail_View.Click -= ThumbnailView_Click;
        }

        private void LoadMusicAlbum()
        {
            if (_UCMyProfileMusicAlbum == null) _UCMyProfileMusicAlbum = new UCMyProfileMusicAlbum();
            _UCMyProfileMusicAlbum.SetMyAlbumsList();
            MusicPanel.Visibility = Visibility.Visible;
            VideosPanel.Visibility = Visibility.Collapsed;
            _UCMyProfileMusicAlbum.ShowListOrThumbViewInMusic(IsListView);
            MusicPanel.Child = _UCMyProfileMusicAlbum;
        }

        private void LoadVideoAlbum()
        {
            if (_UCMyprofileVideoAlbum == null) _UCMyprofileVideoAlbum = new UCMyprofileVideoAlbum();
            _UCMyprofileVideoAlbum.SetMyAlbumsList();
            MusicPanel.Visibility = Visibility.Collapsed;
            VideosPanel.Visibility = Visibility.Visible;
            _UCMyprofileVideoAlbum.ShowListOrThumbViewInVideo(IsListView);
            VideosPanel.Child = _UCMyprofileVideoAlbum;
        }

        public void btnVisibility(bool isSongView)
        {
            if(isSongView)
            {
                viewBtn.Visibility = Visibility.Collapsed;
                //List_View.Visibility = Visibility.Collapsed;
                //Thumbnail_View.Visibility = Visibility.Collapsed;
            }
            else
            {
                viewBtn.Visibility = Visibility.Visible;
                //if (IsListView)
                //{
                //    List_View.Visibility = Visibility.Visible;
                //    Thumbnail_View.Visibility = Visibility.Collapsed;
                //}
                //else
                //{
                //    List_View.Visibility = Visibility.Collapsed;
                //    Thumbnail_View.Visibility = Visibility.Visible;
                //}
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tabItem = ((sender as TabControl).SelectedItem as TabItem).Name as string;
            switch (tabItem)
            {
                case "MusicTab":
                    LoadMusicAlbum();
                    break;
                case "VideosTab":
                    LoadVideoAlbum();
                    break;
            }
        }

        //private void ListView_Click(object sender, RoutedEventArgs e)
        //{
        //    Button bt = (Button)sender;
        //    ButtonName = bt.Name;
        //    IsListView = false;
        //    if (tabItem.Equals("MusicTab"))
        //    {
        //        _UCMyProfileMusicAlbum.ShowListOrThumbViewInMusic(ButtonName);
        //    }
        //    else
        //    {
        //        if (_UCMyprofileVideoAlbum != null)
        //        {
        //            _UCMyprofileVideoAlbum.ShowListOrThumbViewInVideo(ButtonName);
        //        }
        //    }
        //}

        //private void ThumbnailView_Click(object sender, RoutedEventArgs e)
        //{
        //    Button bt = (Button)sender;
        //    ButtonName = bt.Name;
        //    IsListView = true;
        //    if (tabItem.Equals("MusicTab"))
        //    {
        //        _UCMyProfileMusicAlbum.ShowListOrThumbViewInMusic(ButtonName);
        //    }
        //    else
        //    {
        //        if (_UCMyprofileVideoAlbum != null)
        //        {
        //            _UCMyprofileVideoAlbum.ShowListOrThumbViewInVideo(ButtonName);
        //        }
        //    }
        //}

        private void viewBtn_Click(object sender, RoutedEventArgs e)
        {
            if(IsListView)
            {
                IsListView = false;
                if (tabItem.Equals("MusicTab"))
                {
                    _UCMyProfileMusicAlbum.ShowListOrThumbViewInMusic(IsListView);
                }
                else
                {
                    if (_UCMyprofileVideoAlbum != null)
                    {
                        _UCMyprofileVideoAlbum.ShowListOrThumbViewInVideo(IsListView);
                    }
                }
            }
            else
            {
                IsListView = true;
                if (tabItem.Equals("MusicTab"))
                {
                    _UCMyProfileMusicAlbum.ShowListOrThumbViewInMusic(IsListView);
                }
                else
                {
                    if (_UCMyprofileVideoAlbum != null)
                    {
                        _UCMyprofileVideoAlbum.ShowListOrThumbViewInVideo(IsListView);
                    }
                }
            }
        }
    }
}

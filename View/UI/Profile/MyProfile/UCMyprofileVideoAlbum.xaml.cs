﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.ViewModel;
using View.Utility.Auth;

namespace View.UI.Profile.MyProfile
{
    /// <summary>
    /// Interaction logic for UCMyprofileVideoAlbum.xaml
    /// </summary>
    public partial class UCMyprofileVideoAlbum : UserControl, INotifyPropertyChanged
    {
        //public string ButtonView = string.Empty;
        public bool IsListView = false;
        public UCMyprofileVideoAlbum()
        {
            InitializeComponent();
            this.DataContext = this;
            if (RingIDViewModel.Instance.MyVideoAlbums.Count == 0)
                this.VideoGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
        }

        private Visibility _NoVideoTxtVisibility = Visibility.Collapsed;
        public Visibility NoVideoTxtVisibility
        {
            get { return _NoVideoTxtVisibility; }
            set
            {
                if (value == _NoVideoTxtVisibility) return;
                _NoVideoTxtVisibility = value;
                this.OnPropertyChanged("NoVideoTxtVisibility");
            }
        }

        private ICommand _VideoAlbumClickCommand;
        public ICommand VideoAlbumClickCommand
        {
            get
            {
                if (_VideoAlbumClickCommand == null)
                {
                    _VideoAlbumClickCommand = new RelayCommand(param => OnAlbumClick(param));
                }
                return _VideoAlbumClickCommand;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void OnAlbumClick(object param)
        {
            try
            {
                MediaContentModel model = (MediaContentModel)param;
                LisTViewPanelInVideo.Visibility = Visibility.Collapsed;
                ThumbPanelInVideo.Visibility = Visibility.Collapsed;

                if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                VideoAlbumsDetailsPanel.Child = UCMediaContentsView.Instance;
                //UCMediaContentsView.Instance.ParentInstance = this;
                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum.btnVisibility(true);
                UCMediaContentsView.Instance.ShowAlbumContents(model, () =>
                {
                    ShowAfterVideoAlbumDetailsCancel();
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum.btnVisibility(false);
                    return 0;
                });
            }
            catch (Exception)
            {
            }
        }
        public void ShowAfterVideoAlbumDetailsCancel()
        {
            if (IsListView)
            {
                LisTViewPanelInVideo.Visibility = Visibility.Visible;
                ThumbPanelInVideo.Visibility = Visibility.Collapsed;
            }
            else
            {
                LisTViewPanelInVideo.Visibility = Visibility.Collapsed;
                ThumbPanelInVideo.Visibility = Visibility.Visible;
            }
            VideoAlbumsDetailsPanel.Child = null;
        }

        public void LoadCurrentView()
        {
            if (VideoAlbumsDetailsPanel.Child == null)
            {
                if (IsListView)
                {
                    LisTViewPanelInVideo.Visibility = Visibility.Visible;
                    ThumbPanelInVideo.Visibility = Visibility.Collapsed;
                }
                else
                {
                    ThumbPanelInVideo.Visibility = Visibility.Visible;
                    LisTViewPanelInVideo.Visibility = Visibility.Collapsed;
                }
            }
        }

        public void ShowListOrThumbViewInVideo(bool IsListView)
        {
            this.IsListView = IsListView;
            if (VideoAlbumsDetailsPanel.Child == null)
            {
                if (IsListView)
                {
                    LisTViewPanelInVideo.Visibility = Visibility.Visible;
                    ThumbPanelInVideo.Visibility = Visibility.Collapsed;
                }
                else
                {
                    ThumbPanelInVideo.Visibility = Visibility.Visible;
                    LisTViewPanelInVideo.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                //if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo != null)
                //{
                //    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum.UCMediaAlbumDetailsForVideo.LoadCurrentView(ButtonView);
                //}
            }
        }

        public void SetMyAlbumsList()
        {
            if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
            {
                if (VideoGIFCtrl != null && VideoGIFCtrl.IsRunning()) this.VideoGIFCtrl.StopAnimate();
            }
            //else if (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_TABLE_ID) && NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
            //{
            //    NoVideoTxt.Visibility = Visibility.Collapsed;
            //    List<MediaContentDTO> lst = NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Values.ToList();
            //    foreach (var albumDto in lst)
            //    {
            //        if (!RingIDViewModel.Instance.MyVideoAlbums.Any(P => P.AlbumId == albumDto.AlbumId))
            //        {
            //            MediaContentModel albumModel = new MediaContentModel();
            //            albumModel.LoadData(albumDto);
            //            RingIDViewModel.Instance.MyVideoAlbums.Add(albumModel);
            //        }
            //    }
            //    if (VideoGIFCtrl != null && VideoGIFCtrl.IsRunning()) this.VideoGIFCtrl.StopAnimate();
            //}
            else
            {
                NoVideoTxtVisibility = Visibility.Collapsed;
                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    new Thread(() =>
                    {
                        bool success = false;
                        string message = string.Empty;
                        JObject obj = new JObject();
                        obj[JsonKeys.UserTableID] = DefaultSettings.LOGIN_TABLE_ID;
                        obj[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_VIDEO;
                        SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_MEDIA_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                        SetLoaderTextValue(success);
                    }).Start();
                }
                else if (VideoGIFCtrl != null && VideoGIFCtrl.IsRunning()) this.VideoGIFCtrl.StopAnimate();
            }
        }

        public void SetLoaderTextValue(bool success)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (VideoGIFCtrl != null && VideoGIFCtrl.IsRunning()) this.VideoGIFCtrl.StopAnimate();
                if (!success && RingIDViewModel.Instance.MyVideoAlbums.Count == 0)
                {
                    NoVideoTxtVisibility = Visibility.Visible;
                    DefaultSettings.MY_VIDEO_ALBUMS_COUNT = 0;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        #region "Event Trigger"
        private void SingleAlbum_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Grid grid = (Grid)sender;
            if (grid.DataContext is MediaContentModel)
            {
                MediaContentModel model = (MediaContentModel)grid.DataContext;
                if (model.IsStaticAlbum == false)
                {
                    if (grid.ContextMenu != ContextMenuAlbumEditOptions.Instance.cntxMenu)
                    {
                        grid.ContextMenu = ContextMenuAlbumEditOptions.Instance.cntxMenu;
                        ContextMenuAlbumEditOptions.Instance.SetupMenuItem();
                    }
                    ContextMenuAlbumEditOptions.Instance.ShowHandler(grid, model);
                    ContextMenuAlbumEditOptions.Instance.cntxMenu.IsOpen = true;
                }
            }
        }
        #endregion
    }
}

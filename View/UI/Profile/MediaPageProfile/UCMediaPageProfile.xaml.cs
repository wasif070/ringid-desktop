﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.Profile.MusicPageProfile
{
    /// <summary>
    /// Interaction logic for UCMediaPageProfile.xaml
    /// </summary>
    public partial class UCMediaPageProfile : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMediaPageProfile).Name);
        public UCMediaPageProfile(MusicPageModel MediaPageModel)
        {
            this.MediaPageModel = MediaPageModel;
            UserIdentity = MediaPageModel.UserIdentity;
            this.DataContext = this;
            InitializeComponent();
            //
            //this.PreviewMouseWheel += (s, e) => { scroll.OnPreviewMouseWheelScrolled(e.Delta); e.Handled = true; };//this.PreviewMouseWheel += delegate(object sender, MouseWheelEventArgs e) { };
            this.IsVisibleChanged += (s, e) =>
            {
                bool visible = (bool)e.NewValue;
                backBtn.Click -= backBtn_Click;
                followBtn.Click -= followBtn_Click;
                if (visible)
                {
                    backBtn.Click += backBtn_Click;
                    followBtn.Click += followBtn_Click;
                }
                scroll.OnIsVisibleChanged(visible);
            };

            RingIDViewModel.Instance.ProfileMediaCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_PROFILE_MEDIA);
            FeedDataContainer.Instance.ProfileMediaBottomIds.Clear();
            FeedDataContainer.Instance.ProfileMediaTopIds.Clear();
            FeedDataContainer.Instance.ProfileMediaCurrentIds.Clear();
            RingIDViewModel.Instance.ProfileMediaCustomFeeds.UserProfileUtId = MediaPageModel.UserTableID;

            ViewCollection = RingIDViewModel.Instance.ProfileMediaCustomFeeds;
            scroll.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.ProfileMediaCurrentIds, FeedDataContainer.Instance.ProfileMediaTopIds, FeedDataContainer.Instance.ProfileMediaBottomIds, SettingsConstants.PROFILE_TYPE_MUSICPAGE, AppConstants.TYPE_FRIEND_NEWSFEED);
            DefaultSettings.PROFILEMEDIA_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
            {
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.PROFILEMEDIA_STARTPKT);
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #region Property
        public long UserIdentity = 0;
        private CustomFeedCollection _ViewCollection;
        public CustomFeedCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }
        private MusicPageModel _MediaPageModel;
        public MusicPageModel MediaPageModel
        {
            get { return _MediaPageModel; }
            set { _MediaPageModel = value; }
        }
        private string followUnfollowTxt = "";
        public string FollowUnfollowTxt
        {
            get { return followUnfollowTxt; }
            set
            {
                followUnfollowTxt = value;
                this.OnPropertyChanged("FollowUnfollowTxt");
            }
        }
        private string followUnfollowPlus = "";
        public string FollowUnfollowPlus
        {
            get { return followUnfollowPlus; }
            set
            {
                followUnfollowPlus = value;
                this.OnPropertyChanged("FollowUnfollowPlus");
            }
        }
        private bool _IsContextMenuOpened = false;
        public bool IsContextMenuOpened
        {
            get { return _IsContextMenuOpened; }
            set
            {
                if (value == _IsContextMenuOpened) return;
                _IsContextMenuOpened = value;
                this.OnPropertyChanged("IsContextMenuOpened");
            }
        }
        #endregion Property

        //private void UserControl_loaded(object sender, RoutedEventArgs args)
        //{
        //    scroll.ScrollToHome();
        //    backBtn.Click -= backBtn_Click;
        //    backBtn.Click += backBtn_Click;
        //    followBtn.Click -= followBtn_Click;
        //    followBtn.Click += followBtn_Click;
        //    if (ViewCollection == null)
        //    {
        //        if (timer == null)
        //        {
        //            timer = new DispatcherTimer();
        //            timer.Interval = TimeSpan.FromMilliseconds(500);
        //            timer.Tick += TimerEventProcessor;
        //        }
        //        else timer.Stop();
        //        timer.Start();
        //    }
        //}
        //private void UserControl_unloaded(object sender, RoutedEventArgs args)
        //{
        //    //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
        //    //{
        //        backBtn.Click -= backBtn_Click;
        //        followBtn.Click -= followBtn_Click;
        //        scroll.SetScrollEvents(false);
        //        ViewCollection = null;
        //    //}
        //}
        //private DispatcherTimer timer;
        //private void TimerEventProcessor(object sender, EventArgs e)
        //{
        //    if (timer != null && timer.IsEnabled)
        //    {
        //        ViewCollection = RingIDViewModel.Instance.MediaPageProfileFeeds;
        //        scroll.SetScrollEvents(true);
        //        if (!UCGuiRingID.Instance.IsAnyWindowAbove())
        //            Keyboard.Focus(scroll);
        //        timer.Stop();
        //    }
        //}

        void backBtn_Click(object sender, RoutedEventArgs e)
        {
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }

        private void followBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MediaPageModel != null && MediaPageModel.IsSubscribed)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "Music Page"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                if (isTrue)
                    new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_MUSICPAGE, MediaPageModel.UserTableID, 1, null, null, MediaPageModel.MusicPageId).StartThread();
            }
            else
                new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_MUSICPAGE, MediaPageModel.UserTableID, 2, null, null, MediaPageModel.MusicPageId).StartThread();
        }

        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                IsContextMenuOpened = true;
            }
            else
            {
                IsContextMenuOpened = false;
            }
        }

        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            DefaultSettings.PROFILEMEDIA_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.PROFILEMEDIA_STARTPKT);
        }
        #region "Utility methods"
        #endregion ""Utility methods""
    }
}

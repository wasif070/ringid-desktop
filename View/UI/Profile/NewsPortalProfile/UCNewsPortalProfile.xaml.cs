﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.Linq;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;
using System.Windows.Media.Animation;
using Newtonsoft.Json.Linq;
using View.Utility.Auth;
using View.Constants;

namespace View.UI.Profile.NewsPortalProfile
{
    /// <summary>
    /// Interaction logic for UCNewsPortalProfile.xaml
    /// </summary>
    public partial class UCNewsPortalProfile : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCNewsPortalProfile).Name);
        private VirtualizingStackPanel _ImageItemContainer = null;
        private System.Timers.Timer _Timer;
        public bool _IsTimerStart = false;
        private bool _IsFirstTimePrev = true;
        private bool _IsFirstTimeNext = true;
        private int _CurrentLeftMargin = 0;
        private int _NextPrevCount = 0;
        private int _NoOfSwappingCount = 0;

        #region "Constructor"
        public UCNewsPortalProfile(NewsPortalModel NewsPortalModel)
        {
            this.NewsPortalModel = NewsPortalModel;
            PortalUserIdentity = NewsPortalModel.UserIdentity;
            InitializeComponent();
            this.DataContext = this;
            //this.PreviewMouseWheel += (s, e) => { scroll.OnPreviewMouseWheelScrolled(e.Delta); e.Handled = true; };//this.PreviewMouseWheel += delegate(object sender, MouseWheelEventArgs e) { };
            RingIDViewModel.Instance.ProfileNewsPortalCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_PROFILE_MEDIA);
            FeedDataContainer.Instance.ProfileNewsPortalBottomIds.Clear();
            FeedDataContainer.Instance.ProfileNewsPortalTopIds.Clear();
            FeedDataContainer.Instance.ProfileNewsPortalCurrentIds.Clear();
            RingIDViewModel.Instance.ProfileNewsPortalCustomFeeds.UserProfileUtId = NewsPortalModel.UserTableID;

            ViewCollection = RingIDViewModel.Instance.ProfileNewsPortalCustomFeeds;
            scroll.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.ProfileNewsPortalCurrentIds, FeedDataContainer.Instance.ProfileNewsPortalTopIds, FeedDataContainer.Instance.ProfileNewsPortalBottomIds, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, AppConstants.TYPE_FRIEND_NEWSFEED);
            DefaultSettings.PROFILENEWSPORTAL_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
            {
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.PROFILENEWSPORTAL_STARTPKT);
            }
            BreakingNewsRequest();
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Property
        public long PortalUserIdentity = 0;
        private NewsPortalModel _NewsPortalModel;
        public NewsPortalModel NewsPortalModel
        {
            get { return _NewsPortalModel; }
            set { _NewsPortalModel = value; }
        }

        public ObservableCollection<FeedModel> _BreakingNewsPortalFeeds;
        public ObservableCollection<FeedModel> BreakingNewsPortalFeeds
        {
            get
            {
                return _BreakingNewsPortalFeeds;
            }
            set
            {
                _BreakingNewsPortalFeeds = value;
                this.OnPropertyChanged("BreakingNewsPortalFeeds");
            }
        }

        public ObservableCollection<FeedModel> _BreakingNewsPortalSliderFeeds = new ObservableCollection<FeedModel>();
        public ObservableCollection<FeedModel> BreakingNewsPortalSliderFeeds
        {
            get
            {
                return _BreakingNewsPortalSliderFeeds;
            }
            set
            {
                _BreakingNewsPortalSliderFeeds = value;
                this.OnPropertyChanged("BreakingNewsPortalSliderFeeds");
            }
        }
        private CustomFeedCollection _ViewCollection;
        public CustomFeedCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }
        private bool _IsContextMenuOpened = false;
        public bool IsContextMenuOpened
        {
            get { return _IsContextMenuOpened; }
            set
            {
                if (value == _IsContextMenuOpened) return;
                _IsContextMenuOpened = value;
                this.OnPropertyChanged("IsContextMenuOpened");
            }
        }
        #endregion Property

        #region "Public Method"
        public void LoadSliderData()
        {
            try
            {
                if (BreakingNewsPortalFeeds != null && BreakingNewsPortalFeeds.Count > 0)
                {
                    List<FeedModel> breakingNws = BreakingNewsPortalFeeds.ToList();
                    foreach (FeedModel model in breakingNws)
                    {
                        if (!BreakingNewsPortalSliderFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                        {
                            BreakingNewsPortalSliderFeeds.InvokeAdd(model);
                            if (BreakingNewsPortalSliderFeeds.Count == 4)
                            {
                                break;
                            }
                        }
                    }

                    if (BreakingNewsPortalSliderFeeds.Count > 2)
                    {
                        //SetNextPrevoiusButtonVisibility();
                        StartBannerNewsSliding();
                    }
                }
            }

            catch (Exception ex)
            {
                log.Error("Error: LoadSliderData() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        private void StartBannerNewsSliding()
        {
            if (_Timer == null)
            {
                _Timer = new System.Timers.Timer();
                _Timer.Interval = 2500;
                _IsTimerStart = true;
                _Timer.Elapsed += timer_Elapsed;
                _Timer.Start();
            }
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    nextBtn_Click(null, null);
                }
                catch (Exception ex)
                {
                    log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void nextBtn_Click(object sender, RoutedEventArgs e)
        {
            //Hints: For AutoSlide/Next, Viewport is 0 to -630.
            try
            {
                if (_Timer != null)
                    _Timer.Stop();
                if (_IsFirstTimeNext == false)
                {
                    int firstIndex = 0, firstmiddleIndex = 1, secondmiddleIndex = 2, lastIndex = 3, nextLoadingIndex = (_NextPrevCount + lastIndex) % BreakingNewsPortalFeeds.Count;
                    BreakingNewsPortalSliderFeeds.InvokeMove(firstIndex, firstmiddleIndex);
                    BreakingNewsPortalSliderFeeds.InvokeMove(firstmiddleIndex, secondmiddleIndex);
                    if (BreakingNewsPortalSliderFeeds.Count > 3)
                    {
                        BreakingNewsPortalSliderFeeds.InvokeMove(secondmiddleIndex, lastIndex);
                        BreakingNewsPortalSliderFeeds.RemoveAt(lastIndex);
                        FeedModel model = BreakingNewsPortalFeeds.ElementAt(nextLoadingIndex);
                        BreakingNewsPortalSliderFeeds.InvokeInsert(lastIndex, model);

                        if (_NoOfSwappingCount == BreakingNewsPortalFeeds.Count - 2)
                        {
                            _NoOfSwappingCount = 0;
                        }
                        else
                        {
                            _NoOfSwappingCount++;
                        }
                    }
                }

                _IsFirstTimeNext = false; // firstTime no swapping make, just changing viewport from 0 to -630.
                _IsFirstTimePrev = true;

                _NextPrevCount++;
                OnNext(null);
                //SetNextPrevoiusButtonVisibility();
                if (_Timer != null)
                    _Timer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: nextBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        private void AnimationOnArrowClicked(int target, double delay)
        {
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(_CurrentLeftMargin, 0, 0, 0);
            MarginAnimation.To = new Thickness(target, 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
            storyboard.Begin(_ImageItemContainer);
        }
        private void OnNext(object param)
        {
            try
            {
                int target = -320;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ResetData()
        {
            if (_Timer != null)
            {
                _Timer.Elapsed -= timer_Elapsed;
                _Timer.Stop();
                _Timer.Enabled = false;
                _Timer = null;
            }

            AnimationOnArrowClicked(0, 0);
            _NoOfSwappingCount = 0;
            _NextPrevCount = 0;
            _CurrentLeftMargin = 0;
            _IsFirstTimeNext = true;
            _IsFirstTimePrev = true;
            _IsTimerStart = false;

            //IsLeftArrowVisible = Visibility.Hidden;
            //IsRightArrowVisible = Visibility.Hidden;
        }
        private void BreakingNewsRequest()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = NewsPortalModel.UserTableID;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_NEWSPORTAL_BREAKING_FEED, AppConstants.REQUEST_TYPE_REQUEST);
        }

        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                IsContextMenuOpened = true;
            }
            else
            {
                IsContextMenuOpened = false;
            }
        }

        #region "ICommand"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            scroll.OnIsVisibleChanged(true);
            BreakingNewsPortalFeeds = RingIDViewModel.Instance.BreakingNewsPortalProfileFeeds;
            scroll.ScrollToHome();
            if (BreakingNewsPortalFeeds != null && BreakingNewsPortalFeeds.Count > 0)
                LoadSliderData();

            sliderScroll.PreviewMouseWheel += sliderScroll_PreviewMouseWheel;
        }

        private ICommand unLoadedUserConrol;
        public ICommand UnLoadedUserConrol
        {
            get
            {
                if (unLoadedUserConrol == null) unLoadedUserConrol = new RelayCommand(param => OnUnLoadedUserConrol());
                return unLoadedUserConrol;
            }
        }
        public void OnUnLoadedUserConrol()
        {
            scroll.OnIsVisibleChanged(false);
            sliderScroll.PreviewMouseWheel -= sliderScroll_PreviewMouseWheel;
            if (_ImageItemContainer != null)
                ResetData();
            if (BreakingNewsPortalSliderFeeds != null)
                BreakingNewsPortalSliderFeeds.Clear();

            if (BreakingNewsPortalFeeds != null)
                BreakingNewsPortalFeeds = null;
        }

        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            DefaultSettings.PROFILENEWSPORTAL_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.PROFILENEWSPORTAL_STARTPKT);
            BreakingNewsRequest();
        }
        private ICommand _VirtualizingStackPanelLoaded;
        public ICommand VirtualizingStackPanelLoaded
        {
            get
            {
                if (_VirtualizingStackPanelLoaded == null)
                {
                    _VirtualizingStackPanelLoaded = new RelayCommand(param => OnVirtualizingStackPanelLoaded(param));
                }
                return _VirtualizingStackPanelLoaded;
            }
        }
        private void OnVirtualizingStackPanelLoaded(object parameter)
        {
            if (parameter is VirtualizingStackPanel)
                _ImageItemContainer = parameter as VirtualizingStackPanel;
        }
        private ICommand _BreakingNewsDetailsCommand;
        public ICommand BreakingNewsDetailsCommand
        {
            get
            {
                if (_BreakingNewsDetailsCommand == null)
                {
                    _BreakingNewsDetailsCommand = new RelayCommand(param => OnBreakingNewsDetailsCommand(param));
                }
                return _BreakingNewsDetailsCommand;
            }
        }
        private void OnBreakingNewsDetailsCommand(object sender)
        {
            try
            {
                if (sender is FeedModel)
                {
                    FeedModel modelFromContext = (FeedModel)sender;

                    if (UCSingleFeedDetailsView.Instance != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(UCSingleFeedDetailsView.Instance);
                    UCSingleFeedDetailsView.Instance = new UCSingleFeedDetailsView(UCGuiRingID.Instance.MotherPanel);
                    UCSingleFeedDetailsView.Instance.Show();
                    UCSingleFeedDetailsView.Instance.ShowDetailsView();
                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Visible;
                    UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                    UCSingleFeedDetailsView.Instance.OnRemovedUserControl += () =>
                    {
                        UCSingleFeedDetailsView.Instance = null;
                    };

                    MainSwitcher.ThreadManager().DetailsOfaFeed.CallBackEvent += (dto) =>
                    {
                        if (dto != null)
                        {
                            FeedModel fm = null;
                            FeedDataContainer.Instance.FeedModels.TryGetValue(modelFromContext.NewsfeedId, out fm);
                            if (fm == null) fm = modelFromContext;//new FeedModel();
                            fm.LoadData(dto);
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (UCSingleFeedDetailsView.Instance != null)
                                {
                                    UCSingleFeedDetailsView.Instance.LoadFeedModel(fm);
                                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Collapsed;
                                    if (UCSingleFeedDetailsView.Instance.GIFCtrlLoader.IsRunning())
                                        UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StopAnimate();
                                }
                            });
                        }
                    };
                    MainSwitcher.ThreadManager().DetailsOfaFeed.StartThread(modelFromContext.NewsfeedId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Event Trigger"
        private void sliderScroll_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error:PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion
    }
}

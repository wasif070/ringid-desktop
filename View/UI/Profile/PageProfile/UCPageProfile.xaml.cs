﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.Profile.PageProfile
{
    /// <summary>
    /// Interaction logic for UCNewsPortalProfile.xaml
    /// </summary>
    public partial class UCPageProfile : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCPageProfile).Name);
        public UCPageProfile(PageInfoModel pageModel)
        {
            this.PageModel = pageModel;
            PageUserIdentity = pageModel.UserIdentity;
            this.DataContext = this;
            InitializeComponent();
            //
            //if (RingIDViewModel.Instance.PageProfileFeeds.Count > 1) RingIDViewModel.Instance.PageProfileFeeds.RemoveAllModels();
            //if (RingIDViewModel.Instance.PageProfileFeeds.Count != 1) RingIDViewModel.Instance.PageProfileFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_PAGE_PROFILE); //safety check
            FeedDataContainer.Instance.PageProfileFeedsSortedIds.Clear();

            //RingIDViewModel.Instance.PageProfileFeeds.UserProfileUtId = PageModel.UserTableID;//PageUserIdentity;//only for profiles
            //RingIDViewModel.Instance.PageProfileFeeds.pId = PageModel.PageId;//only for pageprofiles
            //ViewCollection = RingIDViewModel.Instance.PageProfileFeeds;
            scroll.SetScrollValues(ViewCollection, FeedDataContainer.Instance.PageProfileFeedsSortedIds, SettingsConstants.PROFILE_TYPE_PAGES, AppConstants.TYPE_FRIEND_NEWSFEED);

            DefaultSettings.PROFILEPAGE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.PROFILEPAGE_STARTPKT);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #region Property
        public long PageUserIdentity = 0;
        private CustomObservableCollection _ViewCollection;
        public CustomObservableCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }
        private PageInfoModel _PageModel;
        public PageInfoModel PageModel
        {
            get { return _PageModel; }
            set { _PageModel = value; }
        }
        private string followUnfollowTxt = "";
        public string FollowUnfollowTxt
        {
            get { return followUnfollowTxt; }
            set
            {
                followUnfollowTxt = value;
                this.OnPropertyChanged("FollowUnfollowTxt");
            }
        }
        private string followUnfollowPlus = "";
        public string FollowUnfollowPlus
        {
            get { return followUnfollowPlus; }
            set
            {
                followUnfollowPlus = value;
                this.OnPropertyChanged("FollowUnfollowPlus");
            }
        }
        private bool _IsContextMenuOpened = false;
        public bool IsContextMenuOpened
        {
            get { return _IsContextMenuOpened; }
            set
            {
                if (value == _IsContextMenuOpened) return;
                _IsContextMenuOpened = value;
                this.OnPropertyChanged("IsContextMenuOpened");
            }
        }

        #endregion Property
        private void UserControl_loaded(object sender, RoutedEventArgs args)
        {
            scroll.ScrollToHome();
            backBtn.Click -= backBtn_Click;
            backBtn.Click += backBtn_Click;
            followBtn.Click -= followBtn_Click;
            followBtn.Click += followBtn_Click;
            if (ViewCollection == null)
            {
                if (timer == null)
                {
                    timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromMilliseconds(500);
                    timer.Tick += TimerEventProcessor;
                }
                else timer.Stop();
                timer.Start();
            }
        }
        private void UserControl_unloaded(object sender, RoutedEventArgs args)
        {
            //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
            //{
            backBtn.Click -= backBtn_Click;
            followBtn.Click -= followBtn_Click;
            scroll.SetScrollEvents(false);
            ViewCollection = null;
            //}
        }
        private DispatcherTimer timer;
        private void TimerEventProcessor(object sender, EventArgs e)
        {
            if (timer != null && timer.IsEnabled)
            {
                //ViewCollection = RingIDViewModel.Instance.PageProfileFeeds;
                scroll.SetScrollEvents(true);
                if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    Keyboard.Focus(scroll);
                timer.Stop();
            }
        }

        void backBtn_Click(object sender, RoutedEventArgs e)
        {
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }

        private void followBtn_Click(object sender, RoutedEventArgs e)
        {
            if (PageModel != null && PageModel.IsSubscribed)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "this Page"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                if (isTrue)
                    new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_PAGES, PageModel.UserTableID, 1, null, null, PageModel.PageId).StartThread();
            }
            else
            {
                if (UCFollowingUnfollowingView.Instance == null)
                {
                    UCFollowingUnfollowingView.Instance = new UCFollowingUnfollowingView(UCGuiRingID.Instance.MotherPanel);
                    UCFollowingUnfollowingView.Instance.Show();
                    UCFollowingUnfollowingView.Instance.ShowFollowUnFollow(PageModel, true);
                    UCFollowingUnfollowingView.Instance.OnRemovedUserControl += () =>
                    {
                        UCFollowingUnfollowingView.Instance = null;
                    };
                }
            }
        }
        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                IsContextMenuOpened = true;
            }
            else
            {
                IsContextMenuOpened = false;
            }
        }

        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            DefaultSettings.PROFILEPAGE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.PROFILEPAGE_STARTPKT);
        }

        #region "Utility methods"
        #endregion ""Utility methods""
    }
}

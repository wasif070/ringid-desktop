﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.Profile.FriendProfile.About;
using View.Utility.Auth;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendAbout.xaml
    /// </summary>
    public partial class UCFriendAbout : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendAbout).Name);

        public UCFriendBasicInfo ucFriendBasicInfo = null;
        public UCFriendProfessionalCareer ucFriendProfessionalCareer = null;
        public UCFriendEducation ucFriendEducation = null;
        public UCFriendSkill ucFriendSkill = null;

        /*private bool isWorkEduSkillRequested = false;*/
        private UserShortInfoModel FriendShortInfoModel;
        private UserBasicInfoModel FriendBasicInfoModel;

        public UCFriendAbout(string nm)
        {
            InitializeComponent();
            this.DataContext = this;
            ucFriendBasicInfo = new UCFriendBasicInfo(nm);
            BasicInfoHolderPanel.Child = ucFriendBasicInfo;
        }

        public UCFriendAbout(UserBasicInfoModel FriendBasicInfoModel)
        {
            InitializeComponent();
            this.DataContext = this;
            this.FriendBasicInfoModel = FriendBasicInfoModel;
            this.FriendShortInfoModel = FriendBasicInfoModel.ShortInfoModel;
            ucFriendBasicInfo = new UCFriendBasicInfo(FriendBasicInfoModel);
            BasicInfoHolderPanel.Child = ucFriendBasicInfo;
            WorkEducationSkillRequest();
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            /*StackPanelBasic.MouseLeftButtonDown += basicInfoLbl_MouseLeftButtonDown;
            StackPanelProCareer.MouseLeftButtonDown += FriendCareerMouseDown;
            StackPanelEducation.MouseLeftButtonDown += educationMouseDown;
            StackPanelSkill.MouseLeftButtonDown += skill_MouseLeftButtonDown;*/

            LoadData();
        }

        private void LoadData()
        {
            if (ucFriendBasicInfo == null) ucFriendBasicInfo = new UCFriendBasicInfo(FriendBasicInfoModel);
            BasicInfoHolderPanel.Child = ucFriendBasicInfo;

            if (ucFriendProfessionalCareer == null)
            {
                ucFriendProfessionalCareer = new UCFriendProfessionalCareer();
            }
            if (FriendBasicInfoModel != null) ucFriendProfessionalCareer.LoadMyFriendsWorkToModels(FriendBasicInfoModel.ShortInfoModel.UserTableID);
            ProfessionalCareerHolderPanel.Child = ucFriendProfessionalCareer;

            if (ucFriendEducation == null)
            {
                ucFriendEducation = new UCFriendEducation();
            }
            if (FriendBasicInfoModel != null) ucFriendEducation.LoadMyFriendsEduToModels(FriendBasicInfoModel.ShortInfoModel.UserTableID);
            EducationHolderPanel.Child = ucFriendEducation;

            if (ucFriendSkill == null)
            {
                ucFriendSkill = new UCFriendSkill();
            }
            if (FriendBasicInfoModel != null) ucFriendSkill.LoadMyFriendsSkillsToModels(FriendBasicInfoModel.ShortInfoModel.UserTableID);
            SkillHolderPanel.Child = ucFriendSkill;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            /*StackPanelBasic.MouseLeftButtonDown -= basicInfoLbl_MouseLeftButtonDown;
            StackPanelProCareer.MouseLeftButtonDown -= FriendCareerMouseDown;
            StackPanelEducation.MouseLeftButtonDown -= educationMouseDown;
            StackPanelSkill.MouseLeftButtonDown -= skill_MouseLeftButtonDown;*/

            BasicInfoHolderPanel.Child = null;
            ProfessionalCareerHolderPanel.Child = null;
            EducationHolderPanel.Child = null;
            SkillHolderPanel.Child = null;
        }

        /* private void basicInfoLbl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
         {
             if (BasicInfoHolderPanel.Child == null)
             {
                 if (ucFriendBasicInfo == null) ucFriendBasicInfo = new UCFriendBasicInfo(FriendBasicInfoModel);
                 BasicInfoHolderPanel.Child = ucFriendBasicInfo;

                 if (ucFriendProfessionalCareer != null) { ucFriendProfessionalCareer.Dispose(); ucFriendProfessionalCareer = null; }
                 if (ucFriendEducation != null) { ucFriendEducation.Dispose(); ucFriendEducation = null; }
                 if (ucFriendSkill != null) { ucFriendSkill.Dispose(); ucFriendSkill = null; }

                 ProfessionalCareerHolderPanel.Child = null;
                 EducationHolderPanel.Child = null;
                 SkillHolderPanel.Child = null;
             }
             else
             {
                 BasicInfoHolderPanel.Child = null;
             }
         }

         private void FriendCareerMouseDown(object sender, MouseButtonEventArgs e)
         {
             if (ProfessionalCareerHolderPanel.Child == null)
             {
                 if (ucFriendProfessionalCareer == null) 
                 { 
                     ucFriendProfessionalCareer = new UCFriendProfessionalCareer();
                 }
                
                 if (FriendBasicInfoModel != null) ucFriendProfessionalCareer.LoadMyFriendsWorkToModels(FriendBasicInfoModel.ShortInfoModel.UserIdentity);
                 ProfessionalCareerHolderPanel.Child = ucFriendProfessionalCareer;

                 if (ucFriendEducation != null) { ucFriendEducation.Dispose(); ucFriendEducation = null; }
                 if (ucFriendSkill != null) { ucFriendSkill.Dispose(); ucFriendSkill = null; }

                 BasicInfoHolderPanel.Child = null;
                 EducationHolderPanel.Child = null;
                 SkillHolderPanel.Child = null;
             }
             else
             {
                 ucFriendProfessionalCareer.Dispose();
                 ucFriendProfessionalCareer = null;
                 ProfessionalCareerHolderPanel.Child = null;               
             }
         }

         private void educationMouseDown(object sender, MouseButtonEventArgs e)
         {
             if (EducationHolderPanel.Child == null)
             {
                 if (ucFriendEducation == null)
                 {
                     ucFriendEducation = new UCFriendEducation();
                 }
                
                 if (FriendBasicInfoModel != null) ucFriendEducation.LoadMyFriendsEduToModels(FriendBasicInfoModel.ShortInfoModel.UserIdentity);
                 EducationHolderPanel.Child = ucFriendEducation;

                 if (ucFriendProfessionalCareer != null) { ucFriendProfessionalCareer.Dispose(); ucFriendProfessionalCareer = null; }
                 if (ucFriendSkill != null) { ucFriendSkill.Dispose(); ucFriendSkill = null; }

                 BasicInfoHolderPanel.Child = null;
                 ProfessionalCareerHolderPanel.Child = null;                                
                 SkillHolderPanel.Child = null;
             }
             else
             {
                 ucFriendEducation.Dispose();
                 ucFriendEducation = null;
                 EducationHolderPanel.Child = null;
             }
         }

         private void skill_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
         {
             if (SkillHolderPanel.Child == null)
             {
                 if (ucFriendSkill == null)
                 {
                     ucFriendSkill = new UCFriendSkill();
                 }
                
                 if (FriendBasicInfoModel != null) ucFriendSkill.LoadMyFriendsSkillsToModels(FriendBasicInfoModel.ShortInfoModel.UserIdentity);
                 SkillHolderPanel.Child = ucFriendSkill;

                 if (ucFriendProfessionalCareer != null) { ucFriendProfessionalCareer.Dispose(); ucFriendProfessionalCareer = null; }
                 if (ucFriendEducation != null) { ucFriendEducation.Dispose(); ucFriendEducation = null; }

                 BasicInfoHolderPanel.Child = null;
                 ProfessionalCareerHolderPanel.Child = null;
                 EducationHolderPanel.Child = null;                
             }
             else
             {
                 ucFriendSkill.Dispose();
                 ucFriendSkill = null;
                 SkillHolderPanel.Child = null;
             }
         }*/

        private void WorkEducationSkillRequest()
        {
            if (FriendShortInfoModel != null && FriendShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            {
                SendDataToServer.WorkEducationSkillRequest(FriendShortInfoModel.UserTableID);
            }
        }
    }
}

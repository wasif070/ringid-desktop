﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.UI.Circle;
using View.Utility;
using View.Utility.Chat.Service;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.FriendProfile;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendInfo.xaml
    /// </summary>
    public partial class UCFriendInfo : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendInfo).Name);

        #region "Private Member"
        #endregion

        #region "Public Member"
        #endregion

        #region "Property"
        //private UserBasicInfoModel _FriendBasicInfoModel;
        //public UserBasicInfoModel FriendBasicInfoModel
        //{
        //    get { return _FriendBasicInfoModel; }
        //    set { _FriendBasicInfoModel = value; }
        //}

        private BlockedNonFriendModel _BlockedNonFriendModel;
        public BlockedNonFriendModel BlockedNonFriendModel
        {
            get { return _BlockedNonFriendModel; }
            set { _BlockedNonFriendModel = value; }
        }

        private UserBasicInfoModel _FriendBasicInfoModel;
        public UserBasicInfoModel FriendBasicInfoModel
        {
            get { return _FriendBasicInfoModel; }
            set
            {
                if (value == _FriendBasicInfoModel)
                    return;

                _FriendBasicInfoModel = value;
                this.OnPropertyChanged("FriendBasicInfoModel");
            }
        }

        //public UCCircleViewWrapper GetCircleViewWrapper
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.GetCircleViewWrapper;
        //    }
        //}
        #endregion Property

        #region "Constructor"
        public UCFriendInfo(UserBasicInfoModel model)
        {
            InitializeComponent();
            this.FriendBasicInfoModel = model;
            this.BlockedNonFriendModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(model.ShortInfoModel.UserTableID);
            this.DataContext = this;
        }
        #endregion

        #region "Command"
        private ICommand _AccessChangeCommand;
        public ICommand AccessChangeCommand
        {
            get
            {
                if (_AccessChangeCommand == null)
                {
                    _AccessChangeCommand = new RelayCommand(param => OnAccessChangeClicked(param));
                }
                return _AccessChangeCommand;
            }
        }

        private ICommand _HideAllFeedCommand;
        public ICommand HideAllFeedCommand
        {
            get
            {
                if (_HideAllFeedCommand == null)
                {
                    _HideAllFeedCommand = new RelayCommand(param => HideAllFeed());
                }
                return _HideAllFeedCommand;
            }
        }

        private ICommand _ReportUserCommand;
        public ICommand ReportUserCommand
        {
            get
            {
                if (_ReportUserCommand == null)
                {
                    _ReportUserCommand = new RelayCommand(param => ReportUser());
                }
                return _ReportUserCommand;
            }
        }
        #endregion

        #region "Event Handler"
        private void btnCircle_Click(object sender, RoutedEventArgs e)
        {
            //CircleUtility.Instance.LoadCirclesList();
            //UCCirclePopup.Instance.Show(btnCircle, FriendBasicInfoModel);

            MainSwitcher.PopupController.GetCircleViewWrapper.Show();
            MainSwitcher.PopupController.circleViewWrapper.SetUserModel(FriendBasicInfoModel);
            MainSwitcher.PopupController.circleViewWrapper.LoadCirclesList();
        }

        //private void mnuReportUser_Click(object sender, RoutedEventArgs e)
        //{
        //    ReportUser();
        //}               

        //private void mnuHideAllFeed_Click(object sender, RoutedEventArgs e)
        //{
        //    HideAllFeed();
        //}

        #endregion

        #region "Private Method"
        private void OnAccessChangeClicked(object param)
        {
            try
            {
                int accessType = Int32.Parse(param.ToString());
                HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, FriendBasicInfoModel, BlockedNonFriendModel, null, accessType == StatusConstants.FULL_ACCESS);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnAccessChangeClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ReportUser()
        {
            MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(Guid.Empty, StatusConstants.SPAM_USER, FriendBasicInfoModel.ShortInfoModel.UserTableID);
        }

        private void HideAllFeed()
        {
            //if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(FriendBasicInfoModel.ShortInfoModel.UserTableID))
            //{
            //    HideUnhideFeedsUsers.UnhideAllNewsFeedsByUser(FriendBasicInfoModel.ShortInfoModel.UserTableID);
            //    LabelHideAllFeed = "Hide feed updates";
            //}
            //else
            //{
            //    MessageBoxResult result = CustomMessageBox.ShowQuestion("You won't see feeds from " + FriendBasicInfoModel.ShortInfoModel.FullName + " anymore.");
            //    if (result == MessageBoxResult.Yes)
            //    {
            //        HideUnhideFeedsUsers.HideAllNewsFeedsByUser(FriendBasicInfoModel.ShortInfoModel.UserTableID, FriendBasicInfoModel.ShortInfoModel.FullName, 0);
            //        LabelHideAllFeed = "Show feed updates";
            //    }
            //}
            if (!FriendBasicInfoModel.ShortInfoModel.IsProfileHidden)
            {
                //hide
                //MessageBoxResult result = CustomMessageBox.ShowQuestion("You won't see feeds from " + FriendBasicInfoModel.ShortInfoModel.FullName + " anymore.");
                //if (result == MessageBoxResult.Yes)
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.HIDE_CONFIRMATIONS, "this profile"), "Hide confirmation!", String.Format("You won't see feeds from {0} anymore.", FriendBasicInfoModel.ShortInfoModel.FullName));
                if (isTrue)
                    MainSwitcher.ThreadManager().HideUser.StartThread(FriendBasicInfoModel.ShortInfoModel.UserTableID, FriendBasicInfoModel.ShortInfoModel);
            }
            else
            {
                //unhide
                MainSwitcher.ThreadManager().HideUser.StartThread(FriendBasicInfoModel.ShortInfoModel.UserTableID, FriendBasicInfoModel.ShortInfoModel, false);
            }
        }
        #endregion

        #region "Public Method"
        #endregion

        #region Utility Methods
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion Utility Methods

        #region "Loaded_Unloaded Events"
        private void UserControl_loaded(object sender, RoutedEventArgs e)
        {
            /*btnCircle.Click -= btnCircle_Click;
            btnCircle.Click += btnCircle_Click;*/
            /*mnuHideAllFeed.Click -= mnuHideAllFeed_Click;
            mnuHideAllFeed.Click += mnuHideAllFeed_Click;
            mnuReportUser.Click -= mnuReportUser_Click;
            mnuReportUser.Click += mnuReportUser_Click;*/
        }

        private void UserControl_unloaded(object sender, RoutedEventArgs e)
        {
            //btnCircle.Click -= btnCircle_Click;
            /*mnuHideAllFeed.Click -= mnuHideAllFeed_Click;
            mnuReportUser.Click -= mnuReportUser_Click;*/
        }
        #endregion
    }
}

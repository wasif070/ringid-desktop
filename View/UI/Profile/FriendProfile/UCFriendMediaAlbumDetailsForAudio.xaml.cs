﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using log4net;
using Models.Entity;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.Utility;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendMediaAlbumDetailsForAudio.xaml
    /// </summary>
    public partial class UCFriendMediaAlbumDetailsForAudio : UserControl, INotifyPropertyChanged
    {
        //private long userIdentity = 0;
        public MediaContentDTO mediaDto;
        public MediaContentModel mediaModel;
        private UCFriendProfile _UCFriendProfile = null;
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendMediaAlbumDetailsForAudio).Name);
        public UCFriendMediaAlbumDetailsForAudio(long userTableId)
        {
            CategoryAlbum = 2; // abum =1, singlemedia =  2;
            this.FriendIdentity = userTableId;
            if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == userTableId)
            {
                _UCFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
            }
            InitializeComponent();
            this.DataContext = this;
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            btnPlayAll.Click += PlayAll_Click;
            closeTheWidnow.Click += closeTheWidnow_Click;
            showMoreText.Click += ShowMore_PanelClick;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                closeTheWidnow_Click(null, null);
                btnPlayAll.Click -= PlayAll_Click;
                closeTheWidnow.Click -= closeTheWidnow_Click;
                showMoreText.Click -= ShowMore_PanelClick;
                ClearAudios();
            }
            catch (Exception ex)
            {

                log.Error(ex.Message + "\n" + ex.StackTrace + " " + ex.Message);

            }
        }

        private void ClearAudios()
        {
            try
            {
                if (FriendSingleMediaAudios != null && FriendSingleMediaAudios.Count > 0)
                {
                    DispatcherTimer _RemoveMembers = new DispatcherTimer();
                    _RemoveMembers.Interval = TimeSpan.FromMilliseconds(1000);
                    _RemoveMembers.Tick += (s, e) =>
                    {
                        lock (FriendSingleMediaAudios)
                        {
                            while (FriendSingleMediaAudios.Count > 0)
                            {
                                if (!this.IsLoaded)
                                {
                                    SingleMediaModel model = FriendSingleMediaAudios.FirstOrDefault();
                                    FriendSingleMediaAudios.Remove(model);
                                    GC.SuppressFinalize(model);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        _RemoveMembers.Stop();
                    };
                    _RemoveMembers.Stop();
                    _RemoveMembers.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + " " + ex.Message);
            }

        }

        #region"Property "
        private long _FriendIdentity;
        public long FriendIdentity
        {
            get { return _FriendIdentity; }
            set
            {
                _FriendIdentity = value;
                if (FriendIdentity == value) { return; }
                _FriendIdentity = value;
                OnPropertyChanged("FriendIdentity");
            }
        }
        private int _CategoryAlbum;
        public int CategoryAlbum
        {
            get { return _CategoryAlbum; }
            set
            {
                _CategoryAlbum = value;
                if (_CategoryAlbum == value) { return; }
                _CategoryAlbum = value;
                OnPropertyChanged("CategoryAlbum");
            }
        }
        private ObservableCollection<SingleMediaModel> _FriendSingleMediaAudios = new ObservableCollection<SingleMediaModel>();
        public ObservableCollection<SingleMediaModel> FriendSingleMediaAudios
        {
            get
            {
                return _FriendSingleMediaAudios;
            }
            set
            {
                _FriendSingleMediaAudios = value;
                this.OnPropertyChanged("FriendSingleMediaAudios");
            }
        }
        private string _AlbumName = string.Empty;
        public string AlbumName
        {
            get { return _AlbumName; }
            set
            {
                _AlbumName = value;
                this.OnPropertyChanged("AlbumName");
            }
        }
        private Guid _AlbumId;
        public Guid AlbumId
        {
            get { return _AlbumId; }
            set
            {
                if (_AlbumId == value) return;
                _AlbumId = value;
                OnPropertyChanged("AlbumId");
            }
        }
        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (_MediaType == value) return;
                _MediaType = value;
                OnPropertyChanged("MediaType");
            }
        }
        private Visibility _IsPlayButtonVisible = Visibility.Hidden;
        public Visibility IsPlayButtonVisible
        {
            get { return _IsPlayButtonVisible; }
            set
            {
                if (_IsPlayButtonVisible == value) return;
                _IsPlayButtonVisible = value;
                OnPropertyChanged("IsPlayButtonVisible");
            }
        }
        private System.Windows.Media.ImageSource _LoaderSmall;
        public System.Windows.Media.ImageSource LoaderSmall
        {
            get
            {
                return _LoaderSmall;
            }
            set
            {
                if (value == _LoaderSmall)
                    return;
                _LoaderSmall = value;
                OnPropertyChanged("LoaderSmall");
            }
        }
        private bool _IsNeeedToLoading = false;
        public bool IsNeeedToLoading
        {
            get
            {
                return _IsNeeedToLoading;
            }
            set
            {
                if (value == _IsNeeedToLoading)
                    return;
                _IsNeeedToLoading = value;
                OnPropertyChanged("IsNeeedToLoading");
            }
        }
        #endregion

        public void ShowLoader(bool neeedToLoading)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (neeedToLoading)
                {
                    IsNeeedToLoading = true;
                    LoaderSmall = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
                    GC.SuppressFinalize(LoaderSmall);
                }
                else
                {
                    IsNeeedToLoading = false;
                    LoaderSmall = null;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //public void LoadData(MediaContentDTO mediaDto)
        //{
        //    try
        //    {
        //        foreach (SingleMediaDTO SingleDto in mediaDto.MediaList)
        //        {
        //            lock (FriendSingleMediaAudios)
        //            {
        //                SingleMediaModel model = RingIDViewModel.Instance.GetExistingorNewSingleMediaModelFromDTO(SingleDto);//new SingleMediaModel();
        //                model.LoadData(SingleDto);
        //                FriendSingleMediaAudios.Add(model);
        //            }
        //        }
        //        if (mediaDto.TotalMediaCount > 0)
        //        {
        //            IsPlayButtonVisible = Visibility.Visible;
        //        }
        //        HideOrShowMoreVisibility();
        //    }
        //    catch (System.Exception ex)
        //    {

        //        log.Error("Exception in UCFriendMediaAlbumDetailsForAudio => " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);

        //    }
        //}

        public void HideOrShowMoreVisibility()
        {
            try
            {
                if (mediaDto != null && mediaDto.MediaList != null && (mediaDto.MediaList.Count < mediaDto.TotalMediaCount))
                {
                    ShowMore();
                }
                else
                {
                    HideShowMoreLoading();
                }
            }
            catch (System.Exception ex)
            {

                log.Error("Exception in UCFriendMediaAlbumDetailsForAudio => " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);

            }
        }

        private void closeTheWidnow_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
            if (_UCFriendProfile != null)
            {
                _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.ShowAfterMusicAlbumDetailsCancel();
            }
            //UCFriendProfile.Instance._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.ShowAfterMusicAlbumDetailsCancel();
        }

        private void PlayAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_UCFriendProfile != null && FriendSingleMediaAudios.Count > 0)
                {
                    MediaUtility.RunPlayList(false, Guid.Empty, FriendSingleMediaAudios, _UCFriendProfile.FriendBasicInfoModel.ShortInfoModel);
                    //RingPlayerViewModel.Instance.NavigateFrom = "album";
                }
            }
            catch (System.Exception ex)
            {

                log.Error("Exception in UCFriendMediaAlbumDetailsForAudio => " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);

            }
        }
        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                SingleMediaModel model = (SingleMediaModel)btn.DataContext;
                if (_UCFriendProfile != null && FriendSingleMediaAudios.Count > 0)
                {
                    int idx = FriendSingleMediaAudios.IndexOf(model);
                    if (idx >= 0)
                    {
                        MediaUtility.RunPlayList(false, Guid.Empty, FriendSingleMediaAudios, _UCFriendProfile.FriendBasicInfoModel.ShortInfoModel, idx);
                        //RingPlayerViewModel.Instance.NavigateFrom = "album";
                    }
                }
            }
            catch (System.Exception ex)
            {

                log.Error("Exception in UCFriendMediaAlbumDetailsForAudio => " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);

            }
        }

        public void ShowMore()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Visible;
                this.showMoreLoader.Visibility = Visibility.Collapsed;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            }
            catch (System.Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void ShowLoading()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Collapsed;
                this.showMoreLoader.Visibility = Visibility.Visible;
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            }
            catch (System.Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void HideShowMoreLoading()
        {
            this.showMorePanel.Visibility = Visibility.Collapsed;
            this.showMoreText.Visibility = Visibility.Visible;
            this.showMoreLoader.Visibility = Visibility.Collapsed;
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        }

        private void ShowMore_PanelClick(object sender, RoutedEventArgs e)
        {
            ShowLoading();
            if (_UCFriendProfile != null)
            {
                new ThradMediaAlbumContentList().StartThread(FriendSingleMediaAudios.Count, AlbumId, _UCFriendProfile.FriendBasicInfoModel.ShortInfoModel.UserTableID, MediaType, FriendIdentity);
                //     new MediaAlbumContentList(FriendSingleMediaAudios.Count, AlbumId, _UCFriendProfile.FriendBasicInfoModel.ShortInfoModel.UserTableID, MediaType, FriendIdentity);
            }
        }

        public void ShowThumbView()
        {
            Thumb_View_Panel.Visibility = Visibility.Visible;
            List_View_Panel.Visibility = Visibility.Collapsed;
        }
        public void ShowListView()
        {
            List_View_Panel.Visibility = Visibility.Visible;
            Thumb_View_Panel.Visibility = Visibility.Collapsed;
        }
        public void LoadCurrentView(string ButtonView)
        {
            if (!string.IsNullOrEmpty(ButtonView) && !ButtonView.Equals("Thumbnail_View"))
            {
                ShowListView();

            }
            else
            {
                ShowThumbView();
            }
        }
    }
}

﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using log4net;
using Models.Entity;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.Utility;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendMediaAlbumDetailsForVideo.xaml
    /// </summary>
    public partial class UCFriendMediaAlbumDetailsForVideo : UserControl, INotifyPropertyChanged
    {
        //private long userIdentity = 0;
        //public MediaContentModel mediaModel;
        public MediaContentDTO mediaDto;
        private UCFriendProfile _UCFriendProfile = null;
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendMediaAlbumDetailsForVideo).Name);
        public UCFriendMediaAlbumDetailsForVideo(long userTableId)
        {
            CategoryAlbum = 2; // abum =1, singlemedia =  2;
            this.FriendIdentity = userTableId;
            InitializeComponent();
            this.DataContext = this;
            if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == userTableId)
            {
                _UCFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
            }
        }

        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            btnPlayAll.Click += PlayAll_Click;
            closeTheWidnow.Click += closeTheWidnow_Click;
            showMoreText.Click += ShowMore_PanelClick;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                closeTheWidnow_Click(null, null);
                btnPlayAll.Click -= PlayAll_Click;
                closeTheWidnow.Click -= closeTheWidnow_Click;
                showMoreText.Click -= ShowMore_PanelClick;
                ClearVideos();
            }
            catch (Exception ex)
            {

                log.Error(ex.Message + "\n" + ex.StackTrace + " " + ex.Message);

            }
        }
        private void ClearVideos()
        {
            try
            {
                DispatcherTimer _RemoveMembers = new DispatcherTimer();
                _RemoveMembers.Interval = TimeSpan.FromMilliseconds(1000);
                _RemoveMembers.Tick += (s, e) =>
                {
                    lock (FriendSingleMediaVedios)
                    {
                        while (FriendSingleMediaVedios.Count > 0)
                        {
                            if (!this.IsLoaded)
                            {
                                SingleMediaModel model = FriendSingleMediaVedios.FirstOrDefault();
                                FriendSingleMediaVedios.Remove(model);
                                GC.SuppressFinalize(model);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    _RemoveMembers.Stop();
                };
                _RemoveMembers.Stop();
                _RemoveMembers.Start();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + " " + ex.Message);
            }
        }


        #region"Property "
        private long _FriendIdentity;
        public long FriendIdentity
        {
            get { return _FriendIdentity; }
            set
            {
                _FriendIdentity = value;
                if (FriendIdentity == value) { return; }
                _FriendIdentity = value;
                OnPropertyChanged("FriendIdentity");
            }
        }
        private int _CategoryAlbum;
        public int CategoryAlbum
        {
            get { return _CategoryAlbum; }
            set
            {
                _CategoryAlbum = value;
                if (_CategoryAlbum == value) { return; }
                _CategoryAlbum = value;
                OnPropertyChanged("CategoryAlbum");
            }
        }
        private ObservableCollection<SingleMediaModel> _FriendSingleMediaVedios = new ObservableCollection<SingleMediaModel>();
        public ObservableCollection<SingleMediaModel> FriendSingleMediaVedios
        {
            get
            {
                return _FriendSingleMediaVedios;
            }
            set
            {
                _FriendSingleMediaVedios = value;
                this.OnPropertyChanged("FriendSingleMediaVedios");
            }
        }
        private string _AlbumName = string.Empty;
        public string AlbumName
        {
            get { return _AlbumName; }
            set
            {
                _AlbumName = value;
                this.OnPropertyChanged("AlbumName");
            }
        }

        private Guid _AlbumId;
        public Guid AlbumId
        {
            get { return _AlbumId; }
            set
            {
                if (_AlbumId == value) return;
                _AlbumId = value;
                OnPropertyChanged("AlbumId");
            }
        }
        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (_MediaType == value) return;
                _MediaType = value;
                OnPropertyChanged("MediaType");
            }
        }
        private Visibility _IsPlayButtonVisible = Visibility.Hidden;
        public Visibility IsPlayButtonVisible
        {
            get { return _IsPlayButtonVisible; }
            set
            {
                if (_IsPlayButtonVisible == value) return;
                _IsPlayButtonVisible = value;
                OnPropertyChanged("IsPlayButtonVisible");
            }
        }

        private System.Windows.Media.ImageSource _LoaderSmall;
        public System.Windows.Media.ImageSource LoaderSmall
        {
            get
            {
                return _LoaderSmall;
            }
            set
            {
                if (value == _LoaderSmall)
                    return;
                _LoaderSmall = value;
                OnPropertyChanged("LoaderSmall");
            }
        }
        private bool _IsNeeedToLoading = false;
        public bool IsNeeedToLoading
        {
            get
            {
                return _IsNeeedToLoading;
            }
            set
            {
                if (value == _IsNeeedToLoading)
                    return;
                _IsNeeedToLoading = value;
                OnPropertyChanged("IsNeeedToLoading");
            }
        }
        #endregion "Property "

        public void ShowLoader(bool neeedToLoading)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (neeedToLoading)
                {
                    IsNeeedToLoading = true;
                    LoaderSmall = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
                    GC.SuppressFinalize(LoaderSmall);
                }
                else
                {
                    IsNeeedToLoading = false;
                    LoaderSmall = null;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //public void LoadData(MediaContentDTO mediaDto)
        //{
        //    try
        //    {
        //        foreach (SingleMediaDTO SingleDto in mediaDto.MediaList)
        //        {
        //            lock (FriendSingleMediaVedios)
        //            {
        //                SingleMediaModel model = RingIDViewModel.Instance.GetExistingorNewSingleMediaModelFromDTO(SingleDto);//new SingleMediaModel();
        //                model.LoadData(SingleDto);
        //                FriendSingleMediaVedios.Add(model);
        //            }
        //        }
        //        if (mediaDto.MediaList != null && mediaDto.MediaList.Count > 0)
        //        {
        //            IsPlayButtonVisible = Visibility.Visible;
        //        }
        //        HideOrShowMoreVisibility();
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Exception occured in UCFriendMediaAlbumDetailsForVideo => " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);

        //    }
        //}

        public void HideOrShowMoreVisibility()
        {
            try
            {
                if (mediaDto != null && mediaDto.MediaList != null && (mediaDto.MediaList.Count < mediaDto.TotalMediaCount))
                {
                    ShowMore();
                }
                else
                {
                    HideShowMoreLoading();
                }
            }
            catch (Exception ex)
            {

                log.Error("Exception occured in UCFriendMediaAlbumDetailsForVideo => " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);

            }
        }

        private void closeTheWidnow_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
            if (_UCFriendProfile != null)
            {
                _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.ShowAfterVideoAlbumDetailsCancel();
            }
        }

        private void PlayAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_UCFriendProfile != null && FriendSingleMediaVedios.Count > 0)
                {
                    MediaUtility.RunPlayList(false, Guid.Empty, FriendSingleMediaVedios, _UCFriendProfile.FriendBasicInfoModel.ShortInfoModel);
                    //RingPlayerViewModel.Instance.NavigateFrom = "album";
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception occured in UCFriendMediaAlbumDetailsForVideo => " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);
            }
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                SingleMediaModel model = (SingleMediaModel)btn.DataContext;
                if (_UCFriendProfile != null && FriendSingleMediaVedios.Count > 0)
                {
                    int idx = FriendSingleMediaVedios.IndexOf(model);
                    if (idx >= 0)
                    {
                        MediaUtility.RunPlayList(false, Guid.Empty, FriendSingleMediaVedios, _UCFriendProfile.FriendBasicInfoModel.ShortInfoModel, idx);
                        //RingPlayerViewModel.Instance.NavigateFrom = "album";
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Exception occured in UCFriendMediaAlbumDetailsForVideo => " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);

            }
        }

        public void ShowMore()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Visible;
                this.showMoreLoader.Visibility = Visibility.Collapsed;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            }
            catch (System.Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void ShowLoading()
        {
            try
            {
                this.showMorePanel.Visibility = Visibility.Visible;
                this.showMoreText.Visibility = Visibility.Collapsed;
                this.showMoreLoader.Visibility = Visibility.Visible;
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            }
            catch (System.Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void HideShowMoreLoading()
        {
            this.showMorePanel.Visibility = Visibility.Collapsed;
            this.showMoreText.Visibility = Visibility.Visible;
            this.showMoreLoader.Visibility = Visibility.Collapsed;
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        }

        private void ShowMore_PanelClick(object sender, RoutedEventArgs e)
        {
            ShowLoading();
            if (_UCFriendProfile != null)
            {
                new ThradMediaAlbumContentList().StartThread(FriendSingleMediaVedios.Count, AlbumId, _UCFriendProfile.FriendBasicInfoModel.ShortInfoModel.UserTableID, MediaType, FriendIdentity);
                //  new MediaAlbumContentList(FriendSingleMediaVedios.Count, AlbumId, _UCFriendProfile.FriendBasicInfoModel.ShortInfoModel.UserTableID, MediaType, FriendIdentity);
            }
        }
        public void ShowThumbView()
        {
            Thumb_View_Panel.Visibility = Visibility.Visible;
            List_View_Panel.Visibility = Visibility.Collapsed;
        }
        public void ShowListView()
        {
            List_View_Panel.Visibility = Visibility.Visible;
            Thumb_View_Panel.Visibility = Visibility.Collapsed;
        }
        public void LoadCurrentView(string ButtonView)
        {
            if (!string.IsNullOrEmpty(ButtonView) && !ButtonView.Equals("Thumbnail_View"))
            {
                ShowListView();
            }
            else
            {
                ShowThumbView();
            }
        }
    }
}
﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using View.BindingModels;
using View.UI.Circle;
using View.Utility;
using View.Utility.Auth;
using View.Utility.FriendList;
using View.ViewModel;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendProfile.xaml
    /// </summary>
    public partial class UCFriendProfile : UserControl, ISwitchable, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendProfile).Name);

        #region "Private Member"
        private BlockedNonFriendModel _BlockedNonFriendModel;
        #endregion

        #region "Public Member"
        //public static UCFriendProfile Instance;
        public long _FriendTableID;
        public UserBasicInfoModel _FriendBasicInfoModel;
        public UCFriendNewsFeeds _UCFriendNewsFeeds;
        public UCFriendPhotos _UCFriendPhotos;
        public UCFriendAbout _UCFriendAbout;
        public UCFriendContactLists _UCFriendContactList;
        public UCFriendProfileMediaAlbum _UCFriendProfileMediaAlbum;
        public UCFriendInfo _UCFriendInfo;
        public int TempFriendPreviewImageCount;
        #endregion

        #region "Property"
        public UserBasicInfoModel FriendBasicInfoModel
        {
            get { return _FriendBasicInfoModel; }
            set { _FriendBasicInfoModel = value; }
        }

        public BlockedNonFriendModel BlockedNonFriendModel
        {
            get { return _BlockedNonFriendModel; }
            set { _BlockedNonFriendModel = value; }
        }

        private ObservableCollection<AlbumModel> _FriendImageAlubms = new ObservableCollection<AlbumModel>();
        public ObservableCollection<AlbumModel> FriendImageAlubms
        {
            get
            {
                return _FriendImageAlubms;
            }
            set
            {
                _FriendImageAlubms = value;
                this.OnPropertyChanged("FriendImageAlubms");
            }
        }
        private ObservableCollection<ImageModel> _FriendProfileImageList = new ObservableCollection<ImageModel>();
        public ObservableCollection<ImageModel> FriendProfileImageList
        {
            get
            {
                return _FriendProfileImageList;
            }
            set
            {
                _FriendProfileImageList = value;
                this.OnPropertyChanged("FriendProfileImageList");
            }
        }

        private ObservableCollection<ImageModel> _FriendCoverImageList = new ObservableCollection<ImageModel>();
        public ObservableCollection<ImageModel> FriendCoverImageList
        {
            get
            {
                return _FriendCoverImageList;
            }
            set
            {
                _FriendCoverImageList = value;
                this.OnPropertyChanged("FriendCoverImageList");
            }
        }

        private ObservableCollection<ImageModel> _FriendFeedImageList = new ObservableCollection<ImageModel>();
        public ObservableCollection<ImageModel> FriendFeedImageList
        {
            get
            {
                return _FriendFeedImageList;
            }
            set
            {
                _FriendFeedImageList = value;
                this.OnPropertyChanged("FriendFeedImageList");
            }
        }

        private ObservableCollection<UserBasicInfoModel> _FriendList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> FriendList
        {
            get
            {
                return _FriendList;
            }
            set
            {
                _FriendList = value;
                this.OnPropertyChanged("FriendList");
            }
        }

        private ObservableCollection<UserBasicInfoModel> _TempFriendList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> TempFriendList
        {
            get
            {
                return _TempFriendList;
            }
            set
            {
                _TempFriendList = value;
                this.OnPropertyChanged("TempFriendList");
            }
        }

        private ObservableCollection<UserBasicInfoModel> _MutualList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> MutualList
        {
            get
            {
                return _MutualList;
            }
            set
            {
                _MutualList = value;
                this.OnPropertyChanged("MutualList");
            }
        }

        public int ProfileImageVisibilityValue { get; set; }
        public int TOTAL_FRIEND_IN_CONTACTLIST_COUNT;

        private int _MUTUAL_FRIEND_COUNT_IN_CONTACTLIST;
        public int MUTUAL_FRIEND_COUNT_IN_CONTACTLIST
        {
            get
            {
                return _MUTUAL_FRIEND_COUNT_IN_CONTACTLIST;
            }
            set
            {
                _MUTUAL_FRIEND_COUNT_IN_CONTACTLIST = value;
                this.OnPropertyChanged("MUTUAL_FRIEND_COUNT_IN_CONTACTLIST");
            }
        }

        private int _MutualFriendGap = 50;
        public int MutualFriendGap
        {
            get
            {
                return _MutualFriendGap;
            }
            set
            {
                _MutualFriendGap = value;
                this.OnPropertyChanged("MutualFriendGap");
            }
        }

        private UserShortInfoModel _UserShortInfoModelOfMutualFriendCircle1;
        public UserShortInfoModel UserShortInfoModelOfMutualFriendCircle1
        {
            get { return _UserShortInfoModelOfMutualFriendCircle1; }
            set
            {
                _UserShortInfoModelOfMutualFriendCircle1 = value;
                this.OnPropertyChanged("UserShortInfoModelOfMutualFriendCircle1");
            }
        }

        private UserShortInfoModel _UserShortInfoModelOfMutualFriendCircle2;
        public UserShortInfoModel UserShortInfoModelOfMutualFriendCircle2
        {
            get { return _UserShortInfoModelOfMutualFriendCircle2; }
            set
            {
                _UserShortInfoModelOfMutualFriendCircle2 = value;
                this.OnPropertyChanged("UserShortInfoModelOfMutualFriendCircle2");
            }
        }

        private UserShortInfoModel _UserShortInfoModelOfMutualFriendCircle3;
        public UserShortInfoModel UserShortInfoModelOfMutualFriendCircle3
        {
            get { return _UserShortInfoModelOfMutualFriendCircle3; }
            set
            {
                _UserShortInfoModelOfMutualFriendCircle3 = value;
                this.OnPropertyChanged("UserShortInfoModelOfMutualFriendCircle3");
            }
        }

        private UCFriendProfileContentSwitcher _UCFPContentSwitcherInstance;
        public UCFriendProfileContentSwitcher UCFPContentSwitcherInstance
        {
            get
            {
                if (_UCFPContentSwitcherInstance == null)
                {
                    _UCFPContentSwitcherInstance = new UCFriendProfileContentSwitcher();
                }
                return _UCFPContentSwitcherInstance;
            }
        }

        private bool _NoImageFound = true;
        public bool NoImageFound
        {
            get { return _NoImageFound; }
            set
            {
                _NoImageFound = value;
                this.OnPropertyChanged("NoImageFound");
            }
        }

        private int _selectedBorder = 0;
        public int SelectedBorder
        {
            get { return _selectedBorder; }
            set
            {
                _selectedBorder = value;
                this.OnPropertyChanged("SelectedBorder");
            }
        }

        private bool _IsCircle2Visible = false;
        public bool IsCircle2Visible
        {
            get { return _IsCircle2Visible; }
            set
            {
                _IsCircle2Visible = value;
                this.OnPropertyChanged("IsCircle2Visible");
            }
        }

        private bool _IsCircle3Visible = false;
        public bool IsCircle3Visible
        {
            get { return _IsCircle3Visible; }
            set
            {
                _IsCircle3Visible = value;
                this.OnPropertyChanged("IsCircle3Visible");
            }
        }

        private bool _IsMutualFreindRequestSend = false;
        public bool IsMutualFreindRequestSend
        {
            get { return _IsMutualFreindRequestSend; }
            set { _IsMutualFreindRequestSend = value; }
        }

        private Guid _AlbumNpUUID = Guid.Empty;
        public Guid AlbumNpUUID
        {
            get
            {
                return _AlbumNpUUID;
            }
            set
            {
                if (value == _AlbumNpUUID)
                    return;

                _AlbumNpUUID = value;
                this.OnPropertyChanged("AlbumNpUUID");
            }
        }

        private int _ImageAlbumCount;
        public int ImageAlbumCount
        {
            get
            {
                return _ImageAlbumCount;
            }
            set
            {
                if (value == _ImageAlbumCount)
                    return;

                _ImageAlbumCount = value;
                this.OnPropertyChanged("ImageAlbumCount");
            }
        }
        private Guid _PreviewPhotoID = Guid.Empty;
        public Guid PreviewPhotoID
        {
            get
            {
                return _PreviewPhotoID;
            }
            set
            {
                if (_PreviewPhotoID == value)
                    return;
                _PreviewPhotoID = value;
                OnPropertyChanged("PreviewPhotoID");
            }
        }
        #endregion

        #region Constructor
        public UCFriendProfile(UserBasicInfoModel model)
        {
            try
            {
                this.FriendBasicInfoModel = model;
                this._FriendTableID = model.ShortInfoModel.UserTableID;
                this.BlockedNonFriendModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(_FriendTableID);
                InitializeComponent();
                this.DataContext = this;
                this.gridContent.Children.Add(UCFPContentSwitcherInstance);
                FriendProfileImageList.Add(new ImageModel() { IsLoadMore = true });
                FriendCoverImageList.Add(new ImageModel() { IsLoadMore = true });
                FriendFeedImageList.Add(new ImageModel() { IsLoadMore = true });
                FriendImageAlubms.Add(new AlbumModel() { IsLoadMore = true });
                FriendList.Add(new UserBasicInfoModel());
                MutualList.Add(new UserBasicInfoModel());
            }
            catch (Exception ex)
            {

                log.Error("Error: UCFriendProfile() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        #endregion

        #region "Command"
        private ICommand _AccessChangeCommand;
        public ICommand AccessChangeCommand
        {
            get
            {
                if (_AccessChangeCommand == null)
                {
                    _AccessChangeCommand = new RelayCommand(param => OnAccessChangeClicked(param));
                }
                return _AccessChangeCommand;
            }
        }

        private ICommand _FriendNameClickCommand;
        public ICommand FriendNameClickCommand
        {
            get
            {
                if (_FriendNameClickCommand == null)
                {
                    _FriendNameClickCommand = new RelayCommand(param => OnClickFriendName());
                }
                return _FriendNameClickCommand;
            }
        }

        private ICommand _AboutPanelClickCommand;
        public ICommand AboutPanelClickCommand
        {
            get
            {
                if (_AboutPanelClickCommand == null)
                {
                    _AboutPanelClickCommand = new RelayCommand(param => OnAboutPanelClick());
                }
                return _AboutPanelClickCommand;
            }
        }
        
        private ICommand _PhotosPanelClickCommand;
        public ICommand PhotosPanelClickCommand
        {
            get
            {
                if (_PhotosPanelClickCommand == null)
                {
                    _PhotosPanelClickCommand = new RelayCommand(param => OnPhotosPanelClick());
                }
                return _PhotosPanelClickCommand;
            }
        }

        private ICommand _FriendsPanelClickCommand;
        public ICommand FriendsPanelClickCommand
        {
            get
            {
                if (_FriendsPanelClickCommand == null)
                {
                    _FriendsPanelClickCommand = new RelayCommand(param => OnFriendsPanelClick());
                }
                return _FriendsPanelClickCommand;
            }
        }
        
        private ICommand _MVPanelClickCommand;        
        public ICommand MVPanelClickCommand
        {
            get
            {
                if (_MVPanelClickCommand == null)
                {
                    _MVPanelClickCommand = new RelayCommand(param => OnMVPanelClick());
                }
                return _MVPanelClickCommand;
            }
        }

        private ICommand _MutualFriendClickCommand;
        public ICommand MutualFriendClickCommand
        {
            get
            {
                if (_MutualFriendClickCommand == null)
                {
                    _MutualFriendClickCommand = new RelayCommand(param => OnMutualFriendClick());
                }
                return _MutualFriendClickCommand;
            }
        }

        private ICommand _InfoClickCommand;
        public ICommand InfoClickCommand
        {
            get
            {
                if (_InfoClickCommand == null)
                {
                    _InfoClickCommand = new RelayCommand(param => InfoLoading());
                }
                return _InfoClickCommand;
            }
        }

        private ICommand _BackBtnClickCommand;
        public ICommand BackBtnClickCommand
        {
            get
            {
                if (_BackBtnClickCommand == null)
                {
                    _BackBtnClickCommand = new RelayCommand(param => OnBackBtnClick());
                }
                return _BackBtnClickCommand;
            }
        }

        private ICommand _UpArrowClickCommand;
        public ICommand UpArrowClickCommand
        {
            get
            {
                if (_UpArrowClickCommand == null)
                {
                    _UpArrowClickCommand = new RelayCommand(param => ScrollInitialize());
                }
                return _UpArrowClickCommand;
            }
        }        
        #endregion

        #region "Event Handler"
        private void OnFavoriteClick(object sender, RoutedEventArgs e)
        {
            try
            {
                new AddToFavouriteFriend().StartProcess(FriendBasicInfoModel);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnFavoriteClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //private void backBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    OnBackBtnClick();
        //}        

        private void pnlInfoContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DrawShape(pnlInfoContainer.ActualWidth);
        }

        private void btnCircle_Click(object sender, RoutedEventArgs e)
        {
            //CircleUtility.Instance.LoadCirclesList();
            //UCCirclePopup.Instance.Show(btnCircle, FriendBasicInfoModel);
            MainSwitcher.PopupController.GetCircleViewWrapper.Show();
            MainSwitcher.PopupController.circleViewWrapper.SetUserModel(FriendBasicInfoModel);
            MainSwitcher.PopupController.circleViewWrapper.LoadCirclesList();
        }

        //private void btnInfo_Click(object sender, RoutedEventArgs e)
        //{
        //    InfoLoading();
        //}

        //private void AboutPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(1, _FriendTableID, 0);
        //    ShowAbout();
        //}

        //private void PhotosPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(2, _FriendTableID, 0);
        //    ShowPhotos();
        //}

        //private void FriendsPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(3, _FriendTableID, 0);
        //    ShowFriends();
        //}

        //private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Home)
        //    {
        //        Scroll.ScrollToHome();
        //        e.Handled = true;
        //    }
        //    else if (e.Key == Key.End)
        //    {
        //        Scroll.ScrollToEnd();
        //        e.Handled = true;
        //    }
        //    else if (e.Key == Key.Escape &&  MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView != null
        //       && !RingPlayerViewModel.Instance.SmallPlayerOpen)
        //    {
        //        //UCGuiRingID.Instance.ucBasicMediaViewWrapper.ucBasicMediaView.ExitSmallPlayer();
        //        //UCGuiRingID.Instance.ucBasicMediaViewWrapper.ucBasicMediaView.DoCancel();
        //        RingPlayerViewModel.Instance.OnDoCancelRequested();
        //    }
        //}

        //private void MVPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(4, _FriendTableID, 0);
        //    ShowMusicVideos();
        //}

        //private void txtBlockMF_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    OnMutualFriendClick();
        //}        

        //private void txtBoxFriendName_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    RingIDViewModel.Instance.OnFriendProfileButtonClicked(FriendBasicInfoModel.ShortInfoModel.UserTableID);
        //}

        //private void imgUpArrow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    ScrollInitialize();
        //}        

        public void Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                if (Scroll.VerticalOffset >= 240)//Cover pic height + Call Chat Panel Height
                {
                    floatingPanel.Visibility = Visibility.Visible;
                }
                else
                {
                    floatingPanel.Visibility = Visibility.Collapsed;
                }

                //if (SelectedBorder == 0 && _UCFriendNewsFeeds != null)
                //{
                //    var scrollViewer = (ScrollViewer)sender;
                //    if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                //    {
                //        scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                //    }
                //    else if (e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight && !_UCFriendNewsFeeds.isBottomLoading && _UCFriendNewsFeeds.BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
                //    {
                //        Task.Factory.StartNew(() =>
                //        {
                //            _UCFriendNewsFeeds.BottomLoadFriendNewsFeeds();
                //        });

                //    }
                //    else if (e.VerticalChange < 0 && e.VerticalOffset > 0 && _UCFriendNewsFeeds.IsNewStatusVisible(Scroll) && !_UCFriendNewsFeeds.isTopLoading)
                //    {
                //        Task.Factory.StartNew(() =>
                //        {
                //            _UCFriendNewsFeeds.TopLoadFriendNewsFeeds(this.Scroll);
                //        });
                //    }
                //    else if (e.VerticalChange < 0 && e.VerticalOffset == 0 && _UCFriendNewsFeeds.IsNewStatusVisible(Scroll) && !_UCFriendNewsFeeds.isTopLoading)
                //    {
                //        Task.Factory.StartNew(() =>
                //        {
                //            _UCFriendNewsFeeds.TopLoadFriendNewsFeeds(this.Scroll, true);
                //        });
                //    }
                //}
                //else 
                if (SelectedBorder == 2 && _UCFriendPhotos != null)
                {
                    var scrollViewer = (ScrollViewer)sender;
                    if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                    {
                        scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                    }
                    else if ((e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight))
                    {
                        if (ImageAlbumCount != (FriendImageAlubms.Count - 1) && (UCImageContentView.Instance == null || !UCImageContentView.Instance.IsVisible))
                        {
                            _UCFriendPhotos.LoadMorePhotos();
                        }
                        else if (UCImageContentView.Instance != null && UCImageContentView.Instance.IsVisible)
                        {
                            UCImageContentView.Instance.LoadMorePhotos();
                        }
                    }
                }
                else if (SelectedBorder == 3 && _UCFriendContactList != null)
                {
                    var scrollViewer = (ScrollViewer)sender;
                    if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                    {
                        scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                    }
                    else if ((e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight))
                    {
                        _UCFriendContactList.LoadMoreFriends();
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: friendFeedScrlViewer_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        #endregion

        #region "Private Method"
        private void DrawShape(double width)
        {
            try
            {
                RectangleGeometry recGeometry = new RectangleGeometry();
                recGeometry.Rect = new Rect(0, 0, width, 80);

                EllipseGeometry ellipseGeometry = new EllipseGeometry();
                ellipseGeometry.RadiusX = 53;
                ellipseGeometry.RadiusY = 53;
                ellipseGeometry.Center = new System.Windows.Point(-30, 39);

                CombinedGeometry combineGeometry = new CombinedGeometry();
                combineGeometry.GeometryCombineMode = GeometryCombineMode.Exclude;
                combineGeometry.Geometry1 = recGeometry;
                combineGeometry.Geometry2 = ellipseGeometry;

                pathInfoContainer.Data = combineGeometry;
            }
            catch (Exception ex)
            {

                log.Error("Error: DrawShape() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnAccessChangeClicked(object param)
        {
            try
            {
                HelperMethods.ChangeBlockUnblockSettingsWrapper(StatusConstants.FULL_ACCESS, FriendBasicInfoModel, BlockedNonFriendModel, null, true);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnAccessChangeClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowAbout()
        {
            SelectedBorder = 1;
            //Scroll.ScrollChanged -= Scroll.ScrollPositionChanged;

            Scroll.ScrollChanged -= Scroll_ScrollChanged;
            Scroll.ScrollChanged += Scroll_ScrollChanged;
            if (_UCFriendAbout == null)
            {
                _UCFriendAbout = FriendBasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED ? new UCFriendAbout(FriendBasicInfoModel)
                    : new UCFriendAbout(FriendBasicInfoModel.ShortInfoModel.FullName);//frnd,nonfrnd
            }
            UCFPContentSwitcherInstance.Content = _UCFriendAbout;
        }

        private void SendFreindContactListRequest()
        {
            if (_UCFriendContactList == null)
            {
                /*_UCFriendContactList = new UCFriendContactLists();
                SendDataToServer.SendFreindContactListRequest(FriendBasicInfoModel.ShortInfoModel.UserTableID, 0);*/
                InitFriendPreviewImage();
                _UCFriendContactList = new UCFriendContactLists(FriendList, MutualList, FriendBasicInfoModel.ShortInfoModel.UserTableID);
                SendDataToServer.SendFreindContactListRequest(FriendBasicInfoModel.ShortInfoModel.UserTableID, 0, DefaultSettings.CONTENT_SHOW_LIMIT);
                if (!IsMutualFreindRequestSend)
                {
                    SendDataToServer.SendMutualFreindRequest(FriendBasicInfoModel.ShortInfoModel.UserTableID);  //ToShowThreeCircularImages
                    IsMutualFreindRequestSend = true;
                }
            }
        }

        private void OnClickFriendName()
        {
            RingIDViewModel.Instance.OnFriendProfileButtonClicked(FriendBasicInfoModel.ShortInfoModel.UserTableID);
        }

        private void OnAboutPanelClick()
        {
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(1, _FriendTableID, 0);
            ShowAbout();
        }

        private void OnPhotosPanelClick()
        {
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(2, _FriendTableID, 0);
            ShowPhotos();
        }

        private void OnFriendsPanelClick()
        {
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(3, _FriendTableID, 0);
            ShowFriends();
        }

        private void OnMVPanelClick()
        {
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInProfilesVisited(4, _FriendTableID, 0);
            ShowMusicVideos();
        }

        private void OnMutualFriendClick()
        {
            SelectedBorder = 3;
            //Scroll.ScrollChanged -= Scroll.ScrollPositionChanged;

            Scroll.ScrollChanged -= Scroll_ScrollChanged;
            Scroll.ScrollChanged += Scroll_ScrollChanged;

            SendFreindContactListRequest(); //while releasing memory
            _UCFriendContactList.TabList.SelectedIndex = 1;
            UCFPContentSwitcherInstance.Content = _UCFriendContactList;
        }

        private void OnBackBtnClick()
        {
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }

        private void ScrollInitialize()
        {
            Scroll.ScrollToVerticalOffset(0);
            Scroll.UpdateLayout();
        }
        #endregion

        #region "Public Method"
        public void ChangeProfileImageForThreeCircle(UserShortInfoModel model)
        {
            if (UserShortInfoModelOfMutualFriendCircle1 != null && UserShortInfoModelOfMutualFriendCircle1.ProfileImage == model.ProfileImage)
            {
                OnPropertyChanged("UserShortInfoModelOfMutualFriendCircle1");
            }
            else if (UserShortInfoModelOfMutualFriendCircle2 != null && UserShortInfoModelOfMutualFriendCircle2.ProfileImage == model.ProfileImage)
            {
                OnPropertyChanged("UserShortInfoModelOfMutualFriendCircle2");
            }
            else if (UserShortInfoModelOfMutualFriendCircle3 != null && UserShortInfoModelOfMutualFriendCircle3.ProfileImage == model.ProfileImage)
            {
                OnPropertyChanged("UserShortInfoModelOfMutualFriendCircle3");
            }
        }

        //public void RemoveTopOrBottomModels()
        //{
        //    bool _TopLoad = (Scroll.VerticalOffset == 0 || Scroll.VerticalOffset <= (Scroll.ScrollableHeight / 2));
        //    if (_TopLoad)
        //    {
        //        Scroll.ScrollChanged -= Scroll_ScrollChanged;
        //        _UCFriendNewsFeeds.FriendNewsFeeds.RemoveBottomModels();
        //        Scroll.ScrollChanged += Scroll_ScrollChanged;
        //    }
        //    else
        //    {
        //        Scroll.ScrollChanged -= Scroll_ScrollChanged;
        //        _UCFriendNewsFeeds.FriendNewsFeeds.RemoveTopModels();
        //        Scroll.ScrollChanged += Scroll_ScrollChanged;

        //        Application.Current.Dispatcher.Invoke((Action)delegate
        //        {
        //            Scroll.ScrollChanged -= Scroll_ScrollChanged;
        //            if (Scroll.ScrollableHeight > 50 && Scroll.VerticalOffset == Scroll.ScrollableHeight)
        //            {
        //                Scroll.ScrollToVerticalOffset(Scroll.ScrollableHeight - 50);
        //            }
        //            Scroll.ScrollChanged += Scroll_ScrollChanged;

        //        }, DispatcherPriority.Send);
        //    }
        //}

        //public void LoadFriendProfileImage(List<ImageModel> sortedList, int count)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            foreach (var image in sortedList)
        //            {
        //                lock (this.FriendProfileImageList)
        //                {
        //                    if (!this.FriendProfileImageList.Any(p => p.ImageId == image.ImageId))
        //                    {
        //                        if (FriendProfileImageList.Count > 0 && FriendProfileImageList[0].Time < image.Time)
        //                        {
        //                            this.FriendProfileImageList.Insert(0, image);
        //                        }
        //                        else
        //                            this.FriendProfileImageList.Insert(FriendProfileImageList.Count - 1, image);
        //                    }
        //                }
        //                Thread.Sleep(5);
        //            }

        //            if (_UCFriendPhotos != null)
        //            {
        //                /*if (_UCFriendPhotos._UCFriendProfilePhotosAlbum != null)
        //                    _UCFriendPhotos._UCFriendProfilePhotosAlbum.ShowHideShowMoreLoading(count > (this.FriendProfileImageList.Count - 1));
        //                else
        //                    _UCFriendPhotos.IsVisibleProfilePhotosLoader = Visibility.Collapsed;*/
        //                //to do
        //            }
        //            this.OnPropertyChanged("FriendProfileImageList");
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Error: LoadFriendProfileImage() ==> " + ex.Message + "\n" + ex.StackTrace);
        //        }
        //    });
        //}

        //public void LoadFriendCoverImage(List<ImageModel> sortedList, int count)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            foreach (var image in sortedList)
        //            {
        //                lock (this.FriendCoverImageList)
        //                {
        //                    if (!this.FriendCoverImageList.Any(p => p.ImageId == image.ImageId))
        //                    {
        //                        if (FriendCoverImageList.Count > 0 && FriendCoverImageList[0].Time < image.Time)
        //                        {
        //                            this.FriendCoverImageList.Insert(0, image);
        //                        }
        //                        else
        //                            this.FriendCoverImageList.Insert(FriendCoverImageList.Count - 1, image);
        //                    }
        //                }
        //                Thread.Sleep(5);
        //            }
        //            if (_UCFriendPhotos != null)
        //            {
        //                /*if (_UCFriendPhotos._UCFriendCoverPhotosAlbum != null)
        //                    _UCFriendPhotos._UCFriendCoverPhotosAlbum.ShowHideShowMoreLoading(count > (this.FriendCoverImageList.Count - 1));
        //                else
        //                    _UCFriendPhotos.IsVisibleCoverPhotosLoader = Visibility.Collapsed;*/
        //                //to do
        //            }
        //            this.OnPropertyChanged("FriendCoverImageList");
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Error: LoadFriendCoverImage() ==> " + ex.Message + "\n" + ex.StackTrace);
        //        }
        //    });
        //}

        //public void LoadFriendFeedImage(List<ImageModel> sortedList, int count)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            foreach (var image in sortedList)
        //            {
        //                lock (this.FriendFeedImageList)
        //                {
        //                    if (!this.FriendFeedImageList.Any(p => p.ImageId == image.ImageId))
        //                    {
        //                        if (FriendFeedImageList.Count > 0 && FriendFeedImageList[0].Time < image.Time)
        //                        {
        //                            this.FriendFeedImageList.Insert(0, image);
        //                        }
        //                        else
        //                            this.FriendFeedImageList.Insert(FriendFeedImageList.Count - 1, image);
        //                    }

        //                }
        //                Thread.Sleep(5);
        //            }
        //            if (_UCFriendPhotos != null)
        //            {
        //                /*if (_UCFriendPhotos._UCFriendFeedPhotosAlbum != null)
        //                    _UCFriendPhotos._UCFriendFeedPhotosAlbum.ShowHideShowMoreLoading(count > (this.FriendFeedImageList.Count - 1));
        //                else
        //                    _UCFriendPhotos.IsVisibleFeedPhotosLoader = Visibility.Collapsed;*/
        //                //to do
        //            }
        //            this.OnPropertyChanged("FriendFeedImageList");
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Error: LoadFriendCoverImage() ==> " + ex.Message + "\n" + ex.StackTrace);
        //        }
        //    });
        //}

        //public int TOTAL_FRIENDS;
        //public int MUTUAL_FRIENDS;         

        public void ProfileLoading()
        {
            InfoHolderPanel.Visibility = Visibility.Collapsed;
            gridProfileNewUX.Visibility = Visibility.Visible;
        }

        public void InfoLoading()
        {
            InfoHolderPanel.Visibility = Visibility.Visible;
            gridProfileNewUX.Visibility = Visibility.Collapsed;
            if (_UCFriendInfo == null) _UCFriendInfo = new UCFriendInfo(FriendBasicInfoModel);
            InfoHolderPanel.Child = _UCFriendInfo;
            //if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(FriendBasicInfoModel.ShortInfoModel.UserTableID))
            //{
            //    //_UCFriendInfo.LabelHideAllFeed = "Show feed updates";
            //}
            //else
            //{
            //   // _UCFriendInfo.LabelHideAllFeed = "Hide feed updates";
            //}
        }

        public bool UtilizeState(object state)
        {
            return true;
        }

        public void LoadData()
        {
            if (_UCFriendContactList != null)
            {
                _UCFriendContactList.TOTAL_FRIEND_COUNT = TOTAL_FRIEND_IN_CONTACTLIST_COUNT;
                //_UCFriendContactList.MUTUAL_FRIEND_COUNT = MUTUAL_FRIEND_COUNT_IN_CONTACTLIST;
                _UCFriendContactList.MakeShowMoreVisible((FriendList.Count - 1) < TOTAL_FRIEND_IN_CONTACTLIST_COUNT);
                //_UCFriendContactList.Check_Search();
            }
        }

        public void ShowSelectedTab(int TabIndxInProfile)
        {
            switch (TabIndxInProfile)
            {
                case 0:
                    ShowPost();
                    break;
                case 1: //about
                    ShowAbout();
                    break;
                case 2://photos
                    if (FriendBasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        ShowPhotos();
                    else
                        ShowPost();
                    break;
                case 3://Friends
                    ShowFriends();
                    break;
                case 4://Musics
                    if (FriendBasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        ShowMusicVideos();
                    else
                        ShowPost();
                    break;
                default:
                    break;
            }
        }

        public void ShowPost()
        {
            SelectedBorder = 0;

            Scroll.ScrollChanged -= Scroll_ScrollChanged;

            //TODO
            if (_UCFriendNewsFeeds == null || RingIDViewModel.Instance.ProfileFriendCustomFeeds.UserProfileUtId != FriendBasicInfoModel.ShortInfoModel.UserTableID)
            {
                _UCFriendNewsFeeds = new UCFriendNewsFeeds(FriendBasicInfoModel.ShortInfoModel.UserIdentity, FriendBasicInfoModel.ShortInfoModel.UserTableID, Scroll);
            }
            else
            {
                Scroll.ScrollToVerticalOffset(0);
                Scroll.UpdateLayout();
            }
            if (_UCFriendNewsFeeds != null && FriendBasicInfoModel != null && FriendBasicInfoModel.ShortInfoModel != null)
                _UCFriendNewsFeeds.FriendShipStatus = FriendBasicInfoModel.ShortInfoModel.FriendShipStatus;
            UCFPContentSwitcherInstance.Content = _UCFriendNewsFeeds;
        }

        public void RequestForPreviewImageOfFeedContact()
        {
            /* if (FriendBasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
             {
                 if (_UCFriendPhotos == null)//ForFriendLatestFeedImages
                 {
                     _UCFriendPhotos = new UCFriendPhotos(FriendBasicInfoModel.ShortInfoModel.UserTableID);
                     SendDataToServer.SendAlbumRequest(FriendBasicInfoModel.ShortInfoModel.UserTableID, DefaultSettings.FEED_IMAGE_ALBUM_ID, 0);
                     SendDataToServer.SendAlbumRequest(FriendBasicInfoModel.ShortInfoModel.UserTableID, DefaultSettings.PROFILE_IMAGE_ALBUM_ID, 0);
                     SendDataToServer.SendAlbumRequest(FriendBasicInfoModel.ShortInfoModel.UserTableID, DefaultSettings.COVER_IMAGE_ALBUM_ID, 0);
                 }
             }*///to do
             SendFreindContactListRequest(); //ToShowFourImages            
        }

        public void ShowPhotos()
        {
            SelectedBorder = 2;
            if (FriendBasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            {
                //Scroll.ScrollChanged -= Scroll.ScrollPositionChanged;

                Scroll.ScrollChanged -= Scroll_ScrollChanged;
                Scroll.ScrollChanged += Scroll_ScrollChanged;

                if (_UCFriendPhotos == null)
                {
                    _UCFriendPhotos = new UCFriendPhotos(FriendBasicInfoModel.ShortInfoModel.UserTableID);
                    _UCFriendPhotos.FriendImageAlubms = FriendImageAlubms;
                }
                UCFPContentSwitcherInstance.Content = _UCFriendPhotos;
            }
            else
            {
                ShowAbout();
            }            
        }

        public void ShowFriends()
        {
            SelectedBorder = 3;
            //Scroll.ScrollChanged -= Scroll.ScrollPositionChanged;

            Scroll.ScrollChanged -= Scroll_ScrollChanged;
            Scroll.ScrollChanged += Scroll_ScrollChanged;
            SendFreindContactListRequest();
            _UCFriendContactList.Check_Search();
            UCFPContentSwitcherInstance.Content = _UCFriendContactList;
        }

        public void ShowMusicVideos()
        {
            SelectedBorder = 4;
            if (FriendBasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            {
                //Scroll.ScrollChanged -= Scroll.ScrollPositionChanged;

                Scroll.ScrollChanged -= Scroll_ScrollChanged;
                Scroll.ScrollChanged += Scroll_ScrollChanged;
                if (_UCFriendProfileMediaAlbum == null)
                {
                    _UCFriendProfileMediaAlbum = new UCFriendProfileMediaAlbum(FriendBasicInfoModel.ShortInfoModel.UserIdentity, FriendBasicInfoModel.ShortInfoModel.UserTableID);
                }
                UCFPContentSwitcherInstance.Content = _UCFriendProfileMediaAlbum;
            }
            else
            {
                ShowAbout();
            }
        }

        public void InitFriendPreviewImage()
        {
            TempFriendPreviewImageCount = 0;
            for (int i = 0; i < 4; i++)
            {
                UserBasicInfoModel model = new UserBasicInfoModel();
                TempFriendList.Add(model);
            }
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion "Utility Methods"

        #region "Loaded_Unloaded Events"
        private void UserControl_loaded(object sender, RoutedEventArgs e)
        {
            //Scroll.PreviewKeyDown -= Scroll_PreviewKeyDown;
            //Scroll.PreviewKeyDown += Scroll_PreviewKeyDown;
            //Scroll.ScrollChanged -= Scroll_ScrollChanged;
            //Scroll.ScrollChanged += Scroll_ScrollChanged;
            favoriteButton.Click -= OnFavoriteClick;
            favoriteButton.Click += OnFavoriteClick;
            //txtBoxFriendName.PreviewMouseDown -= txtBoxFriendName_PreviewMouseDown;
            //txtBoxFriendName.PreviewMouseDown += txtBoxFriendName_PreviewMouseDown;
            /*btnInfo.Click -= btnInfo_Click;
            btnInfo.Click += btnInfo_Click;
            txtBlockMF.MouseLeftButtonDown -= txtBlockMF_MouseLeftButtonDown;
            txtBlockMF.MouseLeftButtonDown += txtBlockMF_MouseLeftButtonDown;
            AboutPanel.MouseLeftButtonDown -= AboutPanel_MouseLeftButtonDown;
            AboutPanel.MouseLeftButtonDown += AboutPanel_MouseLeftButtonDown;
            PhotosPanel.MouseLeftButtonDown -= PhotosPanel_MouseLeftButtonDown;
            PhotosPanel.MouseLeftButtonDown += PhotosPanel_MouseLeftButtonDown;
            FriendsPanel.MouseLeftButtonDown -= FriendsPanel_MouseLeftButtonDown;
            FriendsPanel.MouseLeftButtonDown += FriendsPanel_MouseLeftButtonDown;
            MVPanel.MouseLeftButtonDown -= MVPanel_MouseLeftButtonDown;
            MVPanel.MouseLeftButtonDown += MVPanel_MouseLeftButtonDown;*/
            /*AddFriendPanel.MouseLeftButtonDown -= AddFriendPanel_MouseLeftButtonDown;
            AddFriendPanel.MouseLeftButtonDown += AddFriendPanel_MouseLeftButtonDown;*/
            //mnuAddtoRingIdCircleFrnd.Click -= btnCircle_Click;
            //mnuAddtoRingIdCircleFrnd.Click += btnCircle_Click;
            //backBtn.Click -= backBtn_Click;
            //backBtn.Click += backBtn_Click;
            /*btnfloatingInfo.Click -= btnInfo_Click;
            btnfloatingInfo.Click += btnInfo_Click;*/
            //imgUpArrow.MouseLeftButtonDown -= imgUpArrow_MouseLeftButtonDown;
            //imgUpArrow.MouseLeftButtonDown += imgUpArrow_MouseLeftButtonDown;
        }

        private void UserControl_unloaded(object sender, RoutedEventArgs e)
        {
            //Scroll.PreviewKeyDown -= Scroll_PreviewKeyDown;
            //Scroll.ScrollChanged -= Scroll_ScrollChanged;
            //pnlInfoContainer.SizeChanged -= pnlInfoContainer_SizeChanged;
            favoriteButton.Click -= OnFavoriteClick;
            //txtBoxFriendName.PreviewMouseDown -= txtBoxFriendName_PreviewMouseDown;
            //btnInfo.Click -= btnInfo_Click;
            /*txtBlockMF.MouseLeftButtonDown -= txtBlockMF_MouseLeftButtonDown;
            AboutPanel.MouseLeftButtonDown -= AboutPanel_MouseLeftButtonDown;
            PhotosPanel.MouseLeftButtonDown -= PhotosPanel_MouseLeftButtonDown;
            FriendsPanel.MouseLeftButtonDown -= FriendsPanel_MouseLeftButtonDown;
            MVPanel.MouseLeftButtonDown -= MVPanel_MouseLeftButtonDown;*/
            //AddFriendPanel.MouseLeftButtonDown -= AddFriendPanel_MouseLeftButtonDown;
            //mnuAddtoRingIdCircleFrnd.Click -= btnCircle_Click;
            //backBtn.Click -= backBtn_Click;
            //btnfloatingInfo.Click -= btnInfo_Click;
            floatingPanel.Visibility = Visibility.Collapsed;
            //imgUpArrow.MouseLeftButtonDown -= imgUpArrow_MouseLeftButtonDown;
        }
        #endregion
    }
}

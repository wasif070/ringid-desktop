﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using Models.Utility.Chat;
using View.BindingModels;
using View.Constants;
using View.UI.Chat;
using View.UI.PopUp;
using View.Utility;
using View.Utility.audio;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.Recent;
using View.Utility.Recorder;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendChatCall.xaml
    /// </summary>
    public partial class UCFriendChatCallPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendChatCallPanel).Name);

        #region CHAT DATA MEMBERS

        public event PropertyChangedEventHandler PropertyChanged;
        private UserBasicInfoModel _FriendBasicInfoModel;
        private BlockedNonFriendModel _BlockedNonFriendModel;
        private UCFriendChatScrollViewer _ChatScrollViewer = null;
        private UCChatImageSideShow _ImagePreviewPanel = null;
        private UCChatStickerSlideShow _StickerPreviewPanel = null;
        private UCChatAudioRecorder _AudioRecorderPanel = null;
        private UCChatVideoRecorder _VideoRecorderPanel = null;
        private UCChatContactSharePreview _ContactSharePanel = null;
        private WNRecorderPreview _RecorderPreviewPanel = null;
        private UCChatMediaPreview _MediaPreviewPanel = null;
        private UCChatInformationView _ChatInformationPanel = null;
        private Storyboard _Storyboard;
        private string _TypingMessage = String.Empty;
        private ChatUserTyping _ChatUserTyping;
        private bool _IsDeletePanelVisible = true;
        private bool _IsSecretChatPanelVisible = true;
        private bool _IsSelectAllMode = true;
        private bool _IsSecretCheckOn = false;
        private bool _IsImagePreviewMode = false;
        private bool _IsStickerPreviewMode = false;
        private bool _IsAudioRecordMode = false;
        private bool _IsVideoRecordMode = false;
        private bool _IsContactShareMode = false;
        private bool _IsMediaPreviewMode = false;
        private bool _IsChatInformationMode = false;
        private bool _IsSeperateWindow = false;
        private string _RecentSelectedText = String.Empty;
        private string _SecretCountLeft = String.Empty;
        private string _SecretCountCenter = String.Empty;
        private string _SecretCountRight = String.Empty;
        private Visibility _IsWaterMarkVisible = Visibility.Visible;
        public int SecretTimerValue { get; set; }
        public RecentModel _EditModel { get; set; }

        private ICommand _ChatSendCommand;
        private ICommand _ChatStickerCommand;
        private ICommand _ChatEmoticonCommand;
        private ICommand _SelectAllCommand;
        private ICommand _ChatDeleteCommand;
        private ICommand _ChatDeleteCancelCommand;
        private ICommand _MultimediaCommand;
        private ICommand _MoreOptionsCommand;
        private ICommand _RecentChatCommand;
        private ICommand _PlusCommand;
        private ICommand _PlusPressCommand;
        private ICommand _PlusReleaseCommand;
        private ICommand _MinusCommand;
        private ICommand _MinusPressCommand;
        private ICommand _MinusReleaseCommand;
        private ICommand _AddMemberCommand;
        private ICommand _AccessChangeCommand;

        private DispatcherTimer _PlusTimer = null;
        private DispatcherTimer _MinusTimer = null;
        private DispatcherTimer _StickerOpenTimer = null;
        public long _FriendTableID;

        #endregion CHAT DATA MEMBERS

        #region Constructor

        public UCFriendChatCallPanel(UserBasicInfoModel model)
        {
            InitializeComponent();
            this.FriendBasicInfoModel = model;
            this._FriendTableID = model.ShortInfoModel.UserTableID;
            this.BlockedNonFriendModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(model.ShortInfoModel.UserTableID);
            this.pnlScrollViewer.Child = ChatScrollViewer;
            this.IsVisibleChanged += UCFriendChatCall_IsVisibleChanged;
            this.rtbChatArea.GotFocus += rtbChatArea_GotFocus;
            this.rtbChatArea.RichTextChanged += rtbChatArea_RichTextChanged;
            this.rtbChatArea.PreviewKeyDown += rtbChatArea_PreviewKeyDown;
            this.Unloaded += UCFriendChatCallPanel_Unloaded;
            this.Loaded += UCFriendChatCallPanel_Loaded;
            this.OpenCloseSecretChatPanel(0);
            this.OpenCloseChatDeletePanel(0);
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private void UCFriendChatCall_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                SwitchAudioRecorderView((bool)e.NewValue);
                SwitchVideoRecorderView((bool)e.NewValue);
            });
        }

        private void UCFriendChatCallPanel_Loaded(object sender, RoutedEventArgs e)
        {
            if (UCChatLogPanel.Instance != null)
            {
                UCChatLogPanel.Instance.SelectedContactID = _FriendTableID.ToString();
            }
        }

        private void UCFriendChatCallPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            if (UCChatLogPanel.Instance != null)
            {
                UCChatLogPanel.Instance.SelectedContactID = String.Empty;
            }
            if (_StickerOpenTimer != null)
            {
                _StickerOpenTimer.Stop();
                _StickerOpenTimer = null;
            }
            ChatScrollViewer.ReleaseScrollViewer();
        }

        private void rtbChatArea_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                //ChatService.StartFriendChat(_FriendTableID, FriendBasicInfoModel, BlockedNonFriendModel);

                if (IsSecretChatPanelVisible == false)
                {
                    OpenCloseSecretChatPanel();
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: rtbChatArea_GotFocus() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void rtbChatArea_RichTextChanged(string text)
        {
            try
            {
                if (text.Length > 4 && text.Length % 5 == 0)
                {
                    ChatService.TypingFriendChat(_FriendTableID, FriendBasicInfoModel, BlockedNonFriendModel);
                }

                if (text.Length > 0)
                {
                    IsWaterMarkVisible = Visibility.Collapsed;
                }
                else
                {
                    IsWaterMarkVisible = Visibility.Visible;
                    CancelChatEdit();
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: rtbChatArea_RichTextChanged() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void rtbChatArea_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Return)
                {
                    if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    {
                        e.Handled = true;
                        //btnChatSend.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                        OnChatSend(null);
                    }
                }
                else if (e.Key == Key.Escape)
                {
                    CancelChatEdit();
                }
                else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.V)
                {
                    string tempFilePath = String.Empty;
                    List<string> fileNames = new List<string>();
                    if (Clipboard.ContainsImage())
                    {
                        tempFilePath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + ChatService.GetServerTime() + "_" + DefaultSettings.LOGIN_RING_ID + ".jpg";
                        try
                        {
                            var image = Clipboard.GetImage();
                            var bmf = BitmapFrame.Create(image);

                            var encoder = new PngBitmapEncoder();
                            encoder.Frames.Add(bmf);

                            using (var fileStream = new System.IO.FileStream(tempFilePath, System.IO.FileMode.Create))
                            {
                                encoder.Save(fileStream);
                            }

                            fileNames.Add(tempFilePath);
                        }
                        catch (Exception ex)
                        {

                            log.Error("Error: rtbChatArea_PreviewKeyDown() => Saving Bitmap Image==> " + ex.Message + "\n" + ex.StackTrace);

                        }
                        e.Handled = true;
                    }

                    if (Clipboard.ContainsFileDropList())
                    {

                        var results = Clipboard.GetFileDropList();
                        var tempArray = new string[results.Count];
                        results.CopyTo(tempArray, 0);
                        fileNames.AddRange(tempArray.ToList());
                        e.Handled = true;
                    }

                    if (fileNames.Count > 0)
                    {
                        foreach (string fileName in fileNames)
                        {
                            FileInfo fi = new FileInfo(fileName);
                            if (!fi.Exists || fi.Length > DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE)
                            {
                                if (fi.Length > DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE)
                                {
                                    UIHelperMethods.ShowFailed(String.Format(NotificationMessages.MAX_FILE_TRANSFER_SIZE, DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE / (1024 * 1024 * 1024)), "File transer");
                                }
                                if (fileName.Equals(tempFilePath))
                                {
                                    fi.Delete();
                                }
                                return;
                            }
                        }
                        log.Debug("==> rtbChatArea_PreviewKeyDown => Count = " + fileNames.Count);
                        OnDropFile(fileNames.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: rtbChatArea_PreviewKeyDown() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void rtbChatArea_PreviewDragOver(object sender, DragEventArgs e)
        {
            if (!CanDrop(e, true))
            {
                e.Effects = DragDropEffects.None;
            }
            else
            {
                //e.Effects = DragDropEffects.All;
                e.Handled = true;
            }
        }

        private void grdGroupProfileContainer_Drop(object sender, DragEventArgs e)
        {
            OnDropGroupMember(e);
        }

        private void ChatMainContainer_DragOver(object sender, DragEventArgs e)
        {
            if (!CanDrop(e))
            {
                e.Effects = DragDropEffects.None;
                e.Handled = true;
            }
            else
            {
                //e.Effects = DragDropEffects.All;
            }
        }

        private void ChatMainContainer_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(UserBasicInfoModel)))
            {
                UserBasicInfoModel model = e.Data.GetData(typeof(UserBasicInfoModel)) as UserBasicInfoModel;
                if (model != null)
                {
                    List<UserBasicInfoModel> list = new List<UserBasicInfoModel>();
                    list.Add(model);
                    OnContactShare(list);
                }
            }
            else if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
            {
                string[] fileNames = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                if (fileNames != null && fileNames.Length > 0)
                {
                    log.Debug("==> ChatMainContainer_Drop =>  Count = " + fileNames.Length);
                    OnDropFile(fileNames);
                }
            }
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            if (_Storyboard != null)
            {
                _Storyboard.Completed -= Storyboard_Completed;
                _Storyboard = null;
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void loadInitHistory()
        {
            ChatScrollViewer.InilializeScrollViewer();

            if (_StickerOpenTimer == null)
            {
                _StickerOpenTimer = new DispatcherTimer();
                _StickerOpenTimer.Interval = TimeSpan.FromMilliseconds(1500);
                _StickerOpenTimer.Tick += (o, e) =>
                {
                    if (IsSeperateWindow == false && SettingsConstants.VALUE_RINGID_STICKER_VIEW)
                    {
                        RingIDViewModel.Instance.ExpandCommand.Execute(null);
                    }
                    _StickerOpenTimer.Stop();
                };
                _StickerOpenTimer.Stop();
                _StickerOpenTimer.Start();
            }
        }

        public void BuildAndSendChatPacket(string msg, int messageType, MarkertStickerImagesModel imageModel = null)
        {
            try
            {
                int anonymousSettingName = -1;
                if (!HelperMethods.HasAnonymousChatPermission(FriendBasicInfoModel))
                {
                    if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                    {
                        return;
                    }
                    else
                    {
                        anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                    }
                }

                HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
                {
                    if (result == false) return 0;

                    int accessType = -1;
                    if (!HelperMethods.HasFriendChatPermission(FriendBasicInfoModel, BlockedNonFriendModel, out accessType))
                    {
                        if (HelperMethods.ShowBlockWarning(FriendBasicInfoModel, accessType))
                        {
                            return 0;
                        }
                    }

                    HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, FriendBasicInfoModel, BlockedNonFriendModel, (status) =>
                    {
                        if (status == false) return 0;

                        MessageDTO messageDTO = new MessageDTO();
                        messageDTO.MessageType = messageType;
                        messageDTO.PacketType = ChatConstants.PACKET_TYPE_FRIEND_MESSAGE;
                        messageDTO.SenderTableID = DefaultSettings.LOGIN_TABLE_ID;
                        messageDTO.FriendTableID = _FriendTableID;
                        PacketTimeID packet = ChatService.GeneratePacketID(ModelUtility.CurrentTimeMillis());
                        messageDTO.PacketID = packet.PacketID;
                        messageDTO.MessageDate = packet.Time;
                        messageDTO.OriginalMessage = msg;
                        if (IsSecretCheckOn && SecretTimerValue > 0)
                        {
                            messageDTO.Timeout = SecretTimerValue;
                            messageDTO.IsSecretVisible = FriendBasicInfoModel.ShortInfoModel.IsSecretVisible;
                        }

                        if (messageType == (int)MessageType.DOWNLOAD_STICKER_MESSAGE)
                        {
                            if (FriendBasicInfoModel.ShortInfoModel.ImSoundEnabled)
                            {
                                AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_STICKER_SEND_TUNE);
                            }
                            HelperMethods.UpdateRingMarketStickerLastUsedDate(imageModel);
                        }

                        string link = String.Empty;
                        if (messageDTO.MessageType == (int)MessageType.PLAIN_MESSAGE || messageDTO.MessageType == (int)MessageType.EMOTICON_MESSAGE)
                        {
                            var matches = Regex.Matches(messageDTO.OriginalMessage, DefaultSettings.ONLY_HTTPS_HYPER_LINK_EXPRESSION);
                            foreach (Match match in matches)
                            {
                                if (!String.IsNullOrWhiteSpace(match.Value))
                                {
                                    link = match.Value.ToString();
                                    break;
                                }
                            }
                        }

                        if (String.IsNullOrWhiteSpace(link))
                        {
                            messageDTO.Status = ChatConstants.STATUS_SENDING;
                            ChatJSONParser.ParseMessage(messageDTO);
                            RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);
                            RecentLoadUtility.LoadRecentData(new RecentDTO { ContactID = _FriendTableID.ToString(), FriendTableID = _FriendTableID, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true });
                            ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = _FriendTableID.ToString(), FriendTableID = _FriendTableID, Message = messageDTO, Time = messageDTO.MessageDate });
                            ChatService.SendFriendChat(messageDTO.PacketID, messageDTO.FriendTableID, messageDTO.MessageType, messageDTO.Timeout, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.IsSecretVisible, FriendBasicInfoModel, BlockedNonFriendModel);
                        }
                        else
                        {
                            messageDTO.Status = ChatConstants.STATUS_SENDING;
                            new ChatLinkProcessor(_FriendTableID, 0, null, link, messageDTO).Start();
                        }

                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            rtbChatArea.Document.Blocks.Clear();
                        }, DispatcherPriority.Send);

                        return 1;
                    });

                    return 1;
                }, anonymousSettingName);
            }
            catch (Exception ex)
            {

                log.Error("Error: BuildAndSendChatPacket() ==> " + ex.Message + "\n" + ex.StackTrace);

            }

        }

        public void AddTyping()
        {
            try
            {
                if (_ChatUserTyping == null)
                {
                    _ChatUserTyping = new ChatUserTyping();
                    _ChatUserTyping.ProgressChanged += (o, e) =>
                    {
                        TypingMessage = (string)e.UserState;
                    };
                    _ChatUserTyping.AddUserInfo(_FriendTableID, FriendBasicInfoModel.ShortInfoModel.FullName);
                    _ChatUserTyping.StartThread();
                }
                else if (_ChatUserTyping.IsBusy)
                {
                    _ChatUserTyping.AddUserInfo(_FriendTableID, FriendBasicInfoModel.ShortInfoModel.FullName);
                }
                else
                {
                    _ChatUserTyping.AddUserInfo(_FriendTableID, FriendBasicInfoModel.ShortInfoModel.FullName);
                    _ChatUserTyping.StartThread();
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: AddTyping() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void OnChatSend(object param)
        {
            string msg = rtbChatArea.Text;
            int msgType = rtbChatArea.HasEmoticon ? (int)MessageType.EMOTICON_MESSAGE : (int)MessageType.PLAIN_MESSAGE;
            if (msg.Trim().Length > 0)
            {
                if (this._EditModel != null && this._EditModel.Message.ViewModel.IsEditMode && this._EditModel.Message.MessageType != MessageType.DELETE_MESSAGE)
                {
                    ChatHelpers.FriendChatMessageEdit(this._EditModel.Message, msg, msgType);
                    CancelChatEdit();
                }
                else
                {
                    this._EditModel = null;
                    BuildAndSendChatPacket(msg, msgType);
                }
            }
        }

        public void OnChatEdit(RecentModel recentModel)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.rtbChatArea.Document.Blocks.Clear();
                this.rtbChatArea.AppendText(recentModel.Message.Message);
                this.rtbChatArea.Focus();
                this._EditModel = recentModel;
            });
        }

        public void CancelChatEdit()
        {
            if (this._EditModel != null)
            {
                if (this._EditModel.Message.MessageType == MessageType.LINK_MESSAGE)
                {
                    this._EditModel.IsRecentOpened = false;
                    this._EditModel.Message.ViewModel.IsEditMode = false;
                    this._EditModel.Message.ViewModel.LoadViewStructure(this._EditModel.Message);
                    this._EditModel.Message.ViewModel.LoadViewType(this._EditModel.Message);
                    this._EditModel.IsRecentOpened = true;
                }
                else
                {
                    this._EditModel.Message.ViewModel.IsEditMode = false;
                }
                this.rtbChatArea.Document.Blocks.Clear();
                Keyboard.ClearFocus();
            }
            this._EditModel = null;
        }

        private void OnChatEmoticon(object param)
        {
            MainSwitcher.PopupController.EmoticonPopupWrapper.Show(btnChatEmoticon, UCEmoticonPopup.TYPE_CALL_CHAT_PANEL, (emo) =>
             {
                 if (!String.IsNullOrWhiteSpace(emo))
                 {
                     rtbChatArea.AppendText(emo);
                 }
                 return 0;
             });
        }

        private void OnChatMultimedia(object param)
        {
            try
            {
                if (popupMultimedia.IsOpen)
                {
                    popupMultimedia.IsOpen = false;
                }
                else
                {
                    popupMultimedia.IsOpen = true;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: OnChatMultimedia() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnSelectAll(object param)
        {
            try
            {
                IsSelectAllMode = !IsSelectAllMode;
                List<RecentModel> list = RingIDViewModel.Instance.GetRecentModelListByID(_FriendTableID).Where(P => P.Type != ChatConstants.SUBTYPE_DATE_TITLE).ToList();
                if (list != null)
                {
                    list.ForEach(P => P.IsChecked = !IsSelectAllMode);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: OnSelectAll() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnChatDelete(object param)
        {
            try
            {
                int type = int.Parse(param.ToString());

                List<string> forServerPacketIDs = new List<string>();
                List<string> forAllPacketIDs = new List<string>();
                List<string> forChatDBPacketIDs = new List<string>();
                List<string> forCallDBPacketIDs = new List<string>();
                List<string> forFileStreamPacketIDs = new List<string>();

                List<RecentModel> modelList = RingIDViewModel.Instance.GetRecentModelListByID(_FriendTableID).Where(P => P.IsChecked).ToList();
                if (modelList != null && modelList.Count > 0)
                {
                    //MessageBoxResult messageBoxResult = CustomMessageBox.ShowQuestion(NotificationMessages.DELETE_NOTIFICAITON);

                    //if (messageBoxResult == MessageBoxResult.Yes)
                    //{
                    bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.SURE_WANT_TO, "delete"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
                    if (isTrue)
                    {
                        foreach (RecentModel temp in modelList)
                        {
                            forAllPacketIDs.Add(temp.UniqueKey);
                            if (temp.Message != null)
                            {
                                forChatDBPacketIDs.Add(temp.Message.PacketID);
                                if (type == 1 && temp.Message.FromFriend == false && temp.Message.IsSecretChat == false)
                                {
                                    forServerPacketIDs.Add(temp.Message.PacketID);
                                }
                                if (temp.Message.MessageType == MessageType.FILE_STREAM)
                                {
                                    forFileStreamPacketIDs.Add(temp.Message.PacketID);
                                }

                                string filePath = ChatHelpers.GetChatFileDeleteFile(temp.Message.Message, temp.Message.PrevMessageType, temp.Message.PacketID, temp.Message.SenderTableID);
                                if (!String.IsNullOrWhiteSpace(filePath) && System.IO.File.Exists(filePath))
                                {
                                    try { System.IO.File.Delete(filePath); }
                                    catch { }
                                }

                                filePath = ChatHelpers.GetChatPreviewDeletePath(temp.Message.Message, temp.Message.PrevMessageType, temp.Message.PacketID);
                                if (!String.IsNullOrWhiteSpace(filePath) && System.IO.File.Exists(filePath))
                                {
                                    try { System.IO.File.Delete(filePath); }
                                    catch { }
                                }
                                temp.Message.MessageType = (int)MessageType.DELETE_MESSAGE;
                            }
                            else if (temp.CallLog != null)
                            {
                                forCallDBPacketIDs.Add(temp.CallLog.CallID);
                            }
                        }
                        RecentChatCallActivityDAO.DeleteChatHistory(_FriendTableID, 0, forChatDBPacketIDs);
                        RecentChatCallActivityDAO.DeleteCallHistory(_FriendTableID, forCallDBPacketIDs);
                        FileTransferSession.CancelTransferFileByPacketIDs(_FriendTableID, forFileStreamPacketIDs, false);

                        if (forServerPacketIDs.Count > 0)
                        {
                            ChatService.DeleteFriendChat(_FriendTableID, forServerPacketIDs);
                        }

                        RecentLoadUtility.RemoveRange(_FriendTableID, forAllPacketIDs);
                        OnChatDeleteCancel(null);
                        ChatScrollViewer.ReloadRecentList(null);
                    }
                }
                else
                    UIHelperMethods.ShowWarning("Please, select message", "Delete chat failed");
                //{
                //    CustomMessageBox.ShowWarning("Please, Select message");
                //}
            }
            catch (Exception ex)
            {

                log.Error("Error: OnChatDelete() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnChatDeleteCancel(object param)
        {
            OpenCloseChatDeletePanel();
        }

        private void OnMoreOptions(object param)
        {
            try
            {
                int type = Int32.Parse(param.ToString());
                popupMultimedia.IsOpen = false;

                if (type == 1)
                {
                    //Send Image From Directory
                    OpenFileDialog dialog = new OpenFileDialog();
                    dialog.Title = "Select picture";
                    dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
                    dialog.Multiselect = true;
                    if (dialog.ShowDialog() == true)
                    {
                        foreach (string fileName in dialog.FileNames)
                        {
                            FileInfo fi = new FileInfo(fileName);
                            if (fi.Length > DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE)
                            {
                                UIHelperMethods.ShowFailed(String.Format(NotificationMessages.MAX_FILE_TRANSFER_SIZE, DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE / (1024 * 1024)), "File transer");
                                // CustomMessageBox.ShowError(String.Format(NotificationMessages.MAX_FILE_TRANSFER_SIZE, DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE / (1024 * 1024)));
                                return;
                            }
                        }
                        log.Debug("==> OnMoreOptions  => Count = " + dialog.FileNames.Length);
                        ShowImagePreview(dialog.FileNames, (int)MessageType.IMAGE_FILE_FROM_GALLERY);
                    }
                }
                else if (type == 2)
                {
                    //Send Image From Webcam
                    WNWebcamCapture.Instance.ShowWindow((f) =>
                    {
                        ShowImagePreview(new string[] { f }, (int)MessageType.IMAGE_FILE_FROM_CAMERA);
                    });
                }
                else if (type == 3)
                {
                    //Send File From Directory
                    //CustomMessageBox.ShowWarning("File transfer is currently not supported on Desktop. This feature will be available soon.");
                    //return;

                    OpenFileDialog dialog = new OpenFileDialog();
                    dialog.Filter = "All files (*.*)|*.*";
                    dialog.Multiselect = true;
                    if (dialog.ShowDialog() == true)
                    {
                        int anonymousSettingName = -1;
                        if (!HelperMethods.HasAnonymousChatPermission(FriendBasicInfoModel))
                        {
                            if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                            {
                                return;
                            }
                            else
                            {
                                anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                            }
                        }

                        HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
                        {
                            if (result == false) return 0;

                            int accessType = -1;
                            if (!HelperMethods.HasFriendChatPermission(FriendBasicInfoModel, BlockedNonFriendModel, out accessType))
                            {
                                if (HelperMethods.ShowBlockWarning(FriendBasicInfoModel, accessType))
                                {
                                    return 0;
                                }
                            }

                            HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, FriendBasicInfoModel, BlockedNonFriendModel, (status) =>
                            {
                                if (status == false) return 0;

                                List<string> fileTransferList = new List<string>();
                                foreach (string fileName in dialog.FileNames)
                                {
                                    FileInfo fi = new FileInfo(fileName);
                                    if (fi.Length > DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE)
                                    {
                                        UIHelperMethods.ShowFailed(String.Format(NotificationMessages.MAX_FILE_TRANSFER_SIZE, DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE / (1024 * 1024 * 1024)), "File transer");
                                        return 0;
                                    }
                                    else
                                    {
                                        fileTransferList.Add(fileName);
                                    }
                                }

                                if (fileTransferList.Count > 0)
                                {
                                    new ChatFileTransferProcessor(_FriendTableID, 0, null, fileTransferList).Start();
                                }
                                return 1;
                            });

                            return 1;
                        }, anonymousSettingName);
                    }
                }
                else if (type == 4)
                {
                    //Send Audio Message
                    ShowAudioRecord();
                }
                else if (type == 5)
                {
                    //Send Video Message
                    ShowVideoRecord();
                }
                else if (type == 6)
                {
                    //Share Your Location
                    WNChatLocationShare.ShowWindow(this._FriendTableID, 0, null);
                }
                else if (type == 7)
                {
                    ShowContactShareView();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnMoreOptions() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnRecentChat(object param)
        {
            try
            {
                int type = Int32.Parse(param.ToString());
                lblRecent.ContextMenu.Visibility = Visibility.Hidden;
                btnRecent.ContextMenu.Visibility = Visibility.Hidden;
                lblRecent.ContextMenu.IsOpen = false;
                btnRecent.ContextMenu.IsOpen = false;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (type == 1)
                    {
                        //Today
                        RecentSelectedText = "Today";
                        ChatScrollViewer.ReloadRecentList(ChatConstants.DAY_TODAY);
                    }
                    else if (type == 2)
                    {
                        //Yesterday
                        RecentSelectedText = "Yesterday";
                        ChatScrollViewer.ReloadRecentList(ChatConstants.DAY_YESTERDAY);
                    }
                    else if (type == 3)
                    {
                        //7 Days
                        RecentSelectedText = "7 Days";
                        ChatScrollViewer.ReloadRecentList(ChatConstants.DAY_7_DAYS);
                    }
                    else if (type == 4)
                    {
                        //30 Days
                        RecentSelectedText = "30 Days";
                        ChatScrollViewer.ReloadRecentList(ChatConstants.DAY_30_DAYS);
                    }
                    else if (type == 5)
                    {
                        //3 Months
                        RecentSelectedText = "3 Months";
                        ChatScrollViewer.ReloadRecentList(ChatConstants.DAY_90_DAYS);
                    }
                    else if (type == 6)
                    {
                        //6 Months
                        RecentSelectedText = "6 Months";
                        ChatScrollViewer.ReloadRecentList(ChatConstants.DAY_180_DAYS);
                    }
                    else if (type == 7)
                    {
                        //1 Year
                        RecentSelectedText = "1 Year";
                        ChatScrollViewer.ReloadRecentList(ChatConstants.DAY_365_DAYS);
                    }
                }, DispatcherPriority.Background);
            }
            catch (Exception ex)
            {

                log.Error("Error: OnRecentChat() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void OpenCloseChatDeletePanel(double? delay = null)
        {
            IsDeletePanelVisible = !IsDeletePanelVisible;
            IsSelectAllMode = true;

            List<RecentModel> list = RingIDViewModel.Instance.GetRecentModelListByID(_FriendTableID).Where(P => P.Type != ChatConstants.SUBTYPE_DATE_TITLE).ToList();
            list.ForEach(P =>
            {
                P.IsChecked = false;
                P.IsCheckedVisible = IsDeletePanelVisible;
            });

            AnimationOnClicked(pnlChatDelete, pnlChatDelete.Opacity, (IsDeletePanelVisible ? 1.0d : 0.0d), (double.IsNaN(pnlChatDelete.Height) ? 0 : pnlChatDelete.Height), (IsDeletePanelVisible ? 40.0d : 0.0d), (delay != null ? (double)delay : 0.25d));
        }

        public void OpenCloseSecretChatPanel(double? delay = null)
        {
            SetSecretTimerValue(ChatHelpers.DEFAULT_SECRET_INDEX);
            IsSecretChatPanelVisible = !IsSecretChatPanelVisible;
            AnimationOnClicked(pnlSecretChat, pnlSecretChat.Opacity, (IsSecretChatPanelVisible ? 1.0d : 0.0d), (double.IsNaN(pnlSecretChat.Height) ? 0 : pnlSecretChat.Height), (IsSecretChatPanelVisible ? 45 : 0), (delay != null ? (double)delay : 0.25d));
        }

        private void AnimationOnClicked(FrameworkElement element, double sourceOpacity, double targetOpacity, double sourceHeight, double targetHeight, double delay)
        {
            try
            {
                DoubleAnimation opacityAnimation = new DoubleAnimation();
                opacityAnimation.From = sourceOpacity;
                opacityAnimation.To = targetOpacity;
                opacityAnimation.Duration = new Duration(TimeSpan.FromSeconds(targetOpacity == 0.0 ? delay / 2 : delay * 2));

                DoubleAnimation doubleAnimation = new DoubleAnimation();
                doubleAnimation.From = sourceHeight;
                doubleAnimation.To = targetHeight;
                doubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                if (_Storyboard != null)
                {
                    _Storyboard.Completed -= Storyboard_Completed;
                    _Storyboard = null;
                }

                _Storyboard = new Storyboard();
                _Storyboard.Children.Add(opacityAnimation);
                _Storyboard.Children.Add(doubleAnimation);
                Storyboard.SetTarget(opacityAnimation, element);
                Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath(StackPanel.OpacityProperty));
                Storyboard.SetTarget(doubleAnimation, element);
                Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(StackPanel.HeightProperty));
                _Storyboard.Completed += Storyboard_Completed;
                _Storyboard.Begin(element);
            }
            catch (Exception ex)
            {
                log.Error("Error: AnimationOnClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPlus(object param)
        {
            SetSecretTimerValue(SecretTimerValue + 1);
        }

        private bool CanPlus(object param)
        {
            return IsSecretCheckOn && SecretTimerValue < 120;
        }

        private void OnPlusPress(object param)
        {
            try
            {
                if (_PlusTimer == null)
                {
                    _PlusTimer = new DispatcherTimer();
                    _PlusTimer.Interval = TimeSpan.FromMilliseconds(100);
                    _PlusTimer.Tick += (o, e) =>
                    {
                        SetSecretTimerValue(SecretTimerValue + 1);
                    };
                }
                if (_MinusTimer != null)
                {
                    _MinusTimer.Stop();
                }
                _PlusTimer.Stop();
                _PlusTimer.Start();
            }
            catch (Exception ex)
            {

                log.Error("Error: OnPlusPress() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnMinus(object param)
        {
            SetSecretTimerValue(SecretTimerValue - 1);
        }

        private bool CanMinus(object param)
        {
            return IsSecretCheckOn && SecretTimerValue > 1;
        }

        private void OnMinusPress(object param)
        {
            try
            {
                if (_MinusTimer == null)
                {
                    _MinusTimer = new DispatcherTimer();
                    _MinusTimer.Interval = TimeSpan.FromMilliseconds(100);
                    _MinusTimer.Tick += (o, e) =>
                    {
                        SetSecretTimerValue(SecretTimerValue - 1);
                    };
                }
                if (_PlusTimer != null)
                {
                    _PlusTimer.Stop();
                }
                _MinusTimer.Stop();
                _MinusTimer.Start();
            }
            catch (Exception ex)
            {

                log.Error("Error: OnMinusPress() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnRelease(object param)
        {
            try
            {
                if (_PlusTimer != null)
                {
                    _PlusTimer.Stop();
                }
                if (_MinusTimer != null)
                {
                    _MinusTimer.Stop();
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: OnRelease() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void SetSecretTimerValue(int idx)
        {
            try
            {
                if (idx >= 1 && idx <= 120)
                {
                    SecretTimerValue = idx;
                    SecretCountLeft = ChatHelpers.SECONDS_ARRAY[idx - 1];
                    SecretCountCenter = ChatHelpers.SECONDS_ARRAY[idx];
                    SecretCountRight = ChatHelpers.SECONDS_ARRAY[idx + 1];
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: SetSecretTimerValue() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private bool CanDrop(DragEventArgs e, bool fromChatArea = false)
        {
            bool dropEnabled = true;

            try
            {
                if (fromChatArea == false && (e.Data.GetData(DataFormats.Text, true) as string) != null)
                {
                    dropEnabled = false;
                }
                else if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
                {
                    string[] fileNames = e.Data.GetData(DataFormats.FileDrop, true) as string[];

                    if (fileNames != null && fileNames.Length > 0)
                    {
                        foreach (string fileName in fileNames)
                        {
                            FileInfo fi = new FileInfo(fileName);
                            if (!fi.Exists || fi.Length > DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE)
                            {
                                dropEnabled = false;
                                break;
                            }
                        }
                    }
                }
                else if (e.Data.GetDataPresent(typeof(UserBasicInfoModel)))
                {

                }
                else
                {
                    dropEnabled = false;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: CanDropFile() ==> " + ex.Message + "\n" + ex.StackTrace);

            }

            return dropEnabled;
        }

        private void OnDropFile(string[] fileNames)
        {
            try
            {
                log.Debug("==> OnDropFile ");
                int anonymousSettingName = -1;
                if (!HelperMethods.HasAnonymousChatPermission(FriendBasicInfoModel))
                {
                    if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                    {
                        return;
                    }
                    else
                    {
                        anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                    }
                }

                HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
                {
                    if (result == false) return 0;

                    int accessType = -1;
                    if (!HelperMethods.HasFriendChatPermission(FriendBasicInfoModel, BlockedNonFriendModel, out accessType))
                    {
                        if (HelperMethods.ShowBlockWarning(FriendBasicInfoModel, accessType))
                        {
                            return 0;
                        }
                    }

                    HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, FriendBasicInfoModel, BlockedNonFriendModel, (status) =>
                    {
                        if (status == false) return 0;

                        List<string> fileShareList = new List<string>();
                        List<string> fileTransferList = new List<string>();

                        foreach (string fileName in fileNames)
                        {
                            FileInfo fi = new FileInfo(fileName);
                            if (ChatHelpers.EXT_IMAGE.Contains(System.IO.Path.GetExtension(fileName).ToLower()) && fi.Length <= DefaultSettings.MAX_CHAT_FILE_TRANSFER_SIZE)
                            {
                                fileShareList.Add(fileName);
                            }
                            else
                            {
                                //CustomMessageBox.ShowWarning("File transfer is currently not supported on Desktop. This feature will be available soon. Please, try only image file.");
                                //return 0;

                                fileTransferList.Add(fileName);
                            }
                        }

                        if (fileTransferList.Count > 0)
                        {
                            new ChatFileTransferProcessor(_FriendTableID, 0, null, fileTransferList).Start();
                        }

                        if (fileShareList.Count > 0)
                        {
                            ShowImagePreview(fileShareList.ToArray(), (int)MessageType.IMAGE_FILE_FROM_GALLERY);
                        }

                        return 1;
                    });

                    return 1;
                }, anonymousSettingName);
            }
            catch (Exception ex)
            {

                log.Error("Error: OnDropFile() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnDropGroupMember(DragEventArgs e)
        {
            try
            {
                UserBasicInfoModel model = e.Data.GetData(typeof(UserBasicInfoModel)) as UserBasicInfoModel;
                if (model != null)
                {
                    List<UserBasicInfoModel> list = new List<UserBasicInfoModel>();
                    list.Add(FriendBasicInfoModel);
                    list.Add(model);

                    ChatHelpers.CreateGroup(list, NotificationMessages.GROUP_NAME_ENTER_MESSAGE);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: OnDropFile() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnAddMember(object param)
        {
            List<GroupMemberInfoModel> tempList = new List<GroupMemberInfoModel>();
            tempList.Add(new GroupMemberInfoModel { UserTableID = _FriendTableID });
            MainSwitcher.PopupController.Instance.Show(btnAddMember, tempList, (e) =>
            {
                if (e != null && e.Count > 0)
                {
                    e.Insert(0, FriendBasicInfoModel);

                    if (e.Count + 1 > DefaultSettings.MAX_GROUP_MEMBER_LIMIT)
                    {
                        UIHelperMethods.ShowFailed(String.Format(NotificationMessages.GROUP_MEMBER_LIMIT_EXITED, DefaultSettings.MAX_GROUP_MEMBER_LIMIT), "Add member");
                        return 0;
                    }

                    ChatHelpers.CreateGroup(e, NotificationMessages.GROUP_NAME_ENTER_MESSAGE);
                }
                return 0;
            });
        }

        private void OnContactShare(List<UserBasicInfoModel> list)
        {
            try
            {
                int anonymousSettingName = -1;
                if (!HelperMethods.HasAnonymousChatPermission(FriendBasicInfoModel))
                {
                    if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                    {
                        return;
                    }
                    else
                    {
                        anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                    }
                }

                HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
                {
                    if (result == false) return 0;

                    int accessType = -1;
                    if (!HelperMethods.HasFriendChatPermission(FriendBasicInfoModel, BlockedNonFriendModel, out accessType))
                    {
                        if (HelperMethods.ShowBlockWarning(FriendBasicInfoModel, accessType))
                        {
                            return 0;
                        }
                    }

                    HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, FriendBasicInfoModel, BlockedNonFriendModel, (status) =>
                    {
                        if (status == false) return 0;

                        if (IsSecretCheckOn)
                        {
                            new ChatContactShareProcessor(this._FriendTableID, 0, null, list, FriendBasicInfoModel.ShortInfoModel.IsSecretVisible, SecretTimerValue).Start();
                        }
                        else
                        {
                            new ChatContactShareProcessor(this._FriendTableID, 0, null, list, 0, 0).Start();

                        }

                        return 1;
                    });

                    return 1;
                }, anonymousSettingName);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnContactShare() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnAccessChangeClicked(object param)
        {
            try
            {
                HelperMethods.ChangeBlockUnblockSettingsWrapper(StatusConstants.FULL_ACCESS, FriendBasicInfoModel, BlockedNonFriendModel, null, true);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnAccessChangeClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Utility Methods

        #region Popup Methods

        private void ShowImagePreview(string[] fileNames, int type)
        {
            try
            {
                log.Debug("==> ShowImagePreview => Count = " + fileNames.Length);
                if (IsSecretChatPanelVisible == false)
                {
                    OpenCloseSecretChatPanel(0);
                }

                HideAudioRecord();
                HideVideoRecord();
                HideStickerPreview();

                if (IsImagePreviewMode && _ImagePreviewPanel != null)
                {
                    _ImagePreviewPanel.AddImages(fileNames);
                    return;
                }

                List<ImageUploaderModel> modelList = new List<ImageUploaderModel>();
                int idx = 0;
                foreach (string fileName in fileNames)
                {
                    ImageUploaderModel model = new ImageUploaderModel();
                    model.ImageType = type;
                    model.FilePath = fileName;
                    modelList.Add(model);
                    idx++;

                    if (idx >= 10)
                        break;
                }

                _ImagePreviewPanel = new UCChatImageSideShow(modelList);
                _ImagePreviewPanel.OnCompleted += (l) =>
                {
                    if (l != null && l.Count > 0)
                    {
                        int anonymousSettingName = -1;
                        if (!HelperMethods.HasAnonymousChatPermission(FriendBasicInfoModel))
                        {
                            if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                            {
                                return;
                            }
                            else
                            {
                                anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                            }
                        }

                        HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
                        {
                            if (result == false) return 0;

                            int accessType = -1;
                            if (!HelperMethods.HasFriendChatPermission(FriendBasicInfoModel, BlockedNonFriendModel, out accessType))
                            {
                                if (HelperMethods.ShowBlockWarning(FriendBasicInfoModel, accessType))
                                {
                                    HideImagePreview();
                                    return 0;
                                }
                            }

                            HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, FriendBasicInfoModel, BlockedNonFriendModel, (status) =>
                            {
                                if (status == false) return 0;

                                if (IsSecretCheckOn && SecretTimerValue > 0)
                                {
                                    l.ForEach(i =>
                                    {
                                        i.TimeOut = SecretTimerValue;
                                        i.IsSecretVisible = FriendBasicInfoModel.ShortInfoModel.IsSecretVisible;
                                    });
                                }
                                new ChatFileShareProcessor(_FriendTableID, 0, null, l).Start();

                                return 1;
                            });

                            return 0;
                        }, anonymousSettingName);
                    }
                    HideImagePreview();
                };
                pnlImagePreviewContainer.Child = _ImagePreviewPanel;
                IsImagePreviewMode = true;
            }
            catch (Exception ex)
            {

                log.Error("Error: ShowImagePreview() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void HideImagePreview()
        {
            try
            {
                log.Debug("==> HideImagePreview ");
                IsImagePreviewMode = false;
                if (_ImagePreviewPanel != null)
                {
                    _ImagePreviewPanel.Dispose();
                    _ImagePreviewPanel = null;
                }
                pnlImagePreviewContainer.Child = null;
            }
            catch (Exception ex)
            {

                log.Error("Error: HideImagePreview() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void ShowStickerPreview(object param)
        {
            try
            {
                if (IsSecretChatPanelVisible == false)
                {
                    OpenCloseSecretChatPanel();
                }

                if (_StickerOpenTimer != null) _StickerOpenTimer.Stop();
                if (IsSeperateWindow == false && SettingsConstants.VALUE_RINGID_STICKER_VIEW)
                {
                    RingIDViewModel.Instance.ExpandCommand.Execute(null);
                    return;
                }

                HideAudioRecord();
                HideVideoRecord();

                _StickerPreviewPanel = new UCChatStickerSlideShow();
                _StickerPreviewPanel.OnSelect += (l) =>
                {
                    if (l != null)
                    {
                        string symbol = l.StickerCollectionID.ToString() + Path.AltDirectorySeparatorChar + l.StickerCategoryID.ToString() + Path.AltDirectorySeparatorChar + l.ImageUrl;
                        BuildAndSendChatPacket(symbol, (int)MessageType.DOWNLOAD_STICKER_MESSAGE, l);
                    }
                    else
                    {
                        HideStickerPreview();
                    }
                };
                pnlStickerPreviewContainer.Child = _StickerPreviewPanel;
                IsStickerPreviewMode = true;
            }
            catch (Exception ex)
            {

                log.Error("Error: ShowStickerPreview() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void HideStickerPreview()
        {
            try
            {
                IsStickerPreviewMode = false;
                if (_StickerPreviewPanel != null)
                {
                    _StickerPreviewPanel.Dispose();
                    _StickerPreviewPanel = null;
                }
                pnlStickerPreviewContainer.Child = null;
                pnlTextAreaContainer.Measure(pnlTextAreaContainer.RenderSize);
            }
            catch (Exception ex)
            {
                log.Error("Error: HideStickerPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowAudioRecord()
        {
            try
            {
                if (ChatHelpers.HideAlreadyRunningRecorder() == false)
                {
                    return;
                }

                _AudioRecorderPanel = new UCChatAudioRecorder(FriendBasicInfoModel);
                _AudioRecorderPanel.RecordingCompletedHandler += (f, t) =>
                {
                    if (!String.IsNullOrWhiteSpace(f))
                    {
                        int anonymousSettingName = -1;
                        if (!HelperMethods.HasAnonymousChatPermission(FriendBasicInfoModel))
                        {
                            if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                            {
                                return;
                            }
                            else
                            {
                                anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                            }
                        }

                        HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
                        {
                            if (result == false) return 0;

                            int accessType = -1;
                            if (!HelperMethods.HasFriendChatPermission(FriendBasicInfoModel, BlockedNonFriendModel, out accessType))
                            {
                                if (HelperMethods.ShowBlockWarning(FriendBasicInfoModel, accessType))
                                {
                                    HideAudioRecord();
                                    return 0;
                                }
                            }

                            HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, FriendBasicInfoModel, BlockedNonFriendModel, (status) =>
                            {
                                if (status == false) return 0;

                                List<FileShareData> fileShareList = new List<FileShareData>();
                                FileShareData fileShareData = new FileShareData();
                                fileShareData.MessageType = (int)MessageType.AUDIO_FILE;
                                if (IsSecretCheckOn)
                                {
                                    fileShareData.TimeOut = t;
                                    fileShareData.IsSecretVisible = FriendBasicInfoModel.ShortInfoModel.IsSecretVisible;
                                }
                                fileShareData.FilePath = f;
                                FileInfo fi = new FileInfo(Path.ChangeExtension(fileShareData.FilePath, AudioCapture.AUDIO_FORMAT));
                                fileShareData.FileSize = fi.Exists ? (long)(fi.Length / 1024) : 0;
                                fileShareData.Duration = t;
                                fileShareList.Add(fileShareData);
                                new ChatFileShareProcessor(_FriendTableID, 0, null, fileShareList).Start();

                                return 1;
                            });

                            return 0;
                        }, anonymousSettingName);
                    }
                    HideAudioRecord();
                };
                pnlAudioRecordContainer.Child = _AudioRecorderPanel;
                IsAudioRecordMode = true;
            }
            catch (Exception ex)
            {

                log.Error("Error: ShowAudioRecord() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void HideAudioRecord()
        {
            try
            {
                IsAudioRecordMode = false;
                if (_AudioRecorderPanel != null)
                {
                    _AudioRecorderPanel.Dispose();
                    _AudioRecorderPanel = null;
                }
                pnlAudioRecordContainer.Child = null;
                if (_RecorderPreviewPanel != null)
                {
                    _RecorderPreviewPanel.CloseWindow();
                    _RecorderPreviewPanel = null;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: HideAudioRecord() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void SwitchAudioRecorderView(bool isVisible)
        {
            try
            {
                if (IsAudioRecordMode)
                {
                    if (isVisible)
                    {
                        _AudioRecorderPanel.IsExpanable = true;
                        _RecorderPreviewPanel.CloseWindow();
                        _RecorderPreviewPanel = null;
                        pnlAudioRecordContainer.Child = _AudioRecorderPanel;
                    }
                    else
                    {
                        _AudioRecorderPanel.IsExpanable = false;
                        pnlAudioRecordContainer.Child = null;
                        _RecorderPreviewPanel = new WNRecorderPreview(_AudioRecorderPanel);
                        _RecorderPreviewPanel.ShowWindow();
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: SwitchAudioRecorderView() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void ShowVideoRecord()
        {
            try
            {
                if (ChatHelpers.HideAlreadyRunningRecorder() == false)
                {
                    return;
                }

                _VideoRecorderPanel = new UCChatVideoRecorder(FriendBasicInfoModel);
                _VideoRecorderPanel.RecordingCompletedHandler += (f, t) =>
                {
                    if (!String.IsNullOrWhiteSpace(f))
                    {
                        int anonymousSettingName = -1;
                        if (!HelperMethods.HasAnonymousChatPermission(FriendBasicInfoModel))
                        {
                            if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                            {
                                return;
                            }
                            else
                            {
                                anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                            }
                        }

                        HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
                        {
                            if (result == false) return 0;

                            int accessType = -1;
                            if (!HelperMethods.HasFriendChatPermission(FriendBasicInfoModel, BlockedNonFriendModel, out accessType))
                            {
                                if (HelperMethods.ShowBlockWarning(FriendBasicInfoModel, accessType))
                                {
                                    HideVideoRecord();
                                    return 0;
                                }
                            }

                            HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, FriendBasicInfoModel, BlockedNonFriendModel, (status) =>
                            {
                                if (status == false) return 0;

                                List<FileShareData> fileShareList = new List<FileShareData>();
                                FileShareData fileShareData = new FileShareData();
                                fileShareData.MessageType = (int)MessageType.VIDEO_FILE;
                                if (IsSecretCheckOn)
                                {
                                    fileShareData.TimeOut = t;
                                    fileShareData.IsSecretVisible = FriendBasicInfoModel.ShortInfoModel.IsSecretVisible;
                                }
                                fileShareData.FilePath = f;
                                FileInfo fi = new FileInfo(Path.ChangeExtension(fileShareData.FilePath, RecorderWebcamPreview.VIDEO_FORMAT));
                                fileShareData.FileSize = fi.Exists ? (long)(fi.Length / 1024) : 0;
                                fileShareData.Duration = t;
                                fileShareList.Add(fileShareData);
                                new ChatFileShareProcessor(_FriendTableID, 0, null, fileShareList).Start();

                                return 1;
                            });

                            return 0;
                        }, anonymousSettingName);
                    }
                    HideVideoRecord();
                };
                pnlVideoRecordContainer.Child = _VideoRecorderPanel;
                IsVideoRecordMode = true;
            }
            catch (Exception ex)
            {

                log.Error("Error: ShowVideoRecord() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void HideVideoRecord()
        {
            try
            {
                IsVideoRecordMode = false;
                if (_VideoRecorderPanel != null)
                {
                    _VideoRecorderPanel.Dispose();
                    _VideoRecorderPanel = null;
                }
                pnlVideoRecordContainer.Child = null;
                if (_RecorderPreviewPanel != null)
                {
                    _RecorderPreviewPanel.CloseWindow();
                    _RecorderPreviewPanel = null;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: HideVideoRecord() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void SwitchVideoRecorderView(bool isVisible)
        {
            try
            {
                if (IsVideoRecordMode)
                {
                    if (isVisible)
                    {
                        _VideoRecorderPanel.IsExpanable = true;
                        _RecorderPreviewPanel.CloseWindow();
                        _RecorderPreviewPanel = null;
                        pnlVideoRecordContainer.Child = _VideoRecorderPanel;
                    }
                    else
                    {
                        _VideoRecorderPanel.IsExpanable = false;
                        pnlVideoRecordContainer.Child = null;
                        _RecorderPreviewPanel = new WNRecorderPreview(_VideoRecorderPanel);
                        _RecorderPreviewPanel.ShowWindow();
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: SwitchVideoRecorderView() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void ShowContactShareView()
        {
            try
            {
                _ContactSharePanel = new UCChatContactSharePreview();
                _ContactSharePanel.Show(this._FriendTableID, 0, null, (l) =>
                {
                    if (l != null && l.Count > 0)
                    {
                        OnContactShare(l);
                    }
                    HideContactShareView();
                    return 0;
                });
                pnlContactShareContainer.Child = _ContactSharePanel;
                IsContactShareMode = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowContactShareView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideContactShareView(bool isClosed = false)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (_ContactSharePanel != null)
                    {
                        _ContactSharePanel.Dispose();
                        _ContactSharePanel = null;
                    }
                    pnlContactShareContainer.Child = null;
                    IsContactShareMode = false;
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: HideContactShareView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowMediaPreview(MessageModel messageModel)
        {
            try
            {
                HideChatInformationView();

                _MediaPreviewPanel = new UCChatMediaPreview();
                _MediaPreviewPanel.Show(messageModel, () =>
                {
                    HideMediaPreview();
                    return 0;
                });
                pnlMediaPreviewContainer.Child = _MediaPreviewPanel;
                IsMediaPreviewMode = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMediaPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideMediaPreview(bool isClosed = false)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (_MediaPreviewPanel != null)
                    {
                        _MediaPreviewPanel.Dispose();
                        _MediaPreviewPanel = null;
                    }
                    pnlMediaPreviewContainer.Child = null;
                    IsMediaPreviewMode = false;
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: HideMediaPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowChatInformationView(RecentModel recentModel)
        {
            try
            {
                _ChatInformationPanel = new UCChatInformationView();
                _ChatInformationPanel.Show(recentModel, FriendBasicInfoModel.ChatBgUrl, FriendBasicInfoModel.EventChatBgUrl, () =>
                 {
                     HideChatInformationView();
                     return 0;
                 });
                pnlChatInformationContainer.Child = _ChatInformationPanel;
                IsChatInformationMode = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowChatInformationView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideChatInformationView(bool isClosed = false)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (_ChatInformationPanel != null)
                    {
                        _ChatInformationPanel.Dispose();
                        _ChatInformationPanel = null;
                    }
                    pnlChatInformationContainer.Child = null;
                    IsChatInformationMode = false;
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: HideChatInformationView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Popup Methods

        #region Property

        public ICommand ChatSendCommand
        {
            get
            {
                if (_ChatSendCommand == null)
                {
                    _ChatSendCommand = new RelayCommand((param) => OnChatSend(param));
                }
                return _ChatSendCommand;
            }
        }

        public ICommand ChatStickerCommand
        {
            get
            {
                if (_ChatStickerCommand == null)
                {
                    _ChatStickerCommand = new RelayCommand((param) => ShowStickerPreview(param));
                }
                return _ChatStickerCommand;
            }
        }

        public ICommand ChatEmoticonCommand
        {
            get
            {
                if (_ChatEmoticonCommand == null)
                {
                    _ChatEmoticonCommand = new RelayCommand((param) => OnChatEmoticon(param));
                }
                return _ChatEmoticonCommand;
            }
        }

        public ICommand SelectAllCommand
        {
            get
            {
                if (_SelectAllCommand == null)
                {
                    _SelectAllCommand = new RelayCommand((param) => OnSelectAll(param));
                }
                return _SelectAllCommand;
            }
        }

        public ICommand ChatDeleteCommand
        {
            get
            {
                if (_ChatDeleteCommand == null)
                {
                    _ChatDeleteCommand = new RelayCommand((param) => OnChatDelete(param));
                }
                return _ChatDeleteCommand;
            }
        }

        public ICommand ChatDeleteCancelCommand
        {
            get
            {
                if (_ChatDeleteCancelCommand == null)
                {
                    _ChatDeleteCancelCommand = new RelayCommand((param) => OnChatDeleteCancel(param));
                }
                return _ChatDeleteCancelCommand;
            }
        }

        public ICommand MoreOptionsCommand
        {
            get
            {
                if (_MoreOptionsCommand == null)
                {
                    _MoreOptionsCommand = new RelayCommand((param) => OnMoreOptions(param));
                }
                return _MoreOptionsCommand;
            }
        }

        public ICommand MultimediaCommand
        {
            get
            {
                if (_MultimediaCommand == null)
                {
                    _MultimediaCommand = new RelayCommand((param) => OnChatMultimedia(param));
                }
                return _MultimediaCommand;
            }
        }

        public ICommand RecentChatCommand
        {
            get
            {
                if (_RecentChatCommand == null)
                {
                    _RecentChatCommand = new RelayCommand((param) => OnRecentChat(param));
                }
                return _RecentChatCommand;
            }
        }

        public ICommand PlusCommand
        {
            get
            {
                if (_PlusCommand == null)
                {
                    _PlusCommand = new RelayCommand((param) => OnPlus(param), (param) => CanPlus(param));
                }
                return _PlusCommand;
            }
        }

        public ICommand PlusPressCommand
        {
            get
            {
                if (_PlusPressCommand == null)
                {
                    _PlusPressCommand = new RelayCommand((param) => OnPlusPress(param));
                }
                return _PlusPressCommand;
            }
        }

        public ICommand PlusReleaseCommand
        {
            get
            {
                if (_PlusReleaseCommand == null)
                {
                    _PlusReleaseCommand = new RelayCommand((param) => OnRelease(param));
                }
                return _PlusReleaseCommand;
            }
        }

        public ICommand MinusCommand
        {
            get
            {
                if (_MinusCommand == null)
                {
                    _MinusCommand = new RelayCommand((param) => OnMinus(param), (param) => CanMinus(param));
                }
                return _MinusCommand;
            }
        }

        public ICommand MinusPressCommand
        {
            get
            {
                if (_MinusPressCommand == null)
                {
                    _MinusPressCommand = new RelayCommand((param) => OnMinusPress(param));
                }
                return _MinusPressCommand;
            }
        }

        public ICommand MinusReleaseCommand
        {
            get
            {
                if (_MinusReleaseCommand == null)
                {
                    _MinusReleaseCommand = new RelayCommand((param) => OnRelease(param));
                }
                return _MinusReleaseCommand;
            }
        }

        public ICommand AddMemberCommand
        {
            get
            {
                if (_AddMemberCommand == null)
                {
                    _AddMemberCommand = new RelayCommand((param) => OnAddMember(param));
                }
                return _AddMemberCommand;
            }
        }

        public ICommand AccessChangeCommand
        {
            get
            {
                if (_AccessChangeCommand == null)
                {
                    _AccessChangeCommand = new RelayCommand(param => OnAccessChangeClicked(param));
                }
                return _AccessChangeCommand;
            }
        }

        public UserBasicInfoModel FriendBasicInfoModel
        {
            get { return _FriendBasicInfoModel; }
            set { _FriendBasicInfoModel = value; }
        }

        public BlockedNonFriendModel BlockedNonFriendModel
        {
            get { return _BlockedNonFriendModel; }
            set { _BlockedNonFriendModel = value; }
        }

        public string TypingMessage
        {
            get { return _TypingMessage; }
            set
            {
                _TypingMessage = value;
                this.OnPropertyChanged("TypingMessage");
            }
        }

        public string RecentSelectedText
        {
            get { return _RecentSelectedText; }
            set
            {
                _RecentSelectedText = value;
                this.OnPropertyChanged("RecentSelectedText");
            }
        }

        public string SecretCountLeft
        {
            get { return _SecretCountLeft; }
            set
            {
                _SecretCountLeft = value;
                this.OnPropertyChanged("SecretCountLeft");
            }
        }

        public string SecretCountCenter
        {
            get { return _SecretCountCenter; }
            set
            {
                _SecretCountCenter = value;
                this.OnPropertyChanged("SecretCountCenter");
            }
        }

        public string SecretCountRight
        {
            get { return _SecretCountRight; }
            set
            {
                _SecretCountRight = value;
                this.OnPropertyChanged("SecretCountRight");
            }
        }

        public bool IsDeletePanelVisible
        {
            get { return _IsDeletePanelVisible; }
            set
            {
                _IsDeletePanelVisible = value;
                this.OnPropertyChanged("IsDeletePanelVisible");
            }
        }

        public bool IsSecretChatPanelVisible
        {
            get { return _IsSecretChatPanelVisible; }
            set
            {
                _IsSecretChatPanelVisible = value;
                this.OnPropertyChanged("IsSecretChatPanelVisible");
            }
        }

        public bool IsSelectAllMode
        {
            get { return _IsSelectAllMode; }
            set
            {
                _IsSelectAllMode = value;
                this.OnPropertyChanged("IsSelectAllMode");
            }
        }

        public bool IsSecretCheckOn
        {
            get { return _IsSecretCheckOn; }
            set
            {
                _IsSecretCheckOn = value;
                this.OnPropertyChanged("IsSecretCheckOn");
            }
        }

        public bool IsImagePreviewMode
        {
            get { return _IsImagePreviewMode; }
            set
            {
                if (_IsImagePreviewMode == value)
                    return;
                _IsImagePreviewMode = value;
                this.OnPropertyChanged("IsImagePreviewMode");
            }
        }

        public bool IsMediaPreviewMode
        {
            get { return _IsMediaPreviewMode; }
            set
            {
                if (_IsMediaPreviewMode == value)
                    return;
                _IsMediaPreviewMode = value;
                this.OnPropertyChanged("IsMediaPreviewMode");
            }
        }

        public bool IsChatInformationMode
        {
            get { return _IsChatInformationMode; }
            set
            {
                if (_IsChatInformationMode == value)
                    return;
                _IsChatInformationMode = value;
                this.OnPropertyChanged("IsChatInformationMode");
            }
        }

        public bool IsStickerPreviewMode
        {
            get { return _IsStickerPreviewMode; }
            set
            {
                if (_IsStickerPreviewMode == value)
                    return;
                _IsStickerPreviewMode = value;
                this.OnPropertyChanged("IsStickerPreviewMode");
            }
        }

        public bool IsAudioRecordMode
        {
            get { return _IsAudioRecordMode; }
            set
            {
                if (_IsAudioRecordMode == value)
                    return;
                _IsAudioRecordMode = value;
                this.OnPropertyChanged("IsAudioRecordMode");
            }
        }

        public bool IsVideoRecordMode
        {
            get { return _IsVideoRecordMode; }
            set
            {
                if (_IsVideoRecordMode == value)
                    return;
                _IsVideoRecordMode = value;
                this.OnPropertyChanged("IsVideoRecordMode");
            }
        }

        public bool IsContactShareMode
        {
            get { return _IsContactShareMode; }
            set
            {
                if (_IsContactShareMode == value)
                    return;
                _IsContactShareMode = value;
                this.OnPropertyChanged("IsContactShareMode");
            }
        }

        public bool IsSeperateWindow
        {
            get { return _IsSeperateWindow; }
            set
            {
                if (_IsSeperateWindow == value)
                    return;

                _IsSeperateWindow = value;
                if (_IsSeperateWindow == false)
                {
                    HideStickerPreview();
                }
                this.OnPropertyChanged("IsSeperateWindow");
            }
        }

        public Visibility IsWaterMarkVisible
        {
            get { return _IsWaterMarkVisible; }
            set
            {
                if (_IsWaterMarkVisible == value)
                    return;
                _IsWaterMarkVisible = value;
                OnPropertyChanged("IsWaterMarkVisible");
            }
        }

        public UCFriendChatScrollViewer ChatScrollViewer
        {
            get
            {
                if (_ChatScrollViewer == null)
                {
                    _ChatScrollViewer = new UCFriendChatScrollViewer(FriendBasicInfoModel);
                }
                return _ChatScrollViewer;
            }
        }

        private DateEventDTO _CurrentDateEvent = new DateEventDTO();
        public DateEventDTO CurrentDateEvent
        {
            get { return _CurrentDateEvent; }
            set
            {
                if (_CurrentDateEvent.Day == value.Day && _CurrentDateEvent.Month == value.Month)
                {
                    return;
                }
                _CurrentDateEvent = value;
                new DateEventProcessor(FriendBasicInfoModel, _CurrentDateEvent).Start();
            }
        }

        #endregion Property

    }
}

﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendProfileVideoAlbum.xaml
    /// </summary>
    public partial class UCFriendProfileVideoAlbum : UserControl, INotifyPropertyChanged
    {
        public bool IsListView;
        public UCFriendProfileVideoAlbum(long userIdentity)
        {
            this.FriendIdentity = userIdentity;
            InitializeComponent();
            this.DataContext = this;
        }

        private Visibility _NoVideoTxtVisibility = Visibility.Collapsed;
        public Visibility NoVideoTxtVisibility
        {
            get { return _NoVideoTxtVisibility; }
            set
            {
                if (value == _NoVideoTxtVisibility) return;
                _NoVideoTxtVisibility = value;
                this.OnPropertyChanged("NoVideoTxtVisibility");
            }
        }

        private ICommand _VideoAlbumClickCommand;
        public ICommand VideoAlbumClickCommand
        {
            get
            {
                if (_VideoAlbumClickCommand == null)
                {
                    _VideoAlbumClickCommand = new RelayCommand(param => OnAlbumClick(param));
                }
                return _VideoAlbumClickCommand;
            }
        }

        private long _FriendIdentity;
        public long FriendIdentity
        {
            get { return _FriendIdentity; }
            set
            {
                _FriendIdentity = value;
                if (FriendIdentity == value) { return; }
                _FriendIdentity = value;
                OnPropertyChanged("FriendIdentity");
            }
        }

        private ObservableCollection<MediaContentModel> _FriendVideoAlbums = new ObservableCollection<MediaContentModel>();
        public ObservableCollection<MediaContentModel> FriendVideoAlbums
        {
            get
            {
                return _FriendVideoAlbums;
            }
            set
            {
                _FriendVideoAlbums = value;
                this.OnPropertyChanged("FriendVideoAlbums");
            }
        }

        private void OnAlbumClick(object param)
        {
            try
            {
                MediaContentModel model = (MediaContentModel)param;
                LisTViewPanelInVideo.Visibility = Visibility.Collapsed;
                ThumbPanelInVideo.Visibility = Visibility.Collapsed;

                if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                VideoAlbumsDetailsPanel.Child = UCMediaContentsView.Instance;
                //UCMediaContentsView.Instance.ParentInstance = this;
                UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum.btnVisibility(true);
                UCMediaContentsView.Instance.ShowAlbumContents(model, () =>
                {
                    ShowAfterVideoAlbumDetailsCancel();
                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum.btnVisibility(false);
                    return 0;
                });
            }
            catch (Exception)
            {
            }
        }
        public void ShowAfterVideoAlbumDetailsCancel()
        {
            if (IsListView)
            {
                LisTViewPanelInVideo.Visibility = Visibility.Visible;
                ThumbPanelInVideo.Visibility = Visibility.Collapsed;
            }
            else
            {
                LisTViewPanelInVideo.Visibility = Visibility.Collapsed;
                ThumbPanelInVideo.Visibility = Visibility.Visible;
            }
            VideoAlbumsDetailsPanel.Child = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void ShowListOrThumbViewInVideo(bool IsListView)
        {
            this.IsListView = IsListView;
            if (VideoAlbumsDetailsPanel.Child == null)
            {
                if (IsListView)
                {
                    LisTViewPanelInVideo.Visibility = Visibility.Visible;
                    ThumbPanelInVideo.Visibility = Visibility.Collapsed;
                }
                else
                {
                    ThumbPanelInVideo.Visibility = Visibility.Visible;
                    LisTViewPanelInVideo.Visibility = Visibility.Collapsed;
                }
            }
            //else
            //{
            //    if (_UCFriendProfile != null)
            //    {
            //        _UCFriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForVideo.LoadCurrentView(ButtonView);
            //    }
            //}
        }
    }
}

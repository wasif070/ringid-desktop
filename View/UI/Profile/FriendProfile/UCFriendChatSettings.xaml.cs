﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Auth.Service.Chat;
using Auth.utility;
using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.FriendList;
using View.Utility.FriendProfile;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendChatSettings.xaml
    /// </summary>
    public partial class UCFriendChatSettings : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendChatSettings).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private UserBasicInfoModel _FriendBasicInfoModel;
        private BlockedNonFriendModel _BlockedNonFriendModel;

        public long _FriendTableID;
        private ICommand _AccessChangeCommand;
        private ICommand _ChatSettingsCommand;
        private ICommand _AddMemberCommand;

        private ObservableCollection<ChatBgHistoryModel> _ChatBgHistoryModelList = new ObservableCollection<ChatBgHistoryModel>();
        private ICommand _OnChatBackgroundChangeCommand;
        private BackgroundWorker _ChatBgHistoryLoader = null;
        public bool _IsChatBgLoaded = false;

        #region Constructor

        public UCFriendChatSettings(UserBasicInfoModel model)
        {
            InitializeComponent();
            this.FriendBasicInfoModel = model;
            this._FriendTableID = model.ShortInfoModel.UserTableID;
            this.BlockedNonFriendModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(this._FriendTableID);
            this._ChatBgHistoryLoader = new BackgroundWorker();
            this._ChatBgHistoryLoader.DoWork += ChatBgHistory_DowWork;
            this._ChatBgHistoryLoader.RunWorkerCompleted += ChatBgHistory_RunWorkerCompleted;
            this.ChatBgHistoryModelList.Add(new ChatBgHistoryModel { ChatBgUrl = "", IsAction = true });
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private void ChatBgHistory_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_IsChatBgLoaded == false)
                {
                    Thread.Sleep(200);
                }

                List<ChatBgHistoryModel> bgModels = RingIDViewModel.Instance.ChatBackgroundList.Values.ToList();
                bgModels = bgModels.Where(P => !ChatBgHistoryModelList.Any(Q => Q.ChatBgUrl.Equals(P.ChatBgUrl))).ToList();

                foreach (ChatBgHistoryModel model in bgModels)
                {
                    Thread.Sleep(5);
                    ChatBgHistoryModelList.InvokeInsert(ChatBgHistoryModelList.Count - 1, model);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ChatBgHistory_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
            e.Result = true;
        }

        private void ChatBgHistory_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _IsChatBgLoaded = true;
        }

        private void mnuMoreBackground_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool bgImageFound = false;
                MessegeBoxWithLoader msgBox = UIHelperMethods.ShowMessageWithLoader("Getting Background Images");
                LoadChatBackground loader = new LoadChatBackground(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER);
                loader.OnFetchSingleBgImage += (imageDTO) =>
                {
                    ChatBgHistoryModel model = RingIDViewModel.Instance.ChatBackgroundList.TryGetValue(imageDTO.name);
                    if (model == null)
                    {
                        model = new ChatBgHistoryModel(imageDTO);
                        RingIDViewModel.Instance.ChatBackgroundList[imageDTO.name] = model;
                        ChatBgHistoryDAO.SaveChatBgHistory(imageDTO);
                    }

                    if (!ChatBgHistoryModelList.Contains(model))
                    {
                        bgImageFound = true;
                        ChatBgHistoryModelList.InvokeInsert(ChatBgHistoryModelList.Count - 1, model);
                    }
                };
                loader.OnComplete += () =>
                {
                    if (_ChatBgHistoryLoader.IsBusy == false)
                    {
                        _ChatBgHistoryLoader.RunWorkerAsync();
                    }
                    if (bgImageFound == false)
                    {
                        msgBox.ShowCompleMessage("No More Background");
                    }
                    Thread.Sleep(1000);
                    msgBox.CloseMessage();
                };
                loader.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuMoreBackground_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mnuChooseExistingPhoto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Title = "Select picture";
                dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
                dialog.Multiselect = false;
                if (dialog.ShowDialog() == true)
                {
                    string fileName = dialog.FileName;
                    Bitmap originalBitmap = ImageUtility.GetBitmapOfDynamicResource(fileName);

                    System.Drawing.Rectangle resolution = System.Windows.Forms.Screen.PrimaryScreen.Bounds;

                    if (originalBitmap.Width < DefaultSettings.MIN_CHAT_BG_WIDTH || originalBitmap.Height < DefaultSettings.MIN_CHAT_BG_HEIGHT)
                    {
                        UIHelperMethods.ShowFailed("Chat background photo's minimum resulotion: " + DefaultSettings.MIN_CHAT_BG_WIDTH + " x " + DefaultSettings.MIN_CHAT_BG_HEIGHT, "Chat background");
                        return;
                    }
                    string savedFileName = ChatHelpers.SaveChatBackgroundFromDirectory(originalBitmap);
                    if (!String.IsNullOrWhiteSpace(savedFileName))
                    {
                        ChatBgImageDTO imageDTO = new ChatBgImageDTO();
                        imageDTO.name = savedFileName;
                        imageDTO.UpdateTime = ChatService.GetServerTime();

                        ChatBgHistoryModel model = new ChatBgHistoryModel(imageDTO);
                        RingIDViewModel.Instance.ChatBackgroundList[imageDTO.name] = model;
                        ChatBgHistoryModelList.InvokeInsert(ChatBgHistoryModelList.Count - 1, model);

                        ChatBgHistoryDAO.SaveChatBgHistoryFromThread(imageDTO);
                        OnChatBackgroundChange(model);
                    }
                    else UIHelperMethods.ShowFailed("Chat background photo's selection failed!", "Chat background");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuChooseExistingPhoto_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void LoadInitInformation()
        {
            if (_IsChatBgLoaded == false && _ChatBgHistoryLoader.IsBusy == false)
            {
                _ChatBgHistoryLoader.RunWorkerAsync();
            }
        }

        public UCAddMemberToGroupPopupWrapper Instance
        {
            get
            {
                return MainSwitcher.PopupController.Instance;
            }
        }

        private void OnAddMember(object param)
        {
            List<GroupMemberInfoModel> tempList = new List<GroupMemberInfoModel>();
            tempList.Add(new GroupMemberInfoModel { UserTableID = _FriendTableID });
            Instance.Show(btnAddMember, tempList, (e) =>
            {
                if (e != null && e.Count > 0)
                {
                    e.Insert(0, FriendBasicInfoModel);

                    if (e.Count + 1 > DefaultSettings.MAX_GROUP_MEMBER_LIMIT)
                    {
                        UIHelperMethods.ShowFailed(String.Format(NotificationMessages.GROUP_MEMBER_LIMIT_EXITED, DefaultSettings.MAX_GROUP_MEMBER_LIMIT), "Group member limit");
                        return 0;
                    }
                    ChatHelpers.CreateGroup(e, NotificationMessages.GROUP_NAME_ENTER_MESSAGE);
                }
                return 0;
            });
        }

        private void OnAccessChangeClicked(object param)
        {
            try
            {
                bool isFriend = FriendBasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED;
                HelperMethods.ChangeBlockUnblockSettingsWrapper(isFriend ? StatusConstants.CHAT_ACCESS : StatusConstants.FULL_ACCESS, FriendBasicInfoModel, BlockedNonFriendModel, null, !isFriend);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnAccessChangeClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnChatSettingsClicked(object param)
        {
            try
            {
                int value = Int32.Parse(param.ToString());
                if (value == 0)
                {
                    //SHOW/HIDE SECRET TIMER 
                    UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(FriendBasicInfoModel.ShortInfoModel.UserTableID);
                    if (dto != null)
                    {
                        dto.IsSecretVisible = FriendBasicInfoModel.ShortInfoModel.IsSecretVisible;
                    }

                    ContactListDAO.UpdateHideSecretTimer(FriendBasicInfoModel.ShortInfoModel.UserTableID, FriendBasicInfoModel.ShortInfoModel.IsSecretVisible);
                }

                else if (value == 1)
                {
                    //SHOW/HIDE IMSOUND 
                    UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(FriendBasicInfoModel.ShortInfoModel.UserTableID);
                    if (dto != null)
                    {
                        dto.ImSoundEnabled = FriendBasicInfoModel.ShortInfoModel.ImSoundEnabled;
                    }

                    ContactListDAO.UpdateIMSound(FriendBasicInfoModel.ShortInfoModel.UserTableID, FriendBasicInfoModel.ShortInfoModel.ImSoundEnabled);
                }

                else if (value == 2)
                {
                    //SHOW/HIDE IMNOTIFICATION 
                    //UserBasicInfoDTO userBasicInfo = null;
                    //if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(FriendBasicInfoModel.ShortInfoModel.UserTableID, out userBasicInfo))
                    //{
                    UserBasicInfoDTO userBasicInfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(FriendBasicInfoModel.ShortInfoModel.UserTableID);
                    if (userBasicInfo != null)
                    {
                        userBasicInfo.ImNotificationEnabled = FriendBasicInfoModel.ShortInfoModel.ImNotificationEnabled;
                    }

                    ContactListDAO.UpdateIMNotification(_FriendTableID, FriendBasicInfoModel.ShortInfoModel.ImNotificationEnabled);
                }
                else if (value == 3)
                {
                    //SHOW/HIDE IMPOPUP
                    UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(FriendBasicInfoModel.ShortInfoModel.UserTableID);
                    if (dto != null)
                    {
                        dto.ImPopupEnabled = FriendBasicInfoModel.ShortInfoModel.ImPopupEnabled;
                    }

                    ContactListDAO.UpdateIMPopUp(_FriendTableID, FriendBasicInfoModel.ShortInfoModel.ImPopupEnabled);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChatSettingsClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnChatBackgroundChange(object param)
        {
            try
            {
                //SELECT BACKGROUND
                ChatBgHistoryModel bgModel = param as ChatBgHistoryModel;
                UserBasicInfoDTO _FriendBasicInfoDto = null;

                if (!String.IsNullOrWhiteSpace(FriendBasicInfoModel.ChatBgUrl))
                {
                    if (bgModel.ChatBgUrl.Equals(FriendBasicInfoModel.ChatBgUrl))
                    {
                        FriendBasicInfoModel.ChatBgUrl = String.Empty;
                        _FriendBasicInfoDto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(FriendBasicInfoModel.ShortInfoModel.UserTableID);
                        if (_FriendBasicInfoDto != null)
                        {
                            _FriendBasicInfoDto.ChatBgUrl = FriendBasicInfoModel.ChatBgUrl;
                        }
                        ContactListDAO.UpdateChatBackground(_FriendTableID, String.Empty);
                        return;
                    }
                }

                FriendBasicInfoModel.ChatBgUrl = bgModel.ChatBgUrl;
                FriendBasicInfoModel.EventChatBgUrl = null;
                ContactListDAO.UpdateChatBackground(_FriendTableID, FriendBasicInfoModel.ChatBgUrl);
                bgModel.UpdateTime = ChatService.GetServerTime();

                _FriendBasicInfoDto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(FriendBasicInfoModel.ShortInfoModel.UserTableID);
                if (_FriendBasicInfoDto != null)
                {
                    _FriendBasicInfoDto.ChatBgUrl = bgModel.ChatBgUrl;
                }
                ChatBgHistoryDAO.UpdateChatBgLastUsedTime(FriendBasicInfoModel.ChatBgUrl, bgModel.UpdateTime);

                if (bgModel.Id > 0 && new ImageFile(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER + System.IO.Path.DirectorySeparatorChar + bgModel.ChatBgUrl).IsComplete == false)
                {
                    MessegeBoxWithLoader msgBox = UIHelperMethods.ShowMessageWithLoader("Downloading Selected Background");
                    ChatBackgroundDownloader downloader = new ChatBackgroundDownloader(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER, bgModel.ChatBgUrl);
                    downloader.OnComplete += (imageUrl, status) =>
                    {
                        msgBox.CloseMessage();
                        if (status && FriendBasicInfoModel.ChatBgUrl.Equals(imageUrl))
                        {
                            FriendBasicInfoModel.OnPropertyChanged("ChatBgUrl");
                        }
                    };
                    downloader.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChatBackgroundChange() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public UserBasicInfoModel FriendBasicInfoModel
        {
            get { return _FriendBasicInfoModel; }
            set { _FriendBasicInfoModel = value; }
        }

        public BlockedNonFriendModel BlockedNonFriendModel
        {
            get { return _BlockedNonFriendModel; }
            set { _BlockedNonFriendModel = value; }
        }

        public ICommand AddMemberCommand
        {
            get
            {
                if (_AddMemberCommand == null)
                {
                    _AddMemberCommand = new RelayCommand((param) => OnAddMember(param));
                }
                return _AddMemberCommand;
            }
        }

        public ICommand AccessChangeCommand
        {
            get
            {
                if (_AccessChangeCommand == null)
                {
                    _AccessChangeCommand = new RelayCommand(param => OnAccessChangeClicked(param));
                }
                return _AccessChangeCommand;
            }
        }

        public ICommand ChatSettingsCommand
        {
            get
            {
                if (_ChatSettingsCommand == null)
                {
                    _ChatSettingsCommand = new RelayCommand(param => OnChatSettingsClicked(param));
                }
                return _ChatSettingsCommand;
            }
        }

        public ICommand OnChatBackgroundChangeCommand
        {
            get
            {
                if (_OnChatBackgroundChangeCommand == null)
                {
                    _OnChatBackgroundChangeCommand = new RelayCommand((param) => OnChatBackgroundChange(param));
                }
                return _OnChatBackgroundChangeCommand;
            }
        }

        public ObservableCollection<ChatBgHistoryModel> ChatBgHistoryModelList
        {
            get { return _ChatBgHistoryModelList; }
            set { _ChatBgHistoryModelList = value; }
        }

        #endregion Property

    }
}

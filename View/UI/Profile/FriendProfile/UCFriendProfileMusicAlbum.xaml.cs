﻿using log4net;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendProfileMusicAlbum.xaml
    /// </summary>
    public partial class UCFriendProfileMusicAlbum : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendProfileMusicAlbum).Name);
        //public string ButtonView = string.Empty;
        public bool IsListView;
        public UCFriendProfileMusicAlbum(long userIdentity)
        {
            _FriendIdentity = userIdentity;
            InitializeComponent();
            this.DataContext = this;
        }

        private Visibility _NoAudioTxtVisibility = Visibility.Collapsed;
        public Visibility NoAudioTxtVisibility
        {
            get { return _NoAudioTxtVisibility; }
            set
            {
                if (value == _NoAudioTxtVisibility) return;
                _NoAudioTxtVisibility = value;
                this.OnPropertyChanged("NoAudioTxtVisibility");
            }
        }

        private ICommand _FriendAudioAlbumCommand;
        public ICommand FriendAudioAlbumCommand
        {
            get
            {
                if (_FriendAudioAlbumCommand == null)
                {
                    _FriendAudioAlbumCommand = new RelayCommand(param => OnAlbumClick(param));
                }
                return _FriendAudioAlbumCommand;
            }
        }
        private long _FriendIdentity;
        private ObservableCollection<MediaContentModel> _FriendAudioAlbums = new ObservableCollection<MediaContentModel>();
        public ObservableCollection<MediaContentModel> FriendAudioAlbums
        {
            get
            {
                return _FriendAudioAlbums;
            }
            set
            {
                _FriendAudioAlbums = value;
                this.OnPropertyChanged("FriendAudioAlbums");
            }
        }

        private void OnAlbumClick(object param)
        {
            try
            {
                MediaContentModel model = (MediaContentModel)param;
                ListViewPanel.Visibility = Visibility.Collapsed;
                ThumbPanel.Visibility = Visibility.Collapsed;

                if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                MusicAlbumsDetailsPanel.Child = UCMediaContentsView.Instance;
                UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum.btnVisibility(true);
                UCMediaContentsView.Instance.ShowAlbumContents(model, () =>
                {
                    ShowAfterMusicAlbumDetailsCancel();
                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum.btnVisibility(false);
                    return 0;
                });
            }
            catch (Exception)
            {
            }
        }

        public void ShowListOrThumbViewInMusic(bool IsListView)
        {
            this.IsListView = IsListView;
            if (MusicAlbumsDetailsPanel.Child == null)
            {
                if (IsListView)
                {
                    ListViewPanel.Visibility = Visibility.Visible;
                    ThumbPanel.Visibility = Visibility.Collapsed;
                }
                else
                {
                    ThumbPanel.Visibility = Visibility.Visible;
                    ListViewPanel.Visibility = Visibility.Collapsed;
                }
            }
            //else
            //{
            //    if (_UCFriendProfile != null)
            //    {
            //       // _UCFriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForAudio.LoadCurrentView(ButtonView);
            //    }

            //}
        }

        public void ShowAfterMusicAlbumDetailsCancel()
        {
            if (IsListView)
            {
                ListViewPanel.Visibility = Visibility.Visible;
                ThumbPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                ListViewPanel.Visibility = Visibility.Collapsed;
                ThumbPanel.Visibility = Visibility.Visible;
            }
            MusicAlbumsDetailsPanel.Child = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

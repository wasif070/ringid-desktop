﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.UI.Feed;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendNewsFeeds.xaml
    /// </summary>
    public partial class UCFriendNewsFeeds : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendNewsFeeds).Name);

        public static UCFriendNewsFeeds Instance;

        private long _FriendUtid;
        public long FriendUtid
        {
            get
            {
                return _FriendUtid;
            }
            set
            {
                _FriendUtid = value;
                this.OnPropertyChanged("FriendUtid");
            }
        }

        private CustomFeedCollection _ViewCollection;
        public CustomFeedCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        public CustomFeedScroll scroll;

        public UCFriendNewsFeeds(long friendIdentity, long friendUtid, CustomFeedScroll scroll)
        {
            Instance = this;
            this.FriendUtid = friendUtid;
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            //
            //this.PreviewMouseWheel += (s, e) => { scroll.OnPreviewMouseWheelScrolled(e.Delta); e.Handled = true; };//this.PreviewMouseWheel += delegate(object sender, MouseWheelEventArgs e) { };
            this.IsVisibleChanged += (s, e) => { scroll.OnIsVisibleChanged((bool)e.NewValue); };

            RingIDViewModel.Instance.ProfileFriendCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_PROFILE_FRIEND, true);
            FeedDataContainer.Instance.ProfileFriendBottomIds.Clear();
            FeedDataContainer.Instance.ProfileFriendTopIds.Clear();
            FeedDataContainer.Instance.ProfileFriendCurrentIds.Clear();
            RingIDViewModel.Instance.ProfileFriendCustomFeeds.UserProfileUtId = friendUtid;

            ViewCollection = RingIDViewModel.Instance.ProfileFriendCustomFeeds;
            scroll.SetScrollValues(feeditemControls, ViewCollection, FeedDataContainer.Instance.ProfileFriendCurrentIds, FeedDataContainer.Instance.ProfileFriendTopIds, FeedDataContainer.Instance.ProfileFriendBottomIds, SettingsConstants.PROFILE_TYPE_GENERAL, AppConstants.TYPE_FRIEND_NEWSFEED);
            DefaultSettings.PROFILEPROFILE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
            {
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.PROFILEPROFILE_STARTPKT);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private int _FriendShipStatus;
        public int FriendShipStatus
        {
            get { return _FriendShipStatus; }
            set
            {
                _FriendShipStatus = value;
                this.OnPropertyChanged("FriendShipStatus");
            }
        }

        public NewStatusView NewStatusView = null;
        public NewStatusViewModel NewStatusViewModel = null;

        private void ucnewStatusInstance_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                NewStatusView = (NewStatusView)sender;
                NewStatusViewModel = (NewStatusViewModel)NewStatusView.DataContext;
                NewStatusViewModel.WriteOnText = "Write something...";
                NewStatusViewModel.WritePostText = "Write Post";
                NewStatusViewModel.OptionsButtonVisibility = Visibility.Collapsed;
                NewStatusViewModel.PrivacyButtonVisibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {

                log.Error("Error: ucnewStatusInstance_Loaded() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        //private void UserControl_Loaded(object sender, RoutedEventArgs args)
        //{
        //    scroll.ScrollToHome();
        //    if (ViewCollection == null)
        //    {
        //        if (timer == null)
        //        {
        //            timer = new DispatcherTimer();
        //            timer.Interval = TimeSpan.FromMilliseconds(500);
        //            timer.Tick += TimerEventProcessor;
        //        }
        //        else timer.Stop();
        //        timer.Start();
        //    }
        //}
        //private void UserControl_Unloaded(object sender, RoutedEventArgs args)
        //{
        //    // if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
        //    //{
        //    scroll.SetScrollEvents(false);
        //    ViewCollection = null;
        //    //}
        //}
        //private DispatcherTimer timer;
        //private void TimerEventProcessor(object sender, EventArgs e)
        //{
        //    if (timer != null && timer.IsEnabled)
        //    {
        //        ViewCollection = RingIDViewModel.Instance.FriendProfileFeeds;
        //        scroll.SetScrollEvents(true);
        //        if (!UCGuiRingID.Instance.IsAnyWindowAbove())
        //            Keyboard.Focus(scroll);
        //        timer.Stop();
        //    }
        //}

        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            DefaultSettings.PROFILEPROFILE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.PROFILEPROFILE_STARTPKT);
        }

        #region "Utility Methods"
        #endregion "Utility Methods"
    }
}

﻿using log4net;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;

namespace View.UI.Profile.FriendProfile.About
{
    /// <summary>
    /// Interaction logic for UCFriendProfessionalCareer.xaml
    /// </summary>
    public partial class UCFriendProfessionalCareer : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendProfessionalCareer).Name);
        
        #region "Private Member"
        private UserBasicInfoDTO user;
        #endregion

        #region "Property"
        private Visibility _VSFriendWr = Visibility.Visible;
        public Visibility VSFriendWr
        {
            get { return _VSFriendWr; }
            set
            {
                if (value == _VSFriendWr)
                    return;

                _VSFriendWr = value;
                this.OnPropertyChangedNotify("VSFriendWr");
            }
        }
        private ObservableCollection<SingleWorkModel> _FriendWorkModelsList;// = new ObservableCollection<SingleWorkModel>();
        public ObservableCollection<SingleWorkModel> FriendWorkModelsList
        {
            get { return _FriendWorkModelsList; }
            set
            {
                if (value == _FriendWorkModelsList)
                    return;

                _FriendWorkModelsList = value;
                this.OnPropertyChangedNotify("FriendWorkModelsList");
            }
        }
        private long _FriendIdentity;
        public long FriendIdentity
        {
            get { return _FriendIdentity; }
            set
            {
                if (value == _FriendIdentity)
                    return;

                _FriendIdentity = value;
                this.OnPropertyChangedNotify("FriendIdentity");
            }
        }
        #endregion

        #region "Constructor"
        public UCFriendProfessionalCareer()
        {
            InitializeComponent();
            this.DataContext = this;
            FriendWorkModelsList = new ObservableCollection<SingleWorkModel>();
        }
        #endregion

        #region "Public Method"
        public void LoadMyFriendsWorkToModels(long utId)
        {
            try
            {
                FriendIdentity = utId;
                Dictionary<Guid, WorkDTO> WorksBySinglePerson = null;
                lock (RingDictionaries.Instance.WORK_DICTIONARY)
                {
                    if (RingDictionaries.Instance.WORK_DICTIONARY.TryGetValue(utId, out WorksBySinglePerson))
                    {
                        //FriendWorkModelsList = new ObservableCollection<SingleWorkModel>();
                        //VSFriendWr = Visibility.Visible;
                        workAbsent.Visibility = Visibility.Collapsed;
                        foreach (WorkDTO Work in WorksBySinglePerson.Values)
                        {
                            if (!FriendWorkModelsList.Any(x=>x.WId == Work.Id))
                            {
                                //Application.Current.Dispatcher.Invoke((Action)delegate
                                //{
                                    SingleWorkModel WorkModel = new SingleWorkModel();
                                    WorkModel.LoadData(Work);
                                    FriendWorkModelsList.Add(WorkModel);
                               // }); 
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: LoadMyFriendsWorkToModels() => " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void Dispose()
        {
            VSFriendWr = Visibility.Collapsed;
            user = null;
            if (FriendWorkModelsList != null) FriendWorkModelsList.Clear();
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChangedNotify(string methodName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(methodName));
            }
        }
        #endregion
    }
}

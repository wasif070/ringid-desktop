﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;

namespace View.UI.Profile.FriendProfile.About
{
    /// <summary>
    /// Interaction logic for UCFriendBasicInfo.xaml
    /// </summary>
    public partial class UCFriendBasicInfo : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendBasicInfo).Name);
        
        #region "Property"
        private SingleBasicModelSet _FriendBasicSet = new SingleBasicModelSet();
        public SingleBasicModelSet FriendBasicSet
        {
            get { return _FriendBasicSet; }
            set
            {
                if (value == _FriendBasicSet)
                    return;

                _FriendBasicSet = value;
                this.OnPropertyChangedNotify("FriendBasicSet");
            }
        }
        private Visibility _VSFriendBd = Visibility.Collapsed, _VSFriendEm = Visibility.Collapsed, _VSFriendMb = Visibility.Collapsed, _VSFriendHm = Visibility.Collapsed, _VSFriendCC = Visibility.Collapsed, _VSFriendMd = Visibility.Collapsed, _VSFriendAbt = Visibility.Collapsed;//, VSFriendPp, VSFriendCp;
        public Visibility VSFriendBd
        {
            get { return _VSFriendBd; }
            set
            {
                if (value == _VSFriendBd)
                    return;

                _VSFriendBd = value;
                this.OnPropertyChangedNotify("VSFriendBd");
            }
        }
        public Visibility VSFriendEm
        {
            get { return _VSFriendEm; }
            set
            {
                if (value == _VSFriendEm)
                    return;

                _VSFriendEm = value;
                this.OnPropertyChangedNotify("VSFriendEm");
            }
        }
        public Visibility VSFriendMb
        {
            get { return _VSFriendMb; }
            set
            {
                if (value == _VSFriendMb)
                    return;

                _VSFriendMb = value;
                this.OnPropertyChangedNotify("VSFriendMb");
            }
        }
        public Visibility VSFriendHm
        {
            get { return _VSFriendHm; }
            set { _VSFriendHm = value; this.OnPropertyChangedNotify("VSFriendHM"); }
        }
        public Visibility VSFriendCC
        {
            get { return _VSFriendCC; }
            set { _VSFriendCC = value; this.OnPropertyChangedNotify("VSFriendCC"); }
        }
        public Visibility VSFriendMd
        {
            get { return _VSFriendMd; }
            set { _VSFriendMd = value; this.OnPropertyChangedNotify("VSFriendMd"); }
        }
        public Visibility VSFriendAbt
        {
            get { return _VSFriendAbt; }
            set { _VSFriendAbt = value; this.OnPropertyChangedNotify("_VSFriendAbt"); }
        }

        private string _FullMobileNumber = string.Empty;
        public string FullMobileNumber
        {
            get { return _FullMobileNumber; }
            set { _FullMobileNumber = value; this.OnPropertyChangedNotify("FullMobileNumber"); }
        }

        //// Using a DependencyProperty as the backing store for FullMobileNumber.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty FullMobileNumberProperty =
        //    DependencyProperty.Register("FullMobileNumber", typeof(string), typeof(UCFriendBasicInfo), new PropertyMetadata(string.Empty));
        #endregion

        #region "Constructor"
        public UCFriendBasicInfo(string fn)
        {
            InitializeComponent();
            this.DataContext = this;
            FriendBasicSet.FullName = fn;
        }

        public UCFriendBasicInfo(UserBasicInfoModel friendBasicInfoModel)
        {
            InitializeComponent();
            this.DataContext = this;

            LoadFriendBasicInfoFromModel(friendBasicInfoModel);
        }
        #endregion
                                 
        #region "Public Method"
        public void LoadFriendBasicInfoFromModel(UserBasicInfoModel friendBasicInfoModel)
        {
            FriendBasicSet.FullName = friendBasicInfoModel.ShortInfoModel.FullName;
            FriendBasicSet.Email = friendBasicInfoModel.Email;
            FriendBasicSet.MobilePhone = friendBasicInfoModel.MobilePhone;
            FriendBasicSet.MobilePhoneDialingCode = friendBasicInfoModel.MobilePhoneDialingCode;
            FriendBasicSet.Gender = friendBasicInfoModel.Gender;
            FriendBasicSet.BirthDate = friendBasicInfoModel.BirthDate;
            FriendBasicSet.MarriageDate = friendBasicInfoModel.MarriageDate;
            FriendBasicSet.AboutMe = friendBasicInfoModel.AboutMe;
            FriendBasicSet.HomeCity = friendBasicInfoModel.HomeCity;
            FriendBasicSet.CurrentCity = friendBasicInfoModel.CurrentCity;
            FullMobileNumber = (!string.IsNullOrEmpty(FriendBasicSet.MobilePhoneDialingCode) && !string.IsNullOrEmpty(FriendBasicSet.MobilePhone)) ? string.Format("{0}-{1}", FriendBasicSet.MobilePhoneDialingCode, FriendBasicSet.MobilePhone) : string.Empty;

            VSFriendBd = ((FriendBasicSet.BirthDate == DateTime.MinValue) || (friendBasicInfoModel.BirthDayPrivacy == AppConstants.PRIVACY_ONLY_ME)) ? Visibility.Collapsed : Visibility.Visible;
            VSFriendMd = (FriendBasicSet.MarriageDate == DateTime.MinValue) ? Visibility.Collapsed : Visibility.Visible;
            //VSFriendEm = (string.IsNullOrWhiteSpace(FriendBasicSet.Email) || (friendBasicInfoModel.EmailPrivacy == AppConstants.PRIVACY_SHORT_ONLY_ME)) ? Visibility.Collapsed : Visibility.Visible;
            //VSFriendMb = (string.IsNullOrWhiteSpace(FriendBasicSet.MobilePhoneDialingCode) && string.IsNullOrWhiteSpace(FriendBasicSet.MobilePhone)) || friendBasicInfoModel.MobilePrivacy == AppConstants.PRIVACY_SHORT_ONLY_ME ? Visibility.Collapsed : Visibility.Visible;
            VSFriendHm = !string.IsNullOrWhiteSpace(FriendBasicSet.HomeCity) ? Visibility.Visible : Visibility.Collapsed;
            VSFriendCC = !string.IsNullOrWhiteSpace(FriendBasicSet.CurrentCity) ? Visibility.Visible : Visibility.Collapsed;
            VSFriendAbt = !string.IsNullOrWhiteSpace(FriendBasicSet.AboutMe) ? Visibility.Visible : Visibility.Collapsed;
        }

        public void HideFriendAboutInfos()
        {
            VSFriendEm = Visibility.Collapsed; //later on,on privacy basis
            VSFriendBd = Visibility.Collapsed;
            VSFriendMb = Visibility.Collapsed;
            VSFriendHm = Visibility.Collapsed;
            VSFriendCC = Visibility.Collapsed;
            VSFriendMd = Visibility.Collapsed;
            VSFriendAbt = Visibility.Collapsed;

            FriendBasicSet.HomeCity = string.Empty;
            FriendBasicSet.CurrentCity = string.Empty;
            FriendBasicSet.MarriageDate = DateTime.MinValue;
            FriendBasicSet.Gender = string.Empty;
            FriendBasicSet.AboutMe = string.Empty;
        }

        public void Dispose()
        {
            FullMobileNumber = null;
            FriendBasicSet = null;
            VSFriendAbt = Visibility.Collapsed;
            VSFriendBd = Visibility.Collapsed;
            VSFriendCC = Visibility.Collapsed;
            VSFriendEm = Visibility.Collapsed;
            VSFriendHm = Visibility.Collapsed;
            VSFriendMb = Visibility.Collapsed;
            VSFriendMd = Visibility.Collapsed;
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChangedNotify(string methodName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(methodName));
            }
        }
        #endregion
    }
}

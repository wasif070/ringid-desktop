﻿using log4net;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;

namespace View.UI.Profile.FriendProfile.About
{
    /// <summary>
    /// Interaction logic for UCFriendEducation.xaml
    /// </summary>
    public partial class UCFriendEducation : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendEducation).Name);
        
        #region "Private Member"
        private UserBasicInfoDTO user;
        #endregion       

        #region "Property"
        private Visibility _VSFriendEd = Visibility.Visible;
        public Visibility VSFriendEd
        {
            get { return _VSFriendEd; }
            set
            {
                if (value == _VSFriendEd)
                    return;

                _VSFriendEd = value;
                this.OnPropertyChangedNotify("VSFriendEd");
            }
        }

        private ObservableCollection<SingleEducationModel> _FriendEducationModelsList;// = new ObservableCollection<SingleEducationModel>();
        public ObservableCollection<SingleEducationModel> FriendEducationModelsList
        {
            get { return _FriendEducationModelsList; }
            set
            {
                if (value == _FriendEducationModelsList)
                    return;

                _FriendEducationModelsList = value;
                this.OnPropertyChangedNotify("FriendEducationModelsList");
            }
        }
        private long _FriendIdentity;
        public long FriendIdentity
        {
            get { return _FriendIdentity; }
            set
            {
                if (value == _FriendIdentity)
                    return;

                _FriendIdentity = value;
                this.OnPropertyChangedNotify("FriendIdentity");
            }
        }
        #endregion

        #region "Constructor"
        public UCFriendEducation()
        {
            InitializeComponent();
            this.DataContext = this;
            FriendEducationModelsList = new ObservableCollection<SingleEducationModel>();
        }
        #endregion

        #region "Public Method"
        public void LoadMyFriendsEduToModels(long utId)
        {
            try
            {
                FriendIdentity = utId;
                user = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(utId);
                //       FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(utId, out user);
                Dictionary<Guid, EducationDTO> EducationsBySinglePerson = null;
                lock (RingDictionaries.Instance.EDUCATION_DICTIONARY)
                {
                    if (RingDictionaries.Instance.EDUCATION_DICTIONARY.TryGetValue(user.UserTableID, out EducationsBySinglePerson))
                    {
                        if (VSFriendEd != Visibility.Visible) VSFriendEd = Visibility.Visible;
                        educationAbsent.Visibility = Visibility.Collapsed;
                        foreach (EducationDTO Education in EducationsBySinglePerson.Values)
                        {
                            if (!FriendEducationModelsList.Any(x => x.EId == Education.Id))
                            {
                                SingleEducationModel EducationModel = new SingleEducationModel();
                                EducationModel.LoadData(Education);
                                FriendEducationModelsList.Add(EducationModel);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: LoadMyFriendsEduToModels() => " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void Dispose()
        {
            user = null;
            VSFriendEd = Visibility.Collapsed;

            if (FriendEducationModelsList != null) FriendEducationModelsList.Clear();
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChangedNotify(string methodName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(methodName));
            }
        }
        #endregion        
    }
}

﻿using log4net;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;

namespace View.UI.Profile.FriendProfile.About
{
    /// <summary>
    /// Interaction logic for UCFriendSkill.xaml
    /// </summary>
    public partial class UCFriendSkill : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendSkill).Name);

        #region "Private Member"
        private UserBasicInfoDTO user;
        #endregion

        #region "Property"
        private ObservableCollection<SingleSkillModel> _FriendSkillModelsList;// = new ObservableCollection<SingleSkillModel>();
        public ObservableCollection<SingleSkillModel> FriendSkillModelsList
        {
            get { return _FriendSkillModelsList; }
            set
            {
                if (value == _FriendSkillModelsList)
                    return;

                _FriendSkillModelsList = value;
                this.OnPropertyChangedNotify("FriendSkillModelsList");
            }
        }

        private Visibility _VSFriendSk = Visibility.Visible;

        public Visibility VSFriendSk
        {
            get { return _VSFriendSk; }
            set
            {
                if (value == _VSFriendSk)
                    return;

                _VSFriendSk = value;
                this.OnPropertyChangedNotify("VSFriendSk");
            }
        }

        private long _FriendIdentity;
        public long FriendIdentity
        {
            get { return _FriendIdentity; }
            set
            {
                if (value == _FriendIdentity)
                    return;

                _FriendIdentity = value;
                this.OnPropertyChangedNotify("FriendIdentity");
            }
        }
        #endregion

        #region "Constructor"
        public UCFriendSkill()
        {
            InitializeComponent();
            this.DataContext = this;
            FriendSkillModelsList = new ObservableCollection<SingleSkillModel>();
        }
        #endregion

        #region "Public Method"
        public void LoadMyFriendsSkillsToModels(long utId)
        {
            try
            {
                FriendIdentity = utId;
                Dictionary<Guid, SkillDTO> SkillsBySinglePerson = null;
                lock (RingDictionaries.Instance.SKILL_DICTIONARY)
                {
                    if (RingDictionaries.Instance.SKILL_DICTIONARY.TryGetValue(utId, out SkillsBySinglePerson))
                    {
                        //FriendSkillModelsList = new ObservableCollection<SingleSkillModel>();
                        //VSFriendSk = Visibility.Visible;
                        skillAbsent.Visibility = Visibility.Collapsed;
                        foreach (SkillDTO skill in SkillsBySinglePerson.Values)
                        {
                            SingleSkillModel skillModel = new SingleSkillModel();
                            skillModel.LoadData(skill);
                            if (!FriendSkillModelsList.Any(x => x.SKId == skillModel.SKId))
                            {
                                //Application.Current.Dispatcher.Invoke((Action)delegate
                                //{
                                    FriendSkillModelsList.Add(skillModel);
                               // });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: LoadMyFriendsSkillsToModels() => " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void Dispose()
        {
            user = null;
            VSFriendSk = Visibility.Collapsed;
            if (FriendSkillModelsList != null) FriendSkillModelsList.Clear();
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChangedNotify(string methodName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(methodName));
            }
        }
        #endregion
    }
}

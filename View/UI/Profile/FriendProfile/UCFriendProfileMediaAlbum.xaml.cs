﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.Constants;
using View.Utility;
using System.Linq;
using System;
using View.Utility.Auth;
using System.ComponentModel;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendProfileMediaAlbum.xaml
    /// </summary>
    public partial class UCFriendProfileMediaAlbum : UserControl, INotifyPropertyChanged
    {
        private string tabItem = string.Empty;
        private string ButtonName = string.Empty;
        private long friendIdentity = 0;
        private long userTableId = 0;
        public UCFriendProfileMusicAlbum _UCFriendProfileMusicAlbum = null;
        public UCFriendProfileVideoAlbum _UCFriendProfileVideoAlbum = null;

        public UCFriendProfileMediaAlbum(long userIdentity, long userTableId)
        {
            this.friendIdentity = userIdentity;
            this.userTableId = userTableId;
            InitializeComponent();
            this.DataContext = this;
        }

        #region "Property"

        private bool _IsListView = false;
        public bool IsListView
        {
            get { return _IsListView; }
            set
            {
                if (value == _IsListView)
                    return;
                _IsListView = value;
                this.OnPropertyChanged("IsListView");
            }
        }
        #endregion
        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "EventTrigger"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            TabList.SelectionChanged += TabControl_SelectionChanged;
            viewBtn.Click += viewBtn_Click;
            TabControl_SelectionChanged(TabList, null);
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            TabList.SelectionChanged -= TabControl_SelectionChanged;
            viewBtn.Click -= viewBtn_Click;
        }
        private void LoadMusicAlbum()
        {
            //if (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.ContainsKey(userTableId))
            //{
            //    if (_UCFriendProfileMusicAlbum != null && NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[userTableId].Count != _UCFriendProfileMusicAlbum.FriendAudioAlbums.Count)
            //    {
            //        _UCFriendProfileMusicAlbum.NoAudioTxt.Visibility = Visibility.Collapsed;
            //        foreach (var item in NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[userTableId].Values)
            //        {
            //            if (!_UCFriendProfileMusicAlbum.FriendAudioAlbums.Any(P => P.AlbumId == item.AlbumId))
            //            {
            //                MediaContentModel model = new MediaContentModel();
            //                model.LoadData(item);
            //                _UCFriendProfileMusicAlbum.FriendAudioAlbums.Add(model);
            //            }
            //        }
            //    }
            //}
            //else if (_UCFriendProfileMusicAlbum != null && _UCFriendProfileMusicAlbum.FriendAudioAlbums.Count == 0) { _UCFriendProfileMusicAlbum.NoAudioTxt.Visibility = Visibility.Collapsed; }
            //else
            //{
            if (_UCFriendProfileMusicAlbum == null)
            {
                _UCFriendProfileMusicAlbum = new UCFriendProfileMusicAlbum(friendIdentity);
                _UCFriendProfileMusicAlbum.NoAudioTxtVisibility = Visibility.Collapsed;
                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    _UCFriendProfileMusicAlbum.MusicGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                    new Thread(() =>
                    {
                        bool success = false;
                        string message = string.Empty;
                        JObject obj = new JObject();
                        obj[JsonKeys.UserTableID] = userTableId;
                        obj[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_AUDIO;
                        SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_MEDIA_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                        SetAudioLoaderTextValue(success);
                    }).Start();
                }
            }
            MusicPanel.Visibility = Visibility.Visible;
            VideosPanel.Visibility = Visibility.Collapsed;
            _UCFriendProfileMusicAlbum.ShowListOrThumbViewInMusic(IsListView);
            MusicPanel.Child = _UCFriendProfileMusicAlbum;
        }

        public void SetAudioLoaderTextValue(bool success)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (_UCFriendProfileMusicAlbum.MusicGIFCtrl != null && _UCFriendProfileMusicAlbum.MusicGIFCtrl.IsRunning()) _UCFriendProfileMusicAlbum.MusicGIFCtrl.StopAnimate();
                if (!success && _UCFriendProfileMusicAlbum.FriendAudioAlbums.Count == 0)
                {
                    _UCFriendProfileMusicAlbum.NoAudioTxtVisibility = Visibility.Visible;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void LoadVideoAlbum()
        {
            //if (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.ContainsKey(userTableId))
            //{
            //    if (_UCFriendProfileVideoAlbum != null && NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[userTableId].Count != _UCFriendProfileVideoAlbum.FriendVideoAlbums.Count)
            //    {
            //        _UCFriendProfileVideoAlbum.NoVideoTxt.Visibility = Visibility.Collapsed;
            //        foreach (var item in NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[userTableId].Values)
            //        {
            //            if (!_UCFriendProfileVideoAlbum.FriendVideoAlbums.Any(P => P.AlbumId == item.AlbumId))
            //            {
            //                MediaContentModel model = new MediaContentModel();
            //                model.LoadData(item);
            //                _UCFriendProfileVideoAlbum.FriendVideoAlbums.Add(model);
            //            }
            //        }
            //    }
            //}
            //else if (_UCFriendProfileVideoAlbum != null && _UCFriendProfileVideoAlbum.FriendVideoAlbums.Count == 0) { _UCFriendProfileVideoAlbum.NoVideoTxt.Visibility = Visibility.Collapsed; }
            //else
            //{
            if (_UCFriendProfileVideoAlbum == null)
            {
                _UCFriendProfileVideoAlbum = new UCFriendProfileVideoAlbum(friendIdentity);
                _UCFriendProfileVideoAlbum.NoVideoTxtVisibility = Visibility.Collapsed;
                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    _UCFriendProfileVideoAlbum.VideoGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                    new Thread(() =>
                    {
                        bool success = false;
                        string message = string.Empty;
                        JObject obj = new JObject();
                        obj[JsonKeys.UserTableID] = userTableId;
                        obj[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_VIDEO;
                        SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_MEDIA_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                        SetVideoLoaderTextValue(success);
                    }).Start();
                }
            }
            MusicPanel.Visibility = Visibility.Collapsed;
            VideosPanel.Visibility = Visibility.Visible;
            if (_UCFriendProfileVideoAlbum != null)
            {
                _UCFriendProfileVideoAlbum.ShowListOrThumbViewInVideo(IsListView);
                VideosPanel.Child = _UCFriendProfileVideoAlbum;
            }
        }

        public void SetVideoLoaderTextValue(bool success)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (_UCFriendProfileVideoAlbum.VideoGIFCtrl != null && _UCFriendProfileVideoAlbum.VideoGIFCtrl.IsRunning()) _UCFriendProfileVideoAlbum.VideoGIFCtrl.StopAnimate();
                if (!success && _UCFriendProfileVideoAlbum.FriendVideoAlbums.Count == 0)
                {
                    _UCFriendProfileVideoAlbum.NoVideoTxtVisibility = Visibility.Visible;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public void btnVisibility(bool isSongView)
        {
            if (isSongView)
            {
                viewBtn.Visibility = Visibility.Collapsed;
            }
            else
            {
                viewBtn.Visibility = Visibility.Visible;
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tabItem = ((sender as TabControl).SelectedItem as TabItem).Name as string;
            switch (tabItem)
            {
                case "MusicTab":
                    LoadMusicAlbum();
                    break;
                case "VideosTab":
                    LoadVideoAlbum();
                    break;
            }
        }

        //private void ListView_Click(object sender, RoutedEventArgs e)
        //{
        //    Button bt = (Button)sender;
        //    ButtonName = bt.Name;
        //    List_View.Visibility = Visibility.Collapsed;
        //    Thumbnail_View.Visibility = Visibility.Visible;

        //    if (tabItem.Equals("MusicTab"))
        //    {
        //        if (_UCFriendProfileMusicAlbum != null)
        //        {
        //            _UCFriendProfileMusicAlbum.ShowListOrThumbViewInMusic(ButtonName);
        //        }
        //        else
        //        {
        //            _UCFriendProfileMusicAlbum = new UCFriendProfileMusicAlbum(friendIdentity);
        //        }
        //    }
        //    else
        //    {
        //        if (_UCFriendProfileVideoAlbum != null)
        //        {
        //            _UCFriendProfileVideoAlbum.ShowListOrThumbViewInVideo(ButtonName);
        //        }
        //        else
        //        {
        //            _UCFriendProfileVideoAlbum = new UCFriendProfileVideoAlbum(friendIdentity);
        //        }
        //    }
        //}

        //private void ThumbnailView_Click(object sender, RoutedEventArgs e)
        //{
        //    Button bt = (Button)sender;
        //    ButtonName = bt.Name;
        //    List_View.Visibility = Visibility.Visible;
        //    Thumbnail_View.Visibility = Visibility.Collapsed;
        //    if (tabItem.Equals("MusicTab"))
        //    {
        //        _UCFriendProfileMusicAlbum.ShowListOrThumbViewInMusic(ButtonName);
        //    }
        //    else
        //    {
        //        if (_UCFriendProfileVideoAlbum != null)
        //        {
        //            _UCFriendProfileVideoAlbum.ShowListOrThumbViewInVideo(ButtonName);
        //        }
        //    }
        //}

        private void viewBtn_Click(object sender, RoutedEventArgs e)
        {
            if (IsListView)
            {
                IsListView = false;
                if (tabItem.Equals("MusicTab"))
                {
                    if (_UCFriendProfileMusicAlbum != null)
                    {
                        _UCFriendProfileMusicAlbum.ShowListOrThumbViewInMusic(IsListView);
                    }
                    else
                    {
                        _UCFriendProfileMusicAlbum = new UCFriendProfileMusicAlbum(friendIdentity);
                    }
                }
                else
                {
                    if (_UCFriendProfileVideoAlbum != null)
                    {
                        _UCFriendProfileVideoAlbum.ShowListOrThumbViewInVideo(IsListView);
                    }
                    else
                    {
                        _UCFriendProfileVideoAlbum = new UCFriendProfileVideoAlbum(friendIdentity);
                    }
                }
                
            }
            else
            {
                IsListView = true;

                if (tabItem.Equals("MusicTab"))
                {
                    _UCFriendProfileMusicAlbum.ShowListOrThumbViewInMusic(IsListView);
                }
                else
                {
                    if (_UCFriendProfileVideoAlbum != null)
                    {
                        _UCFriendProfileVideoAlbum.ShowListOrThumbViewInVideo(IsListView);
                    }
                }
            }
        }

        #endregion
    }
}

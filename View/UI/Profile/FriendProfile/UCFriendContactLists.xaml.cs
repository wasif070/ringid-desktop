﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility;
using View.Utility.Auth;
using View.UI.PopUp;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendContactLists.xaml
    /// </summary>
    public partial class UCFriendContactLists : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendContactLists).Name);

        #region "Private Member"
        private int idx = 0;
        //private UserBasicInfoDTO _FriendTempDTO;
        //private bool INITIAL = true;
        //private BackgroundWorker addFriendButtonClickedWorker, removeContactWorker, acceptFriendWorker;
        private ListBoxItem prevItem = null;
        #endregion

        #region "Public Member"
        public long utid;
        public bool MUTUAL_FRIENDS_LOADED = false;
        #endregion

        #region "Property"
        private ObservableCollection<UserBasicInfoModel> _FriendList;
        public ObservableCollection<UserBasicInfoModel> FriendList
        {
            get
            {
                return _FriendList;
            }
            set
            {
                _FriendList = value;
                this.OnPropertyChanged("FriendList");
            }
        }
        private ObservableCollection<UserBasicInfoModel> _MutualList;
        public ObservableCollection<UserBasicInfoModel> MutualList
        {
            get
            {
                return _MutualList;
            }
            set
            {
                _MutualList = value;
                this.OnPropertyChanged("MutualList");
            }
        }

        private int _TOTAL_FRIEND_COUNT;
        public int TOTAL_FRIEND_COUNT
        {
            get
            {
                return _TOTAL_FRIEND_COUNT;
            }
            set
            {
                _TOTAL_FRIEND_COUNT = value;
                this.OnPropertyChanged("TOTAL_FRIEND_COUNT");
            }
        }
        private int _MUTUAL_FRIEND_COUNT;
        public int MUTUAL_FRIEND_COUNT
        {
            get
            {
                return _MUTUAL_FRIEND_COUNT;
            }
            set
            {
                _MUTUAL_FRIEND_COUNT = value;
                this.OnPropertyChanged("MUTUAL_FRIEND_COUNT");
            }
        }

        private int _BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
        public int BOTTOM_LOADING
        {
            get { return _BOTTOM_LOADING; }
            set
            {
                _BOTTOM_LOADING = value;
                this.OnPropertyChanged("BOTTOM_LOADING");
            }
        }

        private int _BOTTOM_LOADING_MF = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
        public int BOTTOM_LOADING_MF
        {
            get { return _BOTTOM_LOADING_MF; }
            set
            {
                _BOTTOM_LOADING_MF = value;
                this.OnPropertyChanged("BOTTOM_LOADING_MF");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Constructor"
        /*public UCFriendContactLists()
        {
            InitializeComponent();
            this.DataContext = this;
        }*/

        public UCFriendContactLists(ObservableCollection<UserBasicInfoModel> FriendList, ObservableCollection<UserBasicInfoModel> MutualList, long utid)
        {
            this.DataContext = this;
            this.FriendList = FriendList;
            this.MutualList = MutualList;
            this.utid = utid;
            InitializeComponent();
        }
        #endregion

        #region "Event Handler"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            cancel.Click += cancel_Click;
            textBoxSearch.TextChanged += SearchTextBox_TextChanged;
            TabList.SelectionChanged += TabControl_SelectionChanged;
            list.PreviewMouseWheel += list_PreviewMouseWheel;
            list.MouseLeftButtonUp += list_MouseLeftButtonUp;
            //showMorePanel.Click += ShowMore_PanelClick;
            //showMorePanelMF.Click += ShowMore_PanelMFClick;
            mutlist.PreviewMouseWheel += list_PreviewMouseWheel;
            mutlist.MouseLeftButtonUp += list_MouseLeftButtonUp;
            TabControl_SelectionChanged(null, null);
            LoadFriends();
            LoadMutualFriends();
        }

        DispatcherTimer _RemoveFriends = null;
        private void UserControlUnloaded(object sender, RoutedEventArgs args)
        {
            cancel.Click -= cancel_Click;
            textBoxSearch.TextChanged -= SearchTextBox_TextChanged;
            TabList.SelectionChanged -= TabControl_SelectionChanged;
            list.PreviewMouseWheel -= list_PreviewMouseWheel;
            list.MouseLeftButtonUp -= list_MouseLeftButtonUp;
            //showMorePanel.Click -= ShowMore_PanelClick;
            //showMorePanelMF.Click -= ShowMore_PanelMFClick;
            mutlist.PreviewMouseWheel -= list_PreviewMouseWheel;
            mutlist.MouseLeftButtonUp -= list_MouseLeftButtonUp;

            if (_RemoveFriends == null)
            {
                _RemoveFriends = new DispatcherTimer();
                _RemoveFriends.Interval = TimeSpan.FromSeconds(1);
                _RemoveFriends.Tick += (s, e) =>
                {
                    if (IsLoaded == false)
                    {
                        UnloadModel();
                    }
                    _RemoveFriends.Stop();
                };
            }
            _RemoveFriends.Stop();
            _RemoveFriends.Start();
        }

        private void UnloadModel()
        {
            lock (FriendList)
            {
                while (FriendList.Count > 1)
                {
                    UserBasicInfoModel model = FriendList.FirstOrDefault();
                    if (model.ShortInfoModel.UserTableID != 0)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            FriendList.Remove(model);
                            GC.SuppressFinalize(model);
                        }, DispatcherPriority.Send);
                    }
                }
            }

            lock (MutualList)
            {
                while (MutualList.Count > 1)
                {
                    UserBasicInfoModel model = MutualList.FirstOrDefault();
                    if (model.ShortInfoModel.UserTableID != 0)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            MutualList.Remove(model);
                            GC.SuppressFinalize(model);
                        }, DispatcherPriority.Send);
                    }
                }
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                idx = TabList.SelectedIndex;
                if (idx == 0)
                {
                    //if (!INITIAL)
                    {
                        allFrnds.Visibility = Visibility.Visible;
                        mutual.Visibility = Visibility.Collapsed;
                    }
                    //INITIAL = false;
                    Check_Search();
                }
                else if (idx == 1)
                {
                    allFrnds.Visibility = Visibility.Collapsed;
                    mutual.Visibility = Visibility.Visible;
                    //if (!MUTUAL_FRIENDS_LOADED)
                    //{
                    //   SendDataToServer.SendMutualFreindRequest(uid);
                    //    MUTUAL_FRIENDS_LOADED = true;
                    //}
                    Check_Search();
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: TabControl_SelectionChanged() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        //private void Control_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    Control control = (Control)sender;
        //    UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;

        //    if (SelectedUserID.Equals(model.UserIdentity))
        //    {
        //        SelectedUserID = 0;
        //        model.OnPropertyChanged("UserIdentity");
        //    }
        //    else
        //    {
        //        long tempIdentity = SelectedUserID;
        //        SelectedUserID = model.UserIdentity;
        //        model.OnPropertyChanged("UserIdentity");
        //    }
        //}

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Search_friends();
        }

        //private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        //{
        //    try
        //    {
        //        showMorePanel.Visibility = Visibility.Collapsed;
        //        showMoreLoader.Visibility = Visibility.Visible;
        //        //    new SendFriendsInfoRequest(utid, AppConstants.TYPE_FRIEND_CONTACT_LIST, FriendList.Count);
        //        SendDataToServer.SendFreindContactListRequest(utid, FriendList.Count);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: ShowMore_PanelClick() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        private void list_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: list_PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void list_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                ListBox listbx = (ListBox)control;
                //ListBoxItem itm = (ListBoxItem)listbx.SelectedItem;
                if (listbx.SelectedItem != null)
                {
                    object selectedItem = listbx.SelectedItem;
                    ListBoxItem selectedListBoxItem = listbx.ItemContainerGenerator.ContainerFromItem(selectedItem) as ListBoxItem;
                    if (selectedListBoxItem == prevItem)
                    {
                        selectedListBoxItem.IsSelected = (!selectedListBoxItem.IsSelected);
                        prevItem = null;
                    }
                    else
                    {
                        prevItem = selectedListBoxItem;
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: list_MouseLeftButtonUp() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void MutualButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
                if (model.NumberOfMutualFriends > 0)
                {
                    //Button button = (Button)sender;
                    //UIElement uiel = (UIElement)button;

                    if (UCMutualFriendsPopup.Instance != null)
                        mainPanel.Children.Remove(UCMutualFriendsPopup.Instance);
                    UCMutualFriendsPopup.Instance = new UCMutualFriendsPopup();
                    UCMutualFriendsPopup.Instance.SetParent(mainPanel);
                    UCMutualFriendsPopup.Instance.Show();
                    UCMutualFriendsPopup.Instance.ShowPopup(control, model.ShortInfoModel.UserTableID, model.ShortInfoModel.FullName);
                    UCMutualFriendsPopup.Instance.OnRemovedUserControl += () =>
                    {
                        UCMutualFriendsPopup.Instance.MutualList.Clear();
                        UCMutualFriendsPopup.Instance = null;
                    };
                    //   new MutualFriendsRequest(model.UserIdentity);
                    SendDataToServer.SendMutualFreindRequest(model.ShortInfoModel.UserTableID);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: Button_Click() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void FrndActivityButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                UserBasicInfoModel _model = (UserBasicInfoModel)control.DataContext;
                if (_model.ShortInfoModel.FriendShipStatus == 0)
                {
                    RingIDViewModel.Instance.OnAddFriendClicked(_model);
                }
                else
                {
                    _model.VisibilityModel.ShowLoading = Visibility.Hidden;
                    Object listObj = null;
                    if (idx == 0) { listObj = list; list.SelectedItem = _model; }
                    else { listObj = mutlist; mutlist.SelectedItem = _model; }
                    list_MouseLeftButtonUp(listObj, null);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: Button_Click_1() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void showMorePanel_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                LoadMoreFriends();
                e.Handled = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMore_PanelClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //private void ShowMore_PanelMFClick(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        showMorePanelMF.Visibility = Visibility.Collapsed;
        //        showMoreLoaderMF.Visibility = Visibility.Visible;
        //        LoadMutualContactFromDictionary();
        //        //SendDataToServer.SendFreindContactListRequest(utid, FriendList.Count);
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: ShowMore_PanelClick() ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            textBoxSearch.Text = string.Empty;
        }
        #endregion

        #region "Private Method"
        private void Search_friends()
        {
            try
            {
                string searchText = textBoxSearch.Text.ToLower();
                if (idx == 0)
                    foreach (UserBasicInfoModel model in FriendList)
                    {
                        string fullName = model.ShortInfoModel.FullName.ToLower();
                        if (fullName.Contains(searchText) || model.ShortInfoModel.UserIdentity.ToString().Contains(searchText))
                        {
                            model.VisibilityModel.IsVisibleInFriendsFriendList = Visibility.Visible;
                        }
                        else
                        {
                            model.VisibilityModel.IsVisibleInFriendsFriendList = Visibility.Collapsed;
                        }
                    }
                else if (idx == 1)
                    foreach (UserBasicInfoModel model in MutualList)
                    {
                        string fullName = model.ShortInfoModel.FullName.ToLower();
                        if (fullName.Contains(searchText) || model.ShortInfoModel.UserIdentity.ToString().Contains(searchText))
                        {
                            model.VisibilityModel.IsVisibleInFriendsFriendList = Visibility.Visible;
                        }
                        else
                        {
                            model.VisibilityModel.IsVisibleInFriendsFriendList = Visibility.Collapsed;
                        }
                    }
            }
            catch (Exception ex)
            {

                log.Error("Error: Search_friends() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void LoadFromDictionaryOrSendToRequest()
        {
            try
            {
                Dictionary<long, UserBasicInfoDTO> TEMP_DTO_DICTIONARY = new Dictionary<long, UserBasicInfoDTO>();
                FriendInfoDTO fiDTO;
                FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY.TryGetValue(utid, out fiDTO);

                if (fiDTO != null && (FriendList.Count - 1) < fiDTO.FriendList.Count)
                {
                    int i = 0;
                    while (i < DefaultSettings.CONTENT_SHOW_LIMIT && (FriendList.Count - 1 + i) < fiDTO.FriendList.Count)
                    {
                        long id = fiDTO.FriendList[FriendList.Count - 1 + i];
                        i++;
                        //  UserBasicInfoDTO user = null;
                        //  FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(id, out user);
                        UserBasicInfoDTO user = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(id);
                        if (user != null)
                        {
                            TEMP_DTO_DICTIONARY[user.UserTableID] = user;
                        }
                    }
                    MainSwitcher.AuthSignalHandler().contactListHandler.ShowFriendTotalContactList(utid, TEMP_DTO_DICTIONARY.Values.ToList());
                }
                else
                {
                    long maxUtID = FriendList.Max(fr => fr.ShortInfoModel.UserTableID);
                    SendDataToServer.SendFreindContactListRequest(utid, maxUtID, DefaultSettings.CONTENT_SHOW_LIMIT);
                    //SendDataToServer.SendFreindContactListRequest(utid, FriendList.Count);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: LoadFromDictionaryOrSendToRequest() ==> " + ex.Message + "\n" + ex.StackTrace);

            }

        }

        private void LoadMutualContactFromDictionary()
        {
            try
            {
                FriendInfoDTO fiDTO;
                FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.TryGetValue(utid, out fiDTO);
                if (fiDTO != null && (MutualList.Count - 1) < fiDTO.FriendList.Count)
                {
                    int i = 0;
                    while (i < DefaultSettings.CONTENT_SHOW_LIMIT && (MutualList.Count - 1 + i) < fiDTO.FriendList.Count)
                    {
                        long id = fiDTO.FriendList[MutualList.Count - 1 + i];
                        i++;
                        FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Add(id);
                    }
                    MainSwitcher.AuthSignalHandler().contactListHandler.ShowFriendMutualContactList(utid, FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: LoadMutualContactFromDictionary() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        #endregion

        #region "Public Method"
        public void LoadFriends()
        {
            try
            {
                Dictionary<long, UserBasicInfoDTO> TEMP_DTO_DICTIONARY = new Dictionary<long, UserBasicInfoDTO>();
                FriendInfoDTO fiDTO;
                FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY.TryGetValue(utid, out fiDTO);
                if (fiDTO != null)
                {
                    int i = 0;
                    TOTAL_FRIEND_COUNT = fiDTO.TotalFriend;
                    foreach (long id in fiDTO.FriendList)
                    {
                        i++;
                        //UserBasicInfoDTO user = null;
                        //FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(id, out user);
                        UserBasicInfoDTO user = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(id);
                        if (user != null)
                        {
                            TEMP_DTO_DICTIONARY[user.UserTableID] = user;
                        }
                        if (i == DefaultSettings.CONTENT_SHOW_LIMIT) break;
                    }
                    MainSwitcher.AuthSignalHandler().contactListHandler.ShowFriendTotalContactList(utid, TEMP_DTO_DICTIONARY.Values.ToList());
                    //MakeShowMoreVisible(FriendList.Count < frndCount);
                    //Check_Search();
                }
                /*else
                {
                    SendDataToServer.SendFreindContactListRequest(utid, 0);
                }*/
            }
            catch (Exception ex)
            {

                log.Error("Error: LoadFriends() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void LoadMutualFriends()
        {
            try
            {
                FriendInfoDTO fiDTO;
                FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.TryGetValue(utid, out fiDTO);
                if (fiDTO != null)
                {
                    int i = 0;
                    MUTUAL_FRIEND_COUNT = fiDTO.TotalFriend;
                    foreach (long id in fiDTO.FriendList)
                    {
                        i++;
                        FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Add(id);
                        if (i == DefaultSettings.CONTENT_SHOW_LIMIT) break;
                    }
                    MainSwitcher.AuthSignalHandler().contactListHandler.ShowFriendMutualContactList(utid, FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: LoadMutualFriends() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void MakeShowMoreVisible(bool makeVisibile, bool isMutualContact = false)
        {
            try
            {
                if (!isMutualContact)
                {
                    if (topLoader.Visibility != Visibility.Collapsed) topLoader.Visibility = Visibility.Collapsed;
                    if (makeVisibile)
                    {
                        /*showMoreWhole.Visibility = Visibility.Visible;
                        showMorePanel.Visibility = Visibility.Visible;
                        showMoreLoader.Visibility = Visibility.Collapsed;*/
                        BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                    }
                    else
                    {
                        /*showMoreWhole.Visibility = Visibility.Collapsed;
                        showMorePanel.Visibility = Visibility.Collapsed;
                        showMoreLoader.Visibility = Visibility.Collapsed;*/
                        BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                    }
                }
                else
                {
                    if (topLoaderMF.Visibility != Visibility.Collapsed) topLoaderMF.Visibility = Visibility.Collapsed;
                    if (makeVisibile)
                    {
                        /*showMoreWholeMF.Visibility = Visibility.Visible;
                        showMorePanelMF.Visibility = Visibility.Visible;
                        showMoreLoaderMF.Visibility = Visibility.Collapsed;*/
                        BOTTOM_LOADING_MF = StatusConstants.LOADING_TEXT_VISIBLE;
                    }
                    else
                    {
                        /*showMoreWholeMF.Visibility = Visibility.Collapsed;
                        showMorePanelMF.Visibility = Visibility.Collapsed;
                        showMoreLoaderMF.Visibility = Visibility.Collapsed;*/
                        BOTTOM_LOADING_MF = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: MakeShowMoreVisible() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void LoadMoreFriends()
        {
            if (TabList.SelectedIndex == 0 && BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                /*showMorePanel.Visibility = Visibility.Collapsed;
                showMoreLoader.Visibility = Visibility.Visible;*/
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
                LoadFromDictionaryOrSendToRequest();
                //SendDataToServer.SendFreindContactListRequest(utid, FriendList.Count);
            }
            else if (TabList.SelectedIndex == 1 && BOTTOM_LOADING_MF == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                /*showMorePanelMF.Visibility = Visibility.Collapsed;
                showMoreLoaderMF.Visibility = Visibility.Visible;*/
                BOTTOM_LOADING_MF = StatusConstants.LOADING_GIF_VISIBLE;
                LoadMutualContactFromDictionary();
                //SendDataToServer.SendFreindContactListRequest(utid, FriendList.Count);
            }
        }

        public void Check_Search()
        {
            if (textBoxSearch != null && textBoxSearch.Text.Trim().Length > 0)
            {
                Search_friends();
            }
        }

        public void SearchFromAuth(UserBasicInfoModel model)
        {
            string searchText = textBoxSearch.Text.ToLower();

            string fullName = model.ShortInfoModel.FullName.ToLower();
            if (fullName.Contains(searchText) || model.ShortInfoModel.UserIdentity.ToString().Contains(searchText))
            {
                model.VisibilityModel.IsVisibleInFriendsFriendList = Visibility.Visible;
            }
            else
            {
                model.VisibilityModel.IsVisibleInFriendsFriendList = Visibility.Collapsed;
            }
        }
        #endregion
    }
}

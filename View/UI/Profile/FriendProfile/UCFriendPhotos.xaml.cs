﻿using Auth.Service.Images;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;

namespace View.UI.Profile.FriendProfile
{
    /// <summary>
    /// Interaction logic for UCFriendPhotos.xaml
    /// </summary>
    public partial class UCFriendPhotos : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendPhotos).Name);
        
        #region "Private Members"
        private long FriendUserTableID;        
        #endregion

        #region "Public Members"
        /*public UCFriendProfilePhotosAlbum _UCFriendProfilePhotosAlbum;
        public UCFriendCoverPhotosAlbum _UCFriendCoverPhotosAlbum;
        public UCFriendFeedPhotosAlbum _UCFriendFeedPhotosAlbum;*/ //to do
        public Visibility IsVisibleProfilePhotosLoader = Visibility.Visible;
        public Visibility IsVisibleCoverPhotosLoader = Visibility.Visible;
        public Visibility IsVisibleFeedPhotosLoader = Visibility.Visible;
        #endregion

        #region Constructor
        public UCFriendPhotos(long FriendUserTableID)
        {
            this.FriendUserTableID = FriendUserTableID;            
            InitializeComponent();
            this.DataContext = this;
            //LoadData();
            SendDataToServer.ListOfImageAlbumsOfUser(FriendUserTableID, SettingsConstants.MEDIA_TYPE_IMAGE, Guid.Empty);
        }
        #endregion

        #region "Properties"
        private int _friendProfileImageCount;
        public int FriendProfileImageCount
        {
            get
            {
                return _friendProfileImageCount;
            }
            set
            {
                SetField(ref _friendProfileImageCount, value, "FriendProfileImageCount");
            }
        }

        private int _friendCoverImageCount;
        public int FriendCoverImageCount
        {
            get
            {
                return _friendCoverImageCount;
            }
            set
            {
                SetField(ref _friendCoverImageCount, value, "FriendCoverImageCount");
            }
        }

        private int _friendFeedImageCount;
        public int FriendFeedImageCount
        {
            get
            {
                return _friendFeedImageCount;
            }
            set
            {
                SetField(ref _friendFeedImageCount, value, "FriendFeedImageCount");
            }
        }
        private ObservableCollection<AlbumModel> _FriendImageAlubms;
        public ObservableCollection<AlbumModel> FriendImageAlubms
        {
            get
            {
                return _FriendImageAlubms;
            }
            set
            {
                _FriendImageAlubms = value;
                this.OnPropertyChanged("FriendImageAlubms");
            }
        }

        private int _BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
        public int BOTTOM_LOADING
        {
            get { return _BOTTOM_LOADING; }
            set
            {
                _BOTTOM_LOADING = value;
                this.OnPropertyChanged("BOTTOM_LOADING");
            }
        }
        #endregion

        #region "Command"
        private ICommand _ImageAlbumCommand;
        public ICommand ImageAlbumCommand
        {
            get
            {
                if (_ImageAlbumCommand == null)
                {
                    _ImageAlbumCommand = new RelayCommand(param => OnAlbumClick(param));
                }
                return _ImageAlbumCommand;
            }
        }

        private ICommand _ShowMoreCommand;
        public ICommand ShowMoreCommand
        {
            get
            {
                if (_ShowMoreCommand == null)
                {
                    _ShowMoreCommand = new RelayCommand(param => OnClickShowMorePanel());
                }
                return _ShowMoreCommand;
            }
        }

        #endregion

        #region "Event Handler"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            //TabList.SelectionChanged += TabControl_SelectionChanged;
            //TabControl_SelectionChanged(null, null);
            //if (TabList.SelectedIndex != 2)//For preview
            //    LoadFriendImages(this.FriendUserTableID, DefaultSettings.FEED_IMAGE_ALBUM_ID);
        }

        //DispatcherTimer _RemovePhotos = null;
        private void UserControlUnloaded(object sender, RoutedEventArgs args)
        {
            //TabList.SelectionChanged -= TabControl_SelectionChanged;

            /*if (_UCFriendProfilePhotosAlbum != null)
            {
                _UCFriendProfilePhotosAlbum.FriendProfileImageList = null;
            }
            if (_UCFriendCoverPhotosAlbum != null)
            {
                _UCFriendCoverPhotosAlbum.FriendCoverImageList = null;
            }
            if (_UCFriendFeedPhotosAlbum != null)
            {
                _UCFriendFeedPhotosAlbum.FriendFeedImageList = null;
            }*/ //to do
            //if (_RemovePhotos == null)
            //{
            //    _RemovePhotos = new DispatcherTimer();
            //    _RemovePhotos.Interval = TimeSpan.FromSeconds(1);
            //    _RemovePhotos.Tick += (s, e) =>
            //    {
            //        if (IsLoaded == false)
            //        {
            //            UnloadModel();
            //        }
            //        _RemovePhotos.Stop();
            //    };
            //}
            //_RemovePhotos.Stop();
            //_RemovePhotos.Start();
        }        

        //private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    try
        //    {
        //        int idx = TabList.SelectedIndex;
        //        UCFriendProfile ucfp = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
        //        switch (idx)
        //        {
        //            /*case 0:         //profile photos
        //                TabItem tabItemProfilePictures = TabList.SelectedItem as TabItem;
        //                if (_UCFriendProfilePhotosAlbum == null)
        //                {
        //                    _UCFriendProfilePhotosAlbum = new UCFriendProfilePhotosAlbum(this.FriendUserTableID, IsVisibleProfilePhotosLoader);
        //                    ((Border)tabItemProfilePictures.Content).Child = _UCFriendProfilePhotosAlbum;
        //                }
        //                if (_UCFriendProfilePhotosAlbum.FriendProfileImageList == null)
        //                {
        //                    _UCFriendProfilePhotosAlbum.FriendProfileImageList = ucfp.FriendProfileImageList;                            
        //                }
        //                _UCFriendProfilePhotosAlbum.ShowHideShowMoreLoading(FriendProfileImageCount > (ucfp.FriendProfileImageList.Count - 1));
        //                break;
        //            case 1:         //cover photos
        //                TabItem tabItemCoverPhotos = TabList.SelectedItem as TabItem;
        //                if (_UCFriendCoverPhotosAlbum == null)
        //                {
        //                    _UCFriendCoverPhotosAlbum = new UCFriendCoverPhotosAlbum(this.FriendUserTableID, IsVisibleCoverPhotosLoader);
        //                    ((Border)tabItemCoverPhotos.Content).Child = _UCFriendCoverPhotosAlbum;
        //                }
        //                if (_UCFriendCoverPhotosAlbum.FriendCoverImageList == null)
        //                {
        //                    _UCFriendCoverPhotosAlbum.FriendCoverImageList = ucfp.FriendCoverImageList;                            
        //                }
        //                _UCFriendCoverPhotosAlbum.ShowHideShowMoreLoading(FriendCoverImageCount > (ucfp.FriendCoverImageList.Count - 1));
        //                break;
        //            case 2:         //feed photos
        //                TabItem tabItemFeedPhotos = TabList.SelectedItem as TabItem;
        //                if (_UCFriendFeedPhotosAlbum == null)
        //                {
        //                    _UCFriendFeedPhotosAlbum = new UCFriendFeedPhotosAlbum(this.FriendUserTableID, IsVisibleFeedPhotosLoader);
        //                    ((Border)tabItemFeedPhotos.Content).Child = _UCFriendFeedPhotosAlbum;
        //                }
        //                if (_UCFriendFeedPhotosAlbum.FriendFeedImageList == null)
        //                {
        //                    _UCFriendFeedPhotosAlbum.FriendFeedImageList = ucfp.FriendFeedImageList;                            
        //                }
        //                _UCFriendFeedPhotosAlbum.ShowHideShowMoreLoading(FriendFeedImageCount > (ucfp.FriendFeedImageList.Count - 1));
        //                break;*/ //to do
        //            default:
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: TabControl_SelectionChanged() ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}

        //private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        //{
        //    if (BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
        //    {
        //        LoadMorePhotos();
        //    }
        //    e.Handled = true;
        //}
        #endregion

        #region "Private Method"
        private void OnAlbumClick(object param)
        {
            try
            {
                AlbumModel model = (AlbumModel)param;
                ThumbPanel.Visibility = Visibility.Collapsed;
                if (UCImageContentView.Instance == null) UCImageContentView.Instance = new UCImageContentView();
                else if (UCImageContentView.Instance.Parent is Border) ((Border)UCImageContentView.Instance.Parent).Child = null;
                ImageAlbumsDetailsPanel.Child = UCImageContentView.Instance;
                UCImageContentView.Instance.SetValue(model, () =>
                {
                    ThumbPanel.Visibility = Visibility.Visible;
                    ImageAlbumsDetailsPanel.Child = null;
                    return 0;
                });
            }
            catch (Exception)
            {
            }
        }

        private void OnClickShowMorePanel()
        {
            if (BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                LoadMorePhotos();
            }
        }
        #endregion

        #region "Public Method"
        //public void LoadData()
        //{
        //    LoadFriendImages(this.FriendUserTableID, DefaultSettings.PROFILE_IMAGE_ALBUM_ID);
        //    LoadFriendImages(this.FriendUserTableID, DefaultSettings.COVER_IMAGE_ALBUM_ID);
        //    LoadFriendImages(this.FriendUserTableID, DefaultSettings.FEED_IMAGE_ALBUM_ID);
        //}

        //public void LoadFriendImages(long userTableID, string friendImageAlbumId)
        //{
        //    try
        //    {
        //        UCFriendProfile ucfp = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
        //        if ((friendImageAlbumId == DefaultSettings.PROFILE_IMAGE_ALBUM_ID && ucfp.FriendProfileImageList.Count == 1)
        //            || (friendImageAlbumId == DefaultSettings.COVER_IMAGE_ALBUM_ID && ucfp.FriendCoverImageList.Count == 1)
        //            || (friendImageAlbumId == DefaultSettings.FEED_IMAGE_ALBUM_ID && ucfp.FriendFeedImageList.Count == 1))
        //        {
        //            //SendDataToServer.SendAlbumRequest(userTableID, friendImageAlbumId, 0);//to do
        //        }                
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: LoadFriendImages() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }

        //}

        public void ShowHideShowMoreLoading(bool makeVisible)
        {
            try
            {
                if (makeVisible)
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                }
                else
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void LoadMorePhotos()
        {
            try
            {
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
                UCFriendProfile ucfp = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                SendDataToServer.ListOfImageAlbumsOfUser(FriendUserTableID, SettingsConstants.MEDIA_TYPE_IMAGE, ucfp.AlbumNpUUID);
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadMorePhotos() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion
        
        #region Utilitymethods
        protected void SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return;
            field = value;
            this.OnPropertyChanged(propertyName);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

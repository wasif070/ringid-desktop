﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Myprofile;
using View.ViewModel;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for UCMainMenuBar.xaml
    /// </summary>
    public partial class UCMainMenuBar : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private const string infoLink = "http://www.ringid.com/info-mobile.html";
        private const string fqLink = "http://www.ringid.com/faq.html";
        private const string privacyPolicylink = "http://www.ringid.com/privacy.html";
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMainMenuBar).Name);
        ThrdCheckForUpdates thrdCheckForUpdates;
        ThrdChangeMood changeMoodThread;
        Grid motherPanel = null;
        #endregion "Fields"
        public UCMainMenuBar(Grid motherGrid, VMRingIDMainWindow model)
        {
            InitializeComponent();
            DataModel = model;
            this.DataContext = this;
            this.motherPanel = motherGrid;
        }
        #region "Properties"
        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion "Properties"

        #region "Commands and Command Methods"

        private ICommand aliveCommand;
        public ICommand AliveCommand
        {
            get
            {
                if (aliveCommand == null) aliveCommand = new RelayCommand(param => ChangeMoodReqeust(StatusConstants.MOOD_ALIVE));
                return aliveCommand;
            }
        }

        private ICommand doNotDisturb;
        public ICommand DoNotDisturbCommand
        {
            get
            {
                if (doNotDisturb == null) doNotDisturb = new RelayCommand(param => ChangeMoodReqeust(StatusConstants.MOOD_DONT_DISTURB));
                return doNotDisturb;
            }
        }

        private ICommand signOutCommand;
        public ICommand SignOutCommand
        {
            get
            {
                if (signOutCommand == null) signOutCommand = new RelayCommand(param => OnSignOutCommand());
                return signOutCommand;
            }
        }
        public void OnSignOutCommand()
        {
            bool isTrue = UIHelperMethods.ShowOKCancel(NotificationMessages.SIGN_OUT_NOTIFICAITON, "Sign out confirmation!", NotificationMessages.SIGN_OUT_DESCRIPTION);
            if (isTrue)
            {
                ShowSignoutLoader(false);
            }

            //WNConfirmationView cv = new WNConfirmationView("Delete confirmation", NotificationMessages.MSG_UNFRIEND_FAIL + msg, CustomConfirmationDialogButtonOptions.YesNo, NotificationMessages.SIGN_OUT_DESCRIPTION);
            //              var result = cv.ShowCustomDialog();
            //              if (result == ConfirmationDialogResult.Yes)
        }

        private ICommand closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (closeCommand == null) closeCommand = new RelayCommand(param => OnCloseCommand());
                return closeCommand;
            }
        }
        private void OnCloseCommand()
        {
            if (RingIDMainWindow != null) RingIDMainWindow.MinimizeToTray();
        }

        private ICommand helpDeskCommand;
        public ICommand HelpDeskCommand
        {
            get
            {
                if (helpDeskCommand == null) helpDeskCommand = new RelayCommand(param => OnHelpDeskCommand());
                return helpDeskCommand;
            }
        }
        private void OnHelpDeskCommand()
        {
            HelperMethods.GoToSite(fqLink);
        }

        private ICommand infoCommand;
        public ICommand InfoCommand
        {
            get
            {
                if (infoCommand == null) infoCommand = new RelayCommand(param => OnInfoCommand());
                return infoCommand;
            }
        }
        private void OnInfoCommand()
        {
            HelperMethods.GoToSite(infoLink);
        }

        private ICommand privacyPolicyCommand;
        public ICommand PrivacyPolicyCommand
        {
            get
            {
                if (privacyPolicyCommand == null) privacyPolicyCommand = new RelayCommand(param => OnPrivacyPolicyCommand());
                return privacyPolicyCommand;
            }
        }
        private void OnPrivacyPolicyCommand()
        {
            HelperMethods.GoToSite(privacyPolicylink);
        }

        private ICommand contactUsCommand;
        public ICommand CntactUsCommand
        {
            get
            {
                if (contactUsCommand == null) contactUsCommand = new RelayCommand(param => OnContactUsCommand());
                return contactUsCommand;
            }
        }
        private void OnContactUsCommand()
        {
            try
            {
                string protocol = "mailto";
                string to = "support@ringid.com";
                string subject = "Contact ringID team";
                string body = "";
                var content = protocol + ":" + to + "?" + "subject=" + subject + "&body=" + body;
                System.Diagnostics.Process.Start(content);
            }//end of try block
            catch (Exception)
            {
                MessageBox.Show(null, "Please, Install mail client first to send mail!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private ICommand aboutRingID;
        public ICommand AboutRingIDCommand
        {
            get
            {
                if (aboutRingID == null) aboutRingID = new RelayCommand(param => OnAboutRingIDCommand());
                return aboutRingID;
            }
        }
        private void OnAboutRingIDCommand()
        {
            UCAboutRingID about = new UCAboutRingID();
            about.SetParent(motherPanel);
            about.ShowThis();
            about.OnRemovedUserControl += () => { about = null; };
        }

        private ICommand settingsMenuCommand;
        public ICommand SettingsMenuCommand
        {
            get
            {
                if (settingsMenuCommand == null) settingsMenuCommand = new RelayCommand(param => OnSettingsMenuCommand());
                return settingsMenuCommand;
            }
        }
        private void OnSettingsMenuCommand()
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
           {
               RingIDViewModel.Instance.GetWindowRingIDSettings.Show();
               RingIDViewModel.Instance._WNRingIDSettings.Activate();
               RingIDViewModel.Instance._WNRingIDSettings.Topmost = true;
               RingIDViewModel.Instance._WNRingIDSettings.Topmost = false;
               RingIDViewModel.Instance._WNRingIDSettings.Focus();
               if (!DefaultSettings.IS_MY_PROFILE_VALUES_LOADED)
               {
                   new ThradMyProfileDetails().StartThread();
               }
           });
        }

        private ICommand checkForUpdateCommand;
        public ICommand CheckForUpdateCommand
        {
            get
            {
                if (checkForUpdateCommand == null) checkForUpdateCommand = new RelayCommand(param => OnCheckForUpdateCommand());
                return checkForUpdateCommand;
            }
        }
        private void OnCheckForUpdateCommand()
        {
            if (thrdCheckForUpdates == null) thrdCheckForUpdates = new ThrdCheckForUpdates();
            thrdCheckForUpdates.StartChecking(true);
        }
        #endregion

        #region"Utility Methods"

        public void ShowSignoutLoader(bool isExtit, string restartMessage = null)
        {
            UIHelperMethods.ShowSignoutLoader(isExtit, motherPanel, null);
        }

        private WNRingIDMain RingIDMainWindow
        {
            get
            {
                if (motherPanel != null)
                {
                    var obj = motherPanel.Parent;
                    if (obj != null && obj is WNRingIDMain) return (WNRingIDMain)obj;
                }
                return null;
            }
        }

        private void ChangeMoodReqeust(int mood)
        {
            if (DefaultSettings.userProfile != null && DefaultSettings.userProfile.Mood != mood)
            {
                if (changeMoodThread == null) changeMoodThread = new ThrdChangeMood();
                changeMoodThread.StartThread(mood);
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}

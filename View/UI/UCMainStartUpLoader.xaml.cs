﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using log4net;
using Models.Constants;
using Models.Utility;
using View.Constants;
using View.UI.RingIDMainUI;
using View.Utility;
using View.ViewModel;
using View.Utility.Feed;
using System.IO;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for UCMainStartUpLoader.xaml
    /// </summary>
    public partial class UCMainStartUpLoader : UserControl, ISwitchable, INotifyPropertyChanged
    {
        #region "Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMainStartUpLoader).Name);
        Grid motherPanel = null;
        Border motherBorder = null;
        LoadSavedDataFromLocal loadSavedDataFromLocal;
        #endregion

        #region "Ctors"
        public UCMainStartUpLoader(VMRingIDMainWindow model, Grid motherGrid, Border motherBorder1)
        {
            InitializeComponent();
            loadSavedDataFromLocal = new LoadSavedDataFromLocal();
            this.DataModel = model;
            this.motherPanel = motherGrid;
            this.motherBorder = motherBorder1;
            this.DataContext = this;
        }
        #endregion "Ctors"

        #region "Properties"
        UCSignOrSignUP SignOrSignUP { get; set; }
        public UCGuiRingID Instance { get; set; }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion

        #region "Public Mehtods"

        public void StartLoader()
        {
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.SPLASH_SCREEN_LOADER));
            ThreadStart loadSavedData = delegate
            {
                //ProcessForceUpdate();

                DataModel.SynchronizeingText = "Please wait";
                new DeleteOldImages().Run();
                bool loadMainUi = loadSavedDataFromLocal.LoadAutoLoginInfoFromLocal(DataModel.PassedArguments);
                RingIDViewModel.Instance.WinDataModel = DataModel;
                loadSavedDataFromLocal.InitObjects();
                if (!string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_NAME.Trim())) DataModel.LoginType = DefaultSettings.VALUE_LOGIN_USER_TYPE;
                if (loadMainUi)
                {
                    HelperMethodsModel.BindSignInBackgroundIntials();
                    dataModel.IsSignedInBackground = true;
                    DataModel.ShowMenubar = true;
                    Application.Current.Dispatcher.Invoke((Action)delegate { UIHelperMethods.ShowMainUIAfterLogin(); });
                    loadSavedDataFromLocal.FetchOtherSavedInfo();
                }
                else Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    DataModel.ShowMenubar = true;
                    SwitchToSignOrSignUP();
                });
                HelperMethods.GetGUID();
            };
            new Thread(loadSavedData).Start();
        }

        private static void ProcessForceUpdate()
        {
            /*string prevVersion = DefaultSettings.VALUE_APP_INSTALLED_VERSION;
            if (String.Compare(AppConfig.DESKTOP_VALID_REALEASE_VERSION, prevVersion) > 0 && AppConfig.FORCE_UPDATE)
            {
                string dbPath = System.IO.Path.Combine(RingIDSettings.APP_FOLDER, DBConstants.DB_NAME);
                if (File.Exists(dbPath))
                {
                    File.Delete(dbPath);                    
                }
            }*/
            #region Commented Code
            /*string path = System.IO.Path.Combine(RingIDSettings.APP_FOLDER, "ForceUpdate.bat");
            if (!File.Exists(path))
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    if (AppConfig.FORCE_UPDATE)
                        sw.WriteLine("1");
                    else
                        sw.WriteLine("0");
                    sw.Close();
                }
            }
            else
            {
                bool flag = false;
                using (StreamReader sr = new StreamReader(path))
                {
                    while (sr.Peek() >= 0)
                    {
                        string str = sr.ReadLine();
                        int val;
                        int.TryParse(str, out val);
                        if (AppConfig.FORCE_UPDATE && val == 1)
                        {
                            string dbPath = System.IO.Path.Combine(RingIDSettings.APP_FOLDER, DBConstants.DB_NAME);
                            if (File.Exists(dbPath))
                            {
                                File.Delete(dbPath);
                                flag = true;
                            }
                        }
                    }
                }
                if (flag)
                {
                    using (StreamWriter sw = new StreamWriter(path, false))
                    {
                        sw.WriteLine(0 + "");
                        sw.Close();
                    }
                }
            }*/
            #endregion
        }

        public void StopLoader()
        {
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        }

        #endregion

        #region "Utility Methods"

        private WNRingIDMain RingIDMainWindow
        {
            get
            {
                if (motherPanel != null)
                {
                    var obj = motherPanel.Parent;
                    if (obj != null && obj is WNRingIDMain) return (WNRingIDMain)obj;
                }
                return null;
            }
        }

        public void SwitchToSignOrSignUP()
        {
            try
            {
                if (SignOrSignUP == null) SignOrSignUP = new UCSignOrSignUP(motherPanel, DataModel);
                if (RingIDMainWindow != null) RingIDMainWindow.Navigate(SignOrSignUP);
                DataModel.MainResizeMode = System.Windows.ResizeMode.CanMinimize;
            }
            catch (Exception ex) { log.Error("Error: SwitchToLoginScreen()==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message); }
        }

        #endregion "Utility Methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region "ISwitchable"

        bool ISwitchable.UtilizeState(object state)
        {
            return true;
        }
        #endregion "ISwitchable"

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.RingIDMainUI
{
    /// <summary>
    /// Interaction logic for UCNewOrExistingUser.xaml
    /// </summary>
    public partial class UCNewOrExistingUser : UserControl
    {
        public UCNewOrExistingUser()
        {
            InitializeComponent();
        }
    }
}

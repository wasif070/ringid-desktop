﻿using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using View.Utility;
using View.ViewModel;

namespace View.UI.RingIDMainUI
{
    /// <summary>
    /// Interaction logic for UCSignOrSignUP.xaml
    /// </summary>
    public partial class UCSignOrSignUP : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCSignOrSignUP).Name);
        Grid motherPanel = null;
        #endregion

        #region"Ctors"

        public UCSignOrSignUP(Grid motherGrid, VMRingIDMainWindow model)
        {
            InitializeComponent();
            this.DataModel = model;
            this.motherPanel = motherGrid;
            this.DataContext = this;
        }

        #endregion"Ctors"

        #region "Properties"

        private UCWelcomePage WelcomePage { get; set; }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion

        #region "ICommands and Command Methods"

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            if (WelcomePage == null) WelcomePage = new UCWelcomePage(motherPanel, _VariableControlsBorder, DataModel);
            _VariableControlsBorder.Child = WelcomePage;
            getFocusOnUserControl();
        }

        #endregion "ICommands and Command Methods"

        #region"Utility methods"

        private void getFocusOnUserControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            this.FocusVisualStyle = null;
        }

        #endregion"Utility methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

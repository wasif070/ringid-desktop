﻿using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.UI.SignIn;
using View.UI.SignUp;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.RingIDMainUI
{
    /// <summary>
    /// Interaction logic for UCWelcomePage.xaml
    /// </summary>
    public partial class UCWelcomePage : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCSignOrSignUP).Name);
        Grid motherPanel = null;
        Border motherBorder = null;
        #endregion

        #region"Ctors"
        public UCWelcomePage(Grid motherGrid, Border motherBorder, VMRingIDMainWindow model)
        {
            InitializeComponent();
            this.DataModel = model;
            this.motherPanel = motherGrid;
            this.DataContext = this;
            this.motherBorder = motherBorder;
        }
        #endregion"Ctors"

        #region "Properties"

        private UCSignUPParent SignUPParent { get; set; }
        private UCSignInParent SignInParent { get; set; }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            if (DataModel.PassedArguments != null && DataModel.PassedArguments.Length > 1 && DataModel.PassedArguments[1].Equals(StartUpConstatns.ARGUMENT_MULTIPLELOGIN))
                UIHelperMethods.ShowWarning(NotificationMessages.LOGGED_IN_FROM_ANOTHER_DEVICE, NotificationMessages.LOGGED_IN_FROM_ANOTHER_DEVICE_HEADER);
            //CustomMessageBox.ShowWarning(NotificationMessages.LOGGED_IN_FROM_ANOTHER_DEVICE, NotificationMessages.LOGGED_IN_FROM_ANOTHER_DEVICE_HEADER);
            else if (DataModel.PassedArguments != null && DataModel.PassedArguments.Length > 1 && DataModel.PassedArguments[1].Equals(StartUpConstatns.ARGUMENT_SESSIONINVALID))
                UIHelperMethods.ShowWarning(NotificationMessages.SESSION_INVALID_BODY, NotificationMessages.SESSION_INVALID_HEADER);
            //CustomMessageBox.ShowWarning(NotificationMessages.SESSION_INVALID_BODY, NotificationMessages.SESSION_INVALID_HEADER);
            else if (DataModel.PassedArguments != null && DataModel.PassedArguments.Length > 1 && DataModel.PassedArguments[1].Equals(StartUpConstatns.ARGUMENT_SIGNIN_FAILD))
                DataModel.ShowErrorMessage(ReasonCodeConstants.REASON_MSG_PASSWORD_DID_NOT_MATCHED);
            if (DataModel.LoginType > 0)
                OnExistingUserCommand(null);
            else getFocusOnUserControl();
        }

        private ICommand newUserCommand;
        public ICommand NewUserCommand
        {
            get
            {
                if (newUserCommand == null) newUserCommand = new RelayCommand(param => OnNewUserCommand(param));
                return newUserCommand;
            }
        }
        public void OnNewUserCommand(object param)
        {
            if (SignUPParent == null)
            {
                SignUPParent = new UCSignUPParent(DataModel, motherPanel, motherBorder);
                SignUPParent.OnBackButtonClicked += () =>
                {
                    DataModel.LoginType = 0;
                    DataModel.ErrorText = string.Empty;
                    motherBorder.Child = null;
                    motherBorder.Child = this;
                    DataModel.ShowWelcomeBG = true;
                    getFocusOnUserControl();
                };
            }
            motherBorder.Child = null;
            motherBorder.Child = SignUPParent;
            DataModel.ShowWelcomeBG = false;
        }

        private ICommand existingUserCommand;
        public ICommand ExistingUserCommand
        {
            get
            {
                if (existingUserCommand == null) existingUserCommand = new RelayCommand(param => OnExistingUserCommand(param));
                return existingUserCommand;
            }
        }
        public void OnExistingUserCommand(object param)
        {
            if (SignInParent == null)
            {
                SignInParent = new UCSignInParent(DataModel, motherPanel, motherBorder);
                SignInParent.OnBackButtonClicked += () =>
                {
                    DataModel.LoginType = 0;
                    DataModel.ErrorText = string.Empty;
                    motherBorder.Child = null;
                    motherBorder.Child = this;
                    DataModel.ShowWelcomeBG = true;
                    getFocusOnUserControl();
                };
            }
            motherBorder.Child = null;
            motherBorder.Child = SignInParent;
            DataModel.ShowWelcomeBG = false;
        }

        private ICommand enterKeyCommand;
        public ICommand EnterKeyCommand
        {
            get
            {
                if (enterKeyCommand == null) enterKeyCommand = new RelayCommand(param => OnEnterKeyCommand(param));
                return enterKeyCommand;
            }
        }
        private void OnEnterKeyCommand(object param)
        {
            OnExistingUserCommand(null);
        }

        #endregion"ICommands and Command Methods"

        #region"Utility methods"
        private void getFocusOnUserControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            this.FocusVisualStyle = null;
        }
        #endregion"Utility methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

﻿using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.Constants;
using View.UI.Feed;
using View.UI.RingMarket;
using View.Utility;
using View.ViewModel;

namespace View.UI.RingIDMainUI
{
    /// <summary>
    /// Interaction logic for UCRightMainButtons.xaml
    /// </summary>
    public partial class UCRightMainButtons : UserControl, INotifyPropertyChanged
    {
        #region "Private Member"
        Grid motherPanel = null;
        #endregion
        
        #region "Property"
        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion

        #region "Constructor"
        public UCRightMainButtons(Grid motherGrid, VMRingIDMainWindow dataModel)
        {
            InitializeComponent();
            this.DataModel = dataModel;
            this.DataContext = this;
            this.motherPanel = motherGrid;
        }

        public UCRightMainButtons()
        {
            InitializeComponent();
            this.DataContext = this;
            this.dataModel = RingIDViewModel.Instance.WinDataModel;
        }
        #endregion

        #region "Command"
        private ICommand _AllFeedsCommand;
        public ICommand AllFeedsCommand
        {
            get
            {
                if (_AllFeedsCommand == null) _AllFeedsCommand = new RelayCommand(param => RingIDViewModel.Instance.OnAllFeedsClicked(param));
                return _AllFeedsCommand;
            }
        }

        private ICommand _MusicAndVideoCommand;
        public ICommand MusicAndVideoCommand
        {
            get
            {
                if (_MusicAndVideoCommand == null) _MusicAndVideoCommand = new RelayCommand(param => OnMusicAndVideoClicked(param));
                return _MusicAndVideoCommand;
            }
        }

        private ICommand _MediaCloudCommand;
        public ICommand MediaCloudCommand
        {
            get
            {
                if (_MediaCloudCommand == null) _MediaCloudCommand = new RelayCommand(param => OnMediaCloudClicked(param));
                return _MediaCloudCommand;
            }
        }

        private ICommand _MediaFeedCommand;
        public ICommand MediaFeedCommand
        {
            get
            {
                if (_MediaFeedCommand == null) _MediaFeedCommand = new RelayCommand(param => OnMediaFeedClicked(param));
                return _MediaFeedCommand;
            }

        }

        private ICommand _NewsPortalCommand;
        public ICommand NewsPortalCommand
        {
            get
            {
                if (_NewsPortalCommand == null) _NewsPortalCommand = new RelayCommand(param => OnNewsPortalClicked(param));
                return _NewsPortalCommand;
            }
        }

        private ICommand _SavedFeedCommand;
        public ICommand SavedFeedCommand
        {
            get
            {
                if (_SavedFeedCommand == null) _SavedFeedCommand = new RelayCommand(param => OnSavedFeedClicked(param));
                return _SavedFeedCommand;
            }
        }

        private ICommand _FollowingListCommand;
        public ICommand FollowingListCommand
        {
            get
            {
                if (_FollowingListCommand == null) _FollowingListCommand = new RelayCommand(param => OnFollowingListClicked(param));
                return _FollowingListCommand;
            }
        }

        private ICommand _StickerMarketCommand;
        public ICommand StickerMarketCommand
        {
            get
            {
                if (_StickerMarketCommand == null) _StickerMarketCommand = new RelayCommand(param => OnStickerMarketClicked(param));
                return _StickerMarketCommand;
            }
        }

        private ICommand _WalletCommand;
        public ICommand WalletCommand
        {
            get
            {
                _WalletCommand = _WalletCommand ?? new RelayCommand(param => OnWalletClicked());
                return _WalletCommand;
            }
        }

        private ICommand _PagesCommand;
        public ICommand PagesCommand
        {
            get
            {
                if (_PagesCommand == null) _PagesCommand = new RelayCommand(param => OnPagesClicked(param));
                return _PagesCommand;
            }
        }

        private ICommand _CelebritesCommand;
        public ICommand CelebritiesCommand
        {
            get
            {
                if (_CelebritesCommand == null) _CelebritesCommand = new RelayCommand(param => OnCelebritesClicked(param));
                return _CelebritesCommand;
            }
        }

        private ICommand _DialpadCommand;
        public ICommand DialpadCommand
        {
            get
            {
                if (_DialpadCommand == null) _DialpadCommand = new RelayCommand(param => RingIDViewModel.Instance.OnDialPadClicked(param));
                return _DialpadCommand;
            }
        }

        private ICommand _CircleListCommand;
        public ICommand CircleListCommand
        {
            get
            {
                if (_CircleListCommand == null) _CircleListCommand = new RelayCommand(param => RingIDViewModel.Instance.OnCircleListClicked(param));
                return _CircleListCommand;
            }
        }

        private ICommand _StreamAndChannelCommand;
        public ICommand StreamAndChannelCommand
        {
            get
            {
                if (_StreamAndChannelCommand == null) _StreamAndChannelCommand = new RelayCommand(param => OnStreamAndChannelCommand(param));
                return _StreamAndChannelCommand;
            }
        }  
        #endregion
                
        #region "Private Method"
        private void OnMusicAndVideoClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMusicAndVideo);
        }

        private void OnMediaCloudClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMediaCloud);
        }

        private void OnMediaFeedClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMediaFeed);
        }

        private void OnNewsPortalClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeNewsPortal);
        }

        private void OnSavedFeedClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeSavedFeed);
        }

        private void OnFollowingListClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeFollowingList);
        }

        private void OnPagesClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypePages);
        }

        private void OnCelebritesClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCelebrity);
        }

        private void OnStickerMarketClicked(object parameter)
        {
            {
                MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeStickerMarket);
                if (UCMiddlePanelSwitcher.View_MenuStickerWrapper != null && !(UCMiddlePanelSwitcher.View_MenuStickerWrapper.Parent.Child is UCStickerMarketInitPanel))
                {
                    UCMiddlePanelSwitcher.View_MenuStickerWrapper.ShowStickerInitView();
                    UCMiddlePanelSwitcher.View_MenuStickerWrapper._UCStickerMarketInitPanel.TabType = 0;
                    UCMiddlePanelSwitcher.View_MenuStickerWrapper._UCStickerMarketInitPanel.myScrlViewer.ScrollToVerticalOffset(0);
                }
            }
        }

        private void OnStreamAndChannelCommand(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeStreamAndChannel, parameter);
        }

        private void OnWalletClicked()
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeWallet);
        }
        //public void OnWalletCommand()
        //{
        //    if (DataModel.IsSignIn)
        //    {
        //        //Utility.Wallet.WalletViewModel.Instance.IsMobileNumberVerified = DefaultSettings.userProfile.IsMobileNumberVerified == 1 ? true : false;
        //        //Utility.Wallet.WalletViewModel.Instance.IsPinNumberSet = !string.IsNullOrWhiteSpace(SettingsConstants.VALUE_WALLET_PIN_NUMBER);
        //        /**old code*/

        //        //if (View.Utility.Wallet.WalletViewModel.Instance.IsLoggedIn)
        //        //{
        //        //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeWallet);
        //        //}
        //        //else
        //        //{
        //        //    View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowWalletPinNumber();
        //        //}

        //        /* New Hobe*/
        //        /*
        //        if (!Utility.Wallet.WalletViewModel.Instance.IsPinNumberSet)
        //        {
        //            Utility.Wallet.WalletViewModel.Instance.IsDigitFromWallet = true;
        //            Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowWalletWelcomeScreenPopup();
        //        }
        //        else
        //        {
        //            if (View.Utility.Wallet.WalletViewModel.Instance.IsLoggedIn)
        //            {
        //                MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeWallet);
        //            }
        //            else
        //            {
        //                View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowWalletPinNumber();
        //            }
        //        }
        //        */
        //        if (MiddlePanelSwitcher.pageSwitcher != null) MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeWallet);
        //    }
        //}
        #endregion
        
        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion        
    }
}

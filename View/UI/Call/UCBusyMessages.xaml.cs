﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using Models.DAO;
using Models.Entity;
using View.UI.PopUp;
using View.Utility;

namespace View.UI.Call
{
    /// <summary>
    /// Interaction logic for UCBussyMessages.xaml
    /// </summary>
    public partial class UCBusyMessages : PopUpBaseControl, INotifyPropertyChanged
    {
        #region "Fields"
        public event DelegateBoolString OnClickedItem;
        #endregion"Fields"

        #region "Constructors"
        public UCBusyMessages(Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            SetParent(motherGrid);
        }
        #endregion "Constructor"

        #region "Properties"
        private string titleBarText = "Busy messages";
        public string TitleBarText
        {
            get { return titleBarText; }
            set { titleBarText = value; OnPropertyChanged("TitleBarText"); }
        }

        private ObservableCollection<InstantMessageDTO> messageList = new ObservableCollection<InstantMessageDTO>();
        public ObservableCollection<InstantMessageDTO> MessageList
        {
            get { return messageList; }
            set { messageList = value; this.OnPropertyChanged("MessageList"); }
        }

        private bool isLoading = true;
        public bool IsLoading
        {
            get { return isLoading; }
            set
            {
                if (value == isLoading) return;
                isLoading = value; OnPropertyChanged("IsLoading");
            }
        }
        #endregion

        #region ICommands
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            Task tsk = new Task(delegate
                {
                    try
                    {
                        IList<InstantMessageDTO> MyInstantMsgsList = InstantMessagesDAO.Instance.GetInstantMessagesFromDB();
                        foreach (InstantMessageDTO inMsg in MyInstantMsgsList)
                        {
                            System.Console.WriteLine("inMsg==>" + inMsg.InstantMessage);
                            MessageList.InvokeAdd(inMsg);
                        }
                    }
                    finally
                    {
                        IsLoading = false;
                    }
                });
            tsk.Start();
        }

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            Hide();
        }

        private ICommand itemClickedCommand;
        public ICommand ItemClickedCommand
        {
            get
            {
                if (itemClickedCommand == null) itemClickedCommand = new RelayCommand(param => OnItemClickedCommand(param));
                return itemClickedCommand;
            }
        }
        public void OnItemClickedCommand(object param)
        {
            string msg = param.ToString();
            if (OnClickedItem != null) OnClickedItem(true, msg);
        }

        #endregion

        #region "Utilty Methods"
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion //INotifyPropertyChanged Members


    }
}

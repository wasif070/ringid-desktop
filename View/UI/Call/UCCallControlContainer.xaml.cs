﻿using System.Windows;
using System.Windows.Controls;
using View.Utility;
using View.Utility.audio;
using View.Utility.Call;

namespace View.UI.Call
{
    /// <summary>
    /// Interaction logic for UCCallControlContainer.xaml
    /// </summary>
    public partial class UCCallControlContainer : UserControl
    {
        #region "Fields"
        PanelTransfromFromLT panelTransfromFromLT;
        private CallController callController;
        #endregion "Fields"

        #region "Constructors"
        public UCCallControlContainer()
        {
            InitializeComponent();
            this.Loaded += Control_Loaded;
        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Events"
        void MainContainer_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            _TopPanel.Visibility = Visibility.Visible;
            _BottomButtonPanel.Visibility = Visibility.Visible;
        }

        void MainContainer_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (callController.ViewModelCallInfo != null
                && callController.ViewModelCallInfo.UiType == ViewConstants.CONNECTED
                && callController.ViewModelCallInfo.NumberOfVideo > 0
                && callController.ViewModelCallInfo.ViewType != ViewConstants.SMALL_SCREEN)
            {
                if (!callController.ViewModelCallInfo.IsBusyMsgPopup && !callController.ViewModelCallInfo.IsOpenVolumPopup)
                {
                    if (!_TopPanel.IsMouseOver) { _TopPanel.Visibility = Visibility.Hidden; }
                    if (!_BottomButtonPanel.IsMouseOver)
                    {
                        _BottomButtonPanel.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        void Control_Loaded(object sender, RoutedEventArgs e)
        {
            panelTransfromFromLT = new PanelTransfromFromLT(_ComponentPanel);
            setClickAtion();
        }

        void VolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int value = (int)e.NewValue;
            if (callController.ViewModelCallInfo != null)
            {
                callController.ViewModelCallInfo.VolumnPercentag = value + "%";
                MasterVolumControl.SetVolume(value);
            }
        }

        void middleImagePanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (panelTransfromFromLT != null)
                panelTransfromFromLT.Translate();
        }
        #endregion

        #region "Utility Methods"
        private void setClickAtion()
        {
            _VolumeSlider.ValueChanged += VolumeSlider_ValueChanged;
            _ComponentPanel.SizeChanged += middleImagePanel_SizeChanged;
            _MiniVideoContainer.MouseDown += panelTransfromFromLT.Grid_MouseDown;
            _MiniVideoContainer.MouseMove += panelTransfromFromLT.Grid_MouseMove;
            _MiniVideoContainer.MouseUp += panelTransfromFromLT.Grid_MouseUp;
            _MainContainer.MouseEnter += MainContainer_MouseEnter;
            _MainContainer.MouseLeave += MainContainer_MouseLeave;
        }

        private void resetClickAtion()
        {
            _VolumeSlider.ValueChanged -= VolumeSlider_ValueChanged;
            _ComponentPanel.SizeChanged -= middleImagePanel_SizeChanged;
            _MiniVideoContainer.MouseDown -= panelTransfromFromLT.Grid_MouseDown;
            _MiniVideoContainer.MouseMove -= panelTransfromFromLT.Grid_MouseMove;
            _MiniVideoContainer.MouseUp -= panelTransfromFromLT.Grid_MouseUp;
            _MainContainer.MouseEnter -= MainContainer_MouseEnter;
            _MainContainer.MouseLeave -= MainContainer_MouseLeave;
        }
        #endregion

        #region "Public Methods"
        public void SetCallController(CallController callController)
        {
            this.callController = callController;
            this.DataContext = callController.ViewModelCallInfo;
        }
        #endregion "Public methods"
    }
}

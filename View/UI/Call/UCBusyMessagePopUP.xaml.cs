﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Models.DAO;
using Models.Entity;
using View.Utility;
using View.Utility.Call;
namespace View.UI.Call
{
    /// <summary>
    /// Interaction logic for BusyMessagePopUP.xaml
    /// </summary>
    public partial class UCBusyMessagePopUP : UserControl
    {
        #region "Private Fields"
        private static IList<InstantMessageDTO> MyInstantMsgsList = new List<InstantMessageDTO>();
        public event DelegateBoolString OnClickedItem;
        #endregion "Private Fields"

        #region "Constructors"
        public UCBusyMessagePopUP()
        {
            InitializeComponent();
        }
        #endregion "Constructors"

        #region "Event Trigger"

        private void Control_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var label = (Label)sender;
            if (OnClickedItem != null) OnClickedItem(true, (string)label.Content);
        }

        private void itmLoadded(object sender, RoutedEventArgs e)
        {
            try
            {
                itemsControl.ItemsSource = new NameList();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("ex.exception==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion "Event Trigger"

        #region "Public Methods"

        public class NameList : ObservableCollection<InstantMessageDTO>
        {
            public NameList()
                : base()
            {
                MyInstantMsgsList = InstantMessagesDAO.Instance.GetInstantMessagesFromDB();
                foreach (InstantMessageDTO inMsg in MyInstantMsgsList)
                {
                    Add(inMsg);
                }
            }
        }

        #endregion "Public methods"

    }
}

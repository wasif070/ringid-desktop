﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;
using View.Utility.Chat;

namespace View.UI.Call
{
    /// <summary>
    /// Interaction logic for UCCallLogTemplate.xaml
    /// </summary>
    public partial class UCCallLogTemplate : UserControl, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCCallLogTemplate).Name);
        private bool disposed = false;

        public UCCallLogTemplate()
        {
            this.MinHeight = ChatHelpers.CALL_VIEW_HEIGHT;
            InitializeComponent();
        }

        ~UCCallLogTemplate()
        {
            Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCCallLogTemplate), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCCallLogTemplate panel = ((UCCallLogTemplate)o);
                RecentModel model = ((RecentModel)panel.DataContext);
                if (e.NewValue != null && (bool)e.NewValue)
                {
                    model.PrevViewHeight = model.MinViewHeight;
                    panel.pnlMessage.SizeChanged += panel.OnSizeChanged;
                    panel.BindPreview();
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                RecentModel model = ((RecentModel)this.DataContext);
                if ((int)this.ActualHeight > model.PrevViewHeight)
                {
                    model.PrevViewHeight = (int)this.ActualHeight;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSizeChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindPreview()
        {
        }

        private void ClearPreview()
        {
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    this.pnlMessage.SizeChanged -= OnSizeChanged;
                    if (disposing)
                    {
                        this.ClearPreview();
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Utility Method
    }
}

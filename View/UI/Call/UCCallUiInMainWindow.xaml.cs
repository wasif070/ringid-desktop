﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;
using View.Utility.RingPlayer;

namespace View.UI.Call
{
    /// <summary>
    /// Interaction logic for UCCallUiInMainWindow.xaml
    /// </summary>
    public partial class UCCallUiInMainWindow : UserControl
    {
        private bool isCallEnd = false;
        public UCCallUiInMainWindow()
        {
            InitializeComponent();
            this.Loaded += UCCallUiInMainWindow_Loaded;
            this.Unloaded += UCCallUiInMainWindow_Unloaded;
        }

        #region "Properties"
        public UCCallControlContainer CallControlContainer { get; set; }
        #endregion

        #region "Event Triggers"
        void UCCallUiInMainWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            _MainControlConainer.Children.Clear();
            CallControlContainer = null;
            if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallUiInMainWindow != null)
            {
                MainSwitcher.CallController.CallUiInMainWindow = null;
            }
            if (MainSwitcher.CallController.ViewModelCallInfo != null
                && !MainSwitcher.CallController.IsCallEnded
                && MainSwitcher.CallController.ViewModelCallInfo.ViewType != ViewConstants.FULL_SCREEN)
            {
                MainSwitcher.CallController.ShowSmallScreen();
            }
        }

        void UCCallUiInMainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            initCallMianUi();
            _MainControlConainer.Children.Add(CallControlContainer);
        }
        #endregion "Event Triggers"

        #region "Public Methods"
        private void initCallMianUi()
        {
            CallControlContainer = new UCCallControlContainer();
            CallControlContainer.SetCallController(MainSwitcher.CallController);
        }
        #endregion "Public methods"

    }
}

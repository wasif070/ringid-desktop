﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using View.BindingModels;
using View.Utility;
using View.Utility.Call.Settings;
using View.Utility.Recent;
using View.ViewModel;
using View.Constants;
using System.ComponentModel;
using View.Utility.WPFMessageBox;
using View.Utility.Chat;
using System.Collections.Concurrent;

namespace View.UI.Call
{
    /// <summary>
    /// Interaction logic for UCCallLog.xaml
    /// </summary>
    public partial class UCCallLog : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCCallLog).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        public static UCCallLog Instance;
        private Thread _Thread = null;
        private Thread _ScrollThread = null;
        private bool _IS_LOADING = false;
        public int _DB_OFFSET = 0;
        private int _LIMIT = 15;
        private bool _IsDeletePanelVisible = false;
        private bool _IsSelectAllMode = true;
        private string _SelectedCallLogID = String.Empty;
        private long _SelectedCallingTime = 0;
        private bool _IsCallLogEnabled;

        private ICommand _OnSelectContactIDCommand;
        private ICommand _ShowDeletePanelCommand;
        private ICommand _SelectAllCommand;
        private ICommand _CallDeleteCommand;
        private ICommand _CallDeleteCancelCommand;
        private ICommand _MakeCallCommand;

        private DispatcherTimer _ContactListResizeTimer = null;

        public UCCallLog()
        {
            InitializeComponent();
            Instance = this;
            this.DataContext = this;
        }

        #region Event Handler

        private void itemsControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                this.ChangeFriendOpenStatus();
            }
            catch (Exception ex)
            {
                log.Error("Error: itemsControl_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CheckBox_CheckedUncheck(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox chkBox = (CheckBox)sender;
                if (chkBox.IsChecked != null)
                {
                    if ((bool)chkBox.IsChecked)
                    {
                        IsSelectAllMode = false;
                        for (int i = 0; i < itemsControl.Items.Count; i++)
                        {
                            ContentPresenter child = (ContentPresenter)itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                            RecentModel model = (RecentModel)child.Content;
                            child.ApplyTemplate();

                            Control presenter = (Control)child.ContentTemplate.FindName("container", child);
                            presenter.ApplyTemplate();

                            CustomBorder border = (CustomBorder)presenter.Template.FindName("customBorder", presenter);
                            if (border.Visibility == Visibility.Visible && model.IsChecked == false)
                            {
                                IsSelectAllMode = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        IsSelectAllMode = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CheckBox_CheckedUncheck() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mnuDeleteConversation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RecentModel model = (RecentModel)((Control)sender).DataContext;
                List<RecentModel> modelList = new List<RecentModel>();
                modelList.Add(model);
                DeletCallLog(modelList);
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuDeleteConversation_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                if (e.VerticalChange > 0 && e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0 && e.VerticalOffset >= (Scroll.ScrollableHeight - 100))
                {
                    this.LoadNextCallLogs();
                }
                if (Math.Abs(e.VerticalChange) > 0)
                {
                    this.ChangeFriendOpenStatus();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Scroll_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void LoadCallLogs()
        {
            try
            {
                this._IS_LOADING = true;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); ;
                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

                _Thread = new Thread(() =>
                {
                    Thread.Sleep(200);

                    List<RecentDTO> list = RecentChatCallActivityDAO.LoadRecentCallContactList(ChatConstants.DAY_365_DAYS, this._DB_OFFSET, this._LIMIT);
                    CallLogLoadUtility.LoadRecentData(list);
                    this._DB_OFFSET = this._DB_OFFSET + list.Count;

                    Thread.Sleep(100);
                    this.IsCallLogEnabled = true;
                    this._IS_LOADING = false;
                    Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                });
                _Thread.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadCallLogs() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void LoadNextCallLogs()
        {
            if (this._IS_LOADING == false && (this._ScrollThread == null || this._ScrollThread.IsAlive == false))
            {
                this._ScrollThread = new Thread(new ThreadStart(LoadCallLogByScroll));
                this._ScrollThread.Start();
            }
        }

        private void LoadCallLogByScroll()
        {
            this._IS_LOADING = true;
            List<RecentDTO> list = RecentChatCallActivityDAO.LoadRecentCallContactList(ChatConstants.DAY_365_DAYS, this._DB_OFFSET, this._LIMIT);
            this._DB_OFFSET = this._DB_OFFSET + list.Count;
            CallLogLoadUtility.LoadRecentData(list);
            this._IS_LOADING = false;
        }

        private void OnSelectContactID(object param)
        {
            RecentModel model = (RecentModel)param;
            this.SelectedCallLogID = model.CallLog.CallLogID;
            this.SelectedCallingTime = model.CallLog.CallingTime;
            RingIDViewModel.Instance.OnFriendCallChatButtonClicked(model.FriendTableID);
        }

        private void ChangeFriendOpenStatus()
        {
            try
            {
                if (_ContactListResizeTimer == null)
                {
                    _ContactListResizeTimer = new DispatcherTimer();
                    _ContactListResizeTimer.Interval = TimeSpan.FromSeconds(1);
                    _ContactListResizeTimer.Tick += (o, e) =>
                    {
                        HelperMethods.ChangeViewPortOpenedProperty(Scroll, itemsControl);
                        _ContactListResizeTimer.Stop();
                        _ContactListResizeTimer.Interval = TimeSpan.FromSeconds(0.2);
                    };
                }

                _ContactListResizeTimer.Stop();
                _ContactListResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnSelectAll(object param)
        {
            try
            {
                IsSelectAllMode = !IsSelectAllMode;

                if (IsSelectAllMode == false)
                {
                    for (int i = 0; i < itemsControl.Items.Count; i++)
                    {
                        ContentPresenter child = (ContentPresenter)itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                        RecentModel model = (RecentModel)child.Content;
                        child.ApplyTemplate();

                        Control presenter = (Control)child.ContentTemplate.FindName("container", child);
                        presenter.ApplyTemplate();

                        CustomBorder border = (CustomBorder)presenter.Template.FindName("customBorder", presenter);
                        if (border.Visibility == Visibility.Visible)
                        {
                            model.IsChecked = !IsSelectAllMode;
                        }
                    }
                }
                else
                {
                    RingIDViewModel.Instance.CallLogModelsList.ToList().ForEach(P => P.IsChecked = !IsSelectAllMode);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSelectAll() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnShowChatDelete(object param)
        {
            IsDeletePanelVisible = true;
            IsSelectAllMode = true;
        }

        private void OnCallDelete(object param)
        {
            try
            {
                List<RecentModel> modelList = new List<RecentModel>();
                for (int i = 0; i < itemsControl.Items.Count; i++)
                {
                    ContentPresenter child = (ContentPresenter)itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                    RecentModel model = (RecentModel)child.Content;
                    child.ApplyTemplate();

                    Control presenter = (Control)child.ContentTemplate.FindName("container", child);
                    presenter.ApplyTemplate();

                    CustomBorder border = (CustomBorder)presenter.Template.FindName("customBorder", presenter);
                    if (border.Visibility == Visibility.Visible && model.IsChecked == true)
                    {
                        modelList.Add(model);
                    }
                }

                if (modelList.Count == 0)
                {
                    UIHelperMethods.ShowWarning("Please Select Call Log First!");
                    //CustomMessageBox.ShowError("Please Select Call Log First!");
                    return;
                }

                DeletCallLog(modelList);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCallDelete() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DeletCallLog(List<RecentModel> modelList)
        {
            //MessageBoxResult messageBoxResult = CustomMessageBox.ShowQuestion(NotificationMessages.DELETE_NOTIFICAITON);

            //WNConfirmationView cv = new WNConfirmationView("Delete confirmation!", NotificationMessages.DELETE_NOTIFICAITON, CustomConfirmationDialogButtonOptions.YesNo);
            //var result = cv.ShowCustomDialog();
            //if (result == ConfirmationDialogResult.Yes)
            //{
            bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.SURE_WANT_TO, "delete"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
            if (isTrue)
            {
                new Thread(() =>
                {
                    try
                    {
                        ConcurrentDictionary<long, List<string>> tempDic = new ConcurrentDictionary<long, List<string>>();
                        foreach (RecentModel model in modelList)
                        {
                            List<string> callIds = tempDic.TryGetValue(model.CallLog.FriendTableID);
                            if (callIds == null)
                            {
                                callIds = new List<string>();
                                tempDic[model.CallLog.FriendTableID] = callIds;
                            }
                            callIds.AddRange(model.CallLog.CallIDs.Keys.ToList());
                        }

                        foreach (KeyValuePair<long, List<string>> pair in tempDic)
                        {
                            RecentChatCallActivityDAO.DeleteCallHistory(pair.Key, pair.Value);
                            ObservableDictionary<string, long> unreadList = ChatViewModel.Instance.GetUnreadCallByID(pair.Key);
                            ChatHelpers.RemoveUnreadCall(pair.Key.ToString(), unreadList.Keys.ToList(), unreadList);
                            RecentLoadUtility.RemoveRange(pair.Key, pair.Value, true);
                            CallLogLoadUtility.RemoveRange(pair.Key.ToString(), pair.Value);
                        }

                        OnCallDeleteCancel(null);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error: DeletCallLog()::Invoke() => " + ex.Message + "\n" + ex.StackTrace);
                    }
                }).Start();
            }
        }

        private void OnCallDeleteCancel(object param)
        {
            try
            {
                IsSelectAllMode = true;
                IsDeletePanelVisible = false;
                RingIDViewModel.Instance.CallLogModelsList.ToList().ForEach(P => P.IsChecked = !IsSelectAllMode);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChatDeleteCancel() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnMakeCallClick(object param)
        {
            RecentModel recentModel = (RecentModel)param;
            if (recentModel.CallLog.CallCategory == XamlStaticValues.CALL_TYPE_VOICE)
            {
                RingIDViewModel.Instance.OnVoiceCallButtonClicked(recentModel.CallLog.FriendTableID);
            }
            else if (recentModel.CallLog.CallCategory == XamlStaticValues.CALL_TYPE_VIDEO)
            {
                RingIDViewModel.Instance.OnVideoCallButtonClicked(recentModel.CallLog.FriendTableID);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility method

        #region Property

        public ICommand OnSelectContactIDCommand
        {
            get
            {
                if (_OnSelectContactIDCommand == null)
                {
                    _OnSelectContactIDCommand = new RelayCommand((param) => OnSelectContactID(param));
                }
                return _OnSelectContactIDCommand;
            }
        }

        public ICommand ShowDeletePanelCommand
        {
            get
            {
                if (_ShowDeletePanelCommand == null)
                {
                    _ShowDeletePanelCommand = new RelayCommand((param) => OnShowChatDelete(param));
                }
                return _ShowDeletePanelCommand;
            }
        }

        public ICommand SelectAllCommand
        {
            get
            {
                if (_SelectAllCommand == null)
                {
                    _SelectAllCommand = new RelayCommand((param) => OnSelectAll(param));
                }
                return _SelectAllCommand;
            }
        }

        public ICommand CallDeleteCommand
        {
            get
            {
                if (_CallDeleteCommand == null)
                {
                    _CallDeleteCommand = new RelayCommand((param) => OnCallDelete(param));
                }
                return _CallDeleteCommand;
            }
        }

        public ICommand CallDeleteCancelCommand
        {
            get
            {
                if (_CallDeleteCancelCommand == null)
                {
                    _CallDeleteCancelCommand = new RelayCommand((param) => OnCallDeleteCancel(param));
                }
                return _CallDeleteCancelCommand;
            }
        }

        public ICommand MakeCallCommand
        {
            get
            {
                if (_MakeCallCommand == null)
                {
                    _MakeCallCommand = new RelayCommand(param => OnMakeCallClick(param));
                }
                return _MakeCallCommand;
            }
        }

        public string SelectedCallLogID
        {
            get { return _SelectedCallLogID; }
            set
            {
                if (value == _SelectedCallLogID)
                    return;

                _SelectedCallLogID = value;
                this.OnPropertyChanged("SelectedCallLogID");
            }
        }

        public long SelectedCallingTime
        {
            get { return this._SelectedCallingTime; }
            set
            {
                if (this._SelectedCallingTime == value)
                    return;

                this._SelectedCallingTime = value;
                this.OnPropertyChanged("SelectedCallingTime");
            }
        }

        public bool IsDeletePanelVisible
        {
            get { return _IsDeletePanelVisible; }
            set
            {
                _IsDeletePanelVisible = value;
                this.OnPropertyChanged("IsDeletePanelVisible");
            }
        }

        public bool IsSelectAllMode
        {
            get { return _IsSelectAllMode; }
            set
            {
                _IsSelectAllMode = value;
                this.OnPropertyChanged("IsSelectAllMode");
            }
        }

        public bool IsCallLogEnabled
        {
            get { return this._IsCallLogEnabled; }
            set
            {
                if (this._IsCallLogEnabled == value)
                    return;

                this._IsCallLogEnabled = value;
                this.OnPropertyChanged("IsCallLogEnabled");
            }
        }

        #endregion Property

    }
}

﻿using System;
using System.Windows;
using System.Windows.Input;
using View.Utility;
using View.Utility.SignInSignUP;

namespace View.UI.Call
{
    /// <summary>
    /// Interaction logic for WNCallUiInSmallScreen.xaml
    /// </summary>
    public partial class WNCallUiInSmallScreen : Window
    {
        #region "Constructors"
        public WNCallUiInSmallScreen()
        {
            InitializeComponent();
            this.Loaded += WNCallUiInSmallScreen_Loaded;
            this.Unloaded += WNCallUiInSmallScreen_Unloaded;
            this.Closed += WNCallUiInSmallScreen_Closed;
            this.MouseDown += WNCallUiInSmallScreen_MouseDown;
            //this.Owner = MainSwitcher.MainController().MainUIController().MainWindow;
            if (WNRingIDMain.WinRingIDMain != null) this.Owner = WNRingIDMain.WinRingIDMain;
        }

        #endregion "Constructors"

        #region "Properties"
        public UCCallControlContainer CallControlContainer { get; set; }
        #endregion "Properties"

        #region "Event Trigger"

        void WNCallUiInSmallScreen_Unloaded(object sender, RoutedEventArgs e)
        {
            _MainControlConainer.Children.Clear();
            CallControlContainer = null;
        }

        void WNCallUiInSmallScreen_Loaded(object sender, RoutedEventArgs e)
        {
            Keyboard.Focus(this);
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom / 2;
            initCallMianUi();
            _MainControlConainer.Children.Add(CallControlContainer);
            this.StateChanged += new EventHandler(Window_StateChanged);
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                //MaxWidth = double.NaN;
                //MaxHeight = double.NaN;
                Keyboard.Focus(this);
                _MainPanel.Height = System.Windows.SystemParameters.WorkArea.Height;
            }
            else if (this.WindowState == WindowState.Normal)
            {
                _MainPanel.Height = double.NaN;
                //   Height = 400;
                //MaxWidth = 360;
                //MaxHeight = 400;
            }
        }

        void WNCallUiInSmallScreen_Closed(object sender, System.EventArgs e)
        {
            if (MainSwitcher.CallController.CallUiInSmallScreen != null)
            {
                MainSwitcher.CallController.CallUiInSmallScreen = null;
            }
        }

        private void FormFadeOut_Completed(object sender, System.EventArgs e)
        {

        }
        void WNCallUiInSmallScreen_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        #endregion "Event Trigger"

        #region "Private methods"
        private void initCallMianUi()
        {
            CallControlContainer = new UCCallControlContainer();
            CallControlContainer.SetCallController(MainSwitcher.CallController);
        }

        #endregion "Private methods"

        #region "Public Methods"


        public void ShowThisWindow()
        {
            this.Show();
        }

        public void CloseThisWindow()
        {
            this.Close();
        }
        #endregion "Public methods"
    }
}

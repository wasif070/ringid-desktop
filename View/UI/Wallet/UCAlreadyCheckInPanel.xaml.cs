﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCAlreadyCheckInPanel.xaml
    /// </summary>
    public partial class UCAlreadyCheckInPanel : UserControl
    {
        public UCAlreadyCheckInPanel()
        {
            InitializeComponent();
            this.Loaded += UCAlreadyCheckInPanel_Loaded;
        }

        void UCAlreadyCheckInPanel_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimateHorizontal(_GridPanel);
        }
    }
}

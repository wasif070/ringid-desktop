﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility.FriendList;
using View.ViewModel;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCCoinTransfer.xaml
    /// </summary>
    public partial class UCCoinTransfer : UserControl
    {
        private Timer SearchTextBoxTimer;
        private int ThreadMaxRunningTime = 0;
        public event EventHandler SwitchToMyCoinStatPanelRequested;
        private void OnSwitchToMyCoinStatPanelRequested()
        {
            var handler = SwitchToMyCoinStatPanelRequested;
            if (handler != null) handler(this, EventArgs.Empty);
        }
        public UCCoinTransfer()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCCoinTransfer_Loaded;
            this.Unloaded += UCCoinTransfer_Unloaded;
        }

        void UCCoinTransfer_Unloaded(object sender, RoutedEventArgs e)
        {
            _ReferralPopup.ItemSelectionHandler -= referralPopup_ItemSelectionHandler;

            //Utility.Wallet.WalletViewModel.Instance.CoinTransactionToggleHandler -= Instance_CoinTransactionToggleHandler;

            //Utility.Wallet.WalletViewModel.Instance.TransactionRemarks = string.Empty;
            //Utility.Wallet.WalletViewModel.Instance.SelectedReferral = null;
            //Utility.Wallet.WalletViewModel.Instance.CoinTransactionAmount = string.Empty;
            //Utility.Wallet.WalletViewModel.Instance.CoinTransferErrorMessage = string.Empty;
        }

        void UCCoinTransfer_Loaded(object sender, RoutedEventArgs e)
        {
            SearchTextBoxTimer = new Timer();
            _TextboxAmountTransfer.Clear();
            _TextboxRingIDSender.Clear();
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimateHorizontal(_BorderPanel);            
            _TextboxAmountTransfer.Focus();
            _ReferralPopup.ItemSelectionHandler += referralPopup_ItemSelectionHandler;

            //Utility.Wallet.WalletViewModel.Instance.CoinTransactionToggleHandler += Instance_CoinTransactionToggleHandler;

            //Utility.Wallet.WalletViewModel.Instance.TransactionRemarks = string.Empty;
            //Utility.Wallet.WalletViewModel.Instance.SelectedReferral = null;
            //Utility.Wallet.WalletViewModel.Instance.CoinTransactionAmount = string.Empty;
            //Utility.Wallet.WalletViewModel.Instance.CoinTransferErrorMessage = string.Empty;
        }

        void Instance_CoinTransactionToggleHandler(bool enable)
        {
            _BorderPanel.IsEnabled = enable;
        }

        void referralPopup_ItemSelectionHandler(BindingModels.UserBasicInfoModel selectedItem)
        {
            //View.Utility.Wallet.WalletViewModel.Instance.SelectedReferral = selectedItem;
            
            //_TextboxRingIDSender.Text = View.Utility.Wallet.WalletViewModel.Instance.SelectedReferral.ShortInfoModel.FullName;

            //ClearTempList();
            //_ReferralPopup.Hide();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();            
        }        

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.OnSwitchToMyCoinStatPanelRequested();
        }

        private string _SearchString;
        public string SearchString
        {
            get { return _SearchString; }
            set
            {
                _SearchString = value;
                //set default params
                SetDefaultParameter();
                if (!string.IsNullOrWhiteSpace(_SearchString))
                {
                    if (!_ReferralPopup.IsOpen)
                    {
                        _ReferralPopup.Show(_TextboxRingIDSender);
                    }
                    OnSearch();
                }
                else
                {
                    View.Utility.FriendList.ThreadLeftSearchFriend.searchParm = null;
                    if (!_ReferralPopup.IsOpen)
                    {
                        _ReferralPopup.Show(_TextboxRingIDSender);
                    }
                    ClearTempList();
                    _ReferralPopup.Hide();
                }
            }
        }

        private void SetDefaultParameter()
        {
            ThreadLeftSearchFriend.processingList = false;
            ThreadLeftSearchFriend.SearchForShowMore = false;
            ThreadLeftSearchFriend.totalSearchCount = 0;
            ThreadLeftSearchFriend.friendCountInOneRequest = 0;
            if (_ReferralPopup != null)
            {
                _ReferralPopup.HideShowMorePanel();
                _ReferralPopup.NoResultsFound = false;
            }
        }

        void SearchTextBoxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                if (string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString) && !ThreadLeftSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        _ReferralPopup._Scroll.ScrollToHome();
                    });
                    ThreadLeftSearchFriend.searchParm = SearchString.ToLower();

                    View.Utility.MainSwitcher.ThreadManager().LeftSearchFriend.StartThread(ThreadLeftSearchFriend.SearchFromLeftPanel, StatusConstants.SEARCH_FRIENDLIST);
                }
            }
            else
            {
                ClearTempListFromTimer();
            }
            ThreadMaxRunningTime += 500;
            if (ThreadMaxRunningTime >= 3000)
            {
                ThreadMaxRunningTime = 0;
                SearchTextBoxTimer.Enabled = false;
                SearchTextBoxTimer.Stop();
            }
        }

        private void ClearTempListFromTimer()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                lock (RingIDViewModel.Instance.TempFriendSearchList)
                {
                    RingIDViewModel.Instance.TempFriendSearchList.Clear();
                }
            });
        }

        private void OnSearch()
        {
            try
            {
                if (string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString) && !ThreadLeftSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    ClearTempList();
                    ThreadMaxRunningTime = 0;
                    SearchTextBoxTimer.Elapsed -= SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Elapsed += SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Interval = 500;
                    SearchTextBoxTimer.AutoReset = true;
                    SearchTextBoxTimer.Enabled = true;
                    SearchTextBoxTimer.Stop();
                    SearchTextBoxTimer.Start();
                }
            }
            catch (Exception ex)
            {
                //log.Error(ex.StackTrace + " " + ex.Message);
            }
        }
        private void ClearTempList()
        {
            if (RingIDViewModel.Instance.TempFriendSearchList != null)
                RingIDViewModel.Instance.TempFriendSearchList.Clear();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (_ReferralPopup.IsOpen)
            {
                ClearTempList();
                _ReferralPopup.Hide();                
            }
            _TextboxRingIDSender.Clear();
            _TextboxRingIDSender.Focus();

            //if (View.Utility.Wallet.WalletViewModel.Instance.SelectedReferral != null)
            //{
            //    View.Utility.Wallet.WalletViewModel.Instance.SelectedReferral = null;
            //}
        }
    }
}

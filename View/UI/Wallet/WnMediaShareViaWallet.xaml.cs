﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using View.ViewModel;
using View.Constants;


namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for WnMediaShareViaWallet.xaml
    /// </summary>
    public partial class WnMediaShareViaWallet : Window, IDisposable, INotifyPropertyChanged
    {
        private string keyPostID = "post_id";
        private string keyErrorCode = "error_code";
        private string keyAccessToken = "#access_token=";
        private string keyClose = "close";
        private System.Windows.Forms.WebBrowser _WebBrowser = null;

        private string InviteString
        {
            get { return string.Format("Hi! Join me in ringID for live conversations, file sharing, free voice & video calls and more. Enter this {0} as your referral code to sign up.", Models.Constants.DefaultSettings.userProfile.RingID.ToString().Substring(2).Insert(4, " ")); }
        }

        private string OAuthURI
        {
            get { return string.Format("{0}client_id={1}&redirect_uri={2}&response_type=token&type=user_agent&display=popup", SocialMediaConstants.FACEBOOK_OAuthUrl, SocialMediaConstants.FACEBOOK_CLIENT_ID, SocialMediaConstants.FACEBOOK_SUCCESS_LINK); }
        }

        private string FeedDialogURI
        {
            get { return string.Format("{0}app_id={1}&display_popup&link={2}&caption={3}", SocialMediaConstants.FACEBOOK_FeedDialogUrl, SocialMediaConstants.FACEBOOK_CLIENT_ID, RingIDViewModel.Instance.ContentIDHash, InviteString); }
        }

        private static WnMediaShareViaWallet instance;
        public static WnMediaShareViaWallet Instance
        {
            get { instance = instance ?? new WnMediaShareViaWallet(); return instance; }
        }

        private bool isLoading = false;
        public bool IsLoading
        {
            get { return isLoading; }
            set { isLoading = value; this.OnPropertyChanged("IsLoading"); }
        }

        private bool isOpen = false;
        public bool IsOpen
        {
            get { return isOpen; }
            set { isOpen = value; this.OnPropertyChanged("IsOpen"); }
        }


        public WnMediaShareViaWallet()
        {
            InitializeComponent();
            this.DataContext = this;
            this.MouseDown += delegate { DragMove(); };
            this.Loaded += WnMediaShareViaWallet_Loaded;
            this.Unloaded += WnMediaShareViaWallet_Unloaded;
            this.Closed += delegate
            {
                instance = null;
                Dispose();
            };
        }

        void WnMediaShareViaWallet_Unloaded(object sender, RoutedEventArgs e)
        {
            IsLoading = false;
            _btnClose.Click -= _btnClose_Click;
        }

        void WnMediaShareViaWallet_Loaded(object sender, RoutedEventArgs e)
        {
            _btnClose.Click += _btnClose_Click;
            if (_WebBrowser == null)
            {
                _WebBrowser = new System.Windows.Forms.WebBrowser();
            }
            _BrowserHost.Child = _WebBrowser;
            _WebBrowser.ScriptErrorsSuppressed = true;
            _WebBrowser.ScrollBarsEnabled = false;
            _WebBrowser.Navigating += _WebBrowser_Navigating;
            _WebBrowser.Navigated += _WebBrowser_Navigated;
            _WebBrowser.Navigate(OAuthURI);
        }

        void _WebBrowser_Navigating(object sender, System.Windows.Forms.WebBrowserNavigatingEventArgs e)
        {
            IsLoading = true;
        }

        void _btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (RingIDViewModel.Instance.MediaShareViaWallet)
            {
                View.Utility.MiddlePanelSwitcher.Switch(Constants.MiddlePanelConstants.TypeWallet);
            }
            RingIDViewModel.Instance.ResetMediaShareInfo();
            Dispose();
            IsOpen = false;
            this.Close();
        }

        private void _WebBrowser_Navigated(object sender, System.Windows.Forms.WebBrowserNavigatedEventArgs e)
        {
            IsLoading = false;

            if (e.Url.AbsoluteUri.Contains(keyAccessToken))
            {
                int idx = e.Url.AbsoluteUri.IndexOf(keyAccessToken);
                string AccessToken = e.Url.AbsoluteUri.Substring(idx + keyAccessToken.Length).Split('&')[0];
                _WebBrowser.Navigate(FeedDialogURI);
            }
            else if (e.Url.AbsoluteUri.Contains(keyClose))
            {
                if (e.Url.AbsoluteUri.Contains(keyPostID))
                {
                    View.Utility.UIHelperMethods.ShowMessageWithTimerFromNonThread(UI.UCGuiRingID.Instance.MotherPanel, "Media shared successfully!");
                    new Utility.Wallet.ThreadWalletDailyUtilities().StartMediaShareOnFacebook();
                }
                else if (e.Url.AbsoluteUri.Contains(keyErrorCode))
                {
                    RingIDViewModel.Instance.ResetMediaShareInfo();
                }
                if (RingIDViewModel.Instance.MediaShareViaWallet)
                {
                    Utility.MiddlePanelSwitcher.Switch(Constants.MiddlePanelConstants.TypeWallet);
                }

                Dispose();

                new System.Threading.Thread(() =>
                {
                    System.Threading.Thread.Sleep(50);
                    CloseWindow();
                }).Start();

            }
        }

        private void CloseBrowser()
        {
            if (_WebBrowser != null)
            {
                _WebBrowser.Navigated -= _WebBrowser_Navigated;
            }
            _WebBrowser = null;
        }

        public void Dispose()
        {
            CloseBrowser();
        }

        public new void Show()
        {
            IsOpen = true;
            base.Show();
            this.Owner = Application.Current.MainWindow;

            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        public void CloseWindow()
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
            {
                IsOpen = false;
                this.Owner = null;
                this.Close();
            });

        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

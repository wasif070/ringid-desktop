﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCCheckInSuccessPanel.xaml
    /// </summary>
    public partial class UCCheckInSuccessPanel : UserControl, System.ComponentModel.INotifyPropertyChanged
    {
        System.Windows.Threading.DispatcherTimer closeTimer = null;
        public UCCheckInSuccessPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCCheckInSuccessPanel_Loaded;
            this.Unloaded += UCCheckInSuccessPanel_Unloaded;
        }

        void UCCheckInSuccessPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            if (closeTimer != null && closeTimer.IsEnabled)
            {
                closeTimer.Stop();
            }
            closeTimer = null;
            _CheckInSuccessPanel.MouseLeftButtonDown -= _CheckInSuccessPanel_MouseLeftButtonDown;
            _BtnClose.Click -= _BtnClose_Click;
            //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowDailyCheckIn();
        }

        void UCCheckInSuccessPanel_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimate(_CheckInSuccessPanel);            
            closeTimer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromSeconds(10) };
            closeTimer.Tick += ((ss, ee) =>
                {
                    closeTimer.Stop();
                    Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                });
            closeTimer.Start();
            _CheckInSuccessPanel.MouseLeftButtonDown += _CheckInSuccessPanel_MouseLeftButtonDown;
            _BtnClose.Click += _BtnClose_Click;
        }

        void _BtnClose_Click(object sender, RoutedEventArgs e)
        {
            if (closeTimer != null && closeTimer.IsEnabled)
            {
                closeTimer.Stop();
            }
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        void _CheckInSuccessPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        public int DailyCheckInCoin { get { return Utility.Wallet.WalletViewModel.Instance.CoinEarningRules.Where(x => x.RuleItemID == WalletConstants.COIN_EARNING_RULE_DAILY_CHECK_IN).First().CoinQuantity; } }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler =  PropertyChanged;
            if (handler != null) handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }
    }
}

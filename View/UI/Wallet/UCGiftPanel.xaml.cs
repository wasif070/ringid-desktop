﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCGiftPanel.xaml
    /// </summary>
    public partial class UCGiftPanel : UserControl
    {
        public UCGiftPanel()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCGiftPanel_Loaded;
            this.Unloaded += UCGiftPanel_Unloaded;
        }       

        void UCGiftPanel_Loaded(object sender, RoutedEventArgs e)
        {
            new Utility.Wallet.ThreadGetGiftProducts().StartGetGifts();
        }

        void UCGiftPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            Utility.Wallet.WalletViewModel.Instance.GiftProductsList.Clear();
        }
    }
}

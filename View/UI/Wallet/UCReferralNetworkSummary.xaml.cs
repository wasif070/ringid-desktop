﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCReferralNetworkSummary.xaml
    /// </summary>
    public partial class UCReferralNetworkSummary : UserControl
    {
        public UCReferralNetworkSummary()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCReferralNetworkSummary_Loaded;
        }

        void UCReferralNetworkSummary_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_BorderPanel);
            
            //Utility.Wallet.WalletViewModel.Instance.AcceptedReferralListType = 0;
        }

        private void goldPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //Utility.Wallet.WalletViewModel.Instance.AcceptedReferralListType = 1;
            new Utility.Wallet.ThreadGetMyReferralList().StartGoldMemberList();
            //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowAcceptedReferralList();
        }

        private void silverPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //Utility.Wallet.WalletViewModel.Instance.AcceptedReferralListType = 2;
            new Utility.Wallet.ThreadGetMyReferralList().StartSilverMemberList();
            //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowAcceptedReferralList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletTransactionHistoryReceived.xaml
    /// </summary>
    public partial class UCWalletTransactionHistoryReceived : UserControl
    {
        public UCWalletTransactionHistoryReceived()
        {
            InitializeComponent();
            DataContext = Utility.Wallet.WalletViewModel.Instance;
            Loaded += UCWalletTransactionHistoryReceived_Loaded;
            Unloaded += UCWalletTransactionHistoryReceived_Unloaded;
        }

        void UCWalletTransactionHistoryReceived_Unloaded(object sender, RoutedEventArgs e)
        {
            //Utility.Wallet.WalletViewModel.Instance.IsReceivedLoading = false;
            //Utility.Wallet.WalletViewModel.Instance.ReceivedNetworkError = false;
        }

        void UCWalletTransactionHistoryReceived_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(receivedPanel);
            //Utility.Wallet.WalletViewModel.Instance.IsReceivedLoading = true;
            //new Utility.Wallet.ThreadGetWalletTransactionHistory().StartReceivedTransactionHistory();
        }
    }
}

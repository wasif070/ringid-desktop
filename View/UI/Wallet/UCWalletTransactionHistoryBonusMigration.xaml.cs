﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletTransactionHistoryBonusMigration.xaml
    /// </summary>
    public partial class UCWalletTransactionHistoryBonusMigration : UserControl
    {
        public UCWalletTransactionHistoryBonusMigration()
        {
            InitializeComponent();
            DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCWalletTransactionHistoryBonusMigration_Loaded;
            this.Unloaded += UCWalletTransactionHistoryBonusMigration_Unloaded;
        }

        void UCWalletTransactionHistoryBonusMigration_Unloaded(object sender, RoutedEventArgs e)
        {
            //Utility.Wallet.WalletViewModel.Instance.IsBonusLoading = false;
            //Utility.Wallet.WalletViewModel.Instance.BonusNetworkError = false;         
        }

        void UCWalletTransactionHistoryBonusMigration_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(bonusPanel);
            //Utility.Wallet.WalletViewModel.Instance.IsBonusLoading = true;
            //new Utility.Wallet.ThreadGetWalletTransactionHistory().StartBonusMigrationTransactionHistory();
        }
    }
}

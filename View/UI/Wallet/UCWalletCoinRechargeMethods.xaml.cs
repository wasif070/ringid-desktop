﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletCoinRechargeMethods.xaml
    /// </summary>
    public partial class UCWalletCoinRechargeMethods : UserControl
    {
        public UCWalletCoinRechargeMethods()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCWalletCoinRechargeMethods_Loaded;
            this.Unloaded += UCWalletCoinRechargeMethods_Unloaded;
            
        }

        void _GridPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        void _BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        void UCWalletCoinRechargeMethods_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_GridPanel);
            _GridPanel.MouseLeftButtonDown += _GridPanel_MouseLeftButtonDown;
            _BtnClose.Click += _BtnClose_Click;
        }        

        void UCWalletCoinRechargeMethods_Unloaded(object sender, RoutedEventArgs e)
        {
            _GridPanel.MouseLeftButtonDown -= _GridPanel_MouseLeftButtonDown;
            _BtnClose.Click -= _BtnClose_Click;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCReferralList.xaml
    /// </summary>
    public partial class UCReferralList : UserControl
    {
        public UCReferralList()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCReferralList_Loaded;
            this.Unloaded += UCReferralList_Unloaded;
        }

        void UCReferralList_Unloaded(object sender, RoutedEventArgs e)
        {
            //_GridPanel.MouseLeftButtonDown -= _GridPanel_MouseLeftButtonDown;
            //_BtnClose.Click -= _BtnClose_Click;
        }

        void UCReferralList_Loaded(object sender, RoutedEventArgs e)
        {
            //initial request
            if (!Utility.Wallet.WalletViewModel.Instance.InitialRequestReferralList)
            {
                Utility.Wallet.WalletViewModel.Instance.ReferralListLoading = true;
                new Utility.Wallet.ThreadGetMyReferralList().StartMyReferralList();
            }
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_BorderPanel);

            //_GridPanel.MouseLeftButtonDown += _GridPanel_MouseLeftButtonDown;
            //_BtnClose.Click += _BtnClose_Click;
        }

        //void _BtnClose_Click(object sender, RoutedEventArgs e)
        //{
        //    Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        //}

        //void _GridPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    e.Handled = true;
        //}
    }
}

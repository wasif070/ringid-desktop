﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCReferrerPanel.xaml
    /// </summary>
    public partial class UCReferrerPanel : UserControl
    {
        public UCReferrerPanel()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCReferrerPanel_Loaded;
        }

        void UCReferrerPanel_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_AddReferrerPanel);
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_ReferrerInfoPanel);
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_ReferralCountPanel);
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_ReferrerErrorPanel);

            //Utility.UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, "Value of referrer set " + Utility.Wallet.WalletViewModel.Instance.IsReferrerSet + "\nValue of referrer skipped " + Utility.Wallet.WalletViewModel.Instance.IsReferrerSkipped);
            
        }
    }
}

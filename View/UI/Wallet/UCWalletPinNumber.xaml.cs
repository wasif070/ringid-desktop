﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Constants;
using View.Utility.Wallet;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletPinNumber.xaml
    /// </summary>
    public partial class UCWalletPinNumber : UserControl
    {
        public UCWalletPinNumber()
        {
            InitializeComponent();
            this.DataContext = WalletViewModel.Instance;
            this.Loaded += UCWalletPinNumber_Loaded;
            this.Unloaded += UCWalletPinNumber_Unloaded;            
        }

        void UCWalletPinNumber_Unloaded(object sender, RoutedEventArgs e)
        {
            //WalletViewModel.Instance.InputPin = string.Empty;
            //WalletViewModel.Instance.ConfirmPin = string.Empty;
            //WalletViewModel.Instance.IsPinNumberSet = !string.IsNullOrWhiteSpace(Models.Constants.DefaultSettings.WALLET_PIN_NUMBER);
            //WalletViewModel.Instance.IsResetPinNumber = false;
        }

        void UCWalletPinNumber_Loaded(object sender, RoutedEventArgs e)
        {
            //View.Utility.Wallet.WalletViewModel.Instance.IsPinNumberSet = !(string.IsNullOrWhiteSpace(SettingsConstants.VALUE_WALLET_PIN_NUMBER));
            //View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimate(borderPanel);            
            //textboxCreateOrLoginPin.Focus();

            //WalletViewModel.Instance.PinNumberSuccessRequested += Instance_PinNumberSuccessRequested;
        }

        void Instance_PinNumberSuccessRequested(object sender, EventArgs e)
        {
            View.Utility.MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeWallet);
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();            
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            //View.Utility.MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }

        private void borderPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void borderPanel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            }
        }
    }
}

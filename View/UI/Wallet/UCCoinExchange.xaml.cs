﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCCoinExchange.xaml
    /// </summary>
    public partial class UCCoinExchange : UserControl
    {
        public UCCoinExchange()
        {
            InitializeComponent();
            this.Loaded += UCCoinExchange_Loaded;
            this.Unloaded += UCCoinExchange_Unloaded;
        }

        void UCCoinExchange_Unloaded(object sender, RoutedEventArgs e)
        {
            //View.Utility.Wallet.WalletViewModel.Instance.CoinTransactionAmount = string.Empty;
            //View.Utility.Wallet.WalletViewModel.Instance.CoinTransferFromBonusToWalletErrorMessage = string.Empty;

            //Utility.Wallet.WalletViewModel.Instance.BonusCoinMigrationToggleHandler -= Instance_BonusCoinMigrationToggleHandler;
        }

        void UCCoinExchange_Loaded(object sender, RoutedEventArgs e)
        {
            //View.Utility.Wallet.WalletViewModel.Instance.CoinTransferFromBonusToWalletErrorMessage = string.Empty;
            //View.Utility.Wallet.WalletViewModel.Instance.CoinTransactionAmount = string.Empty;
            
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimate(borderPanel);            
            textboxAmount.Focus();

            //Utility.Wallet.WalletViewModel.Instance.BonusCoinMigrationToggleHandler += Instance_BonusCoinMigrationToggleHandler;
        }

        void Instance_BonusCoinMigrationToggleHandler(bool enable)
        {
            borderPanel.IsEnabled = enable;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        private void borderPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void borderPanel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            }
        }
    }
}

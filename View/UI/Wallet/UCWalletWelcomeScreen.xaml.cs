﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.UI.PopUp;
using View.Utility;
using View.Utility.SignInSignUP;
using View.ViewModel;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletWelcomeScreen.xaml
    /// </summary>
    public partial class UCWalletWelcomeScreen : UserControl
    {
        //public UCDigitsCustomMsgPopup ucDigitsCustomMsgPopup = null;
        public UCWalletWelcomeScreen()
        {
            InitializeComponent();
            this.Loaded += UCWalletWelcomeScreen_Loaded;
            this.Unloaded += UCWalletWelcomeScreen_Unloaded;
        }

        void UCWalletWelcomeScreen_Loaded(object sender, RoutedEventArgs e)
        {
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimate(borderPanel);

            borderPanel.KeyDown += borderPanel_KeyDown;
            borderPanel.MouseLeftButtonDown += borderPanel_MouseLeftButtonDown;
            btnClose.Click += btnClose_Click;
        }

        void UCWalletWelcomeScreen_Unloaded(object sender, RoutedEventArgs e)
        {
            borderPanel.KeyDown -= borderPanel_KeyDown;
            borderPanel.MouseLeftButtonDown -= borderPanel_MouseLeftButtonDown;
            btnClose.Click -= btnClose_Click;
        }

        void btnClose_Click(object sender, RoutedEventArgs e)
        {
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        void borderPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        void borderPanel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            }
        }

        private void btnVerifyPhone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();

                //if (MainSwitcher.ThreadManager().DigitsVerificationThread == null)
                //    MainSwitcher.ThreadManager().DigitsVerificationThread = new ThradDigitsVerificationThread();
                //MainSwitcher.ThreadManager().DigitsVerificationThread.StartThread(DefaultSettings.LOGIN_RING_ID);

                //MainSwitcher.PopupController.WalletPopupWrapper.ShowDigitsCustomPopup();
            }
            catch (Exception ex)
            {

                //throw;
            }
        }

        private void btnVerifyIdentity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();

                //if (MainSwitcher.ThreadManager().DigitsVerificationThread == null)
                //    MainSwitcher.ThreadManager().DigitsVerificationThread = new ThradDigitsVerificationThread();
                //MainSwitcher.ThreadManager().DigitsVerificationThread.StartThread(DefaultSettings.LOGIN_RING_ID);

                //MainSwitcher.PopupController.WalletPopupWrapper.ShowDigitsCustomPopup();
            }
            catch (Exception ex)
            {

                //throw;
            }
        }
    }
}

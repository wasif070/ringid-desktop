﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCLoadingPanel.xaml
    /// </summary>
    public partial class UCLoadingPanel : UserControl
    {
        public UCLoadingPanel()
        {
            InitializeComponent();
        }
    }
}

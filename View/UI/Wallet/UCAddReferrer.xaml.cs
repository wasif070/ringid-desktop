﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCAddReferrer.xaml
    /// </summary>
    public partial class UCAddReferrer : UserControl
    {
        public UCAddReferrer()
        {
            InitializeComponent();
            this.Loaded += UCAddReferrer_Loaded;
            this.Unloaded += UCAddReferrer_Unloaded;
            
        }

        void UCAddReferrer_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_GridPanel);
            _GridPanel.MouseLeftButtonDown += _GridPanel_MouseLeftButtonDown;            
            _GridPanel.PreviewKeyDown += _GridPanel_PreviewKeyDown;
            _BtnProceed.Click += _BtnProceed_Click;
            _BtnLater.Click += _BtnLater_Click;
            _BtnProceed.Focus();
        }
        
        void UCAddReferrer_Unloaded(object sender, RoutedEventArgs e)
        {
            _GridPanel.MouseLeftButtonDown -= _GridPanel_MouseLeftButtonDown;            
            _GridPanel.PreviewKeyDown -= _GridPanel_PreviewKeyDown;
            _BtnProceed.Click -= _BtnProceed_Click;
            _BtnLater.Click -= _BtnLater_Click;
        }

        void _GridPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        void _GridPanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            }
        }

        void _BtnLater_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        void _BtnProceed_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowSetReferrer();
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_SET_REFERRER);
        }        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletMediaSharePopup.xaml
    /// </summary>
    public partial class UCWalletMediaSharePopup : UserControl
    {
        public UCWalletMediaSharePopup()
        {
            InitializeComponent();
            this.Loaded += UCWalletMediaSharePopup_Loaded;
            this.Unloaded += UCWalletMediaSharePopup_Unloaded;
        }

        void UCWalletMediaSharePopup_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_GridPanel);
            _BtnClose.Click += _BtnClose_Click;
            _BtnLater.Click += _BtnClose_Click;
            _GridPanel.MouseLeftButtonDown += _GridPanel_MouseLeftButtonDown;
        }

        void UCWalletMediaSharePopup_Unloaded(object sender, RoutedEventArgs e)
        {
            _BtnClose.Click -= _BtnClose_Click;
            _BtnLater.Click -= _BtnClose_Click;
            _GridPanel.MouseLeftButtonDown -= _GridPanel_MouseLeftButtonDown;
        }

        void _BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        void _GridPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }        
    }
}

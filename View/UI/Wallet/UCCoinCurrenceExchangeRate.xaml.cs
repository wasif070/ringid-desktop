﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCCoinCurrenceExchangeRate.xaml
    /// </summary>
    public partial class UCCoinCurrenceExchangeRate : UserControl
    {
        public UCCoinCurrenceExchangeRate()
        {
            InitializeComponent();
            this.DataContext = View.Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCCoinCurrenceExchangeRate_Loaded;
        }

        void UCCoinCurrenceExchangeRate_Loaded(object sender, RoutedEventArgs e)
        {
            //if (!View.Utility.Wallet.WalletViewModel.Instance.IsExchangeRateRequested)
            //{
            //    new View.Utility.Wallet.ThreadWalletExchangeRateInformation().StartThread();
            //}            
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimate(borderPanel);            
            textboxCoinAmount.Focus();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        private void borderPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void borderPanel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            }
        }

        private void comboBoxCurrency_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            textboxCoinAmount.Clear();
            textboxCurrency.Clear();
            View.BindingModels.WalletExchangeRateModel mdl = (sender as ComboBox).SelectedItem as View.BindingModels.WalletExchangeRateModel;
            textboxCurrency.Text = mdl.ExchangeRate.ToString("0.00");
            textboxCoinAmount.Text = "1";
        }        
    }
}

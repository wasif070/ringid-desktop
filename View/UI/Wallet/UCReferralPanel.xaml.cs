﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCReferralPanel.xaml
    /// </summary>
    public partial class UCReferralPanel : UserControl
    {
        private UCSendReferralRequestPanel ucSendReferralRequestPanel;
        private UCSendReferral ucSendReferral;
        private UCReferralNetworkSummary ucReferralNetworkSummary;
        private UCReferralPendingList ucReferralPendingList;
        private UCReferralIncomingList ucReferralIncomingList;
        public UCReferralPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCReferralPanel_Loaded;
            this.Unloaded += UCReferralPanel_Unloaded;
        }

        void UCReferralPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        void UCReferralPanel_Loaded(object sender, RoutedEventArgs e)
        {
            //if (!Utility.Wallet.WalletViewModel.Instance.IsReferralNetworkSummaryRequested)
            //{
            //    Utility.Wallet.WalletViewModel.Instance.IsPendingLoading = true; //1042_pReqs=true
            //    Utility.Wallet.WalletViewModel.Instance.IsReferralPanelLoading = true;
            //    Utility.Wallet.WalletViewModel.Instance.ReferralNetworkError = false;
            //    new Utility.Wallet.ThreadGetWalletReferralNetworkSummary().StartThread();
            //    //new Utility.Wallet.ThreadGetMyReferralList().StartThread(true);
            //    new Utility.Wallet.ThreadGetMyReferralList().StartPendingList();
            //}            
            //LoadSendReferralRequest();
            //LoadReferralNetworkSummary();
            //LoadReferralPendingList();
            //LoadReferralIncomingList();
            
        }

        private void LoadReferralIncomingList()
        {
            if (ucReferralIncomingList == null)
            {
                ucReferralIncomingList = new UCReferralIncomingList();
            }
            _ReferralIncomingPanel.Child = ucReferralIncomingList;
        }

        private void LoadReferralNetworkSummary()
        {
            if (ucReferralNetworkSummary == null)
            {
                ucReferralNetworkSummary = new UCReferralNetworkSummary();
            }
            _ReferralNetworkPanel.Child = ucReferralNetworkSummary;
        }

        private void LoadReferralPendingList()
        {
            if (ucReferralPendingList == null)
            {
                ucReferralPendingList = new UCReferralPendingList();
            }          
            _ReferralSentRequestPanel.Child = ucReferralPendingList;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowSendReferralPopup();
        }

        void ucSendReferralRequestPanel_SwitchToSendReferral(object sender, EventArgs e)
        {
            LoadSendReferral();
        }       

        void ucSendReferral_SwitchToSendReferralRequest(object sender, EventArgs e)
        {
            LoadSendReferralRequest();
        }

        public void LoadSendReferralRequest()
        {
            if (ucSendReferralRequestPanel == null)
            {
                ucSendReferralRequestPanel = new UCSendReferralRequestPanel();
            }
            ucSendReferralRequestPanel.SwitchToSendReferral -= ucSendReferralRequestPanel_SwitchToSendReferral;
            ucSendReferralRequestPanel.SwitchToSendReferral += ucSendReferralRequestPanel_SwitchToSendReferral;
            _ReferralControlPanel.Child = ucSendReferralRequestPanel;
        }

        private void LoadSendReferral()
        {
            if (ucSendReferral == null)
            {
                ucSendReferral = new UCSendReferral();
            }
            ucSendReferral.SwitchToSendReferralRequest -= ucSendReferral_SwitchToSendReferralRequest;
            ucSendReferral.SwitchToSendReferralRequest += ucSendReferral_SwitchToSendReferralRequest;
            _ReferralControlPanel.Child = ucSendReferral;
        }
    }
}

﻿using System.Windows;
using System.Windows.Controls;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCBonusCoinStats.xaml
    /// </summary>
    public partial class UCBonusCoinStats : UserControl
    {
        public UCBonusCoinStats()
        {
            InitializeComponent();
            this.DataContext = View.Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCBonusCoinStats_Loaded;
        }

        void UCBonusCoinStats_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_GridPanel);
        }
    }
}

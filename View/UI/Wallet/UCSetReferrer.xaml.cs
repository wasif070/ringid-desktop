﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility.FriendList;
using View.ViewModel;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCSetReferrer.xaml
    /// </summary>
    public partial class UCSetReferrer : UserControl
    {
        private Timer SearchTextBoxTimer;
        private int ThreadMaxRunningTime = 0;

        public UCSetReferrer()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCSetReferrer_Loaded;
            this.Unloaded += UCSetReferrer_Unloaded;
        }

        void UCSetReferrer_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimate(_GridPanel);
            _TextboxReferrerNumber.Clear();
            _TextboxReferrerNumber.Focus();
            SearchTextBoxTimer = new Timer();
            _BtnCancel.Click += btnCancel_Click;
            _BtnCloseSearch.Click += btnCloseSearch_Click;
            _ReferralPopup.ItemSelectionHandler += referralPopup_ItemSelectionHandler;
            _GridPanel.MouseLeftButtonDown += _GridPanel_MouseLeftButtonDown;
            _GridPanel.PreviewKeyDown += _GridPanel_PreviewKeyDown;

            Utility.Wallet.WalletViewModel.Instance.SetReferrerErrorMessage = string.Empty;
            Utility.Wallet.WalletViewModel.Instance.SelectedReferrer = null;
           

        }

        void _GridPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        void _GridPanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            }
        }

        void UCSetReferrer_Unloaded(object sender, RoutedEventArgs e)
        {
            _BtnCancel.Click -= btnCancel_Click;
            _BtnCloseSearch.Click -= btnCloseSearch_Click;
            _ReferralPopup.ItemSelectionHandler -= referralPopup_ItemSelectionHandler;
            _TextboxReferrerNumber.Clear();
            _GridPanel.MouseLeftButtonDown -= _GridPanel_MouseLeftButtonDown;
            _GridPanel.PreviewKeyDown -= _GridPanel_PreviewKeyDown;

            Utility.Wallet.WalletViewModel.Instance.SetReferrerErrorMessage = string.Empty;
            Utility.Wallet.WalletViewModel.Instance.SelectedReferrer = null;
        }

        private string _SearchString;
        public string SearchString
        {
            get { return _SearchString; }
            set
            {
                _SearchString = value;
                //set default params
                SetDefaultParameter();
                if (!string.IsNullOrWhiteSpace(_SearchString))
                {
                    if (!_ReferralPopup.IsOpen)
                    {
                        _ReferralPopup.Show(_TextboxReferrerNumber);
                    }
                    OnSearch();
                }
                else
                {
                    ThreadLeftSearchFriend.searchParm = null;
                    if (!_ReferralPopup.IsOpen)
                    {
                        _ReferralPopup.Show(_TextboxReferrerNumber);
                    }
                    ClearTempList();
                    _ReferralPopup.Hide();
                    Utility.Wallet.WalletViewModel.Instance.SetReferrerErrorMessage = string.Empty;
                }
            }
        }

        private void SetDefaultParameter()
        {
            ThreadLeftSearchFriend.processingList = false;
            ThreadLeftSearchFriend.SearchForShowMore = false;
            ThreadLeftSearchFriend.totalSearchCount = 0;
            ThreadLeftSearchFriend.friendCountInOneRequest = 0;
            if (_ReferralPopup != null)
            {
                _ReferralPopup.HideShowMorePanel();
                _ReferralPopup.NoResultsFound = false;
            }
        }

        void SearchTextBoxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                if (string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString) && !ThreadLeftSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        //ucFriendSearchListPanel.Scroll.ScrollToHome();
                        //ucReferralSearchListPanel.Scroll.ScrollToHome();
                        _ReferralPopup._Scroll.ScrollToHome();
                    });
                    ThreadLeftSearchFriend.searchParm = SearchString.ToLower();

                    View.Utility.MainSwitcher.ThreadManager().LeftSearchFriend.StartThread(ThreadLeftSearchFriend.SearchFromLeftPanel, StatusConstants.SEARCH_FRIENDLIST);
                }
            }
            else
            {
                ClearTempListFromTimer();
            }
            ThreadMaxRunningTime += 500;
            if (ThreadMaxRunningTime >= 3000)
            {
                ThreadMaxRunningTime = 0;
                SearchTextBoxTimer.Enabled = false;
                SearchTextBoxTimer.Stop();
            }
        }

        private void ClearTempListFromTimer()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                lock (RingIDViewModel.Instance.TempFriendSearchList)
                {
                    RingIDViewModel.Instance.TempFriendSearchList.Clear();
                }
            });
        }

        private void OnSearch()
        {
            try
            {
                if (string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString) && !ThreadLeftSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    ClearTempList();
                    ThreadMaxRunningTime = 0;
                    SearchTextBoxTimer.Elapsed -= SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Elapsed += SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Interval = 500;
                    SearchTextBoxTimer.AutoReset = true;
                    SearchTextBoxTimer.Enabled = true;
                    SearchTextBoxTimer.Stop();
                    SearchTextBoxTimer.Start();
                }
            }
            catch (Exception)
            {
                //log.Error(ex.StackTrace + " " + ex.Message);
            }
        }
        private void ClearTempList()
        {
            if (RingIDViewModel.Instance.TempFriendSearchList != null)
                RingIDViewModel.Instance.TempFriendSearchList.Clear();
        }

        void referralPopup_ItemSelectionHandler(BindingModels.UserBasicInfoModel selectedItem)
        {
            View.Utility.Wallet.WalletViewModel.Instance.SelectedReferrer = selectedItem;

            _TextboxReferrerNumber.Text = View.Utility.Wallet.WalletViewModel.Instance.SelectedReferrer.ShortInfoModel.FullName;

            ClearTempList();
            _ReferralPopup.Hide();
        }

        void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            //this.OnSwitchToSendReferralRequest();
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        private void btnCloseSearch_Click(object sender, RoutedEventArgs e)
        {
            if (_ReferralPopup.IsOpen)
            {
                ClearTempList();
                _ReferralPopup.Hide();
            }
            _TextboxReferrerNumber.Clear();
            _TextboxReferrerNumber.Focus();
            if (View.Utility.Wallet.WalletViewModel.Instance.SelectedReferrer != null)
            {
                View.Utility.Wallet.WalletViewModel.Instance.SelectedReferrer = null;
            }
        }
    }
}

﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.Utility;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCCoinsPanel.xaml
    /// </summary>
    public partial class UCCoinsPanel : UserControl
    {
        private UCMyCoinsStat myCoinsStat;
        private UCCoinTransfer coinTransfer;
        //private UCBonusCoinStats bonusCoinStats;
        //private UCCoinCurrencyExchangeStats coinCurrencyExchangeStats;
        private UCEarnFreeCoin earnFreeCoin;
        public UCCoinsPanel()
        {
            InitializeComponent();
            this.DataContext = this;

            this.Loaded += UCCoinsPanel_Loaded;
        }

        void UCCoinsPanel_Loaded(object sender, RoutedEventArgs e)
        {
            //if (!Utility.Wallet.WalletViewModel.Instance.IsCoinStatsRequested)
            //{
                Utility.Wallet.WalletViewModel.Instance.IsCoinStatsLoading = true;
                Utility.Wallet.WalletViewModel.Instance.CoinStatsError = false;
                new Utility.Wallet.ThreadGetWalletInformation().StartThread();
            //}
                MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_BuyCoinsPanel);
            LoadCoinStat();
            //LoadBonusCoinStat();
            //LoadCoinCurrencyExchangeStat();
            //LoadBuyCoinPanel();
            LoadEarnCoinPanel();            
        }

        //private void LoadBuyCoinPanel()
        //{
        //    //throw new NotImplementedException();
        //}

        private void LoadEarnCoinPanel()
        {
            if (earnFreeCoin == null)
            {
                earnFreeCoin = new UCEarnFreeCoin();
            }
            _EarnFreeCoinsPanel.Child = earnFreeCoin;
        }

        //void LoadCoinCurrencyExchangeStat()
        //{
        //    if (coinCurrencyExchangeStats == null)
        //    {
        //        coinCurrencyExchangeStats = new UCCoinCurrencyExchangeStats();
        //    }
        //    _CoinExchangeRatePanel.Child = coinCurrencyExchangeStats;
        //}

        //void LoadBonusCoinStat()
        //{
        //    if (bonusCoinStats == null)
        //    {
        //        bonusCoinStats = new UCBonusCoinStats();
        //    }
        //    _BonusCoinsPanel.Child = bonusCoinStats;
        //}

        public void LoadCoinStat()
        {
            if (myCoinsStat == null)
            {
                myCoinsStat = new UCMyCoinsStat();
            }
            myCoinsStat.SwitchToTransferPanelRequested -= ucMyCoinsStat_SwitchToTransferPanelRequested;
            myCoinsStat.SwitchToTransferPanelRequested += ucMyCoinsStat_SwitchToTransferPanelRequested;
            _MyCoinsPanel.Child = myCoinsStat;
            _MyCoinsPanel.Height = myCoinsStat.Height;
        }

        void ucMyCoinsStat_SwitchToTransferPanelRequested(object sender, EventArgs e)
        {
            if (coinTransfer == null)
            {
                coinTransfer = new UCCoinTransfer();
            }
            coinTransfer.SwitchToMyCoinStatPanelRequested -= ucCoinTransfer_SwitchToMyCoinStatPanelRequested;
            coinTransfer.SwitchToMyCoinStatPanelRequested += ucCoinTransfer_SwitchToMyCoinStatPanelRequested;
            _MyCoinsPanel.Child = coinTransfer;
            _MyCoinsPanel.Height = coinTransfer.Height;
        }

        void ucCoinTransfer_SwitchToMyCoinStatPanelRequested(object sender, EventArgs e)
        {
            LoadCoinStat();
        }

        private void btnCoinExchange_Click(object sender, RoutedEventArgs e)
        {
           // View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowCoinExchangePopup();
        }

        private void ButtonTransfer_Click(object sender, RoutedEventArgs e)
        {
           // View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowCoinTransferPopup();
        }

        private void ButtonExchange_Click(object sender, RoutedEventArgs e)
        {
           // View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowCoinCurrencyExchangePopup();
        }

        private ICommand _CoinExchangeCommand;
        public ICommand CoinExchangeCommand
        {
            get 
            {
                _CoinExchangeCommand = _CoinExchangeCommand ?? new RelayCommand(param => OnCoinExchangeClicked());        
                return _CoinExchangeCommand; 
            }
        }

        private void OnCoinExchangeClicked()
        {
           // View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowCoinExchangePopup();
        }

    }
}

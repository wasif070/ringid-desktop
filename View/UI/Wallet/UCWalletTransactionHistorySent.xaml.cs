﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletTransactionHistorySent.xaml
    /// </summary>
    public partial class UCWalletTransactionHistorySent : UserControl
    {
        public UCWalletTransactionHistorySent()
        {
            InitializeComponent();
            DataContext = Utility.Wallet.WalletViewModel.Instance;
            Loaded += UCWalletTransactionHistorySent_Loaded;
            Unloaded += UCWalletTransactionHistorySent_Unloaded;
        }

        void UCWalletTransactionHistorySent_Unloaded(object sender, RoutedEventArgs e)
        {
            //Utility.Wallet.WalletViewModel.Instance.IsSentLoading = false;
            //Utility.Wallet.WalletViewModel.Instance.SentNetworkError = false;
        }

        void UCWalletTransactionHistorySent_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(sentPanel);
            //Utility.Wallet.WalletViewModel.Instance.IsSentLoading = true;
            //new Utility.Wallet.ThreadGetWalletTransactionHistory().StartSentTransactionHistory();
        }
    }
}

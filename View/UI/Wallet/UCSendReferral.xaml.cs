﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility.FriendList;
using View.ViewModel;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCSendReferral.xaml
    /// </summary>
    public partial class UCSendReferral : UserControl
    {
        public event EventHandler SwitchToSendReferralRequest;
        private void OnSwitchToSendReferralRequest()
        {
            var handler = SwitchToSendReferralRequest;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
        //public UCReferralSearchListPanel ucReferralSearchListPanel;
        private Timer SearchTextBoxTimer;
        private int ThreadMaxRunningTime = 0;

        public UCSendReferral()
        {
            InitializeComponent();
            this.Loaded += UCSendReferral_Loaded;
            this.Unloaded += UCSendReferral_Unloaded;
            this.DataContext = this;
        }

        void UCSendReferral_Loaded(object sender, RoutedEventArgs e)
        {
            View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimateHorizontal(_BorderPanel);
            
            _TextboxReferralNumber.Clear();
            _TextboxReferralNumber.Focus();
            SearchTextBoxTimer = new Timer();
            _BtnCancel.Click += btnCancel_Click;
            _BtnCloseSearch.Click += btnCloseSearch_Click;
            _ReferralPopup.ItemSelectionHandler += referralPopup_ItemSelectionHandler;

            //Utility.Wallet.WalletViewModel.Instance.SendReferralErrorMessage = string.Empty;
            //Utility.Wallet.WalletViewModel.Instance.SelectedReferral = null;

           // Utility.Wallet.WalletViewModel.Instance.SendReferralToggleHandler += instance_SendReferralToggleHandler;
        }

        private void instance_SendReferralToggleHandler(bool enable)
        {
            _BorderPanel.IsEnabled = enable;
        }

        void UCSendReferral_Unloaded(object sender, RoutedEventArgs e)
        {
            _BtnCancel.Click -= btnCancel_Click;
            _BtnCloseSearch.Click -= btnCloseSearch_Click;
            _ReferralPopup.ItemSelectionHandler -= referralPopup_ItemSelectionHandler;
            _TextboxReferralNumber.Clear();
            
            //Utility.Wallet.WalletViewModel.Instance.SendReferralErrorMessage = string.Empty;
            //View.Utility.Wallet.WalletViewModel.Instance.SelectedReferral = null;

          //  Utility.Wallet.WalletViewModel.Instance.SendReferralToggleHandler -= instance_SendReferralToggleHandler;
        }

        private string _SearchString;
        public string SearchString
        {
            get { return _SearchString; }
            set
            {
                _SearchString = value;
                //set default params
                SetDefaultParameter();
                if (!string.IsNullOrWhiteSpace(_SearchString))
                {
                    if (!_ReferralPopup.IsOpen)
                    {
                        _ReferralPopup.Show(_TextboxReferralNumber);
                    }
                    OnSearch();
                }
                else
                {
                    ThreadLeftSearchFriend.searchParm = null;
                    if (!_ReferralPopup.IsOpen)
                    {
                        _ReferralPopup.Show(_TextboxReferralNumber);
                    }
                    ClearTempList();
                    _ReferralPopup.Hide();
                    //Utility.Wallet.WalletViewModel.Instance.SendReferralErrorMessage = string.Empty;
                }
            }
        }

        private void SetDefaultParameter()
        {
            ThreadLeftSearchFriend.processingList = false;
            ThreadLeftSearchFriend.SearchForShowMore = false;
            ThreadLeftSearchFriend.totalSearchCount = 0;
            ThreadLeftSearchFriend.friendCountInOneRequest = 0;
            if (_ReferralPopup != null)
            {
                _ReferralPopup.HideShowMorePanel();
                _ReferralPopup.NoResultsFound = false;
            }
        }

        void SearchTextBoxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                if (string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString) && !ThreadLeftSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        //ucFriendSearchListPanel.Scroll.ScrollToHome();
                        //ucReferralSearchListPanel.Scroll.ScrollToHome();
                        _ReferralPopup._Scroll.ScrollToHome();
                    });
                    ThreadLeftSearchFriend.searchParm = SearchString.ToLower();
                    
                    View.Utility.MainSwitcher.ThreadManager().LeftSearchFriend.StartThread(ThreadLeftSearchFriend.SearchFromLeftPanel, StatusConstants.SEARCH_FRIENDLIST);
                }
            }
            else
            {
                ClearTempListFromTimer();
            }
            ThreadMaxRunningTime += 500;
            if (ThreadMaxRunningTime >= 3000)
            {
                ThreadMaxRunningTime = 0;
                SearchTextBoxTimer.Enabled = false;
                SearchTextBoxTimer.Stop();
            }
        }

        private void ClearTempListFromTimer()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                lock (RingIDViewModel.Instance.TempFriendSearchList)
                {
                    RingIDViewModel.Instance.TempFriendSearchList.Clear();
                }
            });
        }

        private void OnSearch()
        {
            try
            {
                if (string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString) && !ThreadLeftSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    ClearTempList();
                    ThreadMaxRunningTime = 0;
                    SearchTextBoxTimer.Elapsed -= SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Elapsed += SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Interval = 500;
                    SearchTextBoxTimer.AutoReset = true;
                    SearchTextBoxTimer.Enabled = true;
                    SearchTextBoxTimer.Stop();
                    SearchTextBoxTimer.Start();
                }
            }
            catch (Exception ex)
            {
                //log.Error(ex.StackTrace + " " + ex.Message);
            }
        }
        private void ClearTempList()
        {
            if (RingIDViewModel.Instance.TempFriendSearchList != null)
                RingIDViewModel.Instance.TempFriendSearchList.Clear();
        }        

        void referralPopup_ItemSelectionHandler(BindingModels.UserBasicInfoModel selectedItem)
        {
            //View.Utility.Wallet.WalletViewModel.Instance.SelectedReferral = selectedItem;            

            //_TextboxReferralNumber.Text = View.Utility.Wallet.WalletViewModel.Instance.SelectedReferral.ShortInfoModel.FullName;

            ClearTempList();
            _ReferralPopup.Hide();
        }

        void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.OnSwitchToSendReferralRequest();
        }        

        private void btnCloseSearch_Click(object sender, RoutedEventArgs e)
        {
            if (_ReferralPopup.IsOpen)
            {
                ClearTempList();
                _ReferralPopup.Hide();                
            }
            _TextboxReferralNumber.Clear();            
            _TextboxReferralNumber.Focus();
            //if (View.Utility.Wallet.WalletViewModel.Instance.SelectedReferral != null)
            //{
            //    View.Utility.Wallet.WalletViewModel.Instance.SelectedReferral = null;
            //}
        }
    }
}

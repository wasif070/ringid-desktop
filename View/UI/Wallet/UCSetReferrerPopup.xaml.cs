﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.FriendList;
using View.ViewModel;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCSetReferrerPopup.xaml
    /// </summary>
    public partial class UCSetReferrerPopup : UserControl, INotifyPropertyChanged
    {
        #region Properties
        private ObservableCollection<UserBasicInfoModel> mainFriendList = new ObservableCollection<UserBasicInfoModel>();
        private ObservableCollection<UserBasicInfoModel> tempFriendList = new ObservableCollection<UserBasicInfoModel>();

        private ObservableCollection<UserBasicInfoModel> friendList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> FriendList
        {
            get { return friendList; }
            set { friendList = value; this.onPropertyChanged("FriendList"); }
        }

        #endregion        

        public UCSetReferrerPopup()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCSetReferrerPopup_Loaded;
            this.Unloaded += UCSetReferrerPopup_Unloaded;
        }

        void UCSetReferrerPopup_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_GridPanel);
            Utility.Wallet.WalletViewModel.Instance.SetReferrerErrorMessage = string.Empty;
            Utility.Wallet.WalletViewModel.Instance.SelectedReferrer = null;
            loadFriendList();
            _SearchTermTextBox.TextChanged += _SearchTermTextBox_TextChanged;
            _SearchTermTextBox.Focus();
            _btnClose.Click += _btnClose_Click;
            _GridPanel.PreviewKeyDown += _GridPanel_PreviewKeyDown;

        }

        

        void UCSetReferrerPopup_Unloaded(object sender, RoutedEventArgs e)
        {
            UserBasicInfoModel checkedModel = FriendList.Where(x => x.ShortInfoModel.IsVisibleInTagList).FirstOrDefault();
            if (checkedModel != null)
            {
                checkedModel.ShortInfoModel.IsVisibleInTagList = false;
            }
            Utility.Wallet.WalletViewModel.Instance.SetReferrerErrorMessage = string.Empty;
            Utility.Wallet.WalletViewModel.Instance.SelectedReferrer = null;
            _SearchTermTextBox.TextChanged -= _SearchTermTextBox_TextChanged;
            _GridPanel.PreviewKeyDown -= _GridPanel_PreviewKeyDown;
            _btnClose.Click -= _btnClose_Click;
            tempFriendList.Clear();
            mainFriendList.Clear();
            FriendList.Clear();
        }

        void _GridPanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Escape))
            {
                _btnClose_Click(null, null);
            }
        }

        void _btnClose_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        void _SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_SearchTermTextBox.Text.Trim()))
            {
                string txt = _SearchTermTextBox.Text.Trim();
                List<UserBasicInfoModel> list = (from j in mainFriendList where j.ShortInfoModel.FullName.ToLower().Contains(txt.ToLower()) || j.ShortInfoModel.UserIdentity.ToString().Contains(txt) select j).ToList();
                ObservableCollection<UserBasicInfoModel> tempList = new ObservableCollection<UserBasicInfoModel>();
                foreach (UserBasicInfoModel model in list)
                {
                    tempList.Add(model);
                }
                FriendList = tempList;
            }
            else
            {
                FriendList = mainFriendList;
            }
        }

        private void loadFriendList()
        {
            foreach (var model in RingIDViewModel.Instance.FavouriteFriendList)
            {
                model.VisibilityModel.IsAddGroupMember = false;
                mainFriendList.Add(model);
            }
            foreach (var model in RingIDViewModel.Instance.TopFriendList)
            {
                model.VisibilityModel.IsAddGroupMember = false;
                mainFriendList.Add(model);
            }
            foreach (UserBasicInfoDTO userDTO in FriendListController.Instance.FriendDataContainer.NonFavoriteList)
            {
                UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                model.VisibilityModel.IsAddGroupMember = false;
                mainFriendList.Add(model);
            }
            mainFriendList = new ObservableCollection<UserBasicInfoModel>(from i in mainFriendList orderby i.ShortInfoModel.FullName select i);

            FriendList = mainFriendList;
        }

        private ICommand onSelectCommand;
        public ICommand OnSelectCommand
        {
            get { onSelectCommand = onSelectCommand ?? new RelayCommand(param => onSelectClicked(param)); return onSelectCommand; }
        //    set { onSelectCommand = value; }
        }

        private void onSelectClicked(object param)
        {
            UserBasicInfoModel model = param as UserBasicInfoModel;
            UserBasicInfoModel previousModel = FriendList.Where(x => x.ShortInfoModel.UserTableID != model.ShortInfoModel.UserTableID && x.ShortInfoModel.IsVisibleInTagList).FirstOrDefault();
            if (previousModel != null)
            {
                previousModel.ShortInfoModel.IsVisibleInTagList = false;
            }
            model.ShortInfoModel.IsVisibleInTagList = !model.ShortInfoModel.IsVisibleInTagList;
            Utility.Wallet.WalletViewModel.Instance.OnCheckClicked(model);
            
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void onPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

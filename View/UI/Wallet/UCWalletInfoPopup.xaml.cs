﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletInfoPopup.xaml
    /// </summary>
    public partial class UCWalletInfoPopup : UserControl
    {
        //private WebBrowser browser;
        public UCWalletInfoPopup()
        {
            InitializeComponent();
            this.Loaded += UCWalletInfoPopup_Loaded;
            this.Unloaded += UCWalletInfoPopup_Unloaded;
        }        

        void UCWalletInfoPopup_Loaded(object sender, RoutedEventArgs e)
        {
            _GridPanel.MouseLeftButtonDown += _GridPanel_MouseLeftButtonDown;
            //https://www.youtube.com/watch?v=tu-7_SJySn8
            _BtnClose.Click += _BtnClose_Click;
            //string link = "https://www.youtube.com/embed/tu-7_SJySn8";
            //_Browser.Navigate(link);
            _Browser.Navigate("https://www.ringid.com/wallet-privacy-mobile.html");
        }

        void _GridPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        void UCWalletInfoPopup_Unloaded(object sender, RoutedEventArgs e)
        {
            _BtnClose.Click -= _BtnClose_Click;
            _GridPanel.MouseLeftButtonDown -= _GridPanel_MouseLeftButtonDown;
        }

        void _BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }
    }
}

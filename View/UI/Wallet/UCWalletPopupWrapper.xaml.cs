﻿using Models.Constants;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using View.UI.PopUp;
using View.Utility;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletPopupWrapper.xaml
    /// </summary>
    public partial class UCWalletPopupWrapper : UserControl
    {
        
        UCCoinBundlePanel ucCoinBundlePanel = null;
        UCAddReferrer addReferrerMessage = null;
        UCSetReferrerPopup setReferrerPopup = null;
        UCReferralList referralList = null;
        UCCheckInSuccessPanel checkInSuccessPanel = null;
        UCInviteFromWallet invitePanel = null;
        UCCheckInFailPanel checkInFailedPanel = null;
        UCWalletCoinRechargeMethods coinRechargeMethods = null;
        UCWalletMediaSharePopup walletMediaSharePopup = null;
        UCWalletInfoPopup walletInfoPopup = null;

        #region <Deprecated>
        //UCCoinCurrenceExchangeRate _UCCoinCurrenceExchangeRate = null;
        //UCCoinExchange _UCCoinExchange = null;
        //UCCoinTransfer _UCCoinTransfer = null;
        //UCSendReferral _UCSendReferral = null;
        //UCWalletPinNumber _UCWalletPinNumber = null;
        //UCWalletWelcomeScreen _UCWalletWelcomeScreen = null;
        //public UCDigitsCustomMsgPopup ucDigitsCustomMsgPopup = null;
        //UCAcceptedReferralList _UCAcceptedReferralList = null;
        //UCSetReferrer setReferrer = null;
        //UCDailyCheckIn dailyCheckIn = null;
        //UCMediaShareViaWallet shareMediaViaWallet = null;
        #endregion

        public UIElement _parent;
        public UserControl userControl = new UserControl();

        public UCWalletPopupWrapper()
        {
            InitializeComponent();
            this.DataContext = this;
            walletPopupPanel.Child = userControl;
            userControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            userControl.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            this.Loaded += UCWalletPopupWrapper_Loaded;
            this.Unloaded += UCWalletPopupWrapper_Unloaded;
            
        }

        void UCWalletPopupWrapper_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Escape))
            {
                this.HidePopup();
            }
        }

        void UCWalletPopupWrapper_Unloaded(object sender, RoutedEventArgs e)
        {            
        }

        void UCWalletPopupWrapper_Loaded(object sender, RoutedEventArgs e)
        {
            if (userControl.Content == null)
            {
                this.HidePopup();
            }
        }

        public void SetParent(UIElement parent)
        {
            _parent = parent;
        }

        public void ShowPopup(int popupType = 0)
        {
            MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
            switch (popupType)
            {
                case WalletConstants.PU_COIN_RECHARGE_METHODS:
                    coinRechargeMethods = new UCWalletCoinRechargeMethods();
                    userControl.Content = coinRechargeMethods;
                    break;
                case WalletConstants.PU_COIN_BUNDLE_PANEL:
                    ucCoinBundlePanel = new UCCoinBundlePanel();
                    userControl.Content = ucCoinBundlePanel;
                    break;
                case WalletConstants.PU_ADD_REFERRER:
                    addReferrerMessage = new UCAddReferrer();
                    userControl.Content = addReferrerMessage;
                    break;
                case WalletConstants.PU_SET_REFERRER:
                    setReferrerPopup = new UCSetReferrerPopup();
                    userControl.Content = setReferrerPopup;
                    break;
                case WalletConstants.PU_REFERRAL_LIST:
                    referralList = new UCReferralList();
                    userControl.Content = referralList;
                    break;
                case WalletConstants.PU_CHECK_IN_SUCCESS:
                    checkInSuccessPanel = new UCCheckInSuccessPanel();
                    userControl.Content = checkInSuccessPanel;
                    break;
                case WalletConstants.PU_CHECK_IN_FAIL:
                    checkInFailedPanel = new UCCheckInFailPanel();
                    userControl.Content = checkInFailedPanel;
                    break;
                case WalletConstants.PU_INVITE:
                    invitePanel = new UCInviteFromWallet();
                    userControl.Content = invitePanel;
                    break;                
                case WalletConstants.PU_MEDIA_SHARE_MESSAGE:
                    walletMediaSharePopup = new UCWalletMediaSharePopup();
                    userControl.Content = walletMediaSharePopup;
                    break;
                case WalletConstants.PU_WALLET_INFO:
                    walletInfoPopup = new UCWalletInfoPopup();
                    userControl.Content = walletInfoPopup;
                    break;
                default:
                    userControl.Content = null;
                    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
                    break;
            }
        }

        public void HidePopup()
        {
            if (userControl.Content is UCCoinBundlePanel)
            {
                ucCoinBundlePanel = null;
            }
            else if (userControl.Content is UCAddReferrer)
            {
                addReferrerMessage = null;
            }            
            else if(userControl.Content is UCSetReferrerPopup)
            {
                setReferrerPopup = null;
            }
            else if (userControl.Content is UCReferralList)
            {
                referralList = null;
            }            
            else if (userControl.Content is UCCheckInSuccessPanel)
            {
                checkInSuccessPanel = null;
            }
            else if (userControl.Content is UCInviteFromWallet)
            {
                invitePanel = null;
            }
            else if (userControl.Content is UCCheckInFailPanel)
            {
                checkInFailedPanel = null;
            }
            else if (userControl.Content is UCWalletCoinRechargeMethods)
            {
                coinRechargeMethods = null;
            }            
            else if (userControl.Content is UCWalletMediaSharePopup)
            {
                walletMediaSharePopup = null;
            }
            else if(userControl.Content is UCWalletInfoPopup)
            {
                walletInfoPopup = null;
            }
            userControl.Content = null;
            this.Visibility = Visibility.Hidden;

            #region <Deprecated>
            //if (userControl.Content is UCCoinExchange)
            //{
            //    _UCCoinExchange = null;
            //}
            //else if (userControl.Content is UCCoinTransfer)
            //{
            //    _UCCoinTransfer = null;
            //}
            //else if (userControl.Content is UCCoinCurrenceExchangeRate)
            //{
            //    _UCCoinCurrenceExchangeRate = null;
            //}
            //else if (userControl.Content is UCSendReferral)
            //{
            //    _UCSendReferral = null;
            //}
            //else if (userControl.Content is UCWalletPinNumber)
            //{
            //    _UCWalletPinNumber = null;
            //}
            //else if (userControl.Content is UCWalletWelcomeScreen)
            //{
            //    _UCWalletWelcomeScreen = null;
            //}
            //else if (userControl.Content is UCDigitsCustomMsgPopup)
            //{
            //    ucDigitsCustomMsgPopup.Hide();
            //}
            //else if (userControl.Content is UCAcceptedReferralList)
            //{
            //    _UCAcceptedReferralList = null;
            //}
            //else if (userControl.Content is UCSetReferrer)
            //{
            //    setReferrer = null;
            //}
            //else if (userControl.Content is UCDailyCheckIn)
            //{
            //    dailyCheckIn = null;
            //}
            //else if (userControl.Content is UCMediaShareViaWallet)
            //{
            //    shareMediaViaWallet = null;
            //}
            #endregion
        }       

        private void walletPopupPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.HidePopup();
        }

        public void UCPopupFadeIn(UIElement control)
        {
            System.Windows.Media.Animation.DoubleAnimation myAnimation = new System.Windows.Media.Animation.DoubleAnimation
            {
                From = 0.0,
                To = 1.0,
                Duration = TimeSpan.FromMilliseconds(750),
            };
            var transform = new ScaleTransform();
            control.RenderTransform = transform;
            //transform.BeginAnimation(ScaleTransform.Identity, myAnimation);
            transform.BeginAnimation(ScaleTransform.CenterYProperty, myAnimation);
        }

        public void UCPopupAnimate(UIElement control)
        {
            System.Windows.Media.Animation.DoubleAnimation myAnimation = new System.Windows.Media.Animation.DoubleAnimation
            {
                From = 350,
                To = 0,
                Duration = TimeSpan.FromMilliseconds(250),
            };
            var transform = new TranslateTransform();
            control.RenderTransform = transform;
            transform.BeginAnimation(TranslateTransform.YProperty, myAnimation);
        }

        public void UCPopupAnimateHorizontal(UIElement control)
        {
            System.Windows.Media.Animation.DoubleAnimation myAnimation = new System.Windows.Media.Animation.DoubleAnimation
            {
                From = 350,
                To = 0,
                Duration = TimeSpan.FromMilliseconds(250),
            };
            var transform = new TranslateTransform();
            control.RenderTransform = transform;
            transform.BeginAnimation(TranslateTransform.XProperty, myAnimation);
        }

        public void UCControlAnimateFromLeft(UIElement control, long durationInMilliseconds = 250)
        {
            System.Windows.Media.Animation.DoubleAnimation myAnimation = new System.Windows.Media.Animation.DoubleAnimation
            {
                From = -350,
                To = 0,
                Duration = TimeSpan.FromMilliseconds(durationInMilliseconds),
            };
            var transform = new TranslateTransform();
            control.RenderTransform = transform;
            transform.BeginAnimation(TranslateTransform.XProperty, myAnimation);
        }

        #region <Deprecated>
        //public void ShowShareMedia()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (shareMediaViaWallet == null)
        //    {
        //        shareMediaViaWallet = new UCMediaShareViaWallet();
        //    }
        //    userControl.Content = shareMediaViaWallet;
        //}

        //public void ShowCoinRechargeMethods()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (coinRechargeMethods == null)
        //    {
        //        coinRechargeMethods = new UCWalletCoinRechargeMethods();
        //    }
        //    userControl.Content = coinRechargeMethods;
        //}

        //public void ShowInvitePanel()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (invitePanel == null)
        //    {
        //        invitePanel = new UCInviteFromWallet();
        //    }
        //    userControl.Content = invitePanel;
        //}

        //public void ShowCheckInSuccessPanel()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (checkInSuccessPanel == null)
        //    {
        //        checkInSuccessPanel = new UCCheckInSuccessPanel();
        //    }
        //    userControl.Content = checkInSuccessPanel;
        //}

        //public void ShowDailyCheckIn()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (dailyCheckIn == null)
        //    {
        //        dailyCheckIn = new UCDailyCheckIn();
        //    }
        //    userControl.Content = dailyCheckIn;
        //}

        //public void ShowReferralList()
        //{
        //    //Utility.Wallet.WalletViewModel.Instance.IsMemberListLoading = true;
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (referralList == null)
        //    {
        //        referralList = new UCReferralList();
        //    }
        //    userControl.Content = referralList;
        //}

        //public void ShowSetReferrer()
        //{
        //    //Utility.Wallet.WalletViewModel.Instance.IsMemberListLoading = true;
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (setReferrer == null)
        //    {
        //        setReferrer = new UCSetReferrer();
        //    }
        //    userControl.Content = setReferrer;
        //}

        //public void ShowAcceptedReferralList()
        //{
        //    Utility.Wallet.WalletViewModel.Instance.IsMemberListLoading = true;
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (_UCAcceptedReferralList == null)
        //    {
        //        _UCAcceptedReferralList = new UCAcceptedReferralList();
        //    }
        //    userControl.Content = _UCAcceptedReferralList;
        //}

        //public void ShowAddReferrerMessage()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (addReferrerMessage == null)
        //    {
        //        addReferrerMessage = new UCAddReferrer();
        //    }
        //    userControl.Content = addReferrerMessage;
        //}

        //public void ShowDigitsCustomPopup()        
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (ucDigitsCustomMsgPopup == null)
        //    {
        //        ucDigitsCustomMsgPopup = new UCDigitsCustomMsgPopup();                
        //        //MainSwitcher.PopupController.DigitsCustomMsgPopup.Show(2);
        //    }
        //    userControl.Content = ucDigitsCustomMsgPopup;
        //    ucDigitsCustomMsgPopup.Show(3);
        //}

        //public void ShowWalletWelcomeScreenPopup()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (_UCWalletWelcomeScreen == null)
        //    {
        //        _UCWalletWelcomeScreen = new UCWalletWelcomeScreen();
        //    }
        //    userControl.Content = _UCWalletWelcomeScreen;
        //}

        //public void ShowCoinCurrencyExchangePopup()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (_UCCoinCurrenceExchangeRate == null)
        //    {
        //        _UCCoinCurrenceExchangeRate = new UCCoinCurrenceExchangeRate();
        //    }
        //    userControl.Content = _UCCoinCurrenceExchangeRate;
        //}

        //public void ShowCoinExchangePopup()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (_UCCoinExchange == null)
        //    {
        //        _UCCoinExchange = new UCCoinExchange();
        //    }
        //    userControl.Content = _UCCoinExchange;
        //}

        //public void ShowCoinTransferPopup()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (_UCCoinTransfer == null)
        //    {
        //        _UCCoinTransfer = new UCCoinTransfer();
        //    }
        //    userControl.Content = _UCCoinTransfer;
        //}

        //public void ShowSendReferralPopup()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (_UCSendReferral == null)
        //    {
        //        _UCSendReferral = new UCSendReferral();
        //    }
        //    userControl.Content = _UCSendReferral;
        //}

        //public void ShowWalletPinNumber()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (_UCWalletPinNumber == null)
        //    {
        //        _UCWalletPinNumber = new UCWalletPinNumber();
        //    }
        //    userControl.Content = _UCWalletPinNumber;
        //}        

        //public void ShowBuyCoinPanel()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (ucCoinBundlePanel == null)
        //    {
        //        ucCoinBundlePanel = new UCCoinBundlePanel();
        //    }
        //    userControl.Content = ucCoinBundlePanel;
        //}

        //public void ShowCheckInFailedPanel()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (checkInFailedPanel == null)
        //    {
        //        checkInFailedPanel = new UCCheckInFailPanel();
        //    }
        //    userControl.Content = checkInFailedPanel;
        //}

        //public void ShowMediaSharePopup()
        //{
        //    MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
        //    if (walletMediaSharePopup == null)
        //    {
        //        walletMediaSharePopup = new UCWalletMediaSharePopup();
        //    }
        //    userControl.Content = walletMediaSharePopup;
        //}
        #endregion        
    
        public void ShowYoutubeViewer(string videoID)
        {
            MainSwitcher.PopupController.WalletPopupWrapper.Visibility = System.Windows.Visibility.Visible;
            userControl.Content = new View.UI.SocialMedia.UCYoutubeViewer(videoID);
        }
    }
}

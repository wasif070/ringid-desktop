﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCInviteFromWallet.xaml
    /// </summary>
    public partial class UCInviteFromWallet : UserControl
    {
        public UCInviteFromWallet()
        {
            InitializeComponent();
            this.Loaded += UCInviteFromWallet_Loaded;
            this.Unloaded += UCInviteFromWallet_Unloaded;

        }

        void UCInviteFromWallet_Unloaded(object sender, RoutedEventArgs e)
        {
            _BorderPanel.MouseLeftButtonDown -= _BorderPanel_MouseLeftButtonDown;
            _btnClose.Click -= _btnClose_Click;
            _BorderPanel.PreviewKeyDown -= _BorderPanel_PreviewKeyDown;
        }

        void UCInviteFromWallet_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_BorderPanel);
            _BorderPanel.MouseLeftButtonDown += _BorderPanel_MouseLeftButtonDown;
            _btnClose.Click += _btnClose_Click;
            _BorderPanel.PreviewKeyDown += _BorderPanel_PreviewKeyDown;
        }

        void _BorderPanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Escape))
            {
                Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            }
        }

        void _btnClose_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        void _BorderPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
    }
}

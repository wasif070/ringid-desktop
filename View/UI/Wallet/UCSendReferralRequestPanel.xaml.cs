﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCSendReferralRequestPanel.xaml
    /// </summary>
    public partial class UCSendReferralRequestPanel : UserControl
    {
        public event EventHandler SwitchToSendReferral;
        private void OnSwitchToSendReferral()
        {
            var handler = SwitchToSendReferral;
            if (handler != null) { handler(this, EventArgs.Empty); }
        }
        public UCSendReferralRequestPanel()
        {
            InitializeComponent();
            this.Loaded += UCSendReferralRequestPanel_Loaded;
            this.Unloaded += UCSendReferralRequestPanel_Unloaded;
        }

        void UCSendReferralRequestPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            _BtnReferNow.Click -= btnReferNow_Click;
        }

        void UCSendReferralRequestPanel_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_BtnReferNow);
            _BtnReferNow.Click += btnReferNow_Click;
        }

        void btnReferNow_Click(object sender, RoutedEventArgs e)
        {
            this.OnSwitchToSendReferral();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCAcceptedReferralList.xaml
    /// </summary>
    public partial class UCAcceptedReferralList : UserControl
    {
        public UCAcceptedReferralList()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCAcceptedReferralList_Loaded;
            this.Unloaded += UCAcceptedReferralList_Unloaded;
        }

        void UCAcceptedReferralList_Unloaded(object sender, RoutedEventArgs e)
        {
            //Utility.Wallet.WalletViewModel.Instance.IsPendingLoading = false;
            //Utility.Wallet.WalletViewModel.Instance.IsMemberListLoading = false;
            _BtnClose.Click -= btnClose_Click;
        }

        void UCAcceptedReferralList_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimate(_BorderPanel);
            //Utility.Wallet.WalletViewModel.Instance.IsPendingLoading = false;
            //Utility.Wallet.WalletViewModel.Instance.IsMemberListLoading = true;
            _BtnClose.Click += btnClose_Click;
        }

        void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }
    }
}

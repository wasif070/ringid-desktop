﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCCoinCurrencyExchangeStats.xaml
    /// </summary>
    public partial class UCCoinCurrencyExchangeStats : UserControl
    {
        public UCCoinCurrencyExchangeStats()
        {
            InitializeComponent();
            this.DataContext = View.Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCCoinCurrencyExchangeStats_Loaded;
            this.Unloaded += UCCoinCurrencyExchangeStats_Unloaded;
        }        

        void UCCoinCurrencyExchangeStats_Loaded(object sender, RoutedEventArgs e)
        {
         //   throw new NotImplementedException();
            _ComboBoxCurrencyExchange.SelectionChanged += comboBoxCurrencyExchange_SelectionChanged;

            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_GridPanel);
            
            //if (!View.Utility.Wallet.WalletViewModel.Instance.IsExchangeRateRequested)
            //{
            //    Utility.Wallet.WalletViewModel.Instance.IsExchangeRateLoading = true;
            //    new View.Utility.Wallet.ThreadWalletExchangeRateInformation().StartThread();
            //}
            //if (View.Utility.Wallet.WalletViewModel.Instance.ExchangeRateList.Count > 0)
            //{
            //    _ComboBoxCurrencyExchange.SelectedItem = View.Utility.Wallet.WalletViewModel.Instance.ExchangeRateList.ElementAt(0);
            //}
        }        

        void UCCoinCurrencyExchangeStats_Unloaded(object sender, RoutedEventArgs e)
        {
            //_ComboBoxCurrencyExchange.SelectionChanged -= comboBoxCurrencyExchange_SelectionChanged;
            //_ComboBoxCurrencyExchange.SelectedItem = null;
            //View.Utility.Wallet.WalletViewModel.Instance.SelectedExchangeRateModel = null;
        }

        void comboBoxCurrencyExchange_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (((WalletExchangeRateModel)((ComboBox)sender).SelectedItem) != null)
            //{
            //    View.Utility.Wallet.WalletViewModel.Instance.SelectedExchangeRateModel = ((WalletExchangeRateModel)((ComboBox)sender).SelectedValue);    
            //}            
        }
    }
}

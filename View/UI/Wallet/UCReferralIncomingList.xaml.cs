﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCReferralIncomingList.xaml
    /// </summary>
    public partial class UCReferralIncomingList : UserControl
    {
        public UCReferralIncomingList()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCReferralIncomingList_Loaded;
            this.Unloaded += UCReferralIncomingList_Unloaded;
        }

        void UCReferralIncomingList_Unloaded(object sender, RoutedEventArgs e)
        {
           // Utility.Wallet.WalletViewModel.Instance.ReferralIncomingPanelToggleHandler -= Instance_ReferralIncomingPanelToggleHandler;
        }

        void UCReferralIncomingList_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_BorderPanel);

           // Utility.Wallet.WalletViewModel.Instance.ReferralIncomingPanelToggleHandler += Instance_ReferralIncomingPanelToggleHandler;
        }

        void Instance_ReferralIncomingPanelToggleHandler(bool enable)
        {
            _BorderPanel.IsEnabled = enable;
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }
    }
}

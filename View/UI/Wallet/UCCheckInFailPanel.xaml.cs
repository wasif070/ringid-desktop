﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCCheckInFailPanel.xaml
    /// </summary>
    public partial class UCCheckInFailPanel : UserControl
    {
        System.Windows.Threading.DispatcherTimer closeTimer = null;
        public UCCheckInFailPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCCheckInFailPanel_Loaded;
            this.Unloaded += UCCheckInFailPanel_Unloaded;
        }

        void UCCheckInFailPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            if (closeTimer != null && closeTimer.IsEnabled)
            {
                closeTimer.Stop();
            }
            closeTimer = null;
            _CheckInFailedPanel.MouseLeftButtonDown -= _CheckInFailedPanel_MouseLeftButtonDown;
            _BtnClose.Click -= _BtnClose_Click;
        }

        void UCCheckInFailPanel_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimate(_CheckInFailedPanel);
            _CheckInFailedPanel.MouseLeftButtonDown += _CheckInFailedPanel_MouseLeftButtonDown;
            _BtnClose.Click += _BtnClose_Click;
            closeTimer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromSeconds(10) };
            closeTimer.Tick += ((ss, ee) =>
            {
                closeTimer.Stop();
                Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            });
            closeTimer.Start();
        }

        void _BtnClose_Click(object sender, RoutedEventArgs e)
        {
            if (closeTimer != null && closeTimer.IsEnabled)
            {
                closeTimer.Stop();
            }
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        void _CheckInFailedPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            e.Handled = true;
        }
        public int DailyCheckInCoin { get { return Utility.Wallet.WalletViewModel.Instance.CoinEarningRules.Where(x => x.RuleItemID == WalletConstants.COIN_EARNING_RULE_DAILY_CHECK_IN).First().CoinQuantity; } }
    }
}

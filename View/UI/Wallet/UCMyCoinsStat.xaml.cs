﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCMyCoinsStat.xaml
    /// </summary>
    public partial class UCMyCoinsStat : UserControl
    {
        public event EventHandler SwitchToTransferPanelRequested;
        private void OnSwitchToTransferPanelRequested()
        {
            var handler = SwitchToTransferPanelRequested;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
        public UCMyCoinsStat()
        {
            InitializeComponent();
            this.Loaded += UCMyCoinsStat_Loaded;
            this.DataContext = View.Utility.Wallet.WalletViewModel.Instance;
        }

        void UCMyCoinsStat_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_GridPanel);
        }

        private void ButtonTransfer_Click(object sender, RoutedEventArgs e)
        {
            OnSwitchToTransferPanelRequested();
        }
    }
}

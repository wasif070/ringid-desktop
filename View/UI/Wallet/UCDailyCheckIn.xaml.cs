﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCDailyCheckIn.xaml
    /// </summary>
    public partial class UCDailyCheckIn : UserControl
    {
        //private UCLoadingPanel loadingPanel = null;
        //private UCCheckInSuccessPanel checkInSuccessPanel = null;
        //private UCAlreadyCheckInPanel alreadyCheckInPanel = null;
        
        public UCDailyCheckIn()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCDailyCheckIn_Loaded;
            this.Unloaded += UCDailyCheckIn_Unloaded;
        }        

        void UCDailyCheckIn_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCPopupAnimate(_GridPanel);
            _GridPanel.MouseLeftButtonDown += _GridPanel_MouseLeftButtonDown;
            if (!Utility.Wallet.WalletViewModel.Instance.IsCheckedIn)
            {
                Utility.Wallet.WalletViewModel.Instance.CheckInLoading = true;
                new Utility.Wallet.ThreadWalletDailyUtilities().StartDailyCheckIn();
            }
            else
            {
                new Utility.Wallet.ThreadWalletDailyUtilities().StartDailyCheckInHistory();
            }
        }

        void UCDailyCheckIn_Unloaded(object sender, RoutedEventArgs e)
        {
            _GridPanel.MouseLeftButtonDown -= _GridPanel_MouseLeftButtonDown;
        }

        void _GridPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
    }
}

﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.Utility;
using View.ViewModel;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCWalletMainPanel.xaml
    /// </summary>
    public partial class UCWalletMainPanel : UserControl
    {
        public UCCoinsPanel _UCCoinsPanel = null;
        public UCReferralPanel _UCReferralPanel = null;
        public UCCoinTransactionHistory _UCCoinTransactionHistory = null;
        public UCReferrerPanel ReferrerPanel = null;
        public UCWalletMainPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            //this.Visibility = System.Windows.Visibility.Hidden;
            this.Loaded += UCWalletMainPanel_Loaded;
            this.Unloaded += UCWalletMainPanel_Unloaded;            
        }

        void UCWalletMainPanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Escape) && Utility.MainSwitcher.PopupController.WalletPopupWrapper.IsLoaded)
            {
                Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            }
        }        

        void UCWalletMainPanel_Loaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.WalletSelection = true;
            _btnWalletHelp.Click += _btnWalletHelp_Click;
            _btnSetReferrer.Click += _btnSetReferrer_Click;
            performInitialRequest();
        }

        void _btnSetReferrer_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_SET_REFERRER);
        }

        void _btnWalletHelp_Click(object sender, RoutedEventArgs e)
        {
            //Utility.HelperMethods.GoToSite("https://www.ringid.com/wallet-privacy-mobile.html");
            MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_WALLET_INFO);
        }

        private void performInitialRequest()
        {
            if (!Utility.Wallet.WalletViewModel.Instance.ReferrerResponseSuccess)
            {
                new Utility.Wallet.ThreadGetReferrerSummary().StartThread();
                DispatcherTimer referrerNetworkTimer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(250) };
                referrerNetworkTimer.Tick += ((ss, ee) =>
                {
                    if (!Utility.Wallet.WalletViewModel.Instance.ReferrerNetworkError)
                    {
                        if (Utility.Wallet.WalletViewModel.Instance.ReferrerResponseSuccess)
                        {
                            if (!Utility.Wallet.WalletViewModel.Instance.IsReferrerSkipped && !Utility.Wallet.WalletViewModel.Instance.IsReferrerSet)
                            {
                                Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_ADD_REFERRER);
                            }
                            referrerNetworkTimer.Stop();
                        }
                    }
                    else
                    {
                        referrerNetworkTimer.Stop();
                    }
                });
                referrerNetworkTimer.Start();
            }

            if (Utility.Wallet.WalletViewModel.Instance.WalletCheckInRules.Count == 0)
            {
                new Utility.Wallet.ThreadWalletDailyUtilities().StartDailyCheckInRules();
            } 
        }        

        void UCWalletMainPanel_Unloaded(object sender, RoutedEventArgs e)
        {
           // this.PreviewKeyDown -= UCWalletMainPanel_PreviewKeyDown;
            RingIDViewModel.Instance.WalletSelection = false;
            _btnWalletHelp.Click -= _btnWalletHelp_Click;
            _btnSetReferrer.Click -= _btnSetReferrer_Click;
        }

        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommandClicked(param));
                }
                return _BackCommand;
            }
        }

        public void OnBackCommandClicked(object parameter)
        {
            //tabContainerBorder.Visibility = Visibility.Visible;
            //searchContentsBdr.Visibility = Visibility.Collapsed;
            //IsTabView = true;
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (tabControl.SelectedIndex)
            {
                case 0://coins
                    if (_UCCoinsPanel == null)
                    {
                        _UCCoinsPanel = new UCCoinsPanel();
                    }
                    CoinsBorderPanel.Child = _UCCoinsPanel;
                    break;
                case 1://referral
                    //if (_UCReferralPanel == null)
                    //{
                    //    _UCReferralPanel = new UCReferralPanel();
                    //}
                    //ReferralBorderPanel.Child = _UCReferralPanel;
                    if (ReferrerPanel == null)
                    {
                        ReferrerPanel = new UCReferrerPanel();
                    }
                    ReferralBorderPanel.Child = ReferrerPanel;
                    break;
                //case 2://investment
                   // Utility.WPFMessageBox.CustomMessageBox.ShowInformation("Currently available in your area!");
                    //InvestmentBorderPanel.Child = new UCGiftPanel();
                    //InvestmentBorderPanel.Child = new UCCoinBundlePanel();
                    //InvestmentBorderPanel.Child = new UCEarnFreeCoin();
                    //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowAddReferrerMessage();
                    //InvestmentBorderPanel.Child = new UCSetReferrer();
                    //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowSetReferrer();
                    //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowReferralList();
                    //InvestmentBorderPanel.Child = new UCReferrerPanel();
                    //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowCheckInSuccessPanel();
                    //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowDailyCheckIn();
                    //InvestmentBorderPanel.Child = new UCMediaShareViaWallet();
                    //InvestmentBorderPanel.Child = new UCWalletMediaSharePopup();
                    //InvestmentBorderPanel.Child = new UCSetReferrerPopup();
                    //break;
                //case 3://payment
                    //Utility.WPFMessageBox.CustomMessageBox.ShowInformation("Coming Soon!");
                    //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowDailyCheckIn();
                    //break;
                //case 4://transaction history
                //    if (_UCCoinTransactionHistory == null)
                //    {
                //        _UCCoinTransactionHistory = new UCCoinTransactionHistory();
                //    }
                //    TransactionHistoryBorderPanel.Child = _UCCoinTransactionHistory;
                    //break;
            }
        }
    }
}

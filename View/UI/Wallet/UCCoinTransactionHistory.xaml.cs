﻿using System.Windows;
using System.Windows.Controls;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCCoinTransactionHistory.xaml
    /// </summary>
    public partial class UCCoinTransactionHistory : UserControl
    {
        UCWalletTransactionHistorySent walletTransactionHistorySent;
        UCWalletTransactionHistoryReceived walletTransactionHistoryReceived;
        UCWalletTransactionHistoryBonusMigration walletTransactionHistoryBonusMigration;
        public UCCoinTransactionHistory()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCCoinTransactionHistory_Loaded;
            this.Unloaded += UCCoinTransactionHistory_Unloaded;
        }

        void UCCoinTransactionHistory_Unloaded(object sender, RoutedEventArgs e)
        {
            _TxnHistoryType.SelectionChanged -= txnHistoryType_SelectionChanged;
        }

        void UCCoinTransactionHistory_Loaded(object sender, RoutedEventArgs e)
        {
            //Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(borderPanel);

            _TxnHistoryType.SelectionChanged += txnHistoryType_SelectionChanged;
            _TxnHistoryType.SelectedIndex = 0;
        }

        void txnHistoryType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (_TxnHistoryType.SelectedIndex)
            {
                case 0:                    
                    if (walletTransactionHistorySent == null)
                    {
                        walletTransactionHistorySent = new UCWalletTransactionHistorySent();
                    }
                    _HistoryPanel.Child = walletTransactionHistorySent;
                    break;
                case 1:                    
                    if (walletTransactionHistoryReceived == null)
                    {
                        walletTransactionHistoryReceived = new UCWalletTransactionHistoryReceived();
                    }
                    _HistoryPanel.Child = walletTransactionHistoryReceived;
                    break;
                case 2:                    
                    if (walletTransactionHistoryBonusMigration == null)
                    {
                        walletTransactionHistoryBonusMigration = new UCWalletTransactionHistoryBonusMigration();
                    }
                    _HistoryPanel.Child = walletTransactionHistoryBonusMigration;
                    break;
                default:
                    break;
            }
        }
    }
}

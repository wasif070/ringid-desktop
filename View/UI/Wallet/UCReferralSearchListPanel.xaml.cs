﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.FriendList;
using View.ViewModel;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCReferralSearchListPanel.xaml
    /// </summary>
    public partial class UCReferralSearchListPanel : UserControl, INotifyPropertyChanged
    {
        public delegate void ItemSelected(UserBasicInfoModel selectedItem);
        public event ItemSelected ItemSelectionHandler;
        public UCReferralSearchListPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCReferralSearchListPanel_Loaded;
            this.Unloaded += UCReferralSearchListPanel_Unloaded;            
        }

        void UCReferralSearchListPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            _ShowMoreButtonPanel.Click -= ShowMore_PanelClick;
            //Scroll.ScrollChanged -= Scroll_ScrollChanged;
        }

        void Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 20 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
            {
                scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 20);
            }
            else if ((e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight))
            {
                ShowMore_PanelClick(null, null);
            }
        }

        void UCReferralSearchListPanel_Loaded(object sender, RoutedEventArgs e)
        {
            
            _ShowMoreButtonPanel.Click += ShowMore_PanelClick;
            //Scroll.ScrollChanged += Scroll_ScrollChanged;
        }

        private ImageSource _Loader;
        public ImageSource Loader
        {
            get
            {
                return _Loader;
            }
            set
            {
                if (value == _Loader)
                    return;
                _Loader = value;
                OnPropertyChanged("Loader");
            }
        }

        private bool _NoResultsFound = false;
        public bool NoResultsFound
        {
            get
            {
                return _NoResultsFound;
            }
            set
            {
                if (value == _NoResultsFound)
                    return;
                if (string.IsNullOrWhiteSpace(ThreadLeftSearchFriend.searchParm)) { _NoResultsFound = false; }
                else { _NoResultsFound = value; }
                OnPropertyChanged("NoResultsFound");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void HideShowMorePanel()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                _ShowMorePanel.Visibility = Visibility.Collapsed;
            }, System.Windows.Threading.DispatcherPriority.Send);
        }
        public void ShowLoader(bool show)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (!string.IsNullOrWhiteSpace(ThreadLeftSearchFriend.searchParm) && RingIDViewModel.Instance.TempFriendSearchList.Count > 0)
                {
                    if (show)
                    {
                        //showMorePanel.Visibility = Visibility.Visible;
                        _ShowMoreLoaderBorder.Visibility = Visibility.Visible;
                        Loader = ImageObjects.LOADER_FEED;
                    }
                    else
                    {
                        _ShowMoreLoaderBorder.Visibility = Visibility.Collapsed;
                        _ShowMorePanel.Visibility = Visibility.Visible;
                        _ShowMoreButtonPanel.Visibility = Visibility.Visible;
                        if (ImageObjects.LOADER_FEED != null)
                            GC.SuppressFinalize(ImageObjects.LOADER_FEED);
                        if (Loader != null)
                            GC.SuppressFinalize(Loader);
                        ImageObjects.LOADER_FEED = null;
                        Loader = null;
                    }
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }
        private void ShowMore_PanelClick(object sender, RoutedEventArgs e)
        {
            ShowLoader(true);
            _ShowMoreButtonPanel.Visibility = Visibility.Collapsed;
            ThreadLeftSearchFriend.SearchForShowMore = true;
            ThreadLeftSearchFriend.friendCountInOneRequest = 0;

            //ThreadLeftSearchFriend.searchParm =
            //    (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child != null && UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child is UCCoinTransfer) ?
            //    (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child as UCCoinTransfer)._TextboxRingIDSender.Text.ToLower() : 
            //    (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child as View.UI.Wallet.UCSendReferral)._TextboxReferralNumber.Text.ToLower();
            
            if (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child != null && UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child is UCCoinTransfer)
            {
                ThreadLeftSearchFriend.searchParm = (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child as UCCoinTransfer)._TextboxRingIDSender.Text.ToLower();
            }
            else if (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child != null && UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child is View.UI.Wallet.UCSendReferral)
            {
                ThreadLeftSearchFriend.searchParm = (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child as View.UI.Wallet.UCSendReferral)._TextboxReferralNumber.Text.ToLower();
            }
            else if (Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content != null && Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content is UCSetReferrer)
            {
                ThreadLeftSearchFriend.searchParm = (Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content as UCSetReferrer)._TextboxReferrerNumber.Text.ToLower();
            }
            MainSwitcher.ThreadManager().LeftSearchFriend.StartThread(ThreadLeftSearchFriend.SearchFromLeftPanel, StatusConstants.SEARCH_BY_ALL);
        }

        public void Show(UIElement target)
        {
            _ReferralSearchPopup.PlacementTarget = target;
            _ReferralSearchPopup.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            if (!_ReferralSearchPopup.IsOpen)
            {
                _ReferralSearchPopup.IsOpen = true;
            }
            //referralSearchPopup.PlacementTarget = target;
            //referralSearchPopup.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;            
        }

        public void Hide()
        {
            _ReferralSearchPopup.IsOpen = false;            
        }

        public bool IsOpen
        {
            get { return _ReferralSearchPopup.IsOpen; }
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            long utID = Convert.ToInt32((sender as Grid).Tag);
            UserBasicInfoModel selectedItem = RingIDViewModel.Instance.TempFriendSearchList.Where(x => x.ShortInfoModel.UserTableID == utID).FirstOrDefault();
            if (selectedItem != null)
            {
                if (ItemSelectionHandler != null)
                {
                    ItemSelectionHandler(selectedItem);
                }
            }            
        }
    }
}

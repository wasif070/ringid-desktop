﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCCoinBundlePanel.xaml
    /// </summary>
    public partial class UCCoinBundlePanel : UserControl
    {
        public UCCoinBundlePanel()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCCoinBundlePanel_Loaded;
            this.Unloaded += UCCoinBundlePanel_Unloaded;
        }

        void UCCoinBundlePanel_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_GridPanel);
            _GridPanel.MouseLeftButtonDown += _GridPanel_MouseLeftButtonDown;
            _BtnClose.Click += _BtnClose_Click;
            _BtnBack.Click += _BtnBack_Click;

            performInitialRequest();
        }

        private void UCCoinBundlePanel_Unloaded(object sender, RoutedEventArgs e)
        {
            Utility.Wallet.WalletViewModel.Instance.PaymentMethod = Models.Constants.WalletConstants.PAYMENT_METHOD_DEFAULT;
            _GridPanel.MouseLeftButtonDown -= _GridPanel_MouseLeftButtonDown;
            _BtnClose.Click -= _BtnClose_Click;
            _BtnBack.Click -= _BtnBack_Click;
        }

        void _BtnBack_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowCoinRechargeMethods();
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(Models.Constants.WalletConstants.PU_COIN_RECHARGE_METHODS);
        }

        void _BtnClose_Click(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
        }

        void _GridPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void performInitialRequest()
        {
            Utility.Wallet.WalletViewModel.Instance.CoinBundles.Clear();
            Utility.Wallet.WalletViewModel.Instance.CoinBundleListReload = false;
            Utility.Wallet.WalletViewModel.Instance.CoinBundleListLoading = true;
            new Utility.Wallet.ThreadGetCoinBundleList().StartCoinBundleForPurchase();
        }
    }
}

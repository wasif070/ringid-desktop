﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Wallet
{
    /// <summary>
    /// Interaction logic for UCEarnFreeCoin.xaml
    /// </summary>
    public partial class UCEarnFreeCoin : UserControl
    {
        public UCEarnFreeCoin()
        {
            InitializeComponent();
            this.DataContext = Utility.Wallet.WalletViewModel.Instance;
            this.Loaded += UCEarnFreeCoin_Loaded;
        }

        void UCEarnFreeCoin_Loaded(object sender, RoutedEventArgs e)
        {
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.UCControlAnimateFromLeft(_GridPanel);
            Utility.Wallet.WalletViewModel.Instance.RuleListReloadRequire = false;
            Utility.Wallet.WalletViewModel.Instance.RuleListLoading = true;
            new Utility.Wallet.ThreadCoinEarningRuleList().StartGetCoinEarningRuleList();
        }
    }
}

﻿using Auth.Service.Notification;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;
using View.Utility.Notification;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Notification
{
    /// <summary>
    /// Interaction logic for UCAllNotification.xaml
    /// </summary>
    //public partial class UCAllNotification : UserControl, INotifyPropertyChanged
    //{
    //    private static readonly ILog log = LogManager.GetLogger(typeof(UCAllNotification).Name);
    //    //public List<long> notificationIds = new List<long>();

    //    #region Properties
    //    public static UCAllNotification Instance;
    //    private ICommand _NotificationClickCommand;
    //    private bool isDeleteEnabled = true;
    //    private ICommand _NotificationDeleteClickCommand;
    //    private ICommand _NotificationReadUnreadClickCommand;
    //    private long _NotificationModelCount;
    //    private bool _AllRead;
    //    public bool AllRead
    //    {
    //        get { return _AllRead; }
    //        set { _AllRead = value; this.OnPropertyChanged("AllRead"); }
    //    }
    //    public long NotificationModelCount
    //    {
    //        get
    //        {
    //            return _NotificationModelCount;
    //        }
    //        set
    //        {
    //            if (value == _NotificationModelCount)
    //                return;

    //            _NotificationModelCount = value;
    //            this.OnPropertyChanged("NotificationModelCount");
    //        }
    //    }
    //    #endregion

    //    #region Constructor

    //    public UCAllNotification()
    //    {
    //        InitializeComponent();
    //        this.DataContext = this;
    //        Instance = this;
    //        NotificationModelCount = 0;
    //        LoadShowMoreData();
    //    }

    //    #endregion

    //    public void LoadShowMoreData()
    //    {
    //        Application.Current.Dispatcher.Invoke(() =>
    //        {
    //            try
    //            {
    //                List<NotificationDTO> sortedList = (from l in RingDictionaries.Instance.NOTIFICATION_LISTS
    //                                                    orderby l.UpdateTime descending
    //                                                    select l).ToList();

    //                lock (RingIDViewModel.Instance.NotificationList)
    //                {
    //                    foreach (NotificationDTO dto in sortedList)
    //                    {
    //                        if (!RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == dto.ID))
    //                        {
    //                            NotificationModel model = new NotificationModel(dto);
    //                            RingIDViewModel.Instance.NotificationList.Add(model);
    //                        }
    //                    }
    //                }

    //                AppConstants.NOTIFICATION_MIN_UT = RingIDViewModel.Instance.NotificationList.Count > 0 ? RingIDViewModel.Instance.NotificationList.Min(x => x.UpdateTime) : AppConstants.NOTIFICATION_MIN_UT;

    //                NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
    //                NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;
    //                IsAllRead();
    //            }
    //            catch (Exception) { }
    //        });

    //    }

    //    private void SeeMore_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    //    {
    //        Application.Current.Dispatcher.Invoke(() =>
    //        {
    //            try
    //            {
    //                NotificationUtility.Instance.ShowMoreAction(string.Empty);

    //                if (AppConstants.NOTIFICATION_ALL_FETCHED_FROM_DB == false)
    //                {
    //                    NotificationHistoryDAO.Instance.LoadNotificationFromDB(RingIDViewModel.Instance.NotificationList.Min(x => x.UpdateTime));
    //                    LoadShowMoreData();

    //                    NotificationUtility.Instance.ShowMoreAction();
    //                    if (AppConstants.NOTIFICATION_MIN_UT == NotificationHistoryDAO.Instance.GetMinTimeFromNotificationHistory()) { AppConstants.NOTIFICATION_ALL_FETCHED_FROM_DB = true;NotificationRequest(AppConstants.NOTIFICATION_MIN_UT, (short)2); }
    //                }
    //                else
    //                {
    //                   NotificationRequest(AppConstants.NOTIFICATION_MIN_UT, (short)2);
    //                }
    //                IsAllRead();
    //            }
    //            catch (Exception) { }
    //        });
    //    }

    //    #region Commands
    //    public ICommand NotificationReadUnreadClickCommand
    //    {
    //        get
    //        {
    //            _NotificationReadUnreadClickCommand = _NotificationReadUnreadClickCommand ?? new RelayCommand(param => OnNotificationReadUnreadClicked(param));
    //            return _NotificationReadUnreadClickCommand;
    //        }
    //    }

    //    public void OnNotificationReadUnreadClicked(object parameter)
    //    {
    //        Application.Current.Dispatcher.Invoke(() =>
    //        {
    //            try
    //            {
    //                if (parameter is NotificationModel)
    //                {
    //                    NotificationModel model = (NotificationModel)parameter;
    //                    NotificationUtility.Instance.ReadUnreadNotification(model);
    //                    IsAllRead();
    //                }
    //            }
    //            catch (Exception) { }
    //        });
    //    }

    //    public ICommand NotificationDeleteClickCommand
    //    {
    //        get
    //        {
    //            _NotificationDeleteClickCommand = _NotificationDeleteClickCommand ?? new RelayCommand(param => OnNotificationDeleteClicked(param));
    //            return _NotificationDeleteClickCommand;
    //        }
    //    }

    //    public void OnNotificationDeleteClicked(object parameter)
    //    {
    //        Application.Current.Dispatcher.Invoke(() =>
    //        {
    //            try
    //            {
    //                if (parameter is NotificationModel)
    //                {
    //                    NotificationModel model = (NotificationModel)parameter;
    //                    MessageBoxResult result = CustomMessageBox.ShowQuestion(NotificationMessages.ASK_DELETE_NOTIFICATION, NotificationMessages.HEADER_DELETE_NOTIFICATION);

    //                    if (result == MessageBoxResult.Yes) { NotificationUtility.Instance.DeleteNotificationFromCommand(model); }
    //                }
    //            }
    //            catch (Exception) { }
    //        });
    //    }

    //    public ICommand NotificationClickCommand
    //    {
    //        get
    //        {
    //            _NotificationClickCommand = _NotificationClickCommand ?? new RelayCommand(param => OnNotificationClicked(param));
    //            return _NotificationClickCommand;
    //        }
    //    }

    //    public void OnNotificationClicked(object parameter)
    //    {
    //        Application.Current.Dispatcher.Invoke(() =>
    //        {
    //            try
    //            {
    //                if (parameter is NotificationModel)
    //                {
    //                    NotificationModel model = (NotificationModel)parameter;
    //                    NotificationUtility.Instance.NotificationClickOperation(model);
    //                }
    //            }
    //            catch (Exception) { }
    //        });

    //    }


    //    #endregion

    //    public event PropertyChangedEventHandler PropertyChanged;
    //    private void OnPropertyChanged(string propertyName)
    //    {
    //        PropertyChangedEventHandler handler = PropertyChanged;
    //        if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
    //    }


    //    private void SelectButton_Click(object sender, RoutedEventArgs e)
    //    {
    //        deSelectButton.Visibility = Visibility.Visible;
    //        selectButton.Visibility = Visibility.Collapsed;

    //        NotificationUtility.Instance.NotificationSelectDeselect();
    //    }



    //    private void deSelectButton_Click(object sender, RoutedEventArgs e)
    //    {
    //        deSelectButton.Visibility = Visibility.Collapsed;
    //        selectButton.Visibility = Visibility.Visible;

    //        NotificationUtility.Instance.NotificationSelectDeselect(false);
    //    }

    //    private void ntfcnCancelButton_Click(object sender, RoutedEventArgs e)
    //    {
    //        deSelectButton.Visibility = Visibility.Collapsed;
    //        selectButton.Visibility = Visibility.Visible;

    //        NotificationUtility.Instance.NotificationSelectDeselect(false);

    //        this.ntfcnEditMode.Visibility = Visibility.Visible;
    //        this.ntfcnSelectPanel.Visibility = Visibility.Collapsed;
    //    }


    //    private void SetButtonsEnabled(bool flag)
    //    {
    //        isDeleteEnabled = flag;
    //    }

    //    private void ntfcnDeleteButton_Click(object sender, RoutedEventArgs e)
    //    {
    //        try
    //        {
    //            this.ntfcnDeleteButton.IsEnabled = false;
    //            this.ntfcnCancelButton.IsEnabled = false;
    //            this.selectButton.IsEnabled = false;
    //            this.deSelectButton.IsEnabled = false;

    //            bool anyItemSelected = RingIDViewModel.Instance.NotificationList.Any(P => P.IsChecked == true);
    //            if (anyItemSelected)
    //            {
    //                List<long> notificationIds = new List<long>();

    //                foreach (NotificationModel model in RingIDViewModel.Instance.NotificationList)
    //                {
    //                    if (model.IsChecked)
    //                    {
    //                        notificationIds.AddRange(model.DeleteNotificationIDs);
    //                        if (!notificationIds.Contains(model.NotificationID))
    //                        { notificationIds.Add(model.NotificationID); }
    //                    }
    //                }

    //                //MessageBoxResult result = CustomMessageBox.ShowQuestion("Are you sure you want to delete " + (notificationIds.Count > 1 ? "these notifications?" : "this notification?"), "Delete " + (notificationIds.Count>1? "Notifications" : "Notification"));
    //                MessageBoxResult result = CustomMessageBox.ShowQuestion((notificationIds.Count > 1 ? NotificationMessages.ASK_DELETE_NOTIFICATIONS : NotificationMessages.ASK_DELETE_NOTIFICATION), (notificationIds.Count > 1 ? NotificationMessages.HEADER_DELETE_NOTIFICATIONS : NotificationMessages.HEADER_DELETE_NOTIFICATION));

    //                if (result == MessageBoxResult.Yes) { Application.Current.Dispatcher.Invoke(() => { NotificationUtility.Instance.DeleteNotifications(notificationIds); }); }
    //            }
    //            else { CustomMessageBox.ShowWarning("Select an item to delete"); }
    //            this.ntfcnDeleteButton.IsEnabled = true;
    //            this.ntfcnCancelButton.IsEnabled = true;
    //            this.selectButton.IsEnabled = true;
    //            this.deSelectButton.IsEnabled = true;

    //            NotificationUtility.Instance.NotificationSelectDeselect(false);

    //            deSelectButton.Visibility = Visibility.Collapsed;
    //            selectButton.Visibility = Visibility.Visible;

    //            this.ntfcnEditMode.Visibility = Visibility.Visible;
    //            this.ntfcnSelectPanel.Visibility = Visibility.Collapsed;
    //        }
    //        catch (Exception) { }
    //    }

    //    private void MarkAllRead_Click(object sender, MouseButtonEventArgs e)
    //    {
    //        Application.Current.Dispatcher.Invoke(() =>
    //        {
    //            try
    //            {
    //                NotificationUtility.Instance.MarkAllNotificationsAsRead();
    //                AllRead = true;
    //            }
    //            catch (Exception) { }
    //        });
    //    }

    //    private void Notification_Edit_Button_Click(object sender, MouseButtonEventArgs e)
    //    {
    //        this.ntfcnEditMode.Visibility = Visibility.Collapsed;
    //        this.ntfcnSelectPanel.Visibility = Visibility.Visible;
    //    }

    //    public void IsAllRead()
    //    {
    //        Application.Current.Dispatcher.Invoke(() =>
    //        {
    //            try
    //            {
    //                AllRead = true;
    //                //foreach (var item in Notification.Items)
    //                foreach (var item in RingIDViewModel.Instance.NotificationList)
    //                {
    //                    if (((NotificationModel)item).IsRead == false && ((NotificationModel)item).ShowPanelSeeMore != true)
    //                    {
    //                        AllRead = false;
    //                        break;
    //                    }
    //                }
    //            }
    //            catch (Exception) { }
    //        });
    //    }

    //    private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
    //    {
    //        if (e.Key == Key.Home)
    //        {
    //            Scroll.ScrollToHome();
    //            e.Handled = true;
    //        }
    //        else if (e.Key == Key.End)
    //        {
    //            Scroll.ScrollToEnd();
    //            e.Handled = true;
    //        }
    //    }

    //    public string SeeMoreText
    //    {
    //        get { return (string)GetValue(SeeMoreTextProperty); }
    //        set { SetValue(SeeMoreTextProperty, value); }
    //    }

    //    public static readonly DependencyProperty SeeMoreTextProperty =
    //        DependencyProperty.Register("SeeMoreText", typeof(string), typeof(UCAllNotification), new PropertyMetadata(NotificationMessages.NOTIFICATION_SHOW_MORE));

    //    private void ItemsControl_Loaded(object sender, RoutedEventArgs e)
    //    {
    //        Notification.ItemsSource = RingIDViewModel.Instance.NotificationList;
    //    }

    //    public int NotificationLoaded
    //    {
    //        get { return (int)GetValue(NotificationLoadedProperty); }
    //        set { SetValue(NotificationLoadedProperty, value); }
    //    }

    //    // Using a DependencyProperty as the backing store for NotificationLoaded.  This enables animation, styling, binding, etc...
    //    public static readonly DependencyProperty NotificationLoadedProperty =
    //        DependencyProperty.Register("NotificationLoaded", typeof(int), typeof(UCAllNotification), new PropertyMetadata(0));

    //    private void notificationReadUnreadClick(object sender, RoutedEventArgs e)
    //    {
    //        Control control = sender as Control;
    //        NotificationModel model = (NotificationModel)control.DataContext;
    //        OnNotificationReadUnreadClicked(model);
    //    }

    //    private void notificationDeleteClick(object sender, RoutedEventArgs e)
    //    {
    //        Control control = sender as Control;
    //        NotificationModel model = (NotificationModel)control.DataContext;
    //        OnNotificationDeleteClicked(model);
    //    }

    //}
    public partial class UCAllNotification : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCAllNotification).Name);
        //public List<long> notificationIds = new List<long>();

        #region Properties
        public static UCAllNotification Instance;
        //private ICommand _NotificationClickCommand;
        //private bool isDeleteEnabled = true;
        //private ICommand _NotificationDeleteClickCommand;
        //private ICommand _NotificationReadUnreadClickCommand;
        //private long _NotificationModelCount;
        //private bool _AllRead;
        //public bool AllRead
        //{
        //    get { return _AllRead; }
        //    set { _AllRead = value; this.OnPropertyChanged("AllRead"); }
        //}
        //public long NotificationModelCount
        //{
        //    get
        //    {
        //        return _NotificationModelCount;
        //    }
        //    set
        //    {
        //        if (value == _NotificationModelCount)
        //            return;

        //        _NotificationModelCount = value;
        //        this.OnPropertyChanged("NotificationModelCount");
        //    }
        //}
        #endregion

        #region Constructor

        public UCAllNotification()
        {
            InitializeComponent();
            //this.DataContext = this;
            this.DataContext = VMNotification.Instance;
            Instance = this;
            //NotificationModelCount = 0;
            //LoadShowMoreData();
            VMNotification.Instance.LoadData();
        }

        #endregion

        //public void LoadShowMoreData()
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            List<NotificationDTO> sortedList = (from l in RingDictionaries.Instance.NOTIFICATION_LISTS
        //                                                orderby l.UpdateTime descending
        //                                                select l).ToList();

        //            lock (RingIDViewModel.Instance.NotificationList)
        //            {
        //                foreach (NotificationDTO dto in sortedList)
        //                {
        //                    if (!RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == dto.ID))
        //                    {
        //                        NotificationModel model = new NotificationModel(dto);
        //                        RingIDViewModel.Instance.NotificationList.Add(model);
        //                    }
        //                }
        //            }

        //            AppConstants.NOTIFICATION_MIN_UT = RingIDViewModel.Instance.NotificationList.Count > 0 ? RingIDViewModel.Instance.NotificationList.Min(x => x.UpdateTime) : AppConstants.NOTIFICATION_MIN_UT;

        //            NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
        //            NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;
        //            IsAllRead();
        //        }
        //        catch (Exception) { }
        //    });

        //}

        //private void SeeMore_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            NotificationUtility.Instance.ShowMoreAction(string.Empty);

        //            if (AppConstants.NOTIFICATION_ALL_FETCHED_FROM_DB == false)
        //            {
        //                NotificationHistoryDAO.Instance.LoadNotificationFromDB(RingIDViewModel.Instance.NotificationList.Min(x => x.UpdateTime));
        //                //LoadShowMoreData();
        //                VMNotification.Instance.LoadShowMoreData();

        //                NotificationUtility.Instance.ShowMoreAction();
        //                if (AppConstants.NOTIFICATION_MIN_UT == NotificationHistoryDAO.Instance.GetMinTimeFromNotificationHistory()) { AppConstants.NOTIFICATION_ALL_FETCHED_FROM_DB = true;NotificationRequest(AppConstants.NOTIFICATION_MIN_UT, (short)2); }
        //            }
        //            else
        //            {
        //               NotificationRequest(AppConstants.NOTIFICATION_MIN_UT, (short)2);
        //            }
        //            IsAllRead();
        //        }
        //        catch (Exception) { }
        //    });
        //}

        #region Commands
        //public ICommand NotificationReadUnreadClickCommand
        //{
        //    get
        //    {
        //        _NotificationReadUnreadClickCommand = _NotificationReadUnreadClickCommand ?? new RelayCommand(param => OnNotificationReadUnreadClicked(param));
        //        return _NotificationReadUnreadClickCommand;
        //    }
        //}

        //public void OnNotificationReadUnreadClicked(object parameter)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            if (parameter is NotificationModel)
        //            {
        //                NotificationModel model = (NotificationModel)parameter;
        //                NotificationUtility.Instance.ReadUnreadNotification(model);
        //                IsAllRead();
        //            }
        //        }
        //        catch (Exception) { }
        //    });
        //}

        //public ICommand NotificationDeleteClickCommand
        //{
        //    get
        //    {
        //        _NotificationDeleteClickCommand = _NotificationDeleteClickCommand ?? new RelayCommand(param => OnNotificationDeleteClicked(param));
        //        return _NotificationDeleteClickCommand;
        //    }
        //}

        //public void OnNotificationDeleteClicked(object parameter)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            if (parameter is NotificationModel)
        //            {
        //                NotificationModel model = (NotificationModel)parameter;
        //                MessageBoxResult result = CustomMessageBox.ShowQuestion(NotificationMessages.ASK_DELETE_NOTIFICATION, NotificationMessages.HEADER_DELETE_NOTIFICATION);

        //                if (result == MessageBoxResult.Yes) { NotificationUtility.Instance.DeleteNotificationFromCommand(model); }
        //            }
        //        }
        //        catch (Exception) { }
        //    });
        //}

        //public ICommand NotificationClickCommand
        //{
        //    get
        //    {
        //        _NotificationClickCommand = _NotificationClickCommand ?? new RelayCommand(param => OnNotificationClicked(param));
        //        return _NotificationClickCommand;
        //    }
        //}

        //public void OnNotificationClicked(object parameter)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            if (parameter is NotificationModel)
        //            {
        //                NotificationModel model = (NotificationModel)parameter;
        //                NotificationUtility.Instance.NotificationClickOperation(model);
        //            }
        //        }
        //        catch (Exception) { }
        //    });

        //}


        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }


        //private void SelectButton_Click(object sender, RoutedEventArgs e)
        //{
        //    deSelectButton.Visibility = Visibility.Visible;
        //    selectButton.Visibility = Visibility.Collapsed;

        //    NotificationUtility.Instance.NotificationSelectDeselect();
        //}



        //private void deSelectButton_Click(object sender, RoutedEventArgs e)
        //{
        //    deSelectButton.Visibility = Visibility.Collapsed;
        //    selectButton.Visibility = Visibility.Visible;

        //    NotificationUtility.Instance.NotificationSelectDeselect(false);
        //}

        //private void ntfcnCancelButton_Click(object sender, RoutedEventArgs e)
        //{
        //    deSelectButton.Visibility = Visibility.Collapsed;
        //    selectButton.Visibility = Visibility.Visible;

        //    NotificationUtility.Instance.NotificationSelectDeselect(false);

        //    this.ntfcnEditMode.Visibility = Visibility.Visible;
        //    this.ntfcnSelectPanel.Visibility = Visibility.Collapsed;
        //}


        //private void SetButtonsEnabled(bool flag)
        //{
        //    isDeleteEnabled = flag;
        //}

        //private void ntfcnDeleteButton_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        this.ntfcnDeleteButton.IsEnabled = false;
        //        this.ntfcnCancelButton.IsEnabled = false;
        //        this.selectButton.IsEnabled = false;
        //        this.deSelectButton.IsEnabled = false;

        //        bool anyItemSelected = RingIDViewModel.Instance.NotificationList.Any(P => P.IsChecked == true);
        //        if (anyItemSelected)
        //        {
        //            List<long> notificationIds = new List<long>();

        //            foreach (NotificationModel model in RingIDViewModel.Instance.NotificationList)
        //            {
        //                if (model.IsChecked)
        //                {
        //                    notificationIds.AddRange(model.DeleteNotificationIDs);
        //                    if (!notificationIds.Contains(model.NotificationID))
        //                    { notificationIds.Add(model.NotificationID); }
        //                }
        //            }

        //            //MessageBoxResult result = CustomMessageBox.ShowQuestion("Are you sure you want to delete " + (notificationIds.Count > 1 ? "these notifications?" : "this notification?"), "Delete " + (notificationIds.Count>1? "Notifications" : "Notification"));
        //            MessageBoxResult result = CustomMessageBox.ShowQuestion((notificationIds.Count > 1 ? NotificationMessages.ASK_DELETE_NOTIFICATIONS : NotificationMessages.ASK_DELETE_NOTIFICATION), (notificationIds.Count > 1 ? NotificationMessages.HEADER_DELETE_NOTIFICATIONS : NotificationMessages.HEADER_DELETE_NOTIFICATION));

        //            if (result == MessageBoxResult.Yes) { Application.Current.Dispatcher.Invoke(() => { NotificationUtility.Instance.DeleteNotifications(notificationIds); }); }
        //        }
        //        else { CustomMessageBox.ShowWarning("Select an item to delete"); }
        //        this.ntfcnDeleteButton.IsEnabled = true;
        //        this.ntfcnCancelButton.IsEnabled = true;
        //        this.selectButton.IsEnabled = true;
        //        this.deSelectButton.IsEnabled = true;

        //        NotificationUtility.Instance.NotificationSelectDeselect(false);

        //        deSelectButton.Visibility = Visibility.Collapsed;
        //        selectButton.Visibility = Visibility.Visible;

        //        this.ntfcnEditMode.Visibility = Visibility.Visible;
        //        this.ntfcnSelectPanel.Visibility = Visibility.Collapsed;
        //    }
        //    catch (Exception) { }
        //}

        //private void MarkAllRead_Click(object sender, MouseButtonEventArgs e)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            NotificationUtility.Instance.MarkAllNotificationsAsRead();
        //            AllRead = true;
        //        }
        //        catch (Exception) { }
        //    });
        //}

        //private void Notification_Edit_Button_Click(object sender, MouseButtonEventArgs e)
        //{
        //    this.ntfcnEditMode.Visibility = Visibility.Collapsed;
        //    this.ntfcnSelectPanel.Visibility = Visibility.Visible;
        //}

        //public void IsAllRead()
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            AllRead = true;
        //            //foreach (var item in Notification.Items)
        //            foreach (var item in RingIDViewModel.Instance.NotificationList)
        //            {
        //                if (((NotificationModel)item).IsRead == false && ((NotificationModel)item).ShowPanelSeeMore != true)
        //                {
        //                    AllRead = false;
        //                    break;
        //                }
        //            }
        //        }
        //        catch (Exception) { }
        //    });
        //}

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        //public string SeeMoreText
        //{
        //    get { return (string)GetValue(SeeMoreTextProperty); }
        //    set { SetValue(SeeMoreTextProperty, value); }
        //}

        //public static readonly DependencyProperty SeeMoreTextProperty =
        //    DependencyProperty.Register("SeeMoreText", typeof(string), typeof(UCAllNotification), new PropertyMetadata(NotificationMessages.NOTIFICATION_SHOW_MORE));

        private void ItemsControl_Loaded(object sender, RoutedEventArgs e)
        {
            Notification.ItemsSource = RingIDViewModel.Instance.NotificationList;
        }

        //public int NotificationLoaded
        //{
        //    get { return (int)GetValue(NotificationLoadedProperty); }
        //    set { SetValue(NotificationLoadedProperty, value); }
        //}

        //// Using a DependencyProperty as the backing store for NotificationLoaded.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty NotificationLoadedProperty =
        //    DependencyProperty.Register("NotificationLoaded", typeof(int), typeof(UCAllNotification), new PropertyMetadata(0));

        //private void notificationReadUnreadClick(object sender, RoutedEventArgs e)
        //{
        //    Control control = sender as Control;
        //    NotificationModel model = (NotificationModel)control.DataContext;
        //    OnNotificationReadUnreadClicked(model);
        //}

        //private void notificationDeleteClick(object sender, RoutedEventArgs e)
        //{
        //    Control control = sender as Control;
        //    NotificationModel model = (NotificationModel)control.DataContext;
        //    OnNotificationDeleteClicked(model);
        //}

    }

}

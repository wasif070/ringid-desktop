﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using View.UI.RecoverPassword;
using View.Utility;
using View.Utility.SignInSignUP;
using View.ViewModel;

namespace View.UI.SignIn
{
    /// <summary>
    /// Interaction logic for UCSigninWithRingID.xaml
    /// </summary>
    public partial class UCSigninWithRingID : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCSigninWithRingID).Name);
        Border motherborder = null;
        Grid parentGridForPopup = null;
        public event DelegateNoParam OnBackButtonClicked;
        SigninRequest signinRequest;
        #endregion

        #region"Ctors"

        public UCSigninWithRingID(VMRingIDMainWindow model, Grid parentGridForPopup1, Border motherBorder1)
        {
            InitializeComponent();
            this.DataModel = model;
            this.parentGridForPopup = parentGridForPopup1;
            this.motherborder = motherBorder1;
            this.DataContext = this;
        }

        #endregion"Ctors"

        #region "Properties"

        private UCRecoverPasswordParent RecoverPasswordParent { get; set; }
        private UCSignInParent SignInParent { get; set; }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private bool isCheckedSavePassowrd = true;
        public bool IsCheckedSavePassowrd
        {
            get { return isCheckedSavePassowrd; }
            set
            {
                if (value == isCheckedSavePassowrd) return;
                isCheckedSavePassowrd = value;
                onCheckUncheckSavePassword(isCheckedSavePassowrd);
                OnPropertyChanged("IsCheckedSavePassowrd");
            }
        }

        private string ringID = string.Empty;
        public string RingID
        {
            get { return this.ringID; }
            set
            {
                if (value.Length > 4 && !value.Contains(" ")) this.ringID = value.Insert(4, " ");
                else this.ringID = value.Trim();
                this.OnPropertyChanged("RingID");
            }
        }

        private bool isFocusedOnPassword;
        public bool IsFocusedOnPassword
        {
            get { return isFocusedOnPassword; }
            set { isFocusedOnPassword = value; OnPropertyChanged("IsFocusedOnPassword"); }
        }

        private bool isFocusedOnRingID;
        public bool IsFocusedOnRingID
        {
            get { return isFocusedOnRingID; }
            set { isFocusedOnRingID = value; OnPropertyChanged("IsFocusedOnRingID"); }
        }

        private Visibility cancelButtonVisibility = Visibility.Collapsed;
        public Visibility CancelButtonVisibility
        {
            get { return cancelButtonVisibility; }
            set
            {
                cancelButtonVisibility = value;
                if (value == Visibility.Visible) SignInButtonVisibility = Visibility.Collapsed;
                else SignInButtonVisibility = Visibility.Visible;
                OnPropertyChanged("CancelButtonVisibility");
            }
        }

        private Visibility signInButtonVisibility = Visibility.Visible;
        public Visibility SignInButtonVisibility
        {
            get { return signInButtonVisibility; }
            set { signInButtonVisibility = value; OnPropertyChanged("SignInButtonVisibility"); }
        }

        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            showDefaultData();
            focusedOnRingID();
        }

        private ICommand backButtonCommand;
        public ICommand BackButtonCommand
        {
            get
            {
                if (backButtonCommand == null) backButtonCommand = new RelayCommand(param => OnBackButton(param));
                return backButtonCommand;
            }
        }
        private void OnBackButton(object param)
        {
            if (DataModel.IsComponentEnabled)
            {
                DataModel.ErrorText = string.Empty;
                DataModel.PleaseWaitLoader = false;
                DataModel.IsComponentEnabled = true;
                if (OnBackButtonClicked != null) OnBackButtonClicked();
            }
        }


        private ICommand enterKeyCommand;
        public ICommand EnterKeyCommand
        {
            get
            {
                if (enterKeyCommand == null) enterKeyCommand = new RelayCommand(param => OnEnterKeyCommand(param));
                return enterKeyCommand;
            }
        }
        private void OnEnterKeyCommand(object param)
        {
            if (DataModel.IsComponentEnabled) OnSignInCommand();
        }

        private ICommand signInCommand;
        public ICommand SignInCommand
        {
            get
            {
                if (signInCommand == null) signInCommand = new RelayCommand(param => OnSignInCommand());
                return signInCommand;
            }
        }
        private void OnSignInCommand()
        {
            string ringID = RingID.Replace(" ", "").Trim();
            DataModel.RingID = ringID;
            string validationMsg = HelperMethodsModel.LoginValidationMsg(DataModel.RingID, DataModel.Password, SettingsConstants.RINGID_LOGIN);
            if (string.IsNullOrEmpty(validationMsg))
            {
                DataModel.PleaseWaitLoader = true;
                signInRequest();
            }
            else DataModel.ShowErrorMessage(validationMsg);
        }

        private ICommand forgotCommand;
        public ICommand ForgotCommand
        {
            get
            {
                if (forgotCommand == null) forgotCommand = new RelayCommand(param => OnForgotCommand());
                return forgotCommand;
            }
        }
        private void OnForgotCommand()
        {
            Console.WriteLine("Forgot");
            string ringID = RingID.Replace(" ", "").Trim();
            DataModel.RingID = ringID;
            string validationMsg = HelperMethodsModel.RingidorMailorMobileValidationMsg(DataModel.RingID, SettingsConstants.RINGID_LOGIN);
            if (string.IsNullOrEmpty(validationMsg))
            {
                DataModel.PleaseWaitLoader = true;
                forGotPassword();
            }
            else DataModel.ShowErrorMessage(validationMsg);
        }

        private ICommand cancelReqeustCommand;
        public ICommand CancelReqeustCommand
        {
            get
            {
                if (cancelReqeustCommand == null) cancelReqeustCommand = new RelayCommand(param => OnCancelReqeustCommand());
                return cancelReqeustCommand;
            }
        }
        private void OnCancelReqeustCommand()
        {
            if (signinRequest != null) signinRequest.Stop();
        }

        #endregion"ICommands and Command Methods"

        #region "Utility Methods"

        public void showDefaultData()
        {
            CancelButtonVisibility = Visibility.Collapsed;
            if (!string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_NAME.Trim()) && DefaultSettings.VALUE_LOGIN_USER_TYPE == SettingsConstants.RINGID_LOGIN)
            {
                IsCheckedSavePassowrd = (DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD == 1) ? true : false;
                DataModel.Password = (string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_PASSWORD) || (DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD == 0)) ? "" : DefaultSettings.VALUE_LOGIN_USER_PASSWORD;
                RingID = DefaultSettings.VALUE_LOGIN_USER_NAME.Substring(2);
            }
            else
            {
                DataModel.Password = string.Empty;
                RingID = string.Empty;
            }
        }

        private void signInRequest()
        {
            startServerReqeust();
            Task taskForServercommunications = new Task(delegate
              {
                  if (IsCheckedSavePassowrd) DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
                  else DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 0;
                  signinRequest = new SigninRequest();
                  CancelButtonVisibility = Visibility.Visible;
                  SignedInInfoDTO signedInInfoDTO = signinRequest.signinRequestByRingId(DataModel.RingID, DataModel.Password);
                  if (signedInInfoDTO.isSuccess)
                  {
                      resetUI();
                      DataModel.ErrorText = string.Empty;
                      Application.Current.Dispatcher.Invoke(() => { UIHelperMethods.ShowMainUIAfterLogin(); }, System.Windows.Threading.DispatcherPriority.Send);
                  }
                  else
                  {
                      DataModel.ErrorText = string.Empty;
                      if (signedInInfoDTO.ReqeustCancelled) { DataModel.ShowErrorMessage(NotificationMessages.TEXT_CANCELED); }
                      else if (signedInInfoDTO.isDownloadMandatory) HelperMethods.DownloadMandatoryUpdater(signedInInfoDTO.versionMsg);
                      else if (!string.IsNullOrEmpty(signedInInfoDTO.errorMsg)) DataModel.ShowErrorMessage(signedInInfoDTO.errorMsg);
                      if (signedInInfoDTO.reasonCode == ReasonCodeConstants.REASON_CODE_PASSWORD_DID_NOT_MATCHED) focusedOnPassword();
                      else focusedOnRingID();
                      resetUI();
                  }
                  CancelButtonVisibility = Visibility.Collapsed;
              });
            taskForServercommunications.Start();
        }

        private void forGotPassword()
        {
            startServerReqeust();
            Task taskForServercommunications = new Task(delegate
               {
                   List<string> sgnss = new RecoverySuggestionRequest(DataModel.RingID).RecoverySuggesionList(SettingsConstants.RINGID_LOGIN, DataModel.RingID, null);
                   if (sgnss != null)
                   {
                       if (sgnss.Count > 0)
                       {
                           Application.Current.Dispatcher.Invoke(() =>
                           {
                               if (SignInParent == null)
                                   if (motherborder != null)
                                       if (motherborder.Child is UCSignInParent) SignInParent = (UCSignInParent)motherborder.Child;
                               if (SignInParent != null)
                               {
                                   DataModel.ErrorText = string.Empty;
                                   if (RecoverPasswordParent == null)
                                   {
                                       RecoverPasswordParent = new UCRecoverPasswordParent(DataModel, SignInParent.motherPanel, motherborder);
                                       RecoverPasswordParent.OnBackButtonClicked += () =>
                                       {
                                           resetUI();
                                           motherborder.Child = null;
                                           motherborder.Child = SignInParent;
                                       };
                                   }
                                   resetUI();
                                   SignInParent._SignInwithRingIDBorderPanel.Child = null;
                                   RecoverPasswordParent.LoadData(sgnss);
                                   motherborder.Child = RecoverPasswordParent;
                               }
                           }, System.Windows.Threading.DispatcherPriority.Send);
                       }
                       else DataModel.ShowErrorMessage(NotificationMessages.NO_RECOVERY_SUGGESTION_FOUND);
                   }
                   else DataModel.ShowErrorMessage(NotificationMessages.NO_RECOVERY_SUGGESTION_FOUND);
                   focusedOnRingID();
               });
            taskForServercommunications.Start();
        }

        private void focusedOnRingID()
        {
            IsFocusedOnRingID = false;
            IsFocusedOnRingID = true;
        }

        private void focusedOnPassword()
        {
            IsFocusedOnPassword = false;
            IsFocusedOnPassword = true;
        }

        private void onCheckUncheckSavePassword(bool isChecked)
        {
            if (isChecked) DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
            else DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 0;
        }

        private void startServerReqeust()
        {
            DataModel.ErrorMessageType = SettingsConstants.MOBILE_LOGIN;
            DataModel.IsComponentEnabled = false;
            DataModel.PleaseWaitLoader = true;
        }

        private void resetUI()
        {
            DataModel.PleaseWaitLoader = false;
            DataModel.IsComponentEnabled = true;
        }

        #endregion "Utility Methods"

        #region "INotifyPropertyChanged"

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
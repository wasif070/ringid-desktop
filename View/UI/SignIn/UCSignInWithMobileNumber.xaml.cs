﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using View.BindingModels;
using View.UI.RecoverPassword;
using View.Utility;
using View.Utility.SignInSignUP;
using View.ViewModel;

namespace View.UI.SignIn
{
    /// <summary>
    /// Interaction logic for UCSignInWithMobileNumber.xaml
    /// </summary>
    public partial class UCSignInWithMobileNumber : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCSignInWithMobileNumber).Name);
        Border motherborder = null;
        public event DelegateNoParam OnBackButtonClicked;
        //InsertRingLoginSetting insertRingLoginSetting;
        SigninRequest signinRequest;
        #endregion

        #region"Ctors"
        public UCSignInWithMobileNumber(VMRingIDMainWindow model, Border motherBorder1)
        {
            InitializeComponent();
            this.DataModel = model;
            this.motherborder = motherBorder1;
            this.DataContext = this;
        }
        #endregion"Ctors"

        #region "Properties"
        private View.UI.PopUp.UCCountryCodePopup CountryCodePopup { get; set; }
        private UCRecoverPasswordParent RecoverPasswordParent { get; set; }
        private UCSignInParent SignInParent { get; set; }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private CountryCodeModel selectedCountryModel;
        public CountryCodeModel SelectedCountryModel
        {
            get { return selectedCountryModel; }
            set
            {
                if (value == selectedCountryModel) return;
                selectedCountryModel = value;
                DataModel.CountryCode = selectedCountryModel.CountryCode;
                OnPropertyChanged("SelectedCountryModel");
            }
        }

        private bool isCheckedSavePassowrd = true;
        public bool IsCheckedSavePassowrd
        {
            get { return isCheckedSavePassowrd; }
            set
            {
                if (value == isCheckedSavePassowrd) return;
                isCheckedSavePassowrd = value;
                onCheckUncheckSavePassword(isCheckedSavePassowrd);
                OnPropertyChanged("IsCheckedSavePassowrd");
            }
        }

        private bool isFocusedOnPassword;
        public bool IsFocusedOnPassword
        {
            get { return isFocusedOnPassword; }
            set { isFocusedOnPassword = value; OnPropertyChanged("IsFocusedOnPassword"); }
        }

        private bool isFocusOnUserID;
        public bool IsFocusOnUserID
        {
            get { return isFocusOnUserID; }
            set { isFocusOnUserID = value; OnPropertyChanged("IsFocusOnUserID"); }
        }
        private Visibility cancelButtonVisibility = Visibility.Collapsed;
        public Visibility CancelButtonVisibility
        {
            get { return cancelButtonVisibility; }
            set
            {
                cancelButtonVisibility = value;
                if (value == Visibility.Visible) SignInButtonVisibility = Visibility.Collapsed;
                else SignInButtonVisibility = Visibility.Visible;
                OnPropertyChanged("CancelButtonVisibility");
            }
        }

        private Visibility signInButtonVisibility = Visibility.Visible;
        public Visibility SignInButtonVisibility
        {
            get { return signInButtonVisibility; }
            set { signInButtonVisibility = value; OnPropertyChanged("SignInButtonVisibility"); }
        }

        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            showDefaultData();
            if (SelectedCountryModel == null) SelectedCountryModel = HelperMethods.GetCountryCodeModelFromCountry();
            getFocusOnUserControl();
        }

        private ICommand backButtonCommand;
        public ICommand BackButtonCommand
        {
            get
            {
                if (backButtonCommand == null) backButtonCommand = new RelayCommand(param => OnBackButton(param));
                return backButtonCommand;
            }
        }
        private void OnBackButton(object param)
        {
            if (DataModel.IsComponentEnabled)
            {
                DataModel.ErrorText = string.Empty;
                DataModel.PleaseWaitLoader = false;
                DataModel.IsComponentEnabled = true;
                if (OnBackButtonClicked != null) OnBackButtonClicked();
            }
        }


        private ICommand enterKeyCommand;
        public ICommand EnterKeyCommand
        {
            get
            {
                if (enterKeyCommand == null) enterKeyCommand = new RelayCommand(param => OnEnterKeyCommand(param));
                return enterKeyCommand;
            }
        }
        private void OnEnterKeyCommand(object param)
        {
            OnSignInCommand();
        }

        private ICommand signInCommand;
        public ICommand SignInCommand
        {
            get
            {
                if (signInCommand == null) signInCommand = new RelayCommand(param => OnSignInCommand());
                return signInCommand;
            }
        }
        private void OnSignInCommand()
        {
            string validationMsg = HelperMethodsModel.LoginValidationMsg(DataModel.PhoneNumber, DataModel.Password, SettingsConstants.MOBILE_LOGIN);
            if (string.IsNullOrEmpty(validationMsg))
            {
                DataModel.PleaseWaitLoader = true;
                signInRequest();
            }
            else DataModel.ShowErrorMessage(validationMsg);
        }

        private ICommand forgotCommand;
        public ICommand ForgotCommand
        {
            get
            {
                if (forgotCommand == null) forgotCommand = new RelayCommand(param => OnForgotCommand());
                return forgotCommand;
            }
        }
        private void OnForgotCommand()
        {
            string validationMsg = HelperMethodsModel.RingidorMailorMobileValidationMsg(DataModel.PhoneNumber, SettingsConstants.MOBILE_LOGIN);
            if (string.IsNullOrEmpty(validationMsg))
            {
                startServerReqeust();
                Task taskForServercommunications = new Task(delegate
                   {
                       string mobilewithCode = (DataModel.CountryCode.Equals("+880") && DataModel.PhoneNumber[0] == '0') ? (DataModel.CountryCode + DataModel.PhoneNumber.Substring(1)) : (DataModel.CountryCode + DataModel.PhoneNumber);
                       List<string> sgnss = new RecoverySuggestionRequest(mobilewithCode).RecoverySuggesionList(SettingsConstants.MOBILE_LOGIN, null, null, DataModel.CountryCode, DataModel.PhoneNumber);
                       if (sgnss != null)
                       {
                           if (sgnss.Count > 0)
                           {
                               Application.Current.Dispatcher.Invoke(() =>
                               {
                                   if (SignInParent == null)
                                       if (motherborder != null)
                                           if (motherborder.Child is UCSignInParent) SignInParent = (UCSignInParent)motherborder.Child;
                                   if (SignInParent != null)
                                   {
                                       DataModel.ErrorText = string.Empty;
                                       if (RecoverPasswordParent == null)
                                       {
                                           RecoverPasswordParent = new UCRecoverPasswordParent(DataModel, SignInParent.motherPanel, motherborder);
                                           RecoverPasswordParent.OnBackButtonClicked += () =>
                                           {
                                               resetUI();
                                               motherborder.Child = null;
                                               motherborder.Child = SignInParent;
                                           };
                                       }
                                       resetUI();
                                       SignInParent._SignInwithPhoneBorderPanel.Child = null;
                                       RecoverPasswordParent.LoadData(sgnss);
                                       motherborder.Child = RecoverPasswordParent;
                                   }
                               }, System.Windows.Threading.DispatcherPriority.Send);
                           }
                           else DataModel.ShowErrorMessage(NotificationMessages.NO_RECOVERY_SUGGESTION_FOUND);
                       }
                       else DataModel.ShowErrorMessage(NotificationMessages.NO_RECOVERY_SUGGESTION_FOUND);
                       getFocusOnUserControl();
                   });
                taskForServercommunications.Start();
            }
            else DataModel.ShowErrorMessage(validationMsg);
        }

        private ICommand countryPopUpCommand;
        public ICommand CountryPopUpCommand
        {
            get
            {
                if (countryPopUpCommand == null) countryPopUpCommand = new RelayCommand(param => OnCountryPopUpCommand(param));
                return countryPopUpCommand;
            }
        }
        private void OnCountryPopUpCommand(object param)
        {
            if (DataModel.IsComponentEnabled)
            {
                if (CountryCodePopup != null) _CountryCodePopupGrid.Children.Remove(CountryCodePopup);
                if (CountryCodePopup == null)
                {
                    CountryCodePopup = new PopUp.UCCountryCodePopup();
                    CountryCodePopup.OnCountryModelChanged += (selectedCountry) =>
                    {
                        SelectedCountryModel = selectedCountry;
                    };
                    CountryCodePopup.OnHide += () =>
                    {
                        _CountryCodePopupGrid.Children.Remove(CountryCodePopup);
                        getFocusOnUserControl();
                    };
                }
                CountryCodePopup.SelectedCountryModel = SelectedCountryModel;
                CountryCodePopup.ShowThis((UIElement)param);
                _CountryCodePopupGrid.Children.Add(CountryCodePopup);
            }
        }

        private ICommand cancelReqeustCommand;
        public ICommand CancelReqeustCommand
        {
            get
            {
                if (cancelReqeustCommand == null) cancelReqeustCommand = new RelayCommand(param => OnCancelReqeustCommand());
                return cancelReqeustCommand;
            }
        }
        private void OnCancelReqeustCommand()
        {
            if (signinRequest != null) signinRequest.Stop();
        }

        #endregion"ICommands and Command Methods"

        #region "Utility Methods"

        private void resetUI()
        {
            DataModel.PleaseWaitLoader = false;
            DataModel.IsComponentEnabled = true;
        }

        private void startServerReqeust()
        {
            DataModel.ErrorMessageType = SettingsConstants.MOBILE_LOGIN;
            DataModel.IsComponentEnabled = false;
            DataModel.PleaseWaitLoader = true;
        }

        private void signInRequest()
        {
            startServerReqeust();
            Task taskForServercommunications = new Task(delegate
            {

                if (IsCheckedSavePassowrd) DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
                else DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 0;
                signinRequest = new SigninRequest();
                CancelButtonVisibility = Visibility.Visible;
                SignedInInfoDTO signedInInfoDTO = signinRequest.signinRequestByMobile(DataModel.PhoneNumber, DataModel.CountryCode, DataModel.Password);
                if (signedInInfoDTO.isSuccess)
                {
                    resetUI();
                    DataModel.ErrorText = string.Empty;
                    Application.Current.Dispatcher.Invoke((Action)delegate { UIHelperMethods.ShowMainUIAfterLogin(); });
                }
                else
                {
                    DataModel.ErrorText = string.Empty;
                    if (signedInInfoDTO.ReqeustCancelled) { DataModel.ShowErrorMessage(NotificationMessages.TEXT_CANCELED); }
                    else if (signedInInfoDTO.isDownloadMandatory) HelperMethods.DownloadMandatoryUpdater(signedInInfoDTO.versionMsg);
                    else if (!string.IsNullOrEmpty(signedInInfoDTO.errorMsg)) DataModel.ShowErrorMessage(signedInInfoDTO.errorMsg);
                    if (signedInInfoDTO.reasonCode == ReasonCodeConstants.REASON_CODE_PASSWORD_DID_NOT_MATCHED)
                    {
                        IsFocusedOnPassword = false;
                        IsFocusedOnPassword = true;
                        resetUI();
                    }
                    else
                    {
                        getFocusOnUserControl();
                        resetUI();
                    }
                }
                CancelButtonVisibility = Visibility.Collapsed;
            });
            taskForServercommunications.Start();
        }

        public void showDefaultData()
        {
            CancelButtonVisibility = Visibility.Collapsed;
            DataModel.ErrorText = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_NAME.Trim()) && DefaultSettings.VALUE_LOGIN_USER_TYPE == SettingsConstants.MOBILE_LOGIN)
                {
                    IsCheckedSavePassowrd = (DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD == 1) ? true : false;
                    DataModel.Password = (string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_PASSWORD) || (DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD == 0)) ? "" : DefaultSettings.VALUE_LOGIN_USER_PASSWORD;
                    DataModel.PhoneNumber = DefaultSettings.VALUE_LOGIN_USER_NAME.Trim();
                    if (!string.IsNullOrEmpty(DefaultSettings.VALUE_MOBILE_DIALING_CODE))
                    {
                        string nm = HelperMethodsModel.Get_country_name_from_contry_code(0, DefaultSettings.VALUE_MOBILE_DIALING_CODE);
                        SelectedCountryModel = new CountryCodeModel(nm, DefaultSettings.VALUE_MOBILE_DIALING_CODE);
                    }
                }
                else
                {
                    DataModel.Password = string.Empty;
                    DataModel.PhoneNumber = string.Empty;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadShowMoreData() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void getFocusOnUserControl()
        {
            IsFocusOnUserID = false;
            IsFocusOnUserID = true;
        }

        private void onCheckUncheckSavePassword(bool isChecked)
        {
            if (isChecked) DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
            else DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 0;
        }
        #endregion "Utility Methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using View.UI.RecoverPassword;
using View.Utility;
using View.Utility.SignInSignUP;
using View.ViewModel;

namespace View.UI.SignIn
{
    /// <summary>
    /// Interaction logic for UCSignInWithEmail.xaml
    /// </summary>
    public partial class UCSignInWithEmail : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCSignInWithEmail).Name);
        public event DelegateNoParam OnBackButtonClicked;
        InsertRingLoginSetting insertRingLoginSetting;
        SigninRequest signinRequest;
        Border motherborder = null;
        #endregion

        #region"Ctors"
        public UCSignInWithEmail(VMRingIDMainWindow model, Border motherBorder1)
        {
            InitializeComponent();
            this.DataModel = model;
            this.motherborder = motherBorder1;
            this.DataContext = this;
        }
        #endregion"Ctors"

        #region "Properties"

        private UCRecoverPasswordParent RecoverPasswordParent { get; set; }
        private UCSignInParent SignInParent { get; set; }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private bool isCheckedSavePassowrd = true;
        public bool IsCheckedSavePassowrd
        {
            get { return isCheckedSavePassowrd; }
            set
            {
                if (value == isCheckedSavePassowrd) return;
                isCheckedSavePassowrd = value;
                onCheckUncheckSavePassword(isCheckedSavePassowrd);
                OnPropertyChanged("IsCheckedSavePassowrd");
            }
        }

        private bool isFocusedOnPassword;
        public bool IsFocusedOnPassword
        {
            get { return isFocusedOnPassword; }
            set { isFocusedOnPassword = value; OnPropertyChanged("IsFocusedOnPassword"); }
        }

        private bool isFocusedOnEmail;
        public bool IsFocusedOnEmail
        {
            get { return isFocusedOnEmail; }
            set { isFocusedOnEmail = value; OnPropertyChanged("IsFocusedOnEmail"); }
        }

        private Visibility signInButtonVisibility = Visibility.Visible;
        public Visibility SignInButtonVisibility
        {
            get { return signInButtonVisibility; }
            set { signInButtonVisibility = value; OnPropertyChanged("SignInButtonVisibility"); }
        }

        private Visibility cancelButtonVisibility = Visibility.Collapsed;
        public Visibility CancelButtonVisibility
        {
            get { return cancelButtonVisibility; }
            set
            {
                cancelButtonVisibility = value;
                if (value == Visibility.Visible) SignInButtonVisibility = Visibility.Collapsed;
                else SignInButtonVisibility = Visibility.Visible;
                OnPropertyChanged("CancelButtonVisibility");
            }
        }
        #endregion//Properties

        #region "ICommands and Command Methods"

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            getFocusOnUserControl();
            showDefaultData();
        }

        private ICommand backButtonCommand;
        public ICommand BackButtonCommand
        {
            get
            {
                if (backButtonCommand == null) backButtonCommand = new RelayCommand(param => OnBackButton(param));
                return backButtonCommand;
            }
        }
        private void OnBackButton(object param)
        {
            if (DataModel.IsComponentEnabled)
            {
                DataModel.ErrorText = string.Empty;
                DataModel.PleaseWaitLoader = false;
                DataModel.IsComponentEnabled = true;
                if (OnBackButtonClicked != null) OnBackButtonClicked();
            }
        }

        private ICommand enterKeyCommand;
        public ICommand EnterKeyCommand
        {
            get
            {
                if (enterKeyCommand == null) enterKeyCommand = new RelayCommand(param => OnEnterKeyCommand(param));
                return enterKeyCommand;
            }
        }
        private void OnEnterKeyCommand(object param)
        {
            if (DataModel.IsComponentEnabled) OnSignInCommand();
        }

        private ICommand signInCommand;
        public ICommand SignInCommand
        {
            get
            {
                if (signInCommand == null) signInCommand = new RelayCommand(param => OnSignInCommand());
                return signInCommand;
            }
        }
        private void OnSignInCommand()
        {
            DataModel.EmailID = DataModel.EmailID.Trim();
            string validationMsg = HelperMethodsModel.LoginValidationMsg(DataModel.EmailID, DataModel.Password, SettingsConstants.EMAIL_LOGIN);
            if (string.IsNullOrEmpty(validationMsg))
            {
                DataModel.PleaseWaitLoader = true;
                signInRequest();
            }
            else DataModel.ShowErrorMessage(validationMsg);
        }

        private ICommand forgotCommand;
        public ICommand ForgotCommand
        {
            get
            {
                if (forgotCommand == null) forgotCommand = new RelayCommand(param => OnForgotCommand());
                return forgotCommand;
            }
        }
        private void OnForgotCommand()
        {
            DataModel.EmailID = DataModel.EmailID.Trim();
            string validationMsg = HelperMethodsModel.RingidorMailorMobileValidationMsg(DataModel.EmailID, SettingsConstants.EMAIL_LOGIN);
            if (string.IsNullOrEmpty(validationMsg)) forGotPassword();
            else DataModel.ShowErrorMessage(validationMsg);
        }

        private ICommand cancelReqeustCommand;
        public ICommand CancelReqeustCommand
        {
            get
            {
                if (cancelReqeustCommand == null) cancelReqeustCommand = new RelayCommand(param => OnCancelReqeustCommand());
                return cancelReqeustCommand;
            }
        }
        private void OnCancelReqeustCommand()
        {
            if (signinRequest != null) signinRequest.Stop();
        }

        #endregion"ICommands and Command Methods"

        #region "Utility Methods"

        public void showDefaultData()
        {
            CancelButtonVisibility = Visibility.Collapsed;
            if (!string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_NAME.Trim()) && DefaultSettings.VALUE_LOGIN_USER_TYPE == SettingsConstants.EMAIL_LOGIN)
            {
                IsCheckedSavePassowrd = (DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD == 1) ? true : false;
                DataModel.Password = (string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_PASSWORD) || (DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD == 0)) ? "" : DefaultSettings.VALUE_LOGIN_USER_PASSWORD;
                DataModel.EmailID = DefaultSettings.VALUE_LOGIN_USER_NAME;
            }
            else
            {
                DataModel.Password = string.Empty;
                DataModel.RingID = string.Empty;
            }
        }

        private void getFocusOnUserControl()
        {
            IsFocusedOnEmail = false;
            IsFocusedOnEmail = true;
        }

        private void onCheckUncheckSavePassword(bool isChecked)
        {
            if (isChecked) DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
            else DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 0;
        }

        private void signInRequest()
        {
            startServerReqeust();
            Task taskForServercommunications = new Task(delegate
              {
                  if (IsCheckedSavePassowrd) DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
                  else DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 0;
                  signinRequest = new SigninRequest();
                  CancelButtonVisibility = Visibility.Visible;
                  SignedInInfoDTO signedInInfoDTO = signinRequest.signinRequestByEmail(DataModel.EmailID, DataModel.Password);
                  if (signedInInfoDTO.isSuccess)
                  {
                      DataModel.ErrorText = string.Empty;
                      resetUI();
                      Application.Current.Dispatcher.Invoke(() =>
                      {
                          UIHelperMethods.ShowMainUIAfterLogin();
                      }, System.Windows.Threading.DispatcherPriority.Send);
                  }
                  else
                  {
                      if (signedInInfoDTO.isDownloadMandatory) HelperMethods.DownloadMandatoryUpdater(signedInInfoDTO.versionMsg);
                      else if (!string.IsNullOrEmpty(signedInInfoDTO.errorMsg)) DataModel.ShowErrorMessage(signedInInfoDTO.errorMsg);
                      if (signedInInfoDTO.reasonCode == ReasonCodeConstants.REASON_CODE_PASSWORD_DID_NOT_MATCHED)
                      {
                          IsFocusedOnPassword = false;
                          IsFocusedOnPassword = true;
                          resetUI();
                      }
                      else
                      {
                          getFocusOnUserControl();
                          resetUI();
                      }
                  }
                  CancelButtonVisibility = Visibility.Collapsed;
              });
            taskForServercommunications.Start();
        }

        private void forGotPassword()
        {
            startServerReqeust();
            Task taskForServercommunications = new Task(delegate
               {
                   List<string> sgnss = new RecoverySuggestionRequest(DataModel.EmailID).RecoverySuggesionList(SettingsConstants.EMAIL_LOGIN, null, DataModel.EmailID);
                   if (sgnss != null)
                   {
                       if (sgnss.Count > 0)
                       {
                           Application.Current.Dispatcher.Invoke(() =>
                           {
                               if (SignInParent == null)
                                   if (motherborder != null)
                                       if (motherborder.Child is UCSignInParent) SignInParent = (UCSignInParent)motherborder.Child;
                               if (SignInParent != null)
                               {
                                   if (RecoverPasswordParent == null)
                                   {
                                       RecoverPasswordParent = new UCRecoverPasswordParent(DataModel, SignInParent.motherPanel, motherborder);
                                       RecoverPasswordParent.OnBackButtonClicked += () =>
                                       {
                                           resetUI();
                                           motherborder.Child = null;
                                           motherborder.Child = SignInParent;
                                       };
                                   }
                                   resetUI();
                                   SignInParent._SignInwithEmailBorderPanel.Child = null;
                                   RecoverPasswordParent.LoadData(sgnss);
                                   motherborder.Child = RecoverPasswordParent;
                               }
                           }, System.Windows.Threading.DispatcherPriority.Send);
                       }
                       else DataModel.ShowErrorMessage(NotificationMessages.NO_RECOVERY_SUGGESTION_FOUND);
                   }
                   else DataModel.ShowErrorMessage(NotificationMessages.NO_RECOVERY_SUGGESTION_FOUND);
                   focusOnEmail();
               });
            taskForServercommunications.Start();
        }

        private void focusOnEmail()
        {
            IsFocusedOnEmail = false;
            IsFocusedOnEmail = true;
        }

        private void startServerReqeust()
        {
            DataModel.ErrorMessageType = SettingsConstants.MOBILE_LOGIN;
            DataModel.IsComponentEnabled = false;
            DataModel.PleaseWaitLoader = true;
        }

        private void resetUI()
        {
            DataModel.PleaseWaitLoader = false;
            DataModel.IsComponentEnabled = true;
            //DataModel.ErrorText = string.Empty;
        }
        #endregion "Utility Methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
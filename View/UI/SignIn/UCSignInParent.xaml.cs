﻿using System;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.Constants;
using View.UI.PopUp;
using View.UI.SignUp;
using View.UI.SocialMedia;
using View.Utility;
using View.ViewModel;

namespace View.UI.SignIn
{
    /// <summary>
    /// Interaction logic for UCSignInParent.xaml
    /// </summary>
    public partial class UCSignInParent : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCSignInParent).Name);
        public event DelegateNoParam OnBackButtonClicked;
        public Grid motherPanel = null;
        Border motherBorder = null;
        #endregion

        #region"Ctors"
        public UCSignInParent(VMRingIDMainWindow model, Grid motherGrid, Border variableControlsBorder)
        {
            InitializeComponent();
            this.DataModel = model;
            this.motherPanel = motherGrid;
            this.DataContext = this;
            this.motherBorder = variableControlsBorder;
        }
        #endregion"Ctors"

        #region "Properties"
        private UCSignInWithMobileNumber SignInWithMobileNumber { get; set; }
        private UCFBAuthentication FBAuthentication { get; set; }
        private UCWaitForDigitVarification WaitForDigitVarification { get; set; }
        private UCTwitterUI TwitterUI { get; set; }
        private UCSignInWithEmail SignInWithEmail { get; set; }
        private UCSigninWithRingID SigninWithRingID { get; set; }
        public UCGuiRingID Instance { get; set; }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private string typeText;
        public string TypeText
        {
            get { return this.typeText; }
            set { this.typeText = value; this.OnPropertyChanged("TypeText"); }
        }

        private int btnShowType;
        public int BtnShowType
        {
            get { return btnShowType; }
            set
            {
                if (value == btnShowType) return;
                btnShowType = value; OnPropertyChanged("BtnShowType");
            }
        }
        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            getFocusOnUserControl();
            loadDefaultUI();
        }

        private ICommand expanCollaspViewCommand;
        public ICommand ExpanCollaspViewCommand
        {
            get
            {
                if (expanCollaspViewCommand == null) expanCollaspViewCommand = new RelayCommand(param => OnExpanCollaspView(param));
                return expanCollaspViewCommand;
            }
        }

        private void OnExpanCollaspView(object param)
        {
            int parseType = Convert.ToInt32(param);
            if (parseType == SettingsConstants.EMAIL_LOGIN) loadEmailUI();
            else if (parseType == SettingsConstants.RINGID_LOGIN) loadRingIDUI();
            else if (parseType == SettingsConstants.MOBILE_LOGIN) loadPhoneNubmerUI();
            else if (parseType == SettingsConstants.FACEBOOK_LOGIN) loadFBUI();
            else if (parseType == SettingsConstants.TWITTER_LOGIN) loadTWUI();
        }

        private ICommand backButtonCommand;
        public ICommand BackButtonCommand
        {
            get
            {
                if (backButtonCommand == null) backButtonCommand = new RelayCommand(param => OnBackButton(param));
                return backButtonCommand;
            }
        }

        private void OnBackButton(object param)
        {
            OnBackButtonClicked();
        }

        private ICommand enterKeyCommand;
        public ICommand EnterKeyCommand
        {
            get
            {
                if (enterKeyCommand == null) enterKeyCommand = new RelayCommand(param => OnEnterKeyCommand(param));
                return enterKeyCommand;
            }
        }
        private void OnEnterKeyCommand(object param)
        {
            loadRingIDUI();
        }

        #endregion"ICommands and Command Methods"

        #region"Utility Methods"
        private void loadDefaultUI()
        {
            if (DataModel.LoginType == SettingsConstants.EMAIL_LOGIN) loadEmailUI();
            else if (DataModel.LoginType == SettingsConstants.RINGID_LOGIN) loadRingIDUI();
            else if (DataModel.LoginType == SettingsConstants.MOBILE_LOGIN) loadPhoneNubmerUI();
        }

        private void loadFBUI()
        {
            hideExpandedPanels();
            if (FBAuthentication == null)
            {
                FBAuthentication = new UCFBAuthentication(SettingsConstants.TYPE_FROM_SIGNIN);
                FBAuthentication.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                FBAuthentication.SetParent(motherPanel);
                FBAuthentication.OnRemovedUserControl += () =>
                {
                    getFocusOnUserControl();
                    FBAuthentication = null;
                };
                FBAuthentication.OnFBAuthenticationCompleted += (typeToSwitch, fbName, fbProfileImage, fbId, accessToken, ringID, message) => OnFBAuthenticationCompleted(typeToSwitch, fbName, fbProfileImage, fbId, accessToken, ringID, message);
                FBAuthentication.Show();
            }
        }

        private void loadTWUI()
        {
            hideExpandedPanels();
            if (TwitterUI == null)
            {
                TwitterUI = new UCTwitterUI(SettingsConstants.TYPE_FROM_SIGNIN);
                TwitterUI.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                TwitterUI.SetParent(motherPanel);
                TwitterUI.OnRemovedUserControl += () =>
                {
                    getFocusOnUserControl();
                    TwitterUI = null;
                };
                TwitterUI.OnThreadCompleted += (typeToSwitch, screenName, profileImage, socialMediaID, inputToken, ringID, message) => OnTwitterFeedBack(typeToSwitch, screenName, profileImage, socialMediaID, inputToken, ringID, message);
                TwitterUI.Show();
            }
        }

        private void loadPhoneNubmerUI()
        {
            _SignInwithRingIDBorderPanel.Child = null;
            _SignInwithEmailBorderPanel.Child = null;

            if (_SignInwithPhoneBorderPanel.Child == null)
            {
                if (SignInWithMobileNumber == null)
                {
                    SignInWithMobileNumber = new UCSignInWithMobileNumber(DataModel, motherBorder);
                    SignInWithMobileNumber.OnBackButtonClicked += () =>
                    {
                        _SignInwithPhoneBorderPanel.Child = null;
                        getFocusOnUserControl();
                    };
                }
                _SignInwithPhoneBorderPanel.Child = SignInWithMobileNumber;
                BtnShowType = SettingsConstants.MOBILE_LOGIN;
            }
            else
            {
                BtnShowType = 0;
                _SignInwithPhoneBorderPanel.Child = null;
                getFocusOnUserControl();
            }
        }

        private void loadEmailUI()
        {
            _SignInwithRingIDBorderPanel.Child = null;
            _SignInwithPhoneBorderPanel.Child = null;
            if (_SignInwithEmailBorderPanel.Child == null)
            {
                if (SignInWithEmail == null)
                {
                    SignInWithEmail = new UCSignInWithEmail(DataModel, motherBorder);
                    SignInWithEmail.OnBackButtonClicked += () =>
                    {
                        _SignInwithEmailBorderPanel.Child = null;
                        getFocusOnUserControl();
                    };
                }
                _SignInwithEmailBorderPanel.Child = SignInWithEmail;
                BtnShowType = SettingsConstants.EMAIL_LOGIN;
            }
            else
            {
                BtnShowType = 0;
                _SignInwithEmailBorderPanel.Child = null;
                getFocusOnUserControl();
            }
        }

        private void loadRingIDUI()
        {
            _SignInwithPhoneBorderPanel.Child = null;
            _SignInwithEmailBorderPanel.Child = null;
            if (_SignInwithRingIDBorderPanel.Child == null)
            {
                if (SigninWithRingID == null)
                {
                    SigninWithRingID = new UCSigninWithRingID(DataModel, motherPanel, motherBorder);
                    SigninWithRingID.OnBackButtonClicked += () =>
                    {
                        _SignInwithRingIDBorderPanel.Child = null;
                        getFocusOnUserControl();
                    };
                }
                BtnShowType = SettingsConstants.RINGID_LOGIN;
                _SignInwithRingIDBorderPanel.Child = SigninWithRingID;
            }
            else
            {
                BtnShowType = 0;
                _SignInwithRingIDBorderPanel.Child = null;
                getFocusOnUserControl();
            }
        }

        private void showGuiRingID()
        {

        }

        private void getFocusOnUserControl()
        {
            this.FocusVisualStyle = null;
            this.Focusable = true;
            Keyboard.Focus(this);
            BtnShowType = 0;
        }

        private void OnFBAuthenticationCompleted(int typeToSwitch, string facebookName, string facebookProfileImage, string fbID, string accessToken, string ringID = null, string messag = null)
        {
            if (typeToSwitch == WelcomePanelConstants.TypeSigninSuccess) UIHelperMethods.ShowMainUIAfterLogin();
            else if (typeToSwitch == WelcomePanelConstants.TypeSigninFaild) { if (!string.IsNullOrEmpty(messag)) DataModel.ShowErrorMessage(messag); }
            else if (!string.IsNullOrEmpty(messag)) DataModel.ShowErrorMessage(messag);
        }

        private void OnTwitterFeedBack(int typeToSwitch, string screenName, string profileImage, string socialMediaID, string inputToken, string ringID, string messag = null)
        {
            if (typeToSwitch == WelcomePanelConstants.TypeSigninSuccess) UIHelperMethods.ShowMainUIAfterLogin();
            else if (typeToSwitch == WelcomePanelConstants.TypeSigninFaild) { if (!string.IsNullOrEmpty(messag)) DataModel.ShowErrorMessage(messag); }
            else if (!string.IsNullOrEmpty(messag)) DataModel.ShowErrorMessage(messag);
        }

        private void hideExpandedPanels()
        {
            BtnShowType = 0;
            _SignInwithRingIDBorderPanel.Child = null;
            _SignInwithEmailBorderPanel.Child = null;
            _SignInwithPhoneBorderPanel.Child = null;
            getFocusOnUserControl();
        }

        #endregion"Utility Methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
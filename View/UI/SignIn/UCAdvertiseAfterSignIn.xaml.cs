﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.Constants;
using View.Utility;

namespace View.UI.SignIn
{
    /// <summary>
    /// Interaction logic for UCAdvertiseAfterSignIn.xaml
    /// </summary>
    public partial class UCAdvertiseAfterSignIn : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCAdvertiseAfterSignIn).Name);
        private BitmapImage IMAGE_1;
        private BitmapImage IMAGE_2;
        private BitmapImage IMAGE_3;

        private DispatcherTimer timerImageChange;
        private Image[] ImageControls;
        private List<ImageSource> Images = new List<ImageSource>();
        private static string[] TransitionEffects = new[] { "Fade" };
        private string TransitionType;
        private int CurrentSourceIndex, CurrentCtrlIndex, EffectIndex = 0, IntervalTimer = 3;
        private bool IsFirstTime = false;

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Property

        private bool _Visible;
        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (value == _Visible)
                    return;

                _Visible = value;
                this.OnPropertyChanged("Visible");
            }
        }
        private ImageSource _myImageSource;
        public ImageSource MyImageSource
        {
            get
            {
                return _myImageSource;
            }
            set
            {
                if (value == _myImageSource)
                    return;
                _myImageSource = value;
                OnPropertyChanged("MyImageSource");
            }
        }
        #endregion

        public UCAdvertiseAfterSignIn()
        {
            InitializeComponent();
            this.DataContext = this;

            ImageControls = new[] { myImage, myImage2 };

        }

        private ICommand _OnCloseCommand;
        public ICommand OnCloseCommand
        {
            get
            {
                if (_OnCloseCommand == null)
                {
                    _OnCloseCommand = new RelayCommand((param) => Hide(param));
                }
                return _OnCloseCommand;
            }
        }

        #region Utility Method


        public void Show()
        {
            this.IsVisibleChanged += UCAdvertiseAfterSignIn_IsVisibleChanged;
            this.Visible = true;
        }
        public void Hide(object param = null)
        {
            Visible = false;
        }

        private void timerImageChange_Tick(object sender, EventArgs e)
        {
            PlaySlideShow();
            timerImageChange.Interval = new TimeSpan(0, 0, IntervalTimer);
        }

        private void PlaySlideShow()
        {
            try
            {
                if (Images.Count == 0)
                    return;

                if (IsFirstTime)
                {
                    Image imgFadeIn = ImageControls[CurrentCtrlIndex];
                    ImageSource newSource = Images[CurrentCtrlIndex];
                    imgFadeIn.Source = newSource;
                    TransitionType = TransitionEffects[EffectIndex].ToString();
                    Storyboard StboardFadeIn = Resources[string.Format("{0}In", TransitionType.ToString())] as Storyboard;
                    StboardFadeIn.Begin(imgFadeIn);
                    IsFirstTime = false;
                }
                else
                {
                    var oldCtrlIndex = CurrentCtrlIndex;
                    CurrentCtrlIndex = (CurrentCtrlIndex + 1) % 2;
                    CurrentSourceIndex = (CurrentSourceIndex + 1) % Images.Count;

                    Image imgFadeOut = ImageControls[oldCtrlIndex];
                    Image imgFadeIn = ImageControls[CurrentCtrlIndex];
                    ImageSource newSource = Images[CurrentSourceIndex];
                    imgFadeIn.Source = newSource;

                    TransitionType = TransitionEffects[EffectIndex].ToString();

                    Storyboard StboardFadeOut = (Resources[string.Format("{0}Out", TransitionType.ToString())] as Storyboard).Clone();
                    StboardFadeOut.Begin(imgFadeOut);
                    Storyboard StboardFadeIn = Resources[string.Format("{0}In", TransitionType.ToString())] as Storyboard;
                    StboardFadeIn.Begin(imgFadeIn);
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Hide()
        {
            Visible = false;
        }

        void UCAdvertiseAfterSignIn_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue == false)
                {
                    this.IsVisibleChanged -= UCAdvertiseAfterSignIn_IsVisibleChanged;
                    timerImageChange.Stop();
                    timerImageChange = null;
                    IsFirstTime = false;

                    Images.Clear();
                    GC.SuppressFinalize(this.IMAGE_1);
                    GC.SuppressFinalize(this.IMAGE_2);
                    GC.SuppressFinalize(this.IMAGE_3);

                    this.IMAGE_1 = null;
                    this.IMAGE_2 = null;
                    this.IMAGE_3 = null;

                    //if ( !UCGuiRingID.Instance.ShowRightPanel)
                    //{
                    //    Mouse.OverrideCursor = Cursors.Wait;
                    //}
                }
                else if ((bool)e.NewValue == true)
                {
                    this.IMAGE_1 = ImageUtility.GetBitmapImage(ImageLocation.LOGIN_DEFAULT_IMAGE_1);
                    this.IMAGE_2 = ImageUtility.GetBitmapImage(ImageLocation.LOGIN_DEFAULT_IMAGE_2);
                    this.IMAGE_3 = ImageUtility.GetBitmapImage(ImageLocation.LOGIN_DEFAULT_IMAGE_3);

                    Images.Clear();
                    Images.Add(this.IMAGE_1);
                    Images.Add(this.IMAGE_2);
                    Images.Add(this.IMAGE_3);

                    IsFirstTime = true;
                    timerImageChange = new DispatcherTimer();
                    timerImageChange.Interval = new TimeSpan(0, 0, 0);
                    timerImageChange.Tick += new EventHandler(timerImageChange_Tick);
                    timerImageChange.Start();
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: UCAdvertiseAfterSignIn_IsVisibleChanged() => " + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

            }
        }
        #endregion
    }
}

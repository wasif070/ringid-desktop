﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shell;
using System.Windows.Threading;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.Constants;
using View.UI.Chat;
using View.UI.FriendList;
using View.UI.PopUp;
using View.UI.RingIDMainUI;
using View.UI.SignIn;
using View.Utility;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.RingMarket;
using View.Utility.SignInSignUP;
using View.ViewModel;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for UCGuiRingID.xaml
    /// </summary>
    public partial class UCGuiRingID : UserControl, INotifyPropertyChanged
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(UCGuiRingID).Name);
        //private ThrdCheckForUpdates thrdCheckForUpdates;
        private int _LeftPanelWidth;
        private int _RightPanelWidth;
        private Storyboard _WindowStoryboard = null;
        private DispatcherTimer _WindowDispatcherTimer = null;
        private DispatcherTimer _MinWidthDispatcherTimer = null;
        private bool _ShowSearchFriend = false;
        private Timer SearchTextBoxTimer;
        private int ThreadMaxRunningTime = 0;
        public Grid motherGridToShowPopup = null;
        private UCRightMainButtons ucRightMainButtons;
        #endregion "Private Fields"

        #region "Public Fields"
        public readonly object _syncRoot = new object();
        public UCFriendTabControlPanel ucFriendTabControlPanel;
        public UCFriendSearchListPanel ucFriendSearchListPanel;
        public UCAdvertiseAfterSignIn View_UCAdvertiseAfterSignIn { get; set; }
        public bool IsSignedInBackground = false;
        #endregion "Public Fields"

        #region "Constructors"

        public UCGuiRingID(Grid motherGridToShowPopup = null)
        {
            InitializeComponent();
            LeftPanelWidth = RingIDSettings.LEFT_FRIEND_LIST_WIDTH;
            RightPanelWidth = RingIDSettings.RIGHT_PANEL_WIDTH;
            DataContext = this;
            if (motherGridToShowPopup != null) this.motherGridToShowPopup = motherGridToShowPopup;
            if (ucFriendTabControlPanel == null) ucFriendTabControlPanel = new UCFriendTabControlPanel();
            LeftBorderPanel.Child = ucFriendTabControlPanel;
            DataModel = RingIDViewModel.Instance.WinDataModel;
            //   this.Loaded += UCGuiRingID_Loaded;
            if (ucRightMainButtons == null) ucRightMainButtons = new UCRightMainButtons();
            RightBarGrid.Children.Add(ucRightMainButtons);
        }

        #endregion "Constructors"

        #region "Properties"
        public static UCGuiRingID Instance { get; set; }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        public int LeftPanelWidth
        {
            get { return this._LeftPanelWidth; }
            set { this._LeftPanelWidth = value; this.OnPropertyChanged("LeftPanelWidth"); }
        }

        public int RightPanelWidth
        {
            get { return this._RightPanelWidth; }
            set { this._RightPanelWidth = value; this.OnPropertyChanged("RightPanelWidth"); }
        }

        private ImageSource _popupLikeLoader;
        public ImageSource PopupLikeLoader
        {
            get { return _popupLikeLoader; }
            set
            {
                if (value == _popupLikeLoader) return;
                _popupLikeLoader = value; OnPropertyChanged("PopupLikeLoader");
            }
        }

        public bool ShowSearchFriend
        {
            get { return _ShowSearchFriend; }
            set
            {
                if (value == _ShowSearchFriend) return;
                _ShowSearchFriend = value; OnPropertyChanged("ShowSearchFriend");
            }
        }

        private string _SearchString;
        public string SearchString
        {
            get { return _SearchString; }
            set
            {
                _SearchString = value;
                SetDefaultParameter();
                if (!string.IsNullOrWhiteSpace(_SearchString))
                {
                    if (ucFriendSearchListPanel == null) ucFriendSearchListPanel = new UCFriendSearchListPanel();
                    LeftBorderPanel.Child = ucFriendSearchListPanel;
                    OnSearch();
                }
                else
                {
                    ThreadLeftSearchFriend.searchParm = null;
                    if (ucFriendTabControlPanel == null) ucFriendTabControlPanel = new UCFriendTabControlPanel();
                    LeftBorderPanel.Child = ucFriendTabControlPanel;
                    ClearTempList();
                }
            }
        }

        public UCUploadingPopup ucUploadingPopup
        {
            get { return MainSwitcher.PopupController.ucUploadingPopup; }
        }
        public UCStatusPopup ucStatusPopup { get; set; }

        public Grid MotherPanel { get { if (this.motherGridToShowPopup == null) return _MotherPanel; else return this.motherGridToShowPopup; } }

        #endregion "Properties"

        #region "Commands and Command Methods"

        private ICommand loadedMainControl;
        public ICommand LoadedMainControl
        {
            get
            {
                if (loadedMainControl == null) loadedMainControl = new RelayCommand(param => OnLoadedMainControl());
                return loadedMainControl;
            }
        }
        private void OnLoadedMainControl()
        {
            NetworkChange.NetworkAvailabilityChanged += NetworkAvailabilityChanged;
            RingIDViewModel.Instance.ReloadMyProfile();
            loadMyData();

            if (MiddlePanelSwitcher.pageSwitcher == null)
            {
                MiddlePanelSwitcher.pageSwitcher = new UCMiddlePanelSwitcher();
                Grid.SetRow(MiddlePanelSwitcher.pageSwitcher, 2);
                middlePanel.Children.Add(MiddlePanelSwitcher.pageSwitcher);
            }
            else { middlePanel.Children.Add(MiddlePanelSwitcher.pageSwitcher); }
            //if (thrdCheckForUpdates == null) thrdCheckForUpdates = new ThrdCheckForUpdates();
            //thrdCheckForUpdates.StartChecking(false);
        }
        #endregion

        #region "Event Trigger"

        private void NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            DefaultSettings.IS_NETWORK_INTERFACE_AVAILABLE = HelperMethods.IsNetworkConnectionAvailable();
            log.Error("CONNECTION CHANGED");
            if (!DefaultSettings.IS_NETWORK_INTERFACE_AVAILABLE)
            {
                log.Error("CONNECTION CHANGED");
                MainSwitcher.ThreadManager().StartNetworkThread();
            }
        }

        private void StatusMode_ButtonClick(object sender, RoutedEventArgs e)
        {
            Button bt = (Button)sender;
            UIElement uie = (UIElement)bt;
            if (ucStatusPopup != null) MotherPanel.Children.Remove(ucStatusPopup);
            ucStatusPopup = new UCStatusPopup(MotherPanel);
            ucStatusPopup.Show();
            ucStatusPopup.ShowStatusPopup(uie);
            ucStatusPopup.OnRemovedUserControl += () =>
            {
                ucStatusPopup.popupStatus.IsOpen = false;
                ucStatusPopup = null;
            };
        }

        //private void UCMiddlePanelSwitcher_Loaded(object sender, RoutedEventArgs e)
        //{
        //    if (thrdCheckForUpdates == null) thrdCheckForUpdates = new ThrdCheckForUpdates();
        //    thrdCheckForUpdates.StartChecking(false);
        //}

        void SearchTextBoxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                if (string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString) && !ThreadLeftSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        ucFriendSearchListPanel.Scroll.ScrollToHome();
                    });
                    ThreadLeftSearchFriend.searchParm = SearchString.ToLower();
                    MainSwitcher.ThreadManager().LeftSearchFriend.StartThread(ThreadLeftSearchFriend.SearchFromLeftPanel, StatusConstants.SEARCH_BY_ALL);
                }
            }
            else ClearTempListFromTimer();
            ThreadMaxRunningTime += 500;
            if (ThreadMaxRunningTime >= 3000)
            {
                ThreadMaxRunningTime = 0;
                if (SearchTextBoxTimer != null)
                {
                    SearchTextBoxTimer.Enabled = false;
                    SearchTextBoxTimer.Stop();
                }
            }
        }

        private void SearchTermTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Back && (string.IsNullOrWhiteSpace(SearchTermTextBox.Text) || SearchTermTextBox.Text.Length == 0))
            {
                RingIDViewModel.Instance.TempFriendSearchList.Clear();
            }
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.Text = null;
            RingIDViewModel.Instance.TempFriendSearchList.Clear();
            SearchTermTextBox.Focus();
        }

        private void Storyboard_Completed(object sender, EventArgs args)
        {
            if (_WindowStoryboard != null)
            {
                _WindowStoryboard.Completed -= Storyboard_Completed;
                _WindowStoryboard = null;
            }

            if (RingIDViewModel.Instance.IsExpanded)
            {
                if (_MinWidthDispatcherTimer == null)
                {
                    _MinWidthDispatcherTimer = new DispatcherTimer();
                    _MinWidthDispatcherTimer.Interval = TimeSpan.FromSeconds(0.10);
                    _MinWidthDispatcherTimer.Tick += (s, e) =>
                    {
                        //MainSwitcher.MainController().MainUIController().MainWindow.WindowMinWidth = RingIDViewModel.Instance.IsExpanded ? RingIDSettings.FRAME_DEFAULT_WIDTH + RingIDSettings.RIGHT_PANEL_WIDTH : RingIDSettings.FRAME_DEFAULT_WIDTH;
                        RingIDMainWindow.WindowMinWidth = RingIDViewModel.Instance.IsExpanded ? RingIDSettings.FRAME_DEFAULT_WIDTH + RingIDSettings.RIGHT_PANEL_WIDTH : RingIDSettings.FRAME_DEFAULT_WIDTH;
                        _MinWidthDispatcherTimer.Stop();
                    };
                }
                _MinWidthDispatcherTimer.Stop();
                _MinWidthDispatcherTimer.Start();
            }
        }

        #endregion "Event Trigger"

        #region "Private methods"
        private void AddNewTaskIntoJumplist()
        {
            string cmdPath = Assembly.GetEntryAssembly().Location;
            JumpTask jumpTask1 = new JumpTask();
            jumpTask1.ApplicationPath = cmdPath;
            jumpTask1.IconResourcePath = cmdPath;
            jumpTask1.Title = "Sign Out";
            jumpTask1.Arguments = StartUpConstatns.ARGUMENT_RESTART_FROM_ANOTHER_PROCESS;
            JumpList jumpList = JumpList.GetJumpList(App.Current);
            if (jumpList != null)
            {
                jumpList.JumpItems.Insert(0, jumpTask1);
                JumpList.AddToRecentCategory(jumpTask1);
                jumpList.Apply();
            }
            else
            {
                JumpTask jumpTask2 = new JumpTask();
                jumpTask2.ApplicationPath = cmdPath;
                jumpTask2.IconResourcePath = cmdPath;
                jumpTask2.Title = "Quit ringID";
                jumpTask2.Arguments = StartUpConstatns.ARGUMENT_EXIT_FROM_ANOTHER_PROCESS;

                jumpList = new JumpList();
                jumpList.JumpItems.Add(jumpTask1);
                jumpList.JumpItems.Add(jumpTask2);
                JumpList.SetJumpList(App.Current, jumpList);
            }
        }

        private void SetDefaultParameter()
        {
            ThreadLeftSearchFriend.processingList = false;
            ThreadLeftSearchFriend.SearchForShowMore = false;
            ThreadLeftSearchFriend.totalSearchCount = 0;
            ThreadLeftSearchFriend.friendCountInOneRequest = 0;
            if (ucFriendSearchListPanel != null)
            {
                ucFriendSearchListPanel.SelectedFriendUserTableId = 0;
                ucFriendSearchListPanel.HideShowMorePanel();
                ucFriendSearchListPanel.NoResultsFound = false;
            }
        }

        private void ClearTempList()
        {
            if (RingIDViewModel.Instance.TempFriendSearchList != null) RingIDViewModel.Instance.TempFriendSearchList.Clear();
        }

        private void OnSearch()
        {
            try
            {
                if (string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThreadLeftSearchFriend.prevString) && !ThreadLeftSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    ClearTempList();
                    ThreadMaxRunningTime = 0;
                    if (SearchTextBoxTimer == null) SearchTextBoxTimer = new Timer();
                    SearchTextBoxTimer.Elapsed -= SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Elapsed += SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Interval = 500;
                    SearchTextBoxTimer.AutoReset = true;
                    SearchTextBoxTimer.Enabled = true;
                    SearchTextBoxTimer.Stop();
                    SearchTextBoxTimer.Start();
                }
            }
            catch (Exception ex) { log.Error(ex.StackTrace + " " + ex.Message); }
        }

        private void ClearTempListFromTimer()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                lock (RingIDViewModel.Instance.TempFriendSearchList) RingIDViewModel.Instance.TempFriendSearchList.Clear();
            });
        }

        private void UploadingDetailsClick(object sender, MouseButtonEventArgs e)
        {
            ucUploadingPopup.Show();
            ucUploadingPopup.ShowUploadingPopup(Down_Arrow);
        }

        public WNRingIDMain RingIDMainWindow
        {
            get
            {
                if (motherGridToShowPopup != null)
                {
                    var obj = motherGridToShowPopup.Parent;
                    if (obj != null && obj is WNRingIDMain) return (WNRingIDMain)obj;
                }
                return null;
            }
        }

        private void loadMyData()
        {
            Task loadData = new Task(delegate
            {
                DefaultSettings.IS_NETWORK_INTERFACE_AVAILABLE = HelperMethods.IsNetworkConnectionAvailable();
                LoadSavedDataFromLocal loadSavedDataFromLocal = new LoadSavedDataFromLocal();
                loadSavedDataFromLocal.FetchAllCotactsFromDB();
                loadSavedDataFromLocal.MakeFriendsModelsFromDTO();
                loadSavedDataFromLocal.MakeSeparateContactsDTOLists();
                loadSavedDataFromLocal.FetchOtherSavedInfo();
                StorageFeeds.RetriveLastTwentyFeeds();

                if (!DataModel.IsSignIn)
                {
                    SignedInInfoDTO singinDto = new SigninRequest().SignInRequestBackground();
                    if (singinDto != null)
                    {
                        if (singinDto.gotResponseFromServer)
                        {
                            if (!singinDto.isSuccess)
                            {
                                if (singinDto.isDownloadMandatory)
                                {
                                    HelperMethods.DownloadMandatoryUpdater(singinDto.versionMsg);
                                    return;
                                }
                                else
                                {
                                    System.Diagnostics.Process.Start(Application.ResourceAssembly.Location, StartUpConstatns.ARGUMENT_RESTART + " " + StartUpConstatns.ARGUMENT_SIGNIN_FAILD + " " + DefaultSettings.VALUE_LOGIN_USER_TYPE);//"-restart -sessioninvalid DefaultSettings.VALUE_LOGIN_USER_TYPE"
                                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                                }
                            }
                        }
                    }
                }
                DataModel.IsLoadingCotactList = false;
                Models.Constants.DefaultSettings.FRIEND_LIST_LOADED_FROM_DB = true;
                if (DefaultSettings.userProfile == null) DefaultSettings.userProfile = new UserBasicInfoDTO();
                DefaultSettings.userProfile.FriendShipStatus = -1;
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    try
                    {
                        if (RingIDMainWindow != null)
                        {
                            RingIDMainWindow.SetRingIDasToolTipText();
                            RingIDMainWindow.AddActivateEvent();
                        }
                        AddNewTaskIntoJumplist();
                    }
                    finally { }
                });

                try
                {
                    View.Utility.Call.CallHelperMethods.InitCallSDK();
                    HelperMethods.BuildGroupMemberList();
                    if (DataModel.IsSignIn && DefaultSettings.IsInternetAvailable)
                    {
                        ThreadContactUtIdsRequest contactUtIdsRequest = new ThreadContactUtIdsRequest();
                        contactUtIdsRequest.Run();
                        if (ThreadContactUtIdsRequest._TotalRecords > 20)
                        {
                            DataModel.IsLoadingCotactList = true;
                            System.Threading.Thread.Sleep(10000);// // wait 10 secs before other request
                        }
                        RingMarketStickerLoadUtility.RunRecentCat(StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.ToList());
                        if (DefaultSettings.IsInternetAvailable) HelperMethods.SendOtherRequestToserver();
                    }
                    else
                    {
                        DataModel.IsLoadingCotactList = false;
                        MainSwitcher.ThreadManager().StartNetworkThread();
                    }
                    FriendListLoadUtility.AddFriendsNotificationForIncomingRequest();
                }
                finally { }
            });
            loadData.Start();
        }

        #endregion "Private methods"

        #region "Public Methods"

        public bool IsAnyWindowAbove()
        {
            bool IsSettingsWindowOpen = (RingIDViewModel.Instance._WNRingIDSettings != null && RingIDViewModel.Instance._WNRingIDSettings.IsActive);
            bool IsChatWindowOpen = WNChatView.ChatViewActivated();
            return (IsSettingsWindowOpen || IsChatWindowOpen);
        }

        public void DoExpandCollapseWindow(object param)
        {
            double delay = 0.25;
            if (param != null) delay = (double)param;
            if (_WindowStoryboard != null)
            {
                _WindowStoryboard.Stop();
                _WindowStoryboard.Completed -= Storyboard_Completed;
                _WindowStoryboard = null;
            }

            if (_WindowDispatcherTimer != null)
            {
                _WindowDispatcherTimer.Stop();
            }
            double windowSource = RingIDMainWindow.WindowState == WindowState.Maximized ? RingIDMainWindow.Width : RingIDMainWindow.ActualWidth;
            double stickerSource = pnlStickerContainer.Width;
            double windowTarget = 0;
            double stickerTarget = 0;

            if (RingIDViewModel.Instance.IsExpanded)
            {
                RingIDViewModel.Instance.IsExpanded = false;
                windowTarget = Math.Max(windowSource - RingIDSettings.RIGHT_PANEL_WIDTH, RingIDSettings.FRAME_DEFAULT_WIDTH);
                stickerSource = pnlStickerContainer.Width;
            }
            else
            {
                RingIDViewModel.Instance.IsExpanded = true;
                windowTarget = windowSource + Math.Max(RingIDSettings.FRAME_DEFAULT_WIDTH + RingIDSettings.RIGHT_PANEL_WIDTH - windowSource, 0);
                stickerTarget = RingIDSettings.RIGHT_PANEL_WIDTH;
            }

            if (delay == 0)
            {
                if (_WindowDispatcherTimer == null)
                {
                    _WindowDispatcherTimer = new DispatcherTimer();
                    _WindowDispatcherTimer.Interval = TimeSpan.FromSeconds(0.25);
                    _WindowDispatcherTimer.Tick += (s, e) =>
                    {
                        this.AnimateResizeWindow(windowSource, windowTarget, stickerSource, stickerTarget, delay);
                        _WindowDispatcherTimer.Stop();
                        _WindowDispatcherTimer.Interval = TimeSpan.FromSeconds(0.125);
                    };
                }
                _WindowDispatcherTimer.Stop();
                _WindowDispatcherTimer.Start();
            }
            else
            {
                if (RingIDMainWindow.WindowState != WindowState.Maximized)
                {
                    double diff = Math.Abs(windowSource - windowTarget);
                    double frac = delay / 4;
                    if (diff <= 60) delay = frac * 1;
                    else if (diff <= 120) delay = frac * 2;
                }
                this.AnimateResizeWindow(windowSource, windowTarget, stickerSource, stickerTarget, delay);
            }
        }

        public void AnimateResizeWindow(double windowSource, double windowTarget, double stickerSource, double stickerTarget, double delay)
        {
            Application.Current.Dispatcher.BeginInvoke((Action)(() =>
            {
                //if (!RingIDViewModel.Instance.IsExpanded) MainSwitcher.MainController().MainUIController().MainWindow.WindowMinWidth = RingIDSettings.FRAME_DEFAULT_WIDTH;
                if (!RingIDViewModel.Instance.IsExpanded) RingIDMainWindow.WindowMinWidth = RingIDSettings.FRAME_DEFAULT_WIDTH;

                DoubleAnimation windowAnimation = new DoubleAnimation();
                windowAnimation.From = windowSource;
                windowAnimation.To = windowTarget;
                windowAnimation.Duration = TimeSpan.FromSeconds(delay);
                DoubleAnimation stickerAnimation = new DoubleAnimation();
                stickerAnimation.From = stickerSource;
                stickerAnimation.To = stickerTarget;
                stickerAnimation.Duration = TimeSpan.FromSeconds(delay);
                _WindowStoryboard = new Storyboard();
                _WindowStoryboard.Children.Add(windowAnimation);
                _WindowStoryboard.Children.Add(stickerAnimation);

                //Storyboard.SetTarget(windowAnimation, MainSwitcher.MainController().MainUIController().MainWindow);
                Storyboard.SetTarget(windowAnimation, RingIDMainWindow);
                Storyboard.SetTargetProperty(windowAnimation, new PropertyPath(StackPanel.WidthProperty));
                Storyboard.SetTarget(stickerAnimation, pnlStickerContainer);
                Storyboard.SetTargetProperty(stickerAnimation, new PropertyPath(StackPanel.WidthProperty));
                _WindowStoryboard.Completed += Storyboard_Completed;
                //_WindowStoryboard.Begin(MainSwitcher.MainController().MainUIController().MainWindow);
                _WindowStoryboard.Begin(RingIDMainWindow);
                _WindowStoryboard.Begin(pnlStickerContainer);
            }), DispatcherPriority.ApplicationIdle);
        }

        public void AddUserControlInMainGrid(UIElement control)
        {
            MotherPanel.Children.Add(control);
        }

        public void AddUserControlInMainGrid(UIElement control, int zIndex)
        {
            MotherPanel.Children.Add(control);
            Panel.SetZIndex(control, zIndex);
        }

        public void RemoveUserControlFromMainGrid(UIElement control)
        {
            MotherPanel.Children.Remove(control);
        }

        public void AddIntoUpperMiddlePanel(UserControl control)
        {
            middleNotificationPanel.Visibility = Visibility.Visible;
            middleNotificationPanel.Children.Clear();
            middleNotificationPanel.Children.Add(control);
        }

        public void ClearUpperMiddlePanel()
        {
            middleNotificationPanel.Visibility = Visibility.Collapsed;
            middleNotificationPanel.Children.Clear();
        }

        public void AddUserControlInMiddlePanel(UIElement control)
        {
            Grid.SetRow(control, 0);
            Grid.SetRowSpan(control, 3);
            middlePanel.Children.Add(control);
        }

        public void RemoveUserControlFromMiddlePanel(UIElement control)
        {
            middlePanel.Children.Remove(control);
        }

        public void ShowAdvertiseSlider()
        {
            if (View_UCAdvertiseAfterSignIn == null)
            {
                View_UCAdvertiseAfterSignIn = new UCAdvertiseAfterSignIn();
                AddUserControlInMainGrid(View_UCAdvertiseAfterSignIn);
                View_UCAdvertiseAfterSignIn.Show();
            }
        }

        public void HideAdvertiseSlider()
        {
            if (View_UCAdvertiseAfterSignIn != null)
            {
                View_UCAdvertiseAfterSignIn.Hide();
                View_UCAdvertiseAfterSignIn = null;
            }
        }
        #endregion "Public methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

﻿using log4net;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for UCMediaAlbumContentsView.xaml
    /// </summary>
    public partial class UCMediaAlbumsView : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCMediaAlbumsView).Name);
        
        public static UCMediaAlbumsView Instance = null;
        public Func<int> OnBackToPrevious = null;

        #region Constructor

        public UCMediaAlbumsView()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property

        private int _mediaType;
        public int MediaType
        {
            get
            {
                return _mediaType;
            }
            set
            {
                _mediaType = value;
                this.OnPropertyChanged("MediaType");
            }
        }

        private bool _viewListMusic = false;
        public bool ViewListMusic
        {
            get 
            { 
                return _viewListMusic; 
            }
            set
            {
                if (value == _viewListMusic)
                {
                    return;
                }
                _viewListMusic = value;
                this.OnPropertyChanged("ViewListMusic");
            }
        }

        #endregion

        #region ICommand

        private ICommand _backCommand;
        public ICommand BackCommand
        {
            get
            {
                _backCommand = _backCommand ?? new RelayCommand(param => onBackCommandClicked(param));
                return _backCommand;
            }
        }

        #endregion

        #region Events

        private void list_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: list_PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (OnBackToPrevious != null)
                {
                    OnBackToPrevious();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error : backButton_Click() == > " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void musicAlbumListViewBtn_Click(object sender, RoutedEventArgs e)
        {
            ViewListMusic = ViewListMusic ? false : true;
        }

        private void SingleAlbum_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Grid grid = sender as Grid;
                if (grid.DataContext is MediaContentModel)
                {
                    MediaContentModel model = (MediaContentModel)grid.DataContext;
                    albumsGrd.Visibility = Visibility.Collapsed;
                    mediaContentsBdr.Visibility = Visibility.Visible;
                    if (UCMediaContentsView.Instance == null)
                    {
                        UCMediaContentsView.Instance = new UCMediaContentsView();
                    }
                    else if (UCMediaContentsView.Instance.Parent is Border)
                    {
                        ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                    }
                    mediaContentsBdr.Child = UCMediaContentsView.Instance;
                    UCMediaContentsView.Instance.ShowAlbumContents(model, () =>
                    {
                        backtoAlbumsResults();
                        return 0;
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        private void SingleAlbum_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Grid grid = (Grid)sender;
            if (grid.DataContext is MediaContentModel)
            {
                MediaContentModel model = (MediaContentModel)grid.DataContext;
                if (grid.ContextMenu != ContextMenuAlbumEditOptions.Instance.cntxMenu)
                {
                    grid.ContextMenu = ContextMenuAlbumEditOptions.Instance.cntxMenu;
                    ContextMenuAlbumEditOptions.Instance.SetupMenuItem();
                }
                ContextMenuAlbumEditOptions.Instance.ShowHandler(grid, model);
                ContextMenuAlbumEditOptions.Instance.cntxMenu.IsOpen = true;
            }
        }

        #endregion

        #region Public Methods

        public void ShowAlbumContents(int type, Func<int> onBackToPrevious)
        {
            this.MediaType = type;
            this.OnBackToPrevious = onBackToPrevious;
        }

        #endregion

        #region Private Methods

        private void onBackCommandClicked(object parameter)
        {
            try
            {
                if (OnBackToPrevious != null)
                {
                    OnBackToPrevious();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        private void backtoAlbumsResults()
        {
            albumsGrd.Visibility = Visibility.Visible;
            mediaContentsBdr.Visibility = Visibility.Collapsed;
        }

        #endregion
    }
}

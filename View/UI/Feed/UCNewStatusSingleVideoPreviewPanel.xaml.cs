﻿using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.Utility;
using View.Utility.RingPlayer;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCNewStatusSingleVideoPreviewPanel.xaml
    /// </summary>
    public partial class UCNewStatusSingleVideoPreviewPanel : UserControl
    {
        NewStatusViewModel newStatusViewModel = new NewStatusViewModel();
        public UCNewStatusSingleVideoPreviewPanel()
        {
            InitializeComponent();
        }

        private void CrossButton_Click(object sender, RoutedEventArgs e)
        {
            if (newStatusViewModel != null && newStatusViewModel.IsRtbNewStatusAreaEnabled)
            {
                Control control = sender as Control;
                FeedVideoUploaderModel model = (FeedVideoUploaderModel)control.DataContext;
                newStatusViewModel.NewStatusVideoUpload.Remove(model);
                if (newStatusViewModel.NewStatusVideoUpload.Count == 0)
                {
                    if (newStatusViewModel.NewStatusHashTag.Count > 0)
                    {
                        newStatusViewModel.ClearNewStatusHashTagsExceptPlaceHolder();
                    }
                    newStatusViewModel.VideoPanelVisibility = Visibility.Collapsed;
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Collapsed;
                    newStatusViewModel.ActionRemoveLinkPreview();
                    newStatusViewModel.VideoAlbumTitleTextBoxText = string.Empty;
                    if (newStatusViewModel.ucMediaCloudUploadPopup != null)
                    {
                        newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(0);
                    }
                    newStatusViewModel.AddtoAudioAlbumButtonVisibility = Visibility.Visible;
                    newStatusViewModel.SelectedVideoAlbumCrossButtonVisibility = Visibility.Collapsed;
                    newStatusViewModel.PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                    newStatusViewModel.IsPrivacyButtonEnabled = true;
                }
            }
        }

        private void textBoxAudio_Loaded(object sender, RoutedEventArgs e)
        {
            textBoxAudio.Focus();
            textBoxAudio.CaretIndex = textBoxAudio.Text.Length;
            newStatusViewModel = (NewStatusViewModel)this.Tag;
            if (newStatusViewModel.FeedWallType < 0)
            {
                ImageUploadCrossButton.Visibility = Visibility.Hidden;
                textBoxAudio.IsEnabled = false;
            }
        }

        private void videoPanel_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Grid b = (Grid)sender;
            NewStatusViewModel newStatusViewModel = (NewStatusViewModel)this.Tag;
            FeedVideoUploaderModel item = (FeedVideoUploaderModel)b.DataContext;
            int index = newStatusViewModel.NewStatusVideoUpload.IndexOf(item);

            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
            foreach (FeedVideoUploaderModel singleModel in newStatusViewModel.NewStatusVideoUpload)
            {
                SingleMediaModel singleMediamodel = new SingleMediaModel();
                singleMediamodel.StreamUrl = singleModel.FilePath;
                singleMediamodel.Title = singleModel.VideoTitle;
                singleMediamodel.Duration = singleModel.VideoDuration;
                singleMediamodel.Artist = singleModel.VideoArtist;
                singleMediamodel.MediaType = 2;
                singleMediamodel.ThumbUrl = singleModel.ThumbUrl;
                singleMediamodel.IsFromLocalDirectory = true;
                MediaList.Add(singleMediamodel);
            }
            MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, index);
        }
    }
}

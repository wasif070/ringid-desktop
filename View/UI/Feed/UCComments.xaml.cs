﻿using log4net;
using MediaInfoDotNet;
using Microsoft.Win32;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.RingPlayer;
using View.Constants;
using Models.Entity;
using Models.Stores;
using View.Utility.RingMarket;
using Models.DAO;
using View.UI.Comment;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCComments.xaml
    /// </summary>
    public partial class UCComments : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCComments).Name);

        public object Argument
        {
            get { return (object)GetValue(ArgumentProperty); }
            set { SetValue(ArgumentProperty, value); }
        }
        public static readonly DependencyProperty ArgumentProperty = DependencyProperty.Register("Argument", typeof(object), typeof(UCComments), new PropertyMetadata((s, e) =>
        {
            if (e.NewValue != null && e.NewValue is FeedModel)
            {
                UCComments panel = (UCComments)s;
                panel.FeedModel = (FeedModel)e.NewValue;
                panel.DataContext = panel;
            }
        }
       ));

        #region "Property"

        private FeedModel _FeedModel;
        public FeedModel FeedModel
        {
            get
            {
                return _FeedModel;
            }
            set
            {
                if (_FeedModel == value)
                {
                    return;
                }
                _FeedModel = value;
                OnPropertyChanged("FeedModel");
            }
        }
        private bool _UploadPopup;
        public bool UploadPopup
        {
            get { return _UploadPopup; }
            set
            {
                if (value == _UploadPopup)
                    return;
                _UploadPopup = value;
                this.OnPropertyChanged("UploadPopup");
            }
        }
        private object UploaderModel;
        private int _UploadType;//3=image, 5=audio// 6=video //8=sticker
        public int UploadType
        {
            get { return _UploadType; }
            set
            {
                if (value == _UploadType)
                    return;
                _UploadType = value;
                this.OnPropertyChanged("UploadType");
            }
        }

        private ImageUploaderModel _ImageUploaderModel;
        public ImageUploaderModel selectImageUploaderModel
        {
            get
            {
                return _ImageUploaderModel;
            }
            set
            {
                _ImageUploaderModel = value;
                this.OnPropertyChanged("selectImageUploaderModel");
            }
        }

        private MusicUploaderModel _selectMusicUploaderModel;
        public MusicUploaderModel selectMusicUploaderModel
        {
            get
            {
                return _selectMusicUploaderModel;
            }
            set
            {
                _selectMusicUploaderModel = value;
                this.OnPropertyChanged("selectMusicUploaderModel");
            }
        }

        private FeedVideoUploaderModel _selectFeedVideoUploaderModel;
        public FeedVideoUploaderModel selectFeedVideoUploaderModel
        {
            get
            {
                return _selectFeedVideoUploaderModel;
            }
            set
            {
                _selectFeedVideoUploaderModel = value;
                this.OnPropertyChanged("selectFeedVideoUploaderModel");
            }
        }

        private string _SelectedStickerFilePath;
        public string SelectedStickerFilePath
        {
            get
            {
                return _SelectedStickerFilePath;
            }
            set
            {
                _SelectedStickerFilePath = value;
                this.OnPropertyChanged("SelectedStickerFilePath");
            }
        }
        private MarkertStickerImagesModel RecentStickerModel;

        private UCNewCommentPanel _ucNewCommentPanel;
        public UCNewCommentPanel ucNewCommentPanel
        {
            get { return _ucNewCommentPanel; }
            set
            {
                if (value == _ucNewCommentPanel)
                    return;
                _ucNewCommentPanel = value;
                this.OnPropertyChanged("ucNewCommentPanel");
            }
        }

        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Constructor"

        public UCComments()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Event Trigger"

        private void prevComments_MouseDown(object sender, MouseButtonEventArgs e)
        {
            long tm = 0;
            int scl = 0;
            Guid pvtUUID = Guid.Empty;
            if (_FeedModel.CommentList.Count > 0)
            {
                tm = _FeedModel.CommentList.ElementAt(0).Time;
                pvtUUID = _FeedModel.CommentList.ElementAt(0).CommentId;
            }
            if (tm > 0)
            {
                scl = StatusConstants.SCROLL_OLDER;
            }
            if (_FeedModel.CommentList.Count > 0)
            {
                tm = _FeedModel.CommentList.ElementAt(0).Time;
                new PreviousOrNextFeedComments().StartThread(scl, tm, _FeedModel.NewsfeedId, Guid.Empty, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, pvtUUID, SettingsConstants.ACTIVITY_ON_STATUS);
            }
        }

        private void ShowNextComments_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            long tm = 0; int scl = 1;

            if (_FeedModel.CommentList.Count > 0)
            {
                tm = _FeedModel.CommentList.ElementAt(_FeedModel.CommentList.Count - 1).Time;
                new PreviousOrNextFeedComments().StartThread(scl, tm, _FeedModel.NewsfeedId, Guid.Empty, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, _FeedModel.CommentList.ElementAt(_FeedModel.CommentList.Count - 1).CommentId, SettingsConstants.ACTIVITY_ON_STATUS);
            }
        }

        #endregion

        #region "Utility Methods"
        private void addNewCommentPanel()
        {
            if (ucNewCommentPanel == null)
            {
                ucNewCommentPanel = new UCNewCommentPanel();
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    ucNewCommentPanel.motherPanel = UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid;
                else ucNewCommentPanel.motherPanel = UCGuiRingID.Instance.MotherPanel;
                ucNewCommentPanel.FocusNewComment = true;
            }
            else if (ucNewCommentPanel.Parent is Border) ((Border)ucNewCommentPanel.Parent).Child = null;
            _NewCommentPanel.Child = ucNewCommentPanel;

            ucNewCommentPanel.newsfeedId = _FeedModel.NewsfeedId;
            ucNewCommentPanel.imageId = Guid.Empty;
            ucNewCommentPanel.contentId = Guid.Empty;
        }
        private void SetAllModelsToNull()
        {
            UploadType = 0;
            selectFeedVideoUploaderModel = null;
            selectImageUploaderModel = null;
            selectMusicUploaderModel = null;
            SelectedStickerFilePath = null;
            RecentStickerModel = null;
            UploaderModel = null;
        }
        private void imgUploadBorderPanel()
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            op.Multiselect = false;
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if ((bool)op.ShowDialog())
            {
                foreach (string filename in op.FileNames)
                {
                    try
                    {
                        MediaFile mf = new MediaFile(filename);
                        if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit250MBinBytes) { anyMorethan500MB = true; continue; }

                        int formatChecked = ImageUtility.GetFileImageTypeFromHeader(filename);
                        if (formatChecked != 1 && formatChecked != 2) { anyCorrupted = true; continue; }
                        ImageUploaderModel model = new ImageUploaderModel();
                        model.FileSize = mf.size;
                        model.FilePath = filename;
                        selectImageUploaderModel = model; ;
                    }
                    catch (Exception) { anyCorrupted = true; }
                }
                if (selectImageUploaderModel != null)
                {
                    UploadType = SettingsConstants.UPLOADTYPE_IMAGE;
                    UploaderModel = selectImageUploaderModel;
                }
            }
            if (anyCorrupted) UIHelperMethods.ShowWarning("One or more files are corrupted !");
            if (anyMorethan500MB) UIHelperMethods.ShowWarning("One or more files are greater than Maximum file Limit (250MB) !");
        }

        private void uploadAudioBorderPanel()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Music File";
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "All supported Music files|*.mp3|MP3 files(*.mp3)|*.mp3";
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length == 1)
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);
                            if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit500MBinBytes) { anyMorethan500MB = true; continue; }

                            if (mf.Audio == null || mf.Audio.Count == 0 || mf.Video.Count > 0 || !mf.format.Equals("MPEG Audio")) { anyCorrupted = true; continue; }

                            MusicUploaderModel model = new MusicUploaderModel();
                            model.FileSize = mf.size;
                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                                if (file.Tag != null && file.Tag.Pictures.Any())
                                {
                                    if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null)
                                    {
                                        model.IsImageFound = true;
                                    }
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                    {
                                        model.AudioArtist = file.Tag.Performers[0];
                                    }
                                    else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                    {
                                        model.AudioArtist = file.Tag.AlbumArtists[0];
                                    }
                                    model.AudioTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception) { }
                            if (string.IsNullOrEmpty(model.AudioTitle))
                            {
                                model.AudioTitle = Path.GetFileNameWithoutExtension(filename);
                            }
                            model.AudioDuration = (long)(mf.duration / 1000);
                            if (model.AudioDuration == 0)
                                model.AudioDuration = HelperMethods.MediaPlayerDuration(filename);
                            model.FilePath = filename;
                            selectMusicUploaderModel = model;
                        }
                        catch (Exception) { anyCorrupted = true; }
                    }
                    if (selectMusicUploaderModel != null)
                    {
                        UploadType = SettingsConstants.UPLOADTYPE_MUSIC;
                        UploaderModel = selectMusicUploaderModel;
                    }
                }
                else UIHelperMethods.ShowWarning("Can't upload more than 1 music at a time !");
            }
            if (anyCorrupted) UIHelperMethods.ShowWarning("OOne or more files are corrupted or invalid format!");
            if (anyMorethan500MB) UIHelperMethods.ShowWarning("One or more files are greater than Maximum file Limit (250MB) !");
        }

        private void uploadVideoBorderPanel()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Video File";
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "All supported Video files|*.mp4|MP4 files(*.mp4)|*.mp4";
            bool anyCorrupted = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length == 1) // When 5 video select with cntrl--> openFileDialog.FileNames.Length <= 5
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);
                            if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264")) { anyCorrupted = true; continue; }
                            FeedVideoUploaderModel model = new FeedVideoUploaderModel();
                            model.FileSize = mf.size;
                            model.FilePath = filename;
                            selectFeedVideoUploaderModel = model;
                            model.VideoTitle = Path.GetFileNameWithoutExtension(filename);
                            model.VideoDuration = (long)(mf.duration / 1000);
                            if (model.VideoDuration == 0) model.VideoDuration = HelperMethods.MediaPlayerDuration(filename);
                            new SetArtImageFromVideo(model);//120*90
                        }
                        catch (Exception) { anyCorrupted = true; }
                    }
                    if (selectFeedVideoUploaderModel != null)
                    {
                        UploadType = SettingsConstants.UPLOADTYPE_VIDEO;
                        UploaderModel = selectFeedVideoUploaderModel;
                    }
                }
                else UIHelperMethods.ShowFailed("Can't upload more than 1 videos at a time !");
            }
            if (anyCorrupted) UIHelperMethods.ShowWarning("OOne or more files are corrupted or invalid format!");
        }

        private void mediaPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (UploadType == SettingsConstants.UPLOADTYPE_MUSIC || UploadType == SettingsConstants.UPLOADTYPE_VIDEO)
            {
                ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
                if (UploadType == SettingsConstants.UPLOADTYPE_MUSIC && selectMusicUploaderModel != null)
                {
                    SingleMediaModel singleMediamodel = new SingleMediaModel();
                    singleMediamodel.StreamUrl = selectMusicUploaderModel.FilePath;
                    singleMediamodel.Title = selectMusicUploaderModel.AudioTitle;
                    singleMediamodel.Duration = selectMusicUploaderModel.AudioDuration;
                    singleMediamodel.Artist = selectMusicUploaderModel.AudioArtist;
                    singleMediamodel.MediaType = 1;
                    singleMediamodel.ThumbUrl = selectMusicUploaderModel.ThumbUrl;
                    singleMediamodel.IsFromLocalDirectory = true;
                    MediaList.Add(singleMediamodel);
                }
                else if (UploadType == SettingsConstants.UPLOADTYPE_VIDEO && selectFeedVideoUploaderModel != null)
                {
                    SingleMediaModel singleMediamodel = new SingleMediaModel();
                    singleMediamodel.StreamUrl = selectFeedVideoUploaderModel.FilePath;
                    singleMediamodel.Title = selectFeedVideoUploaderModel.VideoTitle;
                    singleMediamodel.Duration = selectFeedVideoUploaderModel.VideoDuration;
                    singleMediamodel.Artist = selectFeedVideoUploaderModel.VideoArtist;
                    singleMediamodel.MediaType = 2;
                    singleMediamodel.ThumbUrl = selectFeedVideoUploaderModel.ThumbUrl;
                    singleMediamodel.IsFromLocalDirectory = true;
                    MediaList.Add(singleMediamodel);
                }
                MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            addNewCommentPanel();
        }

        #endregion "Utility Methods"
    }
}

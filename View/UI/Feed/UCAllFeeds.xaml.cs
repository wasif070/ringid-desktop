﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Utility;
using View.Utility.DataContainer;
using View.ViewModel;
using View.ViewModel.NewStatus;
using System.Windows.Threading;
using System;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCAllFeeds.xaml
    /// </summary>
    public partial class UCAllFeeds : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(UCAllFeeds).Name);
        public static UCAllFeeds Instance;
        public NewStatusViewModel newStatusViewModel = null;
        private FeedHolderModel LoadMoreModel = NewsFeedViewModel.Instance.AllFeedsLoadMoreModel;
        //public BackgroundWorker AllFeedsBackgroundWorker;
        //private bool RequestOn = false;
        //private ConcurrentQueue<FeedHolderModel> AllFeedsQueue = new ConcurrentQueue<FeedHolderModel>();
        #endregion"Fields"

        #region "Ctors"
        public NewStatusView newStatusView = null;
        public UCAllFeeds()
        {
            HelperMethods.SendStartingFeedRequest();
            InitializeComponent();
            Instance = this;
            this.DataContext = this;
            scroll.ScrollChanged += ScrollPositionChanged;
            //this.PreviewMouseWheel += new MouseWheelEventHandler(OnPreviewMouseWheelScrolled);
            //this.IsVisibleChanged += OnIsVisibleChanged;
            _allFeeditemControls.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = ViewCollection });
        }
        #endregion//"Ctors"

        #region"Properties"
        private ObservableCollection<FeedHolderModel> _ViewCollection = NewsFeedViewModel.Instance.AllFeedsViewCollection;
        public ObservableCollection<FeedHolderModel> ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                this.OnPropertyChanged("ViewCollection");
            }
        }

        private int _FeedCollectionType = DefaultSettings.FEED_TYPE_ALL;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }
        #endregion//"Properties"

        #region"Event Triggers"
        private void NewStatusView_Loaded(object sender, RoutedEventArgs e)
        {
            newStatusView = (NewStatusView)sender;
            newStatusViewModel = (NewStatusViewModel)newStatusView.DataContext;
            newStatusViewModel.WriteOnText = "Share your thoughts...";
            newStatusViewModel.WritePostText = "Write Post";
        }
        private void ScrollPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                scroll.ScrollToEnd();
                e.Handled = true;
            }
        }
        private void ScrollPositionChanged(object sender, ScrollChangedEventArgs e)//private void OnPreviewMouseWheelScrolled(object sender, MouseWheelEventArgs e)
        {
            if (NewsFeedViewModel.Instance.AllFeedsScrollEnabled && e.VerticalChange != 0)
            {
                NewsFeedViewModel.Instance.AllFeedsScrollEnabled = false;
                double offset = scroll.VerticalOffset + (e.VerticalChange * 3 / 6);
                if (offset < 0) offset = 0;
                else if (offset > scroll.ScrollableHeight - 30) offset = scroll.ScrollableHeight - 5;
                if (e.VerticalChange > 0) //(e.Delta < 0)
                {
                    if (LoadMoreModel.FeedType != 4 && LoadMoreModel.FeedType != 5 && FeedDataContainer.Instance.AllBottomIds.Count < 10)
                    {
                        NewsFeedViewModel.Instance.AllFeedsRequestFeeds(FeedDataContainer.Instance.AllBottomIds.PvtMinGuid, 2, 0);
                    }
                    if ((LoadMoreModel.FeedType != 4 && LoadMoreModel.FeedType != 5) || FeedDataContainer.Instance.AllBottomIds.Count > 0)
                    {
                        if (scroll.VerticalOffset >= (scroll.ScrollableHeight * 0.60))
                        {
                            new Task(delegate
                            {
                                NewsFeedViewModel.Instance.ActionAllFeedsBottomLoad();
                            }).Start();
                        }
                    }
                    //else if ((NewsFeedViewModel.Instance.AllFeedsBlankFeeds > 0 || !NewsFeedViewModel.Instance.AllFeedsQueue.IsEmpty) && NewsFeedViewModel.Instance.AllFeedsBackgroundWorker != null && !NewsFeedViewModel.Instance.AllFeedsBackgroundWorker.IsBusy)
                    //    NewsFeedViewModel.Instance.AllFeedsBackgroundWorker.RunWorkerAsync();
                    scroll.ScrollToVerticalOffset(offset);
                }
                else if (e.VerticalChange < 0) //(e.Delta > 0)
                {
                    if (scroll.VerticalOffset == 0 && ViewCollection[1].SequenceForAllFeeds > 1)
                    {
                        RingIDViewModel.Instance.FeedUnreadNotificationCounter = 0;
                        if (ViewCollection[1].SequenceForAllFeeds > 1)
                            NewsFeedViewModel.Instance.DropAllKeeping1To20();
                        offset = 0;
                    }
                    else
                    {
                        if (scroll.VerticalOffset <= (scroll.ScrollableHeight * 0.50))
                        {
                            if (ViewCollection.Count > 1 && ViewCollection[1].SequenceForAllFeeds == 1)
                            {
                                if (FeedDataContainer.Instance.AllTopIds.PvtMaxGuid == Guid.Empty)
                                    FeedDataContainer.Instance.AllTopIds.PvtMaxGuid = HelperMethods.GetMaxMinGuidInAllFeedCollection(); //ViewCollection.Max(y => y.FeedId); 
                                NewsFeedViewModel.Instance.AllFeedsRequestFeeds(FeedDataContainer.Instance.AllTopIds.PvtMaxGuid, 1, 0);
                            }
                            new Task(delegate
                            {
                                NewsFeedViewModel.Instance.ActionAllFeedsTopLoad();
                            }).Start();
                        }

                        if (offset < UCAllFeeds.Instance.newStatusView.ActualHeight + 100 && FeedDataContainer.Instance.AllTopIds.Count > 5)
                        {
                            offset = UCAllFeeds.Instance.newStatusView.ActualHeight + 80;//offset + 10;
                            scroll.ScrollToVerticalOffset(offset);
                        }
                        else
                        {
                            scroll.ScrollToVerticalOffset(offset);
                            if (offset == 0)
                            {
                                RingIDViewModel.Instance.FeedUnreadNotificationCounter = 0;
                                if (ViewCollection[1].SequenceForAllFeeds > 1)
                                    NewsFeedViewModel.Instance.DropAllKeeping1To20();
                            }
                        }
                    }
                }
                if (offset < (scroll.ScrollableHeight / 2))
                {
                    if (!IsInUpperHalf
                        //|| ViewCollection.Count > 200
                        )
                    {
                        IsInUpperHalf = true;
                        //LoadShortOrDetailsFeedModels();
                    }
                }
                else
                {
                    if (IsInUpperHalf
                        //|| ViewCollection.Count > 200
                        )
                    {
                        IsInUpperHalf = false;
                        //LoadShortOrDetailsFeedModels();
                    }
                }
                e.Handled = true;
                NewsFeedViewModel.Instance.AllFeedsScrollEnabled = true;
                LoadShortOrDetailsFeedModels();
            }
        }
        #endregion

        #region "ICommands and Command Methods"

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            if (RingIDViewModel.Instance != null) RingIDViewModel.Instance.FeedButtionSelection = true;
            if (UCGuiRingID.Instance != null && !UCGuiRingID.Instance.IsAnyWindowAbove()) Keyboard.Focus(scroll);
            getFocusOnUserControl();
            NewsFeedViewModel.Instance.AllFeedsScrollEnabled = true;
            Task loadData = new Task(delegate
            {
                if (RingIDViewModel.Instance != null) RingIDViewModel.Instance.FeedButtionSelection = true;
                NewsFeedViewModel.Instance.StopRemoving = true;
                if (FeedDataContainer.Instance.AllBottomIds.Count > 0 && (LoadMoreModel.FeedType == 4 || LoadMoreModel.FeedType == 5)) LoadMoreModel.FeedType = 1;
            });
            loadData.Start();
        }
        private ICommand unLoadedUserConrol;
        public ICommand UnLoadedUserConrol
        {
            get
            {
                if (unLoadedUserConrol == null) unLoadedUserConrol = new RelayCommand(param => OnUnLoadedUserConrol());
                return unLoadedUserConrol;
            }
        }
        public void OnUnLoadedUserConrol()
        {
            if (ShortOrDetailsModelLoad != null && ShortOrDetailsModelLoad.IsEnabled)
                ShortOrDetailsModelLoad.Stop();
            NewsFeedViewModel.Instance.AllFeedsScrollEnabled = false;
            NewsFeedViewModel.Instance.DropAllKeeping1To20(true);
        }

        private ICommand homeCommand;
        public ICommand HomeCommand
        {
            get
            {
                if (homeCommand == null) homeCommand = new RelayCommand(param => OnHomeCommand());
                return homeCommand;
            }
        }
        public void OnHomeCommand()
        {
            scroll.ScrollToHome();
            NewsFeedViewModel.Instance.OnHomeRequestandRemoveBottomModels();
        }

        private ICommand endCommand;
        public ICommand EndCommand
        {
            get
            {
                if (endCommand == null) endCommand = new RelayCommand(param => OnEndCommand());
                return endCommand;
            }
        }
        public void OnEndCommand()
        {
            scroll.ScrollToEnd();
        }

        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null) _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                return _ReloadCommand;
            }
        }
        public void OnReloadCommandClicked()
        {
            NewsFeedViewModel.Instance.ActionReloadFeeds();
        }

        #endregion //"ICommands and Command Methods"

        #region "Utility Methods"
        private void getFocusOnUserControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }
        DispatcherTimer ShortOrDetailsModelLoad;
        public bool IsInUpperHalf = true;
        void timer_Tick(object sender, object e)
        {
            Rect scrollRectangle = new Rect(new Point(0, 0), scroll.RenderSize);
            if (IsInUpperHalf)
            {
                for (int i = 0; i < _allFeeditemControls.Items.Count - 1; i++)
                {
                    if ((i + 1) < (_allFeeditemControls.Items.Count - 1))
                    {
                        var nextChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i + 1);//idx bound check necess
                        if (nextChildPresenter == null) break;
                        if (nextChildPresenter.Content != null && nextChildPresenter.Content is FeedHolderModel)
                        {
                            FeedHolderModel nextModel = (FeedHolderModel)nextChildPresenter.Content;
                            if (nextModel.FeedType == 2)
                            {
                                Rect nextChildRectangle = nextChildPresenter.TransformToAncestor(scroll).TransformBounds(new Rect(new Point(0, 0), nextChildPresenter.RenderSize));
                                bool isNextFeedModelInView = scrollRectangle.IntersectsWith(nextChildRectangle);
                                if (isNextFeedModelInView) //current &nextnext?? make visible
                                {
                                    nextModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                    var currentChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i);//opt check
                                    if (currentChildPresenter == null) break;
                                    if (currentChildPresenter.Content != null && currentChildPresenter.Content is FeedHolderModel)
                                    {
                                        FeedHolderModel currentModel = (FeedHolderModel)currentChildPresenter.Content;
                                        if (currentModel.FeedType == 2)
                                        {
                                            currentModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                        }
                                    }
                                    if ((i + 2) < (_allFeeditemControls.Items.Count - 1))
                                    {
                                        var nnChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i + 2); //idx bound check necess
                                        if (nnChildPresenter == null) break;
                                        if (nnChildPresenter.Content != null && nnChildPresenter.Content is FeedHolderModel)
                                        {
                                            FeedHolderModel nnModel = (FeedHolderModel)nnChildPresenter.Content;
                                            if (nnModel.FeedType == 2)
                                            {
                                                nnModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                            }
                                        }
                                    }
                                    i = i + 1;
                                }
                                else
                                {
                                    nextModel.ShortModelType = SettingsConstants.FEED_PARTIAL_VIEW;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                for (int i = _allFeeditemControls.Items.Count - 1; i > 0; i--)
                {
                    if ((i - 1) > 0)
                    {
                        var prevChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i - 1);
                        if (prevChildPresenter == null) break;
                        if (prevChildPresenter.Content != null && prevChildPresenter.Content is FeedHolderModel)
                        {
                            FeedHolderModel prevModel = (FeedHolderModel)prevChildPresenter.Content;
                            if (prevModel.FeedType == 2)
                            {
                                Rect prevChildRectangle = prevChildPresenter.TransformToAncestor(scroll).TransformBounds(new Rect(new Point(0, 0), prevChildPresenter.RenderSize));
                                bool isPrevFeedModelInView = scrollRectangle.IntersectsWith(prevChildRectangle);
                                if (isPrevFeedModelInView) //current &prevprev?? make visible
                                {
                                    prevModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                    var currentChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i);
                                    if (currentChildPresenter == null) break;
                                    if (currentChildPresenter.Content != null && currentChildPresenter.Content is FeedHolderModel)
                                    {
                                        FeedHolderModel currentModel = (FeedHolderModel)currentChildPresenter.Content;
                                        if (currentModel.FeedType == 2)
                                        {
                                            currentModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                        }
                                    }
                                    if ((i - 2) > 0)
                                    {
                                        var ppChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i - 2);
                                        if (ppChildPresenter == null) break;
                                        if (ppChildPresenter.Content != null && ppChildPresenter.Content is FeedHolderModel)
                                        {
                                            FeedHolderModel ppModel = (FeedHolderModel)ppChildPresenter.Content;
                                            if (ppModel.FeedType == 2)
                                            {
                                                ppModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                            }
                                        }
                                    }
                                    i = i - 1;
                                }
                                else
                                {
                                    prevModel.ShortModelType = SettingsConstants.FEED_PARTIAL_VIEW;
                                }
                            }
                        }
                    }
                }
            }
            ShortOrDetailsModelLoad.Stop();
        }
        public void LoadShortOrDetailsFeedModels()
        {
            try
            {
                if (ShortOrDetailsModelLoad == null)
                {
                    ShortOrDetailsModelLoad = new DispatcherTimer();
                    ShortOrDetailsModelLoad.Interval = TimeSpan.FromSeconds(2);
                    ShortOrDetailsModelLoad.Tick += timer_Tick;
                }
                if (!ShortOrDetailsModelLoad.IsEnabled)
                    ShortOrDetailsModelLoad.Start();
            }
            catch (Exception ex)
            {

                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        #endregion "Utility Methods"

        #region"Unused Code"
        //private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        //{
        //    if ((bool)e.NewValue)
        //    {
        //        if (RingIDViewModel.Instance != null)
        //            RingIDViewModel.Instance.FeedButtionSelection = true;
        //        if (UCGuiRingID.Instance != null && !UCGuiRingID.Instance.IsAnyWindowAbove())
        //            Keyboard.Focus(scroll);
        //        StopRemoving = true;
        //        if (FeedDataContainer.Instance.AllBottomIds.Count > 0 && (LoadMoreModel.FeedType == 4 || LoadMoreModel.FeedType == 5))
        //        {
        //            LoadMoreModel.FeedType = 1;
        //        }
        //        //Thread.Sleep(10);
        //        _allFeeditemControls.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Source = ViewCollection });
        //        scroll.ScrollChanged -= ScrollPositionChanged;
        //        scroll.ScrollChanged += ScrollPositionChanged;
        //    }
        //    else
        //    {
        //        StopRemoving = false;
        //        if (RingIDViewModel.Instance != null)
        //            RingIDViewModel.Instance.FeedButtionSelection = false;
        //        scroll.ScrollChanged -= ScrollPositionChanged;
        //        _allFeeditemControls.ClearValue(ItemsControl.ItemsSourceProperty);
        //        //ViewCollection = null;
        //        //Thread.Sleep(500);
        //        if (NewsFeedViewModel.Instance.AllFeedsBackgroundWorker != null && NewsFeedViewModel.Instance.AllFeedsBackgroundWorker.IsBusy)
        //        {
        //            NewsFeedViewModel.Instance.AllFeedsBackgroundWorker.CancelAsync();

        //        }
        //        new Thread(() =>
        //        {
        //            int idxStarts = ((ViewCollection.Count - 2) > 10) ? 10 : ViewCollection.Count - 2;
        //            for (int i = ViewCollection.Count - 2; i > idxStarts; i--)
        //            {
        //                if (!StopRemoving && i < ViewCollection.Count - 1)
        //                {
        //                    FeedHolderModel fm = ViewCollection[i];
        //                    if (fm.Feed == null || fm.Feed.FeedCategory != SettingsConstants.SPECIAL_FEED)
        //                    {
        //                        ViewCollection.InvokeRemove(fm);
        //                        if (fm.Feed != null)
        //                            FeedDataContainer.Instance.AllBottomIds.InsertIntoSortedList(fm.Feed.Time, fm.FeedId);
        //                    }
        //                }
        //            }
        //            NewsFeedViewModel.Instance.AllFeedsBlankFeeds = 0;
        //        }).Start();
        //    }
        //}
        //public void TopLoadUnreadFeeds()
        //{
        //    try
        //    {
        //        RingIDViewModel.Instance.FeedUnreadNotificationCounter = 0;
        //        for (int i = 0; i < FeedDataContainer.Instance.UnreadNewsFeedIds.Count; i++)
        //        {
        //            Guid nfId = FeedDataContainer.Instance.UnreadNewsFeedIds[i];
        //            FeedModel modeltoInsert = null;
        //            if (FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out modeltoInsert) && !ViewCollection.Any(P => P.FeedId == modeltoInsert.NewsfeedId))
        //            {
        //                FeedHolderModel holder = new FeedHolderModel(2);
        //                holder.FeedId = nfId;
        //                holder.Feed = modeltoInsert;
        //                //holder.FeedPanelType = (holder.Feed.ParentFeed != null && holder.Feed.ParentFeed.WhoShareList != null && holder.Feed.ParentFeed.WhoShareList.Count > 1) ? 20 : holder.Feed.FeedPanelType;
        //                if (holder.Feed.ParentFeed != null && holder.Feed.ParentFeed.WhoShareList != null && holder.Feed.ParentFeed.WhoShareList.Count > 1)
        //                {
        //                    holder.FeedPanelType = 20;
        //                    holder.FirstSharer = holder.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
        //                    if (holder.Feed.ParentFeed.WhoShareList.Count == 2)
        //                        holder.SecondSharer = holder.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
        //                    else
        //                        holder.ExtraSharerCount = (holder.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " Others";
        //                }
        //                else
        //                {
        //                    holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                }
        //                holder.ShortModel = false;
        //                NewsFeedViewModel.Instance.InsertModel(holder, true);
        //            }
        //        }
        //        FeedDataContainer.Instance.UnreadNewsFeedIds.Clear();
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.StackTrace);
        //    }
        //}
        //public void EnqueueForLoad(FeedHolderModel modelToQueue)
        //{
        //    if (modelToQueue != null)
        //        AllFeedsQueue.Enqueue(modelToQueue);
        //    if (AllFeedsBackgroundWorker == null)
        //    {
        //        AllFeedsBackgroundWorker = new BackgroundWorker();
        //        AllFeedsBackgroundWorker.WorkerSupportsCancellation = true;
        //        AllFeedsBackgroundWorker.DoWork += (s, e) =>
        //        {
        //            while (!AllFeedsQueue.IsEmpty)
        //            {
        //                FeedHolderModel holder = null;
        //                if (AllFeedsQueue.TryDequeue(out holder))
        //                {
        //                    if (holder.BottomLoad)
        //                    {
        //                        while (FeedDataContainer.Instance.AllBottomIds.Count == 0 && LoadMoreModel.FeedType != 4 && LoadMoreModel.FeedType != 5)
        //                        {
        //                            RequestFeeds(FeedDataContainer.Instance.AllBottomIds.PvtMinGuid, 2, 0);
        //                            Thread.Sleep(100);
        //                        }
        //                        if (FeedDataContainer.Instance.AllBottomIds.Count > 0)
        //                        {
        //                            bool found = false;
        //                            KeyValuePair<long, Guid> kv = new KeyValuePair<long, Guid>();
        //                            try
        //                            {
        //                                kv = FeedDataContainer.Instance.AllBottomIds.LastOrDefault();
        //                                found = true;
        //                            }
        //                            catch (Exception)
        //                            {
        //                                ViewCollection.InvokeRemove(holder);
        //                            }
        //                            if (found)
        //                            {
        //                                FeedDataContainer.Instance.AllBottomIds.Remove(kv.Key);
        //                                holder.Feed = FeedDataContainer.Instance.FeedModels[kv.Value];
        //                                //if (holder.Feed.Status.Equals("singleShare"))
        //                                //{

        //                                //}
        //                                holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                                blankFeed--;
        //                                holder.FeedId = holder.Feed.NewsfeedId;
        //                                holder.ShortModel = false;
        //                                //model.FeedPanelType = (model.Feed.ParentFeed != null && model.Feed.ParentFeed.WhoShareList != null && model.Feed.ParentFeed.WhoShareList.Count > 1) ? 20 : model.Feed.FeedPanelType;
        //                                if (holder.Feed.ParentFeed != null && holder.Feed.ParentFeed.WhoShareList != null && holder.Feed.ParentFeed.WhoShareList.Count > 1)
        //                                {
        //                                    holder.FeedPanelType = 20;
        //                                    holder.FirstSharer = holder.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
        //                                    if (holder.Feed.ParentFeed.WhoShareList.Count == 2)
        //                                        holder.SecondSharer = holder.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
        //                                    else
        //                                        holder.ExtraSharerCount = (holder.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " Others";
        //                                    //
        //                                    Application.Current.Dispatcher.Invoke((Action)delegate
        //                                    {
        //                                        FeedHolderModel prevExtModel = NewsFeedViewModel.Instance.AllFeedsViewCollection.Where(P => (P.Feed != null && P.Feed.ParentFeed != null && P.Feed.ParentFeed.NewsfeedId == holder.Feed.ParentFeed.NewsfeedId)).FirstOrDefault();
        //                                        if (prevExtModel != null)
        //                                        {
        //                                            NewsFeedViewModel.Instance.AllFeedsViewCollection.Remove(prevExtModel);
        //                                        }
        //                                    }, DispatcherPriority.Send);
        //                                }
        //                                else
        //                                {
        //                                    holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                                }
        //                                if (holder.SequenceForAllFeeds > 0)
        //                                {
        //                                    holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                                }
        //                            }
        //                        }
        //                        else if (LoadMoreModel.FeedType == 4 || LoadMoreModel.FeedType == 5)
        //                        {
        //                            ViewCollection.InvokeRemove(holder);
        //                        }
        //                    }
        //                    //else
        //                    //{
        //                    //    model.ShortModel = false;
        //                    //}
        //                    NewsFeedViewModel.Instance.UpdateSequenceNumbers();
        //                    //Thread.Sleep(100);
        //                }
        //            }
        //            //use dispatchertimer to load feedmodel to holder
        //        };
        //        AllFeedsBackgroundWorker.RunWorkerCompleted += (s, e) =>
        //        {
        //        };
        //        AllFeedsBackgroundWorker.RunWorkerAsync();
        //    }
        //    else if (!AllFeedsBackgroundWorker.IsBusy)
        //        AllFeedsBackgroundWorker.RunWorkerAsync();
        //}
        //public void RequestFeeds(Guid pvtUUID, short scrollType, int startLimit, string packetId = null)
        //{
        //    if (!RequestOn)
        //    {
        //        ThreadAnyFeedsRequest thread = new ThreadAnyFeedsRequest();
        //        thread.callBackEvent += (response) =>
        //        {
        //            RequestOn = false;
        //            if (scrollType != 1)
        //            {
        //                switch (response)
        //                {
        //                    case SettingsConstants.RESPONSE_SUCCESS:
        //                        LoadMoreModel.FeedType = 1;
        //                        break;
        //                    case SettingsConstants.RESPONSE_NOTSUCCESS:
        //                        LoadMoreModel.FeedType = 4;
        //                        break;
        //                    case SettingsConstants.NO_RESPONSE:
        //                        LoadMoreModel.FeedType = 5;
        //                        break;
        //                }
        //            }
        //            else
        //            {
        //                Thread.Sleep(100);
        //                if (response == SettingsConstants.RESPONSE_SUCCESS)
        //                    ActionTopLoad();
        //            }
        //        };
        //        RequestOn = true;
        //        LoadMoreModel.FeedType = 3;
        //        thread.StartThread(pvtUUID, scrollType, startLimit, 0, AppConstants.TYPE_NEWS_FEED, packetId, 0, 0);
        //    }
        //}
        //int AllFeedsBlankFeeds = 0;
        //private void ActionBottomLoad()
        //{
        //    // Task.Factory.StartNew(() =>
        //    // {
        //    if (AllFeedsBlankFeeds < 5 || (FeedDataContainer.Instance.AllBottomIds.Count > 0 && AllFeedsQueue.IsEmpty))
        //    {
        //        FeedHolderModel holder = new FeedHolderModel(2);
        //        AllFeedsBlankFeeds++;
        //        scroll.ScrollChanged -= ScrollPositionChanged;
        //        ViewCollection.Insert(UCMiddlePanelSwitcher.View_UCAllFeeds.ViewCollection.Count - 1, holder);
        //        scroll.ScrollChanged += ScrollPositionChanged;
        //        EnqueueForLoad(holder);
        //    }
        //    else if (!AllFeedsBackgroundWorker.IsBusy)
        //        AllFeedsBackgroundWorker.RunWorkerAsync();

        //    // });
        //}
        //private void ActionTopLoad()
        //{
        //    // Task.Factory.StartNew(() =>
        //    // {
        //    if (FeedDataContainer.Instance.AllTopIds.Count > 0)
        //    {
        //        KeyValuePair<long, Guid> kv = FeedDataContainer.Instance.AllTopIds.ElementAt(0);
        //        FeedDataContainer.Instance.AllTopIds.Remove(kv.Key);
        //        FeedHolderModel holder = new FeedHolderModel(2);
        //        holder.BottomLoad = false; //.FirstOrDefault();
        //        holder.Feed = FeedDataContainer.Instance.FeedModels[kv.Value];
        //        //holder.FeedPanelType = (holder.Feed.ParentFeed != null && holder.Feed.ParentFeed.WhoShareList != null && holder.Feed.ParentFeed.WhoShareList.Count > 1) ? 20 : holder.Feed.FeedPanelType;
        //        if (holder.Feed.ParentFeed != null && holder.Feed.ParentFeed.WhoShareList != null && holder.Feed.ParentFeed.WhoShareList.Count > 1)
        //        {
        //            holder.FeedPanelType = 20;
        //            holder.FirstSharer = holder.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
        //            if (holder.Feed.ParentFeed.WhoShareList.Count == 2)
        //                holder.SecondSharer = holder.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
        //            else
        //                holder.ExtraSharerCount = (holder.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " Others";
        //            Application.Current.Dispatcher.Invoke((Action)delegate
        //            {
        //                FeedHolderModel prevExtModel = NewsFeedViewModel.Instance.AllFeedsViewCollection.Where(P => (P.Feed != null && P.Feed.ParentFeed != null && P.Feed.ParentFeed.NewsfeedId == holder.Feed.ParentFeed.NewsfeedId)).FirstOrDefault();
        //                if (prevExtModel != null)
        //                {
        //                    NewsFeedViewModel.Instance.AllFeedsViewCollection.Remove(prevExtModel);
        //                }
        //            }, DispatcherPriority.Send);
        //        }
        //        else
        //        {
        //            holder.FeedPanelType = holder.Feed.FeedPanelType;
        //        }
        //        holder.FeedId = holder.Feed.NewsfeedId;
        //        NewsFeedViewModel.Instance.InsertModel(holder, true); //ViewCollection.InvokeInsert(1, holder);
        //        holder.ShortModel = false;
        //        //EnqueueForLoad(holder);
        //    }
        //    else
        //    {
        //        if (FeedDataContainer.Instance.AllTopIds.PvtMaxGuid == Guid.Empty)
        //            FeedDataContainer.Instance.AllTopIds.PvtMaxGuid = HelperMethods.GetMaxMinGuidInFeedCollection(ViewCollection, 1); //ViewCollection.Max(y => y.FeedId); 
        //        RequestFeeds(FeedDataContainer.Instance.AllTopIds.PvtMaxGuid, 1, 0);
        //    }
        //    //});
        //}
        //public bool AllFeedsScrollEnabled = true;
        /*
                 public void RemoveTopOrBottomModels()
        {
            bool _TopLoad = (_allfeedScrlViewer.VerticalOffset == 0 || _allfeedScrlViewer.VerticalOffset <= (_allfeedScrlViewer.ScrollableHeight / 2));
            if (_TopLoad)
            {
                _allfeedScrlViewer.ScrollChanged -= _allfeedScrlViewer_ScrollChanged;
                RingIDViewModel.Instance.NewsFeeds.RemoveBottomModels();
                _allfeedScrlViewer.ScrollChanged += _allfeedScrlViewer_ScrollChanged;
            }
            else
            {
                _allfeedScrlViewer.ScrollChanged -= _allfeedScrlViewer_ScrollChanged;
                RingIDViewModel.Instance.NewsFeeds.RemoveTopModels();
                _allfeedScrlViewer.ScrollChanged += _allfeedScrlViewer_ScrollChanged;

                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    _allfeedScrlViewer.ScrollChanged -= _allfeedScrlViewer_ScrollChanged;
                    if (_allfeedScrlViewer.ScrollableHeight > 50 && _allfeedScrlViewer.VerticalOffset == _allfeedScrlViewer.ScrollableHeight)
                    {
                        _allfeedScrlViewer.ScrollToVerticalOffset(_allfeedScrlViewer.ScrollableHeight - 50);
                    }
                    _allfeedScrlViewer.ScrollChanged += _allfeedScrlViewer_ScrollChanged;

                }, DispatcherPriority.Send);
            }
        }
         public void AutoLoadMoreOnScrollEnd()
        {
            if ((_allfeedScrlViewer.VerticalOffset == _allfeedScrlViewer.ScrollableHeight || this.BOTTOM_LOADING == StatusConstants.LOADING_GIF_VISIBLE))//
            {
                if (this.BOTTOM_LOADING != StatusConstants.LOADING_GIF_VISIBLE)
                    this.BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
                DownScrollReqOn = true;
                BottomLoadNewsFeeds(true, GetNextFeedModelFromWaitingList());
                DownScrollReqOn = false;
            }
        }

        public void AddScrollChangedEvent(bool Add)
        {
            try
            {
                _allfeedScrlViewer.ScrollChanged -= _allfeedScrlViewer_ScrollChanged;
                if (Add)
                    _allfeedScrlViewer.ScrollChanged += _allfeedScrlViewer_ScrollChanged;
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadMore_ButtonClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        bool IsInUpperHalf = false;
        public void LoadShortOrDetailsFeedModels()
        {
            try
            {
                if (ShortOrDetailsModelLoad == null)
                {
                    ShortOrDetailsModelLoad = new DispatcherTimer();
                    ShortOrDetailsModelLoad.Interval = TimeSpan.FromSeconds(1);
                    ShortOrDetailsModelLoad.Tick += (o, e) =>
                    {
                        Rect scrollRectangle = new Rect(new Point(0, 0), _allfeedScrlViewer.RenderSize);
                        if (IsInUpperHalf)
                        {
                            for (int i = 0; i < _allFeeditemControls.Items.Count - 1; i++)
                            {
                                if ((i + 1) < (_allFeeditemControls.Items.Count - 1))
                                {
                                    var nextChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i + 1);//idx bound check necess
                                    if (nextChildPresenter == null) break;
                                    if (nextChildPresenter.Content != null && nextChildPresenter.Content is FeedModel)
                                    {
                                        FeedModel nextModel = (FeedModel)nextChildPresenter.Content;
                                        if (nextModel.FeedType == 2)
                                        {
                                            Rect nextChildRectangle = nextChildPresenter.TransformToAncestor(_allfeedScrlViewer).TransformBounds(new Rect(new Point(0, 0), nextChildPresenter.RenderSize));
                                            bool isNextFeedModelInView = scrollRectangle.IntersectsWith(nextChildRectangle);
                                            if (isNextFeedModelInView) //current &nextnext?? make visible
                                            {
                                                if (nextModel.ShortModel) nextModel.ShortModel = false;
                                                var currentChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i);//opt check
                                                if (currentChildPresenter == null) break;
                                                if (currentChildPresenter.Content != null && currentChildPresenter.Content is FeedModel)
                                                {
                                                    FeedModel currentModel = (FeedModel)currentChildPresenter.Content;
                                                    if (currentModel.FeedType == 2 && currentModel.ShortModel) currentModel.ShortModel = false;
                                                }
                                                if ((i + 2) < (_allFeeditemControls.Items.Count - 1))
                                                {
                                                    var nnChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i + 2); //idx bound check necess
                                                    if (nnChildPresenter == null) break;
                                                    if (nnChildPresenter.Content != null && nnChildPresenter.Content is FeedModel)
                                                    {
                                                        FeedModel nnModel = (FeedModel)nnChildPresenter.Content;
                                                        if (nnModel.FeedType == 2 && nnModel.ShortModel) nnModel.ShortModel = false;
                                                    }
                                                }
                                                i = i + 1;
                                            }
                                            else if (!nextModel.ShortModel)
                                            {
                                                UCSingleFeed ucSingleFeed = HelperMethods.FindVisualChild<UCSingleFeed>(nextChildPresenter);
                                                if (ucSingleFeed != null)
                                                {
                                                    nextModel.FeedHeight = ucSingleFeed.ActualHeight - 10;
                                                    nextModel.ShortModel = true;
                                                    GC.SuppressFinalize(ucSingleFeed);
                                                    ucSingleFeed = null;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            for (int i = _allFeeditemControls.Items.Count - 1; i > 0; i--)
                            {
                                if ((i - 1) > 0)
                                {
                                    var prevChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i - 1);
                                    if (prevChildPresenter == null) break;
                                    if (prevChildPresenter.Content != null && prevChildPresenter.Content is FeedModel)
                                    {
                                        FeedModel prevModel = (FeedModel)prevChildPresenter.Content;
                                        if (prevModel.FeedType == 2)
                                        {
                                            Rect prevChildRectangle = prevChildPresenter.TransformToAncestor(_allfeedScrlViewer).TransformBounds(new Rect(new Point(0, 0), prevChildPresenter.RenderSize));
                                            bool isPrevFeedModelInView = scrollRectangle.IntersectsWith(prevChildRectangle);
                                            if (isPrevFeedModelInView) //current &prevprev?? make visible
                                            {
                                                if (prevModel.ShortModel) prevModel.ShortModel = false;
                                                var currentChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i);
                                                if (currentChildPresenter == null) break;
                                                if (currentChildPresenter.Content != null && currentChildPresenter.Content is FeedModel)
                                                {
                                                    FeedModel currentModel = (FeedModel)currentChildPresenter.Content;
                                                    if (currentModel.FeedType == 2 && currentModel.ShortModel) currentModel.ShortModel = false;
                                                }
                                                if ((i - 2) > 0)
                                                {
                                                    var ppChildPresenter = (ContentPresenter)_allFeeditemControls.ItemContainerGenerator.ContainerFromIndex(i - 2);
                                                    if (ppChildPresenter == null) break;
                                                    if (ppChildPresenter.Content != null && ppChildPresenter.Content is FeedModel)
                                                    {
                                                        FeedModel ppModel = (FeedModel)ppChildPresenter.Content;
                                                        if (ppModel.FeedType == 2 && ppModel.ShortModel) ppModel.ShortModel = false;
                                                    }
                                                }
                                                i = i - 1;
                                            }
                                            else if (!prevModel.ShortModel)
                                            {
                                                UCSingleFeed ucSingleFeed = HelperMethods.FindVisualChild<UCSingleFeed>(prevChildPresenter);
                                                if (ucSingleFeed != null)
                                                {
                                                    prevModel.FeedHeight = ucSingleFeed.ActualHeight - 10;
                                                    prevModel.ShortModel = true;
                                                    GC.SuppressFinalize(ucSingleFeed);
                                                    ucSingleFeed = null;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ShortOrDetailsModelLoad.Stop();
                        ShortOrDetailsModelLoad.Interval = TimeSpan.FromSeconds(0.2);
                    };
                }
                ShortOrDetailsModelLoad.Stop();
                ShortOrDetailsModelLoad.Start();
            }
            catch (Exception ex)
            {

                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
         * */
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

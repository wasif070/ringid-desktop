﻿using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCSingleFeed.xaml
    /// </summary>
    public partial class UCSingleFeed : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleFeed).Name);
        //private FeedModel _FeedModel;
        public bool _BannerLoaded = false;
        public bool _IsTimerStart = false;
        private DispatcherTimer _LoadTimer = null;
        UCComments ucComments = null;
        DispatcherTimer _NameToolTip = null;

        #region "Constructor"

        public UCSingleFeed()
        {
            try
            {
                //Instance = this;
                InitializeComponent();
                //Loaded -= UCSingleFeed_Loaded;
                //Loaded += UCSingleFeed_Loaded;
            }
            catch (Exception ex)
            {

                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);

            }
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"

        private UCEditDeleteReportOptionsPopup EditDeleteReportOptionsPopup { get; set; }

        #endregion

        private void sliderScroll_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error:PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #region "ICommand"
        private ICommand _editDeleteCommand;
        public ICommand editDeleteCommand
        {
            get
            {
                if (_editDeleteCommand == null)
                {
                    _editDeleteCommand = new RelayCommand(param => OnEditDeleteComman(param));
                }
                return _editDeleteCommand;
            }
        }
        private void OnEditDeleteComman(object parameter)
        {
            try
            {
                if (parameter is FeedModel)
                {
                    FeedModel _FeedModel = (FeedModel)parameter;
                    bool hideOff = true;
                    bool hidethis = true;
                    bool saveUnsave = true;
                    bool followUnfollow = false;
                    bool reportOn = false;

                    if (_FeedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        hideOff = true;
                        hidethis = true;
                        reportOn = false;
                    }
                    else
                    {
                        if ((int)Tag == DefaultSettings.FEED_TYPE_MY || (int)Tag == DefaultSettings.FEED_TYPE_FRIEND || (int)Tag == DefaultSettings.FEED_TYPE_MUSIC_PROFILE
                            || (int)Tag == DefaultSettings.FEED_TYPE_NEWSPORTAL_PROFILE || (int)Tag == DefaultSettings.FEED_TYPE_PAGE_PROFILE
                            || (int)Tag == DefaultSettings.FEED_TYPE_CELEBRITY_PROFILE || (int)Tag == DefaultSettings.FEED_TYPE_CIRCLE
                            || (int)Tag == DefaultSettings.FEED_TYPE_DETAILS)
                        {
                            hideOff = true;
                            hidethis = true;
                            if (_FeedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                            {
                                reportOn = false;
                            }
                            else reportOn = true;
                        }
                        else
                        {
                            hideOff = false;
                            hidethis = false;
                            reportOn = true;
                        }
                    }

                    if (EditDeleteReportOptionsPopup != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(EditDeleteReportOptionsPopup);
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        EditDeleteReportOptionsPopup = new UCEditDeleteReportOptionsPopup(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                    else EditDeleteReportOptionsPopup = new UCEditDeleteReportOptionsPopup(UCGuiRingID.Instance.MotherPanel);
                    EditDeleteReportOptionsPopup.Show();
                    EditDeleteReportOptionsPopup.ShowEditDeletePopup(editDelete, hideOff, hidethis, saveUnsave, followUnfollow, reportOn, _FeedModel, _FeedModel.ParentFeed != null);
                    EditDeleteReportOptionsPopup.OnRemovedUserControl += () =>
                    {
                        EditDeleteReportOptionsPopup = null;
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        {
                            UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        }
                    };
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private ICommand _commentCommand;
        public ICommand CommentCommand
        {
            get
            {
                if (_commentCommand == null)
                {
                    _commentCommand = new RelayCommand(param => OnCommentCommand(param));
                }
                return _commentCommand;
            }
        }
        private void OnCommentCommand(object parameter)
        {
            if (parameter is FeedModel)
            {
                try
                {
                    FeedModel _FeedModel = (FeedModel)parameter;

                    if (_FeedModel.IsShareListVisible == true)
                    {
                        _FeedModel.IsShareListVisible = false;
                    }
                    if (cmntHiddenPanel.Visibility == Visibility.Collapsed)
                    {
                        if (ucComments == null)
                        {
                            ucComments = new UCComments();
                            cmntHiddenPanel.Child = ucComments;
                        }
                        ucComments.FeedModel = _FeedModel;

                        if ((_FeedModel.LikeCommentShare != null && _FeedModel.LikeCommentShare.NumberOfComments > 0 && _FeedModel.LikeCommentShare.NumberOfComments != _FeedModel.CommentList.Count)
                            || (_FeedModel.SingleImageFeedModel != null && _FeedModel.SingleImageFeedModel.LikeCommentShare != null && _FeedModel.SingleImageFeedModel.LikeCommentShare.NumberOfComments > 0 && _FeedModel.SingleImageFeedModel.LikeCommentShare.NumberOfComments != _FeedModel.CommentList.Count)
                            || (_FeedModel.SingleMediaFeedModel != null && _FeedModel.SingleMediaFeedModel.LikeCommentShare != null && _FeedModel.SingleMediaFeedModel.LikeCommentShare.NumberOfComments > 0 && _FeedModel.SingleMediaFeedModel.LikeCommentShare.NumberOfComments != _FeedModel.CommentList.Count))
                        {
                            if (_FeedModel.CommentList.Count > 0) _FeedModel.CommentList.Clear();
                            _FeedModel.NoMoreComments = false;
                            if (_FeedModel.SingleImageFeedModel != null)
                                new PreviousOrNextFeedComments().StartThread(1, 0, _FeedModel.NewsfeedId, Guid.Empty, _FeedModel.SingleImageFeedModel.ImageId, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_STATUS);
                            else if (_FeedModel.SingleMediaFeedModel != null)
                                new PreviousOrNextFeedComments().StartThread(1, 0, _FeedModel.NewsfeedId, _FeedModel.SingleMediaFeedModel.ContentId, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_STATUS);
                            else
                                new PreviousOrNextFeedComments().StartThread(1, 0, _FeedModel.NewsfeedId, Guid.Empty, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_STATUS);
                        }
                        cmntHiddenPanel.Visibility = Visibility.Visible;
                        if (_LoadTimer == null)
                        {
                            _LoadTimer = new DispatcherTimer();
                            _LoadTimer.Interval += TimeSpan.FromSeconds(0.1);
                            _LoadTimer.Tick += (o, a) =>
                            {
                                try
                                {
                                    if (ucComments.ucNewCommentPanel != null)
                                        ucComments.ucNewCommentPanel.richTextBox.Focus();
                                    _LoadTimer.Stop();
                                }
                                catch (Exception ex)
                                {

                                    log.Error("Error: _LoadTimer.Tick() => " + ex.Message + "\n" + ex.StackTrace);

                                }
                            };
                        }
                        _LoadTimer.Stop();
                        _LoadTimer.Start();
                    }
                    else
                    {
                        cmntHiddenPanel.Visibility = Visibility.Collapsed;
                        ucComments = null;
                    }

                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        private ICommand _nameToolTipOpenCommand;
        public ICommand NameToolTipOpenCommand
        {
            get
            {
                if (_nameToolTipOpenCommand == null)
                {
                    _nameToolTipOpenCommand = new RelayCommand(param => OnNameToolTipOpenCommand(param));
                }
                return _nameToolTipOpenCommand;
            }
        }
        int type;
        private void OnNameToolTipOpenCommand(object parameter)
        {
            if (parameter is string)
            {
                try
                {
                    type = Convert.ToInt32(parameter);
                    //if (_NameToolTip != null)
                    //    _NameToolTip = null;

                    //if (type == 0 && name.IsMouseOver)
                    //{
                    //    FeedModel feedModel = ((FeedHolderModel)name.DataContext).Feed;
                    //    ucNameToolTipPopupView.ShowPopUp(name, feedModel, type);
                    //    //MainSwitcher.PopupController.DownloadOrAddToAlbumPopUpWrapper.ShowToolTip(nameTxt, this.commentModel);
                    //}

                    if (_NameToolTip == null)
                    {
                        _NameToolTip = new DispatcherTimer();
                        _NameToolTip.Interval = TimeSpan.FromSeconds(1);
                        _NameToolTip.Tick += (s, es) =>
                        {
                            if (UCNameToolTipPopupView.Instance != null)
                                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCNameToolTipPopupView.Instance);
                            UCNameToolTipPopupView.Instance = new UCNameToolTipPopupView(UCGuiRingID.Instance.MotherPanel);
                            UCNameToolTipPopupView.Instance.Show();
                            if (type == 0 && name.IsMouseOver)
                            {
                                FeedModel feedModel = ((FeedHolderModel)name.DataContext).Feed;
                                UCNameToolTipPopupView.Instance.ShowPopUp(name, feedModel.PostOwner);
                            }
                            else if (type == 1 && FriendName.IsMouseOver)
                            {
                                FeedModel feedModel = ((FeedHolderModel)FriendName.DataContext).Feed;
                                if (feedModel.ParentFeed != null)
                                    UCNameToolTipPopupView.Instance.ShowPopUp(FriendName, feedModel.ParentFeed.PostOwner);
                                else
                                    UCNameToolTipPopupView.Instance.ShowPopUp(FriendName, feedModel.WallOrContentOwner);
                            }
                            else if (type == 3 && tagFirst.IsMouseOver)
                            {
                                FeedModel feedModel = ((FeedHolderModel)tagFirst.DataContext).Feed;
                                if (feedModel.FirstTagProfile != null)
                                    UCNameToolTipPopupView.Instance.ShowPopUp(tagFirst, feedModel.FirstTagProfile);
                            }
                            else if (type == 4 && tagSecond.IsMouseOver)
                            {
                                FeedModel feedModel = ((FeedHolderModel)tagSecond.DataContext).Feed;
                                if (feedModel.SecondTagProfile != null)
                                    UCNameToolTipPopupView.Instance.ShowPopUp(tagSecond, feedModel.SecondTagProfile);
                            }
                            UCNameToolTipPopupView.Instance.OnRemovedUserControl += () =>
                            {
                                UCNameToolTipPopupView.Instance = null;
                                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                                {
                                    UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                                }
                                //if (_NameToolTip != null)
                                //    _NameToolTip = null;
                            };

                            _NameToolTip.Stop();
                            _NameToolTip.Interval = TimeSpan.FromSeconds(0.2);
                        };

                    }
                    _NameToolTip.Stop();
                    _NameToolTip.Start();
                }
                finally { }
            }
        }

        private ICommand _nameToolTipHiddenCommand;
        public ICommand NameToolTipHiddenCommand
        {
            get
            {
                if (_nameToolTipHiddenCommand == null) _nameToolTipHiddenCommand = new RelayCommand(param => OnNameToolTipHiddenCommand(param));
                return _nameToolTipHiddenCommand;
            }
        }

        private void OnNameToolTipHiddenCommand(object parameter)
        {
            if (UCNameToolTipPopupView.Instance != null
                && UCNameToolTipPopupView.Instance.PopupToolTip.IsOpen
                && !UCNameToolTipPopupView.Instance.contentContainer.IsMouseOver)
                UCNameToolTipPopupView.Instance.HidePopUp(false);
            //if (_NameToolTip != null)
            //    _NameToolTip = null;
            //ucNameToolTipPopup = null;
            //this.mainPanel.Children.Remove(ucNameToolTipPopup);
        }

        #endregion "ICommand" 
    }
}

//public static readonly DependencyProperty ArgumentProperty = DependencyProperty.Register("Argument", typeof(object), typeof(UCSingleFeed), new PropertyMetadata((s, e) =>
//{
//    if (e.NewValue != null && e.NewValue is FeedModel)
//    {
//        UCSingleFeed panel = (UCSingleFeed)s;
//        panel.FeedModel = (FeedModel)e.NewValue;
//        //panel.FeedModel.OnPropertyChanged("CurrentInstance");
//        panel.DataContext = panel;
//        if (panel.FeedModel.ShowNextCommentsVisible > 0)
//        {
//            if (panel.ucComments == null)
//            {
//                panel.ucComments = new UCComments();
//                panel.ucComments.FeedModel = panel.FeedModel;
//                panel.cmntHiddenPanel.Child = panel.ucComments;
//            }
//        }
//    }
//}
//));

//public object Argument
//{
//    get { return (object)GetValue(ArgumentProperty); }
//    set
//    {
//        SetValue(ArgumentProperty, value);
//    }
//}


//public FeedModel FeedModel
//{
//    get
//    {
//        return _FeedModel;
//    }
//    set
//    {
//        if (_FeedModel == value)
//        {
//            return;
//        }
//        _FeedModel = value;
//        OnPropertyChanged("FeedModel");
//    }
//}
//public void action_edit_feed(int pvc = 0)
//{
//    rtb_feedEdit.SetStatusandTags();
//    if (!rtb_feedEdit.StringWithoutTags.Equals(_FeedModel.Status) || !HelperMethods.IsTagDTOsSameasJArray(rtb_feedEdit.TagsJArray, _FeedModel.StatusTags))
//    {
//        string st = (rtb_feedEdit.TagsJArray != null) ? rtb_feedEdit.StringWithoutTags : rtb_feedEdit.Text.Trim();
//        if (_FeedModel.BookPostType == 2 && rtb_feedEdit.TagsJArray == null && string.IsNullOrWhiteSpace(st)
//        && _FeedModel.locationModel == null && string.IsNullOrEmpty(_FeedModel.DoingActivity)
//        && (_FeedModel.TaggedFriendsList == null || _FeedModel.TaggedFriendsList.Count == 0) && string.IsNullOrEmpty(FeedModel.PreviewUrl))
//        {
//            CustomMessageBox.ShowWarning(NotificationMessages.NOTHING_TO_POST);
//        }
//        else
//        {
//            if (_FeedModel.GroupId > 0)
//            {
//                LocationDTO location = (_FeedModel.locationModel != null) ? _FeedModel.locationModel.GetLocationDTOFromModel() : null;
//                EditFeed(pvc, _FeedModel.NewsfeedId, 0, _FeedModel.GroupId, st, rtb_feedEdit.TagsJArray, null, null, location);
//            }
//            else if (_FeedModel.FriendShortInfoModel != null && _FeedModel.FriendShortInfoModel.UserIdentity > 0)
//            {
//                LocationDTO location = (_FeedModel.locationModel != null) ? _FeedModel.locationModel.GetLocationDTOFromModel() : null;
//                EditFeed(pvc, _FeedModel.NewsfeedId, _FeedModel.FriendShortInfoModel.UserIdentity, 0, st, rtb_feedEdit.TagsJArray, null, null, location);
//            }
//            else
//            {
//                if (!string.IsNullOrEmpty(_FeedModel.PreviewUrl))
//                {
//                    if (_FeedModel.PreviewUrl.Equals(st) || _FeedModel.PreviewUrl.Equals("http://" + st) || _FeedModel.PreviewUrl.Equals("https://" + st) || _FeedModel.PreviewUrl.Equals("http://" + st + "/") || _FeedModel.PreviewUrl.Equals("https://" + st + "/"))
//                        st = "";
//                }
//                LocationDTO location = (_FeedModel.locationModel != null) ? _FeedModel.locationModel.GetLocationDTOFromModel() : null;
//                EditFeed(pvc, _FeedModel.NewsfeedId, st, rtb_feedEdit.TagsJArray, null, null, location);
//            }
//        }
//    }
//    else
//    {
//        CustomMessageBox.ShowWarning("No change!");
//    }
//}
//private Guid _NewsfeedId;
//public Guid NewsfeedId
//{
//    get { return _NewsfeedId; }
//    set
//    {
//        if (value == _NewsfeedId)
//            return;
//        _NewsfeedId = value;
//        this.OnPropertyChanged("NewsfeedId");
//    }
//}
//private Visibility _TextBlockVisibility;
//public Visibility TextBlockVisibility
//{
//    get { return _TextBlockVisibility; }
//    set
//    {
//        if (value == _TextBlockVisibility)
//            return;

//        _TextBlockVisibility = value;
//        this.OnPropertyChanged("TextBlockVisibility");
//    }
//}

//private Visibility _EditOptionVisibility;
//public Visibility EditOptionVisibility
//{
//    get { return _EditOptionVisibility; }
//    set
//    {
//        if (value == _EditOptionVisibility)
//            return;

//        _EditOptionVisibility = value;
//        this.OnPropertyChanged("EditOptionVisibility");
//    }
//}
//private void emotionBtn_Click(object sender, RoutedEventArgs e)
//{
//    Button btn = (Button)sender;
//    UIElement uiElement = (UIElement)btn;
//    MainSwitcher.PopupController.StickerPopUPInComments.Show(uiElement, UCEmoticonPopup.TYPE_DEFAULT, (emo) =>
//    {
//        if (emo is string)
//        {
//            string emoticonStr = (string)emo;
//            if (!String.IsNullOrWhiteSpace(emoticonStr))
//            {
//                rtb_feedEdit.AppendText(emoticonStr);
//            }
//        }
//        //if (!String.IsNullOrWhiteSpace(emo))
//        //{
//        //    rtb_feedEdit.AppendText(emo);
//        //}
//        return 0;
//    });
//}

//private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
//{
//    System.Windows.Controls.Image img = sender as System.Windows.Controls.Image;
//}

//private void ActivitistName_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
//{
//    try
//    {
//        //if (_FeedModel.ActivistShortInfoModel != null && _FeedModel.ActivistShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
//        //    RingIDViewModel.Instance.OnMyProfileClicked(_FeedModel.ActivistShortInfoModel.UserTableID);
//        //else if (_FeedModel.ActivistShortInfoModel != null && _FeedModel.ActivistShortInfoModel.ContactType != SettingsConstants.SPECIAL_CONTACT)
//        //{
//        //    RingIDViewModel.Instance.OnFriendProfileButtonClicked(_FeedModel.ActivistShortInfoModel.UserTableID);
//        //}
//    }
//    catch (Exception ex)
//    {

//        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

//    }
//}
//private Grid richGrid;
//private void richGridLoaded(object sender, RoutedEventArgs e)
//{
//    richGrid = sender as Grid;
//}
//private RichTextFeedEdit rtb_feedEdit;
//private void rtbfeedEdit_Loaded(object sender, RoutedEventArgs e)
//{
//    rtb_feedEdit = sender as RichTextFeedEdit;
//    FeedModel _FeedModel = (FeedModel)rtb_feedEdit.DataContext;
//    rtb_feedEdit.Document.Blocks.Clear();
//    rtb_feedEdit.AppendText(_FeedModel.Status);
//    if (_FeedModel.StatusTags != null && _FeedModel.StatusTags.Count > 0)
//        rtb_feedEdit.TagsHandle(_FeedModel.StatusTags);

//    if (_FeedModel.StatusTags != null)
//    {
//        foreach (var item in _FeedModel.StatusTags)
//        {
//            rtb_feedEdit._AddedFriendsUtid.Add(item.UserTableID);
//        }
//    }
//}
//private void rtbfeedEdit_RichTextChanged(string text)
//{
//    if (text.Length > 0)
//    {
//        TextBlockVisibility = Visibility.Collapsed;
//        if (text.EndsWith("@"))
//        {
//            rtb_feedEdit.AlphaStarts = rtb_feedEdit.Text.Length;
//            if (UCAlphaTagPopUp.Instance == null)
//            {
//                UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
//            }
//            if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
//            {
//                Grid g = (Grid)UCAlphaTagPopUp.Instance.Parent;
//                g.Children.Remove(UCAlphaTagPopUp.Instance);
//            }
//            richGrid.Children.Add(UCAlphaTagPopUp.Instance);
//            UCAlphaTagPopUp.Instance.InitializePopUpLocation(rtb_feedEdit);
//        }
//        else if (rtb_feedEdit.AlphaStarts >= 0 && rtb_feedEdit.Text.Length > rtb_feedEdit.AlphaStarts)
//        {
//            string searchStr = rtb_feedEdit.Text.Substring(rtb_feedEdit.AlphaStarts);
//            UCAlphaTagPopUp.Instance.ViewSearchFriends(searchStr);
//        }
//    }
//    else
//    {
//        TextBlockVisibility = Visibility.Visible;
//        rtb_feedEdit.AlphaStarts = -1;
//        rtb_feedEdit._AddedFriendsUtid.Clear();
//    }
//}
//private void rtbfeedEdit_PreviewKeyDown(object sender, KeyEventArgs e)
//{
//    try
//    {
//        rtb_feedEdit = sender as RichTextFeedEdit;
//        FeedModel _FeedModel = (FeedModel)rtb_feedEdit.DataContext;
//        if (e.Key == Key.Enter && (UCAlphaTagPopUp.Instance == null || (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen == false)))
//        {
//            if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
//            {
//                e.Handled = true;
//                //action_edit_feed();
//            }
//        }
//        else if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
//        {
//            if (e.Key == Key.Down)
//            {
//                e.Handled = true;
//                if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
//                {
//                    UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
//                }
//                else
//                {
//                    UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
//                }
//            }
//            if (e.Key == Key.Up)
//            {
//                e.Handled = true;

//                if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
//                {
//                    UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
//                }
//                else
//                {
//                    UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
//                }
//            }
//            if (e.Key == Key.Enter || e.Key == Key.Tab)
//            {
//                e.Handled = true;
//                UCAlphaTagPopUp.Instance.SelectFriendTag((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
//            }
//        }
//        else if (e.Key == Key.Escape)
//        {
//            if (_FeedModel.LinkModel != null && !string.IsNullOrEmpty(_FeedModel.LinkModel.PreviewUrl) && !string.IsNullOrEmpty(_FeedModel.Status) && _FeedModel.LinkModel.PreviewUrl.Equals(_FeedModel.Status))
//            {
//                _FeedModel.Status = "";
//            }
//            _FeedModel.IsEditMode = false;
//        }
//    }
//    catch (Exception ex)
//    {
//        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
//    }
//}

//private void tagBtn_Click(object sender, RoutedEventArgs e)
//{
//    try
//    {
//        Button btn = (Button)sender;
//        UIElement uiElement = (UIElement)btn;
//        FeedModel _FeedModel = (FeedModel)btn.DataContext;
//        rtb_feedEdit.SetStatusandTags();
//        if (rtb_feedEdit.TagsJArray != null)
//        {
//            DownloadOrAddToAlbumPopUpWrapper.Show(uiElement, _FeedModel, rtb_feedEdit.StringWithoutTags, rtb_feedEdit.TagsJArray);
//        }
//        else
//        {
//            DownloadOrAddToAlbumPopUpWrapper.Show(uiElement, _FeedModel, rtb_feedEdit.Text.Trim(), null);
//        }
//    }
//    catch (Exception ex)
//    {

//        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

//    }
//}
//private void shareAudioBorderPanel_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
//{
//    Border b = (Border)sender;
//    SingleMediaModel item = (SingleMediaModel)b.DataContext;
//    if (FeedModel.CurrentInstance.ParentFeed.MediaContent.MediaList != null && FeedModel.CurrentInstance.ParentFeed.MediaContent.MediaList.Count > 0)
//    {
//        int idx = FeedModel.CurrentInstance.ParentFeed.MediaContent.MediaList.IndexOf(item);
//        if (idx >= 0)
//            MediaUtility.RunPlayList(FeedModel.CurrentInstance.ParentFeed.PortalMediaModel != null, FeedModel.CurrentInstance.ParentFeed.NewsfeedId, FeedModel.CurrentInstance.ParentFeed.MediaContent.MediaList, null, idx);
//    }
//}
//private void PlayMedia_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
//{
//Border b = (Border)sender;
//if (b.DataContext is SingleMediaModel)
//{
//    SingleMediaModel item = (SingleMediaModel)b.DataContext;
//    int idx = FeedModel.MediaContent.MediaList.IndexOf(item);
//    BaseUserProfileModel ownerOfMedia = null;
//    if (item.UserTableID == FeedModel.PostOwner.UserTableID) ownerOfMedia = FeedModel.PostOwner;
//    else ownerOfMedia = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(item.UserTableID, 0, item.FullName, item.ProfileImage);
//    //(item.UserTableID == FeedModel.UserShortInfoModel.UserTableID) ? FeedModel.UserShortInfoModel : RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(item.UserTableID, 0, item.FullName, item.ProfileImage);
//    if (idx >= 0 && idx < FeedModel.MediaContent.MediaList.Count)
//    {
//        MediaUtility.RunPlayList(FeedModel.PortalMediaModel != null, FeedModel.NewsfeedId, FeedModel.MediaContent.MediaList, ownerOfMedia, idx);
//        RingPlayerViewModel.Instance.NavigateFrom = "feed";
//        // PlayerHelperMethods.InitiMediaPlayer(idx, item);
//        // MainSwitcher.MediaPlayerController.AddPlayerInView();
//    }

//}
//else if (b.DataContext is UCSingleFeed && ((int)Tag == DefaultSettings.FEED_TYPE_DETAILS))
//{
//    UCSingleFeed singleFeed = (UCSingleFeed)b.DataContext;
//    if (singleFeed.FeedModel != null && FeedModel.MediaContent != null && FeedModel.MediaContent.MediaList.Count == 1)
//        MediaUtility.RunPlayList(true, FeedModel.NewsfeedId, FeedModel.MediaContent.MediaList, FeedModel.PostOwner, 0);
//}
//}
//private void MediaPlayClick_MouseDown(object sender, MouseButtonEventArgs e)
//{
//    Border b = (Border)sender;
//    SingleMediaModel item = (SingleMediaModel)b.DataContext;
//    //if (FeedModel.MediaContent.MediaList != null && FeedModel.MediaContent.MediaList.Count > 0)
//    //{
//    //    int idx = FeedModel.MediaContent.MediaList.IndexOf(item);
//    //    if (idx >= 0)
//    //        MediaUtility.RunPlayList(FeedModel.PortalMediaModel != null, FeedModel.NewsfeedId, FeedModel.MediaContent.MediaList, null, idx);
//    //}
//}

//public UCPopupViewWrapper PopupViewWrapper
//{
//    get
//    {
//        return MainSwitcher.PopupController.popupViewWrapper;
//    }
//}
//public UCDownloadOrAddToAlbumPopUpWrapper DownloadOrAddToAlbumPopUpWrapper
//{
//    get
//    {
//        return MainSwitcher.PopupController.DownloadOrAddToAlbumPopUpWrapper;
//    }
//}
//void UCSingleFeed_Loaded(object sender, RoutedEventArgs e)
//{
//    try
//    {
//        UserControl us = (UserControl)sender;
//        FeedModel _FeedModel = (FeedModel)us.DataContext;

//        if (PopupViewWrapper != null
//            && PopupViewWrapper.IsVisible
//            && PopupViewWrapper.ucSingleFeedDetailsView != null
//            && PopupViewWrapper.ucSingleFeedDetailsView.FeedModel != null && _FeedModel != null
//            && PopupViewWrapper.ucSingleFeedDetailsView.FeedModel.NewsfeedId == _FeedModel.NewsfeedId)
//        {
//            if (!PopupViewWrapper.ucSingleFeedDetailsView.IsVisible)
//            {
//                cmntHiddenPanel.Visibility = Visibility.Collapsed;
//                if (_FeedModel != null) _FeedModel.ShowNextCommentsVisible = 0;
//            }
//            else if (_FeedModel.ShowNextCommentsVisible >= 1) cmntHiddenPanel.Visibility = Visibility.Visible;
//        }
//    }
//    catch (Exception ex)
//    {
//        log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
//    }
//}
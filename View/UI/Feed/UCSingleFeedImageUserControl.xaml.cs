﻿using log4net;
using Models.Constants;
using System;
<<<<<<< HEAD
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Utility;
using View.Utility.Images;
=======
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Converter;
using View.Utility;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCSingleFeedImageUserControl.xaml
    /// </summary>
    public partial class UCSingleFeedImageUserControl : UserControl, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleFeedImageUserControl).Name);
        private bool disposed = false;
<<<<<<< HEAD
        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleFeedImageUserControl), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
=======

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(int), typeof(UCSingleFeedImageUserControl), new PropertyMetadata(1, OnOpenChanged));
        public int IsOpened
        {
            get
            {
                return (int)GetValue(IsOpenedProperty);
            }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }
<<<<<<< HEAD
=======
        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleFeedImageUserControl panel = ((UCSingleFeedImageUserControl)o);
                object data = panel.DataContext;
                if (e.NewValue != null && data is ImageModel)
                {
                    int newShortModelType = (int)e.NewValue;
                    ImageModel model = (ImageModel)data;
                    if (newShortModelType == 2) //image on
                    {
                        model.IsOpenedInFeed = true;
                        panel.BindPreview(model);
                    }
                    else if (newShortModelType == 1) //image off
                    {
                        model.IsOpenedInFeed = false;
                        panel.Dispose(true);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        public UCSingleFeedImageUserControl()
        {
            InitializeComponent();
        }
        ~UCSingleFeedImageUserControl()
        {
            Dispose(false);
        }
        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    if (disposing)
                    {
                        BitmapImage src = (BitmapImage)singleImg.Source;
                        singleImg.ClearValue(System.Windows.Controls.Image.SourceProperty);
                        singleImg.Source = null;
                        if (src != null)
                        {
                            GC.SuppressFinalize(src);
                            src = null;
                        }
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
<<<<<<< HEAD
        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleFeedImageUserControl panel = ((UCSingleFeedImageUserControl)o);
                object data = panel.DataContext;

                if (e.NewValue != null && (bool)e.NewValue)
                {
                    panel.BindPreview((ImageModel)data);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
=======
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        private void BindPreview(ImageModel imageModel)
        {
            try
            {
<<<<<<< HEAD
                if (imageModel.NoImageFound)
                {
                    singleImg.Source = ImageObjects.NO_IMAGE_FOUND;
                }
                else if (!string.IsNullOrEmpty(imageModel.ImageLocation) && File.Exists(imageModel.ImageLocation))
                {
                    double mw = 0, mh = 0;
                    ImageUtility.GetResizeImageParameters(imageModel.FeedImageInfoModel.Width, imageModel.FeedImageInfoModel.Height, imageModel.FeedImageInfoModel.ReWidth, imageModel.FeedImageInfoModel.ReHeight, out mw, out mh);
                    if (imageModel.IsOpenedInFeed)
                    {
                        BitmapImage bitmap = new BitmapImage();
                        var stream = new FileStream(imageModel.ImageLocation, FileMode.Open);
                        bitmap.BeginInit();
                        bitmap.CacheOption = BitmapCacheOption.OnLoad;
                        bitmap.StreamSource = stream;
                        bitmap.DecodePixelHeight = (int)mh;
                        bitmap.DecodePixelWidth = (int)mw;
                        bitmap.EndInit();
                        stream.Close();
                        stream.Dispose();
                        bitmap.Freeze();
                        //Application.Current.Dispatcher.Invoke(() =>
                        //{
                        singleImg.Source = bitmap;
                        //});
                        //singleImg.Source = bitmap;
                    }
                    else
                    {
                        singleImg.Source = null;
                    }
                }
                else if (!string.IsNullOrEmpty(imageModel.ImageUrl))
                {
                    if (ServerAndPortSettings.GetImageServerResourceURL != null)
                    {
                        string imgUrl = HelperMethods.GetUrlWithSize(imageModel.ImageUrl, ImageUtility.IMG_600);
                        if (!string.IsNullOrWhiteSpace(imgUrl))
                        {
                            //new Thread(() =>
                            //{
                            //string image_url_with_base = ServerAndPortSettings.GetImageServerResourceURL + imgUrl;
                            //bool downloaded = ImageUtility.DownloadRemoteImageFile(image_url_with_base, imageModel.ImageLocation);
                            //if (downloaded)
                            // BindPreview(imageModel);
                            //}).Start();
                            BackgroundWorker bgworker = new BackgroundWorker();
                            bgworker.DoWork += (s, e) =>
                            {
                                string image_url_with_base = ServerAndPortSettings.GetImageServerResourceURL + imgUrl;
                                e.Result = ImageUtility.DownloadRemoteImageFile(image_url_with_base, imageModel.ImageLocation);
                            };
                            bgworker.RunWorkerCompleted += (s, e) =>
                            {
                                if (e.Result != null && e.Result is bool && (bool)e.Result)
                                {
                                    BindPreview(imageModel);
                                }
                            };
                            bgworker.RunWorkerAsync();
                        }
                    }
                    //bitmap = ImageObjects.PHOTO_LOADING;
                    //GC.SuppressFinalize(bitmap);
                    singleImg.Source = ImageObjects.PHOTO_LOADING;
                }
=======
                singleImg.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("CurrentInstance"), Converter = new BookImageConverter() });
                //if (imageModel.NoImageFound)
                //{
                //    singleImg.Source = ImageObjects.NO_IMAGE_FOUND;
                //}
                //else if (!string.IsNullOrEmpty(imageModel.ImageLocation) && File.Exists(imageModel.ImageLocation))
                //{
                //    double mw = 0, mh = 0;
                //    ImageUtility.GetResizeImageParameters(imageModel.FeedImageInfoModel.Width, imageModel.FeedImageInfoModel.Height, imageModel.FeedImageInfoModel.ReWidth, imageModel.FeedImageInfoModel.ReHeight, out mw, out mh);
                //    if (IsOpened == 2)//(imageModel.IsOpenedInFeed)
                //    {
                //        BitmapImage bitmap = new BitmapImage();
                //        var stream = new FileStream(imageModel.ImageLocation, FileMode.Open);
                //        bitmap.BeginInit();
                //        bitmap.CacheOption = BitmapCacheOption.OnLoad;
                //        bitmap.StreamSource = stream;
                //        bitmap.DecodePixelHeight = (int)mh;
                //        bitmap.DecodePixelWidth = (int)mw;
                //        bitmap.EndInit();
                //        stream.Close();
                //        stream.Dispose();
                //        bitmap.Freeze();
                //        singleImg.Source = bitmap;
                //    }
                //    else
                //    {
                //        singleImg.Source = null;
                //    }
                //}
                //else if (!string.IsNullOrEmpty(imageModel.ImageUrl))
                //{
                //    if (ServerAndPortSettings.GetImageServerResourceURL != null)
                //    {
                //        string imgUrl = HelperMethods.GetUrlWithSize(imageModel.ImageUrl, ImageUtility.IMG_600);
                //        if (!string.IsNullOrWhiteSpace(imgUrl))
                //        {
                //            //new Thread(() =>
                //            //{
                //            //string image_url_with_base = ServerAndPortSettings.GetImageServerResourceURL + imgUrl;
                //            //bool downloaded = ImageUtility.DownloadRemoteImageFile(image_url_with_base, imageModel.ImageLocation);
                //            //if (downloaded)
                //            // BindPreview(imageModel);
                //            //}).Start();
                //            BackgroundWorker bgworker = new BackgroundWorker();
                //            bgworker.DoWork += (s, e) =>
                //            {
                //                string image_url_with_base = ServerAndPortSettings.GetImageServerResourceURL + imgUrl;
                //                e.Result = ImageUtility.DownloadRemoteImageFile(image_url_with_base, imageModel.ImageLocation);
                //            };
                //            bgworker.RunWorkerCompleted += (s, e) =>
                //            {
                //                if (e.Result != null && e.Result is bool && (bool)e.Result)
                //                {
                //                    BindPreview(imageModel);
                //                }
                //            };
                //            bgworker.RunWorkerAsync();
                //        }
                //    }
                //    //GC.SuppressFinalize(bitmap);
                //    singleImg.Source = ImageObjects.PHOTO_LOADING;
                //}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }
    }
}

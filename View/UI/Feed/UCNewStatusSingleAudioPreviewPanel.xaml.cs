﻿using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.UI.PopUp;
using View.ViewModel;
using System.Collections.ObjectModel;
using System;
using View.Utility;
using View.Utility.RingPlayer;
using View.ViewModel.NewStatus;
using Models.Constants;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCNewStatusSingleAudioPreviewPanel.xaml
    /// </summary>
    public partial class UCNewStatusSingleAudioPreviewPanel : UserControl
    {
        private NewStatusViewModel newStatusViewModel = new NewStatusViewModel();
        public UCNewStatusSingleAudioPreviewPanel()
        {
            InitializeComponent();
        }

        private void CrossButton_Click(object sender, RoutedEventArgs e)
        {
            if (newStatusViewModel != null && newStatusViewModel.IsRtbNewStatusAreaEnabled)
            {
                Control control = (Control)sender;
                MusicUploaderModel model = (MusicUploaderModel)control.DataContext;

                newStatusViewModel.NewStatusMusicUpload.Remove(model);
                if (newStatusViewModel.NewStatusMusicUpload.Count == 0)
                {
                    if (newStatusViewModel.NewStatusHashTag.Count > 0) newStatusViewModel.ClearNewStatusHashTagsExceptPlaceHolder();
                    newStatusViewModel.AudioPanelVisibility = Visibility.Collapsed;
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Collapsed;
                    newStatusViewModel.ActionRemoveLinkPreview();
                    newStatusViewModel.AudioAlbumTitleTextBoxText = "";
                    if (newStatusViewModel.ucMediaCloudUploadPopup != null) newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(0);
                    newStatusViewModel.AddtoAudioAlbumButtonVisibility = Visibility.Visible;
                    newStatusViewModel.SelectedAudioAlbumCrossButtonVisibility = Visibility.Collapsed;
                    newStatusViewModel.PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                    newStatusViewModel.IsPrivacyButtonEnabled = true;
                }
            }
        }

        private void hashTagGridPanel_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (UCSingleHashTagPopup.Instance == null)
            {
                UCSingleHashTagPopup.Instance = new UCSingleHashTagPopup();
            }
            //if (UCSingleHashTagPopup.Instance.Parent != null && UCSingleHashTagPopup.Instance.Parent is Grid)
            //{
            //    Grid g = (Grid)UCSingleHashTagPopup.Instance.Parent;
            //    g.Children.Remove(UCSingleHashTagPopup.Instance);
            //}
            //hashTagGridPanel.Children.Add(UCSingleHashTagPopup.Instance);
            //hashTagGridPanel
            //UCNewStatus NewStatusInstance = (UCNewStatus)this.Tag;
            //UCSingleHashTagPopup.Instance.Show(NewStatusInstance.audioAddHashTagTxtBoxContainerGrid);
        }

        private void TextBox_Loaded(object sender, RoutedEventArgs e)
        {
            textBoxAudio.Focus();
            textBoxAudio.CaretIndex = textBoxAudio.Text.Length;
            newStatusViewModel = (NewStatusViewModel)this.Tag;
            if (newStatusViewModel.FeedWallType < 0)
            {
                AudioUploadCrossButton.Visibility = Visibility.Hidden;
                textBoxAudio.IsEnabled = false;
            }
        }

        private void audioPanel_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Grid b = (Grid)sender;
            NewStatusViewModel newStatusViewModel = (NewStatusViewModel)this.Tag;

            MusicUploaderModel item = (MusicUploaderModel)b.DataContext;
            int index = newStatusViewModel.NewStatusMusicUpload.IndexOf(item);

            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
            foreach (MusicUploaderModel singleModel in newStatusViewModel.NewStatusMusicUpload)
            {
                SingleMediaModel singleMediamodel = new SingleMediaModel();
                singleMediamodel.StreamUrl = singleModel.FilePath;
                singleMediamodel.Title = singleModel.AudioTitle;
                singleMediamodel.Duration = singleModel.AudioDuration;
                singleMediamodel.Artist = singleModel.AudioArtist;
                singleMediamodel.MediaType = 1;
                singleMediamodel.ThumbUrl = singleModel.ThumbUrl;
                singleMediamodel.IsFromLocalDirectory = true;
                MediaList.Add(singleMediamodel);
            }

            MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, index);
        }
    }
}

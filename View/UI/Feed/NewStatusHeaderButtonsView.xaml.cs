﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.ViewModel.NewStatus;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for NewStatusHeaderButtonsView.xaml
    /// </summary>
    public partial class NewStatusHeaderButtonsView : UserControl
    {
        public NewStatusHeaderButtonsView()
        {
            InitializeComponent();
        }
    }
}

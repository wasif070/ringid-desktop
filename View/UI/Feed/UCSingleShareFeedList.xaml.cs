﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Circle;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCSingleShareFeedList.xaml
    /// </summary>
    public partial class UCSingleShareFeedList : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleShareFeedList).Name);
        //private FeedModel _FeedModel;
        private DispatcherTimer _LoadTimer = null;
        UCComments ucComments = null;
        public UCEditHistoryPopupStyle ucEditHistoryPopupStyle = null;
        DispatcherTimer _NameToolTip = null;

        public UCSingleShareFeedList()
        {
            InitializeComponent();
        }

        //public static readonly DependencyProperty ArgumentProperty = DependencyProperty.Register("Argument", typeof(object), typeof(UCSingleShareFeedList), new PropertyMetadata((s, e) =>
        //{
        //    if (e.NewValue != null && e.NewValue is FeedModel)
        //    {
        //        UCSingleShareFeedList panel = (UCSingleShareFeedList)s;
        //        //panel.FeedModel = (FeedModel)e.NewValue;
        //        //panel.DataContext = panel;
        //        //if (!RingIDViewModel.Instance.ShareListFeedModel.Any(P => P.NewsfeedId == panel.FeedModel.NewsfeedId))
        //        //{
        //        //    RingIDViewModel.Instance.ShareListFeedModel.Add(panel.FeedModel);
        //        //}
        //    }
        //}
        //));

        #region "Properties"
        //public object Argument
        //{
        //    get { return (object)GetValue(ArgumentProperty); }
        //    set
        //    {
        //        SetValue(ArgumentProperty, value);
        //    }
        //}

        //public FeedModel FeedModel
        //{
        //    get
        //    {
        //        return _FeedModel;
        //    }
        //    set
        //    {
        //        if (_FeedModel == value)
        //        {
        //            return;
        //        }
        //        _FeedModel = value;
        //        OnPropertyChanged("FeedModel");
        //    }
        //}

        private Visibility _TextBlockVisibility;
        public Visibility TextBlockVisibility
        {
            get { return _TextBlockVisibility; }
            set
            {
                if (value == _TextBlockVisibility)
                    return;

                _TextBlockVisibility = value;
                this.OnPropertyChanged("TextBlockVisibility");
            }
        }

        private ImageSource _popupLikeLoader;
        public ImageSource PopupLikeLoader
        {
            get
            {
                return _popupLikeLoader;
            }
            set
            {
                if (value == _popupLikeLoader)
                    return;
                _popupLikeLoader = value;
                OnPropertyChanged("PopupLikeLoader");
            }
        }

        private UCNameToolTipPopupView ucNameToolTipPopupView { get; set; }

        private UCEditDeleteReportOptionsPopup EditDeleteReportOptionsPopup { get; set; }

        #endregion"Properties"

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void likeHide()
        {
            Thread.Sleep(2000);
            Application.Current.Dispatcher.Invoke(() =>
            {
                PopupLikeLoader = null;
                likeAnimationGrid.Visibility = Visibility.Collapsed;
            });
        }

        public void ShowLikeAnimation()
        {
            if (PopupLikeLoader == null)
            {
                PopupLikeLoader = ImageObjects.LOADER_LIKE_ANIMATION;
                likeAnimationGrid.Visibility = Visibility.Visible;
                Thread t = new Thread(likeHide);
                t.Start();
            }
            else
            {
                PopupLikeLoader = null;
                likeAnimationGrid.Visibility = Visibility.Collapsed;
            }
        }
        #region "ICommand"
        private ICommand _editDeleteCommand;
        public ICommand editDeleteCommand
        {
            get
            {
                if (_editDeleteCommand == null)
                {
                    _editDeleteCommand = new RelayCommand(param => OnEditDeleteComman(param));
                }
                return _editDeleteCommand;
            }
        }
        private void OnEditDeleteComman(object parameter)
        {
            try
            {
                if (parameter is FeedModel)
                {
                    FeedModel _FeedModel = (FeedModel)parameter;
                    bool hideOff = true;
                    bool hidethis = true;
                    bool saveUnsave = true;
                    bool followUnfollow = false;
                    bool reportOn = false;

                    if (_FeedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        hideOff = true;
                        hidethis = true;
                        reportOn = false;
                    }
                    else
                    {
                        if ((int)Tag == DefaultSettings.FEED_TYPE_MY || (int)Tag == DefaultSettings.FEED_TYPE_FRIEND || (int)Tag == DefaultSettings.FEED_TYPE_MUSIC_PROFILE
                            || (int)Tag == DefaultSettings.FEED_TYPE_NEWSPORTAL_PROFILE || (int)Tag == DefaultSettings.FEED_TYPE_PAGE_PROFILE
                            || (int)Tag == DefaultSettings.FEED_TYPE_CELEBRITY_PROFILE || (int)Tag == DefaultSettings.FEED_TYPE_CIRCLE
                            || (int)Tag == DefaultSettings.FEED_TYPE_DETAILS)
                        {
                            hideOff = true;
                            hidethis = true;
                            if (_FeedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                            {
                                reportOn = false;
                            }
                            else reportOn = true;
                        }
                        else
                        {
                            hideOff = false;
                            hidethis = false;
                            reportOn = true;
                        }
                    }

                    if (EditDeleteReportOptionsPopup != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(EditDeleteReportOptionsPopup);
                    EditDeleteReportOptionsPopup = new UCEditDeleteReportOptionsPopup(UCGuiRingID.Instance.MotherPanel);
                    EditDeleteReportOptionsPopup.Show();
                    EditDeleteReportOptionsPopup.ShowEditDeletePopup(editDelete, hideOff, hidethis, saveUnsave, followUnfollow, reportOn, _FeedModel, _FeedModel.ParentFeed != null);
                    EditDeleteReportOptionsPopup.OnRemovedUserControl += () =>
                    {
                        EditDeleteReportOptionsPopup = null;
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        {
                            UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        }
                        //if (ImageViewInMain != null)
                        //{
                        //    ImageViewInMain.GrabKeyboardFocus();
                        //}
                    };
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private ICommand _commentCommand;
        public ICommand CommentCommand
        {
            get
            {
                if (_commentCommand == null)
                {
                    _commentCommand = new RelayCommand(param => OnCommentCommand(param));
                }
                return _commentCommand;
            }
        }
        private void OnCommentCommand(object parameter)
        {
            if (parameter is FeedModel)
            {
                try
                {
                    FeedModel _FeedModel = (FeedModel)parameter;

                    if (_FeedModel.IsShareListVisible == true)
                    {
                        _FeedModel.IsShareListVisible = false;
                    }
                    if (cmntHiddenPanel.Visibility == Visibility.Collapsed)
                    {
                        if (ucComments == null)
                        {
                            ucComments = new UCComments();
                            cmntHiddenPanel.Child = ucComments;
                        }
                        ucComments.FeedModel = _FeedModel;

                        if ((_FeedModel.LikeCommentShare != null && _FeedModel.LikeCommentShare.NumberOfComments > 0 && _FeedModel.LikeCommentShare.NumberOfComments != _FeedModel.CommentList.Count)
                            || (_FeedModel.SingleImageFeedModel != null && _FeedModel.SingleImageFeedModel.LikeCommentShare != null && _FeedModel.SingleImageFeedModel.LikeCommentShare.NumberOfComments > 0 && _FeedModel.SingleImageFeedModel.LikeCommentShare.NumberOfComments != _FeedModel.CommentList.Count)
                            || (_FeedModel.SingleMediaFeedModel != null && _FeedModel.SingleMediaFeedModel.LikeCommentShare != null && _FeedModel.SingleMediaFeedModel.LikeCommentShare.NumberOfComments > 0 && _FeedModel.SingleMediaFeedModel.LikeCommentShare.NumberOfComments != _FeedModel.CommentList.Count))
                        {
                            if (_FeedModel.CommentList.Count > 0) _FeedModel.CommentList.Clear();
                            _FeedModel.NoMoreComments = false;
                            if (_FeedModel.SingleImageFeedModel != null)
                                new PreviousOrNextFeedComments().StartThread(1, 0, _FeedModel.NewsfeedId, Guid.Empty, _FeedModel.SingleImageFeedModel.ImageId, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_STATUS);
                            else if (_FeedModel.SingleMediaFeedModel != null)
                                new PreviousOrNextFeedComments().StartThread(1, 0, _FeedModel.NewsfeedId, _FeedModel.SingleMediaFeedModel.ContentId, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_STATUS);
                            else
                                new PreviousOrNextFeedComments().StartThread(1, 0, _FeedModel.NewsfeedId, Guid.Empty, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_STATUS);
                        }
                        cmntHiddenPanel.Visibility = Visibility.Visible;
                        if (_LoadTimer == null)
                        {
                            _LoadTimer = new DispatcherTimer();
                            _LoadTimer.Interval += TimeSpan.FromSeconds(0.1);
                            _LoadTimer.Tick += (o, a) =>
                            {
                                try
                                {
                                    ucComments.ucNewCommentPanel.richTextBox.Focus();
                                    _LoadTimer.Stop();
                                }
                                catch (Exception ex)
                                {

                                    log.Error("Error: _LoadTimer.Tick() => " + ex.Message + "\n" + ex.StackTrace);

                                }
                            };
                        }
                        _LoadTimer.Stop();
                        _LoadTimer.Start();
                    }
                    else
                    {
                        cmntHiddenPanel.Visibility = Visibility.Collapsed;
                        ucComments = null;
                    }

                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
        private ICommand _nameToolTipOpenCommand;
        public ICommand NameToolTipOpenCommand
        {
            get
            {
                if (_nameToolTipOpenCommand == null)
                {
                    _nameToolTipOpenCommand = new RelayCommand(param => OnNameToolTipOpenCommand(param));
                }
                return _nameToolTipOpenCommand;
            }
        }
        int type;
        private void OnNameToolTipOpenCommand(object parameter)
        {
            if (parameter is string)
            {
                try
                {
                    type = Convert.ToInt32(parameter);
                    //if (_NameToolTip != null)
                    //    _NameToolTip = null;

                    //if (type == 0 && name.IsMouseOver)
                    //{
                    //    FeedModel feedModel = ((FeedHolderModel)name.DataContext).Feed;
                    //    ucNameToolTipPopupView.ShowPopUp(name, feedModel, type);
                    //    //MainSwitcher.PopupController.DownloadOrAddToAlbumPopUpWrapper.ShowToolTip(nameTxt, this.commentModel);
                    //}

                    if (_NameToolTip == null)
                    {
                        _NameToolTip = new DispatcherTimer();
                        _NameToolTip.Interval = TimeSpan.FromSeconds(1);
                        _NameToolTip.Tick += (s, es) =>
                        {
                            if (ucNameToolTipPopupView != null)
                                UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNameToolTipPopupView);
                            ucNameToolTipPopupView = new UCNameToolTipPopupView(UCGuiRingID.Instance.MotherPanel);
                            ucNameToolTipPopupView.Show();
                            if (type == 0 && name.IsMouseOver)
                            {
                                FeedModel feedModel = ((FeedHolderModel)name.DataContext).Feed;
                                ucNameToolTipPopupView.ShowPopUp(name, feedModel.PostOwner);
                            }
                            //else if (type == 1 && FriendName.IsMouseOver)
                            //{
                            //    FeedModel feedModel = ((FeedHolderModel)FriendName.DataContext).Feed;
                            //    ucNameToolTipPopupView.ShowPopUp(FriendName, feedModel.WallOrContentOwner);
                            //}
                            else if (type == 3 && tagFirst.IsMouseOver)
                            {
                                FeedModel feedModel = ((FeedHolderModel)tagFirst.DataContext).Feed;
                                ucNameToolTipPopupView.ShowPopUp(tagFirst, feedModel.FirstTagProfile);
                            }
                            else if (type == 4 && tagSecond.IsMouseOver)
                            {
                                FeedModel feedModel = ((FeedHolderModel)tagSecond.DataContext).Feed;
                                if (feedModel.SecondTagProfile != null)
                                    ucNameToolTipPopupView.ShowPopUp(tagSecond, feedModel.SecondTagProfile);
                            }
                            ucNameToolTipPopupView.OnRemovedUserControl += () =>
                            {
                                ucNameToolTipPopupView = null;
                                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                                {
                                    UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                                }
                                //if (_NameToolTip != null)
                                //    _NameToolTip = null;
                            };

                            _NameToolTip.Stop();
                            _NameToolTip.Interval = TimeSpan.FromSeconds(0.2);
                        };

                    }
                    _NameToolTip.Stop();
                    _NameToolTip.Start();
                }
                finally { }
            }
        }

        private ICommand _nameToolTipHiddenCommand;
        public ICommand NameToolTipHiddenCommand
        {
            get
            {
                if (_nameToolTipHiddenCommand == null) _nameToolTipHiddenCommand = new RelayCommand(param => OnNameToolTipHiddenCommand(param));
                return _nameToolTipHiddenCommand;
            }
        }

        private void OnNameToolTipHiddenCommand(object parameter)
        {
            if (ucNameToolTipPopupView != null
                && ucNameToolTipPopupView.PopupToolTip.IsOpen
                && !ucNameToolTipPopupView.contentContainer.IsMouseOver)
                ucNameToolTipPopupView.HidePopUp(false);
        }
        #endregion "ICommand"
        private Grid richGrid;
        private void richGridLoaded(object sender, RoutedEventArgs e)
        {
            richGrid = sender as Grid;
        }

        private RichTextFeedEdit rtb_feedEdit;
        private void rtbfeedEdit_RichTextChanged(string text)
        {
            if (text.Length > 0)
            {
                TextBlockVisibility = Visibility.Collapsed;
                if (text.EndsWith("@"))
                {
                    rtb_feedEdit.AlphaStarts = rtb_feedEdit.Text.Length;
                    if (UCAlphaTagPopUp.Instance == null)
                    {
                        UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
                    }
                    if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
                    {
                        Grid g = (Grid)UCAlphaTagPopUp.Instance.Parent;
                        g.Children.Remove(UCAlphaTagPopUp.Instance);
                    }
                    richGrid.Children.Add(UCAlphaTagPopUp.Instance);
                    UCAlphaTagPopUp.Instance.InitializePopUpLocation(rtb_feedEdit);
                }
                else if (rtb_feedEdit.AlphaStarts >= 0 && rtb_feedEdit.Text.Length > rtb_feedEdit.AlphaStarts)
                {
                    string searchStr = rtb_feedEdit.Text.Substring(rtb_feedEdit.AlphaStarts);
                    UCAlphaTagPopUp.Instance.ViewSearchFriends(searchStr);
                }
            }
            else
            {
                TextBlockVisibility = Visibility.Visible;
                rtb_feedEdit.AlphaStarts = -1;
                rtb_feedEdit._AddedFriendsUtid.Clear();
            }
        }

        //private void rtbfeedEdit_Loaded(object sender, RoutedEventArgs e)
        //{
        //    rtb_feedEdit = sender as RichTextFeedEdit;
        //    rtb_feedEdit.Document.Blocks.Clear();
        //    rtb_feedEdit.AppendText(_FeedModel.Status);
        //    if (_FeedModel.StatusTags != null && _FeedModel.StatusTags.Count > 0)
        //        rtb_feedEdit.TagsHandle(_FeedModel.StatusTags);

        //    if (_FeedModel.StatusTags != null)
        //    {
        //        foreach (var item in _FeedModel.StatusTags)
        //        {
        //            rtb_feedEdit._AddedFriendsUtid.Add(item.UserTableID);
        //        }
        //    }
        //}

        public void action_edit_feed(int pvc = 0)
        {
            //rtb_feedEdit.SetStatusandTags();
            //if (!rtb_feedEdit.StringWithoutTags.Equals(_FeedModel.Status) || !HelperMethods.IsTagDTOsSameasJArray(rtb_feedEdit.TagsJArray, _FeedModel.StatusTags))
            //{
            //    string st = (rtb_feedEdit.TagsJArray != null) ? rtb_feedEdit.StringWithoutTags : rtb_feedEdit.Text.Trim();
            //    if (_FeedModel.BookPostType == 2 && rtb_feedEdit.TagsJArray == null && string.IsNullOrWhiteSpace(st)
            //    && _FeedModel.locationModel == null && string.IsNullOrEmpty(_FeedModel.DoingActivity)
            //    && (_FeedModel.TaggedFriendsList == null || _FeedModel.TaggedFriendsList.Count == 0) && string.IsNullOrEmpty(FeedModel.PreviewUrl))
            //    {
            //        CustomMessageBox.ShowWarning(NotificationMessages.NOTHING_TO_POST);
            //    }
            //    else
            //    {
            //        if (_FeedModel.GroupId > 0)
            //        {
            //            LocationDTO location = (_FeedModel.locationModel != null) ? _FeedModel.locationModel.GetLocationDTOFromModel() : null;
            //            new ThradEditFeed().StartThread(pvc, _FeedModel.NewsfeedId, 0, _FeedModel.GroupId, st, rtb_feedEdit.TagsJArray, null, null, location);
            //        }
            //        else if (_FeedModel.FriendShortInfoModel != null && _FeedModel.FriendShortInfoModel.UserIdentity > 0)
            //        {
            //            LocationDTO location = (_FeedModel.locationModel != null) ? _FeedModel.locationModel.GetLocationDTOFromModel() : null;
            //            new ThradEditFeed().StartThread(pvc, _FeedModel.NewsfeedId, _FeedModel.FriendShortInfoModel.UserIdentity, 0, st, rtb_feedEdit.TagsJArray, null, null, location);
            //        }
            //        else
            //        {
            //            if (!string.IsNullOrEmpty(_FeedModel.PreviewUrl))
            //            {
            //                if (_FeedModel.PreviewUrl.Equals(st) || _FeedModel.PreviewUrl.Equals("http://" + st) || _FeedModel.PreviewUrl.Equals("https://" + st) || _FeedModel.PreviewUrl.Equals("http://" + st + "/") || _FeedModel.PreviewUrl.Equals("https://" + st + "/"))
            //                    st = "";
            //            }
            //            LocationDTO location = (_FeedModel.locationModel != null) ? _FeedModel.locationModel.GetLocationDTOFromModel() : null;
            //            new ThreaA().StartThread(pvc, _FeedModel.NewsfeedId, st, rtb_feedEdit.TagsJArray, null, null, location);
            //        }
            //    }
            //}
            //else
            //{
            //    CustomMessageBox.ShowWarning("No change!");
            //}
        }

        //private void rtbfeedEdit_PreviewKeyDown(object sender, KeyEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Key == Key.Enter && (UCAlphaTagPopUp.Instance == null || (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen == false)))
        //        {
        //            if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
        //            {
        //                e.Handled = true;
        //                action_edit_feed();
        //            }
        //        }
        //        else if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
        //        {
        //            if (e.Key == Key.Down)
        //            {
        //                e.Handled = true;
        //                if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
        //                {
        //                    UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
        //                }
        //                else
        //                {
        //                    UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
        //                }
        //            }
        //            if (e.Key == Key.Up)
        //            {
        //                e.Handled = true;

        //                if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
        //                {
        //                    UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
        //                }
        //                else
        //                {
        //                    UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
        //                }
        //            }
        //            if (e.Key == Key.Enter || e.Key == Key.Tab)
        //            {
        //                e.Handled = true;
        //                UCAlphaTagPopUp.Instance.SelectFriendTag((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
        //            }
        //        }
        //        else if (e.Key == Key.Escape)
        //        {
        //            _FeedModel.IsEditMode = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        //private void tagBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        Button btn = (Button)sender;
        //        UIElement uiElement = (UIElement)btn;
        //        FeedModel _FeedModel = (FeedModel)btn.DataContext;
        //        rtb_feedEdit.SetStatusandTags();
        //        if (rtb_feedEdit.TagsJArray != null)
        //        {
        //            DownloadOrAddToAlbumPopUpWrapper.Show(uiElement, _FeedModel, rtb_feedEdit.StringWithoutTags, rtb_feedEdit.TagsJArray);
        //        }
        //        else
        //        {
        //            DownloadOrAddToAlbumPopUpWrapper.Show(uiElement, _FeedModel, rtb_feedEdit.Text.Trim(), null);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        //private void noOfLike_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (_FeedModel.SingleImageFeedModel != null)
        //        LikeListViewWrapper.Show(_FeedModel.SingleImageFeedModel.NumberOfLikes, _FeedModel.SingleImageFeedModel.ImageId);
        //    else if (_FeedModel.SingleMediaFeedModel != null)
        //        LikeListViewWrapper.Show(_FeedModel.SingleMediaFeedModel.LikeCount, Guid.Empty, Guid.Empty, _FeedModel.SingleMediaFeedModel.ContentId, Guid.Empty);
        //    else
        //        LikeListViewWrapper.Show(_FeedModel.NumberOfLikes, _FeedModel.NewsfeedId, AppConstants.TYPE_LIKES_FOR_STATUS);
        //}

        private void Comment_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                TextBlock cmntPanel = (TextBlock)sender;
                FeedModel _FeedModel = (FeedModel)cmntPanel.DataContext;
                if (_FeedModel.IsShareListVisible == true)
                {
                    _FeedModel.IsShareListVisible = false;
                }
                if (cmntHiddenPanel.Visibility == Visibility.Collapsed)
                {
                    if (ucComments == null)
                    {
                        ucComments = new UCComments();
                        cmntHiddenPanel.Child = ucComments;
                    }
                    ucComments.FeedModel = _FeedModel;

                    if ((_FeedModel.LikeCommentShare != null && _FeedModel.LikeCommentShare.NumberOfComments > 0 && _FeedModel.LikeCommentShare.NumberOfComments != _FeedModel.CommentList.Count)
                            || (_FeedModel.SingleImageFeedModel != null && _FeedModel.SingleImageFeedModel.LikeCommentShare != null && _FeedModel.SingleImageFeedModel.LikeCommentShare.NumberOfComments > 0 && _FeedModel.SingleImageFeedModel.LikeCommentShare.NumberOfComments != _FeedModel.CommentList.Count)
                            || (_FeedModel.SingleMediaFeedModel != null && _FeedModel.SingleMediaFeedModel.LikeCommentShare != null && _FeedModel.SingleMediaFeedModel.LikeCommentShare.NumberOfComments > 0 && _FeedModel.SingleMediaFeedModel.LikeCommentShare.NumberOfComments != _FeedModel.CommentList.Count))
                    {
                        if (_FeedModel.CommentList.Count > 0) _FeedModel.CommentList.Clear();
                        _FeedModel.NoMoreComments = false;
                        if (_FeedModel.SingleImageFeedModel != null)
                            new PreviousOrNextFeedComments().StartThread(1, 0, _FeedModel.SingleImageFeedModel.NewsFeedId, Guid.Empty, _FeedModel.SingleImageFeedModel.ImageId, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_IMAGE);
                        else if (_FeedModel.SingleMediaFeedModel != null)
                            new PreviousOrNextFeedComments().StartThread(1, 0, _FeedModel.NewsfeedId, _FeedModel.SingleMediaFeedModel.ContentId, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_MULTIMEDIA);
                        else
                            new PreviousOrNextFeedComments().StartThread(1, 0, _FeedModel.NewsfeedId, Guid.Empty, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_STATUS);
                    }
                    cmntHiddenPanel.Visibility = Visibility.Visible;
                    if (_LoadTimer == null)
                    {
                        _LoadTimer = new DispatcherTimer();
                        _LoadTimer.Interval += TimeSpan.FromSeconds(0.1);
                        _LoadTimer.Tick += (o, a) =>
                        {
                            try
                            {
                                ucComments.ucNewCommentPanel.richTextBox.Focus();
                                _LoadTimer.Stop();
                            }
                            catch (Exception ex)
                            {
                                log.Error("Error: _LoadTimer.Tick() => " + ex.Message + "\n" + ex.StackTrace);
                            }
                        };
                    }
                    _LoadTimer.Stop();
                    _LoadTimer.Start();
                }
                else
                {
                    cmntHiddenPanel.Visibility = Visibility.Collapsed;
                }

            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #region "Utility Methods"

        //public void PreviousOrNextFeedComments(int scl, long tm, long nfid, long cntId = 0, long imgId = 0)
        //{
        //    new PreviousOrNextFeedComments().StartThread(scl, tm, _FeedModel.NewsfeedId, cntId, imgId);
        //}
        //public void PreviousOrNextFeedComments(int st, long nfid, long cntId = 0, long imgId = 0)
        //{
        //    new PreviousOrNextFeedComments().StartThread(st, nfid, cntId, imgId);
        //}

        //public void EditFeed(int pvc, long NfId, long FriendIdentity, long CircleId, string sts, JArray JobjStsTags, List<long> AddedTags, List<long> RemovedTags, LocationDTO locationDTO)
        //{
        //    new ThradEditFeed().StartThread(pvc, NfId, FriendIdentity, CircleId, sts, JobjStsTags, AddedTags, RemovedTags, locationDTO);
        //}

        //public void EditFeed(int pvc, long NfId, string sts, JArray JobjStsTags, List<long> AddedTags, List<long> RemovedTags, LocationDTO locationDTO)
        //{
        //    new ThradEditFeed().StartThread(pvc, NfId, sts, JobjStsTags, AddedTags, RemovedTags, locationDTO);
        //}

        //public void EditFeed(int pvc, long NfId, string sts, JArray JobjStsTags, long FriendIdentity, long CircleId, LocationDTO locationDTO)
        //{
        //    new ThradEditFeed().StartThread(pvc, NfId, sts, JobjStsTags, FriendIdentity, CircleId, locationDTO);
        //}
        // public void NotificationRequest(long max_ut, short scl)
        //{
        //    new ThradNotificationRequest().StartThread(max_ut, scl);
        //}
        //public void LikeUnlikeFeed(long NfId, Object feedModelObj, long SfId, bool Like)
        //{
        //    new ThradLikeUnlikeFeed().StartThread(NfId, feedModelObj, SfId, Like);
        //}
        #endregion "Utility Methods"
    }
}

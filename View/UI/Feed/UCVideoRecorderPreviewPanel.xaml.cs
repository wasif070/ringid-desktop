﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Constants;
using View.UI.PopUp;
using View.Utility;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCVideoRecorderPreviewPanel.xaml
    /// </summary>
    public partial class UCVideoRecorderPreviewPanel : PopUpBaseControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCVideoRecorderPreviewPanel).Name);

        #region "fields"
        public static UCVideoRecorderPreviewPanel Instance;
        private UCVideoRecorderView _UCVideoRecorderView = null;
        private UCVideoRecorderPlayerView _UCVideoRecorderPlayerView = null;
        #endregion

        #region "Constructor"
        public UCVideoRecorderPreviewPanel(Grid motherPanel)
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_TRANSPARENT_BG_COLOR, 0.0);//background Color Transparent Black
            SetParent(motherPanel);
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private bool _IsExpandView = false;
        public bool IsExpandView
        {
            get
            {
                return _IsExpandView;
            }
            set
            {
                if (value == _IsExpandView)
                    return;
                _IsExpandView = value;
                OnPropertyChanged("IsExpandView");
            }
        }

        private bool ContainerHasChildren
        {
            get
            {
                return (TaskContainer.Children != null && TaskContainer.Children.Count > 0);
            }
        }

        public delegate void VideoAdded(string fileName, int duration);
        public event VideoAdded OnVideoAddedToUploadList;
        #endregion

        #region Utility Methods

        private void CloseRecorder()
        {
            //_UCVideoRecorderView.Dispose();
            _UCVideoRecorderView.WebCamPreview.Dispose();
            _UCVideoRecorderView = null;
        }
        private void ClosePlayer()
        {
            //_UCVideoRecorderPlayerView.Dispose();
            _UCVideoRecorderPlayerView = null;
        }
        public void CloseAll()
        {
            if (_UCVideoRecorderView != null) CloseRecorder();
            if (_UCVideoRecorderPlayerView != null) ClosePlayer();
        }

        public void DeleteFilesInDirectory()
        {
            try
            {
                string[] filters = new string[] { "*.avi", "*.mp4", "*.1" };
                foreach (var filter in filters)
                {
                    string[] files = System.IO.Directory.GetFiles(RingIDSettings.TEMP_CAM_IMAGE_FOLDER, filter, SearchOption.TopDirectoryOnly);
                    if (files != null)
                    {
                        foreach (var file in files)
                        {
                            System.IO.File.Delete(file);
                        }
                    }
                }
            }
            catch (Exception ex) 
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public void ClearTaskContainer()
        {
            if (ContainerHasChildren)
            {
                CloseAll();
                TaskContainer.Children.Clear();
            }
        }

        #endregion

        #region Initialize Methods

        private void InitRecorder()
        {
            _UCVideoRecorderView = new UCVideoRecorderView();
            _UCVideoRecorderView.RecordingCompleteHandler += ((fileName, duration) =>
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    //string tempFile = System.IO.Path.ChangeExtension(fileName, RecorderWebcamPreview.VIDEO_FORMAT);
                    //if (File.Exists(tempFile))
                    //{
                    //Media Player Loading Code here
                    ClearTaskContainer();
                    InitPlayer(fileName, duration);
                    ShowPlayer();
                    //}
                }
            });
        }
        private void InitPlayer(string videoUriRaw, int duration)
        {
            _UCVideoRecorderPlayerView = new UCVideoRecorderPlayerView(new Uri(videoUriRaw), duration);
            //.................
            _UCVideoRecorderPlayerView.OnMediaPlayerLoaded += (s) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    _UCVideoRecorderPlayerView = (UCVideoRecorderPlayerView)s;
                    _UCVideoRecorderPlayerView.PlayStart();
                });
            };
            //.................
            _UCVideoRecorderPlayerView.OnPlayComplete += () =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (_UCVideoRecorderPlayerView != null)
                    {
                        _UCVideoRecorderPlayerView.MediaPlayer.Stop();
                        _UCVideoRecorderPlayerView.MEDIA_STATE = StatusConstants.MEDIA_INIT_STATE;
                        _UCVideoRecorderPlayerView.PlayProgress = 100;
                        _UCVideoRecorderPlayerView.SeekBarValue = 0;
                    }
                });
            };
            //.................
            _UCVideoRecorderPlayerView.OnVideoRecordCommand += (fileName) =>
            {
                try
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        new Thread(() =>
                        {
                            try
                            {
                                if (File.Exists(fileName))
                                    File.Delete(fileName);
                            }
                            catch (Exception ex)
                            {
                                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                            }
                        }).Start();
                        ShowRecorder();
                    });
                }
                catch (Exception ex) 
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            };
            _UCVideoRecorderPlayerView.OnVideoAdded += (fileName, videoDuration) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    CloseAll();
                    ClearTaskContainer();
                    Hide();
                   // CloseWindow();
                    if (OnVideoAddedToUploadList != null) OnVideoAddedToUploadList(fileName, duration);
                });
            };
        }
        #endregion

        #region Show Methods
        //private void ShowView()
        //{
        //    this.Visibility = Visibility.Visible;

        //    //this.Owner = Application.Current.MainWindow;

        //    double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
        //    double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
        //    double windowWidth = this.Width;
        //    double windowHeight = this.Height;
        //    //this.Left = (screenWidth / 2) - (windowWidth / 2);
        //    //this.Top = (screenHeight / 2) - (windowHeight / 2);;
        //}
        public void ShowRecorder()
        {
            ClearTaskContainer();
            InitRecorder();
            _UCVideoRecorderView.IsExpandView = IsExpandView;
            TaskContainer.Children.Add(_UCVideoRecorderView);
            //App.Current.Dispatcher.Invoke(new Action(() =>
            //{
            //    this.ShowView();
            //}));
        }
        public void ShowPlayer()
        {
            _UCVideoRecorderPlayerView.IsExpandView = IsExpandView;
            TaskContainer.Children.Add(_UCVideoRecorderPlayerView);
            //App.Current.Dispatcher.Invoke(new Action(() =>
            //{
            //    this.ShowView();
            //}));
        }
        #endregion

        #region Commands
        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                _CloseCommand = _CloseCommand ?? new RelayCommand((param) => OnCloseClicked());
                return _CloseCommand;
            }
        }
        private void OnCloseClicked()
        {
            CloseAll();
            DeleteFilesInDirectory();
            this.Hide();
        }

        private ICommand _ExpandCommand;
        public ICommand ExpandCommand { get { _ExpandCommand = _ExpandCommand ?? new RelayCommand((param) => OnExpandClicked(param)); return _ExpandCommand; } }

        private void OnExpandClicked(object param)
        {
            IsExpandView = IsExpandView ? false : true;
            if (_UCVideoRecorderView != null) _UCVideoRecorderView.IsExpandView = IsExpandView;
            else if (_UCVideoRecorderPlayerView != null) _UCVideoRecorderPlayerView.IsExpandView = IsExpandView;
        }

        #endregion
    }
}

﻿using log4net;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Recorder;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCFeedAudioRecorder.xaml
    /// </summary>
    public partial class UCFeedAudioRecorder : System.Windows.Controls.UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFeedAudioRecorder).Name);

        public static UCFeedAudioRecorder Instance = null;

        private int _Type;
        private int _State = RecorderConstants.STATE_READY;
        private int _RecordingProgress = 0;
        private TimeSpan _TimerValue = TimeSpan.FromSeconds(0);
        //private UserBasicInfoModel _UserBasicInfoModel;
        //private GroupInfoModel _GroupInfoModel;
        private ICommand _CloseCommand;
        private ICommand _RecordCommand;
        private AudioCapture audioCapture;

        public delegate void OnRecordingCompleted(string fileName, int countDown);
        public event OnRecordingCompleted RecordingCompletedHandler;
        public UCFeedAudioRecorder()
        {
            InitializeComponent();
            Instance = this;
            this.Type = RecorderConstants.TYPE_FRIEND_VOICE_RECORD;
            this.State = RecorderConstants.STATE_READY;
            this.DataContext = this;
        }
        //public UCFeedAudioRecorder(UserBasicInfoModel userBasicInfoModel)
        //{
        //    this.InitializeComponent();
        //    Instance = this;
        //    this.UserBasicInfoModel = userBasicInfoModel;
        //    this.Type = RecorderConstants.TYPE_FRIEND_VOICE_RECORD;
        //    this.State = RecorderConstants.STATE_READY;
        //    this.DataContext = this;
        //}


        //public UserBasicInfoModel UserBasicInfoModel
        //{
        //    get { return _UserBasicInfoModel; }
        //    set { _UserBasicInfoModel = value; }
        //}

        public int Type
        {
            get { return _Type; }
            set
            {
                _Type = value;
                this.OnPropertyChanged("Type");
            }
        }

        public int State
        {
            get { return _State; }
            set
            {
                _State = value;
                this.OnPropertyChanged("State");
            }
        }

        public int RecordingProgress
        {
            get { return _RecordingProgress; }
            set
            {
                _RecordingProgress = value;
                this.OnPropertyChanged("RecordingProgress");
            }
        }

        public TimeSpan TimerValue
        {
            get { return _TimerValue; }
            set
            {
                _TimerValue = value;
                this.OnPropertyChanged("TimerValue");
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) =>
                    {
                        if (RecordingCompletedHandler != null)
                        {
                            RecordingCompletedHandler(String.Empty, 0);
                        }
                    });
                }
                return _CloseCommand;
            }
        }

        public ICommand RecordCommand
        {
            get
            {
                if (_RecordCommand == null)
                {
                    _RecordCommand = new RelayCommand((param) => OnRecordClick(param));
                }
                return _RecordCommand;
            }
        }


        private void OnRecordClick(object param)
        {
            try
            {
                int buttonType = (int)param;
                switch (buttonType)
                {
                    case RecorderConstants.BUTTON_START:
                        State = RecorderConstants.STATE_RUNNING;
                        audioCapture = new AudioCapture(RecorderConstants.MAX_VOICE_RECORD_TIME);
                        audioCapture.OnRecordingCountDownChange += (v) =>
                        {
                            TimerValue = TimeSpan.FromSeconds(v);
                            RecordingProgress = (v * 100) / RecorderConstants.MAX_VOICE_RECORD_TIME;
                        };
                        audioCapture.OnRecordingCompleted += () =>
                        {
                            State = RecorderConstants.STATE_COMPLETED;
                        };
                        string fileName = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + System.IO.Path.DirectorySeparatorChar + DefaultSettings.LOGIN_RING_ID + "_" + ModelUtility.CurrentTimeMillis() + "." + "mp3";
                        audioCapture.Record(fileName);
                        break;
                    case RecorderConstants.BUTTON_STOP:
                        
                        audioCapture.RequestForStop();
                        State = RecorderConstants.STATE_COMPLETED;
                        break;
                    case RecorderConstants.BUTTON_PAUSE:
                        State = RecorderConstants.STATE_PAUSED;
                        audioCapture.Pause();
                        break;
                    case RecorderConstants.BUTTON_RESUME:
                        State = RecorderConstants.STATE_RUNNING;
                        audioCapture.Resume();
                        break;
                    case RecorderConstants.BUTTON_RESTART:
                        State = RecorderConstants.STATE_RUNNING;
                        break;
                    case RecorderConstants.BUTTON_SEND:
                        if (RecordingCompletedHandler != null)
                        {
                            RecordingCompletedHandler(audioCapture._FileName, (int)_TimerValue.TotalSeconds);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: OnRecordClick() => " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void Dispose()
        {
            try
            {
                if (audioCapture != null)
                {
                    audioCapture.Dispose();
                    audioCapture = null;
                }
                Instance = null;
            }
            catch (Exception ex)
            {

                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {

        }
    }
}

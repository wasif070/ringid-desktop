﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for NewStatusView.xaml
    /// </summary>
    public partial class NewStatusView : UserControl
    {
        NewStatusViewModel newStatusViewModel = new NewStatusViewModel();
        public NewStatusView()
        {
            this.DataContext = newStatusViewModel;
            NewStatusHeaderButtonsView newStatusHeaderButtonsView = new NewStatusHeaderButtonsView();
            InitializeComponent();
            NewStatusHeaderContent.Content = newStatusHeaderButtonsView;
        }

        private void ParentBorder_DragLeave(object sender, DragEventArgs e)
        {
            newStatusViewModel.onRtbNewStatusAreaPreviewDragLeaveEvent(e);
        }

        private void imageScrollViwer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {
                //log.Error("Error:PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using View.Constants;
using View.Utility;
using View.Utility.Recorder;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for WNVideoRecorderPreviewPanel.xaml
    /// </summary>
    public partial class WNVideoRecorderPreviewPanel : Window, IDisposable
    {
        // When I wrote this code only God and I knew what I was writing. 
        // Now only God knows.
        #region Properties
        
        private UCVideoRecorderFeed _InstanceRecorder = null;
        private UCVideoPlayerFeed _InstancePlayer = null;
        private static WNVideoRecorderPreviewPanel _Instance = null;
        private ICommand _CloseCommand;
        private ICommand _ExpandCommand;

        public delegate void VideoAdded(string fileName, int duration);
        public event VideoAdded OnVideoAddedToUploadList;


        private bool ContainerHasChildren
        {
            get
            {
                return (TaskContainer.Children != null && TaskContainer.Children.Count > 0);
            }
        }
        public static WNVideoRecorderPreviewPanel Instance
        {
            get
            {
                _Instance = _Instance ?? new WNVideoRecorderPreviewPanel();
                return _Instance;
            }
        }
        #endregion

        #region Dependancy Properties
        public bool IsExpandView
        {
            get { return (bool)GetValue(IsExpandViewProperty); }
            set { SetValue(IsExpandViewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsExpandView.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsExpandViewProperty =
            DependencyProperty.Register("IsExpandView", typeof(bool), typeof(WNVideoRecorderPreviewPanel), new PropertyMetadata(false));
        #endregion

        #region Commands

        public ICommand CloseCommand
        {
            get
            {
                _CloseCommand = _CloseCommand ?? new RelayCommand((param) => OnCloseClicked(param));
                return _CloseCommand;
            }
        }
        private void OnCloseClicked(object param)
        {
            CloseAll();
            DeleteFilesInDirectory();
            this.CloseWindow();
        }
        public ICommand ExpandCommand { get { _ExpandCommand = _ExpandCommand ?? new RelayCommand((param) => OnExpandClicked(param)); return _ExpandCommand; } }

        private void OnExpandClicked(object param)
        {
            IsExpandView = IsExpandView?false: true;
            if (_InstanceRecorder != null) _InstanceRecorder.ExpandView = IsExpandView;
            else if (_InstancePlayer != null) _InstancePlayer.ExpandView = IsExpandView;
        }

        #endregion
        #region Constructor
        public WNVideoRecorderPreviewPanel()
        {
            InitializeComponent();
            DataContext = this;
            this.MouseDown += delegate { DragMove(); };
            this.Closed += (s, e) =>
            {
                CloseAll();
                _Instance = null;
            };
            this.PreviewKeyDown += WNVideoRecorderPreviewPanel_PreviewKeyDown;
        }

        void WNVideoRecorderPreviewPanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(System.Windows.Input.Key.Escape))
            {
                CloseAll();
                _Instance = null;
                CloseWindow();
            }
        }
        #endregion
        #region Show Methods
        private new void Show()
        {
            base.Show();

            this.Owner = Application.Current.MainWindow;
            
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);

            //System.Drawing.Rectangle workingArea = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
            //this.Left = (workingArea.Right - this.ActualWidth) / 2;
            //this.Top = (workingArea.Bottom - this.ActualHeight) / 2;
            //Instance = this;
        }
        public void ShowRecorder()
        {
            ClearTaskContainer();
            InitRecorder();
            _InstanceRecorder.ExpandView = IsExpandView;
            TaskContainer.Children.Add(_InstanceRecorder);
            App.Current.Dispatcher.Invoke(new Action(() =>
            {
                this.Show();
            }));
        }
        public void ShowPlayer()
        {
            _InstancePlayer.ExpandView = IsExpandView;
            TaskContainer.Children.Add(_InstancePlayer);
            App.Current.Dispatcher.Invoke(new Action(() =>
            {
                this.Show();
            }));
        }
        #endregion
        #region Close Methods
        public void CloseWindow(object param = null)
        {
            this.Owner = null;
            this.Close();
        }
        public void CloseRecorder()
        {
            _InstanceRecorder.Dispose();
            _InstanceRecorder = null;
        }
        public void ClosePlayer()
        {
            _InstancePlayer.Dispose();
            _InstancePlayer = null;
        }
        public void CloseAll()
        {
            if (_InstanceRecorder != null) CloseRecorder();
            if (_InstancePlayer != null) ClosePlayer();
        }
        #endregion
        #region Initialize Methods
        private void InitRecorder()
        {
            _InstanceRecorder = new UCVideoRecorderFeed();
            _InstanceRecorder.RecordingCompleteHandler += ((fileName, duration) =>
                {
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        //string tempFile = System.IO.Path.ChangeExtension(fileName, RecorderWebcamPreview.VIDEO_FORMAT);
                        //if (File.Exists(tempFile))
                        //{
                            //Media Player Loading Code here
                            ClearTaskContainer();
                            InitPlayer(fileName, duration);
                            ShowPlayer();
                        //}
                    }
                });
        }
        private void InitPlayer(string videoUriRaw, int duration)
        {
            _InstancePlayer = new UCVideoPlayerFeed(new Uri(videoUriRaw), duration);
            //.................
            _InstancePlayer.OnMediaPlayerLoaded += (s) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                    {
                        _InstancePlayer = (UCVideoPlayerFeed)s;
                        _InstancePlayer.PlayStart();
                    });
            };
            //.................
            _InstancePlayer.OnPlayComplete += () =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                    {
                        _InstancePlayer.MediaPlayer.Stop();
                        _InstancePlayer.MEDIA_STATE = StatusConstants.MEDIA_INIT_STATE;
                        _InstancePlayer.PlayProgress = 100;
                        _InstancePlayer.PlaybackSlider.Value = 0;
                    });
            };
            //.................
            _InstancePlayer.OnVideoRecordCommand += (fileName) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                    {
                        new Thread(() =>
                        {
                            if (File.Exists(fileName))
                                File.Delete(fileName);
                        }).Start();
                        ShowRecorder();
                    });
            };
            _InstancePlayer.OnVideoAdded += (fileName, videoDuration) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                    {
                        CloseAll();
                        ClearTaskContainer();
                        CloseWindow();
                        if (OnVideoAddedToUploadList != null) OnVideoAddedToUploadList(fileName, duration);
                    });
            };
        }
        #endregion
        #region Utility Methods

        public void DeleteFilesInDirectory()
        {
            string[] filters = new string[] { "*.avi", "*.mp4", "*.1" };
            foreach (var filter in filters)
            {
                string[] files = System.IO.Directory.GetFiles(RingIDSettings.TEMP_CAM_IMAGE_FOLDER, filter, SearchOption.TopDirectoryOnly);
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        System.IO.File.Delete(file);
                    }
                }
            }

        }
        public void ClearTaskContainer()
        {
            if (ContainerHasChildren)
            {
                CloseAll();
                TaskContainer.Children.Clear();
            }
        }
        public void Dispose()
        {
            //throw new NotImplementedException();
        }
        //public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}

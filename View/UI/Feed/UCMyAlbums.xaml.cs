﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.UI.Profile.MyProfile;
using View.Utility;
using View.Utility.Auth;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.ViewModel.NewStatus;
using System;
using log4net;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCMyAlbums.xaml
    /// </summary>
    public partial class UCMyAlbums : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyAlbums).Name);
        
        #region "Private Member"
        #endregion

        #region "Public Member"
        public NewStatusViewModel newStatusViewModel = null;
        #endregion

        #region "Property"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private int _SelectedImageCount;
        public int SelectedImageCount
        {
            get
            {
                return _SelectedImageCount;
            }
            set
            {
                if (value == _SelectedImageCount)
                    return;

                _SelectedImageCount = value;
                this.OnPropertyChanged("SelectedImageCount");
            }
        }

        private ObservableCollection<ImageModel> _ImageList = new ObservableCollection<ImageModel>();
        public ObservableCollection<ImageModel> ImageList
        {
            get
            {
                return _ImageList;
            }
            set
            {
                _ImageList = value;
                // this.OnPropertyChanged("ImageList");
            }
        }

        private int _BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
        public int BOTTOM_LOADING
        {
            get { return _BOTTOM_LOADING; }
            set
            {
                _BOTTOM_LOADING = value;
                this.OnPropertyChanged("BOTTOM_LOADING");
            }
        }
        #endregion

        #region "Constructor"
        public UCMyAlbums()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Command"
        private ICommand _ImageAlbumCommand;
        public ICommand ImageAlbumCommand
        {
            get
            {
                if (_ImageAlbumCommand == null)
                {
                    _ImageAlbumCommand = new RelayCommand(param => OnAlbumClick(param));
                }
                return _ImageAlbumCommand;
            }
        }
        #endregion

        #region "Event Handler"
        private void BtnPost_Click(object sender, RoutedEventArgs e)
        {
            BtnReset.IsEnabled = false;
            if (ImageList.Count > 0)
            {
                MainSwitcher.PopupController.PhotoSelectionView.OnCloseCommand();
                foreach (ImageModel model in ImageList)
                {
                    string convertedUrl = HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_300);
                    convertedUrl = convertedUrl.Replace("/", "_");

                    if (model.ImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
                    {
                        convertedUrl = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    }
                    else if (model.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                    {
                        convertedUrl = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    }
                    else if (model.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
                    {
                        convertedUrl = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    }

                    ImageUploaderModel imgUpldModel = new ImageUploaderModel
                    {
                        FilePath = convertedUrl,
                        IsFromUpload = false,
                        CaptionEnabled = false,
                        ImageUrl = model.ImageUrl
                    };
                    newStatusViewModel.NewStatusImageUpload.Add(imgUpldModel);
                    newStatusViewModel.SetStatusImagePanelVisibility();
                    if (newStatusViewModel != null)
                    {
                        newStatusViewModel.UploadingText = "Selected Image(s): " + newStatusViewModel.NewStatusImageUpload.Count;
                    }
                    model.IsSelected = false;
                    SelectedImageCount--;
                }
                ImageList.Clear();
            }
            else
                UIHelperMethods.ShowWarning("Please select image to post !", "Post failed");
            //{
            //    CustomMessageBox.ShowWarning("Please select image to post !");
            //}
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            if (ImageList.Count > 0)
            {
                BtnPost.IsEnabled = false;
                foreach (ImageModel model in ImageList)
                {
                    model.IsSelected = false;
                }
                SelectedImageCount = 0;
                ImageList.Clear();
            }
        }

        private void ScrollViewer_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        {
            if (BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                LoadMorePhotos();
            }
            e.Handled = true;
        }
        #endregion

        #region "Private Method"
        private void LoadMyImages(string AlbumId)
        {
            if ((AlbumId == DefaultSettings.PROFILE_IMAGE_ALBUM_ID && RingIDViewModel.Instance.ProfileImageList.Count == 1)
                    || (AlbumId == DefaultSettings.COVER_IMAGE_ALBUM_ID && RingIDViewModel.Instance.CoverImageList.Count == 1)
                    || (AlbumId == DefaultSettings.FEED_IMAGE_ALBUM_ID && RingIDViewModel.Instance.FeedImageList.Count == 1))
            {
                //SendDataToServer.SendAlbumRequest(0, AlbumId, 0);//to do
            }
        }

        public void OnImageClick(object param)
        {
            ImageModel model = (ImageModel)param;
            // ImageModel Imgmodel = RingIDViewModel.Instance.GetImageModelByID(model.ImageId);
            // ImageModel Imgmodel = RingIDViewModel.Instance.ProfileImageList.Where(P => P.ImageUrl.Equals(model.ImageUrl)).FirstOrDefault();
            if (model != null)
            {
                if (ImageList.Contains(model))
                {
                    model.IsSelected = false;
                    SelectedImageCount--;
                    ImageList.Remove(model);
                }
                else
                {
                    SelectedImageCount++;
                    model.IsSelected = true;
                    ImageList.Add(model);
                }
                BtnPost.IsEnabled = true;
                BtnReset.IsEnabled = true;
            }
        }

        private void OnAlbumClick(object param)
        {
            try
            {
                AlbumModel model = (AlbumModel)param;
                ThumbPanel.Visibility = Visibility.Collapsed;
                if (UCImageContentViewForFeedAlbum.Instance == null) UCImageContentViewForFeedAlbum.Instance = new UCImageContentViewForFeedAlbum();
                else if (UCImageContentViewForFeedAlbum.Instance.Parent is Border) ((Border)UCImageContentViewForFeedAlbum.Instance.Parent).Child = null;
                ImageAlbumsDetailsPanel.Child = UCImageContentViewForFeedAlbum.Instance;
                UCImageContentViewForFeedAlbum.Instance.SetValue(model, () =>
                {
                    ThumbPanel.Visibility = Visibility.Visible;
                    ImageAlbumsDetailsPanel.Child = null;
                    return 0;
                });
            }
            catch (Exception)
            {
            }
        }
        #endregion

        #region "Public Method"
        public void LoadAllImages(NewStatusViewModel newStatusViewModel)
        {
            this.newStatusViewModel = newStatusViewModel;
            this.DataContext = this;
            if (DefaultSettings.MY_IMAGE_ALBUMS_COUNT == -1)
            {
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
                SendDataToServer.ListOfImageAlbumsOfUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_IMAGE, Guid.Empty);                
            }
        }

        public void ShowHideShowMoreLoading(bool makeVisible)
        {
            try
            {
                if (makeVisible)
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                }
                else
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void LoadMorePhotos()
        {
            try
            {
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
                SendDataToServer.ListOfImageAlbumsOfUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_IMAGE, RingIDViewModel.Instance.ImageAlbumNpUUId);
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadMorePhotos() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Loaded_Unloaded Events"
        private void root_Loaded(object sender, RoutedEventArgs e)
        {
            Scroll.PreviewKeyDown += ScrollViewer_PreviewKeyDown;
        }

        private void root_Unloaded(object sender, RoutedEventArgs e)
        {
            Scroll.PreviewKeyDown -= ScrollViewer_PreviewKeyDown;
        }
        #endregion        
    }
}

﻿using log4net;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Constants;
using View.Utility;
using View.Utility.Recorder;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCVideoRecorderView.xaml
    /// </summary>
    public partial class UCVideoRecorderView : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCVideoRecorderView).Name);

        private int _MaxVideoTime = 99999;

        #region "Constructor"
        public UCVideoRecorderView()
        {
            InitializeComponent();
            this.DataContext = this;
            this.State = RecorderConstants.STATE_READY;
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private bool _IsExpandView = false;
        public bool IsExpandView
        {
            get
            {
                return _IsExpandView;
            }
            set
            {
                if (value == _IsExpandView)
                    return;
                _IsExpandView = value;
                OnPropertyChanged("IsExpandView");
            }
        }
        private bool _HasWebCamError;
        public bool HasWebCamError 
        { 
            get
            { 
                return _HasWebCamError; 
            } 
            set 
            { 
                _HasWebCamError = value;
                this.OnPropertyChanged("HasWebCamError");
            }
        }

        private string _ErrorMessage = string.Empty;
        public string ErrorMessage
        {
            get 
            { 
                return _ErrorMessage; 
            }
            set
            { 
                _ErrorMessage = value;
                this.OnPropertyChanged("ErrorMessage"); 
            }
        }

        private int _State = RecorderConstants.STATE_READY;
        public int State 
        { 
            get 
            { 
                return _State; 
            } 
            set 
            {
                _State = value;
                this.OnPropertyChanged("State");
            } 
        }

        private string _FileName = string.Empty;
        public string FileName { get { return _FileName; } set { _FileName = value; this.OnPropertyChanged("FileName"); } }

        private TimeSpan _TimerValue = TimeSpan.FromSeconds(0);
        public TimeSpan TimerValue { get { return _TimerValue; } set { _TimerValue = value; this.OnPropertyChanged("TimerValue"); } }

        private bool _SendForConversion;
        public bool SendForConversion { get { return _SendForConversion; } set { _SendForConversion = value; this.OnPropertyChanged("SendForConversion"); } }
        #endregion

        #region Delegates
        public delegate void OnRecordingCompleted(string fileName, int countDown);
        public event OnRecordingCompleted RecordingCompleteHandler;
        #endregion

        #region Animation Event
        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            try
            {
                // int errorCode = WebCamPreview.InitWebcamDevices(_MaxVideoTime, (bool)chkIncludeVoice.IsChecked);
                int errorCode = WebCamPreview.InitWebcamDevices(_MaxVideoTime);
                if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND)
                {
                    HasWebCamError = true;
                    ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                }
                else if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING)
                {
                    HasWebCamError = true;
                    ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
                }
                ((AnimationClock)sender).Completed -= DoubleAnimation_Completed;
                recorderContainer.Triggers.Clear();
                //System.Diagnostics.Debug.WriteLine("Error Message => " + ErrorMessage);
            }
            catch (Exception ex)
            {

                log.Error("Error: DoubleAnimationCompleted()=> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        #endregion

        #region Command
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            UCVideoRecorderPreviewPanel.Instance.Focusable = true;
            Keyboard.Focus(UCVideoRecorderPreviewPanel.Instance);
        }

        private ICommand _RecordCommand;
        public ICommand RecordCommand
        {
            get
            {
                if (_RecordCommand == null)
                {
                    _RecordCommand = new RelayCommand((param) => OnRecordClick(param));
                }
                return _RecordCommand;
            }
        }
        private void OnRecordClick(object param)
        {
            try
            {

                int buttonType = (int)param;
                switch (buttonType)
                {
                    case RecorderConstants.BUTTON_START:
                        //_FileName = RingIDSettings.TEMP_CAM_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + DefaultSettings.LOGIN_USER_ID + "_" + ModelUtility.CurrentTimeMillisLocal() + "." + "mp4";
                        _FileName = RingIDSettings.TEMP_CAM_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + "Video_" + ModelUtility.CurrentTimeMillis() + "." + "mp4";
                        WebCamPreview.OnRecordingCountDownChange += (v) =>
                        {
                            TimerValue = TimeSpan.FromSeconds(v);
                        };
                        WebCamPreview.OnRecordingCompleted += () =>
                        {
                            if ((int)TimerValue.TotalSeconds > 1)
                            {
                                State = RecorderConstants.STATE_COMPLETED;
                                SendToMediaPlayer();
                            }
                            else
                            {
                                State = RecorderConstants.STATE_READY;
                                TimerValue = TimeSpan.FromSeconds(0);
                            }
                        };
                        int errorCode = WebCamPreview.StartRecording(_FileName);
                        if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND)
                        {
                            HasWebCamError = true;
                            ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                        }
                        else if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING)
                        {
                            HasWebCamError = true;
                            ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
                        }
                        else
                        {
                            State = RecorderConstants.STATE_RUNNING;
                        }
                        break;
                    case RecorderConstants.BUTTON_STOP:
                        WebCamPreview.RequestForStop();
                        break;
                    case RecorderConstants.BUTTON_RESTART:
                        _FileName = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + System.IO.Path.DirectorySeparatorChar + DefaultSettings.LOGIN_RING_ID + "_" + ModelUtility.CurrentTimeMillis() + "." + "mp4";
                        errorCode = WebCamPreview.StartRecording(_FileName);
                        if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND)
                        {
                            HasWebCamError = true;
                            ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                        }
                        else if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING)
                        {
                            HasWebCamError = true;
                            ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
                        }
                        else
                        {
                            State = RecorderConstants.STATE_RUNNING;
                        }
                        break;
                    case RecorderConstants.BUTTON_SEND:
                        SendForConversion = true;
                        if (RecordingCompleteHandler != null)
                            RecordingCompleteHandler(_FileName, (int)TimerValue.TotalSeconds);
                        break;
                }

            }
            catch (Exception ex)
            {

                log.Error("UCFeedVideoRecorder.OnRecordClick() => " + ex.Message + "\n" + ex.StackTrace);

                //Application.Current.Dispatcher.Invoke(() => {
                //    ((this.Parent as UCVideoRecorderPreviewPanel).Parent as UCNewStatus).ForceCloseVideoRecorderPreviewPanel();
                //    CustomMessageBox.ShowInformation("Recording Unavailable!");
                //});

            }
        }

        private void SendToMediaPlayer()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                SendForConversion = true;
                if (RecordingCompleteHandler != null)
                    RecordingCompleteHandler(_FileName, (int)TimerValue.TotalSeconds);
            }
                );
        }
        #endregion
    }
}

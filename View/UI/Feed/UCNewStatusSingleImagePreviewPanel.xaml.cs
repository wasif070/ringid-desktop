﻿using Models.Constants;
using Models.Stores;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCNewStatusSingleImagePreviewPanel.xaml
    /// </summary>
    public partial class UCNewStatusSingleImagePreviewPanel : UserControl
    {
        NewStatusViewModel newStatusViewModel = null;
        public UCNewStatusSingleImagePreviewPanel()
        {
            InitializeComponent();
        }

        private void imageUploadEmoticonBtn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            UIElement uiElement = (UIElement)btn;
            ImageUploaderModel model = (ImageUploaderModel)btn.DataContext;
            MainSwitcher.PopupController.EmoticonPopupWrapper.Show(uiElement, UCEmoticonPopup.TYPE_DEFAULT, (emo) =>
             {
                 if (!String.IsNullOrWhiteSpace(emo))
                 {
                     rtb.AppendText(emo);
                 }
                 return 0;
             });
        }

        private RichTextCommentEdit rtb;
        private void rtbRichText_Loaded(object sender, RoutedEventArgs e)
        {
            rtb = (RichTextCommentEdit)sender;
            //rtb.Document.Blocks.Clear();
            ImageUploaderModel model = (ImageUploaderModel)rtb.DataContext;
            rtb.AppendText(model.ImageCaption);
            newStatusViewModel = (NewStatusViewModel)this.Tag;
            if (newStatusViewModel.FeedWallType < 0)
            {
                crossButton.Visibility = Visibility.Hidden;
                emoticonBtn.IsEnabled = false;
                textBoxImageUpload.IsEnabled = false;
            }
            rtb.OnGotFocusOnThis += (onFocus) =>
            {
                if (onFocus)
                {
                    if (newStatusViewModel.FeedWallType == 0)
                        NewsFeedViewModel.Instance.AllFeedsScrollEnabled = false;
                    else if (newStatusViewModel.FeedWallType == 1)
                        View.UI.Profile.MyProfile.UCMyNewsFeeds.Instance.scroll.scrollEnabled = false;
                    else if (newStatusViewModel.FeedWallType == 2)
                        View.UI.Profile.FriendProfile.UCFriendNewsFeeds.Instance.scroll.scrollEnabled = false;
                    //else if (newStatusViewModel.FeedWallType == 0)
                    //    View.UI.Circle.UCCircleAllNewsFeeds.Instance.scroll.scrollEnabled = false;
                }
                else
                {
                    if (newStatusViewModel.FeedWallType == 0)
                        NewsFeedViewModel.Instance.AllFeedsScrollEnabled = true;
                    else if (newStatusViewModel.FeedWallType == 1)
                        View.UI.Profile.MyProfile.UCMyNewsFeeds.Instance.scroll.scrollEnabled = true;
                    else if (newStatusViewModel.FeedWallType == 2)
                        View.UI.Profile.FriendProfile.UCFriendNewsFeeds.Instance.scroll.scrollEnabled = true;
                }
            };
        }

        private void CrossButton_Click(object sender, RoutedEventArgs e)
        {
            if (newStatusViewModel != null && newStatusViewModel.IsRtbNewStatusAreaEnabled)
            {
                Control control = (Control)sender;
                ImageUploaderModel model = (ImageUploaderModel)control.DataContext;

                if (newStatusViewModel.NewStatusImageUpload != null)
                {
                    newStatusViewModel.NewStatusImageUpload.Remove(model);
                    if (ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.ContainsKey(model.FilePath))
                    {
                        ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[model.FilePath] = null;
                        ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.Remove(model.FilePath);
                    }
                    newStatusViewModel.UploadingText = "Selected Image(s): " + newStatusViewModel.NewStatusImageUpload.Count;
                }
                if (newStatusViewModel.NewStatusImageUpload.Count == 0)
                {
                    newStatusViewModel.StatusImagePanelVisibility = Visibility.Collapsed;
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Collapsed;
                    newStatusViewModel.ActionRemoveLinkPreview();
                    newStatusViewModel.ImageAlbumTitleTextBoxText = string.Empty;
                    newStatusViewModel.AddToImageAlbumButtonVisibility = Visibility.Visible;
                    newStatusViewModel.SelectedImageAlbumCrossButtonVisibility = Visibility.Collapsed;
                    newStatusViewModel.PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                    newStatusViewModel.IsPrivacyButtonEnabled = true;
                }
            }
        }

        private void textBoxImageUpload_TextChanged(object sender, TextChangedEventArgs e)
        {
            Control control = (Control)sender;
            RichTextCommentEdit rtbox = (RichTextCommentEdit)control;
            ImageUploaderModel model = (ImageUploaderModel)control.DataContext;
            model.ImageCaption = rtbox.Text;
        }
    }
}

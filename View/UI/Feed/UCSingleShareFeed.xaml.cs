﻿using Auth.Service.Feed;
using Auth.Service.Notification;
using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.ViewModel;
using View.Utility.Feed;
using View.UI.PopUp;
using View.Utility.RingPlayer;
using System.Windows.Threading;
using Newtonsoft.Json.Linq;
using View.Utility.Auth;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCSingleShareFeed.xaml
    /// </summary>
    public partial class UCSingleShareFeed : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleShareFeed).Name);

        DispatcherTimer _NameToolTip = null;

        public UCSingleShareFeed()
        {
            InitializeComponent();
        }

        // public static readonly DependencyProperty ArgumentProperty = DependencyProperty.Register("Argument", typeof(object), typeof(UCSingleShareFeed), new PropertyMetadata((s, e) =>
        // {
        //     if (e.NewValue != null && e.NewValue is FeedModel)
        //     {
        //         UCSingleShareFeed panel = (UCSingleShareFeed)s;
        //         panel.FeedModel = (FeedModel)e.NewValue;
        //         panel.DataContext = panel;
        //     }
        // }
        //));

        // public object Argument
        // {
        //     get { return (object)GetValue(ArgumentProperty); }
        //     set
        //     {
        //         SetValue(ArgumentProperty, value);
        //     }
        // }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region"Property "

        private UCNameToolTipPopupView ucNameToolTipPopupView { get; set; }

        private UCEditDeleteReportOptionsPopup EditDeleteReportOptionsPopup { get; set; }
        #endregion "Property"

        #region "ICommand"
        private ICommand _nameToolTipOpenCommand;
        public ICommand NameToolTipOpenCommand
        {
            get
            {
                if (_nameToolTipOpenCommand == null)
                {
                    _nameToolTipOpenCommand = new RelayCommand(param => OnNameToolTipOpenCommand(param));
                }
                return _nameToolTipOpenCommand;
            }
        }
        int type;
        private void OnNameToolTipOpenCommand(object parameter)
        {
            if (parameter is string)
            {
                try
                {
                    type = Convert.ToInt32(parameter);
          
                    if (_NameToolTip == null)
                    {
                        _NameToolTip = new DispatcherTimer();
                        _NameToolTip.Interval = TimeSpan.FromSeconds(1);
                        _NameToolTip.Tick += (s, es) =>
                        {
                            if (ucNameToolTipPopupView != null)
                                UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNameToolTipPopupView);
                            ucNameToolTipPopupView = new UCNameToolTipPopupView(UCGuiRingID.Instance.MotherPanel);
                            ucNameToolTipPopupView.Show();
                            if (type == 0 && name.IsMouseOver)
                            {
                                FeedModel feedModel = ((FeedHolderModel)name.DataContext).Feed;
                                ucNameToolTipPopupView.ShowPopUp(name, feedModel.WallOrContentOwner);
                            }
                           
                            ucNameToolTipPopupView.OnRemovedUserControl += () =>
                            {
                                ucNameToolTipPopupView = null;
                                //if (_NameToolTip != null)
                                //    _NameToolTip = null;
                            };

                            _NameToolTip.Stop();
                            _NameToolTip.Interval = TimeSpan.FromSeconds(0.2);
                        };
                    }
                    _NameToolTip.Stop();
                    _NameToolTip.Start();
                }
                finally { }
            }
        }

        private ICommand _nameToolTipHiddenCommand;
        public ICommand NameToolTipHiddenCommand
        {
            get
            {
                if (_nameToolTipHiddenCommand == null) _nameToolTipHiddenCommand = new RelayCommand(param => OnNameToolTipHiddenCommand(param));
                return _nameToolTipHiddenCommand;
            }
        }

        private void OnNameToolTipHiddenCommand(object parameter)
        {
            if (ucNameToolTipPopupView != null
                && ucNameToolTipPopupView.PopupToolTip.IsOpen
                && !ucNameToolTipPopupView.contentContainer.IsMouseOver)
                ucNameToolTipPopupView.HidePopUp(false);
        }

        private ICommand _editDeleteCommand;
        public ICommand editDeleteCommand
        {
            get
            {
                if (_editDeleteCommand == null)
                {
                    _editDeleteCommand = new RelayCommand(param => OnEditDeleteComman(param));
                }
                return _editDeleteCommand;
            }
        }
        private void OnEditDeleteComman(object parameter)
        {
            try
            {
                if (parameter is FeedModel)
                {
                    FeedModel _FeedModel = (FeedModel)parameter;
                    bool hideOff = true;
                    bool hidethis = true;
                    bool saveUnsave = true;
                    bool followUnfollow = false;
                    bool reportOn = false;

                    if (_FeedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        hideOff = true;
                        hidethis = true;
                        reportOn = false;
                    }
                    else
                    {
                        if ((int)Tag == DefaultSettings.FEED_TYPE_MY || (int)Tag == DefaultSettings.FEED_TYPE_FRIEND || (int)Tag == DefaultSettings.FEED_TYPE_MUSIC_PROFILE
                            || (int)Tag == DefaultSettings.FEED_TYPE_NEWSPORTAL_PROFILE || (int)Tag == DefaultSettings.FEED_TYPE_PAGE_PROFILE
                            || (int)Tag == DefaultSettings.FEED_TYPE_CELEBRITY_PROFILE || (int)Tag == DefaultSettings.FEED_TYPE_CIRCLE
                            || (int)Tag == DefaultSettings.FEED_TYPE_DETAILS)
                        {
                            hideOff = true;
                            hidethis = true;
                            if (_FeedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                            {
                                reportOn = false;
                            }
                            else reportOn = true;
                        }
                        else
                        {
                            hideOff = false;
                            hidethis = false;
                            reportOn = true;
                        }
                    }

                    if (EditDeleteReportOptionsPopup != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(EditDeleteReportOptionsPopup);
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        EditDeleteReportOptionsPopup = new UCEditDeleteReportOptionsPopup(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                    else EditDeleteReportOptionsPopup = new UCEditDeleteReportOptionsPopup(UCGuiRingID.Instance.MotherPanel);
                    EditDeleteReportOptionsPopup.Show();
                    EditDeleteReportOptionsPopup.ShowEditDeletePopup(editDelete, hideOff, hidethis, saveUnsave, followUnfollow, reportOn, _FeedModel, _FeedModel.ParentFeed != null);
                    EditDeleteReportOptionsPopup.OnRemovedUserControl += () =>
                    {
                        EditDeleteReportOptionsPopup = null;
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        {
                            UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        }
                    };
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        //private ICommand loadedControl;
        //public ICommand LoadedControl
        //{
        //    get
        //    {
        //        if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadMultipleShareList(param));
        //        return loadedControl;
        //    }
        //}
        //private void OnLoadMultipleShareList(Object parameter)
        //{
        //    if (parameter is FeedHolderModel)
        //    {
        //        FeedHolderModel holder = (FeedHolderModel)parameter;
        //        FeedModel _FeedModel = holder.Feed;
              
        //        if (holder.FeedPanelType == 20)
        //        {
        //            if (_FeedModel.IsShareListVisible == false)
        //            {
        //                _FeedModel.IsShareListVisible = true;
        //                //if (_FeedModel.WhoShareList != null && _FeedModel.WhoShareList.Count == _FeedModel.LikeCommentShare.NumberOfShares)
        //                //{
        //                //    _FeedModel.IsShareListVisible = true;
        //                //    _FeedModel.ShowMoreSharesState = 0;
        //                //}
        //                //else
        //                //{
        //                //    JObject pakToSend = new JObject();
        //                //    pakToSend[JsonKeys.NewsfeedId] = _FeedModel.NewsfeedId;
        //                //    pakToSend[JsonKeys.StartLimit] = 0;
        //                //    (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_WHO_SHARES_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        //                //}
        //            }
        //            else
        //            {
        //                _FeedModel.IsShareListVisible = false;
        //            }
        //        }
        //    }
        //}
        #endregion

        private void shareMoreMediaBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (_FeedModel.ParentFeed.ExtraMediaCount.StartsWith("+"))
                //{
                //    for (int i = _FeedModel.ParentFeed.MediaContent.FeedMediaList.Count; i < _FeedModel.ParentFeed.MediaContent.MediaList.Count; i++)
                //    {
                //        _FeedModel.ParentFeed.MediaContent.FeedMediaList.Add(_FeedModel.ParentFeed.MediaContent.MediaList.ElementAt(i));
                //    }
                //    _FeedModel.ParentFeed.ExtraMediaCount = "Show Less";
                //}
                //else
                //{
                //    for (int i = _FeedModel.ParentFeed.MediaContent.FeedMediaList.Count - 1; i >= 2; i--)
                //    {
                //        _FeedModel.ParentFeed.MediaContent.FeedMediaList.RemoveAt(i);
                //    }

                //    _FeedModel.ParentFeed.ExtraMediaCount = "+" + (_FeedModel.ParentFeed.MediaContent.MediaList.Count - 2) + " More";
                //}
            }

            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
    }
}

﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.Utility;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCVideoRecorderPlayerView.xaml
    /// </summary>
    public partial class UCVideoRecorderPlayerView : UserControl, INotifyPropertyChanged
    {
        private CustomizeTimer _PlayTimer = null;
        private int _PlayProgress = 0;
        Thumb progressBarThumb;

        #region "Constructor"
        public UCVideoRecorderPlayerView(Uri videoUri = null, int duration = 0)
        {
            InitializeComponent();
            this.DataContext = this;
            this.VideoSource = videoUri;
            this.TimerValue = TimeSpan.FromSeconds(duration);
            //this.DataContext = this;
            this._PlayTimer = new CustomizeTimer();
            this._PlayTimer.InitIntervalInSecond = 1;
            this._PlayTimer.Tick += this._PlayTimer_Tick;
            this.MediaPlayer.ScrubbingEnabled = true;
            PlayStart();
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private bool _IsExpandView = false;
        public bool IsExpandView
        {
            get
            {
                return _IsExpandView;
            }
            set
            {
                if (value == _IsExpandView)
                    return;
                _IsExpandView = value;
                OnPropertyChanged("IsExpandView");
            }
        }

        private TimeSpan _TimerValue;
        public TimeSpan TimerValue
        {
            get
            {
                return _TimerValue;
            }
            set
            {
                _TimerValue = value;
                this.OnPropertyChanged("TimerValue");
            }
        }

        private int _MEDIA_STATE;
        public int MEDIA_STATE 
        { 
            get 
            { 
                return _MEDIA_STATE; 
            } 
            set 
            { 
                _MEDIA_STATE = value;
                this.OnPropertyChanged("MEDIA_STATE"); 
            } 
        }

        public int PlayProgress 
        { 
            get 
            { 
                return _PlayProgress;
            }
            set
            { 
                _PlayProgress = value; 
                this.OnPropertyChanged("PlayProgress"); 
            } 
        }

        private Uri _VideoSource;
        public Uri VideoSource 
        { 
            get 
            { 
                return _VideoSource;
            }
            set 
            { 
                _VideoSource = value;
                this.OnPropertyChanged("VideoSource"); 
            }
        }

        //private TimeSpan _ProgressTime = TimeSpan.FromSeconds(0);
        //public TimeSpan ProgressTime { get { return _ProgressTime; } set { _ProgressTime = value; this.OnPropertyChanged("ProgressTime"); } }

        private double seekBarValue;
        public double SeekBarValue
        {
            get { return seekBarValue; }
            set { seekBarValue = value; this.OnPropertyChanged("SeekBarValue"); }
        }

        private double _SliderDuration;
        public double SliderDuration
        {
            get { return _SliderDuration = TimerValue.TotalSeconds; }
            set { _SliderDuration = value; OnPropertyChanged("SliderDuration"); }
        }
        #endregion

        #region Delegates
        public delegate void PlayCompleteHandler();
        public event PlayCompleteHandler OnPlayComplete;

        public delegate void MediaPlayerLoadedHandler(object sender);
        public event MediaPlayerLoadedHandler OnMediaPlayerLoaded;

        public delegate void VideoRecordHandler(string fileName);
        public event VideoRecordHandler OnVideoRecordCommand;

        public delegate void PlayStartHandler();
        public event PlayStartHandler OnPlayStart;

        public delegate void PlayPauseHandler(int seconds);
        public event PlayPauseHandler OnPlayPause;

        public delegate void VideoAddToHandler(string fileName, int duration);
        public event VideoAddToHandler OnVideoAdded;

        public delegate void PlayProgressHandler(int seconds, int duration);
        public event PlayProgressHandler OnPlayProgress;
        #endregion

        #region Control Method
        public void PlayPause()
        {
            OnClickHandler("2");
        }
        public void PlayStart()
        {
            OnClickHandler("1");
            _PlayTimer.Start();
        }
        #endregion

        #region"Commands"

        private ICommand loadedMainControl;
        public ICommand LoadedMainControl
        {
            get
            {
                if (loadedMainControl == null) loadedMainControl = new RelayCommand(param => OnLoadedMainControl());
                return loadedMainControl;
            }
        }
        private void OnLoadedMainControl()
        {
            var slider = (Slider)PlaybackSlider;
            var track = (System.Windows.Controls.Primitives.Track)slider.Template.FindName("PART_Track", slider);
            if (track != null)
            {
                progressBarThumb = track.Thumb;
                //progressBarThumb.DragStarted += progressBarThumb_DragStarted;
                progressBarThumb.DragCompleted += progressBarThumb_DragCompleted;
                //progressBarThumb.MouseEnter += progressBarThumb_MouseEnter;
            }
            PlaybackSlider.ValueChanged += PlaybackSlider_ValueChanged;
            UCVideoRecorderPreviewPanel.Instance.Focusable = true;
            Keyboard.Focus(UCVideoRecorderPreviewPanel.Instance);
        }

        private ICommand unLoadedMainControl;
        public ICommand UnLoadedMainControl
        {
            get
            {
                if (unLoadedMainControl == null) unLoadedMainControl = new RelayCommand(param => OnUnLoadedMainControl());
                return unLoadedMainControl;
            }
        }
        private void OnUnLoadedMainControl()
        {
            if (progressBarThumb != null)
            {
                //progressBarThumb.DragStarted -= progressBarThumb_DragStarted;
                progressBarThumb.DragCompleted -= progressBarThumb_DragCompleted;
                //progressBarThumb.MouseEnter -= progressBarThumb_MouseEnter;
            }
            PlaybackSlider.ValueChanged -= PlaybackSlider_ValueChanged;
        }
        private ICommand _OnClickCommand;
        public ICommand OnClickCommand
        {
            get
            {
                _OnClickCommand = _OnClickCommand ?? new RelayCommand((param) => OnClickHandler(param), (param) => CanOnClickHandler(param));
                return _OnClickCommand;
            }
        }
        private bool CanOnClickHandler(object param)
        {
            return (TimerValue.TotalSeconds > 0);
        }

        private void OnClickHandler(object param) // play pause
        {
            int type = int.Parse(param.ToString());
            if (type == StatusConstants.MEDIA_PLAY_STATE)
            {
                MediaPlayer.Play();
                MEDIA_STATE = StatusConstants.MEDIA_PLAY_STATE;
                if (OnPlayStart != null) { OnPlayStart(); }
            }
            else if (type == StatusConstants.MEDIA_PAUSE_STATE)
            {
                MediaPlayer.Pause();
                MEDIA_STATE = StatusConstants.MEDIA_PAUSE_STATE;
                if (OnPlayPause != null)
                {
                    OnPlayPause((int)MediaPlayer.Position.TotalSeconds);
                }
            }
        }
        #endregion

        #region "Event Trigger"
        private void MediaPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (MediaPlayer.NaturalDuration.HasTimeSpan)
            {
                TimerValue = MediaPlayer.NaturalDuration.TimeSpan;
                PlayProgress = (int)((MediaPlayer.Position.Seconds * 100) / MediaPlayer.NaturalDuration.TimeSpan.TotalSeconds);
            }
            if (OnMediaPlayerLoaded != null) OnMediaPlayerLoaded(this);
        }
        private void MediaPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (OnPlayComplete != null) OnPlayComplete();
        }
        private void MediaPlayer_OnRestartRecordingClicked(object sender, RoutedEventArgs e)
        {
            if (OnVideoRecordCommand != null) OnVideoRecordCommand(MediaPlayer.Source.AbsolutePath);
        }

        private void MediaPlayer_OnVideoAcceptedForUploading(object sender, RoutedEventArgs e)
        {
            if (OnVideoAdded != null) OnVideoAdded(MediaPlayer.Source.AbsolutePath, (int)TimerValue.TotalSeconds);
            //if (UCVideoRecorderPreviewPanel.Instance != null && UCVideoRecorderPreviewPanel.Instance.IsVisible)
            //    UCVideoRecorderPreviewPanel.Instance.Hide();
        }
        void progressBarThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            MediaPlayer.Position = TimeSpan.FromSeconds(SeekBarValue);
            //DataContext = this;
            //DataModel.MediaElementPosition = TimeSpan.FromSeconds(SeekBarValue);
        }

        private void PlaybackSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SeekBarValue = (double)e.NewValue;
            MediaPlayer.Position = TimeSpan.FromSeconds(SeekBarValue);
        }

        #endregion

        private void _PlayTimer_Tick(int counter, bool initTick = false, object state = null)
        {
            try
            {
                if (initTick) return;
                bool hasTimeSpan = false;
                double position = 0;
                double totalSeconds = 0;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    hasTimeSpan = MediaPlayer.NaturalDuration.HasTimeSpan;
                    if (hasTimeSpan)
                    {
                        position = MediaPlayer.Position.TotalSeconds;
                        totalSeconds = MediaPlayer.NaturalDuration.TimeSpan.TotalSeconds;
                    }
                }, DispatcherPriority.Send);
                if (hasTimeSpan)
                {
                    if (_MEDIA_STATE == StatusConstants.MEDIA_PLAY_STATE)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            //SeekBarValue = (MediaPlayer.Position.TotalSeconds * PlaybackSlider.Width * 10) / SliderDuration;     // When I wrote this only God and I knew the calculation. Now Only God Knows. --Sakib Arman
                            SeekBarValue = MediaPlayer.Position.TotalSeconds;
                            //this.ProgressTime = TimeSpan.FromSeconds(this.MediaPlayer.Position.TotalSeconds);
                            this.PlayProgress = (int)((this.MediaPlayer.Position.TotalSeconds * 125) / this.MediaPlayer.NaturalDuration.TimeSpan.TotalSeconds);
                        });
                        if (OnPlayProgress != null) OnPlayProgress((int)position, (int)totalSeconds);
                    }
                    if (position == totalSeconds)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            MediaPlayer.Stop();
                            _MEDIA_STATE = StatusConstants.MEDIA_INIT_STATE;
                            MediaPlayer.Position = TimeSpan.FromSeconds(0);
                            DataContext = this;
                        }, DispatcherPriority.Send);

                        if (OnPlayComplete != null) OnPlayComplete();
                    }
                }
            }
            catch (Exception) { }
        }
        #region Animation Event
        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            try
            {
                ((AnimationClock)sender).Completed -= DoubleAnimation_Completed;
                previewContainer.Triggers.Clear();

            }
            catch (Exception) { }
        }
        #endregion 
    }
}

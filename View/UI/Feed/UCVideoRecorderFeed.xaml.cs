﻿using log4net;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Constants;
using View.Utility;
using View.Utility.Recorder;
using View.Utility.WPFMessageBox;

namespace View.UI.Feed
{
    /// <summary>
    /// Interaction logic for UCVideoRecorderFeed.xaml
    /// </summary>
    public partial class UCVideoRecorderFeed : UserControl, INotifyPropertyChanged, IDisposable
    {
        private UCVideoRecorderFeed Instance = null;
        private static readonly ILog log = LogManager.GetLogger(typeof(UCVideoRecorderFeed).Name);

        #region Properties Private
        private int _MaxVideoTime = 99999; 
        private int _State = RecorderConstants.STATE_READY;
        private TimeSpan _TimerValue = TimeSpan.FromSeconds(0);

        private bool _HasWebCamError = false;
        private string _ErrorMessage = string.Empty;

        private string _FileName = string.Empty;

        private bool _SendForConversion = false;
        #endregion
        #region Properties Public
        public int State { get { return _State; } set { _State = value; this.OnPropertyChanged("State"); } }
        public TimeSpan TimerValue { get { return _TimerValue; } set { _TimerValue = value; this.OnPropertyChanged("TimerValue"); } }

        public bool HasWebCamError { get { return _HasWebCamError; } set { _HasWebCamError = value; this.OnPropertyChanged("HasWebCamError"); } }
        public string ErrorMessage { get { return _ErrorMessage; } set { _ErrorMessage = value; this.OnPropertyChanged("ErrorMessage"); } }

        public string FileName { get { return _FileName; } set { _FileName = value; this.OnPropertyChanged("FileName"); } }

        public bool SendForConversion { get { return _SendForConversion; } set { _SendForConversion = value; this.OnPropertyChanged("SendForConversion"); } }
        #endregion



        public bool ExpandView
        {
            get { return (bool)GetValue(ExpandViewProperty); }
            set { SetValue(ExpandViewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ExpandView.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExpandViewProperty =
            DependencyProperty.Register("ExpandView", typeof(bool), typeof(UCVideoRecorderFeed), new PropertyMetadata(false));



        #region Delegates
        public delegate void OnRecordingCompleted(string fileName, int countDown);
        public event OnRecordingCompleted RecordingCompleteHandler;
        #endregion
        #region Command
        private ICommand _RecordCommand;
        public ICommand RecordCommand
        {
            get
            {
                if (_RecordCommand == null)
                {
                    _RecordCommand = new RelayCommand((param) => OnRecordClick(param));
                }
                return _RecordCommand;
            }
        }
        private void OnRecordClick(object param)
        {
            try
            {
                
                    int buttonType = (int)param;
                    switch (buttonType)
                    {
                        case RecorderConstants.BUTTON_START:
                            //_FileName = RingIDSettings.TEMP_CAM_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + DefaultSettings.LOGIN_USER_ID + "_" + ModelUtility.CurrentTimeMillisLocal() + "." + "mp4";
                            _FileName = RingIDSettings.TEMP_CAM_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + "Video_" + ModelUtility.CurrentTimeMillis() + "." + "mp4";
                            WebCamPreview.OnRecordingCountDownChange += (v) =>
                            {
                                TimerValue = TimeSpan.FromSeconds(v);
                            };
                            WebCamPreview.OnRecordingCompleted += () =>
                            {
                                if ((int)TimerValue.TotalSeconds > 1)
                                {
                                    State = RecorderConstants.STATE_COMPLETED;
                                    SendToMediaPlayer();
                                }
                                else
                                {
                                    State = RecorderConstants.STATE_READY;
                                    TimerValue = TimeSpan.FromSeconds(0);
                                }
                            };
                            int errorCode = WebCamPreview.StartRecording(_FileName);
                            if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND)
                            {
                                HasWebCamError = true;
                                ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                            }
                            else if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING)
                            {
                                HasWebCamError = true;
                                ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
                            }
                            else
                            {
                                State = RecorderConstants.STATE_RUNNING;
                            }
                            break;
                        case RecorderConstants.BUTTON_STOP:
                            WebCamPreview.RequestForStop();
                            break;
                        case RecorderConstants.BUTTON_RESTART:
                            _FileName = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + System.IO.Path.DirectorySeparatorChar + DefaultSettings.LOGIN_RING_ID + "_" + ModelUtility.CurrentTimeMillis() + "." + "mp4";
                            errorCode = WebCamPreview.StartRecording(_FileName);
                            if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND)
                            {
                                HasWebCamError = true;
                                ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                            }
                            else if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING)
                            {
                                HasWebCamError = true;
                                ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
                            }
                            else
                            {
                                State = RecorderConstants.STATE_RUNNING;
                            }
                            break;
                        case RecorderConstants.BUTTON_SEND:
                            SendForConversion = true;
                            if (RecordingCompleteHandler != null)
                                RecordingCompleteHandler(_FileName, (int)TimerValue.TotalSeconds);
                            break;
                    } 
                
            }
            catch (Exception ex)
            {

                log.Error("UCFeedVideoRecorder.OnRecordClick() => " + ex.Message + "\n" + ex.StackTrace);

                //Application.Current.Dispatcher.Invoke(() => {
                //    ((this.Parent as UCVideoRecorderPreviewPanel).Parent as UCNewStatus).ForceCloseVideoRecorderPreviewPanel();
                //    CustomMessageBox.ShowInformation("Recording Unavailable!");
                //});
                
            }
        }

        private void SendToMediaPlayer()
        {
            Application.Current.Dispatcher.Invoke(()=>
                {
                    SendForConversion = true;
                    if (RecordingCompleteHandler != null)
                        RecordingCompleteHandler(_FileName, (int)TimerValue.TotalSeconds);
                }
                );
        }
        #endregion

        public UCVideoRecorderFeed()
        {
            this.InitializeComponent();
            Instance = this;
            this.State = RecorderConstants.STATE_READY;
            this.DataContext = this;
        }

        #region Interface Implementations
        public void Dispose()
        {
            WebCamPreview.Dispose();
            Instance = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        } 
        #endregion
        #region Animation Event
        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            try
            {
               // int errorCode = WebCamPreview.InitWebcamDevices(_MaxVideoTime, (bool)chkIncludeVoice.IsChecked);
                int errorCode = WebCamPreview.InitWebcamDevices(_MaxVideoTime);
                if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND)
                {
                    HasWebCamError = true;
                    ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                }
                else if (errorCode == RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING)
                {
                    HasWebCamError = true;
                    ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
                }
                ((AnimationClock)sender).Completed -= DoubleAnimation_Completed;
                recorderContainer.Triggers.Clear();
                //System.Diagnostics.Debug.WriteLine("Error Message => " + ErrorMessage);
            }
            catch (Exception ex)
            {

                log.Error("Error: DoubleAnimationCompleted()=> " + ex.Message + "\n" + ex.StackTrace);

            }
        } 
        #endregion
    }
}

﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.BindingModels;
using View.UI.Comment;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Images;
using View.Utility.ImageViewer;

namespace View.UI.ImageViewer
{
    /// <summary>
    /// Interaction logic for UCImageCommentsNewComment.xaml
    /// </summary>
    public partial class UCImageCommentsNewComment : UserControl, INotifyPropertyChanged
    {
        #region "Fields"

        #endregion "Fields"

        #region "Ctors"
        public UCImageCommentsNewComment(ImageViewerDataModel DataModel, Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            this.DataModel = DataModel;
            this.motherPanel = motherGrid;
        }
        #endregion "Ctors"

        #region "Properties"
        private UCLikeList LikeListView { get; set; }
        private UCNewCommentPanel NewCommentPanel { get; set; }
        private UCUserProfile UserProfile { get; set; }
        private ImageViewerDataModel dataModel;
        public ImageViewerDataModel DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private Grid _motherPanel;
        public Grid motherPanel
        {
            get
            {
                return _motherPanel;
            }
            set
            {
                if (_motherPanel == value)
                {
                    return;
                }
                _motherPanel = value;
                OnPropertyChanged("motherPanel");
            }
        }
        #endregion "Properties"

        #region "Commands and Command Methods"

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            AddNewCommentUserControl();
            ChangeSingleModel(DataModel.SingleImageModel);

            UserProfile = new UCUserProfile(DataModel.SingleImageModel.UserShortInfoModel, DataModel.SingleImageModel.Caption, DataModel.SingleImageModel.Time, motherPanel);
            _ProfileImageAndCaptionPanel.Children.Add(UserProfile);
        }

        private ICommand numberofLikeCommand;
        public ICommand NumberofLikeCommand
        {
            get
            {
                if (numberofLikeCommand == null) numberofLikeCommand = new RelayCommand(param => OnNumberofLikeCommand());
                return numberofLikeCommand;
            }
        }
        private void OnNumberofLikeCommand()
        {
            if (DataModel.SingleImageModel.LikeCommentShare != null)
                ShowLikePopup(DataModel.SingleImageModel.ImageId, DataModel.SingleImageModel.NewsFeedId, DataModel.SingleImageModel.LikeCommentShare.NumberOfLikes);
        }

        private ICommand showMoreComments;
        public ICommand ShowMoreComments
        {
            get
            {
                if (showMoreComments == null) showMoreComments = new RelayCommand(param => OnShowMoreComments());
                return showMoreComments;
            }
        }
        private void OnShowMoreComments()
        {
            commentsRequests(DataModel.SingleImageModel);
        }

        private ICommand numberofCommentCommand;
        public ICommand NumberofCommentCommand
        {
            get
            {
                if (numberofCommentCommand == null) numberofCommentCommand = new RelayCommand(param => OnNumberofCommentCommand());
                return numberofCommentCommand;
            }
        }
        private void OnNumberofCommentCommand()
        {
        }

        #endregion

        #region "Utility Methods"
        public void ShowLikePopup(Guid imageID, Guid nfID, long numerOflike)
        {
            if (LikeListView != null)
                motherPanel.Children.Remove(LikeListView);
            LikeListView = new UCLikeList(motherPanel);
            LikeListView.ImageId = imageID;
            LikeListView.NewsfeedId = nfID;
            LikeListView.NumberOfLike = numerOflike;
            LikeListView.Show();
            LikeListView.SendLikeListRequest();
            LikeListView.OnRemovedUserControl += () =>
            {
                LikeListView = null;
            };
        }

        public bool HideLikePopup()
        {
            if (LikeListView != null)
            {
                LikeListView.LikeList.Clear();
                LikeListView.Hide();
                return true;
            }
            LikeListView = null;
            return false;
        }

        public void AddNewCommentUserControl()
        {
            try
            {
                if (NewCommentPanel != null)
                {
                    _NewCommentPanel.Child = null;
                }
                NewCommentPanel = new UCNewCommentPanel();
                NewCommentPanel.motherPanel = motherPanel;
                NewCommentPanel.SetNeedToFocus(false);
                _NewCommentPanel.Child = NewCommentPanel;
            }
            catch (Exception)
            {
            }
        }
        public void ChangeSingleModel(ImageModel model)
        {
            if (NewCommentPanel != null)
            {
                NewCommentPanel.newsfeedId = model.NewsFeedId;
                NewCommentPanel.imageId = model.ImageId;
                NewCommentPanel.contentId = Guid.Empty;
            }
            if (UserProfile != null)
            {
                UserProfile.ChangeUserProfile(model.UserShortInfoModel, model.Caption, model.Time);
            }
        }

        private void commentsRequests(ImageModel model)
        {
            if (!DataModel.IsLoadingComments && model.LikeCommentShare != null && DataModel.CommentList.Count < model.LikeCommentShare.NumberOfComments && model.ImageId != Guid.Empty)
            {
                DataModel.IsLoadingComments = true;
                ThreadStart mediaMoreComments = delegate
                {
                    long time = 0;
                    int scroll = 0;
                    Guid pvtUUID = Guid.Empty;
                    if (DataModel.CommentList.Count > 0)
                    {
                        time = DataModel.CommentList.ElementAt(DataModel.CommentList.Count - 1).Time;
                        pvtUUID = DataModel.CommentList.ElementAt(DataModel.CommentList.Count - 1).CommentId;
                    }
                    if (time > 0)
                    {
                        scroll = StatusConstants.SCROLL_OLDER;
                    }
                    bool commentsReq = new ImageRequests().CommentReqeust(model.NewsFeedId, model.ImageId, pvtUUID, time, scroll);
                    DataModel.IsLoadingComments = false;
                };
                new Thread(mediaMoreComments).Start();
            }
        }
        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

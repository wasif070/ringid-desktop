﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Images;
using View.Utility.ImageViewer;
using View.ViewModel;

namespace View.UI.ImageViewer
{
    /// <summary>
    ///For Data as static resources
    /// https://social.msdn.microsoft.com/Forums/vstudio/en-US/59701ab4-ff79-4db5-ad36-7c5a10255159/how-do-you-reference-a-path-stored-as-a-resource?forum=wpf
    /// http://nerdplusart.com/simple-path-data-resources-that-i-add-to-every-wpf-project/
    /// </summary>
    public partial class UCImageViewInMain : PopUpBaseControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCImageViewInMain).Name);
        ImageRequests imageRequests;
        bool fechingMoreImagesFromserver = false;
        UCNumberOfLikeComment likeCommentNumbers = null;
        public int NavigateFrom;
        bool firstImage = true;
        #endregion " Fields"

        #region "Constructors"
        public UCImageViewInMain()
        {
            DataModel = new ImageViewerDataModel();
            InitializeComponent();
            this.DataContext = this;
            imageRequests = new ImageRequests();
            DataModel.CommentList = RingIDViewModel.Instance.MediaComments;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, "#FF000000", 1.0);
        }
        #endregion "Constructors"

        #region "Properties"

        private ImageViewerDataModel dataModel;
        public ImageViewerDataModel DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        private ObservableCollection<ImageModel> imageModels = new ObservableCollection<ImageModel>();
        public ObservableCollection<ImageModel> ImageModels
        {
            get { return imageModels; }
            set { imageModels = value; this.OnPropertyChanged("ImageModels"); }
        }

        private Stretch _FullImageStretch = Stretch.Uniform;
        public Stretch FullImageStretch
        {
            get { return _FullImageStretch; }
            set { _FullImageStretch = value; OnPropertyChanged("FullImageStretch"); }
        }
        private UCImageCommentsNewComment ImageCommentsNewComment { get; set; }

        private double imageToViewWidth;

        //public double ImageToViewWidth
        //{
        //    get { return imageToViewWidth; }
        //    set { imageToViewWidth = value; OnPropertyChanged("ImageToViewWidth"); }
        //}

        #endregion "Properties"

        #region "Commands"

        private ICommand loadedMainControl;
        public ICommand LoadedMainControl
        {
            get
            {
                if (loadedMainControl == null) loadedMainControl = new RelayCommand(param => OnLoadedMainControl());
                return loadedMainControl;
            }
        }
        private void OnLoadedMainControl()
        {
            GrabKeyboardFocus();
            loadAnImage(DataModel.SingleImageModel);
            if (DataModel.NeedToShowLikeComment)
            {
                likeCommentNumbers = new UCNumberOfLikeComment(DataModel, _MotherPanel);
                _BottomContainer.Children.Add(likeCommentNumbers);
                ImageCommentsNewComment = new UCImageCommentsNewComment(DataModel, _MotherPanel);
                _RightPanel.Children.Add(ImageCommentsNewComment);
            }
        }

        private ICommand sizeChangedCommand;
        public ICommand SizeChangedCommand
        {
            get
            {
                if (sizeChangedCommand == null) sizeChangedCommand = new RelayCommand(param => OnSizeChangedCommand());
                return sizeChangedCommand;
            }
        }
        private void OnSizeChangedCommand()
        {
            changeSize();
        }

        private ICommand exit;
        public ICommand Exit
        {
            get
            {
                if (exit == null) exit = new RelayCommand(param => OnExit());
                return exit;
            }
        }
        public void OnExit()
        {
            Hide();
        }

        private ICommand previousCommand;
        public ICommand PreviousCommand
        {
            get
            {
                if (previousCommand == null) previousCommand = new RelayCommand(param => OnPreviousCommand());
                return previousCommand;
            }
        }
        private void OnPreviousCommand()
        {
            if (DataModel.PreviousButtonEnable)
                if (DataModel.SelectedIndex < DataModel.TotalImages
                && DataModel.SelectedIndex > 0)
                {
                    DataModel.SelectedIndex = DataModel.SelectedIndex - 1;//--;
                    ImageModel model = ImageModels.ElementAt(DataModel.SelectedIndex);
                    ChangeSelectedModel(DataModel.SelectedIndex, DataModel.TotalImages, model);
                    loadAnImage(model);
                }
        }

        private ICommand nextCommand;
        public ICommand NextCommand
        {
            get
            {
                if (nextCommand == null) nextCommand = new RelayCommand(param => OnNextCommand());
                return nextCommand;
            }
        }
        private void OnNextCommand()
        {
            if (DataModel.NextButtonEnable && DataModel.SingleImageModel.ImageId != Guid.Empty)
                if (DataModel.SelectedIndex < DataModel.TotalImages - 1)
                {
                    if (ImageModels.Count > DataModel.SelectedIndex + 1)
                    {
                        DataModel.SelectedIndex = DataModel.SelectedIndex + 1;
                        ImageModel model = ImageModels.ElementAt(DataModel.SelectedIndex);
                        ChangeSelectedModel(DataModel.SelectedIndex, DataModel.TotalImages, model);
                        loadAnImage(model);
                    }
                }
        }

        private ICommand saveAsCommand;
        public ICommand SaveAsCommand
        {
            get
            {
                if (saveAsCommand == null) saveAsCommand = new RelayCommand(param => OnSaveAsCommand());
                return saveAsCommand;
            }
        }
        private void OnSaveAsCommand()
        {
            SaveOrDownloadImage();
        }

        private ICommand mouseEnterCommand;
        public ICommand MouseEnterCommand
        {
            get
            {
                if (mouseEnterCommand == null) mouseEnterCommand = new RelayCommand(param => OnMouseEnterCommand());
                return mouseEnterCommand;
            }
        }
        private void OnMouseEnterCommand()
        {
            _BottomContainer.Visibility = Visibility.Visible;
            _NextButtonPanel.Visibility = Visibility.Visible;
            _PreviousButtonPanel.Visibility = Visibility.Visible;
        }

        private ICommand mouseLeaveCommand;
        public ICommand MouseLeaveCommand
        {
            get
            {
                if (mouseLeaveCommand == null) mouseLeaveCommand = new RelayCommand(param => OnMouseLeaveCommand());
                return mouseLeaveCommand;
            }
        }
        private void OnMouseLeaveCommand()
        {
            if (!_BottomContainer.IsMouseOver) _BottomContainer.Visibility = Visibility.Hidden;
            if (!_NextButtonPanel.IsMouseOver) _NextButtonPanel.Visibility = Visibility.Hidden;
            if (!_PreviousButtonPanel.IsMouseOver) _PreviousButtonPanel.Visibility = Visibility.Hidden;
        }

        #endregion "Commands"

        #region "Utility Methods"

        public void SetParentGrid(System.Windows.Controls.Grid grid = null)
        {
            if (grid == null) grid = UCGuiRingID.Instance.MotherPanel;
            SetParent(grid);
            firstImage = true;
        }

        public void ChangeSelectedModel(int index, int totalImages, ImageModel singleImageModel1)
        {
            DataModel.TotalImages = totalImages;
            DataModel.SelectedIndex = index;
            DataModel.SingleImageModel = singleImageModel1;
        }

        public void GrabKeyboardFocus()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private void loadAnImage(ImageModel model)
        {
            ViewConstants.ImageID = model.ImageId;
            if (DataModel.NeedToShowLikeComment) imageDetailsReqeust(model);
            changeSize();
            showDefaultImage(model);
        }

        private void ImageDrectorySet(int imageType)
        {
            if (imageType == SettingsConstants.TYPE_COVER_IMAGE) DataModel.ImageDirectory = RingIDSettings.TEMP_COVER_IMAGE_FOLDER;
            else if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE) DataModel.ImageDirectory = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER;
            else if (imageType == SettingsConstants.TYPE_IMAGE_IN_COMMENT) DataModel.ImageDirectory = RingIDSettings.TEMP_COMMENT_IMAGE_FOLDER;
            else DataModel.ImageDirectory = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER;
        }

        private void showDefaultImage(ImageModel model)
        {
            ImageDrectorySet(model.ImageType);
            if (model.ImageUrl != null)
            {
                int directoryImageType = 0;
                string imageDirectory = DataModel.ImageDirectory;
                if (File.Exists(imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_FULL)))
                    directoryImageType = ImageUtility.IMG_FULL;
                else if (File.Exists(imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_600)))
                    directoryImageType = ImageUtility.IMG_600;
                else if (File.Exists(imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_300)))
                    directoryImageType = ImageUtility.IMG_300;
                else if (File.Exists(imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_THUMB)))
                    directoryImageType = ImageUtility.IMG_THUMB;
                if (directoryImageType > 0)
                {
                    ChangeSelectedImageSource(model.ImageWidth, model.ImageHeight, imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(model.ImageUrl, directoryImageType));
                    //FullImageStretch = Stretch.Fill;
                    if (directoryImageType != ImageUtility.IMG_FULL)
                        DownloadFullImage(model.ImageUrl, imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_FULL));
                }
                else
                {
                    DataModel.ImageSourceToView = null;
                    DownloadFullImage(model.ImageUrl, imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(model.ImageUrl, ImageUtility.IMG_FULL));
                }
            }
            else DataModel.ImageSourceToView = null;
        }

        private void ChangeSelectedImageSource(double width, double height, string newSource)
        {
            DataModel.ImageSourceToView = newSource;
        }

        private void changeSize()
        {
            try
            {
                double imageWidth = dataModel.SingleImageModel.ImageWidth;
                double imageHeight = dataModel.SingleImageModel.ImageHeight;

                double mxWidth = _MainBorder.ActualWidth - (_RightPanel.ActualWidth + 40);//becase margin is 20,30,20,20
                double mxHeight = _MainBorder.ActualHeight - 50; //becase margin is 20,30,20,20
                if (mxWidth > 0 && mxHeight > 0)
                {
                    double actualWidth = _ImageContainerBorder.ActualWidth;
                    double actualHeight = _ImageContainerBorder.ActualHeight;
                    if (imageWidth >= mxWidth || imageHeight >= mxHeight)
                    {
                        FullImageStretch = Stretch.Uniform;
                        if (!firstImage)
                        {
                            if (imageWidth >= mxWidth) _ImageToView.Width = mxWidth;
                            else
                            {
                                if (imageWidth > actualWidth) _ImageToView.Width = imageWidth;
                                else _ImageToView.Width = actualWidth;
                            }
                        }
                    }
                    else
                    {
                        FullImageStretch = Stretch.None;
                        if (!firstImage)
                        {
                            if (imageWidth > actualWidth) _ImageToView.Width = imageWidth;
                            else _ImageToView.Width = actualWidth;
                        }
                    }
                    firstImage = false;
                }
                else FullImageStretch = Stretch.Uniform;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            finally { }
        }

        public void SaveOrDownloadImage()
        {
            try
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
                dialog.FileName = HelperMethods.ConvertUrlToName(DataModel.SingleImageModel.ImageUrl, ImageUtility.IMG_FULL);
                if (dialog.ShowDialog() == true)
                {
                    var encoder = new JpegBitmapEncoder(); // Or PngBitmapEncoder, or whichever encoder you want
                    encoder.Frames.Add(BitmapFrame.Create((BitmapSource)_ImageToView.Source));
                    using (var stream = dialog.OpenFile()) encoder.Save(stream);
                }
                Keyboard.Focus(this);
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        #endregion "Utility Methods"

        #region "Server Communications"
        /// <summary>
        /// This thread will
        /// Download full image from server and save into local directory 
        /// and will change Current DataModel
        /// </summary>
        /// <param name="serverImageUrl"></param>
        /// <param name="fullImageUrlForLoacalDirectory"></param>
        private void DownloadFullImage(string serverImageUrl, string fullImageUrlForLoacalDirectory)
        {
            ThreadStart downloadFull = delegate
           {
               string url = ServerAndPortSettings.GetImageServerResourceURL + serverImageUrl;
               bool flag = ImageUtility.DownloadRemoteImageFile(url, fullImageUrlForLoacalDirectory);
               if (flag && serverImageUrl.Equals(DataModel.SingleImageModel.ImageUrl))
               {
                   //Uri uriAddress = new Uri(fullImageUrlForLoacalDirectory, UriKind.RelativeOrAbsolute);
                   //try
                   //{
                   //    BitmapImage bi = new BitmapImage();
                   //    bi.BeginInit();
                   //    bi.CacheOption = BitmapCacheOption.OnLoad;
                   //    bi.UriSource = uriAddress;
                   //    bi.EndInit();
                   //    bi.Freeze();
                   //    if (DataModel.SingleImageModel.ImageWidth != (int)bi.Width || DataModel.SingleImageModel.ImageHeight != (int)bi.Height)
                   //    {
                   //        if (bi.Width != DataModel.SingleImageModel.ImageWidth) DataModel.SingleImageModel.ImageWidth = (int)bi.Width;
                   //        if (bi.Height != DataModel.SingleImageModel.ImageHeight) DataModel.SingleImageModel.ImageHeight = (int)bi.Height;
                   //        Application.Current.Dispatcher.Invoke((Action)delegate { changeSize(); });
                   //    }
                   //}
                   //catch (Exception ex)
                   //{
                   //    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                   //}
                   ChangeSelectedImageSource(DataModel.SingleImageModel.ImageWidth, DataModel.SingleImageModel.ImageHeight, fullImageUrlForLoacalDirectory);
               }
           };
            if (ServerAndPortSettings.GetImageServerResourceURL != null && DefaultSettings.IsInternetAvailable) new Thread(downloadFull).Start();
        }
        /// <summary>
        /// Thread from requesting details of an image
        /// </summary>
        /// <param name="imageid"></param>
        private void imageDetailsReqeust(ImageModel model)
        {
            DataModel.CommentList.Clear();

            if (DataModel.ShowCommentsPanel)
            {
                DataModel.IsStartedCommentUILoading = true;
                changeSize();
            }

            if (ImageCommentsNewComment != null) ImageCommentsNewComment.ChangeSingleModel(model);

            ThreadStart thdStart = delegate
            {
                bool imageDetailsReqeust = imageRequests.SingleImageDetailsRequest(model.ImageId);
                if (DataModel.ImageSourceToView == null && !string.IsNullOrEmpty(model.ImageUrl)) showDefaultImage(model);
                if (ImageCommentsNewComment != null) ImageCommentsNewComment.ChangeSingleModel(model);
                if (DataModel.SingleImageModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID && likeCommentNumbers != null)
                {
                    likeCommentNumbers.IsEnableProfileCoverPicMenuItem = true;
                    likeCommentNumbers.IsEnableReportMediaItem = false;
                }
                DataModel.IsStartedCommentUILoading = false;
                if (DataModel.ShowCommentsPanel && model.LikeCommentShare != null && model.LikeCommentShare.NumberOfComments > 0)
                {
                    DataModel.IsLoadingComments = true;
                    imageRequests.CommentReqeust(model.NewsFeedId, model.ImageId, Guid.Empty, 0, 0);
                    DataModel.IsLoadingComments = false;
                }

                if (imageDetailsReqeust && !fechingMoreImagesFromserver && ImageModels.Count < DataModel.TotalImages
                    && DataModel.SelectedIndex >= ImageModels.Count - 2)
                {
                    fechingMoreImagesFromserver = true;
                    JArray jarray = imageRequests.FetchMoreImagesFromServer(ImageModels.Count, DataModel.TotalImages, model, NavigateFrom);
                    if (jarray != null)
                    {
                        try
                        {
                            foreach (JObject singleObj in jarray)
                            {
                                FeedModel fm = null;
                                FeedDataContainer.Instance.FeedModels.TryGetValue(model.NewsFeedId, out fm);
                                if (fm != null)
                                    fm.LoadData(singleObj);
                                foreach (ImageModel img in fm.ImageList)
                                {
                                    if (!ImageModels.Any(p => p.ImageId == img.ImageId))
                                    {
                                        ImageModel modelIndictionary = ImageDataContainer.Instance.GetFromImageModelDictionaryNotNullable(img.ImageId);
                                        modelIndictionary.LoadData(singleObj);
                                        ImageModels.InvokeAdd(modelIndictionary);
                                    }
                                }
                            }
                        }
                        finally { }
                    }
                    fechingMoreImagesFromserver = false;
                }
            };
            if (DefaultSettings.IsInternetAvailable && model.ImageId != Guid.Empty) new Thread(thdStart).Start();
        }
        #endregion""

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

﻿using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.UI.Profile.MyProfile;
using View.Utility;
using View.Utility.Images;
using View.Utility.ImageViewer;
using View.Utility.WPFMessageBox;
using System.Windows;
using System;
using Newtonsoft.Json.Linq;
using View.Utility.DataContainer;
using View.Utility.Feed;

namespace View.UI.ImageViewer
{
    /// <summary>
    /// Interaction logic for UCNumberOfLikeComment.xaml
    /// </summary>
    public partial class UCNumberOfLikeComment : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        Grid motherPanel = null;
        #endregion "Fields"

        #region "Ctors"
        public UCNumberOfLikeComment(ImageViewerDataModel DataModel, Grid motherGrid)
        {
            InitializeComponent();
            this.DataModel = DataModel;
            this.DataContext = this;
            this.motherPanel = motherGrid;
            if (DataModel.SingleImageModel.UserShortInfoModel.UserTableID == DefaultSettings.userProfile.UserTableID && ViewConstants.NavigateFrom == StatusConstants.NAVIGATE_FROM_PROFILE) IsEnableDeleteThisImage = true;
            else IsEnableDeleteThisImage = false;
        }
        #endregion "Ctors"

        #region "Properties"
        private UCLikeList LikeListView { get; set; }

        private ImageViewerDataModel dataModel;
        public ImageViewerDataModel DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private bool isEnableProfileCoverPicMenuItem = false;
        public bool IsEnableProfileCoverPicMenuItem
        {
            get { return isEnableProfileCoverPicMenuItem; }
            set { isEnableProfileCoverPicMenuItem = value; OnPropertyChanged("IsEnableProfileCoverPicMenuItem"); }
        }

        private bool isEnableReportMediaItem = true;
        public bool IsEnableReportMediaItem
        {
            get { return isEnableReportMediaItem; }
            set { isEnableReportMediaItem = value; OnPropertyChanged("IsEnableReportMediaItem"); }
        }

        private bool isEnableDeleteThisImage = false;
        public bool IsEnableDeleteThisImage
        {
            get { return isEnableDeleteThisImage; }
            set { isEnableDeleteThisImage = value; OnPropertyChanged("IsEnableDeleteThisImage"); }
        }

        #endregion "Properties"

        #region "Commands and Command Methods"

        private ICommand likeCommand;
        public ICommand LikeCommand
        {
            get
            {
                if (likeCommand == null) likeCommand = new RelayCommand(param => OnLikeCommand());
                return likeCommand;
            }
        }
        private void OnLikeCommand()
        {
            ImageModel imageModel = DataModel.SingleImageModel;
            if (imageModel.LikeCommentShare != null)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    bool ImageIdToContentId = false;
                    int liked = (imageModel.LikeCommentShare.ILike == 0) ? 1 : 0;
                    Guid NewsfeedId = imageModel.NewsFeedId;
                    Guid ImageId = imageModel.ImageId;
                    Guid AlbumId = imageModel.AlbumId;
                    FeedModel feedModel = null;
                    if (NewsfeedId != Guid.Empty)
                        FeedDataContainer.Instance.FeedModels.TryGetValue(NewsfeedId, out feedModel);
                    bool fromFeed = (ViewConstants.NavigateFrom == StatusConstants.NAVIGATE_FROM_FEED) ? true : false;
                    if (imageModel.LikeCommentShare.ILike == 0)
                    {
                        ShowLikeAnimation();
                        imageModel.LikeCommentShare.ILike = 1;
                        imageModel.LikeCommentShare.NumberOfLikes = imageModel.LikeCommentShare.NumberOfLikes + 1;
                        //if (feedModel != null && feedModel.SingleImageFeedModel != null && feedModel.LikeCommentShare != null)
                        //{
                        //    feedModel.LikeCommentShare.ILike = 1;
                        //    feedModel.LikeCommentShare.NumberOfLikes = feedModel.LikeCommentShare.NumberOfLikes + 1;
                        //    feedModel.OnPropertyChanged("CurrentInstance");
                        //}
                    }
                    else
                    {
                        imageModel.LikeCommentShare.ILike = 0;
                        if (imageModel.LikeCommentShare.NumberOfLikes > 0)
                            imageModel.LikeCommentShare.NumberOfLikes = imageModel.LikeCommentShare.NumberOfLikes - 1;
                        if (feedModel != null && feedModel.SingleImageFeedModel != null && (feedModel.BookPostType != 3 || feedModel.BookPostType != 6 || feedModel.BookPostType != 9)
                            && feedModel.ActivityType == AppConstants.NEWSFEED_LIKED)
                        {
                            feedModel.Activist = null;
                            feedModel.ActivityType = 0;
                        }
                        //if (feedModel != null && feedModel.SingleImageFeedModel != null && feedModel.LikeCommentShare != null)
                        //{
                        //    feedModel.LikeCommentShare.ILike = 0;
                        //    if (feedModel.LikeCommentShare.NumberOfLikes > 0) feedModel.LikeCommentShare.NumberOfLikes = feedModel.LikeCommentShare.NumberOfLikes - 1;
                        //    feedModel.OnPropertyChanged("CurrentInstance");
                        //}
                    }
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_LIKE_STATUS;
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_IMAGE;
                    pakToSend[JsonKeys.Liked] = liked;
                    pakToSend[JsonKeys.ContentId] = ImageId;
                    ImageIdToContentId = true;
                    pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                    if (fromFeed)
                    {
                        pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                        pakToSend[JsonKeys.NewsfeedId] = NewsfeedId;
                        pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                        pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;
                    }
                    else
                    {
                        pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                        pakToSend[JsonKeys.AlbumId] = AlbumId;
                    }
                    new ThreadLikeUnlikeAny().StartThread(pakToSend, ImageIdToContentId);
                }
                finally { }
            }
        }

        private ICommand numberofLikeCommand;
        public ICommand NumberofLikeCommand
        {
            get
            {
                if (numberofLikeCommand == null) numberofLikeCommand = new RelayCommand(param => OnNumberofLikeCommand());
                return numberofLikeCommand;
            }
        }
        private void OnNumberofLikeCommand()
        {
            ShowLikePopup(DataModel.SingleImageModel.ImageId, DataModel.SingleImageModel.NewsFeedId, DataModel.SingleImageModel.LikeCommentShare.NumberOfLikes);
        }

        private ICommand doCommentCommand;
        public ICommand DoCommentCommand
        {
            get
            {
                if (doCommentCommand == null) doCommentCommand = new RelayCommand(param => OnDoCommentCommand());
                return doCommentCommand;
            }
        }
        private void OnDoCommentCommand()
        {
            OnShowCommentsCommand();
        }

        private ICommand numberofCommentCommand;
        public ICommand NumberofCommentCommand
        {
            get
            {
                if (numberofCommentCommand == null) numberofCommentCommand = new RelayCommand(param => OnNumberofCommentCommand());
                return numberofCommentCommand;
            }
        }
        private void OnNumberofCommentCommand()
        {
            OnShowCommentsCommand();
        }

        private ICommand makeProfile;
        public ICommand MakeProfile
        {
            get
            {
                if (makeProfile == null) makeProfile = new RelayCommand(param => OnMakeProfile());
                return makeProfile;
            }
        }
        private void OnMakeProfile()
        {
            ImageModel objImageModel = DataModel.SingleImageModel;
            if (objImageModel != null)
            {
                if (objImageModel.ImageWidth < RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH || objImageModel.ImageHeight < RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH)
                    UIHelperMethods.ShowWarning("Profile photo's minimum resulotion: " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH + " x " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH, "Profile photo");
                else
                {
                    UCProfilePictureUpload profilePictureUpload = new UCProfilePictureUpload(motherPanel);
                    profilePictureUpload.Show();
                    profilePictureUpload.ShowHandlerDialog(objImageModel);
                    profilePictureUpload.OnRemovedUserControl += () =>
                    {
                        profilePictureUpload = null;
                        if (ImageViewInMain != null) ImageViewInMain.GrabKeyboardFocus();
                    };
                }
            }
        }

        private ICommand makeCover;
        public ICommand MakeCover
        {
            get
            {
                if (makeCover == null) makeCover = new RelayCommand(param => OnMakeCover());
                return makeCover;
            }
        }
        private void OnMakeCover()
        {
            ImageModel objImageModel = DataModel.SingleImageModel;
            if (objImageModel != null && !objImageModel.NoImageFound)
            {
                if (objImageModel.ImageWidth < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH || objImageModel.ImageHeight < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT)
                    UIHelperMethods.ShowWarning("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT, "Cover photo");
                else
                {
                    HideMotherPanel();
                    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyProfile);
                    UCMiddlePanelSwitcher.InstanceOfMyProfile.ShowCoverPicChangePanel(objImageModel);
                }
            }
        }

        private ICommand sendViaChat;
        public ICommand SendViaChat
        {
            get
            {
                if (sendViaChat == null) sendViaChat = new RelayCommand(param => OnSendViaChat());
                return sendViaChat;
            }
        }
        private void OnSendViaChat()
        {
            View.UI.PopUp.MediaSendViaChat.UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new View.UI.PopUp.MediaSendViaChat.UCMediaShareToFriendPopup(motherPanel);
            ucMediaShareToFriendPopup.Show();
            ucMediaShareToFriendPopup.SetSingleImage(DataModel.SingleImageModel);
            ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
            {
                if (ImageViewInMain != null) HideMotherPanel();
            };
        }

        private ICommand report;
        public ICommand Report
        {
            get
            {
                if (report == null) report = new RelayCommand(param => OnReport());
                return report;
            }
        }
        private void OnReport()
        {
            MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(DataModel.SingleImageModel.ImageId, StatusConstants.SPAM_IMAGE, DataModel.SingleImageModel.UserShortInfoModel.UserIdentity);
        }

        private ICommand deleteThisImage;
        public ICommand DeletePhotoCommand
        {
            get
            {
                if (deleteThisImage == null) deleteThisImage = new RelayCommand(param => OnDeleteThisImage());
                return deleteThisImage;
            }
        }
        private void OnDeleteThisImage()
        {
            //Console.WriteLine("***********OnDeleteThisImage******************");
            // if(DataModel.TotalImages==0)
            if (UCImageContentView.Instance.OnDeletePhoto(DataModel.SingleImageModel))
            {
                HideMotherPanel();
            }
        }

        private ICommand showCommentsCommand;
        public ICommand ShowCommentsCommand
        {
            get
            {
                if (showCommentsCommand == null) showCommentsCommand = new RelayCommand(param => OnShowCommentsCommand());
                return showCommentsCommand;
            }
        }
        private void OnShowCommentsCommand()
        {
            if (!DataModel.ShowCommentsPanel)
            {
                DataModel.ShowCommentsPanel = true;
                commentsRequests(DataModel.SingleImageModel);
            }
            else DataModel.ShowCommentsPanel = false;
        }

        #endregion

        #region "Utility Methods"

        private void commentsRequests(ImageModel imageModel)
        {
            if (imageModel.LikeCommentShare != null && !DataModel.IsLoadingComments && DataModel.CommentList.Count < imageModel.LikeCommentShare.NumberOfComments)
            {
                DataModel.IsLoadingComments = true;
                ThreadStart mediaMoreComments = delegate
                {
                    long time = 0;
                    int scroll = 0;
                    if (DataModel.CommentList.Count > 0) time = DataModel.CommentList.ElementAt(DataModel.CommentList.Count - 1).Time;
                    if (time > 0)
                    {
                        scroll = StatusConstants.SCROLL_OLDER;//for previous comments
                    }
                    bool commentsReq = new ImageRequests().CommentReqeust(imageModel.NewsFeedId, imageModel.ImageId, Guid.Empty, time, scroll);// scl =1 for previous scroll
                    DataModel.IsLoadingComments = false;
                };
                new Thread(mediaMoreComments).Start();
            }
        }

        public void ShowLikePopup(Guid imageId, Guid newsfeedId, long numberOfLike)
        {
            if (LikeListView != null)
                motherPanel.Children.Remove(LikeListView);
            LikeListView = new UCLikeList(motherPanel);
            LikeListView.ImageId = imageId;
            LikeListView.NewsfeedId = newsfeedId;
            LikeListView.NumberOfLike = numberOfLike;
            LikeListView.Show();
            LikeListView.SendLikeListRequest();
            LikeListView.OnRemovedUserControl += () =>
            {
                LikeListView = null;
                if (ImageViewInMain != null)
                {
                    ImageViewInMain.GrabKeyboardFocus();
                }
            };
        }

        public bool HideLikePopup()
        {
            if (LikeListView != null)
            {
                LikeListView.LikeList.Clear();
                LikeListView.Hide();
                return true;
            }
            LikeListView = null;
            return false;
        }

        private void HideMotherPanel()
        {
            if (ImageViewInMain != null) ImageViewInMain.Hide();
        }

        private UCImageViewInMain ImageViewInMain
        {
            get
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                if (obj != null && obj is UCImageViewInMain) return (UCImageViewInMain)obj;
                return null;
            }
        }

        private void likeHide()
        {
            Thread.Sleep(2000);
            Application.Current.Dispatcher.Invoke(() =>
            {
                DataModel.LikeAnimationloader = null;
                likeAnimationGrid.Visibility = Visibility.Collapsed;
            });
        }

        private void ShowLikeAnimation()
        {
            if (DataModel.LikeAnimationloader == null)
            {
                DataModel.LikeAnimationloader = ImageLocation.LOADER_LIKE_ANIMATION;
                likeAnimationGrid.Visibility = Visibility.Visible;
                Thread thread = new Thread(likeHide);
                thread.Start();
            }
            else
            {
                DataModel.LikeAnimationloader = null;
                likeAnimationGrid.Visibility = Visibility.Collapsed;
            }
        }
        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

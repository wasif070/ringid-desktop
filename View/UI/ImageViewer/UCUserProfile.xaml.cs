﻿using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.BindingModels;
using View.Utility;
using View.ViewModel;

namespace View.UI.ImageViewer
{
    /// <summary>
    /// Interaction logic for UCUserProfile.xaml
    /// </summary>
    public partial class UCUserProfile : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        Grid motherPanel = null;
        #endregion "Fields"

        #region "Ctors"
        public UCUserProfile(BaseUserProfileModel shortInfoModel, string caption1, long time, Grid motherGrid)
        {
            InitializeComponent();
            this.UserShortInfoModel = shortInfoModel;
            this.MediaCaption = caption1;
            this.Time = time;
            this.DataContext = this;
            this.motherPanel = motherGrid;
        }

        #endregion "Ctors"

        #region "Properties"

        private BaseUserProfileModel _UserShortInfoModel;
        public BaseUserProfileModel UserShortInfoModel
        {
            get { return _UserShortInfoModel; }
            set
            {
                if (value == _UserShortInfoModel)
                    return;
                _UserShortInfoModel = value;
                this.OnPropertyChanged("UserShortInfoModel");
            }
        }

        private long time;
        public long Time
        {
            get { return time; }
            set
            {
                if (value == time) return;
                time = value; OnPropertyChanged("Time");
            }
        }

        private string mediaCaption;
        public string MediaCaption
        {
            get { return mediaCaption; }
            set
            {
                if (value == mediaCaption) return;
                mediaCaption = value; OnPropertyChanged("MediaCaption");
            }
        }
        #endregion "Properties"

        #region "Commands and Command Methods"

        private ICommand gotoProfileCommand;
        public ICommand GotoProfileCommand
        {
            get
            {
                if (gotoProfileCommand == null) gotoProfileCommand = new RelayCommand(param => OnGotoProfileCommand());
                return gotoProfileCommand;
            }
        }
        private void OnGotoProfileCommand()
        {
            if (UserShortInfoModel.UserIdentity > 0)
            {
                if (!HelperMethods.IsRingIDOfficialAccount(UserShortInfoModel.UserIdentity))
                {
                    if (UserShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                        RingIDViewModel.Instance.OnMyProfileClicked(UserShortInfoModel.UserIdentity);
                    else
                        RingIDViewModel.Instance.OnFriendProfileButtonClicked(UserShortInfoModel.UserTableID);
                    HideMotherPanel();
                }
            }
        }

        private void HideMotherPanel()
        {
            var obj = ((Border)(motherPanel.Parent)).Parent;
            if (obj != null && obj is UCImageViewInMain)
            {
                UCImageViewInMain imageViewInMain1 = (UCImageViewInMain)obj;
                imageViewInMain1.Hide();
                imageViewInMain1 = null;
            }
        }
        #endregion

        #region "Utility Methods"
        public void ChangeUserProfile(BaseUserProfileModel shortInfoModel, string caption1, long time)
        {
            this.UserShortInfoModel = shortInfoModel;
            this.MediaCaption = caption1;
            this.Time = time;
        }
        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

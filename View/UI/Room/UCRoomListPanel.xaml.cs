﻿using imsdkwrapper;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.Chat;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Room
{
    /// <summary>
    /// Interaction logic for UCRoomListPanel.xaml
    /// </summary>
    /// 

    public partial class UCRoomListPanel : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCRoomListPanel).Name);

        public delegate void OnSelectRoom(RoomModel model, bool fromSearch);
        public event PropertyChangedEventHandler PropertyChanged;
        public UCRoomListAllPanel _RoomListAllPanel = null;
        public UCRoomListSearchPanel _RoomListSearchPanel = null;

        private ICommand _SearchCommand;
        private ICommand _BackCommand;
        private ICommand _OnCountryListCommand;
        private ICommand _OnCategoryListCommand;
        private DispatcherTimer _TextChangeTimer = null;

        private UCRoomPreview _RoomPreviewPanel = null;
        private bool _IsRoomPreviewMode = false;

        #region Constructor

        public UCRoomListPanel()
        {
            InitializeComponent();
            this.Loaded += UCRoomListPanel_Loaded;
            this.Unloaded += UCRoomListPanel_Unloaded;
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private void UCRoomListPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.RoomButtonSelection = false;
            ScrlViewer.ScrollChanged -= ScrlViewer_ScrollChanged;
        }

        private void UCRoomListPanel_Loaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.RoomButtonSelection = true;
            ScrlViewer.ScrollChanged += ScrlViewer_ScrollChanged;
        }

        private void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (this.pnlBody.Child == null)
                return;

            if (this.pnlBody.Child is UCRoomListAllPanel)
            {
                ((UCRoomListAllPanel)this.pnlBody.Child).ScrlViewer_ScrollChanged(sender, e);
            }
            else if (this.pnlBody.Child is UCRoomListSearchPanel)
            {
                ((UCRoomListSearchPanel)this.pnlBody.Child).ScrlViewer_ScrollChanged(sender, e);
            }
        }

        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs args)
        {
            try
            {
                _RoomListSearchPanel.ClearSearchRoomList(true);

                if (this._TextChangeTimer == null)
                {
                    this._TextChangeTimer = new DispatcherTimer();
                    this._TextChangeTimer.Interval = TimeSpan.FromMilliseconds(800);
                    this._TextChangeTimer.Tick += (o, e) =>
                    {
                        RingIDViewModel.Instance.RoomListSearchQuery.PrevSearchText = RingIDViewModel.Instance.RoomListSearchQuery.SearchText;
                        RingIDViewModel.Instance.RoomListSearchQuery.SearchText = SearchTermTextBox.Text;
                        if (_RoomListSearchPanel.Parent != null)
                        {
                            _RoomListSearchPanel.InitSearchRoomList();
                        }
                        this._TextChangeTimer.Stop();
                    };
                }

                this._TextChangeTimer.Stop();
                this._TextChangeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: SearchTermTextBox_TextChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void LoadInitData()
        {
            try
            {
                OnBackCommandClicked(null);
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadInitData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnSearchCommandClicked(object param)
        {
            try
            {
                this.pnlRoomListTop.Visibility = Visibility.Collapsed;
                this.pnlRoomSearchTop.Visibility = Visibility.Visible;

                if (this._RoomListSearchPanel == null)
                {
                    this._RoomListSearchPanel = new UCRoomListSearchPanel(ShowRoomPreview);
                }
                this.SearchTermTextBox.Focus();
                this.pnlBody.Child = this._RoomListSearchPanel;
                //this._RoomListSearchPanel.InitSearchRoomList();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSearchCommandClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnBackCommandClicked(object param)
        {
            try
            {
                //if (this._RoomListSearchPanel != null)
                //{
                //    this._RoomListSearchPanel.ClearSearchRoomList();
                //}

                this.pnlRoomListTop.Visibility = Visibility.Visible;
                this.pnlRoomSearchTop.Visibility = Visibility.Collapsed;
                if (this._RoomListAllPanel == null)
                {
                    this._RoomListAllPanel = new UCRoomListAllPanel(ShowRoomPreview);
                }
                this.pnlBody.Child = this._RoomListAllPanel;
                this._RoomListAllPanel.BuildRoomList();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnBackCommandClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnCategoryListClick(object param)
        {
            try
            {
                if (UCRoomSearchCategoryPopup.Instance == null)
                {
                    UCRoomSearchCategoryPopup.Instance = new UCRoomSearchCategoryPopup();
                    categoryPanel.Children.Add(UCRoomSearchCategoryPopup.Instance);
                }
                UCRoomSearchCategoryPopup.Instance.Show(categoryPanel, () =>
                {
                    if (!RingIDViewModel.Instance.RoomListSearchQuery.CategoryName.Equals(UCRoomSearchCategoryPopup.Instance.CatName))
                    {
                        //SearchedDiscovery.Clear();
                        RingIDViewModel.Instance.RoomListSearchQuery.CategoryName = UCRoomSearchCategoryPopup.Instance.CatName;
                        _RoomListSearchPanel.InitSearchRoomList();
                        //RequestToGetDsicoverCelebrities();
                    }
                    return 0;
                });
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCategoryListClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnCountryListClicked(object param)
        {
            try
            {
                if (UCCelebrityDiscoverCountryPopup.Instance == null)
                {
                    UCCelebrityDiscoverCountryPopup.Instance = new UCCelebrityDiscoverCountryPopup();
                    cntryPanel.Children.Add(UCCelebrityDiscoverCountryPopup.Instance);
                }
                UCCelebrityDiscoverCountryPopup.Instance.Show(cntryPanel, () =>
                {
                    if (!RingIDViewModel.Instance.RoomListSearchQuery.CountryName.Equals(UCCelebrityDiscoverCountryPopup.Instance.CntryName))
                    {
                        //SearchedDiscovery.Clear();
                        RingIDViewModel.Instance.RoomListSearchQuery.CountryName = UCCelebrityDiscoverCountryPopup.Instance.CntryName;
                        _RoomListSearchPanel.InitSearchRoomList();
                        //RequestToGetDsicoverCelebrities();
                    }
                    return 0;
                });
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCountryListClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowRoomPreview(RoomModel roomModel, bool fromSearch)
        {
            try
            {
                _RoomPreviewPanel = new UCRoomPreview();
                _RoomPreviewPanel.Show(roomModel, fromSearch, () =>
                {
                    HideRoomPreview();
                    return 0;
                });
                pnlRoomPreviewContainer.Child = _RoomPreviewPanel;
                IsRoomPreviewMode = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowRoomPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideRoomPreview(bool isClosed = false)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (_RoomPreviewPanel != null)
                    {
                        _RoomPreviewPanel.Dispose();
                        _RoomPreviewPanel = null;
                    }
                    pnlRoomPreviewContainer.Child = null;
                    IsRoomPreviewMode = false;
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: HideRoomPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand SearchCommand
        {
            get
            {
                if (_SearchCommand == null)
                {
                    _SearchCommand = new RelayCommand(param => OnSearchCommandClicked(param));
                }
                return _SearchCommand;
            }
        }

        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommandClicked(param));
                }
                return _BackCommand;
            }
        }

        public ICommand OnCategoryListCommand
        {
            get
            {
                if (_OnCategoryListCommand == null)
                {
                    _OnCategoryListCommand = new RelayCommand(param => OnCategoryListClick(param));
                }
                return _OnCategoryListCommand;
            }
        }

        public ICommand OnCountryListCommand
        {
            get
            {
                if (_OnCountryListCommand == null)
                {
                    _OnCountryListCommand = new RelayCommand(param => OnCountryListClicked(param));
                }
                return _OnCountryListCommand;
            }
        }

        public bool IsRoomPreviewMode
        {
            get
            {
                return _IsRoomPreviewMode;
            }
            set
            {
                if (_IsRoomPreviewMode == value)
                    return;
                _IsRoomPreviewMode = value;
                this.OnPropertyChanged("IsRoomPreviewMode");
            }
        }

        #endregion Property


    }

    //public partial class UCRoomListPanel : UserControl, INotifyPropertyChanged
    //{

    //    private static readonly ILog log = LogManager.GetLogger(typeof(UCRoomListPanel).Name);

    //    public event PropertyChangedEventHandler PropertyChanged;
    //    private bool _IsLoading = false;
    //    public bool _IsNoMoreRoom = false;
    //    private double _SingleItemsMaxWidth = 315;
    //    private double _SingleItemsMinHeight = 300;
    //    private DispatcherTimer _ResizeTimer = null;
    //    private DispatcherTimer _WaitTimer = null;
    //    private bool _IsInitialize = false;
    //    public int _StartIndex = 0;

    //    #region Constructor

    //    public UCRoomListPanel()
    //    {
    //        InitializeComponent();
    //        this.Loaded += UCRoomListPanel_Loaded;
    //        this.Unloaded += UCRoomListPanel_Unloaded;
    //        this.DataContext = this;
    //    }

    //    #endregion Constructor

    //    #region Event Handler

    //    private void UCRoomListPanel_Unloaded(object sender, RoutedEventArgs e)
    //    {
    //        RingIDViewModel.Instance.RoomButtonSelection = false;
    //        feedScrlViewer.ScrollChanged -= feedScrlViewer_ScrollChanged;
    //    }

    //    private void UCRoomListPanel_Loaded(object sender, RoutedEventArgs e)
    //    {
    //        RingIDViewModel.Instance.RoomButtonSelection = true;
    //        if (itemControls.ItemsSource == null)
    //        {
    //            itemControls.ItemsSource = RingIDViewModel.Instance.RoomList;
    //        }
    //        feedScrlViewer.ScrollChanged += feedScrlViewer_ScrollChanged;
    //    }

    //    private void feedScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
    //    {
    //        var scrollViewer = (ScrollViewer)sender;
    //        if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
    //        {
    //            scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
    //        }
    //        else if (e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight && BOTTOM_LOADING == false)
    //        {
    //            new Thread(() =>
    //            {
    //                BottomLoadRoomList();
    //            }).Start();
    //        }
    //    }

    //    private void MainGrid_SizeChanged(object sender, SizeChangedEventArgs args)
    //    {
    //        try
    //        {
    //            if (_IsLoading == false)
    //            {
    //                if (_ResizeTimer == null)
    //                {
    //                    _ResizeTimer = new DispatcherTimer();
    //                    _ResizeTimer.Interval += TimeSpan.FromSeconds(1);
    //                    _ResizeTimer.Tick += (o, e) =>
    //                    {
    //                        double gridWidth = MainGrid.ActualWidth > 1260 ? 1260 : MainGrid.ActualWidth;
    //                        double gridHeight = MainGrid.ActualHeight;

    //                        int itemsNeedToLoad = (int)((gridWidth * gridHeight) - (_StartIndex * _SingleItemsMaxWidth * _SingleItemsMinHeight)) / (int)(_SingleItemsMaxWidth * _SingleItemsMinHeight);
    //                        if (itemsNeedToLoad > 0)
    //                        {
    //                            ChatService.GetRoomListWithHistory(_StartIndex, _StartIndex + itemsNeedToLoad, null);
    //                        }
    //                        _ResizeTimer.Stop();
    //                    };
    //                }

    //                _ResizeTimer.Stop();
    //                if (_WaitTimer != null && !_WaitTimer.IsEnabled)
    //                {
    //                    _ResizeTimer.Start();
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("Error: MainGrid_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
    //        }
    //    }

    //    #endregion Event Handler

    //    #region Utility Methods

    //    public void LoadInitData()
    //    {
    //        try
    //        {
    //            if (_WaitTimer == null)
    //            {
    //                _WaitTimer = new DispatcherTimer();
    //                _WaitTimer.Interval = TimeSpan.FromMilliseconds(1000);
    //                _WaitTimer.Tick += (s, e) =>
    //                {
    //                    double gridWidth = MainGrid.ActualWidth > 1260 ? 1260 : MainGrid.ActualWidth;
    //                    double gridHeight = MainGrid.ActualHeight;
    //                    int itemsNeedToLoad = (int)((gridWidth * gridHeight) - (_StartIndex * _SingleItemsMaxWidth * _SingleItemsMinHeight)) / (int)(_SingleItemsMaxWidth * _SingleItemsMinHeight);

    //                    if (!(_IsInitialize && RingIDViewModel.Instance.RoomList.Count > 0) && _IsLoading == false)
    //                    {
    //                        _IsInitialize = true;
    //                        _IsLoading = true;
    //                        IsNeedToShowAnnimation = true;

    //                        ChatService.GetRoomListWithHistory(0, itemsNeedToLoad, (args) =>
    //                        {
    //                            if (args.Status)
    //                            {
    //                                int elipseTime = 0;
    //                                while (RingIDViewModel.Instance.RoomList.Count <= 0 && elipseTime < 2000)
    //                                {
    //                                    Thread.Sleep(200);
    //                                    elipseTime += 200;
    //                                }
    //                                IsNeedToShowAnnimation = false;
    //                                _IsLoading = false;
    //                            }
    //                            else
    //                            {

    //                                IsNeedToShowAnnimation = false;
    //                                _IsLoading = false;
    //                            }
    //                        });
    //                    }

    //                    _WaitTimer.Stop();
    //                };
    //                _WaitTimer.Stop();
    //                _WaitTimer.Start();
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("Error: LoadInitData() ==> " + ex.Message + "\n" + ex.StackTrace);
    //        }
    //    }

    //    private void BottomLoadRoomList()
    //    {
    //        if (_IsNoMoreRoom == false)
    //        {
    //            BOTTOM_LOADING = true;
    //            ShowLoading();
    //            ChatService.GetRoomListWithHistory(_StartIndex, 4, (args) =>
    //            {
    //                if (args.Status)
    //                {
    //                    HideShowMoreLoading();
    //                    BOTTOM_LOADING = false;
    //                }
    //                else
    //                {
    //                    BOTTOM_LOADING = false;
    //                    HideShowMoreLoading();
    //                }
    //            });
    //        }
    //    }

    //    public void ShowLoading()
    //    {
    //        try
    //        {
    //            Application.Current.Dispatcher.BeginInvoke(delegate
    //            {
    //                this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
    //            });
    //        }
    //        catch (System.Exception ex)
    //        {
    //            log4net.LogManager.GetLogger(typeof(UCRoomListPanel).Name).Error(ex.StackTrace + ex.Message);
    //        }
    //    }

    //    public void HideShowMoreLoading()
    //    {
    //        Application.Current.Dispatcher.BeginInvoke(delegate
    //        {
    //            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
    //        });
    //    }

    //    private void OnPropertyChanged(string propertyName)
    //    {
    //        PropertyChangedEventHandler handler = PropertyChanged;
    //        if (handler != null)
    //        {
    //            handler(this, new PropertyChangedEventArgs(propertyName));
    //        }
    //    }

    //    #endregion Utility Methods


    //    #region Property

    //    private bool _BOTTOM_LOADING = false;
    //    public bool BOTTOM_LOADING
    //    {
    //        get { return _BOTTOM_LOADING; }
    //        set
    //        {
    //            _BOTTOM_LOADING = value;
    //            OnPropertyChanged("BOTTOM_LOADING");
    //        }
    //    }

    //    private bool _IsNeedToShowAnnimation = false;
    //    public bool IsNeedToShowAnnimation
    //    {
    //        get { return _IsNeedToShowAnnimation; }
    //        set
    //        {
    //            _IsNeedToShowAnnimation = value;
    //            OnPropertyChanged("IsNeedToShowAnnimation");
    //        }
    //    }

    //    #endregion Property

    //}
}

﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.ViewModel;

namespace View.UI.Room
{
    /// <summary>
    /// Interaction logic for UCRoomListSearchPanel.xaml
    /// </summary>
    public partial class UCRoomListSearchPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCRoomListSearchPanel).Name);

         public event PropertyChangedEventHandler PropertyChanged;
        private bool _IsInitLoading = false;
        private bool _IsMoreLoading = false;

        private ICommand _OnSelectCommand;
        private UCRoomListPanel.OnSelectRoom _OnSelectRoom;

        private DispatcherTimer _InitTimer = null;
        private DispatcherTimer _ResizeTimer = null;
        private DispatcherTimer _ScorllTimer = null;
        private StackPanel _StackPanel = null;

        #region Constructor

        public UCRoomListSearchPanel(UCRoomListPanel.OnSelectRoom onSelectRoom)
        {
            InitializeComponent();
            this._OnSelectRoom = onSelectRoom;
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this._StackPanel = (StackPanel)sender;
            this._StackPanel.SizeChanged -= this.RoomListContainer_SizeChanged;
            this._StackPanel.SizeChanged += this.RoomListContainer_SizeChanged;
        }

        private void RoomListContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                this.ChangeMessageStatusOnResize();
            }
            catch (Exception ex)
            {
                log.Error("Error: RoomListContainer_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            try
            {
                if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 45 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                }
                else if (IsInitLoading == false && IsMoreLoading == false && RingIDViewModel.Instance.RoomListSearchQuery.IsActive && RingIDViewModel.Instance.RoomListSearchQuery.NoMoreRoom == false && e.VerticalChange > 0 && scrollViewer.VerticalOffset > (scrollViewer.ScrollableHeight - 100))
                {
                    IsMoreLoading = true;
                    this.MoreGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
                    int startIndex = RingIDViewModel.Instance.RoomListSearchQuery.StartIndex;

                    ChatService.SearchRoomList(RingIDViewModel.Instance.RoomListSearchQuery.SearchText, RingIDViewModel.Instance.RoomListSearchQuery.CountryName, RingIDViewModel.Instance.RoomListSearchQuery.CategoryName, startIndex, 5, (args) =>
                    {
                        if (args.Status)
                        {
                            for (int i = 0; startIndex == RingIDViewModel.Instance.RoomListSearchQuery.StartIndex && i < 2000; i += 100)
                            {
                                Thread.Sleep(100);
                            }
                            IsMoreLoading = false;
                            Application.Current.Dispatcher.BeginInvoke(() => { if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                        }
                        else
                        {
                            IsMoreLoading = false;
                            Application.Current.Dispatcher.BeginInvoke(() => { if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                        }
                    });
                }

                this.ChangeMessageStatusOnScroll();
            }
            catch (Exception ex)
            {
                log.Error("Error: ScrlViewer_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void ClearSearchRoomList(bool partial = false)
        {
            try
            {
                RingIDViewModel.Instance.RoomListSearchQuery.IsActive = false;
                RingIDViewModel.Instance.RoomListSearchQuery.StartIndex = 0;
                RingIDViewModel.Instance.RoomListSearchQuery.NoMoreRoom = false;

                if (this._InitTimer != null && !this._InitTimer.IsEnabled)
                {
                    this._InitTimer.Stop();
                }
                if (this._ResizeTimer != null && !this._ResizeTimer.IsEnabled)
                {
                    this._ResizeTimer.Stop();
                }
                if (this._ScorllTimer != null && !this._ScorllTimer.IsEnabled)
                {
                    this._ScorllTimer.Stop();
                }

                this.IsInitLoading = false;
                this.IsMoreLoading = false;
                if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate();
                if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate();

                if (!partial)
                {
                    this.itcRoomList.ClearValue(ItemsControl.ItemsSourceProperty);
                    this.itcRoomList.ItemsSource = null;
                    RingIDViewModel.Instance.RoomSearchList.Clear();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ClearSearchRoomList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void InitSearchRoomList()
        {
            try
            {
                if (String.IsNullOrEmpty(RingIDViewModel.Instance.RoomListSearchQuery.SearchText) && String.IsNullOrEmpty(RingIDViewModel.Instance.RoomListSearchQuery.CategoryName) && String.IsNullOrEmpty(RingIDViewModel.Instance.RoomListSearchQuery.CountryName))
                {
                    ClearSearchRoomList();
                    return;
                }
                else
                {
                    if (!String.IsNullOrEmpty(RingIDViewModel.Instance.RoomListSearchQuery.PrevSearchText) && !RingIDViewModel.Instance.RoomListSearchQuery.SearchText.ToLower().Equals(RingIDViewModel.Instance.RoomListSearchQuery.PrevSearchText.ToLower()) && RingIDViewModel.Instance.RoomListSearchQuery.PrevSearchText.ToLower().StartsWith(RingIDViewModel.Instance.RoomListSearchQuery.SearchText.ToLower()))
                    {
                        ClearSearchRoomList(true);
                    }
                    else
                    {
                        ClearSearchRoomList();

                        object resource = Application.Current.FindResource("RingIDSettings");
                        itcRoomList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                        {
                            Path = new PropertyPath("MainViewModel.RoomSearchList"),
                            Source = resource
                        });
                    }
                }

                IsInitLoading = true;
                this.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

                int startIndex = RingIDViewModel.Instance.RoomListSearchQuery.StartIndex;
                RingIDViewModel.Instance.RoomListSearchQuery.IsActive = true;

                ChatService.SearchRoomList(RingIDViewModel.Instance.RoomListSearchQuery.SearchText, RingIDViewModel.Instance.RoomListSearchQuery.CountryName, RingIDViewModel.Instance.RoomListSearchQuery.CategoryName, 0, 10, (args) =>
                {
                    if (args.Status)
                    {
                        for (int i = 0; startIndex == RingIDViewModel.Instance.RoomListSearchQuery.StartIndex && i < 2000; i += 100)
                        {
                            Thread.Sleep(100);
                        }
                        IsInitLoading = false;
                        Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                    }
                    else
                    {
                        IsInitLoading = false;
                        Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                    }
                });

                //if (_InitTimer == null)
                //{
                //    _InitTimer = new DispatcherTimer();
                //    _InitTimer.Interval = TimeSpan.FromSeconds(0.2);
                //    _InitTimer.Tick += (s, e) =>
                //    {
                //        if(String.IsNullOrWhiteSpace(RingIDViewModel.Instance.RoomListSearchQuery.SearchText) && String.IsNullOrWhiteSpace(RingIDViewModel.Instance.RoomListSearchQuery.CategoryName) && String.IsNullOrWhiteSpace(RingIDViewModel.Instance.RoomListSearchQuery.CountryName))
                //        {
                //            _InitTimer.Stop();
                //            return;
                //        }

                //        IsInitLoading = true;
                //        this.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

                //        object resource = Application.Current.FindResource("RingIDSettings");
                //        itcRoomList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                //        {
                //            Path = new PropertyPath("MainViewModel.RoomSearchList"),
                //            Source = resource
                //        });
                //        int startIndex = RingIDViewModel.Instance.RoomListSearchQuery.StartIndex;
                //        RingIDViewModel.Instance.RoomListSearchQuery.IsActive = true;

                //        ChatService.SearchRoomList(RingIDViewModel.Instance.RoomListSearchQuery.SearchText, RingIDViewModel.Instance.RoomListSearchQuery.CountryName, RingIDViewModel.Instance.RoomListSearchQuery.CategoryName, 0, 10, (args) =>
                //        {
                //            if (args.Status)
                //            {
                //                for (int i = 0; startIndex == RingIDViewModel.Instance.RoomListSearchQuery.StartIndex && i < 2000; i += 100)
                //                {
                //                    Thread.Sleep(100);
                //                }
                //                IsInitLoading = false;
                //                Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                //            }
                //            else
                //            {
                //                IsInitLoading = false;
                //                Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                //            }
                //        });
                //        _InitTimer.Stop();
                //    };
                //}
                //_InitTimer.Stop();
                //_InitTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: InitSearchRoomList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ChangeMessageStatusOnResize()
        {
            try
            {
                if (this._ResizeTimer == null)
                {
                    this._ResizeTimer = new DispatcherTimer();
                    this._ResizeTimer.Interval = TimeSpan.FromMilliseconds(350);
                    this._ResizeTimer.Tick += (o, e) =>
                    {
                        UCRoomListPanel panel = (UCRoomListPanel)HelperMethods.FindVisualParent<UserControl>(this);
                        if (UIHelperMethods.IsViewActivateAndVisible(panel))
                        {
                            ChatHelpers.ChangeRoomViewPortOpenedProperty(panel.ScrlViewer, this, this.itcRoomList, this._StackPanel);
                        }
                        this._ResizeTimer.Stop();
                    };
                }

                this._ResizeTimer.Stop();
                if (this._ScorllTimer != null && this._ScorllTimer.IsEnabled)
                {
                    this._ScorllTimer.Stop();
                }
                if (this._InitTimer != null && !this._InitTimer.IsEnabled)
                {
                    this._ResizeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ChangeMessageStatusOnScroll()
        {
            try
            {
                if (this._ScorllTimer == null)
                {
                    this._ScorllTimer = new DispatcherTimer();
                    this._ScorllTimer.Interval = TimeSpan.FromMilliseconds(30);
                    this._ScorllTimer.Tick += (o, e) =>
                    {
                        UCRoomListPanel panel = (UCRoomListPanel)HelperMethods.FindVisualParent<UserControl>(this);
                        if (UIHelperMethods.IsViewActivateAndVisible(panel))
                        {
                            ChatHelpers.ChangeRoomViewPortOpenedProperty(panel.ScrlViewer, this, this.itcRoomList, this._StackPanel);
                        }
                        this._ScorllTimer.Stop();
                    };
                }

                this._ScorllTimer.Stop();
                if (this._InitTimer != null && !this._InitTimer.IsEnabled)
                {
                    this._ScorllTimer.Start();
                }
                if (this._ResizeTimer != null && !this._ResizeTimer.IsEnabled)
                {
                    this._ScorllTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand OnSelectCommand
        {
            get
            {
                if (_OnSelectCommand == null)
                {
                    _OnSelectCommand = new RelayCommand(param => this._OnSelectRoom((RoomModel)param, true));
                }
                return _OnSelectCommand;
            }
        }

        public bool IsInitLoading
        {
            get
            {
                return _IsInitLoading;
            }
            set
            {
                if (_IsInitLoading == value)
                    return;
                _IsInitLoading = value;
                this.OnPropertyChanged("IsInitLoading");
            }
        }

        public bool IsMoreLoading
        {
            get
            {
                return _IsMoreLoading;
            }
            set
            {
                if (_IsMoreLoading == value)
                    return;
                _IsMoreLoading = value;
                this.OnPropertyChanged("IsMoreLoading");
            }
        }

        #endregion Property
    }
}

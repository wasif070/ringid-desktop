﻿using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Room
{
    /// <summary>
    /// Interaction logic for UCRoomChatLikeMemberListPanel.xaml
    /// </summary>
    public partial class UCRoomChatLikeMemberListPanel : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCRoomChatLikeMemberListPanel).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private bool _Visible = false;
        private Func<int> _OnClose = null;
        private MessageModel _MessageModel = null;
        private bool _IsInitLoading = false;
        private bool _IsMoreLoading = false;
        public string _PacketID = null;

        private DispatcherTimer _ViewTimer = null;
        private ScrollViewer _Scroll = null;

        private ICommand _OnCloseCommand;
        private ICommand _OnEmptyCommand;

        public UCRoomChatLikeMemberListPanel()
        {
            InitializeComponent();
        }

        #region Event Handler

        private void UCRoomChatLikeMemberListPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue == false)
                {
                    if (_Scroll != null)
                    {
                        _Scroll.ScrollChanged -= _Scroll_ScrollChanged;
                        _Scroll = null;
                    }

                    IsInitLoading = false;
                    if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate();
                    IsMoreLoading = false;
                    if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate();

                    _ViewTimer.Stop();
                    _ViewTimer = null;

                    itcLikeMemberList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                    itcLikeMemberList.ItemsSource = null;

                    this.MessageModel.LikeMemberList.Clear();

                    this._PacketID = null;
                    this.DataContext = null;
                    this.MessageModel = null;
                    this.Focusable = false;
                    this._OnClose = null;

                    this.IsVisibleChanged -= UCRoomChatLikeMemberListPanel_IsVisibleChanged;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCRoomChatLikeMemberListPanel_IsVisibleChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            _Scroll = (ScrollViewer)sender;
            _Scroll.ScrollChanged += _Scroll_ScrollChanged;
        }

        private void _Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            if (IsInitLoading == false && IsMoreLoading == false && _Scroll != null && MessageModel.ViewModel.LikeMemberNotFound == false && e.VerticalChange > 0 && e.VerticalOffset >= (_Scroll.ScrollableHeight - 106.0))
            {
                if (!MessageModel.ViewModel.LikeMemberNotFound)
                {
                    this.MoreGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
                    IsMoreLoading = true;

                    int prevMemberCount = this.MessageModel.LikeMemberList.Count;
                    long lastLikerId = prevMemberCount > 0 ? this.MessageModel.LikeMemberList[prevMemberCount - 1].MemberID : 0;

                    ChatService.GetPublicChatLikeMemberList(this.MessageModel.RoomID, this.MessageModel.PacketID, lastLikerId, 10, (args) =>
                    {
                        if (!String.IsNullOrWhiteSpace(_PacketID) && _PacketID.Equals(args.PacketID))
                        {
                            if (args.Status)
                            {
                                for (int idx = 0; idx < 5000 && prevMemberCount == this.MessageModel.LikeMemberList.Count; idx += 250)
                                {
                                    Thread.Sleep(250);
                                }
                                IsMoreLoading = false;
                                Application.Current.Dispatcher.BeginInvoke(() => { if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                            }
                            else
                            {
                                IsMoreLoading = false;
                                Application.Current.Dispatcher.BeginInvoke(() => { if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                            }
                        }
                    });
                }
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void Show(MessageModel model, Func<int> onClose = null)
        {
            this.IsVisibleChanged += UCRoomChatLikeMemberListPanel_IsVisibleChanged;

            this._PacketID = model.PacketID;
            this.IsInitLoading = true;
            this.IsMoreLoading = false;
            this.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
            this._OnClose = onClose;

            this.MessageModel = model;
            this.DataContext = this;
            this.BuildSeenMemberList();

            this.Visible = true;
            this.Focusable = true;
            this.Focus();
        }

        public void Hide(object param = null)
        {
            if (_OnClose != null)
            {
                _OnClose();
            }
            else
            {
                this.Visible = false;
            }
        }

        private void BuildSeenMemberList()
        {
            try
            {
                if (_ViewTimer == null)
                {
                    _ViewTimer = new DispatcherTimer();
                    _ViewTimer.Interval = TimeSpan.FromSeconds(0.5);
                    _ViewTimer.Tick += (s, e) =>
                    {
                        itcLikeMemberList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                        {
                            Path = new PropertyPath("MessageModel.LikeMemberList"),
                        });

                        int prevMemberCount = this.MessageModel.LikeMemberList.Count;
                        long lastLikerId = prevMemberCount > 0 ? this.MessageModel.LikeMemberList[prevMemberCount - 1].MemberID : 0;

                        ChatService.GetPublicChatLikeMemberList(this.MessageModel.RoomID, this.MessageModel.PacketID, lastLikerId, 15, (args) => 
                        {
                            if (!String.IsNullOrWhiteSpace(_PacketID) && _PacketID.Equals(args.PacketID))
                            {
                                if (args.Status)
                                {
                                    for (int idx = 0; idx < 5000 && prevMemberCount == this.MessageModel.LikeMemberList.Count; idx += 250)
                                    {
                                        Thread.Sleep(250);
                                    }
                                    IsInitLoading = false;
                                    Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                                }
                                else
                                {
                                    IsInitLoading = false;
                                    Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                                }
                            }
                        });
                        _ViewTimer.Stop();
                    };
                }
                _ViewTimer.Stop();
                _ViewTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: BuildSeenMemberList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.Visible = false;
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Property

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (value == _Visible)
                    return;

                _Visible = value;
                this.OnPropertyChanged("Visible");
            }
        }

        public bool IsInitLoading
        {
            get { return _IsInitLoading; }
            set
            {
                if (value == _IsInitLoading)
                    return;

                _IsInitLoading = value;
                this.OnPropertyChanged("IsInitLoading");
            }
        }

        public bool IsMoreLoading
        {
            get { return _IsMoreLoading; }
            set
            {
                if (value == _IsMoreLoading)
                    return;

                _IsMoreLoading = value;
                this.OnPropertyChanged("IsMoreLoading");
            }
        }

        public MessageModel MessageModel
        {
            get { return _MessageModel; }
            set
            {
                if (value == _MessageModel)
                    return;

                _MessageModel = value;
                this.OnPropertyChanged("MessageModel");
            }
        }

        public ICommand OnCloseCommand
        {
            get
            {
                if (_OnCloseCommand == null)
                {
                    _OnCloseCommand = new RelayCommand((param) => Hide(param));
                }
                return _OnCloseCommand;
            }
        }

        public ICommand OnEmptyCommand
        {
            get
            {
                if (_OnEmptyCommand == null)
                {
                    _OnEmptyCommand = new RelayCommand(param => { });
                }
                return _OnEmptyCommand;
            }
        }

        #endregion Property


    }
}

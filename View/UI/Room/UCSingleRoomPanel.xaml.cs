﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.UI.Chat;
using View.Utility;

namespace View.UI.Room
{
    /// <summary>
    /// Interaction logic for UCSingleRoomPanel.xaml
    /// </summary>
    public partial class UCSingleRoomPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleRoomPanel).Name);

        private RoomModel _RoomModel;
        private bool _IsImageVisible;
        private ICommand _OnClickCommand;

        public UCSingleRoomPanel()
        {
            InitializeComponent();
        }

        #region Property

        public RoomModel RoomModel
        {
            get { return _RoomModel; }
            set
            {
                if (value == _RoomModel)
                    return;

                _RoomModel = value;
                this.OnPropertyChanged("RoomModel");
            }
        }

        public bool IsImageVisible
        {
            get { return _IsImageVisible; }
            set
            {
                if (value == _IsImageVisible)
                    return;

                _IsImageVisible = value;
                this.OnPropertyChanged("IsImageVisible");
            }
        }

        #endregion Property

        #region Event Handler

        #endregion Event Handler

        #region Utility Methods

        public void InitDataContext(RoomModel model)
        {
            try
            {
                Dispose();

                this.RoomModel = model;
                if (this.DataContext == null)
                {
                    this.DataContext = this;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: InitDataContext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void InitView()
        {
            try
            {
                Binding itemsSourceBinding = new Binding
                {
                    Path = new PropertyPath("RoomModel.MessageList"),
                };
                itcMessageList.SetBinding(ItemsControl.ItemsSourceProperty, itemsSourceBinding);
                IsImageVisible = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: InitView() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            try
            {
                this.itcMessageList.ClearValue(ItemsControl.ItemsSourceProperty);
                this.itcMessageList.ItemsSource = null;
                this.DataContext = null;
                this.RoomModel = null;
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

    }
}

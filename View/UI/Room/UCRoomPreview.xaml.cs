﻿using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Room
{
    /// <summary>
    /// Interaction logic for UCRoomPreview.xaml
    /// </summary>
    public partial class UCRoomPreview : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCRoomPreview).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private List<RoomModel> _RoomList = null;
        private bool _Visible = false;
        private int _CurrIndex = 0;
        private int _SequenceNumber = 0;
        private int _TotalCount = 0;
        private Visibility _LeftArrowVisible = Visibility.Collapsed;
        private Visibility _RightArrowVisible = Visibility.Collapsed;
        private ICommand _OnCloseCommand;
        private ICommand _OnLeftCommand;
        private ICommand _OnRightCommand;
        private Func<int> _OnClose = null;
        public DispatcherTimer _ViewTimer = null;
        public string _RoomID;
        

        private UCSingleRoomPanel _SingleRoomPanel = null;

        public UCRoomPreview()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Event Handler

        private void UCRoomPreview_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue == false)
                {
                    this.Visible = false;

                    this._CurrIndex = 0;
                    this.SequenceNumber = 0;
                    this.TotalCount = 0;

                    this.Focusable = false;
                    this._OnClose = null;

                    this.IsVisibleChanged -= UCRoomPreview_IsVisibleChanged;
                    this.PreviewKeyDown -= UCRoomPreview_PreviewKeyDown;

                    if (pnlRoomPreview.Child != null)
                    {
                        ((UCSingleRoomPanel)pnlRoomPreview.Child).Dispose();
                        pnlRoomPreview.Child = null;
                    }
                    this._RoomList.Clear();
                    this._RoomList = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCRoomPreview_IsVisibleChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UCRoomPreview_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                {
                    Hide();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCRoomPreview_PreviewKeyDown() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void Show(RoomModel model, bool fromSearch, Func<int> onClose = null)
        {
            this.IsVisibleChanged += UCRoomPreview_IsVisibleChanged;
            this.PreviewKeyDown += UCRoomPreview_PreviewKeyDown;
            this._OnClose = onClose;
            this.BuildRoomList(model, fromSearch);
            this.Visible = true;
            this.Focusable = true;
            this.Focus();
        }

        public void Hide(object param = null)
        {
            if (_OnClose != null)
            {
                _OnClose();
            }
            else
            {
                this.Visible = false;
            }
        }

        private void BuildRoomList(RoomModel model, bool fromSearch)
        {
            try
            {
                string contactID = model.RoomID;
                this._RoomList = fromSearch ? RingIDViewModel.Instance.RoomSearchList.Values.ToList() : RingIDViewModel.Instance.RoomList.Values.ToList();
                this._CurrIndex = this._RoomList.IndexOf(model);
                this.TotalCount = this._RoomList.Count;
                this.RevalidateRoomPreview(model);
            }
            catch (Exception ex)
            {
                log.Error("Error: InitView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void RevalidateRoomPreview(RoomModel model)
        {
            try
            {
                if (this.TotalCount == 1)
                {
                    this.LeftArrowVisible = Visibility.Collapsed;
                    this.RightArrowVisible = Visibility.Collapsed;
                }
                else if (this._CurrIndex == this.TotalCount - 1)
                {
                    this.LeftArrowVisible = Visibility.Visible;
                    this.RightArrowVisible = Visibility.Collapsed;
                }
                else if (this._CurrIndex == 0)
                {
                    this.LeftArrowVisible = Visibility.Collapsed;
                    this.RightArrowVisible = Visibility.Visible;
                }
                else
                {
                    this.LeftArrowVisible = Visibility.Visible;
                    this.RightArrowVisible = Visibility.Visible;
                }

                this.SequenceNumber = this._CurrIndex + 1;

                this._RoomID = model.RoomID;
                if (this._SingleRoomPanel == null)
                {
                    this._SingleRoomPanel = new UCSingleRoomPanel();
                }
                this._SingleRoomPanel.InitDataContext(model);
                this.pnlRoomPreview.Child = _SingleRoomPanel;
                this.InitRoomView();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnRight() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void InitRoomView()
        {
            try
            {
                if (_ViewTimer == null)
                {
                    _ViewTimer = new DispatcherTimer();
                    _ViewTimer.Interval = TimeSpan.FromSeconds(0.1);
                    _ViewTimer.Tick += (s, e) =>
                    {
                        if (pnlRoomPreview.Child != null)
                        {
                            ((UCSingleRoomPanel)pnlRoomPreview.Child).InitView();
                        }
                        _ViewTimer.Stop();
                    };
                }
                _ViewTimer.Stop();
                _ViewTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: InitMediaView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.Visible = false;
        }
        
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Property

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (value == _Visible)
                    return;

                _Visible = value;
                this.OnPropertyChanged("Visible");
            }
        }

        public int TotalCount
        {
            get { return _TotalCount; }
            set
            {
                if (value == _TotalCount)
                    return;

                _TotalCount = value;
                this.OnPropertyChanged("TotalCount");
            }
        }
        
        public int SequenceNumber
        {
            get { return _SequenceNumber; }
            set
            {
                if (value == _SequenceNumber)
                    return;

                _SequenceNumber = value;
                this.OnPropertyChanged("SequenceNumber");
            }
        }

        public Visibility LeftArrowVisible
        {
            get { return _LeftArrowVisible; }
            set
            {
                if (value == _LeftArrowVisible)
                    return;

                _LeftArrowVisible = value;
                this.OnPropertyChanged("LeftArrowVisible");
            }
        }

        public Visibility RightArrowVisible
        {
            get { return _RightArrowVisible; }
            set
            {
                if (value == _RightArrowVisible)
                    return;

                _RightArrowVisible = value;
                this.OnPropertyChanged("RightArrowVisible");
            }
        }

        public ICommand OnCloseCommand
        {
            get
            {
                if (_OnCloseCommand == null)
                {
                    _OnCloseCommand = new RelayCommand((param) => Hide(param));
                }
                return _OnCloseCommand;
            }
        }

        public ICommand OnLeftCommand
        {
            get
            {
                if (_OnLeftCommand == null)
                {
                    _OnLeftCommand = new RelayCommand((param) => { RevalidateRoomPreview(_RoomList[--_CurrIndex]); });
                }
                return _OnLeftCommand;
            }
        }

        public ICommand OnRightCommand
        {
            get
            {
                if (_OnRightCommand == null)
                {
                    _OnRightCommand = new RelayCommand((param) => { RevalidateRoomPreview(_RoomList[++_CurrIndex]); });
                }
                return _OnRightCommand;
            }
        }

        #endregion Property

    }
}

﻿using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Room
{
    /// <summary>
    /// Interaction logic for UCRoomMemberListPanel.xaml
    /// </summary>
    public partial class UCRoomMemberListPanel : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCRoomMemberListPanel).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private bool _Visible = false;
        private Func<int> _OnClose = null;
        private RoomModel _RoomModel = null;
        private bool _IsInitLoading = false;
        private bool _IsMoreLoading = false;
        public string _RoomID = null;

        private DispatcherTimer _ViewTimer = null;
        private ScrollViewer _Scroll = null;

        private ICommand _OnCloseCommand;
        private ICommand _OnEmptyCommand;

        public UCRoomMemberListPanel()
        {
            InitializeComponent();
        }

        #region Event Handler

        private void UCRoomMemberListPanel_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if ((bool)e.NewValue == false)
                {
                    if (_Scroll != null)
                    {
                        _Scroll.ScrollChanged -= _Scroll_ScrollChanged;
                        _Scroll = null;
                    }

                    IsInitLoading = false;
                    if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate();
                    IsMoreLoading = false;
                    if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate();

                    _ViewTimer.Stop();
                    _ViewTimer = null;

                    itcLikeMemberList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                    itcLikeMemberList.ItemsSource = null;

                    this._RoomID = null;
                    this.DataContext = null;
                    this.RoomModel = null;
                    this.Focusable = false;
                    this._OnClose = null;

                    this.IsVisibleChanged -= UCRoomMemberListPanel_IsVisibleChanged;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UCRoomMemberListPanel_IsVisibleChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            _Scroll = (ScrollViewer)sender;
            _Scroll.ScrollChanged += _Scroll_ScrollChanged;
        }

        private void _Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            if (IsInitLoading == false && IsMoreLoading == false && _Scroll != null && RoomModel.ChatMemberNotFound == false && e.VerticalChange > 0 && e.VerticalOffset >= (_Scroll.ScrollableHeight - 106.0))
            {
                if (!RoomModel.ChatMemberNotFound)
                {
                    this.MoreGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
                    IsMoreLoading = true;

                    string prevPagingState = RoomModel.PagingState ?? String.Empty;
                    ChatService.GetRoomMemberList(RoomModel.RoomID, RoomModel.PagingState, 15, (args) =>
                    {
                        if (!String.IsNullOrWhiteSpace(_RoomID) && _RoomID.Equals(args.RoomID))
                        {
                            if (args.Status)
                            {
                                //log.Debug("prevPagingState => " + prevPagingState + ", RoomModel.PagingState => " + RoomModel.PagingState);
                                for (int i = 0; prevPagingState.Equals(RoomModel.PagingState ?? String.Empty) && i < 5000; i += 250)
                                {
                                    Thread.Sleep(250);
                                    //log.Debug("prevPagingState => " + prevPagingState + ", RoomModel.PagingState => " + RoomModel.PagingState);
                                }
                                IsMoreLoading = false;
                                Application.Current.Dispatcher.BeginInvoke(() => { if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                            }
                            else
                            {
                                IsMoreLoading = false;
                                Application.Current.Dispatcher.BeginInvoke(() => { if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                            }
                        }
                    });
                }
            }
        }

        #endregion Event Handler

        #region Utility Method

        public void Show(RoomModel model, Func<int> onClose = null)
        {
            this.IsVisibleChanged += UCRoomMemberListPanel_IsVisibleChanged;

            this._RoomID = model.RoomID;
            this.IsInitLoading = true;
            this.IsMoreLoading = false;
            this.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
            this._OnClose = onClose;

            this.RoomModel = model;
            this.DataContext = this;
            this.BuildMemberList();

            this.Visible = true;
            this.Focusable = true;
            this.Focus();
        }

        public void Hide(object param = null)
        {
            if (_OnClose != null)
            {
                _OnClose();
            }
            else
            {
                this.Visible = false;
            }
        }

        private void BuildMemberList()
        {
            try
            {
                if (_ViewTimer == null)
                {
                    _ViewTimer = new DispatcherTimer();
                    _ViewTimer.Interval = TimeSpan.FromSeconds(0.5);
                    _ViewTimer.Tick += (s, e) =>
                    {
                        itcLikeMemberList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                        {
                            Path = new PropertyPath("RoomModel.MemberInfoList"),
                        });

                        string prevPagingState = RoomModel.PagingState ?? String.Empty;
                        ChatService.GetRoomMemberList(RoomModel.RoomID, RoomModel.PagingState, 15, (args) =>
                        {
                            if (!String.IsNullOrWhiteSpace(_RoomID) && _RoomID.Equals(args.RoomID))
                            {
                                if (args.Status)
                                {
                                    //log.Debug("prevPagingState => " + prevPagingState + ", RoomModel.PagingState => " + RoomModel.PagingState);
                                    for (int i = 0; prevPagingState.Equals(RoomModel.PagingState ?? String.Empty) && i < 5000; i += 250)
                                    {
                                        Thread.Sleep(250);
                                        //log.Debug("prevPagingState => " + prevPagingState + ", RoomModel.PagingState => " + RoomModel.PagingState);
                                    }
                                    IsInitLoading = false;
                                    Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                                }
                                else
                                {
                                    IsInitLoading = false;
                                    Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                                }
                            }
                        });

                        _ViewTimer.Stop();
                    };
                }
                _ViewTimer.Stop();
                _ViewTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: BuildSeenMemberList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.Visible = false;
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Property

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                if (value == _Visible)
                    return;

                _Visible = value;
                this.OnPropertyChanged("Visible");
            }
        }

        public bool IsInitLoading
        {
            get { return _IsInitLoading; }
            set
            {
                if (value == _IsInitLoading)
                    return;

                _IsInitLoading = value;
                this.OnPropertyChanged("IsInitLoading");
            }
        }

        public bool IsMoreLoading
        {
            get { return _IsMoreLoading; }
            set
            {
                if (value == _IsMoreLoading)
                    return;

                _IsMoreLoading = value;
                this.OnPropertyChanged("IsMoreLoading");
            }
        }

        public RoomModel RoomModel
        {
            get { return _RoomModel; }
            set
            {
                if (value == _RoomModel)
                    return;

                _RoomModel = value;
                this.OnPropertyChanged("RoomModel");
            }
        }

        public ICommand OnCloseCommand
        {
            get
            {
                if (_OnCloseCommand == null)
                {
                    _OnCloseCommand = new RelayCommand((param) => Hide(param));
                }
                return _OnCloseCommand;
            }
        }

        public ICommand OnEmptyCommand
        {
            get
            {
                if (_OnEmptyCommand == null)
                {
                    _OnEmptyCommand = new RelayCommand(param => { });
                }
                return _OnEmptyCommand;
            }
        }

        #endregion Property


    }
}

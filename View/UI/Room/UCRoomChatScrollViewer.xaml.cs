﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using Models.Utility.Chat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.Recent;
using View.ViewModel;

namespace View.UI.Room
{
    /// <summary>
    /// Interaction logic for UCRoomChatScrollViewer.xaml
    /// </summary>
    public partial class UCRoomChatScrollViewer : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCRoomChatScrollViewer).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private RoomModel _RoomModel;
        private ObservableCollection<RecentModel> _RecentModelList = new ObservableCollection<RecentModel>();
        public string _RoomID;

        private bool _IS_INITIALIZED = false;
        private bool _IS_LOADING = false;
        private bool _IsHistoryLoadingFromServer = false;
        private double? _PrevOffset = null;
        private double? _PrevHeight = null;
        private System.Timers.Timer _ResizeTimer = null;
        private System.Timers.Timer _ScorllTimer = null;
        public StackPanel _StackPanel = null;
        private ScrollBar _ScrollBar = null;
        public bool _IsScrollBarCaptured = false;

        #region Constructor

        public UCRoomChatScrollViewer(RoomModel model)
        {
            InitializeComponent();
            this.RoomModel = model;
            this._RoomID = model.RoomID;
            this.RecentModelList = RingIDViewModel.Instance.GetRecentModelListByID(_RoomID);
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private void ScrollBar_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this._IsScrollBarCaptured = false;
        }

        private void ScrollBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this._IsScrollBarCaptured = true;
        }

        private void RecentListContainer_SizeChanged(object sender, SizeChangedEventArgs args)
        {
            try
            {
                if (!this._IsScrollBarCaptured)
                {
                    this.ChangeMessageStatusOnResize();

                    if (this._PrevOffset != null && this._PrevHeight != null && this._IS_LOADING)
                    {
                        this.srvRecentList.ScrollToVerticalOffset(this._StackPanel.ActualHeight - (double)this._PrevHeight + (double)this._PrevOffset);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: RecentListContainer_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
                {
                    if (this._IS_LOADING && this._PrevHeight != null && this._PrevOffset != null)
                    {
                        this._PrevOffset += e.VerticalChange;
                    }

                    if (this._IS_LOADING == false && e.VerticalChange < 0 && e.VerticalOffset <= ChatHelpers.UP_SCROLL_LIMIT && this.RecentModelList.Count > 1)
                    {
                        // SCROLL TO TOP
                        this._IS_LOADING = true;
                        this.srvRecentList.CanContentScroll = false;

                        if (this.RoomModel.ChatHistoryNotFound == false)
                        {
                            string prevMinPacketID = this.RoomModel.ChatMinPacketID ?? String.Empty;
                            this.IsHistoryLoadingFromServer = true;
                            this._PrevOffset = e.VerticalOffset;
                            this._PrevHeight = this._StackPanel.ActualHeight;
#if CHAT_LOG
                            log.Info("FETCH ROOM HISTORY(SCROLL) => RoomID = " + this._RoomID + ", PacketID = " + this.RoomModel.ChatMinPacketID + ", LIMIT = " + 3);
#endif
                            ChatService.RequestRoomChatHistory(this._RoomID, this.RoomModel.ChatMinPacketID, ChatConstants.HISTORY_UP, 8, (args) =>
                            {
                                Thread.Sleep(1000);
                                for (int i = 0; prevMinPacketID.Equals(this.RoomModel.ChatMinPacketID ?? String.Empty) && i < 5000; i += 100)
                                {
                                    Thread.Sleep(100);
                                }

                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    this.srvRecentList.CanContentScroll = true;
                                    this._IS_LOADING = false;
                                    this.IsHistoryLoadingFromServer = false;
                                    this._PrevOffset = null;
                                    this._PrevHeight = null;

                                    if (srvRecentList.VerticalOffset < 200 && !prevMinPacketID.Equals(this.RoomModel.ChatMinPacketID ?? String.Empty))
                                    {
                                        this.srvRecentList.ScrollToVerticalOffset(this.srvRecentList.VerticalOffset + 200);
                                    }
                                }, DispatcherPriority.ApplicationIdle);
                            });
                        }
                        else
                        {
                            this._PrevOffset = null;
                            this._PrevHeight = null;
                            this._IS_LOADING = false;
                            this.srvRecentList.CanContentScroll = true;
                        }
                    }

                    this.ChangeMessageStatusOnScroll();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: srvChatContainer_OnScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
                this._PrevOffset = null;
                this._PrevHeight = null;
                this._IS_LOADING = false;
                this.srvRecentList.CanContentScroll = true;
            }
        }

        private void ScrollViewer_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                this.srvRecentList.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                this.srvRecentList.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this._StackPanel = (StackPanel)sender;
            this.srvRecentList.ApplyTemplate();
            this._ScrollBar = this.srvRecentList.Template.FindName("PART_VerticalScrollBar", this.srvRecentList) as ScrollBar;
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeScrollViewer()
        {
            if (!this._IS_INITIALIZED)
            {
                this._IS_INITIALIZED = true;
                this.srvRecentList.ScrollToBottom();

                this.RoomModel.ChatHistoryNotFound = false;
                this.RoomModel.ChatMinPacketID = String.Empty;
                this.RoomModel.ChatMaxPacketID = String.Empty;

                this.tvRecentList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                {
                    Path = new PropertyPath("RecentModelList"),
                });
                this.srvRecentList.PreviewKeyDown += this.ScrollViewer_PreviewKeyDown;

                ReloadRecentList(null, (args) =>
                {
                    Thread.Sleep(350);
                    this.MoveToBottom(true);
                    Thread.Sleep(1000);

                    if (this._IS_INITIALIZED)
                    {
                        this._IS_LOADING = false;
                        this.srvRecentList.ScrollChanged += this.ScrollViewer_OnScrollChanged;
                        this.SizeChanged += this.RecentListContainer_SizeChanged;
                        if (this._StackPanel != null) this._StackPanel.SizeChanged += this.RecentListContainer_SizeChanged;
                        if (this._ScrollBar != null) this._ScrollBar.PreviewMouseLeftButtonDown += this.ScrollBar_MouseLeftButtonDown;
                        if (this._ScrollBar != null) this._ScrollBar.PreviewMouseLeftButtonUp += this.ScrollBar_MouseLeftButtonUp;
                        this.ChangeMessageStatusOnResize();
                    }
                });
            }
            else
            {
                ReloadRecentList(null);
            }
        }

        public void ReleaseScrollViewer()
        {
            this._IS_INITIALIZED = false;
            this._IS_LOADING = false;
            this.IsHistoryLoadingFromServer = false;
            this.srvRecentList.ScrollChanged -= this.ScrollViewer_OnScrollChanged;
            this.srvRecentList.PreviewKeyDown -= this.ScrollViewer_PreviewKeyDown;
            this.SizeChanged -= this.RecentListContainer_SizeChanged;
            if (this._StackPanel != null) this._StackPanel.SizeChanged -= this.RecentListContainer_SizeChanged;
            if (this._ScrollBar != null) this._ScrollBar.PreviewMouseLeftButtonDown -= this.ScrollBar_MouseLeftButtonDown;
            if (this._ScrollBar != null) this._ScrollBar.PreviewMouseLeftButtonUp -= this.ScrollBar_MouseLeftButtonUp;
            if (this._ResizeTimer != null) this._ResizeTimer.Stop();
            if (this._ScorllTimer != null) this._ScorllTimer.Stop();

            Task.Factory.StartNew(() =>
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this.RecentModelList.Where(P => P.IsRecentOpened).ToList().ForEach(P => P.IsRecentOpened = false);
                    this.tvRecentList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                    this.tvRecentList.ItemsSource = null;

                    List<RecentModel> tempList = this.RecentModelList.Skip(this.RecentModelList.Count - 20 > 0 ? this.RecentModelList.Count - 20 : 0).ToList();
                    RecentModel tempModel = tempList.FirstOrDefault();
                    if (tempModel != null)
                    {
                        if (tempModel.Message != null)
                        {
                            tempModel.Message.ViewModel.PrevIdentity = 0;
                            tempModel.Message.ViewModel.IsSamePrevAndCurrID = false;
                        }
                        if (tempModel.Type != ChatConstants.SUBTYPE_DATE_TITLE)
                        {
                            tempList.Insert(0, new RecentModel(new RecentDTO { ContactID = tempModel.ContactID, RoomID = tempModel.RoomID, Time = ModelUtility.GetStartTimeMillisLocal(tempModel.DateTime) }));
                        }
                    }

                    this.RecentModelList.Clear();
                    tempList.ForEach(i => this.RecentModelList.Add(i));
                }, DispatcherPriority.ApplicationIdle);
            });
        }

        public void ReloadRecentList(int? days, ChatEventDelegate onComplete = null)
        {
            try
            {
                if (this._IS_LOADING == false)
                {
                    this._IS_LOADING = true;

                    int limit = (int)((this.ActualHeight + 150) / ChatHelpers.UI_UNIT_SIZE);
                    List<string> uniqueKeys = this.RecentModelList.Where(P => P.Type != ChatConstants.SUBTYPE_DATE_TITLE).Select(P => P.UniqueKey).ToList();

                    if (days != null)
                    {
                        long time = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - ((int)days * SettingsConstants.MILISECONDS_IN_DAY);

                        ContentPresenter minItem = null;
                        int max = this.tvRecentList.Items.Count;
                        int min = 0;
                        int pivot;

                        while (max > min)
                        {
                            pivot = (min + max) / 2;
                            ContentPresenter item = (ContentPresenter)this.tvRecentList.ItemContainerGenerator.ContainerFromIndex(pivot);
                            long pivotTime = item != null ? ((RecentModel)item.Content).Time : 0;
                            if (time > pivotTime)
                            {
                                min = pivot + 1;
                                minItem = item;
                            }
                            else
                            {
                                max = pivot;
                            }
                        }

                        if (minItem != null)
                        {
                            GeneralTransform itemTransform = minItem.TransformToAncestor(this.srvRecentList);
                            Rect rectangle = itemTransform.TransformBounds(new Rect(new System.Windows.Point(0, 0), minItem.RenderSize));
                            this.srvRecentList.ScrollToVerticalOffset(rectangle.Top + this.srvRecentList.VerticalOffset);
                        }
                        else if (min == 0)
                        {
                            this.srvRecentList.ScrollToTop();
                        }
                        else
                        {
                            this.srvRecentList.ScrollToBottom();
                        }

                        ChangeLoadingStatus(onComplete);
                    }
                    else if (uniqueKeys.Count <= 0 || onComplete != null)
                    {
                        if (this.RoomModel.ChatHistoryNotFound == false)
                        {
#if CHAT_LOG
                            log.Info("FETCH ROOM HISTORY(EMPTY) => RoomID = " + this._RoomID + ", PacketID = " + this.RoomModel.ChatMinPacketID + ", LIMIT = " + limit);
#endif
                            ChatService.RequestRoomChatHistory(this._RoomID, this.RoomModel.ChatMinPacketID, ChatConstants.HISTORY_UP, limit, onComplete);
                        }
                        else
                        {
                            ChangeLoadingStatus(onComplete);
                        }
                    }
                    else if (uniqueKeys.Count < limit)
                    {
                        if (this.RoomModel.ChatHistoryNotFound == false)
                        {
#if CHAT_LOG
                            log.Info("FETCH ROOM HISTORY(FILL) => RoomID = " + this._RoomID + ", PacketID = " + this.RoomModel.ChatMinPacketID + ", LIMIT = " + (limit - uniqueKeys.Count));
#endif
                            ChatService.RequestRoomChatHistory(this._RoomID, this.RoomModel.ChatMinPacketID, ChatConstants.HISTORY_UP, (limit - uniqueKeys.Count), onComplete);
                        }
                        else
                        {
                            ChangeLoadingStatus(onComplete);
                        }
                    }
                    else
                    {
                        ChangeLoadingStatus(onComplete);
                    }

                    if (onComplete == null)
                    {
                        this.ChangeMessageStatusOnResize();
                        this._IS_LOADING = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ReloadRecentList() ==> " + ex.Message + "\n" + ex.StackTrace);
                this._IS_LOADING = false;
            }
        }

        private void ChangeLoadingStatus(ChatEventDelegate onComplete)
        {
            Thread t = new Thread(new ThreadStart(() =>
            {
                try
                {
                    Thread.Sleep(10);
                    if (onComplete != null)
                    {
                        onComplete(null);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: ChangeLoadingStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }));
            t.Start();
        }

        public void ChangeMessageStatusOnResize()
        {
            try
            {
                if (this._ResizeTimer == null)
                {
                    this._ResizeTimer = new System.Timers.Timer { AutoReset = false };
                    this._ResizeTimer.Interval = 350;
                    this._ResizeTimer.Elapsed += (o, e) =>
                    {
                        this.ChangeMessageStatus();
                        this._ResizeTimer.Stop();
                    };
                }

                if (this._ScorllTimer != null && this._ScorllTimer.Enabled)
                {
                    this._ScorllTimer.Stop();
                }
                this._ResizeTimer.Stop();
                this._ResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeMessageStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ChangeMessageStatusOnScroll()
        {
            try
            {
                if (this._ScorllTimer == null)
                {
                    this._ScorllTimer = new System.Timers.Timer { AutoReset = false };
                    this._ScorllTimer.Elapsed += (o, e) =>
                    {
                        this.ChangeMessageStatus();
                        this._ScorllTimer.Stop();
                    };
                }
                this._ScorllTimer.Stop();
                this._ScorllTimer.Interval = this._IsScrollBarCaptured ? 10 : 30;
                if (this._ResizeTimer == null || !this._ResizeTimer.Enabled)
                {
                    this._ScorllTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeMessageStatusOnScroll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ChangeMessageStatus()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (UIHelperMethods.IsViewActivateAndVisible(HelperMethods.FindVisualParent<UserControl>(this)))
                {
                    ChatHelpers.ChangeChatViewPortOpenedProperty(this.srvRecentList, this.tvRecentList, this._StackPanel, this._IsScrollBarCaptured, null, null);
                }
            }, DispatcherPriority.Send);
        }

        public void MoveToBottom(bool triggerMessageOpened = false)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    List<RecentModel> list = this.RecentModelList.ToList();
                    for (int idx = list.Count - 1, height = (int)this.srvRecentList.ViewportHeight; idx >= 0 && height > 0; idx--)
                    {
                        RecentModel model = list.ElementAt(idx);
                        model.IsRecentOpened = true;
                        if (triggerMessageOpened && model.Message != null)
                        {
                            model.Message.IsMessageOpened = true;
                        }
                        height -= model.PrevViewHeight;
                    }
                    this.srvRecentList.ScrollToBottom();
                }, DispatcherPriority.ApplicationIdle);
            }
            catch (Exception ex)
            {
                log.Error("Error: MoveToBottom() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public RoomModel RoomModel
        {
            get { return _RoomModel; }
            set { _RoomModel = value; }
        }

        public ObservableCollection<RecentModel> RecentModelList
        {
            get { return _RecentModelList; }
            set
            {
                _RecentModelList = value;
                this.OnPropertyChanged("RecentModelList");
            }
        }

        public bool IsHistoryLoadingFromServer
        {
            get { return _IsHistoryLoadingFromServer; }
            set
            {
                if (_IsHistoryLoadingFromServer == value)
                    return;

                _IsHistoryLoadingFromServer = value;
                this.OnPropertyChanged("IsHistoryLoadingFromServer");
            }
        }

        #endregion Property

    }
}

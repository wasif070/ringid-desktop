﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.ViewModel;

namespace View.UI.Room
{
    /// <summary>
    /// Interaction logic for UCRoomListAllPanel.xaml
    /// </summary>
    public partial class UCRoomListAllPanel : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCRoomListAllPanel).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private bool _IsInitLoading = false;
        private bool _IsMoreLoading = false;

        private ICommand _OnSelectCommand;
        private UCRoomListPanel.OnSelectRoom _OnSelectRoom;

        private DispatcherTimer _InitTimer = null;
        private DispatcherTimer _ResizeTimer = null;
        private DispatcherTimer _ScorllTimer = null;
        private StackPanel _StackPanel = null;

        #region Constructor

        public UCRoomListAllPanel(UCRoomListPanel.OnSelectRoom onSelectRoom)
        {
            InitializeComponent();
            this._OnSelectRoom = onSelectRoom;
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this._StackPanel = (StackPanel)sender;
            this._StackPanel.SizeChanged -= this.RoomListContainer_SizeChanged;
            this._StackPanel.SizeChanged += this.RoomListContainer_SizeChanged;
        }

        private void RoomListContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                this.ChangeMessageStatusOnResize();
            }
            catch (Exception ex)
            {
                log.Error("Error: RoomListContainer_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            try
            {
                if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 45 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                }
                else if (IsInitLoading == false && IsMoreLoading == false && RingIDViewModel.Instance.RoomListFetchQuery.NoMoreRoom == false && e.VerticalChange > 0 && scrollViewer.VerticalOffset > (scrollViewer.ScrollableHeight - 100))
                {
                    IsMoreLoading = true;
                    this.MoreGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
                    int startIndex = RingIDViewModel.Instance.RoomListFetchQuery.StartIndex;

                    ChatService.GetRoomListWithHistory(startIndex, 5, (args) =>
                    {
                        if (args.Status)
                        {
                            for (int i = 0; startIndex == RingIDViewModel.Instance.RoomListFetchQuery.StartIndex && i < 2000; i += 100)
                            {
                                Thread.Sleep(100);
                            }
                            IsMoreLoading = false;
                            Application.Current.Dispatcher.BeginInvoke(() => { if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                        }
                        else
                        {
                            IsMoreLoading = false;
                            Application.Current.Dispatcher.BeginInvoke(() => { if (MoreGIFCtrl != null && MoreGIFCtrl.IsRunning()) this.MoreGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                        }
                    });
                }

                this.ChangeMessageStatusOnScroll();
            }
            catch (Exception ex)
            {
                log.Error("Error: ScrlViewer_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void BuildRoomList()
        {
            try
            {
                if (_InitTimer == null)
                {
                    this.IsInitLoading = true;
                    this.IsMoreLoading = false;
                    this.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

                    _InitTimer = new DispatcherTimer();
                    _InitTimer.Interval = TimeSpan.FromSeconds(0.3);
                    _InitTimer.Tick += (s, e) =>
                    {
                        object resource = Application.Current.FindResource("RingIDSettings");
                        itcRoomList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                        {
                            Path = new PropertyPath("MainViewModel.RoomList"),
                            Source = resource
                        });
                        int startIndex = RingIDViewModel.Instance.RoomListFetchQuery.StartIndex;

                        if (RingIDViewModel.Instance.RoomList.Count < 10)
                        {
                            ChatService.GetRoomListWithHistory(0, 10, (args) =>
                            {
                                if (args.Status)
                                {
                                    for (int i = 0; startIndex == RingIDViewModel.Instance.RoomListFetchQuery.StartIndex && i < 2000; i += 100)
                                    {
                                        Thread.Sleep(100);
                                    }
                                    this.IsInitLoading = false;
                                    Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                                }
                                else
                                {
                                    _InitTimer.Stop();
                                    _InitTimer = null;

                                    this.IsInitLoading = false;
                                    Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                                }

                                ChangeMessageStatusOnResize();
                            });
                        }
                        else
                        {
                            this.IsInitLoading = false;
                            Application.Current.Dispatcher.BeginInvoke(() => { if (InitGIFCtrl != null && InitGIFCtrl.IsRunning()) this.InitGIFCtrl.StopAnimate(); }, DispatcherPriority.Send);
                            ChangeMessageStatusOnResize();
                        }
                        _InitTimer.Stop();

                    };
                    _InitTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: BuildRoomList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ChangeMessageStatusOnResize()
        {
            try
            {
                if (this._ResizeTimer == null)
                {
                    this._ResizeTimer = new DispatcherTimer();
                    this._ResizeTimer.Interval = TimeSpan.FromMilliseconds(350);
                    this._ResizeTimer.Tick += (o, e) =>
                    {
                        UCRoomListPanel panel = (UCRoomListPanel)HelperMethods.FindVisualParent<UserControl>(this);
                        if (UIHelperMethods.IsViewActivateAndVisible(panel))
                        {
                            ChatHelpers.ChangeRoomViewPortOpenedProperty(panel.ScrlViewer, this, this.itcRoomList, this._StackPanel);
                        }
                        this._ResizeTimer.Stop();
                    };
                }

                this._ResizeTimer.Stop();
                if (this._ScorllTimer != null && this._ScorllTimer.IsEnabled)
                {
                    this._ScorllTimer.Stop();
                }
                if (this._InitTimer != null && !this._InitTimer.IsEnabled)
                {
                    this._ResizeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ChangeMessageStatusOnScroll()
        {
            try
            {
                if (this._ScorllTimer == null)
                {
                    this._ScorllTimer = new DispatcherTimer();
                    this._ScorllTimer.Interval = TimeSpan.FromMilliseconds(30);
                    this._ScorllTimer.Tick += (o, e) =>
                    {
                        UCRoomListPanel panel = (UCRoomListPanel)HelperMethods.FindVisualParent<UserControl>(this);
                        if (UIHelperMethods.IsViewActivateAndVisible(panel))
                        {
                            ChatHelpers.ChangeRoomViewPortOpenedProperty(panel.ScrlViewer, this, this.itcRoomList, this._StackPanel);
                        }
                        this._ScorllTimer.Stop();
                    };
                }

                this._ScorllTimer.Stop();
                if (this._InitTimer != null && !this._InitTimer.IsEnabled)
                {
                    this._ScorllTimer.Start();
                }
                if (this._ResizeTimer != null && !this._ResizeTimer.IsEnabled)
                {
                    this._ScorllTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand OnSelectCommand
        {
            get
            {
                if (_OnSelectCommand == null)
                {
                    _OnSelectCommand = new RelayCommand(param => this._OnSelectRoom((RoomModel)param, false));
                }
                return _OnSelectCommand;
            }
        }

        public bool IsInitLoading
        {
            get
            {
                return _IsInitLoading;
            }
            set
            {
                if (_IsInitLoading == value)
                    return;
                _IsInitLoading = value;
                this.OnPropertyChanged("IsInitLoading");
            }
        }

        public bool IsMoreLoading
        {
            get
            {
                return _IsMoreLoading;
            }
            set
            {
                if (_IsMoreLoading == value)
                    return;
                _IsMoreLoading = value;
                this.OnPropertyChanged("IsMoreLoading");
            }
        }

        #endregion Property

    }
}

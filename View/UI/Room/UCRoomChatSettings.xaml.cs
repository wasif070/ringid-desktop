﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Auth.Service.Chat;
using Auth.utility;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using System.Windows.Threading;

namespace View.UI.Room
{
    /// <summary>
    /// Interaction logic for UCRoomChatSettings.xaml
    /// </summary>
    public partial class UCRoomChatSettings : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCRoomChatSettings).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private RoomModel _RoomModel;
        public string _RoomID;
        private ICommand _ChatSettingsCommand;
        private ICommand _ShowRoomMemberCommand;

        private UCRoomMemberListPanel _RoomMemberListPanel = null;
        private bool _IsRoomMemberListMode = false;

        private ObservableCollection<ChatBgHistoryModel> _ChatBgHistoryModelList = new ObservableCollection<ChatBgHistoryModel>();
        private ICommand _OnChatBackgroundChangeCommand;
        private BackgroundWorker _ChatBgHistoryLoader = null;
        public bool _IsChatBgLoaded = false;

        #region Constructor

        public UCRoomChatSettings(RoomModel model)
        {
            InitializeComponent();
            this.RoomModel = model;
            this._RoomID = model.RoomID;
            this._ChatBgHistoryLoader = new BackgroundWorker();
            this._ChatBgHistoryLoader.DoWork += ChatBgHistory_DowWork;
            this._ChatBgHistoryLoader.RunWorkerCompleted += ChatBgHistory_RunWorkerCompleted;
            this.ChatBgHistoryModelList.Add(new ChatBgHistoryModel { ChatBgUrl = "", IsAction = true });
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private void ChatBgHistory_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_IsChatBgLoaded == false)
                {
                    Thread.Sleep(200);
                }

                List<ChatBgHistoryModel> bgModels = RingIDViewModel.Instance.ChatBackgroundList.Values.ToList();
                bgModels = bgModels.Where(P => !ChatBgHistoryModelList.Any(Q => Q.ChatBgUrl.Equals(P.ChatBgUrl))).ToList();

                foreach (ChatBgHistoryModel model in bgModels)
                {
                    Thread.Sleep(5);
                    ChatBgHistoryModelList.InvokeInsert(ChatBgHistoryModelList.Count - 1, model);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ChatBgHistory_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
            e.Result = true;
        }

        private void ChatBgHistory_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _IsChatBgLoaded = true;
        }

        private void mnuMoreBackground_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool bgImageFound = false;
                MessegeBoxWithLoader msgBox = UIHelperMethods.ShowMessageWithLoader("Getting Background Images");
                LoadChatBackground loader = new LoadChatBackground(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER);
                loader.OnFetchSingleBgImage += (imageDTO) =>
                {
                    ChatBgHistoryModel model = RingIDViewModel.Instance.ChatBackgroundList.TryGetValue(imageDTO.name);
                    if (model == null)
                    {
                        model = new ChatBgHistoryModel(imageDTO);
                        RingIDViewModel.Instance.ChatBackgroundList[imageDTO.name] = model;
                        ChatBgHistoryDAO.SaveChatBgHistory(imageDTO);
                    }

                    if (!ChatBgHistoryModelList.Contains(model))
                    {
                        bgImageFound = true;
                        ChatBgHistoryModelList.InvokeInsert(ChatBgHistoryModelList.Count - 1, model);
                    }
                };
                loader.OnComplete += () =>
                {
                    if (_ChatBgHistoryLoader.IsBusy == false)
                    {
                        _ChatBgHistoryLoader.RunWorkerAsync();
                    }
                    if (bgImageFound == false)
                    {
                        msgBox.ShowCompleMessage("No More Background");
                    }
                    Thread.Sleep(1000);
                    msgBox.CloseMessage();
                };
                loader.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuMoreBackground_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mnuChooseExistingPhoto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Title = "Select picture";
                dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
                dialog.Multiselect = false;
                if (dialog.ShowDialog() == true)
                {
                    string fileName = dialog.FileName;
                    Bitmap originalBitmap = ImageUtility.GetBitmapOfDynamicResource(fileName);

                    System.Drawing.Rectangle resolution = System.Windows.Forms.Screen.PrimaryScreen.Bounds;

                    if (originalBitmap.Width < DefaultSettings.MIN_CHAT_BG_WIDTH || originalBitmap.Height < DefaultSettings.MIN_CHAT_BG_HEIGHT)
                    {
                        UIHelperMethods.ShowWarning("Chat background photo's minimum resulotion: " + DefaultSettings.MIN_CHAT_BG_WIDTH + " x " + DefaultSettings.MIN_CHAT_BG_HEIGHT, "Chat background");
                        //  CustomMessageBox.ShowError("Chat background photo's minimum resulotion: " + DefaultSettings.MIN_CHAT_BG_WIDTH + " x " + DefaultSettings.MIN_CHAT_BG_HEIGHT);
                        return;
                    }

                    string savedFileName = ChatHelpers.SaveChatBackgroundFromDirectory(originalBitmap);
                    if (!String.IsNullOrWhiteSpace(savedFileName))
                    {
                        ChatBgImageDTO imageDTO = new ChatBgImageDTO();
                        imageDTO.name = savedFileName;
                        imageDTO.UpdateTime = ChatService.GetServerTime();

                        ChatBgHistoryModel model = new ChatBgHistoryModel(imageDTO);
                        RingIDViewModel.Instance.ChatBackgroundList[imageDTO.name] = model;
                        ChatBgHistoryModelList.InvokeInsert(ChatBgHistoryModelList.Count - 1, model);

                        ChatBgHistoryDAO.SaveChatBgHistoryFromThread(imageDTO);
                        OnChatBackgroundChange(model);
                    }
                    else
                    {
                        UIHelperMethods.ShowWarning("Chat background photo's selection failed!", "Chat background");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuChooseExistingPhoto_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void btnLeave_Click(object sender, RoutedEventArgs e)
        {
            ChatService.UnRegisterPublicRoomChat(_RoomID);
            RingIDViewModel.Instance.OnRoomListClicked(null);
        }

        #endregion Event Handler

        #region Utility Methods

        public void LoadInitInformation()
        {
            if (_IsChatBgLoaded == false && _ChatBgHistoryLoader.IsBusy == false)
            {
                _ChatBgHistoryLoader.RunWorkerAsync();
            }
        }

        private void OnChatSettingsClicked(object param)
        {
            try
            {
                int value = Int32.Parse(param.ToString());
                if (value == 0)
                {
                    //RoomModel.ImNotificationEnabled = !RoomModel.ImNotificationEnabled;
                    RoomDAO.UpdateIMNotification(RoomModel.RoomID, RoomModel.ImNotificationEnabled);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: OnChatSettingsClicked() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void OnChatBackgroundChange(object param)
        {
            try
            {
                //SELECT BACKGROUND
                ChatBgHistoryModel bgModel = param as ChatBgHistoryModel;

                if (!String.IsNullOrWhiteSpace(RoomModel.ChatBgUrl))
                {
                    if (bgModel.ChatBgUrl.Equals(RoomModel.ChatBgUrl))
                    {
                        RoomModel.ChatBgUrl = String.Empty;
                        RoomDAO.UpdateChatBackground(RoomModel.RoomID, String.Empty);
                        return;
                    }
                }

                RoomModel.ChatBgUrl = bgModel.ChatBgUrl;
                RoomModel.EventChatBgUrl = null;
                RoomDAO.UpdateChatBackground(RoomModel.RoomID, RoomModel.ChatBgUrl);

                bgModel.UpdateTime = ChatService.GetServerTime();
                ChatBgHistoryDAO.UpdateChatBgLastUsedTime(RoomModel.ChatBgUrl, bgModel.UpdateTime);

                if (bgModel.Id > 0 && new ImageFile(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER + System.IO.Path.DirectorySeparatorChar + bgModel.ChatBgUrl).IsComplete == false)
                {
                    MessegeBoxWithLoader msgBox = UIHelperMethods.ShowMessageWithLoader("Downloading Selected Background");
                    ChatBackgroundDownloader downloader = new ChatBackgroundDownloader(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER, bgModel.ChatBgUrl);
                    downloader.OnComplete += (imageUrl, status) =>
                    {
                        msgBox.CloseMessage();
                        if (status && RoomModel.ChatBgUrl.Equals(imageUrl))
                        {
                            RoomModel.OnPropertyChanged("ChatBgUrl");
                        }
                    };
                    downloader.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChatBackgroundChange() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnRoomMembersClick(object param)
        {
            ShowRoomMemberList();
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Popup Methods

        public void ShowRoomMemberList()
        {
            try
            {
                _RoomMemberListPanel = new UCRoomMemberListPanel();
                _RoomMemberListPanel.Show(RoomModel, () =>
                {
                    HideRoomMemberList();
                    return 0;
                });
                pnlRoomMemberListContainer.Child = _RoomMemberListPanel;
                IsRoomMemberListMode = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowRoomMemberList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideRoomMemberList(bool isClosed = false)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (_RoomMemberListPanel != null)
                    {
                        _RoomMemberListPanel.Dispose();
                        _RoomMemberListPanel = null;
                    }
                    pnlRoomMemberListContainer.Child = null;
                    IsRoomMemberListMode = false;
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: HideRoomMemberList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Popup Methods

        #region Property

        public RoomModel RoomModel
        {
            get { return _RoomModel; }
            set { _RoomModel = value; }
        }

        public ICommand ChatSettingsCommand
        {
            get
            {
                if (_ChatSettingsCommand == null)
                {
                    _ChatSettingsCommand = new RelayCommand(param => OnChatSettingsClicked(param));
                }
                return _ChatSettingsCommand;
            }
        }

        public ICommand OnChatBackgroundChangeCommand
        {
            get
            {
                if (_OnChatBackgroundChangeCommand == null)
                {
                    _OnChatBackgroundChangeCommand = new RelayCommand((param) => OnChatBackgroundChange(param));
                }
                return _OnChatBackgroundChangeCommand;
            }
        }

        public ICommand RoomMembersCommand
        {
            get
            {
                if (_ShowRoomMemberCommand == null)
                {
                    _ShowRoomMemberCommand = new RelayCommand((param) => OnRoomMembersClick(param));
                }
                return _ShowRoomMemberCommand;
            }
        }

        public ObservableCollection<ChatBgHistoryModel> ChatBgHistoryModelList
        {
            get { return _ChatBgHistoryModelList; }
            set { _ChatBgHistoryModelList = value; }
        }

        public bool IsRoomMemberListMode
        {
            get { return _IsRoomMemberListMode; }
            set
            {
                if (_IsRoomMemberListMode == value)
                    return;
                _IsRoomMemberListMode = value;
                this.OnPropertyChanged("IsRoomMemberListMode");
            }
        }

        #endregion Property

    }
}
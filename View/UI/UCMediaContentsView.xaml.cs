﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for UCSingleAlbumSong.xaml
    /// </summary>
    public partial class UCMediaContentsView : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCMediaContentsView).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        public MediaContentModel albumModel = null;
        public HashTagModel hashTagModel = null;
        public string SearchParam;
        public bool RemoveFromAlbum;//1 for album, 2 for download, 3 for recent
        //public Object ParentInstance = null;
        public static UCMediaContentsView Instance = null;
        int type;

        #region "Constructor"
        public UCMediaContentsView()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion
        #region "Property "
        private Visibility _DeleteButtonVisibility = Visibility.Visible;
        public Visibility DeleteButtonVisibility
        {
            get
            {
                return _DeleteButtonVisibility;
            }
            set
            {
                if (value == _DeleteButtonVisibility)
                    return;
                _DeleteButtonVisibility = value;
                this.OnPropertyChanged("DeleteButtonVisibility");
            }
        }
        private String _DeleteTooltip = "";
        public String DeleteTooltip
        {
            get
            {
                return _DeleteTooltip;
            }
            set
            {
                if (value == _DeleteTooltip)
                    return;
                _DeleteTooltip = value;
                this.OnPropertyChanged("DeleteTooltip");
            }
        }
        private ObservableCollection<SingleMediaModel> _SingleMediaItems;
        public ObservableCollection<SingleMediaModel> SingleMediaItems
        {
            get
            {
                return _SingleMediaItems;
            }
            set
            {
                _SingleMediaItems = value;
                this.OnPropertyChanged("SingleMediaItems");
            }
        }

        public Func<int> _OnBackToPrevious = null;
        #endregion "Property "

        #region "public method"
        public void ShowMediaSearchResults(string SearchParam) //search results
        {
            showMorePanel.Visibility = Visibility.Collapsed;
            this.albumModel = null;
            this.hashTagModel = null;
            SingleMediaItems = new ObservableCollection<SingleMediaModel>();
            DeleteButtonVisibility = Visibility.Collapsed;
            this.SearchParam = SearchParam;
            this._OnBackToPrevious = null;
            upperPanel.Visibility = Visibility.Collapsed;
            noTxt.Text = NotificationMessages.NO_MEDIA_FOUND_TEXT;
            searchTxtName.Text = SearchParam;
            //if (BigGIFCtrl != null && BigGIFCtrl.IsRunning()) this.BigGIFCtrl.StopAnimate();
            if (SingleMediaItems.Count > 0)
            {
                noTxtBlock.Visibility = Visibility.Collapsed;
            }
            else noTxtBlock.Visibility = Visibility.Visible;
        }
        public void ShowAlbumContents(MediaContentModel albumModel, Func<int> _OnBackToPrevious)
        {
            RemoveFromAlbum = true;
            showMorePanel.Visibility = Visibility.Collapsed;
            this.albumModel = albumModel;
            this.hashTagModel = null;
            SingleMediaItems = null;
            this.SearchParam = null;
            DeleteButtonVisibility = (albumModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID) ? Visibility.Visible : Visibility.Collapsed;
            this._OnBackToPrevious = _OnBackToPrevious;
            DeleteTooltip = "Delete";
            noTxt.Text = NotificationMessages.NO_MEDIA_FOUND_TEXT;
            searchTxtName.Text = albumModel.AlbumName;
            titleTxt.Text = albumModel.AlbumName;
            upperPanel.Visibility = Visibility.Visible;
            backBtn.Visibility = Visibility.Visible;
            noTxtBlock.Visibility = Visibility.Collapsed;
            if (albumModel.MediaList != null)
            {
                SingleMediaItems = albumModel.MediaList;
                if (albumModel.MediaList.Count > 0)
                {
                    showMorePanel.Visibility = Visibility.Visible;
                    showMoreText.Visibility = Visibility.Visible;
                }
                else noTxtBlock.Visibility = Visibility.Visible;
            }
            else
            {
                albumModel.MediaList = new ObservableCollection<SingleMediaModel>();
                SingleMediaItems = albumModel.MediaList;
                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    this.BigGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                    this.BigGIFCtrl.Visibility = Visibility.Visible;
                    itemsContainer.Visibility = Visibility.Collapsed;
                    showMorePanel.Visibility = Visibility.Collapsed;
                    new Thread(() =>
                    {
                        bool success = false;
                        string message = string.Empty;
                        JObject obj = new JObject();
                        obj[JsonKeys.MediaType] = albumModel.MediaType;
                        obj[JsonKeys.AlbumId] = albumModel.AlbumId;
                        obj[JsonKeys.AlbumName] = albumModel.AlbumName;
                        SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                        SetLoaderTextValue(success);
                    }).Start();
                }
                else
                {
                    showMorePanel.Visibility = Visibility.Visible;
                    showMoreText.Visibility = Visibility.Visible;
                }
            }
        }

        public void SetLoaderTextValue(bool success)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (BigGIFCtrl != null && BigGIFCtrl.IsRunning()) this.BigGIFCtrl.StopAnimate();
                this.BigGIFCtrl.Visibility = Visibility.Collapsed;
                if (success)
                {
                    noTxtBlock.Visibility = Visibility.Collapsed;
                    itemsContainer.Visibility = Visibility.Visible;
                    showMorePanel.Visibility = Visibility.Visible;
                    showMoreText.Visibility = Visibility.Visible;
                }
                else
                {
                    noTxtBlock.Visibility = Visibility.Visible;
                    showMorePanel.Visibility = Visibility.Collapsed;
                    showMoreText.Visibility = Visibility.Collapsed;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public void ShowRecentMedias(Func<int> _OnBackToPrevious) //recent or downloaded int RemoveUIType, 
        {
            showMorePanel.Visibility = Visibility.Collapsed;
            this.albumModel = null;
            this.hashTagModel = null;
            this.SearchParam = null;
            DeleteButtonVisibility = Visibility.Visible;
            upperPanel.Visibility = Visibility.Visible;
            backBtn.Visibility = Visibility.Collapsed;
            this._OnBackToPrevious = _OnBackToPrevious;
            RemoveFromAlbum = false;

            DeleteTooltip = "Remove from Recent History";
            titleTxt.Text = "Recent";
            noTxt.Text = NotificationMessages.NO_RECENT_FOUND_TEXT;
            searchTxtName.Text = "";

            SingleMediaItems = RingIDViewModel.Instance.MyRecentPlayList;
            if (RingIDViewModel.Instance.MyRecentPlayList.Count == 0)
            {
                noTxtBlock.Visibility = Visibility.Visible;
            }
            else
            {
                noTxtBlock.Visibility = Visibility.Collapsed;
            }
        }
        public void ShowHashTagContents(HashTagModel hashTagModel, Func<int> _OnBackToPrevious)
        {
            showMorePanel.Visibility = Visibility.Collapsed;
            this.hashTagModel = hashTagModel;
            this.albumModel = null;
            SingleMediaItems = null;
            this.SearchParam = null;
            DeleteButtonVisibility = Visibility.Collapsed;
            this._OnBackToPrevious = _OnBackToPrevious;
            DeleteTooltip = "Delete";
            noTxt.Text = NotificationMessages.NO_MEDIA_FOUND_TEXT;
            searchTxtName.Text = hashTagModel.HashTagSearchKey;
            titleTxt.Text = hashTagModel.HashTagSearchKey;
            upperPanel.Visibility = Visibility.Visible;
            backBtn.Visibility = Visibility.Visible;
            noTxtBlock.Visibility = Visibility.Collapsed;
            if (hashTagModel.MediaList != null)
            {
                SingleMediaItems = hashTagModel.MediaList;
                if (hashTagModel.MediaList.Count > 0)
                {
                    showMorePanel.Visibility = Visibility.Visible;
                    showMoreText.Visibility = Visibility.Visible;
                }
                else noTxtBlock.Visibility = Visibility.Visible;
            }
            else
            {
                hashTagModel.MediaList = new ObservableCollection<SingleMediaModel>();
                SingleMediaItems = hashTagModel.MediaList;
                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    this.BigGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                    this.BigGIFCtrl.Visibility = Visibility.Visible;
                    itemsContainer.Visibility = Visibility.Collapsed;
                    showMorePanel.Visibility = Visibility.Collapsed;
                    new Thread(() =>
                    {
                        bool success = false;
                        string message = string.Empty;
                        JObject obj = new JObject();
                        obj[JsonKeys.HashTagName] = hashTagModel.HashTagSearchKey;
                        obj[JsonKeys.Scroll] = 2;
                        SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (BigGIFCtrl != null && BigGIFCtrl.IsRunning()) this.BigGIFCtrl.StopAnimate();
                            this.BigGIFCtrl.Visibility = Visibility.Collapsed;
                            if (success)
                            {
                                noTxtBlock.Visibility = Visibility.Collapsed;
                                itemsContainer.Visibility = Visibility.Visible;
                                showMorePanel.Visibility = Visibility.Visible;
                                showMoreText.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                noTxtBlock.Visibility = Visibility.Visible;
                                showMorePanel.Visibility = Visibility.Collapsed;
                                showMoreText.Visibility = Visibility.Collapsed;
                            }
                        });
                    }).Start();
                }
                else
                {
                    showMorePanel.Visibility = Visibility.Visible;
                    showMoreText.Visibility = Visibility.Visible;
                }
            }
        }
        #endregion

        #region "PropertyChanged"
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion        
        
        #region "Event Trigger"
        private void list_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: list_PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        #endregion

        #region "ICommand"
        private ICommand _BackClicked;
        public ICommand BackClicked
        {
            get
            {
                if (_BackClicked == null)
                {
                    _BackClicked = new RelayCommand(param => OnBackClicked());
                }
                return _BackClicked;
            }
        }
        private void OnBackClicked()
        {
            try
            {
                if (_OnBackToPrevious != null) _OnBackToPrevious();
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }

        private ICommand _PlayAllCommand;
        public ICommand PlayAllCommand
        {
            get
            {
                if (_PlayAllCommand == null)
                {
                    _PlayAllCommand = new RelayCommand(param => OnPlayAllCommand());
                }
                return _PlayAllCommand;
            }
        }
        private void OnPlayAllCommand()
        {
            try
            {
                if (SingleMediaItems.Count > 0) MediaUtility.RunPlayList(false, Guid.Empty, SingleMediaItems);
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }

        private ICommand _ShowMorePanelClick;
        public ICommand ShowMorePanelClick
        {
            get
            {
                if (_ShowMorePanelClick == null)
                {
                    _ShowMorePanelClick = new RelayCommand(param => OnShowMorePanelClick());
                }
                return _ShowMorePanelClick;
            }
        }
        private void OnShowMorePanelClick()
        {
            try
            {
                if (albumModel != null && albumModel.MediaList != null)
                {
                    if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                    {
                        showMoreText.Visibility = Visibility.Collapsed;
                        this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
                        new Thread(() =>
                        {
                            bool success = false;
                            string message = string.Empty;
                            JObject obj = new JObject();
                            //obj[JsonKeys.UserTableID] = albumModel.UserTableID;
                            obj[JsonKeys.MediaType] = albumModel.MediaType;
                            obj[JsonKeys.AlbumId] = albumModel.AlbumId;
                            //obj[JsonKeys.StartLimit] = albumModel.MediaList.Count;
                            obj[JsonKeys.AlbumName] = albumModel.AlbumName;
                            //Guid maxContentId = (albumModel.MediaList.Count > 0) ? albumModel.MediaList.Min(item => item.ContentId) : Guid.Empty; //  hashTagModel.MediaList.OrderByDescending(item => item.ContentId).First().ContentId
                            Guid maxContentId = (albumModel.MediaList.Count > 0) ? HelperMethods.GetMaxMinGuidInMediaCollection(albumModel.MediaList, 0, false) : Guid.Empty;
                            obj[JsonKeys.PivotUUID] = maxContentId;
                            obj[JsonKeys.Scroll] = 2;

                            SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                                if (success) showMoreText.Visibility = Visibility.Visible;
                                else showMorePanel.Visibility = Visibility.Collapsed;
                            }, System.Windows.Threading.DispatcherPriority.Send);
                        }).Start();
                    }
                }
                else if (hashTagModel != null && hashTagModel.MediaList != null)
                {
                    if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                    {
                        showMoreText.Visibility = Visibility.Collapsed;
                        this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
                        new Thread(() =>
                        {
                            bool success = false;
                            string message = string.Empty;
                            JObject obj = new JObject();
                            obj[JsonKeys.HashTagName] = hashTagModel.HashTagSearchKey;
                            Guid maxContentId = (hashTagModel.MediaList.Count > 0) ? HelperMethods.GetMaxMinGuidInMediaCollection(hashTagModel.MediaList, 0, false) : Guid.Empty;//hashTagModel.MediaList.Min(item => item.ContentId) : Guid.Empty; //  hashTagModel.MediaList.OrderByDescending(item => item.ContentId).First().ContentId
                            obj[JsonKeys.PivotUUID] = maxContentId;
                            obj[JsonKeys.Scroll] = 2;
                            SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                                if (success) showMoreText.Visibility = Visibility.Visible;
                                else showMorePanel.Visibility = Visibility.Collapsed;
                            }, System.Windows.Threading.DispatcherPriority.Send);
                        }).Start();
                    }
                }
                else if (!string.IsNullOrEmpty(SearchParam))
                {
                    //Guid maxContentId = (SingleMediaItems.Count > 0) ? SingleMediaItems.OrderByDescending(item => item.ContentId).First().ContentId : Guid.Empty;
                    //Guid maxContentId = (SingleMediaItems.Count > 0) ? HelperMethods.GetMaxMinGuidInMediaCollection(SingleMediaItems, 0, false) : Guid.Empty;
                    showMoreText.Visibility = Visibility.Collapsed;
                    GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
                    ThreadMediaCloudItemsSearch thrd = new ThreadMediaCloudItemsSearch();
                    thrd.CallBackEvent += (success) =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (GIFCtrl.IsRunning()) GIFCtrl.StopAnimate();
                            if (success)
                            {
                                showMorePanel.Visibility = Visibility.Visible;
                                showMoreText.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                showMorePanel.Visibility = Visibility.Collapsed;
                                showMoreText.Visibility = Visibility.Collapsed;
                            }
                        });
                    };
                    thrd.StartThread(SearchParam, SettingsConstants.MEDIA_SEARCH_TYPE_SONGS, SingleMediaItems.Count, Guid.Empty);
                    //SendDataToServer.MediaFullSearchByType(UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.SuggestionSelected, SettingsConstants.MEDIA_SEARCH_TYPE_SONGS, 1, maxContentId);
                }
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }
        private ICommand _MediaRightOptionsCommand;
        public ICommand MediaRightOptionsCommand
        {
            get
            {
                if (_MediaRightOptionsCommand == null)
                {
                    _MediaRightOptionsCommand = new RelayCommand(param => OnMediaRightOptionsCommand(param));
                }
                return _MediaRightOptionsCommand;
            }
        }

        public void OnMediaRightOptionsCommand(object parameter)
        {
            Grid grid = (Grid)parameter;
            if (grid.DataContext is SingleMediaModel)
            {
                SingleMediaModel model = (SingleMediaModel)grid.DataContext;

                if (grid.ContextMenu != ContextMenuMediaOptions.Instance.cntxMenu)
                {
                    grid.ContextMenu = ContextMenuMediaOptions.Instance.cntxMenu;
                    ContextMenuMediaOptions.Instance.SetupMenuItem();
                }

                if (model.MediaOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                    type = 1;
                else if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.IsVisible
                    && UCMeidaCloudDownloadPopup.Instance != null
                    && UCMeidaCloudDownloadPopup.Instance.IsVisible)
                    type = 2;
                else if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.IsVisible
                    && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.tabContainerBorder.IsVisible
                    && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.TabType == 4)
                    type = 3;
                else
                    type = -1;

                ContextMenuMediaOptions.Instance.ShowHandler(grid, model, type);
                ContextMenuMediaOptions.Instance.cntxMenu.IsOpen = true;
            }
        }

        private ICommand _MediaPlayCommand;
        public ICommand MediaPlayCommand
        {
            get
            {
                if (_MediaPlayCommand == null)
                {
                    _MediaPlayCommand = new RelayCommand(param => OnMediaPlayCommand(param));
                }
                return _MediaPlayCommand;
            }
        }

        public void OnMediaPlayCommand(object parameter)
        {
            if (parameter is SingleMediaModel)
            {
                SingleMediaModel mediaToPlay = (SingleMediaModel)parameter;
                int idx = SingleMediaItems.IndexOf(mediaToPlay);
                if (idx >= 0 && idx < SingleMediaItems.Count)
                {
                    //RingPlayerViewModel.Instance.NavigateFrom = "album";
                    MediaUtility.RunPlayList(false, Guid.Empty, SingleMediaItems, null, idx);
                }

                //if (_OnBackToPrevious != null)
                //{
                //    _OnBackToPrevious();
                //}
            }
        }

        private ICommand _RemoveCommand;
        public ICommand RemoveCommand
        {
            get
            {
                if (_RemoveCommand == null)
                {
                    _RemoveCommand = new RelayCommand(param => removeBtnClick(param));
                }
                return _RemoveCommand;
            }
        }
        private void removeBtnClick(object parameter)
        {
            if (parameter is SingleMediaModel && SingleMediaItems != null && SingleMediaItems.Count > 0)
            {
                SingleMediaModel modelToRemove = (SingleMediaModel)parameter;
                if (RemoveFromAlbum)
                {
                    bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this Media"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.DELETE_MEDIA_MESSAGE_TITLE));
                    if (isTrue)
                    {
                        ThreadDeleteMediaOrAlbum thrd = new ThreadDeleteMediaOrAlbum(modelToRemove.ContentId, modelToRemove.AlbumId, modelToRemove.MediaType);
                        thrd.callBackEvent += (success) =>
                        {
                            if (success == SettingsConstants.RESPONSE_SUCCESS)
                            {
                                if (albumModel != null) albumModel.TotalMediaCount -= 1;
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    SingleMediaItems.Remove(modelToRemove);
                                    if (SingleMediaItems.Count == 0)
                                        noTxtBlock.Visibility = Visibility.Visible;
                                });
                                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.DELETE_MEDIA_SUCCESS_MESSAGE);
                            }
                            else
                            {
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    UIHelperMethods.ShowWarning(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Remove media");
                                    // CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                                });
                            }
                        };
                        thrd.StartThread();
                    }
                }
                else
                {
                    SingleMediaItems.Remove(modelToRemove);
                    Models.DAO.MediaDAO.Instance.DeleteFromRecentMediasTable(modelToRemove.ContentId);
                }
            }
        }
        #endregion
    }
}

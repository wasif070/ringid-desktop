<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;

namespace View.UI.SocialMedia
{
    /// <summary>
    /// Interaction logic for UCYoutubeViewer.xaml
    /// </summary>
    public partial class UCYoutubeViewer : PopUp.PopUpBaseControl, INotifyPropertyChanged
    {
        private string youtubeID = string.Empty;
        public UCYoutubeViewer(string youtubeVideoID)
        {
            InitializeComponent();
            this.DataContext = this;
            youtubeID = youtubeVideoID;
            if (_Browser == null) _Browser = new System.Windows.Forms.WebBrowser();
            _Browser.ScriptErrorsSuppressed = true;
            _Browser.Navigating += _Browser_Navigating;            
        }

        void _Browser_Navigating(object sender, System.Windows.Forms.WebBrowserNavigatingEventArgs e)
        {
            if (e.Url.AbsoluteUri.Contains("embed"))
            {
                PleaseWaitLoader = false;
            }
        }

        private void _Browser_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (e.Uri.AbsoluteUri.Contains("embed"))
            {
                PleaseWaitLoader = false;
            }
        }

        private void setAndGoWebbrowser(string urlNav = null)
        {
            try
            {
                //BrowserVisibility = Visibility.Visible;
                if (urlNav == null) urlNav = string.Format(View.Constants.SocialMediaConstants.YOUTUBE_EMBEDDING_URL, youtubeID);
                _Browser.Navigate(new Uri(urlNav).AbsoluteUri);
            }
            catch (Exception)
            {
                //log.Error(e.Message + "\n" + e.StackTrace + e.Message);
            }
        }

        private void disposeWebbrowser()
        {
            if (_Browser != null)
            {
                _Browser.Navigating -= _Browser_Navigating;
                _Browser.Dispose();
                _Browser = null;
            }
        }

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            ThreadStart start = delegate { setAndGoWebbrowser(); };
            new Thread(start).Start();
        }

        private ICommand escapeCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapeCommand == null) escapeCommand = new RelayCommand(param => OnEscapeCommand(param));
                return escapeCommand;
            }
        }
        private void OnEscapeCommand(object param)
        {
            PleaseWaitLoader = false;
            try
            {
                disposeWebbrowser();
                Hide();
            }
            catch (Exception ex) { /*log.Error(ex.StackTrace); */}
        }

        private bool pleaseWaitLoader = true;
        public bool PleaseWaitLoader
        {
            get { return this.pleaseWaitLoader; }
            set { pleaseWaitLoader = value; this.OnPropertyChanged("PleaseWaitLoader"); }
        }

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;

namespace View.UI.SocialMedia
{
    /// <summary>
    /// Interaction logic for UCYoutubeViewer.xaml
    /// </summary>
    public partial class UCYoutubeViewer : PopUp.PopUpBaseControl, INotifyPropertyChanged
    {
        private string youtubeID = string.Empty;
        public UCYoutubeViewer(string youtubeVideoID)
        {
            InitializeComponent();
            this.DataContext = this;
            youtubeID = youtubeVideoID;
            if (_Browser == null) _Browser = new System.Windows.Forms.WebBrowser();
            _Browser.ScriptErrorsSuppressed = true;
            _Browser.Navigating += _Browser_Navigating;            
        }

        void _Browser_Navigating(object sender, System.Windows.Forms.WebBrowserNavigatingEventArgs e)
        {
            if (e.Url.AbsoluteUri.Contains("embed"))
            {
                PleaseWaitLoader = false;
            }
        }

        private void setAndGoWebbrowser(string urlNav = null)
        {
            try
            {
                //BrowserVisibility = Visibility.Visible;
                if (urlNav == null) urlNav = string.Format(View.Constants.SocialMediaConstants.YOUTUBE_EMBEDDING_URL, youtubeID);
                _Browser.Navigate(new Uri(urlNav).AbsoluteUri);
            }
            catch (Exception)
            {
                //log.Error(e.Message + "\n" + e.StackTrace + e.Message);
            }
        }

        private void disposeWebbrowser()
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                if (_Browser != null)
                {
                    _Browser.Navigating -= _Browser_Navigating;
                    _Browser.Dispose();
                    _Browser = null;
                }
            });
        }

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            PleaseWaitLoader = true;
            this.Focusable = true;
            Keyboard.Focus(this);
            ThreadStart start = delegate { setAndGoWebbrowser(); };
            new Thread(start).Start();
        }

        private ICommand escapeCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapeCommand == null) escapeCommand = new RelayCommand(param => OnEscapeCommand(param));
                return escapeCommand;
            }
        }
        private void OnEscapeCommand(object param)
        {
            PleaseWaitLoader = false;
            try
            {
                new Thread(new ThreadStart(delegate { disposeWebbrowser(); })).Start();
                //disposeWebbrowser();
                Hide();
            }
            catch (Exception) { /*log.Error(ex.StackTrace); */}
        }

        private bool pleaseWaitLoader = true;
        public bool PleaseWaitLoader
        {
            get { return this.pleaseWaitLoader; }
            set { pleaseWaitLoader = value; this.OnPropertyChanged("PleaseWaitLoader"); }
        }        

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using View.Constants;
using View.UI.PopUp;
using View.Utility;

namespace View.UI.SocialMedia
{
    /// <summary>
    /// Interaction logic for UCFBShare.xaml
    /// </summary>
    public partial class UCFBShare : PopUpBaseControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCFBShare).Name);
        //public delegate void FBAuthenticationCompleted(int typeToSwitch, string facebookName, string facebookProfileImage, string fbID, string accessToken, string ringID = null, string messag = null);
        //public event FBAuthenticationCompleted OnFBAuthenticationCompleted;
        public event DelegateBoolStringString OnClosed;
        private string mediaLinkToShare;
        private string keyAccessToken = "#access_token=";
        private string keyErrorCode = "error_code";
        private string keyClose = "close";
        private string keyPostID = "post_id";
        private bool isSuccess = false;
        private string errorMsg = string.Empty;
        private string cancelMessage = string.Empty;
        string userCancelled = "User cancelled";
        string errorCancelled = "Error cancelled";
        private bool isCancelledShare = false;
        private bool sendAuthRequestForShare = true;
        private int contentTypeToshare;
        #endregion

        #region"Ctors"
        public UCFBShare(string mediaLinkToShare, int contentTypeToshare, bool sendAuthRequestForShare)
        {
            InitializeComponent();
            this.DataContext = this;
            if (_Browser == null) _Browser = new System.Windows.Forms.WebBrowser();
            _Browser.ScriptErrorsSuppressed = true;
            _Browser.Navigating += Browser_Navigating;
            _Browser.Navigated += Browser_OnNavigated;
            this.mediaLinkToShare = mediaLinkToShare;
            this.contentTypeToshare = contentTypeToshare;
            this.sendAuthRequestForShare = sendAuthRequestForShare;
        }
        #endregion"Ctors"

        #region "Properties"

        private string title = "Facebook share!";
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }

        private string successOrFailedMsg = "Media shared successfully!";
        public string SuccessOrFailedMsg
        {
            get { return successOrFailedMsg; }
            set { successOrFailedMsg = value; OnPropertyChanged("SuccessOrFailedMsg"); }
        }

        private bool pleaseWaitLoader = true;
        public bool PleaseWaitLoader
        {
            get { return this.pleaseWaitLoader; }
            set
            {
                if (this.pleaseWaitLoader == value) return;
                this.pleaseWaitLoader = value;
                if (value)
                {
                    BrowserVisibility = Visibility.Collapsed;
                    SuccessfullScreenVisibility = Visibility.Collapsed;
                }
                this.OnPropertyChanged("PleaseWaitLoader");
            }
        }

        private Visibility browserVisibility = Visibility.Collapsed;
        public Visibility BrowserVisibility
        {
            get { return browserVisibility; }
            set
            {
                browserVisibility = value;
                if (value == Visibility.Visible)
                {
                    PleaseWaitLoader = false;
                    SuccessfullScreenVisibility = Visibility.Collapsed;
                }
                OnPropertyChanged("BrowserVisibility");
            }
        }

        private Visibility successfullScreenVisibility = Visibility.Collapsed;
        public Visibility SuccessfullScreenVisibility
        {
            get { return successfullScreenVisibility; }
            set
            {
                successfullScreenVisibility = value;
                if (value == Visibility.Visible)
                {
                    BrowserVisibility = Visibility.Collapsed;
                    PleaseWaitLoader = false;
                }
                OnPropertyChanged("SuccessfullScreenVisibility");
            }
        }

        private string InviteString
        {
            get { return string.Format("Hi! Join me in ringID for live conversations, file sharing, free voice & video calls and more. Enter this {0} as your referral code to sign up.", Models.Constants.DefaultSettings.userProfile.RingID.ToString().Substring(2).Insert(4, " ")); }
        }
        private string OAuthURI
        {
            get { return string.Format("{0}client_id={1}&redirect_uri={2}&response_type=token&type=user_agent&display=popup", SocialMediaConstants.FACEBOOK_OAuthUrl, SocialMediaConstants.FACEBOOK_CLIENT_ID, SocialMediaConstants.FACEBOOK_SUCCESS_LINK); }
        }

        private string FeedDialogURI
        {
            get { return string.Format("{0}app_id={1}&display_popup&link={2}&caption={3}", SocialMediaConstants.FACEBOOK_FeedDialogUrl, SocialMediaConstants.FACEBOOK_CLIENT_ID, mediaLinkToShare, InviteString); }
        }

        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            ThreadStart start = delegate { setAndGoWebbrowser(); };
            new Thread(start).Start();
        }

        private ICommand escapeCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapeCommand == null) escapeCommand = new RelayCommand(param => OnEscapeCommand(param));
                return escapeCommand;
            }
        }
        private void OnEscapeCommand(object param)
        {
            PleaseWaitLoader = false;
            try
            {
                isCancelledShare = true;
                disposeWebbrowser();
                Hide();
            }
            catch (Exception ex) { log.Error(ex.StackTrace); }
        }

        private ICommand shareAgainCommand;
        public ICommand ShareAgainCommand
        {
            get
            {
                if (shareAgainCommand == null) shareAgainCommand = new RelayCommand(param => OnShareAgainCommand());
                return shareAgainCommand;
            }
        }
        private void OnShareAgainCommand()
        {
            try
            {
                setAndGoWebbrowser();
            }
            catch (Exception ex) { log.Error(ex.StackTrace); }
        }

        #endregion"ICommands and Command Methods"

        #region "Utility Methods"

        private void setAndGoWebbrowser(string urlNav = null)
        {
            try
            {
                BrowserVisibility = Visibility.Visible;
                if (urlNav == null) urlNav = OAuthURI;
                _Browser.Navigate(new Uri(urlNav).AbsoluteUri);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + e.Message);
            }
        }

        private void disposeWebbrowser()
        {
            if (_Browser != null)
            {
                _Browser.Navigated -= Browser_OnNavigated;
                _Browser.Navigating -= Browser_Navigating;
                _Browser.Dispose();
                _Browser = null;
            }
        }

        private void showFacebookError()
        {
            if (OnClosed != null) OnClosed(isSuccess, errorMsg, cancelMessage);
        }

        private string runShareMediaOnFacebook()
        {
            string message = string.Empty;
            try
            {
                string pakID = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SHARE_TO_SOCIAL_MEDIA;
                int idx = mediaLinkToShare.IndexOf('=') + 1;
                pakToSend[WalletConstants.REQ_KEY_ENCRYPTED_CONTENT_ID] = mediaLinkToShare.Substring(idx);
                pakToSend[JsonKeys.MediaType] = contentTypeToshare;//2;
                pakToSend[WalletConstants.REQ_KEY_SOCIAL_MEDIA_TYPE] = SocialMediaConstants.SOCIAL_MEDIA_FACEBOOK;//1;

                try
                {
                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
                    Thread.Sleep(25);
                    for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    {
                        if (isCancelledShare) break;
                        Thread.Sleep(DefaultSettings.WAITING_TIME);
                        if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakID))
                        {
                            if (i % DefaultSettings.SEND_INTERVAL == 0) SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
                        }
                        else
                        {
                            JObject feedbackFields = null;
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                            if (feedbackFields != null)
                            {
                                if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success]) message = "Media shared successfully!";
                                else message = string.IsNullOrEmpty((string)feedbackFields[JsonKeys.Message]) ? "Faild to share ! Try Again later." : (string)feedbackFields[JsonKeys.Message];
                            }
                            else
                                message = "Faild to share ! Try Again later.";
                            break;
                        }
                    }
                }
                catch (Exception e) { log.Error("runShareMediaOnFacebook ==>" + e.StackTrace); }
            }
            catch (Exception ex)
            {
                message = string.Format("Exception occured ==> {1} {0} {2}", Environment.NewLine, ex.Message, ex.StackTrace);
            }
            return message;
        }
        #endregion

        #region"Event Triggers"
        private void Browser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            try
            {
                string urlNav = e.Url.ToString();
#if HTTP_LOG
                log.Info("FACEBOOK NAVIGATING::>" + urlNav);
#endif
                if (e.Url.AbsoluteUri.Contains(keyErrorCode))
                {
                    e.Cancel = true;
                    cancelMessage = userCancelled;
                    OnEscapeCommand(null);
                }
                else if (e.Url.AbsoluteUri.Contains(keyClose) && e.Url.AbsoluteUri.Contains(keyPostID))
                {
                    e.Cancel = true;
                    if (e.Url.AbsoluteUri.Contains(keyPostID))
                    {
                        if (sendAuthRequestForShare)
                        {
                            PleaseWaitLoader = true;
                            Task sendAuthRequest = new Task(() =>
                            {
                                isCancelledShare = false;
                                string successMessage = runShareMediaOnFacebook();
                                SuccessfullScreenVisibility = Visibility.Visible;
                                isSuccess = true;
                                SuccessOrFailedMsg = successMessage;
                                SuccessfullScreenVisibility = Visibility.Visible;
                            });
                            sendAuthRequest.Start();
                        }
                        else
                        {
                            SuccessfullScreenVisibility = Visibility.Visible;
                            isSuccess = true;
                            SuccessOrFailedMsg = "Media shared successfully!";
                            SuccessfullScreenVisibility = Visibility.Visible;
                        }
                    }
                    else if (e.Url.AbsoluteUri.Contains(keyErrorCode))
                    {
                        SuccessfullScreenVisibility = Visibility.Visible;
                        isSuccess = false;
                        successOrFailedMsg = "Can not Share this media!";
                        SuccessfullScreenVisibility = Visibility.Visible;
                    }
                }
                else if (urlNav.Contains("ieframe.dll/navcancl"))
                {
                    e.Cancel = true;
                    cancelMessage = errorCancelled;
                    OnEscapeCommand(null);
                }
                else if (urlNav.Contains("/recover/initiate") || urlNav.Contains("/r.php"))   //forgot password or sign up for an account
                {
                    e.Cancel = true;
                    cancelMessage = errorCancelled;
                    HelperMethods.GoToSite(urlNav);
                }
                else if (urlNav.Contains("&error_reason=user_denied#"))
                {
                    e.Cancel = true;
                    cancelMessage = errorCancelled;
                    OnEscapeCommand(null);
                }
            }
            catch (Exception ex)
            {
                OnEscapeCommand(null);
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        private void Browser_OnNavigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            try
            {
                //PleaseWaitLoader = false;
                //   BrowserVisibility = Visibility.Visible;
                string urlNav = e.Url.ToString();
#if HTTP_LOG
                log.Error("NAVIGATED::>" + urlNav);
#endif
                if (urlNav.Contains(keyAccessToken)) //success page
                {
                    int idx = e.Url.AbsoluteUri.IndexOf(keyAccessToken);
                    string accessToken = e.Url.AbsoluteUri.Substring(idx + keyAccessToken.Length).Split('&')[0];
                    setAndGoWebbrowser(FeedDialogURI);
                }
                //else if (e.Url.AbsoluteUri.Contains(keyClose))
                //{
                //    if (e.Url.AbsoluteUri.Contains(keyPostID))
                //    {
                //        SuccessfullScreenVisibility = Visibility.Visible;
                //        isSuccess = true;
                //        successOrFailedMsg = "Media shared successfully!";
                //        SuccessfullScreenVisibility = Visibility.Visible;
                //        //View.Utility.UIHelperMethods.ShowMessageWithTimerFromNonThread(UI.UCGuiRingID.Instance.MotherPanel, "Media shared successfully!");
                //        //  new Utility.Wallet.ThreadWalletDailyUtilities().StartMediaShareOnFacebook();
                //    }
                //    else if (e.Url.AbsoluteUri.Contains(keyErrorCode))
                //    {
                //        SuccessfullScreenVisibility = Visibility.Visible;
                //        isSuccess = false;
                //        successOrFailedMsg = "Can not Share this media!";
                //        SuccessfullScreenVisibility = Visibility.Visible;
                //    }
                //}
                else
                {
                    //if (_Browser != null)
                    //{
                    //    PleaseWaitLoader = false;
                    //    _FormContainer.Visibility = Visibility.Visible;
                    //    _Browser.Visible = true;
                    //}
                }
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }
        #endregion"Event Triggers"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
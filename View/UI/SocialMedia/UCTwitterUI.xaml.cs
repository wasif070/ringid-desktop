﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Auth.Service.SigninSignup;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Auth;
using View.Utility.SignInSignUP;
using View.Utility.WPFMessageBox;

namespace View.UI.SocialMedia
{
    /// <summary>
    /// Interaction logic for UCTwitterUI.xaml
    /// </summary>
    public partial class UCTwitterUI : PopUpBaseControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCTwitterUI).Name);
        public int CurrentUIType;
        OAuthHelper oah = null;
        private string TWITTER_AUTHORIZE_URL = "https://twitter.com/oauth/authorize";
        private string TWITTER_LOGOUT_URL = "https://twitter.com/logout";
        private string RINGID_DENID = "https://www.ringid.com/?denied";
        private string JAVASCRIPT_ERROR = "javascript:false";//about:blank
        private string ERROR_BLANK = "about:blank";

        public delegate void ThreadCompleted(int typeToSwitch, string screenName, string profileImage, string socialMediaID, string inputToken, string ringID = null, string messag = null);
        public event ThreadCompleted OnThreadCompleted;
        private BackgroundWorker bgworker;
        #endregion

        #region"Ctors"
        public UCTwitterUI(int currentUIType1)
        {
            InitializeComponent();
            this.DataContext = this;
            this.CurrentUIType = currentUIType1;
        }
        #endregion"Ctors"

        #region "Properties"
        private string title = "Welcome to Twitter";
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }

        private bool pleaseWaitLoader = true;
        public bool PleaseWaitLoader
        {
            get { return this.pleaseWaitLoader; }
            set
            {
                if (this.pleaseWaitLoader == value) return;
                this.pleaseWaitLoader = value; this.OnPropertyChanged("PleaseWaitLoader");
            }
        }

        private string GetDefaultTwitterUrl
        {
            get { return TWITTER_AUTHORIZE_URL + "?oauth_token="; }
        }
        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            ThreadStart start = delegate { setAndGoWebbrowser(); };
            new Thread(start).Start();
        }

        private ICommand escapeCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapeCommand == null) escapeCommand = new RelayCommand(param => OnEscapeCommand(param));
                return escapeCommand;
            }
        }
        private void OnEscapeCommand(object param)
        {
            PleaseWaitLoader = false;
            disposeWebbrowser();
            Hide();
        }

        #endregion"ICommands and Command Methods"

        #region "Utility Methods"

        private void setAndGoWebbrowser(string urlNav = null)
        {
            try
            {
                oah = new OAuthHelper();
                oah.GetRequestToken();
                _Browser.Navigating += Browser_Navigating;
                if (urlNav == null) urlNav = GetDefaultTwitterUrl + oah.oauth_request_token;
                _Browser.Navigate(new Uri(urlNav).AbsoluteUri);
            }
            catch (Exception e) { log.Error(e.Message + "\n" + e.StackTrace + e.Message); }
        }

        private void disposeWebbrowser()
        {
            if (_Browser != null)
            {
                _Browser.Navigating -= Browser_Navigating;
                _Browser = null;
            }
            if (bgworker != null && bgworker.IsBusy) bgworker.CancelAsync();
        }

        private void SwitchBacktoCallerUI(string fromString, bool CloseFromException = false)
        {
            try
            {
                if (CloseFromException) disposeWebbrowser();
                OnEscapeCommand(null);
                if (CloseFromException) UIHelperMethods.ShowWarning("Failed to Navigate! Please try later!", "Twitter navigations");
                log.Error("SwitchBacktoCallerUI from " + fromString);
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }

        private bool TwCancelBtnHTMLElement_onclick()
        {
            try
            {
                OnEscapeCommand(null);
                SwitchBacktoCallerUI("TwCancelBtnHTMLElement_onclick");
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
            return true;
        }

        private void VisibleBrowser()
        {
            PleaseWaitLoader = false;
            if (_FormContainer.Visibility != Visibility.Visible)
                _FormContainer.Visibility = Visibility.Visible;
        }
        #endregion

        #region"Event Triggers"
        private void Browser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            try
            {
                string urlNav = e.Url.ToString();
                log.Error("TW NAVIGATING::>" + urlNav);
                if (urlNav.Contains("ieframe.dll/navcancl"))
                {
                    e.Cancel = true; OnEscapeCommand(null);
                }
                else if (urlNav.Contains("twitter.com/signup") || urlNav.Contains("twitter.com/home"))  //home or sign up for an account
                {
                    e.Cancel = true; HelperMethods.GoToSite(urlNav, true);
                }
                else if (urlNav.Contains(JAVASCRIPT_ERROR)) e.Cancel = true;
                else if (urlNav.Contains("oauth_verifier"))
                {
                    e.Cancel = true;
                    string verifier = urlNav.Split('&')[1].Replace("oauth_verifier=", "");
                    oah.GetUserTwAccessToken(oah.oauth_request_token, verifier);
                    string InputToken = oah.Verify_CredentialsandReturnInputToken();
                    _FormContainer.Visibility = Visibility.Collapsed;
                    PleaseWaitLoader = true;
                    verifyTwitterWithAuth(InputToken);
                }
                else if (urlNav.StartsWith(TWITTER_LOGOUT_URL)) e.Cancel = true;
                else if (!urlNav.StartsWith(TWITTER_AUTHORIZE_URL) && urlNav.Contains("twitter.com/login/error"))
                {
                    e.Cancel = true;
                    UIHelperMethods.ShowWarning("Wrong Username password Combination! Please try again", "Twitter navigations");
                }
                else if (urlNav.Contains(RINGID_DENID)) e.Cancel = true;
                else if (urlNav.Contains(ERROR_BLANK)) e.Cancel = true;
                else if (urlNav.Contains(GetDefaultTwitterUrl)) VisibleBrowser();
                //else if (urlNav.Contains(TWITTER_AUTHORIZE_URL)) e.Cancel = true;
            }
            catch (Exception ex)
            {
                SwitchBacktoCallerUI("Exception in navigating" + e.Url.ToString(), true);
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }
        #endregion"Event Triggers"

        #region"Utility Methods"

        private void navigateToTwitterHome()
        {
            string homePage = GetDefaultTwitterUrl + oah.oauth_request_token;
            _Browser.Navigate(new Uri(homePage).AbsoluteUri);
        }

        private void verifyTwitterWithAuth(string InputToken)
        {
            int TypeToSwitch = 0; //100 to download latest version "Download the new version of ringID",200 to Success
            if (!string.IsNullOrEmpty(InputToken))
            {
                string MessageToShow = null, ringIdForthisTw = null;
                if (CurrentUIType == SettingsConstants.TYPE_FROM_SIGNUP) TypeToSwitch = WelcomePanelConstants.TypeSignup;
                else if (CurrentUIType == SettingsConstants.TYPE_FROM_SIGNIN) TypeToSwitch = WelcomePanelConstants.TypeSignin;
                else if (CurrentUIType == SettingsConstants.TYPE_FROM_RECOVERY) TypeToSwitch = WelcomePanelConstants.TypeForgotPassword;
                else if (CurrentUIType == SettingsConstants.TYPE_FROM_PROFILE) TypeToSwitch = WelcomePanelConstants.TypeProfile;
                if (bgworker == null)
                {
                    bgworker = new BackgroundWorker();
                    bgworker.WorkerSupportsCancellation = true;
                    bgworker.DoWork += (s, ee) =>
                    {
                        SocialMediaHelper helper = new SocialMediaHelper(SettingsConstants.TWITTER_LOGIN, CurrentUIType, oah.user_id, InputToken);
                        TypeToSwitch = helper.ProcessMedia();
                        ringIdForthisTw = helper.RingID;
                        MessageToShow = helper.MessageToShow;
                        ee.Result = null;
                    };
                    bgworker.RunWorkerCompleted += (s, ee) =>
                    {
                        if (ee.Result == null)
                        {
                            if (TypeToSwitch == 100) HelperMethods.DownloadMandatoryUpdater();
                            else if (OnThreadCompleted != null) OnThreadCompleted(TypeToSwitch, oah.screen_name, oah.profile_img_url, oah.user_id, InputToken, ringIdForthisTw, MessageToShow);
                        }
                        OnEscapeCommand(null);
                    };
                }
                bgworker.RunWorkerAsync();
            }
            else
            {
                UIHelperMethods.ShowWarning("Failed to Navigate! Please try later!", "Twitter navigation");
                if (OnThreadCompleted != null) OnThreadCompleted(TypeToSwitch, null, null, null, null);
                OnEscapeCommand(null);
            }
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
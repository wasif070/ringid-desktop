﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Auth.Service.SigninSignup;
using Auth.utility;
using Facebook;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Auth;
using View.Utility.SignInSignUP;

namespace View.UI.SocialMedia
{
    /// <summary>
    /// Interaction logic for UCFacebookAuthentication.xaml
    /// 
    /// It is Strongly Recommanded to use WebBrowser of System.Windows.Forms;
    /// Becase ScriptErrorsSuppressed not supported here
    /// </summary>
    public partial class UCFBAuthentication : PopUpBaseControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCFBAuthentication).Name);
        private BackgroundWorker bgworker;
        private string keyAccessToken = "#access_token=";
        //private string FACEBOOK_SUCCESS_LINK = "www.facebook.com/connect/login_success.html";
        //private string FACEBOOK_GRAPH_URL = "https://graph.facebook.com/";
        //private string FACEBOOK_APPLICATION_SECRTE = "6f6fef8701d4b084b5151092ea74bea9";
        public int CurrentUIType;
        public delegate void FBAuthenticationCompleted(int typeToSwitch, string facebookName, string facebookProfileImage, string fbID, string accessToken, string ringID = null, string messag = null);
        public event FBAuthenticationCompleted OnFBAuthenticationCompleted;
        #endregion

        #region"Ctors"
        public UCFBAuthentication(int currentUIType)
        {
            InitializeComponent();
            this.DataContext = this;
            this.CurrentUIType = currentUIType;
        }
        #endregion"Ctors"

        #region "Properties"

        private string title = "Facebook Authentication!";
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }

        private bool pleaseWaitLoader = true;
        public bool PleaseWaitLoader
        {
            get { return this.pleaseWaitLoader; }
            set
            {
                if (this.pleaseWaitLoader == value) return;
                this.pleaseWaitLoader = value; this.OnPropertyChanged("PleaseWaitLoader");
            }
        }

        private Visibility browserVisibility = Visibility.Collapsed;
        public Visibility BrowserVisibility
        {
            get { return browserVisibility; }
            set { browserVisibility = value; OnPropertyChanged("BrowserVisibility"); }
        }

        private string GetDefaultFacebookUrl
        {
            get { return "https://www.facebook.com/dialog/oauth?client_id=" + SocialMediaConstants.FACEBOOK_CLIENT_ID + "&redirect_uri=https://" + SocialMediaConstants.FACEBOOK_SUCCESS_LINK + "&type=user_agent&display=popup"; }
        }

        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            ThreadStart start = delegate { setAndGoWebbrowser(); };
            new Thread(start).Start();
        }

        private ICommand escapeCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapeCommand == null) escapeCommand = new RelayCommand(param => OnEscapeCommand(param));
                return escapeCommand;
            }
        }
        private void OnEscapeCommand(object param)
        {
            PleaseWaitLoader = false;
            try
            {
                disposeWebbrowser();
                Hide();
            }
            catch (Exception ex) { log.Error(ex.StackTrace); }
        }

        #endregion"ICommands and Command Methods"

        #region "Utility Methods"
        private void setAndGoWebbrowser(string urlNav = null)
        {
            try
            {
                if (_Browser == null) _Browser = new WebBrowser();
                _Browser.ScriptErrorsSuppressed = true;
                _Browser.Navigating += Browser_Navigating;
                _Browser.Navigated += Browser_OnNavigated;
                if (urlNav == null) urlNav = GetDefaultFacebookUrl;
                _Browser.Navigate(new Uri(urlNav).AbsoluteUri);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + e.Message);
            }
        }

        private void disposeWebbrowser()
        {
            if (bgworker != null && bgworker.IsBusy) bgworker.CancelAsync();
            if (_Browser != null)
            {
                _Browser.Navigated -= Browser_OnNavigated;
                _Browser.Navigating -= Browser_Navigating;
                _Browser.Dispose();
                _Browser = null;
            }
            //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 255");
        }


        internal string Facebook_appsecret_proof(string content, string key)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            byte[] messageBytes = Encoding.UTF8.GetBytes(content);
            byte[] hash;
            using (HMACSHA256 hmacsha256 = new HMACSHA256(keyBytes))
            {
                hash = hmacsha256.ComputeHash(messageBytes);
            }

            StringBuilder sbHash = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sbHash.Append(hash[i].ToString("x2"));
            }
            return sbHash.ToString();
        }

        private void showFacebookError()
        {
            if (OnFBAuthenticationCompleted != null) OnFBAuthenticationCompleted(0, null, null, null, null, null, "Facebook error!");
        }
        #endregion

        #region"Event Triggers"
        private void Browser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            try
            {
                string urlNav = e.Url.ToString();
#if FEED_LOG
                log.Error("FACEBOOK NAVIGATING::>" + urlNav);
                log.Info("CurrentUIType==>" + CurrentUIType);
#endif
                if (urlNav.Contains("ieframe.dll/navcancl"))
                {
                    e.Cancel = true;
                    OnEscapeCommand(null);
                }
                else if (urlNav.Contains("/recover/initiate") || urlNav.Contains("/r.php"))   //forgot password or sign up for an account
                {
                    e.Cancel = true;
                    HelperMethods.GoToSite(urlNav);
                }
                else if (urlNav.Contains("&error_reason=user_denied#"))
                {
                    e.Cancel = true;
                    OnEscapeCommand(null);
                }
            }
            catch (Exception ex)
            {
                OnEscapeCommand(null);
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        private void Browser_OnNavigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            try
            {
                PleaseWaitLoader = false;
                BrowserVisibility = Visibility.Visible;
                string urlNav = e.Url.ToString();
#if HTTP_LOG
                log.Error("NAVIGATED::>" + urlNav);
#endif
                if (urlNav.Contains(keyAccessToken)) //success page
                {
                    int i = urlNav.IndexOf(keyAccessToken);
                    string AccessToken = urlNav.Substring(i + 14);
                    AccessToken = AccessToken.Split('&')[0];
#if HTTP_LOG
                    log.Info("access_token::>" + AccessToken);
#endif
                    //    disposeWebbrowser();
                    string MessageToShow = null, fid = string.Empty, fprUrl = string.Empty, fnm = string.Empty, ringIdForthisfb = null;
                    int TypeToSwitch = 0; //100 to download latest version "Download the new version of ringID",200 to Success
                    if (CurrentUIType == SettingsConstants.TYPE_FROM_SIGNUP) { TypeToSwitch = WelcomePanelConstants.TypeSignup; }
                    else if (CurrentUIType == SettingsConstants.TYPE_FROM_SIGNIN) { TypeToSwitch = WelcomePanelConstants.TypeSignin; }
                    else if (CurrentUIType == SettingsConstants.TYPE_FROM_RECOVERY) { TypeToSwitch = WelcomePanelConstants.TypeForgotPassword; }
                    else if (CurrentUIType == SettingsConstants.TYPE_FROM_PROFILE) TypeToSwitch = WelcomePanelConstants.TypeProfile;
                    if (bgworker == null)
                    {
                        bgworker = new BackgroundWorker();
                        bgworker.WorkerSupportsCancellation = true;
                        bgworker.WorkerReportsProgress = true;
                        bgworker.DoWork += (s, ee) =>
                        {
                            try
                            {
                                BrowserVisibility = Visibility.Collapsed;
                                PleaseWaitLoader = true;
                                var myfbclient = new FacebookClient(AccessToken);
                                var facebookAppSecretProof = Facebook_appsecret_proof(AccessToken, SocialMediaConstants.FACEBOOK_APPLICATION_SECRTE);
                                dynamic facebookInfo = myfbclient.Get(string.Format("/me?appsecret_proof={0}&fields=id,name,first_name,last_name,email,birthday,gender", facebookAppSecretProof));
                                if (!string.IsNullOrEmpty(facebookInfo.id))
                                {
                                    fid = facebookInfo.id;
                                    fnm = facebookInfo.name;
                                    fprUrl = string.Format("{0}{1}" + "/picture?type=large&appsecret_proof={2}", SocialMediaConstants.FACEBOOK_GRAPH_URL, fid, facebookAppSecretProof);
                                    SocialMediaHelper helper = new SocialMediaHelper(SettingsConstants.FACEBOOK_LOGIN, CurrentUIType, fid, AccessToken);
                                    TypeToSwitch = helper.ProcessMedia();
                                    ringIdForthisfb = helper.RingID;
                                    MessageToShow = helper.MessageToShow;
                                    ee.Result = null;
                                }
                                else showFacebookError();
                            }
                            catch (Exception ex)
                            {
                                showFacebookError();
                                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                            }
                        };
                        bgworker.RunWorkerCompleted += (s, ee) =>
                        {
                            try
                            {
                                if (ee.Result == null)
                                {
                                    if (TypeToSwitch == 100) HelperMethods.DownloadMandatoryUpdater();
                                    else if (OnFBAuthenticationCompleted != null) OnFBAuthenticationCompleted(TypeToSwitch, fnm, fprUrl, fid, AccessToken, ringIdForthisfb, MessageToShow);
                                }
                                OnEscapeCommand(null);
                            }
                            catch (Exception ex)
                            {
                                showFacebookError();
                                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                            }
                        };
                    }
                    bgworker.RunWorkerAsync();
                }
                else
                {
                    if (_Browser != null)
                    {
                        PleaseWaitLoader = false;
                        _FormContainer.Visibility = Visibility.Visible;
                        _Browser.Visible = true;
                    }
                }
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }
        #endregion"Event Triggers"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using View.Constants;
using View.ViewModel;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for WNConfirmationView.xaml
    /// </summary>
    public partial class WNConfirmationView : Window
    {
        private ConfirmationViewModel cvm;

        // Handle of Button StackPanel UI Control to dynamically build button list
        private StackPanel buttonStackPanel
        {
            get
            {
                return (StackPanel)MainGrid.FindName("_ButtonStackPanel");
            }
        }

        /// <summary>
        /// Confirmation - Constructor
        /// </summary>
        public WNConfirmationView()
        {
            InitializeComponent();
            cvm = new ConfirmationViewModel(buttonStackPanel);
            this.DataContext = cvm;
            this.Owner = Application.Current.MainWindow;

            // Close Handler
            if (cvm.CloseAction == null)
            {
                cvm.CloseAction = new Action(() => this.Close());
            }
        }

        /// <summary>
        /// Confirmation - alternate constructor
        /// </summary>
        public WNConfirmationView(Window owner)
            : this()
        {
            this.Owner = owner;
        }

        /// <summary>
        /// Confirmation - alternate constructor
        /// </summary>
        /// <param Name="customMsg"></param>
        public WNConfirmationView(string customMsg)
        {
            InitializeComponent();
            cvm = new ConfirmationViewModel(buttonStackPanel, customMsg);
            this.DataContext = cvm;
            this.Owner = Application.Current.MainWindow;
            // Close Handler
            if (cvm.CloseAction == null)
            {
                cvm.CloseAction = new Action(() => this.Close());
            }
        }

        public WNConfirmationView(string customMsg, bool autoResize)
            : this(customMsg)
        {
            if (autoResize)
            {
                this.Height = Double.NaN;
                this.Width = Double.NaN;
            }
        }

        /// <summary>
        /// WNConfirmationView - alternate constructor
        /// </summary>
        /// <param name="customMsg"></param>
        /// <param name="owner"></param>
        public WNConfirmationView(string customMsg, Window owner)
            : this(customMsg)
        {
            this.Owner = owner;
        }

        /// <summary>
        /// WNConfirmationView - alternate constructor
        /// </summary>
        /// <param name="title"></param>
        /// <param name="customMsg"></param>
        /// <param name="options"></param>
        public WNConfirmationView(string title, string customMsg, CustomConfirmationDialogButtonOptions options)
        {
            InitializeComponent();
            cvm = new ConfirmationViewModel(buttonStackPanel, customMsg, options);
            cvm.ConfirmTitle = title;
            this.DataContext = cvm;
            this.Owner = Application.Current.MainWindow;

            // Close Handler
            if (cvm.CloseAction == null)
            {
                cvm.CloseAction = new Action(() => this.Close());
            }
        }

        /// <summary>
        /// WNConfirmationView - alternate constructor
        /// </summary>
        /// <param name="title"></param>
        /// <param name="customMsg"></param>
        /// <param name="options"></param>
        /// <param name="customButtonText"></param>
        public WNConfirmationView(string title, string customMsg, CustomConfirmationDialogButtonOptions options, string[] customButtonText)
        {
            InitializeComponent();
            cvm = new ConfirmationViewModel(buttonStackPanel, customMsg, options, customButtonText);
            cvm.ConfirmTitle = title;
            this.DataContext = cvm;
            this.Owner = Application.Current.MainWindow;

            // Close Handler
            if (cvm.CloseAction == null)
            {
                cvm.CloseAction = new Action(() => this.Close());
            }
        }

        /// <summary>
        /// WNConfirmationView - alternate constructor
        /// </summary>
        /// <param name="title"></param>
        /// <param name="customMsg"></param>
        /// <param name="options"></param>
        /// <param name="owner"></param>
        public WNConfirmationView(string title, string customMsg, CustomConfirmationDialogButtonOptions options, Window owner)
            : this(title, customMsg, options)
        {
            this.Owner = owner;
        }


        /// <summary>
        /// WNConfirmationView - alternate constructor
        /// </summary>
        /// <param name="title"></param>
        /// <param name="customMsg"></param>
        /// <param name="options"></param>
        /// <param name="description"></param>
        public WNConfirmationView(string title, string customMsg, CustomConfirmationDialogButtonOptions options, string description)
            : this(title, customMsg, options)
        {
            cvm.Description = description;
        }

        /// <summary>
        /// WNConfirmationView - alternate constructor
        /// </summary>
        /// <param name="title"></param>
        /// <param name="customMsg"></param>
        /// <param name="options"></param>
        /// <param name="description"></param>
        /// <param name="owner"></param>
        public WNConfirmationView(string title, string customMsg, CustomConfirmationDialogButtonOptions options, string description, Window owner)
            : this(title, customMsg, options, description)
        {
            this.Owner = owner;
        }

        /// <summary>
        /// Shows the confirmation dialog, returns result
        /// </summary>
        /// <returns></returns>
        public ConfirmationDialogResult ShowCustomDialog()
        {
            this.ShowDialog();
            return cvm.Result;
        }

        /// <summary>
        /// Shows the confirmation dialog, returns result with optional checkbox out value
        /// </summary>
        /// <param name="checkBoxValue"></param>
        /// <returns></returns>
        public ConfirmationDialogResult ShowCustomDialog(out bool checkBoxValue)
        {
            this.ShowDialog();
            checkBoxValue = cvm.CheckBoxValue;
            return cvm.Result;
        }

        public void ChangeICon(ConfirmationDialogType type)
        {
            if (type == ConfirmationDialogType.FAILD)
                cvm.IconSource = ImageLocation.CONFIRMATIONS_CROSS;
            else if (type == ConfirmationDialogType.WARNING)
                cvm.IconSource = ImageLocation.CONFIRMATIONS_WARNING;
        }
    }
}

﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.DataContainer;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.NewsPortal
{
    /// <summary>
    /// Interaction logic for UCFollowingListPanel.xaml
    /// </summary>
    public partial class UCFollowingPortalsPanel : UserControl, INotifyPropertyChanged
    {
        public CustomFeedScroll scroll;

        #region "Constructor"
        public UCFollowingPortalsPanel(CustomFeedScroll scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
            //   new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_NEWSPORTAL);
            //new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_NEWSPORTAL);

            ThradDiscoverOrFollowingChannelsList request = new ThradDiscoverOrFollowingChannelsList();
            request.CallBack += (response) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                    switch (response)
                    {
                        case SettingsConstants.RESPONSE_SUCCESS:
                            showMoreBtn.Visibility = Visibility.Visible;
                            showMorePanel.Visibility = Visibility.Visible;
                            if (FollowingNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.RESPONSE_NOTSUCCESS:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (FollowingNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.NO_RESPONSE:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (FollowingNewsPortals.Count == 0) noMoreTxt.Visibility = Visibility.Visible;
                            UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK);
                            break;
                    }
                });
            };
            request.StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_NEWSPORTAL);
        }
        #endregion

        #region "Properties"
        private ObservableCollection<NewsPortalModel> _FollowingNewsPortals = new ObservableCollection<NewsPortalModel>();
        public ObservableCollection<NewsPortalModel> FollowingNewsPortals
        {
            get
            {
                return _FollowingNewsPortals;
            }
            set
            {
                _FollowingNewsPortals = value;
                this.OnPropertyChanged("FollowingNewsPortals");
            }
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
        #region "ContextMenu"

        ContextMenu cntxMenu = new ContextMenu();
        MenuItem mnuItem1 = new MenuItem();
        MenuItem mnuItem2 = new MenuItem();
        MenuItem mnuItem3 = new MenuItem();

        private void SetupMenuItem()
        {
            if (cntxMenu.Items != null)
                cntxMenu.Items.Clear();
            cntxMenu.Style = (Style)Application.Current.Resources["CustomCntxtMenu"];
            mnuItem1.Header = "Edit";
            mnuItem2.Header = "Unfollow";

            mnuItem1.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem2.Style = (Style)Application.Current.Resources["ContextMenuItem"];

            cntxMenu.Items.Add(mnuItem1);
            cntxMenu.Items.Add(mnuItem2);

            mnuItem1.Click -= MnuEditFlwngList_Click;
            mnuItem2.Click -= MnuUnfollow_Click;
            cntxMenu.Closed -= ContextMenu_Closed;
            cntxMenu.Closed += ContextMenu_Closed;
            mnuItem1.Click += MnuEditFlwngList_Click;
            mnuItem2.Click += MnuUnfollow_Click;

        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            mnuItem1.Click -= MnuEditFlwngList_Click;
            mnuItem2.Click -= MnuUnfollow_Click;
            cntxMenu.Closed -= ContextMenu_Closed;

            cntxMenu.Style = null;
            mnuItem1.Style = null;
            mnuItem2.Style = null;


            //cntxMenu.Items.Remove(mnuItem1);
            //cntxMenu.Items.Remove(mnuItem2);
            cntxMenu.Items.Clear();

            optnBtn.ContextMenu = null;
        }

        private void MnuEditFlwngList_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (portalModel != null)
            {
                if (UCFollowingUnfollowingView.Instance == null)
                {
                    UCFollowingUnfollowingView.Instance = new UCFollowingUnfollowingView(UCGuiRingID.Instance.MotherPanel);
                    UCFollowingUnfollowingView.Instance.Show();
                    UCFollowingUnfollowingView.Instance.ShowFollowUnFollow(portalModel, true);
                    UCFollowingUnfollowingView.Instance.OnRemovedUserControl += () =>
                    {
                        UCFollowingUnfollowingView.Instance = null;
                    };
                }
                portalModel.IsContextMenuOpened = false;
                portalModel = null;
            }
        }

        private void MnuUnfollow_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (portalModel != null)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "News Portal"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                if (isTrue)
                {
                    new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_NEWSPORTAL, portalModel.UserTableID, 1, null, null, portalModel.PortalId).StartThread();
                    portalModel = null;
                }
            }
            if (FollowingNewsPortals.Count == 0 && !showMoreBtn.IsVisible)
                noMoreTxt.Visibility = Visibility.Visible;
        }

        #endregion "ContextMenu"
        #region "Public Methods"
        //public void ShowMoreStates(int state) //0 first success,1false, 2no resp \3 success,4 false, 5 no response
        //{
        //    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
        //    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        //    showMoreLoader.Visibility = Visibility.Collapsed;
        //    switch (state)
        //    {
        //        case 0:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 1:
        //            //
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 2:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //        case 3:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 4:
        //            showMoreBtn.Visibility = Visibility.Collapsed;
        //            showMorePanel.Visibility = Visibility.Collapsed;
        //            if (FollowingNewsPortals.Count == 0)
        //                noMoreTxt.Visibility = Visibility.Visible;
        //            break;
        //        case 5:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //    }
        //}
        #endregion

        #region "Event Triggers"
        Button optnBtn;

        private NewsPortalModel portalModel = null;
        private NewsPortalModel prevPortalModel = null;
        private void optnBtn_Click(object sender, RoutedEventArgs e)
        {
            optnBtn = (Button)sender;
            if (optnBtn.DataContext is NewsPortalModel)
            {
                portalModel = (NewsPortalModel)optnBtn.DataContext;
                if (cntxMenu.Items.Count == 0 || optnBtn.ContextMenu != cntxMenu)
                {
                    optnBtn.ContextMenu = cntxMenu;
                    SetupMenuItem();
                }
                cntxMenu.IsOpen = true;
                portalModel.IsContextMenuOpened = true;
                if (prevPortalModel != null && portalModel.PortalId != prevPortalModel.PortalId)
                {
                    prevPortalModel.IsContextMenuOpened = false;
                }
                prevPortalModel = portalModel;
                optnBtn.ContextMenu.IsVisibleChanged -= ContextMenu_IsVisibleChanged;
                optnBtn.ContextMenu.IsVisibleChanged += ContextMenu_IsVisibleChanged;
            }
        }
        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false)
            {
                if (portalModel != null)
                    portalModel.IsContextMenuOpened = false;
                optnBtn.ContextMenu.IsVisibleChanged -= ContextMenu_IsVisibleChanged;
            }
        }

        private void LoadMore_ButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            //new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, FollowingNewsPortals.Count);
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            ThradDiscoverOrFollowingChannelsList request = new ThradDiscoverOrFollowingChannelsList();
            request.CallBack += (response) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                    showMoreLoader.Visibility = Visibility.Collapsed;
                    switch (response)
                    {
                        case SettingsConstants.RESPONSE_SUCCESS:
                            showMoreBtn.Visibility = Visibility.Visible;
                            showMorePanel.Visibility = Visibility.Visible;
                            if (FollowingNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.RESPONSE_NOTSUCCESS:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (FollowingNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.NO_RESPONSE:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (FollowingNewsPortals.Count == 0) noMoreTxt.Visibility = Visibility.Visible;
                            UIHelperMethods.ShowWarning(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Load More..");
                            break;
                    }
                });
            };
            request.StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, FollowingNewsPortals.Count);
        }

        //private List<NewsPortalDTO> TMP_DTOS = new List<NewsPortalDTO>();
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);
            //scroll.ScrollChanged += feedScrlViewer_ScrollChanged;
            showMoreBtn.Click += LoadMore_ButtonClick;
        }
        private double previousScrollHeight = 0;
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            prevPortalModel = null;
            previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;
            //scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
            showMoreBtn.Click -= LoadMore_ButtonClick;
        }
        #endregion
    }
}

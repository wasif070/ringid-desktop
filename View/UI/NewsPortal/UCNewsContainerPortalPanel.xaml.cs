﻿using Auth.utility;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Auth;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;
using System.Threading.Tasks;

namespace View.UI.NewsPortal
{
    /// <summary>
    /// Interaction logic for UCNewsContainerPanel.xaml
    /// </summary>
    public partial class UCNewsContainerPortalPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCNewsContainerPortalPanel).Name);
        private ObservableCollection<FeedModel> _BreakingNewsPortalFeeds = new ObservableCollection<FeedModel>();
        //private VirtualizingStackPanel _sliderItemContainer = null;
        private Visibility _IsLeftArrowVisible = Visibility.Hidden;
        private Visibility _IsRightArrowVisible = Visibility.Hidden;
        public System.Timers.Timer _Timer;
        private VirtualizingStackPanel _ImageItemContainer = null;
        public CustomFeedScroll scroll;

        private bool _IsFirstTimePrev = true;
        private bool _IsFirstTimeNext = true;

        private int _CurrentLeftMargin = 0;
        private int _NextPrevCount = 0;
        private int _NoOfSwappingCount = 0;

        #region "Constructor"
        public UCNewsContainerPortalPanel(CustomFeedScroll scroll)
        {
            this.scroll = scroll;
            this.DataContext = this;
            InitializeComponent();
            //this.PreviewMouseWheel += (s, e) => { scroll.OnPreviewMouseWheelScrolled(e.Delta); e.Handled = true; };//this.PreviewMouseWheel += delegate(object sender, MouseWheelEventArgs e) { };
            //this.IsVisibleChanged += (s, e) => { scroll.OnIsVisibleChanged((bool)e.NewValue); };

            ViewCollection = RingIDViewModel.Instance.AllNewsPortalCustomFeeds;
            scroll.SetScrollValues(feedItemsControl, BreakingNewsPortalSliderFeeds, ViewCollection, FeedDataContainer.Instance.AllNewsPortalCurrentIds, FeedDataContainer.Instance.AllNewsPortalTopIds, FeedDataContainer.Instance.AllNewsPortalBottomIds, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, AppConstants.TYPE_NEWSPORTAL_FEED);
            DefaultSettings.ALLNEWSPORTAL_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
            {
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.ALLNEWSPORTAL_STARTPKT);
            }
            BreakingNewsRequest();
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }

        public ObservableCollection<FeedModel> BreakingNewsPortalFeeds
        {
            get
            {
                return _BreakingNewsPortalFeeds;
            }
            set
            {
                _BreakingNewsPortalFeeds = value;
                this.OnPropertyChanged("BreakingNewsPortalFeeds");
            }
        }

        private ObservableCollection<FeedModel> _BreakingNewsPortalSliderFeeds = new ObservableCollection<FeedModel>();
        public ObservableCollection<FeedModel> BreakingNewsPortalSliderFeeds
        {
            get
            {
                return _BreakingNewsPortalSliderFeeds;
            }
            set
            {
                _BreakingNewsPortalSliderFeeds = value;
                this.OnPropertyChanged("BreakingNewsPortalSliderFeeds");
            }
        }

        private CustomFeedCollection _ViewCollection;
        public CustomFeedCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }
        #endregion

        #region "Public Methods"
        public void LoadSliderData()
        {
            try
            {
                //if (BreakingNewsPortalSliderFeeds.Count > 0)
                //    BreakingNewsPortalSliderFeeds.Clear();
                if (BreakingNewsPortalFeeds != null && BreakingNewsPortalFeeds.Count > 0)
                {
                    List<FeedModel> breakingNws = BreakingNewsPortalFeeds.ToList();
                    foreach (FeedModel model in breakingNws)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            if (!BreakingNewsPortalSliderFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                            {
                                BreakingNewsPortalSliderFeeds.Add(model);
                            }
                        }, DispatcherPriority.Send);
                        Thread.Sleep(10);
                        if (BreakingNewsPortalSliderFeeds.Count == 4)
                        {
                            break;
                        }
                    }
                    Thread.Sleep(1000);
                    if (BreakingNewsPortalSliderFeeds.Count > 1)
                    {
                        SetNextPrevoiusButtonVisibility();
                        StartBannerNewsSliding();
                    }
                }
            }

            catch (Exception ex)
            {
                log.Error("Error: LoadSliderData() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Private Methods"

        private void StartBannerNewsSliding()
        {
            if (_Timer == null)
            {
                _Timer = new System.Timers.Timer();
                _Timer.Interval = 2500;
                _Timer.Elapsed += timer_Elapsed;
                _Timer.Start();
            }
        }

        private void SetNextPrevoiusButtonVisibility()
        {
            if (BreakingNewsPortalFeeds != null && _NextPrevCount == BreakingNewsPortalFeeds.Count)
            {
                _NextPrevCount = 0;
            }

            IsLeftArrowVisible = _NextPrevCount > 0 ? Visibility.Visible : Visibility.Collapsed;
            IsRightArrowVisible = BreakingNewsPortalFeeds != null && _NextPrevCount < BreakingNewsPortalFeeds.Count - 1 ? Visibility.Visible : Visibility.Collapsed;
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Application.Current.Dispatcher.BeginInvoke(() =>
            //{
            try
            {
                OnNextCommandClicked();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
            //}, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    ThicknessAnimation MarginAnimation = new ThicknessAnimation();
                    MarginAnimation.From = new Thickness(_CurrentLeftMargin, 0, 0, 0);
                    MarginAnimation.To = new Thickness(target, 0, 0, 0);
                    MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                    Storyboard storyboard = new Storyboard();
                    storyboard.Children.Add(MarginAnimation);
                    Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
                    Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
                    storyboard.Begin(_ImageItemContainer);
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: AnimationOnArrowClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }

        }

        private void OnPrevious(object param)
        {
            try
            {
                int target = 0;
                _CurrentLeftMargin = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNext(object param)
        {
            try
            {
                //int target = _CurrentLeftMargin - (int)pnlImageContainerWrapper.ActualWidth;
                int target = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ResetData()
        {
            if (_Timer != null)
            {
                _Timer.Elapsed -= timer_Elapsed;
                _Timer.Stop();
                _Timer.Enabled = false;
                _Timer = null;
            }
            AnimationOnArrowClicked(0, 0);
            _NoOfSwappingCount = 0;
            _NextPrevCount = 0;
            _CurrentLeftMargin = 0;
            _IsFirstTimeNext = true;
            _IsFirstTimePrev = true;

            IsLeftArrowVisible = Visibility.Hidden;
            IsRightArrowVisible = Visibility.Hidden;
        }

        private void BreakingNewsRequest()
        {
            JObject pakToSend = new JObject();
            //pakToSend[JsonKeys.UserTableID] = NewsPortalModel.UserTableID;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_NEWSPORTAL_BREAKING_FEED, AppConstants.REQUEST_TYPE_REQUEST);
        }
        #endregion

        #region "Event Triggers"
        private void sliderScroll_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
            eventArg.RoutedEvent = UIElement.MouseWheelEvent;
            eventArg.Source = sender;
            var parent = ((Control)sender).Parent as UIElement;
            parent.RaiseEvent(eventArg);
        }
        #endregion

        #region "ICommand"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            scroll.OnIsVisibleChanged(true);
            BreakingNewsPortalFeeds = RingIDViewModel.Instance.BreakingNewsPortalFeeds;
            new Task(delegate
            {
                if (BreakingNewsPortalFeeds != null && BreakingNewsPortalFeeds.Count > 1)
                {
                    LoadSliderData();
                }
            }).Start();
            sliderScroll.PreviewMouseWheel += sliderScroll_PreviewMouseWheel;
        }

        private ICommand unLoadedUserConrol;
        public ICommand UnLoadedUserConrol
        {
            get
            {
                if (unLoadedUserConrol == null) unLoadedUserConrol = new RelayCommand(param => OnUnLoadedUserConrol());
                return unLoadedUserConrol;
            }
        }
        public void OnUnLoadedUserConrol()
        {
            scroll.OnIsVisibleChanged(false);
            scroll.ScrollToHome();
            sliderScroll.PreviewMouseWheel -= sliderScroll_PreviewMouseWheel;
            if (_ImageItemContainer != null)
                ResetData();
            //if (BreakingNewsPortalSliderFeeds != null)
            //    BreakingNewsPortalSliderFeeds.Clear();

            if (BreakingNewsPortalFeeds != null)
                BreakingNewsPortalFeeds = null;
        }
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            if (!scroll.RequestOn)
            {
                FeedDataContainer.Instance.AllNewsPortalTopIds.Reset();
                FeedDataContainer.Instance.AllNewsPortalBottomIds.Reset();
                ViewCollection.RemoveAllModels();
                DefaultSettings.ALLNEWSPORTAL_STARTPKT = Auth.utility.SendToServer.GetRanDomPacketID();
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.ALLNEWSPORTAL_STARTPKT);
            }
            BreakingNewsRequest();
        }
        private ICommand _PreviousCommand;
        public ICommand PreviousCommand
        {
            get
            {
                if (_PreviousCommand == null)
                {
                    _PreviousCommand = new RelayCommand(param => OnPreviousCommandClicked());
                }
                return _PreviousCommand;
            }
        }
        private void OnPreviousCommandClicked()
        {
            try
            {
                if (_Timer != null)
                    _Timer.Stop();

                int firstIndex = 0, middleIndex = 1, lastIndex = 2;

                if (_IsFirstTimePrev == false)
                {
                    if (BreakingNewsPortalSliderFeeds.Count > 2)
                    {
                        BreakingNewsPortalSliderFeeds.InvokeMove(middleIndex, lastIndex);
                        BreakingNewsPortalSliderFeeds.InvokeMove(firstIndex, middleIndex);
                        BreakingNewsPortalSliderFeeds.RemoveAt(firstIndex);

                        FeedModel model = BreakingNewsPortalFeeds.ElementAt(_NoOfSwappingCount);
                        BreakingNewsPortalSliderFeeds.InvokeInsert(firstIndex, model);
                    }
                }

                _IsFirstTimeNext = true;
                _IsFirstTimePrev = false;

                OnPrevious(null);
                _NextPrevCount--;

                if (_NoOfSwappingCount > 0)
                {
                    _NoOfSwappingCount = _NoOfSwappingCount - 1;
                }

                SetNextPrevoiusButtonVisibility();
                if (_Timer != null)
                    _Timer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: prevBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private ICommand _NextCommand;
        public ICommand NextCommand
        {
            get
            {
                if (_NextCommand == null)
                {
                    _NextCommand = new RelayCommand(param => OnNextCommandClicked());
                }
                return _NextCommand;
            }
        }
        private void OnNextCommandClicked()
        {
            //Hints: For AutoSlide/Next, Viewport is 0 to -630.
            try
            {
                if (_Timer != null)
                    _Timer.Stop();
                if (_IsFirstTimeNext == false)
                {
                    int firstIndex = 0, middleIndex = 1, lastIndex = 2, nextLoadingIndex = (_NextPrevCount + lastIndex) % BreakingNewsPortalFeeds.Count;
                    BreakingNewsPortalSliderFeeds.InvokeMove(firstIndex, middleIndex);
                    if (BreakingNewsPortalSliderFeeds.Count > 2)
                    {
                        BreakingNewsPortalSliderFeeds.InvokeMove(middleIndex, lastIndex);
                        BreakingNewsPortalSliderFeeds.InvokeRemoveAt(lastIndex);
                        FeedModel model = BreakingNewsPortalFeeds.ElementAt(nextLoadingIndex);
                        BreakingNewsPortalSliderFeeds.InvokeInsert(lastIndex, model);

                        if (_NoOfSwappingCount == BreakingNewsPortalFeeds.Count - 1)
                        {
                            _NoOfSwappingCount = 0;
                        }

                        else
                        {
                            _NoOfSwappingCount++;
                        }
                    }
                }

                _IsFirstTimeNext = false; // firstTime no swapping make, just changing viewport from 0 to -630.
                _IsFirstTimePrev = true;

                _NextPrevCount++;
                OnNext(null);
                SetNextPrevoiusButtonVisibility();
                if (_Timer != null)
                    _Timer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: nextBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private ICommand _BreakingNewsDetailsCommand;
        public ICommand BreakingNewsDetailsCommand
        {
            get
            {
                if (_BreakingNewsDetailsCommand == null)
                {
                    _BreakingNewsDetailsCommand = new RelayCommand(param => OnBreakingNewsDetailsCommand(param));
                }
                return _BreakingNewsDetailsCommand;
            }
        }
        private void OnBreakingNewsDetailsCommand(object sender)
        {
            try
            {
                if (sender is FeedModel)
                {
                    FeedModel modelFromContext = (FeedModel)sender;

                    if (UCSingleFeedDetailsView.Instance != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(UCSingleFeedDetailsView.Instance);
                    UCSingleFeedDetailsView.Instance = new UCSingleFeedDetailsView(UCGuiRingID.Instance.MotherPanel);
                    UCSingleFeedDetailsView.Instance.Show();
                    UCSingleFeedDetailsView.Instance.ShowDetailsView();
                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Visible;
                    UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                    UCSingleFeedDetailsView.Instance.OnRemovedUserControl += () =>
                    {
                        UCSingleFeedDetailsView.Instance = null;
                    };

                    MainSwitcher.ThreadManager().DetailsOfaFeed.CallBackEvent += (dto) =>
                    {
                        if (dto != null)
                        {
                            FeedModel fm = null;
                            FeedDataContainer.Instance.FeedModels.TryGetValue(modelFromContext.NewsfeedId, out fm);
                            if (fm == null) fm = modelFromContext;//new FeedModel();
                            fm.LoadData(dto);
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (UCSingleFeedDetailsView.Instance != null)
                                {
                                    UCSingleFeedDetailsView.Instance.LoadFeedModel(fm);
                                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Collapsed;
                                    if (UCSingleFeedDetailsView.Instance.GIFCtrlLoader.IsRunning())
                                        UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StopAnimate();
                                }
                            });
                        }
                    };
                    MainSwitcher.ThreadManager().DetailsOfaFeed.StartThread(modelFromContext.NewsfeedId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private ICommand _VirtualizingStackPanelLoaded;
        public ICommand VirtualizingStackPanelLoaded
        {
            get
            {
                if (_VirtualizingStackPanelLoaded == null)
                {
                    _VirtualizingStackPanelLoaded = new RelayCommand(param => OnVirtualizingStackPanelLoaded(param));
                }
                return _VirtualizingStackPanelLoaded;
            }
        }
        private void OnVirtualizingStackPanelLoaded(object parameter)
        {
            if (parameter is VirtualizingStackPanel)
                _ImageItemContainer = parameter as VirtualizingStackPanel;
        }
        #endregion
    }
}

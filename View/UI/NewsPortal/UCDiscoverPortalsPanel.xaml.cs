﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.NewsPortal
{
    /// <summary>
    /// Interaction logic for UCDiscoverPanel.xaml
    /// </summary>
    public partial class UCDiscoverPortalsPanel : UserControl, INotifyPropertyChanged
    {
        public CustomFeedScroll scroll;
        //private List<NewsPortalDTO> TMP_DTOS = new List<NewsPortalDTO>();

        #region "Constructor"
        public UCDiscoverPortalsPanel(CustomFeedScroll scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
            //new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_NEWSPORTAL);
            ThradDiscoverOrFollowingChannelsList request = new ThradDiscoverOrFollowingChannelsList();
            request.CallBack += (response) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                    switch (response)
                    {
                        case SettingsConstants.RESPONSE_SUCCESS:
                            showMoreBtn.Visibility = Visibility.Visible;
                            showMorePanel.Visibility = Visibility.Visible;
                            if (SearchedNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.RESPONSE_NOTSUCCESS:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (SearchedNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.NO_RESPONSE:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (SearchedNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            //CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                            break;
                    }
                });
            };
            request.StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_NEWSPORTAL);
        }
        #endregion

        #region "Property"
        private ObservableCollection<NewsPortalModel> _SearchedNewsPortals = new ObservableCollection<NewsPortalModel>();
        public ObservableCollection<NewsPortalModel> SearchedNewsPortals
        {
            get
            {
                return _SearchedNewsPortals;
            }
            set
            {
                _SearchedNewsPortals = value;
                this.OnPropertyChanged("SearchedNewsPortals");
            }
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Event Triggers"
        //public void feedScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        //{
        //    var scrollViewer = (ScrollViewer)sender;
        //    if (scrollViewer.VerticalOffset >= 51)//51 
        //    {
        //        UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.floatingPanel.Visibility = Visibility.Visible;
        //    }
        //    else
        //    {
        //        UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.floatingPanel.Visibility = Visibility.Collapsed;
        //    }
        //}

        private void LoadMore_ButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            //new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, SearchedNewsPortals.Count);
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            ThradDiscoverOrFollowingChannelsList request = new ThradDiscoverOrFollowingChannelsList();
            request.CallBack += (response) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                    showMoreLoader.Visibility = Visibility.Collapsed;
                    switch (response)
                    {
                        case SettingsConstants.RESPONSE_SUCCESS:
                            showMoreBtn.Visibility = Visibility.Visible;
                            showMorePanel.Visibility = Visibility.Visible;
                            if (SearchedNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.RESPONSE_NOTSUCCESS:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (SearchedNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.NO_RESPONSE:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (SearchedNewsPortals.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            //CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                            break;
                    }
                });
            };
            //request.StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, SearchedNewsPortals.Count);
            string countryName = !UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel.SelectedCountryName.Equals("All") ? UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel.SelectedCountryName : "";
            request.StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel.SelectedSearchParam, countryName, SearchedNewsPortals.Count);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //if (TMP_DTOS.Count > 0)
            //{
            //    foreach (var item in TMP_DTOS)
            //    {
            //        if (!SearchedNewsPortals.Any(P => P.PortalId == item.PortalId))
            //        {
            //            NewsPortalModel model = NewsPortalDataContainer.Instance.SearchNewsPortalModelByUtId(item.PortalId);
            //            if (model == null) model = new NewsPortalModel();
            //            model.LoadData(item);
            //            SearchedNewsPortals.Add(model);
            //        }
            //    }
            //    TMP_DTOS.Clear();
            //}
            //scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
            scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);
            //scroll.ScrollChanged += feedScrlViewer_ScrollChanged;
            showMoreBtn.Click += LoadMore_ButtonClick;
            foreach (var model in SearchedNewsPortals)
            {
                int index = SearchedNewsPortals.IndexOf(model);
                if ((index + 1) % 3 == 0)
                {
                    model.IsRightMarginOff = true;
                }
                else
                {
                    model.IsRightMarginOff = false;
                }
            }
        }
        private double previousScrollHeight = 0;
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //if (SearchedNewsPortals.Count > 0)
            //{
            //    foreach (var item in SearchedNewsPortals)
            //    {
            //        NewsPortalDTO dto = null;
            //        if (NewsPortalDictionaries.Instance.UTID_NEWSPORTALINFO_DICTIONARY.TryGetValue(item.PortalId, out dto))
            //        {
            //            TMP_DTOS.Add(dto);
            //        }
            //    }
            //    SearchedNewsPortals.Clear();
            //}
            previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;
            //scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
            showMoreBtn.Click -= LoadMore_ButtonClick;
        }
        #endregion

        #region "Public Methods"
        //public void ShowMoreStates(int state) //0 first success,1false, 2no resp \3 success,4 false, 5 no response
        //{
        //    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
        //    GIFCtrlLoader.Visibility = Visibility.Collapsed;
        //    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        //    showMoreLoader.Visibility = Visibility.Collapsed;
        //    switch (state)
        //    {
        //        case 0:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 1:
        //            //
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 2:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //        case 3:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 4:
        //            showMoreBtn.Visibility = Visibility.Collapsed;
        //            showMorePanel.Visibility = Visibility.Collapsed;
        //            break;
        //        case 5:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //    }
        //}
        #endregion
    }
}

﻿using log4net;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.Constants;
using View.UI.PopUp;
using View.Utility;

namespace View.UI.NewsPortal
{
    /// <summary>
    /// Interaction logic for UCNewsPortalMainPanel.xaml
    /// </summary>
    public partial class UCNewsPortalMainPanel : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCNewsPortalMainPanel).Name);

        public UCNewsContainerPortalPanel _UCNewsContainerPanel = null;
        public UCDiscoverPortalsPanel _UCDiscoverPanel = null;
        public UCFollowingPortalsPanel _UCFollowingListPanel = null;
        public UCSavedPortalFeedsPanel _UCSavedContentPanel = null;
        public CustomScrollViewer scroll;
        public UCNewsPortalMainPanel()
        {
            InitializeComponent();
            //new NewsPortalRequest(0, 2, 10).StartThis();
            this.DataContext = this;
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeNewsPortal, TabType);
            SwitchToTab(TabType);
            this.Loaded += UserControl_Loaded;
            this.Unloaded += UserControl_Unloaded;
        }

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"

        private string _TitleName = "News Portal";
        public string TitleName
        {
            get { return _TitleName; }
            set
            {
                if (value == _TitleName)
                    return;
                _TitleName = value;
                this.OnPropertyChanged("TitleName");
            }
        }

        private bool _IsTabView = true;
        public bool IsTabView
        {
            get { return _IsTabView; }
            set
            {
                if (value == _IsTabView)
                    return;
                _IsTabView = value;
                this.OnPropertyChanged("IsTabView");
            }
        }

        private int _TabType = 0;
        public int TabType
        {
            get { return _TabType; }
            set
            {
                _TabType = value;
                OnPropertyChanged("TabType");
            }
        }
        #endregion
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            View.ViewModel.RingIDViewModel.Instance.NewsPortalSelection = true;
            //searchBtn.Click -= searchBtn_Click;
            //searchBtn.Click += searchBtn_Click;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            View.ViewModel.RingIDViewModel.Instance.NewsPortalSelection = false;
            //searchBtn.Click -= searchBtn_Click;
        }

        public void SwitchToTab(int SelectedIndex)
        {
            try
            {
                if (TabType != SelectedIndex)
                    TabType = SelectedIndex;
                switch (SelectedIndex)
                {
                    case 0://"NewsTab":
                        if (_UCNewsContainerPanel == null)
                        {
                            _UCNewsContainerPanel = new UCNewsContainerPortalPanel(ScrlViewer);
                        }

                        tabContainerBorder.Child = _UCNewsContainerPanel;
                        break;
                    case 1://"DiscoverTab":
                        if (_UCDiscoverPanel == null)
                        {
                            _UCDiscoverPanel = new UCDiscoverPortalsPanel(ScrlViewer);
                        }
                        tabContainerBorder.Child = _UCDiscoverPanel;
                        break;
                    case 2://"FollowingTab":
                        if (_UCFollowingListPanel == null)
                        {
                            _UCFollowingListPanel = new UCFollowingPortalsPanel(ScrlViewer);
                        }
                        //else
                        //{
                        //    if (_UCFollowingListPanel.FollowingNewsPortals.Count == 0 && !_UCFollowingListPanel.showMoreBtn.IsVisible)
                        //        _UCFollowingListPanel.noMoreTxt.Visibility = Visibility.Visible;
                        //    else
                        //        _UCFollowingListPanel.noMoreTxt.Visibility = Visibility.Collapsed;
                        //}
                        tabContainerBorder.Child = _UCFollowingListPanel;
                        break;
                    case 3://"SavedTab":
                        if (_UCSavedContentPanel == null) _UCSavedContentPanel = new UCSavedPortalFeedsPanel(ScrlViewer);
                        tabContainerBorder.Child = _UCSavedContentPanel;
                        break;
                }
            }
            catch (System.Exception) { }
        }

        public void LoadUI(bool type)
        {
            try
            {
                //IsSearch = type;
                // if(IsSearch)
                // {
                //      if (View_UCNewsPortalSearchPanel == null) View_UCNewsPortalSearchPanel = new UCNewsPortalSearchPanel(ScrlViewer);
                //        userControl.Content = View_UCNewsPortalSearchPanel;
                //}
                //else
                // {
                tabContainerBorder.Visibility = Visibility.Visible;
                searchContentsBdr.Visibility = Visibility.Collapsed;
                IsTabView = true;
                //}
                //switch (IsSearch)
                //{
                //    case false:
                //        if (View_UCNewsPortalMainPanel == null) View_UCNewsPortalMainPanel = new UCNewsPortalMainPanel(ScrlViewer);
                //        userControl.Content = View_UCNewsPortalMainPanel;
                //        break;

                //    case true:
                //        if (View_UCNewsPortalSearchPanel == null) View_UCNewsPortalSearchPanel = new UCNewsPortalSearchPanel(ScrlViewer);
                //        userControl.Content = View_UCNewsPortalSearchPanel;
                //        break;

                //    default:
                //        if (View_UCNewsPortalMainPanel == null) View_UCNewsPortalMainPanel = new UCNewsPortalMainPanel(ScrlViewer);
                //        userControl.Content = View_UCNewsPortalMainPanel;
                //        break;
                //}
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        #region "ICommand"
        private ICommand _BtnTabCommand;
        public ICommand BtnTabCommand
        {
            get
            {
                if (_BtnTabCommand == null)
                {
                    _BtnTabCommand = new RelayCommand(param => OnBtnTabCommandClicked(param));
                }
                return _BtnTabCommand;
            }
        }

        public void OnBtnTabCommandClicked(object parameter)
        {
            if (parameter is string)
            {
                int TabIdx = Convert.ToInt32(parameter);
                MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeNewsPortal, TabIdx);
                SwitchToTab(TabIdx);
            }
        }
        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommandClicked(param));
                }
                return _BackCommand;
            }
        }

        public void OnBackCommandClicked(object parameter)
        {
            tabContainerBorder.Visibility = Visibility.Visible;
            searchContentsBdr.Visibility = Visibility.Collapsed;
            IsTabView = true;
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }
        private ICommand _SearchCommand;
        public ICommand SearchCommand
        {
            get
            {
                if (_SearchCommand == null)
                {
                    _SearchCommand = new RelayCommand(param => OnSearchCommandClicked(param));
                }
                return _SearchCommand;
            }
        }

        public void OnSearchCommandClicked(object parameter)
        {
            tabContainerBorder.Visibility = Visibility.Collapsed;
            searchContentsBdr.Visibility = Visibility.Visible;
            IsTabView = false;
            if (UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel == null) UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel = new UCNewsPortalSearchPanel(ScrlViewer);
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeNewsPortalSearch, 0);
            searchContentsBdr.Child = UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel;
        }

        private ICommand _DiscoverSearchClick;
        public ICommand DiscoverSearchClick
        {
            get
            {
                if (_DiscoverSearchClick == null)
                {
                    _DiscoverSearchClick = new RelayCommand(param => OnDiscoverSearchClick(param));
                }
                return _DiscoverSearchClick;
            }
        }
        public string SelectedSearchParam = string.Empty;
        public string SelectedCountryName = string.Empty;
        private void OnDiscoverSearchClick(object parameter)
        {
            if (UCDiscoverSearchView.Instance != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCDiscoverSearchView.Instance);
            UCDiscoverSearchView.Instance = new UCDiscoverSearchView(UCGuiRingID.Instance.MotherPanel);
            UCDiscoverSearchView.Instance.SetDiscoverSettings(true, SelectedSearchParam, SelectedCountryName);
            UCDiscoverSearchView.Instance.Show();
            UCDiscoverSearchView.Instance.OnRemovedUserControl += () =>
            {
                UCDiscoverSearchView.Instance = null;
            };
        }
        #endregion "ICommand"

        //private void backBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    SearchTermTextBox.Text = "";//CurrentSearchStr = "";
        //    IsSearch = false;
        //    AskSuggestions();
        //}


        //public string CurrentSearchStr = string.Empty;

        //private TextBox textBox;

        //private void SearchTermTextBox_Loaded(object sender, RoutedEventArgs e)
        //{
        //    textBox = (TextBox)sender;
        //}
    }
}

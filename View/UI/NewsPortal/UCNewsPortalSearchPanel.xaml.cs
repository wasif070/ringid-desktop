﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Feed;
using View.Utility.DataContainer;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.UI.PopUp;

namespace View.UI.NewsPortal
{
    /// <summary>
    /// Interaction logic for UCNewsPortalSearchPanel.xaml
    /// </summary>
    public partial class UCNewsPortalSearchPanel : UserControl, INotifyPropertyChanged
    {
        public CustomFeedScroll scroll;
        public UCNewsPortalSearchPanel(CustomFeedScroll scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
        }

        private ObservableCollection<NewsPortalModel> _SearchedNewsPortals = new ObservableCollection<NewsPortalModel>();
        public ObservableCollection<NewsPortalModel> SearchedNewsPortals
        {
            get
            {
                return _SearchedNewsPortals;
            }
            set
            {
                _SearchedNewsPortals = value;
                this.OnPropertyChanged("SearchedNewsPortals");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void AskSuggestions()
        {
            string CurrentSearchStr = SearchTermTextBox.Text.Trim();
            SearchedNewsPortals.Clear();
            List<long> list = null;
            noResultFound.Visibility = Visibility.Collapsed;
            if (UserProfilesContainer.Instance.SearchedPortalIdsByParam.TryGetValue(CurrentSearchStr, out list))
            {
                foreach (var item in list)
                {
                    NewsPortalModel model = null;
                    if (UserProfilesContainer.Instance.NewsPortalModels.TryGetValue(item, out model))
                    {
                        if (!SearchedNewsPortals.Any(P => P.PortalId == model.PortalId))
                            SearchedNewsPortals.Add(model);
                    }
                }
            }
            else
            {
                showMoreBtn.Visibility = Visibility.Collapsed;
                showMoreLoader.Visibility = Visibility.Collapsed;
                showMorePanel.Visibility = Visibility.Visible;
                this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
              ThradSearchNewsPortals request = new ThradSearchNewsPortals();
                request.CallBack += (response) =>
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                        switch (response)
                        {
                            case SettingsConstants.RESPONSE_SUCCESS:
                                showMoreBtn.Visibility = Visibility.Visible;
                                showMorePanel.Visibility = Visibility.Visible;
                                if (SearchedNewsPortals.Count == 0)
                                    noResultFound.Visibility = Visibility.Visible;
                                else
                                    noResultFound.Visibility = Visibility.Collapsed ;
                                break;
                            case SettingsConstants.RESPONSE_NOTSUCCESS:
                                showMoreBtn.Visibility = Visibility.Collapsed;
                                showMorePanel.Visibility = Visibility.Collapsed;
                                if (SearchedNewsPortals.Count == 0)
                                    noResultFound.Visibility = Visibility.Visible;
                                else
                                    noResultFound.Visibility = Visibility.Collapsed;
                                break;
                            case SettingsConstants.NO_RESPONSE:
                                showMoreBtn.Visibility = Visibility.Collapsed;
                                showMorePanel.Visibility = Visibility.Collapsed;
                                if (SearchedNewsPortals.Count == 0)
                                    noResultFound.Visibility = Visibility.Visible;
                                else
                                    noResultFound.Visibility = Visibility.Collapsed;
                                //CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                                break;
                        }
                    });
                };
                request.StartThread(CurrentSearchStr, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, SearchedNewsPortals.Count);
            }
        }
        private void LookUp(object sender, EventArgs e)
        {
            Dispatcher.Invoke(AskSuggestions);
            if (_Timer != null) _Timer.Stop();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.TextChanged -= SearchTermTextBox_TextChanged;
            SearchTermTextBox.TextChanged += SearchTermTextBox_TextChanged;
            cancel.Click -= cancel_Click;
            cancel.Click += cancel_Click;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.TextChanged -= SearchTermTextBox_TextChanged;
            SearchedNewsPortals.Clear();
            SearchTermTextBox.Text = "";
            cancel.Click -= cancel_Click;
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Collapsed;
            noResultFound.Visibility = Visibility.Collapsed;
        }
        private DispatcherTimer _Timer;

        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchTermTextBox.Text.Trim().Length > 0)//if (CurrentSearchStr.Length > 0)
            {
                if (_Timer == null)
                {
                    //AskSuggestions();
                    _Timer = new DispatcherTimer(DispatcherPriority.Background);
                    _Timer.Interval = TimeSpan.FromSeconds(1); //100ms
                    _Timer.Tick += LookUp;
                    _Timer.Start();
                }
                else if (!_Timer.IsEnabled)
                {
                    _Timer.Start();
                }
            }
            else  //load default Values
            {
                noResultFound.Visibility = Visibility.Collapsed;
                showMoreBtn.Visibility = Visibility.Collapsed;
                showMoreLoader.Visibility = Visibility.Collapsed;
                if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                SearchedNewsPortals.Clear();
                _Timer.Stop();
            }
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.Clear();
            UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel.LoadUI(false);
            noResultFound.Visibility = Visibility.Collapsed;
        }

        private void LoadMore_ButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            noResultFound.Visibility = Visibility.Collapsed;
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            ThradSearchNewsPortals request = new ThradSearchNewsPortals();
            request.CallBack += (response) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                    showMoreLoader.Visibility = Visibility.Collapsed;
                    switch (response)
                    {
                        case SettingsConstants.RESPONSE_SUCCESS:
                            showMoreBtn.Visibility = Visibility.Visible;
                            showMorePanel.Visibility = Visibility.Visible;
                            if (SearchedNewsPortals.Count == 0)
                                noResultFound.Visibility = Visibility.Visible;
                            else
                                noResultFound.Visibility = Visibility.Collapsed;
                            break;
                        case SettingsConstants.RESPONSE_NOTSUCCESS:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (SearchedNewsPortals.Count == 0)
                                noResultFound.Visibility = Visibility.Visible;
                            else
                                noResultFound.Visibility = Visibility.Collapsed;
                            break;
                        case SettingsConstants.NO_RESPONSE:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (SearchedNewsPortals.Count == 0)
                                noResultFound.Visibility = Visibility.Visible;
                            else
                                noResultFound.Visibility = Visibility.Collapsed;
                            //CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                            break;
                    }
                });
            };
            request.StartThread(SearchTermTextBox.Text.Trim(), SettingsConstants.PROFILE_TYPE_NEWSPORTAL, SearchedNewsPortals.Count);
        }

        //public void ShowMoreStates(int state) //0 first success,1false, 2no resp \3 success,4 false, 5 no response
        //{
        //    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
        //    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        //    showMoreLoader.Visibility = Visibility.Collapsed;
        //    switch (state)
        //    {
        //        case 0:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 1:
        //            showMoreBtn.Visibility = Visibility.Collapsed;
        //            showMorePanel.Visibility = Visibility.Collapsed;
        //            break;
        //        case 2:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //        case 3:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 4:
        //            showMoreBtn.Visibility = Visibility.Collapsed;
        //            showMorePanel.Visibility = Visibility.Collapsed;
        //            break;
        //        case 5:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //    }
        //}
        #region "Utility Methods"

        //private void SearchNewsPortals(string searchParam, int profileType, int st = 0)
        //{
        //    new ThradSearchNewsPortals().StartThread(searchParam, profileType, st);

        //}

        //private void SubscribeOrUnsubscribe(int profileType, long utId, int SubUnsubscrbType, List<long> subscrbeLst, List<long> unsubscrbeLst)
        //{
        //    new ThradSubscribeOrUnsubscribe(profileType, utId, SubUnsubscrbType, subscrbeLst, unsubscrbeLst).StartThread();
        //}

        //private FeedModel GetFeedModel(FeedDTO feedDto)
        //{
        //    return (FeedModel)MainSwitcher.AuthSignalHandler().feedSignalHandler.GetFeedModel(feedDto);
        //}

        #endregion "Utility Methods"
    }
}

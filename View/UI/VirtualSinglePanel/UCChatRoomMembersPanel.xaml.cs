﻿using Models.Constants;
using Models.DAO;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.Chat;
using View.UI.PopUp;
using View.UI.Room;
using View.Utility;
using View.ViewModel;

namespace View.UI.VirtualSinglePanel
{
    /// <summary>
    /// Interaction logic for UCChatRoomMembersPanel.xaml
    /// </summary>
    public partial class UCChatRoomMembersPanel : UserControl
    {
        public UCChatRoomMembersPanel()
        {
            InitializeComponent();
        }

        private void MainGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RoomMemberModel model = (RoomMemberModel)((FrameworkElement)sender).DataContext;

            UCRoomChatLikeMemberListPanel likeListParent = HelperMethods.FindVisualParent<UCRoomChatLikeMemberListPanel>(this);
            if (likeListParent != null)
            {
                likeListParent.OnCloseCommand.Execute(null);
            }

            UCRoomMemberListPanel memberListParent = HelperMethods.FindVisualParent<UCRoomMemberListPanel>(this);
            if (memberListParent != null)
            {
                memberListParent.OnCloseCommand.Execute(null);
            }

            if (model.MemberID == DefaultSettings.LOGIN_TABLE_ID)
            {
                RingIDViewModel.Instance.OnMyProfileClicked(null);
            }
            else
            {
                RingIDViewModel.Instance.OnFriendCallChatButtonClicked(model.MemberID);
            }
        }
    }
}

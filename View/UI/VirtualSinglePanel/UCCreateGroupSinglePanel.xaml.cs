﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;

namespace View.UI.VirtualSinglePanel
{
    /// <summary>
    /// Interaction logic for UCCreateGroupSinglePanel.xaml
    /// </summary>
    public partial class UCCreateGroupSinglePanel : UserControl
    {
        public UCCreateGroupSinglePanel()
        {
            InitializeComponent();
            this.Loaded += UCCreateGroupSinglePanel_Loaded;
            this.Unloaded += UCCreateGroupSinglePanel_Unloaded;
        }

        private void UCCreateGroupSinglePanel_Unloaded(object sender, RoutedEventArgs e)
        {
            cb.Checked -= CheckBox_Checked;
            cb.Unchecked -= CheckBox_Unchecked;
            MainPanel.MouseLeftButtonDown -= Grid_MouseLeftButtonDown;
        }

        private void UCCreateGroupSinglePanel_Loaded(object sender, RoutedEventArgs e)
        {
            cb.Checked += CheckBox_Checked;
            cb.Unchecked += CheckBox_Unchecked;
            MainPanel.MouseLeftButtonDown += Grid_MouseLeftButtonDown;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            UCCreateGroupPopUp parent = HelperMethods.FindVisualParent<UCCreateGroupPopUp>(this);
            if (parent != null)
            {
                if (((CheckBox)sender).DataContext is UserBasicInfoModel)
                {
                    UserBasicInfoModel model = (UserBasicInfoModel)((CheckBox)sender).DataContext;
                    model.ShortInfoModel.IsVisibleInTagList = true;
                    parent.SelectedFriendList.InvokeAdd(model);
                    parent.TotalSelectedFriend += 1;
                }
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            UCCreateGroupPopUp parent = HelperMethods.FindVisualParent<UCCreateGroupPopUp>(this);
            if (parent != null)
            {
                if (((CheckBox)sender).DataContext is UserBasicInfoModel)
                {
                    UserBasicInfoModel model = (UserBasicInfoModel)((CheckBox)sender).DataContext;
                    model.ShortInfoModel.IsVisibleInTagList = false;
                    parent.SelectedFriendList.InvokeRemove(model);
                    parent.TotalSelectedFriend -= 1;
                }
            }
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (((Grid)sender).DataContext is UserBasicInfoModel)
            {
                UserBasicInfoModel selectedModel = (UserBasicInfoModel)((Grid)sender).DataContext;
                if (selectedModel.ShortInfoModel.IsVisibleInTagList == true)
                {
                    selectedModel.ShortInfoModel.IsVisibleInTagList = false;
                    return;
                }
                selectedModel.ShortInfoModel.IsVisibleInTagList = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.PopUp.MediaSendViaChat;
using View.Utility;

namespace View.UI.VirtualSinglePanel
{
    /// <summary>
    /// Interaction logic for UCMediaShareRecentSelection.xaml
    /// </summary>
    public partial class UCMediaShareRecentSelection : UserControl
    {
        public UCMediaShareRecentSelection()
        {
            InitializeComponent();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.ViewModel;

namespace View.UI.VirtualSinglePanel
{
    /// <summary>
    /// Interaction logic for UCCreateGroupFromChatCallPanel.xaml
    /// </summary>
    public partial class UCCreateGroupFromChatCallPanel : UserControl, INotifyPropertyChanged
    {
        public UCCreateGroupFromChatCallPanel()
        {
            InitializeComponent();
            DataContext = this;
            this.Loaded += UCCreateGroupFromChatCallPanel_Loaded;
            this.Unloaded += UCCreateGroupFromChatCallPanel_Unloaded;
        }

        public UCAddMemberToGroupPopupWrapper Instance
        {
            get
            {
                return MainSwitcher.PopupController.Instance;
            }
        }

        private void UCCreateGroupFromChatCallPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            MainPanel.MouseLeftButtonDown -= MainPanel_MouseLeftButtonDown;
            Cb.Checked -= Cb_Checked;
            Cb.Unchecked -= Cb_Unchecked;
            BtnProfile.Click -= BtnProfile_Click;
        }

        private void UCCreateGroupFromChatCallPanel_Loaded(object sender, RoutedEventArgs e)
        {
            MainPanel.MouseLeftButtonDown += MainPanel_MouseLeftButtonDown;
            Cb.Checked += Cb_Checked;
            Cb.Unchecked += Cb_Unchecked;
            BtnProfile.Click += BtnProfile_Click;
        }

        private void Cb_Unchecked(object sender, RoutedEventArgs e)
        {
            //UCAddMemberToGroupPopup parent = HelperMethods.FindVisualParent<UCAddMemberToGroupPopup>(this);
            //if (parent != null)
            //{
            if (((CheckBox)sender).DataContext is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)((CheckBox)sender).DataContext;
                if (Instance._UCAddMemberToGroupPopup.SelectedFriendList.Contains(model))
                {
                    Instance._UCAddMemberToGroupPopup.SelectedFriendList.Remove(model);
                    Instance._UCAddMemberToGroupPopup.TotalSelectedFriend -= 1;
                }
            }
            //}
        }

        private void Cb_Checked(object sender, RoutedEventArgs e)
        {
            //UCAddMemberToGroupPopup parent = HelperMethods.FindVisualParent<UCAddMemberToGroupPopup>(this);
            //if (parent != null)
            //{
            if (((CheckBox)sender).DataContext is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)((CheckBox)sender).DataContext;
                if (!Instance._UCAddMemberToGroupPopup.SelectedFriendList.Contains(model))
                {
                    Instance._UCAddMemberToGroupPopup.SelectedFriendList.Add(model);
                    Instance._UCAddMemberToGroupPopup.TotalSelectedFriend += 1;
                }
            }
            //}
        }

        private void MainPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UserBasicInfoModel model = (UserBasicInfoModel)((Grid)sender).DataContext;
            model.VisibilityModel.IsAddGroupMember = !model.VisibilityModel.IsAddGroupMember;
        }

        private void BtnProfile_Click(object sender, RoutedEventArgs e)
        {
            UserBasicInfoModel model = (UserBasicInfoModel)((Button)sender).DataContext;
            RingIDViewModel.Instance.OnFriendProfileButtonClicked(model.ShortInfoModel.UserTableID);
            //UCAddMemberToGroupPopup parent = HelperMethods.FindVisualParent<UCAddMemberToGroupPopup>(this);
            //if (parent != null)
            //{
            Instance._UCAddMemberToGroupPopup.popupAddMember.IsOpen = false;
            //}
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿using Models.Constants;
using Models.DAO;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.Chat;
using View.UI.PopUp;
using View.UI.Room;
using View.Utility;
using View.ViewModel;

namespace View.UI.VirtualSinglePanel
{
    /// <summary>
    /// Interaction logic for UCChatContactPanel.xaml
    /// </summary>
    public partial class UCChatContactPanel : UserControl
    {
        public UCChatContactPanel()
        {
            InitializeComponent();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UCChatContactSharePreview parent = HelperMethods.FindVisualParent<UCChatContactSharePreview>(this);
            if (parent != null && parent.SelectedContactList != null)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)border.DataContext;
                if (parent.SelectedContactList.ContainsKey(model.ShortInfoModel.UserTableID))
                {
                    parent.SelectedContactList.TryRemove(model.ShortInfoModel.UserTableID);
                }
                else
                {
                    parent.SelectedContactList[model.ShortInfoModel.UserTableID] = model;
                }
                
            }
        }
    }
}

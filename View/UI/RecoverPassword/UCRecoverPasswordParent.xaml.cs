﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.Utility;
using View.Constants;
using View.UI.PopUp;
using View.UI.SignUp;
using View.UI.SocialMedia;
using View.Utility;
using View.ViewModel;

namespace View.UI.RecoverPassword
{
    /// <summary>
    /// Interaction logic for UCRecoverPasswordParent.xaml
    /// </summary>
    public partial class UCRecoverPasswordParent : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCRecoverPasswordParent).Name);
        Grid motherPanel = null;
        Border signUpBorder = null;
        public event DelegateNoParam OnBackButtonClicked;
        string mobileNumberWithCode = null;
        #endregion

        #region"Ctors"
        public UCRecoverPasswordParent(VMRingIDMainWindow model, Grid motherGrid, Border signUpBorder)
        {
            InitializeComponent();
            this.DataModel = model;
            this.motherPanel = motherGrid;
            this.signUpBorder = signUpBorder;
            this.DataContext = this;
        }
        #endregion"Ctors"

        #region "Properties"
        private UCSignUPWithMail SignUPWithMail { get; set; }
        private UCFBAuthentication FBAuthentication { get; set; }
        private UCWaitForDigitVarification WaitForDigitVarification { get; set; }
        private UCTwitterUI TwitterUI { get; set; }
        private UCResetPassword ResetPassword { get; set; }
        private UCSignUPSuccess SignUPSuccess { get; set; }
        private UCFullNamePassword FullNamePassword { get; set; }
        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private string typeText;
        public string TypeText
        {
            get { return this.typeText; }
            set { this.typeText = value; this.OnPropertyChanged("TypeText"); }
        }

        private int btnShowType;
        public int BtnShowType
        {
            get { return btnShowType; }
            set
            {
                if (value == btnShowType) return;
                btnShowType = value; OnPropertyChanged("BtnShowType");
            }
        }

        private Visibility isFbVisibility = Visibility.Collapsed;
        public Visibility IsFbVisibility
        {
            get { return isFbVisibility; }
            set
            {
                if (value == isFbVisibility) return;
                isFbVisibility = value; OnPropertyChanged("IsFbVisibility");
            }
        }

        private Visibility isPhnVisibility = Visibility.Collapsed;
        public Visibility IsPhnVisibility
        {
            get { return isPhnVisibility; }
            set
            {
                if (value == isPhnVisibility) return;
                isPhnVisibility = value; OnPropertyChanged("IsPhnVisibility");
            }
        }

        private Visibility isEmailVisibility = Visibility.Collapsed;
        public Visibility IsEmailVisibility
        {
            get { return isEmailVisibility; }
            set
            {
                if (value == isEmailVisibility) return;
                isEmailVisibility = value; OnPropertyChanged("IsEmailVisibility");
            }
        }

        private Visibility isTwitterVisibility = Visibility.Collapsed;
        public Visibility IsTwitterVisibility
        {
            get { return isTwitterVisibility; }
            set
            {
                if (value == isTwitterVisibility) return;
                isTwitterVisibility = value; OnPropertyChanged("IsTwitterVisibility");
            }
        }

        #endregion

        #region "ICommands and Command Methods"

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            getFocusOnUserControl();
        }

        private ICommand expanCollaspViewCommand;
        public ICommand ExpanCollaspViewCommand
        {
            get
            {
                if (expanCollaspViewCommand == null) expanCollaspViewCommand = new RelayCommand(param => OnExpanCollaspView(param));
                return expanCollaspViewCommand;
            }
        }
        private void OnExpanCollaspView(object param)
        {
            int parseType = Convert.ToInt32(param);
            DataModel.LoginType = parseType;
            if (parseType == SettingsConstants.EMAIL_LOGIN)
            {
                expanEmailPanel();
            }
            else if (parseType == SettingsConstants.MOBILE_LOGIN)
            {
                // DefaultSettings.TMP_RECOVERY_MOBILE = mobileNumberWithCode;
                if (WaitForDigitVarification == null)
                {
                    long tempRingID = 0;
                    try
                    {
                        if (!string.IsNullOrEmpty(DataModel.RingID)) tempRingID = long.Parse(DataModel.RingID);
                    }
                    finally { }
                    WaitForDigitVarification = new UCWaitForDigitVarification(2, tempRingID, mobileNumberWithCode);
                    WaitForDigitVarification.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 0.0);//background Color Transparent Black
                    WaitForDigitVarification.SetParent(motherPanel);
                    WaitForDigitVarification.OnCompletedDigitsVarifications += (successfullyVerified, mbl, mblDc, previousRingId) =>
                    {
                        Console.WriteLine(previousRingId + " ==>" + previousRingId);
                        WaitForDigitVarification = null;
                        if (successfullyVerified) loadResetPassword(parseType);
                    };
                    WaitForDigitVarification.Show();
                }
            }
            else if (parseType == SettingsConstants.FACEBOOK_LOGIN)
            {
                if (FBAuthentication == null)
                {
                    FBAuthentication = new UCFBAuthentication(SettingsConstants.TYPE_FROM_RECOVERY);
                    FBAuthentication.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                    FBAuthentication.SetParent(motherPanel);
                    FBAuthentication.OnRemovedUserControl += () =>
                    {
                        getFocusOnUserControl();
                        FBAuthentication = null;
                    };
                    FBAuthentication.OnFBAuthenticationCompleted += (typeToSwitch, fbName, fbProfileImage, fbId, accessToken, ringID, message) => OnFBAuthenticationCompleted(typeToSwitch, fbName, fbProfileImage, fbId, accessToken, ringID, message);
                    FBAuthentication.Show();
                }
            }
            else if (parseType == SettingsConstants.TWITTER_LOGIN)
            {
                if (TwitterUI == null)
                {
                    TwitterUI = new UCTwitterUI(SettingsConstants.TYPE_FROM_RECOVERY);
                    TwitterUI.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                    TwitterUI.SetParent(motherPanel);
                    TwitterUI.OnRemovedUserControl += () =>
                    {
                        getFocusOnUserControl();
                        TwitterUI = null;
                    };
                    TwitterUI.OnThreadCompleted += (typeToSwitch, screenName, profileImage, socialMediaID, inputToken, ringID, message) => OnTwitterFeedBack(typeToSwitch, screenName, profileImage, socialMediaID, inputToken, ringID, message);
                    TwitterUI.Show();
                }
            }
        }

        private ICommand backButtonCommand;
        public ICommand BackButtonCommand
        {
            get
            {
                if (backButtonCommand == null) backButtonCommand = new RelayCommand(param => OnBackButton(param));
                return backButtonCommand;
            }
        }
        private void OnBackButton(object param)
        {
            OnBackButtonClicked();
        }

        private ICommand enterKeyCommand;
        public ICommand EnterKeyCommand
        {
            get
            {
                if (enterKeyCommand == null) enterKeyCommand = new RelayCommand(param => OnEnterKeyCommand(param));
                return enterKeyCommand;
            }
        }
        private void OnEnterKeyCommand(object param)
        {

        }
        #endregion"ICommands and Command Methods"

        #region "Utility Methods"
        public void LoadData(List<string> sgnss)
        {
            foreach (string sgn in sgnss)
            {
                if (!string.IsNullOrEmpty(sgn))
                {
                    if (string.Equals(sgn, "facebook", StringComparison.OrdinalIgnoreCase)) IsFbVisibility = Visibility.Visible;
                    else if (string.Equals(sgn, "twitter", StringComparison.OrdinalIgnoreCase)) IsTwitterVisibility = Visibility.Visible;
                    else if (HelperMethodsModel.IsValidEmail(sgn))
                    {
                        DataModel.EmailID = sgn;
                        IsEmailVisibility = Visibility.Visible;
                    }
                    else if (sgn.StartsWith("+"))
                    {
                        mobileNumberWithCode = sgn;
                        IsPhnVisibility = Visibility.Visible;
                    }
                    else if (HelperMethodsModel.IsValidNumber(sgn)) DataModel.RingID = sgn;
                }
            }
        }

        private void expanEmailPanel()
        {
            if (_SendCodeEmailBorderPanel.Child == null)
            {
                if (SignUPWithMail == null)
                {
                    SignUPWithMail = new UCSignUPWithMail(DataModel);
                    SignUPWithMail.RecoveryByEmail = true;
                    SignUPWithMail.OnBackButtonClicked += (isSuccess, string1, string2) =>
                    {
                        DataModel.ErrorText = string.Empty;
                        DataModel.IsComponentEnabled = true;
                        if (!isSuccess)
                        {
                            BtnShowType = 0;
                            _SendCodeEmailBorderPanel.Child = null;
                            getFocusOnUserControl();
                        }
                        else loadResetPassword(SettingsConstants.EMAIL_LOGIN);
                        SignUPWithMail = null;
                    };
                }
                _SendCodeEmailBorderPanel.Child = SignUPWithMail;
                BtnShowType = 2;
            }
            else
            {
                BtnShowType = 0;
                _SendCodeEmailBorderPanel.Child = null;
            }
        }

        private void loadResetPassword(int loginType, string token = null, string socialMediaID = null)
        {
            if (ResetPassword == null)
            {
                ResetPassword = new UCResetPassword(loginType, DataModel.RingID, DataModel.EmailID, DataModel.CountryCode, DataModel.PhoneNumber, socialMediaID, token);
                ResetPassword.OnCancelButtonClicked += (isSuccess, string1, string2) =>
                {
                    if (isSuccess) addSignUPSuccess();
                    else
                    {
                        signUpBorder.Child = null;
                        signUpBorder.Child = this;
                    }
                    ResetPassword = null;
                };
                signUpBorder.Child = ResetPassword;
            }
        }

        private void onSignUPSuccessOfFaild(int typeToSwitch, int loginType, string fullName, string profileImage, string token, string socialMediaID, string ringID, string messag = null)
        {
            if (typeToSwitch > 0 && typeToSwitch == WelcomePanelConstants.TypeSignupNamePassword)
            {
                DataModel.RingID = DefaultSettings.VALUE_NEW_USER_NAME.ToString();
                loadFullNameProfilePanel(loginType, token, socialMediaID);
            }
            else if (ringID == DataModel.RingID) loadResetPassword(loginType, token, socialMediaID);
            else DataModel.ShowErrorMessage(messag);
        }

        private void OnFBAuthenticationCompleted(int typeToSwitch, string facebookName, string facebookProfileImage, string fbID, string accessToken, string ringID = null, string messag = null)
        {
            if (string.IsNullOrEmpty(messag)) onSignUPSuccessOfFaild(typeToSwitch, SettingsConstants.FACEBOOK_LOGIN, facebookName, facebookProfileImage, accessToken, fbID, ringID, NotificationMessages.FB_NOT_SAME);
            else DataModel.ShowErrorMessage(messag);
        }

        private void OnTwitterFeedBack(int typeToSwitch, string screenName, string profileImage, string socialMediaID, string inputToken, string ringID, string messag = null)
        {
            if (string.IsNullOrEmpty(messag)) onSignUPSuccessOfFaild(typeToSwitch, SettingsConstants.TWITTER_LOGIN, screenName, profileImage, inputToken, socialMediaID, ringID, NotificationMessages.TWITTER_NOT_SAME);
            else DataModel.ShowErrorMessage(messag);
        }

        private void getFocusOnUserControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            this.FocusVisualStyle = null;
        }

        private void addSignUPSuccess()
        {
            UIHelperMethods.ShowMessageWithTimerFromNonThread(motherPanel, NotificationMessages.PASSWORD_RESET_COMPLETED);
            if (SignUPSuccess == null) SignUPSuccess = new UCSignUPSuccess(DataModel, WelcomePanelConstants.TypeForgotPassword);
            SignUPSuccess.ChangeProfileImage();
            if (signUpBorder != null)
            {
                signUpBorder.Child = null;
                signUpBorder.Child = SignUPSuccess;
            }
        }

        private void loadFullNameProfilePanel(int logintype, string accessToken = null, string socialMediaID = null)
        {
            DataModel.Password = string.Empty;
            FullNamePassword = new UCFullNamePassword(DataModel, accessToken, socialMediaID);
            FullNamePassword.OnCancelButtonClicked += () =>
            {
                if (signUpBorder != null)
                {
                    signUpBorder.Child = null;
                    signUpBorder.Child = this;
                    getFocusOnUserControl();
                }
            };
            if (signUpBorder != null)
            {
                signUpBorder.Child = null;
                signUpBorder.Child = FullNamePassword;
                DataModel.LoginType = logintype;
            }
        }

        #endregion "Utility Methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

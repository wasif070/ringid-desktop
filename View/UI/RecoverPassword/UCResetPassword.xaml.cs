﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.Constants;
using View.Utility;
using View.Utility.Auth;
using View.Utility.SignInSignUP;

namespace View.UI.RecoverPassword
{
    /// <summary>
    /// Interaction logic for UCResetPassword.xaml
    /// </summary>
    public partial class UCResetPassword : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCResetPassword).Name);
        private int loginType;
        private string ringID, email, countryCode, phoneNumber, socialMediaId, inputToken;
        public event DelegateBoolStringString OnCancelButtonClicked;
        bool returnSuccess = false;
        AuthRequestWithMessageNoSession resetPasswordReqeust;
        #endregion

        #region"Ctors"
        public UCResetPassword(int loginType, string ringId, string email, string mobileCode, string phoneNumber, string socialMediaId = null, string inputToken = null)
        {
            InitializeComponent();
            this.loginType = loginType;
            this.DataContext = this;
            this.ringID = ringId;
            this.email = email;
            this.countryCode = mobileCode;
            this.phoneNumber = phoneNumber;
            this.socialMediaId = socialMediaId;
            this.inputToken = inputToken;
        }
        #endregion"Ctors"

        #region "Properties"
        private string password;
        public string Password
        {
            get { return this.password; }
            set { this.password = value; this.OnPropertyChanged("Password"); }
        }

        private string retypePassword;
        public string RetypePassword
        {
            get { return this.retypePassword; }
            set { this.retypePassword = value; this.OnPropertyChanged("RetypePassword"); }
        }

        private string errorText;
        public string ErrorText
        {
            get { return this.errorText; }
            set
            {
                this.errorText = value;
                this.OnPropertyChanged("ErrorText");
            }
        }

        private int errorMessageType;
        public int ErrorMessageType
        {
            get { return errorMessageType; }
            set
            {
                if (value == errorMessageType) return;
                errorMessageType = value; OnPropertyChanged("ErrorMessageType");
            }
        }
        private Visibility capsLockVisibility = Visibility.Hidden;
        public Visibility CapsLockVisibility
        {
            get { return this.capsLockVisibility; }
            set { this.capsLockVisibility = value; this.OnPropertyChanged("CapsLockVisibility"); }
        }

        private Visibility pleaseWaitLoaderVisibility = Visibility.Collapsed;
        public Visibility PleaseWaitLoaderVisibility
        {
            get { return this.pleaseWaitLoaderVisibility; }
            set
            {
                this.pleaseWaitLoaderVisibility = value;
                if (this.pleaseWaitLoaderVisibility == Visibility.Visible) ErrorText = "Please wait...";
                ChangeLoaderAnimation(this.pleaseWaitLoaderVisibility);
                this.OnPropertyChanged("PleaseWaitLoaderVisibility");
            }
        }

        private bool isComponentEnabled = true;
        public bool IsComponentEnabled
        {
            get { return isComponentEnabled; }
            set
            {
                if (value == isComponentEnabled) return;
                isComponentEnabled = value; OnPropertyChanged("IsComponentEnabled");
            }
        }

        private bool isFocusedOnPassword;
        public bool IsFocusedOnPassword
        {
            get { return isFocusedOnPassword; }
            set { isFocusedOnPassword = value; OnPropertyChanged("IsFocusedOnPassword"); }
        }

        private bool isFocusedRetypePassword;
        public bool IsFocusedRetypePassword
        {
            get { return isFocusedRetypePassword; }
            set { isFocusedRetypePassword = value; OnPropertyChanged("IsFocusedRetypePassword"); }
        }
        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            getFocusOnThis();
            OnCapsLockCommand();
        }

        private ICommand capsLockCommand;
        public ICommand CapsLockCommand
        {
            get
            {
                if (capsLockCommand == null) capsLockCommand = new RelayCommand(param => OnCapsLockCommand());
                return capsLockCommand;
            }
        }
        private void OnCapsLockCommand()
        {
            if (Console.CapsLock) CapsLockVisibility = Visibility.Visible;
            else CapsLockVisibility = Visibility.Hidden;
        }

        private ICommand cancelButtonCommand;
        public ICommand CancelButtonCommand
        {
            get
            {
                if (cancelButtonCommand == null) cancelButtonCommand = new RelayCommand(param => OnCancelButtonCommand(param));
                return cancelButtonCommand;
            }
        }
        private void OnCancelButtonCommand(object param)
        {
            if (IsComponentEnabled)
                if (OnCancelButtonClicked != null) OnCancelButtonClicked(returnSuccess, null, null);
        }

        private ICommand doneButtonCommand;
        public ICommand VerifyOrDoneButtonCommand
        {
            get
            {
                if (doneButtonCommand == null) doneButtonCommand = new RelayCommand(param => OnVerifyOrDoneButton(param));
                return doneButtonCommand;
            }
        }
        private void OnVerifyOrDoneButton(object param)
        {
            returnSuccess = false;
            string validationMsg = HelperMethodsModel.NamePasswordValidationMsg("demoname", Password, RetypePassword);
            if (string.IsNullOrEmpty(validationMsg))
            {
                ErrorMessageType = loginType;
                PleaseWaitLoaderVisibility = Visibility.Visible;
                IsComponentEnabled = false;
                Task taskForServercommunications = new Task(() =>
                {
                    string msg = resetPasswordRequest(ringID);
                    if (string.IsNullOrEmpty(msg))
                    {
                        msg = null;
                        DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN = 1;
                        DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
                        DefaultSettings.VALUE_LOGIN_AUTO_START = 1;
                        DefaultSettings.VALUE_LOGIN_USER_INFO = null;
                        SignedInInfoDTO signedInInfoDTO = new SigninRequest().FetchPortsAndSignIn(loginType, ringID, Password, email, countryCode, phoneNumber, socialMediaId, inputToken);
                        if (signedInInfoDTO != null)
                        {
                            if (signedInInfoDTO.isSuccess)
                            {
                                showErrorMessage(null);
                                returnSuccess = true;
                                Application.Current.Dispatcher.Invoke(delegate { OnCancelButtonCommand(null); });
                            }
                            else if (signedInInfoDTO.isDownloadMandatory)
                            {
                                Application.Current.Dispatcher.Invoke(delegate
                                {
                                    errorLabel.Visibility = Visibility.Collapsed;
                                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                                });
                                HelperMethods.DownloadMandatoryUpdater(signedInInfoDTO.versionMsg);
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(signedInInfoDTO.errorMsg))
                                {
                                    if (!DefaultSettings.IsInternetAvailable) msg = NotificationMessages.INTERNET_UNAVAILABLE;
                                    else msg = NotificationMessages.PLEASE_ENTER_DETAILS_CORRECTLY;
                                }
                                else msg = signedInInfoDTO.errorMsg;
                            }
                        }
                        else msg = NotificationMessages.FAILED_TO_RESET_PASSWORD; ;
                        if (!string.IsNullOrEmpty(msg)) showErrorMessage(msg);
                    }
                    else showErrorMessage(NotificationMessages.FAILED_TO_RESET_PASSWORD);
                });
                taskForServercommunications.Start();
            }
            else showErrorMessage(validationMsg);
        }

        private ICommand cancelServerReqeust;
        public ICommand CancelServerReqeustCommand
        {
            get
            {
                if (cancelServerReqeust == null) cancelServerReqeust = new RelayCommand(param => OnCancelServerReqeustCommand(param));
                return cancelServerReqeust;
            }
        }
        private void OnCancelServerReqeustCommand(object param)
        {
            if (resetPasswordReqeust != null) resetPasswordReqeust.Stop();
        }

        #endregion"ICommands and Command Methods"

        #region"Utility methods"

        private string resetPasswordRequest(string ringID)
        {
            bool SC = false;
            string MS = "";
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserIdentity] = ringID;
            pakToSend[JsonKeys.NewPassword] = Password;
            pakToSend[JsonKeys.IsDigits] = 1;
            resetPasswordReqeust = new AuthRequestWithMessageNoSession(pakToSend, AppConstants.TYPE_RESET_PASSWORD, AppConstants.REQUEST_TYPE_AUTH);
            resetPasswordReqeust.Run(out SC, out MS);
            return MS;
        }

        private void showErrorMessage(string Msg)
        {
            ChangeLoaderAnimation(Visibility.Collapsed);
            ErrorMessageType = 0;
            ErrorText = Msg;
            IsComponentEnabled = true;
        }

        private void ChangeLoaderAnimation(Visibility visibility)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (visibility == Visibility.Visible)
                {
                    GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_SIGN_IN));
                    GIFCtrl.Visibility = Visibility.Visible;
                }
                else
                {
                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                    GIFCtrl.Visibility = Visibility.Collapsed;
                    //ErrorText = string.Empty;
                }
            });
        }

        private void getFocusOnThis()
        {
            IsFocusedOnPassword = true;
        }
        #endregion
        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
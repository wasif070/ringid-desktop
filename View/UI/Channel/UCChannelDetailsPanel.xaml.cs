﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Utility;
using View.Utility.Channel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelDetailsPanel.xaml
    /// </summary>
    public partial class UCChannelDetailsPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCChannelDetailsPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private ObservableCollection<ChannelMediaModel> _NowPlayingList = null;
        private ObservableCollection<ChannelMediaModel> _NextToPlayList = null;

        public bool _IS_INITIALIZED = false;
        private ICommand _BackCommand = null;
        private bool _IsFromMyCahnnel = false;
        private ChannelModel _ChannelInfoModel = null;
        private DispatcherTimer _WindowResizeTimer = null;
        private DispatcherTimer _ScrollChangeTimer = null;

        private bool _IS_AT_TOP = true;
        private bool _IS_AT_BOTTOM = false;

        #region Constructor

        static UCChannelDetailsPanel()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCChannelDetailsPanel(dynamic param)
        {
            InitializeComponent();
            this.IsFromMyCahnnel = param.IsFromMyChannel;
            this.ChannelInfoModel = param.ChannelInfoModel;
            this.DataContext = null;
        }

        #endregion Constructor

        #region Event Handler

        private static void OnLoadingStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d != null && e.NewValue != null)
                {
                    UCChannelDetailsPanel panel = ((UCChannelDetailsPanel)d);
                    int loadingStatus = (int)e.NewValue;

                    switch (loadingStatus)
                    {
                        case StatusConstants.NO_DATA:
                            log.Debug("*********************    NO_DATA                 *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.NO_DATA_AND_LOADING:
                            log.Debug("*********************    NO_DATA_AND_LOADING     *********************");
                            panel.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            break;
                        case StatusConstants.HAS_DATA:
                            log.Debug("*********************    HAS_DATA                *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.HAS_DATA_AND_LOADING:
                            log.Debug("*********************    HAS_DATA_AND_LOADING    *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLoadingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int topLimit = 0;
            int bottomLimit = 0;

            int count = this.itcNowPlayingList.Items.Count + this.itcNextToPlayList.Items.Count;
            int nextToPlayCount = this.itcNextToPlayList.Items.Count;
            topLimit = count > 3 ? 300 : 150;
            bottomLimit = count > 3 ? 300 : 150;

            if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
            {
                if (!ChannelViewModel.Instance.LoadStatusModel.IsPlaylistLoading && e.ExtentHeight > e.ViewportHeight)
                {
                    if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_AT_BOTTOM == false)
                    {
                        Debug.WriteLine("IS_AT_BOTTOM");
                        ChannelHelpers.LoadChannelPlayList(ChannelInfoModel.ChannelID, false, 10, null);
                    }
                }
                this.ChangeScrollOpenStatusOnScroll();
            }

            if (e.ExtentHeight > e.ViewportHeight)
            {
                this._IS_AT_TOP = e.VerticalOffset <= topLimit;
                this._IS_AT_BOTTOM = (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit);
            }
            else
            {
                this._IS_AT_TOP = true;
                this._IS_AT_BOTTOM = false;
            }
        }

        private void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.ChangeScrollOpenStatusOnResize();
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            this.NowPlayingList = ChannelViewModel.Instance.GetChannelNowPlayingList(ChannelInfoModel.ChannelID);
            this.NextToPlayList = ChannelViewModel.Instance.GetChannelPlayList(ChannelInfoModel.ChannelID);

            ChannelHelpers.LoadChannelPlayList(ChannelInfoModel.ChannelID, true);

            this.DataContext = this;
            this.itcNextToPlayList.SizeChanged += ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged += ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged += ScrlViewer_SizeChanged;

            MultiBinding sourceProfileBinding = new MultiBinding { Converter = new ChannelProfileImageConverter(), ConverterParameter = ImageUtility.IMG_CROP };
            sourceProfileBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ChannelInfoModel"),
            });
            sourceProfileBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ChannelInfoModel.ProfileImage"),
            });
            imgProfile.SetBinding(Image.SourceProperty, sourceProfileBinding);

            MultiBinding sourceCoverBinding = new MultiBinding { Converter = new ChannelCoverImageConverter(), ConverterParameter = ImageUtility.IMG_FULL };
            sourceCoverBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ChannelInfoModel"),
            });
            sourceCoverBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ChannelInfoModel.CoverImage"),
            });
            imgCover.SetBinding(Image.SourceProperty, sourceCoverBinding);

            Guid channelID = this.ChannelInfoModel.ChannelID;
            new ThrdGetChannelDetails(channelID, (channelDTO) =>
            {
                if (this._IS_INITIALIZED && channelDTO != null && this.ChannelInfoModel != null && ChannelInfoModel.ChannelID.Equals(channelID))
                {
                    this.ChannelInfoModel.LoadData(channelDTO);
                }
                return 0;
            }).Start();

            this.itcNowPlayingList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("NowPlayingList") });
            this.itcNextToPlayList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("NextToPlayList") });

            MultiBinding loadingStatusBinding = new MultiBinding { Converter = new LoadingStatusConverter() };
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("NextToPlayList.Count") });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("NextToPlayList.Count") });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsPlaylistLoading"), Source = RingIDSettings });
            this.SetBinding(UCChannelDetailsPanel.LoadingStatusProperty, loadingStatusBinding);
        }

        private void OnBackClick(object param)
        {
            try
            {
                if (this.IsFromMyCahnnel)
                {
                    MyChannelSwitcher.Switch(MyChannelConstants.TypeMyChannelAndFollowingPanel);
                }
                else
                {
                    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeStreamAndChannel, true);
                }

            }
            catch (Exception ex)
            {
                log.Error("Error: OnBackClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnResize()
        {
            try
            {
                if (this._WindowResizeTimer == null)
                {
                    this._WindowResizeTimer = new DispatcherTimer();
                    this._WindowResizeTimer.Interval = TimeSpan.FromMilliseconds(350);
                    this._WindowResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._WindowResizeTimer.Stop();
                    };
                }

                if (this._ScrollChangeTimer != null && this._ScrollChangeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Stop();
                }
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnScroll()
        {
            try
            {
                if (this._ScrollChangeTimer == null)
                {
                    this._ScrollChangeTimer = new DispatcherTimer();
                    this._ScrollChangeTimer.Interval = TimeSpan.FromMilliseconds(30);
                    this._ScrollChangeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._ScrollChangeTimer.Stop();
                    };
                }
                this._ScrollChangeTimer.Stop();
                if (this._WindowResizeTimer == null || !this._WindowResizeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnScroll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatus()
        {
            try
            {
                int paddingTop = (int)bdrInfo.ActualHeight + (int)bdrNowPlayingList.ActualHeight + 45;
                ChannelHelpers.ChangeChannelMediaViewPortOpenedProperty(this.ScrlViewer, this.itcNextToPlayList, paddingTop);
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this._IS_INITIALIZED = false;

            if (this._WindowResizeTimer != null)
            {
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer = null;
            }
            if (this._ScrollChangeTimer != null)
            {
                this._ScrollChangeTimer.Stop();
                this._ScrollChangeTimer = null;
            }

            this.itcNextToPlayList.SizeChanged -= ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged -= ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged -= ScrlViewer_SizeChanged;

            Task.Factory.StartNew(() =>
            {
                NowPlayingList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                NextToPlayList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
            });

            this.ClearValue(UCChannelDetailsPanel.LoadingStatusProperty);
            this.itcNowPlayingList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcNowPlayingList.ItemsSource = null;
            this.itcNextToPlayList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcNextToPlayList.ItemsSource = null;
            this.imgCover.ClearValue(Image.SourceProperty);
            this.imgCover.Source = null;
            this.imgProfile.ClearValue(Image.SourceProperty);
            this.imgProfile.Source = null;
            this.imgCover.ClearValue(Image.SourceProperty);
            this.imgCover.Source = null;
            this.DataContext = null;
            ChannelViewModel.Instance.RemoveChannelNowPlayingList(ChannelInfoModel.ChannelID);
            ChannelViewModel.Instance.RemoveChannelPlayList(ChannelInfoModel.ChannelID);
            this.NowPlayingList.Clear();
            //this.NowPlayingList = null;
            this.NextToPlayList.Clear();
            //this.NextToPlayList = null;
            this.IsFromMyCahnnel = false;
            this.ChannelInfoModel = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty LoadingStatusProperty = DependencyProperty.Register("LoadingStatus", typeof(int), typeof(UCChannelDetailsPanel), new PropertyMetadata(StatusConstants.NO_DATA, OnLoadingStatusChanged));

        public int LoadingStatus
        {
            get { return (int)GetValue(LoadingStatusProperty); }
            set
            {
                SetValue(LoadingStatusProperty, value);
            }
        }

        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand((param) => OnBackClick(param));
                }
                return _BackCommand;
            }
        }

        public bool IsFromMyCahnnel
        {
            get { return _IsFromMyCahnnel; }
            set
            {
                if (_IsFromMyCahnnel == value)
                    return;

                _IsFromMyCahnnel = value;
                this.OnPropertyChanged("IsFromMyCahnnel");
            }
        }

        public ChannelModel ChannelInfoModel
        {
            get { return _ChannelInfoModel; }
            set
            {
                if (_ChannelInfoModel == value)
                    return;

                _ChannelInfoModel = value;
                this.OnPropertyChanged("ChannelInfoModel");
            }
        }

        public ObservableCollection<ChannelMediaModel> NowPlayingList
        {
            get { return _NowPlayingList; }
            set
            {
                _NowPlayingList = value;
                this.OnPropertyChanged("NowPlayingList");
            }
        }

        public ObservableCollection<ChannelMediaModel> NextToPlayList
        {
            get { return _NextToPlayList; }
            set
            {
                _NextToPlayList = value;
                this.OnPropertyChanged("NextToPlayList");
            }
        }

        #endregion Property

    }
}

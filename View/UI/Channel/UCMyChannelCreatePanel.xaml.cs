﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Media;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Utility.Channel;
using View.Utility.WPFMessageBox;
using View.Utility;
using Models.Entity;
using Models.Constants;
using View.Constants;
using Microsoft.Win32;
using System.Drawing;
using View.Utility.GIF;
using System.Dynamic;
using Auth.Service.Images;
using System.Drawing.Imaging;
using View.Utility.Images;
using Newtonsoft.Json.Linq;
using System.Windows;
using System.Windows.Data;
using View.Converter;
using System.Collections.ObjectModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelCreatePanel.xaml
    /// </summary>
    public partial class UCMyChannelCreatePanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelCreatePanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object _RingIDSettings = null;
        private List<ChannelCategoryDTO> _SelectedCategoryList = new List<ChannelCategoryDTO>();
        private ObservableCollection<CountryCodeModel> _CountryModelList = new ObservableCollection<CountryCodeModel>();
        private HashSet<int> _UploadStatusList = new HashSet<int>();
        public delegate void OnUploadComplete();
        public event OnUploadComplete _UploadComplte;
        public delegate void OnProPicUploadComplete();
        public event OnProPicUploadComplete _UploadProPicComplete;
        private bool _IsChannelCreateRunning;
        private string _ChannelTitle = string.Empty;
        private string _ChannelDescription = string.Empty;
        private String _SelectedCountry;

        #region ProfileImage

        private Canvas _ProfileImgCanvas;
        private System.Windows.Controls.Image _ProImgControl;
        private BitmapImage _BitmapProfileImage;
        private System.Windows.Point _ProImageMousePosition = new System.Windows.Point(-1, -1);
        private BackgroundWorker _ProfileImageUploadWorker = null;

        private const int STATE_INIT = 0;
        private const int STATE_BROWSE = 1;
        private const int STATE_UPLOADING = 2;
        private const int STATE_UPLOADED = 3;

        private const int PRO_PIC_UPLOAD_SCREEN_WIDTH = 140;
        private const int PRO_PIC_UPLOAD_SCREEN_HEIGHT = 140;

        private double _ProfileImageWidth;
        private double _ProfileImageHeight;
        private double _CropProfileX = 0;
        private double _CropProfileY = 0;
        private int _ProfileImageUploadState;
        private ICommand _ProfileImageChangeCommand;
        private string _ProfileImageUrl = string.Empty;
        private string _PreviousProImageUrl;
        private bool isProfileImageClicked;
        private ICommand _SaveCommand;
        #endregion

        #region CoverImage

        private System.Windows.Point _CoverImgMouseCapturePosition = new System.Windows.Point(-1, -1);
        private BackgroundWorker _CoverImageUploadWorker = null;
        private System.Windows.Controls.Image _UiImage;
        private BitmapImage _BitmapCoverImage;
        public ICommand _CoverImageChangeCommand;

        private int _CoverImageUploadState;

        private double _CoverImageWidth;
        private double _CoverImageHeight;
        private double _CropCoverImageX = 0;
        private double _CropCoverImageY = 0;
        private bool isCoverImageClicked;
        private string _CoverImageUrl = string.Empty;
        private string _PreViousCoverImageUrl;

        #endregion

        #region Constructor

        public UCMyChannelCreatePanel()
        {
            InitializeComponent();
            this._CoverImageUploadWorker = new BackgroundWorker();
            this._CoverImageUploadWorker.DoWork += CoverImageUploadWorker_DoWork;
            this._CoverImageUploadWorker.RunWorkerCompleted += CoverImageUploadWorker_RunWorkerCompleted;

            this._ProfileImageUploadWorker = new BackgroundWorker();
            this._ProfileImageUploadWorker.DoWork += Profile_Image_UploadWorker_DoWork;
            this._ProfileImageUploadWorker.RunWorkerCompleted += Profile_ImageUploadWorker_RunWorkerCompleted;

            if (Application.Current != null)
            {
                _RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }

            if (ChannelViewModel.Instance.ChannelCategoryList == null || ChannelViewModel.Instance.ChannelCategoryList.Count == 0)
            {
                new ThrdGetChannelCategoryList().Start();
            }

            for (int i = 2; i < DefaultSettings.COUNTRY_MOBILE_CODE.Length / 2; i++)
            {
                string name = DefaultSettings.COUNTRY_MOBILE_CODE[i, 0];
                string code = DefaultSettings.COUNTRY_MOBILE_CODE[i, 1];
                CountryModelList.Add(new CountryCodeModel(name, code));
            }
        }

        #endregion Constructor

        #region Utility Methods

        public void InilializeViewer()
        {
            this.DataContext = this;
            SetDefaultCoverImage();
            SetDefaultProfileImage();
            this.comboboxCategoryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelCategoryList"), Source = _RingIDSettings });
            this.comboboxCountryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("CountryModelList") });
            comboboxCategoryList.SelectionChanged += comboboxCategoryList_SelectionChanged;
            comboboxCountryList.SelectionChanged += comboboxCountryList_SelectionChanged;
            rtbTitle.TextChanged += rtbTitle_TextChanged;
            rtbdescription.TextChanged += rtbdescription_TextChanged;
            rtbTitle.KeyDown += rtbTitle_KeyDown;
            rtbdescription.KeyDown += rtbdescription_KeyDown;
        }

        private void SetDefaultCoverImage()
        {
            System.Windows.Controls.Image img = new System.Windows.Controls.Image { Source = ImageObjects.DEFAULT_COVER_IMAGE };
            Canvas.SetLeft(img, 0);
            Canvas.SetTop(img, 0);
            coverImgCanvus.Children.Clear();
            coverImgCanvus.Children.Add(img);
        }

        private void SetDefaultProfileImage()
        {
            if (_ProImgControl != null)
            {
                _ProImgControl.Source = ImageUtility.GetBitmapImage(ImageLocation.CHANNEL_PROFILE_UNKNOWN);
                Canvas.SetLeft(_ProImgControl, 0);
                Canvas.SetTop(_ProImgControl, 0);
            }
        }

        public void Dispose()
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                this.DataContext = null;
                coverImgCanvus.Children.Clear();
                ChannelTitle = string.Empty;
                ChannelDescription = string.Empty;
                comboboxCategoryList.ClearValue(ItemsControl.ItemsSourceProperty);
                comboboxCategoryList.ItemsSource = null;
                comboboxCategoryList.SelectionChanged -= comboboxCategoryList_SelectionChanged;
                comboboxCountryList.SelectionChanged -= comboboxCountryList_SelectionChanged;
                rtbTitle.TextChanged -= rtbTitle_TextChanged;
                rtbdescription.TextChanged -= rtbdescription_TextChanged;
                rtbTitle.KeyDown -= rtbTitle_KeyDown;
                rtbdescription.KeyDown -= rtbdescription_KeyDown;
            });
        }

        private void OnSaveCommand()
        {
            try
            {
                if (string.IsNullOrEmpty(ChannelTitle))
                {
                    UIHelperMethods.ShowWarning("Please give a Channel Title");
                    return;
                }
                BitmapImage _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                imgControl.Source = _LoadingIcon;
                ImageBehavior.SetAnimatedSource(imgControl, _LoadingIcon);

                bool alreadyRequested = false;
                IsChannelCreateRunning = true;
                _UploadComplte = null;
                _UploadProPicComplete = null;

                if (_BitmapProfileImage != null && !(!string.IsNullOrEmpty(_PreviousProImageUrl) && !string.IsNullOrEmpty(_ProfileImageUrl) && _PreviousProImageUrl.Equals(_ProfileImageUrl)))
                {
                    this._UploadStatusList.Add(ChannelConstants.CHANNEL_PRO_IMAGE);
                    ProfileImageUploadState = STATE_UPLOADING;
                    UploadAndSaveProfileImage();
                }
                if (_BitmapCoverImage != null && !(!string.IsNullOrEmpty(_PreViousCoverImageUrl) && !string.IsNullOrEmpty(_CoverImageUrl) && _PreViousCoverImageUrl.Equals(_CoverImageUrl)))
                {
                    this._UploadStatusList.Add(ChannelConstants.CHANNEL_COVER_IMAGE);
                    CoverImageUploadState = STATE_UPLOADING;
                    if (_UploadStatusList.Count == 1)
                    {
                        UploadAndSaveCoverImage();
                    }
                    else
                    {
                        this._UploadProPicComplete += () =>
                        {
                            UploadAndSaveCoverImage();
                        };
                    }
                }

                this._UploadComplte += () =>
                {
                    this._UploadStatusList.Clear();
                    if (_SelectedCategoryList.Count == 0)
                    {
                        _SelectedCategoryList.Add(new ChannelCategoryDTO { CategoryID = 1, CategoryName = "All" });
                    }
                    string country = string.IsNullOrEmpty(_SelectedCountry) ? "Canada" : _SelectedCountry;
                    int channelType = Int32.TryParse(comboboxChannelType.SelectedValue.ToString(), out channelType) ? Int32.Parse(comboboxChannelType.SelectedValue.ToString()) : 2;
                    new ThrdCreateChannel(ChannelTitle, ChannelDescription, _SelectedCategoryList, country, _ProfileImageUrl, (int)_ProfileImageWidth, (int)_ProfileImageHeight, _CoverImageUrl,
                                         (int)_CoverImageWidth, (int)_CoverImageHeight, (int)_CropCoverImageX, (int)_CropCoverImageY, channelType, (status, chanlDTO) =>
                                         {
                                             alreadyRequested = true;
                                             ProfileImageUploadState = STATE_INIT;
                                             CoverImageUploadState = STATE_INIT;
                                             IsChannelCreateRunning = false;
                                             Application.Current.Dispatcher.BeginInvoke(delegate
                                             {
                                                 ImageBehavior.SetAnimatedSource(imgControl, null);
                                                 imgControl.Source = null;
                                                 if (status)
                                                 {
                                                     ChannelModel channelModel = null;
                                                     if (chanlDTO != null)
                                                     {
                                                         ChannelHelpers.AddIntoMyChannelList(chanlDTO);
                                                         channelModel = new ChannelModel(chanlDTO);
                                                     }
                                                     //UCMiddlePanelSwitcher.View_UCMyChannel._BackChannelList.Add(ChannelConstants._BACK_TO_CREATE_CHANNEL);
                                                     UCMiddlePanelSwitcher.View_UCMyChannel.Navigate(MyChannelConstants.TypeMyChannelMainPanel, channelModel);

                                                 }
                                                 else if (!DefaultSettings.IsInternetAvailable) UIHelperMethods.ShowWarning(NotificationMessages.INTERNET_UNAVAILABLE);
                                                 else UIHelperMethods.ShowFailed("Channel Creation Failed!");
                                             }, System.Windows.Threading.DispatcherPriority.Send);
                                             return 0;
                                         }).Start();
                };
                if (alreadyRequested == false && _UploadStatusList.Count == 0)
                {
                    _UploadComplte();
                }
            }
            catch (Exception ex)
            {
                ProfileImageUploadState = STATE_INIT;
                CoverImageUploadState = STATE_INIT;
                IsChannelCreateRunning = false;
                log.Error(ex.Message + ex.StackTrace);
            }
        }

        private void OnCoverImageChange(object param)
        {
            try
            {
                int type = int.Parse(param.ToString());
                if (type == STATE_BROWSE)
                {
                    OpenFileDialog dialog = new OpenFileDialog();
                    dialog.Title = "Select a picture";
                    dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
                    if ((bool)dialog.ShowDialog())
                    {
                        Bitmap bmp = ImageUtility.GetBitmapOfDynamicResource(dialog.FileName);
                        if (bmp.Width < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH || bmp.Height < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT)
                        {
                            //  CustomMessageBox.ShowError("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                            UIHelperMethods.ShowWarning("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                        }
                        else
                        {
                            CoverImageUploadState = STATE_BROWSE;
                            _CoverImageUrl = string.Empty;
                            _BitmapCoverImage = ImageUtility.GetResizedImageToFit(bmp, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                            _CoverImageWidth = _BitmapCoverImage.Width;
                            _CoverImageHeight = _BitmapCoverImage.Height;
                            _UiImage = new System.Windows.Controls.Image { Source = _BitmapCoverImage };

                            double left = (coverImgCanvus.Width - _CoverImageWidth) / 2;
                            Canvas.SetLeft(_UiImage, left);
                            double top = (coverImgCanvus.Height - _CoverImageHeight) / 2;
                            Canvas.SetTop(_UiImage, top);
                            coverImgCanvus.Children.Clear();
                            coverImgCanvus.Children.Add(_UiImage);
                        }
                    }
                }
                else if (type == STATE_UPLOADED)
                {
                    //Save Change
                    //UploadAndSaveCoverImage();
                }
                else if (type == STATE_INIT)
                {
                    //Cancel Change
                    ResetCoverImageChange();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCoverImageChange() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UploadAndSaveCoverImage()
        {
            try
            {
                Bitmap btmap = ImageUtility.ConvertBitmapImageToBitmap(_BitmapCoverImage);
                byte[] byteArray = ImageUtility.ConvertBitmapToByteArray(btmap);
                dynamic data = new ExpandoObject();
                data.Bitmap = btmap;
                data.ByteArray = byteArray;
                if (_CoverImageUploadWorker.IsBusy != true)
                {
                    _CoverImageUploadWorker.RunWorkerAsync(data);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UploadAndSaveCoverImage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ResetCoverImageChange()
        {
            _BitmapCoverImage = null;
            _CoverImageUrl = string.Empty;
            CoverImageUploadState = STATE_INIT;
            SetDefaultCoverImage();
            _CropCoverImageX = 0;
            _CropCoverImageY = 0;
            _CoverImgMouseCapturePosition = new System.Windows.Point(-1, -1);
        }

        private void OnProfileImageChangeCommand(object param)
        {
            try
            {
                int type = int.Parse(param.ToString());
                if (type == 1)
                {
                    OpenFileDialog op = new OpenFileDialog();
                    op.Title = "Select a picture";
                    op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                      "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                      "Portable Network Graphic (*.png)|*.png";

                    if ((bool)op.ShowDialog())
                    {
                        Bitmap bmp = ImageUtility.GetBitmapOfDynamicResource(op.FileName);
                        if (bmp.Width < RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH || bmp.Height < RingIDSettings.PROFILE_PIC_MINIMUM_HEIGHT)
                            //  CustomMessageBox.ShowError("Profile photo's minimum resulotion: " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH + " x " + RingIDSettings.PROFILE_PIC_MINIMUM_HEIGHT);
                            UIHelperMethods.ShowWarning("Cover photo's minimum resulotion: " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH + " x " + RingIDSettings.PROFILE_PIC_MINIMUM_HEIGHT);
                        else
                        {
                            ProfileImageUploadState = STATE_BROWSE;
                            _ProfileImageUrl = string.Empty;
                            _BitmapProfileImage = ImageUtility.GetResizedImageToFit(bmp, RingIDSettings.PROFILE_PIC_SCREEN_WIDTH, RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT);
                            _ProImgControl.Source = _BitmapProfileImage;

                            _ProfileImageWidth = _BitmapProfileImage.Width;
                            _ProfileImageHeight = _BitmapProfileImage.Height;
                            double left = (PRO_PIC_UPLOAD_SCREEN_WIDTH - _ProfileImageWidth) / 2;
                            Canvas.SetLeft(_ProImgControl, left);
                            double top = (PRO_PIC_UPLOAD_SCREEN_HEIGHT - _ProfileImageHeight) / 2;
                            Canvas.SetTop(_ProImgControl, top);
                        }
                    }
                }
                //else if (type == 2)
                //{
                //    //WEBCAM Image
                //    WNWebcamCapture.Instance.ShowWindow((f) =>
                //    {
                //        _BitmapImage = ImageUtility.GetBitmapImageOfDynamicResource(f);
                //        GroupImageUploadState = STATE_SAVE;
                //        _ImgControl.Source = _BitmapImage;
                //        ResetGroupImageChange(_BitmapImage.Width, _BitmapImage.Height);
                //    });
                //}
                else if (type == 3)
                {
                    //Save Change
                    //UploadAndSaveProfileImage();
                }
                else if (type == 4)
                {
                    //Cancel Change
                    CancelProfileImageChange();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnProfileImageChangeCommand() ==> " + ex.StackTrace);
            }
        }

        private void UploadAndSaveProfileImage()
        {
            try
            {
                Bitmap btmap = ImageUtility.ConvertBitmapImageToBitmap(_BitmapProfileImage);
                byte[] byteArray = ImageUtility.ConvertBitmapToByteArray(btmap);
                dynamic data = new ExpandoObject();
                data.Bitmap = btmap;
                data.ByteArray = byteArray;
                if (_ProfileImageUploadWorker.IsBusy != true)
                {
                    _ProfileImageUploadWorker.RunWorkerAsync(data);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UploadAndSaveProfileImage() ==> " + ex.StackTrace);
            }
        }

        private void CancelProfileImageChange()
        {
            ProfileImageUploadState = STATE_INIT;
            _BitmapProfileImage = null;
            _ProfileImageUrl = string.Empty;
            _ProImgControl.Source = null;
            ImageBehavior.SetAnimatedSource(_ProImgControl, null);
            SetDefaultProfileImage();
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Event Handler

        #region Category

        private void rtbTitle_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textTitle = new TextRange(rtbTitle.Document.ContentStart, rtbTitle.Document.ContentEnd).Text;
            ChannelTitle = textTitle.Replace(Environment.NewLine, "");
        }

        private void rtbdescription_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textDescription = new TextRange(rtbdescription.Document.ContentStart, rtbdescription.Document.ContentEnd).Text;
            ChannelDescription = textDescription.Replace(Environment.NewLine, "");
        }

        private void rtbTitle_KeyDown(object sender, KeyEventArgs e)
        {
            var textTitle = new TextRange(rtbTitle.Document.ContentStart, rtbTitle.Document.ContentEnd).Text;
            if (textTitle.Length >= 150)
            {
                e.Handled = true;
                SystemSounds.Asterisk.Play();
                UIHelperMethods.ShowWarning("Channel Title is too large!");
                //CustomMessageBox.ShowError("Channel Title is too large!");
                return;
            }
        }

        private void rtbdescription_KeyDown(object sender, KeyEventArgs e)
        {
            var textDescription = new TextRange(rtbdescription.Document.ContentStart, rtbdescription.Document.ContentEnd).Text;
            if (textDescription.Length >= 250)
            {
                e.Handled = true;
                SystemSounds.Asterisk.Play();
                UIHelperMethods.ShowWarning("Channel description is too large!");
                // CustomMessageBox.ShowError("Channel Description is too large!");
                return;
            }
        }

        private void comboboxCategoryList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _SelectedCategoryList.Clear();
            if ((sender as ComboBox).SelectedItem is ChannelCategoryModel)
            {
                ChannelCategoryModel ctgModel = (ChannelCategoryModel)(sender as ComboBox).SelectedItem;
                _SelectedCategoryList.Add(new ChannelCategoryDTO { CategoryID = ctgModel.CategoryID, CategoryName = ctgModel.CategoryName });
            }
        }

        private void comboboxCountryList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem is CountryCodeModel)
            {
                _SelectedCountry = ((CountryCodeModel)(sender as ComboBox).SelectedItem).CountryName;
            }
        }

        #endregion

        #region Profile

        private void UIElement_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender is System.Windows.Controls.Image)
            {
                _ProImgControl = (System.Windows.Controls.Image)sender;
                SetDefaultProfileImage();
            }
            else if (sender is Canvas)
            {
                _ProfileImgCanvas = (Canvas)sender;
            }
        }

        private void proImgCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (ProfileImageUploadState == STATE_BROWSE && this.isProfileImageClicked)
                {
                    System.Windows.Point position = e.GetPosition(_ProfileImgCanvas);
                    Vector offset = position - _ProImageMousePosition;
                    _ProImageMousePosition = position;
                    double MinX = 0;
                    double MinY = 0;
                    double MaxX = (-_ProfileImageWidth + PRO_PIC_UPLOAD_SCREEN_WIDTH);
                    double MaxY = (-_ProfileImageHeight + PRO_PIC_UPLOAD_SCREEN_HEIGHT);

                    double imgL = Canvas.GetLeft(_ProImgControl);
                    double imgR = Canvas.GetTop(_ProImgControl);

                    if ((Canvas.GetLeft(_ProImgControl) + offset.X) <= MinX
                        && (Canvas.GetLeft(_ProImgControl) + offset.X) >= MaxX
                        && (Canvas.GetTop(_ProImgControl) + offset.Y) <= MinY
                        && (Canvas.GetTop(_ProImgControl) + offset.Y) >= MaxY)
                    {
                        _CropProfileX = Canvas.GetLeft(_ProImgControl) + offset.X;
                        _CropProfileY = Canvas.GetTop(_ProImgControl) + offset.Y;
                        Canvas.SetLeft(_ProImgControl, _CropProfileX);
                        Canvas.SetTop(_ProImgControl, _CropProfileY);
                        _ProfileImgCanvas.UpdateLayout();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: proImgCanvas_MouseMove() ==> " + ex.StackTrace);
            }
        }

        private void proImgCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_ProfileImageUploadState == STATE_BROWSE && _ProfileImgCanvas.CaptureMouse())
                {
                    this.isProfileImageClicked = true;
                    _ProImageMousePosition = e.GetPosition(_ProfileImgCanvas);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: proImgCanvas_MouseLeftButtonDown() ==> " + ex.StackTrace);
            }
        }

        private void proImgCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (ProfileImageUploadState == STATE_BROWSE)
                {
                    this.isProfileImageClicked = false;
                    _ProfileImgCanvas.ReleaseMouseCapture();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: proImgCanvas_MouseLeftButtonUp() ==> " + ex.StackTrace);
            }
        }

        private void Profile_Image_UploadWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                dynamic data = (ExpandoObject)e.Argument;
                data.Response = ProfileCoverImageUpload.Instance.UploadToImageServerWebRequest(data.ByteArray, SettingsConstants.TYPE_PROFILE_IMAGE, (int)-_CropProfileX, (int)-_CropProfileY, (int)_ProfileImageWidth, (int)_ProfileImageHeight);
                e.Result = data;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Profile_ImageUploadWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                dynamic data = (ExpandoObject)e.Result;
                Bitmap btmap = data.Bitmap;
                string _JsonResponse = data.Response;
                JObject jObj = JObject.Parse(_JsonResponse);

                string imageUrl = jObj != null && jObj[JsonKeys.ImageUrl] != null ? (string)jObj[JsonKeys.ImageUrl] : string.Empty;
                if (!string.IsNullOrEmpty(imageUrl))
                {
                    _ProfileImageUrl = (string)jObj[JsonKeys.ImageUrl];
                    btmap.Save(RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + ImageUrlHelper.GetImageNameFromUrl(imageUrl), ImageFormat.Jpeg);
                    //ProfileImageUploadState = STATE_UPLOADED;
                }
                else
                {
                    log.Info("Failed to Upload Channel Profile Image......=>");
                    //ResetProfileImageChange();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Profile_ImageUploadWorker_RunWorkerCompleted() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                ProfileImageUploadState = STATE_UPLOADED;
                _PreviousProImageUrl = _ProfileImageUrl;
                _UploadStatusList.Remove(ChannelConstants.CHANNEL_PRO_IMAGE);
                if (_UploadStatusList.Count == 0)
                {
                    this._UploadComplte();
                }
                if (this._UploadProPicComplete != null)
                {
                    this._UploadProPicComplete();
                }

                //_LoadingIcon = null;
                //_ImgControl.Source = null;
                //ImageBehavior.SetAnimatedSource(_ImgControl, null);
                //ProfileImageUploadState = ProfileImageUploadState == STATE_UPLOADED ? STATE_UPLOADED : STATE_INIT;

                //_ImgControl.Source = _BitmapProfileImage;
                //double left = (PRO_PIC_UPLOAD_SCREEN_WIDTH - _ProfileImageWidth) / 2;
                //Canvas.SetLeft(_ImgControl, _CropProfileX);
                //double top = (PRO_PIC_UPLOAD_SCREEN_HEIGHT - _ProfileImageHeight) / 2;
                // Canvas.SetTop(_ImgControl, _CropProfileY);
            }
        }

        #endregion

        #region CoverPhoto

        private void CoverImageCanvasMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_UiImage != null && coverImgCanvus.CaptureMouse())
                {
                    this.isCoverImageClicked = true;
                    _CoverImgMouseCapturePosition = e.GetPosition(coverImgCanvus);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageCanvasMouseLeftButtonDown() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverImageCanvasMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_UiImage != null)
                {
                    this.isCoverImageClicked = false;
                    coverImgCanvus.ReleaseMouseCapture();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageCanvasMouseLeftButtonUp() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverImageCanvasMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (_UiImage != null && this.isCoverImageClicked)
                {
                    var position = e.GetPosition(coverImgCanvus);
                    var offset = position - _CoverImgMouseCapturePosition;
                    _CoverImgMouseCapturePosition = position;
                    double MinX = 0;
                    double MinY = 0;
                    double MaxX = (-_CoverImageWidth + RingIDSettings.COVER_PIC_DISPLAY_WIDTH);
                    double MaxY = (-_CoverImageHeight + RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);

                    if ((Canvas.GetLeft(_UiImage) + offset.X) <= MinX
                        && (Canvas.GetLeft(_UiImage) + offset.X) >= MaxX
                        && (Canvas.GetTop(_UiImage) + offset.Y) <= MinY
                        && (Canvas.GetTop(_UiImage) + offset.Y) >= MaxY)
                    {
                        _CropCoverImageX = Canvas.GetLeft(_UiImage) + offset.X;
                        _CropCoverImageY = Canvas.GetTop(_UiImage) + offset.Y;
                        Canvas.SetLeft(_UiImage, _CropCoverImageX);
                        Canvas.SetTop(_UiImage, _CropCoverImageY);
                        coverImgCanvus.UpdateLayout();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageCanvasMouseMove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverImageUploadWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                dynamic data = (ExpandoObject)e.Argument;
                data.Response = ProfileCoverImageUpload.Instance.UploadToImageServerWebRequest(data.ByteArray, SettingsConstants.TYPE_COVER_IMAGE, (int)-_CropCoverImageX, (int)-_CropCoverImageY, (int)_CoverImageWidth, (int)_CoverImageHeight);
                e.Result = data;
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageUploadWorker_DoWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverImageUploadWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                dynamic data = (ExpandoObject)e.Result;
                Bitmap btmap = data.Bitmap;
                string _JsonResponse = data.Response;
                JObject jObj = JObject.Parse(_JsonResponse);
                string imageUrl = jObj != null && jObj[JsonKeys.ImageUrl] != null ? (string)jObj[JsonKeys.ImageUrl] : string.Empty;

                if (!string.IsNullOrEmpty(imageUrl))
                {
                    _CoverImageUrl = (string)jObj[JsonKeys.ImageUrl];
                    btmap.Save(RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + ImageUrlHelper.GetImageNameFromUrl(imageUrl), ImageFormat.Jpeg);

                    //_ChannelModel.CoverImage = imageUrl;
                    //imgCanvus.Children.Clear();
                    //double left = (imgCanvus.Width - _CoverImageWidth) / 2;
                    //Canvas.SetLeft(_UiImage, _CropCoverImageX);
                    //double top = (imgCanvus.Height - _CoverImageHeight) / 2;
                    //Canvas.SetTop(_UiImage, _CropCoverImageY);
                    //imgCanvus.Children.Add(_UiImage);
                }
                else
                {
                    log.Info("Failed to save Channel Cover Image......=>");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageUploadWorker_RunWorkerCompleted() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            finally
            {
                CoverImageUploadState = STATE_UPLOADED;
                _PreViousCoverImageUrl = _CoverImageUrl;
                _UploadStatusList.Remove(ChannelConstants.CHANNEL_COVER_IMAGE);
                if (_UploadStatusList.Count == 0)
                {
                    this._UploadComplte();
                }
            }
        }

        #endregion

        #endregion Event Handler

        #region Property

        public string ChannelTitle
        {
            get { return _ChannelTitle; }
            set
            {
                if (value == _ChannelTitle) return;
                _ChannelTitle = value;
                this.OnPropertyChanged("ChannelTitle");
            }
        }

        public string ChannelDescription
        {
            get { return _ChannelDescription; }
            set
            {
                if (value == _ChannelDescription) return;
                _ChannelDescription = value;
                this.OnPropertyChanged("ChannelDescription");
            }
        }

        public int CoverImageUploadState
        {
            get { return _CoverImageUploadState; }
            set
            {
                if (value == _CoverImageUploadState) return;
                _CoverImageUploadState = value;
                this.OnPropertyChanged("CoverImageUploadState");
            }
        }

        public int ProfileImageUploadState
        {
            get { return _ProfileImageUploadState; }
            set
            {
                if (value == _ProfileImageUploadState) return;
                _ProfileImageUploadState = value;
                this.OnPropertyChanged("ProfileImageUploadState");
            }
        }

        public bool IsChannelCreateRunning
        {
            get { return _IsChannelCreateRunning; }
            set
            {
                if (value == _IsChannelCreateRunning) return;
                _IsChannelCreateRunning = value;
                this.OnPropertyChanged("IsChannelCreateRunning");
            }
        }

        public ObservableCollection<CountryCodeModel> CountryModelList
        {
            get { return _CountryModelList; }
            set
            {
                _CountryModelList = value;
                this.OnPropertyChanged("CountryModelList");
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (_SaveCommand == null)
                {
                    _SaveCommand = new RelayCommand(param => OnSaveCommand());
                }
                return _SaveCommand;
            }
        }

        public ICommand CoverImageChangeCommand
        {
            get
            {
                if (_CoverImageChangeCommand == null)
                {
                    _CoverImageChangeCommand = new RelayCommand((param) => OnCoverImageChange(param));
                }
                return _CoverImageChangeCommand;
            }
        }

        public ICommand ProfileImageChangeCommand
        {
            get
            {
                if (_ProfileImageChangeCommand == null)
                {
                    _ProfileImageChangeCommand = new RelayCommand((param) => OnProfileImageChangeCommand(param));
                }
                return _ProfileImageChangeCommand;
            }
        }

        #endregion Property

    }
}




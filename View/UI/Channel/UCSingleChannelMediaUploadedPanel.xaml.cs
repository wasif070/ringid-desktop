﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;
using View.Utility;
using View.Utility.Channel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCSingleChannelMediaUploadedPanel.xaml
    /// </summary>
    public partial class UCSingleChannelMediaUploadedPanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleChannelMediaUploadedPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private bool disposed = false;

        public UCSingleChannelMediaUploadedPanel()
        {
            InitializeComponent();
        }

        ~UCSingleChannelMediaUploadedPanel()
        {
           Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty ChannelIDProperty = DependencyProperty.Register("ChannelID", typeof(Guid), typeof(UCSingleChannelMediaUploadedPanel));

        public Guid ChannelID
        {
            get { return (Guid)GetValue(ChannelIDProperty); }
            set
            {
                SetValue(ChannelIDProperty, value);
            }
        }

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleChannelMediaUploadedPanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleChannelMediaUploadedPanel panel = ((UCSingleChannelMediaUploadedPanel)o);
                object data = panel.DataContext;

                if (e.NewValue != null && (bool)e.NewValue)
                {
                    panel.BindPreview((ChannelMediaModel)data);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindPreview(ChannelMediaModel model)
        {
            MultiBinding sourceBinding = new MultiBinding { Converter = new ChannelMediaImageConverter() };
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath(""),
            });
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ThumbImageUrl"),
            });
            imgMediaThumb.SetBinding(Image.SourceProperty, sourceBinding);
            lblTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Title") });
            lblTitle.SetBinding(TextBlock.ToolTipProperty, new Binding { Path = new PropertyPath("Title") });
            lblDuation.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Duration"), Converter = new DurationTextConverter(), ConverterParameter = 1 });

            mnuAddToPeningList.Click += mnuAddToPeningList_Click;
            mnuRemove.Click += mnuRemove_Click;
        }

        private void ClearPreview()
        {
            imgMediaThumb.ClearValue(Image.SourceProperty);
            imgMediaThumb.Source = null;
            lblTitle.ClearValue(TextBlock.TextProperty);
            lblTitle.ClearValue(TextBlock.TextProperty);
            lblDuation.ClearValue(TextBlock.TextProperty);
            mnuAddToPeningList.Click -= mnuAddToPeningList_Click;
            mnuRemove.Click -= mnuRemove_Click;
        }

        private void mnuAddToPeningList_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelMediaModel mediaModel = ((ChannelMediaModel)this.DataContext);
                List<Guid> channelMediaIds = new List<Guid>();
                channelMediaIds.Add(mediaModel.MediaID);

                new ThrdUpdateMediaStatus(ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_PENDING, null, (status) =>
                {
                    if (status)
                    {
                        UIHelperMethods.ShowMessageWithTimerFromThread(null, "Media is added to pending list successfully.");
                    }
                    else
                    {
                        Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            UIHelperMethods.ShowFailed("Failed to add media pending list!");
                        });
                    }
                    return 0;
                }).Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuAddToPeningList_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mnuRemove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChannelMediaModel mediaModel = ((ChannelMediaModel)this.DataContext);
                List<Guid> channelMediaIds = new List<Guid>();
                channelMediaIds.Add(mediaModel.MediaID);

                new ThrdDeleteUploadedChannelMedia(channelMediaIds, (status) =>
                {
                    if (status)
                    {
                        mediaModel.IsViewOpened = false;
                        ChannelViewModel.Instance.GetChannelUploadedMediaList(mediaModel.MediaType).InvokeRemove(mediaModel);
                    }
                    else
                    {
                        Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            UIHelperMethods.ShowFailed("Failed to remove media from uploaded list!");
                        });
                    }
                    return 0;
                }).Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuRemove_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    if (disposing)
                    {
                        this.ClearPreview();
                        this.imgMediaThumb = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

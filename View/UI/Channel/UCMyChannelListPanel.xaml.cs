﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.Constants;
using View.Converter;
using View.Utility;
using View.Utility.Channel;
using View.Utility.GIF;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelListPanel.xaml
    /// </summary>
    public partial class UCMyChannelListPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelListPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private DispatcherTimer _WindowResizeTimer = null;
        private DispatcherTimer _ScrollChangeTimer = null;

        private bool _IS_AT_TOP = true;
        private bool _IS_AT_BOTTOM = false;

        #region Constructor

        static UCMyChannelListPanel()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCMyChannelListPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.itcChannelList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.MyChannelList"), Source = RingIDSettings });
        }

        #endregion Constructor

        #region Event Handler

        private static void OnLoadingStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d != null && e.NewValue != null)
                {
                    UCMyChannelListPanel panel = ((UCMyChannelListPanel)d);
                    int loadingStatus = (int)e.NewValue;

                    switch (loadingStatus)
                    {
                        case StatusConstants.NO_DATA:
                            log.Debug("*********************    NO_DATA                 *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.NO_DATA_AND_LOADING:
                            log.Debug("*********************    NO_DATA_AND_LOADING     *********************");
                            panel.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            break;
                        case StatusConstants.HAS_DATA:
                            log.Debug("*********************    HAS_DATA                *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.HAS_DATA_AND_LOADING:
                            log.Debug("*********************    HAS_DATA_AND_LOADING    *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLoadingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int count = this.itcChannelList.Items.Count;
            int topLimit = 300;
            int bottomLimit = count > 5 ? 400 : (count > 3 ? 200 : 140);

            if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
            {
                if (!ChannelViewModel.Instance.LoadStatusModel.IsMyChannelLoading && e.ExtentHeight > e.ViewportHeight)
                {
                    if (e.VerticalChange < 0 && e.VerticalOffset <= topLimit && this._IS_AT_TOP == false)
                    {
                        //Debug.WriteLine("IS_AT_TOP");
                        ChannelHelpers.LoadMyChannelList(0, true);
                    }
                    else if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_AT_BOTTOM == false)
                    {
                        //Debug.WriteLine("IS_AT_BOTTOM");
                        ChannelHelpers.LoadMyChannelList(0);
                    }
                }
                this.ChangeScrollOpenStatusOnScroll();
            }

            if (e.ExtentHeight > e.ViewportHeight)
            {
                this._IS_AT_TOP = e.VerticalOffset <= topLimit;
                this._IS_AT_BOTTOM = (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit);
            }
            else
            {
                this._IS_AT_TOP = true;
                this._IS_AT_BOTTOM = false;
            }
        }

        public void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.ChangeScrollOpenStatusOnResize();
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            
            //if (ChannelViewModel.Instance.MyChannelList.Count == 0)
            //{
            //    BitmapImage _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
            //    this.imgControl.Source = _LoadingIcon;
            //    ImageBehavior.SetAnimatedSource(this.imgControl, _LoadingIcon);
            //}
            //ChannelHelpers.LoadMyChannelList(0, true, 10, (status) =>
            //{
            //    if (status == false && ChannelViewModel.Instance.MyChannelList.Count == 0)
            //    {
            //        IsNoChannelFound = true;
            //    }
            //    Application.Current.Dispatcher.BeginInvoke(delegate
            //    {
            //        ImageBehavior.SetAnimatedSource(imgControl, null);
            //        imgControl.Source = null;
            //    });
            //    return 0;
            //});

            ChannelHelpers.LoadMyChannelList(0, true);

            this.itcChannelList.SizeChanged += ScrlViewer_SizeChanged;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.ChangeScrollOpenStatus();
            }, DispatcherPriority.ApplicationIdle);

            MultiBinding loadingStatusBinding = new MultiBinding { Converter = new LoadingStatusConverter() };
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.MyChannelList.Count"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsMyChannelLoading"), Source = RingIDSettings });
            this.SetBinding(UCMyChannelListPanel.LoadingStatusProperty, loadingStatusBinding);
        }

        public void ReleaseViewer()
        {
            if (this._WindowResizeTimer != null) this._WindowResizeTimer.Stop();
            if (this._ScrollChangeTimer != null) this._ScrollChangeTimer.Stop();

            this.itcChannelList.SizeChanged -= ScrlViewer_SizeChanged;
            this.ClearValue(UCMyChannelListPanel.LoadingStatusProperty);

            Task.Factory.StartNew(() =>
            {
                ChannelViewModel.Instance.MyChannelList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
            });

            this._IS_AT_TOP = true;
            this._IS_AT_BOTTOM = false;
        }

        private void ChangeScrollOpenStatusOnResize()
        {
            try
            {
                if (this._WindowResizeTimer == null)
                {
                    this._WindowResizeTimer = new DispatcherTimer();
                    this._WindowResizeTimer.Interval = TimeSpan.FromMilliseconds(350);
                    this._WindowResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._WindowResizeTimer.Stop();
                    };
                }

                if (this._ScrollChangeTimer != null && this._ScrollChangeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Stop();
                }
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnScroll()
        {
            try
            {
                if (this._ScrollChangeTimer == null)
                {
                    this._ScrollChangeTimer = new DispatcherTimer();
                    this._ScrollChangeTimer.Interval = TimeSpan.FromMilliseconds(30);
                    this._ScrollChangeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._ScrollChangeTimer.Stop();
                    };
                }
                this._ScrollChangeTimer.Stop();
                if (this._WindowResizeTimer == null || !this._WindowResizeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnScroll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatus()
        {
            try
            {
                int paddingTop = 51 + 45;
                UCMyChannelAndFollowingMainPanel parent = HelperMethods.FindVisualParent<UCMyChannelAndFollowingMainPanel>(this);
                if (parent != null)
                {
                    ChannelHelpers.ChangeChannelViewPortOpenedProperty(parent.ScrlViewer, itcChannelList, paddingTop);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty LoadingStatusProperty = DependencyProperty.Register("LoadingStatus", typeof(int), typeof(UCMyChannelListPanel), new PropertyMetadata(StatusConstants.NO_DATA, OnLoadingStatusChanged));

        public int LoadingStatus
        {
            get { return (int)GetValue(LoadingStatusProperty); }
            set
            {
                SetValue(LoadingStatusProperty, value);
            }
        }

        #endregion Property

    }
}

﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Utility;
using View.Utility.Channel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelAndFollowingMainPanel.xaml
    /// </summary>
    public partial class UCMyChannelAndFollowingMainPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelAndFollowingMainPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        public static UCMyChannelListPanel MyChannelListPanel = null;
        public static UCChannelFollowingPanel ChannelFollowingPanel = null;

        private int _TabType;
        private ICommand _TabChangeCommand;

        public UCMyChannelAndFollowingMainPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Utility Methods

        public void InilializeViewer()
        {
            switch (this.TabType)
            {
                case 0:
                    ShowMyChannelListView(true);
                    break;
                case 1:
                    ShowMyChannelFollowingView(true);
                    break;
                default:
                    {
                        this.TabType = 0;
                        ShowMyChannelListView(true);
                    }
                    break;
            }
        }

        public void ReleaseViewer()
        {
            Destroy(TabType);
        }

        private void OnTabChangeClicked(object param)
        {
            try
            {
                int type = Int32.Parse(param.ToString());
                bool isChanged = this.TabType != type;

                if (isChanged)
                {
                    Destroy(this.TabType);
                }
                this.TabType = type;

                switch (this.TabType)
                {
                    case 0:
                        ShowMyChannelListView(isChanged);
                        break;
                    case 1:
                        ShowMyChannelFollowingView(isChanged);
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnTabChangeClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Destroy(int type)
        {
            switch (type)
            {
                case 0:
                    if (MyChannelListPanel != null)
                    {
                        ScrlViewer.ScrollChanged -= MyChannelListPanel.ScrlViewer_ScrollChanged;
                        ScrlViewer.SizeChanged -= MyChannelListPanel.ScrlViewer_SizeChanged;
                        MyChannelListPanel.ReleaseViewer();
                    }
                    break;
                case 1:
                    if (ChannelFollowingPanel != null)
                    {
                        ScrlViewer.ScrollChanged -= ChannelFollowingPanel.ScrlViewer_ScrollChanged;
                        ScrlViewer.SizeChanged -= ChannelFollowingPanel.ScrlViewer_SizeChanged;
                        ChannelFollowingPanel.ReleaseViewer();
                    }
                    break;

            }
        }

        public void ShowMyChannelListView(bool isChanged)
        {
            try
            {
                if (MyChannelListPanel == null)
                {
                    MyChannelListPanel = new UCMyChannelListPanel();
                }

                if (isChanged)
                {
                    MyChannelListPanel.InilializeViewer();
                    ScrlViewer.ScrollChanged += MyChannelListPanel.ScrlViewer_ScrollChanged;
                    ScrlViewer.SizeChanged += MyChannelListPanel.ScrlViewer_SizeChanged;
                    bdrContentPanel.Child = MyChannelListPanel;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMyChannelListView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }



        public void ShowMyChannelFollowingView(bool isChanged)
        {
            try
            {
                if (ChannelFollowingPanel == null)
                {
                    ChannelFollowingPanel = new UCChannelFollowingPanel();
                }

                if (isChanged)
                {
                    ChannelFollowingPanel.InilializeViewer();
                    ScrlViewer.ScrollChanged += ChannelFollowingPanel.ScrlViewer_ScrollChanged;
                    ScrlViewer.SizeChanged += ChannelFollowingPanel.ScrlViewer_SizeChanged;
                    bdrContentPanel.Child = ChannelFollowingPanel;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMyChannelFollowingView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {

        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods


        #region Property

        public int TabType
        {
            get { return _TabType; }
            set
            {
                if (_TabType == value)
                    return;

                _TabType = value;
                this.OnPropertyChanged("TabType");
            }
        }

        public ICommand TabChangeCommand
        {
            get
            {
                if (_TabChangeCommand == null)
                {
                    _TabChangeCommand = new RelayCommand(param => OnTabChangeClicked(param));
                }
                return _TabChangeCommand;
            }
        }

        #endregion

    }
}

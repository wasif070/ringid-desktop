﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp.Stream;
using View.Utility;
using View.Utility.Channel;
using View.Utility.Chat.Service;
using View.Utility.GIF;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelPendingPanel.xaml
    /// </summary>
    public partial class UCMyChannelPendingPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelPlaylistPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<ChannelMediaModel> _PendingMediaList = new ObservableCollection<ChannelMediaModel>();
        private bool _IsPendingListLoaded;
        private bool _IsDataFound;
        private bool _IsPublishRunning = false;
        private bool _IsSelectButtonEnable = true;
        private bool _IsDeletePublishEnable = false;
        private bool _IS_PENDING_LOADING { get; set; }
        private int _StartLimit;

        public ChannelModel _SingleChannelInfoModel = new ChannelModel();
        private ChannelModel _PrevChannelInfoModel { get; set; }
        private List<ChannelMediaModel> _SelectedMediaList = new List<ChannelMediaModel>();
        private ICommand _SelectAllCommand;
        private ICommand _CancelCommand;
        private ICommand _PublishCommand;
        private ICommand _MediaPlayCommand;

        public UCMyChannelPendingPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            PendingMediaList.CollectionChanged += PendingList_CollectionChanged;
        }

        #region Utility

        public void InilializeViewer()
        {
            this.itcntrlMedia.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("PendingMediaList") });
            PendingMediaList.CollectionChanged += PendingList_CollectionChanged;
            if (_SingleChannelInfoModel != null && !(_PrevChannelInfoModel != null && _PrevChannelInfoModel.ChannelID == _SingleChannelInfoModel.ChannelID))
            {
                _PrevChannelInfoModel = _SingleChannelInfoModel;
                BitmapImage _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                imgControl.Source = _LoadingIcon;
                ImageBehavior.SetAnimatedSource(imgControl, _LoadingIcon);
            }
        }

        public void ReleaseViewer()
        {
            this.itcntrlMedia.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcntrlMedia.ItemsSource = null;
            //_SelectedMediaList.Clear();
        }

        public void Dispose()
        {
            PendingMediaList.Clear();
        }

        private void SetDefaultData()
        {
            IsPendingListLoaded = false;
            _SelectedMediaList.Clear();
            PendingMediaList.Clear();
            _StartLimit = 0;
        }

        public void GetPendingMediaList()
        {
            SetDefaultData();
            if (!(UCMyChannelMainPanel.MyChannelPlaylistPanel != null && UCMyChannelMainPanel.MyChannelPlaylistPanel.MediaPlayedList.Count > 0))
            {
                ChannelHelpers.LoadChannelPlayList(_SingleChannelInfoModel.ChannelID, false, 5, (status) =>
                {
                    LoadPendingList();
                    return 0;
                });
            }
            else
            {
                LoadPendingList();
            }
        }

        private void LoadPendingList(int offset = 15)
        {
            int startLimit = PendingMediaList.Count;
            Guid pivot = startLimit > 0 ? PendingMediaList.ElementAt(PendingMediaList.Count - 1).ChannelID : Guid.NewGuid();

            this._IS_PENDING_LOADING = true;
            new ThrdGetChannelMediaList(_SingleChannelInfoModel.ChannelID, _SingleChannelInfoModel.ChannelType, startLimit, pivot, offset, ChannelConstants.MEDIA_STATUS_PENDING, (isSucces) =>
            {
                if (isSucces)
                {
                    IsSelectButtonEnable = true;
                    IsDeletePublishEnable = false;
                }
                this._IS_PENDING_LOADING = false;
                IsPendingListLoaded = true;
                Application.Current.Dispatcher.BeginInvoke(delegate
                {
                    imgControl.Source = null;
                    ImageBehavior.SetAnimatedSource(imgControl, null);
                });
                return 0;
            }).Start();
        }

        private void OnMediaPlayCommand(object param)
        {
            ChannelMediaModel item = (ChannelMediaModel)param;
            int index = PendingMediaList.IndexOf(item);
            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
            foreach (ChannelMediaModel mediaModel in PendingMediaList)
            {
                SingleMediaModel singleMediaModel = new SingleMediaModel();
                singleMediaModel.StreamUrl = mediaModel.MediaUrl;
                singleMediaModel.ContentId = mediaModel.MediaID;
                singleMediaModel.MediaOwner = new BaseUserProfileModel();
                singleMediaModel.MediaOwner.UserTableID = mediaModel.OwnerID;
                singleMediaModel.Title = mediaModel.Title;
                singleMediaModel.Duration = mediaModel.Duration / 1000;
                singleMediaModel.Artist = mediaModel.Artist;
                singleMediaModel.MediaType = 2;
                singleMediaModel.ThumbUrl = mediaModel.ThumbImageUrl;
                singleMediaModel.IsFromLocalDirectory = false;
                singleMediaModel.ChannelId = mediaModel.ChannelID;
                MediaList.Add(singleMediaModel);
            }
            MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, index);
        }

        private void AdjustPublishingTimeAndDate(ChannelMediaModel mediaModel)
        {
            ChannelMediaModel latestScheduledModel = null;
            if (UCMyChannelMainPanel.MyChannelPlaylistPanel != null && UCMyChannelMainPanel.MyChannelPlaylistPanel.MediaPlayedList.Count > 0 && _SelectedMediaList.Count == 0)
            {
                latestScheduledModel = UCMyChannelMainPanel.MyChannelPlaylistPanel.MediaPlayedList.FirstOrDefault();
            }
            else
            {
                latestScheduledModel = _SelectedMediaList.LastOrDefault();
            }
            long currentTime = ChatService.GetServerTime();
            if (latestScheduledModel == null)
            {
                mediaModel.StartTime = currentTime + ChannelConstants.CHANNEL_MEDIA_DURATION_OFFSET_5_MIN;
                mediaModel.EndTime = mediaModel.StartTime + mediaModel.Duration;
            }
            else
            {
                long latestScheduledEndTime = latestScheduledModel.StartTime + latestScheduledModel.Duration;
                if (currentTime < (latestScheduledEndTime))// some media's are currently scheduled
                {
                    long playingRemaining = latestScheduledEndTime - currentTime;
                    long offset = playingRemaining > ChannelConstants.CHANNEL_MEDIA_DURATION_OFFSET_5_MIN ? 0 : ChannelConstants.CHANNEL_MEDIA_DURATION_OFFSET_5_MIN - playingRemaining;
                    //MediaStartTime = latestScheduledEndTime + offset;
                    mediaModel.StartTime = latestScheduledEndTime + offset;
                }
                else
                {
                    mediaModel.StartTime = currentTime + ChannelConstants.CHANNEL_MEDIA_DURATION_OFFSET_5_MIN;
                }
                //MediaEndTime = _MediaStartTime + mediaModel.Duration;
                mediaModel.EndTime = mediaModel.StartTime + mediaModel.Duration;
            }
        }


        private void OnSelectAllCommand()
        {
            IsSelectButtonEnable = false;
            foreach (ChannelMediaModel mediaModel in PendingMediaList)
            {
                mediaModel.IsChecked = true;
            }
        }
        private void OnCancelCommand()
        {
            IsSelectButtonEnable = true;
            IsDeletePublishEnable = false;
            foreach (ChannelMediaModel mediaModel in PendingMediaList)
            {
                mediaModel.IsChecked = false;
            }
        }

        private void OnPublishCommand()
        {
            if (_SelectedMediaList.Count > 0)
            {
                List<Guid> channelMediaIds = new List<Guid>();
                foreach (ChannelMediaModel mediaModel in _SelectedMediaList)
                {
                    //if (mediaModel.StartTime < ChatService.GetServerTime())
                    //{
                    //    AdjustPublishingTimeAndDate(mediaModel);
                    //}
                    channelMediaIds.Add(mediaModel.MediaID);
                }

                bool prevSelectBtnStatus = IsSelectButtonEnable;
                bool prevDeletePublishBtnStatus = IsDeletePublishEnable;
                IsPublishRunning = true;
                IsSelectButtonEnable = false;
                IsDeletePublishEnable = false;

                int offset = 15 - (PendingMediaList.Count - channelMediaIds.Count);
                bool isAtLeastOnePublished = UCMyChannelMainPanel.MyChannelPlaylistPanel != null && UCMyChannelMainPanel.MyChannelPlaylistPanel.MediaPlayedList.Count > 0;
                long mediaStartTime = _SelectedMediaList.ElementAt(0).StartTime;
                if (isAtLeastOnePublished == false)
                {
                    UCChannelStartTimePopUp channelStartTimePopUp = new UCChannelStartTimePopUp(gridParent);
                    channelStartTimePopUp.ShowPopup((publishTimeInMilli) =>
                    {
                        if (publishTimeInMilli > 0)
                        {
                            PublishMedia(channelMediaIds, isAtLeastOnePublished, offset, prevSelectBtnStatus, prevDeletePublishBtnStatus, publishTimeInMilli);
                        }
                        else
                        {
                            IsSelectButtonEnable = prevSelectBtnStatus;
                            IsDeletePublishEnable = prevDeletePublishBtnStatus;
                        }
                        return 0;
                    });
                }
                else
                {
                    PublishMedia(channelMediaIds, isAtLeastOnePublished, offset, prevSelectBtnStatus, prevDeletePublishBtnStatus, 0);
                }
            }
            else
            {
                UIHelperMethods.ShowWarning("Please select media to publish");
            }
        }

        private void PublishMedia(List<Guid> channelMediaIds, bool isAtLeastOnePublished, int offset, bool prevSelectBtnStatus, bool prevDeletePublishBtnStatus, long publishTimeInMilli)
        {
            new ThrdUpdateMediaStatus(_SingleChannelInfoModel.ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_PUBLISHED, (isAtLeastOnePublished ? null : (long?)publishTimeInMilli), (status) =>
            {
                IsPublishRunning = false;
                if (status)
                {
                    foreach (ChannelMediaModel mediaModel in _SelectedMediaList)
                    {
                        mediaModel.MediaStatus = ChannelConstants.MEDIA_STATUS_PUBLISHED;
                        PendingMediaList.InvokeRemove(mediaModel);
                        //if (UCMyChannelMainPanel.MyChannelPlaylistPanel != null)
                        //{
                        //    if (UCMyChannelMainPanel.MyChannelPlaylistPanel.MediaPlayedList.Where(P => P.MediaID == mediaModel.MediaID).FirstOrDefault() == null)
                        //    {
                        //        UCMyChannelMainPanel.MyChannelPlaylistPanel.MediaPlayedList.InvokeInsert(0, mediaModel);
                        //    }

                        //}
                        Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            UIHelperMethods.ShowMessageWithTimerFromNonThread(this.gripPopUpMessage, "Media published Successfully!");
                        });
                    }
                    if (offset > 0)
                    {
                        _StartLimit = PendingMediaList.Count;
                        Guid pivot = _StartLimit > 0 ? PendingMediaList.ElementAt(PendingMediaList.Count - 1).ChannelID : Guid.NewGuid();
                        new ThrdGetChannelMediaList(_SingleChannelInfoModel.ChannelID, _SingleChannelInfoModel.ChannelType, _StartLimit, pivot, offset, ChannelConstants.MEDIA_STATUS_PENDING, (succes) =>
                        {
                            if (succes)
                            {
                                IsSelectButtonEnable = true;
                                IsDeletePublishEnable = false;
                            }
                            else
                            {
                                IsSelectButtonEnable = true;
                                IsDeletePublishEnable = false;
                            }
                            this._IS_PENDING_LOADING = false;
                            IsPendingListLoaded = true;
                            _StartLimit += offset;
                            return 0;
                        }).Start();
                    }
                    else
                    {
                        IsSelectButtonEnable = true;
                        IsDeletePublishEnable = false;
                    }
                    _SelectedMediaList.Clear();
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        UIHelperMethods.ShowWarning("Failed to publish media");
                    });
                    //  CustomMessageBox.ShowError("Failed to publish media");

                    IsSelectButtonEnable = prevSelectBtnStatus;
                    IsDeletePublishEnable = prevDeletePublishBtnStatus;
                }

                return 0;
            }).Start();
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region EventHandler

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int count = this.itcntrlMedia.Items.Count;
            int bottomLimit = count > 5 ? 400 : (count > 3 ? 200 : 140);

            if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_PENDING_LOADING == false)
            {
                this.LoadPendingList(10);
            }
        }
        public void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        #endregion

        #region Property

        public ObservableCollection<ChannelMediaModel> PendingMediaList
        {
            get { return _PendingMediaList; }
            set
            {
                _PendingMediaList = value;
                this.OnPropertyChanged("PendingMediaList");
            }
        }

        public bool IsPendingListLoaded
        {
            get { return _IsPendingListLoaded; }
            set
            {
                if (value == _IsPendingListLoaded) return;
                _IsPendingListLoaded = value;
                this.OnPropertyChanged("IsPendingListLoaded");
            }
        }

        public bool IsPublishRunning
        {
            get { return _IsPublishRunning; }
            set
            {
                if (value == _IsPublishRunning) return;
                _IsPublishRunning = value;
                this.OnPropertyChanged("IsPublishRunning");
            }
        }

        public bool IsSelectButtonEnable
        {
            get { return _IsSelectButtonEnable; }
            set
            {
                if (value == _IsSelectButtonEnable) return;
                _IsSelectButtonEnable = value;
                this.OnPropertyChanged("IsSelectButtonEnable");
            }
        }
        public bool IsDeletePublishEnable
        {
            get { return _IsDeletePublishEnable; }
            set
            {
                if (value == _IsDeletePublishEnable) return;
                _IsDeletePublishEnable = value;
                this.OnPropertyChanged("IsDeletePublishEnable");
            }
        }

        public bool IsDataFound
        {
            get { return _IsDataFound; }
            set
            {
                if (value == _IsDataFound) return;
                _IsDataFound = value;
                this.OnPropertyChanged("IsDataFound");
            }
        }



        #endregion


        #region Command


        public ICommand SelectAllCommand
        {
            get
            {
                if (_SelectAllCommand == null)
                {
                    _SelectAllCommand = new RelayCommand(param => OnSelectAllCommand());
                }
                return _SelectAllCommand;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand(param => OnCancelCommand());
                }
                return _CancelCommand;
            }
        }

        public ICommand PublishCommand
        {
            get
            {
                if (_PublishCommand == null)
                {
                    _PublishCommand = new RelayCommand(param => OnPublishCommand());
                }
                return _PublishCommand;
            }
        }

        public ICommand MediaPlayCommand
        {
            get
            {
                if (_MediaPlayCommand == null)
                {
                    _MediaPlayCommand = new RelayCommand(param => OnMediaPlayCommand(param));
                }
                return _MediaPlayCommand;
            }
        }

        #endregion

        private void menuItem_Remove_Click(object sender, RoutedEventArgs e)
        {
            if (((MenuItem)sender).DataContext is ChannelMediaModel)
            {
                ChannelMediaModel mediaModel = (ChannelMediaModel)((MenuItem)sender).DataContext;
                List<Guid> channelMediaIds = new List<Guid>();
                channelMediaIds.Add(mediaModel.MediaID);
                new ThrdUpdateMediaStatus(mediaModel.ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_DELETED, null, (status) =>
                {
                    if (status)
                    {
                        PendingMediaList.InvokeRemove(mediaModel);
                        _SelectedMediaList.Remove(mediaModel);
                        if (_SelectedMediaList.Count == 0)
                        {
                            IsSelectButtonEnable = true;
                            IsDeletePublishEnable = false;
                        }
                    }
                    return 0;
                }).Start();
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            IsDeletePublishEnable = true;
            if (((CheckBox)sender).DataContext is ChannelMediaModel)
            {
                ChannelMediaModel mediaModel = (ChannelMediaModel)((CheckBox)sender).DataContext;
                mediaModel.IsChecked = true;
                //AdjustPublishingTimeAndDate(mediaModel);
                _SelectedMediaList.Add(mediaModel);
                if (_SelectedMediaList.Count == PendingMediaList.Count)
                {
                    IsSelectButtonEnable = false;
                }
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            IsSelectButtonEnable = true;
            if (((CheckBox)sender).DataContext is ChannelMediaModel)
            {
                ChannelMediaModel mediaModel = (ChannelMediaModel)((CheckBox)sender).DataContext;
                mediaModel.IsChecked = false;
                mediaModel.StartTime = 0;
                _SelectedMediaList.Remove(mediaModel);
                List<Guid> tempList = _SelectedMediaList.Select(P => P.MediaID).ToList();
                _SelectedMediaList.Clear();
                foreach (Guid mediaId in tempList)
                {
                    ChannelMediaModel model = PendingMediaList.Where(P => P.MediaID == mediaId).FirstOrDefault();
                    if (model != null)
                    {
                        model.StartTime = 0;
                        //AdjustPublishingTimeAndDate(model);
                        _SelectedMediaList.Add(model);
                    }
                }
                if (_SelectedMediaList.Count == 0)
                {
                    IsDeletePublishEnable = false;
                }
            }
        }

        private void PendingList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (PendingMediaList.Count > 0)
            {
                IsDataFound = true;
            }
            else
            {
                IsDataFound = false;
                IsSelectButtonEnable = true;
                IsDeletePublishEnable = false;
            }
        }

    }
}

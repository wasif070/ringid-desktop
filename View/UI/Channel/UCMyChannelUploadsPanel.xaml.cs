﻿using log4net;
using Models.Constants;
using System;
<<<<<<< HEAD
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
=======
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
<<<<<<< HEAD
using View.BindingModels;
using View.Utility;
using View.Utility.Channel;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelUploadsPanel.xaml
    /// </summary>
    public partial class UCMyChannelUploadsPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelUploadsPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private ChannelModel _SingleChannelInfoModel = new ChannelModel();
        private ICommand _AddToPendingCommand;
        private ICommand _MediaPlayCommand;
        private bool _IS_UPLOAD_LOADING { get; set; }

        #region Constructor

        public UCMyChannelUploadsPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int count = SingleChannelInfoModel.MediaList.Count;
            int bottomLimit = count > 5 ? 400 : (count > 3 ? 200 : 140);

            if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_UPLOAD_LOADING == false)
            {
                this._IS_UPLOAD_LOADING = true;
                int startLimit = _SingleChannelInfoModel.MediaList.Count;
                new ThrdGetChannelMediaList(_SingleChannelInfoModel.ChannelID, _SingleChannelInfoModel.ChannelType, startLimit, 10, ChannelConstants.MEDIA_STATUS_UPLOADED, (status) =>
                {
                    this._IS_UPLOAD_LOADING = false;
                    return 0;
                }).Start();
            }
        }

        public void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            this.itcntrlMedia.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("SingleChannelInfoModel.MediaList") });
        }

        public void ReleaseViewer()
        {
            this.itcntrlMedia.ClearValue(ItemsControl.ItemsSourceProperty);
            itcntrlMedia.ItemsSource = null;
        }

        public void Dispose()
        {

        }

        public void GetUploadedMediaList(int startIndex = 0)
        {
            if (_SingleChannelInfoModel != null)
            {
                this._IS_UPLOAD_LOADING = true;
                new ThrdGetChannelMediaList(_SingleChannelInfoModel.ChannelID, _SingleChannelInfoModel.ChannelType, startIndex, 10, ChannelConstants.MEDIA_STATUS_UPLOADED, (status) =>
                {
                    this._IS_UPLOAD_LOADING = false;
                    return 0;
                }).Start();
            }
        }

        private void OnMediaPlayCommand(object param)
        {
            ChannelMediaModel item = (ChannelMediaModel)param;
            int index = SingleChannelInfoModel.MediaList.IndexOf(item);
            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
            foreach (ChannelMediaModel mediaModel in SingleChannelInfoModel.MediaList)
            {
                SingleMediaModel singleMediaModel = new SingleMediaModel();
                singleMediaModel.StreamUrl = mediaModel.MediaUrl;
                singleMediaModel.ContentId = mediaModel.MediaID;
                singleMediaModel.MediaOwner = new BaseUserProfileModel();
                singleMediaModel.MediaOwner.UserTableID = mediaModel.OwnerID;
                singleMediaModel.Title = mediaModel.Title;
                singleMediaModel.Duration = mediaModel.Duration / 1000;
                singleMediaModel.Artist = mediaModel.Artist;
                singleMediaModel.MediaType = 2;
                singleMediaModel.ThumbUrl = mediaModel.ThumbImageUrl;
                singleMediaModel.IsFromLocalDirectory = false;
                singleMediaModel.ChannelId = mediaModel.ChannelID;
                MediaList.Add(singleMediaModel);
            }
            MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, index);
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods


        private void MenuItem_Add_To_Pending_Click(object sender, RoutedEventArgs e)
        {
            if (((MenuItem)sender).DataContext is ChannelMediaModel)
            {
                ChannelMediaModel mediaModel = (ChannelMediaModel)((MenuItem)sender).DataContext;
                List<Guid> channelMediaIds = new List<Guid>();
                channelMediaIds.Add(mediaModel.MediaID);
                new ThrdUpdateMediaStatus(mediaModel.ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_PENDING, null, (status) =>
=======
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Utility;
using View.Utility.Channel;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelUploadsPanel.xaml
    /// </summary>
    public partial class UCMyChannelUploadsPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelUploadsPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private ObservableCollection<ChannelMediaModel> _UploadedMediaList = null;
        private ChannelModel _ChannelInfoModel = null;

        private DispatcherTimer _WindowResizeTimer = null;
        private DispatcherTimer _ScrollChangeTimer = null;

        private bool _IS_AT_TOP = true;
        private bool _IS_AT_BOTTOM = false;

        #region Constructor

        static UCMyChannelUploadsPanel()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCMyChannelUploadsPanel(ChannelModel channelModel)
        {
            InitializeComponent();
            this.ChannelInfoModel = channelModel;
            this.UploadedMediaList = ChannelViewModel.Instance.GetChannelUploadedMediaList(channelModel.ChannelType);
            this.itcChannelMediaList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("UploadedMediaList")});
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private static void OnLoadingStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d != null && e.NewValue != null)
                {
                    UCMyChannelUploadsPanel panel = ((UCMyChannelUploadsPanel)d);
                    int loadingStatus = (int)e.NewValue;

                    switch (loadingStatus)
                    {
                        case StatusConstants.NO_DATA:
                            log.Debug("*********************    NO_DATA                 *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.NO_DATA_AND_LOADING:
                            log.Debug("*********************    NO_DATA_AND_LOADING     *********************");
                            panel.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            break;
                        case StatusConstants.HAS_DATA:
                            log.Debug("*********************    HAS_DATA                *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.HAS_DATA_AND_LOADING:
                            log.Debug("*********************    HAS_DATA_AND_LOADING    *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLoadingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int recentCount = this.itcChannelMediaList.Items.Count;
            int topLimit = recentCount < 4 ? 150 : 300;
            int bottomLimit = recentCount > 5 ? 400 : (recentCount > 3 ? 200 : 140);

            if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
            {
                if (!ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading && e.ExtentHeight > e.ViewportHeight)
                {
                    if (e.VerticalChange < 0 && e.VerticalOffset <= topLimit && this._IS_AT_TOP == false)
                    {
                        //Debug.WriteLine("IS_AT_TOP");
                    }
                    else if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_AT_BOTTOM == false)
                    {
                        //Debug.WriteLine("IS_AT_BOTTOM");
                        ChannelHelpers.LoadUploadedChannelMediaList(this.ChannelInfoModel.ChannelType);
                    }
                }
                this.ChangeScrollOpenStatusOnScroll();
            }

            if (e.ExtentHeight > e.ViewportHeight)
            {
                this._IS_AT_TOP = e.VerticalOffset <= topLimit;
                this._IS_AT_BOTTOM = (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit);
            }
            else
            {
                this._IS_AT_TOP = true;
                this._IS_AT_BOTTOM = false;
            }
        }

        public void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.ChangeScrollOpenStatusOnResize();
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            ChannelHelpers.LoadUploadedChannelMediaList(this.ChannelInfoModel.ChannelType, true, 20);

            this.itcChannelMediaList.SizeChanged += ScrlViewer_SizeChanged;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.ChangeScrollOpenStatus();
            }, DispatcherPriority.ApplicationIdle);

            MultiBinding loadingStatusBinding = new MultiBinding { Converter = new LoadingStatusConverter() };
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("UploadedMediaList.Count")});
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsUploadedMediaLoading"), Source = RingIDSettings });
            this.SetBinding(UCMyChannelUploadsPanel.LoadingStatusProperty, loadingStatusBinding);
        }

        public void ReleaseViewer()
        {
            if (this._WindowResizeTimer != null) this._WindowResizeTimer.Stop();
            if (this._ScrollChangeTimer != null) this._ScrollChangeTimer.Stop();

            this.itcChannelMediaList.SizeChanged -= ScrlViewer_SizeChanged;
            this.ClearValue(UCMyChannelUploadsPanel.LoadingStatusProperty);

            Task.Factory.StartNew(() =>
            {
                this.UploadedMediaList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
            });

            this._IS_AT_TOP = true;
            this._IS_AT_BOTTOM = false;
        }

        private void ChangeScrollOpenStatusOnResize()
        {
            try
            {
                if (this._WindowResizeTimer == null)
                {
                    this._WindowResizeTimer = new DispatcherTimer();
                    this._WindowResizeTimer.Interval = TimeSpan.FromMilliseconds(350);
                    this._WindowResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._WindowResizeTimer.Stop();
                    };
                }

                if (this._ScrollChangeTimer != null && this._ScrollChangeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Stop();
                }
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnScroll()
        {
            try
            {
                if (this._ScrollChangeTimer == null)
                {
                    this._ScrollChangeTimer = new DispatcherTimer();
                    this._ScrollChangeTimer.Interval = TimeSpan.FromMilliseconds(30);
                    this._ScrollChangeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._ScrollChangeTimer.Stop();
                    };
                }
                this._ScrollChangeTimer.Stop();
                if (this._WindowResizeTimer == null || !this._WindowResizeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnScroll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatus()
        {
            try
            {
                UCMyChannelMainPanel parent = HelperMethods.FindVisualParent<UCMyChannelMainPanel>(this);
                if (parent != null)
                {
                    int paddingTop = 51 + 45 + 10;
                    ChannelHelpers.ChangeChannelMediaViewPortOpenedProperty(parent.ScrlViewer, itcChannelMediaList, paddingTop, 3);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {

        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty LoadingStatusProperty = DependencyProperty.Register("LoadingStatus", typeof(int), typeof(UCMyChannelUploadsPanel), new PropertyMetadata(StatusConstants.NO_DATA, OnLoadingStatusChanged));

        public int LoadingStatus
        {
            get { return (int)GetValue(LoadingStatusProperty); }
            set
            {
                SetValue(LoadingStatusProperty, value);
            }
        }

        public ObservableCollection<ChannelMediaModel> UploadedMediaList
        {
            get { return _UploadedMediaList; }
            set
            {
                _UploadedMediaList = value;
                this.OnPropertyChanged("UploadedMediaList");
            }
        }

        public ChannelModel ChannelInfoModel
        {
            get { return _ChannelInfoModel; }
            set
            {
                if (_ChannelInfoModel == value)
                    return;

                _ChannelInfoModel = value;
                this.OnPropertyChanged("ChannelInfoModel");
            }
        }

        #endregion Property

        /*private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelUploadsPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private ChannelModel _SingleChannelInfoModel = new ChannelModel();
        private ICommand _AddToPendingCommand;
        private ICommand _MediaPlayCommand;
        private bool _IS_UPLOAD_LOADING { get; set; }

        #region Constructor

        public UCMyChannelUploadsPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int count = SingleChannelInfoModel.MediaList.Count;
            int bottomLimit = count > 5 ? 400 : (count > 3 ? 200 : 140);

            if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_UPLOAD_LOADING == false)
            {
                this._IS_UPLOAD_LOADING = true;
                int startLimit = _SingleChannelInfoModel.MediaList.Count;
                new ThrdGetChannelMediaList(_SingleChannelInfoModel.ChannelID, _SingleChannelInfoModel.ChannelType, startLimit, 10, ChannelConstants.MEDIA_STATUS_UPLOADED, (status) =>
                {
                    this._IS_UPLOAD_LOADING = false;
                    return 0;
                }).Start();
            }
        }

        public void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            this.itcntrlMedia.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("SingleChannelInfoModel.MediaList") });
        }

        public void ReleaseViewer()
        {
            this.itcntrlMedia.ClearValue(ItemsControl.ItemsSourceProperty);
            itcntrlMedia.ItemsSource = null;
        }

        public void Dispose()
        {

        }

        public void GetUploadedMediaList(int startIndex = 0)
        {
            if (_SingleChannelInfoModel != null)
            {
                this._IS_UPLOAD_LOADING = true;
                new ThrdGetChannelMediaList(_SingleChannelInfoModel.ChannelID, _SingleChannelInfoModel.ChannelType, startIndex, 10, ChannelConstants.MEDIA_STATUS_UPLOADED, (status) =>
                {
                    this._IS_UPLOAD_LOADING = false;
                    return 0;
                }).Start();
            }
        }

        private void OnMediaPlayCommand(object param)
        {
            ChannelMediaModel item = (ChannelMediaModel)param;
            int index = SingleChannelInfoModel.MediaList.IndexOf(item);
            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
            foreach (ChannelMediaModel mediaModel in SingleChannelInfoModel.MediaList)
            {
                SingleMediaModel singleMediaModel = new SingleMediaModel();
                singleMediaModel.StreamUrl = mediaModel.MediaUrl;
                singleMediaModel.ContentId = mediaModel.MediaID;
                singleMediaModel.MediaOwner = new BaseUserProfileModel();
                singleMediaModel.MediaOwner.UserTableID = mediaModel.OwnerID;
                singleMediaModel.Title = mediaModel.Title;
                singleMediaModel.Duration = mediaModel.Duration / 1000;
                singleMediaModel.Artist = mediaModel.Artist;
                singleMediaModel.MediaType = 2;
                singleMediaModel.ThumbUrl = mediaModel.ThumbImageUrl;
                singleMediaModel.IsFromLocalDirectory = false;
                singleMediaModel.ChannelId = mediaModel.ChannelID;
                MediaList.Add(singleMediaModel);
            }
            MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, index);
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods


        private void MenuItem_Add_To_Pending_Click(object sender, RoutedEventArgs e)
        {
            if (((MenuItem)sender).DataContext is ChannelMediaModel)
            {
                ChannelMediaModel mediaModel = (ChannelMediaModel)((MenuItem)sender).DataContext;
                List<Guid> channelMediaIds = new List<Guid>();
                channelMediaIds.Add(mediaModel.MediaID);
                new ThrdUpdateMediaStatus(mediaModel.ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_PENDING, null, (status) =>
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                {
                    if (status)
                    {
                        mediaModel.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                        SingleChannelInfoModel.MediaList.InvokeRemove(mediaModel);
                        if (UCMyChannelMainPanel.MyChannelPendingPanel != null)
                        {
                            UCMyChannelMainPanel.MyChannelPendingPanel.PendingMediaList.InvokeAdd(mediaModel);
                        }
                        UIHelperMethods.ShowMessageWithTimerFromThread(gridTimerMessage, "Media added as Pending to PlayList!");
                    }
<<<<<<< HEAD
                    else UIHelperMethods.ShowFailed("Failed to change media status");
                    return 0;
                }).Start();
            }
        }

        private void menuItem_Remove_Click(object sender, RoutedEventArgs e)
        {
            if (((MenuItem)sender).DataContext is ChannelMediaModel)
            {
                ChannelMediaModel mediaModel = (ChannelMediaModel)((MenuItem)sender).DataContext;
                List<Guid> channelMediaIds = new List<Guid>();
                channelMediaIds.Add(mediaModel.MediaID);
                new ThrdUpdateMediaStatus(mediaModel.ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_DELETED, null, (status) =>
                {
                    if (status)
                    {
                        mediaModel.MediaStatus = ChannelConstants.MEDIA_STATUS_DELETED;
                        SingleChannelInfoModel.MediaList.InvokeRemove(mediaModel);
                        //UIHelperMethods.ShowMessageWithTimerFromThread(_parentGrid, "Media added as Pending to PlayList!");
                    }
                    else UIHelperMethods.ShowFailed("Failed to remove media");
                    return 0;
                }).Start();
            }
        }
        #region Property

        public ChannelModel SingleChannelInfoModel
        {
            get { return _SingleChannelInfoModel; }
            set
            {
                _SingleChannelInfoModel = value;
                this.OnPropertyChanged("SingleChannelInfoModel");
            }
        }

        #endregion Property

        #region ICommand

        public ICommand AddToPendingCommand
        {
            get
            {
                if (_AddToPendingCommand == null)
                {
                    _AddToPendingCommand = new RelayCommand(param => OnAddToPendingCommand(param));
                }
                return _AddToPendingCommand;
            }
        }

        public ICommand MediaPlayCommand
        {
            get
            {
                if (_MediaPlayCommand == null)
                {
                    _MediaPlayCommand = new RelayCommand(param => OnMediaPlayCommand(param));
                }
                return _MediaPlayCommand;
            }
        }

        private void OnAddToPendingCommand(object param)
        {
            ChannelMediaModel mediaModel = (ChannelMediaModel)param;
            List<Guid> channelMediaIds = new List<Guid>();
            channelMediaIds.Add(mediaModel.MediaID);
            new ThrdUpdateMediaStatus(mediaModel.ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_PENDING, null, (status) =>
            {
                if (status)
                {
                    mediaModel.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                    SingleChannelInfoModel.MediaList.InvokeRemove(mediaModel);
                    if (UCMyChannelMainPanel.MyChannelPendingPanel != null)
                    {
                        UCMyChannelMainPanel.MyChannelPendingPanel.PendingMediaList.InvokeAdd(mediaModel);
                    }
                    UIHelperMethods.ShowMessageWithTimerFromThread(gridTimerMessage, "Media added as pending to playList!");
                }
                else UIHelperMethods.ShowFailed("Failed to change media status.", "Media status");
                return 0;
            }).Start();
        }

        #endregion

    }
}
=======
                    else
                    {
                        Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            UIHelperMethods.ShowFailed("Failed to change media status");
                        });
                    }
                    return 0;
                }).Start();
            }
        }

        private void menuItem_Remove_Click(object sender, RoutedEventArgs e)
        {
            if (((MenuItem)sender).DataContext is ChannelMediaModel)
            {
                ChannelMediaModel mediaModel = (ChannelMediaModel)((MenuItem)sender).DataContext;
                List<Guid> channelMediaIds = new List<Guid>();
                channelMediaIds.Add(mediaModel.MediaID);
                new ThrdUpdateMediaStatus(mediaModel.ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_DELETED, null, (status) =>
                {
                    if (status)
                    {
                        mediaModel.MediaStatus = ChannelConstants.MEDIA_STATUS_DELETED;
                        SingleChannelInfoModel.MediaList.InvokeRemove(mediaModel);
                        //UIHelperMethods.ShowMessageWithTimerFromThread(_parentGrid, "Media added as Pending to PlayList!");
                    }
                    else UIHelperMethods.ShowFailed("Failed to remove media");
                    return 0;
                }).Start();
            }
        }
        #region Property

        public ChannelModel SingleChannelInfoModel
        {
            get { return _SingleChannelInfoModel; }
            set
            {
                _SingleChannelInfoModel = value;
                this.OnPropertyChanged("SingleChannelInfoModel");
            }
        }

        #endregion Property

        #region ICommand

        public ICommand AddToPendingCommand
        {
            get
            {
                if (_AddToPendingCommand == null)
                {
                    _AddToPendingCommand = new RelayCommand(param => OnAddToPendingCommand(param));
                }
                return _AddToPendingCommand;
            }
        }

        public ICommand MediaPlayCommand
        {
            get
            {
                if (_MediaPlayCommand == null)
                {
                    _MediaPlayCommand = new RelayCommand(param => OnMediaPlayCommand(param));
                }
                return _MediaPlayCommand;
            }
        }

        private void OnAddToPendingCommand(object param)
        {
            ChannelMediaModel mediaModel = (ChannelMediaModel)param;
            List<Guid> channelMediaIds = new List<Guid>();
            channelMediaIds.Add(mediaModel.MediaID);
            new ThrdUpdateMediaStatus(mediaModel.ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_PENDING, null, (status) =>
            {
                if (status)
                {
                    mediaModel.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                    SingleChannelInfoModel.MediaList.InvokeRemove(mediaModel);
                    if (UCMyChannelMainPanel.MyChannelPendingPanel != null)
                    {
                        UCMyChannelMainPanel.MyChannelPendingPanel.PendingMediaList.InvokeAdd(mediaModel);
                    }
                    UIHelperMethods.ShowMessageWithTimerFromThread(gridTimerMessage, "Media added as pending to playList!");
                }
                else UIHelperMethods.ShowFailed("Failed to change media status.", "Media status");
                return 0;
            }).Start();
        }

        #endregion
        */
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

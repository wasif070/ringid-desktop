﻿using callsdkwrapper;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.UI.Stream;
using View.UI.StreamAndChannel;
using View.Utility;
using View.Utility.Call;
using View.Utility.Channel;
using View.Utility.Stream;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCChannelViewer.xaml
    /// </summary>
    public partial class UCChannelViewer : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCChannelViewer).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        public WNChannelPreview _ChannelPreview = null;

        public bool IS_INITIALIZED = false;
        public bool _IsNotAvailable = false;
        public bool _IsHighDefinition = true;
        private bool _IsSeperateWindowMode = false;
        private bool _IsSettingMode = false;
        private ChannelModel _ChannelModel = null;
        private ChannelMediaModel _ChannelMediaModel = null;

        private ICommand _SettingsCommand;
        private ICommand _CancelCommand;
        private ICommand _StreamingQualityCommand;

        #region Constructor

        static UCChannelViewer()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCChannelViewer(ChannelModel channelModel)
        {
            InitializeComponent();
            this.DataContext = null;
            this._ChannelModel = new ChannelModel
            {
                ChannelID = channelModel.ChannelID,
                OwnerID = channelModel.OwnerID,
                Title = channelModel.Title,
                ProfileImage = channelModel.ProfileImage,
                Description = channelModel.Description,
                ChannelStatus = channelModel.ChannelStatus,
                ChannelType = channelModel.ChannelType,
                PublisherID = ChannelHelpers.GetPublisherID(channelModel.ChannelID, this.IsHighDefinition),
                StreamIP = channelModel.StreamIP,
                StreamPort = channelModel.StreamPort
            };
            this._ChannelMediaModel = new ChannelMediaModel
            {
                MediaID = Guid.Empty,
                ChannelID = channelModel.ChannelID
            };
        }

        #endregion Constructor

        #region Event Handler

        static void OnStreamingStatusChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (e.NewValue != null)
                {
                    UCChannelViewer panel = ((UCChannelViewer)o);
                    int streamingStatus = (int)e.NewValue;
                    switch (streamingStatus)
                    {
                        case StreamConstants.STREAMING_CONNECTED:
                            log.Debug("*********************    STREAMING_CONNECTED      *********************");
                            panel.statusAnimation.StopAnimation();
                            panel.statusCircularAnimation.StopAnimation();
                            StreamViewModel.Instance.StopStreamingInturruptTimer();
                            break;
                        case StreamConstants.STREAMING_CONNECTING:
                            log.Debug("*********************    STREAMING_CONNECTING     *********************");
                            panel.statusAnimation.StartAnimation();
                            break;
                        case StreamConstants.STREAMING_RECONNECTING:
                            log.Debug("*********************    STREAMING_RECONNECTING     *********************");
                            panel.statusCircularAnimation.StartAnimation();
                            break;
                        case StreamConstants.STREAMING_POOR_NERWORK:
                            log.Debug("*********************    STREAMING_POOR_NERWORK   *********************");
                            panel.statusAnimation.StopAnimation();
                            panel.statusCircularAnimation.StopAnimation();
                            break;
                        case StreamConstants.STREAMING_INTERRUPTED:
                            log.Debug("*********************    STREAMING_INTERRUPTED    *********************");
                            panel.statusAnimation.StopAnimation();
                            panel.statusCircularAnimation.StopAnimation();
                            break;
                        case StreamConstants.STREAMING_NO_DATA:
                            log.Debug("*********************    STREAMING_NO_DATA        *********************");
                            panel.statusAnimation.StopAnimation();
                            panel.statusCircularAnimation.StopAnimation();
                            StreamViewModel.Instance.StartStreamingInturruptTimer();
                            break;
                        case StreamConstants.STREAMING_FINISHED:
                            log.Debug("*********************    STREAMING_FINISHED       *********************");
                            panel.statusAnimation.StopAnimation();
                            panel.statusCircularAnimation.StopAnimation();
                            StreamViewModel.Instance.StopStreamingInturruptTimer();
                            panel.ClearValue(UCChannelViewer.StreamingStatusProperty);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnStreamingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            if (this.IS_INITIALIZED == false)
            {
                this.IS_INITIALIZED = true;
                StreamViewModel.Instance.InitStreamingInfo(this._ChannelModel, this._ChannelMediaModel);
                StreamViewModel.Instance.InitStreamingChannel(true);

#if CHAT_LOG
                log.Debug("AUTH::CHANNEL::RESPONSE".PadRight(65, ' ') + "==>   publisherID = " + StreamViewModel.Instance.ChannelInfoModel.PublisherID + ", channelID = " + StreamViewModel.Instance.ChannelInfoModel.ChannelID + ", title = " + StreamViewModel.Instance.ChannelInfoModel.Title + ", ownerID = " + StreamViewModel.Instance.ChannelInfoModel.OwnerID + ", profileImage = " + StreamViewModel.Instance.ChannelInfoModel.ProfileImage + ", streamIP = " + StreamViewModel.Instance.ChannelInfoModel.StreamIP + ", streamPort = " + StreamViewModel.Instance.ChannelInfoModel.StreamPort);
#endif

                StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_CONNECTING;
                CallHelperMethods.ViewerRegisterForChannel(
                    StreamViewModel.Instance.ChannelInfoModel.PublisherID,
                    StreamViewModel.Instance.ChannelInfoModel.StreamIP,
                    StreamViewModel.Instance.ChannelInfoModel.StreamPort,
                    RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName,
                    RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage,
                    StreamViewModel.Instance.ChannelInfoModel.ChannelID.ToString(),
                    StreamViewModel.Instance.ChannelInfoModel.ChannelType == ChannelConstants.MEDIA_TYPE_AUDIO ? RegType.AudioChannel : RegType.VedioChanel);

                Guid channelID = StreamViewModel.Instance.ChannelInfoModel.ChannelID;
                new ThrdGetChannelDetails(channelID, (channelDTO) =>
                {
                    if (this.IS_INITIALIZED)
                    {
                        if (channelDTO != null && StreamViewModel.Instance.ChannelInfoModel != null && StreamViewModel.Instance.ChannelInfoModel.ChannelID.Equals(channelID))
                        {
                            StreamViewModel.Instance.ChannelInfoModel.LoadData(channelDTO);
                        }
                        else
                        {
                            StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_FINISHED;

                            this.OnPublisherNotAvailable();
                        }
                    }
                    return 0;
                }).Start();

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this.imgStream.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRenderModel.RenderSource"), Source = RingIDSettings });
                    MultiBinding sourceBinding = new MultiBinding { Converter = new ChannelProfileImageConverter(), ConverterParameter = ImageUtility.IMG_THUMB };
                    sourceBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.ChannelInfoModel"), Source = RingIDSettings });
                    sourceBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.ChannelInfoModel.ProfileImage"), Source = RingIDSettings });
                    this.imgProfile.SetBinding(Image.SourceProperty, sourceBinding);
                    this.SetBinding(UCChannelViewer.StreamingStatusProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamingStatus"), Source = RingIDSettings });

                    StreamViewModel.Instance.InitStreamingSourceInfo(StreamConstants.SPEAKER, StreamConstants.MONITOR_SCREEN, null);
                    StreamViewModel.Instance.SetStreamingChannelSource();
                    this.DataContext = this;
                }, DispatcherPriority.ApplicationIdle);
            }
        }

        public void ReleaseViewer()
        {

        }

        public void OnPublisherNotAvailable()
        {
            this.IS_INITIALIZED = false;
            CallHelperMethods.ViewerUnregister(StreamViewModel.Instance.ChannelInfoModel.PublisherID);
            this.IsNotAvailable = true;
        }

        public void OnInternetNotAvailable()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                //MessageBoxResult result = CustomMessageBox.ShowWarning("No internet! Please check your network connectivity?", "Poor Network", MessageBoxButton.YesNo, MessageBoxResult.Yes, new String[] { "Retry", "Ok" });
                //if (result == MessageBoxResult.No)
                //{
                //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeStreamAndChannel, true);
                //}
                WNConfirmationView cv = new WNConfirmationView("Poor network", "No internet! Please check your network connectivity?", CustomConfirmationDialogButtonOptions.YesNo, new[] { "Retry", "Ok" });
                var result = cv.ShowCustomDialog();
                if (result == ConfirmationDialogResult.No)
                {
                    UCStreamAndChannelViewer.Instance.Dispose();
                }
            }, DispatcherPriority.ApplicationIdle);
        }

        private void OnCancelClick(object param)
        {
            try
            {
                UCStreamAndChannelViewer.Instance.Dispose();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCancelClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnSettingsClick(object param)
        {
            this.IsSettingMode = !IsSettingMode;
        }

        private void OnStreamingQualityClick(object param)
        {
            try
            {
                this.IsSettingMode = false;

                if (this.IsHighDefinition == (bool)param)
                {
                    return;
                }
                else
                {
                    this.IsHighDefinition = (bool)param;
                }

                StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_RECONNECTING;
                StreamViewModel.Instance.StreamingChannel.IsRegistered = false;
                CallHelperMethods.ViewerUnregister(StreamViewModel.Instance.ChannelInfoModel.PublisherID, (args) =>
                {
                    long publisherID = ChannelHelpers.GetPublisherID(_ChannelModel.ChannelID, this.IsHighDefinition);
                    this._ChannelModel.PublisherID = publisherID;
                    StreamViewModel.Instance.ChannelInfoModel.PublisherID = publisherID;

#if CHAT_LOG
                    log.Debug("AUTH::CHANNEL::RESPONSE".PadRight(65, ' ') + "==>   publisherID = " + StreamViewModel.Instance.ChannelInfoModel.PublisherID + ", channelID = " + StreamViewModel.Instance.ChannelInfoModel.ChannelID + ", title = " + StreamViewModel.Instance.ChannelInfoModel.Title + ", ownerID = " + StreamViewModel.Instance.ChannelInfoModel.OwnerID + ", profileImage = " + StreamViewModel.Instance.ChannelInfoModel.ProfileImage + ", streamIP = " + StreamViewModel.Instance.ChannelInfoModel.StreamIP + ", streamPort = " + StreamViewModel.Instance.ChannelInfoModel.StreamPort);
#endif

                    CallHelperMethods.ViewerRegisterForChannel(
                            StreamViewModel.Instance.ChannelInfoModel.PublisherID,
                            StreamViewModel.Instance.ChannelInfoModel.StreamIP,
                            StreamViewModel.Instance.ChannelInfoModel.StreamPort,
                            RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName,
                            RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage,
                            StreamViewModel.Instance.ChannelInfoModel.ChannelID.ToString(),
                            StreamViewModel.Instance.ChannelInfoModel.ChannelType == ChannelConstants.MEDIA_TYPE_AUDIO ? RegType.AudioChannel : RegType.VedioChanel);
                });
            }
            catch (Exception ex)
            {
                log.Error("Error: OnStreamingQualityClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.IS_INITIALIZED = false;

            this.HideWindowPreview();

            CallHelperMethods.ViewerUnregister(StreamViewModel.Instance.ChannelInfoModel.PublisherID);

            StreamViewModel.Instance.DestroyStreamingChannel();
            StreamViewModel.Instance.DestroyStreamingInfo();

            this.ClearValue(UCChannelViewer.StreamingStatusProperty);
            this.imgStream.ClearValue(Image.SourceProperty);
            this.imgStream.Source = null;
            this.imgProfile.ClearValue(Image.SourceProperty);
            this.imgProfile.Source = null;

            this.DataContext = null;
            this._ChannelModel = null;
            this._ChannelMediaModel = null;
            this.IsNotAvailable = false;
            this.IsHighDefinition = true;
            this.IsSettingMode = false;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Popup

        public void ShowWindowPreview()
        {
            this.IsSeperateWindowMode = true;
            if (this._ChannelPreview == null)
            {
                this._ChannelPreview = new WNChannelPreview();
                this._ChannelPreview.ShowWindow();
            }
        }

        public void HideWindowPreview()
        {
            this.IsSeperateWindowMode = false;
            if (this._ChannelPreview != null)
            {
                this._ChannelPreview.CloseWindow();
                this._ChannelPreview = null;
            }
        }

        #endregion

        #region Property

        public static readonly DependencyProperty StreamingStatusProperty = DependencyProperty.Register("StreamingStatus", typeof(int), typeof(UCChannelViewer), new PropertyMetadata(StreamConstants.STREAMING_FINISHED, OnStreamingStatusChanged));

        public bool StreamingStatus
        {
            get { return (bool)GetValue(StreamingStatusProperty); }
            set
            {
                SetValue(StreamingStatusProperty, value);
            }
        }

        public ICommand SettingsCommand
        {
            get
            {
                if (_SettingsCommand == null)
                {
                    _SettingsCommand = new RelayCommand((param) => OnSettingsClick(param));
                }
                return _SettingsCommand;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand((param) => OnCancelClick(param));
                }
                return _CancelCommand;
            }
        }

        public ICommand StreamingQualityCommand
        {
            get
            {
                if (_StreamingQualityCommand == null)
                {
                    _StreamingQualityCommand = new RelayCommand((param) => OnStreamingQualityClick(param));
                }
                return _StreamingQualityCommand;
            }
        }

        public ChannelModel ChannelInfoModel
        {
            get { return _ChannelModel; }
        }

        public bool IsSeperateWindowMode
        {
            get { return _IsSeperateWindowMode; }
            set
            {
                if (_IsSeperateWindowMode == value)
                    return;
                _IsSeperateWindowMode = value;
                this.OnPropertyChanged("IsSeperateWindowMode");
            }
        }

        public bool IsNotAvailable
        {
            get { return _IsNotAvailable; }
            set
            {
                if (_IsNotAvailable == value)
                    return;

                _IsNotAvailable = value;
                this.OnPropertyChanged("IsNotAvailable");
            }
        }

        public bool IsHighDefinition
        {
            get { return _IsHighDefinition; }
            set
            {
                if (_IsHighDefinition == value)
                    return;

                _IsHighDefinition = value;
                this.OnPropertyChanged("IsHighDefinition");
            }
        }

        public bool IsSettingMode
        {
            get { return _IsSettingMode; }
            set
            {
                if (_IsSettingMode == value)
                    return;

                _IsSettingMode = value;
                this.OnPropertyChanged("IsSettingMode");
            }
        }

        #endregion Property
    }
}

﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.Utility.Channel;
using View.ViewModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelWrapper.xaml
    /// </summary>
    public partial class UCMyChannelWrapper : UserControl
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelWrapper).Name);

        public UCMyChannelAndFollowingMainPanel MyChannelAndFollowingMainPanel;
        public UCMyChannelCreatePanel MyChannelCreatePanel = null;
        public UCMyChannelMainPanel MyChannelMainPanel = null;
        public UCChannelDetailsPanel MyChannelDetailsPanel = null;
        public List<int> _BackChannelList = new List<int>();
        private int ViewType = -1;

        #region Constructor

        public UCMyChannelWrapper()
        {
            InitializeComponent();
            this.DataContext = null;
            this.Loaded += UCMyChannelWrapper_Loaded;
            this.Unloaded += UCMyChannelWrapper_Unloaded;
            MyChannelSwitcher.pageSwitcher = this;
        }

        #endregion Constructor

        #region Event Handler

        private void UCMyChannelWrapper_Unloaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.MyChannelButtonSelection = false;
        }

        void UCMyChannelWrapper_Loaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.MyChannelButtonSelection = true;
        }

        #endregion Event Handler

        #region Utility Methods

        public void Navigate(int type, object state = null)
        {
            bool isChanged = this.ViewType != type;
            if (isChanged)
            {
                Destroy(this.ViewType);
            }
            this.ViewType = type;

            switch (type)
            {
                case MyChannelConstants.TypeMyChannelAndFollowingPanel:
                    LoadMyChannelListAndFollowingPanel(state, isChanged);
                    break;
                case MyChannelConstants.TypeMyChannelCreatePanel:
                    Destroy(this.ViewType);
                    LoadMyChannelCreatePanel(state, isChanged);
                    break;
                case MyChannelConstants.TypeMyChannelMainPanel:
                    LoadMyChannelMainPanel(state, isChanged);
                    break;
                case MyChannelConstants.TypeMyChannelDetailsPanel:
                    LoadMyChannelDetailsPanel(state, isChanged);
                    break;
            }
        }

        private void Destroy(int type)
        {
            switch (type)
            {
                case MyChannelConstants.TypeMyChannelAndFollowingPanel:
                    if (MyChannelAndFollowingMainPanel != null)
                    {
                        MyChannelAndFollowingMainPanel.ReleaseViewer();
                    }
                    break;
                case MyChannelConstants.TypeMyChannelCreatePanel:
                    if (MyChannelCreatePanel != null)
                    {
                        MyChannelCreatePanel.Dispose();
                        MyChannelCreatePanel = null;
                    }
                    break;
                case MyChannelConstants.TypeMyChannelMainPanel:
                    if (MyChannelMainPanel != null)
                    {
                        MyChannelMainPanel.ReleaseViewer();
                    }
                    break;
                case MyChannelConstants.TypeMyChannelDetailsPanel:
                    if (MyChannelDetailsPanel != null)
                    {
                        MyChannelDetailsPanel.Dispose();
                        MyChannelDetailsPanel = null;
                    }
                    break;
            }
        }

        private void LoadMyChannelListAndFollowingPanel(object state, bool isChanged)
        {
            if (MyChannelAndFollowingMainPanel == null)
            {
                MyChannelAndFollowingMainPanel = new UCMyChannelAndFollowingMainPanel();
            }
            if (isChanged)
            {
                MyChannelAndFollowingMainPanel.InilializeViewer();
                bdrParent.Child = MyChannelAndFollowingMainPanel;
            }
        }

        private void LoadMyChannelCreatePanel(object state, bool isChanged)
        {
            if (MyChannelCreatePanel == null)
            {
                MyChannelCreatePanel = new UCMyChannelCreatePanel();
            }
            if (isChanged)
            {
                MyChannelCreatePanel.InilializeViewer();
                bdrParent.Child = MyChannelCreatePanel;
            }
        }

        private void LoadMyChannelMainPanel(object state, bool isChanged)
        {
            if (MyChannelMainPanel == null)
            {
                MyChannelMainPanel = new UCMyChannelMainPanel();
            }
            if (isChanged)
            {
                MyChannelMainPanel.SelectedChannelModel = (ChannelModel)state;
                MyChannelMainPanel.InilializeViewer();
                bdrParent.Child = MyChannelMainPanel;
            }
        }

        private void LoadMyChannelDetailsPanel(object state, bool isChanged)
        {
            if (MyChannelDetailsPanel == null)
            {
                MyChannelDetailsPanel = new UCChannelDetailsPanel(state);
                MyChannelDetailsPanel.InilializeViewer();
            }
            if (isChanged)
            {
                bdrParent.Child = MyChannelDetailsPanel;
            }
        }

        #endregion Utility Methods

    }
}

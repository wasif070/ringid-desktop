<<<<<<< HEAD:View/UI/Channel/UCSingleMediaUploadInChannel.xaml.cs
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.PopUp.Stream;
//using View.UI.PopUp.Stream_Channel;
using View.Utility;
using View.ViewModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCSingleMediaUploadInChannel.xaml
    /// </summary>
    public partial class UCSingleMediaUploadInChannel : UserControl
    {
        public UCSingleMediaUploadInChannel()
        {
            InitializeComponent();
        }

        private void CrossButton_Click(object sender, RoutedEventArgs e)
        {
            Control control = sender as Control;
            FeedVideoUploaderModel model = (FeedVideoUploaderModel)control.DataContext;
            UCUploadMediaToChannelPopUp.Instance.ChannelVideoUploadList.Remove(model);
            //if (UCUploadMediaToChannelPopUp.Instance.ChannelVideoUploadList.Count == 0)
            //{
            //    UCUploadMediaToChannelPopUp.Instance.Dispose();
            //}
        }

        private void textBoxAudio_Loaded(object sender, RoutedEventArgs e)
        {
            textBoxAudio.Focus();
            textBoxAudio.CaretIndex = textBoxAudio.Text.Length;
        }

        private void videoPanel_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Grid b = (Grid)sender;
            FeedVideoUploaderModel item = (FeedVideoUploaderModel)b.DataContext;
            int index = UCUploadMediaToChannelPopUp.Instance.ChannelVideoUploadList.IndexOf(item);

            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
            foreach (FeedVideoUploaderModel feedUploaderModel in UCUploadMediaToChannelPopUp.Instance.ChannelVideoUploadList)
            {
                SingleMediaModel singleMediamodel = new SingleMediaModel();
                singleMediamodel.StreamUrl = feedUploaderModel.FilePath;
                singleMediamodel.Title = feedUploaderModel.VideoTitle;
                singleMediamodel.Duration = feedUploaderModel.VideoDuration;
                singleMediamodel.Artist = feedUploaderModel.VideoArtist;
                singleMediamodel.MediaType = 2;
                singleMediamodel.ThumbUrl = feedUploaderModel.ThumbUrl;
                singleMediamodel.IsFromLocalDirectory = true;
                MediaList.Add(singleMediamodel);
            }
            MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, index);
        }
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.PopUp.Stream;
//using View.UI.PopUp.Stream_Channel;
using View.Utility;
using View.ViewModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCSingleChannelMediaUploadingPanel.xaml
    /// </summary>
    public partial class UCSingleChannelMediaUploadingPanel : UserControl
    {
        public UCSingleChannelMediaUploadingPanel()
        {
            InitializeComponent();
        }

        private void CrossButton_Click(object sender, RoutedEventArgs e)
        {
            Control control = sender as Control;
            FeedVideoUploaderModel model = (FeedVideoUploaderModel)control.DataContext;
            UCUploadMediaToChannelPopUp.Instance.ChannelVideoUploadList.Remove(model);
            //if (UCUploadMediaToChannelPopUp.Instance.ChannelVideoUploadList.Count == 0)
            //{
            //    UCUploadMediaToChannelPopUp.Instance.Dispose();
            //}
        }

        private void textBoxAudio_Loaded(object sender, RoutedEventArgs e)
        {
            textBoxAudio.Focus();
            textBoxAudio.CaretIndex = textBoxAudio.Text.Length;
        }

        private void videoPanel_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Grid b = (Grid)sender;
            FeedVideoUploaderModel item = (FeedVideoUploaderModel)b.DataContext;
            int index = UCUploadMediaToChannelPopUp.Instance.ChannelVideoUploadList.IndexOf(item);

            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
            foreach (FeedVideoUploaderModel feedUploaderModel in UCUploadMediaToChannelPopUp.Instance.ChannelVideoUploadList)
            {
                SingleMediaModel singleMediamodel = new SingleMediaModel();
                singleMediamodel.StreamUrl = feedUploaderModel.FilePath;
                singleMediamodel.Title = feedUploaderModel.VideoTitle;
                singleMediamodel.Duration = feedUploaderModel.VideoDuration;
                singleMediamodel.Artist = feedUploaderModel.VideoArtist;
                singleMediamodel.MediaType = 2;
                singleMediamodel.ThumbUrl = feedUploaderModel.ThumbUrl;
                singleMediamodel.IsFromLocalDirectory = true;
                MediaList.Add(singleMediamodel);
            }
            MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, index);
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596:View/UI/Channel/UCSingleChannelMediaUploadingPanel.xaml.cs

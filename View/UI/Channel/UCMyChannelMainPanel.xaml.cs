﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp.Stream;
using View.Utility;
using View.Utility.Channel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelMainPanel.xaml
    /// </summary>
    public partial class UCMyChannelMainPanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelMainPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        public static UCMyChannelPlaylistPanel MyChannelPlaylistPanel = null;
        public static UCMyChannelUploadsPanel MyChannelUploadsPanel = null;
        public static UCMyChannelPendingPanel MyChannelPendingPanel = null;
        public static UCMyChannelInfoPanel MyChannelInfoPanel = null;

        public ChannelModel _SelectedChannelModel;
        private int _TabType = -1;
        private ICommand _TabChangeCommand;
        private ICommand _OptionCommad;
        public bool _UploadPopup;
        private Func<int> _OnClosing = null;

        #region Constructor

        public UCMyChannelMainPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            switch (this.TabType)
            {
                case 0:
                    ShowMyChannelPlaylistView(true);
                    break;
                case 1:
                    ShowMyChannelPendingView(true);
                    break;
                case 2:
                    ShowMyChannelUploadsView(true);
                    break;
                case 3:
                    ShowMyChannelInfoView(true);
                    break;
                default:
                    {
                        this.TabType = 0;
                        ShowMyChannelPlaylistView(true);
                    }
                    break;
            }
        }

        public void ReleaseViewer()
        {
            Destroy(this.TabType);
        }

        private void OnTabChangeClicked(object param)
        {
            try
            {
                int type = Int32.Parse(param.ToString());
                bool isChanged = this.TabType != type;

                if (isChanged)
                {
                    Destroy(this.TabType);
                }
                this.TabType = type;

                switch (this.TabType)
                {
                    case 0:
                        ShowMyChannelPlaylistView(isChanged, true);
                        break;
                    case 1:
                        ShowMyChannelPendingView(isChanged, true);
                        break;
                    case 2:
                        ShowMyChannelUploadsView(isChanged, true);
                        break;
                    case 3:
                        ShowMyChannelInfoView(isChanged, true);
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnTabChangeClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Destroy(int type)
        {
            switch (type)
            {
                case 0:
                    if (MyChannelPlaylistPanel != null)
                    {
                        ScrlViewer.ScrollChanged -= MyChannelPlaylistPanel.ScrlViewer_ScrollChanged;
                        ScrlViewer.SizeChanged -= MyChannelPlaylistPanel.ScrlViewer_SizeChanged;
                        MyChannelPlaylistPanel.ReleaseViewer();
                    }
                    break;
                case 1:
                    if (MyChannelPendingPanel != null)
                    {
                        ScrlViewer.ScrollChanged -= MyChannelPendingPanel.ScrlViewer_ScrollChanged;
                        ScrlViewer.SizeChanged -= MyChannelPendingPanel.ScrlViewer_SizeChanged;
                        MyChannelPendingPanel.ReleaseViewer();
                    }
                    break;
                case 2:
                    if (MyChannelUploadsPanel != null)
                    {
                        ScrlViewer.ScrollChanged -= MyChannelUploadsPanel.ScrlViewer_ScrollChanged;
                        ScrlViewer.SizeChanged -= MyChannelUploadsPanel.ScrlViewer_SizeChanged;
                        MyChannelUploadsPanel.ReleaseViewer();
                    }
                    break;
                case 3:
                    if (MyChannelInfoPanel != null)
                    {
                        ScrlViewer.ScrollChanged -= MyChannelInfoPanel.ScrlViewer_ScrollChanged;
                        ScrlViewer.SizeChanged -= MyChannelInfoPanel.ScrlViewer_SizeChanged;
                        MyChannelInfoPanel.ReleaseViewer();
                    }
                    break;
            }
        }

        public void ShowMyChannelPlaylistView(bool isChanged, bool isFromTab = false)
        {
            try
            {
                if (MyChannelPlaylistPanel == null)
                {
                    MyChannelPlaylistPanel = new UCMyChannelPlaylistPanel();
                    isFromTab = false;
                }

                if (isChanged)
                {
                    if (isFromTab == false || (this._SelectedChannelModel != null && !(MyChannelPlaylistPanel.SingleChannelInfoModel != null && MyChannelPlaylistPanel.SingleChannelInfoModel.ChannelID == this._SelectedChannelModel.ChannelID)))
                    {
                        MyChannelPlaylistPanel.Destroy();
                        MyChannelPlaylistPanel.SingleChannelInfoModel = SelectedChannelModel;
                        MyChannelPlaylistPanel.MediaPlayedList = ChannelViewModel.Instance.GetChannelPlayList(SelectedChannelModel.ChannelID);
                       
                    }
                    MyChannelPlaylistPanel.GetPublishedList(true);
                    MyChannelPlaylistPanel.InilializeViewer();
                    ScrlViewer.ScrollChanged += MyChannelPlaylistPanel.ScrlViewer_ScrollChanged;
                    ScrlViewer.SizeChanged += MyChannelPlaylistPanel.ScrlViewer_SizeChanged;
                    bdrParent.Child = MyChannelPlaylistPanel;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMyChannelPlaylistView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mediaSearchBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMediaCloud, MiddlePanelConstants.TypeMyChannel);
            UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.OnSearchCommandClicked(MiddlePanelConstants.TypeMyChannel);
            UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId = SelectedChannelModel.ChannelID;
        }

        private void uploadBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!(UCUploadMediaToChannelPopUp.Instance != null && UCUploadMediaToChannelPopUp.Instance.IsUploadButtonEnabled == false))
            {
                UCUploadMediaToChannelPopUp ucUploadMediaToChannel = new UCUploadMediaToChannelPopUp(UCGuiRingID.Instance._MotherPanel, SelectedChannelModel.ChannelID);
                ucUploadMediaToChannel.InilializeViewer();
                ucUploadMediaToChannel.ShowPopup(_SelectedChannelModel.ChannelType);
                //ucUploadMediaToChannel._OnCompleteEvent += (uploaList) =>
                //{
                //    if (uploaList.Count > 0)
                //    {
                //        ChannelHelpers.AddMediaIntoMediaList(uploaList, ChannelConstants.MEDIA_STATUS_UPLOADED);
                //    }
                //};
            }
            else
            {
                UCUploadMediaToChannelPopUp.Instance.Visibility = Visibility.Visible;
            }
        }

        public void ShowMyChannelUploadsView(bool isChanged, bool isFromTab = false)
        {
            try
            {
                if (MyChannelUploadsPanel == null)
                {
                    MyChannelUploadsPanel = new UCMyChannelUploadsPanel(SelectedChannelModel);
                    isFromTab = false;
                }

                if (isChanged)
                {
                    //if (isFromTab == false || (this._SelectedChannelModel != null && !(MyChannelUploadsPanel.SingleChannelInfoModel != null && MyChannelUploadsPanel.SingleChannelInfoModel.ChannelID == this._SelectedChannelModel.ChannelID)))
                    //{
                    //    MyChannelUploadsPanel.SingleChannelInfoModel = SelectedChannelModel;
                    //    MyChannelUploadsPanel.GetUploadedMediaList();
                    //}
                    MyChannelUploadsPanel.InilializeViewer();
                    ScrlViewer.ScrollChanged += MyChannelUploadsPanel.ScrlViewer_ScrollChanged;
                    ScrlViewer.SizeChanged += MyChannelUploadsPanel.ScrlViewer_SizeChanged;
                    bdrParent.Child = MyChannelUploadsPanel;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMyChannelUploadsView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowMyChannelPendingView(bool isChanged, bool isFromTab = false)
        {
            if (MyChannelPendingPanel == null)
            {
                MyChannelPendingPanel = new UCMyChannelPendingPanel();
                isFromTab = false;
            }
            if (isChanged)
            {
                if (isFromTab == false || (this._SelectedChannelModel != null && !(MyChannelPendingPanel._SingleChannelInfoModel != null && MyChannelPendingPanel._SingleChannelInfoModel.ChannelID == this._SelectedChannelModel.ChannelID)))
                {
                    MyChannelPendingPanel.Dispose();
                    MyChannelPendingPanel._SingleChannelInfoModel = SelectedChannelModel;
                    MyChannelPendingPanel.GetPendingMediaList();
                }

                MyChannelPendingPanel.InilializeViewer();
                ScrlViewer.ScrollChanged += MyChannelPendingPanel.ScrlViewer_ScrollChanged;
                ScrlViewer.SizeChanged += MyChannelPendingPanel.ScrlViewer_SizeChanged;
                bdrParent.Child = MyChannelPendingPanel;
            }
        }


        public void ShowMyChannelInfoView(bool isChanged, bool isFromTab = false)
        {
            try
            {
                if (MyChannelInfoPanel == null)
                {
                    MyChannelInfoPanel = new UCMyChannelInfoPanel();
                    isFromTab = false;
                }

                if (isChanged)
                {
                    MyChannelInfoPanel.InilializeViewer();
                    MyChannelInfoPanel.SetDefaultValue(SelectedChannelModel);
                    ScrlViewer.ScrollChanged += MyChannelInfoPanel.ScrlViewer_ScrollChanged;
                    ScrlViewer.SizeChanged += MyChannelInfoPanel.ScrlViewer_SizeChanged;
                    bdrParent.Child = MyChannelInfoPanel;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMyChannelInfoView() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Show(UIElement target, Func<int> onClosing)
        {
            _OnClosing = onClosing;
            if (popupUpAddOption.IsOpen)
            {
                popupUpAddOption.IsOpen = false;
            }
            else
            {
                popupUpAddOption.PlacementTarget = target;
                popupUpAddOption.IsOpen = true;
                popupUpAddOption.Closed += (o, e) =>
                {
                    if (this._OnClosing != null)
                    {
                        this._OnClosing();
                    }
                };
            }
        }

        private void OnOptionCommad(object param)
        {
            UploadPopup = true;
            Button btn = (Button)param;
            Show(btn, () =>
            {
                UploadPopup = false;
                return 0;
            });
        }

        public void Dispose()
        {

        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand TabChangeCommand
        {
            get
            {
                if (_TabChangeCommand == null)
                {
                    _TabChangeCommand = new RelayCommand(param => OnTabChangeClicked(param));
                }
                return _TabChangeCommand;
            }
        }

        public ICommand OptionCommad
        {
            get
            {
                if (_OptionCommad == null)
                {
                    _OptionCommad = new RelayCommand(param => OnOptionCommad(param));
                }
                return _OptionCommad;
            }
        }

        public int TabType
        {
            get { return _TabType; }
            set
            {
                if (_TabType == value)
                    return;

                _TabType = value;
                this.OnPropertyChanged("TabType");
            }
        }

        public ChannelModel SelectedChannelModel
        {
            get { return _SelectedChannelModel; }
            set
            {
                _SelectedChannelModel = value;
                this.OnPropertyChanged("SelectedChannelModel");
            }
        }

        public bool UploadPopup
        {
            get { return _UploadPopup; }
            set
            {
                if (value == _UploadPopup) return;
                _UploadPopup = value;
                this.OnPropertyChanged("UploadPopup");
            }
        }

        #endregion Property

    }
}

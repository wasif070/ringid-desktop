﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.UI.StreamAndChannel;
using View.Utility;
using View.Utility.Channel;
using View.Utility.StreamAndChannel;
using View.ViewModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCChannelDiscoveryPanel.xaml
    /// </summary>
    public partial class UCChannelDiscoveryPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCChannelDiscoveryPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private ObservableCollection<CountryCodeModel> _CountryList = new ObservableCollection<CountryCodeModel>();

        private string _PrevSearchText = null;
        private bool _IsSearchMode = false;
        private bool _IsCountrySelectMode = false;
        private ICommand _ShowMoreCommand;
        private ICommand _ClearSearchCommand;
        private ICommand _BackCommand;
        private DispatcherTimer _WindowResizeTimer = null;
        private DispatcherTimer _ScrollChangeTimer = null;
        private DispatcherTimer _FilterTimer = null;

        private bool _IS_AT_TOP = true;
        private bool _IS_AT_BOTTOM = false;

        private UCStreamAndChannelCountryListPanel _CountryListPanel = null;

        #region Constructor

        static UCChannelDiscoveryPanel()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCChannelDiscoveryPanel()
        {
            InitializeComponent();
            CountryList.Add(new CountryCodeModel("Canada", "+1"));
            CountryList.Add(new CountryCodeModel("United States", "+1"));
            CountryList.Add(new CountryCodeModel("United Kingdom", "+44"));
            CountryList.Add(new CountryCodeModel("Bangladesh", "+880"));
            CountryList.Add(new CountryCodeModel("India", "+91"));
            CountryList.Add(new CountryCodeModel("Pakistan", "+92"));
            CountryList.Add(new CountryCodeModel("Australia", "+61"));
            CountryList.Add(new CountryCodeModel("Iran", "+98"));
            this.DataContext = null;
        }

        #endregion Constructor

        #region Event Handler

        private static void OnLoadingStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d != null && e.NewValue != null)
                {
                    UCChannelDiscoveryPanel panel = ((UCChannelDiscoveryPanel)d);
                    int loadingStatus = (int)e.NewValue;

                    switch (loadingStatus)
                    {
                        case StatusConstants.NO_DATA:
                            log.Debug("*********************    NO_DATA                 *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.NO_DATA_AND_LOADING:
                            log.Debug("*********************    NO_DATA_AND_LOADING     *********************");
                            panel.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            break;
                        case StatusConstants.HAS_DATA:
                            log.Debug("*********************    HAS_DATA                *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.HAS_DATA_AND_LOADING:
                            log.Debug("*********************    HAS_DATA_AND_LOADING    *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLoadingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int topLimit = 0;
            int bottomLimit = 0;

            if (this.IsSearchMode)
            {
                int seachCount = this.itcSearchChannelList.Items.Count;
                topLimit = seachCount > 5 ? 300 : 150;
                bottomLimit = seachCount > 5 ? 400 : 100;

                if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
                {
                    if (!ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading && e.ExtentHeight > e.ViewportHeight)
                    {
                        if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_AT_BOTTOM == false)
                        {
                            Debug.WriteLine("IS_AT_BOTTOM");
                            ChannelHelpers.LoadSearchChannelList(false, TxtSearch.Text);
                        }
                    }
                    this.ChangeScrollOpenStatusOnScroll();
                }
            }
            else
            {
                int categoryCount = this.itcCategoryList.Items.Count;
                int mostViewedCount = this.itcMostViewedChannelList.Items.Count;
                topLimit = (51 + (int)bdrCategoryList.ActualHeight + (int)bdrCountryList.ActualHeight + (mostViewedCount > 0 ? 50 : 0) + (300 /* Extra */)) / 2;
                bottomLimit = mostViewedCount > 5 || (topLimit > 200 && mostViewedCount > 2) ? 400 : 100;

                if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
                {
                    if (!ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading && e.ExtentHeight > e.ViewportHeight)
                    {
                        if (e.VerticalChange < 0 && e.VerticalOffset <= topLimit && this._IS_AT_TOP == false)
                        {
                            Debug.WriteLine("IS_AT_TOP");
                            ChannelHelpers.LoadMostViewChannelList(true);
                        }
                        else if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_AT_BOTTOM == false)
                        {
                            Debug.WriteLine("IS_AT_BOTTOM");
                            ChannelHelpers.LoadMostViewChannelList();
                        }
                    }
                    this.ChangeScrollOpenStatusOnScroll();
                }
            }

            if (e.ExtentHeight > e.ViewportHeight)
            {
                this._IS_AT_TOP = e.VerticalOffset <= topLimit;
                this._IS_AT_BOTTOM = (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit);
            }
            else
            {
                this._IS_AT_TOP = true;
                this._IS_AT_BOTTOM = false;
            }
        }

        private void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.ChangeScrollOpenStatusOnResize();
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs args)
        {
            try
            {
                this.IsSearchMode = TxtSearch.Text.Trim().Length > 0;
                this.ResetDataOnSearching(!this.IsSearchMode || String.IsNullOrWhiteSpace(this._PrevSearchText) || !this._PrevSearchText.ToLower().Contains(TxtSearch.Text.ToLower()));

                if (this._FilterTimer == null)
                {
                    this._FilterTimer = new DispatcherTimer();
                    this._FilterTimer.Interval = TimeSpan.FromMilliseconds(800);
                    this._FilterTimer.Tick += (o, e) =>
                    {
                        this._PrevSearchText = TxtSearch.Text;
                        this.ResetDataOnSearchComplete();
                        this._FilterTimer.Stop();
                    };
                }

                this._FilterTimer.Stop();
                this._FilterTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: SearchBox_TextChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer(object param)
        {
            ChannelHelpers.LoadMostViewChannelList(true);

            this.DataContext = this;
            this.TxtSearch.TextChanged += SearchBox_TextChanged;
            this.itcMostViewedChannelList.SizeChanged += ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged += ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged += ScrlViewer_SizeChanged;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.itcCountryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("CountryList") });
                this.itcCategoryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelCategoryList"), Source = RingIDSettings });
                this.itcMostViewedChannelList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelMostViewList"), Source = RingIDSettings });
                
                if (param != null && param is ExpandoObject)
                {
                    this.ShowCountrySelectPreview(param);
                }
                else
                {
                    this.ChangeScrollOpenStatus();
                }
            });
        }

        private void ResetDataOnSearching(bool doClear)
        {
            this.itcMostViewedChannelList.SizeChanged -= ScrlViewer_SizeChanged;
            this.itcSearchChannelList.SizeChanged -= ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged -= ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged -= ScrlViewer_SizeChanged;

            Task.Factory.StartNew(() =>
            {
                ChannelViewModel.Instance.ChannelMostViewList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                if (doClear) ChannelViewModel.Instance.ChannelSearchList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
            });

            if (doClear)
            {
                this.ClearValue(UCChannelDiscoveryPanel.LoadingStatusProperty);
                this.itcSearchChannelList.ClearValue(ItemsControl.ItemsSourceProperty);
                this.itcSearchChannelList.ItemsSource = null;
                ChannelViewModel.Instance.ChannelSearchList.Clear();
            }
        }

        private void ResetDataOnSearchComplete()
        {
            if (this.IsSearchMode)
            {
                this.itcMostViewedChannelList.SizeChanged -= ScrlViewer_SizeChanged;
                this.itcSearchChannelList.SizeChanged += ScrlViewer_SizeChanged;
                this.ScrlViewer.ScrollChanged += ScrlViewer_ScrollChanged;
                this.ScrlViewer.SizeChanged += ScrlViewer_SizeChanged;

                ChannelHelpers.LoadSearchChannelList(true, TxtSearch.Text);
                this.itcSearchChannelList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelSearchList"), Source = RingIDSettings });

                MultiBinding loadingStatusBinding = new MultiBinding { Converter = new LoadingStatusConverter() };
                loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.ChannelSearchList.Count"), Source = RingIDSettings });
                loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("ChannelViewModel.LoadStatusModel.IsSearchLoading"), Source = RingIDSettings });
                this.SetBinding(UCChannelDiscoveryPanel.LoadingStatusProperty, loadingStatusBinding);
            }
            else
            {
                this.itcSearchChannelList.SizeChanged -= ScrlViewer_SizeChanged;
                this.itcMostViewedChannelList.SizeChanged += ScrlViewer_SizeChanged;
                this.ScrlViewer.ScrollChanged += ScrlViewer_ScrollChanged;
                this.ScrlViewer.SizeChanged += ScrlViewer_SizeChanged;
                this.ChangeScrollOpenStatus();
            }
        }

        private void ChangeScrollOpenStatusOnResize()
        {
            try
            {
                if (this._WindowResizeTimer == null)
                {
                    this._WindowResizeTimer = new DispatcherTimer();
                    this._WindowResizeTimer.Interval = TimeSpan.FromMilliseconds(350);
                    this._WindowResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._WindowResizeTimer.Stop();
                    };
                }

                if (this._ScrollChangeTimer != null && this._ScrollChangeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Stop();
                }
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnScroll()
        {
            try
            {
                if (this._ScrollChangeTimer == null)
                {
                    this._ScrollChangeTimer = new DispatcherTimer();
                    this._ScrollChangeTimer.Interval = TimeSpan.FromMilliseconds(30);
                    this._ScrollChangeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._ScrollChangeTimer.Stop();
                    };
                }
                this._ScrollChangeTimer.Stop();
                if (this._WindowResizeTimer == null || !this._WindowResizeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnScroll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatus()
        {
            try
            {
                if (this.IsSearchMode)
                {
                    int paddingTop = 51;
                    ChannelHelpers.ChangeChannelViewPortOpenedProperty(this.ScrlViewer, this.itcSearchChannelList, paddingTop);
                }
                else
                {
                    int mostViewedCount = this.itcMostViewedChannelList.Items.Count;
                    int paddingTop = 51 + (int)bdrCategoryList.ActualHeight + (int)bdrCountryList.ActualHeight + (mostViewedCount > 0 ? 50 : 0);
                    ChannelHelpers.ChangeChannelViewPortOpenedProperty(this.ScrlViewer, this.itcMostViewedChannelList, paddingTop);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnClearSearchClick(object param)
        {
            this.TxtSearch.Text = String.Empty;
        }

        private void OnShowMoreClick(object param)
        {
            try
            {
                ShowCountrySelectPreview(null);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnShowMoreClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnBackClick(object param)
        {
            try
            {
                RingIDViewModel.Instance.OnStreamAndChannelCommand(true);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnBackClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowCountrySelectPreview(dynamic param)
        {
            try
            {
                this._CountryListPanel = new UCStreamAndChannelCountryListPanel();
                this._CountryListPanel.OnCountrySelect += (CountryCodeModel countryModel, string searchParam) => 
                {
                    if (countryModel != null)
                    {
                        dynamic prevParam = new ExpandoObject();
                        prevParam.SearchParam = searchParam;

                        ParamModel paramModel = new ParamModel();
                        paramModel.Title = countryModel.CountryName + " - Channels";
                        paramModel.ActionType = AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST;
                        paramModel.Param = countryModel;
                        paramModel.PrevParam = prevParam;
                        paramModel.PrevViewType = StreamAndChannelConstants.TypeChannelDiscoveryPanel;
                        StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel, paramModel);
                    }
                    this.HideCountrySelectPreview(true);
                };
                this.pnlCountrySelectContainer.Child = this._CountryListPanel;
                this.IsCountrySelectMode = true;

                this._CountryListPanel.InilializeViewer(param != null ? param.SearchParam : null);
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowCountrySelectPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideCountrySelectPreview(bool isRefresh = false)
        {
            try
            {
                if (this._CountryListPanel != null)
                {
                    this._CountryListPanel.Dispose();
                    this._CountryListPanel = null;
                }
                this.IsCountrySelectMode = false;
                this.pnlCountrySelectContainer.Child = null;
                if (isRefresh)
                {
                    this.ChangeScrollOpenStatus();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: HideCountrySelectPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.HideCountrySelectPreview();

            if (this._WindowResizeTimer != null)
            {
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer = null;
            }
            if (this._ScrollChangeTimer != null)
            {
                this._ScrollChangeTimer.Stop();
                this._ScrollChangeTimer = null;
            }
            if (this._FilterTimer != null)
            {
                this._FilterTimer.Stop();
                this._FilterTimer = null;
            }

            this.TxtSearch.TextChanged -= SearchBox_TextChanged;
            this.itcMostViewedChannelList.SizeChanged -= ScrlViewer_SizeChanged;
            this.itcSearchChannelList.SizeChanged -= ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged -= ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged -= ScrlViewer_SizeChanged;
            this.ClearValue(UCChannelDiscoveryPanel.LoadingStatusProperty);

            Task.Factory.StartNew(() =>
            {
                ChannelViewModel.Instance.ChannelMostViewList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                ChannelViewModel.Instance.ChannelSearchList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
            });
            
            this.itcMostViewedChannelList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcMostViewedChannelList.ItemsSource = null;
            this.itcSearchChannelList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcSearchChannelList.ItemsSource = null;
            this.itcCountryList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcCountryList.ItemsSource = null;
            this.itcCategoryList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcCategoryList.ItemsSource = null;
            ChannelViewModel.Instance.ChannelSearchList.Clear();
            this.CountryList.Clear();
            this.DataContext = null;
            this.CountryList = null;
            this.IsSearchMode = false;
            this._PrevSearchText = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty LoadingStatusProperty = DependencyProperty.Register("LoadingStatus", typeof(int), typeof(UCChannelDiscoveryPanel), new PropertyMetadata(StatusConstants.NO_DATA, OnLoadingStatusChanged));

        public int LoadingStatus
        {
            get { return (int)GetValue(LoadingStatusProperty); }
            set
            {
                SetValue(LoadingStatusProperty, value);
            }
        }

        public ICommand ShowMoreCommand
        {
            get
            {
                if (_ShowMoreCommand == null)
                {
                    _ShowMoreCommand = new RelayCommand((param) => OnShowMoreClick(param));
                }
                return _ShowMoreCommand;
            }
        }

        public ICommand ClearSearchCommand
        {
            get
            {
                if (_ClearSearchCommand == null)
                {
                    _ClearSearchCommand = new RelayCommand((param) => OnClearSearchClick(param));
                }
                return _ClearSearchCommand;
            }
        }

        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand((param) => OnBackClick(param));
                }
                return _BackCommand;
            }
        }

        public bool IsSearchMode
        {
            get { return _IsSearchMode; }
            set
            {
                if (_IsSearchMode == value)
                    return;

                _IsSearchMode = value;
                this.OnPropertyChanged("IsSearchMode");
            }
        }

        public bool IsCountrySelectMode
        {
            get { return _IsCountrySelectMode; }
            set
            {
                if (_IsCountrySelectMode == value)
                    return;

                _IsCountrySelectMode = value;
                this.OnPropertyChanged("IsCountrySelectMode");
            }
        }

        public ObservableCollection<CountryCodeModel> CountryList
        {
            get { return _CountryList; }
            set
            {
                _CountryList = value;
                OnPropertyChanged("CountryList");
            }
        }

        #endregion Property
    }
}

﻿using log4net;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.Utility;
using View.Utility.Channel;
using View.Utility.Chat;
using View.Utility.Recorder;
using View.Utility.Stream;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for WNChannelPreview.xaml
    /// </summary>
    public partial class WNChannelPreview : Window, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WNChannelPreview).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private ICommand _FullViewCommand;

        static WNChannelPreview()
        {
            if (System.Windows.Application.Current != null)
            {
                RingIDSettings = System.Windows.Application.Current.FindResource("RingIDSettings");
            }
        }

        public WNChannelPreview()
        {
            InitializeComponent();
            this.DataContext = null;
            this.MouseDown += delegate { DragMove(); };
        }

        #region Event Handler

        #endregion

        #region Utility Methods

        public void ShowWindow()
        {
            this.Show();

            this.DataContext = this;
            this.imgStreamPreview.SetBinding(Image.SourceProperty, new System.Windows.Data.Binding { Path = new PropertyPath("StreamViewModel.StreamRenderModel.RenderSource"), Source = RingIDSettings });
        }

        public void CloseWindow(object param = null)
        {
            this.imgStreamPreview.ClearValue(Image.SourceProperty);
            this.DataContext = null;
            this.Owner = null;
            this.Close();
        }

        private new void Show()
        {
            this.Topmost = true;
            base.Show();

            this.Owner = System.Windows.Application.Current.MainWindow;
            System.Drawing.Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
            this.Left = workingArea.Right - this.ActualWidth - 3;
            this.Top = workingArea.Top + 3; //workingArea.Bottom - this.ActualHeight;
        }

        private void OnFullViewClick(object param)
        {
            try
            {
                ChannelViewModel.Instance.OnChannelViewCommand(StreamViewModel.Instance.ChannelInfoModel);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnFullViewClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property

        public ICommand FullViewCommand
        {
            get
            {
                if (_FullViewCommand == null)
                {
                    _FullViewCommand = new RelayCommand((param) => OnFullViewClick(param));
                }
                return _FullViewCommand;
            }
        }

        #endregion
    }
}

﻿using Auth.Service.Images;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Dynamic;
using System.Linq;
using System.Media;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Channel;
using View.Utility.GIF;
using View.Utility.Images;
using View.Utility.WPFMessageBox;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelInfoPanel.xaml
    /// </summary>
    public partial class UCMyChannelInfoPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelInfoPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private static object _RingIDSettings = null;
        private List<ChannelCategoryDTO> _SelectedCategoryList = new List<ChannelCategoryDTO>();
        private ObservableCollection<CountryCodeModel> _CountryModelList = new ObservableCollection<CountryCodeModel>();
        private HashSet<int> _DefaultStatusList = new HashSet<int>();
        private HashSet<int> _UploadStatusList = new HashSet<int>();
        private ChannelModel _ChannelModel = new ChannelModel();
        public delegate void OnUploadComplete();
        public event OnUploadComplete _UploadComplete;
        public delegate void OnProPicUploadComplete();
        public event OnProPicUploadComplete _UploadProPicComplete;
        private bool _IsChannelUpdateRunning;
        private string _ChannelTitle = string.Empty;
        private string _ChannelDescription = string.Empty;
        private String _SelectedCountry;

        #region ProfileImage

        private Canvas _ProfileImgCanvas;
        private System.Windows.Controls.Image _ProImgControl;
        private BitmapImage _BitmapProfileImage;
        private System.Windows.Point _ProImageMousePosition = new System.Windows.Point(-1, -1);
        private BackgroundWorker _ProfileImageUploadWorker = null;

        private const int STATE_INIT = 0;
        private const int STATE_BROWSE = 1;
        private const int STATE_UPLOADING = 2;
        private const int STATE_UPLOADED = 3;

        private const int PRO_PIC_UPLOAD_SCREEN_WIDTH = 140;
        private const int PRO_PIC_UPLOAD_SCREEN_HEIGHT = 140;

        private double _ProfileImageWidth;
        private double _ProfileImageHeight;
        private double _CropProfileX = 0;
        private double _CropProfileY = 0;
        private int _ProfileImageUploadState;
        private ICommand _ProfileImageChangeCommand;
        private string _ProfileImageUrl = string.Empty;
        private string _PreviousProImageUrl;
        private bool isProfileImageClicked;
        private bool proImageUrlChanged = false;
        #endregion

        #region CoverImage
        private System.Windows.Point _CoverImgMouseCapturePosition = new System.Windows.Point(-1, -1);
        private BackgroundWorker _CoverImageUploadWorker = null;
        private System.Windows.Controls.Image _UiImage;
        private BitmapImage _BitmapCoverImage;
        public ICommand _CoverImageChangeCommand;

        private int _CoverImageUploadState;

        private double _CoverImageWidth;
        private double _CoverImageHeight;
        private double _CropCoverImageX = 0;
        private double _CropCoverImageY = 0;
        private bool isCoverImageClicked;
        private string _CoverImageUrl = string.Empty;
        private string _PreViousCoverImageUrl;
        private bool coverImageUrlChanged = false;
        public ICommand _UpdateCommand;
        #endregion


        #region Constructor

        public UCMyChannelInfoPanel()
        {
            InitializeComponent();
            this._CoverImageUploadWorker = new BackgroundWorker();
            this._CoverImageUploadWorker.DoWork += CoverImageUploadWorker_DoWork;
            this._CoverImageUploadWorker.RunWorkerCompleted += CoverImageUploadWorker_RunWorkerCompleted;

            this._ProfileImageUploadWorker = new BackgroundWorker();
            this._ProfileImageUploadWorker.DoWork += Profile_Image_UploadWorker_DoWork;
            this._ProfileImageUploadWorker.RunWorkerCompleted += Profile_ImageUploadWorker_RunWorkerCompleted;

            if (Application.Current != null)
            {
                _RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }

            if (ChannelViewModel.Instance.ChannelCategoryList == null || ChannelViewModel.Instance.ChannelCategoryList.Count == 0)
            {
                new ThrdGetChannelCategoryList().Start();
            }

            for (int i = 2; i < DefaultSettings.COUNTRY_MOBILE_CODE.Length / 2; i++)
            {
                string name = DefaultSettings.COUNTRY_MOBILE_CODE[i, 0];
                string code = DefaultSettings.COUNTRY_MOBILE_CODE[i, 1];
                CountryModelList.Add(new CountryCodeModel(name, code));
            }
        }

        #endregion Constructor

        #region Event Handler

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

        }

        public void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            this.DataContext = this;
            this.comboboxCategoryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ChannelViewModel.ChannelCategoryList"), Source = _RingIDSettings });
            this.comboboxCountryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("CountryModelList") });
            this.comboboxCategoryList.SelectionChanged += comboboxCategoryList_SelectionChanged;
            this.comboboxCountryList.SelectionChanged += comboboxCountryList_SelectionChanged;
            this.rtbTitle.PreviewKeyDown += rtbTitle_KeyDown;
            this.rtbdescription.PreviewKeyDown += rtbdescription_KeyDown;
        }

        public void ReleaseViewer()
        {
            this.Dispose();
            this.comboboxCategoryList.SelectionChanged -= comboboxCategoryList_SelectionChanged;
            this.comboboxCountryList.SelectionChanged -= comboboxCountryList_SelectionChanged;
            this.rtbTitle.KeyDown -= rtbTitle_KeyDown;
            this.rtbdescription.KeyDown -= rtbdescription_KeyDown;
        }

        public void Dispose()
        {
            if (IsChannelUpdateRunning == false)
            {
                Application.Current.Dispatcher.BeginInvoke(delegate
                {
                    this.DataContext = null;
                    _ProImgControl.Source = null;
                    _BitmapProfileImage = null;
                    _BitmapCoverImage = null;
                    ProfileImageUploadState = STATE_INIT;
                    CoverImageUploadState = STATE_INIT;
                    coverImgCanvus.Children.Clear();
                    ChannelTitle = string.Empty;
                    ChannelDescription = string.Empty;
                    comboboxCategoryList.ClearValue(ItemsControl.ItemsSourceProperty);
                    comboboxCategoryList.ItemsSource = null;
                    ImageBehavior.SetAnimatedSource(defaultImgeLoader, null);
                    defaultImgeLoader.Source = null;
                    ImageBehavior.SetAnimatedSource(imgControl, null);
                    imgControl.Source = null;
                });
            }
        }

        public void SetDefaultValue(ChannelModel chnnlModel)
        {
            this._ChannelModel = chnnlModel;
            this.SetCoverImage();
            if (chnnlModel != null)
            {
                new ThrdGetChannelDetails(chnnlModel.ChannelID, (channelDTO) =>
                {
                    if (channelDTO != null)
                    {
                        foreach (ChannelCategoryDTO ctgDTO in channelDTO.CategoryList)
                        {
                            ChannelCategoryModel ctgModel = new ChannelCategoryModel(ctgDTO);
                            _ChannelModel.CategoryList.InvokeAdd(ctgModel);
                            int index = ChannelViewModel.Instance.ChannelCategoryList.Where(P => P.CategoryID == ctgModel.CategoryID).FirstOrDefault().CategoryID;
                            Application.Current.Dispatcher.BeginInvoke(delegate
                            {
                                comboboxCategoryList.SelectedIndex = index > 0 ? index - 1 : 0;
                            }, System.Windows.Threading.DispatcherPriority.Send);
                        }
                    }
                    return 0;
                }).Start();

                ChannelTitle = chnnlModel.Title;
                ChannelDescription = chnnlModel.Description;
                rtbTitle.Document.Blocks.Clear();
                rtbTitle.Document.Blocks.Add(new Paragraph(new Run(ChannelTitle)));
                rtbdescription.Document.Blocks.Clear();
                rtbdescription.Document.Blocks.Add(new Paragraph(new Run(ChannelDescription)));
                CountryCodeModel cntryModel = CountryModelList.Where(P => P.CountryName == chnnlModel.Country).FirstOrDefault();
                if (cntryModel != null)
                {
                    comboboxCountryList.SelectedIndex = CountryModelList.IndexOf(cntryModel);
                    _SelectedCountry = cntryModel.CountryName;
                }
                else
                {
                    comboboxCountryList.SelectedIndex = 37;
                    _SelectedCountry = "Canada";
                    _ChannelModel.Country = _SelectedCountry;
                }
            }
        }

        private void SetCoverImage(bool isFromCancel = false)
        {
            if (_ChannelModel != null && !string.IsNullOrEmpty(_ChannelModel.CoverImage))
            {
                string imageUrl = _ChannelModel.CoverImage;
                int imgType = ImageUtility.IMG_CROP;
                string convertedCoverUrl = HelperMethods.ConvertUrlToPurl(imageUrl, imgType);
                string convertedName = HelperMethods.ConvertUrlToName(imageUrl, imgType);
                string imagePath = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedName;
                BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);

                if (image != null)
                {
                    UpdateCoverImageToCanvas(new System.Windows.Controls.Image { Source = image });
                }
                else if (isFromCancel == false)
                {
                    if (this.defaultImgeLoader.Source == null)
                    {
                        BitmapImage _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                        defaultImgeLoader.Source = _LoadingIcon;
                        ImageBehavior.SetAnimatedSource(defaultImgeLoader, _LoadingIcon);
                    }
                    IsChannelUpdateRunning = true;
                    _DefaultStatusList.Add(ChannelConstants.CHANNEL_COVER_IMAGE);
                    DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedName, convertedCoverUrl, _ChannelModel, SettingsConstants.IMAGE_TYPE_CHANNEL, true);
                    ImageDictionaries.Instance.RING_IMAGE_QUEUE.Enqueue(obj);
                    DownloadCoverImage(obj, imagePath);
                }
            }
            else
            {
                UpdateCoverImageToCanvas(new System.Windows.Controls.Image { Source = ImageObjects.DEFAULT_COVER_IMAGE });
            }
        }

        private void DownloadCoverImage(DownloadImageShortInfo obj, string imagePath)
        {
            try
            {
                BackgroundWorker bgworker = new BackgroundWorker();
                bgworker.DoWork += (s, e) =>
                {
                    e.Result = ImageUtility.DownloadImage(obj.ImageUrl, RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + obj.ImageName, ServerAndPortSettings.IMAGE_SERVER_RESOURCE + System.IO.Path.AltDirectorySeparatorChar);
                };
                bgworker.RunWorkerCompleted += (s, e) =>
                {
                    if (e.Result != null && e.Result is bool && (bool)e.Result)
                    {
                        if (!string.IsNullOrEmpty(imagePath))
                        {
                            Application.Current.Dispatcher.BeginInvoke(delegate
                            {
                                BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);
                                if (image != null)
                                {
                                    UpdateCoverImageToCanvas(new System.Windows.Controls.Image { Source = image });
                                }
                            });
                        }
                    }
                    this._DefaultStatusList.Remove(ChannelConstants.CHANNEL_COVER_IMAGE);
                    if (_DefaultStatusList.Count == 0)
                    {
                        IsChannelUpdateRunning = false;
                        Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            ImageBehavior.SetAnimatedSource(defaultImgeLoader, null);
                            defaultImgeLoader.Source = null;
                        });
                    }
                    ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
                };
                bgworker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                UpdateCoverImageToCanvas(new System.Windows.Controls.Image { Source = ImageObjects.DEFAULT_COVER_IMAGE });
                this._DefaultStatusList.Remove(ChannelConstants.CHANNEL_COVER_IMAGE);
                log.Error(ex.Message + ex.StackTrace);
            }
        }

        private void UpdateCoverImageToCanvas(System.Windows.Controls.Image image)
        {
            Canvas.SetLeft(image, 0);
            Canvas.SetTop(image, 0);
            coverImgCanvus.Children.Clear();
            coverImgCanvus.Children.Add(image);
        }

        private void SetProfileImage(bool isFromCancel = false)
        {
            if (_ChannelModel != null && !string.IsNullOrEmpty(_ChannelModel.ProfileImage))
            {
                string imageUrl = _ChannelModel.ProfileImage;
                int imgType = ImageUtility.IMG_CROP;
                string convertedProfileUrl = HelperMethods.ConvertUrlToPurl(imageUrl, imgType);
                string convertedName = HelperMethods.ConvertUrlToName(imageUrl, imgType);
                string imagePath = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedName;
                BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);

                if (image != null)
                {
                    UpdateProfileImageToCanvas(image);
                }
                else if (isFromCancel == false)
                {
                    IsChannelUpdateRunning = true;
                    if (this.defaultImgeLoader.Source == null)
                    {
                        BitmapImage _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                        defaultImgeLoader.Source = _LoadingIcon;
                        ImageBehavior.SetAnimatedSource(defaultImgeLoader, _LoadingIcon);
                    }
                    _DefaultStatusList.Add(ChannelConstants.CHANNEL_PRO_IMAGE);
                    DownloadImageShortInfo obj = new DownloadImageShortInfo(convertedName, convertedProfileUrl, _ChannelModel, SettingsConstants.IMAGE_TYPE_CHANNEL, true);
                    ImageDictionaries.Instance.RING_IMAGE_QUEUE.Enqueue(obj);
                    DownloadProfileImage(obj, imagePath);
                }
            }
            else
            {
                UpdateProfileImageToCanvas(ImageUtility.GetBitmapImage(ImageLocation.CHANNEL_PROFILE_UNKNOWN));
            }
        }

        private void DownloadProfileImage(DownloadImageShortInfo obj, string imagePath)
        {
            try
            {
                BackgroundWorker bgworker = new BackgroundWorker();
                bgworker.DoWork += (s, e) =>
                {
                    e.Result = ImageUtility.DownloadImage(obj.ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + obj.ImageName, ServerAndPortSettings.IMAGE_SERVER_RESOURCE + System.IO.Path.AltDirectorySeparatorChar);
                };
                bgworker.RunWorkerCompleted += (s, e) =>
                {
                    if (e.Result != null && e.Result is bool && (bool)e.Result)
                    {
                        if (!string.IsNullOrEmpty(imagePath))
                        {
                            Application.Current.Dispatcher.BeginInvoke(delegate
                            {
                                BitmapImage image = ImageUtility.GetBitmapImageOfDynamicResource(imagePath);
                                if (image != null)
                                {
                                    UpdateProfileImageToCanvas(image);
                                }
                            });
                        }
                    }
                    this._DefaultStatusList.Remove(ChannelConstants.CHANNEL_PRO_IMAGE);
                    if (_DefaultStatusList.Count == 0)
                    {
                        IsChannelUpdateRunning = false;
                        Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            ImageBehavior.SetAnimatedSource(defaultImgeLoader, null);
                            defaultImgeLoader.Source = null;
                        });
                    }
                    ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
                };
                bgworker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                this._DefaultStatusList.Remove(ChannelConstants.CHANNEL_PRO_IMAGE);
                UpdateProfileImageToCanvas(ImageObjects.DEFAULT_COVER_IMAGE);
                log.Error(ex.Message + ex.StackTrace);
            }
        }

        private void UpdateProfileImageToCanvas(BitmapImage image)
        {
            if (_ProImgControl != null)
            {
                _ProImgControl.Source = image;
                Canvas.SetLeft(_ProImgControl, 0);
                Canvas.SetTop(_ProImgControl, 0);
                _ProfileImgCanvas.UpdateLayout();
            }
        }

        private void OnUpdateCommand()
        {
            try
            {
                if (string.IsNullOrEmpty(ChannelTitle))
                {
                    UIHelperMethods.ShowWarning("Please give a Channel Title!");
                    //CustomMessageBox.ShowError("Please give a Channel Title");
                    return;
                }
                this._UploadComplete = null;
                this._UploadProPicComplete = null;
                bool isNoCtgChanged = _SelectedCategoryList.Where(P => !_ChannelModel.CategoryList.Any(Q => Q.CategoryID == P.CategoryID)).ToList().Count <= 0;
                bool isNoTitleChanged = !string.IsNullOrEmpty(_ChannelModel.Title) && _ChannelModel.Title.Equals(ChannelTitle);
                bool isNoDesChanged = (string.IsNullOrEmpty(_ChannelModel.Description) && string.IsNullOrEmpty(ChannelDescription)) || (!string.IsNullOrEmpty(_ChannelModel.Description) && _ChannelModel.Description.Equals(ChannelDescription));
                bool isNoCntryChanged = !string.IsNullOrEmpty(_ChannelModel.Country) && _ChannelModel.Country.Equals(_SelectedCountry);

                bool isChannelIfoChanged = !(isNoCtgChanged && isNoTitleChanged && isNoDesChanged && isNoCntryChanged);

                if (isChannelIfoChanged == false && _BitmapProfileImage == null && _BitmapCoverImage == null)
                {
                    UIHelperMethods.ShowWarning("No Information Changed!");
                    //CustomMessageBox.ShowError("No Information Changed !");
                    return;
                }

                BitmapImage _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                imgControl.Source = _LoadingIcon;
                ImageBehavior.SetAnimatedSource(imgControl, _LoadingIcon);

                IsChannelUpdateRunning = true;
                if (_BitmapProfileImage != null && !(proImageUrlChanged == false && !string.IsNullOrEmpty(_PreviousProImageUrl) && !string.IsNullOrEmpty(_ProfileImageUrl) && _PreviousProImageUrl.Equals(_ProfileImageUrl)))
                {
                    proImageUrlChanged = true;
                    this._UploadStatusList.Add(ChannelConstants.CHANNEL_PRO_IMAGE);
                    UploadAndSaveProfileImage();
                }
                if (_BitmapCoverImage != null && !(coverImageUrlChanged == false && !string.IsNullOrEmpty(_PreViousCoverImageUrl) && !string.IsNullOrEmpty(_CoverImageUrl) && _PreViousCoverImageUrl.Equals(_CoverImageUrl)))
                {
                    coverImageUrlChanged = true;
                    this._UploadStatusList.Add(ChannelConstants.CHANNEL_COVER_IMAGE);
                    if (proImageUrlChanged == false)
                    {
                        UploadAndSaveCoverImage();
                    }
                    else
                    {
                        this._UploadProPicComplete += () =>
                        {
                            UploadAndSaveCoverImage();
                        };
                    }
                }

                this._UploadComplete += () =>
                {
                    this._UploadStatusList.Clear();
                    if (_SelectedCategoryList.Count == 0)
                    {
                        _SelectedCategoryList.Add(new ChannelCategoryDTO { CategoryID = 1, CategoryName = "All" });
                    }
                    Guid channelId = _ChannelModel != null ? _ChannelModel.ChannelID : Guid.Empty;
                    if (proImageUrlChanged)
                    {
                        _UploadStatusList.Add(ChannelConstants.CHANNEL_PRO_IMAGE);
                        new ThrdAddChannelProfileImage(channelId, _ProfileImageUrl, (int)_ProfileImageWidth, (int)_ProfileImageHeight, (status) =>
                        {
                            if (status)
                            {
                                _BitmapProfileImage = null;
                                ProfileImageUploadState = STATE_INIT;
                                _ChannelModel.ProfileImage = this._ProfileImageUrl;
                            }
                            _UploadStatusList.Remove(ChannelConstants.CHANNEL_PRO_IMAGE);

                            if (_UploadStatusList.Count == 0)
                            {
                                IsChannelUpdateRunning = false;
                                DisposeLoader(status);
                            }
                            return 0;
                        }).Start();
                    }
                    if (coverImageUrlChanged)
                    {
                        _UploadStatusList.Add(ChannelConstants.CHANNEL_COVER_IMAGE);
                        new ThrdAddChannelCoverImage(channelId, _CoverImageUrl, (int)_CoverImageWidth, (int)_CoverImageHeight, (int)_CropCoverImageX, (int)_CropCoverImageY, (status) =>
                        {
                            if (status)
                            {
                                _BitmapCoverImage = null;
                                CoverImageUploadState = STATE_INIT;
                                _ChannelModel.CoverImage = this._CoverImageUrl;
                            }
                            _UploadStatusList.Remove(ChannelConstants.CHANNEL_COVER_IMAGE);

                            if (_UploadStatusList.Count == 0)
                            {
                                IsChannelUpdateRunning = false;
                                DisposeLoader(status);
                            }
                            return 0;
                        }).Start();
                    }
                    if (isChannelIfoChanged)
                    {
                        _UploadStatusList.Add(ChannelConstants.CHANNEL_INFO);
                        new ThrdUpdateChannelInfo(channelId, ChannelTitle, _SelectedCountry, ChannelDescription, _SelectedCategoryList, (status) =>
                        {
                            if (status)
                            {
                                _ChannelModel.Title = ChannelTitle;
                                _ChannelModel.Description = ChannelDescription;
                                _ChannelModel.Country = _SelectedCountry;
                                List<ChannelCategoryModel> ctgModeList = ChannelViewModel.Instance.ChannelCategoryList.Where(P => _SelectedCategoryList.Any(Q => Q.CategoryID == P.CategoryID)).ToList();
                                _ChannelModel.CategoryList.Clear();
                                foreach (ChannelCategoryModel ctgModel in ctgModeList)
                                {
                                    _ChannelModel.CategoryList.InvokeAdd(ctgModel);
                                }
                            }

                            _UploadStatusList.Remove(ChannelConstants.CHANNEL_INFO);
                            if (_UploadStatusList.Count == 0)
                            {
                                IsChannelUpdateRunning = false;
                                DisposeLoader(status);
                            }
                            return 0;
                        }).Start();
                    }
                };

                if (proImageUrlChanged == false && coverImageUrlChanged == false)
                {
                    _UploadComplete();
                }
            }
            catch (Exception ex)
            {
                IsChannelUpdateRunning = false;
                log.Error("Exception occured OnUpdateCommand => " + ex.Message + ex.StackTrace);
            }
        }

        private void DisposeLoader(bool isSucces = false)
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                ImageBehavior.SetAnimatedSource(imgControl, null);
                if (imgControl.Source != null && isSucces) UIHelperMethods.ShowMessageWithTimerFromNonThread(pnlBody, "Succesfully Upadted Information!");
                else if (!DefaultSettings.IsInternetAvailable && imgControl.Source != null) UIHelperMethods.ShowWarning(NotificationMessages.INTERNET_UNAVAILABLE);
                //CustomMessageBox.ShowError(NotificationMessages.INTERNET_UNAVAILABLE);
                imgControl.Source = null;
            });
        }


        private void OnCoverImageChange(object param)
        {
            try
            {
                int type = int.Parse(param.ToString());
                if (type == STATE_BROWSE)
                {
                    OpenFileDialog dialog = new OpenFileDialog();
                    dialog.Title = "Select a picture";
                    dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
                    if ((bool)dialog.ShowDialog())
                    {
                        Bitmap bmp = ImageUtility.GetBitmapOfDynamicResource(dialog.FileName);
                        if (bmp.Width < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH || bmp.Height < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT)
                            UIHelperMethods.ShowWarning("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                        //CustomMessageBox.ShowError("Cover photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                        else
                        {
                            CoverImageUploadState = STATE_BROWSE;
                            coverImageUrlChanged = true;
                            _BitmapCoverImage = ImageUtility.GetResizedImageToFit(bmp, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                            _CoverImageWidth = _BitmapCoverImage.Width;
                            _CoverImageHeight = _BitmapCoverImage.Height;
                            _UiImage = new System.Windows.Controls.Image { Source = _BitmapCoverImage };

                            double left = (coverImgCanvus.Width - _CoverImageWidth) / 2;
                            Canvas.SetLeft(_UiImage, left);
                            double top = (coverImgCanvus.Height - _CoverImageHeight) / 2;
                            Canvas.SetTop(_UiImage, top);
                            coverImgCanvus.Children.Clear();
                            coverImgCanvus.Children.Add(_UiImage);
                        }
                    }
                }
                else if (type == STATE_UPLOADED)
                {
                    //Save Change
                    //UploadAndSaveCoverImage();
                }
                else if (type == STATE_INIT)
                {
                    //Cancel Change
                    ResetCoverImageChange();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCoverImageChange() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UploadAndSaveCoverImage()
        {
            try
            {
                Bitmap btmap = ImageUtility.ConvertBitmapImageToBitmap(_BitmapCoverImage);
                byte[] byteArray = ImageUtility.ConvertBitmapToByteArray(btmap);
                CoverImageUploadState = STATE_UPLOADING;
                dynamic data = new ExpandoObject();
                data.Bitmap = btmap;
                data.ByteArray = byteArray;
                if (_CoverImageUploadWorker.IsBusy != true) _CoverImageUploadWorker.RunWorkerAsync(data);
            }
            catch (Exception ex)
            {
                log.Error("Error: UploadAndSaveCoverImage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ResetCoverImageChange()
        {
            _BitmapCoverImage = null;
            coverImageUrlChanged = false;
            _CoverImageUrl = string.Empty;
            _PreViousCoverImageUrl = null;
            CoverImageUploadState = STATE_INIT;
            _CropCoverImageX = 0;
            _CropCoverImageY = 0;
            _CoverImgMouseCapturePosition = new System.Windows.Point(-1, -1);
            SetCoverImage(true);
        }

        private void OnProfileImageChangeCommand(object param)
        {
            try
            {
                int type = int.Parse(param.ToString());
                if (type == 1)
                {
                    OpenFileDialog op = new OpenFileDialog();
                    op.Title = "Select a picture";
                    op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                      "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                      "Portable Network Graphic (*.png)|*.png";

                    if ((bool)op.ShowDialog())
                    {
                        Bitmap bmp = ImageUtility.GetBitmapOfDynamicResource(op.FileName);
                        if (bmp.Width < RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH || bmp.Height < RingIDSettings.PROFILE_PIC_MINIMUM_HEIGHT)
                            UIHelperMethods.ShowWarning("Profile photo's minimum resulotion: " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH + " x " + RingIDSettings.PROFILE_PIC_MINIMUM_HEIGHT);
                        // CustomMessageBox.ShowError("Profile photo's minimum resulotion: " + RingIDSettings.PROFILE_PIC_MINIMUM_WIDTH + " x " + RingIDSettings.PROFILE_PIC_MINIMUM_HEIGHT);
                        else
                        {
                            ProfileImageUploadState = STATE_BROWSE;
                            proImageUrlChanged = true;
                            _BitmapProfileImage = ImageUtility.GetResizedImageToFit(bmp, RingIDSettings.PROFILE_PIC_SCREEN_WIDTH, RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT);
                            _ProImgControl.Source = _BitmapProfileImage;

                            _ProfileImageWidth = _BitmapProfileImage.Width;
                            _ProfileImageHeight = _BitmapProfileImage.Height;
                            double left = (PRO_PIC_UPLOAD_SCREEN_WIDTH - _ProfileImageWidth) / 2;
                            Canvas.SetLeft(_ProImgControl, left);
                            double top = (PRO_PIC_UPLOAD_SCREEN_HEIGHT - _ProfileImageHeight) / 2;
                            Canvas.SetTop(_ProImgControl, top);
                            _ProfileImgCanvas.UpdateLayout();
                        }
                    }
                }
                else if (type == 3)
                {
                    //Save Change
                    //UploadAndSaveProfileImage();
                }
                else if (type == 4)
                {
                    //Cancel Change
                    CancelProfileImageChange();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnProfileImageChangeCommand() ==> " + ex.StackTrace);
            }
        }

        private void UploadAndSaveProfileImage()
        {
            try
            {
                Bitmap btmap = ImageUtility.ConvertBitmapImageToBitmap(_BitmapProfileImage);
                byte[] byteArray = ImageUtility.ConvertBitmapToByteArray(btmap);
                ProfileImageUploadState = STATE_UPLOADING;
                dynamic data = new ExpandoObject();
                data.Bitmap = btmap;
                data.ByteArray = byteArray;
                if (_ProfileImageUploadWorker.IsBusy != true)
                {
                    _ProfileImageUploadWorker.RunWorkerAsync(data);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UploadAndSaveProfileImage() ==> " + ex.StackTrace);
            }
        }

        private void CancelProfileImageChange()
        {
            ProfileImageUploadState = STATE_INIT;
            proImageUrlChanged = false;
            _BitmapProfileImage = null;
            _ProfileImageUrl = string.Empty;
            _PreviousProImageUrl = null;
            _ProImgControl.Source = null;
            ImageBehavior.SetAnimatedSource(_ProImgControl, null);
            SetProfileImage(true);
        }

        private void OnActivateDeactivateCommand(object param)
        {
            int activateType = param != null ? Int32.Parse(param.ToString()) : 1;
            IsChannelUpdateRunning = true;
            new ThrdActivateDeactivateChannel(_ChannelModel.ChannelID, activateType, (status) =>
            {
                if (status)
                {
                    ChannelModel.ChannelStatus = activateType;
                }
                IsChannelUpdateRunning = false;
                return 0;
            }).Start();
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Event Handler

        #region Category

        private void rtbTitle_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textTitle = new TextRange(rtbTitle.Document.ContentStart, rtbTitle.Document.ContentEnd).Text;
            ChannelTitle = textTitle.Replace(Environment.NewLine, "");
        }

        private void rtbdescription_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textDescription = new TextRange(rtbdescription.Document.ContentStart, rtbdescription.Document.ContentEnd).Text;
            ChannelDescription = textDescription.Replace(Environment.NewLine, "");
        }

        private void rtbTitle_KeyDown(object sender, KeyEventArgs e)
        {
            var textTitle = new TextRange(rtbTitle.Document.ContentStart, rtbTitle.Document.ContentEnd).Text;
            if (textTitle.Length >= 150)
            {
                e.Handled = true;
                SystemSounds.Asterisk.Play();
                UIHelperMethods.ShowWarning("Channel Title is too large!");
                // CustomMessageBox.ShowError("Channel Title is too large!");
                return;
            }
            rtbTitle.TextChanged -= rtbTitle_TextChanged;
            rtbTitle.TextChanged += rtbTitle_TextChanged;
        }

        private void rtbdescription_KeyDown(object sender, KeyEventArgs e)
        {
            var textDescription = new TextRange(rtbdescription.Document.ContentStart, rtbdescription.Document.ContentEnd).Text;
            if (textDescription.Length >= 250)
            {
                e.Handled = true;
                SystemSounds.Asterisk.Play();
                UIHelperMethods.ShowWarning("Channel description is too large!");
                return;
            }
            rtbdescription.TextChanged -= rtbdescription_TextChanged;
            rtbdescription.TextChanged += rtbdescription_TextChanged;
        }

        private void comboboxCategoryList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _SelectedCategoryList.Clear();
            if ((sender as ComboBox).SelectedItem is ChannelCategoryModel)
            {
                ChannelCategoryModel ctgModel = (ChannelCategoryModel)(sender as ComboBox).SelectedItem;
                _SelectedCategoryList.Add(new ChannelCategoryDTO { CategoryID = ctgModel.CategoryID, CategoryName = ctgModel.CategoryName });
            }
        }

        private void comboboxCountryList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem is CountryCodeModel)
            {
                _SelectedCountry = ((CountryCodeModel)(sender as ComboBox).SelectedItem).CountryName;
            }
        }

        #endregion

        #region Profile

        private void UIElement_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender is System.Windows.Controls.Image)
            {
                _ProImgControl = (System.Windows.Controls.Image)sender;
                SetProfileImage();
            }
            else if (sender is Canvas)
            {
                _ProfileImgCanvas = (Canvas)sender;
            }
        }

        private void proImgCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (ProfileImageUploadState == STATE_BROWSE && this.isProfileImageClicked)
                {
                    System.Windows.Point position = e.GetPosition(_ProfileImgCanvas);
                    Vector offset = position - _ProImageMousePosition;
                    _ProImageMousePosition = position;
                    double MinX = 0;
                    double MinY = 0;
                    double MaxX = (-_ProfileImageWidth + PRO_PIC_UPLOAD_SCREEN_WIDTH);
                    double MaxY = (-_ProfileImageHeight + PRO_PIC_UPLOAD_SCREEN_HEIGHT);

                    double imgL = Canvas.GetLeft(_ProImgControl);
                    double imgR = Canvas.GetTop(_ProImgControl);

                    if ((Canvas.GetLeft(_ProImgControl) + offset.X) <= MinX
                        && (Canvas.GetLeft(_ProImgControl) + offset.X) >= MaxX
                        && (Canvas.GetTop(_ProImgControl) + offset.Y) <= MinY
                        && (Canvas.GetTop(_ProImgControl) + offset.Y) >= MaxY)
                    {
                        _CropProfileX = Canvas.GetLeft(_ProImgControl) + offset.X;
                        _CropProfileY = Canvas.GetTop(_ProImgControl) + offset.Y;
                        Canvas.SetLeft(_ProImgControl, _CropProfileX);
                        Canvas.SetTop(_ProImgControl, _CropProfileY);
                        _ProfileImgCanvas.UpdateLayout();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: proImgCanvas_MouseMove() ==> " + ex.StackTrace);
            }
        }

        private void proImgCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_ProfileImageUploadState == STATE_BROWSE && _ProfileImgCanvas.CaptureMouse())
                {
                    this.isProfileImageClicked = true;
                    _ProImageMousePosition = e.GetPosition(_ProfileImgCanvas);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: proImgCanvas_MouseLeftButtonDown() ==> " + ex.StackTrace);
            }
        }

        private void proImgCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (ProfileImageUploadState == STATE_BROWSE)
                {
                    this.isProfileImageClicked = false;
                    _ProfileImgCanvas.ReleaseMouseCapture();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: proImgCanvas_MouseLeftButtonUp() ==> " + ex.StackTrace);
            }
        }

        private void Profile_Image_UploadWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                dynamic data = (ExpandoObject)e.Argument;
                data.Response = ProfileCoverImageUpload.Instance.UploadToImageServerWebRequest(data.ByteArray, SettingsConstants.TYPE_PROFILE_IMAGE, (int)-_CropProfileX, (int)-_CropProfileY, (int)_ProfileImageWidth, (int)_ProfileImageHeight);
                e.Result = data;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Profile_ImageUploadWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                dynamic data = (ExpandoObject)e.Result;
                Bitmap btmap = data.Bitmap;
                string _JsonResponse = data.Response;
                JObject jObj = JObject.Parse(_JsonResponse);

                string imageUrl = jObj != null && jObj[JsonKeys.ImageUrl] != null ? (string)jObj[JsonKeys.ImageUrl] : string.Empty;
                if (!string.IsNullOrEmpty(imageUrl))
                {
                    _ProfileImageUrl = imageUrl;
                    string convertedProfileUrl = HelperMethods.ConvertUrlToPurl(imageUrl, ImageUtility.IMG_CROP);
                    btmap.Save(RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + ImageUrlHelper.GetImageNameFromUrl(convertedProfileUrl), ImageFormat.Jpeg);
                    //ProfileImageUploadState = STATE_UPLOADED;
                }
                else
                {
                    log.Info("Failed to Upload Channel Profile Image......=>");
                    //ResetProfileImageChange();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Profile_ImageUploadWorker_RunWorkerCompleted() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                ProfileImageUploadState = STATE_UPLOADED;
                _PreviousProImageUrl = _ProfileImageUrl;
                _UploadStatusList.Remove(ChannelConstants.CHANNEL_PRO_IMAGE);
                if (_UploadStatusList.Count == 0)
                {
                    this._UploadComplete();
                }
                if (this._UploadProPicComplete != null)
                {
                    this._UploadProPicComplete();
                }
            }
        }

        #endregion

        #region CoverPhoto

        private void CoverImageCanvasMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_UiImage != null && coverImgCanvus.CaptureMouse())
                {
                    this.isCoverImageClicked = true;
                    _CoverImgMouseCapturePosition = e.GetPosition(coverImgCanvus);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageCanvasMouseLeftButtonDown() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverImageCanvasMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_UiImage != null)
                {
                    this.isCoverImageClicked = false;
                    coverImgCanvus.ReleaseMouseCapture();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageCanvasMouseLeftButtonUp() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverImageCanvasMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (_UiImage != null && this.isCoverImageClicked)
                {
                    var position = e.GetPosition(coverImgCanvus);
                    var offset = position - _ProImageMousePosition;
                    _ProImageMousePosition = position;
                    double MinX = 0;
                    double MinY = 0;
                    double MaxX = (-_CoverImageWidth + RingIDSettings.COVER_PIC_DISPLAY_WIDTH);
                    double MaxY = (-_CoverImageHeight + RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);

                    if ((Canvas.GetLeft(_UiImage) + offset.X) <= MinX
                        && (Canvas.GetLeft(_UiImage) + offset.X) >= MaxX
                        && (Canvas.GetTop(_UiImage) + offset.Y) <= MinY
                        && (Canvas.GetTop(_UiImage) + offset.Y) >= MaxY)
                    {
                        _CropCoverImageX = Canvas.GetLeft(_UiImage) + offset.X;
                        _CropCoverImageY = Canvas.GetTop(_UiImage) + offset.Y;
                        Canvas.SetLeft(_UiImage, _CropCoverImageX);
                        Canvas.SetTop(_UiImage, _CropCoverImageY);
                        coverImgCanvus.UpdateLayout();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageCanvasMouseMove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverImageUploadWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                dynamic data = (ExpandoObject)e.Argument;
                data.Response = ProfileCoverImageUpload.Instance.UploadToImageServerWebRequest(data.ByteArray, SettingsConstants.TYPE_COVER_IMAGE, (int)-_CropCoverImageX, (int)-_CropCoverImageY, (int)_CoverImageWidth, (int)_CoverImageHeight);
                e.Result = data;
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageUploadWorker_DoWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CoverImageUploadWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                dynamic data = (ExpandoObject)e.Result;
                Bitmap btmap = data.Bitmap;
                string _JsonResponse = data.Response;
                JObject jObj = JObject.Parse(_JsonResponse);
                string imageUrl = jObj != null && jObj[JsonKeys.ImageUrl] != null ? (string)jObj[JsonKeys.ImageUrl] : string.Empty;

                if (!string.IsNullOrEmpty(imageUrl))
                {
                    _CoverImageUrl = (string)jObj[JsonKeys.ImageUrl];
                    btmap.Save(RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + ImageUrlHelper.GetImageNameFromUrl(imageUrl), ImageFormat.Jpeg);
                }
                else
                {
                    log.Info("Failed to save Channel Cover Image......=>");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CoverImageUploadWorker_RunWorkerCompleted() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            finally
            {
                CoverImageUploadState = STATE_UPLOADED;
                _PreViousCoverImageUrl = _CoverImageUrl;
                this._UploadStatusList.Remove(ChannelConstants.CHANNEL_COVER_IMAGE);
                if (_UploadStatusList.Count == 0)
                {
                    this._UploadComplete();
                }
            }
        }

        #endregion

        #endregion Event Handler

        #region Property

        public string ChannelTitle
        {
            get { return _ChannelTitle; }
            set
            {
                if (value == _ChannelTitle) return;
                _ChannelTitle = value;
                this.OnPropertyChanged("ChannelTitle");
            }
        }

        public string ChannelDescription
        {
            get { return _ChannelDescription; }
            set
            {
                if (value == _ChannelDescription) return;
                _ChannelDescription = value;
                this.OnPropertyChanged("ChannelDescription");
            }
        }

        public bool IsChannelUpdateRunning
        {
            get { return _IsChannelUpdateRunning; }
            set
            {
                if (value == _IsChannelUpdateRunning) return;
                _IsChannelUpdateRunning = value;
                this.OnPropertyChanged("IsChannelUpdateRunning");
            }
        }

        public int ProfileImageUploadState
        {
            get { return _ProfileImageUploadState; }
            set
            {
                if (value == _ProfileImageUploadState) return;
                _ProfileImageUploadState = value;
                this.OnPropertyChanged("ProfileImageUploadState");
            }
        }

        public int CoverImageUploadState
        {
            get { return _CoverImageUploadState; }
            set
            {
                if (value == _CoverImageUploadState) return;
                _CoverImageUploadState = value;
                this.OnPropertyChanged("CoverImageUploadState");
            }
        }

        public ObservableCollection<CountryCodeModel> CountryModelList
        {
            get { return _CountryModelList; }
            set
            {
                _CountryModelList = value;
                this.OnPropertyChanged("CountryModelList");
            }
        }

        public ChannelModel ChannelModel
        {
            get { return _ChannelModel; }
            set
            {
                _ChannelModel = value;
                this.OnPropertyChanged("ChannelModel");
            }
        }

        #endregion Property

        #region Command

        public ICommand ProfileImageChangeCommand
        {
            get
            {
                if (_ProfileImageChangeCommand == null)
                {
                    _ProfileImageChangeCommand = new RelayCommand((param) => OnProfileImageChangeCommand(param));
                }
                return _ProfileImageChangeCommand;
            }
        }

        public ICommand CoverImageChangeCommand
        {
            get
            {
                if (_CoverImageChangeCommand == null)
                {
                    _CoverImageChangeCommand = new RelayCommand((param) => OnCoverImageChange(param));
                }
                return _CoverImageChangeCommand;
            }
        }

        public ICommand UpdateCommand
        {
            get
            {
                if (_UpdateCommand == null)
                {
                    _UpdateCommand = new RelayCommand(param => OnUpdateCommand());
                }
                return _UpdateCommand;
            }
        }

        private ICommand _ActivateDeactivateCommand;
        public ICommand ActivateDeactivateCommand
        {
            get
            {
                if (_ActivateDeactivateCommand == null)
                {
                    _ActivateDeactivateCommand = new RelayCommand(param => OnActivateDeactivateCommand(param));
                }
                return _ActivateDeactivateCommand;
            }
        }

        #endregion

    }
}

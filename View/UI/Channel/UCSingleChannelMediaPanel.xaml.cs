﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;
using View.Utility;
using View.Utility.Channel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCSingleChannelMediaPanel.xaml
    /// </summary>
    public partial class UCSingleChannelMediaPanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleChannelMediaPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private bool disposed = false;
        private ICommand _SelectCommand;

        public UCSingleChannelMediaPanel()
        {
            InitializeComponent();
        }

        ~UCSingleChannelMediaPanel()
        {
            Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsMyChannelProperty = DependencyProperty.Register("IsMyChannel", typeof(bool), typeof(UCSingleChannelMediaPanel), new PropertyMetadata(false));

        public bool IsMyChannel
        {
            get { return (bool)GetValue(IsMyChannelProperty); }
            set
            {
                SetValue(IsMyChannelProperty, value);
            }
        }

        public static readonly DependencyProperty IsPlayingProperty = DependencyProperty.Register("IsPlaying", typeof(bool), typeof(UCSingleChannelMediaPanel), new PropertyMetadata(false));

        public bool IsPlaying
        {
            get { return (bool)GetValue(IsPlayingProperty); }
            set
            {
                SetValue(IsPlayingProperty, value);
            }
        }

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleChannelMediaPanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        public ICommand SelectCommand
        {
            get
            {
                if (_SelectCommand == null)
                {
                    _SelectCommand = new RelayCommand((param) => OnSelectCommand(param));
                }
                return _SelectCommand;
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleChannelMediaPanel panel = ((UCSingleChannelMediaPanel)o);
                object data = panel.DataContext;

                if (e.NewValue != null && (bool)e.NewValue)
                {
                    panel.BindPreview((ChannelMediaModel)data);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindPreview(ChannelMediaModel model)
        {
            MultiBinding sourceBinding = new MultiBinding { Converter = new ChannelMediaImageConverter() };
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath(""),
            });
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ThumbImageUrl"),
            });
            imgMediaThumb.SetBinding(Image.SourceProperty, sourceBinding);
            lblTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Title") });
            lblArtist.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Artist"), StringFormat = "Artist: {0}" });
            lblDuation.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Duration"), Converter = new DurationTextConverter(), ConverterParameter = 1, StringFormat = "Duration: {0}" });

            if (this.IsPlaying)
            {
                MouseBinding mouseBinding = new MouseBinding();
                mouseBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
                mouseBinding.Command = SelectCommand;
                imgMediaThumb.InputBindings.Add(mouseBinding);
                imgMediaThumb.Cursor = Cursors.Hand;
                imgMediaThumb.ToolTip = "Click To View";
                icon.InputBindings.Add(mouseBinding);
                icon.Cursor = Cursors.Hand;
                icon.ToolTip = "Click To View";
            }
        }

        private void ClearPreview()
        {
            imgMediaThumb.ClearValue(Image.SourceProperty);
            imgMediaThumb.Source = null;
            imgMediaThumb.InputBindings.Clear();
            icon.InputBindings.Clear();
            lblTitle.ClearValue(TextBlock.TextProperty);
            lblArtist.ClearValue(TextBlock.TextProperty);
            lblDuation.ClearValue(TextBlock.TextProperty);
        }

        private void OnSelectCommand(object param)
        {
            try
            {
                UCChannelDetailsPanel parent = HelperMethods.FindVisualParent<UCChannelDetailsPanel>(this);
                if (parent != null && parent.ChannelInfoModel != null)
                {
                    ChannelViewModel.Instance.OnChannelViewCommand(parent.ChannelInfoModel);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSelectCommand => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    if (disposing)
                    {
                        this.ClearPreview();
                        this.imgMediaThumb = null;
                        this.icon = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

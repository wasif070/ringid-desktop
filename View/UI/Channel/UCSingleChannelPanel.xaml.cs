﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;
using View.Utility;
using View.Utility.Channel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCSingleChannelPanel.xaml
    /// </summary>
    public partial class UCSingleChannelPanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleChannelPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private bool disposed = false;

        public UCSingleChannelPanel()
        {
            InitializeComponent();
        }

        ~UCSingleChannelPanel()
        {
           Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsThumbViewProperty = DependencyProperty.Register("IsThumbView", typeof(bool), typeof(UCSingleChannelPanel), new PropertyMetadata(false));

        public bool IsThumbView
        {
            get { return (bool)GetValue(IsThumbViewProperty); }
            set
            {
                SetValue(IsThumbViewProperty, value);
            }
        }

        public static readonly DependencyProperty IsMyChannelProperty = DependencyProperty.Register("IsMyChannel", typeof(bool), typeof(UCSingleChannelPanel), new PropertyMetadata(false));

        public bool IsMyChannel
        {
            get { return (bool)GetValue(IsMyChannelProperty); }
            set
            {
                SetValue(IsMyChannelProperty, value);
            }
        }

        public static readonly DependencyProperty IsFollowingProperty = DependencyProperty.Register("IsFollowing", typeof(bool), typeof(UCSingleChannelPanel), new PropertyMetadata(false));

        public bool IsFollowing
        {
            get { return (bool)GetValue(IsFollowingProperty); }
            set
            {
                SetValue(IsFollowingProperty, value);
            }
        }

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleChannelPanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleChannelPanel panel = ((UCSingleChannelPanel)o);
                object data = panel.DataContext;

                if (e.NewValue != null && (bool)e.NewValue)
                {
                    panel.BindPreview((ChannelModel)data);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindPreview(ChannelModel model)
        {
            MultiBinding sourceBinding = new MultiBinding { Converter = new ChannelProfileImageConverter(), ConverterParameter = this.IsThumbView ? ImageUtility.IMG_CROP : ImageUtility.IMG_600 };
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath(""),
            });
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ProfileImage"),
            });
            imgControl.SetBinding(Image.SourceProperty, sourceBinding);

            if (this.IsThumbView)
            {
                lblTitleThumb.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Title") });
            }
            else
            {
                lblSubscriber.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("SubscriberCount"), StringFormat = "0,0 Followers" });
                lblTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Title") });
            }

            if (this.IsMyChannel)
            {
                MouseBinding mouseBinding = new MouseBinding();
                mouseBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
                mouseBinding.Command = ChannelViewModel.Instance.MyChannelMainCommand;
                mouseBinding.CommandParameter = model;
                imgControl.InputBindings.Add(mouseBinding);
            }
            else
            {
                MouseBinding mouseBinding = new MouseBinding();
                mouseBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
                mouseBinding.Command = ChannelViewModel.Instance.ChannelViewCommand;
                mouseBinding.CommandParameter = model;
                imgControl.InputBindings.Add(mouseBinding);
            }

            dynamic param = new ExpandoObject();
            param.IsFromMyChannel = this.IsMyChannel || this.IsFollowing;
            param.ChannelInfoModel = model;

            MouseBinding infoBinding = new MouseBinding();
            infoBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
            infoBinding.Command = ChannelViewModel.Instance.ChannelDetailsCommand;
            infoBinding.CommandParameter = param;
            btnChannelInfo.InputBindings.Add(infoBinding);
            btnThumbChannelInfo.InputBindings.Add(infoBinding);
        }

        private void ClearPreview()
        {
            imgControl.ClearValue(Image.SourceProperty);
            imgControl.Source = null;
            imgControl.InputBindings.Clear();
            btnChannelInfo.InputBindings.Clear();
            btnThumbChannelInfo.InputBindings.Clear();
            lblSubscriber.ClearValue(TextBlock.TextProperty);
            lblTitle.ClearValue(TextBlock.TextProperty);
            lblTitleThumb.ClearValue(TextBlock.TextProperty);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    if (disposing)
                    {
                        this.ClearPreview();
                        this.imgControl = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

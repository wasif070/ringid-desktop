﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Channel;
using View.Utility.Chat.Service;
using View.Utility.GIF;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Channel
{
    /// <summary>
    /// Interaction logic for UCMyChannelPlaylistPanel.xaml
    /// </summary>
    public partial class UCMyChannelPlaylistPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMyChannelPlaylistPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<ChannelMediaModel> _MediaPlayedList = new ObservableCollection<ChannelMediaModel>();

        private ChannelModel _SingleChannelInfoModel = new ChannelModel();
        private ChannelModel _PrevChannelInfoModel { get; set; }
        private ICommand _MediaPlayCommand;
        private bool _IsPublishListLoaded;
        private bool _IS_PUBLISHED_LOADING { get; set; }


        #region Constructor

        public UCMyChannelPlaylistPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int count = this.itcntrlMedia.Items.Count;
            int bottomLimit = count > 5 ? 400 : (count > 3 ? 200 : 140);

            if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_PUBLISHED_LOADING == false)
            {
                this.GetPublishedList();
            }
        }

        public void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            this.itcntrlMedia.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("MediaPlayedList") });
            if (_SingleChannelInfoModel != null && !(_PrevChannelInfoModel != null && _PrevChannelInfoModel.ChannelID == _SingleChannelInfoModel.ChannelID))
            {
                _PrevChannelInfoModel = _SingleChannelInfoModel;
                BitmapImage _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                imgControl.Source = _LoadingIcon;
                ImageBehavior.SetAnimatedSource(imgControl, _LoadingIcon);
            }
        }

        public void ReleaseViewer()
        {
            //this.Destroy();
            this.itcntrlMedia.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcntrlMedia.ItemsSource = null;
        }

        public void Destroy()
        {
            MediaPlayedList.Clear();
            if (_SingleChannelInfoModel != null)
            {
                ChannelViewModel.Instance.RemoveChannelPlayList(_SingleChannelInfoModel.ChannelID);
            }
        }

        public void GetPublishedList(bool isInitRequest = false)
        {
            IsPublishListLoaded = false;
            _IS_PUBLISHED_LOADING = true;

            if (_SingleChannelInfoModel != null)
            {
                ChannelHelpers.LoadChannelPlayList(_SingleChannelInfoModel.ChannelID, isInitRequest, 15, (status) =>
                {
                    IsPublishListLoaded = true;
                    _IS_PUBLISHED_LOADING = false;
                    Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        imgControl.Source = null;
                        ImageBehavior.SetAnimatedSource(imgControl, null);
                    });
                    return 0;
                });
            }
        }

        public void Dispose()
        {

        }

        private void OnMediaPlayCommand(object param)
        {
            ChannelMediaModel item = (ChannelMediaModel)param;
            int index = MediaPlayedList.IndexOf(item);
            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
            foreach (ChannelMediaModel mediaModel in MediaPlayedList)
            {
                SingleMediaModel singleMediaModel = new SingleMediaModel();
                singleMediaModel.StreamUrl = mediaModel.MediaUrl;
                singleMediaModel.ContentId = mediaModel.MediaID;
                singleMediaModel.MediaOwner = new BaseUserProfileModel();
                singleMediaModel.MediaOwner.UserTableID = mediaModel.OwnerID;
                singleMediaModel.Title = mediaModel.Title;
                singleMediaModel.Duration = mediaModel.Duration / 1000;
                singleMediaModel.Artist = mediaModel.Artist;
                singleMediaModel.MediaType = 2;
                singleMediaModel.ThumbUrl = mediaModel.ThumbImageUrl;
                singleMediaModel.IsFromLocalDirectory = false;
                singleMediaModel.ChannelId = mediaModel.ChannelID;
                MediaList.Add(singleMediaModel);
            }
            MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, index);
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region EventHandler

        private void menuItem_Remove_Click(object sender, RoutedEventArgs e)
        {
            if (((MenuItem)sender).DataContext is ChannelMediaModel)
            {
                ChannelMediaModel mediaModel = (ChannelMediaModel)((MenuItem)sender).DataContext;
                List<Guid> channelMediaIds = new List<Guid>();
                channelMediaIds.Add(mediaModel.MediaID);
                new ThrdUpdateMediaStatus(mediaModel.ChannelID, channelMediaIds, ChannelConstants.MEDIA_STATUS_PENDING, null, (status) =>
                {
                    if (status)
                    {
                        mediaModel.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                        mediaModel.StartTime = 0;
                        MediaPlayedList.InvokeRemove(mediaModel);
                        if (UCMyChannelMainPanel.MyChannelPendingPanel != null)
                        {
                            if (UCMyChannelMainPanel.MyChannelPendingPanel.PendingMediaList.Where(P => P.MediaID == mediaModel.MediaID).FirstOrDefault() == null)
                            {
                                UCMyChannelMainPanel.MyChannelPendingPanel.PendingMediaList.InvokeAdd(mediaModel);
                            }
                        }
                    }
                    return 0;
                }).Start();
            }
        }

        #endregion
        
        #region Property

        public ObservableCollection<ChannelMediaModel> MediaPlayedList
        {
            get { return _MediaPlayedList; }
            set
            {
                _MediaPlayedList = value;
                this.OnPropertyChanged("MediaPlayedList");
            }
        }


        public ChannelModel SingleChannelInfoModel
        {
            get { return _SingleChannelInfoModel; }
            set
            {
                _SingleChannelInfoModel = value;
                this.OnPropertyChanged("SingleChannelInfoModel");
            }
        }

        public bool IsPublishListLoaded
        {
            get { return _IsPublishListLoaded; }
            set
            {
                if (value == _IsPublishListLoaded) return;
                _IsPublishListLoaded = value;
                this.OnPropertyChanged("IsPublishListLoaded");
            }
        }

        #endregion Property

        #region Command

        public ICommand MediaPlayCommand
        {
            get
            {
                if (_MediaPlayCommand == null)
                {
                    _MediaPlayCommand = new RelayCommand(param => OnMediaPlayCommand(param));
                }
                return _MediaPlayCommand;
            }
        }

        #endregion

    }
}

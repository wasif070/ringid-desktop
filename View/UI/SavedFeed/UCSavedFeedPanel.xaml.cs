<<<<<<< HEAD
﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.Constants;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.SavedFeed
{
    /// <summary>
    /// Interaction logic for UCSavedFeedPanel.xaml
    /// </summary>
    public partial class UCSavedFeedPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSavedFeedPanel).Name);

        public UCSavedFeedPanel()
        {
            //InitializeComponent();
            //this.DataContext = this;
            //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeSavedFeed, TabType);
            //SwitchToTab(TabType);
            //this.Loaded += UCSavedFeedPanel_Loaded;
            //this.Unloaded += UCSavedFeedPanel_Unloaded;
            ///
            this.DataContext = this;
            InitializeComponent();
            ViewCollection = RingIDViewModel.Instance.SavedAllCustomFeeds;
            ScrlViewer.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.SavedAllCurrentIds, FeedDataContainer.Instance.SavedAllTopIds, FeedDataContainer.Instance.SavedAllBottomIds, 100, AppConstants.TYPE_ALL_SAVED_FEEDS);
            //this.PreviewMouseWheel += (s, e) => { ScrlViewer.OnPreviewMouseWheelScrolled(e.Delta); e.Handled = true; };//this.PreviewMouseWheel += delegate(object sender, MouseWheelEventArgs e) { };
            this.IsVisibleChanged += (s, e) =>
            {
                bool visible = (bool)e.NewValue;
                RingIDViewModel.Instance.SavedFeedSelection = visible;
                ScrlViewer.OnIsVisibleChanged(visible);
            };
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeSavedFeed, TabType);
            //SwitchToTab(TabType);
            DefaultSettings.SAVEDALL_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!ScrlViewer.RequestOn)
                ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDALL_STARTPKT);
        }

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private CustomFeedCollection _ViewCollection;
        public CustomFeedCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        private string _TitleName = "Saved Feed";
        public string TitleName
        {
            get { return _TitleName; }
            set
            {
                if (value == _TitleName)
                    return;
                _TitleName = value;
                this.OnPropertyChanged("TitleName");
            }
        }

        private bool _IsTabView = false;
        public bool IsTabView
        {
            get { return _IsTabView; }
            set
            {
                if (value == _IsTabView)
                    return;
                _IsTabView = value;
                this.OnPropertyChanged("IsTabView");
            }
        }
        private int _TabType = 0;
        public int TabType
        {
            get { return _TabType; }
            set
            {
                _TabType = value;
                OnPropertyChanged("TabType");
            }
        }
        //public int SelectedTbIndx = 0;
        #endregion

        #region "Event Triggers"
        //private void UCSavedFeedPanel_Loaded(object sender, RoutedEventArgs args)
        //{
        //    ScrlViewer.ScrollToHome();
        //    RingIDViewModel.Instance.SavedFeedSelection = true;
        //    //SwitchToTab(TabType);
        //}
        //private void UCSavedFeedPanel_Unloaded(object sender, RoutedEventArgs args)
        //{
        //    //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
        //    //{
        //    //    RingIDViewModel.Instance.SavedFeedSelection = false;
        //    //    ScrlViewer.SetScrollEvents(false);
        //    //    ViewCollection = null;
        //    //}
        //}
        #endregion

        private void ScrlViewer_PreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        //private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    ChangeTab(SavedControl.SelectedIndex);
        //}

        public void SwitchToTab(int idx)
        {
            if (TabType != idx)
                TabType = idx;
            // SelectedTbIndx = idx;
            ViewCollection = null;
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += (s, ex) =>
            {
                switch (idx)
                {
                    case 0:
                        ViewCollection = RingIDViewModel.Instance.SavedAllCustomFeeds;
                        ScrlViewer.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.SavedAllCurrentIds, FeedDataContainer.Instance.SavedAllTopIds, FeedDataContainer.Instance.SavedAllBottomIds, 100, AppConstants.TYPE_ALL_SAVED_FEEDS);
                        if (DefaultSettings.SAVEDALL_STARTPKT == null)
                        {
                            DefaultSettings.SAVEDALL_STARTPKT = SendToServer.GetRanDomPacketID();
                            if (!ScrlViewer.RequestOn)
                                ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDALL_STARTPKT);
                        }
                        break;
                    case 1:
                        ViewCollection = RingIDViewModel.Instance.SavedMediaCustomFeeds;
                        ScrlViewer.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.SavedMediaCurrentIds, FeedDataContainer.Instance.SavedMediaTopIds, FeedDataContainer.Instance.SavedMediaBottomIds, SettingsConstants.PROFILE_TYPE_MUSICPAGE, AppConstants.TYPE_ALL_SAVED_FEEDS);
                        if (DefaultSettings.SAVEDMEDIA_STARTPKT == null)
                        {
                            DefaultSettings.SAVEDMEDIA_STARTPKT = SendToServer.GetRanDomPacketID();
                            if (!ScrlViewer.RequestOn)
                                ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDMEDIA_STARTPKT);
                        }
                        break;
                    //case 2:
                    //    ViewCollection = RingIDViewModel.Instance.SavedFeedsPage;
                    //    ScrlViewer.SetScrollValues(ViewCollection, FeedDataContainer.Instance.SavedFeedsPageSortedIds, SettingsConstants.PROFILE_TYPE_PAGES, AppConstants.TYPE_ALL_SAVED_FEEDS);
                    //    ScrlViewer.SetScrollEvents(true);
                    //    if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    //        Keyboard.Focus(ScrlViewer);
                    //    if (DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID == null)
                    //    {
                    //        DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID = SendToServer.GetRanDomPacketID();
                    //        if (!ScrlViewer.ViewCollection.LoadMoreModel.ShowContinue)
                    //            ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID);
                    //    }
                    //    break;
                    case 2:
                        ViewCollection = RingIDViewModel.Instance.SavedNewsPortalCustomFeeds;
                        ScrlViewer.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.SavedNewsPortalCurrentIds, FeedDataContainer.Instance.SavedNewsPortalTopIds, FeedDataContainer.Instance.SavedNewsPortalBottomIds, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, AppConstants.TYPE_ALL_SAVED_FEEDS);
                        if (DefaultSettings.SAVEDNEWSPORTAL_STARTPKT == null)
                        {
                            DefaultSettings.SAVEDNEWSPORTAL_STARTPKT = SendToServer.GetRanDomPacketID();
                            if (!ScrlViewer.RequestOn)
                                ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDNEWSPORTAL_STARTPKT);
                        }
                        break;
                    //case 4:
                    //    ViewCollection = RingIDViewModel.Instance.SavedFeedsCelebrity;
                    //    ScrlViewer.SetScrollValues(ViewCollection, FeedDataContainer.Instance.SavedFeedsCelebritySortedIds, SettingsConstants.PROFILE_TYPE_CELEBRITY, AppConstants.TYPE_ALL_SAVED_FEEDS);
                    //    ScrlViewer.SetScrollEvents(true);
                    //    if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    //        Keyboard.Focus(ScrlViewer);
                    //    if (DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID == null)
                    //    {
                    //        DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID = SendToServer.GetRanDomPacketID();
                    //        if (!ScrlViewer.ViewCollection.LoadMoreModel.ShowContinue)
                    //            ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID);
                    //    }
                    //    break;
                    case 3:

                    default:
                        break;
                }
                timer.Stop();
            };
            timer.Stop();
            timer.Start();
        }
        DispatcherTimer timer = new DispatcherTimer();

        #region "ICommand"
        private ICommand _BtnTabCommand;
        public ICommand BtnTabCommand
        {
            get
            {
                if (_BtnTabCommand == null)
                {
                    _BtnTabCommand = new RelayCommand(param => OnBtnTabCommandClicked(param));
                }
                return _BtnTabCommand;
            }
        }

        public void OnBtnTabCommandClicked(object parameter)
        {
            if (parameter is string)
            {
                int TabIdx = Convert.ToInt32(parameter);
                MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeSavedFeed, TabIdx);
                SwitchToTab(TabIdx);
            }
        }
        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommandClicked(param));
                }
                return _BackCommand;
            }
        }

        public void OnBackCommandClicked(object parameter)
        {
            //tabContainerBorder.Visibility = Visibility.Visible;
            //searchContentsBdr.Visibility = Visibility.Collapsed;
            //IsTabView = true;
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            switch (TabType)
            {
                case 0:
                    DefaultSettings.SAVEDALL_STARTPKT = SendToServer.GetRanDomPacketID();
                    if (!ScrlViewer.RequestOn)
                        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDALL_STARTPKT);
                    break;
                case 1:
                    DefaultSettings.SAVEDMEDIA_STARTPKT = SendToServer.GetRanDomPacketID();
                    if (!ScrlViewer.RequestOn)
                        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDMEDIA_STARTPKT);
                    break;
                //case 2:
                //    DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID = SendToServer.GetRanDomPacketID();
                //    if (!ScrlViewer.ViewCollection.LoadMoreModel.ShowContinue)
                //        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID);
                //    break;
                case 2:
                    DefaultSettings.SAVEDNEWSPORTAL_STARTPKT = SendToServer.GetRanDomPacketID();
                    if (!ScrlViewer.RequestOn)
                        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDNEWSPORTAL_STARTPKT);
                    break;
                //case 4:
                //    DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID = SendToServer.GetRanDomPacketID();
                //    if (!ScrlViewer.ViewCollection.LoadMoreModel.ShowContinue)
                //        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID);
                //    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
=======
﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.Constants;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.SavedFeed
{
    /// <summary>
    /// Interaction logic for UCSavedFeedPanel.xaml
    /// </summary>
    public partial class UCSavedFeedPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSavedFeedPanel).Name);

        public UCSavedFeedPanel()
        {
            //InitializeComponent();
            //this.DataContext = this;
            //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeSavedFeed, TabType);
            //SwitchToTab(TabType);
            //this.Loaded += UCSavedFeedPanel_Loaded;
            //this.Unloaded += UCSavedFeedPanel_Unloaded;
            ///
            this.DataContext = this;
            InitializeComponent();
            ViewCollection = RingIDViewModel.Instance.SavedAllCustomFeeds;
            ScrlViewer.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.SavedAllCurrentIds, FeedDataContainer.Instance.SavedAllTopIds, FeedDataContainer.Instance.SavedAllBottomIds, 100, AppConstants.TYPE_ALL_SAVED_FEEDS);
            //this.PreviewMouseWheel += (s, e) => { ScrlViewer.OnPreviewMouseWheelScrolled(e.Delta); e.Handled = true; };//this.PreviewMouseWheel += delegate(object sender, MouseWheelEventArgs e) { };
            this.IsVisibleChanged += (s, e) =>
            {
                bool visible = (bool)e.NewValue;
                RingIDViewModel.Instance.SavedFeedSelection = visible;
                ScrlViewer.OnIsVisibleChanged(visible);
            };
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeSavedFeed, TabType);
            //SwitchToTab(TabType);
            DefaultSettings.SAVEDALL_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!ScrlViewer.RequestOn)
                ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDALL_STARTPKT);
        }

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private CustomFeedCollection _ViewCollection;
        public CustomFeedCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        private string _TitleName = "Saved Feed";
        public string TitleName
        {
            get { return _TitleName; }
            set
            {
                if (value == _TitleName)
                    return;
                _TitleName = value;
                this.OnPropertyChanged("TitleName");
            }
        }

        private bool _IsTabView = false;
        public bool IsTabView
        {
            get { return _IsTabView; }
            set
            {
                if (value == _IsTabView)
                    return;
                _IsTabView = value;
                this.OnPropertyChanged("IsTabView");
            }
        }
        private int _TabType = 0;
        public int TabType
        {
            get { return _TabType; }
            set
            {
                _TabType = value;
                OnPropertyChanged("TabType");
            }
        }
        //public int SelectedTbIndx = 0;
        #endregion

        #region "Event Triggers"
        //private void UCSavedFeedPanel_Loaded(object sender, RoutedEventArgs args)
        //{
        //    ScrlViewer.ScrollToHome();
        //    RingIDViewModel.Instance.SavedFeedSelection = true;
        //    //SwitchToTab(TabType);
        //}
        //private void UCSavedFeedPanel_Unloaded(object sender, RoutedEventArgs args)
        //{
        //    //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
        //    //{
        //    //    RingIDViewModel.Instance.SavedFeedSelection = false;
        //    //    ScrlViewer.SetScrollEvents(false);
        //    //    ViewCollection = null;
        //    //}
        //}
        #endregion

        private void ScrlViewer_PreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        //private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    ChangeTab(SavedControl.SelectedIndex);
        //}

        public void SwitchToTab(int idx)
        {
            if (TabType != idx)
                TabType = idx;
            // SelectedTbIndx = idx;
            ViewCollection = null;
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += (s, ex) =>
            {
                switch (idx)
                {
                    case 0:
                        ViewCollection = RingIDViewModel.Instance.SavedAllCustomFeeds;
                        ScrlViewer.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.SavedAllCurrentIds, FeedDataContainer.Instance.SavedAllTopIds, FeedDataContainer.Instance.SavedAllBottomIds, 100, AppConstants.TYPE_ALL_SAVED_FEEDS);
                        if (DefaultSettings.SAVEDALL_STARTPKT == null)
                        {
                            DefaultSettings.SAVEDALL_STARTPKT = SendToServer.GetRanDomPacketID();
                            ScrlViewer.RequestOn = false;
                            ViewCollection.DummyModel.FeedType = 12;
                            //if (!ScrlViewer.RequestOn)
                            ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDALL_STARTPKT);
                        }
                        break;
                    case 1:
                        ViewCollection = RingIDViewModel.Instance.SavedMediaCustomFeeds;
                        ScrlViewer.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.SavedMediaCurrentIds, FeedDataContainer.Instance.SavedMediaTopIds, FeedDataContainer.Instance.SavedMediaBottomIds, SettingsConstants.PROFILE_TYPE_MUSICPAGE, AppConstants.TYPE_ALL_SAVED_FEEDS);
                        if (DefaultSettings.SAVEDMEDIA_STARTPKT == null)
                        {
                            DefaultSettings.SAVEDMEDIA_STARTPKT = SendToServer.GetRanDomPacketID();
                            ScrlViewer.RequestOn = false;
                            ViewCollection.DummyModel.FeedType = 12;
                            //if (!ScrlViewer.RequestOn)
                            ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDMEDIA_STARTPKT);
                        }
                        break;
                    //case 2:
                    //    ViewCollection = RingIDViewModel.Instance.SavedFeedsPage;
                    //    ScrlViewer.SetScrollValues(ViewCollection, FeedDataContainer.Instance.SavedFeedsPageSortedIds, SettingsConstants.PROFILE_TYPE_PAGES, AppConstants.TYPE_ALL_SAVED_FEEDS);
                    //    ScrlViewer.SetScrollEvents(true);
                    //    if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    //        Keyboard.Focus(ScrlViewer);
                    //    if (DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID == null)
                    //    {
                    //        DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID = SendToServer.GetRanDomPacketID();
                    //        if (!ScrlViewer.ViewCollection.LoadMoreModel.ShowContinue)
                    //            ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID);
                    //    }
                    //    break;
                    case 2:
                        ViewCollection = RingIDViewModel.Instance.SavedNewsPortalCustomFeeds;
                        ScrlViewer.SetScrollValues(feedItemsControl, ViewCollection, FeedDataContainer.Instance.SavedNewsPortalCurrentIds, FeedDataContainer.Instance.SavedNewsPortalTopIds, FeedDataContainer.Instance.SavedNewsPortalBottomIds, SettingsConstants.PROFILE_TYPE_NEWSPORTAL, AppConstants.TYPE_ALL_SAVED_FEEDS);
                        if (DefaultSettings.SAVEDNEWSPORTAL_STARTPKT == null)
                        {
                            DefaultSettings.SAVEDNEWSPORTAL_STARTPKT = SendToServer.GetRanDomPacketID();
                            ViewCollection.DummyModel.FeedType = 12;
                            ScrlViewer.RequestOn = false;
                            //if (!ScrlViewer.RequestOn)
                            ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDNEWSPORTAL_STARTPKT);
                        }
                        break;
                    //case 4:
                    //    ViewCollection = RingIDViewModel.Instance.SavedFeedsCelebrity;
                    //    ScrlViewer.SetScrollValues(ViewCollection, FeedDataContainer.Instance.SavedFeedsCelebritySortedIds, SettingsConstants.PROFILE_TYPE_CELEBRITY, AppConstants.TYPE_ALL_SAVED_FEEDS);
                    //    ScrlViewer.SetScrollEvents(true);
                    //    if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    //        Keyboard.Focus(ScrlViewer);
                    //    if (DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID == null)
                    //    {
                    //        DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID = SendToServer.GetRanDomPacketID();
                    //        if (!ScrlViewer.ViewCollection.LoadMoreModel.ShowContinue)
                    //            ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID);
                    //    }
                    //    break;
                    case 3:

                    default:
                        break;
                }
                timer.Stop();
            };
            timer.Stop();
            timer.Start();
        }
        DispatcherTimer timer = new DispatcherTimer();

        #region "ICommand"
        private ICommand _BtnTabCommand;
        public ICommand BtnTabCommand
        {
            get
            {
                if (_BtnTabCommand == null)
                {
                    _BtnTabCommand = new RelayCommand(param => OnBtnTabCommandClicked(param));
                }
                return _BtnTabCommand;
            }
        }

        public void OnBtnTabCommandClicked(object parameter)
        {
            if (parameter is string)
            {
                int TabIdx = Convert.ToInt32(parameter);
                MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeSavedFeed, TabIdx);
                SwitchToTab(TabIdx);
            }
        }
        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommandClicked(param));
                }
                return _BackCommand;
            }
        }

        public void OnBackCommandClicked(object parameter)
        {
            //tabContainerBorder.Visibility = Visibility.Visible;
            //searchContentsBdr.Visibility = Visibility.Collapsed;
            //IsTabView = true;
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            switch (TabType)
            {
                case 0:
                    DefaultSettings.SAVEDALL_STARTPKT = SendToServer.GetRanDomPacketID();
                    if (!ScrlViewer.RequestOn)
                        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDALL_STARTPKT);
                    break;
                case 1:
                    DefaultSettings.SAVEDMEDIA_STARTPKT = SendToServer.GetRanDomPacketID();
                    if (!ScrlViewer.RequestOn)
                        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDMEDIA_STARTPKT);
                    break;
                //case 2:
                //    DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID = SendToServer.GetRanDomPacketID();
                //    if (!ScrlViewer.ViewCollection.LoadMoreModel.ShowContinue)
                //        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID);
                //    break;
                case 2:
                    DefaultSettings.SAVEDNEWSPORTAL_STARTPKT = SendToServer.GetRanDomPacketID();
                    if (!ScrlViewer.RequestOn)
                        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDNEWSPORTAL_STARTPKT);
                    break;
                //case 4:
                //    DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID = SendToServer.GetRanDomPacketID();
                //    if (!ScrlViewer.ViewCollection.LoadMoreModel.ShowContinue)
                //        ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID);
                //    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

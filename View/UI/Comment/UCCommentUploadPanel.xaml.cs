<<<<<<< HEAD
﻿using log4net;
using MediaInfoDotNet;
using Microsoft.Win32;
using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.UI.Feed;
using View.Utility;
using View.Utility.Recorder;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCCommentUploadPanel.xaml
    /// </summary>
    public partial class UCCommentUploadPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCommentUploadPanel).Name);

        public Func<int> _OnBackToPrevious = null;
        public Grid motherPanel;
        //private UCCommentRecorder _CommentRecorderPanel = null;
        private UCCommentAudioRecorder _CommentAudioRecorderPanel = null;
        Func<int, int> _OnClosing = null;

        #region "Constructor"
        public UCCommentUploadPanel(Func<int, int> onClosing)
        {
            _OnClosing = onClosing;
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Property"

        private CommonUploaderModel _CommonUploaderModel;
        public CommonUploaderModel commonUploaderModel
        {
            get
            {
                return _CommonUploaderModel;
            }
            set
            {
                _CommonUploaderModel = value;
                this.OnPropertyChanged("commonUploaderModel");
            }
        }
        private bool _RemoveButtonEnabled = true;
        public bool RemoveButtonEnabled
        {
            get
            {
                return _RemoveButtonEnabled;
            }
            set
            {
                _RemoveButtonEnabled = value;
                this.OnPropertyChanged("RemoveButtonEnabled");
            }
        }

        private bool _IsEditMode;
        public bool IsEditMode
        {
            get
            {
                return _IsEditMode;
            }
            set
            {
                _IsEditMode = value;
                this.OnPropertyChanged("IsEditMode");
            }
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Methods"
        public void SetAllModelsToNull()
        {
            commonUploaderModel = null;
            //TODO
        }

        public void imageSelectToUpload()
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            op.Multiselect = false;
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if ((bool)op.ShowDialog())
            {
                foreach (string filename in op.FileNames)
                {
                    try
                    {
                        MediaFile mf = new MediaFile(filename);
                        if (mf.size > DefaultSettings.MaxFileLimit250MBinBytes) { anyMorethan500MB = true; continue; }
                        if (!mf.format.Equals("JPEG") && !mf.format.Equals("PNG")) { anyCorrupted = true; continue; }
                        //if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit500MBinBytes) { anyMorethan500MB = true; continue; }
                        //int formatChecked = ImageUtility.GetFileImageTypeFromHeader(filename);
                        //if (formatChecked != 1 && formatChecked != 2) { anyCorrupted = true; continue; }
                        commonUploaderModel = new CommonUploaderModel();
                        commonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_IMAGE;
                        commonUploaderModel.FileSize = mf.size;
                        commonUploaderModel.FilePath = filename;
                    }
                    catch (Exception) { anyCorrupted = true; }
                }
            }
            if (anyCorrupted) UIHelperMethods.ShowWarning("One or more files are corrupted !");
            if (anyMorethan500MB) UIHelperMethods.ShowWarning("One or more files are greater than Maximum file Limit (250MB) !");
        }

        public void audioSelectToUpload()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Music File";
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "All supported Music files|*.mp3|MP3 files(*.mp3)|*.mp3";
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length == 1)
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);
                            if (mf.size > DefaultSettings.MaxFileLimit500MBinBytes) { anyMorethan500MB = true; continue; }
                            if (mf.Audio == null || mf.Audio.Count == 0 || mf.Video.Count > 0 || !mf.format.Equals("MPEG Audio")) { anyCorrupted = true; continue; }

                            commonUploaderModel = new CommonUploaderModel();
                            commonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_MUSIC;
                            commonUploaderModel.FileSize = mf.size;
                            commonUploaderModel.FilePath = filename;

                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                                if (file.Tag != null && file.Tag.Pictures.Any())
                                {
                                    if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null)
                                    {
                                        commonUploaderModel.IsThumbImageFound = true;
                                    }
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                    {
                                        commonUploaderModel.FileArtist = file.Tag.Performers[0];
                                    }
                                    else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                    {
                                        commonUploaderModel.FileArtist = file.Tag.AlbumArtists[0];
                                    }
                                    commonUploaderModel.FileTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception) { }

                            if (string.IsNullOrEmpty(commonUploaderModel.FileTitle))
                                commonUploaderModel.FileTitle = Path.GetFileNameWithoutExtension(filename);
                            commonUploaderModel.FileDuration = (long)(mf.duration / 1000);
                            if (commonUploaderModel.FileDuration == 0)
                                commonUploaderModel.FileDuration = HelperMethods.MediaPlayerDuration(filename);
                        }
                        catch (Exception) { anyCorrupted = true; }
                    }
                }
                else UIHelperMethods.ShowFailed("Can't upload more than 1 music at a time !");
            }
            if (anyCorrupted) UIHelperMethods.ShowFailed("One or more files are corrupted or invalid format!");
            if (anyMorethan500MB) UIHelperMethods.ShowWarning("One or more files are greater than Maximum file Limit (500MB) !");
        }

        public void videoSelectToUpload()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Video File";
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "All supported Video files|*.mp4|MP4 files(*.mp4)|*.mp4";
            bool anyCorrupted = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length == 1) // When 5 video select with cntrl--> openFileDialog.FileNames.Length <= 5
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);
                            if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264")) { anyCorrupted = true; continue; }
                            commonUploaderModel = new CommonUploaderModel();
                            commonUploaderModel.FileSize = mf.size;
                            commonUploaderModel.FilePath = filename;
                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the video file location
                                if (file.Tag != null)
                                {
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0])) commonUploaderModel.FileArtist = file.Tag.Performers[0];
                                    commonUploaderModel.FileTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception) { }

                            if (string.IsNullOrEmpty(commonUploaderModel.FileTitle)) commonUploaderModel.FileTitle = Path.GetFileNameWithoutExtension(filename);
                            commonUploaderModel.FileDuration = (long)(mf.duration / 1000);
                            if (commonUploaderModel.FileDuration == 0) commonUploaderModel.FileDuration = HelperMethods.MediaPlayerDuration(filename);
                            HelperMethods.SetVideoArtImage(commonUploaderModel);// new SetArtImageFromVideo(CommonUploaderModel);//120*90
                            commonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_VIDEO;
                        }
                        catch (Exception) { anyCorrupted = true; }
                    }
                }
                else UIHelperMethods.ShowWarning("Can't upload more than 1 videos at a time !");
            }
            if (anyCorrupted) UIHelperMethods.ShowWarning("One or more files are corrupted or Invalid format!");
        }

        private void ShowRecorderView()
        {
            if (UCVideoRecorderPreviewPanel.Instance != null)
                motherPanel.Children.Remove(UCVideoRecorderPreviewPanel.Instance);
            UCVideoRecorderPreviewPanel.Instance = new UCVideoRecorderPreviewPanel(motherPanel);
            UCVideoRecorderPreviewPanel.Instance.Show();
            UCVideoRecorderPreviewPanel.Instance.ShowRecorder();
            UCVideoRecorderPreviewPanel.Instance.OnRemovedUserControl += () =>
            {
                UCVideoRecorderPreviewPanel.Instance = null;
            };
        }

        public void webCamToUpload()
        {
            try
            {
                ShowRecorderView();
                UCVideoRecorderPreviewPanel.Instance.OnVideoAddedToUploadList += (f, t) =>
                {
                    if (!string.IsNullOrEmpty(f))
                    {
                        MediaFile mf = new MediaFile(f);

                        //if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264"))
                        //{

                        //}
                        if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264")) {}
                        commonUploaderModel = new CommonUploaderModel();
                        //File.Move(f, Path.ChangeExtension(".avi", ".mp4"));
                        commonUploaderModel.FilePath = f;
                        FileInfo fi = new FileInfo(Path.ChangeExtension(commonUploaderModel.FilePath, ".mp4"));
                        //commonUploaderModel.FilePath = fi.FullName;

                        commonUploaderModel.FileSize = fi.Exists ? (long)(fi.Length / 1024) : 0;
                        commonUploaderModel.FileDuration = t;
                        try
                        {
                            //var uri = new System.Uri(f);
                            //var converted = uri.AbsoluteUri;
                            string newName = f.Replace("/", "\\");
                            //String RelativePath = AbsolutePath.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);
                            commonUploaderModel.FileSize = mf.size;
                        }
                        catch (Exception) { }

                        if (string.IsNullOrEmpty(commonUploaderModel.FileTitle)) commonUploaderModel.FileTitle = Path.GetFileNameWithoutExtension(f);
                        commonUploaderModel.FileDuration = (long)(mf.duration / 1000);
                        if (commonUploaderModel.FileDuration == 0) commonUploaderModel.FileDuration = HelperMethods.MediaPlayerDuration(f);
                        HelperMethods.SetVideoArtImage(commonUploaderModel);// new SetArtImageFromVideo(CommonUploaderModel);//120*90
                        commonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_VIDEO;
                    }
                    HideVideoRecord();
                };
                IsVideoRecordMode = true;
                //string a = _CommentRecorderPanel.FileName;
            }

            catch (Exception ex)
            {

                log.Error("Error: webCamToUpload() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void recordToUpload()
        {
            try
            {
                _CommentAudioRecorderPanel = new UCCommentAudioRecorder();
                UCGuiRingID.Instance.MotherPanel.Children.Add(_CommentAudioRecorderPanel);
                _CommentAudioRecorderPanel.RecordingCompletedHandler += (f, t) =>
                {
                    MediaFile mf = new MediaFile(f);

                    if (mf.size > DefaultSettings.MaxFileLimit500MBinBytes) { }
                    if (mf.Audio == null || mf.Audio.Count == 0 || mf.Video.Count > 0 || !mf.format.Equals("MPEG Audio")) { }

                    commonUploaderModel = new CommonUploaderModel();
                    commonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_MUSIC;
                    //commonUploaderModel.FileSize = mf.size;
                    commonUploaderModel.FilePath = f;
                    FileInfo fi = new FileInfo(Path.ChangeExtension(commonUploaderModel.FilePath, AudioCapture.AUDIO_FORMAT));
                    commonUploaderModel.FileSize = fi.Exists ? (long)(fi.Length / 1024) : 0;
                    try
                    {
                        TagLib.File file = TagLib.File.Create(f); //FilePath is the audio file location
                        if (file.Tag != null && file.Tag.Pictures.Any())
                        {
                            if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null) commonUploaderModel.IsThumbImageFound = true;
                            if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0])) commonUploaderModel.FileArtist = file.Tag.Performers[0];
                            else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0])) commonUploaderModel.FileArtist = file.Tag.AlbumArtists[0];
                            commonUploaderModel.FileTitle = file.Tag.Title;
                        }
                    }
                    catch (Exception) { }

                    if (string.IsNullOrEmpty(commonUploaderModel.FileTitle)) commonUploaderModel.FileTitle = Path.GetFileNameWithoutExtension(f);
                    commonUploaderModel.FileDuration = (long)(mf.duration / 1000);
                    if (commonUploaderModel.FileDuration == 0) commonUploaderModel.FileDuration = HelperMethods.MediaPlayerDuration(f);
                    HideAudioRecord();
                };
                IsAudioRecordMode = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: recordToUpload() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideAudioRecord()
        {
            try
            {
                IsAudioRecordMode = false;
                if (_CommentAudioRecorderPanel != null)
                {
                    _CommentAudioRecorderPanel.Dispose();
                    _CommentAudioRecorderPanel = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: HideAudioRecord() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool _IsAudioRecordMode = false;
        public bool IsAudioRecordMode
        {
            get { return _IsAudioRecordMode; }
            set
            {
                if (_IsAudioRecordMode == value)
                    return;
                _IsAudioRecordMode = value;
                this.OnPropertyChanged("IsAudioRecordMode");
            }
        }

        private bool _IsVideoRecordMode = false;
        public bool IsVideoRecordMode
        {
            get { return _IsVideoRecordMode; }
            set
            {
                if (_IsVideoRecordMode == value)
                    return;
                _IsVideoRecordMode = value;
                this.OnPropertyChanged("IsVideoRecordMode");
            }
        }

        public void HideVideoRecord()
        {
            try
            {
                IsVideoRecordMode = false;
                //if (_CommentRecorderPanel != null)
                //{
                //    _CommentRecorderPanel.Dispose();
                //    _CommentRecorderPanel = null;
                //}
            }
            catch (Exception ex)
            {
                log.Error("Error: HideVideoRecord() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void stickerSelectedToUpload(string selctedStickerPath)
        {
            commonUploaderModel = new CommonUploaderModel();
            commonUploaderModel.UploadedUrl = selctedStickerPath;
            commonUploaderModel.FilePath = @RingIDSettings.STICKER_FOLDER + System.IO.Path.DirectorySeparatorChar + (selctedStickerPath).Replace(System.IO.Path.AltDirectorySeparatorChar, System.IO.Path.DirectorySeparatorChar);
            commonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_STICKER;
        }
        #endregion

        #region "handlers"
        //private void CrossButton_Click(object sender, RoutedEventArgs e)
        //{
        //    if (RemoveButtonEnabled)
        //        SetAllModelsToNull();
        //    if (IsEditMode)
        //        IsEditMode = false;
        //}
        private ICommand _CrossButtonCommand;
        public ICommand CrossButtonCommand
        {
            get
            {
                if (_CrossButtonCommand == null)
                {
                    _CrossButtonCommand = new RelayCommand(param => OnCrossButtonClickCommand(param));
                }
                return _CrossButtonCommand;
            }
        }
        private void OnCrossButtonClickCommand(object parameter)
        {
            if (RemoveButtonEnabled)
                SetAllModelsToNull();
            if (IsEditMode)
                IsEditMode = false;
            if (_OnClosing != null)
            {
                _OnClosing(1);
                _OnClosing = null;
            }
        }

        //private void mediaPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    //ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
        //    SingleMediaModel singleMediamodel = new SingleMediaModel();

        //    singleMediamodel.UserTableID = DefaultSettings.LOGIN_TABLE_ID;
        //    singleMediamodel.ProfileImage = DefaultSettings.LOGIN_USER_PROFILE_IMAGE;
        //    singleMediamodel.Title = commonUploaderModel.FileTitle;
        //    singleMediamodel.Duration = commonUploaderModel.FileDuration;
        //    singleMediamodel.Artist = commonUploaderModel.FileArtist;
        //    singleMediamodel.ThumbUrl = commonUploaderModel.UploadedThumbUrl;

        //    if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_MUSIC)
        //    {
        //        singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_AUDIO;

        //        if (string.IsNullOrEmpty(commonUploaderModel.FilePath))
        //        {
        //            singleMediamodel.StreamUrl = commonUploaderModel.UploadedUrl;
        //            singleMediamodel.IsFromLocalDirectory = false;
        //            //PlayerHelperMethods.RunPlayListFromComments(false, commonUploaderModel.UploadedUrl, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 1);
        //        }
        //        else
        //        {
        //            singleMediamodel.StreamUrl = commonUploaderModel.FilePath;
        //            singleMediamodel.IsFromLocalDirectory = true;
        //            //PlayerHelperMethods.RunPlayListFromComments(true, commonUploaderModel.FilePath, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 1);
        //        }
        //    }
        //    else if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_VIDEO)
        //    {
        //        singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;

        //        if (string.IsNullOrEmpty(commonUploaderModel.FilePath))
        //        {
        //            singleMediamodel.StreamUrl = commonUploaderModel.UploadedUrl;
        //            singleMediamodel.IsFromLocalDirectory = false;
        //            //PlayerHelperMethods.RunPlayListFromComments(false, commonUploaderModel.UploadedUrl, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 2);
        //        }
        //        else
        //        {
        //            singleMediamodel.StreamUrl = commonUploaderModel.FilePath;
        //            singleMediamodel.IsFromLocalDirectory = false;
        //        }
        //        //PlayerHelperMethods.RunPlayListFromComments(true, commonUploaderModel.FilePath, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 2);
        //        //MediaList.Add(singleMediamodel);
        //        //MediaUtility.RunPlayList(singleMediamodel.IsFromLocalDirectory, MediaList);
        //        //PlayerHelperMethods.RunPlayListFromComments(singleMediamodel.IsFromLocalDirectory, MediaList);   
        //    }
        //    MediaUtility.PlaySingleMediaNoContentID(singleMediamodel, singleMediamodel.IsFromLocalDirectory);
        //}
        #endregion
    }
=======
﻿using log4net;
using MediaInfoDotNet;
using Microsoft.Win32;
using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.UI.Feed;
using View.Utility;
using View.Utility.Recorder;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCCommentUploadPanel.xaml
    /// </summary>
    public partial class UCCommentUploadPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCommentUploadPanel).Name);

        public Func<int> _OnBackToPrevious = null;
        public Grid motherPanel;
        //private UCCommentRecorder _CommentRecorderPanel = null;
        private UCCommentAudioRecorder _CommentAudioRecorderPanel = null;
        Func<int, int> _OnClosing = null;

        #region "Constructor"
        public UCCommentUploadPanel(Func<int, int> onClosing)
        {
            _OnClosing = onClosing;
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Property"

        private CommonUploaderModel _CommonUploaderModel;
        public CommonUploaderModel CommonUploaderModel
        {
            get
            {
                return _CommonUploaderModel;
            }
            set
            {
                _CommonUploaderModel = value;
                this.OnPropertyChanged("commonUploaderModel");
            }
        }
        private bool _RemoveButtonEnabled = true;
        public bool RemoveButtonEnabled
        {
            get
            {
                return _RemoveButtonEnabled;
            }
            set
            {
                _RemoveButtonEnabled = value;
                this.OnPropertyChanged("RemoveButtonEnabled");
            }
        }

        private bool _IsEditMode;
        public bool IsEditMode
        {
            get
            {
                return _IsEditMode;
            }
            set
            {
                _IsEditMode = value;
                this.OnPropertyChanged("IsEditMode");
            }
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Methods"
        public void SetAllModelsToNull()
        {
            CommonUploaderModel = null;
            //TODO
        }

        public void ImageSelectToUpload()
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            op.Multiselect = false;
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if ((bool)op.ShowDialog())
            {
                foreach (string filename in op.FileNames)
                {
                    try
                    {
                        MediaFile mediaFile = new MediaFile(filename);
                        if (mediaFile.size > DefaultSettings.MaxFileLimit250MBinBytes) { anyMorethan500MB = true; continue; }
                        if (!mediaFile.format.Equals("JPEG") && !mediaFile.format.Equals("PNG")) { anyCorrupted = true; continue; }
                        //if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit500MBinBytes) { anyMorethan500MB = true; continue; }
                        //int formatChecked = ImageUtility.GetFileImageTypeFromHeader(filename);
                        //if (formatChecked != 1 && formatChecked != 2) { anyCorrupted = true; continue; }
                        CommonUploaderModel = new CommonUploaderModel();
                        CommonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_IMAGE;
                        CommonUploaderModel.FileSize = mediaFile.size;
                        CommonUploaderModel.FilePath = filename;
                    }
                    catch (Exception) { anyCorrupted = true; }
                }
            }
            if (anyCorrupted) UIHelperMethods.ShowWarning("One or more files are corrupted !");
            if (anyMorethan500MB) UIHelperMethods.ShowWarning("One or more files are greater than Maximum file Limit (250MB) !");
        }

        public void audioSelectToUpload()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Music File";
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "All supported Music files|*.mp3|MP3 files(*.mp3)|*.mp3";
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length == 1)
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);
                            if (mf.size > DefaultSettings.MaxFileLimit500MBinBytes) { anyMorethan500MB = true; continue; }
                            if (mf.Audio == null || mf.Audio.Count == 0 || mf.Video.Count > 0 || !mf.format.Equals("MPEG Audio")) { anyCorrupted = true; continue; }

                            CommonUploaderModel = new CommonUploaderModel();
                            CommonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_MUSIC;
                            CommonUploaderModel.FileSize = mf.size;
                            CommonUploaderModel.FilePath = filename;

                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                                if (file.Tag != null && file.Tag.Pictures.Any())
                                {
                                    if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null)
                                    {
                                        CommonUploaderModel.IsThumbImageFound = true;
                                    }
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                    {
                                        CommonUploaderModel.FileArtist = file.Tag.Performers[0];
                                    }
                                    else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                    {
                                        CommonUploaderModel.FileArtist = file.Tag.AlbumArtists[0];
                                    }
                                    CommonUploaderModel.FileTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception) { }

                            if (string.IsNullOrEmpty(CommonUploaderModel.FileTitle))
                                CommonUploaderModel.FileTitle = Path.GetFileNameWithoutExtension(filename);
                            CommonUploaderModel.FileDuration = (long)(mf.duration / 1000);
                            if (CommonUploaderModel.FileDuration == 0)
                                CommonUploaderModel.FileDuration = HelperMethods.MediaPlayerDuration(filename);
                        }
                        catch (Exception) { anyCorrupted = true; }
                    }
                }
                else UIHelperMethods.ShowFailed("Can't upload more than 1 music at a time !");
            }
            if (anyCorrupted) UIHelperMethods.ShowFailed("One or more files are corrupted or invalid format!");
            if (anyMorethan500MB) UIHelperMethods.ShowWarning("One or more files are greater than Maximum file Limit (500MB) !");
        }

        public void videoSelectToUpload()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Video File";
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "All supported Video files|*.mp4|MP4 files(*.mp4)|*.mp4";
            bool anyCorrupted = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length == 1) // When 5 video select with cntrl--> openFileDialog.FileNames.Length <= 5
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);
                            if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264")) { anyCorrupted = true; continue; }
                            CommonUploaderModel = new CommonUploaderModel();
                            CommonUploaderModel.FileSize = mf.size;
                            CommonUploaderModel.FilePath = filename;
                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the video file location
                                if (file.Tag != null)
                                {
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0])) CommonUploaderModel.FileArtist = file.Tag.Performers[0];
                                    CommonUploaderModel.FileTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception) { }

                            if (string.IsNullOrEmpty(CommonUploaderModel.FileTitle)) CommonUploaderModel.FileTitle = Path.GetFileNameWithoutExtension(filename);
                            CommonUploaderModel.FileDuration = (long)(mf.duration / 1000);
                            if (CommonUploaderModel.FileDuration == 0) CommonUploaderModel.FileDuration = HelperMethods.MediaPlayerDuration(filename);
                            HelperMethods.SetVideoArtImage(CommonUploaderModel);// new SetArtImageFromVideo(CommonUploaderModel);//120*90
                            CommonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_VIDEO;
                        }
                        catch (Exception) { anyCorrupted = true; }
                    }
                }
                else UIHelperMethods.ShowWarning("Can't upload more than 1 videos at a time !");
            }
            if (anyCorrupted) UIHelperMethods.ShowWarning("One or more files are corrupted or Invalid format!");
        }

        private void ShowRecorderView()
        {
            if (UCVideoRecorderPreviewPanel.Instance != null)
                motherPanel.Children.Remove(UCVideoRecorderPreviewPanel.Instance);
            UCVideoRecorderPreviewPanel.Instance = new UCVideoRecorderPreviewPanel(motherPanel);
            UCVideoRecorderPreviewPanel.Instance.Show();
            UCVideoRecorderPreviewPanel.Instance.ShowRecorder();
            UCVideoRecorderPreviewPanel.Instance.OnRemovedUserControl += () =>
            {
                UCVideoRecorderPreviewPanel.Instance = null;
            };
        }

        public void webCamToUpload()
        {
            try
            {
                ShowRecorderView();
                UCVideoRecorderPreviewPanel.Instance.OnVideoAddedToUploadList += (f, t) =>
                {
                    if (!string.IsNullOrEmpty(f))
                    {
                        MediaFile mf = new MediaFile(f);

                        //if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264"))
                        //{

                        //}
                        if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264")) {}
                        CommonUploaderModel = new CommonUploaderModel();
                        //File.Move(f, Path.ChangeExtension(".avi", ".mp4"));
                        CommonUploaderModel.FilePath = f;
                        FileInfo fi = new FileInfo(Path.ChangeExtension(CommonUploaderModel.FilePath, ".mp4"));
                        //commonUploaderModel.FilePath = fi.FullName;

                        CommonUploaderModel.FileSize = fi.Exists ? (long)(fi.Length / 1024) : 0;
                        CommonUploaderModel.FileDuration = t;
                        try
                        {
                            //var uri = new System.Uri(f);
                            //var converted = uri.AbsoluteUri;
                            string newName = f.Replace("/", "\\");
                            //String RelativePath = AbsolutePath.Replace(Request.ServerVariables["APPL_PHYSICAL_PATH"], String.Empty);
                            CommonUploaderModel.FileSize = mf.size;
                        }
                        catch (Exception) { }

                        if (string.IsNullOrEmpty(CommonUploaderModel.FileTitle)) CommonUploaderModel.FileTitle = Path.GetFileNameWithoutExtension(f);
                        CommonUploaderModel.FileDuration = (long)(mf.duration / 1000);
                        if (CommonUploaderModel.FileDuration == 0) CommonUploaderModel.FileDuration = HelperMethods.MediaPlayerDuration(f);
                        HelperMethods.SetVideoArtImage(CommonUploaderModel);// new SetArtImageFromVideo(CommonUploaderModel);//120*90
                        CommonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_VIDEO;
                    }
                    HideVideoRecord();
                };
                IsVideoRecordMode = true;
                //string a = _CommentRecorderPanel.FileName;
            }

            catch (Exception ex)
            {

                log.Error("Error: webCamToUpload() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        public void recordToUpload()
        {
            try
            {
                _CommentAudioRecorderPanel = new UCCommentAudioRecorder();
                UCGuiRingID.Instance.MotherPanel.Children.Add(_CommentAudioRecorderPanel);
                _CommentAudioRecorderPanel.RecordingCompletedHandler += (f, t) =>
                {
                    MediaFile mf = new MediaFile(f);

                    if (mf.size > DefaultSettings.MaxFileLimit500MBinBytes) { }
                    if (mf.Audio == null || mf.Audio.Count == 0 || mf.Video.Count > 0 || !mf.format.Equals("MPEG Audio")) { }

                    CommonUploaderModel = new CommonUploaderModel();
                    CommonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_MUSIC;
                    //commonUploaderModel.FileSize = mf.size;
                    CommonUploaderModel.FilePath = f;
                    FileInfo fi = new FileInfo(Path.ChangeExtension(CommonUploaderModel.FilePath, AudioCapture.AUDIO_FORMAT));
                    CommonUploaderModel.FileSize = fi.Exists ? (long)(fi.Length / 1024) : 0;
                    try
                    {
                        TagLib.File file = TagLib.File.Create(f); //FilePath is the audio file location
                        if (file.Tag != null && file.Tag.Pictures.Any())
                        {
                            if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null) CommonUploaderModel.IsThumbImageFound = true;
                            if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0])) CommonUploaderModel.FileArtist = file.Tag.Performers[0];
                            else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0])) CommonUploaderModel.FileArtist = file.Tag.AlbumArtists[0];
                            CommonUploaderModel.FileTitle = file.Tag.Title;
                        }
                    }
                    catch (Exception) { }

                    if (string.IsNullOrEmpty(CommonUploaderModel.FileTitle)) CommonUploaderModel.FileTitle = Path.GetFileNameWithoutExtension(f);
                    CommonUploaderModel.FileDuration = (long)(mf.duration / 1000);
                    if (CommonUploaderModel.FileDuration == 0) CommonUploaderModel.FileDuration = HelperMethods.MediaPlayerDuration(f);
                    HideAudioRecord();
                };
                IsAudioRecordMode = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: recordToUpload() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideAudioRecord()
        {
            try
            {
                IsAudioRecordMode = false;
                if (_CommentAudioRecorderPanel != null)
                {
                    _CommentAudioRecorderPanel.Dispose();
                    _CommentAudioRecorderPanel = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: HideAudioRecord() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool _IsAudioRecordMode = false;
        public bool IsAudioRecordMode
        {
            get { return _IsAudioRecordMode; }
            set
            {
                if (_IsAudioRecordMode == value)
                    return;
                _IsAudioRecordMode = value;
                this.OnPropertyChanged("IsAudioRecordMode");
            }
        }

        private bool _IsVideoRecordMode = false;
        public bool IsVideoRecordMode
        {
            get { return _IsVideoRecordMode; }
            set
            {
                if (_IsVideoRecordMode == value)
                    return;
                _IsVideoRecordMode = value;
                this.OnPropertyChanged("IsVideoRecordMode");
            }
        }

        public void HideVideoRecord()
        {
            try
            {
                IsVideoRecordMode = false;
                //if (_CommentRecorderPanel != null)
                //{
                //    _CommentRecorderPanel.Dispose();
                //    _CommentRecorderPanel = null;
                //}
            }
            catch (Exception ex)
            {
                log.Error("Error: HideVideoRecord() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void stickerSelectedToUpload(string selctedStickerPath)
        {
            CommonUploaderModel = new CommonUploaderModel();
            CommonUploaderModel.UploadedUrl = selctedStickerPath;
            CommonUploaderModel.FilePath = @RingIDSettings.STICKER_FOLDER + System.IO.Path.DirectorySeparatorChar + (selctedStickerPath).Replace(System.IO.Path.AltDirectorySeparatorChar, System.IO.Path.DirectorySeparatorChar);
            CommonUploaderModel.UploadType = SettingsConstants.UPLOADTYPE_STICKER;
        }
        #endregion

        #region "handlers"
        //private void CrossButton_Click(object sender, RoutedEventArgs e)
        //{
        //    if (RemoveButtonEnabled)
        //        SetAllModelsToNull();
        //    if (IsEditMode)
        //        IsEditMode = false;
        //}
        private ICommand _CrossButtonCommand;
        public ICommand CrossButtonCommand
        {
            get
            {
                if (_CrossButtonCommand == null)
                {
                    _CrossButtonCommand = new RelayCommand(param => OnCrossButtonClickCommand(param));
                }
                return _CrossButtonCommand;
            }
        }
        private void OnCrossButtonClickCommand(object parameter)
        {
            if (RemoveButtonEnabled)
                SetAllModelsToNull();
            if (IsEditMode)
                IsEditMode = false;
            if (_OnClosing != null)
            {
                _OnClosing(1);
                _OnClosing = null;
            }
        }

        //private void mediaPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    //ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
        //    SingleMediaModel singleMediamodel = new SingleMediaModel();

        //    singleMediamodel.UserTableID = DefaultSettings.LOGIN_TABLE_ID;
        //    singleMediamodel.ProfileImage = DefaultSettings.LOGIN_USER_PROFILE_IMAGE;
        //    singleMediamodel.Title = commonUploaderModel.FileTitle;
        //    singleMediamodel.Duration = commonUploaderModel.FileDuration;
        //    singleMediamodel.Artist = commonUploaderModel.FileArtist;
        //    singleMediamodel.ThumbUrl = commonUploaderModel.UploadedThumbUrl;

        //    if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_MUSIC)
        //    {
        //        singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_AUDIO;

        //        if (string.IsNullOrEmpty(commonUploaderModel.FilePath))
        //        {
        //            singleMediamodel.StreamUrl = commonUploaderModel.UploadedUrl;
        //            singleMediamodel.IsFromLocalDirectory = false;
        //            //PlayerHelperMethods.RunPlayListFromComments(false, commonUploaderModel.UploadedUrl, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 1);
        //        }
        //        else
        //        {
        //            singleMediamodel.StreamUrl = commonUploaderModel.FilePath;
        //            singleMediamodel.IsFromLocalDirectory = true;
        //            //PlayerHelperMethods.RunPlayListFromComments(true, commonUploaderModel.FilePath, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 1);
        //        }
        //    }
        //    else if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_VIDEO)
        //    {
        //        singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;

        //        if (string.IsNullOrEmpty(commonUploaderModel.FilePath))
        //        {
        //            singleMediamodel.StreamUrl = commonUploaderModel.UploadedUrl;
        //            singleMediamodel.IsFromLocalDirectory = false;
        //            //PlayerHelperMethods.RunPlayListFromComments(false, commonUploaderModel.UploadedUrl, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 2);
        //        }
        //        else
        //        {
        //            singleMediamodel.StreamUrl = commonUploaderModel.FilePath;
        //            singleMediamodel.IsFromLocalDirectory = false;
        //        }
        //        //PlayerHelperMethods.RunPlayListFromComments(true, commonUploaderModel.FilePath, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 2);
        //        //MediaList.Add(singleMediamodel);
        //        //MediaUtility.RunPlayList(singleMediamodel.IsFromLocalDirectory, MediaList);
        //        //PlayerHelperMethods.RunPlayListFromComments(singleMediamodel.IsFromLocalDirectory, MediaList);   
        //    }
        //    MediaUtility.PlaySingleMediaNoContentID(singleMediamodel, singleMediamodel.IsFromLocalDirectory);
        //}
        #endregion
    }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
}
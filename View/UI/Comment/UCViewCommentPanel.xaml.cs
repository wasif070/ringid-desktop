<<<<<<< HEAD
using System.ComponentModel;
using System.Windows.Controls;
﻿using System.ComponentModel;
﻿using log4net;
using Models.Constants;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls;
using System;
using System.Windows.Input;
using View.Utility;
using View.BindingModels;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCViewCommentPanel.xaml
    /// </summary>
    public partial class UCViewCommentPanel : UserControl, INotifyPropertyChanged
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(UCViewCommentPanel).Name);
        public Grid motherPanel;

        public UCViewCommentPanel()
        {
            InitializeComponent();
            //this.DataContext = this;
        }

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        #endregion

        #region "ICommand"
        private ICommand _ImageOrMediaClickCommand;
        public ICommand ImageOrMediaClickCommand
        {
            get
            {
                if (_ImageOrMediaClickCommand == null)
                {
                    _ImageOrMediaClickCommand = new RelayCommand(param => OnImageOrMediaClickCommand(param));
                }
                return _ImageOrMediaClickCommand;
            }
        }
        private void OnImageOrMediaClickCommand(object parameter)
        {
            try
            {
                if (parameter is CommentModel)
                {
                    CommentModel commentModel = (CommentModel)parameter;
                    if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
                    {
                        ObservableCollection<ImageModel> tempList = new ObservableCollection<ImageModel>();
                        ImageModel imageModel = new ImageModel();
                        imageModel.ImageUrl = commentModel.MediaOrImageUrl;
                        tempList.Add(imageModel);
                        ImageUtility.ShowImageViewer(tempList, 0, 1, StatusConstants.NAVIGATE_FROM_FEED, Guid.Empty, motherPanel, false);
                    }
                    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC)
                    {
                        SingleMediaModel singleMediaModel = new SingleMediaModel();
                        singleMediaModel.StreamUrl = commentModel.MediaOrImageUrl;
                        singleMediaModel.ThumbUrl = commentModel.ThumbUrl;
                        singleMediaModel.Duration = commentModel.Duration;
                        singleMediaModel.MediaOwner = commentModel.UserShortInfoModel;
                        singleMediaModel.MediaType = 1;
                        //singleMediaModel.MediaOwner.UserTableID = commentModel.PostOwnerUtId;
                        MediaUtility.PlaySingleMediaNoContentID(singleMediaModel, false, motherPanel);
                    }
                    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
                    {
                        SingleMediaModel singleMediaModel = new SingleMediaModel();
                        singleMediaModel.StreamUrl = commentModel.MediaOrImageUrl;
                        singleMediaModel.ThumbUrl = commentModel.ThumbUrl;
                        singleMediaModel.Duration = commentModel.Duration;
                        singleMediaModel.MediaOwner = commentModel.UserShortInfoModel;
                        singleMediaModel.MediaType = 2;
                        //singleMediaModel.MediaOwner.UserTableID = commentModel.PostOwnerUtId;
                        MediaUtility.PlaySingleMediaNoContentID(singleMediaModel, false, motherPanel);
                    }
                }
            }
            catch (Exception ex)
            {
                //log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion
        //private void commentSeeMore_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    ThreadGetSingleFullComment threadFullComment = new ThreadGetSingleFullComment(commentModel.NewsfeedId, commentModel.CommentId, 0, 0);
        //    threadFullComment.callBackEvent += (jObj) =>
        //    {
        //        if (jObj != null) commentModel.LoadData(jObj);
        //    };
        //    threadFullComment.StartThread();
        //}

        //private void mediaPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
        //    {
        //        //List<ImageModel> tempList = new List<ImageModel>();
        //        //ImageModel imageModel = new ImageModel();
        //        //imageModel.ImageUrl = commentModel.MediaOrImageUrl;
        //        //tempList.Add(imageModel);
        //        // MainSwitcher.PopupController.UCPortalPagesImageView.ShowHandlerDialog(tempList, true);
        //        ImageUtility.ShowFullImageFromComments(commentModel.MediaOrImageUrl);
        //    }
        //    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC)
        //    {
        //        //ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
        //        SingleMediaModel singleMediamodel = new SingleMediaModel();

        //        singleMediamodel.UserTableID = commentModel.UserShortInfoModel.UserTableID;
        //        singleMediamodel.FullName = commentModel.UserShortInfoModel.FullName;
        //        singleMediamodel.ProfileImage = commentModel.UserShortInfoModel.ProfileImage;
        //        singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
        //        singleMediamodel.Duration = commentModel.Duration;
        //        singleMediamodel.ThumbUrl = commentModel.ThumbUrl;
        //        singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_AUDIO;
        //        singleMediamodel.IsFromLocalDirectory = false;

        //        MediaUtility.PlaySingleMediaNoContentID(singleMediamodel, singleMediamodel.IsFromLocalDirectory);

        //        //singleMediamodel.Title = fileTitle;        
        //        //singleMediamodel.Artist = artist;
        //        //MediaList.Add(singleMediamodel);
        //        //MediaUtility.RunPlayList(false, commentModel.CommentId, MediaList);
        //        //MediaUtility.RunPlayList(false, FeedModel.NewsfeedId, FeedModel.MediaContent.MediaList, ownerOfMedia, idx);
        //        //PlayerHelperMethods.RunPlayListFromComments(false, MediaList);
        //    }
        //    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
        //    {
        //        ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
        //        SingleMediaModel singleMediamodel = new SingleMediaModel();

        //        singleMediamodel.UserTableID = commentModel.UserShortInfoModel.UserTableID;
        //        singleMediamodel.FullName = commentModel.UserShortInfoModel.FullName;
        //        singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
        //        singleMediamodel.Duration = commentModel.Duration;
        //        singleMediamodel.ThumbUrl = commentModel.ThumbUrl;
        //        singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;
        //        singleMediamodel.IsFromLocalDirectory = false;

        //        MediaUtility.PlaySingleMediaNoContentID(singleMediamodel, singleMediamodel.IsFromLocalDirectory);

        //        //singleMediamodel.Title = fileTitle;        
        //        //singleMediamodel.Artist = artist;
        //        //MediaList.Add(singleMediamodel);
        //        //MediaUtility.RunPlayList(false, commentModel.CommentId, MediaList);
        //        //PlayerHelperMethods.RunPlayListFromComments(false, MediaList);
        //        //PlayerHelperMethods.RunPlayListFromComments(false, commentModel.MediaOrImageUrl, commentModel.ThumbUrl, string.Empty, string.Empty, commentModel.Duration, 2);
        //    }

        //}
    }
}
//=======
//                if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
//                {
//                    List<ImageModel> tempList = new List<ImageModel>();
//                    ImageModel imageModel = new ImageModel();

//                    imageModel.ImageUrl = commentModel.MediaOrImageUrl;
//                    tempList.Add(imageModel);
//                    MainSwitcher.PopupController.UCPortalPagesImageView.SetUserModel(tempList, true);
//                }
//                else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC)
//                {
//                    ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//                    SingleMediaModel singleMediamodel = new SingleMediaModel();

//                        singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//                        singleMediamodel.Duration = commentModel.Duration;
//                        singleMediamodel.ThumbUrl = commentModel.ThumbUrl;

//                    singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_AUDIO;
//                    MediaList.Add(singleMediamodel);
//                    MediaUtility.RunPlayList(true, 0, MediaList);
//                }
//                else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
//                {
//                    ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//                    SingleMediaModel singleMediamodel = new SingleMediaModel();

//                    singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//                    singleMediamodel.Duration = commentModel.Duration;
//                    singleMediamodel.ThumbUrl = commentModel.ThumbUrl;

//                    singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;
//                    singleMediamodel.IsFromLocalDirectory = true;
//                    MediaList.Add(singleMediamodel);
//                    MediaUtility.RunPlayList(true, 0, MediaList);
//                }

//>>>>>>> .r836
//private ICommand _imageMediaCommand;
//public ICommand ImageMediaCommand
//{
//    get
//    {
//        if (_imageMediaCommand == null)
//        {
//            _imageMediaCommand = new RelayCommand(param => OnImageMediaCommandClicked(param));
//        }
//        return _imageMediaCommand;
//    }
//}

//private void OnImageMediaCommandClicked(object parameter)
//{
//    try
//    {
//        if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
//        {
//            List<ImageModel> tempList = new List<ImageModel>();
//            ImageModel imageModel = new ImageModel();

//            imageModel.ImageUrl = commentModel.MediaOrImageUrl;
//            tempList.Add(imageModel);
//            //MainSwitcher.PopupController.UCPortalPagesImageView.SetUserModel(tempList, true);
//        }
//        else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC)
//        {
//            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//            SingleMediaModel singleMediamodel = new SingleMediaModel();

//            singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//            singleMediamodel.Duration = commentModel.Duration;
//            singleMediamodel.ThumbUrl = commentModel.ThumbUrl;

//            singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_AUDIO;
//            MediaList.Add(singleMediamodel);
//            // MediaUtility.RunPlayList(true, 0, MediaList);
//        }
//        else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
//        {
//            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//            SingleMediaModel singleMediamodel = new SingleMediaModel();

//            singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//            singleMediamodel.Duration = commentModel.Duration;
//            singleMediamodel.ThumbUrl = commentModel.ThumbUrl;

//            singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;
//            singleMediamodel.IsFromLocalDirectory = true;
//            MediaList.Add(singleMediamodel);
//            //MediaUtility.RunPlayList(true, 0, MediaList);
//        }

//    }
//    catch (Exception ex)
//    {
//        log.Error(ex.StackTrace + ex.Message);
//    }
//}>>>>>>> .merge-right.r2138
=======
using System.ComponentModel;
using System.Windows.Controls;
﻿using System.ComponentModel;
﻿using log4net;
using Models.Constants;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls;
using System;
using System.Windows.Input;
using View.Utility;
using View.BindingModels;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCViewCommentPanel.xaml
    /// </summary>
    public partial class UCViewCommentPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCViewCommentPanel).Name);
        public Grid motherPanel;

        public UCViewCommentPanel()
        {
            InitializeComponent();
            //this.DataContext = this;
        }

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        #endregion

        #region "ICommand"
        private ICommand _ImageOrMediaClickCommand;
        public ICommand ImageOrMediaClickCommand
        {
            get
            {
                if (_ImageOrMediaClickCommand == null)
                {
                    _ImageOrMediaClickCommand = new RelayCommand(param => OnImageOrMediaClickCommand(param));
                }
                return _ImageOrMediaClickCommand;
            }
        }
        private void OnImageOrMediaClickCommand(object parameter)
        {
            try
            {
                if (parameter is CommentModel)
                {
                    CommentModel commentModel = (CommentModel)parameter;
                    if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
                    {
                        ObservableCollection<ImageModel> tempList = new ObservableCollection<ImageModel>();
                        ImageModel imageModel = new ImageModel();
                        imageModel.ImageUrl = commentModel.MediaOrImageUrl;
                        tempList.Add(imageModel);
                        ImageUtility.ShowImageViewer(tempList, 0, 1, StatusConstants.NAVIGATE_FROM_FEED, Guid.Empty, motherPanel, false);
                    }
                    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC)
                    {
                        SingleMediaModel singleMediaModel = new SingleMediaModel();
                        singleMediaModel.StreamUrl = commentModel.MediaOrImageUrl;
                        singleMediaModel.ThumbUrl = commentModel.ThumbUrl;
                        singleMediaModel.Duration = commentModel.Duration;
                        singleMediaModel.MediaOwner = commentModel.UserShortInfoModel;
                        singleMediaModel.MediaType = 1;
                        MediaUtility.PlaySingleMediaNoContentID(singleMediaModel, false, motherPanel);
                    }
                    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
                    {
                        SingleMediaModel singleMediaModel = new SingleMediaModel();
                        singleMediaModel.StreamUrl = commentModel.MediaOrImageUrl;
                        singleMediaModel.ThumbUrl = commentModel.ThumbUrl;
                        singleMediaModel.Duration = commentModel.Duration;
                        singleMediaModel.MediaOwner = commentModel.UserShortInfoModel;
                        singleMediaModel.MediaType = 2;
                        MediaUtility.PlaySingleMediaNoContentID(singleMediaModel, false, motherPanel);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion
    }
}
//=======
//                if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
//                {
//                    List<ImageModel> tempList = new List<ImageModel>();
//                    ImageModel imageModel = new ImageModel();

//                    imageModel.ImageUrl = commentModel.MediaOrImageUrl;
//                    tempList.Add(imageModel);
//                    MainSwitcher.PopupController.UCPortalPagesImageView.SetUserModel(tempList, true);
//                }
//                else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC)
//                {
//                    ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//                    SingleMediaModel singleMediamodel = new SingleMediaModel();

//                        singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//                        singleMediamodel.Duration = commentModel.Duration;
//                        singleMediamodel.ThumbUrl = commentModel.ThumbUrl;

//                    singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_AUDIO;
//                    MediaList.Add(singleMediamodel);
//                    MediaUtility.RunPlayList(true, 0, MediaList);
//                }
//                else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
//                {
//                    ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//                    SingleMediaModel singleMediamodel = new SingleMediaModel();

//                    singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//                    singleMediamodel.Duration = commentModel.Duration;
//                    singleMediamodel.ThumbUrl = commentModel.ThumbUrl;

//                    singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;
//                    singleMediamodel.IsFromLocalDirectory = true;
//                    MediaList.Add(singleMediamodel);
//                    MediaUtility.RunPlayList(true, 0, MediaList);
//                }

//>>>>>>> .r836
//private ICommand _imageMediaCommand;
//public ICommand ImageMediaCommand
//{
//    get
//    {
//        if (_imageMediaCommand == null)
//        {
//            _imageMediaCommand = new RelayCommand(param => OnImageMediaCommandClicked(param));
//        }
//        return _imageMediaCommand;
//    }
//}

//private void OnImageMediaCommandClicked(object parameter)
//{
//    try
//    {
//        if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
//        {
//            List<ImageModel> tempList = new List<ImageModel>();
//            ImageModel imageModel = new ImageModel();

//            imageModel.ImageUrl = commentModel.MediaOrImageUrl;
//            tempList.Add(imageModel);
//            //MainSwitcher.PopupController.UCPortalPagesImageView.SetUserModel(tempList, true);
//        }
//        else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC)
//        {
//            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//            SingleMediaModel singleMediamodel = new SingleMediaModel();

//            singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//            singleMediamodel.Duration = commentModel.Duration;
//            singleMediamodel.ThumbUrl = commentModel.ThumbUrl;

//            singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_AUDIO;
//            MediaList.Add(singleMediamodel);
//            // MediaUtility.RunPlayList(true, 0, MediaList);
//        }
//        else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
//        {
//            ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//            SingleMediaModel singleMediamodel = new SingleMediaModel();

//            singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//            singleMediamodel.Duration = commentModel.Duration;
//            singleMediamodel.ThumbUrl = commentModel.ThumbUrl;

//            singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;
//            singleMediamodel.IsFromLocalDirectory = true;
//            MediaList.Add(singleMediamodel);
//            //MediaUtility.RunPlayList(true, 0, MediaList);
//        }

//    }
//    catch (Exception ex)
//    {
//        log.Error(ex.StackTrace + ex.Message);
//    }
//}>>>>>>> .merge-right.r2138

//private void commentSeeMore_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
//{
//    ThreadGetSingleFullComment threadFullComment = new ThreadGetSingleFullComment(commentModel.NewsfeedId, commentModel.CommentId, 0, 0);
//    threadFullComment.callBackEvent += (jObj) =>
//    {
//        if (jObj != null) commentModel.LoadData(jObj);
//    };
//    threadFullComment.StartThread();
//}

//private void mediaPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
//{
//    if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
//    {
//        //List<ImageModel> tempList = new List<ImageModel>();
//        //ImageModel imageModel = new ImageModel();
//        //imageModel.ImageUrl = commentModel.MediaOrImageUrl;
//        //tempList.Add(imageModel);
//        // MainSwitcher.PopupController.UCPortalPagesImageView.ShowHandlerDialog(tempList, true);
//        ImageUtility.ShowFullImageFromComments(commentModel.MediaOrImageUrl);
//    }
//    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC)
//    {
//        //ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//        SingleMediaModel singleMediamodel = new SingleMediaModel();

//        singleMediamodel.UserTableID = commentModel.UserShortInfoModel.UserTableID;
//        singleMediamodel.FullName = commentModel.UserShortInfoModel.FullName;
//        singleMediamodel.ProfileImage = commentModel.UserShortInfoModel.ProfileImage;
//        singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//        singleMediamodel.Duration = commentModel.Duration;
//        singleMediamodel.ThumbUrl = commentModel.ThumbUrl;
//        singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_AUDIO;
//        singleMediamodel.IsFromLocalDirectory = false;

//        MediaUtility.PlaySingleMediaNoContentID(singleMediamodel, singleMediamodel.IsFromLocalDirectory);

//        //singleMediamodel.Title = fileTitle;        
//        //singleMediamodel.Artist = artist;
//        //MediaList.Add(singleMediamodel);
//        //MediaUtility.RunPlayList(false, commentModel.CommentId, MediaList);
//        //MediaUtility.RunPlayList(false, FeedModel.NewsfeedId, FeedModel.MediaContent.MediaList, ownerOfMedia, idx);
//        //PlayerHelperMethods.RunPlayListFromComments(false, MediaList);
//    }
//    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
//    {
//        ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
//        SingleMediaModel singleMediamodel = new SingleMediaModel();

//        singleMediamodel.UserTableID = commentModel.UserShortInfoModel.UserTableID;
//        singleMediamodel.FullName = commentModel.UserShortInfoModel.FullName;
//        singleMediamodel.StreamUrl = commentModel.MediaOrImageUrl;
//        singleMediamodel.Duration = commentModel.Duration;
//        singleMediamodel.ThumbUrl = commentModel.ThumbUrl;
//        singleMediamodel.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;
//        singleMediamodel.IsFromLocalDirectory = false;

//        MediaUtility.PlaySingleMediaNoContentID(singleMediamodel, singleMediamodel.IsFromLocalDirectory);

//        //singleMediamodel.Title = fileTitle;        
//        //singleMediamodel.Artist = artist;
//        //MediaList.Add(singleMediamodel);
//        //MediaUtility.RunPlayList(false, commentModel.CommentId, MediaList);
//        //PlayerHelperMethods.RunPlayListFromComments(false, MediaList);
//        //PlayerHelperMethods.RunPlayListFromComments(false, commentModel.MediaOrImageUrl, commentModel.ThumbUrl, string.Empty, string.Empty, commentModel.Duration, 2);
//    }

//}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

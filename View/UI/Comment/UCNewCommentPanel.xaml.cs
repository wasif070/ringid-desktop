<<<<<<< HEAD
﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.ImageViewer;
using View.UI.PopUp;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingMarket;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using System.Linq;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCNewCommentPanel.xaml
    /// </summary>
    public partial class UCNewCommentPanel : UserControl, INotifyPropertyChanged
    {
        #region
        private bool needTofocus = true;
        public Grid motherPanel;
        #endregion
        #region "Constructor"
        public UCNewCommentPanel()
        {
            InitializeComponent();
        }
        #endregion

        #region "Property"
        public Guid newsfeedId, contentId, imageId;//set imageId,cntnid=0 from feed

        private MarkertStickerImagesModel RecentStickerModel;
        private UCCommentUploadPanel ucCommentUploadPanel;

        private bool _UploadPopup;
        public bool UploadPopup
        {
            get { return _UploadPopup; }
            set
            {
                if (value == _UploadPopup)
                    return;
                _UploadPopup = value;
                this.OnPropertyChanged("UploadPopup");
            }
        }
        private bool _FocusNewComment;
        public bool FocusNewComment
        {
            get { return _FocusNewComment; }
            set
            {
                if (value == true)
                {
                    //if (View.UI.Feed.UCAllFeeds.Instance != null)
                    NewsFeedViewModel.Instance.AllFeedsScrollEnabled = false;
                    richTextBox.Focusable = true;
                    richTextBox.Focus();
                }
                if (value == _FocusNewComment)
                    return;
                _FocusNewComment = value;
            }
        }

        private UCCommentUploadPopup ucCommentUploadPopup { get; set; }
        #endregion

        #region "OnPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Method"
        private void EnablePostbutton(bool flag)
        {
            richTextBox.IsEnabled = flag;
            commentPostBtn.IsEnabled = flag;
            emoticonBtn.IsEnabled = flag;
            uploadBtn.IsEnabled = flag;

            if (ucCommentUploadPanel != null) ucCommentUploadPanel.RemoveButtonEnabled = flag;
        }

        private void addUploadPanel()
        {
            if (ucCommentUploadPanel == null)
            {
                ucCommentUploadPanel = new UCCommentUploadPanel((type) =>
                {
                    int CancelType = type;
                    if (CancelType == 1)
                    {
                        richTextBox.Focusable = true;
                        richTextBox.Focus();
                    }
                    return 0;
                });
            }
            else if (ucCommentUploadPanel.Parent is Border) ((Border)ucCommentUploadPanel.Parent).Child = null;
            ucCommentUploadPanel.motherPanel = motherPanel;
            _uploadPanel.Child = ucCommentUploadPanel;
        }

        public void SetNeedToFocus(bool focus)
        {
            needTofocus = focus;
        }

        #endregion
        private ICommand _UploadButtonCommand;
        public ICommand UploadButtonCommand
        {
            get
            {
                if (_UploadButtonCommand == null)
                {
                    _UploadButtonCommand = new RelayCommand(param => OnUploadButtonCommand(param));
                }
                return _UploadButtonCommand;
            }
        }
        private void OnUploadButtonCommand(object parameter)
        {
            UploadPopup = true;
            if (ucCommentUploadPopup != null)
                motherPanel.Children.Remove(ucCommentUploadPopup);
            ucCommentUploadPopup = new UCCommentUploadPopup(motherPanel);
            ucCommentUploadPopup.Show();
            ucCommentUploadPopup.ShowPopup(uploadBtn, (type) =>
            {
                int upType = type;
                addUploadPanel();

                if (upType == 1)
                {
                    ucCommentUploadPanel.imageSelectToUpload();
                }
                else if (upType == 2)
                {
                    ucCommentUploadPanel.audioSelectToUpload();
                }
                else if (upType == 3)
                {
                    ucCommentUploadPanel.videoSelectToUpload();
                }
                else if (upType == 4)
                {
                    ucCommentUploadPanel.webCamToUpload();
                }
                else if (upType == 5)
                {
                    ucCommentUploadPanel.recordToUpload();
                }
                UploadPopup = false;
                richTextBox.Focusable = true;
                richTextBox.Focus();
                return 0;
            });
        }
        private ICommand _EmoticonButtonCommand;
        public ICommand EmoticonButtonCommand
        {
            get
            {
                if (_EmoticonButtonCommand == null)
                {
                    _EmoticonButtonCommand = new RelayCommand(param => OnEmoticonButtonCommand(param));
                }
                return _EmoticonButtonCommand;
            }
        }
        private void OnEmoticonButtonCommand(object parameter)
        {
            UIElement uiElement = (UIElement)emoticonBtn;
            MainSwitcher.PopupController.StickerPopUPInComments.Show(uiElement, UCEmoticonPopup.TYPE_DEFAULT, motherPanel, (obj) =>
            {
                if (obj is string)
                {
                    string emoticonStr = (string)obj;
                    if (!String.IsNullOrWhiteSpace(emoticonStr))
                    {
                        richTextBox.AppendText(emoticonStr + " ");
                    }
                }
                else if (obj is MarkertStickerImagesModel)
                {
                    RecentStickerModel = (MarkertStickerImagesModel)obj;
                    addUploadPanel();
                    ucCommentUploadPanel.stickerSelectedToUpload(RecentStickerModel.ToString());
                    if (string.IsNullOrWhiteSpace(richTextBox.Text.Trim()))
                        OnCommentPostCommand();
                    else
                    {
                        richTextBox.Focusable = true;
                        richTextBox.Focus();
                    }
                }
                return 0;
            });

        }
        #region "Event Trigger"
        private ICommand _CommentPostCommand;
        public ICommand CommentPostCommand
        {
            get
            {
                if (_CommentPostCommand == null)
                {
                    _CommentPostCommand = new RelayCommand(param => OnCommentPostCommand());
                }
                return _CommentPostCommand;
            }
        }

        private void richTextFocus()
        {
            richTextBox.Focus();
            richTextBox.Focusable = true;
        }

        private void OnCommentPostCommand()
        {
            if (string.IsNullOrWhiteSpace(richTextBox.Text.Trim()) && (ucCommentUploadPanel == null || ucCommentUploadPanel.commonUploaderModel == null))
            {
                //CustomMessageBox.ShowInformation(NotificationMessages.NOTHING_TO_POST);
                UIHelperMethods.ShowWarning(NotificationMessages.NOTHING_TO_POST);
            }
            else
            {
                richTextBox.SetStatusandTags();
                FeedModel feedModel = null;
                bool ImageIdToContentId = false;
                ThreadAddOrEditAnyComment threadAddOrEditAnyComment = null;
                JObject pakToSend = new JObject();

                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_STATUS_COMMENT;

                if (richTextBox.TagsJArray != null) pakToSend[JsonKeys.CommentTagList] = richTextBox.TagsJArray;
                pakToSend[JsonKeys.Comment] = (richTextBox.TagsJArray != null) ? richTextBox.StringWithoutTags : richTextBox.StringWithoutTags.Trim();

                //pakToSend[JsonKeys.Comment] = string.Empty;

                //if (richTextBox.TagsJArray == null richTextBox.StringWithoutTags.Trim() != null) pakToSend[JsonKeys.Comment] = richTextBox.StringWithoutTags.Trim();
                //else pakToSend[JsonKeys.Comment] = string.Empty;

                if (imageId != Guid.Empty) //COMMENT ON IMAGE
                {
                    ImageModel imageModel = null;
                    ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(imageId, out imageModel);
                    bool fromFeed = (ViewConstants.NavigateFrom == StatusConstants.NAVIGATE_FROM_FEED) ? true : false;
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_IMAGE;
                    pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                    pakToSend[JsonKeys.ContentId] = imageModel.ImageId;
                    ImageIdToContentId = true;

                    if (fromFeed)
                    {
                        pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                        if (imageModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(imageModel.NewsFeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.NewsfeedId] = imageModel.NewsFeedId;
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;
                            pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            pakToSend[JsonKeys.UserTableID] = feedModel.WallOrContentOwner.UserTableID;
                        }
                    }
                    else
                    {
                        pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                        pakToSend[JsonKeys.AlbumId] = imageModel.AlbumId;
                        if (imageModel.Privacy != 0) pakToSend[JsonKeys.Privacy] = imageModel.Privacy;
                        else pakToSend[JsonKeys.Privacy] = SettingsConstants.PRIVACY_PUBLIC;
                        pakToSend[JsonKeys.UserTableID] = imageModel.UserShortInfoModel.UserTableID;
                    }
                    threadAddOrEditAnyComment = new ThreadAddOrEditAnyComment(pakToSend, (ucCommentUploadPanel == null) ? null : ucCommentUploadPanel.commonUploaderModel, ImageIdToContentId);
                }

                else if (contentId != Guid.Empty) //COMMENT ON MULTIMEDIA
                {
                    SingleMediaModel singleMediaModel = null;
                    MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel);
                    bool fromFeed = singleMediaModel.PlayedFromFeed ? true : false;
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_MULTIMEDIA;
                    pakToSend[JsonKeys.ContentId] = singleMediaModel.ContentId;
                    pakToSend[JsonKeys.MediaType] = singleMediaModel.MediaType;

                    if (fromFeed)
                    {
                        if (singleMediaModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(singleMediaModel.NewsFeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            pakToSend[JsonKeys.NewsfeedId] = singleMediaModel.NewsFeedId;
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;
                            pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            pakToSend[JsonKeys.UserTableID] = feedModel.WallOrContentOwner.UserTableID;
                        }
                    }
                    else
                    {
                        pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                        pakToSend[JsonKeys.AlbumId] = singleMediaModel.AlbumId;
                        if (singleMediaModel.Privacy != 0) pakToSend[JsonKeys.Privacy] = singleMediaModel.Privacy;
                        else pakToSend[JsonKeys.Privacy] = SettingsConstants.PRIVACY_PUBLIC;
                        pakToSend[JsonKeys.UserTableID] = singleMediaModel.MediaOwner.UserTableID;
                    }
                    threadAddOrEditAnyComment = new ThreadAddOrEditAnyComment(pakToSend, (ucCommentUploadPanel == null) ? null : ucCommentUploadPanel.commonUploaderModel, ImageIdToContentId);
                }

                else  //COMMENT ON STATUS
                {
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_STATUS;

                    if (newsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel))
                    {
                        pakToSend[JsonKeys.NewsfeedId] = newsfeedId;
                        pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                        pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;
                        pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                        pakToSend[JsonKeys.UserTableID] = feedModel.WallOrContentOwner.UserTableID;

                        if (feedModel.BookPostType == 2 || feedModel.BookPostType == 5 || feedModel.BookPostType == 8)
                        {
                            if (feedModel.SingleMediaFeedModel != null)
                            {
                                pakToSend[JsonKeys.ContentId] = feedModel.SingleMediaFeedModel.ContentId;
                            }
                            else if (feedModel.SingleImageFeedModel != null)
                            {
                                pakToSend[JsonKeys.ContentId] = feedModel.SingleImageFeedModel.ImageId;
                                ImageIdToContentId = true;
                            }
                        }
                    }
                    threadAddOrEditAnyComment = new ThreadAddOrEditAnyComment(pakToSend, (ucCommentUploadPanel == null) ? null : ucCommentUploadPanel.commonUploaderModel, ImageIdToContentId);
                }

                threadAddOrEditAnyComment.callBackEvent += (sucess) =>
                {
                    if (sucess)
                    {

                        if (ucCommentUploadPanel != null && ucCommentUploadPanel.commonUploaderModel != null)
                        {
                            ucCommentUploadPanel.commonUploaderModel.IsUploading = false;
                            if (ucCommentUploadPanel.commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_STICKER && RecentStickerModel != null)
                            {
                                RingMarketStickerLoadUtility.AddRecentSticker(RecentStickerModel);
                                RecentStickerModel = null;
                            }
                            ucCommentUploadPanel.SetAllModelsToNull();
                        }
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            EnablePostbutton(true);
                            richTextBox.Document.Blocks.Clear();
                            richTextFocus();
                        });
                    }
                    else
                    {
                        if (ucCommentUploadPanel != null && ucCommentUploadPanel.commonUploaderModel != null)
                            ucCommentUploadPanel.commonUploaderModel.IsUploading = false;
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            EnablePostbutton(true);
                            UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to Comment! ", "Failed!");
                            richTextFocus();
                        });
                    }
                };
                if (threadAddOrEditAnyComment != null)
                {
                    EnablePostbutton(false);
                    threadAddOrEditAnyComment.StartThread();
                }
            }
        }

        private ICommand _rtbEditCommentTextChangedCommand;
        public ICommand RtbEditCommentTextChangedCommand
        {
            get
            {
                _rtbEditCommentTextChangedCommand = _rtbEditCommentTextChangedCommand ?? new RelayCommand(param => onRtbEditCommentTextChanged(param));
                return _rtbEditCommentTextChangedCommand;
            }
        }

        private ICommand _rtbEditCommentPreviewKeyDownCommand;
        public ICommand RtbEditCommentPreviewKeyDownCommand
        {
            get
            {
                _rtbEditCommentPreviewKeyDownCommand = _rtbEditCommentPreviewKeyDownCommand ?? new RelayCommand(param => onRtbEditCommentPreviewKeyDown(param));
                return _rtbEditCommentPreviewKeyDownCommand;
            }
        }

        private void onRtbEditCommentTextChanged(object param)
        {
            if (param is RichTextFeedEdit)
            {
                string text = richTextBox.Text;
                if (text.Length > 0)
                {

                    writeSmthngTxtBlock.Visibility = Visibility.Collapsed;
                    if (text.EndsWith("@"))
                    {
                        richTextBox.AlphaStarts = text.Length;
                        if (UCAlphaTagPopUp.Instance == null)
                        {
                            UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
                        }
                        if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
                        {
                            Grid g = (Grid)UCAlphaTagPopUp.Instance.Parent;
                            g.Children.Remove(UCAlphaTagPopUp.Instance);
                        }
                        Grid richGrid = (Grid)richTextBox.Parent;
                        richGrid.Children.Add(UCAlphaTagPopUp.Instance);
                        UCAlphaTagPopUp.Instance.InitializePopUpLocation(richTextBox);
                    }
                    else if (richTextBox.AlphaStarts >= 0 && richTextBox.Text.Length > richTextBox.AlphaStarts)
                    {
                        string searchString = richTextBox.Text.Substring(richTextBox.AlphaStarts);
                        UCAlphaTagPopUp.Instance.ViewSearchFriends(searchString);
                    }
                }
                else
                {
                    writeSmthngTxtBlock.Visibility = Visibility.Visible;
                    richTextBox.AlphaStarts = -1;
                    richTextBox._AddedFriendsUtid.Clear();
                }
            }
        }

        private void onRtbEditCommentPreviewKeyDown(object param)
        {
            if (param is KeyEventArgs)
            {
                KeyEventArgs pressedKeyEvent = (KeyEventArgs)param;
                if (pressedKeyEvent.Key == Key.Enter && (UCAlphaTagPopUp.Instance == null || (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen == false)))
                {
                    pressedKeyEvent.Handled = true;
                }

                if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
                {
                    if (pressedKeyEvent.Key == Key.Down)
                    {
                        pressedKeyEvent.Handled = true;
                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Up)
                    {
                        pressedKeyEvent.Handled = true;

                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Enter || pressedKeyEvent.Key == Key.Tab)
                    {
                        pressedKeyEvent.Handled = true;
                        UCAlphaTagPopUp.Instance.SelectFriendTag((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
                    }
                }
                else if (pressedKeyEvent.Key == Key.Return)
                {
                    if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    {
                        pressedKeyEvent.Handled = true;
                        OnCommentPostCommand();
                    }
                    else
                    {
                        richTextBox.AppendText("\n");
                        pressedKeyEvent.Handled = true;
                    }
                }
            }
        }

        private ICommand _gotFocusCommand;

        public ICommand GotFocusCommand
        {
            get
            {
                if (_gotFocusCommand == null)
                {
                    _gotFocusCommand = new RelayCommand(param => OnGotFocusCommand(param));
                }
                return _gotFocusCommand;
            }
        }

        private void OnGotFocusCommand(object param)
        {
            //if (imageId != Guid.Empty && basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.IsVisible)
            //{
            //    basicImageViewWrapper().ucBasicImageView.AddOrRemovePeviewKeyDownHandler(false);
            //}
            //else if (contentId != Guid.Empty && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView != null && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.IsVisible)
            //{
            //    MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.AddOrRemovePeviewKeyDownHandler(false);
            //}
            //commentPostBtn.Click += commentPostBtn_Click;
            //richTextBox.RichTextChanged += richTextBox_RichTextChanged;
            //richTextBox.PreviewKeyDown += richTextBox_PreviewKeyDown;
            //uploadBtn.Click += uploadBtn_Click;
            //emoticonBtn.Click += emoticonBtn_Click;
            if (needTofocus)
            {
                richTextBox.Focusable = true;
                richTextBox.Focus();
            }
        }

        private ICommand _lostFocusCommand;

        public ICommand LostFocusCommand
        {
            get
            {
                if (_lostFocusCommand == null)
                {
                    _lostFocusCommand = new RelayCommand(param => OnLostFocusCOmmand(param));
                }
                return _lostFocusCommand;
            }
        }

        private void OnLostFocusCOmmand(object param)
        {
            //if (View.UI.Feed.UCAllFeeds.Instance != null)
            NewsFeedViewModel.Instance.AllFeedsScrollEnabled = true;
            //if (imageId != Guid.Empty && basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.IsVisible)
            //{
            //    basicImageViewWrapper().ucBasicImageView.AddOrRemovePeviewKeyDownHandler(true);
            //}
            //else if (contentId != Guid.Empty && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView != null && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.IsVisible)
            //{
            //    MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.AddOrRemovePeviewKeyDownHandler(true);
            //}
        }
        #endregion
    }
}
=======
﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.ImageViewer;
using View.UI.PopUp;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingMarket;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using System.Linq;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCNewCommentPanel.xaml
    /// </summary>
    public partial class UCNewCommentPanel : UserControl, INotifyPropertyChanged
    {
        #region
        private bool needToFocus = true;
        public Grid motherPanel;
        #endregion

        #region "Constructor"
        public UCNewCommentPanel()
        {
            InitializeComponent();
        }
        #endregion

        #region "Property"
        public Guid newsfeedId, contentId, imageId;

        private MarkertStickerImagesModel RecentStickerModel;
        private UCCommentUploadPanel ucCommentUploadPanel;

        private bool _UploadPopup;
        public bool UploadPopup
        {
            get { return _UploadPopup; }
            set
            {
                if (value == _UploadPopup)
                    return;
                _UploadPopup = value;
                this.OnPropertyChanged("UploadPopup");
            }
        }
        private bool _FocusNewComment;
        public bool FocusNewComment
        {
            get { return _FocusNewComment; }
            set
            {
                if (value == true)
                {
                    NewsFeedViewModel.Instance.AllFeedsScrollEnabled = false;
                    richTextBox.Focusable = true;
                    richTextBox.Focus();
                }
                if (value == _FocusNewComment)
                    return;
                _FocusNewComment = value;
            }
        }

        private UCCommentUploadPopup ucCommentUploadPopup { get; set; }
        #endregion

        #region "OnPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Method"
        private void EnablePostButton(bool flag)
        {
            richTextBox.IsEnabled = flag;
            commentPostBtn.IsEnabled = flag;
            emoticonBtn.IsEnabled = flag;
            uploadBtn.IsEnabled = flag;

            if (ucCommentUploadPanel != null) ucCommentUploadPanel.RemoveButtonEnabled = flag;
        }

        private void AddUploadPanel()
        {
            if (ucCommentUploadPanel == null)
            {
                ucCommentUploadPanel = new UCCommentUploadPanel((type) =>
                {
                    int CancelType = type;
                    if (CancelType == 1)
                    {
                        richTextBox.Focusable = true;
                        richTextBox.Focus();
                    }
                    return 0;
                });
            }
            else if (ucCommentUploadPanel.Parent is Border) ((Border)ucCommentUploadPanel.Parent).Child = null;
            ucCommentUploadPanel.motherPanel = motherPanel;
            _uploadPanel.Child = ucCommentUploadPanel;
        }

        public void SetNeedToFocus(bool focus)
        {
            needToFocus = focus;
        }

        #endregion

        #region "Event Trigger"
        private ICommand _UploadButtonCommand;
        public ICommand UploadButtonCommand
        {
            get
            {
                if (_UploadButtonCommand == null)
                {
                    _UploadButtonCommand = new RelayCommand(param => OnUploadButtonCommand(param));
                }
                return _UploadButtonCommand;
            }
        }

        private void OnUploadButtonCommand(object parameter)
        {
            UploadPopup = true;
            if (ucCommentUploadPopup != null)
                motherPanel.Children.Remove(ucCommentUploadPopup);
            ucCommentUploadPopup = new UCCommentUploadPopup(motherPanel);
            ucCommentUploadPopup.Show();
            ucCommentUploadPopup.ShowPopup(uploadBtn, (type) =>
            {
                int uploadType = type;
                AddUploadPanel();

                if (uploadType == 1)
                {
                    ucCommentUploadPanel.ImageSelectToUpload();
                }
                else if (uploadType == 2)
                {
                    ucCommentUploadPanel.audioSelectToUpload();
                }
                else if (uploadType == 3)
                {
                    ucCommentUploadPanel.videoSelectToUpload();
                }
                else if (uploadType == 4)
                {
                    ucCommentUploadPanel.webCamToUpload();
                }
                else if (uploadType == 5)
                {
                    ucCommentUploadPanel.recordToUpload();
                }
                UploadPopup = false;
                richTextBox.Focusable = true;
                richTextBox.Focus();
                return 0;
            });
        }

        private ICommand _EmoticonButtonCommand;
        public ICommand EmoticonButtonCommand
        {
            get
            {
                if (_EmoticonButtonCommand == null)
                {
                    _EmoticonButtonCommand = new RelayCommand(param => OnEmoticonButtonCommand(param));
                }
                return _EmoticonButtonCommand;
            }
        }

        private void OnEmoticonButtonCommand(object parameter)
        {
            UIElement uiElement = (UIElement)emoticonBtn;
            MainSwitcher.PopupController.StickerPopUPInComments.Show(uiElement, UCEmoticonPopup.TYPE_DEFAULT, motherPanel, (obj) =>
            {
                if (obj is string)
                {
                    string emoticonStr = (string)obj;
                    if (!String.IsNullOrWhiteSpace(emoticonStr))
                    {
                        richTextBox.AppendText(emoticonStr + " ");
                    }
                }
                else if (obj is MarkertStickerImagesModel)
                {
                    RecentStickerModel = (MarkertStickerImagesModel)obj;
                    AddUploadPanel();
                    ucCommentUploadPanel.stickerSelectedToUpload(RecentStickerModel.ToString());
                    if (string.IsNullOrWhiteSpace(richTextBox.Text.Trim()))
                        OnCommentPostCommand();
                    else
                    {
                        richTextBox.Focusable = true;
                        richTextBox.Focus();
                    }
                }
                return 0;
            });

        }
        
        private ICommand _CommentPostCommand;
        public ICommand CommentPostCommand
        {
            get
            {
                if (_CommentPostCommand == null)
                {
                    _CommentPostCommand = new RelayCommand(param => OnCommentPostCommand());
                }
                return _CommentPostCommand;
            }
        }

        private void richTextFocus()
        {
            richTextBox.Focus();
            richTextBox.Focusable = true;
        }

        private void OnCommentPostCommand()
        {
            if (string.IsNullOrWhiteSpace(richTextBox.Text.Trim()) && (ucCommentUploadPanel == null || ucCommentUploadPanel.CommonUploaderModel == null))
            {
                //CustomMessageBox.ShowInformation(NotificationMessages.NOTHING_TO_POST);
                UIHelperMethods.ShowWarning(NotificationMessages.NOTHING_TO_POST);
            }
            else
            {
                richTextBox.SetStatusandTags();
                FeedModel feedModel = null;
                bool ImageIdToContentId = false;
                ThreadAddOrEditAnyComment threadAddOrEditAnyComment = null;
                JObject pakToSend = new JObject();

                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_STATUS_COMMENT;

                if (richTextBox.TagsJArray != null) pakToSend[JsonKeys.CommentTagList] = richTextBox.TagsJArray;
                pakToSend[JsonKeys.Comment] = (richTextBox.TagsJArray != null) ? richTextBox.StringWithoutTags : richTextBox.StringWithoutTags.Trim();

                if (imageId != Guid.Empty) //COMMENT ON IMAGE
                {
                    ImageModel imageModel = null;
                    ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(imageId, out imageModel);
                    bool fromFeed = (ViewConstants.NavigateFrom == StatusConstants.NAVIGATE_FROM_FEED) ? true : false;
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_IMAGE;
                    pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                    pakToSend[JsonKeys.ContentId] = imageModel.ImageId;
                    ImageIdToContentId = true;

                    if (fromFeed)
                    {
                        pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                        if (imageModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(imageModel.NewsFeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.NewsfeedId] = imageModel.NewsFeedId;
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;
                            pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            pakToSend[JsonKeys.UserTableID] = feedModel.WallOrContentOwner.UserTableID;
                        }
                    }
                    else
                    {
                        pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                        pakToSend[JsonKeys.AlbumId] = imageModel.AlbumId;
                        if (imageModel.Privacy != 0) pakToSend[JsonKeys.Privacy] = imageModel.Privacy;
                        else pakToSend[JsonKeys.Privacy] = SettingsConstants.PRIVACY_PUBLIC;
                        pakToSend[JsonKeys.UserTableID] = imageModel.UserShortInfoModel.UserTableID;
                    }
                }

                else if (contentId != Guid.Empty) //COMMENT ON MULTIMEDIA
                {
                    SingleMediaModel singleMediaModel = null;
                    MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel);
                    bool fromFeed = singleMediaModel.PlayedFromFeed ? true : false;
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_MULTIMEDIA;
                    pakToSend[JsonKeys.ContentId] = singleMediaModel.ContentId;
                    pakToSend[JsonKeys.MediaType] = singleMediaModel.MediaType;

                    if (fromFeed)
                    {
                        if (singleMediaModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(singleMediaModel.NewsFeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            pakToSend[JsonKeys.NewsfeedId] = singleMediaModel.NewsFeedId;
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;
                            pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            pakToSend[JsonKeys.UserTableID] = feedModel.WallOrContentOwner.UserTableID;
                        }
                    }
                    else
                    {
                        pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                        pakToSend[JsonKeys.AlbumId] = singleMediaModel.AlbumId;
                        if (singleMediaModel.Privacy != 0) pakToSend[JsonKeys.Privacy] = singleMediaModel.Privacy;
                        else pakToSend[JsonKeys.Privacy] = SettingsConstants.PRIVACY_PUBLIC;
                        pakToSend[JsonKeys.UserTableID] = singleMediaModel.MediaOwner.UserTableID;
                    }
                }

                else  //COMMENT ON STATUS
                {
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_STATUS;

                    if (newsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel))
                    {
                        pakToSend[JsonKeys.NewsfeedId] = newsfeedId;
                        pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                        pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;
                        pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                        pakToSend[JsonKeys.UserTableID] = feedModel.WallOrContentOwner.UserTableID;

                        if (feedModel.BookPostType == 2 || feedModel.BookPostType == 5 || feedModel.BookPostType == 8)
                        {
                            if (feedModel.SingleMediaFeedModel != null)
                            {
                                pakToSend[JsonKeys.ContentId] = feedModel.SingleMediaFeedModel.ContentId;
                            }
                            else if (feedModel.SingleImageFeedModel != null)
                            {
                                pakToSend[JsonKeys.ContentId] = feedModel.SingleImageFeedModel.ImageId;
                                ImageIdToContentId = true;
                            }
                        }
                    }
                }
                threadAddOrEditAnyComment = new ThreadAddOrEditAnyComment(pakToSend, (ucCommentUploadPanel == null) ? null : ucCommentUploadPanel.CommonUploaderModel, ImageIdToContentId);

                threadAddOrEditAnyComment.callBackEvent += (success) =>
                {
                    if (success)
                    {

                        if (ucCommentUploadPanel != null && ucCommentUploadPanel.CommonUploaderModel != null)
                        {
                            ucCommentUploadPanel.CommonUploaderModel.IsUploading = false;
                            if (ucCommentUploadPanel.CommonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_STICKER && RecentStickerModel != null)
                            {
                                RingMarketStickerLoadUtility.AddRecentSticker(RecentStickerModel);
                                RecentStickerModel = null;
                            }
                            ucCommentUploadPanel.SetAllModelsToNull();
                        }
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            EnablePostButton(true);
                            richTextBox.Document.Blocks.Clear();
                            richTextFocus();
                        });
                    }
                    else
                    {
                        if (ucCommentUploadPanel != null && ucCommentUploadPanel.CommonUploaderModel != null)
                            ucCommentUploadPanel.CommonUploaderModel.IsUploading = false;
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            EnablePostButton(true);
                            UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to Comment! ", "Failed!");
                            richTextFocus();
                        });
                    }
                };
                if (threadAddOrEditAnyComment != null)
                {
                    EnablePostButton(false);
                    threadAddOrEditAnyComment.StartThread();
                }
            }
        }

        private ICommand _rtbEditCommentTextChangedCommand;
        public ICommand RtbEditCommentTextChangedCommand
        {
            get
            {
                _rtbEditCommentTextChangedCommand = _rtbEditCommentTextChangedCommand ?? new RelayCommand(param => onRtbEditCommentTextChanged(param));
                return _rtbEditCommentTextChangedCommand;
            }
        }

        private void onRtbEditCommentTextChanged(object parameter)
        {
            if (parameter is RichTextFeedEdit)
            {
                string text = richTextBox.Text;
                if (text.Length > 0)
                {

                    writeSomethingTextBlock.Visibility = Visibility.Collapsed;
                    if (text.EndsWith("@"))
                    {
                        richTextBox.AlphaStarts = text.Length;
                        if (UCAlphaTagPopUp.Instance == null)
                        {
                            UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
                        }
                        if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
                        {
                            Grid grid = (Grid)UCAlphaTagPopUp.Instance.Parent;
                            grid.Children.Remove(UCAlphaTagPopUp.Instance);
                        }
                        Grid richGrid = (Grid)richTextBox.Parent;
                        richGrid.Children.Add(UCAlphaTagPopUp.Instance);
                        UCAlphaTagPopUp.Instance.InitializePopUpLocation(richTextBox);
                    }
                    else if (richTextBox.AlphaStarts >= 0 && richTextBox.Text.Length > richTextBox.AlphaStarts)
                    {
                        string searchString = richTextBox.Text.Substring(richTextBox.AlphaStarts);
                        UCAlphaTagPopUp.Instance.ViewSearchFriends(searchString);
                    }
                }
                else
                {
                    writeSomethingTextBlock.Visibility = Visibility.Visible;
                    richTextBox.AlphaStarts = -1;
                    richTextBox._AddedFriendsUtid.Clear();
                }
            }
        }

        private ICommand _rtbEditCommentPreviewKeyDownCommand;
        public ICommand RtbEditCommentPreviewKeyDownCommand
        {
            get
            {
                _rtbEditCommentPreviewKeyDownCommand = _rtbEditCommentPreviewKeyDownCommand ?? new RelayCommand(param => onRtbEditCommentPreviewKeyDown(param));
                return _rtbEditCommentPreviewKeyDownCommand;
            }
        }

        private void onRtbEditCommentPreviewKeyDown(object parameter)
        {
            if (parameter is KeyEventArgs)
            {
                KeyEventArgs pressedKeyEvent = (KeyEventArgs)parameter;
                if (pressedKeyEvent.Key == Key.Enter && (UCAlphaTagPopUp.Instance == null || (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen == false)))
                {
                    pressedKeyEvent.Handled = true;
                }

                if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
                {
                    if (pressedKeyEvent.Key == Key.Down)
                    {
                        pressedKeyEvent.Handled = true;
                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Up)
                    {
                        pressedKeyEvent.Handled = true;

                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Enter || pressedKeyEvent.Key == Key.Tab)
                    {
                        pressedKeyEvent.Handled = true;
                        UCAlphaTagPopUp.Instance.SelectFriendTag((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
                    }
                }
                else if (pressedKeyEvent.Key == Key.Return)
                {
                    if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    {
                        pressedKeyEvent.Handled = true;
                        OnCommentPostCommand();
                    }
                    else
                    {
                        richTextBox.AppendText("\n");
                        pressedKeyEvent.Handled = true;
                    }
                }
            }
        }

        private ICommand _gotFocusCommand;
        public ICommand GotFocusCommand
        {
            get
            {
                if (_gotFocusCommand == null)
                {
                    _gotFocusCommand = new RelayCommand(param => OnGotFocusCommand(param));
                }
                return _gotFocusCommand;
            }
        }

        private void OnGotFocusCommand(object parameter)
        {
            if (needToFocus)
            {
                richTextBox.Focusable = true;
                richTextBox.Focus();
            }
        }

        private ICommand _lostFocusCommand;
        public ICommand LostFocusCommand
        {
            get
            {
                if (_lostFocusCommand == null)
                {
                    _lostFocusCommand = new RelayCommand(param => OnLostFocusCommand(param));
                }
                return _lostFocusCommand;
            }
        }

        private void OnLostFocusCommand(object parameter)
        {
            NewsFeedViewModel.Instance.AllFeedsScrollEnabled = true;
        }
        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

<<<<<<< HEAD
﻿using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingMarket;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCEditCommentPanel.xaml
    /// </summary>
    public partial class UCEditCommentPanel : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCEditCommentPanel).Name);

        public Grid motherPanel;

        #region "Constructor"
        public UCEditCommentPanel()
        {
            InitializeComponent();
            //if (((CommentModel)this.DataContext).Comment.Length > 0) TextBlockVisibility = Visibility.Visible;
            //this.DataContext = this;
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Properties"
        private MarkertStickerImagesModel RecentStickerModel;
        //private UCCommentUploadPanel ucCommentUploadPanel;
        private UCCommentUploadPanel _ucCommentUploadPanel;
        public UCCommentUploadPanel ucCommentUploadPanel
        {
            get
            {
                return _ucCommentUploadPanel;
            }
            set
            {
                _ucCommentUploadPanel = value;
                this.OnPropertyChanged("ucCommentUploadPanel");
            }
        }

        public Func<int> _OnEditCompletion = null;
        public CommentModel commentModel;

        private Visibility _TextBlockVisibility = Visibility.Collapsed;
        public Visibility TextBlockVisibility
        {
            get { return _TextBlockVisibility; }
            set
            {
                if (value == _TextBlockVisibility)
                    return;

                _TextBlockVisibility = value;
                this.OnPropertyChanged("TextBlockVisibility");
            }
        }

        private Visibility _uploadBtnVisibility = Visibility.Hidden;
        public Visibility UploadBtnVisibility
        {
            get { return _uploadBtnVisibility; }
            set
            {
                if (value == _uploadBtnVisibility)
                    return;

                _uploadBtnVisibility = value;
                this.OnPropertyChanged("UploadBtnVisibility");
            }
        }

        private bool _UploadPopup;
        public bool UploadPopup
        {
            get { return _UploadPopup; }
            set
            {
                if (value == _UploadPopup)
                    return;
                _UploadPopup = value;
                this.OnPropertyChanged("UploadPopup");
            }
        }

        private UCCommentUploadPopup ucCommentUploadPopup { get; set; }
        #endregion

        #region "Methods"
        private void addUploadPanel()
        {
            if (ucCommentUploadPanel == null)
            {
                ucCommentUploadPanel = new UCCommentUploadPanel((type) =>
                {
                    int CancelType = type;
                    if (CancelType == 1)
                    {
                        richTextBox.Focusable = true;
                        richTextBox.Focus();
                    }
                    return 0;
                });
            }
            else if (ucCommentUploadPanel.Parent is Border) ((Border)ucCommentUploadPanel.Parent).Child = null;
            ucCommentUploadPanel.IsEditMode = true;
            _uploadPanel.Child = ucCommentUploadPanel;
        }

        public void SetRichTextBoxFieldsandUploaderModel()
        {
            //richTextBox.Document.Blocks.Clear();
            richTextBox.AppendText(commentModel.Comment);
            TextBlockVisibility = string.IsNullOrEmpty(commentModel.Comment) ? Visibility.Visible : Visibility.Collapsed;
            if (commentModel.TaggedUsers != null && commentModel.TaggedUsers.Count > 0)
            {
                richTextBox.TagsHandle(commentModel.TaggedUsers);
                foreach (var item in commentModel.TaggedUsers)
                {
                    richTextBox._AddedFriendsUtid.Add(item.UserTableID);
                }
            }
            UploadBtnVisibility = Visibility.Visible;
            if (commentModel.UrlType > 0)
            {
                addUploadPanel();
                ucCommentUploadPanel.commonUploaderModel = new CommonUploaderModel { UploadType = commentModel.UrlType, UploadedUrl = commentModel.MediaOrImageUrl, IsUploadedInServer = true };
                if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
                {
                    string convertedUrl = HelperMethods.ConvertUrlToName(this.commentModel.MediaOrImageUrl, ImageUtility.IMG_300);
                    ucCommentUploadPanel.commonUploaderModel.FilePath = RingIDSettings.TEMP_COMMENT_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl.Replace("/", "_");
                }
                else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_STICKER)
                {
                    ucCommentUploadPanel.commonUploaderModel.FilePath = @RingIDSettings.STICKER_FOLDER + System.IO.Path.DirectorySeparatorChar + (commentModel.MediaOrImageUrl).Replace(System.IO.Path.AltDirectorySeparatorChar, System.IO.Path.DirectorySeparatorChar);
                }
                else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC || commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
                {
                    if (!string.IsNullOrEmpty(commentModel.ThumbUrl))
                    {
                        ucCommentUploadPanel.commonUploaderModel.UploadedThumbUrl = commentModel.ThumbUrl;
                        ucCommentUploadPanel.commonUploaderModel.IsThumbImageFound = true;
                    }
                    ucCommentUploadPanel.commonUploaderModel.FileDuration = this.commentModel.Duration;
                }
            }
        }

        private bool CommentImageOrMediaChanged()
        {
            bool changed = false;
            if (ucCommentUploadPanel != null && ucCommentUploadPanel.commonUploaderModel != null)
            {
                if (string.IsNullOrEmpty(commentModel.MediaOrImageUrl) || string.IsNullOrEmpty(ucCommentUploadPanel.commonUploaderModel.UploadedUrl)) changed = true;
                else if (!commentModel.MediaOrImageUrl.Equals(ucCommentUploadPanel.commonUploaderModel.UploadedUrl)) changed = true;
            }
            else if (!string.IsNullOrEmpty(commentModel.MediaOrImageUrl))
                changed = true;

            return changed;
        }

        private void EnablePostbutton(bool flag)
        {
            emoticonBtn.IsEnabled = flag;
            uploadBtn.IsEnabled = flag;
            richTextBox.IsEnabled = flag;

            if (ucCommentUploadPanel != null) ucCommentUploadPanel.RemoveButtonEnabled = flag;
        }

        private void action_edit_feed()
        {
            richTextBox.SetStatusandTags();
            if (!richTextBox.StringWithoutTags.Equals(commentModel.Comment) || !HelperMethods.IsTagDTOsSameasJArray(richTextBox.TagsJArray, commentModel.TaggedUsers) || CommentImageOrMediaChanged())
            {
                if (string.IsNullOrEmpty(richTextBox.StringWithoutTags) && (ucCommentUploadPanel == null || ucCommentUploadPanel.commonUploaderModel == null))
                {
                    // CustomMessageBox.ShowInformation(NotificationMessages.NOTHING_TO_POST);
                    UIHelperMethods.ShowWarning(NotificationMessages.NOTHING_TO_POST);
                }
                else
                {
                    FeedModel feedModel = null;
                    bool ImgToContentID = false;
                    ThreadAddOrEditAnyComment threadAddOrEditAnyComment = null;
                    JObject pakToSend = new JObject();
                    if (richTextBox.TagsJArray != null)
                        pakToSend[JsonKeys.CommentTagList] = richTextBox.TagsJArray;

                    if (richTextBox.StringWithoutTags.Trim() != null)
                        pakToSend[JsonKeys.Comment] = richTextBox.StringWithoutTags.Trim();
                    else
                        pakToSend[JsonKeys.Comment] = string.Empty;

                    pakToSend[JsonKeys.CommentId] = commentModel.CommentId;

                    if (commentModel.ImageId != Guid.Empty)   // EDIT COMMENT ON IMAGE
                    {
                        ImageModel imageModel = null;
                        ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(commentModel.ImageId, out imageModel);
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_IMAGE_COMMENT;
                        pakToSend[JsonKeys.ImageId] = imageModel.ImageId;
                        pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                        pakToSend[JsonKeys.AlbumId] = imageModel.AlbumId;
                        pakToSend[JsonKeys.NewsfeedId] = imageModel.NewsFeedId;

                        threadAddOrEditAnyComment = new ThreadAddOrEditAnyComment(pakToSend, (ucCommentUploadPanel == null) ? null : ucCommentUploadPanel.commonUploaderModel, ImgToContentID);
                    }

                    else if (commentModel.ContentId != Guid.Empty)   // EDIT COMMENT ON MULTIMEDIA
                    {
                        SingleMediaModel singleMediaModel = null;
                        if (MediaDataContainer.Instance.ContentModels.TryGetValue(commentModel.ContentId, out singleMediaModel))
                        {
                            pakToSend[JsonKeys.ContentId] = singleMediaModel.ContentId;
                            pakToSend[JsonKeys.MediaType] = singleMediaModel.MediaType;
                            pakToSend[JsonKeys.AlbumId] = singleMediaModel.AlbumId;
                            pakToSend[JsonKeys.NewsfeedId] = singleMediaModel.NewsFeedId;
                        }
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_COMMENT_ON_MEDIA;
                        threadAddOrEditAnyComment = new ThreadAddOrEditAnyComment(pakToSend, (ucCommentUploadPanel == null) ? null : ucCommentUploadPanel.commonUploaderModel, ImgToContentID);
                    }

                    else   // EDIT COMMENT ON FEED
                    {
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_STATUS_COMMENT;
                        if (commentModel.NewsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(commentModel.NewsfeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.NewsfeedId] = commentModel.NewsfeedId;
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;

                            if (feedModel.BookPostType == 1 || feedModel.BookPostType == 4 || feedModel.BookPostType == 7 || feedModel.BookPostType == 10)
                            {

                            }
                            else
                            {
                                if (feedModel.SingleMediaFeedModel != null)
                                {
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleMediaFeedModel.ContentId;
                                }
                                else if (feedModel.SingleImageFeedModel != null)
                                {
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleImageFeedModel.ImageId;
                                    ImgToContentID = true;
                                }
                            }
                        }
                        threadAddOrEditAnyComment = new ThreadAddOrEditAnyComment(pakToSend, (ucCommentUploadPanel == null) ? null : ucCommentUploadPanel.commonUploaderModel, ImgToContentID);
                    }

                    threadAddOrEditAnyComment.callBackEvent += (sucess) =>
                    {
                        if (sucess)
                        {
                            if (ucCommentUploadPanel != null && ucCommentUploadPanel.commonUploaderModel != null)
                            {
                                ucCommentUploadPanel.commonUploaderModel.IsUploading = false;
                                if (ucCommentUploadPanel.commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_STICKER && RecentStickerModel != null)
                                {
                                    RingMarketStickerLoadUtility.AddRecentSticker(RecentStickerModel);
                                    RecentStickerModel = null;
                                }
                                ucCommentUploadPanel.SetAllModelsToNull();
                            }
                            if (_OnEditCompletion != null)
                            {
                                _OnEditCompletion();
                            }
                            //Application.Current.Dispatcher.Invoke(() =>
                            //{
                            //    EnablePostbutton(true);
                            //    richTextBox.Document.Blocks.Clear();
                            //});
                        }
                        else
                        {
                            if (ucCommentUploadPanel != null && ucCommentUploadPanel.commonUploaderModel != null)
                                ucCommentUploadPanel.commonUploaderModel.IsUploading = false;
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                EnablePostbutton(true);
                                UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to Edit Comment! ", "Failed!");
                            });
                        }
                    };
                    if (threadAddOrEditAnyComment != null)
                    {
                        EnablePostbutton(false);
                        threadAddOrEditAnyComment.StartThread();
                    }
                }
            }
            //}
            else
                UIHelperMethods.ShowWarning(NotificationMessages.ABOUT_NO_CHANGE, "Edit feed");
            //{
            //    CustomMessageBox.ShowWarning("No change!");
            //}
        }
        #endregion

        #region "Handlers"
        private ICommand _UploadButtonCommand;
        public ICommand UploadButtonCommand
        {
            get
            {
                if (_UploadButtonCommand == null)
                {
                    _UploadButtonCommand = new RelayCommand(param => OnUploadButtonCommand(param));
                }
                return _UploadButtonCommand;
            }
        }
        private void OnUploadButtonCommand(object parameter)
        {
            UploadPopup = true;
            if (ucCommentUploadPopup != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(ucCommentUploadPopup);
            ucCommentUploadPopup = new UCCommentUploadPopup(UCGuiRingID.Instance.MotherPanel);
            ucCommentUploadPopup.Show();
            ucCommentUploadPopup.ShowPopup(uploadBtn, (type) =>
            {
                int upType = type;
                addUploadPanel();

                if (upType == 1)
                {
                    ucCommentUploadPanel.imageSelectToUpload();
                }
                else if (upType == 2)
                {
                    ucCommentUploadPanel.audioSelectToUpload();
                }
                else if (upType == 3)
                {
                    ucCommentUploadPanel.videoSelectToUpload();
                }
                UploadPopup = false;
                richTextBox.Focusable = true;
                richTextBox.Focus();
                return 0;
            });
        }
        private ICommand _EmoticonButtonCommand;
        public ICommand EmoticonButtonCommand
        {
            get
            {
                if (_EmoticonButtonCommand == null)
                {
                    _EmoticonButtonCommand = new RelayCommand(param => OnEmoticonButtonCommand(param));
                }
                return _EmoticonButtonCommand;
            }
        }
        private void OnEmoticonButtonCommand(object parameter)
        {
            UIElement uiElement = (UIElement)emoticonBtn;
            MainSwitcher.PopupController.StickerPopUPInComments.Show(uiElement, UCEmoticonPopup.TYPE_DEFAULT, motherPanel, (obj) =>
            {
                if (obj is string)
                {
                    string emoticonStr = (string)obj;
                    if (!String.IsNullOrWhiteSpace(emoticonStr))
                    {
                        richTextBox.AppendText(emoticonStr);
                    }
                }
                else if (obj is MarkertStickerImagesModel)
                {
                    RecentStickerModel = (MarkertStickerImagesModel)obj;
                    addUploadPanel();
                    ucCommentUploadPanel.stickerSelectedToUpload(RecentStickerModel.ToString());
                    UploadBtnVisibility = Visibility.Hidden;
                    richTextBox.Focusable = true;
                    richTextBox.Focus();
                }
                return 0;
            });

        }
        private ICommand _rtbEditCommentTextChangedCommand;
        public ICommand RtbEditCommentTextChangedCommand
        {
            get
            {
                _rtbEditCommentTextChangedCommand = _rtbEditCommentTextChangedCommand ?? new RelayCommand(param => onRtbEditCommentTextChanged(param));
                return _rtbEditCommentTextChangedCommand;
            }
        }

        private ICommand _rtbEditCommentPreviewKeyDownCommand;
        public ICommand RtbEditCommentPreviewKeyDownCommand
        {
            get
            {
                _rtbEditCommentPreviewKeyDownCommand = _rtbEditCommentPreviewKeyDownCommand ?? new RelayCommand(param => onRtbEditCommentPreviewKeyDown(param));
                return _rtbEditCommentPreviewKeyDownCommand;
            }
        }

        private void onRtbEditCommentTextChanged(object param)
        {
            if (param is RichTextFeedEdit)
            {
                string text = richTextBox.Text;
                if (text.Length > 0)
                {

                    TextBlockVisibility = Visibility.Collapsed;
                    if (text.EndsWith("@"))
                    {
                        richTextBox.AlphaStarts = text.Length;
                        if (UCAlphaTagPopUp.Instance == null)
                        {
                            UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
                        }
                        if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
                        {
                            Grid g = (Grid)UCAlphaTagPopUp.Instance.Parent;
                            g.Children.Remove(UCAlphaTagPopUp.Instance);
                        }
                        Grid richGrid = (Grid)richTextBox.Parent;
                        richGrid.Children.Add(UCAlphaTagPopUp.Instance);
                        UCAlphaTagPopUp.Instance.InitializePopUpLocation(richTextBox);
                    }
                    else if (richTextBox.AlphaStarts >= 0 && richTextBox.Text.Length > richTextBox.AlphaStarts)
                    {
                        string searchString = richTextBox.Text.Substring(richTextBox.AlphaStarts);
                        UCAlphaTagPopUp.Instance.ViewSearchFriends(searchString);
                    }
                }
                else
                {
                    TextBlockVisibility = Visibility.Visible;
                    richTextBox.AlphaStarts = -1;
                    richTextBox._AddedFriendsUtid.Clear();
                }
            }
        }

        private void onRtbEditCommentPreviewKeyDown(object param)
        {
            if (param is KeyEventArgs)
            {
                KeyEventArgs pressedKeyEvent = (KeyEventArgs)param;
                if (pressedKeyEvent.Key == Key.Enter && (UCAlphaTagPopUp.Instance == null || (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen == false)))
                {
                    //richTextBox.AppendText("\n");
                    pressedKeyEvent.Handled = true;
                }

                if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
                {
                    if (pressedKeyEvent.Key == Key.Down)
                    {
                        pressedKeyEvent.Handled = true;
                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Up)
                    {
                        pressedKeyEvent.Handled = true;

                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Enter || pressedKeyEvent.Key == Key.Tab)
                    {
                        pressedKeyEvent.Handled = true;
                        UCAlphaTagPopUp.Instance.SelectFriendTag((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
                    }
                }
                else if (pressedKeyEvent.Key == Key.Return)
                {
                    if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    {
                        pressedKeyEvent.Handled = true;
                        action_edit_feed();
                    }
                }
                else if (pressedKeyEvent.Key == Key.Escape)
                {
                    if (_OnEditCompletion != null)
                    {
                        _OnEditCompletion();
                    }
                }

            }
        }

        private ICommand _gotFocusCommand;

        public ICommand GotFocusCommand
        {
            get
            {
                if (_gotFocusCommand == null)
                {
                    _gotFocusCommand = new RelayCommand(param => OnGotFocusCommand(param));
                }
                return _gotFocusCommand;
            }
        }

        private void OnGotFocusCommand(object param)
        {
            //if (commentModel.ImageId != Guid.Empty && basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.IsVisible)
            //{
            //    basicImageViewWrapper().ucBasicImageView.AddOrRemovePeviewKeyDownHandler(false);
            //}
            //else if (commentModel.ContentId != Guid.Empty && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView != null && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.IsVisible)
            //{
            //    MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.AddOrRemovePeviewKeyDownHandler(false);
            //}
        }

        private ICommand _lostFocusCommand;
        public ICommand LostFocusCommand
        {
            get
            {
                if (_lostFocusCommand == null)
                {
                    _lostFocusCommand = new RelayCommand(param => OnLostFocusCommand(param));
                }
                return _lostFocusCommand;
            }
        }

        private void OnLostFocusCommand(object param)
        {
            //if (commentModel.ImageId != Guid.Empty && basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.IsVisible)
            //{
            //    basicImageViewWrapper().ucBasicImageView.AddOrRemovePeviewKeyDownHandler(true);
            //}
            //else if (commentModel.ContentId != Guid.Empty && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView != null && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.IsVisible)
            //{
            //    MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.AddOrRemovePeviewKeyDownHandler(true);
            //}
        }

        private ICommand _CancelCommand;

        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand(param => onCancelEditCommandClicked());
                }
                return _CancelCommand;
            }
        }
        private void onCancelEditCommandClicked()
        {
            if (_OnEditCompletion != null)
            {
                _OnEditCompletion();
            }
        }
        #endregion
    }
=======
﻿using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingMarket;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCEditCommentPanel.xaml
    /// </summary>
    public partial class UCEditCommentPanel : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCEditCommentPanel).Name);

        public Grid motherPanel;

        #region "Constructor"
        public UCEditCommentPanel()
        {
            InitializeComponent();
            //if (((CommentModel)this.DataContext).Comment.Length > 0) TextBlockVisibility = Visibility.Visible;
            //this.DataContext = this;
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Properties"
        private MarkertStickerImagesModel RecentStickerModel;
        //private UCCommentUploadPanel ucCommentUploadPanel;
        private UCCommentUploadPanel _ucCommentUploadPanel;
        public UCCommentUploadPanel ucCommentUploadPanel
        {
            get
            {
                return _ucCommentUploadPanel;
            }
            set
            {
                _ucCommentUploadPanel = value;
                this.OnPropertyChanged("ucCommentUploadPanel");
            }
        }

        public Func<int> _OnEditCompletion = null;
        public CommentModel commentModel;

        private Visibility _TextBlockVisibility = Visibility.Collapsed;
        public Visibility TextBlockVisibility
        {
            get { return _TextBlockVisibility; }
            set
            {
                if (value == _TextBlockVisibility)
                    return;

                _TextBlockVisibility = value;
                this.OnPropertyChanged("TextBlockVisibility");
            }
        }

        private Visibility _uploadBtnVisibility = Visibility.Hidden;
        public Visibility UploadBtnVisibility
        {
            get { return _uploadBtnVisibility; }
            set
            {
                if (value == _uploadBtnVisibility)
                    return;

                _uploadBtnVisibility = value;
                this.OnPropertyChanged("UploadBtnVisibility");
            }
        }

        private bool _UploadPopup;
        public bool UploadPopup
        {
            get { return _UploadPopup; }
            set
            {
                if (value == _UploadPopup)
                    return;
                _UploadPopup = value;
                this.OnPropertyChanged("UploadPopup");
            }
        }

        private UCCommentUploadPopup ucCommentUploadPopup { get; set; }
        #endregion

        #region "Methods"
        private void addUploadPanel()
        {
            if (ucCommentUploadPanel == null)
            {
                ucCommentUploadPanel = new UCCommentUploadPanel((type) =>
                {
                    int CancelType = type;
                    if (CancelType == 1)
                    {
                        richTextBox.Focusable = true;
                        richTextBox.Focus();
                    }
                    return 0;
                });
            }
            else if (ucCommentUploadPanel.Parent is Border) ((Border)ucCommentUploadPanel.Parent).Child = null;
            ucCommentUploadPanel.IsEditMode = true;
            _uploadPanel.Child = ucCommentUploadPanel;
        }

        public void SetRichTextBoxFieldsAndUploaderModel()
        {
            //richTextBox.Document.Blocks.Clear();
            richTextBox.AppendText(commentModel.Comment);
            TextBlockVisibility = string.IsNullOrEmpty(commentModel.Comment) ? Visibility.Visible : Visibility.Collapsed;
            if (commentModel.TaggedUsers != null && commentModel.TaggedUsers.Count > 0)
            {
                richTextBox.TagsHandle(commentModel.TaggedUsers);
                foreach (var item in commentModel.TaggedUsers)
                {
                    richTextBox._AddedFriendsUtid.Add(item.UserTableID);
                }
            }
            UploadBtnVisibility = Visibility.Visible;
            if (commentModel.UrlType > 0)
            {
                addUploadPanel();
                ucCommentUploadPanel.CommonUploaderModel = new CommonUploaderModel { UploadType = commentModel.UrlType, UploadedUrl = commentModel.MediaOrImageUrl, IsUploadedInServer = true };
                if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
                {
                    string convertedUrl = HelperMethods.ConvertUrlToName(this.commentModel.MediaOrImageUrl, ImageUtility.IMG_300);
                    ucCommentUploadPanel.CommonUploaderModel.FilePath = RingIDSettings.TEMP_COMMENT_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl.Replace("/", "_");
                }
                else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_STICKER)
                {
                    ucCommentUploadPanel.CommonUploaderModel.FilePath = @RingIDSettings.STICKER_FOLDER + System.IO.Path.DirectorySeparatorChar + (commentModel.MediaOrImageUrl).Replace(System.IO.Path.AltDirectorySeparatorChar, System.IO.Path.DirectorySeparatorChar);
                }
                else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC || commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
                {
                    if (!string.IsNullOrEmpty(commentModel.ThumbUrl))
                    {
                        ucCommentUploadPanel.CommonUploaderModel.UploadedThumbUrl = commentModel.ThumbUrl;
                        ucCommentUploadPanel.CommonUploaderModel.IsThumbImageFound = true;
                    }
                    ucCommentUploadPanel.CommonUploaderModel.FileDuration = this.commentModel.Duration;
                }
            }
        }

        private bool CommentImageOrMediaChanged()
        {
            bool changed = false;
            if (ucCommentUploadPanel != null && ucCommentUploadPanel.CommonUploaderModel != null)
            {
                if (string.IsNullOrEmpty(commentModel.MediaOrImageUrl) || string.IsNullOrEmpty(ucCommentUploadPanel.CommonUploaderModel.UploadedUrl))
                    changed = true;
                else if (!commentModel.MediaOrImageUrl.Equals(ucCommentUploadPanel.CommonUploaderModel.UploadedUrl))
                    changed = true;
            }
            else if (!string.IsNullOrEmpty(commentModel.MediaOrImageUrl))
                changed = true;

            return changed;
        }

        private void EnablePostbutton(bool flag)
        {
            emoticonBtn.IsEnabled = flag;
            uploadBtn.IsEnabled = flag;
            richTextBox.IsEnabled = flag;

            if (ucCommentUploadPanel != null) ucCommentUploadPanel.RemoveButtonEnabled = flag;
        }

        private void actionEditComment()
        {
            richTextBox.SetStatusandTags();
            if (!richTextBox.StringWithoutTags.Equals(commentModel.Comment) || !HelperMethods.IsTagDTOsSameasJArray(richTextBox.TagsJArray, commentModel.TaggedUsers) || CommentImageOrMediaChanged())
            {
                if (string.IsNullOrEmpty(richTextBox.StringWithoutTags) && (ucCommentUploadPanel == null || ucCommentUploadPanel.CommonUploaderModel == null))
                {
                    UIHelperMethods.ShowWarning(NotificationMessages.NOTHING_TO_POST);
                }
                else
                {
                    FeedModel feedModel = null;
                    bool ImageIdToContentId = false;
                    ThreadAddOrEditAnyComment threadAddOrEditAnyComment = null;
                    JObject pakToSend = new JObject();
                    if (richTextBox.TagsJArray != null)
                        pakToSend[JsonKeys.CommentTagList] = richTextBox.TagsJArray;

                    if (richTextBox.StringWithoutTags.Trim() != null)
                        pakToSend[JsonKeys.Comment] = richTextBox.StringWithoutTags.Trim();
                    else
                        pakToSend[JsonKeys.Comment] = string.Empty;

                    pakToSend[JsonKeys.CommentId] = commentModel.CommentId;

                    if (commentModel.ImageId != Guid.Empty)   //EDIT COMMENT ON IMAGE
                    {
                        ImageModel imageModel = null;
                        ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(commentModel.ImageId, out imageModel);
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_IMAGE_COMMENT;
                        pakToSend[JsonKeys.ImageId] = imageModel.ImageId;
                        pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                        pakToSend[JsonKeys.AlbumId] = imageModel.AlbumId;
                        pakToSend[JsonKeys.NewsfeedId] = imageModel.NewsFeedId;
                    }

                    else if (commentModel.ContentId != Guid.Empty)   //EDIT COMMENT ON MULTIMEDIA
                    {
                        SingleMediaModel singleMediaModel = null;
                        if (MediaDataContainer.Instance.ContentModels.TryGetValue(commentModel.ContentId, out singleMediaModel))
                        {
                            pakToSend[JsonKeys.ContentId] = singleMediaModel.ContentId;
                            pakToSend[JsonKeys.MediaType] = singleMediaModel.MediaType;
                            pakToSend[JsonKeys.AlbumId] = singleMediaModel.AlbumId;
                            pakToSend[JsonKeys.NewsfeedId] = singleMediaModel.NewsFeedId;
                        }
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_COMMENT_ON_MEDIA;
                    }

                    else   //EDIT COMMENT ON FEED
                    {
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_STATUS_COMMENT;
                        if (commentModel.NewsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(commentModel.NewsfeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.NewsfeedId] = commentModel.NewsfeedId;
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;

                            if (feedModel.BookPostType == 1 || feedModel.BookPostType == 4 || feedModel.BookPostType == 7 || feedModel.BookPostType == 10)
                            {

                            }
                            else
                            {
                                if (feedModel.SingleMediaFeedModel != null)
                                {
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleMediaFeedModel.ContentId;
                                }
                                else if (feedModel.SingleImageFeedModel != null)
                                {
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleImageFeedModel.ImageId;
                                    ImageIdToContentId = true;
                                }
                            }
                        }
                    }
                    threadAddOrEditAnyComment = new ThreadAddOrEditAnyComment(pakToSend, (ucCommentUploadPanel == null) ? null : ucCommentUploadPanel.CommonUploaderModel, ImageIdToContentId);

                    threadAddOrEditAnyComment.callBackEvent += (sucess) =>
                    {
                        if (sucess)
                        {
                            if (ucCommentUploadPanel != null && ucCommentUploadPanel.CommonUploaderModel != null)
                            {
                                ucCommentUploadPanel.CommonUploaderModel.IsUploading = false;
                                if (ucCommentUploadPanel.CommonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_STICKER && RecentStickerModel != null)
                                {
                                    RingMarketStickerLoadUtility.AddRecentSticker(RecentStickerModel);
                                    RecentStickerModel = null;
                                }
                                ucCommentUploadPanel.SetAllModelsToNull();
                            }
                            if (_OnEditCompletion != null)
                            {
                                _OnEditCompletion();
                            }
                            //Application.Current.Dispatcher.Invoke(() =>
                            //{
                            //    EnablePostbutton(true);
                            //    richTextBox.Document.Blocks.Clear();
                            //});
                        }
                        else
                        {
                            if (ucCommentUploadPanel != null && ucCommentUploadPanel.CommonUploaderModel != null)
                                ucCommentUploadPanel.CommonUploaderModel.IsUploading = false;
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                EnablePostbutton(true);
                                UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to Edit Comment! ", "Failed!");
                            });
                        }
                    };
                    if (threadAddOrEditAnyComment != null)
                    {
                        EnablePostbutton(false);
                        threadAddOrEditAnyComment.StartThread();
                    }
                }
            }
            else
                UIHelperMethods.ShowWarning(NotificationMessages.ABOUT_NO_CHANGE, "Edit comment");
        }
        #endregion

        #region "Handlers"
        private ICommand _UploadButtonCommand;
        public ICommand UploadButtonCommand
        {
            get
            {
                if (_UploadButtonCommand == null)
                {
                    _UploadButtonCommand = new RelayCommand(param => OnUploadButtonCommand(param));
                }
                return _UploadButtonCommand;
            }
        }
        private void OnUploadButtonCommand(object parameter)
        {
            UploadPopup = true;
            if (ucCommentUploadPopup != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(ucCommentUploadPopup);
            ucCommentUploadPopup = new UCCommentUploadPopup(UCGuiRingID.Instance.MotherPanel);
            ucCommentUploadPopup.Show();
            ucCommentUploadPopup.ShowPopup(uploadBtn, (type) =>
            {
                int upType = type;
                addUploadPanel();

                if (upType == 1)
                {
                    ucCommentUploadPanel.ImageSelectToUpload();
                }
                else if (upType == 2)
                {
                    ucCommentUploadPanel.audioSelectToUpload();
                }
                else if (upType == 3)
                {
                    ucCommentUploadPanel.videoSelectToUpload();
                }
                UploadPopup = false;
                richTextBox.Focusable = true;
                richTextBox.Focus();
                return 0;
            });
        }

        private ICommand _EmoticonButtonCommand;
        public ICommand EmoticonButtonCommand
        {
            get
            {
                if (_EmoticonButtonCommand == null)
                {
                    _EmoticonButtonCommand = new RelayCommand(param => OnEmoticonButtonCommand(param));
                }
                return _EmoticonButtonCommand;
            }
        }
        private void OnEmoticonButtonCommand(object parameter)
        {
            UIElement uiElement = (UIElement)emoticonBtn;
            MainSwitcher.PopupController.StickerPopUPInComments.Show(uiElement, UCEmoticonPopup.TYPE_DEFAULT, motherPanel, (obj) =>
            {
                if (obj is string)
                {
                    string emoticonStr = (string)obj;
                    if (!String.IsNullOrWhiteSpace(emoticonStr))
                    {
                        richTextBox.AppendText(emoticonStr);
                    }
                }
                else if (obj is MarkertStickerImagesModel)
                {
                    RecentStickerModel = (MarkertStickerImagesModel)obj;
                    addUploadPanel();
                    ucCommentUploadPanel.stickerSelectedToUpload(RecentStickerModel.ToString());
                    UploadBtnVisibility = Visibility.Hidden;
                    richTextBox.Focusable = true;
                    richTextBox.Focus();
                }
                return 0;
            });

        }

        private ICommand _rtbEditCommentTextChangedCommand;
        public ICommand RtbEditCommentTextChangedCommand
        {
            get
            {
                _rtbEditCommentTextChangedCommand = _rtbEditCommentTextChangedCommand ?? new RelayCommand(param => onRtbEditCommentTextChanged(param));
                return _rtbEditCommentTextChangedCommand;
            }
        }
        private void onRtbEditCommentTextChanged(object parameter)
        {
            if (parameter is RichTextFeedEdit)
            {
                string text = richTextBox.Text;
                if (text.Length > 0)
                {

                    TextBlockVisibility = Visibility.Collapsed;
                    if (text.EndsWith("@"))
                    {
                        richTextBox.AlphaStarts = text.Length;
                        if (UCAlphaTagPopUp.Instance == null)
                        {
                            UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
                        }
                        if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
                        {
                            Grid g = (Grid)UCAlphaTagPopUp.Instance.Parent;
                            g.Children.Remove(UCAlphaTagPopUp.Instance);
                        }
                        Grid richGrid = (Grid)richTextBox.Parent;
                        richGrid.Children.Add(UCAlphaTagPopUp.Instance);
                        UCAlphaTagPopUp.Instance.InitializePopUpLocation(richTextBox);
                    }
                    else if (richTextBox.AlphaStarts >= 0 && richTextBox.Text.Length > richTextBox.AlphaStarts)
                    {
                        string searchString = richTextBox.Text.Substring(richTextBox.AlphaStarts);
                        UCAlphaTagPopUp.Instance.ViewSearchFriends(searchString);
                    }
                }
                else
                {
                    TextBlockVisibility = Visibility.Visible;
                    richTextBox.AlphaStarts = -1;
                    richTextBox._AddedFriendsUtid.Clear();
                }
            }
        }

        private ICommand _rtbEditCommentPreviewKeyDownCommand;
        public ICommand RtbEditCommentPreviewKeyDownCommand
        {
            get
            {
                _rtbEditCommentPreviewKeyDownCommand = _rtbEditCommentPreviewKeyDownCommand ?? new RelayCommand(param => onRtbEditCommentPreviewKeyDown(param));
                return _rtbEditCommentPreviewKeyDownCommand;
            }
        }
        private void onRtbEditCommentPreviewKeyDown(object parameter)
        {
            if (parameter is KeyEventArgs)
            {
                KeyEventArgs pressedKeyEvent = (KeyEventArgs)parameter;
                if (pressedKeyEvent.Key == Key.Enter && (UCAlphaTagPopUp.Instance == null || (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen == false)))
                {
                    //richTextBox.AppendText("\n");
                    pressedKeyEvent.Handled = true;
                }

                if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
                {
                    if (pressedKeyEvent.Key == Key.Down)
                    {
                        pressedKeyEvent.Handled = true;
                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Up)
                    {
                        pressedKeyEvent.Handled = true;

                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Enter || pressedKeyEvent.Key == Key.Tab)
                    {
                        pressedKeyEvent.Handled = true;
                        UCAlphaTagPopUp.Instance.SelectFriendTag((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
                    }
                }
                else if (pressedKeyEvent.Key == Key.Return)
                {
                    if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    {
                        pressedKeyEvent.Handled = true;
                        actionEditComment();
                    }
                }
                else if (pressedKeyEvent.Key == Key.Escape)
                {
                    if (_OnEditCompletion != null)
                    {
                        _OnEditCompletion();
                    }
                }
            }
        }

        //private ICommand _gotFocusCommand;
        //public ICommand GotFocusCommand
        //{
        //    get
        //    {
        //        if (_gotFocusCommand == null)
        //        {
        //            _gotFocusCommand = new RelayCommand(param => OnGotFocusCommand(param));
        //        }
        //        return _gotFocusCommand;
        //    }
        //}

        //private void OnGotFocusCommand(object param)
        //{
        //    //if (commentModel.ImageId != Guid.Empty && basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.IsVisible)
        //    //{
        //    //    basicImageViewWrapper().ucBasicImageView.AddOrRemovePeviewKeyDownHandler(false);
        //    //}
        //    //else if (commentModel.ContentId != Guid.Empty && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView != null && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.IsVisible)
        //    //{
        //    //    MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.AddOrRemovePeviewKeyDownHandler(false);
        //    //}
        //}

        //private ICommand _lostFocusCommand;
        //public ICommand LostFocusCommand
        //{
        //    get
        //    {
        //        if (_lostFocusCommand == null)
        //        {
        //            _lostFocusCommand = new RelayCommand(param => OnLostFocusCommand(param));
        //        }
        //        return _lostFocusCommand;
        //    }
        //}

        //private void OnLostFocusCommand(object param)
        //{
        //    //if (commentModel.ImageId != Guid.Empty && basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.IsVisible)
        //    //{
        //    //    basicImageViewWrapper().ucBasicImageView.AddOrRemovePeviewKeyDownHandler(true);
        //    //}
        //    //else if (commentModel.ContentId != Guid.Empty && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView != null && MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.IsVisible)
        //    //{
        //    //    MainSwitcher.PopupController.basicMediaViewWrapper.ucBasicMediaView.AddOrRemovePeviewKeyDownHandler(true);
        //    //}
        //}

        private ICommand _CancelCommand;
        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand(param => onCancelEditCommandClicked());
                }
                return _CancelCommand;
            }
        }
        private void onCancelEditCommandClicked()
        {
            if (_OnEditCompletion != null)
            {
                _OnEditCompletion();
            }
        }
        #endregion
    }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
}
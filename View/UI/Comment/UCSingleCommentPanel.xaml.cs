<<<<<<< HEAD
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Models.Constants;
using View.BindingModels;
using View.UI.ImageViewer;
using View.UI.MediaPlayer;
using View.UI.PopUp;
using View.Utility;
using View.ViewModel;
using View.Constants;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCSingleCommentPanel.xaml
    /// </summary>
    public partial class UCSingleCommentPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleCommentPanel).Name);
        Grid motherPanel;

        #region "Constructor"
        public UCSingleCommentPanel()
        {
            InitializeComponent();
        }

        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private UCViewCommentPanel ucViewCommentPanel;
        private UCEditCommentPanel ucEditCommentPanel;
        private UCNameToolTipPopupView ucNameToolTipPopupView { get; set; }
        private UCEditDeleteReportOptionsPopup EditDeleteReportOptionsPopup { get; set; }

        private UCLikeList LikeListView { get; set; }

        private CommentModel _commentModel;
        public CommentModel commentModel
        {
            get
            {
                return _commentModel;
            }
            set
            {
                if (_commentModel == value)
                {
                    return;
                }
                _commentModel = value;
                if (commentModel != null && ucViewCommentPanel != null)
                    commentModel.IsEditMode = false;
                OnPropertyChanged("commentModel");
            }
        }

        private ImageSource _popupLikeLoader;
        public ImageSource PopupLikeLoader
        {
            get
            {
                return _popupLikeLoader;
            }
            set
            {
                if (value == _popupLikeLoader)
                    return;
                _popupLikeLoader = value;
                OnPropertyChanged("PopupLikeLoader");
            }
        }
        #endregion

        #region "Private Method"
        private void setMotherPanel()
        {
            if (this.Tag != null && (Grid)this.Tag is Grid)
            {
                motherPanel = (Grid)this.Tag;
            }
            else if(UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                motherPanel = UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid;
            else motherPanel = UCGuiRingID.Instance.MotherPanel;
        }

        private void setFocusToMotherPanel()
        {
            if (this.Tag != null)
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                //Hide();
                if (obj != null && obj is UCImageViewInMain)
                {
                    UCImageViewInMain imageViewInMain1 = (UCImageViewInMain)obj;
                    imageViewInMain1.GrabKeyboardFocus();
                }
                else if (obj != null && obj is UCMediaPlayerInMain)
                {
                    UCMediaPlayerInMain imageViewInMain1 = (UCMediaPlayerInMain)obj;
                    imageViewInMain1.GrabKeyBoardFocus();
                }
            }
            else if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
        }
        private void addViewCommentPanel()
        {
            if (commentModel == null && this.DataContext is CommentModel)
                commentModel = (CommentModel)this.DataContext;
            if (ucViewCommentPanel == null)
                ucViewCommentPanel = new UCViewCommentPanel();
            setMotherPanel();
            _commentContainer.Child = null;
            ucEditCommentPanel = null;
            ucViewCommentPanel.motherPanel = motherPanel;
            _commentContainer.Child = ucViewCommentPanel;
            //ucViewCommentPanel.commentModel = commentModel;
        }

        private void addEditCommentPanel()
        {
            if (commentModel == null && this.DataContext is CommentModel)
                commentModel = (CommentModel)this.DataContext;
            if (ucEditCommentPanel == null)
            {
                ucEditCommentPanel = new UCEditCommentPanel();
                ucEditCommentPanel._OnEditCompletion = () =>
                {
                    commentModel.IsEditMode = false;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        addViewCommentPanel();
                    });
                    return 0;
                };
            }
            setMotherPanel();
            _commentContainer.Child = null;
            ucEditCommentPanel.motherPanel = motherPanel;
            _commentContainer.Child = ucEditCommentPanel;
            ucEditCommentPanel.commentModel = commentModel;
            ucEditCommentPanel.SetRichTextBoxFieldsandUploaderModel();
        }
        #endregion

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            addViewCommentPanel();
        }
        private ICommand _EditDeleteCommand;
        public ICommand EditDeleteCommand
        {
            get
            {
                if (_EditDeleteCommand == null)
                {
                    _EditDeleteCommand = new RelayCommand(param => OnEditDeleteCommand(param));
                }
                return _EditDeleteCommand;
            }
        }
        private void OnEditDeleteCommand(object parameter)
        {
            try
            {
                if (commentModel == null && this.DataContext is CommentModel)
                    commentModel = (CommentModel)this.DataContext;
                if (!commentModel.IsEditMode)
                {
                    bool EditOff = false;
                    if (commentModel.UserShortInfoModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID || commentModel.IsEditMode) EditOff = true;

                    if (EditDeleteReportOptionsPopup != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(EditDeleteReportOptionsPopup);
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        EditDeleteReportOptionsPopup = new UCEditDeleteReportOptionsPopup(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                    else EditDeleteReportOptionsPopup = new UCEditDeleteReportOptionsPopup(UCGuiRingID.Instance.MotherPanel);
                    EditDeleteReportOptionsPopup.Show();
                    EditDeleteReportOptionsPopup.ShowEditDeletePopup(editDelete, false, false, false, false, false, commentModel);
                    EditDeleteReportOptionsPopup.OnRemovedUserControl += () =>
                    {
                        if (commentModel.IsEditMode)
                        {
                            addEditCommentPanel();
                        }
                        EditDeleteReportOptionsPopup = null;
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible && !commentModel.IsEditMode)
                        {
                            UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        }
                    };
                    //if (ucEditDeletePopup != null)
                    //    mainPanel.Children.Remove(ucEditDeletePopup);
                    //ucEditDeletePopup = new UCEditDeletePopup(mainPanel);
                    //ucEditDeletePopup.Show();
                    //ucEditDeletePopup.ShowPopup(editDelete, EditOff, commentModel);
                    //ucEditDeletePopup.OnRemovedUserControl += () =>
                    //{
                    //    ucEditDeletePopup = null;
                    //    if (commentModel.IsEditMode)
                    //    {
                    //        addEditCommentPanel();
                    //    }
                    //};
                }
                //}
                //ucEditDeletePopup.ShowPopup(editDelete, EditOff, commentModel, () =>
                //    {
                //        if (commentModel.IsEditMode)
                //        {
                //            addEditCommentPanel();
                //        }
                //        return 0;
                //    });
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        DispatcherTimer _NameToolTip = null;
        private ICommand _nameToolTipOpenCommand;
        public ICommand NameToolTipOpenCommand
        {
            get
            {
                if (_nameToolTipOpenCommand == null)
                {
                    _nameToolTipOpenCommand = new RelayCommand(param => OnNameToolTipOpenCommand(param));
                }
                return _nameToolTipOpenCommand;
            }
        }
        private void OnNameToolTipOpenCommand(object parameter)
        {
            if (parameter is BaseUserProfileModel)
            {
                try
                {
                    BaseUserProfileModel model = (BaseUserProfileModel)parameter;

                    if (_NameToolTip == null)
                    {
                        _NameToolTip = new DispatcherTimer();
                        _NameToolTip.Interval = TimeSpan.FromSeconds(1);
                        _NameToolTip.Tick += (s, es) =>
                        {
                            if (ucNameToolTipPopupView != null)
                                UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNameToolTipPopupView);
                            ucNameToolTipPopupView = new UCNameToolTipPopupView(UCGuiRingID.Instance.MotherPanel);
                            ucNameToolTipPopupView.Show();
                            if (nameTxt.IsMouseOver)
                            {
                                if (this.Tag != null && (Grid)this.Tag is Grid)
                                {
                                    motherPanel = (Grid)this.Tag;
                                    ucNameToolTipPopupView.ShowPopUp(nameTxt, model, motherPanel);
                                }
                                else ucNameToolTipPopupView.ShowPopUp(nameTxt, model);
                            }

                            ucNameToolTipPopupView.OnRemovedUserControl += () =>
                            {
                                ucNameToolTipPopupView = null;
                                //if (_NameToolTip != null)
                                //    _NameToolTip = null;
                            };

                            _NameToolTip.Stop();
                            _NameToolTip.Interval = TimeSpan.FromSeconds(0.2);
                        };
                    }
                    _NameToolTip.Stop();
                    _NameToolTip.Start();
                }
                finally { }
            }
        }

        private ICommand _nameToolTipHiddenCommand;
        public ICommand NameToolTipHiddenCommand
        {
            get
            {
                if (_nameToolTipHiddenCommand == null) _nameToolTipHiddenCommand = new RelayCommand(param => OnNameToolTipHiddenCommand(param));
                return _nameToolTipHiddenCommand;
            }
        }
        private void OnNameToolTipHiddenCommand(object parameter)
        {
            if (ucNameToolTipPopupView != null
                && ucNameToolTipPopupView.PopupToolTip.IsOpen
                && !ucNameToolTipPopupView.contentContainer.IsMouseOver)
                ucNameToolTipPopupView.HidePopUp(false);
        }

        private ICommand _CommentNameCommand;
        public ICommand CommentNameCommand
        {
            get
            {
                if (_CommentNameCommand == null)
                {
                    _CommentNameCommand = new RelayCommand(param => OnCommentNameCommand(param));
                }
                return _CommentNameCommand;
            }
        }

        private void OnCommentNameCommand(object parameter)
        {
            if (this.Tag != null && (Grid)this.Tag is Grid)
            {
                motherPanel = (Grid)this.Tag;
                var obj = ((Border)(motherPanel.Parent)).Parent;
                //Hide();
                if (obj != null && obj is UCImageViewInMain)
                {
                    UCImageViewInMain imageViewInMain1 = (UCImageViewInMain)obj;
                    imageViewInMain1.Hide();
                    imageViewInMain1 = null;
                }
                else if (obj != null && obj is UCMediaPlayerInMain)
                {
                    UCMediaPlayerInMain imageViewInMain1 = (UCMediaPlayerInMain)obj;
                    imageViewInMain1.OnlyPlayerView.OnGoToSmallScreen();
                }
                obj = null;
            }

            if (parameter is BaseUserProfileModel)
            {
                BaseUserProfileModel model = (BaseUserProfileModel)parameter;
                RingIDViewModel.Instance.OnUserProfileClicked(model);
            }
        }
        private ICommand _noOfLikeCommand;
        public ICommand NoOfLikeCommand
        {
            get
            {
                if (_noOfLikeCommand == null)
                {
                    _noOfLikeCommand = new RelayCommand(param => OnNoOfLikeCommand(param));
                }
                return _noOfLikeCommand;
            }
        }
        private void ShowLikePopup(Guid contentID, Guid nfID, long numerOflike, Guid imageID, Guid commentID)
        {
            ViewConstants.CommentID = commentID;
            setMotherPanel();
            if (LikeListView != null)
                motherPanel.Children.Remove(LikeListView);
            LikeListView = new UCLikeList(motherPanel);
            LikeListView.ContentId = contentID;
            LikeListView.NewsfeedId = nfID;
            LikeListView.NumberOfLike = numerOflike;
            LikeListView.ImageId = imageID;
            LikeListView.CommentId = commentID;
            LikeListView.Show();
            LikeListView.SendLikeListRequest();
            LikeListView.OnRemovedUserControl += () =>
            {
                setFocusToMotherPanel();
                LikeListView = null;
            };
        }
        private void OnNoOfLikeCommand(object parameter)
        {
            if (parameter is CommentModel)
            {
                try
                {
                    CommentModel commentModel = (CommentModel)parameter;
                    if (commentModel.ContentId != Guid.Empty)
                    {
                        ShowLikePopup(commentModel.ContentId, Guid.Empty, commentModel.TotalLikeComment, Guid.Empty, commentModel.CommentId);
                    }
                    else if (commentModel.ImageId != Guid.Empty)
                    {
                        ShowLikePopup(Guid.Empty, Guid.Empty, commentModel.TotalLikeComment, commentModel.ImageId, commentModel.CommentId);
                    }
                    else
                        ShowLikePopup(Guid.Empty, commentModel.NewsfeedId, commentModel.TotalLikeComment, Guid.Empty, commentModel.CommentId);
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
        #endregion
    }
}
=======
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Models.Constants;
using View.BindingModels;
using View.UI.ImageViewer;
using View.UI.MediaPlayer;
using View.UI.PopUp;
using View.Utility;
using View.ViewModel;
using View.Constants;

namespace View.UI.Comment
{
    /// <summary>
    /// Interaction logic for UCSingleCommentPanel.xaml
    /// </summary>
    public partial class UCSingleCommentPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleCommentPanel).Name);
        Grid motherPanel;

        #region "Constructor"
        public UCSingleCommentPanel()
        {
            InitializeComponent();
        }

        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private UCViewCommentPanel ucViewCommentPanel;
        private UCEditCommentPanel ucEditCommentPanel;
        private UCNameToolTipPopupView ucNameToolTipPopupView { get; set; }
        private UCEditDeleteReportOptionsPopup EditDeleteReportOptionsPopup { get; set; }

        private UCLikeList LikeListView { get; set; }

        private CommentModel _commentModel;
        public CommentModel commentModel
        {
            get
            {
                return _commentModel;
            }
            set
            {
                if (_commentModel == value)
                {
                    return;
                }
                _commentModel = value;
                if (commentModel != null && ucViewCommentPanel != null)
                    commentModel.IsEditMode = false;
                OnPropertyChanged("commentModel");
            }
        }

        private ImageSource _popupLikeLoader;
        public ImageSource PopupLikeLoader
        {
            get
            {
                return _popupLikeLoader;
            }
            set
            {
                if (value == _popupLikeLoader)
                    return;
                _popupLikeLoader = value;
                OnPropertyChanged("PopupLikeLoader");
            }
        }
        #endregion

        #region "Private Method"
        private void setMotherPanel()
        {
            if (this.Tag != null && (Grid)this.Tag is Grid)
            {
                motherPanel = (Grid)this.Tag;
            }
            else if(UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                motherPanel = UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid;
            else 
                motherPanel = UCGuiRingID.Instance.MotherPanel;
        }

        private void SetFocusToMotherPanel()
        {
            if (this.Tag != null)
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                //Hide();
                if (obj != null && obj is UCImageViewInMain)
                {
                    UCImageViewInMain imageViewInMain = (UCImageViewInMain)obj;
                    imageViewInMain.GrabKeyboardFocus();
                }
                else if (obj != null && obj is UCMediaPlayerInMain)
                {
                    UCMediaPlayerInMain mediaPlayerInMain = (UCMediaPlayerInMain)obj;
                    mediaPlayerInMain.GrabKeyBoardFocus();
                }
            }
            else if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
        }

        private void addViewCommentPanel()
        {
            if (commentModel == null && this.DataContext is CommentModel)
                commentModel = (CommentModel)this.DataContext;
            if (ucViewCommentPanel == null)
                ucViewCommentPanel = new UCViewCommentPanel();
            setMotherPanel();
            _commentContainer.Child = null;
            ucEditCommentPanel = null;
            ucViewCommentPanel.motherPanel = motherPanel;
            _commentContainer.Child = ucViewCommentPanel;
            //ucViewCommentPanel.commentModel = commentModel;
        }

        private void addEditCommentPanel()
        {
            if (commentModel == null && this.DataContext is CommentModel)
                commentModel = (CommentModel)this.DataContext;
            if (ucEditCommentPanel == null)
            {
                ucEditCommentPanel = new UCEditCommentPanel();
                ucEditCommentPanel._OnEditCompletion = () =>
                {
                    commentModel.IsEditMode = false;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        addViewCommentPanel();
                    });
                    return 0;
                };
            }
            setMotherPanel();
            _commentContainer.Child = null;
            ucEditCommentPanel.motherPanel = motherPanel;
            _commentContainer.Child = ucEditCommentPanel;
            ucEditCommentPanel.commentModel = commentModel;
            ucEditCommentPanel.SetRichTextBoxFieldsAndUploaderModel();
        }
        #endregion

        #region "ICommand"
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {
            addViewCommentPanel();
        }

        private ICommand _EditDeleteCommand;
        public ICommand EditDeleteCommand
        {
            get
            {
                if (_EditDeleteCommand == null)
                {
                    _EditDeleteCommand = new RelayCommand(param => OnEditDeleteCommand(param));
                }
                return _EditDeleteCommand;
            }
        }
        private void OnEditDeleteCommand(object parameter)
        {
            try
            {
                if (commentModel == null && this.DataContext is CommentModel)
                    commentModel = (CommentModel)this.DataContext;
                if (!commentModel.IsEditMode)
                {
                    if (EditDeleteReportOptionsPopup != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(EditDeleteReportOptionsPopup);
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        EditDeleteReportOptionsPopup = new UCEditDeleteReportOptionsPopup(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                    else 
                        EditDeleteReportOptionsPopup = new UCEditDeleteReportOptionsPopup(UCGuiRingID.Instance.MotherPanel);

                    EditDeleteReportOptionsPopup.Show();
                    EditDeleteReportOptionsPopup.ShowEditDeletePopup(editDelete, false, false, false, false, false, commentModel);
                    EditDeleteReportOptionsPopup.OnRemovedUserControl += () =>
                    {
                        if (commentModel.IsEditMode)
                        {
                            addEditCommentPanel();
                        }
                        EditDeleteReportOptionsPopup = null;
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible && !commentModel.IsEditMode)
                        {
                            UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        }
                    };
                    //if (ucEditDeletePopup != null)
                    //    mainPanel.Children.Remove(ucEditDeletePopup);
                    //ucEditDeletePopup = new UCEditDeletePopup(mainPanel);
                    //ucEditDeletePopup.Show();
                    //ucEditDeletePopup.ShowPopup(editDelete, EditOff, commentModel);
                    //ucEditDeletePopup.OnRemovedUserControl += () =>
                    //{
                    //    ucEditDeletePopup = null;
                    //    if (commentModel.IsEditMode)
                    //    {
                    //        addEditCommentPanel();
                    //    }
                    //};
                }
                //}
                //ucEditDeletePopup.ShowPopup(editDelete, EditOff, commentModel, () =>
                //    {
                //        if (commentModel.IsEditMode)
                //        {
                //            addEditCommentPanel();
                //        }
                //        return 0;
                //    });
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        DispatcherTimer _NameToolTip = null;
        private ICommand _nameToolTipOpenCommand;
        public ICommand NameToolTipOpenCommand
        {
            get
            {
                if (_nameToolTipOpenCommand == null)
                {
                    _nameToolTipOpenCommand = new RelayCommand(param => OnNameToolTipOpenCommand(param));
                }
                return _nameToolTipOpenCommand;
            }
        }
        private void OnNameToolTipOpenCommand(object parameter)
        {
            if (parameter is BaseUserProfileModel)
            {
                try
                {
                    BaseUserProfileModel userProfileModel = (BaseUserProfileModel)parameter;

                    if (_NameToolTip == null)
                    {
                        _NameToolTip = new DispatcherTimer();
                        _NameToolTip.Interval = TimeSpan.FromSeconds(1);
                        _NameToolTip.Tick += (s, es) =>
                        {
                            if (ucNameToolTipPopupView != null)
                                UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNameToolTipPopupView);
                            ucNameToolTipPopupView = new UCNameToolTipPopupView(UCGuiRingID.Instance.MotherPanel);
                            ucNameToolTipPopupView.Show();
                            if (nameTxt.IsMouseOver)
                            {
                                if (this.Tag != null && (Grid)this.Tag is Grid)
                                {
                                    motherPanel = (Grid)this.Tag;
                                    ucNameToolTipPopupView.ShowPopUp(nameTxt, userProfileModel, motherPanel);
                                }
                                else ucNameToolTipPopupView.ShowPopUp(nameTxt, userProfileModel);
                            }

                            ucNameToolTipPopupView.OnRemovedUserControl += () =>
                            {
                                ucNameToolTipPopupView = null;
                                //if (_NameToolTip != null)
                                //    _NameToolTip = null;
                            };

                            _NameToolTip.Stop();
                            _NameToolTip.Interval = TimeSpan.FromSeconds(0.2);
                        };
                    }
                    _NameToolTip.Stop();
                    _NameToolTip.Start();
                }
                finally { }
            }
        }

        private ICommand _nameToolTipHiddenCommand;
        public ICommand NameToolTipHiddenCommand
        {
            get
            {
                if (_nameToolTipHiddenCommand == null) _nameToolTipHiddenCommand = new RelayCommand(param => OnNameToolTipHiddenCommand(param));
                return _nameToolTipHiddenCommand;
            }
        }
        private void OnNameToolTipHiddenCommand(object parameter)
        {
            if (ucNameToolTipPopupView != null
                && ucNameToolTipPopupView.PopupToolTip.IsOpen
                && !ucNameToolTipPopupView.contentContainer.IsMouseOver)
                ucNameToolTipPopupView.HidePopUp(false);
        }

        private ICommand _CommentNameCommand;
        public ICommand CommentNameCommand
        {
            get
            {
                if (_CommentNameCommand == null)
                {
                    _CommentNameCommand = new RelayCommand(param => OnCommentNameCommand(param));
                }
                return _CommentNameCommand;
            }
        }
        private void OnCommentNameCommand(object parameter)
        {
            if (this.Tag != null && (Grid)this.Tag is Grid)
            {
                motherPanel = (Grid)this.Tag;
                var obj = ((Border)(motherPanel.Parent)).Parent;
                //Hide();
                if (obj != null && obj is UCImageViewInMain)
                {
                    UCImageViewInMain imageViewInMain = (UCImageViewInMain)obj;
                    imageViewInMain.Hide();
                    imageViewInMain = null;
                }
                else if (obj != null && obj is UCMediaPlayerInMain)
                {
                    UCMediaPlayerInMain mediaPlayerInMain = (UCMediaPlayerInMain)obj;
                    mediaPlayerInMain.OnlyPlayerView.OnGoToSmallScreen();
                }
                obj = null;
            }

            if (parameter is BaseUserProfileModel)
            {
                BaseUserProfileModel userProfileModel = (BaseUserProfileModel)parameter;
                RingIDViewModel.Instance.OnUserProfileClicked(userProfileModel);
            }
        }

        private ICommand _NumberOfLikesCommand;
        public ICommand NumberOfLikesCommand
        {
            get
            {
                if (_NumberOfLikesCommand == null)
                {
                    _NumberOfLikesCommand = new RelayCommand(param => OnNumberOfLikesCommand(param));
                }
                return _NumberOfLikesCommand;
            }
        }
        private void OnNumberOfLikesCommand(object parameter)
        {
            if (parameter is CommentModel)
            {
                try
                {
                    CommentModel commentModel = (CommentModel)parameter;
                    if (commentModel.ContentId != Guid.Empty)
                    {
                        ShowLikePopup(commentModel.ContentId, Guid.Empty, commentModel.TotalLikeComment, Guid.Empty, commentModel.CommentId);
                    }
                    else if (commentModel.ImageId != Guid.Empty)
                    {
                        ShowLikePopup(Guid.Empty, Guid.Empty, commentModel.TotalLikeComment, commentModel.ImageId, commentModel.CommentId);
                    }
                    else
                        ShowLikePopup(Guid.Empty, commentModel.NewsfeedId, commentModel.TotalLikeComment, Guid.Empty, commentModel.CommentId);
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
        private void ShowLikePopup(Guid contentId, Guid newsfeedId, long numberOfLikes, Guid imageId, Guid commentId)
        {
            ViewConstants.CommentID = commentId;
            setMotherPanel();
            if (LikeListView != null)
                motherPanel.Children.Remove(LikeListView);
            LikeListView = new UCLikeList(motherPanel);
            LikeListView.ContentId = contentId;
            LikeListView.NewsfeedId = newsfeedId;
            LikeListView.NumberOfLike = numberOfLikes;
            LikeListView.ImageId = imageId;
            LikeListView.CommentId = commentId;
            LikeListView.Show();
            LikeListView.SendLikeListRequest();
            LikeListView.OnRemovedUserControl += () =>
            {
                SetFocusToMotherPanel();
                LikeListView = null;
            };
        }
        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.Constants;
using View.Utility;

namespace View.UI.Pages
{
    /// <summary>
    /// Interaction logic for UCNewsPortalMainPanel.xaml
    /// </summary>
    public partial class UCPagesMainPanel : UserControl, INotifyPropertyChanged
    {
        public UCNewsContainerPagesPanel _UCNewsContainerPanel = null;
        public UCDiscoverPagesPanel _UCDiscoverPanel = null;
        public UCFollowingPagesPanel _UCFollowingListPanel = null;
        public UCSavedPagesFeedsPanel _UCSavedContentPanel = null;
        public UCPagesMainPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypePages, TabType);
            SwitchToTab(TabType);
            //new NewsPortalRequest(0, 2, 10).StartThis();
            this.Loaded += UserControl_Loaded;
            this.Unloaded += UserControl_Unloaded;
        }

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"

        private string _TitleName = "Pages";
        public string TitleName
        {
            get { return _TitleName; }
            set
            {
                if (value == _TitleName)
                    return;
                _TitleName = value;
                this.OnPropertyChanged("TitleName");
            }
        }

        private bool _IsTabView = true;
        public bool IsTabView
        {
            get { return _IsTabView; }
            set
            {
                if (value == _IsTabView)
                    return;
                _IsTabView = value;
                this.OnPropertyChanged("IsTabView");
            }
        }

        private int _TabType = 0;
        public int TabType
        {
            get { return _TabType; }
            set
            {
                _TabType = value;
                OnPropertyChanged("TabType");
            }
        }
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            View.ViewModel.RingIDViewModel.Instance.PagesSelection = true;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            View.ViewModel.RingIDViewModel.Instance.PagesSelection = false;
        }

        public void SwitchToTab(int SelectedIndex)
        {
            try
            {
                //string tabItem = ((sender as TabControl).SelectedItem as TabItem).Name as string;
                if (TabType != SelectedIndex)
                    TabType = SelectedIndex;
                switch (SelectedIndex)//(tabItem)
                {
                    case 0://"NewsTab":
                        if (_UCNewsContainerPanel == null)
                        {
                            _UCNewsContainerPanel = new UCNewsContainerPagesPanel(ScrlViewer);
                        }
                        tabContainerBorder.Child = _UCNewsContainerPanel;
                        break;
                    case 1://"DiscoverTab":
                        if (_UCDiscoverPanel == null)
                        {
                            _UCDiscoverPanel = new UCDiscoverPagesPanel(ScrlViewer);
                        }
                        //if (textBox != null) textBox
                        tabContainerBorder.Child = _UCDiscoverPanel;
                        break;
                    case 2://"FollowingTab":
                        if (_UCFollowingListPanel == null)
                        {
                            _UCFollowingListPanel = new UCFollowingPagesPanel(ScrlViewer);
                        }
                        //if (textBox != null) textBox
                        tabContainerBorder.Child = _UCFollowingListPanel;
                        break;
                    case 3://"SavedTab":
                        if (_UCSavedContentPanel == null) _UCSavedContentPanel = new UCSavedPagesFeedsPanel(ScrlViewer);
                        tabContainerBorder.Child = _UCSavedContentPanel;
                        break;
                }
            }
            catch (System.Exception) { }
        }

        #region "ICommand"
        private ICommand _BtnTabCommand;
        public ICommand BtnTabCommand
        {
            get
            {
                if (_BtnTabCommand == null)
                {
                    _BtnTabCommand = new RelayCommand(param => OnBtnTabCommandClicked(param));
                }
                return _BtnTabCommand;
            }
        }

        public void OnBtnTabCommandClicked(object parameter)
        {
            if (parameter is string)
            {
                int TabIdx = Convert.ToInt32(parameter);
                MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypePages, TabIdx);
                SwitchToTab(TabIdx);
            }
        }
        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommandClicked(param));
                }
                return _BackCommand;
            }
        }

        public void OnBackCommandClicked(object parameter)
        {
            tabContainerBorder.Visibility = Visibility.Visible;
            searchContentsBdr.Visibility = Visibility.Collapsed;
            IsTabView = true;
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }

        private ICommand _SearchCommand;
        public ICommand SearchCommand
        {
            get
            {
                if (_SearchCommand == null)
                {
                    _SearchCommand = new RelayCommand(param => OnSearchCommandClicked(param));
                }
                return _SearchCommand;
            }
        }

        public void OnSearchCommandClicked(object parameter)
        {
            tabContainerBorder.Visibility = Visibility.Collapsed;
            searchContentsBdr.Visibility = Visibility.Visible;
            IsTabView = false;
            if (UCMiddlePanelSwitcher.View_UCPagesSearchPanel == null) UCMiddlePanelSwitcher.View_UCPagesSearchPanel = new UCPagesSearchPanel(ScrlViewer);
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypePagesSearch, 0);
            searchContentsBdr.Child = UCMiddlePanelSwitcher.View_UCPagesSearchPanel;
        }
        #endregion
        //private void backBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    SearchTermTextBox.Text = "";//CurrentSearchStr = "";
        //    IsSearch = false;
        //    AskSuggestions();
        //}


        //public string CurrentSearchStr = string.Empty;

        //private TextBox textBox;

        //private void SearchTermTextBox_Loaded(object sender, RoutedEventArgs e)
        //{
        //    textBox = (TextBox)sender;
        //}
    }
}

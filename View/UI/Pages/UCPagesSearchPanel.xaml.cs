﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Feed;
using View.Utility.DataContainer;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.UI.PopUp;

namespace View.UI.Pages
{
    /// <summary>
    /// Interaction logic for UCNewsPortalSearchPanel.xaml
    /// </summary>
    public partial class UCPagesSearchPanel : UserControl, INotifyPropertyChanged
    {
        public CustomScrollViewer scroll;
        public UCPagesSearchPanel(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
        }

        private ObservableCollection<PageInfoModel> _SearchedPages = new ObservableCollection<PageInfoModel>();
        public ObservableCollection<PageInfoModel> SearchedPages
        {
            get
            {
                return _SearchedPages;
            }
            set
            {
                _SearchedPages = value;
                this.OnPropertyChanged("SearchedPages");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void AskSuggestions()
        {
            string CurrentSearchStr = SearchTermTextBox.Text.Trim();
            SearchedPages.Clear();
            List<long> list = null;
            if (UserProfilesContainer.Instance.SearchedPageIdsByParam.TryGetValue(CurrentSearchStr, out list))
            {
                foreach (var item in list)
                {
                    PageInfoModel model = null;
                    if (UserProfilesContainer.Instance.PageModels.TryGetValue(item, out model))
                    {
                        if (!SearchedPages.Any(P => P.PageId == model.PageId))
                            SearchedPages.Add(model);
                    }
                }
            }
            else
            {
                SearchNewsPortals(CurrentSearchStr, SettingsConstants.PROFILE_TYPE_PAGES);
                showMoreBtn.Visibility = Visibility.Collapsed;
                showMoreLoader.Visibility = Visibility.Collapsed;
                showMorePanel.Visibility = Visibility.Visible;
                this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
            }
        }
        private void LookUp(object sender, EventArgs e)
        {
            Dispatcher.Invoke(AskSuggestions);
            if (_Timer != null) _Timer.Stop();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.TextChanged -= SearchTermTextBox_TextChanged;
            SearchTermTextBox.TextChanged += SearchTermTextBox_TextChanged;
            cancel.Click -= cancel_Click;
            cancel.Click += cancel_Click;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.TextChanged -= SearchTermTextBox_TextChanged;
            SearchedPages.Clear();
            SearchTermTextBox.Text = "";
            cancel.Click -= cancel_Click;
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Collapsed;
        }
        private DispatcherTimer _Timer;

        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchTermTextBox.Text.Trim().Length > 0)//if (CurrentSearchStr.Length > 0)
            {
                if (_Timer == null)
                {
                    //AskSuggestions();
                    _Timer = new DispatcherTimer(DispatcherPriority.Background);
                    _Timer.Interval = TimeSpan.FromSeconds(1); //100ms
                    _Timer.Tick += LookUp;
                    _Timer.Start();
                }
                else if (!_Timer.IsEnabled)
                {
                    _Timer.Start();
                }
            }
            else  //load default Values
            {
                showMoreBtn.Visibility = Visibility.Collapsed;
                showMoreLoader.Visibility = Visibility.Collapsed;
                if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                SearchedPages.Clear();
                _Timer.Stop();
            }
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            //UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.LoadUI(false);
        }

        private void followBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.DataContext is PageInfoModel)
            {
                PageInfoModel pageModel = (PageInfoModel)btn.DataContext;
                if (pageModel.IsSubscribed)
                {
                    bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "Page"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                    if (isTrue)
                        new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_PAGES, pageModel.UserTableID, 1, null, null, pageModel.PageId).StartThread();
                }
                else
                {
                    if (UCFollowingUnfollowingView.Instance == null)
                    {
                        UCFollowingUnfollowingView.Instance = new UCFollowingUnfollowingView(UCGuiRingID.Instance.MotherPanel);
                        UCFollowingUnfollowingView.Instance.Show();
                        UCFollowingUnfollowingView.Instance.ShowFollowUnFollow(pageModel, true);
                        UCFollowingUnfollowingView.Instance.OnRemovedUserControl += () =>
                        {
                            UCFollowingUnfollowingView.Instance = null;
                        };
                    }
                }

            }
        }

        private void LoadMore_ButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            SearchNewsPortals(SearchTermTextBox.Text.Trim(), SettingsConstants.PROFILE_TYPE_PAGES, SearchedPages.Count);
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
        }

        public void ShowMoreStates(int state) //0 first success,1false, 2no resp \3 success,4 false, 5 no response
        {
            if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            showMoreLoader.Visibility = Visibility.Collapsed;
            switch (state)
            {
                case 0:
                    showMoreBtn.Visibility = Visibility.Visible;
                    break;
                case 1:
                    showMoreBtn.Visibility = Visibility.Collapsed;
                    showMorePanel.Visibility = Visibility.Collapsed;
                    break;
                case 2:
                    showMoreBtn.Visibility = Visibility.Visible;
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Load more...");
                    break;
                case 3:
                    showMoreBtn.Visibility = Visibility.Visible;
                    break;
                case 4:
                    showMoreBtn.Visibility = Visibility.Collapsed;
                    showMorePanel.Visibility = Visibility.Collapsed;
                    break;
                case 5:
                    showMoreBtn.Visibility = Visibility.Visible;
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Load more...");
                    break;
            }
        }
        #region "Utility Methods"

        private void SearchNewsPortals(string searchParam, int profileType, int st = 0)
        {
            new ThradSearchNewsPortals().StartThread(searchParam, profileType, st);
        }

        //private void SubscribeOrUnsubscribe(int profileType, long utId, int SubUnsubscrbType, List<long> subscrbeLst, List<long> unsubscrbeLst)
        //{
        //    new ThradSubscribeOrUnsubscribe(profileType, utId, SubUnsubscrbType, subscrbeLst, unsubscrbeLst,pageM).StartThread();
        //}

        //private FeedModel GetFeedModel(FeedDTO feedDto)
        //{
        //    return (FeedModel)MainSwitcher.AuthSignalHandler().feedSignalHandler.GetFeedModel(feedDto);
        //}

        #endregion "Utility Methods"
    }
}

﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.DataContainer;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Pages
{
    /// <summary>
    /// Interaction logic for UCFollowingListPanel.xaml
    /// </summary>
    public partial class UCFollowingPagesPanel : UserControl, INotifyPropertyChanged
    {
        public CustomScrollViewer scroll;
        public UCFollowingPagesPanel(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
            new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_PAGES);

        }

        #region "Properties"

        private ObservableCollection<PageInfoModel> _FollowingNewsPages = new ObservableCollection<PageInfoModel>();
        public ObservableCollection<PageInfoModel> FollowingNewsPages
        {
            get
            {
                return _FollowingNewsPages;
            }
            set
            {
                _FollowingNewsPages = value;
                this.OnPropertyChanged("FollowingNewsPages");
            }
        }
        #endregion "Properties"

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #region "ContextMenu"

        ContextMenu cntxMenu = new ContextMenu();
        MenuItem mnuItem1 = new MenuItem();
        MenuItem mnuItem2 = new MenuItem();
        MenuItem mnuItem3 = new MenuItem();

        private void SetupMenuItem()
        {
            if (cntxMenu.Items != null)
                cntxMenu.Items.Clear();
            cntxMenu.Style = (Style)Application.Current.Resources["CustomCntxtMenu"];
            mnuItem1.Header = "Edit";
            mnuItem2.Header = "Unfollow";

            mnuItem1.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem2.Style = (Style)Application.Current.Resources["ContextMenuItem"];

            cntxMenu.Items.Add(mnuItem1);
            cntxMenu.Items.Add(mnuItem2);

            mnuItem1.Click -= MnuEditFlwngList_Click;
            mnuItem2.Click -= MnuUnfollow_Click;
            cntxMenu.Closed -= ContextMenu_Closed;
            cntxMenu.Closed += ContextMenu_Closed;
            mnuItem1.Click += MnuEditFlwngList_Click;
            mnuItem2.Click += MnuUnfollow_Click;

        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            mnuItem1.Click -= MnuEditFlwngList_Click;
            mnuItem2.Click -= MnuUnfollow_Click;
            cntxMenu.Closed -= ContextMenu_Closed;

            cntxMenu.Style = null;
            mnuItem1.Style = null;
            mnuItem2.Style = null;


            //cntxMenu.Items.Remove(mnuItem1);
            //cntxMenu.Items.Remove(mnuItem2);
            cntxMenu.Items.Clear();

            optnBtn.ContextMenu = null;
        }

        private void MnuEditFlwngList_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (pageModel != null)
            {
                //DownloadOrAddToAlbumPopUpWrapper.Show(pageModel, true);
                pageModel = null;
            }
        }

        private void MnuUnfollow_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (pageModel != null)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "News Page"), "Unfollow confirmation!");
                if (isTrue)
                {
                    new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_PAGES, pageModel.UserTableID, 1, null, null, pageModel.PageId).StartThread();
                    pageModel = null;
                }
            }
        }

        #endregion "ContextMenu"

        Button optnBtn;
        private PageInfoModel pageModel = null;
        private PageInfoModel prevPageModel = null;

        #region "Event Triggers"

        public void feedScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            if (scrollViewer.VerticalOffset >= 51)//51 
            {
                // UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.floatingPanel.Visibility = Visibility.Visible;
            }
            else
            {
                //UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.floatingPanel.Visibility = Visibility.Collapsed;
            }
        }
        private void optnBtn_Click(object sender, RoutedEventArgs e)
        {
            optnBtn = (Button)sender;
            if (optnBtn.DataContext is PageInfoModel)
            {
                pageModel = (PageInfoModel)optnBtn.DataContext;
                if (cntxMenu.Items.Count == 0 || optnBtn.ContextMenu != cntxMenu)
                {
                    optnBtn.ContextMenu = cntxMenu;
                    SetupMenuItem();
                }
                cntxMenu.IsOpen = true;
                pageModel.IsContextMenuOpened = true;
                if (prevPageModel != null && pageModel.PageId != prevPageModel.PageId)
                {
                    prevPageModel.IsContextMenuOpened = false;
                }
                prevPageModel = pageModel;
                optnBtn.ContextMenu.IsVisibleChanged -= ContextMenu_IsVisibleChanged;
                optnBtn.ContextMenu.IsVisibleChanged += ContextMenu_IsVisibleChanged;
            }
        }
        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false)
            {
                if (pageModel != null)
                    pageModel.IsContextMenuOpened = false;
                optnBtn.ContextMenu.IsVisibleChanged -= ContextMenu_IsVisibleChanged;
            }
        }
        private void LoadMore_ButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            //   new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_PAGES, FollowingNewsPages.Count);
            new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_PAGES, FollowingNewsPages.Count);
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
        }

        //private List<PageInfoDTO> TMP_DTOS = new List<PageInfoDTO>();
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
            scroll.ScrollChanged += feedScrlViewer_ScrollChanged;

            scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);
            //if (TMP_DTOS.Count > 0)
            //{
            //    foreach (var item in TMP_DTOS)
            //    {
            //        if (!FollowingNewsPages.Any(P => P.PageId == item.PageId))
            //        {
            //            PageInfoModel model = NewsPortalDataContainer.Instance.SearchPageInfoModelByUtId(item.PageId);
            //            if (model == null) model = new PageInfoModel();
            //            model.LoadData(item);
            //            FollowingNewsPages.Add(model);
            //        }
            //    }
            //    TMP_DTOS.Clear();
            //}
            showMoreBtn.Click += LoadMore_ButtonClick;
        }
        private double previousScrollHeight = 0;
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            prevPageModel = null;
            scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;

            previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;
            //if (FollowingNewsPages.Count > 0)
            //{
            //    foreach (var item in FollowingNewsPages)
            //    {
            //        PageInfoDTO dto = null;
            //        if (NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY.TryGetValue(item.PageId, out dto))
            //        {
            //            TMP_DTOS.Add(dto);
            //        }
            //    }
            //    FollowingNewsPages.Clear();
            //}
            showMoreBtn.Click -= LoadMore_ButtonClick;
        }

        #endregion

        public void ShowMoreStates(int state) //0 first success,1false, 2no resp \3 success,4 false, 5 no response
        {
            if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            showMoreLoader.Visibility = Visibility.Collapsed;
            switch (state)
            {
                case 0:
                    showMoreBtn.Visibility = Visibility.Visible;
                    break;
                case 1:
                    //
                    showMoreBtn.Visibility = Visibility.Visible;
                    break;
                case 2:
                    showMoreBtn.Visibility = Visibility.Visible;
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Load more...");
                    break;
                case 3:
                    showMoreBtn.Visibility = Visibility.Visible;
                    break;
                case 4:
                    showMoreBtn.Visibility = Visibility.Collapsed;
                    showMorePanel.Visibility = Visibility.Collapsed;
                    if (FollowingNewsPages.Count == 0)
                        noMoreTxt.Visibility = Visibility.Visible;
                    break;
                case 5:
                    showMoreBtn.Visibility = Visibility.Visible;
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Load more...");
                    break;
            }
        }
    }
}

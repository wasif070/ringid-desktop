﻿using Auth.utility;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Auth;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using System.Linq;
using View.ViewModel;

namespace View.UI.Pages
{
    /// <summary>
    /// Interaction logic for UCNewsContainerPanel.xaml
    /// </summary>
    public partial class UCNewsContainerPagesPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCNewsContainerPagesPanel).Name);
        private VirtualizingStackPanel _ImageItemContainer = null;
        private Visibility _IsLeftArrowVisible = Visibility.Hidden;
        private Visibility _IsRightArrowVisible = Visibility.Hidden;
        public bool _IsTimerStart = false;
        private System.Timers.Timer _Timer;

        private bool _IsFirstTimePrev = true;
        private bool _IsFirstTimeNext = true;

        private int _CurrentLeftMargin = 0;
        private int _NextPrevCount = 0;
        private int _NoOfSwappingCount = 0;
        public CustomScrollViewer scroll;

        #region "Constructor"

        public UCNewsContainerPagesPanel(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            //ViewCollection = RingIDViewModel.Instance.AllPagesFeeds;
            scroll.SetScrollValues(ViewCollection, FeedDataContainer.Instance.AllPagesFeedsSortedIds, SettingsConstants.PROFILE_TYPE_PAGES, AppConstants.TYPE_BUSINESS_PAGE_FEED);
            DefaultSettings.ALLPAGE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.ALLPAGE_STARTPKT);
            JObject pakToSend = new JObject();
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_BUSINESSPAGE_BREAKING_FEED, AppConstants.REQUEST_TYPE_REQUEST);
        }
        #endregion

        #region "INotifyPropertyChanged"

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }

        private CustomObservableCollection _ViewCollection;
        public CustomObservableCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        private ObservableCollection<FeedModel> _BreakingFeeds = new ObservableCollection<FeedModel>();
        public ObservableCollection<FeedModel> BreakingFeeds
        {
            get
            {
                return _BreakingFeeds;
            }
            set
            {
                _BreakingFeeds = value;
                this.OnPropertyChanged("BreakingFeeds");
            }
        }

        private ObservableCollection<FeedModel> _BreakingPagesSliderFeeds = new ObservableCollection<FeedModel>();
        public ObservableCollection<FeedModel> BreakingPagesSliderFeeds
        {
            get
            {
                return _BreakingPagesSliderFeeds;
            }
            set
            {
                _BreakingPagesSliderFeeds = value;
                this.OnPropertyChanged("BreakingPagesSliderFeeds");
            }
        }
        //public UCBasicMediaViewWrapper BasicMediaViewWrapper
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.BasicMediaViewWrapper;
        //    }
        //}

        #endregion "Property "

        #region "Public Methods"

        public void LoadSliderData()
        {
            try
            {
                if (BreakingFeeds != null && BreakingFeeds.Count > 0)
                {
                    List<FeedModel> breakingNws = BreakingFeeds.ToList();
                    foreach (FeedModel model in breakingNws)
                    {
                        if (!BreakingPagesSliderFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                        {
                            BreakingPagesSliderFeeds.InvokeAdd(model);
                            if (BreakingPagesSliderFeeds.Count == 3)
                            {
                                break;
                            }
                        }
                    }

                    if (BreakingPagesSliderFeeds.Count > 1)
                    {
                        SetNextPrevoiusButtonVisibility();
                        StartBannerNewsSliding();
                    }
                }
            }

            catch (Exception ex)
            {
                log.Error("Error: LoadSliderData() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Private Methods"

        private void StartBannerNewsSliding()
        {
            if (_Timer == null)
            {
                _Timer = new System.Timers.Timer();
                _Timer.Interval = 2500;
                _IsTimerStart = true;
                _Timer.Elapsed += timer_Elapsed;
                _Timer.Start();
            }
        }

        private void SetNextPrevoiusButtonVisibility()
        {
            if (BreakingFeeds != null && _NextPrevCount == BreakingFeeds.Count)
            {
                _NextPrevCount = 0;
            }

            IsLeftArrowVisible = _NextPrevCount > 0 ? Visibility.Visible : Visibility.Collapsed;
            IsRightArrowVisible = BreakingFeeds != null && _NextPrevCount < BreakingFeeds.Count - 1 ? Visibility.Visible : Visibility.Collapsed;
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    nextBtn_Click(null, null);
                }
                catch (Exception ex)
                {
                    log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(_CurrentLeftMargin, 0, 0, 0);
            MarginAnimation.To = new Thickness(target, 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
            storyboard.Begin(_ImageItemContainer);
        }

        private void OnPrevious(object param)
        {
            try
            {
                int target = 0;
                _CurrentLeftMargin = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNext(object param)
        {
            try
            {
                //int target = _CurrentLeftMargin - (int)pnlImageContainerWrapper.ActualWidth;
                int target = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ResetData()
        {
            if (_Timer != null)
            {
                _Timer.Elapsed -= timer_Elapsed;
                _Timer.Stop();
                _Timer.Enabled = false;
                _Timer = null;
            }

            AnimationOnArrowClicked(0, 0);
            _NoOfSwappingCount = 0;
            _NextPrevCount = 0;
            _CurrentLeftMargin = 0;
            _IsFirstTimeNext = true;
            _IsFirstTimePrev = true;
            _IsTimerStart = false;

            IsLeftArrowVisible = Visibility.Hidden;
            IsRightArrowVisible = Visibility.Hidden;
        }
        #endregion

        #region "Event Triggers"
        private void UserControl_Loaded(object sender, RoutedEventArgs args)
        {
            scroll.ScrollToHome();
            //((INotifyCollectionChanged)BreakingFeeds).CollectionChanged += BreakingFeeds_CollectionChanged;
            prevBtn.Click += prevBtn_Click;
            nextBtn.Click += nextBtn_Click;
            LeftArrowPanel.MouseLeftButtonUp += LeftArrowPanel_MouseLeftButtonUp;
            RightArrowPanel.MouseLeftButtonUp += RightArrowPanel_MouseLeftButtonUp;
            //sliderListBox.Loaded += SliderListBox_Loaded;
            if (BreakingFeeds != null && BreakingFeeds.Count > 0 && !_IsTimerStart)
                LoadSliderData();

            if (ViewCollection == null)
            {
                if (timer == null)
                {
                    timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromMilliseconds(500);
                    timer.Tick += TimerEventProcessor;
                }
                else timer.Stop();
                timer.Start();
            }
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs args)
        {
            // if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
            // {
            scroll.SetScrollEvents(false);
            ViewCollection = null;
            //((INotifyCollectionChanged)BreakingFeeds).CollectionChanged -= BreakingFeeds_CollectionChanged;
            prevBtn.Click -= prevBtn_Click;
            nextBtn.Click -= nextBtn_Click;
            LeftArrowPanel.MouseLeftButtonUp -= LeftArrowPanel_MouseLeftButtonUp;
            RightArrowPanel.MouseLeftButtonUp -= RightArrowPanel_MouseLeftButtonUp;
            //sliderListBox.Loaded -= SliderListBox_Loaded;
            if (_ImageItemContainer != null)
                ResetData();
            if (BreakingPagesSliderFeeds != null)
                BreakingPagesSliderFeeds.Clear();
            //}
        }
        private DispatcherTimer timer;
        private void TimerEventProcessor(object sender, EventArgs e)
        {
            if (timer != null && timer.IsEnabled)
            {
                //ViewCollection = RingIDViewModel.Instance.AllPagesFeeds;
                scroll.SetScrollEvents(true);
                if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    Keyboard.Focus(scroll);
                timer.Stop();
            }
        }

        private void LeftArrowPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (_Timer != null)
                //{
                //    _Timer.Stop();
                //    OnPrevious(null);
                //    _Timer.Start();
                //}
                prevBtn_Click(null, null);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void RightArrowPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //if (_Timer != null)
                //{
                //    _Timer.Stop();
                //    OnNext(null);
                //    _Timer.Start();
                //}
                nextBtn_Click(null, null);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        private void prevBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_Timer != null)
                    _Timer.Stop();

                int firstIndex = 0, middleIndex = 1, lastIndex = 2;

                if (_IsFirstTimePrev == false)
                {
                    if (BreakingPagesSliderFeeds.Count > 2)
                    {
                        BreakingPagesSliderFeeds.InvokeMove(middleIndex, lastIndex);
                        BreakingPagesSliderFeeds.InvokeMove(firstIndex, middleIndex);
                        BreakingPagesSliderFeeds.RemoveAt(firstIndex);

                        FeedModel model = BreakingFeeds.ElementAt(_NoOfSwappingCount);
                        BreakingPagesSliderFeeds.InvokeInsert(firstIndex, model);
                    }
                }

                _IsFirstTimeNext = true;
                _IsFirstTimePrev = false;

                OnPrevious(null);
                _NextPrevCount--;

                if (_NoOfSwappingCount > 0)
                {
                    _NoOfSwappingCount = _NoOfSwappingCount - 1;
                }

                SetNextPrevoiusButtonVisibility();
                if (_Timer != null)
                    _Timer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: prevBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }


        private void nextBtn_Click(object sender, RoutedEventArgs e)
        {
            //Hints: For AutoSlide/Next, Viewport is 0 to -630.
            try
            {
                if (_Timer != null)
                    _Timer.Stop();
                if (_IsFirstTimeNext == false)
                {
                    int firstIndex = 0, middleIndex = 1, lastIndex = 2, nextLoadingIndex = (_NextPrevCount + lastIndex) % BreakingFeeds.Count;
                    BreakingPagesSliderFeeds.InvokeMove(firstIndex, middleIndex);
                    if (BreakingPagesSliderFeeds.Count > 2)
                    {
                        BreakingPagesSliderFeeds.InvokeMove(middleIndex, lastIndex);
                        BreakingPagesSliderFeeds.RemoveAt(lastIndex);
                        FeedModel model = BreakingFeeds.ElementAt(nextLoadingIndex);
                        BreakingPagesSliderFeeds.InvokeInsert(lastIndex, model);

                        if (_NoOfSwappingCount == BreakingFeeds.Count - 1)
                        {
                            _NoOfSwappingCount = 0;
                        }

                        else
                        {
                            _NoOfSwappingCount++;
                        }
                    }
                }

                _IsFirstTimeNext = false; // firstTime no swapping make, just changing viewport from 0 to -630.
                _IsFirstTimePrev = true;

                _NextPrevCount++;
                OnNext(null);
                SetNextPrevoiusButtonVisibility();
                if (_Timer != null)
                    _Timer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: nextBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }


        //private void BreakingFeeds_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        //{
        //    if (e.Action == NotifyCollectionChangedAction.Add)
        //    {
        //        SliderListBox_Loaded(null, null);
        //    }
        //    else if (e.Action == NotifyCollectionChangedAction.Remove)
        //    {
        //        SliderListBox_Loaded(null, null);
        //    }
        //}

        //private void SliderListBox_Loaded(object sender, RoutedEventArgs e)
        //{
        //    IsLeftArrowVisible = CurrentLeftMargin < 0 ? Visibility.Visible : Visibility.Hidden;
        //    IsRightArrowVisible = CurrentLeftMargin > -(((float)BreakingFeeds.Count) - 1) * pnlsliderContainerWrapper.ActualWidth ? Visibility.Visible : Visibility.Hidden;
        //}

        private void HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as VirtualizingStackPanel;
        }
        private void BreakingNews_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Grid grd = (Grid)sender;
                if (grd.DataContext is FeedModel)
                {
                    FeedModel modelFromContext = (FeedModel)grd.DataContext;
                    if (UCSingleFeedDetailsView.Instance != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(UCSingleFeedDetailsView.Instance);
                    UCSingleFeedDetailsView.Instance = new UCSingleFeedDetailsView(UCGuiRingID.Instance.MotherPanel);
                    UCSingleFeedDetailsView.Instance.Show();
                    UCSingleFeedDetailsView.Instance.ShowDetailsView();
                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Visible;
                    UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                    UCSingleFeedDetailsView.Instance.OnRemovedUserControl += () =>
                    {
                        UCSingleFeedDetailsView.Instance = null;
                    }; 
                    MainSwitcher.ThreadManager().DetailsOfaFeed.CallBackEvent += (dto) =>
                    {
                        if (dto != null)
                        {
                            FeedModel fm = null;
                            FeedDataContainer.Instance.FeedModels.TryGetValue(modelFromContext.NewsfeedId, out fm);
                            if (fm == null) fm = modelFromContext;//new FeedModel();
                            fm.LoadData(dto);
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (UCSingleFeedDetailsView.Instance != null)
                                {
                                    UCSingleFeedDetailsView.Instance.LoadFeedModel(fm);
                                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Collapsed;
                                    if (UCSingleFeedDetailsView.Instance.GIFCtrlLoader.IsRunning())
                                        UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StopAnimate();
                                }
                            });
                        }
                    };
                    MainSwitcher.ThreadManager().DetailsOfaFeed.StartThread(modelFromContext.NewsfeedId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion
        #region "Public Method"

        #endregion

        #region "Private Method"

        #endregion

        #region "ICommand"
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            DefaultSettings.ALLPAGE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.ALLPAGE_STARTPKT);
        }
        #endregion
        #region "Utility Methods"

        #endregion "Utility Methods"
    }
}

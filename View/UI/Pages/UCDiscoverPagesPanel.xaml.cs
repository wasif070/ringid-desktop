﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.DataContainer;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Pages
{
    /// <summary>
    /// Interaction logic for UCDiscoverPanel.xaml
    /// </summary>
    public partial class UCDiscoverPagesPanel : UserControl, INotifyPropertyChanged
    {
        public CustomScrollViewer scroll;
        public UCDiscoverPagesPanel(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
            new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_PAGES);
        }
        private ObservableCollection<PageInfoModel> _SearchedNewsPages = new ObservableCollection<PageInfoModel>();
        public ObservableCollection<PageInfoModel> SearchedNewsPages
        {
            get
            {
                return _SearchedNewsPages;
            }
            set
            {
                _SearchedNewsPages = value;
                this.OnPropertyChanged("SearchedNewsPages");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        #region "Event Triggers"

        private void LoadMore_ButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            new ThradDiscoverOrFollowingChannelsList().StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_PAGES, SearchedNewsPages.Count);
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
        }
        public void ShowMoreStates(int state) //0 first success,1false, 2no resp \3 success,4 false, 5 no response
        {
            if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
            GIFCtrlLoader.Visibility = Visibility.Collapsed;
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            showMoreLoader.Visibility = Visibility.Collapsed;
            switch (state)
            {
                case 0:
                    showMoreBtn.Visibility = Visibility.Visible;
                    break;
                case 1:
                    //
                    showMoreBtn.Visibility = Visibility.Visible;
                    break;
                case 2:
                    showMoreBtn.Visibility = Visibility.Visible;
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Load more..");
                    break;
                case 3:
                    showMoreBtn.Visibility = Visibility.Visible;
                    break;
                case 4:
                    showMoreBtn.Visibility = Visibility.Collapsed;
                    showMorePanel.Visibility = Visibility.Collapsed;
                    break;
                case 5:
                    showMoreBtn.Visibility = Visibility.Visible;
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Load more..");
                    break;
            }
        }

        //private List<PageInfoDTO> TMP_DTOS = new List<PageInfoDTO>();
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);
            //if (TMP_DTOS.Count > 0)
            //{
            //    foreach (var item in TMP_DTOS)
            //    {
            //        if (!SearchedNewsPages.Any(P => P.PageId == item.PageId))
            //        {
            //            PageInfoModel model = NewsPortalDataContainer.Instance.SearchPageInfoModelByUtId(item.PageId);
            //            if (model == null) model = new PageInfoModel();
            //            model.LoadData(item);
            //            SearchedNewsPages.Add(model);
            //        }
            //    }
            //    TMP_DTOS.Clear();
            //}
            showMoreBtn.Click += LoadMore_ButtonClick;
        }
        private double previousScrollHeight = 0;
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;
            //if (SearchedNewsPages.Count > 0)
            //{
            //    foreach (var item in SearchedNewsPages)
            //    {
            //        PageInfoDTO dto = null;
            //        if (NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY.TryGetValue(item.PageId, out dto))
            //        {
            //            TMP_DTOS.Add(dto);
            //        }
            //    }
            //    SearchedNewsPages.Clear();
            //}
            showMoreBtn.Click -= LoadMore_ButtonClick;
        }

        #endregion
    }
}

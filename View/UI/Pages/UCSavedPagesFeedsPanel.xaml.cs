﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.UI.Pages
{
    /// <summary>
    /// Interaction logic for UCSavedContentPanel.xaml
    /// </summary>
    public partial class UCSavedPagesFeedsPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSavedPagesFeedsPanel).Name);
        public CustomScrollViewer scroll;

        #region "Constructor"

        public UCSavedPagesFeedsPanel(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            //ViewCollection = RingIDViewModel.Instance.SavedFeedsPage;
            scroll.SetScrollValues(ViewCollection, FeedDataContainer.Instance.SavedFeedsPageSortedIds, SettingsConstants.PROFILE_TYPE_PAGES, AppConstants.TYPE_ALL_SAVED_FEEDS);
            if (DefaultSettings.SAVEDPAGE_STARTPKT == null)
            {
                DefaultSettings.SAVEDPAGE_STARTPKT = SendToServer.GetRanDomPacketID();
                if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                    scroll.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDPAGE_STARTPKT);
            }
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private CustomObservableCollection _ViewCollection;
        public CustomObservableCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        #endregion

        #region "Private Method"
        #endregion

        #region "Public Method"
        #endregion

        #region "Event Triggers"

        private void UserControl_Loaded(object sender, RoutedEventArgs args)
        {
            scroll.ScrollToHome();
            if (ViewCollection == null)
            {
                if (timer == null)
                {
                    timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromMilliseconds(500);
                    timer.Tick += TimerEventProcessor;
                }
                else timer.Stop();
                timer.Start();
            }
        }
        private void UserControl_Unloaded(object sender, RoutedEventArgs args)
        {
            //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
            //{
                scroll.SetScrollEvents(false);
                ViewCollection = null;
            //}
        }
        private DispatcherTimer timer;
        private void TimerEventProcessor(object sender, EventArgs e)
        {
            if (timer != null && timer.IsEnabled)
            {
                //ViewCollection = RingIDViewModel.Instance.SavedFeedsPage;
                scroll.SetScrollEvents(true);
                if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    Keyboard.Focus(scroll);
                timer.Stop();
            }
        }

        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }
        private void OnReloadCommandClicked()
        {
            DefaultSettings.SAVEDPAGE_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDPAGE_STARTPKT);
        }



        #endregion

        #region "Utility Methods"


        #endregion "Utility Methods"

    }
}

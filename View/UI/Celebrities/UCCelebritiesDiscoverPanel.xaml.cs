﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Celebrities
{
    /// <summary>
    /// Interaction logic for UCCelebritiesDiscoverPanel.xaml
    /// </summary>
    public partial class UCCelebritiesDiscoverPanel : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCCelebritiesDiscoverPanel).Name);

        public CustomScrollViewer scroll;
        public int selectedIndex = 0;
        bool IsSuccess = true;

        private VirtualizingStackPanel _ImageItemContainer = null;
        private bool _IsFirstTimePrev = true;
        private bool _IsFirstTimeNext = true;

        private int _CurrentLeftMargin = 0;
        private int _NextPrevCount = 0;
        private int _NoOfSwappingCount = 0;

        #region "Constructor"
        public UCCelebritiesDiscoverPanel(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

            RequestToGetDsicoverCelebrities();
        }
        #endregion "Constructor"

        #region "Properties"
        private ObservableCollection<CelebrityModel> _SearchedDiscovery = new ObservableCollection<CelebrityModel>();
        public ObservableCollection<CelebrityModel> SearchedDiscovery
        {
            get
            {
                return _SearchedDiscovery;
            }
            set
            {
                _SearchedDiscovery = value;
                this.OnPropertyChanged("SearchedDiscovery");
            }
        }

        private ObservableCollection<CelebrityModel> _celebritiesDiscoverList = new ObservableCollection<CelebrityModel>();
        public ObservableCollection<CelebrityModel> CelebritiesDiscoverList
        {
            get
            {
                return _celebritiesDiscoverList;
            }
            set
            {
                _celebritiesDiscoverList = value;
                this.OnPropertyChanged("CelebritiesDiscoverList");
            }
        }

        private String _countryName = "";
        public String CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                _countryName = value;
                this.OnPropertyChanged("CountryName");
            }
        }

        private String _categoryName = "";
        public String CategoryName
        {
            get
            {
                return _categoryName;
            }
            set
            {
                _categoryName = value;
                this.OnPropertyChanged("CategoryName");
            }
        }

        private Guid _CategoryId;
        public Guid CategoryId
        {
            get { return _CategoryId; }
            set
            {
                if (value == _CategoryId)
                    return;

                _CategoryId = value;
                this.OnPropertyChanged("CategoryId");
            }
        }

        private Visibility _IsLeftArrowVisible = Visibility.Hidden;
        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        private Visibility _IsRightArrowVisible = Visibility.Hidden;
        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }

        #endregion "Properties"

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Event Trigger"
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchedDiscovery = RingIDViewModel.Instance.CelebritiesDiscovered;
            prevBtn.Click += prevBtn_Click;
            nextBtn.Click += nextBtn_Click;
            cntryPanel.MouseLeftButtonUp += cntryPanel_MouseLeftButtonUp;
            categoryPanel.MouseLeftButtonUp += categoryPanel_MouseLeftButtonUp;
            sliderListBox.PreviewMouseWheel += sliderListBox_PreviewMouseWheel;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            prevBtn.Click -= prevBtn_Click;
            nextBtn.Click -= nextBtn_Click;
            cntryPanel.MouseLeftButtonUp -= cntryPanel_MouseLeftButtonUp;
            categoryPanel.MouseLeftButtonUp -= categoryPanel_MouseLeftButtonUp;
            sliderListBox.PreviewMouseWheel -= sliderListBox_PreviewMouseWheel;
        }

        //private void prevBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (SelectedIndex1 > 0)
        //        {
        //            StartAnimation(true);
        //            IsSuccess = true;
        //            SelectedIndex1 = SelectedIndex1 - 1;
        //            ChangeDiscoverList();
        //            LeftArrowVisibility();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}
        //private void nextBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    if (SelectedIndex1 < (SearchedDiscovery.Count - 1))
        //    {
        //        SelectedIndex1 = SelectedIndex1 + 1;
        //        ChangeDiscoverList();
        //        LeftArrowVisibility();
        //    }
        //    else
        //    {
        //        nextBtn.IsEnabled = false;
        //        imageContainerPanel.Visibility = Visibility.Hidden;
        //        showMoreLoader.Visibility = Visibility.Visible;
        //        this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));

        //        CelebrityDiscoverPopularorFollowingList thrd = new CelebrityDiscoverPopularorFollowingList(SettingsConstants.CELEBRITY_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_CELEBRITY, CountryName, CategoryId, SearchedDiscovery.Count);
        //        thrd.callBackEvent += (success) =>
        //        {
        //            Application.Current.Dispatcher.Invoke(() =>
        //            {
        //                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        //                showMoreLoader.Visibility = Visibility.Collapsed;

        //                if (success == SettingsConstants.RESPONSE_NOTSUCCESS)
        //                {
        //                    imageContainerPanel.Visibility = Visibility.Visible;
        //                    IsRightArrowVisible = Visibility.Hidden;
        //                    IsSuccess = false;
        //                }
        //                else
        //                {
        //                    IsSuccess = true;
        //                    if (success == SettingsConstants.RESPONSE_SUCCESS)
        //                    {
        //                        SelectedIndex1 = SelectedIndex1 + 1;
        //                        ChangeDiscoverList();
        //                    }
        //                    imageContainerPanel.Visibility = Visibility.Visible;
        //                }
        //                nextBtn.IsEnabled = true;
        //            });
        //        };
        //        thrd.StartThread();
        //    }
        //    StartAnimation(false);
        //}

        private void sliderListBox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error:PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void cntryPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OnCountryCommand();
        }

        private void categoryPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OnCategoryCommand();
        }

        private void prevBtn_Click(object sender, RoutedEventArgs e)
        {
            //Hints: For Previous, Viewport is -630 to 0.
            try
            {
                //_Timer.Stop();

                int firstIndex = 0, middleIndex = 1, lastIndex = 2;

                if (_IsFirstTimePrev == false)
                {

                    if (CelebritiesDiscoverList.Count > 2)
                    {
                        CelebritiesDiscoverList.InvokeMove(middleIndex, lastIndex);
                        CelebritiesDiscoverList.InvokeMove(firstIndex, middleIndex);
                        CelebritiesDiscoverList.RemoveAt(firstIndex);

                        CelebrityModel celModel = SearchedDiscovery.ElementAt(_NoOfSwappingCount);
                        CelebritiesDiscoverList.InvokeInsert(firstIndex, celModel);
                    }
                }

                _IsFirstTimeNext = true;
                _IsFirstTimePrev = false;

                OnPrevious(null);
                _NextPrevCount--;

                if (_NoOfSwappingCount > 0)
                {
                    _NoOfSwappingCount = _NoOfSwappingCount - 1;
                }
                IsSuccess = true;
                SetNextPrevoiusButtonVisibility();
                //_Timer.Start();
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCCelebritiesDiscoverPanel).Name).Error("Error: prevBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }


        private void nextBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //_Timer.Stop();
                if (_NextPrevCount < SearchedDiscovery.Count)
                {
                    if (_NextPrevCount == (SearchedDiscovery.Count - 2) && IsSuccess)
                    {
                        SeeMoreRequestToServer();
                    }

                    if (_IsFirstTimeNext == false)
                    {
                        int firstIndex = 0, middleIndex = 1, lastIndex = 2, nextLoadingIndex = (_NextPrevCount + lastIndex) % SearchedDiscovery.Count;
                        CelebritiesDiscoverList.InvokeMove(firstIndex, middleIndex);
                        if (CelebritiesDiscoverList.Count > 2)
                        {
                            CelebritiesDiscoverList.InvokeMove(middleIndex, lastIndex);
                            CelebritiesDiscoverList.RemoveAt(lastIndex);
                            CelebrityModel celModel = SearchedDiscovery.ElementAt(nextLoadingIndex);
                            CelebritiesDiscoverList.InvokeInsert(lastIndex, celModel);

                            if (_NoOfSwappingCount == SearchedDiscovery.Count - 1)
                            {
                                _NoOfSwappingCount = 0;
                            }

                            else
                            {
                                _NoOfSwappingCount++;
                            }
                        }
                    }

                    _IsFirstTimeNext = false; // firstTime no swapping make, just changing viewport from 0 to -630.
                    _IsFirstTimePrev = true;

                    _NextPrevCount++;
                    OnNext(null);
                    SetNextPrevoiusButtonVisibility();
                    Console.WriteLine("RingIDViewModel.Instance.CelebritiesDiscovered = " + RingIDViewModel.Instance.CelebritiesDiscovered.Count);
                    Console.WriteLine("_NextPrevCount = " + _NextPrevCount);
                }
                else
                {
                    if (IsSuccess)
                        SeeMoreRequestToServer();
                    SetNextPrevoiusButtonVisibility();
                }
                //_Timer.Start();
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCCelebritiesDiscoverPanel).Name).Error("Error: nextBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SeeMoreRequestToServer()
        {
            //CelebrityDiscoverPopularorFollowingList thrd = new CelebrityDiscoverPopularorFollowingList(SettingsConstants.CELEBRITY_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_CELEBRITY, CountryName, CategoryId, SearchedDiscovery.Count);
            //thrd.callBackEvent += (success) =>
            //{
            //    Application.Current.Dispatcher.Invoke(() =>
            //    {
            //        if (success == SettingsConstants.RESPONSE_NOTSUCCESS)
            //        {
            //            IsSuccess = false;
            //        }
            //        else
            //        {
            //            IsSuccess = true;
            //        }
            //        nextBtn.IsEnabled = true;
            //    });
            //};
            //thrd.StartThread();
        }
        private void HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as VirtualizingStackPanel;
        }
        #endregion

        #region "Private Method"
        private void RequestToGetDsicoverCelebrities()
        {
            //CelebrityDiscoverPopularorFollowingList thrd = new CelebrityDiscoverPopularorFollowingList(SettingsConstants.CELEBRITY_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_CELEBRITY, CountryName, CategoryId);

            //thrd.callBackEvent += (success) =>
            //{
            //    Application.Current.Dispatcher.Invoke(() =>
            //    {
            //        if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            //        if (success == SettingsConstants.RESPONSE_NOTSUCCESS)
            //        {

            //            IsRightArrowVisible = Visibility.Hidden;
            //            IsSuccess = false;
            //            noData.Visibility = Visibility.Visible;
            //        }
            //        else if (success == SettingsConstants.NO_RESPONSE)
            //        {
            //            IsSuccess = true;
            //            noData.Visibility = Visibility.Collapsed;
            //            reloadBtn.Visibility = Visibility.Visible;
            //        }
            //        else
            //        {
            //            IsSuccess = true;
            //            noData.Visibility = Visibility.Collapsed;

            //            if (SearchedDiscovery.Count > 0)
            //            {
            //                //ChangeDiscoverList();
            //                SetNextPrevoiusButtonVisibility();
            //                List<CelebrityModel> discoverList = SearchedDiscovery.ToList();
            //                foreach (CelebrityModel celModel in discoverList)
            //                {
            //                    if (!CelebritiesDiscoverList.Any(P => P.UserTableID == celModel.UserTableID))
            //                    {
            //                        CelebritiesDiscoverList.InvokeAdd(celModel);
            //                        if (CelebritiesDiscoverList.Count == 3)
            //                        {
            //                            break;
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    });
            //};
            //thrd.StartThread();
        }

        private void SetNextPrevoiusButtonVisibility()
        {
            if (_NextPrevCount == SearchedDiscovery.Count)
            {
                // _NextPrevCount = 0;
            }

            IsLeftArrowVisible = _NextPrevCount > 0 ? Visibility.Visible : Visibility.Collapsed;
            IsRightArrowVisible = IsSuccess ? Visibility.Visible : Visibility.Collapsed;
            //IsRightArrowVisible = _NextPrevCount < SearchedDiscovery.Count  ? Visibility.Visible : Visibility.Collapsed;
        }

        private void OnPrevious(object param)
        {
            try
            {
                int target = 0;
                _CurrentLeftMargin = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCCelebritiesDiscoverPanel).Name).Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNext(object param)
        {
            try
            {
                int target = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCCelebritiesDiscoverPanel).Name).Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(_CurrentLeftMargin, 0, 0, 0);
            MarginAnimation.To = new Thickness(target, 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
            storyboard.Begin(_ImageItemContainer);
        }

        private void ResetData()
        {
            CelebritiesDiscoverList.Clear();
            AnimationOnArrowClicked(0, 0);
            _NoOfSwappingCount = 0;
            _NextPrevCount = 0;
            _CurrentLeftMargin = 0;
            _IsFirstTimeNext = true;
            _IsFirstTimePrev = true;

            IsLeftArrowVisible = Visibility.Hidden;
            IsRightArrowVisible = Visibility.Hidden;
        }

        #endregion

        #region "Icommands"
        private ICommand _DiscoverDetailsCommand;
        public ICommand DiscoverDetailsCommand
        {
            get
            {
                if (_DiscoverDetailsCommand == null)
                {
                    _DiscoverDetailsCommand = new RelayCommand(param => OnDiscoverDetailsCommand(param));
                }
                return _DiscoverDetailsCommand;
            }
        }

        private void OnDiscoverDetailsCommand(object parameter)
        {
            if (parameter is CelebrityModel)
            {
                CelebrityModel model = (CelebrityModel)parameter;
                RingIDViewModel.Instance.OnCelebrityProfleButtonClicked(model);
            }
        }
        private ICommand _followBtnCommand;
        public ICommand FollowBtnCommand
        {
            get
            {
                if (_followBtnCommand == null)
                {
                    _followBtnCommand = new RelayCommand(param => OnFollowBtnCommandCommand(param));
                }
                return _followBtnCommand;
            }
        }
        private void OnFollowBtnCommandCommand(object parameter)
        {
            if (parameter is CelebrityModel)
            {
                CelebrityModel model = (CelebrityModel)parameter;
                if (model != null && !model.IsSubscribed)
                {
                    if (UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.FOLLOW_CONFIRMATION, "this celebrity"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Follow")))
                        new ThrdCelebrityFollowUnfollow(model.UserTableID, true, model).StartThread();
                }
                else
                {
                    if (UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "this celebrity"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow")))
                        new ThrdCelebrityFollowUnfollow(model.UserTableID, false, model).StartThread();
                }
            }
        }
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            reloadBtn.Visibility = Visibility.Collapsed;
            //showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
            RequestToGetDsicoverCelebrities();
        }

        ICommand _countryClick;
        public ICommand CountryClick
        {
            get
            {
                if (_countryClick == null)
                {
                    _countryClick = new RelayCommand(param => OnCountryCommand());
                }
                return _countryClick;
            }
        }
        private void OnCountryCommand()
        {
            if (UCCelebrityDiscoverCountryPopup.Instance == null)
            {
                UCCelebrityDiscoverCountryPopup.Instance = new UCCelebrityDiscoverCountryPopup();
                cntryPanel.Children.Add(UCCelebrityDiscoverCountryPopup.Instance);
            }
            UCCelebrityDiscoverCountryPopup.Instance.Show(cntryPanel, () =>
            {
                if (!CountryName.Equals(UCCelebrityDiscoverCountryPopup.Instance.CntryName))
                {
                    SearchedDiscovery.Clear();
                    ResetData();
                    noData.Visibility = Visibility.Collapsed;
                    this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

                    CountryName = UCCelebrityDiscoverCountryPopup.Instance.CntryName;

                    RequestToGetDsicoverCelebrities();
                }
                return 0;
            });
        }

        ICommand _categoryClick;
        public ICommand CategoryClick
        {
            get
            {
                if (_categoryClick == null)
                {
                    _categoryClick = new RelayCommand(param => OnCategoryCommand());
                }
                return _categoryClick;
            }
        }
        private void OnCategoryCommand()
        {
            if (UCCelebrityDiscoverCategoryPopup.Instance == null)
            {
                UCCelebrityDiscoverCategoryPopup.Instance = new UCCelebrityDiscoverCategoryPopup();
                categoryPanel.Children.Add(UCCelebrityDiscoverCategoryPopup.Instance);
            }
            UCCelebrityDiscoverCategoryPopup.Instance.Show(categoryPanel, () =>
            {
                if (!CategoryName.Equals(UCCelebrityDiscoverCategoryPopup.Instance.CatName))
                {
                    SearchedDiscovery.Clear();
                    ResetData();
                    noData.Visibility = Visibility.Collapsed;
                    this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

                    CategoryId = UCCelebrityDiscoverCategoryPopup.Instance.CatID;
                    CategoryName = UCCelebrityDiscoverCategoryPopup.Instance.CatName;

                    RequestToGetDsicoverCelebrities();
                }
                return 0;
            });
        }

        #endregion
    }
}

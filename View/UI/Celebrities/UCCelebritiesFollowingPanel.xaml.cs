﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Celebrities
{
    /// <summary>
    /// Interaction logic for UCCelebritiesFollowingPanel.xaml
    /// </summary>
    public partial class UCCelebritiesFollowingPanel : UserControl, INotifyPropertyChanged
    {
        public CustomScrollViewer scroll;
        private ObservableCollection<CelebrityModel> _FollowingCelebs = new ObservableCollection<CelebrityModel>();
        public ObservableCollection<CelebrityModel> FollowingCelebs
        {
            get
            {
                return _FollowingCelebs;
            }
            set
            {
                _FollowingCelebs = value;
                this.OnPropertyChanged("FollowingCelebs");
            }
        }
        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
        public UCCelebritiesFollowingPanel(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;

            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));

            RequestToGetFollowingCelebs();
        }

        private void RequestToGetFollowingCelebs()
        {
            CelebrityDiscoverPopularorFollowingList thrd = new CelebrityDiscoverPopularorFollowingList(SettingsConstants.CELEBRITY_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_CELEBRITY, st: FollowingCelebs.Count);
            thrd.callBackEvent += (success) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                    showMoreLoader.Visibility = Visibility.Collapsed;
                    if (success == SettingsConstants.RESPONSE_NOTSUCCESS)
                    {
                        showMoreBtn.Visibility = Visibility.Collapsed;
                        if (FollowingCelebs.Count == 0)
                            noMoreTxt.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        showMoreBtn.Visibility = Visibility.Visible;
                    }
                });
            };
            thrd.StartThread();
        }

        private void LoadMore_ButtonClick(object sender, RoutedEventArgs e)
        {
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            RequestToGetFollowingCelebs();
        }

        private void CelebrityName_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock tb = (TextBlock)sender;
            if (tb.DataContext is CelebrityModel)
            {
                RingIDViewModel.Instance.OnCelebrityProfleButtonClicked((CelebrityModel)tb.DataContext);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            FollowingCelebs = RingIDViewModel.Instance.CelebritiesFollowing;
            showMoreBtn.Click += LoadMore_ButtonClick;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            FollowingCelebs = null;
            showMoreBtn.Click -= LoadMore_ButtonClick;
        }

        private void btnUnfollow_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.DataContext is CelebrityModel)
            {
                CelebrityModel model = (CelebrityModel)btn.DataContext;
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.SURE_WANT_TO, "unfollow"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"), String.Format("If you unfollow, {0} will be no longer on your following list.", model.FullName));
                if (isTrue)
                    new ThrdCelebrityFollowUnfollow(model.UserTableID, false, model).StartThread();
            }
        }
    }
}

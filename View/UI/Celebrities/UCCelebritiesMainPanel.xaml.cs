﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Constants;
using View.Utility;

namespace View.UI.Celebrities
{
    /// <summary>
    /// Interaction logic for UCCelebritiesMainPanel.xaml
    /// </summary>
    public partial class UCCelebritiesMainPanel : UserControl, INotifyPropertyChanged
    {
        public UCCelebritiesFeedPanel _UCCelebritiesFeedPanel = null;
        public UCCelebritiesDiscoverPanel _UCCelebritiesDiscoverPanel = null;
        public UCCelebritiesFollowingPanel _UCCelebritiesFollowingPanel = null;
        public UCCelebritiesSavedFeed _UCCelebritiesSavedFeed = null;
        //public UCCelebritiesSearchPanel _UCCelebritiesSearchPanel = null;

        public UCCelebritiesMainPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeCelebrity, TabType);
            SwitchToTab(TabType);
            //NewsPortalNewsSelection();
        }

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"

        private string _TitleName = "Celebrities";
        public string TitleName
        {
            get { return _TitleName; }
            set
            {
                if (value == _TitleName)
                    return;
                _TitleName = value;
                this.OnPropertyChanged("TitleName");
            }
        }

        private bool _IsTabView = true;
        public bool IsTabView
        {
            get { return _IsTabView; }
            set
            {
                if (value == _IsTabView)
                    return;
                _IsTabView = value;
                this.OnPropertyChanged("IsTabView");
            }
        }

        private int _TabType = 0;
        public int TabType
        {
            get { return _TabType; }
            set
            {
                _TabType = value;
                OnPropertyChanged("TabType");
            }
        }
        #endregion

        private void BacktoAlbumsResults()
        {
            tabContainerBorder.Visibility = Visibility.Visible;
            searchContentsBdr.Visibility = Visibility.Collapsed;
            if (searchContentsBdr.Child != null)
                searchContentsBdr.Child = null;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            View.ViewModel.RingIDViewModel.Instance.CelebritesSelection = true;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            View.ViewModel.RingIDViewModel.Instance.CelebritesSelection = false;
        }
        #region "ICommand"
        private ICommand _BtnTabCommand;
        public ICommand BtnTabCommand
        {
            get
            {
                if (_BtnTabCommand == null)
                {
                    _BtnTabCommand = new RelayCommand(param => OnBtnTabCommandClicked(param));
                }
                return _BtnTabCommand;
            }
        }

        public void OnBtnTabCommandClicked(object parameter)
        {
            if (parameter is string)
            {
                int TabIdx = Convert.ToInt32(parameter);
                MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeCelebrity, TabIdx);
                SwitchToTab(TabIdx);
            }
        }

        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommandClicked(param));
                }
                return _BackCommand;
            }
        }

        public void OnBackCommandClicked(object parameter)
        {
            tabContainerBorder.Visibility = Visibility.Visible;
            searchContentsBdr.Visibility = Visibility.Collapsed;
            IsTabView = true;
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }
        private ICommand _SearchCommand;
        public ICommand SearchCommand
        {
            get
            {
                if (_SearchCommand == null)
                {
                    _SearchCommand = new RelayCommand(param => OnSearchCommandClicked(param));
                }
                return _SearchCommand;
            }
        }

        public void OnSearchCommandClicked(object parameter)
        {
            tabContainerBorder.Visibility = Visibility.Collapsed;
            searchContentsBdr.Visibility = Visibility.Visible;
            IsTabView = false;
            if (UCMiddlePanelSwitcher._UCCelebritiesSearchPanel == null) UCMiddlePanelSwitcher._UCCelebritiesSearchPanel = new UCCelebritiesSearchPanel(ScrlViewer);
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeCelebritySearch, 0);
            searchContentsBdr.Child = UCMiddlePanelSwitcher._UCCelebritiesSearchPanel;
        }
        #endregion

        #region "Private Method"
        public void SwitchToTab(int SelectedIndex)
        {
            try
            {
                //string tabItem = ((sender as TabControl).SelectedItem as TabItem).Name as string;
                if (TabType != SelectedIndex)
                    TabType = SelectedIndex;
                switch (SelectedIndex)
                {
                    case 0://"FeedsTab":
                        if (_UCCelebritiesFeedPanel == null)
                        {
                            _UCCelebritiesFeedPanel = new UCCelebritiesFeedPanel(ScrlViewer);
                        }
                        tabContainerBorder.Child = _UCCelebritiesFeedPanel;
                        break;
                    case 1://"DiscoverTab":
                        if (_UCCelebritiesDiscoverPanel == null)
                        {
                            _UCCelebritiesDiscoverPanel = new UCCelebritiesDiscoverPanel(ScrlViewer);
                        }
                        tabContainerBorder.Child = _UCCelebritiesDiscoverPanel;
                        break;
                    case 2://"FollowingTab":
                        if (_UCCelebritiesFollowingPanel == null)
                        {
                            _UCCelebritiesFollowingPanel = new UCCelebritiesFollowingPanel(ScrlViewer);
                        }
                        tabContainerBorder.Child = _UCCelebritiesFollowingPanel;
                        break;
                    case 3://"PlaylistTab":
                        if (_UCCelebritiesSavedFeed == null)
                            _UCCelebritiesSavedFeed = new UCCelebritiesSavedFeed(ScrlViewer);

                        tabContainerBorder.Child = _UCCelebritiesSavedFeed;
                        break;
                }
            }
            catch (System.Exception) { }
        }
        #endregion
    }
}

﻿using Auth.utility;
using log4net;
using Models.Constants;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.ViewModel;

namespace View.UI.Celebrities
{
    /// <summary>
    /// Interaction logic for UCCelebritiesSavedFeed.xaml
    /// </summary>
    public partial class UCCelebritiesSavedFeed : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCelebritiesSavedFeed).Name);
        public CustomScrollViewer scroll;

        #region "Constructor"
        public UCCelebritiesSavedFeed(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            //ViewCollection = RingIDViewModel.Instance.SavedFeedsCelebrity;
            scroll.SetScrollValues(ViewCollection, FeedDataContainer.Instance.SavedFeedsCelebritySortedIds, SettingsConstants.PROFILE_TYPE_CELEBRITY, AppConstants.TYPE_ALL_SAVED_FEEDS);
            if (DefaultSettings.SAVEDCELEBRITY_STARTPKT == null)
            {
                DefaultSettings.SAVEDCELEBRITY_STARTPKT = SendToServer.GetRanDomPacketID();
                if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                    scroll.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDCELEBRITY_STARTPKT);
            }
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Property
        private CustomObservableCollection _ViewCollection;
        public CustomObservableCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }
        #endregion

        #region "Private Methods"
        #endregion

        #region "Public Methods"
        #endregion

        #region "Event Triggers"
        private void UserControl_Loaded(object sender, RoutedEventArgs args)
        {
            scroll.Focusable = true;
            scroll.FocusVisualStyle = null;
            if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                Keyboard.Focus(scroll);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs args)
        {
            //TODO if (BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
            //{
            //    scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
            //    scroll.PreviewKeyDown -= feedScrlViewer_PreviewKeyDown;
            //}
        }

        #endregion
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }
        private void OnReloadCommandClicked()
        {
            DefaultSettings.SAVEDCELEBRITY_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDCELEBRITY_STARTPKT);
        }

        #region "Utility Methods"

        #endregion "Utility Methods"
    }
}

﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;
using System.Linq;
using View.ViewModel;

namespace View.UI.Celebrities
{
    /// <summary>
    /// Interaction logic for UCCelebritiesFeedPanel.xaml
    /// </summary>
    public partial class UCCelebritiesFeedPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCCelebritiesFeedPanel).Name);

        public CustomScrollViewer scroll;
        private Visibility _IsLeftArrowVisible = Visibility.Hidden;
        private Visibility _IsRightArrowVisible = Visibility.Hidden;
        private System.Timers.Timer _Timer;
        public bool _IsTimerStart = false;
        private VirtualizingStackPanel _ImageItemContainer = null;
        private int _CurrentLeftMargin = 0;
        private int _NextPrevCount = 0;
        private bool _IsFirstTimePrev = true;
        private bool _IsFirstTimeNext = true;
        private int _NoOfSwappingCount = 0;
        bool IsSuccess = true;
        bool IsFirstTime = true;

        public UCCelebritiesFeedPanel(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            //ViewCollection = RingIDViewModel.Instance.AllCelebrityFeeds;
            scroll.SetScrollValues(ViewCollection, FeedDataContainer.Instance.AllCelebrityFeedsSortedIds, SettingsConstants.PROFILE_TYPE_CELEBRITY, AppConstants.TYPE_CELEBRITY_FEED);
            DefaultSettings.ALLCELEBRITY_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.ALLCELEBRITY_STARTPKT);
            //TODO celeb popular
            //RequestToGetPopularCelebrities();
        }

        #region "INotifyPropertyChanged"

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"

        private ObservableCollection<CelebrityModel> _SliderPopularCelebrity;
        public ObservableCollection<CelebrityModel> SliderPopularCelebrity
        {
            get { return _SliderPopularCelebrity; }
            set
            {
                _SliderPopularCelebrity = value;
                this.OnPropertyChanged("SliderPopularCelebrity");
            }
        }

        private ObservableCollection<CelebrityModel> _SliderCelebrityCollection = new ObservableCollection<CelebrityModel>();
        public ObservableCollection<CelebrityModel> SliderCelebrityCollection
        {
            get { return _SliderCelebrityCollection; }
            set
            {
                _SliderCelebrityCollection = value;
                this.OnPropertyChanged("SliderCelebrityCollection");
            }
        }

        private CustomObservableCollection _ViewCollection;
        public CustomObservableCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }
        #endregion

        #region "Private Method"
        private void RequestToGetPopularCelebrities()
        {
            CelebrityDiscoverPopularorFollowingList thrd = new CelebrityDiscoverPopularorFollowingList(SettingsConstants.CELEBRITY_TYPE_POPULAR, SettingsConstants.PROFILE_TYPE_CELEBRITY, st: SliderPopularCelebrity.Count);

            thrd.callBackEvent += (success) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (success == SettingsConstants.RESPONSE_NOTSUCCESS)
                    {
                        IsRightArrowVisible = Visibility.Hidden;
                        IsSuccess = false;
                    }
                    else
                    {
                        if (IsFirstTime)
                        {
                            SliderListBox_Loaded(null, null);
                            IsFirstTime = false;
                        }
                        IsRightArrowVisible = Visibility.Visible;
                        IsSuccess = true;
                    }
                });
            };
            thrd.StartThread();
        }

        private void StartBannerNewsSliding()
        {
            if (_Timer == null)
            {
                _Timer = new System.Timers.Timer();
                _Timer.Interval = 2500;
                _IsTimerStart = true;
                _Timer.Elapsed += timer_Elapsed;
                _Timer.Start();
            }
        }

        private void SetNextPrevoiusButtonVisibility()
        {
            if (SliderPopularCelebrity != null && _NextPrevCount == SliderPopularCelebrity.Count)
            {
                _NextPrevCount = 0;
            }

            IsLeftArrowVisible = _NextPrevCount > 0 ? Visibility.Visible : Visibility.Collapsed;
            //IsRightArrowVisible = SliderPopularCelebrity != null && _NextPrevCount < SliderPopularCelebrity.Count - 1 ? Visibility.Visible : Visibility.Collapsed;
            if (SliderPopularCelebrity != null && _NextPrevCount < SliderPopularCelebrity.Count - 1)
            {
                IsRightArrowVisible = Visibility.Visible;
            }
            else
            {
                IsRightArrowVisible = Visibility.Collapsed;
                if (IsSuccess)
                    RequestToGetPopularCelebrities();
            }
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    nextBtn_Click(null, null);
                }
                catch (Exception ex)
                {
                    log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(_CurrentLeftMargin, 0, 0, 0);
            MarginAnimation.To = new Thickness(target, 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
            storyboard.Begin(_ImageItemContainer);
        }

        private void OnPrevious(object param)
        {
            try
            {
                int target = 0;
                _CurrentLeftMargin = -320;
                AnimationOnArrowClicked(target, 0.25);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNext(object param)
        {
            try
            {
                //int target = _CurrentLeftMargin - (int)pnlImageContainerWrapper.ActualWidth;
                int target = -320;
                AnimationOnArrowClicked(target, 0.25);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ResetData()
        {
            if (_Timer != null)
            {
                _Timer.Elapsed -= timer_Elapsed;
                _Timer.Stop();
                _Timer.Enabled = false;
                _Timer = null;
            }

            AnimationOnArrowClicked(0, 0);
            _NoOfSwappingCount = 0;
            _NextPrevCount = 0;
            _CurrentLeftMargin = 0;
            _IsFirstTimeNext = true;
            _IsFirstTimePrev = true;
            _IsTimerStart = false;

            IsLeftArrowVisible = Visibility.Hidden;
            IsRightArrowVisible = Visibility.Hidden;
        }
        public void LoadSliderData()
        {
            try
            {
                if (SliderPopularCelebrity != null && SliderPopularCelebrity.Count > 0)
                {
                    List<CelebrityModel> breakingNws = SliderPopularCelebrity.ToList();
                    foreach (CelebrityModel model in breakingNws)
                    {
                        if (!SliderCelebrityCollection.Any(P => P.UserTableID == model.UserTableID))
                        {
                            SliderCelebrityCollection.InvokeAdd(model);
                            if (SliderCelebrityCollection.Count == 4)
                            {
                                break;
                            }
                        }
                    }

                    if (SliderCelebrityCollection.Count > 2)
                    {
                        SetNextPrevoiusButtonVisibility();
                        StartBannerNewsSliding();
                    }
                }
            }

            catch (Exception ex)
            {
                log.Error("Error: LoadSliderData() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private DispatcherTimer timer;
        private void TimerEventProcessor(object sender, EventArgs e)
        {
            if (timer != null && timer.IsEnabled)
            {
                //ViewCollection = RingIDViewModel.Instance.AllCelebrityFeeds;
                scroll.SetScrollEvents(true);
                if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                    Keyboard.Focus(scroll);
                timer.Stop();
            }
        }
        #endregion

        #region "Event Trigger"
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                SliderPopularCelebrity = RingIDViewModel.Instance.CelebritiesPopular;
                RequestToGetPopularCelebrities();
                scroll.ScrollToHome();
                if (ViewCollection == null)
                {
                    if (timer == null)
                    {
                        timer = new DispatcherTimer();
                        timer.Interval = TimeSpan.FromMilliseconds(500);
                        timer.Tick += TimerEventProcessor;
                    }
                    else timer.Stop();
                    timer.Start();
                }
                //((INotifyCollectionChanged)SliderCelebrityCollection).CollectionChanged += SliderCelebrityCollection_CollectionChanged;
                sliderListBox.Loaded += SliderListBox_Loaded;
                sliderListBox.PreviewMouseWheel += sliderListBox_PreviewMouseWheel;
                prevBtn.Click += prevBtn_Click;
                nextBtn.Click += nextBtn_Click;
                LeftArrowPanel.MouseLeftButtonDown += LeftArrowPanel_MouseLeftButtonUp;
                RightArrowPanel.MouseLeftButtonDown += RightArrowPanel_MouseLeftButtonUp;
            }
            else
            {
                //if (MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView == null || !RingPlayerViewModel.Instance.FullScreen)
                //{
                scroll.SetScrollEvents(false);
                ViewCollection = null;
                //}
                //if (SliderPopularCelebrityCollection != null)
                //    ((INotifyCollectionChanged)SliderCelebrityCollection).CollectionChanged -= SliderCelebrityCollection_CollectionChanged;

                sliderListBox.Loaded -= SliderListBox_Loaded;
                sliderListBox.PreviewMouseWheel -= sliderListBox_PreviewMouseWheel;
                prevBtn.Click -= prevBtn_Click;
                nextBtn.Click -= nextBtn_Click;
                LeftArrowPanel.MouseLeftButtonDown -= LeftArrowPanel_MouseLeftButtonUp;
                RightArrowPanel.MouseLeftButtonDown -= RightArrowPanel_MouseLeftButtonUp;
                if (_ImageItemContainer != null)
                    ResetData();
                if (SliderCelebrityCollection != null)
                    SliderCelebrityCollection.Clear();
                SliderPopularCelebrity = null;
            }
        }

        private void SliderListBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (SliderPopularCelebrity != null && SliderPopularCelebrity.Count > 1)
            {
                LoadSliderData();
            }
        }

        private void HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as VirtualizingStackPanel;
        }

        //private void sliderListBox_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        //{
        //    if (SliderTimer != null)
        //    {
        //        SliderTimer.Stop();
        //        SliderTimer.Enabled = false;
        //    }
        //}

        private void sliderListBox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error:PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LeftArrowPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            prevBtn_Click(null, null);
        }

        private void RightArrowPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            nextBtn_Click(null, null);
        }
        private void prevBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_Timer != null)
                    _Timer.Stop();

                if (_IsFirstTimePrev == false)
                {
                    int firstIndex = 0, firstmiddleIndex = 1, secondmiddleIndex = 2, lastIndex = 3, prevLoadingIndex = (_NextPrevCount + lastIndex) % SliderPopularCelebrity.Count;
                    SliderCelebrityCollection.InvokeMove(lastIndex, secondmiddleIndex);
                    SliderCelebrityCollection.InvokeMove(secondmiddleIndex, firstmiddleIndex);
                    if (SliderCelebrityCollection.Count > 3)
                    {
                        SliderCelebrityCollection.InvokeMove(firstmiddleIndex, firstIndex);
                        SliderCelebrityCollection.RemoveAt(firstIndex);
                        CelebrityModel model = SliderPopularCelebrity.ElementAt(prevLoadingIndex);
                        SliderCelebrityCollection.InvokeInsert(firstIndex, model);

                        if (_NoOfSwappingCount == SliderPopularCelebrity.Count - 2)
                        {
                            _NoOfSwappingCount = 0;
                        }
                        else
                        {
                            _NoOfSwappingCount++;
                        }
                    }
                }

                _IsFirstTimeNext = true;
                _IsFirstTimePrev = false;

                OnPrevious(null);
                _NextPrevCount--;

                if (_NoOfSwappingCount > 0)
                {
                    _NoOfSwappingCount = _NoOfSwappingCount - 1;
                }

                SetNextPrevoiusButtonVisibility();
                if (_Timer != null)
                    _Timer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: prevBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void nextBtn_Click(object sender, RoutedEventArgs e)
        {
            //Hints: For AutoSlide/Next, Viewport is 0 to -630.
            try
            {
                if (_Timer != null)
                    _Timer.Stop();
                if (_IsFirstTimeNext == false)
                {
                    int firstIndex = 0, firstmiddleIndex = 1, secondmiddleIndex = 2, lastIndex = 3, nextLoadingIndex = (_NextPrevCount + lastIndex) % SliderPopularCelebrity.Count;
                    SliderCelebrityCollection.InvokeMove(firstIndex, firstmiddleIndex);
                    SliderCelebrityCollection.InvokeMove(firstmiddleIndex, secondmiddleIndex);
                    if (SliderCelebrityCollection.Count > 3)
                    {
                        SliderCelebrityCollection.InvokeMove(secondmiddleIndex, lastIndex);
                        SliderCelebrityCollection.RemoveAt(lastIndex);
                        CelebrityModel model = SliderPopularCelebrity.ElementAt(nextLoadingIndex);
                        SliderCelebrityCollection.InvokeInsert(lastIndex, model);

                        if (_NoOfSwappingCount == SliderPopularCelebrity.Count - 2)
                        {
                            _NoOfSwappingCount = 0;
                        }
                        else
                        {
                            _NoOfSwappingCount++;
                        }
                    }
                }

                _IsFirstTimeNext = false; // firstTime no swapping make, just changing viewport from 0 to -630.
                _IsFirstTimePrev = true;

                _NextPrevCount++;
                OnNext(null);
                SetNextPrevoiusButtonVisibility();
                if (_Timer != null)
                    _Timer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: nextBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region "Icommands"
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        private void OnReloadCommandClicked()
        {
            DefaultSettings.ALLCELEBRITY_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.ViewCollection.LoadMoreModel.ShowContinue)
                scroll.RequestFeeds(0, 2, 0, DefaultSettings.ALLCELEBRITY_STARTPKT);
        }

        ICommand _followbtnClick;
        public ICommand FollowBtnClick
        {
            get
            {
                if (_followbtnClick == null)
                {
                    _followbtnClick = new RelayCommand(param => OnFollowBtnClickedCommand(param));
                }
                return _followbtnClick;
            }
        }
        private void OnFollowBtnClickedCommand(object param)
        {
            if (param is CelebrityModel)
            {
                CelebrityModel model = (CelebrityModel)param;
                if (model != null && !model.IsSubscribed)
                {
                    if (UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.FOLLOW_CONFIRMATION, "this celebrity"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Follow")))
                        new ThrdCelebrityFollowUnfollow(model.UserTableID, true, model).StartThread();
                }
                else
                {
                    if (UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "this celebrity"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow")))
                        new ThrdCelebrityFollowUnfollow(model.UserTableID, false, model).StartThread();
                }
            }
        }
        ICommand _ProfileCommand;
        public ICommand ProfileCommand
        {
            get
            {
                if (_ProfileCommand == null)
                {
                    _ProfileCommand = new RelayCommand(param => OnProfileCommand(param));
                }
                return _ProfileCommand;
            }
        }
        private void OnProfileCommand(object param)
        {
            if (param is CelebrityModel)
            {
                CelebrityModel model = (CelebrityModel)param;
                RingIDViewModel.Instance.OnCelebrityProfleButtonClicked(model);
            }
        }
        #endregion
    }
}
//private void LeftArrowPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
//{
//    try
//    {
//        if (SliderTimer != null)
//        {
//            SliderTimer.Stop();
//            SliderTimer.Start();
//            OnPrevious(null);
//        }
//        //else
//        //{
//        //    SliderTimer = new System.Timers.Timer();
//        //    SliderTimer.Interval = 2000;
//        //    _IsTimerStart = true;
//        //    SliderTimer.Start();
//        //    OnPrevious(null);
//        //}
//    }
//    catch (Exception ex)
//    {
//        log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
//    }
//}

//private void RightArrowPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
//{
//    try
//    {
//        if (SliderTimer != null)
//        {
//            SliderTimer.Stop();
//            SliderTimer.Start();
//            OnNext(null);
//        }
//        else
//        {
//            SliderTimer = new System.Timers.Timer();
//            SliderTimer.Interval = 2000;
//            _IsTimerStart = true;
//            SliderTimer.Start();
//            OnNext(null);
//        }
//    }
//    catch (Exception ex)
//    {
//        log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
//    }
//}
//private void prevBtn_Click(object sender, RoutedEventArgs e)
//{
//    LeftArrowPanel_MouseLeftButtonDown(null, null);
//}
//private void OnPrevious(object param)
//{
//    try
//    {
//        int target = CurrentLeftMargin + 320;
//        AnimationOnArrowClicked(target, 0.35);
//        CurrentLeftMargin = target;
//        SliderListBox_Loaded(null, null);
//    }
//    catch (Exception ex)
//    {
//        log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
//    }
//}

//void nextBtn_Click(object sender, RoutedEventArgs e)
//{
//    RightArrowPanel_MouseLeftButtonDown(null, null);
//}

//private void OnNext(object param)
//{
//    try
//    {
//        int target = CurrentLeftMargin - 320;
//        AnimationOnArrowClicked(target, 0.35);
//        CurrentLeftMargin = target;
//        SliderListBox_Loaded(null, null);
//    }
//    catch (Exception ex)
//    {
//        log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
//    }
//}

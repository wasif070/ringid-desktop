﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Feed;
using View.Utility.DataContainer;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Celebrities
{
    /// <summary>
    /// Interaction logic for UCCelebritiesSearchPanel.xaml
    /// </summary>
    public partial class UCCelebritiesSearchPanel : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCCelebritiesSearchPanel).Name);

        public ScrollViewer scroll;
        //public Func<int> _OnBackToPrevious = null;
        private DispatcherTimer _Timer;

        #region "Constructor"
        public UCCelebritiesSearchPanel(CustomScrollViewer scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
        }
        #endregion

        #region "Property"
        private ObservableCollection<CelebrityModel> _SearchedCelebrities = new ObservableCollection<CelebrityModel>();
        public ObservableCollection<CelebrityModel> SearchedCelebrities
        {
            get
            {
                return _SearchedCelebrities;
            }
            set
            {
                _SearchedCelebrities = value;
                this.OnPropertyChanged("SearchedCelebrities");
            }
        }

        private int _BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
        public int BOTTOM_LOADING
        {
            get { return _BOTTOM_LOADING; }
            set
            {
                _BOTTOM_LOADING = value;
                this.OnPropertyChanged("BOTTOM_LOADING");
            }
        }
        #endregion

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Public Method"

        //public void ShowMediaCollection(Func<int> _OnBackToPrevious)
        //{
        //    this._OnBackToPrevious = _OnBackToPrevious;
        //}

        public void ShowMoreStates(int state) //0 first success,1false, 2no resp \3 success,4 false, 5 no response
        {
            if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
            //if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            //GIFCtrlLoader.Visibility = Visibility.Collapsed;
            switch (state)
            {
                case 0:
                    ShowHideShowMoreLoading(true);
                    break;
                case 1:
                    noTxt.Visibility = Visibility.Visible;
                    break;
                case 2:
                    ShowHideShowMoreLoading(true);
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK);
                    //  CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                    break;
                case 3:
                    ShowHideShowMoreLoading(true);
                    break;
                case 4:
                    ShowHideShowMoreLoading(false);
                    break;
                case 5:
                    ShowHideShowMoreLoading(true);
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK);
                    break;
            }
        }

        public void ShowHideShowMoreLoading(bool makeVisible)
        {
            try
            {
                if (makeVisible)
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                }
                else
                {
                    BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowMore() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowLoading()
        {
            try
            {
                BOTTOM_LOADING = StatusConstants.LOADING_GIF_VISIBLE;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowLoading() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }


        #endregion

        #region "Private Method"

        private void AskSuggestions()
        {
            string CurrentSearchStr = SearchTermTextBox.Text.Trim();
            SearchedCelebrities.Clear();
            SearchedCelebrities.Add(new CelebrityModel() { IsLoadMore = true });
            //BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;

            List<long> list = null;
            if (UserProfilesContainer.Instance.SearchedCelebrityIdsByParam.TryGetValue(CurrentSearchStr, out list))
            {
                foreach (var item in list)
                {
                    CelebrityModel model = null;
                    if (UserProfilesContainer.Instance.CelebrityModels.TryGetValue(item, out model))
                    {
                        if (!SearchedCelebrities.Any(P => P.UserTableID == model.UserTableID))
                            SearchedCelebrities.Insert(SearchedCelebrities.Count - 1, model);
                        int index = SearchedCelebrities.IndexOf(model);
                        if ((index + 1) % 3 == 0)
                        {
                            model.IsRightMarginOff = true;
                        }
                        else
                        {
                            model.IsRightMarginOff = false;
                        }
                    }
                }
            }
            else
            {
                SearchCelebrities(CurrentSearchStr, SettingsConstants.PROFILE_TYPE_CELEBRITY);
                //showMoreBtn.Visibility = Visibility.Collapsed;
                //showMoreLoader.Visibility = Visibility.Collapsed;
                //showMorePanel.Visibility = Visibility.Visible;
                this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
            }
        }
        private void LookUp(object sender, EventArgs e)
        {
            Dispatcher.Invoke(AskSuggestions);
            if (_Timer != null) _Timer.Stop();
        }

        private void SearchCelebrities(string searchParam, int profileType, int st = 0)
        {
            new ThradSearchNewsPortals().StartThread(searchParam, profileType, st);
        }
        #endregion

        #region "Icommands"
        //ICommand _backBtnClick;
        //public ICommand BackBtnClick
        //{
        //    get
        //    {
        //        if (_backBtnClick == null)
        //        {
        //            _backBtnClick = new RelayCommand(param => OnBackBtnClickedCommand(param));
        //        }
        //        return _backBtnClick;
        //    }
        //}
        //private void OnBackBtnClickedCommand(object param)
        //{
        //    try
        //    {
        //        if (_OnBackToPrevious != null)
        //        {
        //            _OnBackToPrevious();
        //        }
        //    }
        //    catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        //}

        ICommand _followbtnClick;
        public ICommand FollowBtnClick
        {
            get
            {
                if (_followbtnClick == null)
                {
                    _followbtnClick = new RelayCommand(param => OnFollowBtnClickedCommand(param));
                }
                return _followbtnClick;
            }
        }
        private void OnFollowBtnClickedCommand(object param)
        {
            if (param is CelebrityModel)
            {
                CelebrityModel model = (CelebrityModel)param;
                if (model != null && !model.IsSubscribed)
                {
                    if (UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.FOLLOW_CONFIRMATION, "this celebrity"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Follow")))
                        new ThrdCelebrityFollowUnfollow(model.UserTableID, true, model).StartThread();
                }
                else
                {
                    if (UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "this celebrity"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow")))
                        new ThrdCelebrityFollowUnfollow(model.UserTableID, false, model).StartThread();
                }
            }
        }
        #endregion

        #region "Event Trigger"
        private void discoverMainPreviewPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Border bd = (Border)sender;
            CelebrityModel model = (CelebrityModel)bd.DataContext;
            RingIDViewModel.Instance.OnCelebrityProfleButtonClicked(model);
        }

        private void SearchTermTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            noTxt.Visibility = Visibility.Collapsed;
            if (SearchTermTextBox.Text.Trim().Length > 0)//if (CurrentSearchStr.Length > 0)
            {
                if (_Timer == null)
                {
                    _Timer = new DispatcherTimer(DispatcherPriority.Background);
                    _Timer.Interval = TimeSpan.FromSeconds(1); //100ms
                    _Timer.Tick += LookUp;
                    _Timer.Start();
                }
                else if (!_Timer.IsEnabled)
                {
                    _Timer.Start();
                }
            }
            else  //load default Values
            {
                //showMoreBtn.Visibility = Visibility.Collapsed;
                //showMoreLoader.Visibility = Visibility.Collapsed;
                if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                //if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                SearchedCelebrities.Clear();
                _Timer.Stop();
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.TextChanged += SearchTermTextBox_TextChanged;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.TextChanged -= SearchTermTextBox_TextChanged;
            SearchedCelebrities.Clear();
            SearchTermTextBox.Text = "";
            noTxt.Visibility = Visibility.Collapsed;
        }
        #endregion

        private void ShowMore_PanelClick(object sender, MouseButtonEventArgs e)
        {
            if (BOTTOM_LOADING == StatusConstants.LOADING_TEXT_VISIBLE)
            {
                SearchCelebrities(SearchTermTextBox.Text.Trim(), SettingsConstants.PROFILE_TYPE_CELEBRITY, SearchedCelebrities.Count);
                ShowLoading();
            }
            e.Handled = true;
        }
    }
}

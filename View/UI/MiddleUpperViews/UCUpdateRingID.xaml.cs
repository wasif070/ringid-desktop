﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Models.Constants;
using View.Utility;

namespace View.UI.MiddleUpperViews
{
    /// <summary>
    /// Interaction logic for UCUpdateRingID.xaml
    /// </summary>
    public partial class UCUpdateRingID : UserControl, INotifyPropertyChanged
    {
        public UCUpdateRingID(string updateVersion=null)
        {
            InitializeComponent();
            this.DataContext = this;
            RingIDUPdateIcon = View.Utility.ImageObjects.UPDATE_VERSION_ICON;
            setUpdaterText(updateVersion);
            this.Loaded += UCUpdateRingID_Loaded;
            this.Unloaded += UCUpdateRingID_Unloaded;
        }

        #region "Property"
        private ImageSource ringIDUPdateIcon;
        public ImageSource RingIDUPdateIcon
        {
            get
            {
                return ringIDUPdateIcon;
            }
            set
            {
                if (value == ringIDUPdateIcon)
                    return;
                ringIDUPdateIcon = value;
                OnPropertyChanged("RingIDUPdateIcon");
            }
        }

        private string updateText = NotificationMessages.UPDATE_VERSION_TEXT;

        public string UpdateText
        {
            get { return updateText; }
            set
            {
                if (value == updateText)
                    return;
                updateText = value;
                OnPropertyChanged("UpdateText");
            }
        }

        private string currentVersionText = string.Empty;

        public string CurrentVersionText
        {
            get { return currentVersionText; }
            set { currentVersionText = value; OnPropertyChanged("CurrentVersionText"); }
        }

        #endregion "Property"

        #region "Event Trigger"
        void UCUpdateRingID_Loaded(object sender, RoutedEventArgs e)
        {
            btnDownload.Click += btnDownload_Click;
            btnDecideLater.Click += btnDecideLater_Click;
        }
        void UCUpdateRingID_Unloaded(object sender, RoutedEventArgs e)
        {
            btnDownload.Click -= btnDownload_Click;
            btnDecideLater.Click -= btnDecideLater_Click;
            this.Loaded -= UCUpdateRingID_Loaded;
            this.Unloaded -= UCUpdateRingID_Unloaded;
        }
        void btnDecideLater_Click(object sender, RoutedEventArgs e)
        {
            if (UCGuiRingID.Instance != null)
            {
                UCGuiRingID.Instance.ClearUpperMiddlePanel();
            }
        }

        void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            HelperMethods.Runupdater(true);
            if (UCGuiRingID.Instance != null)
            {
                UCGuiRingID.Instance.ClearUpperMiddlePanel();
            }
        }
        #endregion "Event Trigger"

        #region Private Method
        private void setUpdaterText(string updateVersion)
        {
            CurrentVersionText = string.Format(NotificationMessages.CURRENT_VERSION_TEXT_WITH_VERSION_NUMBER, AppConfig.DESKTOP_REALEASE_VERSION);
            UpdateText = !string.IsNullOrEmpty(updateVersion)?  string.Format(NotificationMessages.UPDATE_VERSION_TEXT_WITH_VERSION_NUMBER, updateVersion) : NotificationMessages.UPDATE_VERSION_TEXT;
        }
        #endregion

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion "INotifyPropertyChanged"


    }
}

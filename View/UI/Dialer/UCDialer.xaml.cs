﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.BindingModels;
using View.Utility;
using View.Utility.FriendList;
using View.ViewModel;
using View.Utility.WPFMessageBox;
using System.Windows.Threading;
using System.Windows.Media;
using System.Timers;
using View.Utility.Auth;
using View.UI.PopUp;

namespace View.UI.Dialer
{
    /// <summary>
    /// Interaction logic for UCDialer.xaml
    /// </summary>
    public partial class UCDialer : UserControl, INotifyPropertyChanged
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(UCDialer).Name);
        public static UCDialer Instance;
        private long _SelectedFriendUserTableId = 0;
        private bool _NeedToChangeSelectedUI = false;
        private UserBasicInfoModel _SelectedFriendModel;
        private string _SearchString = String.Empty;
        private ICommand _AddFriendButtonCommand;
        private ICommand _OpenPopupCommand;

        private bool IsSelectedAnyFriend = false;
        private bool isBackSapce = false;
        private Timer SearchTextBoxTimer;
        private int ThreadMaxRunningTime = 0;

        public UCDialer()
        {
            InitializeComponent();
            Instance = this;
            this.DataContext = this;
            this.Loaded += UCDialer_Loaded;
            this.Unloaded += UCDialer_Unloaded;
        }
        void UCDialer_Loaded(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.Focus();
            AddRemoveInCollections.ClearTempFriendList();
            SearchTextBoxTimer = new Timer();
            SearchTermTextBox.LostFocus += UCDialer_LostFocus;
            SearchTermTextBox.GotKeyboardFocus += SearchTermTextBox_GotKeyboardFocus;
            DialPadMaximize_Click(new object(), null);
            RingIDViewModel.Instance.DialPadButtionSelection = true;
        }

        void SearchTermTextBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(SearchTermTextBox.Text) && RingIDViewModel.Instance.TempMyFriendList.Count > 0)
            {
                RingIDViewModel.Instance.TempFriendList.Clear();
            }
        }
        void UCDialer_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SetDefaultParameter();
                if (SearchTextBoxTimer != null) { SearchTextBoxTimer.Stop(); }
                SearchTextBoxTimer.Enabled = false;
                SearchTextBoxTimer = null;
                SearchTermTextBox.Text = string.Empty;
                ThrdSearchFriend.searchParm = null;
                SelectedFriendUserTableId = 0;
                SelectedFriendModel = null;
                AddRemoveInCollections.ClearTempFriendList();
                SearchTermTextBox.LostFocus -= UCDialer_LostFocus;
                SearchTermTextBox.GotKeyboardFocus -= SearchTermTextBox_GotKeyboardFocus;
                RingIDViewModel.Instance.DialPadButtionSelection = false;
            }
            catch (Exception)
            {
            }
            finally
            {
                ThrdSearchFriend.runningSearchFriendThread = false;
            }
        }
        #region "Event Listner"

        void UCDialer_LostFocus(object sender, RoutedEventArgs e)
        {
            ThrdSearchFriend.runningSearchFriendThread = false;
        }
        private void Control_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Control control = (Control)sender;
            IsSelectedAnyFriend = true;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            if (SelectedFriendUserTableId.Equals(model.ShortInfoModel.UserTableID))
            {
                SelectedFriendUserTableId = 0;
                SelectedFriendModel = null;
                NeedToChangeSelectedUI = false;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
            }
            else
            {
                long tempUTId = SelectedFriendUserTableId;
                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                SelectedFriendModel = model;
                if (model.ShortInfoModel.UserIdentity > 0)
                    SearchTermTextBox.Text = HelperMethods.GetDisplayRingIDfromUserID(model.ShortInfoModel.UserIdentity);
                ThrdSearchFriend.searchParm = model.ShortInfoModel.UserIdentity.ToString();
                ThrdSearchFriend.prevString = SearchTermTextBox.Text.Replace(" ", ""); ;
                SearchTermTextBox.CaretIndex = SearchTermTextBox.Text.Length;
                NeedToChangeSelectedUI = true;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
                UserBasicInfoModel tempModel = AddRemoveInCollections.getSingleTempFriend(tempUTId);
                if (tempModel != null)
                {
                    tempModel.ShortInfoModel.OnPropertyChanged("UserTableID");
                }
                DialPadBtn.Visibility = Visibility.Visible;
                DialPadConatiner.Visibility = Visibility.Collapsed;
                Down_Arrow.Visibility = Visibility.Collapsed;
            }
        }
        private void Control_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Control control = (Control)sender;
            UserBasicInfoModel userBasicInfoModel = (UserBasicInfoModel)control.DataContext;
        }

        private void OnSearch()
        {
            try
            {
                ThrdSearchFriend.searchParm = SearchString;
                ThrdSearchFriend.processingList = false;
                SelectedFriendModel = null;
                SelectedFriendUserTableId = 0;
                NoResultsFound = false;
                NeedToChangeSelectedUI = false;
                if (!ThrdSearchFriend.runningSearchFriendThread)
                {
                    (new ThrdSearchFriend(ThrdSearchFriend.SearchFromDailPad, StatusConstants.SEARCH_BY_RINGID)).StartThread();
                }
                //if (!String.IsNullOrEmpty(SearchString))
                //{
                //    CheckAnySingleFriendIsVisibleThenMakeItSelected();
                //}
                //else
                //{
                //    SelectedFriendModel = null;
                //    NeedToChangeSelectedUI = false;
                //}
            }
            catch (Exception e)
            {

                log.Error("Exception in Dialer search==>" + e.Message + "\n" + e.StackTrace);

            }
        }
        #endregion "Event Listner"
        #region Property
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public long SelectedFriendUserTableId
        {
            get { return _SelectedFriendUserTableId; }
            set
            {
                _SelectedFriendUserTableId = value;
                this.OnPropertyChanged("SelectedFriendUserTableId");
            }
        }
        public UserBasicInfoModel SelectedFriendModel
        {
            get { return _SelectedFriendModel; }
            set
            {
                _SelectedFriendModel = value;
                this.OnPropertyChanged("SelectedFriendModel");
            }
        }
        public bool NeedToChangeSelectedUI
        {
            get { return _NeedToChangeSelectedUI; }
            set
            {
                _NeedToChangeSelectedUI = value;
                this.OnPropertyChanged("NeedToChangeSelectedUI");
            }
        }

        private bool _NoResultsFound = false;
        public bool NoResultsFound
        {
            get
            {
                return _NoResultsFound;
            }
            set
            {
                if (value == _NoResultsFound)
                    return;
                if (string.IsNullOrWhiteSpace(ThrdSearchFriend.searchParm)) { _NoResultsFound = false; }
                else { _NoResultsFound = value; }
                OnPropertyChanged("NoResultsFound");
            }
        }

        private ImageSource _LoaderSmall;
        public ImageSource LoaderSmall
        {
            get
            {
                return _LoaderSmall;
            }
            set
            {
                if (value == _LoaderSmall)
                    return;
                _LoaderSmall = value;
                OnPropertyChanged("LoaderSmall");
            }
        }

        public string SearchString
        {
            get { return _SearchString; }
            set
            {
                _SearchString = value;
                _SearchString = _SearchString.Replace(" ", "");
                //OnSearch();
            }
        }

        #endregion

        #region "Dialpad Button Events"

        private void BtnAny_Click(object sender, RoutedEventArgs e)
        {
            isBackSapce = false;
            string str = (e.Source as Button).Content.ToString();
            SearchTermTextBox.Focus();
            SearchTermTextBox.SelectedText = "";
            if (SearchTermTextBox.Text.Length >= 9)
            {
                SystemSounds.Asterisk.Play();
            }
            else if (SearchTermTextBox.Text.Length < 9)
            {
                int caret = SearchTermTextBox.CaretIndex;
                SearchTermTextBox.Text = String.Format("{0}{2}{1}", SearchTermTextBox.Text.Substring(0, caret), SearchTermTextBox.Text.Substring(caret), str);
                if (caret == 4)
                    SearchTermTextBox.CaretIndex = caret + 2;
                else SearchTermTextBox.CaretIndex = caret + 1;
            }
        }

        private void ButnCross_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(SearchTermTextBox.Text))
            {
                SystemSounds.Asterisk.Play();
                RingIDViewModel.Instance.TempFriendList.Clear();
            }
            else if (!string.IsNullOrEmpty(SearchTermTextBox.Text))
            {

                int caret = SearchTermTextBox.CaretIndex;
                if (caret >= 0)
                {
                    if (SearchTermTextBox.SelectionLength > 0)
                    {
                        SearchTermTextBox.SelectedText = "";
                        if (SelectedFriendModel != null)
                        {
                            SelectedFriendModel = null;
                            NeedToChangeSelectedUI = false;
                        }
                    }
                    else
                    {
                        if (SearchTermTextBox.SelectionStart == 0 && caret != 0)
                        {
                            SearchTermTextBox.SelectionStart = SearchTermTextBox.Text.Length;
                            SearchTermTextBox.Text = SearchTermTextBox.Text.Remove(SearchTermTextBox.Text.Length - 1);
                        }
                        else
                        {
                            if (caret > 0)
                            {
                                //SearchTermTextBox.Text = SearchTermTextBox.Text.Substring(0, (SearchTermTextBox.SelectionStart - 1)) + SearchTermTextBox.Text.Substring((SearchTermTextBox.SelectionStart ), (SearchTermTextBox.Text.Length ));
                                SearchTermTextBox.Text = SearchTermTextBox.Text.Substring(0, SearchTermTextBox.SelectionStart - 1) + SearchTermTextBox.Text.Substring(SearchTermTextBox.SelectionStart, SearchTermTextBox.Text.Length - SearchTermTextBox.SelectionStart);
                                SearchTermTextBox.CaretIndex = caret - 1;
                            }
                        }
                        SearchString = SearchTermTextBox.Text;
                    }
                }
                SearchTermTextBox.Focus();
            }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SearchString = SearchTermTextBox.Text;
            IsSelectedAnyFriend = false;
            SetDefaultParameter();
            if (!isBackSapce)
            {
                isBackSapce = true;
                if (SearchTermTextBox.Text.Length > 3)
                {
                    int caret = SearchTermTextBox.CaretIndex;
                    int countSpaces = SearchTermTextBox.Text.Count(Char.IsWhiteSpace);
                    if (countSpaces > 0)
                    {
                        SearchTermTextBox.Text = SearchTermTextBox.Text.Replace(" ", "");
                        caret = caret - 1;
                    }
                    if (SearchTermTextBox.Text.Length > 3)
                        SearchTermTextBox.Text = SearchTermTextBox.Text.Insert(4, " ");
                    if (caret == 4)
                        SearchTermTextBox.CaretIndex = caret + 2;
                    else SearchTermTextBox.CaretIndex = caret + 1;
                }
            }
            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                if (!(SearchString.Length > 2 && SelectedFriendModel != null && SelectedFriendModel.ShortInfoModel.UserIdentity > 0 && SearchString.Equals(SelectedFriendModel.ShortInfoModel.UserIdentity.ToString().Substring(2))) && !IsSelectedAnyFriend)
                {
                    DoSearch();
                }
            }
            else
            {
                ThrdSearchFriend.searchParm = null;
                ThrdSearchFriend.prevString = null;
                NoResultsFound = false;
                SelectedFriendUserTableId = 0;
                RingIDViewModel.Instance.TempFriendList.Clear();
            }
        }

        private void SearchTextBoxTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SearchString))
            {
                if (string.IsNullOrEmpty(ThrdSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThrdSearchFriend.prevString) && !ThrdSearchFriend.prevString.Trim().Equals(SearchString)))
                {
                    if (!(SearchString.Length > 2 && SelectedFriendModel != null && SelectedFriendModel.ShortInfoModel.UserIdentity > 0 && SearchString.Equals(SelectedFriendModel.ShortInfoModel.UserIdentity.ToString().Substring(2))) && !IsSelectedAnyFriend)
                    {
                        NoResultsFound = false;
                        ClearTempListFromTimer();
                        OnSearch();
                    }
                }
            }
            else
            {
                ClearTempListFromTimer();
            }

            ThreadMaxRunningTime += 400;
            if (ThreadMaxRunningTime >= 3000)
            {
                ThreadMaxRunningTime = 0;
                SearchTextBoxTimer.Enabled = false;
                SearchTextBoxTimer.Stop();
            }
        }

        private void SearchTermTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            isBackSapce = false;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("^[0-9]$", RegexOptions.Compiled);
            if (regex.IsMatch(e.Text))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void DialPadMaximize_Click(object sender, RoutedEventArgs e)
        {
            DialPadConatiner.Visibility = Visibility.Visible;
            Down_Arrow.Visibility = Visibility.Visible;
            DialPadBtn.Visibility = Visibility.Collapsed;
        }

        private void DialPadMinimize_Click(object sender, MouseButtonEventArgs e)
        {
            DialPadBtn.Visibility = Visibility.Visible;
            DialPadConatiner.Visibility = Visibility.Collapsed;
            Down_Arrow.Visibility = Visibility.Collapsed;
        }

        private void BtnFriend_Click(object sender, RoutedEventArgs e)
        {
            //SearchTermTextBox.TextChanged -= SearchBox_TextChanged;
            //SearchTermTextBox.CaretIndex = 0;
            //SearchTermTextBox.Text = string.Empty;
            //SearchTermTextBox.TextChanged += SearchBox_TextChanged;
            //SearchTermTextBox.Focus();
            //if (RingIDViewModel.Instance.TempFriendList.Count > 0)
            //{
            //    RingIDViewModel.Instance.TempFriendList.ToList().ForEach(P => P.VisibilityModel.IsVisibleInDialer = IsFriendVisibilityAfterFriendsButtonClick(P));
            //    SelectedFriendIdentity = 0;

            //    if (SelectedFriendModel != null)
            //    {
            //        SelectedFriendModel.ShortInfoModel.OnPropertyChanged("UserIdentity");
            //        SelectedFriendModel = null;
            //    }
            //}


        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (SelectedFriendModel != null && SelectedFriendModel.ShortInfoModel.UserTableID > 0)
            {
                RingIDViewModel.Instance.OnFriendProfileButtonClickedByUTID(SelectedFriendModel.ShortInfoModel.UserTableID);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Control control = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
            if (SelectedFriendUserTableId.Equals(model.ShortInfoModel.UserTableID))
            {
                SelectedFriendUserTableId = 0;
                SelectedFriendModel = null;
                NeedToChangeSelectedUI = false;
                model.ShortInfoModel.OnPropertyChanged("UserTableID");
            }
            else
            {
                long tempUserTableId = SelectedFriendUserTableId;
                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                SelectedFriendModel = model;

                SearchTermTextBox.TextChanged -= SearchBox_TextChanged;

                if (model.ShortInfoModel.UserIdentity > 0)
                    SearchTermTextBox.Text = HelperMethods.GetDisplayRingIDfromUserID(model.ShortInfoModel.UserIdentity);

                SearchTermTextBox.TextChanged += SearchBox_TextChanged;
                SearchTermTextBox.Focus();
                SearchTermTextBox.CaretIndex = SearchTermTextBox.Text.Length;
                ThrdSearchFriend.runningSearchFriendThread = false;
                NeedToChangeSelectedUI = true;

                model.ShortInfoModel.OnPropertyChanged("UserTableID");

                //   UserBasicInfoModel tempModel = DialledFriendList.Where(P => P.ShortInfoModel.UserIdentity == tempIdentity).FirstOrDefault();
                UserBasicInfoModel tempModel = AddRemoveInCollections.getSingleTempFriend(tempUserTableId);
                if (tempModel != null)
                {
                    tempModel.ShortInfoModel.OnPropertyChanged("UserTableID");
                }

                DialPadBtn.Visibility = Visibility.Visible;
                DialPadConatiner.Visibility = Visibility.Collapsed;
                Down_Arrow.Visibility = Visibility.Collapsed;
            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        #endregion "Dialpad Button Events"

        #region ICommand

        public ICommand OpenPopupCommand
        {
            get
            {
                if (_OpenPopupCommand == null)
                    _OpenPopupCommand = new RelayCommand(param => OpenPopupExecute(param));
                return _OpenPopupCommand;
            }
            set
            {
                _OpenPopupCommand = value;
            }
        }

        public ICommand AddFriendButtonCommand
        {
            get
            {
                if (_AddFriendButtonCommand == null)
                {
                    _AddFriendButtonCommand = new RelayCommand(param => OnAddFriendButtonClicked(param));
                }
                return _AddFriendButtonCommand;
            }
        }

        #endregion

        #region "Utility methods"

        private void SetDefaultParameter()
        {
            ThrdSearchFriend.processingList = false;
            ThrdSearchFriend.SearchForShowMore = false;
            ThrdSearchFriend.totalSearchCount = 0;
            ThrdSearchFriend.friendCountInOneRequest = 0;
            NeedToChangeSelectedUI = false;
            ShowSearchingLoader(false);
        }

        private void DoSearch()
        {
            try
            {
                if (string.IsNullOrEmpty(ThrdSearchFriend.prevString)
                    || (!string.IsNullOrEmpty(ThrdSearchFriend.prevString) && !ThrdSearchFriend.prevString.Trim().Equals(SearchString.Trim())))
                {
                    ThreadMaxRunningTime = 0;
                    SearchTextBoxTimer.Elapsed -= SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Elapsed += SearchTextBoxTimer_Elapsed;
                    SearchTextBoxTimer.Interval = 400;
                    SearchTextBoxTimer.AutoReset = true;
                    SearchTextBoxTimer.Enabled = true;
                    SearchTextBoxTimer.Stop();
                    SearchTextBoxTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCDialer).Name).Error(ex.StackTrace + " " + ex.Message);
            }
        }

        public void OnAddFriendButtonClicked(object parameter)
        {
            UserBasicInfoModel model = (UserBasicInfoModel)parameter;
            BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(model.ShortInfoModel.UserTableID);

            int accessType = -1;
            if (!HelperMethods.HasFriendChatPermission(model, blockModel, out accessType))
            {
                if (HelperMethods.ShowBlockWarning(model, accessType))
                {
                    return;
                }
            }
            HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, model, blockModel, (status) =>
            {
                if (status == false) return 0;

                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                new ThrdAddFriend().StartProcess(model);
                return 1;
            }, true);
        }

        private void ClearTempListFromTimer()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                lock (RingIDViewModel.Instance.TempFriendList)
                {
                    RingIDViewModel.Instance.TempFriendList.Clear();
                }
            });
        }

        public void ShowSearchingLoader(bool isSearching)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (isSearching)
                {
                    LoaderSmall = ImageUtility.GetBitmapImage(View.Constants.ImageLocation.LOADER_SMALL);
                    GC.SuppressFinalize(LoaderSmall);
                }
                else
                {
                    LoaderSmall = null;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public void CheckAnySingleFriendIsVisibleThenMakeItSelected()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
               {
                   if (RingIDViewModel.Instance.TempFriendList.Count == 1)
                   {
                       UserBasicInfoModel model = RingIDViewModel.Instance.TempFriendList.ElementAt(0); //list.ElementAt(0);
                       if (model != null)
                       {
                           SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                           model.ShortInfoModel.OnPropertyChanged("UserTableID");
                           SelectedFriendModel = model;
                           NeedToChangeSelectedUI = true;
                       }
                   }
                   else
                   {
                       SelectedFriendUserTableId = 0;
                       NeedToChangeSelectedUI = false;
                   }
               }, DispatcherPriority.Input);
        }

        public Visibility IsFriendVisibilityAfterFriendsButtonClick(UserBasicInfoModel model)
        {
            Visibility result = Visibility.Collapsed;
            try
            {
                if (model.ShortInfoModel.FriendShipStatus > 0)
                {
                    result = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {

                log.Error("UpdateVisibility ==> " + ex.Message + "\n" + ex.StackTrace);

            }
            return result;
        }

        public void OpenPopupExecute(object parameter)
        {
            Button control = (Button)parameter;
            UserBasicInfoModel model = (UserBasicInfoModel)control.Tag;
            if (model.NumberOfMutualFriends > 0)
            {
                if (UCMutualFriendsPopup.Instance != null)
                    mainPanel.Children.Remove(UCMutualFriendsPopup.Instance);
                UCMutualFriendsPopup.Instance = new UCMutualFriendsPopup();
                UCMutualFriendsPopup.Instance.SetParent(mainPanel);
                UCMutualFriendsPopup.Instance.Show();
                UCMutualFriendsPopup.Instance.ShowPopup(control, model.ShortInfoModel.UserTableID, model.ShortInfoModel.FullName);
                UCMutualFriendsPopup.Instance.OnRemovedUserControl += () =>
                {
                    UCMutualFriendsPopup.Instance.MutualList.Clear();
                    UCMutualFriendsPopup.Instance = null;
                };
                SendDataToServer.SendMutualFreindRequest(model.ShortInfoModel.UserTableID);
            }
        }

        #endregion
    }
}
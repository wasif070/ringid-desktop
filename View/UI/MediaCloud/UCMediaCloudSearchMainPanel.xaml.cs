﻿using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Auth;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCMediaCloudSearchMainPanel.xaml
    /// </summary>
    /// ///MediaSearchPanelConstants.TypeRecentTrending etc are indexes
    public partial class UCMediaCloudSearchMainPanel : UserControl
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCMediaCloudSearchMainPanel).Name);

        //public static UCMediaCloudSearchMainPanel Instance = null;
        public Func<int> _OnBackToPrevious = null;
        UserControl userControl = new UserControl();
        public UCRecentTrendingSearch View_RecentTrending;
        public UCSearchListPanel View_Suggestions;
        public UCSearchingTabPanel View_FullSearch;
        public int SearchPanelIndexType;
        public string CurrentlytxtbxSuggestionParam, SelectedSearchedParam;

        public UCMediaCloudSearchMainPanel()
        {
            //Instance = this;
            InitializeComponent();
            this.DataContext = this;
            this.SubPanelContainer.Child = userControl;

            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeMediaCloudSearch, MediaSearchIndexConstants.TypeRecentTrending); //-2 for recentrending, -1 for suggestions
            LoadUI(MediaSearchIndexConstants.TypeRecentTrending);
        }

        public void LoadUI(int idxType, string suggestionSelected = null)
        {
            try
            {
                SearchPanelIndexType = idxType;
                switch (SearchPanelIndexType)
                {
                    case MediaSearchIndexConstants.TypeSearchSuggestions:
                        if (View_Suggestions == null) View_Suggestions = new UCSearchListPanel();
                        userControl.Content = View_Suggestions;
                        break;

                    case MediaSearchIndexConstants.TypeRecentTrending:
                        if (View_RecentTrending == null) View_RecentTrending = new UCRecentTrendingSearch();
                        userControl.Content = View_RecentTrending;
                        break;

                    default:
                        if (View_FullSearch == null) View_FullSearch = new UCSearchingTabPanel();
                        userControl.Content = View_FullSearch;
                        if (suggestionSelected != null)
                            View_FullSearch.LoadIntoSearchUI(idxType, suggestionSelected);
                        else
                            View_FullSearch.LoadIntoSearchUI(idxType, SearchTermTextBox.Text);
                        break;
                }
            }
            catch (System.Exception) { }
        }
        //public void SwitchToTab(int SelectedIndex)
        //{
        //try
        //{
        //    //string tabItem = ((sender as TabControl).SelectedItem as TabItem).Name as string;
        //    if (TabType != SelectedIndex)
        //        TabType = SelectedIndex;
        //    switch (SelectedIndex)//(tabItem)
        //    {
        //        case 0://"NewsTab":
        //            if (_UCNewsContainerPanel == null)
        //            {
        //                _UCNewsContainerPanel = new UCNewsContainerPagesPanel(ScrlViewer);
        //            }
        //            tabContainerBorder.Child = _UCNewsContainerPanel;
        //            break;
        //        case 1://"DiscoverTab":
        //            if (_UCDiscoverPanel == null)
        //            {
        //                _UCDiscoverPanel = new UCDiscoverPagesPanel(ScrlViewer);
        //            }
        //            //if (textBox != null) textBox
        //            tabContainerBorder.Child = _UCDiscoverPanel;
        //            break;
        //        case 2://"FollowingTab":
        //            if (_UCFollowingListPanel == null)
        //            {
        //                _UCFollowingListPanel = new UCFollowingPagesPanel(ScrlViewer);
        //            }
        //            //if (textBox != null) textBox
        //            tabContainerBorder.Child = _UCFollowingListPanel;
        //            break;
        //        case 3://"SavedTab":
        //            if (_UCSavedContentPanel == null) _UCSavedContentPanel = new UCSavedPagesFeedsPanel(ScrlViewer);
        //            tabContainerBorder.Child = _UCSavedContentPanel;
        //            break;
        //    }
        //}
        //catch (System.Exception) { }
        //}
        //public void SwitchtoTabFromBackButton(int idx)
        //{
        //    try
        //    {
        //        switch (idx)
        //        {
        //            case MediaSearchPanelConstants.TypeSearchSuggestions:
        //                SearchType = idx;
        //                if (View_Suggestions != null)
        //                {
        //                    userControl.Content = View_Suggestions;
        //                }
        //                break;
        //            case MediaSearchPanelConstants.TypeRecentTrending:
        //                SearchType = idx;
        //                if (View_RecentTrending != null)
        //                {
        //                    userControl.Content = View_RecentTrending;
        //                }
        //                break;
        //            default:
        //                SearchType = MediaSearchPanelConstants.TypeFullSearch;
        //                if (View_FullSearch != null)
        //                {
        //                    userControl.Content = View_FullSearch;
        //                    View_FullSearch.SetValuesFromPreviousUI(SelectedSearchedParam, idx);
        //                }
        //                break;
        //        }
        //    }
        //    catch (System.Exception) { }
        //}
        public void FullSearchByType(string sgtn, int searchType, long ut)
        {
            SelectedSearchedParam = sgtn;
            SearchMediaDTO newDto = new SearchMediaDTO { SearchSuggestion = sgtn, SearchType = searchType, UpdateTime = ut };
            //update into dictionary & delete oldest & also same to DB
            lock (MediaDictionaries.Instance.MEDIA_SEARCHES)
            {
                if (MediaDictionaries.Instance.MEDIA_SEARCHES.Count >= 10)
                {
                    SearchMediaDTO minDto = MediaDictionaries.Instance.MEDIA_SEARCHES.ElementAt(0);
                    for (int i = 1; i < MediaDictionaries.Instance.MEDIA_SEARCHES.Count; i++)
                    {
                        SearchMediaDTO currentDto = MediaDictionaries.Instance.MEDIA_SEARCHES.ElementAt(i);
                        if (currentDto.UpdateTime < minDto.UpdateTime) minDto = currentDto;
                    }
                    MediaDictionaries.Instance.MEDIA_SEARCHES.Remove(minDto);
                }
                MediaDictionaries.Instance.MEDIA_SEARCHES.Add(newDto);
                MediaDictionaries.Instance.MEDIA_SEARCHES.Sort((x, y) => y.SearchSuggestion.Length.CompareTo(x.SearchSuggestion.Length));
            }
            new InsertIntoRecentMediaSearchesTable(newDto);
            //clear tmp results
            MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.Clear();
            //clear current UI
            //if (View_Suggestions != null)
            //{
            //    View_Suggestions.SearchSuggestions.Clear();
            //    //View_Suggestions.CurrentTextBoxSearchParam = string.Empty;
            //}
            //set textfield text to new selected suggestion
            SearchTermTextBox.TextChanged -= SearchTextChanged;
            SearchTermTextBox.Text = sgtn;
            SearchTermTextBox.CaretIndex = sgtn.Length;
            SearchTermTextBox.TextChanged += SearchTextChanged;
            //go to next search & next ui
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeMediaCloudSearch, searchType);
            LoadUI(searchType, sgtn); //searchType = searchIndex now
            /////SettingsConstants.MEDIA_SEARCH_TYPE_ = RingIDEnum.MediaSearchIndexConstants
        }

        public void ShowMediaCollection(Func<int> _OnBackToPrevious)
        {

            this._OnBackToPrevious = _OnBackToPrevious;

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //backBtn.Click += backBtn_Click;
            SearchTermTextBox.TextChanged += SearchTextChanged;
            SearchTermTextBox.PreviewKeyDown += SearchTermTextBox_PreviewKeyDown;
            cancel.Click -= cancel_Click;
            cancel.Click += cancel_Click;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //backBtn.Click -= backBtn_Click;
            SearchTermTextBox.TextChanged -= SearchTextChanged;
            SearchTermTextBox.PreviewKeyDown -= SearchTermTextBox_PreviewKeyDown;
            cancel.Click -= cancel_Click;
        }
        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            SearchTermTextBox.Clear();
        }
        public void SearchTermTextBox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            string SearchParam = SearchTermTextBox.Text.Trim();
            if (SearchParam.Length > 0 && (e.Key == Key.Enter))
            {
                FullSearchByType(SearchParam, 0, Models.Utility.ModelUtility.CurrentTimeMillis());
                e.Handled = true;
            }
        }
        public void SearchTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                string SearchParam = SearchTermTextBox.Text.Trim();
                if (SearchParam.Length > 0)
                {
                    if (SearchPanelIndexType != MediaSearchIndexConstants.TypeSearchSuggestions)
                    {
                        MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeMediaCloudSearch, MediaSearchIndexConstants.TypeSearchSuggestions);
                        LoadUI(MediaSearchIndexConstants.TypeSearchSuggestions);
                    }
                    CurrentlytxtbxSuggestionParam = SearchParam;
                    View_Suggestions.SearchSuggestions.Clear();
                    List<SearchMediaDTO> lst = null;
                    lock (MediaDictionaries.Instance.TEMP_SEARCH_SGTNS)
                    {
                        if (MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.TryGetValue(SearchParam, out lst) && lst.Count > 0)
                        {
                            foreach (var item in lst)
                            {
                                if (!View_Suggestions.SearchSuggestions.Any(P => P.SearchSuggestion.Equals(item.SearchSuggestion) && P.SearchType == item.SearchType))
                                {
                                    SearchMediaModel model = new SearchMediaModel();
                                    model.LoadData(item);
                                    View_Suggestions.SearchSuggestions.Add(model);
                                }
                            }
                        }
                        else
                        {
                            JObject SendObj = new JObject();
                            SendObj[JsonKeys.SearchParam] = SearchParam;
                            (new AuthRequestNoResult()).StartThread(SendObj, AppConstants.TYPE_MEDIA_SUGGESTION, AppConstants.REQUEST_TYPE_REQUEST);
                        }
                    }
                }
                else
                {
                    if (SearchPanelIndexType != MediaSearchIndexConstants.TypeRecentTrending)
                        LoadUI(MediaSearchIndexConstants.TypeRecentTrending);
                }
            }
            catch (System.Exception)
            {
            }
        }

        //private void backBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (_OnBackToPrevious != null)
        //        {
        //            _OnBackToPrevious();
        //        }
        //    }
        //    catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        //}

    }
}

﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using System.Linq;
using View.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCMediaCloudFeedsPanel.xaml
    /// </summary>
    public partial class UCMediaCloudFeedsPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMediaCloudFeedsPanel).Name);

        public CustomFeedScroll scroll;
        private System.Timers.Timer _Timer;
        public bool _IsTimerStart = false;
        private VirtualizingStackPanel _ImageItemContainer = null;
        private bool _IsFirstTimePrev = true;
        private bool _IsFirstTimeNext = true;

        private int _CurrentLeftMargin = 0;
        private int _NextPrevCount = 0;
        private int _NoOfSwappingCount = 0;
        private Visibility _IsLeftArrowVisible = Visibility.Hidden;
        private Visibility _IsRightArrowVisible = Visibility.Hidden;

        public UCMediaCloudFeedsPanel(CustomFeedScroll scroll)
        {
            this.scroll = scroll;

            InitializeComponent();
            this.DataContext = this;
            //this.PreviewMouseWheel += (s, e) => { scroll.OnPreviewMouseWheelScrolled(e.Delta); e.Handled = true; };//this.PreviewMouseWheel += delegate(object sender, MouseWheelEventArgs e) { };
            ViewCollection = RingIDViewModel.Instance.AllMediaCustomFeeds;
            scroll.SetScrollValues(mediaFeedsContainer, BreakingMediaCloudSliderFeeds, ViewCollection, FeedDataContainer.Instance.AllMediaCurrentIds, FeedDataContainer.Instance.AllMediaTopIds, FeedDataContainer.Instance.AllMediaBottomIds, SettingsConstants.PROFILE_TYPE_MUSICPAGE, AppConstants.TYPE_MEDIA_FEED);
            DefaultSettings.ALLMEDIA_STARTPKT = SendToServer.GetRanDomPacketID();
            if (!scroll.RequestOn)
            {
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.ALLMEDIA_STARTPKT);
            }
            SendDataToServer.BreakingMediaCloudFeeds();
        }
        #region "INotifyPropertyChanged"

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"

        public ObservableCollection<SingleMediaModel> _BreakingMediaCloudFeeds;
        public ObservableCollection<SingleMediaModel> BreakingMediaCloudFeeds
        {
            get
            {
                return _BreakingMediaCloudFeeds;
            }
            set
            {
                _BreakingMediaCloudFeeds = value;
                this.OnPropertyChanged("BreakingMediaCloudFeeds");
            }
        }

        public ObservableCollection<SingleMediaModel> _BreakingMediaCloudSliderFeeds = new ObservableCollection<SingleMediaModel>();
        public ObservableCollection<SingleMediaModel> BreakingMediaCloudSliderFeeds
        {
            get
            {
                return _BreakingMediaCloudSliderFeeds;
            }
            set
            {
                _BreakingMediaCloudSliderFeeds = value;
                this.OnPropertyChanged("BreakingMediaCloudSliderFeeds");
            }
        }
        private CustomFeedCollection _ViewCollection;
        public CustomFeedCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                if (_ViewCollection != null) FeedCollectionType = _ViewCollection.Type;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        private int _FeedCollectionType;
        public int FeedCollectionType
        {
            get { return _FeedCollectionType; }
            set
            {
                _FeedCollectionType = value;
                this.OnPropertyChanged("FeedCollectionType");
            }
        }

        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }

        #endregion "Property "
        #region "Methods"

        //private DispatcherTimer timer;
        //private void TimerEventProcessor(object sender, EventArgs e)
        //{
        //    if (timer != null && timer.IsEnabled)
        //    {
        //        //ViewCollection = RingIDViewModel.Instance.AllMediaPageFeeds;
        //        //scroll.SetScrollEvents(true);
        //        if (!UCGuiRingID.Instance.IsAnyWindowAbove())
        //            Keyboard.Focus(scroll);
        //        timer.Stop();
        //    }
        //}

        //private void sliderListBox_Loaded(object sender, RoutedEventArgs e)
        //{
        //    if (BreakingMediaCloudFeeds != null && BreakingMediaCloudFeeds.Count > 1)
        //    {
        //        LoadSliderData();
        //    }
        //}
        private void sliderScroll_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error:PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }



        public void LoadSliderData()
        {
            //Application.Current.Dispatcher.Invoke((Action)delegate
            //{
            try
            {
                //Thread.Sleep(1000);
                //BreakingMediaCloudSliderFeeds.Clear();
                List<SingleMediaModel> breakingNws = BreakingMediaCloudFeeds.ToList();
                if (BreakingMediaCloudFeeds != null && BreakingMediaCloudFeeds.Count > 0)
                {
                    foreach (SingleMediaModel model in breakingNws)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            if (!BreakingMediaCloudSliderFeeds.Any(P => P.ContentId == model.ContentId))
                            {
                                BreakingMediaCloudSliderFeeds.Add(model);
                            }
                        }, DispatcherPriority.Send);
                        Thread.Sleep(10);
                        if (BreakingMediaCloudSliderFeeds.Count == 4)
                        {
                            break;
                        }
                    }
                    //

                    Thread.Sleep(1000);
                    if (BreakingMediaCloudSliderFeeds.Count > 2)
                    {
                        SetNextPrevoiusButtonVisibility();
                        StartBannerNewsSliding();
                    }
                }
            }
            catch (Exception ex) { log.Error("Error: LoadSliderData ." + ex.Message + "\n" + ex.StackTrace); }
            //}, DispatcherPriority.Send);
        }

        private void StartBannerNewsSliding()
        {
            if (_Timer == null)
            {
                _Timer = new System.Timers.Timer();
                _Timer.Interval = 2500;
                _IsTimerStart = true;
                _Timer.Elapsed += timer_Elapsed;
                _Timer.Start();
            }
        }

        private void SetNextPrevoiusButtonVisibility()
        {
            if (BreakingMediaCloudFeeds != null && _NextPrevCount == BreakingMediaCloudFeeds.Count)
            {
                _NextPrevCount = 0;
            }

            IsLeftArrowVisible = _NextPrevCount > 0 ? Visibility.Visible : Visibility.Collapsed;
            IsRightArrowVisible = BreakingMediaCloudFeeds != null && _NextPrevCount < BreakingMediaCloudFeeds.Count - 1 ? Visibility.Visible : Visibility.Collapsed;
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //  Application.Current.Dispatcher.BeginInvoke(() =>
            //  {
            try
            {
                OnNextCommandClicked();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
            //}, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                ThicknessAnimation MarginAnimation = new ThicknessAnimation();
                MarginAnimation.From = new Thickness(_CurrentLeftMargin, 0, 0, 0);
                MarginAnimation.To = new Thickness(target, 0, 0, 0);
                MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                Storyboard storyboard = new Storyboard();
                storyboard.Children.Add(MarginAnimation);
                Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
                Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));

                storyboard.Begin(_ImageItemContainer);
            }, DispatcherPriority.Send);
        }

        private void OnPrevious(object param)
        {
            try
            {
                int target = 0;
                _CurrentLeftMargin = -320;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNext(object param)
        {
            try
            {
                int target = -320;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ResetData()
        {
            if (_Timer != null)
            {
                _Timer.Elapsed -= timer_Elapsed;
                _Timer.Stop();
                _Timer.Enabled = false;
                _Timer = null;
            }

            AnimationOnArrowClicked(0, 0);
            _NoOfSwappingCount = 0;
            _NextPrevCount = 0;
            _CurrentLeftMargin = 0;
            _IsFirstTimeNext = true;
            _IsFirstTimePrev = true;
            _IsTimerStart = false;

            IsLeftArrowVisible = Visibility.Hidden;
            IsRightArrowVisible = Visibility.Hidden;
        }
        #endregion

        #region "ICommand"
        private ICommand _ReloadCommand;
        public ICommand ReloadCommand
        {
            get
            {
                if (_ReloadCommand == null)
                {
                    _ReloadCommand = new RelayCommand(param => OnReloadCommandClicked());
                }
                return _ReloadCommand;
            }
        }

        public void OnReloadCommandClicked()
        {
            if (!scroll.RequestOn)
            {
                FeedDataContainer.Instance.AllMediaTopIds.Reset();
                FeedDataContainer.Instance.AllMediaBottomIds.Reset();
                ViewCollection.RemoveAllModels();
                DefaultSettings.ALLMEDIA_STARTPKT = Auth.utility.SendToServer.GetRanDomPacketID();
                scroll.RequestFeeds(Guid.Empty, 2, 0, DefaultSettings.ALLMEDIA_STARTPKT);
            }
            SendDataToServer.BreakingMediaCloudFeeds();
        }

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            scroll.OnIsVisibleChanged(true);
            BreakingMediaCloudFeeds = RingIDViewModel.Instance.BreakingMediaCloudFeeds;
            new Task(delegate
            {
                if (BreakingMediaCloudFeeds != null && BreakingMediaCloudFeeds.Count > 1)
                {
                    LoadSliderData();
                }
            }).Start();
            sliderScroll.PreviewMouseWheel += sliderScroll_PreviewMouseWheel;
        }

        private ICommand unLoadedUserConrol;
        public ICommand UnLoadedUserConrol
        {
            get
            {
                if (unLoadedUserConrol == null) unLoadedUserConrol = new RelayCommand(param => OnUnLoadedUserConrol());
                return unLoadedUserConrol;
            }
        }
        public void OnUnLoadedUserConrol()
        {
            scroll.OnIsVisibleChanged(false);
            sliderScroll.PreviewMouseWheel -= sliderScroll_PreviewMouseWheel;
            scroll.ScrollToHome();
            if (_ImageItemContainer != null)
                ResetData();
            //if (BreakingMediaCloudSliderFeeds != null)
            //    BreakingMediaCloudSliderFeeds.Clear();
            BreakingMediaCloudFeeds = null;
        }

        private ICommand _VirtualizingStackPanelLoaded;
        public ICommand VirtualizingStackPanelLoaded
        {
            get
            {
                if (_VirtualizingStackPanelLoaded == null)
                {
                    _VirtualizingStackPanelLoaded = new RelayCommand(param => OnVirtualizingStackPanelLoaded(param));
                }
                return _VirtualizingStackPanelLoaded;
            }
        }
        private void OnVirtualizingStackPanelLoaded(object parameter)
        {
            if (parameter is VirtualizingStackPanel)
                _ImageItemContainer = parameter as VirtualizingStackPanel;
        }

        private ICommand _PreviousCommand;
        public ICommand PreviousCommand
        {
            get
            {
                if (_PreviousCommand == null)
                {
                    _PreviousCommand = new RelayCommand(param => OnPreviousCommandClicked());
                }
                return _PreviousCommand;
            }
        }
        private void OnPreviousCommandClicked()
        {
            try
            {
                if (_Timer != null)
                    _Timer.Stop();

                if (_IsFirstTimePrev == false)
                {
                    int firstIndex = 0, firstmiddleIndex = 1, secondmiddleIndex = 2, lastIndex = 3, prevLoadingIndex = (_NextPrevCount + lastIndex) % BreakingMediaCloudFeeds.Count;
                    BreakingMediaCloudSliderFeeds.InvokeMove(lastIndex, secondmiddleIndex);
                    BreakingMediaCloudSliderFeeds.InvokeMove(secondmiddleIndex, firstmiddleIndex);
                    if (BreakingMediaCloudSliderFeeds.Count > 3)
                    {
                        BreakingMediaCloudSliderFeeds.InvokeMove(firstmiddleIndex, firstIndex);
                        BreakingMediaCloudSliderFeeds.RemoveAt(firstIndex);
                        SingleMediaModel model = BreakingMediaCloudFeeds.ElementAt(prevLoadingIndex);
                        BreakingMediaCloudSliderFeeds.InvokeInsert(firstIndex, model);

                        if (_NoOfSwappingCount == BreakingMediaCloudFeeds.Count - 2)
                        {
                            _NoOfSwappingCount = 0;
                        }
                        else
                        {
                            _NoOfSwappingCount++;
                        }
                    }
                }

                _IsFirstTimeNext = true;
                _IsFirstTimePrev = false;

                OnPrevious(null);
                _NextPrevCount--;

                if (_NoOfSwappingCount > 0)
                {
                    _NoOfSwappingCount = _NoOfSwappingCount - 1;
                }

                SetNextPrevoiusButtonVisibility();
                if (_Timer != null)
                    _Timer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: prevBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private ICommand _NextCommand;
        public ICommand NextCommand
        {
            get
            {
                if (_NextCommand == null)
                {
                    _NextCommand = new RelayCommand(param => OnNextCommandClicked());
                }
                return _NextCommand;
            }
        }
        private void OnNextCommandClicked()
        {
            //Hints: For AutoSlide/Next, Viewport is 0 to -630.
            try
            {
                if (_Timer != null)
                    _Timer.Stop();
                if (_IsFirstTimeNext == false)
                {
                    int firstIndex = 0, firstmiddleIndex = 1, secondmiddleIndex = 2, lastIndex = 3, nextLoadingIndex = (_NextPrevCount + lastIndex) % BreakingMediaCloudFeeds.Count;
                    BreakingMediaCloudSliderFeeds.InvokeMove(firstIndex, firstmiddleIndex);
                    BreakingMediaCloudSliderFeeds.InvokeMove(firstmiddleIndex, secondmiddleIndex);
                    if (BreakingMediaCloudSliderFeeds.Count > 3)
                    {
                        BreakingMediaCloudSliderFeeds.InvokeMove(secondmiddleIndex, lastIndex);
                        BreakingMediaCloudSliderFeeds.InvokeRemoveAt(lastIndex);
                        SingleMediaModel model = BreakingMediaCloudFeeds.ElementAt(nextLoadingIndex);
                        BreakingMediaCloudSliderFeeds.InvokeInsert(lastIndex, model);

                        if (_NoOfSwappingCount == BreakingMediaCloudFeeds.Count - 2)
                        {
                            _NoOfSwappingCount = 0;
                        }
                        else
                        {
                            _NoOfSwappingCount++;
                        }
                    }
                }

                _IsFirstTimeNext = false; // firstTime no swapping make, just changing viewport from 0 to -630.
                _IsFirstTimePrev = true;

                _NextPrevCount++;
                OnNext(null);
                SetNextPrevoiusButtonVisibility();
                if (_Timer != null)
                    _Timer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: nextBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        //private ICommand _MediaPagePlayCommand;
        //public ICommand MediaPagePlayCommand
        //{
        //    get
        //    {
        //        if (_MediaPagePlayCommand == null)
        //        {
        //            _MediaPagePlayCommand = new RelayCommand(param => OnMediaPagePlayCommand(param));
        //        }
        //        return _MediaPagePlayCommand;
        //    }
        //}

        //private void OnMediaPagePlayCommand(object param)
        //{
        //    try
        //    {
        //        FeedModel item = (FeedModel)param;
        //        MediaUtility.RunPlayList(false, item.NewsfeedId, item.MediaContent.MediaList, item.PostOwner, 0);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: OnImageClick() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}
        #endregion

    }
}

//private void OnPreviewMouseWheelScrolled(object sender, MouseWheelEventArgs e)
//{
//    if (scroll != null)
//    {
//        double offset = scroll.VerticalOffset - (e.Delta * 3 / 6);
//        if (offset < 0) offset = 0;
//        else if (offset > scroll.ScrollableHeight - 30) offset = scroll.ScrollableHeight - 30;
//        if (e.Delta < 0)
//        {
//            if ((ViewCollection.LoadMoreModel.FeedType != 4 && ViewCollection.LoadMoreModel.FeedType != 5) || FeedDataContainer.Instance.AllWaitingBottomIds.Count > 0)
//            {
//                if (scroll.VerticalOffset >= (scroll.ScrollableHeight * 0.80))
//                {
//                    scroll.ActionBottomLoad();
//                    //ActionBottomLoad();
//                }
//                else if (scroll.VerticalOffset >= (scroll.ScrollableHeight * 0.40))
//                {
//                    scroll.ActionBottomLoad();
//                }
//            }
//        }
//        else if (e.Delta > 0)
//        {
//            if (scroll.VerticalOffset <= (scroll.ScrollableHeight * 0.50))
//            {
//                scroll.ActionTopLoad();
//            }
//        }
//        scroll.ScrollToVerticalOffset(offset);
//        e.Handled = true;
//    }
//}
//private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
//{
//    if ((bool)e.NewValue)
//    {
//        if (RingIDViewModel.Instance != null)
//        {
//            RingIDViewModel.Instance.FeedButtionSelection = true;
//            //scroll.ScrollChanged -= ScrollPositionChanged;
//            //scroll.ScrollChanged += ScrollPositionChanged;
//            if (!UCGuiRingID.Instance.IsAnyWindowAbove())
//                Keyboard.Focus(scroll);
//        }
//    }
//    else
//    {
//        RingIDViewModel.Instance.FeedButtionSelection = false;
//        //scroll.ScrollChanged -= ScrollPositionChanged;
//    }
//}
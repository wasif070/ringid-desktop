﻿using Models.Constants;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Feed;
using View.ViewModel;
using View.UI.PopUp;
using View.Utility.Auth;
using View.Utility.WPFMessageBox;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCMediaCloudDiscoverPanel.xaml
    /// </summary>
    public partial class UCMediaCloudDiscoverPanel : UserControl, INotifyPropertyChanged
    {
        public CustomFeedScroll scroll;

        public UCMediaCloudDiscoverPanel(CustomFeedScroll scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

            ThradDiscoverOrFollowingChannelsList thrd = new ThradDiscoverOrFollowingChannelsList();
            thrd.CallBack += (success) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    //GIFCtrlLoader.Visibility = Visibility.Collapsed;
                    //showMoreBtn.Visibility = Visibility.Visible;
                    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                    switch (success)
                    {
                        case SettingsConstants.RESPONSE_SUCCESS:
                            showMoreBtn.Visibility = Visibility.Visible;
                            showMorePanel.Visibility = Visibility.Visible;
                            if (RingIDViewModel.Instance.MusicPagesDiscovered.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.RESPONSE_NOTSUCCESS:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (RingIDViewModel.Instance.MusicPagesDiscovered.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            break;
                        case SettingsConstants.NO_RESPONSE:
                            showMoreBtn.Visibility = Visibility.Collapsed;
                            showMorePanel.Visibility = Visibility.Collapsed;
                            if (RingIDViewModel.Instance.MusicPagesDiscovered.Count == 0)
                                noMoreTxt.Visibility = Visibility.Visible;
                            //CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                            break;
                    }
                });
            };
            thrd.StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_MUSICPAGE);
        }

        #region "Properties"

        private ObservableCollection<MusicPageModel> _SearchedNewsPages = null;
        public ObservableCollection<MusicPageModel> SearchedNewsPages
        {
            get
            {
                return _SearchedNewsPages;
            }
            set
            {
                _SearchedNewsPages = value;
                this.OnPropertyChanged("SearchedNewsPages");
            }
        }

        #endregion "Properties"


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public void feedScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            if (scrollViewer.VerticalOffset >= 51)//51 
            {
                // UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.upArrowContainerPanel.Visibility = Visibility.Visible;
            }
            else
            {
                // UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.upArrowContainerPanel.Visibility = Visibility.Collapsed;
            }
        }

        private void discoverMainPreviewPanel_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Border bdr = (Border)sender;
            if (bdr.DataContext is MusicPageModel)
            {
                MusicPageModel pageModel = (MusicPageModel)bdr.DataContext;
                RingIDViewModel.Instance.OnMediaPageProfileButtonClicked(pageModel);
            }
        }

        private void followBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.DataContext is MusicPageModel)
            {
                MusicPageModel musicPageModel = (MusicPageModel)btn.DataContext;
                new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_MUSICPAGE, musicPageModel.UserTableID, 2, null, null, musicPageModel.MusicPageId).StartThread();
                //DownloadOrAddToAlbumPopUpWrapper.Show(musicPageModel);
            }
        }

        private void LoadMore_ButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            ThradDiscoverOrFollowingChannelsList thrd = new ThradDiscoverOrFollowingChannelsList();
            thrd.CallBack += (success) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                    showMoreLoader.Visibility = Visibility.Collapsed;
                    //showMoreBtn.Visibility = success ? Visibility.Visible : Visibility.Collapsed;
                    if (success == SettingsConstants.RESPONSE_NOTSUCCESS)
                    {
                        showMoreBtn.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        showMoreBtn.Visibility = Visibility.Visible;
                    }
                });
            };
            string countryName = !UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.SelectedCountryName.Equals("All") ? UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.SelectedCountryName : "";
            thrd.StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_MUSICPAGE, UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.SelectedSearchParam, countryName, SearchedNewsPages.Count);
            //thrd.StartThread(SettingsConstants.CHANNEL_TYPE_DISCOVER, SettingsConstants.PROFILE_TYPE_MUSICPAGE, SearchedNewsPages.Count);
        }

        //private void UserControl_Loaded(object sender, RoutedEventArgs e)
        //{
        //    scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
        //    scroll.ScrollChanged += feedScrlViewer_ScrollChanged;
        //    scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);
        //    SearchedNewsPages = RingIDViewModel.Instance.MusicPagesDiscovered;
        //    showMoreBtn.Click += LoadMore_ButtonClick;
        //}

        private double previousScrollHeight = 0;

        //private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        //{
        //    showMoreBtn.Click -= LoadMore_ButtonClick;
        //    previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;
        //    scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
        //    SearchedNewsPages = null;
        //}
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
                scroll.ScrollChanged += feedScrlViewer_ScrollChanged;
                scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);
                SearchedNewsPages = RingIDViewModel.Instance.MusicPagesDiscovered;
                showMoreBtn.Click += LoadMore_ButtonClick;
                foreach (var model in SearchedNewsPages)
                {
                    int index = SearchedNewsPages.IndexOf(model);
                    if ((index + 1) % 3 == 0)
                    {
                        model.IsRightMarginOff = true;
                    }
                    else
                    {
                        model.IsRightMarginOff = false;
                    }
                }
            }
            else
            {
                showMoreBtn.Click -= LoadMore_ButtonClick;
                previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;
                scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
                SearchedNewsPages = null;
            }
        }
    }
}

﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.Constants;
using View.UI.Feed;
using View.UI.PopUp;
using View.Utility;
using View.ViewModel;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCMediaCloudMainPanel.xaml
    /// </summary>
    public partial class UCMediaCloudMainPanel : UserControl, INotifyPropertyChanged
    {
        public UCMediaCloudFeedsPanel _UCMediaCloudFeedsPanel = null;
        public UCMediaCloudDiscoverPanel _UCMediaCloudDiscoverPanel = null;
        public UCAlbumDownloadPanel _UCAlbumDownloadPanel = null;
        //public UCMediaCloudSearchMainPanel _UCMediaCloudSearch = null;
        public UCFollowingMediaCloudPanel _UCFollowingMediaCloudPanel = null;
        public Guid _ChannelId = Guid.Empty;


        public UCMediaCloudMainPanel(bool isFromChannel = false)
        {
            InitializeComponent();
            this.DataContext = this;
            if (isFromChannel == false)
            {
                MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeMediaCloud, TabType);
                SwitchToTab(TabType);
            }
            this.Loaded += UCMediaCloudMainPanel_Loaded;
            this.Unloaded += UCMediaCloudMainPanel_Unloaded;
        }

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private string _TitleName = "Media Cloud";
        public string TitleName
        {
            get { return _TitleName; }
            set
            {
                if (value == _TitleName)
                    return;
                _TitleName = value;
                this.OnPropertyChanged("TitleName");
            }
        }

        private bool _UploadPopup;
        public bool UploadPopup
        {
            get { return _UploadPopup; }
            set
            {
                if (value == _UploadPopup)
                    return;
                _UploadPopup = value;
                this.OnPropertyChanged("UploadPopup");
            }
        }

        private bool _IsTabView = true;
        public bool IsTabView
        {
            get { return _IsTabView; }
            set
            {
                if (value == _IsTabView)
                    return;
                _IsTabView = value;
                this.OnPropertyChanged("IsTabView");
            }
        }

        private int _TabType = 0;//0=feed, 1=discover, 2=following, 3=albums, 4=playlist
        public int TabType
        {
            get { return _TabType; }
            set
            {
                _TabType = value;
                OnPropertyChanged("TabType");
            }
        }

        public UCMediaCloudUploadPopup MediaCloudUploadPopup
        {
            get
            {
                return MainSwitcher.PopupController.ucMediaCloudUploadPopup;
            }
        }

        #endregion


        #region "Public Methods"
        Func<int> _OnClosing = null;
        public void Show(UIElement target, Func<int> onClosing)
        {
            _OnClosing = onClosing;
            if (popupUpAddOption.IsOpen)
            {
                popupUpAddOption.IsOpen = false;
            }
            else
            {
                popupUpAddOption.PlacementTarget = target;
                popupUpAddOption.IsOpen = true;
            }
        }
        #endregion

        #region "Event Trigger"
        void UCMediaCloudMainPanel_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            RingIDViewModel.Instance.MediaCloudSelection = true;
            this.popupUpAddOption.Closed -= popupUpAddOption_Closed;
            this.popupUpAddOption.Closed += popupUpAddOption_Closed;
            uploadAudioBorder.MouseLeftButtonDown -= uploadAudioBorder_MouseLeftButtonDown;
            uploadVideoBorder.MouseLeftButtonDown -= uploadVideoBorder_MouseLeftButtonDown;
            recordVideoBorder.MouseLeftButtonDown -= recordVideoBorder_MouseLeftButtonDown;
            downloadBorder.MouseLeftButtonDown -= downloadBorder_MouseLeftButtonDown;
            uploadAudioBorder.MouseLeftButtonDown += uploadAudioBorder_MouseLeftButtonDown;
            uploadVideoBorder.MouseLeftButtonDown += uploadVideoBorder_MouseLeftButtonDown;
            recordVideoBorder.MouseLeftButtonDown += recordVideoBorder_MouseLeftButtonDown;
            downloadBorder.MouseLeftButtonDown += downloadBorder_MouseLeftButtonDown;
            //imgUpArrow.MouseLeftButtonUp += imgUpArrow_MouseLeftButtonUp;
            //feedsPanel.MouseLeftButtonUp += feeds_MouseLeftButtonUp;
            //discoverPanel.MouseLeftButtonUp += discover_MouseLeftButtonUp;
            //followingPanel.MouseLeftButtonUp += following_MouseLeftButtonUp;
            //AlbumsPanel.MouseLeftButtonUp += albums_MouseLeftButtonUp;
            //playlistPanel.MouseLeftButtonUp += playlistPanel_MouseLeftButtonUp;
            //downloadPanel.MouseLeftButtonUp += downloadPanel_MouseLeftButtonUp;
            this.PreviewKeyDown += UserControl_PreviewKeyDown;
        }
        void UCMediaCloudMainPanel_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (RingIDViewModel.Instance.MediaShareViaWallet)
            {
                RingIDViewModel.Instance.MediaShareViaWallet = false;
            }
            RingIDViewModel.Instance.MediaCloudSelection = false;
            this.popupUpAddOption.Closed -= popupUpAddOption_Closed;
            uploadAudioBorder.MouseLeftButtonDown -= uploadAudioBorder_MouseLeftButtonDown;
            uploadVideoBorder.MouseLeftButtonDown -= uploadVideoBorder_MouseLeftButtonDown;
            recordVideoBorder.MouseLeftButtonDown -= recordVideoBorder_MouseLeftButtonDown;
            //imgUpArrow.MouseLeftButtonUp -= imgUpArrow_MouseLeftButtonUp;
            //feedsPanel.MouseLeftButtonUp -= feeds_MouseLeftButtonUp;
            //discoverPanel.MouseLeftButtonUp -= discover_MouseLeftButtonUp;
            //followingPanel.MouseLeftButtonUp -= following_MouseLeftButtonUp;
            //AlbumsPanel.MouseLeftButtonUp -= albums_MouseLeftButtonUp;
            //playlistPanel.MouseLeftButtonUp -= playlistPanel_MouseLeftButtonUp;
            //downloadPanel.MouseLeftButtonUp -= downloadPanel_MouseLeftButtonUp;
            this.PreviewKeyDown -= UserControl_PreviewKeyDown;
            this._ChannelId = Guid.Empty;
        }

        public void SwitchToTab(int SelectedIdx)
        {
            try
            {
                if (TabType != SelectedIdx)
                    TabType = SelectedIdx;
                switch (SelectedIdx)
                {
                    case 0://"FeedsTab":
                        if (_UCMediaCloudFeedsPanel == null)
                        {
                            _UCMediaCloudFeedsPanel = new UCMediaCloudFeedsPanel(ScrlViewer);
                        }
                        tabContainerBorder.Child = _UCMediaCloudFeedsPanel;
                        //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(0, 0);
                        tabContainerBorder.Margin = new Thickness(0);
                        break;
                    case 1://"DiscoverTab":
                        if (_UCMediaCloudDiscoverPanel == null)
                        {
                            _UCMediaCloudDiscoverPanel = new UCMediaCloudDiscoverPanel(ScrlViewer);
                        }
                        tabContainerBorder.Child = _UCMediaCloudDiscoverPanel;
                        //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(1, 0);
                        tabContainerBorder.Margin = new Thickness(0, 6, 0, 0);
                        break;
                    case 2://"FollowingTab":
                        if (_UCFollowingMediaCloudPanel == null)
                        {
                            _UCFollowingMediaCloudPanel = new UCFollowingMediaCloudPanel(ScrlViewer);
                        }
                        //if (textBox != null) textBox
                        tabContainerBorder.Child = _UCFollowingMediaCloudPanel;
                        //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(2, 0);
                        tabContainerBorder.Margin = new Thickness(0, 10, 0, 0);
                        break;
                    case 3://"AlbumDowloadTab":
                        if (_UCAlbumDownloadPanel == null)
                        {
                            _UCAlbumDownloadPanel = new UCAlbumDownloadPanel(ScrlViewer);
                        }
                        _UCAlbumDownloadPanel.SetMyAlbumsList();
                        tabContainerBorder.Child = _UCAlbumDownloadPanel;
                        _UCAlbumDownloadPanel.albumsGrd.Visibility = Visibility.Visible;
                        _UCAlbumDownloadPanel.mediaContentsBdr.Visibility = Visibility.Collapsed;
                        //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(3, 0);
                        tabContainerBorder.Margin = new Thickness(0, 10, 0, 0);
                        break;
                    case 4://"PlaylistTab":
                        if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                        else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                        tabContainerBorder.Margin = new Thickness(0, 10, 0, 0);
                        tabContainerBorder.Child = UCMediaContentsView.Instance;
                        UCMediaContentsView.Instance.ShowRecentMedias(null);
                        ScrlViewer.ScrollToTop();
                        //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(4, 0);
                        break;
                    /*case 4://"DownloadTab":
                        if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                        else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                        DownloadBorderPanel.Child = UCMediaContentsView.Instance;
                        UCMediaContentsView.Instance.ShowRecentOrDownloadedMedias(2, null);
                        ScrlViewer.ScrollToTop();
                        MediaCloudDownloadSelection();
                        break;*/
                }
            }
            catch (System.Exception) { }
        }
        //private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    SwitchToTab(tabControl.SelectedIndex);
        //    MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(tabControl.SelectedIndex, 0);
        //}

        public void SwitchToTabFromBackBtn(int SelectedIdx)
        {
            try
            {
                TabType = SelectedIdx;
                SwitchToTab(TabType);
            }
            catch (System.Exception) { }
        }

        private void popupUpAddOption_Closed(object sender, EventArgs e)
        {
            if (_OnClosing != null)
            {
                _OnClosing();
            }
        }

        private void uploadAudioBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Music File";
            openFileDialog.Filter = "All supported Music files|*.mp3|MP3 files(*.mp3)|*.mp3";
            openFileDialog.Multiselect = true;
            //bool anyCorrupted = false;
            //bool anyMorethan500MB = false;
            if (openFileDialog.ShowDialog() == true)
            {
                MediaCloudUploadPopup.Show();
                MediaCloudUploadPopup.ShowPopup(openFileDialog, false);
            }
        }

        private void uploadVideoBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "All supported Video files|*.mp4|MP4 files(*.mp4)|*.mp4";
            //bool anyCorrupted = false;
            //bool anyMorethan500MB = false;
            if (openFileDialog.ShowDialog() == true)
            {
                MediaCloudUploadPopup.Show();
                MediaCloudUploadPopup.ShowPopup(openFileDialog, true);
            }
        }

        private void recordVideoBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupUpAddOption.IsOpen = false;
            if (UCVideoRecorderPreviewPanel.Instance != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCVideoRecorderPreviewPanel.Instance);
            UCVideoRecorderPreviewPanel.Instance = new UCVideoRecorderPreviewPanel(UCGuiRingID.Instance.MotherPanel);
            UCVideoRecorderPreviewPanel.Instance.Show();
            UCVideoRecorderPreviewPanel.Instance.ShowRecorder();
            UCVideoRecorderPreviewPanel.Instance.OnRemovedUserControl += () =>
            {
                UCVideoRecorderPreviewPanel.Instance = null;
            };
            UCVideoRecorderPreviewPanel.Instance.OnVideoAddedToUploadList += (fileName, duration) =>
            {
                MediaCloudUploadPopup.Show();
                MediaCloudUploadPopup.ShowRecordVideo(fileName, duration);
            };
            //WNVideoRecorderPreviewPanel.Instance.ShowRecorder();
            //WNVideoRecorderPreviewPanel.Instance.OnVideoAddedToUploadList += (fileName, duration) =>
            //{
            //    MediaCloudUploadPopup.Show();
            //    MediaCloudUploadPopup.ShowRecordVideo(fileName, duration);
            //};
        }
        private void downloadBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            popupUpAddOption.IsOpen = false;
            if (UCMeidaCloudDownloadPopup.Instance != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCMeidaCloudDownloadPopup.Instance);
            UCMeidaCloudDownloadPopup.Instance = new UCMeidaCloudDownloadPopup(UCGuiRingID.Instance.MotherPanel);
            UCMeidaCloudDownloadPopup.Instance.Show();
            UCMeidaCloudDownloadPopup.Instance.ShowMediaDownload();
            UCMeidaCloudDownloadPopup.Instance.OnRemovedUserControl += () =>
            {
                UCMeidaCloudDownloadPopup.Instance = null;
            };
        }

        //private void downloadPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    MediaCloudDownloadSelection();
        //    tabControl.SelectedIndex = 4;
        //}

        //private void imgUpArrow_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    ScrlViewer.ScrollToVerticalOffset(0);
        //    ScrlViewer.UpdateLayout();
        //}

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape && popupUpAddOption.IsOpen == true)
                popupUpAddOption.IsOpen = false;
        }
        #endregion

        #region "ICommand"
        private ICommand _BtnTabCommand;
        public ICommand BtnTabCommand
        {
            get
            {
                if (_BtnTabCommand == null)
                {
                    _BtnTabCommand = new RelayCommand(param => OnBtnTabCommandClicked(param));
                }
                return _BtnTabCommand;
            }
        }

        public void OnBtnTabCommandClicked(object parameter)
        {
            if (parameter is string)
            {
                int TabIdx = Convert.ToInt32(parameter);
                MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeMediaCloud, TabIdx);
                SwitchToTab(TabIdx);
            }
        }
        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommandClicked(param));
                }
                return _BackCommand;
            }
        }

        public void OnBackCommandClicked(object parameter)
        {
            //tabContainerBorder.Visibility = Visibility.Visible;
            //searchContentsBdr.Visibility = Visibility.Collapsed;
            //IsTabView = true;
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }

        private ICommand _SearchCommand;
        public ICommand SearchCommand
        {
            get
            {
                if (_SearchCommand == null)
                {
                    _SearchCommand = new RelayCommand(param => OnSearchCommandClicked(param));
                }
                return _SearchCommand;
            }
        }

        public void OnSearchCommandClicked(object parameter)
        {
            tabContainerBorder.Visibility = Visibility.Collapsed;
            searchContentsBdr.Visibility = Visibility.Visible;
            IsTabView = false;
            if (UCMiddlePanelSwitcher._UCMediaCloudSearch == null)
            {
                UCMiddlePanelSwitcher._UCMediaCloudSearch = new UCMediaCloudSearchMainPanel();
            }
            else
            {
                MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeMediaCloudSearch, UCMiddlePanelSwitcher._UCMediaCloudSearch.SearchPanelIndexType);
            }
            //TODO
            searchContentsBdr.Child = UCMiddlePanelSwitcher._UCMediaCloudSearch;
            //UCMiddlePanelSwitcher._UCMediaCloudSearch._ChannelId = Guid.Empty;//Needed to Reset
        }

        private void BacktoAlbumsResults()
        {
            //TODO if (tabControl.SelectedIndex == 0)
            //{
            //    _UCMediaCloudFeedsPanel.scroll.ScrollChanged += _UCMediaCloudFeedsPanel.feedScrlViewer_ScrollChanged;
            //    _UCMediaCloudFeedsPanel.scroll.PreviewKeyDown += _UCMediaCloudFeedsPanel.feedScrlViewer_PreviewKeyDown;
            //}
            tabContainerBorder.Visibility = Visibility.Visible;
            searchContentsBdr.Visibility = Visibility.Collapsed;
            if (searchContentsBdr.Child != null)
                searchContentsBdr.Child = null;
        }

        private ICommand _UploadMusicandVideoClick;
        public ICommand UploadMusicandVideoClick
        {
            get
            {
                if (_UploadMusicandVideoClick == null)
                {
                    _UploadMusicandVideoClick = new RelayCommand(param => OnUploadMusicandVideoClick(param));
                }
                return _UploadMusicandVideoClick;
            }
        }
        private void OnUploadMusicandVideoClick(object parameter)
        {
            if (parameter is Button)
            {
                UploadPopup = true;
                Button btn = (Button)parameter;
                Show(btn, () =>
                {
                    UploadPopup = false;
                    return 0;
                }
                    );
            }
        }

        private ICommand _DiscoverSearchClick;
        public ICommand DiscoverSearchClick
        {
            get
            {
                if (_DiscoverSearchClick == null)
                {
                    _DiscoverSearchClick = new RelayCommand(param => OnDiscoverSearchClick(param));
                }
                return _DiscoverSearchClick;
            }
        }
        public string SelectedSearchParam = string.Empty;
        public string SelectedCountryName = string.Empty;
        private void OnDiscoverSearchClick(object parameter)
        {
            if (UCDiscoverSearchView.Instance != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCDiscoverSearchView.Instance);
            UCDiscoverSearchView.Instance = new UCDiscoverSearchView(UCGuiRingID.Instance.MotherPanel);
            UCDiscoverSearchView.Instance.SetDiscoverSettings(false, SelectedSearchParam, SelectedCountryName);
            UCDiscoverSearchView.Instance.Show();
            UCDiscoverSearchView.Instance.OnRemovedUserControl += () =>
            {
                UCDiscoverSearchView.Instance = null;
            };
        }
        #endregion
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.RingPlayer;
using View.UI.PopUp;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCMediaContentsDownloadView.xaml
    /// </summary>
    public partial class UCMediaContentsDownloadView : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCMediaContentsDownloadView).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        public static UCMediaContentsDownloadView Instance = null;

        public UCMediaContentsDownloadView()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
        }

        #region"Property "
        private Visibility _DeleteButtonVisibility = Visibility.Visible;
        public Visibility DeleteButtonVisibility
        {
            get
            {
                return _DeleteButtonVisibility;
            }
            set
            {
                if (value == _DeleteButtonVisibility)
                    return;
                _DeleteButtonVisibility = value;
                this.OnPropertyChanged("DeleteButtonVisibility");
            }
        }
        //private String _DeleteTooltip = "";
        //public String DeleteTooltip
        //{
        //    get
        //    {
        //        return _DeleteTooltip;
        //    }
        //    set
        //    {
        //        if (value == _DeleteTooltip)
        //            return;
        //        _DeleteTooltip = value;
        //        this.OnPropertyChanged("DeleteTooltip");
        //    }
        //}
        #endregion "Property "

        public void ShowDownloadedMedias(Func<int> _OnBackToPrevious) //recent or downloaded
        {
            showMorePanel.Visibility = Visibility.Collapsed;
            DeleteButtonVisibility = Visibility.Visible;
            upperPanel.Visibility = Visibility.Visible;
            backBtn.Visibility = Visibility.Collapsed;
            this._OnBackToPrevious = _OnBackToPrevious;
            backBtn.Visibility = Visibility.Visible;
            titleTxt.Text = "Downloads";
            SingleMediaItems = RingIDViewModel.Instance.MyDownloads;
            //DeleteTooltip = "Remove from Download History";
        }
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private ObservableCollection<SingleMediaModel> _SingleMediaItems;
        public ObservableCollection<SingleMediaModel> SingleMediaItems
        {
            get
            {
                return _SingleMediaItems;
            }
            set
            {
                _SingleMediaItems = value;
                this.OnPropertyChanged("SingleMediaItems");
            }
        }

        //private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Home)
        //    {
        //        SongsOfanAlbumScroll.ScrollToHome();
        //        e.Handled = true;
        //    }
        //    else if (e.Key == Key.End)
        //    {
        //        SongsOfanAlbumScroll.ScrollToEnd();
        //        e.Handled = true;
        //    }
        //}
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            scroll.PreviewKeyDown += Scroll_PreviewKeyDown;
            backBtn.Click += backBtn_Click;
            playALl.Click += playALl_Click;
            list.PreviewMouseWheel += list_PreviewMouseWheel;
            //showMoreText.Click += ShowMore_PanelClick;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            scroll.PreviewKeyDown -= Scroll_PreviewKeyDown;
            backBtn_Click(null, null);
            backBtn.Click -= backBtn_Click;
            playALl.Click -= playALl_Click;
            list.PreviewMouseWheel -= list_PreviewMouseWheel;
            //showMoreText.Click -= ShowMore_PanelClick;
        }
        public Func<int> _OnBackToPrevious = null;
        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UCMeidaCloudDownloadPopup.Instance != null
                    && UCMeidaCloudDownloadPopup.Instance.IsVisible)
                {
                    UCMeidaCloudDownloadPopup.Instance.HideMediaDowload();
                }
                SingleMediaItems = null;
                if (_OnBackToPrevious != null)
                {
                    _OnBackToPrevious();
                }
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }

        private void list_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: list_PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        private void playALl_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SingleMediaItems.Count > 0) MediaUtility.RunPlayList(false, Guid.Empty, SingleMediaItems);
            }
            catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
        }
        private ICommand _MediaRightOptionsCommand;
        public ICommand MediaRightOptionsCommand
        {
            get
            {
                if (_MediaRightOptionsCommand == null)
                {
                    _MediaRightOptionsCommand = new RelayCommand(param => OnMediaRightOptionsCommand(param));
                }
                return _MediaRightOptionsCommand;
            }
        }

        public void OnMediaRightOptionsCommand(object parameter)
        {
            Grid grid = (Grid)parameter;
            if (grid.DataContext is SingleMediaModel)
            {

                SingleMediaModel model = (SingleMediaModel)grid.DataContext;
                int type = 0;

                if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.IsVisible
                    && UCMeidaCloudDownloadPopup.Instance != null
                    && UCMeidaCloudDownloadPopup.Instance.IsVisible)
                    type = 2;
                else if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.IsVisible
                    && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.tabContainerBorder.IsVisible
                    && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.TabType == 4)
                    type = 3;
                else if (model.MediaOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                    type = 1;
                else
                    type = -1;

                if (grid.ContextMenu != ContextMenuMediaOptions.Instance.cntxMenu)
                {
                    grid.ContextMenu = ContextMenuMediaOptions.Instance.cntxMenu;
                    ContextMenuMediaOptions.Instance.SetupMenuItem();
                }
                ContextMenuMediaOptions.Instance.ShowHandler(grid, model, type);
                ContextMenuMediaOptions.Instance.cntxMenu.IsOpen = true;
            }
        }

        private ICommand _MediaPlayCommand;
        public ICommand MediaPlayCommand
        {
            get
            {
                if (_MediaPlayCommand == null)
                {
                    _MediaPlayCommand = new RelayCommand(param => OnMediaPlayCommand(param));
                }
                return _MediaPlayCommand;
            }
        }

        public void OnMediaPlayCommand(object parameter)
        {
            if (UCMeidaCloudDownloadPopup.Instance != null && UCMeidaCloudDownloadPopup.Instance.IsVisible)
            {
                UCMeidaCloudDownloadPopup.Instance.HideMediaDowload();
            }
            if (parameter is SingleMediaModel)
            {
                SingleMediaModel mediaToPlay = (SingleMediaModel)parameter;
                int idx = SingleMediaItems.IndexOf(mediaToPlay);
                if (idx >= 0 && idx < SingleMediaItems.Count)
                    MediaUtility.RunPlayList(false, Guid.Empty, SingleMediaItems, null, idx);
            }
        }

        private ICommand _RemoveCommand;
        public ICommand RemoveCommand
        {
            get
            {
                if (_RemoveCommand == null)
                {
                    _RemoveCommand = new RelayCommand(param => removeBtnClick(param));
                }
                return _RemoveCommand;
            }
        }
        private void removeBtnClick(object parameter)
        {
            if (parameter is SingleMediaModel && SingleMediaItems != null && SingleMediaItems.Count > 0)
            {
                SingleMediaModel modelToRemove = (SingleMediaModel)parameter;
                string text = string.Empty;
                if (modelToRemove.MediaType == 1) { text = NotificationMessages.REMOVE_FROM_DOWNLOAD_MEDIA_TEXT + NotificationMessages.SINGLE_MEDIA_MUSIC; }
                else { text = NotificationMessages.REMOVE_FROM_DOWNLOAD_MEDIA_TEXT + NotificationMessages.SINGLE_MEDIA_VIDEO; }
                //MessageBoxResult result = CustomMessageBox.ShowQuestion(text);
                //if (result == MessageBoxResult.Yes)
                //{
                bool isTrue = UIHelperMethods.ShowQuestion(text, "Remove confirmation!");
                if (isTrue)
                {
                    SingleMediaItems.Remove(modelToRemove);
                    modelToRemove.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                    //MediaDictionaries.Instance.DOWNLOAD_MEDIAS.Remove(modelToRemove.ContentId);
                    Models.DAO.MediaDAO.Instance.DeleteFromDownloadedMediasTable(modelToRemove.ContentId);
                    string curFile = HelperMethods.GetDownloadedFilePathFromStreamURL(modelToRemove.StreamUrl, modelToRemove.ContentId);
                    if (System.IO.File.Exists(curFile))
                    {
                        System.IO.File.Delete(curFile);
                    }
                    //SingleMediaDTO dto;
                    //if (MediaDictionaries.Instance.RECENT_MEDIAS.TryGetValue(modelToRemove.ContentId, out dto))
                    //{
                    //    dto.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                    //    Models.DAO.MediaDAO.Instance.InsertIntoRingRecentMedia(dto);
                    //}
                    //remove from prev ui
                    //if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel != null)
                    //{
                    //    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel.MyDownloadsList.Clear();
                    //    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel.LoadDownloadedData();
                    //}
                }
            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

    }
}

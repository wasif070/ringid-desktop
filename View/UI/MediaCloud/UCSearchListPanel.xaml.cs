﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCSearchListPanel.xaml
    /// </summary>
    public partial class UCSearchListPanel : UserControl, INotifyPropertyChanged
    {
        public UCSearchListPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private ObservableCollection<SearchMediaModel> _SearchSuggestions = new ObservableCollection<SearchMediaModel>();
        public ObservableCollection<SearchMediaModel> SearchSuggestions
        {
            get
            {
                return _SearchSuggestions;
            }
            set
            {
                _SearchSuggestions = value;
                this.OnPropertyChanged("SearchSuggestions ");
            }
        }

        private void Suggestion_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DockPanel dp = sender as DockPanel;
            if (dp.DataContext is SearchMediaModel)
            {
                SearchMediaModel model = (SearchMediaModel)dp.DataContext;
                UCMiddlePanelSwitcher._UCMediaCloudSearch.View_RecentTrending.RecentMediaSearches.Insert(0, model);
                UCMiddlePanelSwitcher._UCMediaCloudSearch.FullSearchByType(model.SearchSuggestion, model.SearchType, model.UpdateTime);
            }
        }
    }
}

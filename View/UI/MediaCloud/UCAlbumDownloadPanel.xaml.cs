﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Auth;
using View.ViewModel;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCAlbumDownloadPanel.xaml
    /// </summary>
    public partial class UCAlbumDownloadPanel : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCAlbumDownloadPanel).Name);

        public CustomFeedScroll scroll;
        public static int AUDIO_ALBUMS_COUNT = -1;
        public static int VIDEO_ALBUMS_COUNT = -1;

        public UCAlbumDownloadPanel(CustomFeedScroll scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
        }
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                if (albumsGrd.Visibility != Visibility.Visible)
                {
                    albumsGrd.Visibility = Visibility.Visible;
                    mediaContentsBdr.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                IsNoAudioTxt = false;
                IsNoVideoTxt = false;
                NoMusicTxt.Text = "";
                NoVideoTxt.Text = "";
            }
        }
        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private ObservableCollection<MediaContentModel> _MyAudioAlbumsList = new ObservableCollection<MediaContentModel>();
        public ObservableCollection<MediaContentModel> MyAudioAlbumsList
        {
            get
            {
                return _MyAudioAlbumsList;
            }
            set
            {
                _MyAudioAlbumsList = value;
                this.OnPropertyChanged("MyAudioAlbumsList");
            }
        }

        private ObservableCollection<MediaContentModel> _MyVideoAlbumsList = new ObservableCollection<MediaContentModel>();
        public ObservableCollection<MediaContentModel> MyVideoAlbumsList
        {
            get
            {
                return _MyVideoAlbumsList;
            }
            set
            {
                _MyVideoAlbumsList = value;
                this.OnPropertyChanged("MyVideoAlbumsList");
            }
        }

        //private ObservableCollection<SingleMediaModel> _MyDownloadsList = new ObservableCollection<SingleMediaModel>();
        //public ObservableCollection<SingleMediaModel> MyDownloadsList
        //{
        //    get
        //    {
        //        return _MyDownloadsList;
        //    }
        //    set
        //    {
        //        _MyDownloadsList = value;
        //        this.OnPropertyChanged("MyDownloadsList");
        //    }
        //}

        private bool _IsNoAudioTxt;
        public bool IsNoAudioTxt
        {
            get { return _IsNoAudioTxt; }
            set
            {
                if (value == _IsNoAudioTxt) return;
                _IsNoAudioTxt = value;
                this.OnPropertyChanged("IsNoAudioTxt");
            }
        }

        private bool _IsNoVideoTxt;
        public bool IsNoVideoTxt
        {
            get { return _IsNoVideoTxt; }
            set
            {
                if (value == _IsNoVideoTxt) return;
                _IsNoVideoTxt = value;
                this.OnPropertyChanged("IsNoVideoTxt");
            }
        }

        private bool _NoInternet;
        public bool NoInternet
        {
            get { return _NoInternet; }
            set
            {
                if (value == _NoInternet) return;
                _NoInternet = value;
                this.OnPropertyChanged("NoInternet");
            }
        }
        #endregion
        public void SetMyAlbumsList()
        {
            if (AUDIO_ALBUMS_COUNT == MyAudioAlbumsList.Count)
            {
                if (MusicGIFCtrl != null && MusicGIFCtrl.IsRunning()) this.MusicGIFCtrl.StopAnimate();
                IsNoAudioTxt = true;
            }
            
            //if (MusicGIFCtrl != null && MusicGIFCtrl.IsRunning()) this.MusicGIFCtrl.StopAnimate();
            if (RingIDViewModel.Instance.MyAudioAlbums != null && RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
            {
                int index = 0;
                foreach (MediaContentModel model in RingIDViewModel.Instance.MyAudioAlbums)
                {
                    if (index < 3 && !MyAudioAlbumsList.Any(P => P.AlbumId == model.AlbumId))
                    {
                        MyAudioAlbumsList.Add(model);
                        index++;
                    }
                }
                AUDIO_ALBUMS_COUNT = MyAudioAlbumsList.Count;
            }
            else
            {
                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    MusicGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                    new Thread(() =>
                    {
                        bool success = false;
                        string message = string.Empty;
                        JObject obj = new JObject();
                        obj[JsonKeys.UserTableID] = DefaultSettings.LOGIN_TABLE_ID;
                        obj[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_AUDIO;
                        SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_MEDIA_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (MusicGIFCtrl != null && MusicGIFCtrl.IsRunning())
                            {
                                this.MusicGIFCtrl.StopAnimate();
                                IsNoAudioTxt = true;
                            }
                            if (!success)
                            {
                                DefaultSettings.MY_AUDIO_ALBUMS_COUNT = 0;
                                AUDIO_ALBUMS_COUNT = 0;
                            }
                            else
                            {
                                if (RingIDViewModel.Instance.MyAudioAlbums != null)
                                {
                                    int index = 0;
                                    foreach (MediaContentModel model in RingIDViewModel.Instance.MyAudioAlbums)
                                    {
                                        if (index < 3 && !MyAudioAlbumsList.Any(P => P.AlbumId == model.AlbumId))
                                        {
                                            MyAudioAlbumsList.Add(model);
                                            index++;
                                        }
                                    }
                                    AUDIO_ALBUMS_COUNT = MyAudioAlbumsList.Count;
                                }
                            }
                        }, System.Windows.Threading.DispatcherPriority.Send);
                    }).Start();
                }
                else if (MusicGIFCtrl != null && MusicGIFCtrl.IsRunning())
                {
                    this.MusicGIFCtrl.StopAnimate();
                    IsNoAudioTxt = true;
                }
                else
                {
                    NoMusicTxt.Text = "No Internet!";
                    NoVideoTxt.Text = "No Internet!";
                    //NoInternet = true;
                }
            }

            if (MyVideoAlbumsList.Count == VIDEO_ALBUMS_COUNT)
            {
                if (VideoGIFCtrl != null && VideoGIFCtrl.IsRunning())
                {
                    this.VideoGIFCtrl.StopAnimate();
                }
                IsNoVideoTxt = true;
            }
            
            //if (VideoGIFCtrl != null && VideoGIFCtrl.IsRunning()) this.VideoGIFCtrl.StopAnimate();
            if (RingIDViewModel.Instance.MyVideoAlbums != null && RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
            {
                int index = 0;
                foreach (MediaContentModel model in RingIDViewModel.Instance.MyVideoAlbums)
                {
                    if (index < 3 && !MyVideoAlbumsList.Any(P => P.AlbumId == model.AlbumId))
                    {
                        MyVideoAlbumsList.Add(model);
                        index++;
                    }
                }
                VIDEO_ALBUMS_COUNT = MyVideoAlbumsList.Count;
            }
            else
            {
                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    VideoGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                    new Thread(() =>
                    {
                        bool success = false;
                        string message = string.Empty;
                        JObject obj = new JObject();
                        obj[JsonKeys.UserTableID] = DefaultSettings.LOGIN_TABLE_ID;
                        obj[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_VIDEO;
                        SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_MEDIA_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (VideoGIFCtrl != null && VideoGIFCtrl.IsRunning())
                            {
                                this.VideoGIFCtrl.StopAnimate();
                                IsNoVideoTxt = true;
                            }
                            if (!success)
                            {
                                DefaultSettings.MY_VIDEO_ALBUMS_COUNT = 0;
                                VIDEO_ALBUMS_COUNT = 0;
                            }
                            else
                            {
                                if (RingIDViewModel.Instance.MyVideoAlbums != null)
                                {
                                    int index = 0;
                                    foreach (MediaContentModel model in RingIDViewModel.Instance.MyVideoAlbums)
                                    {
                                        if (index < 3 && !MyVideoAlbumsList.Any(P => P.AlbumId == model.AlbumId))
                                        {
                                            MyVideoAlbumsList.Add(model);
                                            index++;
                                        }
                                    }
                                    VIDEO_ALBUMS_COUNT = MyVideoAlbumsList.Count;
                                }
                            }
                        }, System.Windows.Threading.DispatcherPriority.Send);
                    }).Start();
                }
                else if (VideoGIFCtrl != null && VideoGIFCtrl.IsRunning())
                {
                    this.VideoGIFCtrl.StopAnimate();
                    IsNoVideoTxt = true;
                }
                else NoInternet = true;
            }

            //LoadDownloadedData();
        }

        #region "Private Method"

        //public void LoadDownloadedData()
        //{
        //    var result = MediaDictionaries.Instance.DOWNLOAD_MEDIAS.Values.OrderByDescending(P => P.DownloadTime).ToList();
        //    int i = 0;
        //    foreach (SingleMediaDTO item in result)
        //    {
        //        SingleMediaModel _SingleMediaModel = MyDownloadsList.Where(P => P.ContentId == item.ContentId).FirstOrDefault();
        //        if (_SingleMediaModel != null)
        //        {
        //            lock (MyDownloadsList)
        //            {
        //                int index = MyDownloadsList.IndexOf(_SingleMediaModel);
        //                if (i != index)
        //                {
        //                    MyDownloadsList.RemoveAt(index);
        //                    MyDownloadsList.Insert(i, _SingleMediaModel);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            _SingleMediaModel = RingIDViewModel.Instance.GetExistingorNewSingleMediaModelFromDTO(item);
        //            lock (MyDownloadsList)
        //            {
        //                MyDownloadsList.Insert(i, _SingleMediaModel);
        //            }
        //        }
        //        i++;

        //        if (MyDownloadsList.Count == 3) break;
        //    }
        //}

        private void BacktoAlbumsResults()
        {
            albumsGrd.Visibility = Visibility.Visible;
            mediaContentsBdr.Visibility = Visibility.Collapsed;
            if (mediaContentsBdr.Child != null)
                mediaContentsBdr.Child = null;
        }
        #endregion


        private void SingleAlbum_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Grid bd = sender as Grid;
                if (bd.DataContext is MediaContentModel)
                {
                    MediaContentModel model = (MediaContentModel)bd.DataContext;
                    albumsGrd.Visibility = Visibility.Collapsed;
                    mediaContentsBdr.Visibility = Visibility.Visible;
                    if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                    else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                    mediaContentsBdr.Child = UCMediaContentsView.Instance;

                    UCMediaContentsView.Instance.ShowAlbumContents(model, () =>
                    {
                        BacktoAlbumsResults();
                        return 0;
                    });
                }
                //else if (bd.DataContext is SingleMediaModel)
                //{
                //    SingleMediaModel model = (SingleMediaModel)bd.DataContext;
                //    int idx = MyDownloadsList.IndexOf(model);
                //    if (idx >= 0 && idx < MyDownloadsList.Count)
                //        UCGuiRingID.Instance.ucBasicMediaViewWrapper.RunPlayList(false, 0, MyDownloadsList, null, idx);
                //}
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        private ICommand _SeeAllCommand;
        public ICommand SeeAllCommand
        {
            get
            {
                if (_SeeAllCommand == null)
                {
                    _SeeAllCommand = new RelayCommand(param => OnSeeAllCommandClicked(param));
                }
                return _SeeAllCommand;
            }
        }

        public void OnSeeAllCommandClicked(object parameter)
        {
            int type = int.Parse(parameter.ToString());
            albumsGrd.Visibility = Visibility.Collapsed;
            mediaContentsBdr.Visibility = Visibility.Visible;

            if (UCMediaAlbumsView.Instance == null) UCMediaAlbumsView.Instance = new UCMediaAlbumsView();
            if (mediaContentsBdr.Child == null)
            {
                mediaContentsBdr.Child = UCMediaAlbumsView.Instance;
                UCMediaAlbumsView.Instance.ShowAlbumContents(type, () =>
                {
                    BacktoAlbumsResults();
                    return 0;
                });
            }
            //if (type == 3)
            //{
            //    var result = MediaDictionaries.Instance.DOWNLOAD_MEDIAS.Values.OrderByDescending(P => P.DownloadTime).ToList();
            //    int i = 0;
            //    foreach (SingleMediaDTO item in result)
            //    {
            //        SingleMediaModel _SingleMediaModel = RingIDViewModel.Instance.MyDownloads.Where(P => P.ContentId == item.ContentId).FirstOrDefault();
            //        if (_SingleMediaModel != null)
            //        {
            //            lock (RingIDViewModel.Instance.MyDownloads)
            //            {
            //                int index = RingIDViewModel.Instance.MyDownloads.IndexOf(_SingleMediaModel);
            //                if (i != index)
            //                {
            //                    RingIDViewModel.Instance.MyDownloads.RemoveAt(index);
            //                    RingIDViewModel.Instance.MyDownloads.Insert(i, _SingleMediaModel);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            _SingleMediaModel = RingIDViewModel.Instance.GetExistingorNewSingleMediaModelFromDTO(item);
            //            lock (RingIDViewModel.Instance.MyDownloads)
            //            {
            //                RingIDViewModel.Instance.MyDownloads.Insert(i, _SingleMediaModel);
            //            }
            //        }
            //        i++;
            //    }
            //    if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
            //    else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
            //    mediaContentsBdr.Child = UCMediaContentsView.Instance;
            //    UCMediaContentsView.Instance.ShowRecentOrDownloadedMedias(2, () =>
            //    {
            //        BacktoAlbumsResults();
            //        return 0;
            //    });
            //}
            //else
            //{
            //    if (UCMediaAlbumsView.Instance == null) UCMediaAlbumsView.Instance = new UCMediaAlbumsView();
            //    mediaContentsBdr.Child = UCMediaAlbumsView.Instance;
            //    UCMediaAlbumsView.Instance.ShowAlbumContents(type, () =>
            //    {
            //        BacktoAlbumsResults();
            //        return 0;
            //    });
            //}
        }

        private void imageContainer_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Grid grid = (Grid)sender;
            if (grid.DataContext is MediaContentModel)
            {
                MediaContentModel model = (MediaContentModel)grid.DataContext;
                if (model.IsStaticAlbum == false)
                {
                    if (grid.ContextMenu != ContextMenuAlbumEditOptions.Instance.cntxMenu)
                    {
                        grid.ContextMenu = ContextMenuAlbumEditOptions.Instance.cntxMenu;
                        ContextMenuAlbumEditOptions.Instance.SetupMenuItem();
                    }
                    ContextMenuAlbumEditOptions.Instance.ShowHandler(grid, model);
                    ContextMenuAlbumEditOptions.Instance.cntxMenu.IsOpen = true;
                }
            }
        }
    }
}

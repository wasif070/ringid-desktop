<<<<<<< HEAD
﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCFollowingMediaCloudPanel.xaml
    /// </summary>
    public partial class UCFollowingMediaCloudPanel : UserControl, INotifyPropertyChanged
    {
        public CustomFeedScroll scroll;
        public UCFollowingMediaCloudPanel(CustomFeedScroll scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

            RequestToGetFollowingMediaPages();
        }

        private void RequestToGetFollowingMediaPages()
        {
            ThradDiscoverOrFollowingChannelsList thrd = new ThradDiscoverOrFollowingChannelsList();
            thrd.CallBack += (success) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                    GIFCtrlLoader.Visibility = Visibility.Collapsed;
                    showMoreLoader.Visibility = Visibility.Collapsed;
                    if (success == SettingsConstants.RESPONSE_NOTSUCCESS)
                    {
                        showMoreBtn.Visibility = Visibility.Collapsed;
                        if (FollowingMusicPages.Count == 0)
                            noMoreTxt.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        showMoreBtn.Visibility = Visibility.Visible;
                        noMoreTxt.Visibility = Visibility.Collapsed;
                    }
                });
            };
            thrd.StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_MUSICPAGE, FollowingMusicPages.Count);
        }

        #region "Properties"

        private ObservableCollection<MusicPageModel> _FollowingMusicPages = new ObservableCollection<MusicPageModel>();
        public ObservableCollection<MusicPageModel> FollowingMusicPages
        {
            get
            {
                return _FollowingMusicPages;
            }
            set
            {
                _FollowingMusicPages = value;
                this.OnPropertyChanged("FollowingMusicPages");
            }
        }
        #endregion "Properties"

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #region "ContextMenu"

        ContextMenu cntxMenu = new ContextMenu();
        MenuItem mnuItem1 = new MenuItem();
        //MenuItem mnuItem2 = new MenuItem();
        //MenuItem mnuItem3 = new MenuItem();

        private void SetupMenuItem()
        {
            if (cntxMenu.Items != null)
                cntxMenu.Items.Clear();
            cntxMenu.Style = (Style)Application.Current.Resources["CustomCntxtMenu"];
            mnuItem1.Header = "Unfollow";
            //mnuItem2.Header = "Unfollow";

            mnuItem1.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            //mnuItem2.Style = (Style)Application.Current.Resources["ContextMenuItem"];

            cntxMenu.Items.Add(mnuItem1);
            //cntxMenu.Items.Add(mnuItem2);

            //mnuItem1.Click -= MnuEditFlwngList_Click;
            mnuItem1.Click -= MnuUnfollow_Click;
            cntxMenu.Closed -= ContextMenu_Closed;
            cntxMenu.Closed += ContextMenu_Closed;
            //mnuItem1.Click += MnuEditFlwngList_Click;
            mnuItem1.Click += MnuUnfollow_Click;

        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            //mnuItem1.Click -= MnuEditFlwngList_Click;
            mnuItem1.Click -= MnuUnfollow_Click;
            cntxMenu.Closed -= ContextMenu_Closed;

            cntxMenu.Style = null;
            mnuItem1.Style = null;
            //mnuItem2.Style = null;


            //cntxMenu.Items.Remove(mnuItem1);
            //cntxMenu.Items.Remove(mnuItem2);
            cntxMenu.Items.Clear();

            optnBtn.ContextMenu = null;
        }

        private void MnuEditFlwngList_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (musicModel != null)
            {
                //DownloadOrAddToAlbumPopUpWrapper.Show(musicModel, true);
                musicModel = null;
            }
        }

        private void MnuUnfollow_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (musicModel != null)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "Music Page"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                if (isTrue)
                {
                    new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_MUSICPAGE, musicModel.UserTableID, 1, null, null, musicModel.MusicPageId).StartThread();
                    musicModel = null;
                }
            }
        }

        #endregion "ContextMenu"

        Button optnBtn;
        private MusicPageModel musicModel = null;
        private MusicPageModel prevMusicModel = null;
        //MusicPageModel musicModel = null;

        #region "Event Triggers"

        public void feedScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            if (scrollViewer.VerticalOffset >= 51)//51 
            {
                //UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.upArrowContainerPanel.Visibility = Visibility.Visible;
            }
            else
            {
                //UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.upArrowContainerPanel.Visibility = Visibility.Collapsed;
            }
        }
        private void optnBtn_Click(object sender, RoutedEventArgs e)
        {
            optnBtn = (Button)sender;
            if (optnBtn.DataContext is MusicPageModel)
            {
                musicModel = (MusicPageModel)optnBtn.DataContext;
                if (cntxMenu.Items.Count == 0 || optnBtn.ContextMenu != cntxMenu)
                {
                    optnBtn.ContextMenu = cntxMenu;
                    SetupMenuItem();
                }
                cntxMenu.IsOpen = true;
                musicModel.IsContextMenuOpened = true;
                if (prevMusicModel != null && musicModel.MusicPageId != prevMusicModel.MusicPageId)
                {
                    prevMusicModel.IsContextMenuOpened = false;
                }
                prevMusicModel = musicModel;
                optnBtn.ContextMenu.IsVisibleChanged -= ContextMenu_IsVisibleChanged;
                optnBtn.ContextMenu.IsVisibleChanged += ContextMenu_IsVisibleChanged;
            }
        }
        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false)
            {
                if (musicModel != null)
                    musicModel.IsContextMenuOpened = false;
                optnBtn.ContextMenu.IsVisibleChanged -= ContextMenu_IsVisibleChanged;
            }
        }

        private void LoadMore_ButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            RequestToGetFollowingMediaPages();
        }

        private void pageName_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TextBlock tb = (TextBlock)sender;
            if (tb.DataContext is MusicPageModel)
            {
                musicModel = (MusicPageModel)tb.DataContext;
                RingIDViewModel.Instance.OnMediaPageProfileButtonClicked(musicModel);
            }
        }


        //private void UserControl_Loaded(object sender, RoutedEventArgs e)
        //{
        //    scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
        //    scroll.ScrollChanged += feedScrlViewer_ScrollChanged;

        //    FollowingMusicPages = RingIDViewModel.Instance.MusicPagesFollowing;
        //    scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);            
        //}
        private double previousScrollHeight = 0;
        //private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        //{
        //    prevMusicModel = null;
        //    scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
        //    FollowingMusicPages = null;
        //    previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;            
        //}
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
                scroll.ScrollChanged += feedScrlViewer_ScrollChanged;
                scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);
                FollowingMusicPages = RingIDViewModel.Instance.MusicPagesFollowing;
                showMoreBtn.Click += LoadMore_ButtonClick;

                //if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();

                //if (FollowingMusicPages.Count == 0 && !showMoreBtn.IsVisible)
                //{
                //    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                //    noMoreTxt.Visibility = Visibility.Visible;
                //}
                //else
                //    noMoreTxt.Visibility = Visibility.Collapsed;
            }
            else
            {
                showMoreBtn.Click -= LoadMore_ButtonClick;
                previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;
                scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
                //FollowingMusicPages = null;
            }
        }
        #endregion

        //public void ShowMoreStates(int state) //0 first success,1false, 2no resp \3 success,4 false, 5 no response
        //{
        //    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
        //    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        //    showMoreLoader.Visibility = Visibility.Collapsed;
        //    switch (state)
        //    {
        //        case 0:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 1:
        //            //
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 2:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //        case 3:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 4:
        //            showMoreBtn.Visibility = Visibility.Collapsed;
        //            showMorePanel.Visibility = Visibility.Collapsed;
        //            break;
        //        case 5:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //    }
        //}
    }
}
=======
﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCFollowingMediaCloudPanel.xaml
    /// </summary>
    public partial class UCFollowingMediaCloudPanel : UserControl, INotifyPropertyChanged
    {
        public CustomFeedScroll scroll;
        public UCFollowingMediaCloudPanel(CustomFeedScroll scroll)
        {
            this.scroll = scroll;
            InitializeComponent();
            this.DataContext = this;
            this.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));

            RequestToGetFollowingMediaPages();
        }

        private void RequestToGetFollowingMediaPages()
        {
            ThradDiscoverOrFollowingChannelsList thradDiscoverOrFollowingChannelsList = new ThradDiscoverOrFollowingChannelsList();
            thradDiscoverOrFollowingChannelsList.CallBack += (success) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                    GIFCtrlLoader.Visibility = Visibility.Collapsed;
                    showMoreLoader.Visibility = Visibility.Collapsed;
                    if (success == SettingsConstants.RESPONSE_NOTSUCCESS)
                    {
                        showMoreBtn.Visibility = Visibility.Collapsed;
                        if (FollowingMusicPages.Count == 0)
                            noMoreTxt.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        showMoreBtn.Visibility = Visibility.Visible;
                        noMoreTxt.Visibility = Visibility.Collapsed;
                    }
                });
            };
            thradDiscoverOrFollowingChannelsList.StartThread(SettingsConstants.CHANNEL_TYPE_FOLLOWING, SettingsConstants.PROFILE_TYPE_MUSICPAGE, FollowingMusicPages.Count);
        }

        #region "Properties"

        private ObservableCollection<MusicPageModel> _FollowingMusicPages = new ObservableCollection<MusicPageModel>();
        public ObservableCollection<MusicPageModel> FollowingMusicPages
        {
            get
            {
                return _FollowingMusicPages;
            }
            set
            {
                _FollowingMusicPages = value;
                this.OnPropertyChanged("FollowingMusicPages");
            }
        }
        #endregion "Properties"

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #region "ContextMenu"

        ContextMenu contextMenu = new ContextMenu();
        MenuItem mnuItem1 = new MenuItem();
        //MenuItem mnuItem2 = new MenuItem();
        //MenuItem mnuItem3 = new MenuItem();

        private void SetupMenuItem()
        {
            if (contextMenu.Items != null)
                contextMenu.Items.Clear();
            contextMenu.Style = (Style)Application.Current.Resources["CustomCntxtMenu"];
            mnuItem1.Header = "Unfollow";
            //mnuItem2.Header = "Unfollow";

            mnuItem1.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            //mnuItem2.Style = (Style)Application.Current.Resources["ContextMenuItem"];

            contextMenu.Items.Add(mnuItem1);
            //cntxMenu.Items.Add(mnuItem2);

            //mnuItem1.Click -= MnuEditFlwngList_Click;
            mnuItem1.Click -= MenuUnfollowClick;
            contextMenu.Closed -= ContextMenuClosed;
            contextMenu.Closed += ContextMenuClosed;
            //mnuItem1.Click += MnuEditFlwngList_Click;
            mnuItem1.Click += MenuUnfollowClick;

        }

        private void ContextMenuClosed(object sender, RoutedEventArgs e)
        {
            //mnuItem1.Click -= MnuEditFlwngList_Click;
            mnuItem1.Click -= MenuUnfollowClick;
            contextMenu.Closed -= ContextMenuClosed;

            contextMenu.Style = null;
            mnuItem1.Style = null;
            //mnuItem2.Style = null;


            //cntxMenu.Items.Remove(mnuItem1);
            //cntxMenu.Items.Remove(mnuItem2);
            contextMenu.Items.Clear();

            optionButton.ContextMenu = null;
        }

        private void MenuEditFollowingListClick(object sender, System.Windows.RoutedEventArgs e)
        {
            if (musicPageModel != null)
            {
                //DownloadOrAddToAlbumPopUpWrapper.Show(musicModel, true);
                musicPageModel = null;
            }
        }

        private void MenuUnfollowClick(object sender, System.Windows.RoutedEventArgs e)
        {
            if (musicPageModel != null)
            {
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "Music Page"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                if (isTrue)
                {
                    new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_MUSICPAGE, musicPageModel.UserTableID, 1, null, null, musicPageModel.MusicPageId).StartThread();
                    musicPageModel = null;
                }
            }
        }

        #endregion "ContextMenu"

        Button optionButton;
        private MusicPageModel musicPageModel = null;
        private MusicPageModel previousMusicPageModel = null;
        //MusicPageModel musicModel = null;

        #region "Event Triggers"

        public void feedScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var scrollViewer = (ScrollViewer)sender;
            if (scrollViewer.VerticalOffset >= 51)//51 
            {
                //UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.upArrowContainerPanel.Visibility = Visibility.Visible;
            }
            else
            {
                //UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.upArrowContainerPanel.Visibility = Visibility.Collapsed;
            }
        }
        private void OptionButtonClicked(object sender, RoutedEventArgs e)
        {
            optionButton = (Button)sender;
            if (optionButton.DataContext is MusicPageModel)
            {
                musicPageModel = (MusicPageModel)optionButton.DataContext;
                if (contextMenu.Items.Count == 0 || optionButton.ContextMenu != contextMenu)
                {
                    optionButton.ContextMenu = contextMenu;
                    SetupMenuItem();
                }
                contextMenu.IsOpen = true;
                musicPageModel.IsContextMenuOpened = true;
                if (previousMusicPageModel != null && musicPageModel.MusicPageId != previousMusicPageModel.MusicPageId)
                {
                    previousMusicPageModel.IsContextMenuOpened = false;
                }
                previousMusicPageModel = musicPageModel;
                optionButton.ContextMenu.IsVisibleChanged -= ContextMenu_IsVisibleChanged;
                optionButton.ContextMenu.IsVisibleChanged += ContextMenu_IsVisibleChanged;
            }
        }
        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == false)
            {
                if (musicPageModel != null)
                    musicPageModel.IsContextMenuOpened = false;
                optionButton.ContextMenu.IsVisibleChanged -= ContextMenu_IsVisibleChanged;
            }
        }

        private void LoadMoreButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            showMoreBtn.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            RequestToGetFollowingMediaPages();
        }

        private void PageNameMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TextBlock textBlock = (TextBlock)sender;
            if (textBlock.DataContext is MusicPageModel)
            {
                musicPageModel = (MusicPageModel)textBlock.DataContext;
                RingIDViewModel.Instance.OnMediaPageProfileButtonClicked(musicPageModel);
            }
        }


        //private void UserControl_Loaded(object sender, RoutedEventArgs e)
        //{
        //    scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
        //    scroll.ScrollChanged += feedScrlViewer_ScrollChanged;

        //    FollowingMusicPages = RingIDViewModel.Instance.MusicPagesFollowing;
        //    scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);            
        //}
        private double previousScrollHeight = 0;
        //private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        //{
        //    prevMusicModel = null;
        //    scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
        //    FollowingMusicPages = null;
        //    previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;            
        //}
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
                scroll.ScrollChanged += feedScrlViewer_ScrollChanged;
                scroll.ScrollToVerticalOffset((previousScrollHeight >= 180) ? (previousScrollHeight + 180) : previousScrollHeight);
                FollowingMusicPages = RingIDViewModel.Instance.MusicPagesFollowing;
                showMoreBtn.Click += LoadMoreButtonClick;

                //if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();

                //if (FollowingMusicPages.Count == 0 && !showMoreBtn.IsVisible)
                //{
                //    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
                //    noMoreTxt.Visibility = Visibility.Visible;
                //}
                //else
                //    noMoreTxt.Visibility = Visibility.Collapsed;
            }
            else
            {
                showMoreBtn.Click -= LoadMoreButtonClick;
                previousScrollHeight = (scroll.VerticalOffset >= 180) ? (scroll.VerticalOffset - 180) : scroll.VerticalOffset;
                scroll.ScrollChanged -= feedScrlViewer_ScrollChanged;
                //FollowingMusicPages = null;
            }
        }
        #endregion

        //public void ShowMoreStates(int state) //0 first success,1false, 2no resp \3 success,4 false, 5 no response
        //{
        //    if (GIFCtrlLoader != null && GIFCtrlLoader.IsRunning()) this.GIFCtrlLoader.StopAnimate();
        //    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
        //    showMoreLoader.Visibility = Visibility.Collapsed;
        //    switch (state)
        //    {
        //        case 0:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 1:
        //            //
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 2:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //        case 3:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            break;
        //        case 4:
        //            showMoreBtn.Visibility = Visibility.Collapsed;
        //            showMorePanel.Visibility = Visibility.Collapsed;
        //            break;
        //        case 5:
        //            showMoreBtn.Visibility = Visibility.Visible;
        //            CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
        //            break;
        //    }
        //}
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

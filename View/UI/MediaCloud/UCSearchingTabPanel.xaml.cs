﻿using Models.Constants;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.RingPlayer;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCSearchingTabPanel.xaml
    /// </summary>
    public partial class UCSearchingTabPanel : UserControl, INotifyPropertyChanged
    {
        public string SuggestionSelected = null;
        public bool SongsRequested, AlbumsRequested, TagsRequested, AllRequested;

        #region "Constructor"
        public UCSearchingTabPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            searchControl.SelectionChanged += searchControl_SelectionChanged;
        }
        #endregion "Constructor"

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion "PropertyChanged"

        #region "Property"
        private ObservableCollection<MediaContentModel> _AlbumsFromSearch = new ObservableCollection<MediaContentModel>();
        public ObservableCollection<MediaContentModel> AlbumsFromSearch
        {
            get
            {
                return _AlbumsFromSearch;
            }
            set
            {
                _AlbumsFromSearch = value;
                this.OnPropertyChanged("AlbumsFromSearch");
            }
        }

        private ObservableCollection<HashTagModel> _HashTagsFromSearch = new ObservableCollection<HashTagModel>();
        public ObservableCollection<HashTagModel> HashTagsFromSearch
        {
            get
            {
                return _HashTagsFromSearch;
            }
            set
            {
                _HashTagsFromSearch = value;
                this.OnPropertyChanged("HashTagsFromSearch");
            }
        }

        private ObservableCollection<SingleMediaModel> _MediasAllTab = new ObservableCollection<SingleMediaModel>();
        public ObservableCollection<SingleMediaModel> MediasAllTab
        {
            get
            {
                return _MediasAllTab;
            }
            set
            {
                _MediasAllTab = value;
                this.OnPropertyChanged("MediasAllTab");
            }
        }
        private ObservableCollection<MediaContentModel> _AlbumsAllTab = new ObservableCollection<MediaContentModel>();
        public ObservableCollection<MediaContentModel> AlbumsAllTab
        {
            get
            {
                return _AlbumsAllTab;
            }
            set
            {
                _AlbumsAllTab = value;
                this.OnPropertyChanged("AlbumsAllTab");
            }
        }

        private ObservableCollection<HashTagModel> _HashTagsAllTab = new ObservableCollection<HashTagModel>();
        public ObservableCollection<HashTagModel> HashTagsAllTab
        {
            get
            {
                return _HashTagsAllTab;
            }
            set
            {
                _HashTagsAllTab = value;
                this.OnPropertyChanged("HashTagsAllTab");
            }
        }

        private UCMediaShareMoreOptions ucMediaShareMoreOptions { get; set; }
        #endregion "Property"

        public void LoadIntoSearchUI(int idxType, string suggestionSelected)
        {
            if (suggestionSelected != null)
                SuggestionSelected = suggestionSelected;
            SongsRequested = false;
            AlbumsRequested = false;
            TagsRequested = false;
            AllRequested = false;
            HashTagsFromSearch.Clear();
            AlbumsFromSearch.Clear();
            MediasAllTab.Clear();
            AlbumsAllTab.Clear();
            HashTagsAllTab.Clear();

            if (searchControl.SelectedIndex != idxType)
                searchControl.SelectedIndex = idxType;
            else SwitchToTabItem(idxType);
        }

        private void searchControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int idx = searchControl.SelectedIndex;
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeMediaCloudSearch, searchControl.SelectedIndex);
            SwitchToTabItem(idx);
        }
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                int idx = searchControl.SelectedIndex;
                SwitchToTabItem(idx);
            }
            else
            {
            }
        }
        public void SwitchToTabItem(int idx)
        {
            switch (idx)
            {
                case 0:
                    allTabGrd.Visibility = Visibility.Visible;
                    allTabContentsBdr.Visibility = Visibility.Collapsed;
                    if (!AllRequested)
                    {
                        AllRequested = true;
                        //allShowMoreText.Visibility = Visibility.Collapsed;
                        allShowMorePanel.Visibility = Visibility.Visible;
                        allGIFCtrl.Visibility = Visibility.Visible;
                        allGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                        noAllTxtBlock.Visibility = Visibility.Collapsed;
                        allSearchItemsContainer.Visibility = Visibility.Collapsed;
                        ThreadMediaCloudItemsSearch thrd = new ThreadMediaCloudItemsSearch();
                        thrd.CallBackEvent += (success) =>
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (allGIFCtrl.IsRunning()) allGIFCtrl.StopAnimate();
                                allGIFCtrl.Visibility = Visibility.Collapsed;
                                if (success)
                                {
                                    allSearchItemsContainer.Visibility = Visibility.Visible;
                                    allShowMorePanel.Visibility = Visibility.Visible;
                                    //allShowMoreText.Visibility = Visibility.Visible;
                                }
                                else
                                {
                                    noAllTxtBlock.Visibility = Visibility.Visible;
                                    allShowMorePanel.Visibility = Visibility.Collapsed;
                                    //allShowMoreText.Visibility = Visibility.Collapsed;
                                }
                            });
                        };
                        thrd.StartThread(SuggestionSelected, SettingsConstants.MEDIA_SEARCH_TYPE_ALL, 0, Guid.Empty);
                    }
                    //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(0, 1);
                    break;
                case 1:
                    if (UCMediaContentsView.Instance == null)
                    {
                        SongsRequested = false;
                        UCMediaContentsView.Instance = new UCMediaContentsView();
                        SongsTabBorderPanel.Child = UCMediaContentsView.Instance;
                        UCMediaContentsView.Instance.ShowMediaSearchResults(SuggestionSelected);
                    }
                    else if (UCMediaContentsView.Instance.Parent is Border)
                    {
                        Border bdr = (Border)UCMediaContentsView.Instance.Parent;
                        if (bdr != SongsTabBorderPanel)
                        {
                            SongsRequested = false;
                            bdr.Child = null;
                            SongsTabBorderPanel.Child = UCMediaContentsView.Instance;
                            UCMediaContentsView.Instance.ShowMediaSearchResults(SuggestionSelected);
                        }
                    }
                    if (!SongsRequested)
                    {
                        SongsRequested = true;
                        UCMediaContentsView.Instance.SearchParam = SuggestionSelected;
                        UCMediaContentsView.Instance.SingleMediaItems.Clear();
                        if (UCMediaContentsView.Instance.GIFCtrl.IsRunning()) UCMediaContentsView.Instance.GIFCtrl.StopAnimate();
                        UCMediaContentsView.Instance.showMoreText.Visibility = Visibility.Collapsed;
                        UCMediaContentsView.Instance.noTxtBlock.Visibility = Visibility.Collapsed;
                        UCMediaContentsView.Instance.searchTxtName.Text = SuggestionSelected;
                        UCMediaContentsView.Instance.BigGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                        UCMediaContentsView.Instance.itemsContainer.Visibility = Visibility.Collapsed;
                        ThreadMediaCloudItemsSearch thrd = new ThreadMediaCloudItemsSearch();
                        thrd.CallBackEvent += (success) =>
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                                {
                                    if (UCMediaContentsView.Instance.BigGIFCtrl.IsRunning()) UCMediaContentsView.Instance.BigGIFCtrl.StopAnimate();
                                    if (success)
                                    {
                                        UCMediaContentsView.Instance.itemsContainer.Visibility = Visibility.Visible;
                                        UCMediaContentsView.Instance.noTxtBlock.Visibility = Visibility.Collapsed;
                                        UCMediaContentsView.Instance.showMorePanel.Visibility = Visibility.Visible;
                                        UCMediaContentsView.Instance.showMoreText.Visibility = Visibility.Visible;
                                    }
                                    else
                                    {
                                        UCMediaContentsView.Instance.noTxtBlock.Visibility = Visibility.Visible;
                                        UCMediaContentsView.Instance.showMorePanel.Visibility = Visibility.Collapsed;
                                        UCMediaContentsView.Instance.showMoreText.Visibility = Visibility.Collapsed;
                                    }
                                });
                        };
                        thrd.StartThread(SuggestionSelected, SettingsConstants.MEDIA_SEARCH_TYPE_SONGS, 0, Guid.Empty);
                    }
                    //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(1, 1);
                    break;
                case 2:
                    albumsGrd.Visibility = Visibility.Visible;
                    mediaContentsBdr.Visibility = Visibility.Collapsed;
                    if (!AlbumsRequested)
                    {
                        AlbumsRequested = true;
                        albumShowMoreText.Visibility = Visibility.Collapsed;
                        albumShowMorePanel.Visibility = Visibility.Visible;
                        noAlbumBlock.Visibility = Visibility.Collapsed;
                        albumGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                        noAlbumBlock.Visibility = Visibility.Collapsed;
                        albumsItemsContainer.Visibility = Visibility.Collapsed;
                        ThreadMediaCloudItemsSearch thrd = new ThreadMediaCloudItemsSearch();
                        thrd.CallBackEvent += (success) =>
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (albumGIFCtrl.IsRunning()) albumGIFCtrl.StopAnimate();
                                if (success)
                                {
                                    albumsItemsContainer.Visibility = Visibility.Visible;
                                    albumShowMorePanel.Visibility = Visibility.Visible;
                                    albumShowMoreText.Visibility = Visibility.Visible;
                                }
                                else
                                {
                                    noAlbumBlock.Visibility = Visibility.Visible;
                                    searchTxtName.Text = SuggestionSelected;
                                    albumShowMorePanel.Visibility = Visibility.Collapsed;
                                    albumShowMoreText.Visibility = Visibility.Collapsed;
                                }
                            });
                        };
                        thrd.StartThread(SuggestionSelected, SettingsConstants.MEDIA_SEARCH_TYPE_ALBUMS, 0, Guid.Empty);
                    }
                    //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(2, 1);
                    break;
                case 3:
                    hashTagsGrd.Visibility = Visibility.Visible;
                    hashMediaContentsBdr.Visibility = Visibility.Collapsed;
                    if (!TagsRequested)
                    {
                        TagsRequested = true;
                        hashShowMoreText.Visibility = Visibility.Collapsed;
                        hashShowMorePanel.Visibility = Visibility.Visible;
                        noHashBlock.Visibility = Visibility.Collapsed;
                        hashGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                        noHashBlock.Visibility = Visibility.Collapsed;
                        hashTagItemsContainer.Visibility = Visibility.Collapsed;
                        ThreadMediaCloudItemsSearch thrd = new ThreadMediaCloudItemsSearch();
                        thrd.CallBackEvent += (success) =>
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (hashGIFCtrl.IsRunning()) hashGIFCtrl.StopAnimate();
                                if (success)
                                {
                                    hashTagItemsContainer.Visibility = Visibility.Visible;
                                    //hashShowMorePanel.Visibility = Visibility.Visible;
                                    //hashShowMoreText.Visibility = Visibility.Visible;
                                }
                                else
                                {
                                    noHashBlock.Visibility = Visibility.Visible;
                                    searchHashTxtName.Text = SuggestionSelected;
                                    //hashShowMorePanel.Visibility = Visibility.Collapsed;
                                    //hashShowMoreText.Visibility = Visibility.Collapsed;
                                }
                                hashShowMorePanel.Visibility = Visibility.Collapsed;
                            });
                        };
                        thrd.StartThread(SuggestionSelected, SettingsConstants.MEDIA_SEARCH_TYPE_TAGS, 0, Guid.Empty);
                    }
                    //MiddlePanelSwitcher.pageSwitcher.InsertTabIndexInMediaCloudVisited(3, 1);
                    break;
            }
        }

        private void ShowOptionsPopup(SingleMediaModel mediaToPlay, UIElement btn)
        {
            if (ucMediaShareMoreOptions != null)
                mainPanel.Children.Remove(ucMediaShareMoreOptions);
            ucMediaShareMoreOptions = new UCMediaShareMoreOptions(mainPanel);
            ucMediaShareMoreOptions.Show();
            ucMediaShareMoreOptions.ShowOptionsPopup(mediaToPlay, btn, mediaToPlay.MediaType, -1);
            ucMediaShareMoreOptions.OnRemovedUserControl += () =>
            {
                mediaToPlay.ShareOptionsPopup = false;
                ucMediaShareMoreOptions = null;
                //if (ImageViewInMain != null)
                //{
                //    ImageViewInMain.GrabKeyboardFocus();
                //}
            };
        }
        #region "Event Trigger"

        private void AlbumClick_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Control ctrl = sender as Control;
            if (ctrl.DataContext is MediaContentModel)
            {
                MediaContentModel model = (MediaContentModel)ctrl.DataContext;
                albumsGrd.Visibility = Visibility.Collapsed;
                mediaContentsBdr.Visibility = Visibility.Visible;
                if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                mediaContentsBdr.Child = UCMediaContentsView.Instance;

                UCMediaContentsView.Instance.ShowAlbumContents(model, () =>
                {
                    albumsGrd.Visibility = Visibility.Visible;
                    mediaContentsBdr.Visibility = Visibility.Collapsed;
                    return 0;
                });
            }
        }

        private void MediaPlayClick_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Control ctrl = sender as Control;
            if (ctrl.DataContext is HashTagModel)
            {
                HashTagModel model = (HashTagModel)ctrl.DataContext;
                hashTagsGrd.Visibility = Visibility.Collapsed;
                hashMediaContentsBdr.Visibility = Visibility.Visible;
                if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                hashMediaContentsBdr.Child = UCMediaContentsView.Instance;
                UCMediaContentsView.Instance.ShowHashTagContents(model, () =>
                {
                    hashTagsGrd.Visibility = Visibility.Visible;
                    hashMediaContentsBdr.Visibility = Visibility.Collapsed;
                    return 0;
                });
            }
        }

        private void hashShowMoreText_Click(object sender, RoutedEventArgs e)
        {
            //long maxHashTagId = (HashTagsFromSearch.Count > 0) ? HashTagsFromSearch.OrderByDescending(item => item.HashTagId).First().HashTagId : 0;
            hashShowMoreText.Visibility = Visibility.Collapsed;
            hashGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            ThreadMediaCloudItemsSearch thrd = new ThreadMediaCloudItemsSearch();
            thrd.CallBackEvent += (success) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (hashGIFCtrl.IsRunning()) hashGIFCtrl.StopAnimate();
                    if (success)
                    {
                        hashShowMoreText.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        hashShowMoreText.Visibility = Visibility.Collapsed;
                        hashShowMorePanel.Visibility = Visibility.Collapsed;
                    }
                });
            };
            thrd.StartThread(SuggestionSelected, SettingsConstants.MEDIA_SEARCH_TYPE_TAGS, HashTagsFromSearch.Count, Guid.Empty);
        }

        private void albumShowMoreText_Click(object sender, RoutedEventArgs e)
        {
            //Guid maxAlbumId = (AlbumsFromSearch.Count > 0) ? AlbumsFromSearch.OrderByDescending(item => item.AlbumId).First().AlbumId : Guid.Empty;
            albumShowMoreText.Visibility = Visibility.Collapsed;
            albumGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
            ThreadMediaCloudItemsSearch thrd = new ThreadMediaCloudItemsSearch();
            thrd.CallBackEvent += (success) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (albumGIFCtrl.IsRunning()) albumGIFCtrl.StopAnimate();
                    if (success)
                    {
                        albumShowMoreText.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        albumShowMoreText.Visibility = Visibility.Collapsed;
                        albumShowMorePanel.Visibility = Visibility.Collapsed;
                    }
                });
            };
            thrd.StartThread(SuggestionSelected, SettingsConstants.MEDIA_SEARCH_TYPE_ALBUMS, AlbumsFromSearch.Count, Guid.Empty);
        }

        //private void allShowMoreText_Click(object sender, RoutedEventArgs e)
        //{
        //long maxAll = (HashTagsFromSearch.Count > 0) ? HashTagsFromSearch.OrderByDescending(item => item.HashTagId).First().HashTagId : 0;
        //allShowMoreText.Visibility = Visibility.Collapsed;
        //allGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
        //ThreadMediaCloudItemsSearch thrd = new ThreadMediaCloudItemsSearch();
        //thrd.CallBackEvent += (success) =>
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        if (allGIFCtrl.IsRunning()) hashGIFCtrl.StopAnimate();
        //        if (success)
        //        {
        //            allShowMoreText.Visibility = Visibility.Visible;
        //        }
        //        else
        //        {
        //            allShowMoreText.Visibility = Visibility.Collapsed;
        //            allShowMorePanel.Visibility = Visibility.Collapsed;
        //        }
        //    });
        //};
        //thrd.StartThread(SuggestionSelected, SettingsConstants.MEDIA_SEARCH_TYPE_ALL, 1, maxHashTagId);
        //}

        private void allHashTag_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Control ctrl = sender as Control;
            if (ctrl.DataContext is HashTagModel)
            {
                HashTagModel model = (HashTagModel)ctrl.DataContext;
                allTabGrd.Visibility = Visibility.Collapsed;
                allTabContentsBdr.Visibility = Visibility.Visible;
                if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                allTabContentsBdr.Child = UCMediaContentsView.Instance;
                UCMediaContentsView.Instance.ShowHashTagContents(model, () =>
                {
                    allTabGrd.Visibility = Visibility.Visible;
                    allTabContentsBdr.Visibility = Visibility.Collapsed;
                    return 0;
                });
            }
        }

        private void allAlbumClick_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Control ctrl = sender as Control;
            if (ctrl.DataContext is MediaContentModel)
            {
                MediaContentModel model = (MediaContentModel)ctrl.DataContext;
                allTabGrd.Visibility = Visibility.Collapsed;
                allTabContentsBdr.Visibility = Visibility.Visible;
                if (UCMediaContentsView.Instance == null) UCMediaContentsView.Instance = new UCMediaContentsView();
                else if (UCMediaContentsView.Instance.Parent is Border) ((Border)UCMediaContentsView.Instance.Parent).Child = null;
                allTabContentsBdr.Child = UCMediaContentsView.Instance;

                UCMediaContentsView.Instance.ShowAlbumContents(model, () =>
                {
                    allTabGrd.Visibility = Visibility.Visible;
                    allTabContentsBdr.Visibility = Visibility.Collapsed;
                    return 0;
                });
            }
        }
        #endregion "Event Trigger"
        #region "ICommand"
        private ICommand _MediaPlayCommand;
        public ICommand MediaPlayCommand
        {
            get
            {
                if (_MediaPlayCommand == null)
                {
                    _MediaPlayCommand = new RelayCommand(param => OnMediaPlayCommand(param));
                }
                return _MediaPlayCommand;
            }
        }

        public void OnMediaPlayCommand(object parameter)
        {
            if (parameter is SingleMediaModel)
            {
                SingleMediaModel mediaToPlay = (SingleMediaModel)parameter;
                int idx = MediasAllTab.IndexOf(mediaToPlay);
                if (idx >= 0 && idx < MediasAllTab.Count)
                    MediaUtility.RunPlayList(false, Guid.Empty, MediasAllTab, null, idx);
            }
        }

        private ICommand _MediaOptionCommand;
        public ICommand MediaOptionCommand
        {
            get
            {
                if (_MediaOptionCommand == null)
                {
                    _MediaOptionCommand = new RelayCommand(param => OnMediaOptionCommand(param));
                }
                return _MediaOptionCommand;
            }
        }

        public void OnMediaOptionCommand(object parameter)
        {
            if (parameter is Button)
            {
                Button btn = (Button)parameter;
                SingleMediaModel mediaToPlay = (SingleMediaModel)btn.DataContext;
                mediaToPlay.ShareOptionsPopup = true;

                ShowOptionsPopup(mediaToPlay, btn);
                //MainSwitcher.PopupController.MediaListFromAlbumClickWrapper.Show(mediaToPlay, btn, mediaToPlay.MediaType, -1, () =>
                //{
                //    mediaToPlay.ShareOptionsPopup = false;
                //    return 0;
                //});
            }
        }
        #endregion "ICommand"
    }
}

﻿using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility.Auth;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCRecentTrendingSearch.xaml
    /// </summary>
    public partial class UCRecentTrendingSearch : UserControl, INotifyPropertyChanged
    {
        public UCRecentTrendingSearch()
        {
            new DatabaseActivityDAO().FetchMediaSearchList();
            InitializeComponent();
            if (MediaDictionaries.Instance.MEDIA_SEARCHES.Count > 0)
            {
                foreach (var item in MediaDictionaries.Instance.MEDIA_SEARCHES)
                {
                    SearchMediaModel model = new SearchMediaModel();
                    model.LoadData(item);
                    RecentMediaSearches.Add(model);
                }
            }
            DefaultSettings.SearchTrendsSeq = 0;
            (new AuthRequestNoResult()).StartThread(new JObject(), AppConstants.TYPE_SEARCH_TRENDS, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private ObservableCollection<SearchMediaModel> _RecentMediaSearches = new ObservableCollection<SearchMediaModel>();
        public ObservableCollection<SearchMediaModel> RecentMediaSearches
        {
            get
            {
                return _RecentMediaSearches;
            }
            set
            {
                _RecentMediaSearches = value;
                this.OnPropertyChanged("RecentMediaSearches");
            }
        }

        private ObservableCollection<SearchMediaModel> _RecentMediaTrends = new ObservableCollection<SearchMediaModel>();
        public ObservableCollection<SearchMediaModel> RecentMediaTrends
        {
            get
            {
                return _RecentMediaTrends;
            }
            set
            {
                _RecentMediaTrends = value;
                this.OnPropertyChanged("RecentMediaTrends");
            }
        }

        private void AnySearch_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn.DataContext is SearchMediaModel)
            {
                SearchMediaModel model = (SearchMediaModel)btn.DataContext;
                RecentMediaSearches.Insert(0, model);
                UCMiddlePanelSwitcher._UCMediaCloudSearch.FullSearchByType(model.SearchSuggestion, model.SearchType, model.UpdateTime);
            }
        }
    }
}

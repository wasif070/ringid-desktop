﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Auth;

namespace View.UI.MediaCloud
{
    /// <summary>
    /// Interaction logic for UCSearchSongTabPanel.xaml
    /// </summary>
    public partial class UCSearchSongTabPanel : UserControl, INotifyPropertyChanged
    {
        public UCSearchSongTabPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private ObservableCollection<SingleMediaModel> _MediasFromSearch = new ObservableCollection<SingleMediaModel>();
        public ObservableCollection<SingleMediaModel> MediasFromSearch
        {
            get
            {
                return _MediasFromSearch;
            }
            set
            {
                _MediasFromSearch = value;
                this.OnPropertyChanged("MediasFromSearch");
            }
        }
        private Visibility txtVisibility = Visibility.Collapsed;
        public Visibility TxtVisibility
        {
            get { return txtVisibility; }
            set
            {
                if (value == txtVisibility)
                    return;
                txtVisibility = value;
                this.OnPropertyChanged("TxtVisibility");
            }
        }
        private string _TooltipText = string.Empty; //No uses but to Avoid Binding Error
        public string TooltipText
        {
            get { return _TooltipText; }
        }
        private Visibility _DeleteButtonVisibility = Visibility.Collapsed; //No uses but to Avoid Binding Error
        public Visibility DeleteButtonVisibility
        {
            get
            {
                return _DeleteButtonVisibility;
            }
        }
        private ICommand _RemoveFromListCommand;
        public ICommand RemoveFromListCommand //No uses but to Avoid Binding Error
        {
            get
            {
                return _RemoveFromListCommand;
            }
        }

        //public void LoadStates(int loadingState) //0=loading, 1=results came, 2= no results
        //{
        //    try
        //    {
        //        if (loadingState == 0)
        //        {
        //            UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_SMALL));
        //            TxtVisibility = Visibility.Collapsed;
        //        }
        //        else if (loadingState == 1)
        //        {
        //            if (UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.GIFCtrl != null &&
        //                UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.GIFCtrl.IsRunning()) UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.GIFCtrl.StopAnimate();
        //            TxtVisibility = Visibility.Collapsed;
        //            VisibilityShowMore(true);
        //        }
        //        else if (loadingState == 2)
        //        {
        //            if (UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.GIFCtrl != null &&
        //                UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.GIFCtrl.IsRunning()) UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.GIFCtrl.StopAnimate();
        //            if (MediasFromSearch.Count == 0)
        //            {
        //                noTaxt.Text = "No Song found for";
        //                nameTxt.Text = UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.SearchTermTextBox.Text;
        //                TxtVisibility = Visibility.Visible;
        //            }
        //            VisibilityShowMore(false);
        //        }
        //    }
        //    catch (System.Exception) { }
        //}

        public void Clear()
        {
            lock (MediasFromSearch)
            {
                while (MediasFromSearch.Count > 0)
                {
                    SingleMediaModel model = MediasFromSearch.FirstOrDefault();
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        MediasFromSearch.Remove(model);
                        GC.SuppressFinalize(model);
                    }, DispatcherPriority.Send);
                }
            }
        }

        public void VisibilityShowMore(bool showMore)
        {
            if (showMore)
            {
                showMorePanel.Visibility = Visibility.Visible;
                showMoreText.Visibility = Visibility.Visible;
                showMoreLoader.Visibility = Visibility.Collapsed;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            }
            else
            {
                showMorePanel.Visibility = Visibility.Collapsed;
                showMoreText.Visibility = Visibility.Collapsed;
                showMoreLoader.Visibility = Visibility.Collapsed;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            }
        }
        public void LoadMoreFailed(bool noData)
        {
            showMoreLoader.Visibility = Visibility.Collapsed;
            if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
            if (noData)
            {
                showMorePanel.Visibility = Visibility.Collapsed;
                showMoreText.Visibility = Visibility.Collapsed;
            }
            else
            {
                showMorePanel.Visibility = Visibility.Visible;
                showMoreText.Visibility = Visibility.Visible;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            showMoreText.Click += ShowMore_PanelClick;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            showMoreText.Click -= ShowMore_PanelClick;
        }

        private void ShowMore_PanelClick(object sender, RoutedEventArgs e)
        {
            Guid maxContentId = (MediasFromSearch.Count > 0) ? MediasFromSearch.OrderByDescending(item => item.ContentId).First().ContentId : Guid.Empty;
            SendDataToServer.MediaFullSearchByType(UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.SuggestionSelected, SettingsConstants.MEDIA_SEARCH_TYPE_SONGS, 1, maxContentId);
            showMoreText.Visibility = Visibility.Collapsed;
            showMoreLoader.Visibility = Visibility.Visible;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED));
        }
    }
}

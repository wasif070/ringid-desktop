﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using View.BindingModels;
using View.Constants;

namespace View.UI.Group
{
    /// <summary>
    /// Interaction logic for WNGroupChatCallPanel.xaml
    /// </summary>
    public partial class WNGroupChatCallPanel : Window, INotifyPropertyChanged
    {
        private GroupInfoModel _GroupInfoModel;

        #region Constructor

        public WNGroupChatCallPanel(GroupInfoModel model)
        {
            InitializeComponent();
            this.GroupInfoModel = model;
            this.WindowMinWidth = RingIDSettings.MAIN_RIGHT_CONTENT_WIDTH;
            this.WindowMaxWidth = RingIDSettings.MAIN_RIGHT_CONTENT_WIDTH;
            this.WindowMinHeight = RingIDSettings.FRAME_DEFAULT_HEIGHT;
            this.Left = (SystemParameters.PrimaryScreenWidth / 2) - (WindowMaxWidth / 2);
            this.Top = (SystemParameters.PrimaryScreenHeight / 2) - (WindowMinHeight / 2) - 20;
            this.DataContext = this;
        }

        #endregion Constructor

        #region Property

        public GroupInfoModel GroupInfoModel
        {
            get { return _GroupInfoModel; }
            set { _GroupInfoModel = value; }
        }

        private int _WindowMinWidth;
        public int WindowMinWidth
        {
            get
            {
                return this._WindowMinWidth;
            }
            set
            {
                this._WindowMinWidth = value;
                this.OnPropertyChanged("WindowMinWidth");
            }
        }

        private int _WindowMaxWidth;
        public int WindowMaxWidth
        {
            get
            {
                return this._WindowMaxWidth;
            }
            set
            {
                this._WindowMaxWidth = value;
                this.OnPropertyChanged("WindowMaxWidth");
            }
        }

        private int _WindowMinHeight;
        public int WindowMinHeight
        {
            get
            {
                return this._WindowMinHeight;
            }
            set
            {
                this._WindowMinHeight = value;
                this.OnPropertyChanged("WindowMinHeight");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Property
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.Entity;
using View.BindingModels;
using View.Utility;
using View.Utility.Chat;
using View.Utility.FriendProfile;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using System.Windows.Threading;
using View.Constants;
using System.Windows.Data;
using System.Collections.ObjectModel;
using View.UI.PopUp;

namespace View.UI.Group
{
    /// <summary>
    /// Interaction logic for UCGroupOwnerLeave.xaml
    /// </summary>
    public partial class UCGroupOwnerLeave : PopUpBaseControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCGroupOwnerLeave).Name);

        private ICommand _LeaveCommand;
        private ICommand _CancelCommand;
        private ICommand _OnSelectCommand;
        private ICommand _ShowProfileCommand;
        private GroupMemberInfoModel _SelectedMemberInfoModel = null;
        private ObservableCollection<GroupMemberInfoModel> _MemberInfoList = new ObservableCollection<GroupMemberInfoModel>();
        private string _SelectedFullName = String.Empty;
        private bool _IsProcessing = false;

        public delegate void OnCompleted(long ownerID);
        public event OnCompleted CompletedHandler;

        private GroupInfoModel _GroupInfoModel = null;

        public UCGroupOwnerLeave(Grid motherGrid, GroupInfoModel groupInfoModel)
        {
            InitializeComponent();
            this.GroupInfoModel = groupInfoModel;
            this.DataContext = this;
            InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
            SetParent(motherGrid);
        }

        #region Event Handler

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            if (GroupInfoModel != null)
            {
                List<long> uIDs = new List<long>();
                MemberInfoList.Clear();
                foreach (KeyValuePair<long, GroupMemberInfoModel> pair in GroupInfoModel.MemberInfoDictionary)
                {
                    if (pair.Value.UserTableID != DefaultSettings.LOGIN_TABLE_ID && pair.Value.MemberAccessType != ChatConstants.MEMBER_TYPE_NOT_MEMBER)
                    {
                        uIDs.Add(pair.Key);
                        pair.Value.IsChecked = false;
                        MemberInfoList.InvokeAdd(pair.Value);
                    }
                }
                new ThreadFriendPresenceInfo(uIDs);

                itemsControl.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding
                {
                    Path = new PropertyPath("MemberInfoList"),
                });
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox chkBox = (CheckBox)sender;
            if (chkBox.IsChecked != null && (bool)chkBox.IsChecked)
            {
                OnSelect(chkBox.DataContext);
            }
            else
            {
                if (SelectedMemberInfoModel != null)
                {
                    SelectedMemberInfoModel.IsChecked = false;
                }
                SelectedMemberInfoModel = null;
                SelectedFullName = String.Empty;
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void OnSelect(object param)
        {
            GroupMemberInfoModel model = (GroupMemberInfoModel)param;
            if (SelectedMemberInfoModel != null)
            {
                SelectedMemberInfoModel.IsChecked = false;
                if (model.UserTableID == SelectedMemberInfoModel.UserTableID)
                {
                    SelectedMemberInfoModel = null;
                    SelectedFullName = String.Empty;
                    return;
                }
            }
            SelectedMemberInfoModel = model;
            SelectedMemberInfoModel.IsChecked = true;
            SelectedFullName = SelectedMemberInfoModel.BasicInfoModel.ShortInfoModel.FullName;
        }

        private void OnLeaveClick(object param)
        {
            IsProcessing = true;
            this.GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_CLOCK));

            GroupMemberInfoModel memberModel = GroupInfoModel.MemberInfoDictionary.TryGetValue(DefaultSettings.LOGIN_TABLE_ID);
            if (memberModel == null || SelectedMemberInfoModel == null || memberModel.MemberAccessType != ChatConstants.MEMBER_TYPE_OWNER || SelectedMemberInfoModel.MemberAccessType == ChatConstants.MEMBER_TYPE_NOT_MEMBER)
            {
                UIHelperMethods.ShowFailed("Cann't process this request. Please try again.", "Leave failed!");
                OnCancelClick(null);
            }

            List<BaseMemberDTO> memberMsgList = new List<BaseMemberDTO>();
            List<GroupMemberInfoDTO> oldMemberDTOs = new List<GroupMemberInfoDTO>();
            List<GroupMemberInfoModel> memberModelList = new List<GroupMemberInfoModel>();

            SelectedMemberInfoModel.IsReadyForAction = false;
            BaseMemberDTO msgDTO1 = new BaseMemberDTO();
            msgDTO1.MemberIdentity = SelectedMemberInfoModel.UserTableID;
            msgDTO1.RingID = SelectedMemberInfoModel.RingID;
            msgDTO1.Status = ChatConstants.MEMBER_TYPE_OWNER;
            memberMsgList.Add(msgDTO1);

            GroupMemberInfoDTO oldMemberDTO1 = new GroupMemberInfoDTO();
            oldMemberDTO1.UserTableID = SelectedMemberInfoModel.UserTableID;
            oldMemberDTO1.MemberAccessType = SelectedMemberInfoModel.MemberAccessType;
            SelectedMemberInfoModel.MemberAccessType = ChatConstants.MEMBER_TYPE_OWNER;
            oldMemberDTOs.Add(oldMemberDTO1);
            memberModelList.Add(SelectedMemberInfoModel);

            if (memberModel != null)
            {
                memberModel.IsReadyForAction = false;
                BaseMemberDTO msgDTO2 = new BaseMemberDTO();
                msgDTO2.MemberIdentity = DefaultSettings.LOGIN_TABLE_ID;
                msgDTO2.RingID = DefaultSettings.LOGIN_RING_ID;
                msgDTO2.Status = ChatConstants.MEMBER_TYPE_MEMBER;
                memberMsgList.Add(msgDTO2);

                GroupMemberInfoDTO oldMemberDTO2 = new GroupMemberInfoDTO();
                oldMemberDTO2.UserTableID = memberModel.UserTableID;
                oldMemberDTO2.MemberAccessType = memberModel.MemberAccessType;
                memberModel.MemberAccessType = ChatConstants.MEMBER_TYPE_MEMBER;
                oldMemberDTOs.Add(oldMemberDTO2);
                memberModelList.Add(memberModel);
            }

            ChatHelpers.SendMemberAccessType(GroupInfoModel, memberMsgList, oldMemberDTOs, memberModelList, OnCompleteAccessChange);
        }

        private int OnCompleteAccessChange(bool status)
        {
            if (status)
            {
                GroupMemberInfoModel memberModel = GroupInfoModel.MemberInfoDictionary.TryGetValue(DefaultSettings.LOGIN_TABLE_ID);//GroupInfoModel.RemoveMemberData(DefaultSettings.LOGIN_USER_ID);
                if (memberModel != null)
                {
                    memberModel.IsReadyForAction = false;
                    List<GroupMemberInfoDTO> dbList = new List<GroupMemberInfoDTO>();

                    GroupMemberInfoDTO memberDTO = new GroupMemberInfoDTO();
                    memberDTO.GroupID = GroupInfoModel.GroupID;
                    memberDTO.UserTableID = memberModel.UserTableID;
                    memberDTO.IntegerStatus = StatusConstants.STATUS_DELETED;
                    dbList.Add(memberDTO);

                    ChatHelpers.LeaveFromGroup(GroupInfoModel, memberModel, dbList, OnCompleteLeave);
                }
                else
                {
                    OnCompleteLeave(false);
                }
            }
            else
            {
                OnCompleteLeave(false);
            }
            return 0;
        }

        private int OnCompleteLeave(bool status)
        {
            IsProcessing = false;
            Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); }, DispatcherPriority.Send);

            if (status)
            {
                if (CompletedHandler != null)
                {
                    CompletedHandler(SelectedMemberInfoModel.UserTableID);
                }
            }
            else
            {
                if (CompletedHandler != null) CompletedHandler(0);
                UIHelperMethods.ShowFailed("Sorry! Conversation leave request failed. Try again.");
            }
            return 0;
        }

        private void OnCancelClick(object param)
        {
            IsProcessing = false;
            Application.Current.Dispatcher.BeginInvoke(() => { if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate(); }, DispatcherPriority.Send);

            if (CompletedHandler != null)
            {
                CompletedHandler(0);
            }
        }

        private void ShowProfile(object param)
        {
            OnCancelClick(null);
            RingIDViewModel.Instance.OnFriendProfileButtonClicked(param);
        }

        public void Dispose()
        {
            try
            {
                this.IsProcessing = false;
                if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                this.itemsControl.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                this.itemsControl.ItemsSource = null;
                this.DataContext = null;
                this.MemberInfoList.Clear();
                this.SelectedMemberInfoModel = null;
                this.SelectedFullName = String.Empty;
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnLoadedControl()
        {
            _MainBorder.Focusable = true;
            Keyboard.Focus(_MainBorder);
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Property

        public ICommand LeaveCommand
        {
            get
            {
                if (_LeaveCommand == null)
                {
                    _LeaveCommand = new RelayCommand((param) => OnLeaveClick(param), (param) => { return SelectedMemberInfoModel != null; });
                }
                return _LeaveCommand;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand((param) => OnCancelClick(param), (param) => { return !IsProcessing; });
                }
                return _CancelCommand;
            }
        }

        public ICommand OnSelectCommand
        {
            get
            {
                if (_OnSelectCommand == null)
                {
                    _OnSelectCommand = new RelayCommand((param) => OnSelect(param), (param) => { return !IsProcessing; });
                }
                return _OnSelectCommand;
            }
        }

        public ICommand ShowProfileCommand
        {
            get
            {
                if (_ShowProfileCommand == null)
                {
                    _ShowProfileCommand = new RelayCommand((param) => ShowProfile(param), (param) => { return !IsProcessing; });
                }
                return _ShowProfileCommand;
            }
        }
        private ICommand _LoadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (_LoadedControl == null)
                {
                    _LoadedControl = new RelayCommand(param => OnLoadedControl());
                }
                return _LoadedControl;
            }
        }

        public GroupMemberInfoModel SelectedMemberInfoModel
        {
            get { return _SelectedMemberInfoModel; }
            set
            {
                _SelectedMemberInfoModel = value;
                this.OnPropertyChanged("SelectedMemberInfoModel");
            }
        }

        public ObservableCollection<GroupMemberInfoModel> MemberInfoList
        {
            get { return _MemberInfoList; }
            set
            {
                _MemberInfoList = value;
                this.OnPropertyChanged("MemberInfoList");
            }
        }

        public string SelectedFullName
        {
            get { return _SelectedFullName; }
            set
            {
                if (SelectedFullName == value)
                    return;
                _SelectedFullName = value;
                this.OnPropertyChanged("SelectedFullName");
            }
        }

        public bool IsProcessing
        {
            get { return _IsProcessing; }
            set
            {
                if (_IsProcessing == value)
                    return;
                _IsProcessing = value;
                this.OnPropertyChanged("IsProcessing");
            }
        }

        public GroupInfoModel GroupInfoModel
        {
            get { return _GroupInfoModel; }
            set { _GroupInfoModel = value; }
        }

        #endregion Property

    }
}

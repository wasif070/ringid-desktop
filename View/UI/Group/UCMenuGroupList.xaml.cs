﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using View.BindingModels;
using View.Dictonary;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Group
{
    /// <summary>
    /// Interaction logic for UCMenuGroupList.xaml
    /// </summary>
    public partial class UCMenuGroupList : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMenuGroupList).Name);
        private ConcurrentQueue<GroupInfoModel> _ModelQueue = new ConcurrentQueue<GroupInfoModel>();
        private DispatcherTimer _GroupListResizeTimer = null;

        public UCMenuGroupList()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCGroupPanel_Loaded;
            this.Unloaded += UCGroupPanel_Unloaded;
        }

        #region Property

        private string _SearchText = string.Empty;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                _SearchText = value;
                OnPropertyChanged("SearchText");
            }
        }

        private ICommand _OnSelectCommand;
        public ICommand OnSelectCommand
        {
            get
            {
                if (_OnSelectCommand == null)
                {
                    _OnSelectCommand = new RelayCommand((param) => OnSelect(param));
                }
                return _OnSelectCommand;
            }
        }

        private void OnSelect(object param)
        {
            if (param is GroupInfoModel)
            {
                GroupInfoModel model = (GroupInfoModel)param;
                if (model != null)
                {
                    RingIDViewModel.Instance.OnGroupCallChatButtonClicked(model.GroupID);
                }
            }
        }

        #endregion"Properties"

        #region Utility

        private void ChangeFriendOpenStatus()
        {
            try
            {
                if (_GroupListResizeTimer == null)
                {
                    _GroupListResizeTimer = new DispatcherTimer();
                    _GroupListResizeTimer.Interval = TimeSpan.FromSeconds(1);
                    _GroupListResizeTimer.Tick += (o, e) =>
                    {
                        if (Visibility == Visibility.Visible)
                        {
                            HelperMethods.ChangeViewPortOpenedProperty(scvGroupList, itemsControl);
                        }

                        _GroupListResizeTimer.Stop();
                        _GroupListResizeTimer.Interval = TimeSpan.FromSeconds(0.2);
                    };
                }

                _GroupListResizeTimer.Stop();
                _GroupListResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region EventHandler

        private void UCGroupPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.GroupButtonSelection = false;
            itemsControl.SizeChanged -= itemsControl_SizeChanged;
            scvGroupList.ScrollChanged -= scvGroupList_ScrollChanged;
        }

        private void UCGroupPanel_Loaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.GroupButtonSelection = true;
            itemsControl.SizeChanged += itemsControl_SizeChanged;
            scvGroupList.ScrollChanged += scvGroupList_ScrollChanged;
        }

        private void scvGroupList_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                if (Math.Abs(e.VerticalChange) > 0)
                {
                    ChangeFriendOpenStatus();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: scvFriendList_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void itemsControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                ChangeFriendOpenStatus();
            }
            catch (Exception ex)
            {
                log.Error("Error: itemsControl_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion

        private void ContextMenu_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Control control = (Control)sender;
            if (sender != null && control is ContextMenu)
            {
                KeyValuePair<long, GroupInfoModel> pair = (KeyValuePair<long, GroupInfoModel>)control.DataContext;
                GroupInfoModel cmodel = pair.Value;
                if (control.IsVisible)
                {
                    //cmodel.SingleGroupPanelBackgroundColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFf7f8f8"));
                    //cmodel.GroupSettingsBtnVisibilty = Visibility.Visible;
                    cmodel.SingleGroupPanelFocusable = true;
                }
                else
                {
                    //cmodel.SingleGroupPanelBackgroundColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFFFFFFF"));
                    //cmodel.GroupSettingsBtnVisibilty = Visibility.Hidden;
                    cmodel.SingleGroupPanelFocusable = false;
                }
            }
        }

        private void RemoveGroupMember(GroupInfoModel grpInfoModel)
        {
            try
            {
                GroupMemberInfoModel memberModel = grpInfoModel.MemberInfoDictionary.TryGetValue(DefaultSettings.LOGIN_TABLE_ID); //grpInfoModel.RemoveMemberData(DefaultSettings.LOGIN_USER_ID);
                if (memberModel != null)
                {
                    memberModel.IsReadyForAction = false;
                    List<GroupMemberInfoDTO> dbList = new List<GroupMemberInfoDTO>();

                    GroupMemberInfoDTO memberDTO = new GroupMemberInfoDTO();
                    memberDTO.GroupID = grpInfoModel.GroupID;
                    memberDTO.UserTableID = DefaultSettings.LOGIN_TABLE_ID;
                    memberDTO.IntegerStatus = StatusConstants.STATUS_DELETED;
                    dbList.Add(memberDTO);

                    ChatHelpers.LeaveFromGroup(grpInfoModel, memberModel, dbList, (status) =>
                    {
                        if (status)
                        {
                            Application.Current.Dispatcher.BeginInvoke(() =>
                            {
                                UCGroupChatSettings ucGroupChatSettings = UIDictionaries.Instance.GROUP_CHAT_SETTINGS_DICTIONARY.TryGetValue(grpInfoModel.GroupID);
                                if (ucGroupChatSettings != null)
                                {
                                    ucGroupChatSettings.CancelGroupImageChange();
                                    ucGroupChatSettings.CancelGroupNameChange();
                                }
                            });
                        }
                        else UIHelperMethods.ShowWarning("Sorry! Conversation leave request failed. Try again.", "Conversation leave!");
                        return 0;
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: RemoveGroupMember() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CreateNewGroup(object sender, RoutedEventArgs e)
        {
            UCCreateGroupPopUp ucCreateGroupPopUp = new UCCreateGroupPopUp(UCGuiRingID.Instance.MotherPanel);
            ucCreateGroupPopUp.Show();
            ucCreateGroupPopUp.ShowFriendList();
            //DownloadOrAddToAlbumPopUpWrapper.ShowCreateGroupPopUp();
        }

        private void mnItemleaveGroup_Click(object sender, RoutedEventArgs e)
        {
            if (((MenuItem)sender).DataContext is KeyValuePair<long, GroupInfoModel>)
            {
                GroupInfoModel grpInfoModel = ((KeyValuePair<long, GroupInfoModel>)((MenuItem)sender).DataContext).Value;
                if (grpInfoModel.IsPartial == false)
                {
                    if (grpInfoModel.AccessType == ChatConstants.MEMBER_TYPE_OWNER && grpInfoModel.NumberOfMembers == 1)
                    {
                        LoadGroupInfoFromServer(grpInfoModel);
                    }
                    else
                    {
                        OnLeave(grpInfoModel);
                    }
                }
                else
                {
                    LoadGroupInfoFromServer(grpInfoModel);
                }
            }
        }

        private void LoadGroupInfoFromServer(GroupInfoModel grpInfoModel)
        {
            ChatService.GetGroupInformationWithMembers(grpInfoModel.GroupID, (args) =>
            {
                if (args.Status)
                {
                    int elapsedTime = 0;
                    while (grpInfoModel.IsPartial && elapsedTime < 5000)
                    {
                        Thread.Sleep(200);
                        elapsedTime += 200;
                    }

                    log.Debug(" ================================> " + grpInfoModel.IsPartial);
                    if (grpInfoModel.IsPartial == false)
                    {
                        Thread.Sleep(200);
                        OnLeave(grpInfoModel);
                    }
                    else UIHelperMethods.ShowWarning(" Request failed, Try again !");
                }
                else
                {
                    GroupMemberInfoModel memberModel = grpInfoModel.MemberInfoDictionary.TryGetValue(DefaultSettings.LOGIN_TABLE_ID);
                    if (memberModel != null) UIHelperMethods.ShowWarning("Sorry! Conversation " + (memberModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID ? "leave" : "member remove") + " request failed. Try again.", "Conversation " + (memberModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID ? "leave" : "member remove"));
                    else
                    {
                        UIHelperMethods.ShowWarning(" Request failed, Try again !");
                    }
                }
            });
        }


        private void OnLeave(GroupInfoModel grpInfoModel)
        {
            GroupMemberInfoModel memberModel = grpInfoModel.MemberInfoDictionary.TryGetValue(DefaultSettings.LOGIN_TABLE_ID);
            if (memberModel != null && memberModel.MemberAccessType != ChatConstants.MEMBER_TYPE_NOT_MEMBER)
            {
                if (memberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_OWNER && grpInfoModel.NumberOfMembers == 1)
                {
                    //MessageBoxResult messageBoxResult = CustomMessageBox.ShowWarning(NotificationMessages.GROUP_NON_OWNER_LEAVE_PERMISSION, MessageBoxButton.YesNoCancel, MessageBoxResult.Cancel, new[] { "Mute", "Leave", "Cancel" });
                    //if (messageBoxResult == MessageBoxResult.No)
                    //{
                    //    RemoveGroupMember(grpInfoModel);
                    //}
                    //else if (messageBoxResult == MessageBoxResult.Yes)
                    //{
                    //    if (grpInfoModel.ImNotificationEnabled)
                    //    {
                    //        grpInfoModel.ImNotificationEnabled = !grpInfoModel.ImNotificationEnabled;
                    //        GroupInfoDAO.UpdateIMNotification(grpInfoModel.GroupID, grpInfoModel.ImNotificationEnabled);
                    //    }
                    //}
                    WNConfirmationView cv = new WNConfirmationView("Leave confirmation!", NotificationMessages.GROUP_NON_OWNER_LEAVE_PERMISSION, CustomConfirmationDialogButtonOptions.YesNoCancel, new[] { "Mute", "Leave", "Cancel" });
                    var result = cv.ShowCustomDialog();
                    if (result == ConfirmationDialogResult.No)
                    {
                        RemoveGroupMember(grpInfoModel);
                    }
                    else if (result == ConfirmationDialogResult.Yes)
                    {
                        if (grpInfoModel.ImNotificationEnabled)
                        {
                            grpInfoModel.ImNotificationEnabled = !grpInfoModel.ImNotificationEnabled;
                            GroupInfoDAO.UpdateIMNotification(grpInfoModel.GroupID, grpInfoModel.ImNotificationEnabled);
                        }
                    }
                }
                else
                {
                    string warnigMessage = NotificationMessages.GROUP_NON_OWNER_LEAVE_PERMISSION;
                    if (memberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_OWNER)
                    {
                        warnigMessage = NotificationMessages.GROUP_OWNER_LEAVE_PERMISSION;
                    }
                    //MessageBoxResult messageBoxResult = CustomMessageBox.ShowWarning(warnigMessage, MessageBoxButton.YesNoCancel, MessageBoxResult.Cancel, new[] { "Mute", "Leave", "Cancel" });
                    //if (messageBoxResult == MessageBoxResult.Yes)
                    //{
                    //    if (grpInfoModel.ImNotificationEnabled)
                    //    {
                    //        grpInfoModel.ImNotificationEnabled = !grpInfoModel.ImNotificationEnabled;
                    //        GroupInfoDAO.UpdateIMNotification(grpInfoModel.GroupID, grpInfoModel.ImNotificationEnabled);
                    //    }
                    //}
                    //else if (messageBoxResult == MessageBoxResult.No)
                    //{
                    //    Application.Current.Dispatcher.BeginInvoke(() =>
                    //    {
                    //        if (memberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_OWNER)
                    //        {
                    //            this.ShowGroupOwnerLeaveView(UCGuiRingID.Instance.MotherPanel, grpInfoModel, (status) =>
                    //            {
                    //                return 0;
                    //            });
                    //        }
                    //        else
                    //        {
                    //            RemoveGroupMember(grpInfoModel);
                    //        }
                    //    });
                    //}
                    WNConfirmationView cv = new WNConfirmationView("Leave confirmation!", warnigMessage, CustomConfirmationDialogButtonOptions.YesNoCancel, new[] { "Mute", "Leave", "Cancel" });
                    var result = cv.ShowCustomDialog();
                    if (result == ConfirmationDialogResult.Yes)
                    {
                        if (grpInfoModel.ImNotificationEnabled)
                        {
                            grpInfoModel.ImNotificationEnabled = !grpInfoModel.ImNotificationEnabled;
                            GroupInfoDAO.UpdateIMNotification(grpInfoModel.GroupID, grpInfoModel.ImNotificationEnabled);
                        }
                    }
                    else if (result == ConfirmationDialogResult.No)
                    {
                        Application.Current.Dispatcher.BeginInvoke(() =>
                         {
                             if (memberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_OWNER)
                             {
                                 this.ShowGroupOwnerLeaveView(UCGuiRingID.Instance.MotherPanel, grpInfoModel, (status) =>
                                 {
                                     return 0;
                                 });
                             }
                             else
                             {
                                 RemoveGroupMember(grpInfoModel);
                             }
                         });
                    }
                }
            }
            else UIHelperMethods.ShowWarning("Can't leave from this group!", "Leave group");
        }

        public void ShowGroupOwnerLeaveView(Grid motherGrid, GroupInfoModel groupInfoModel, Func<bool, int> onComplete)
        {
            UCGroupChatCallPanel ucGroupChatCallPanel;
            if (UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(groupInfoModel.GroupID, out ucGroupChatCallPanel))
            {
                ucGroupChatCallPanel.HideOwnerLeaveView();
            }
            UCGroupChatSettings ucGroupChatSettings;
            if (UIDictionaries.Instance.GROUP_CHAT_SETTINGS_DICTIONARY.TryGetValue(groupInfoModel.GroupID, out ucGroupChatSettings))
            {
                ucGroupChatSettings.HideOwnerLeaveView();
            }

            if (motherGrid == null)
            {
                motherGrid = UCGuiRingID.Instance.MotherPanel;
            }
            UCGroupOwnerLeave ucGroupOwnerLeavePanel = new UCGroupOwnerLeave(motherGrid, groupInfoModel);
            ucGroupOwnerLeavePanel.Show();
            //motherGrid.Children.Add(ucGroupOwnerLeavePanel);
            //this.Visibility = Visibility.Visible;
            //userControl.Content = ucGroupOwnerLeavePanel;

            ucGroupOwnerLeavePanel.CompletedHandler += (ownerID) =>
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (ownerID > 0)
                    {
                        if (ucGroupChatSettings != null)
                        {
                            ucGroupChatSettings.CancelGroupImageChange();
                            ucGroupChatSettings.CancelGroupNameChange();
                        }
                        if (onComplete != null)
                        {
                            onComplete(true);
                        }
                    }
                    else
                    {
                        if (onComplete != null)
                        {
                            onComplete(false);
                        }
                    }
                    motherGrid.Children.Remove(ucGroupOwnerLeavePanel);
                }, DispatcherPriority.Send);
            };
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Border bd = (Border)sender;
            double h = bd.ActualHeight;
        }

    }
}

﻿using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.Recent;
using View.ViewModel;

namespace View.UI.Group
{
    /// <summary>
    /// Interaction logic for UCGroupChatScrollViewer.xaml
    /// </summary>
    public partial class UCGroupChatScrollViewer : UserControl, INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCGroupChatScrollViewer).Name);

        public event PropertyChangedEventHandler PropertyChanged;
        private GroupInfoModel _GroupInfoModel;
        private ObservableCollection<RecentModel> _RecentModelList = new ObservableCollection<RecentModel>();
        public ObservableDictionary<string, long> _UnreadChatList = null;
        public long _GroupID;

        private bool _IS_INITIALIZED = false;
        private bool _IS_LOADING = false;
        private bool _IsHistoryLoadingFromServer = false;
        private double? _PrevOffset = null;
        private double? _PrevHeight = null;
        private System.Timers.Timer _ResizeTimer = null;
        private System.Timers.Timer _ScorllTimer = null;
        public StackPanel _StackPanel = null;
        private ScrollBar _ScrollBar = null;
        public bool _IsScrollBarCaptured = false;

        #region Constructor

        public UCGroupChatScrollViewer(GroupInfoModel model)
        {
            InitializeComponent();
            this.GroupInfoModel = model;
            this._GroupID = model.GroupID;
            this.RecentModelList = RingIDViewModel.Instance.GetRecentModelListByID(this._GroupID);
            this._UnreadChatList = ChatViewModel.Instance.GetUnreadChatByID(this._GroupID);
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private void ScrollBar_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this._IsScrollBarCaptured = false;
        }

        private void ScrollBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this._IsScrollBarCaptured = true;
        }

        private void RecentListContainer_SizeChanged(object sender, SizeChangedEventArgs args)
        {
            try
            {
                if (!this._IsScrollBarCaptured)
                {
                    this.ChangeMessageStatusOnResize();

                    if (this._PrevOffset != null && this._PrevHeight != null && this._IS_LOADING)
                    {
                        this.srvRecentList.ScrollToVerticalOffset(this._StackPanel.ActualHeight - (double)this._PrevHeight + (double)this._PrevOffset);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: RecentListContainer_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
                {
                    if (this._IS_LOADING && this._PrevHeight != null && this._PrevOffset != null)
                    {
                        this._PrevOffset += e.VerticalChange;
                    }

                    if (!this._IS_LOADING && e.VerticalChange < 0 && e.VerticalOffset <= ChatHelpers.UP_SCROLL_LIMIT && this.RecentModelList.Count > 1)
                    {
                        // SCROLL TO TOP
                        this._IS_LOADING = true;
                        this.srvRecentList.CanContentScroll = false;
                        List<RecentDTO> rList = RecentChatCallActivityDAO.LoadRecentChatCallActivityListByScroll(this._GroupID, null, this.RecentModelList.ElementAt(1).Time, 25, ChatConstants.HISTORY_UP);

                        if (rList.Count > 0)
                        {
                            this.IsHistoryLoadingFromServer = false;
                            this._PrevOffset = e.VerticalOffset;
                            this._PrevHeight = this._StackPanel.ActualHeight;

                            RecentLoadUtility.LoadRecentData(rList);
                            ChangeLoadingStatus(rList, () =>
                            {
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    this.srvRecentList.CanContentScroll = true;
                                    this._IS_LOADING = false;
                                    this.IsHistoryLoadingFromServer = false;
                                    this._PrevOffset = null;
                                    this._PrevHeight = null;
                                }, DispatcherPriority.ApplicationIdle);
                                return 0;
                            });
                        }
                        else if (this.GroupInfoModel.ChatHistoryNotFound == false)
                        {
                            string prevMinPacketID = this.GroupInfoModel.ChatMinPacketID ?? String.Empty;
                            this.IsHistoryLoadingFromServer = true;
                            this._PrevOffset = e.VerticalOffset;
                            this._PrevHeight = this._StackPanel.ActualHeight;
#if CHAT_LOG
                            log.Info("FETCH GROUP HISTORY(SCROLL) => GroupID = " + this._GroupID + ", PacketID = " + this.GroupInfoModel.ChatMinPacketID + ", LIMIT = " + 5);
#endif
                            ChatService.RequestGroupChatHistory(this._GroupID, this.GroupInfoModel.ChatMinPacketID, ChatConstants.HISTORY_UP, 8, (args) =>
                            {
                                Thread.Sleep(1000);
                                for (int i = 0; prevMinPacketID.Equals(this.GroupInfoModel.ChatMinPacketID ?? String.Empty) && i < 5000; i += 100)
                                {
                                    Thread.Sleep(100);
                                }

                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    this.srvRecentList.CanContentScroll = true;
                                    this._IS_LOADING = false;
                                    this.IsHistoryLoadingFromServer = false;
                                    this._PrevOffset = null;
                                    this._PrevHeight = null;

                                    if (srvRecentList.VerticalOffset < 200 && !prevMinPacketID.Equals(this.GroupInfoModel.ChatMinPacketID ?? String.Empty))
                                    {
                                        this.srvRecentList.ScrollToVerticalOffset(this.srvRecentList.VerticalOffset + 200);
                                    }
                                }, DispatcherPriority.ApplicationIdle);
                            });
                        }
                        else
                        {
                            this._PrevOffset = null;
                            this._PrevHeight = null;
                            this._IS_LOADING = false;
                            this.srvRecentList.CanContentScroll = true;
                        }
                    }

                    this.ChangeMessageStatusOnScroll();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: srvChatContainer_OnScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
                this._PrevOffset = null;
                this._PrevHeight = null;
                this._IS_LOADING = false;
                this.srvRecentList.CanContentScroll = true;
            }
        }

        private void ScrollViewer_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                this.srvRecentList.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                this.srvRecentList.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {
            this._StackPanel = (StackPanel)sender;
            this.srvRecentList.ApplyTemplate();
            this._ScrollBar = this.srvRecentList.Template.FindName("PART_VerticalScrollBar", this.srvRecentList) as ScrollBar;
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeScrollViewer()
        {
            if (!this._IS_INITIALIZED)
            {
                this._IS_INITIALIZED = true;
                this.srvRecentList.ScrollToBottom();
                this.srvRecentList.PreviewKeyDown += this.ScrollViewer_PreviewKeyDown;
                List<RecentDTO> rList = this.ReloadRecentList(null, false);
                this.ChangeLoadingStatus(rList, () =>
                {
                    Thread.Sleep(350);
                    this.MoveToBottom(true);
                    Thread.Sleep(1000);

                    if (this._IS_INITIALIZED)
                    {
                        this._IS_LOADING = false;
                        this.srvRecentList.ScrollChanged += this.ScrollViewer_OnScrollChanged;
                        this.SizeChanged += this.RecentListContainer_SizeChanged;
                        if (this._StackPanel != null) this._StackPanel.SizeChanged += this.RecentListContainer_SizeChanged;
                        if (this._ScrollBar != null) this._ScrollBar.PreviewMouseLeftButtonDown += this.ScrollBar_MouseLeftButtonDown;
                        if (this._ScrollBar != null) this._ScrollBar.PreviewMouseLeftButtonUp += this.ScrollBar_MouseLeftButtonUp;
                        this.ChangeMessageStatusOnResize();
                    }
                    return 0;
                });
            }
            else
            {
                this.ReloadRecentList(null);
            }
        }

        public void ReleaseScrollViewer()
        {
            this._IS_INITIALIZED = false;
            this._IS_LOADING = false;
            this.IsHistoryLoadingFromServer = false;
            this.srvRecentList.ScrollChanged -= this.ScrollViewer_OnScrollChanged;
            this.srvRecentList.PreviewKeyDown -= this.ScrollViewer_PreviewKeyDown;
            this.SizeChanged -= this.RecentListContainer_SizeChanged;
            if (this._StackPanel != null) this._StackPanel.SizeChanged -= this.RecentListContainer_SizeChanged;
            if (this._ScrollBar != null) this._ScrollBar.PreviewMouseLeftButtonDown -= this.ScrollBar_MouseLeftButtonDown;
            if (this._ScrollBar != null) this._ScrollBar.PreviewMouseLeftButtonUp -= this.ScrollBar_MouseLeftButtonUp;
            if (this._ResizeTimer != null) this._ResizeTimer.Stop();
            if (this._ScorllTimer != null) this._ScorllTimer.Stop();
            Task.Factory.StartNew(() =>
            {
                this.RecentModelList.Where(P => P.IsRecentOpened).ToList().ForEach(P => P.IsRecentOpened = false);
            });
        }

        public List<RecentDTO> ReloadRecentList(int? days, bool isReload = true)
        {
            List<RecentDTO> rList = new List<RecentDTO>();
            try
            {
                if (this._IS_LOADING == false)
                {
                    this._IS_LOADING = true;

                    int limit = (int)(SystemParameters.PrimaryScreenHeight / ChatHelpers.UI_UNIT_SIZE);
                    List<string> uniqueKeys = this.RecentModelList.Where(P => P.Type != ChatConstants.SUBTYPE_DATE_TITLE).Select(P => P.UniqueKey).ToList();

                    if (days != null)
                    {
                        long time = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - ((int)days * SettingsConstants.MILISECONDS_IN_DAY);
                        rList = RecentChatCallActivityDAO.LoadRecentChatCallActivityListByDays(this._GroupID, null, time, null, uniqueKeys, true);

                        if (rList.Count > 0)
                        {
                            this.srvRecentList.ScrollToTop();
                        }
                        else
                        {
                            ContentPresenter minItem = null;
                            int max = this.tvRecentList.Items.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                ContentPresenter item = (ContentPresenter)this.tvRecentList.ItemContainerGenerator.ContainerFromIndex(pivot);
                                long pivotTime = item != null ? ((RecentModel)item.Content).Time : 0;
                                if (time > pivotTime)
                                {
                                    min = pivot + 1;
                                    minItem = item;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (minItem != null)
                            {
                                GeneralTransform itemTransform = minItem.TransformToAncestor(this.srvRecentList);
                                Rect rectangle = itemTransform.TransformBounds(new Rect(new System.Windows.Point(0, 0), minItem.RenderSize));
                                this.srvRecentList.ScrollToVerticalOffset(rectangle.Top + this.srvRecentList.VerticalOffset);
                            }
                            else if (min == 0)
                            {
                                this.srvRecentList.ScrollToTop();
                            }
                            else
                            {
                                this.srvRecentList.ScrollToBottom();
                            }
                        }
                    }
                    else if (uniqueKeys.Count <= 0)
                    {
                        rList = RecentChatCallActivityDAO.LoadRecentChatCallActivityListByDays(this._GroupID, null, ChatConstants.DAY_365_DAYS, 50);
                        //foreach (RecentDTO dto in rList)
                        //{
                        //    if (dto.Message != null)
                        //    {
                        //        Debug.WriteLine(dto.Message.PacketID + " => " + PacketIDGenerator.GetTime(dto.Message.PacketID) + " => " + PacketIDGenerator.GetUnixTimestamp(dto.Message.PacketID));
                        //    }
                        //}
                        if (!this.GroupInfoModel.ChatHistoryNotFound && rList.Count < limit)
                        {
#if CHAT_LOG
                            log.Info("FETCH GROUP HISTORY(EMPTY) => GroupID = " + this._GroupID + ", PacketID = " + this.GroupInfoModel.ChatMinPacketID + ", LIMIT = " + (limit - rList.Count));
#endif
                            ChatService.RequestGroupChatHistory(this._GroupID, this.GroupInfoModel.ChatMinPacketID, ChatConstants.HISTORY_UP, (limit - rList.Count), null);
                        }
                    }
                    else if (uniqueKeys.Count < limit)
                    {
                        rList = RecentChatCallActivityDAO.LoadRecentChatCallActivityListByDays(this._GroupID, null, this.RecentModelList.ElementAt(1).Time, null, uniqueKeys);
                        if (uniqueKeys.Count + rList.Count < limit)
                        {
                            rList.AddRange(RecentChatCallActivityDAO.LoadRecentChatCallActivityListByScroll(this._GroupID, null, this.RecentModelList.ElementAt(1).Time, limit - (uniqueKeys.Count + rList.Count), ChatConstants.HISTORY_UP));
                            if (!this.GroupInfoModel.ChatHistoryNotFound && uniqueKeys.Count + rList.Count < limit)
                            {
#if CHAT_LOG
                                log.Info("FETCH GROUP HISTORY(FILL) => GroupID = " + this._GroupID + ", PacketID = " + this.GroupInfoModel.ChatMinPacketID + ", LIMIT = " + (limit - (uniqueKeys.Count + rList.Count)));
#endif
                                ChatService.RequestGroupChatHistory(this._GroupID, this.GroupInfoModel.ChatMinPacketID, ChatConstants.HISTORY_UP, (limit - (uniqueKeys.Count + rList.Count)), null);
                            }
                        }
                    }
                    else
                    {
                        rList = RecentChatCallActivityDAO.LoadRecentChatCallActivityListByDays(this._GroupID, null, this.RecentModelList.ElementAt(1).Time, null, uniqueKeys);
                    }

                    RecentLoadUtility.LoadRecentData(rList);

                    if (days != null)
                    {
                        this.ChangeLoadingStatus(rList.Take(15).ToList(), () =>
                        {
                            this.ChangeMessageStatus();
                            this._IS_LOADING = false;
                            return 0;
                        });
                    }
                    else if (isReload)
                    {
                        this.ChangeMessageStatusOnResize();
                        this._IS_LOADING = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ReloadRecentList() ==> " + ex.Message + "\n" + ex.StackTrace);
                this._IS_LOADING = false;
            }
            return rList;
        }

        private void ChangeLoadingStatus(List<RecentDTO> list, Func<int> onComplete)
        {
            Thread t = new Thread(new ThreadStart(() =>
            {
                try
                {
                    Thread.Sleep(10);
                    while (!RecentLoadUtility.IsProcessed(list))
                    {
                        Thread.Sleep(1);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: ChangeLoadingStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
                }
                finally
                {
                    onComplete();
                }
            }));
            t.Start();
        }

        public void ChangeMessageStatusOnResize()
        {
            try
            {
                if (this._ResizeTimer == null)
                {
                    this._ResizeTimer = new System.Timers.Timer { AutoReset = false };
                    this._ResizeTimer.Interval = 350;
                    this._ResizeTimer.Elapsed += (o, e) =>
                    {
                        this.ChangeMessageStatus();
                        this._ResizeTimer.Stop();
                    };
                }

                if (this._ScorllTimer != null && this._ScorllTimer.Enabled)
                {
                    this._ScorllTimer.Stop();
                }
                this._ResizeTimer.Stop();
                this._ResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeMessageStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ChangeMessageStatusOnScroll()
        {
            try
            {
                if (this._ScorllTimer == null)
                {
                    this._ScorllTimer = new System.Timers.Timer { AutoReset = false };
                    this._ScorllTimer.Elapsed += (o, e) =>
                    {
                        this.ChangeMessageStatus();
                        this._ScorllTimer.Stop();
                    };
                }
                this._ScorllTimer.Stop();
                this._ScorllTimer.Interval = this._IsScrollBarCaptured ? 10 : 30;
                if (this._ResizeTimer == null || !this._ResizeTimer.Enabled)
                {
                    this._ScorllTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeMessageStatusOnScroll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ChangeMessageStatus()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (UIHelperMethods.IsViewActivateAndVisible(HelperMethods.FindVisualParent<UserControl>(this)))
                {
                    ChatHelpers.ChangeChatViewPortOpenedProperty(this.srvRecentList, this.tvRecentList, this._StackPanel, this._IsScrollBarCaptured, this._UnreadChatList, null);
                }
            }, DispatcherPriority.Send);
        }

        public void MoveToBottom(bool triggerMessageOpened = false)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    List<RecentModel> list = this.RecentModelList.ToList();
                    for (int idx = list.Count - 1, height = (int)this.srvRecentList.ViewportHeight; idx >= 0 && height > 0; idx--)
                    {
                        RecentModel model = list.ElementAt(idx);
                        model.IsRecentOpened = true;
                        if (triggerMessageOpened && model.Message != null && !model.Message.IsUnread)
                        {
                            model.Message.IsMessageOpened = true;
                        }
                        height -= model.PrevViewHeight;
                    }
                    this.srvRecentList.ScrollToBottom();
                }, DispatcherPriority.ApplicationIdle);
            }
            catch (Exception ex)
            {
                log.Error("Error: MoveToBottom() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public GroupInfoModel GroupInfoModel
        {
            get { return _GroupInfoModel; }
            set { _GroupInfoModel = value; }
        }

        public ObservableCollection<RecentModel> RecentModelList
        {
            get { return _RecentModelList; }
            set
            {
                _RecentModelList = value;
                this.OnPropertyChanged("RecentModelList");
            }
        }

        public bool IsHistoryLoadingFromServer
        {
            get { return _IsHistoryLoadingFromServer; }
            set
            {
                if (_IsHistoryLoadingFromServer == value)
                    return;

                _IsHistoryLoadingFromServer = value;
                this.OnPropertyChanged("IsHistoryLoadingFromServer");
            }
        }

        #endregion Property
    }
}

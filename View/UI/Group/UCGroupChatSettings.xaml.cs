﻿using Auth.Service.Chat;
using Auth.Service.Images;
using Auth.utility;
using imsdkwrapper;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.Dictonary;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.GIF;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Images;

namespace View.UI.Group
{
    /// <summary>
    /// Interaction logic for UCGroupProfile.xaml
    /// </summary>
    public partial class UCGroupChatSettings : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCGroupChatSettings).Name);

        private const int STATE_INITIALIZED = 0;
        private const int STATE_SELECTED = 1;
        private const int STATE_UPLOADED = 2;

        private UCGroupOwnerLeave _GroupOwnerLeavePanel = null;
        private ICommand _AddMemberCommand;
        private ICommand _LeaveGroupCommand;
        private ICommand _ShowProfileCommand;
        private ICommand _OnGroupNameChangeCommand;
        private ICommand _OnGroupImageChangeCommand;
        private ICommand _OnBackCommand;
        private ICommand _ChatSettingsCommand;

        private bool _IsGroupNameEditVisible = false;
        private bool _IsOwnerLeaveMode = false;
        private int _GroupImageUploadState = STATE_INITIALIZED;

        private System.Windows.Point _MouseCapturePosition = new System.Windows.Point(-1, -1);
        private BitmapImage _BitmapImage;
        private double _ImageWidth = 0;
        private double _ImageHeight = 0;
        private double _CropX = 0;
        private double _CropY = 0;
        private BackgroundWorker _ImageUploadWorker = null;
        private BackgroundWorker _MemberListLoader = null;

        private DispatcherTimer _MemberListResizeTimer = null;
        private bool _IsValidGroupMember = false;
        private BitmapImage _LoadingIcon = null;
        private ListBoxItem prevItem = null;
        //private BackgroundWorker addFriendButtonClickedWorker, removeContactWorker, acceptFriendWorker;

        public event PropertyChangedEventHandler PropertyChanged;
        private GroupInfoModel _GroupInfoModel;
        public long _GroupID;

        private ObservableCollection<ChatBgHistoryModel> _ChatBgHistoryModelList = new ObservableCollection<ChatBgHistoryModel>();
        private ICommand _OnChatBackgroundChangeCommand;
        private BackgroundWorker _ChatBgHistoryLoader = null;
        public bool _IsChatBgLoaded = false;

        #region Constructor

        public UCGroupChatSettings(GroupInfoModel model)
        {
            InitializeComponent();
            this.GroupInfoModel = model;
            this._GroupID = model.GroupID;
            this._ImageUploadWorker = new BackgroundWorker();
            this._ImageUploadWorker.DoWork += ImageUploadWorker_DoWork;
            this._ImageUploadWorker.RunWorkerCompleted += ImageUploadWorker_RunWorkerCompleted;
            this._MemberListLoader = new BackgroundWorker();
            this._MemberListLoader.DoWork += MemberListLoader_DowWork;
            this._MemberListLoader.RunWorkerCompleted += MemberListLoader_RunWorkerCompleted;
            this._ChatBgHistoryLoader = new BackgroundWorker();
            this._ChatBgHistoryLoader.DoWork += ChatBgHistory_DowWork;
            this._ChatBgHistoryLoader.RunWorkerCompleted += ChatBgHistory_RunWorkerCompleted;
            this.Scroll.ScrollChanged += scvMemberContainer_ScrollChanged;
            this.TabList.SelectionChanged += TabControl_SelectionChanged;
            this.lbAllMembers.PreviewMouseWheel += list_PreviewMouseWheel;
            this.lbAllMembers.MouseLeftButtonUp += list_MouseLeftButtonUp;
            this.lbAllMembers.SizeChanged += grdMemberContainer_SizeChanged;
            this.lbAdmins.PreviewMouseWheel += list_PreviewMouseWheel;
            this.lbAdmins.MouseLeftButtonUp += list_MouseLeftButtonUp;
            this.lbAdmins.SizeChanged += grdMemberContainer_SizeChanged;
            this.pnlAllMembers.DragEnter += grdMemberContainer_DragOver;
            this.pnlAllMembers.DragOver += grdMemberContainer_DragEnter;
            this.pnlAllMembers.Drop += grdMemberContainer_Drop;
            this.pnlAdmins.DragEnter += grdMemberContainer_DragOver;
            this.pnlAdmins.DragOver += grdMemberContainer_DragEnter;
            this.pnlAdmins.Drop += grdMemberContainer_Drop;
            this.ChatBgHistoryModelList.Add(new ChatBgHistoryModel { ChatBgUrl = "", IsAction = true });
            this.DataContext = this;
        }

        #endregion Constructor

        #region Event Handler

        private void scvMemberContainer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                if (Math.Abs(e.VerticalChange) > 0)
                {
                    ChangeMemberOpenStatus();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: scvMemberList_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void grdMemberContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                ChangeMemberOpenStatus();
            }
            catch (Exception ex)
            {
                log.Error("Error: GroupMemberListPanel_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void grdMemberContainer_DragEnter(object sender, DragEventArgs e)
        {
            _IsValidGroupMember = CanDropGroupMember(e);
        }

        private void grdMemberContainer_DragOver(object sender, DragEventArgs e)
        {
            if (_IsValidGroupMember == false)
            {
                e.Effects = DragDropEffects.None;
                e.Handled = true;
            }
            else
            {
                //e.Effects = DragDropEffects.All;
            }
        }

        private void grdMemberContainer_Drop(object sender, DragEventArgs e)
        {
            OnDropGroupMember(e);
        }

        private void RemoveFromGroup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = ((Button)sender);
                long memberIdentity = (long)button.Tag;

                if (memberIdentity == DefaultSettings.LOGIN_TABLE_ID)
                {
                    if (GroupInfoModel.IsPartial == false)
                    {
                        if (GroupInfoModel.AccessType == ChatConstants.MEMBER_TYPE_OWNER && GroupInfoModel.NumberOfMembers == 1)
                        {
                            LoadGroupInfoFromServer(memberIdentity);
                        }
                        else
                        {
                            OnLeaveGroup(null);
                        }
                    }
                    else
                    {
                        LoadGroupInfoFromServer(memberIdentity);
                    }
                }
                else
                {
                    RemoveGroupMember(memberIdentity);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: RemoveFromGroup_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void MakeAsAdmin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = ((Button)sender);
                long memberIdentity = (long)button.Tag;

                List<GroupMemberInfoDTO> memberDTOs = new List<GroupMemberInfoDTO>();
                GroupMemberInfoDTO memeberDTO = new GroupMemberInfoDTO();
                memeberDTO.UserTableID = memberIdentity;
                memeberDTO.MemberAccessType = ChatConstants.MEMBER_TYPE_ADMIN;
                memberDTOs.Add(memeberDTO);

                ChangeGroupMemberAccessType(memberDTOs);
            }
            catch (Exception ex)
            {
                log.Error("Error: MakeAsAdmin_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void MakeAsMember_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = ((Button)sender);
                long memberIdentity = (long)button.Tag;

                List<GroupMemberInfoDTO> memberDTOs = new List<GroupMemberInfoDTO>();
                GroupMemberInfoDTO memeberDTO = new GroupMemberInfoDTO();
                memeberDTO.UserTableID = memberIdentity;
                memeberDTO.MemberAccessType = ChatConstants.MEMBER_TYPE_MEMBER;
                memberDTOs.Add(memeberDTO);

                ChangeGroupMemberAccessType(memberDTOs);
            }
            catch (Exception ex)
            {
                log.Error("Error: MakeAsMember_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void MemberListLoader_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                List<GroupMemberInfoModel> list = GroupInfoModel.MemberInfoDictionary.Values.Where(P => P.MemberAccessType != ChatConstants.MEMBER_TYPE_NOT_MEMBER).ToList();
                foreach (GroupMemberInfoModel memberModel in list)
                {
                    Thread.Sleep(5);

                    bool state = GroupInfoModel.AdminMemberList.Where(P => P.UserTableID == memberModel.UserTableID).FirstOrDefault() != null;
                    if (memberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_ADMIN)
                    {
                        if (!state)
                        {
                            GroupInfoModel.AdminMemberList.InvokeAdd(memberModel);
                        }
                    }
                    else
                    {
                        if (state)
                        {
                            GroupInfoModel.AdminMemberList.InvokeRemove(memberModel);
                        }
                    }

                    if (GroupInfoModel.AllMemberList.Where(P => P.UserTableID == memberModel.UserTableID).FirstOrDefault() == null)
                    {
                        GroupInfoModel.AllMemberList.InvokeAdd(memberModel);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: MemberListLoader_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            e.Result = true;
        }

        private void MemberListLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            GroupInfoModel.IsProfilePanelOpened = true;
            ChangeMemberOpenStatus();
        }

        private void ImageUploadWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            dynamic data = (ExpandoObject)e.Argument;
            data.Response = ProfileCoverImageUpload.Instance.UploadToImageServerWebRequest(data.ByteArray, SettingsConstants.TYPE_GROUP_IMAGE);
            e.Result = data;
        }

        private void ImageUploadWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                dynamic data = (ExpandoObject)e.Result;
                Bitmap btmap = data.Bitmap;
                string imageUrl = data.Response;

                _LoadingIcon = null;
                imgControl.Source = null;
                ImageBehavior.SetAnimatedSource(imgControl, null);

                if (!string.IsNullOrEmpty(imageUrl))
                {
                    string previous_image_url = GroupInfoModel.GroupProfileImage;
                    string previous_image = ImageUrlHelper.GetImageNameFromUrl(previous_image_url);
                    string new_image = ImageUrlHelper.GetImageNameFromUrl(imageUrl);

                    btmap.Save(RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + new_image, ImageFormat.Jpeg);
                    ImageUtility.DeleteImageFromLocalDirectory(RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + previous_image);

                    GroupImageUploadState = STATE_INITIALIZED;

                    GroupInfoModel.GroupProfileImage = imageUrl;
                    //TRIGGER TO GROUP IMAGE IN GROUP CHAT & LOG PANEL
                    GroupInfoModel.OnPropertyChanged("CurrentInstance");

                    ChatHelpers.SendGroupImage(GroupInfoModel, previous_image_url, (status) =>
                    {
                        if (status == false) UIHelperMethods.ShowWarning("Sorry! Conversation image change request failed. Try again.", "Image change");
                        return 0;
                    });

                    ////START OF CONDITION
                    //if (!ChatHelpers.StartGroupChat(_GroupID, GroupInfoModel, (groupID, success) =>
                    //{
                    //    ChatHelpers.SendGroupImage(GroupInfoModel, success, previous_image_url, (status) =>
                    //    {
                    //        if (status == false)
                    //        {
                    //            CustomMessageBox.ShowError("Sorry! Conversation image change request failed. Try again.");
                    //        }
                    //        return 0;
                    //    });
                    //    return 0;
                    //}))
                    ////END OF CONDITION
                    //{
                    //    ChatHelpers.SendGroupImage(GroupInfoModel, true, previous_image_url, (status) =>
                    //    {
                    //        if (status == false)
                    //        {
                    //            CustomMessageBox.ShowError("Sorry! Conversation image change request failed. Try again.");
                    //        }
                    //        return 0;
                    //    });
                    //}

                    ResetGroupImageChange();
                }
                else
                {
                    GroupImageUploadState = STATE_INITIALIZED;
                    ResetGroupImageChange();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: bgworkerUpload_RunWorkerCompleted() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void imgCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (GroupImageUploadState == STATE_SELECTED && _MouseCapturePosition.X != -1 && _MouseCapturePosition.Y != -1 && imgCanvus.IsMouseCaptured)
                {
                    System.Windows.Point position = e.GetPosition(imgCanvus);
                    Vector offset = position - _MouseCapturePosition;
                    _MouseCapturePosition = position;
                    double MinX = 0;
                    double MinY = 0;
                    double MaxX = (-_ImageWidth + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH);
                    double MaxY = (-_ImageHeight + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);

                    if ((Canvas.GetLeft(imgControl) + offset.X) <= MinX
                        && (Canvas.GetLeft(imgControl) + offset.X) >= MaxX
                        && (Canvas.GetTop(imgControl) + offset.Y) <= MinY
                        && (Canvas.GetTop(imgControl) + offset.Y) >= MaxY)
                    {
                        _CropX = Canvas.GetLeft(imgControl) + offset.X;
                        _CropY = Canvas.GetTop(imgControl) + offset.Y;
                        Canvas.SetLeft(imgControl, _CropX);
                        Canvas.SetTop(imgControl, _CropY);
                        imgCanvus.UpdateLayout();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: imgCanvas_MouseMove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void imgCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (GroupImageUploadState == STATE_SELECTED)
                {
                    imgCanvus.ReleaseMouseCapture();
                    _MouseCapturePosition = new System.Windows.Point(-1, -1); ;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: imgCanvas_MouseLeftButtonUp() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void imgCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (GroupImageUploadState == STATE_SELECTED && imgCanvus.CaptureMouse())
                {
                    _MouseCapturePosition = e.GetPosition(imgCanvus);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: imgCanvas_MouseLeftButtonDown() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChatBgHistory_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_IsChatBgLoaded == false)
                {
                    Thread.Sleep(200);
                }

                List<ChatBgHistoryModel> bgModels = RingIDViewModel.Instance.ChatBackgroundList.Values.ToList();
                bgModels = bgModels.Where(P => !ChatBgHistoryModelList.Any(Q => Q.ChatBgUrl.Equals(P.ChatBgUrl))).ToList();

                foreach (ChatBgHistoryModel model in bgModels)
                {
                    Thread.Sleep(5);
                    ChatBgHistoryModelList.InvokeInsert(ChatBgHistoryModelList.Count - 1, model);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChatBgHistory_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            e.Result = true;
        }

        private void ChatBgHistory_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _IsChatBgLoaded = true;
            if (GroupInfoModel.IsProfilePanelOpened == false && _MemberListLoader.IsBusy == false)
            {
                _MemberListLoader.RunWorkerAsync();
            }
        }

        private void mnuMoreBackground_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool bgImageFound = false;
                MessegeBoxWithLoader msgBox = UIHelperMethods.ShowMessageWithLoader("Getting Background Images");
                LoadChatBackground loader = new LoadChatBackground(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER);
                loader.OnFetchSingleBgImage += (imageDTO) =>
                {
                    ChatBgHistoryModel model = RingIDViewModel.Instance.ChatBackgroundList.TryGetValue(imageDTO.name);
                    if (model == null)
                    {
                        model = new ChatBgHistoryModel(imageDTO);
                        RingIDViewModel.Instance.ChatBackgroundList[imageDTO.name] = model;
                        ChatBgHistoryDAO.SaveChatBgHistory(imageDTO);
                    }

                    if (!ChatBgHistoryModelList.Contains(model))
                    {
                        bgImageFound = true;
                        ChatBgHistoryModelList.InvokeInsert(ChatBgHistoryModelList.Count - 1, model);
                    }
                };
                loader.OnComplete += () =>
                {
                    if (_ChatBgHistoryLoader.IsBusy == false)
                    {
                        _ChatBgHistoryLoader.RunWorkerAsync();
                    }
                    if (bgImageFound == false)
                    {
                        msgBox.ShowCompleMessage("No More Background");
                    }
                    Thread.Sleep(1000);
                    msgBox.CloseMessage();
                };
                loader.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuMoreBackground_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void mnuChooseExistingPhoto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Title = "Select picture";
                dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
                dialog.Multiselect = false;
                if (dialog.ShowDialog() == true)
                {
                    string fileName = dialog.FileName;
                    Bitmap originalBitmap = ImageUtility.GetBitmapOfDynamicResource(fileName);
                    System.Drawing.Rectangle resolution = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                    if (originalBitmap.Width < DefaultSettings.MIN_CHAT_BG_WIDTH || originalBitmap.Height < DefaultSettings.MIN_CHAT_BG_HEIGHT)
                    {
                        UIHelperMethods.ShowWarning("Chat background photo's minimum resulotion: " + DefaultSettings.MIN_CHAT_BG_WIDTH + " x " + DefaultSettings.MIN_CHAT_BG_HEIGHT);
                        //    CustomMessageBox.ShowError("Chat background photo's minimum resulotion: " + DefaultSettings.MIN_CHAT_BG_WIDTH + " x " + DefaultSettings.MIN_CHAT_BG_HEIGHT);
                        return;
                    }

                    string savedFileName = ChatHelpers.SaveChatBackgroundFromDirectory(originalBitmap);
                    if (!String.IsNullOrWhiteSpace(savedFileName))
                    {
                        ChatBgImageDTO imageDTO = new ChatBgImageDTO();
                        imageDTO.name = savedFileName;
                        imageDTO.UpdateTime = ChatService.GetServerTime();

                        ChatBgHistoryModel model = new ChatBgHistoryModel(imageDTO);
                        RingIDViewModel.Instance.ChatBackgroundList[imageDTO.name] = model;
                        ChatBgHistoryModelList.InvokeInsert(ChatBgHistoryModelList.Count - 1, model);

                        ChatBgHistoryDAO.SaveChatBgHistoryFromThread(imageDTO);
                        OnChatBackgroundChange(model);
                    }
                    else
                        UIHelperMethods.ShowWarning("Chat background photo's minimum resulotion: " + DefaultSettings.MIN_CHAT_BG_WIDTH + " x " + DefaultSettings.MIN_CHAT_BG_HEIGHT, "Chat background");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuChooseExistingPhoto_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        private void LoadGroupInfoFromServer(long memberIdentity)
        {
            ChatService.GetGroupInformationWithMembers(GroupInfoModel.GroupID, (args) =>
            {
                if (args.Status)
                {
                    int elapsedTime = 0;
                    while (GroupInfoModel.IsPartial && elapsedTime < 5000)
                    {
                        Thread.Sleep(200);
                        elapsedTime += 200;
                    }

                    log.Debug(" ================================> " + GroupInfoModel.IsPartial);
                    if (GroupInfoModel.IsPartial == false)
                    {
                        Thread.Sleep(200);
                        OnLeaveGroup(null);
                    }
                    else UIHelperMethods.ShowFailed(" Request failed, Try again !");
                }
                else
                {
                    GroupMemberInfoModel memberModel = GroupInfoModel.MemberInfoDictionary.TryGetValue(DefaultSettings.LOGIN_TABLE_ID);
                    if (memberModel != null)
                    {
                        UIHelperMethods.ShowFailed("Sorry! Conversation " + (memberModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID ? "leave" : "member remove") + " request failed. Try again.");
                    }
                    else UIHelperMethods.ShowFailed(" Request failed, Try again !");
                }
            });
        }

        public void LoadInitInformation()
        {
            if (_IsChatBgLoaded == false && _ChatBgHistoryLoader.IsBusy == false)
            {
                _ChatBgHistoryLoader.RunWorkerAsync();
            }
        }

        private void ChangeMemberOpenStatus()
        {
            try
            {
                if (_MemberListResizeTimer == null)
                {
                    _MemberListResizeTimer = new DispatcherTimer();
                    _MemberListResizeTimer.Interval = TimeSpan.FromSeconds(1);
                    _MemberListResizeTimer.Tick += (o, e) =>
                    {
                        if (pnlAllMembers.IsVisible)
                        {
                            HelperMethods.ChangeViewPortOpenedProperty(Scroll, lbAllMembers);
                        }
                        if (lbAdmins.IsVisible)
                        {
                            HelperMethods.ChangeViewPortOpenedProperty(Scroll, lbAdmins);
                        }

                        _MemberListResizeTimer.Stop();
                        _MemberListResizeTimer.Interval = TimeSpan.FromSeconds(0.2);
                    };
                }

                _MemberListResizeTimer.Stop();
                _MemberListResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnGroupImageChange(object param)
        {
            try
            {
                int type = int.Parse(param.ToString());

                if (type == STATE_SELECTED)
                {
                    //Directory Image
                    OpenFileDialog dialog = new OpenFileDialog();
                    dialog.Title = "Select a picture";
                    dialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
                    if ((bool)dialog.ShowDialog())
                    {
                        Bitmap bmp = ImageUtility.GetBitmapOfDynamicResource(dialog.FileName);
                        if (bmp.Width < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH || bmp.Height < RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT)
                        {
                            UIHelperMethods.ShowFailed("Group Profile photo's minimum resulotion: " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH + " x " + RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                        }
                        else
                        {
                            GroupImageUploadState = STATE_SELECTED;
                            _BitmapImage = ImageUtility.GetResizedImageToFit(bmp, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                            imgControl.Source = _BitmapImage;
                            ResetGroupImageChange(_BitmapImage.Width, _BitmapImage.Height);
                        }
                    }
                }
                else if (type == STATE_UPLOADED)
                {
                    //Save Change
                    UploadAndSaveGroupImage();
                }
                else if (type == STATE_INITIALIZED)
                {
                    //Cancel Change
                    CancelGroupImageChange();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnGroupImageChange() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UploadAndSaveGroupImage()
        {
            try
            {
                Bitmap btmap = ImageUtility.CropBitmap(
                    ImageUtility.ConvertBitmapImageToBitmap(_BitmapImage),
                    (int)_CropX < 0 ? -(int)_CropX : (int)_CropX,
                    (int)_CropY < 0 ? -(int)_CropY : (int)_CropY,
                    RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH,
                    RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT);
                byte[] byteArray = ImageUtility.ConvertBitmapToByteArray(btmap);

                GroupImageUploadState = STATE_UPLOADED;
                _LoadingIcon = null;
                _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                imgControl.Source = _LoadingIcon;
                Canvas.SetLeft(imgControl, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_WIDTH / 2 - _LoadingIcon.Width / 2);
                Canvas.SetTop(imgControl, RingIDSettings.COVER_PIC_UPLOAD_SCREEN_HEIGHT / 2 - _LoadingIcon.Height / 2);
                ImageBehavior.SetAnimatedSource(imgControl, _LoadingIcon);

                dynamic data = new ExpandoObject();
                data.Bitmap = btmap;
                data.ByteArray = byteArray;
                if (_ImageUploadWorker.IsBusy != true)
                {
                    _ImageUploadWorker.RunWorkerAsync(data);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UploadAndSaveGroupImage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ResetGroupImageChange(double width = 0.0, double height = 0.0)
        {
            try
            {
                if (width == 0.0 || height == 0.0)
                {
                    _BitmapImage = null;
                    imgControl.Source = null;

                    Binding sourceBinding = new Binding
                    {
                        Path = new PropertyPath("CurrentInstance"),
                        Source = GroupInfoModel,
                        Converter = new PathToCoverImageConverter(),
                        Mode = BindingMode.OneWay,
                    };
                    imgControl.SetBinding(System.Windows.Controls.Image.SourceProperty, sourceBinding);
                }
                _ImageWidth = width;
                _ImageHeight = height;
                _CropX = 0;
                _CropY = 0;
                Canvas.SetLeft(imgControl, 0);
                Canvas.SetTop(imgControl, 0);
                _MouseCapturePosition = new System.Windows.Point(-1, -1);
            }
            catch (Exception ex)
            {
                log.Error("Error: ResetGroupImageChange() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void CancelGroupImageChange()
        {
            if (GroupImageUploadState == STATE_SELECTED)
            {
                GroupImageUploadState = STATE_INITIALIZED;
                ResetGroupImageChange();
            }
            else
            {
                GroupImageUploadState = STATE_INITIALIZED;
            }
        }

        private void OnGroupNameChange(object param)
        {
            try
            {
                int type = int.Parse(param.ToString());

                if (type == 1)
                {
                    //EDIT
                    IsGroupNameEditVisible = true;
                    GroupInfoModel.OnPropertyChanged("GroupName");
                    txtGroupName.CaretIndex = 0;
                    txtGroupName.Focusable = true;
                    txtGroupName.Focus();
                    //ChatService.StartGroupChat(_GroupID, GroupInfoModel);
                }
                else if (type == 2)
                {
                    //Save Changes
                    string prevText = GroupInfoModel.GroupName.Trim();
                    string currText = txtGroupName.Text.Trim();

                    if (currText.Length > 0 && !prevText.Equals(currText))
                    {
                        IsGroupNameEditVisible = false;
                        GroupInfoModel.GroupName = currText;

                        ChatHelpers.SendGroupName(GroupInfoModel, prevText, (status) =>
                        {
                            if (status == false) UIHelperMethods.ShowFailed("Sorry! Conversation name change request failed. Try again.");
                            return 0;
                        });
                    }
                }
                else if (type == 3)
                {
                    //Cancel Changes
                    CancelGroupNameChange();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AddGroupMember() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool CanGroupNameChange(object param)
        {
            return GroupInfoModel.AccessType == ChatConstants.MEMBER_TYPE_ADMIN || GroupInfoModel.AccessType == ChatConstants.MEMBER_TYPE_OWNER;
        }

        public void CancelGroupNameChange()
        {
            if (IsGroupNameEditVisible)
            {
                IsGroupNameEditVisible = false;
                GroupInfoModel.OnPropertyChanged("GroupName");
            }
        }

        private void OnAddMemberClick(object param)
        {
            MainSwitcher.PopupController.Instance.Show(btnAddMember, GroupInfoModel.MemberInfoDictionary.Values.ToList(), (e) =>
            {
                AddGroupMember(e);
                return 0;
            });
        }

        private void OnLeaveGroup(object param)
        {
            try
            {
                GroupMemberInfoModel memberModel = GroupInfoModel.MemberInfoDictionary.TryGetValue(DefaultSettings.LOGIN_TABLE_ID);
                if (memberModel != null)
                {
                    if (memberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_OWNER && GroupInfoModel.NumberOfMembers == 1)
                    {
                        //MessageBoxResult messageBoxResult = CustomMessageBox.ShowWarning(NotificationMessages.GROUP_NON_OWNER_LEAVE_PERMISSION, MessageBoxButton.YesNoCancel, MessageBoxResult.Cancel, new[] { "Mute", "Leave", "Cancel" });
                        //if (messageBoxResult == MessageBoxResult.No)
                        //{
                        //    RemoveGroupMember(DefaultSettings.LOGIN_TABLE_ID);
                        //}
                        //else if (messageBoxResult == MessageBoxResult.Yes)
                        //{
                        //    if (GroupInfoModel.ImNotificationEnabled)
                        //    {
                        //        OnChatSettingsClicked(0);
                        //    }
                        //}
                        WNConfirmationView cv = new WNConfirmationView("Leave confirmation!", NotificationMessages.GROUP_NON_OWNER_LEAVE_PERMISSION, CustomConfirmationDialogButtonOptions.YesNoCancel, new[] { "Mute", "Leave", "Cancel" });
                        var result = cv.ShowCustomDialog();
                        if (result == ConfirmationDialogResult.No)
                        {
                            RemoveGroupMember(DefaultSettings.LOGIN_TABLE_ID);
                        }
                        else if (result == ConfirmationDialogResult.Yes)
                        {
                            if (GroupInfoModel.ImNotificationEnabled)
                            {
                                OnChatSettingsClicked(0);
                            }
                        }
                    }
                    else
                    {
                        string warnigMessage = NotificationMessages.GROUP_NON_OWNER_LEAVE_PERMISSION;
                        if (memberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_OWNER)
                        {
                            warnigMessage = NotificationMessages.GROUP_OWNER_LEAVE_PERMISSION;
                        }

                        //MessageBoxResult messageBoxResult = CustomMessageBox.ShowWarning(warnigMessage, MessageBoxButton.YesNoCancel, MessageBoxResult.Cancel, new[] { "Mute", "Leave", "Cancel" });
                        //if (messageBoxResult == MessageBoxResult.Yes)
                        //{
                        //    if (GroupInfoModel.ImNotificationEnabled)
                        //    {
                        //        OnChatSettingsClicked(0);
                        //    }
                        //}
                        //else if (messageBoxResult == MessageBoxResult.No)
                        //{
                        //    if (memberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_OWNER)
                        //    {
                        //        ShowOwnerLeaveView();
                        //    }
                        //    else
                        //    {
                        //        RemoveGroupMember(DefaultSettings.LOGIN_TABLE_ID);
                        //    }
                        //}
                        WNConfirmationView cv = new WNConfirmationView("Leave confirmation!", warnigMessage, CustomConfirmationDialogButtonOptions.YesNoCancel, new[] { "Mute", "Leave", "Cancel" });
                        var result = cv.ShowCustomDialog();
                        if (result == ConfirmationDialogResult.Yes)
                        {
                            if (GroupInfoModel.ImNotificationEnabled)
                            {
                                OnChatSettingsClicked(0);
                            }
                        }
                        else if (result == ConfirmationDialogResult.No)
                        {
                            if (memberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_OWNER)
                            {
                                ShowOwnerLeaveView();
                            }
                            else
                            {
                                RemoveGroupMember(DefaultSettings.LOGIN_TABLE_ID);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLeaveGroupClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool CanLeaveGroupClick(object param)
        {
            try
            {
                GroupMemberInfoModel memberModel = GroupInfoModel.MemberInfoDictionary.TryGetValue(DefaultSettings.LOGIN_TABLE_ID);
                if (memberModel != null)
                {
                    return memberModel.IsReadyForAction;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLeaveGroupClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        private void AddGroupMember(List<UserBasicInfoModel> modelList)
        {
            try
            {
                if (modelList != null && modelList.Count > 0)
                {
                    if ((modelList.Count + GroupInfoModel.NumberOfMembers) > DefaultSettings.MAX_GROUP_MEMBER_LIMIT)
                    {
                        UIHelperMethods.ShowFailed(String.Format(NotificationMessages.GROUP_MEMBER_LIMIT_EXITED, DefaultSettings.MAX_GROUP_MEMBER_LIMIT));
                        return;
                    }

                    List<GroupMemberInfoDTO> dbList = new List<GroupMemberInfoDTO>();
                    List<BaseMemberDTO> msgDTOs = new List<BaseMemberDTO>();

                    foreach (UserBasicInfoModel model in modelList)
                    {
                        GroupMemberInfoDTO memberDTO = new GroupMemberInfoDTO();
                        memberDTO.UserTableID = model.ShortInfoModel.UserTableID;
                        memberDTO.RingID = model.ShortInfoModel.UserIdentity;
                        memberDTO.GroupID = _GroupID;
                        memberDTO.FullName = model.ShortInfoModel.FullName;
                        memberDTO.MemberAccessType = ChatConstants.MEMBER_TYPE_MEMBER;
                        memberDTO.MemberAddedBy = DefaultSettings.LOGIN_TABLE_ID;
                        dbList.Add(memberDTO);

                        GroupInfoModel.LoadMemberData(memberDTO);

                        BaseMemberDTO msgDTO = new BaseMemberDTO();
                        msgDTO.MemberIdentity = memberDTO.UserTableID;
                        msgDTO.RingID = memberDTO.RingID;
                        msgDTO.FullName = memberDTO.FullName;
                        msgDTO.Status = memberDTO.MemberAccessType;
                        msgDTO.AddedBy = memberDTO.MemberAddedBy;
                        msgDTOs.Add(msgDTO);
                    }

                    ChatHelpers.SendGroupMemberAdd(GroupInfoModel, msgDTOs, dbList, (status) =>
                    {
                        if (status == false) UIHelperMethods.ShowFailed("Sorry! Conversation member add request failed. Try again.");
                        return 0;
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AddGroupMember() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void RemoveGroupMember(long memberIdentity)
        {
            try
            {
                GroupMemberInfoModel memberModel = GroupInfoModel.MemberInfoDictionary.TryGetValue(memberIdentity);//GroupInfoModel.RemoveMemberData(memberIdentity);
                if (memberModel != null)
                {
                    memberModel.IsReadyForAction = false;
                    List<GroupMemberInfoDTO> dbList = new List<GroupMemberInfoDTO>();

                    GroupMemberInfoDTO memberDTO = new GroupMemberInfoDTO();
                    memberDTO.GroupID = _GroupID;
                    memberDTO.UserTableID = memberIdentity;
                    memberDTO.RingID = memberModel.RingID;
                    memberDTO.IntegerStatus = StatusConstants.STATUS_DELETED;
                    dbList.Add(memberDTO);

                    if (memberIdentity == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        ChatHelpers.LeaveFromGroup(GroupInfoModel, memberModel, dbList, (status) =>
                        {
                            if (status)
                            {
                                Application.Current.Dispatcher.BeginInvoke(() =>
                                {
                                    CancelGroupImageChange();
                                    CancelGroupNameChange();
                                });
                            }
                            else
                            {
                                string leaveRemove = (memberIdentity == DefaultSettings.LOGIN_TABLE_ID) ? "leave" : "member remove";
                                UIHelperMethods.ShowFailed("Sorry! Conversation " + leaveRemove + " request failed. Try again.", leaveRemove);
                                //CustomMessageBox.ShowError("Sorry! Conversation " + (memberIdentity == DefaultSettings.LOGIN_TABLE_ID ? "leave" : "member remove") + " request failed. Try again.");
                            }
                            return 0;
                        });
                    }
                    else
                    {
                        ChatHelpers.SendRemoveGroupMember(GroupInfoModel, memberModel, dbList, (status) =>
                        {
                            string leaveRemove = (memberIdentity == DefaultSettings.LOGIN_TABLE_ID) ? "leave" : "member remove";
                            if (status == false) UIHelperMethods.ShowFailed("Sorry! Conversation " + leaveRemove + " request failed. Try again.", leaveRemove);
                            return 0;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: RemoveGroupMember() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeGroupMemberAccessType(List<GroupMemberInfoDTO> memberDTOs)
        {
            try
            {
                List<GroupMemberInfoDTO> oldMemberDTOs = new List<GroupMemberInfoDTO>();
                List<BaseMemberDTO> memberMsgList = new List<BaseMemberDTO>();
                List<GroupMemberInfoModel> memberModelList = new List<GroupMemberInfoModel>();

                foreach (GroupMemberInfoDTO memberDTO in memberDTOs)
                {
                    GroupMemberInfoModel memberModel = GroupInfoModel.MemberInfoDictionary.TryGetValue(memberDTO.UserTableID);
                    if (memberModel != null)
                    {
                        memberModel.IsReadyForAction = false;
                        BaseMemberDTO msgDTO = new BaseMemberDTO();
                        msgDTO.MemberIdentity = memberDTO.UserTableID;
                        msgDTO.RingID = memberDTO.RingID;
                        msgDTO.Status = memberDTO.MemberAccessType;
                        memberMsgList.Add(msgDTO);

                        GroupMemberInfoDTO oldMemberDTO = new GroupMemberInfoDTO();
                        oldMemberDTO.UserTableID = memberModel.UserTableID;
                        oldMemberDTO.MemberAccessType = memberModel.MemberAccessType;
                        memberModel.MemberAccessType = memberDTO.MemberAccessType;
                        oldMemberDTOs.Add(oldMemberDTO);
                        memberModelList.Add(memberModel);
                    }
                }

                ChatHelpers.SendMemberAccessType(GroupInfoModel, memberMsgList, oldMemberDTOs, memberModelList, (status) =>
                {
                    if (status == false) UIHelperMethods.ShowFailed("Sorry! Conversation access change request failed. Try again.");
                    return 0;
                });
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeGroupMemberAccessType() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnBack(object param)
        {
            try
            {
                MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnBack() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowProfile(object param)
        {
            if ((long)param == DefaultSettings.LOGIN_TABLE_ID)
            {
                RingIDViewModel.Instance.OnMyProfileClicked(null);
            }
            else
            {
                RingIDViewModel.Instance.OnFriendProfileButtonClicked(param);
            }
        }

        private bool CanDropGroupMember(DragEventArgs e)
        {
            bool dropEnabled = true;
            try
            {
                if (e.Data.GetDataPresent(typeof(UserBasicInfoModel)))
                {
                    UserBasicInfoModel model = e.Data.GetData(typeof(UserBasicInfoModel)) as UserBasicInfoModel;
                    if (model != null)
                    {
                        if (GroupInfoModel.MemberInfoDictionary.Values.FirstOrDefault(P => P.UserTableID == model.ShortInfoModel.UserTableID && P.MemberAccessType != ChatConstants.MEMBER_TYPE_NOT_MEMBER) != null)
                        {
                            dropEnabled = false;
                        }
                    }
                    else
                    {
                        dropEnabled = false;
                    }
                }
                else
                {
                    dropEnabled = false;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CanDropGroupMember() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return dropEnabled;
        }

        private void OnDropGroupMember(DragEventArgs e)
        {
            try
            {
                UserBasicInfoModel model = e.Data.GetData(typeof(UserBasicInfoModel)) as UserBasicInfoModel;
                if (model != null)
                {
                    List<UserBasicInfoModel> list = new List<UserBasicInfoModel>();
                    list.Add(model);
                    AddGroupMember(list);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnDropGroupMember() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowOwnerLeaveView()
        {
            try
            {
                UCGroupChatCallPanel ucGroupChatCall = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(_GroupID);
                if (ucGroupChatCall != null)
                {
                    ucGroupChatCall.HideOwnerLeaveView();
                }

                _GroupOwnerLeavePanel = new UCGroupOwnerLeave(pnlOwnerLeaveContainer, GroupInfoModel);
                _GroupOwnerLeavePanel.Show();
                _GroupOwnerLeavePanel.CompletedHandler += (ownerID) =>
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        if (ownerID > 0)
                        {
                            CancelGroupImageChange();
                            CancelGroupNameChange();
                        }
                        HideOwnerLeaveView();
                    }, DispatcherPriority.Send);
                };
                //pnlOwnerLeaveContainer.Child = _GroupOwnerLeavePanel;
                IsOwnerLeaveMode = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowImagePreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideOwnerLeaveView()
        {
            try
            {
                IsOwnerLeaveMode = false;
                if (_GroupOwnerLeavePanel != null)
                {
                    pnlOwnerLeaveContainer.Children.Remove(_GroupOwnerLeavePanel);
                    _GroupOwnerLeavePanel.Dispose();
                    _GroupOwnerLeavePanel = null;
                }

            }
            catch (Exception ex)
            {
                log.Error("Error: HideImagePreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnChatBackgroundChange(object param)
        {
            try
            {
                //SELECT BACKGROUND
                ChatBgHistoryModel bgModel = param as ChatBgHistoryModel;

                if (!String.IsNullOrWhiteSpace(GroupInfoModel.ChatBgUrl))
                {
                    if (bgModel.ChatBgUrl.Equals(GroupInfoModel.ChatBgUrl))
                    {
                        GroupInfoModel.ChatBgUrl = String.Empty;
                        GroupInfoDAO.UpdateChatBackground(_GroupID, String.Empty);
                        return;
                    }
                }

                GroupInfoModel.ChatBgUrl = bgModel.ChatBgUrl;
                GroupInfoModel.EventChatBgUrl = null;
                GroupInfoDAO.UpdateChatBackground(_GroupID, GroupInfoModel.ChatBgUrl);

                bgModel.UpdateTime = ChatService.GetServerTime();
                ChatBgHistoryDAO.UpdateChatBgLastUsedTime(GroupInfoModel.ChatBgUrl, bgModel.UpdateTime);

                if (bgModel.Id > 0 && new ImageFile(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER + System.IO.Path.DirectorySeparatorChar + bgModel.ChatBgUrl).IsComplete == false)
                {
                    MessegeBoxWithLoader msgBox = UIHelperMethods.ShowMessageWithLoader("Downloading Selected Background");
                    ChatBackgroundDownloader downloader = new ChatBackgroundDownloader(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER, bgModel.ChatBgUrl);
                    downloader.OnComplete += (imageUrl, status) =>
                    {
                        msgBox.CloseMessage();
                        if (status && GroupInfoModel.ChatBgUrl.Equals(imageUrl))
                        {
                            GroupInfoModel.OnPropertyChanged("ChatBgUrl");
                        }
                    };
                    downloader.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChatBackgroundChange() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnChatSettingsClicked(object param)
        {
            try
            {
                int value = Int32.Parse(param.ToString());
                if (value == 0)
                {
                    //SHOW/HIDE IMSOUND 
                    //GroupInfoModel.ImSoundEnabled = !GroupInfoModel.ImSoundEnabled;
                    GroupInfoDAO.UpdateIMSound(_GroupID, GroupInfoModel.ImSoundEnabled);
                }
                else if (value == 1)
                {
                    //SHOW/HIDE IMNOTIFICATION 
                    //GroupInfoModel.ImNotificationEnabled = !GroupInfoModel.ImNotificationEnabled;
                    GroupInfoDAO.UpdateIMNotification(_GroupID, GroupInfoModel.ImNotificationEnabled);
                }
                else if (value == 2)
                {
                    //SHOW/HIDE IMPOPUP 
                    //GroupInfoModel.ImPopupEnabled = !GroupInfoModel.ImPopupEnabled;
                    GroupInfoDAO.UpdateIMPopUp(_GroupID, GroupInfoModel.ImPopupEnabled);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChatSettingsClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand AddMemberCommand
        {
            get
            {
                if (_AddMemberCommand == null)
                {
                    _AddMemberCommand = new RelayCommand((param) => OnAddMemberClick(param));
                }
                return _AddMemberCommand;
            }
        }

        public ICommand LeaveGroupCommand
        {
            get
            {
                if (_LeaveGroupCommand == null)
                {
                    _LeaveGroupCommand = new RelayCommand((param) => OnLeaveGroup(param), (param) => CanLeaveGroupClick(param));
                }
                return _LeaveGroupCommand;
            }
        }

        public ICommand ShowProfileCommand
        {
            get
            {
                if (_ShowProfileCommand == null)
                {
                    _ShowProfileCommand = new RelayCommand((param) => ShowProfile(param));
                }
                return _ShowProfileCommand;
            }
        }

        public ICommand OnGroupNameChangeCommand
        {
            get
            {
                if (_OnGroupNameChangeCommand == null)
                {
                    _OnGroupNameChangeCommand = new RelayCommand((param) => OnGroupNameChange(param), (param) => CanGroupNameChange(param));
                }
                return _OnGroupNameChangeCommand;
            }
        }

        public ICommand OnGroupImageChangeCommand
        {
            get
            {
                if (_OnGroupImageChangeCommand == null)
                {
                    _OnGroupImageChangeCommand = new RelayCommand((param) => OnGroupImageChange(param));
                }
                return _OnGroupImageChangeCommand;
            }
        }

        public ICommand OnBackCommand
        {
            get
            {
                if (_OnBackCommand == null)
                {
                    _OnBackCommand = new RelayCommand((param) => OnBack(param));
                }
                return _OnBackCommand;
            }
        }

        public ICommand ChatSettingsCommand
        {
            get
            {
                if (_ChatSettingsCommand == null)
                {
                    _ChatSettingsCommand = new RelayCommand(param => OnChatSettingsClicked(param));
                }
                return _ChatSettingsCommand;
            }
        }

        public bool IsGroupNameEditVisible
        {
            get { return _IsGroupNameEditVisible; }
            set
            {
                _IsGroupNameEditVisible = value;
                this.OnPropertyChanged("IsGroupNameEditVisible");
            }
        }

        public bool IsOwnerLeaveMode
        {
            get { return _IsOwnerLeaveMode; }
            set
            {
                _IsOwnerLeaveMode = value;
                this.OnPropertyChanged("IsOwnerLeaveMode");
            }
        }

        public int GroupImageUploadState
        {
            get { return _GroupImageUploadState; }
            set
            {
                _GroupImageUploadState = value;
                this.OnPropertyChanged("GroupImageUploadState");
            }
        }

        public GroupInfoModel GroupInfoModel
        {
            get { return _GroupInfoModel; }
            set { _GroupInfoModel = value; }
        }

        public ICommand OnChatBackgroundChangeCommand
        {
            get
            {
                if (_OnChatBackgroundChangeCommand == null)
                {
                    _OnChatBackgroundChangeCommand = new RelayCommand((param) => OnChatBackgroundChange(param));
                }
                return _OnChatBackgroundChangeCommand;
            }
        }

        public ObservableCollection<ChatBgHistoryModel> ChatBgHistoryModelList
        {
            get { return _ChatBgHistoryModelList; }
            set { _ChatBgHistoryModelList = value; }
        }

        #endregion Property

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int idx = TabList.SelectedIndex;
                if (idx == 0)
                {
                    pnlAllMembers.Visibility = Visibility.Visible;
                    pnlAdmins.Visibility = Visibility.Collapsed;
                }
                else if (idx == 1)
                {
                    pnlAllMembers.Visibility = Visibility.Collapsed;
                    pnlAdmins.Visibility = Visibility.Visible;
                }

                ChangeMemberOpenStatus();
            }
            catch (Exception ex)
            {

                log.Error("Error: TabControl_SelectionChanged() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void list_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                ListBox listbx = (ListBox)control;
                if (listbx.SelectedItem != null)
                {
                    object selectedItem = listbx.SelectedItem;
                    ListBoxItem selectedListBoxItem = listbx.ItemContainerGenerator.ContainerFromItem(selectedItem) as ListBoxItem;
                    if (selectedListBoxItem == prevItem)
                    {
                        selectedListBoxItem.IsSelected = (!selectedListBoxItem.IsSelected);
                        prevItem = null;
                    }
                    else
                    {
                        prevItem = selectedListBoxItem;
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: list_MouseLeftButtonUp() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void list_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            try
            {
                if (!e.Handled)
                {
                    e.Handled = true;
                    var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                    eventArg.Source = sender;
                    var parent = ((Control)sender).Parent as UIElement;
                    parent.RaiseEvent(eventArg);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: list_PreviewMouseWheel() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

    }
}

﻿using Models.Constants;
using Models.DAO;
using Models.Stores;
using Models.Utility;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.Dictonary;
using View.UI.Call;
using View.UI.Chat;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.Utility.Chat;
using View.Utility.Recent;
using View.ViewModel;
using Models.Entity;
using Auth.utility;
using imsdkwrapper;
using View.Utility.Chat.Service;
using View.Utility;
using System.Collections.Concurrent;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCHistorySettings.xaml
    /// </summary>
    public partial class UCHistorySettings : UserControl
    {
        public UCHistorySettings()
        {
            InitializeComponent();
            this.Loaded += UCHistorySettings_Loaded;
            this.Unloaded += UCHistorySettings_Unloaded;
        }

        void UCHistorySettings_Unloaded(object sender, RoutedEventArgs e)
        {
            BtnClrHistory.Click -= ClearHistory_Click;
            RadioBtnNone.Checked -= RadioBtnAll_Checked;
            RadioBtn_15_days.Checked -= RadioBtnAll_Checked;
            RadioBtn_3_days.Checked -= RadioBtnAll_Checked;
            RadioBtn_3_Months.Checked -= RadioBtnAll_Checked;
            RadioBtn_7_days.Checked -= RadioBtnAll_Checked;
            RadioBtn_1_Month.Checked -= RadioBtnAll_Checked;
            //RadioBtnToday.Checked -= RadioBtnAll_Checked;
            RadioBtnYesterday.Checked -= RadioBtnAll_Checked;
            RadioBtnAll.Checked -= RadioBtnAll_Checked;
            RadioBtn_Custom.Checked -= RadioBtnAll_Checked;
            customDaysBox.PreviewTextInput -= textBoxCodeVerification_PreviewTextInput;
            customDaysBox.GotFocus -= TextBlock_GotFocus;
        }

        void UCHistorySettings_Loaded(object sender, RoutedEventArgs e)
        {
            BtnClrHistory.Click += ClearHistory_Click;
            RadioBtnNone.Checked += RadioBtnAll_Checked;
            RadioBtn_15_days.Checked += RadioBtnAll_Checked;
            RadioBtn_3_days.Checked += RadioBtnAll_Checked;
            RadioBtn_3_Months.Checked += RadioBtnAll_Checked;
            RadioBtn_7_days.Checked += RadioBtnAll_Checked;
            RadioBtn_1_Month.Checked += RadioBtnAll_Checked;
            // RadioBtnToday.Checked += RadioBtnAll_Checked;
            RadioBtnYesterday.Checked += RadioBtnAll_Checked;
            RadioBtnAll.Checked += RadioBtnAll_Checked;
            customDaysBox.PreviewTextInput += textBoxCodeVerification_PreviewTextInput;
            customDaysBox.GotFocus += TextBlock_GotFocus;
            RadioBtn_Custom.Checked += RadioBtnAll_Checked;
        }

        private RadioButton prevRb = null;
        private void RadioBtnAll_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (prevRb != null && !rb.Name.Equals(prevRb.Name))
            {
                prevRb.IsChecked = false;
            }
            prevRb = rb;
        }

        private void ClearHistory_Click(object sender, RoutedEventArgs e)
        {
            this.errorLabel.Content = "";
            long maxTime = 0;

            if (prevRb.TabIndex > 0)
            {
                if (prevRb.TabIndex == 9) //custom
                {
                    int numberOfDays = 0;
                    if (customDaysBox.Text.Trim().Length > 0 && int.TryParse(customDaysBox.Text, out numberOfDays))
                    {
                        maxTime = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - (numberOfDays * SettingsConstants.MILISECONDS_IN_DAY);
                    }
                    else
                    {
                        errorLabel.Content = "Please enter a number in days!";
                        return;
                    }
                }
                else if (prevRb.TabIndex == 8) //All
                {
                    maxTime = ModelUtility.CurrentTimeMillis();
                }
                else  //others,dates specified
                {
                    maxTime = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - (DefaultSettings.HISTORY_ARRAY_DAY[prevRb.TabIndex] * SettingsConstants.MILISECONDS_IN_DAY);
                }

                ChatHelpers.UpdateAllHistory(maxTime);
            }

            RingIDViewModel.Instance._WNRingIDSettings.Close();
        }

        private void AddTestDataHistory_Click(object sender, RoutedEventArgs e)
        {
            new Thread(() =>
            {
                ChatHelpers.TestChatLoad();
            }).Start();
        }

        private void textBoxCodeVerification_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void TextBlock_GotFocus(object sender, RoutedEventArgs e)
        {
            prevRb.IsChecked = false;
            RadioBtn_Custom.IsChecked = true;
            prevRb = RadioBtn_Custom;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using View.ViewModel;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for WNRingIDSettingsWindow.xaml
    /// </summary>
    public partial class WNRingIDSettingsWindow : Window, INotifyPropertyChanged
    {
        public int prevIdx = -1;
        public const int accountSetting = 0;
        public const int privcySettings = 1;
        public const int generalSettings = 2;
        public const int imSettings = 3;
        public const int callDivrt = 4;
        public const int historySettings = 5;
        public const int audioSettings = 6;
        public const int videoSettings = 7;

        public UCAccountSettings _AccountSettings = null;
        public UCRingPrivacySettings _UCRingPrivacySettings = null;
        public UCPrivacySettings _PrivacySettingsInstance = null; //old
        public UCCallSettings _UCCallSettings = null;
        public UCIMSettings _IMSettings = null;
        public UCChangePassword _ChangePassword = null;
        public UCAudioSettings _AudioSettings = null;
        public UCHistorySettings _HistorySettings = null;
        public UCVideoSettings _VideoSettings = null;
        public UCGeneralSettings _UCGeneralSettings;

        public WNRingIDSettingsWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            this.PreviewKeyDown += WNRingIDSettingsWindow_PreviewKeyDown;
        }

        void WNRingIDSettingsWindow_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key.Equals(System.Windows.Input.Key.Escape))
            {
                if (!(_UCGeneralSettings != null && _UCGeneralSettings.ucValidityPopup != null && _UCGeneralSettings.ucValidityPopup.popupValidity.IsOpen)) this.Close();
            }
        }

        public void SettingIndex(int index)
        {
            switch (index)
            {
                case accountSetting:
                    if (_AccountSettings == null)
                    {
                        _AccountSettings = new UCAccountSettings(_WindowFirstGrid);
                        AccountSettingsPanel.Child = _AccountSettings;
                    }
                    break;
                case (privcySettings):
                    if (_UCRingPrivacySettings == null)
                    {
                        _UCRingPrivacySettings = new UCRingPrivacySettings();
                        PrivacyPanel.Child = _UCRingPrivacySettings;
                    }
                    break;
                case generalSettings:
                    if (_UCGeneralSettings == null)
                    {
                        _UCGeneralSettings = new UCGeneralSettings();
                        _UCGeneralSettings.LoadCheckBoxValues();
                        GeneralSetiingsPanel.Child = _UCGeneralSettings;
                    }
                    break;
                case imSettings:
                    if (_IMSettings == null)
                    {
                        _IMSettings = new UCIMSettings();
                        IMSettingsPanel.Child = _IMSettings;
                    }
                    break;
                case (callDivrt):

                    if (_UCCallSettings == null)
                    {
                        _UCCallSettings = new UCCallSettings();
                        CallSettingsPanel.Child = _UCCallSettings;
                    }
                    _UCCallSettings.LoadData();
                    break;
                case (historySettings):
                    {
                        if (_HistorySettings == null)
                        {
                            _HistorySettings = new UCHistorySettings();
                            HistorySettingsPanel.Child = _HistorySettings;
                        }
                    }
                    break;
                case audioSettings:
                    if (_AudioSettings == null)
                    {
                        _AudioSettings = new UCAudioSettings();
                        AudioSettingsPanel.Child = _AudioSettings;
                    }
                    break;
                case videoSettings:
                    if (_VideoSettings == null)
                    {
                        _VideoSettings = new UCVideoSettings();
                        VideoSettingsPanel.Child = _VideoSettings;
                    }
                    break;
                default:
                    break;
            }
        }


        private void SettingWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = false;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int idx = TabList.SelectedIndex;
            if (idx == prevIdx) return;
            prevIdx = idx;
            SettingIndex(idx);
        }

        public List<int> _ComboboxList = new List<int>();
        public void FindLastItem()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.userProfile.ProfileImage)) _ComboboxList.Add(1);
            if (!string.IsNullOrEmpty(DefaultSettings.userProfile.CoverImage)) _ComboboxList.Add(2);
            if (!string.IsNullOrEmpty(DefaultSettings.userProfile.Email)) _ComboboxList.Add(3);
            if (!string.IsNullOrEmpty(DefaultSettings.userProfile.MobileNumber)) _ComboboxList.Add(4);
            if (DefaultSettings.userProfile.BirthDay != 1) _ComboboxList.Add(5);
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            try
            {
                if (RingIDViewModel.Instance._WNRingIDSettings != null) RingIDViewModel.Instance._WNRingIDSettings = null;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(WNRingIDSettingsWindow).Name).Error("Error occured in WNRingIDSettingsWindow.....> " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message);
            }
        }
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion//INotifyPropertyChanged Members
    }
}

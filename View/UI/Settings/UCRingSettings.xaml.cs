﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using View.Utility;
using View.ViewModel;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCRingSettings.xaml
    /// </summary>
    public partial class UCRingSettings : UserControl, INotifyPropertyChanged
    {
        public UCRingSettings()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCRingSettings_Loaded;
            this.Unloaded += UCRingSettings_Unloaded;
            LoadCheckBoxValues();
        }

        void UCRingSettings_Unloaded(object sender, RoutedEventArgs e)
        {
            SaveChangesBtn.Click -= SaveChangesBtn_Click;
            ResetBtn.Click -= ResetBtn_Click;
            AutologinCheckbox.Click -= AutologinCheckbox_Click;
            AutoWindowsStartCheckbox.Click -= AutoWindowsStartCheckbox_Click;            
        }
        void UCRingSettings_Loaded(object sender, RoutedEventArgs e)
        {
            SaveChangesBtn.Click += SaveChangesBtn_Click;
            ResetBtn.Click += ResetBtn_Click;
            AutologinCheckbox.Click += AutologinCheckbox_Click;
            AutoWindowsStartCheckbox.Click += AutoWindowsStartCheckbox_Click;
        }

        private void LoadCheckBoxValues()
        {
            AutologinCheckbox.IsChecked = (DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN == 1);
            AutoWindowsStartCheckbox.IsChecked = (DefaultSettings.VALUE_LOGIN_AUTO_START == 1);
        }

        private void SaveChangesBtn_Click(object sender, RoutedEventArgs e)
        {
            IList<InfoKeyValue> infoKeyValues = new List<InfoKeyValue>();
            if ((AutologinCheckbox.IsChecked != (DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN == 1)))
            {
                DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN = (AutologinCheckbox.IsChecked == true) ? 1 : 0;
                if (DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN == 1)
                {
                    DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
                    infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_SAVE_PASSWORD, DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD + ""));
                }
                infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_AUTO_SIGNIN, DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN + ""));
            }
            if ((AutoWindowsStartCheckbox.IsChecked != (DefaultSettings.VALUE_LOGIN_AUTO_START == 1)))
            {
                DefaultSettings.VALUE_LOGIN_AUTO_START = (AutoWindowsStartCheckbox.IsChecked == true) ? 1 : 0;
                //infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_AUTO_START, DefaultSettings.VALUE_LOGIN_AUTO_START + ""));
                HelperMethods.WriteAutoStartValuetoFile();
            }
            new InsertRingLoginSetting(infoKeyValues).Start();
            errorLbl.Content = "Successfully Changed!";
            errorLbl.Visibility = Visibility.Visible;
            RingIDViewModel.Instance._WNRingIDSettings.Close();
            //exit;
        }

        private void ResetBtn_Click(object sender, RoutedEventArgs e)
        {
            errorLbl.Content = "";
            errorLbl.Visibility = Visibility.Collapsed;
            AutologinCheckbox.IsChecked = true;
            AutoWindowsStartCheckbox.IsChecked = true;
            if ((DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN == 1) && (DefaultSettings.VALUE_LOGIN_AUTO_START == 1) && (DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD == 1))
            {
                //do nothing
            }
            else
            {
                IList<InfoKeyValue> infoKeyValues = new List<InfoKeyValue>();
                DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN = 1;
                DefaultSettings.VALUE_LOGIN_AUTO_START = 1;
                DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
                infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_AUTO_SIGNIN, DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN + ""));
                infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_SAVE_PASSWORD, DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD + ""));
                //infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_AUTO_START, DefaultSettings.VALUE_LOGIN_AUTO_START + ""));
                HelperMethods.WriteAutoStartValuetoFile();
                new InsertRingLoginSetting(infoKeyValues).Start();
            }
            //exit;
            RingIDViewModel.Instance._WNRingIDSettings.Close();
        }

        private bool _IsSelectionchanged = false;
        public bool IsSelectionChanged
        {
            get { return _IsSelectionchanged; }
            set
            {
                _IsSelectionchanged = value;
                OnPropertyChanged("IsSelectionChanged");
            }
        }

        private void AutologinCheckbox_Click(object sender, RoutedEventArgs e)
        {
            if (AutologinCheckbox.IsChecked == (DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN == 1) && AutoWindowsStartCheckbox.IsChecked == (DefaultSettings.VALUE_LOGIN_AUTO_START == 1))
            {
                IsSelectionChanged = false;
            }
            else
            {
                IsSelectionChanged = true;
            }
        }

        private void AutoWindowsStartCheckbox_Click(object sender, RoutedEventArgs e)
        {
            if (AutoWindowsStartCheckbox.IsChecked == (DefaultSettings.VALUE_LOGIN_AUTO_START == 1) && AutologinCheckbox.IsChecked == (DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN == 1))
            {
                IsSelectionChanged = false;
            }
            else
            {
                IsSelectionChanged = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using Newtonsoft.Json.Linq;
using View.Utility;
using View.Utility.Auth;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCPrivacySettings.xaml
    /// </summary>
    public partial class UCPrivacySettings : UserControl, INotifyPropertyChanged
    {

        public UCPrivacySettings()
        {

            this.DataContext = this;
            InitializeComponent();
            ProfileImgPanel.Visibility = Visibility.Collapsed;
            CoverImgPanel.Visibility = Visibility.Collapsed;
            EmailPnl.Visibility = Visibility.Collapsed;
            MoblPnl.Visibility = Visibility.Collapsed;
            BirthdayPnl.Visibility = Visibility.Collapsed;
            //FindLastItem();
        }
        public void LoadPrivacy()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (!string.IsNullOrEmpty(DefaultSettings.userProfile.ProfileImage))
                {
                    ProfileImgPanel.Visibility = Visibility.Visible;
                    ProfileImageBox.SelectedIndex = DefaultSettings.userProfile.ProfileImagePrivacy - 1;
                }
                else
                {
                    ProfileImgPanel.Visibility = Visibility.Collapsed;
                }
                if (!string.IsNullOrEmpty(DefaultSettings.userProfile.CoverImage))
                {
                    CoverImgPanel.Visibility = Visibility.Visible;
                    CoverImageBox.SelectedIndex = DefaultSettings.userProfile.CoverImagePrivacy - 1;

                }
                else
                {
                    CoverImgPanel.Visibility = Visibility.Collapsed;
                }

                if (!string.IsNullOrEmpty(DefaultSettings.userProfile.Email))
                {
                    EmailPnl.Visibility = Visibility.Visible;
                    EmailBox.SelectedIndex = DefaultSettings.userProfile.EmailPrivacy - 1;

                }
                else
                {
                    EmailPnl.Visibility = Visibility.Collapsed;
                }

                if (!string.IsNullOrEmpty(DefaultSettings.userProfile.MobileNumber))
                {

                    MoblPnl.Visibility = Visibility.Visible;
                    MobileBox.SelectedIndex = DefaultSettings.userProfile.MobilePrivacy - 1;
                }
                else
                {
                    MoblPnl.Visibility = Visibility.Collapsed;
                }
                if (DefaultSettings.userProfile.BirthDay != 1)
                {

                    BirthdayPnl.Visibility = Visibility.Visible;
                    BirthdayBox.SelectedIndex = DefaultSettings.userProfile.BirthdayPrivacy - 1;
                }
                else
                {
                    BirthdayPnl.Visibility = Visibility.Collapsed;
                }
                IsSelectionChanged = false;
            });
        }

        private void SaveChangesBtn_Click(object sender, RoutedEventArgs e)
        {
            JObject pakToSend = null;
            if (ProfileImageBox.SelectedIndex >= 0 && ProfileImageBox.SelectedIndex != (DefaultSettings.userProfile.ProfileImagePrivacy - 1))
            {
                if (pakToSend == null) { pakToSend = new JObject(); } pakToSend[JsonKeys.ProfileImagePrivacy] = (short)ProfileImageBox.SelectedIndex + 1;
            }
            if (CoverImageBox.SelectedIndex >= 0 && CoverImageBox.SelectedIndex != (DefaultSettings.userProfile.CoverImagePrivacy - 1))
            {
                if (pakToSend == null) { pakToSend = new JObject(); } pakToSend[JsonKeys.CoverImagePrivacy] = (short)CoverImageBox.SelectedIndex + 1;
            }
            if (EmailBox.SelectedIndex >= 0 && EmailBox.SelectedIndex != (DefaultSettings.userProfile.EmailPrivacy - 1))
            {
                if (pakToSend == null) { pakToSend = new JObject(); } pakToSend[JsonKeys.EmailPrivacy] = (short)EmailBox.SelectedIndex + 1;

            }
            if (MobileBox.SelectedIndex >= 0 && MobileBox.SelectedIndex != (DefaultSettings.userProfile.MobilePrivacy - 1))
            {
                if (pakToSend == null) { pakToSend = new JObject(); } pakToSend[JsonKeys.MobilePhonePrivacy] = (short)MobileBox.SelectedIndex + 1;
            }
            if (BirthdayBox.SelectedIndex >= 0 && BirthdayBox.SelectedIndex != (DefaultSettings.userProfile.BirthdayPrivacy - 1))
            {
                if (pakToSend == null) { pakToSend = new JObject(); } pakToSend[JsonKeys.BirthdayPrivacy] = (short)BirthdayBox.SelectedIndex + 1;
            }
            if (pakToSend == null)
            {
                //CustomMessageBox.ShowWarning(NotificationMessages.ABOUT_NO_CHANGE, "Edit failed");
                UIHelperMethods.ShowWarning(NotificationMessages.ABOUT_NO_CHANGE, "Edit failed");
            }
            else
            {
                Application.Current.Dispatcher.Invoke(() =>
           {
               bool _Success = false;
               string _Msg = "";
               // ChangePrivacy changePrivacy = new ChangePrivacy(pakToSend);
               //  changePrivacy.Run(out _Success, out _Msg);
               (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_PRIVACY_SETTINGS, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _Success, out _Msg);

               if (_Success)
               {
                   //exit window
                   if (EmailBox.SelectedIndex >= 0)
                   {
                       DefaultSettings.userProfile.Privacy[0] = DefaultSettings.userProfile.EmailPrivacy = (short)(EmailBox.SelectedIndex + 1);
                   }
                   if (MobileBox.SelectedIndex >= 0)
                   {
                       DefaultSettings.userProfile.Privacy[1] = DefaultSettings.userProfile.MobilePrivacy = (short)(MobileBox.SelectedIndex + 1);
                   }
                   if (ProfileImageBox.SelectedIndex >= 0)
                   {
                       DefaultSettings.userProfile.Privacy[2] = DefaultSettings.userProfile.ProfileImagePrivacy = (short)(ProfileImageBox.SelectedIndex + 1);
                   }
                   if (BirthdayBox.SelectedIndex >= 0)
                   {
                       DefaultSettings.userProfile.Privacy[3] = DefaultSettings.userProfile.BirthdayPrivacy = (short)(BirthdayBox.SelectedIndex + 1);
                   }
                   if (CoverImageBox.SelectedIndex >= 0)
                   {
                       DefaultSettings.userProfile.Privacy[4] = DefaultSettings.userProfile.CoverImagePrivacy = (short)(CoverImageBox.SelectedIndex + 1);
                   }
                   RingIDViewModel.Instance._WNRingIDSettings.Close();
               }
               else
               {
                   if (!string.IsNullOrEmpty(_Msg))
                       //MessageBox.Show(MainSwitcher.pageSwitcher, "Failed! " + _Msg, "Failed!", MessageBoxButton.OK, MessageBoxImage.Error);
                       UIHelperMethods.ShowFailed(_Msg);
               }

           });
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            IsSelectionChanged = false;
            LoadPrivacy();
        }

        private bool _IsLastItem = false;
        public bool IsLastItem
        {
            get { return _IsLastItem; }
            set
            {
                _IsLastItem = value;
                this.OnPropertyChanged("IsLastItem");
            }
        }
        private bool _IsSelectionChanged = false;
        public bool IsSelectionChanged
        {
            get { return _IsSelectionChanged; }
            set
            {
                _IsSelectionChanged = value;
                this.OnPropertyChanged("IsSelectionChanged");
            }
        }

        private void IsIndexchanged()
        {
            if (ProfileImageBox.SelectedIndex >= 0 && ProfileImageBox.SelectedIndex != (DefaultSettings.userProfile.ProfileImagePrivacy - 1)
                || CoverImageBox.SelectedIndex >= 0 && CoverImageBox.SelectedIndex != (DefaultSettings.userProfile.CoverImagePrivacy - 1)
                || EmailBox.SelectedIndex >= 0 && EmailBox.SelectedIndex != (DefaultSettings.userProfile.EmailPrivacy - 1)
                || MobileBox.SelectedIndex >= 0 && MobileBox.SelectedIndex != (DefaultSettings.userProfile.MobilePrivacy - 1)
                || BirthdayBox.SelectedIndex >= 0 && BirthdayBox.SelectedIndex != (DefaultSettings.userProfile.BirthdayPrivacy - 1))
            {
                IsSelectionChanged = true;
            }
            else
            {
                IsSelectionChanged = false;
            }
        }

        private void ProfileImageBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsIndexchanged();
        }

        private void CoverImageBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsIndexchanged();

        }

        private void EmailBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsIndexchanged();
        }

        private void MobileBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsIndexchanged();
        }

        private void BirthdayBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsIndexchanged();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

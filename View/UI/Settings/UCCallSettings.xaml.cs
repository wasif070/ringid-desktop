﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.BindingModels;
using View.Utility;
using View.Utility.Myprofile;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCCallSettings.xaml
    /// </summary>
    public partial class UCCallSettings : UserControl
    {
        public long _SelectedFriendIdentity = 0;
        private ICommand _AddDivertClickCommand;
        private ICommand _RemoveDivertClickCommand;
        public UCCustomizeMessageSettings _UCCustomizeMessageSettings;
        public UCCallSettings()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCCallSettings_Loaded;
            this.Unloaded += UCCallSettings_Unloaded;
        }

        void UCCallSettings_Unloaded(object sender, RoutedEventArgs e)
        {
            EveryoneCheckbox.Click -= Checkbox_Click;
            FriendsOnlyCheckbox.Click -= Checkbox_Click;
        }

        void UCCallSettings_Loaded(object sender, RoutedEventArgs e)
        {
            EveryoneCheckbox.Click += Checkbox_Click;
            FriendsOnlyCheckbox.Click += Checkbox_Click;
        }

        private void Checkbox_Click(object sender, RoutedEventArgs e)
        {
            int prevValue = RingIDViewModel.Instance.MyBasicInfoModel.AnonymousCallAccess;
            int currValue = prevValue == StatusConstants.CHAT_CALL_EVERYONE ? StatusConstants.CHAT_CALL_FRIENDS_ONLY : StatusConstants.CHAT_CALL_EVERYONE;
            RingIDViewModel.Instance.MyBasicInfoModel.AnonymousCallAccess = currValue;
            EveryoneCheckbox.IsEnabled = false;
            FriendsOnlyCheckbox.IsEnabled = false;
            ThradChangeAccessSettings request = new ThradChangeAccessSettings(StatusConstants.ANONYMOUS_CALL, currValue);
            request.StartThread();
            request.OnComplete += (status, erroMessage) =>
            {
                if (!status)
                {
                    RingIDViewModel.Instance.MyBasicInfoModel.AnonymousCallAccess = prevValue;
                    UIHelperMethods.ShowFailed(erroMessage);
                    //  CustomMessageBox.ShowError(erroMessage, "Failed!");
                }

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    EveryoneCheckbox.IsEnabled = true;
                    FriendsOnlyCheckbox.IsEnabled = true;
                }, System.Windows.Threading.DispatcherPriority.Send);
            };
        }

        public void LoadCallSettings()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (_UCCustomizeMessageSettings == null) _UCCustomizeMessageSettings = new UCCustomizeMessageSettings();
                CallDivertPanel.Child = _UCCustomizeMessageSettings;
            });
        }

        public UserBasicInfoModel userInfoModel;

        #region Call Settings
        public void LoadData()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (!DefaultSettings.IS_MY_PROFILE_VALUES_LOADED) new ThradMyProfileDetails().StartThread();
                else LoadCallSettings();

            });
        }
        #endregion
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}

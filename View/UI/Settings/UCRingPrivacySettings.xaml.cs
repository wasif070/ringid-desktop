﻿using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using View.Utility.Myprofile;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Channel;
using Models.Entity;
using System.Collections.Generic;
using System;
using View.Utility;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCRingPrivacySettings.xaml
    /// </summary>
    public partial class UCRingPrivacySettings : UserControl
    {
        public UCRingPrivacySettings()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCRingPrivacySettings_Loaded;
            this.Unloaded += UCRingPrivacySettings_Unloaded;
        }


        void UCRingPrivacySettings_Unloaded(object sender, RoutedEventArgs e)
        {
            AllowIncomingFRCheckbox.Click -= AllowIncomingFRCheckbox_Click;
            AllowFriendsToAddMeCheckBox.Click -= AllowFriendsToAddMeCheckBox_Click;
            AutoAddFriendsCheckBox.Click -= AutoAddFriendsCheckBox_Click;
        }
        void UCRingPrivacySettings_Loaded(object sender, RoutedEventArgs e)
        {
            AllowIncomingFRCheckbox.Click += AllowIncomingFRCheckbox_Click;
            AllowFriendsToAddMeCheckBox.Click += AllowFriendsToAddMeCheckBox_Click;
            AutoAddFriendsCheckBox.Click += AutoAddFriendsCheckBox_Click;
        }

        private void AllowIncomingFRCheckbox_Click(object sender, RoutedEventArgs e)
        {
            int prevValue = RingIDViewModel.Instance.MyBasicInfoModel.AllowIncomingFriendRequest;
            int currValue = prevValue == StatusConstants.AIFR_AFAM_AAD_SET ? StatusConstants.AIFR_AFAM_AAD_UNSET : StatusConstants.AIFR_AFAM_AAD_SET;
            RingIDViewModel.Instance.MyBasicInfoModel.AllowIncomingFriendRequest = currValue;

            AllowIncomingFRCheckbox.IsEnabled = false;

            ThradChangeAccessSettings request = new ThradChangeAccessSettings(StatusConstants.ALLOW_INCOMING_FRIEND_REQUEST, currValue);
            request.StartThread();
            request.OnComplete += (status, erroMessage) =>
            {
                if (!status)
                {
                    RingIDViewModel.Instance.MyBasicInfoModel.AllowIncomingFriendRequest = prevValue;
                    UIHelperMethods.ShowFailed(erroMessage);
                }

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    AllowIncomingFRCheckbox.IsEnabled = true;
                }, System.Windows.Threading.DispatcherPriority.Send);
            };
        }

        private void AllowFriendsToAddMeCheckBox_Click(object sender, RoutedEventArgs e)
        {
            int prevValue = RingIDViewModel.Instance.MyBasicInfoModel.AllowFriendsToAddMe;
            int currValue = prevValue == StatusConstants.AIFR_AFAM_AAD_SET ? StatusConstants.AIFR_AFAM_AAD_UNSET : StatusConstants.AIFR_AFAM_AAD_SET;
            RingIDViewModel.Instance.MyBasicInfoModel.AllowFriendsToAddMe = currValue;

            AllowFriendsToAddMeCheckBox.IsEnabled = false;

            ThradChangeAccessSettings request = new ThradChangeAccessSettings(StatusConstants.ALLOW_FRIENDS_TO_ADD_ME, currValue);
            request.StartThread();
            request.OnComplete += (status, erroMessage) =>
            {
                if (!status)
                {
                    RingIDViewModel.Instance.MyBasicInfoModel.AllowFriendsToAddMe = prevValue;
                    UIHelperMethods.ShowFailed(erroMessage);
                }

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    AllowFriendsToAddMeCheckBox.IsEnabled = true;
                }, System.Windows.Threading.DispatcherPriority.Send);
            };
        }

        private void AutoAddFriendsCheckBox_Click(object sender, RoutedEventArgs e)
        {
            int prevValue = RingIDViewModel.Instance.MyBasicInfoModel.AutoAddFriends;
            int currValue = prevValue == StatusConstants.AIFR_AFAM_AAD_SET ? StatusConstants.AIFR_AFAM_AAD_UNSET : StatusConstants.AIFR_AFAM_AAD_SET;
            RingIDViewModel.Instance.MyBasicInfoModel.AutoAddFriends = currValue;

            AutoAddFriendsCheckBox.IsEnabled = false;

            ThradChangeAccessSettings request = new ThradChangeAccessSettings(StatusConstants.AUTO_ADD_FRIENDS, currValue);
            request.StartThread();
            request.OnComplete += (status, erroMessage) =>
            {
                if (!status)
                {
                    RingIDViewModel.Instance.MyBasicInfoModel.AutoAddFriends = prevValue;
                    UIHelperMethods.ShowFailed(erroMessage);
                }

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    AutoAddFriendsCheckBox.IsEnabled = true;
                }, System.Windows.Threading.DispatcherPriority.Send);
            };
        }
    }
}

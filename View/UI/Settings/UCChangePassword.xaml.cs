﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Models.Constants;
using Models.DAO;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.Constants;
using View.Utility;
using View.Utility.Auth;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Chat.Service;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCChangePassword.xaml
    /// </summary>
    public partial class UCChangePassword : UserControl, INotifyPropertyChanged
    {
        public UCChangePassword()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCChangePassword_Loaded;
            this.Unloaded += UCChangePassword_Unloaded;
        }

        void UCChangePassword_Unloaded(object sender, RoutedEventArgs e)
        {
            SaveChangesBUtton.Click -= SaveBtn_Click;
            CancelButton.Click -= CancelBtn_Click;

            OldPwdBox.PasswordChanged -= OldPwdBoxChanged;
            ConfirmNewPwdBox.PasswordChanged -= NewPwdBoxChanged;
            ConfirmNewPwdBox.PasswordChanged -= ConfirmNewPwdBoxChanged;

            this.PreviewKeyDown -= GeneralPasswordBox_KeyDown;

            //OldPwdBox.KeyDown -= GeneralPasswordBox_KeyDown;
            //NewPwdBox.KeyDown -= GeneralPasswordBox_KeyDown;
            //ConfirmNewPwdBox.KeyDown -= GeneralPasswordBox_KeyDown;
            HideLoader();

        }

        private void HideLoader()
        {
            if (pstLoader != null)
            {
                System.GC.SuppressFinalize(pstLoader);
                pstLoader = null;
            }
        }

        void UCChangePassword_Loaded(object sender, RoutedEventArgs e)
        {
            SaveChangesBUtton.Click += SaveBtn_Click;
            CancelButton.Click += CancelBtn_Click;

            OldPwdBox.PasswordChanged += OldPwdBoxChanged;
            NewPwdBox.PasswordChanged += NewPwdBoxChanged;
            ConfirmNewPwdBox.PasswordChanged += ConfirmNewPwdBoxChanged;

            this.PreviewKeyDown += GeneralPasswordBox_KeyDown;

            //OldPwdBox.KeyDown += GeneralPasswordBox_KeyDown;
            //NewPwdBox.KeyDown += GeneralPasswordBox_KeyDown;
            //ConfirmNewPwdBox.KeyDown += GeneralPasswordBox_KeyDown;

            pstLoader = null;
        }

        void GeneralPasswordBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key.Equals(System.Windows.Input.Key.Enter))
            {
                if (CancelButton.IsFocused)
                {
                    CancelBtn_Click(null, null);
                }
                else
                {
                    SaveBtn_Click(null, null);
                }
            }
            //e.Handled = true;
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            pstLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
            ErrorMessage = "";
            if (OldPwdBox.Password != null && OldPwdBox.Password.Trim().Length > 0 && NewPwdBox.Password != null && NewPwdBox.Password.Trim().Length > 0 && ConfirmNewPwdBox.Password != null && ConfirmNewPwdBox.Password.Trim().Length > 0)
            {
                if (!NewPwdBox.Password.Equals(ConfirmNewPwdBox.Password)) { ErrorMessage = "You must enter the same password twice in order to confirm it."; HideLoader(); return; }
                if (!OldPwdBox.Password.Equals(DefaultSettings.VALUE_LOGIN_USER_PASSWORD)) { ErrorMessage = "Your old password was typed incorrectly."; HideLoader(); return; }
                if (NewPwdBox.Password.Equals(ConfirmNewPwdBox.Password) && NewPwdBox.Password.Equals(DefaultSettings.VALUE_LOGIN_USER_PASSWORD)) { ErrorMessage = "You are using previous password. Please use a new one."; HideLoader(); return; }
                string validationMsg = HelperMethodsModel.PasswordValidationMsg(NewPwdBox.Password);
                if (validationMsg.Trim().Length == 0)
                {
                    Application.Current.Dispatcher.Invoke(new System.Action(() =>
                    {
                        bool _Success = false;
                        string _Msg = string.Empty;
                        SaveChangesBUtton.IsEnabled = false;
                        CancelButton.IsEnabled = false;
                        //pstLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
                        //System.Threading.Thread.Sleep(200);

                        //  ChangeMyPassword changeMyPassword = new ChangeMyPassword(OldPwdBox.Password, NewPwdBox.Password);
                        // changeMyPassword.Run(out _Success, out _Msg);

                        JObject pakToSend = new JObject();
                        pakToSend[JsonKeys.OldPassword] = OldPwdBox.Password;
                        pakToSend[JsonKeys.NewPassword] = NewPwdBox.Password;
                        (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_CHANGE_PASSWORD, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _Success, out _Msg);
                        if (_Success)
                        {
                            //CustomMessageBox.ShowInformation(RingIDViewModel.Instance._WNRingIDSettings, "Password changed successfully.");
                            UIHelperMethods.ShowMessageWithTimerFromNonThread(null, "Password changed successfully.");
                            DefaultSettings.VALUE_LOGIN_USER_PASSWORD = NewPwdBox.Password;
                            ChatService.UpdateUserPassword(DefaultSettings.VALUE_LOGIN_USER_PASSWORD);
                            new DatabaseActivityDAO().SaveChangedPasswordIntoDB();
                            //exit window
                            RingIDViewModel.Instance._WNRingIDSettings.Close();
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_Msg))
                                UIHelperMethods.ShowFailed(_Msg, "Password chage failed");
                            //CustomMessageBox.ShowError(RingIDViewModel.Instance._WNRingIDSettings, "Failed! " + _Msg);
                            else if ((string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && !DefaultSettings.IsInternetAvailable))
                                UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Password chage failed");

                        }
                        SaveChangesBUtton.IsEnabled = true;
                        CancelButton.IsEnabled = true;
                    }));
                }
                else
                {
                    ErrorMessage = validationMsg;
                }
            }
            else
            {
                ErrorMessage = "Please fill out all fields";
            }
            HideLoader();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            NewPwdBox.Password = "";
            OldPwdBox.Password = "";
            ConfirmNewPwdBox.Password = "";
            ErrorMessage = "";
            RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ViewPassWordPanel.Visibility = Visibility.Visible;
            RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ChangePasswordPanel.Child = null;
        }

        private void OldPwdBoxChanged(object sender, RoutedEventArgs e)
        {
            if (OldPwdBox.Password != null && OldPwdBox.Password.Trim().Length > 0 && NewPwdBox.Password != null && NewPwdBox.Password.Trim().Length > 0 && ConfirmNewPwdBox.Password != null && ConfirmNewPwdBox.Password.Trim().Length > 0)
            {
                ErrorMessage = "";
            }
        }

        private void NewPwdBoxChanged(object sender, RoutedEventArgs e)
        {
            if (OldPwdBox.Password != null && OldPwdBox.Password.Trim().Length > 0 && NewPwdBox.Password != null && NewPwdBox.Password.Trim().Length > 0 && ConfirmNewPwdBox.Password != null && ConfirmNewPwdBox.Password.Trim().Length > 0)
            {
                ErrorMessage = "";
            }
        }

        private void ConfirmNewPwdBoxChanged(object sender, RoutedEventArgs e)
        {
            if (OldPwdBox.Password != null && OldPwdBox.Password.Trim().Length > 0 && NewPwdBox.Password != null && NewPwdBox.Password.Trim().Length > 0 && ConfirmNewPwdBox.Password != null && ConfirmNewPwdBox.Password.Trim().Length > 0)
            {
                ErrorMessage = "";
            }
        }



        public ImageSource pstLoader
        {
            get { return (ImageSource)GetValue(pstLoaderProperty); }
            set { SetValue(pstLoaderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for pstLoader.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty pstLoaderProperty =
            DependencyProperty.Register("pstLoader", typeof(ImageSource), typeof(UCChangePassword), new PropertyMetadata(null));

        private string _ErrorMessage = string.Empty;
        public string ErrorMessage
        {
            get
            {
                return _ErrorMessage;
            }
            set
            {
                _ErrorMessage = value;
                this.OnPropertyChanged("ErrorMessage");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Models.DAO;
using View.ViewModel;
using View.Utility.Recorder.DirectX.Capture;
using Models.Constants;
using System.Collections.Generic;
namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCAudioSettings.xaml
    /// </summary>
    public partial class UCVideoSettings : UserControl, INotifyPropertyChanged
    {

        private WebcamComboBoxItem _PrevSelectedItem;
        private WebcamComboBoxItem _SelectedItem;
        private int _SelectedIndex;
        private bool _HasWebCamError = false;
        private string _ErrorMessage = String.Empty;
        private ObservableCollection<WebcamComboBoxItem> _WebcamList = new ObservableCollection<WebcamComboBoxItem>();

        private VideoCapture capture = null;
        private Filters filters = new Filters();

        public UCVideoSettings()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCVideoSettings_Loaded;
            this.Unloaded += UCVideoSettings_Unloaded;
            InitWebcamDeviceList();
        }

        void UCVideoSettings_Unloaded(object sender, RoutedEventArgs e)
        {
            webcamCombobox.SelectionChanged -= webcamCombobox_SelectionChanged;
            SaveBtn.Click -= SaveBtn_Click;
            CancelBtn.Click -= CancelBtn_Click;
            WebSettings.Click -= btnWebcamSettings_Click;
        }

        void UCVideoSettings_Loaded(object sender, RoutedEventArgs e)
        {
            webcamCombobox.SelectionChanged +=webcamCombobox_SelectionChanged;
            SaveBtn.Click +=SaveBtn_Click;
            CancelBtn.Click +=CancelBtn_Click;
            WebSettings.Click += btnWebcamSettings_Click;
        }

        #region Handler

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID = SelectedItem != null ? SelectedItem.DeviceMonikerString : String.Empty;
            new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_WEBCAM_DEVICE_ID, SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID.ToString());
            RingIDViewModel.Instance._WNRingIDSettings.Close();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            HasWebCamError = false;
            ErrorMessage = String.Empty;
            ShowWebcam();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            CloseWebcam();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance._WNRingIDSettings.Close();
        }

        private void btnWebcamSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (capture != null)
                {
                    capture.PropertyPages[0].Show(WebcamPanel);
                }
            }
            catch
            {
            }
        }

        void webcamCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectedItem != null && (_PrevSelectedItem == null || !_PrevSelectedItem.DeviceMonikerString.Equals(SelectedItem.DeviceMonikerString)))
            {
                CloseWebcam();
                ShowWebcam();
            }
            _PrevSelectedItem = SelectedItem;
        }

        #endregion Handler
        
        #region Utility methods

        private void InitWebcamDeviceList()
        {
            if (filters.VideoInputDevices == null || filters.VideoInputDevices.Count <= 0)
            {
                HasWebCamError = true;
                ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                return;
            }

            int index = 0;
            foreach (Filter endPoint in filters.VideoInputDevices)
            {
                WebcamComboBoxItem comboItem = new WebcamComboBoxItem();
                comboItem.DeviceMonikerString = endPoint.MonikerString;
                comboItem.DeviceName = endPoint.Name;
                comboItem.DeviceIndex = index;
                WebcamList.Add(comboItem);
                index++;
            }

            SetDefaultDevice();
        }

        private void SetDefaultDevice()
        {
            if (!String.IsNullOrWhiteSpace(SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID))
            {
                WebcamComboBoxItem item = WebcamList.Where(P => P.DeviceMonikerString.Equals(SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID)).FirstOrDefault();
                if (item != null)
                {
                    SelectedIndex = WebcamList.IndexOf(item);
                    SelectedItem = item;
                }
            }

            if (SelectedItem == null && WebcamList.Count > 0)
            {
                SelectedIndex = 0;
                SelectedItem = WebcamList.ElementAt(0);
            }
            _PrevSelectedItem = SelectedItem;
        }

        private void ShowWebcam()
        {
            try
            {
                if (filters.VideoInputDevices == null || filters.VideoInputDevices.Count <= 0)
                {
                    HasWebCamError = true;
                    ErrorMessage = NotificationMessages.WEBCAM_NOT_FOUND;
                    return;
                }

                capture = new VideoCapture(filters.VideoInputDevices[SelectedIndex], null);
                capture.PreviewWindow = WebcamPanel;
                capture.CheckWebcam();
            }
            catch
            {
                HasWebCamError = true;
                ErrorMessage = NotificationMessages.WEBCAM_ALREADY_USING;
            }
        }

        private void CloseWebcam()
        {
            try
            {
                if (capture != null)
                {
                    capture.Dispose();
                    capture = null;
                }

                GC.SuppressFinalize(this);
            }
            catch
            {
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Utility methods

        #region Property

        public bool HasWebCamError
        {
            get
            {
                return _HasWebCamError;
            }
            set
            {
                _HasWebCamError = value;
                this.OnPropertyChanged("HasWebCamError");
            }
        }

        public string ErrorMessage
        {
            get
            {
                return _ErrorMessage;
            }
            set
            {
                _ErrorMessage = value;
                this.OnPropertyChanged("ErrorMessage");
            }
        }

        public ObservableCollection<WebcamComboBoxItem> WebcamList
        {
            get { return _WebcamList; }
            set
            {
                _WebcamList = value;
                this.OnPropertyChanged("WebcamList");
            }
        }

        
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set
            {
                _SelectedIndex = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        
        public WebcamComboBoxItem SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                _SelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        #endregion Property

        #region Combobox Item

        public class WebcamComboBoxItem
        {
            public int DeviceIndex { set; get; }
            public string DeviceName { set; get; }
            public string DeviceMonikerString { set; get; }
        }

        #endregion Combobox Item


    }
}

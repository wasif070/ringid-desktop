﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCAccountSettings.xaml
    /// </summary>
    public partial class UCAccountSettings : UserControl
    {
        public UCChangePassword ucChangePassword;
        public UCRecoverySettings ucRecoverySettings;
        private Grid motherGrid;

        public UCAccountSettings(Grid motherGrid1)
        {
            InitializeComponent();
            this.motherGrid = motherGrid1;
            LoadPanel();
        }

        private void LoadPanel()
        {
            ucRecoverySettings = new UCRecoverySettings(this.motherGrid);
            RecoverySettingsPanel.Child = ucRecoverySettings;
        }

        private void ChangePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            ViewPassWordPanel.Visibility = Visibility.Collapsed;
            if (ucChangePassword == null) ucChangePassword = new UCChangePassword();
            ChangePasswordPanel.Child = ucChangePassword;
        }

        private void ViewPassWordPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            ChangePasswordButton.Visibility = Visibility.Visible;
        }

        private void ViewPassWordPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            ChangePasswordButton.Visibility = Visibility.Collapsed;
        }
    }
}

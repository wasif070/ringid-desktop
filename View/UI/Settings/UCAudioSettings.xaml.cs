﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using Models.DAO;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using View.Utility;
using View.Utility.audio;
using View.Utility.Call;
using View.Utility.Naudio;
using View.ViewModel;
using System.Collections.Generic;
namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCAudioSettings.xaml
    /// </summary>
    public partial class UCAudioSettings : UserControl, INotifyPropertyChanged
    {
        #region "Private Fields"
        private WasapiCapture capture;
        private float peak;
        private readonly SynchronizationContext synchronizationContext;
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion "Private Fields"

        #region "Constructors"
        public UCAudioSettings()
        {
            InitializeComponent();
            initAudioDeviceList();
            synchronizationContext = SynchronizationContext.Current;
            this.DataContext = this;
            this.IsVisibleChanged += UCAudioSettings_IsVisibleChanged;
        }

        #endregion "Constructors"

        #region"Properties"

        AudioPlayerRecorder PCMDataPlayerAndRecorder
        {
            get
            {
                return MainSwitcher.CallController.PCMDataPlayerAndRecorder;
            }
        }

        public ObservableCollection<AudioComboboxItem> MicrophoneList
        {
            get;
            set;
        }

        public ObservableCollection<AudioComboboxItem> SpeakerList
        {
            get;
            set;
        }

        private int _selectedMicroPhoneIndex;
        public int SelectedMicroPhoneIndex
        {
            get
            {
                return _selectedMicroPhoneIndex;
            }
            set
            {
                _selectedMicroPhoneIndex = value;
            }
        }

        private int _selectedSpeakerIndex;
        public int SelectedSpeakerIndex
        {
            get
            {
                return _selectedSpeakerIndex;
            }
            set
            {
                _selectedSpeakerIndex = value;
            }
        }

        private AudioComboboxItem _selectedMircoPhoneComboboxItem;
        public AudioComboboxItem SelectedMircoPhoneComboboxItem
        {
            get
            {
                return _selectedMircoPhoneComboboxItem;
            }
            set
            {
                _selectedMircoPhoneComboboxItem = value;
                OnPropertyChanged("SelectedMircoPhoneComboboxItem");
                _selectedMircoPhoneComboboxItem.MmDevice.AudioEndpointVolume.MasterVolumeLevelScalar = 1F;
                SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER = _selectedMircoPhoneComboboxItem.DeviceIndex;
                if (PCMDataPlayerAndRecorder != null)
                {
                    string msg = PCMDataPlayerAndRecorder.RestartPlayerRecorder();
                }
            }
        }

        private AudioComboboxItem _selectedSpeakerComboboxItem;
        public AudioComboboxItem SelectedSpeakerComboboxItem
        {
            get
            {
                return _selectedSpeakerComboboxItem;
            }
            set
            {
                _selectedSpeakerComboboxItem = value;
                OnPropertyChanged("SelectedSpeakerComboboxItem");
                SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER = _selectedSpeakerComboboxItem.DeviceIndex;
                if (PCMDataPlayerAndRecorder != null)
                {
                    string msg = PCMDataPlayerAndRecorder.RestartPlayerRecorder();
                }
            }
        }

        private float _speakerPeakLabel;
        public float SpeakerPeakLabel
        {
            get
            {
                return _speakerPeakLabel;
            }
            set
            {
                if (_speakerPeakLabel != value)
                {
                    _speakerPeakLabel = value;
                    OnPropertyChanged("SpeakerPeakLabel");
                    //  SelectedSpeakerComboboxItem.MmDevice.AudioEndpointVolume.MasterVolumeLevelScalar = value;
                    SettingsConstants.VALUE_RINGID_SPEAKER_VOLUME = value;
                    SettingsConstants.VALUE_RINGID_SPEAKER_VOLUME = (float)System.Math.Round(SettingsConstants.VALUE_RINGID_SPEAKER_VOLUME, 2);
                }
            }
        }

        public float Peak
        {
            get
            {
                return peak;
            }
            set
            {
                if (peak != value)
                {
                    peak = value;
                    OnPropertyChanged("Peak");
                }
            }
        }

        #endregion

        #region Event Trigger
        void UCAudioSettings_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            UserControl user = (UserControl)sender;
            if ((bool)e.NewValue == true)
            {
                StartRecording();
                SaveBtn.Click += SaveBtn_Click;
                CancelBtn.Click += CancelBtn_Click;
                SpekaerTest.Click += SpekaerTest_Click;
            }
            else
            {
                if (AudioFilesAndSettings.SepeakerTestTune != null)
                {
                    AudioFilesAndSettings.SepeakerTestTune.Stop();
                }
                StopRecording();
                SaveBtn.Click -= SaveBtn_Click;
                CancelBtn.Click -= CancelBtn_Click;
                SpekaerTest.Click -= SpekaerTest_Click;
            }
        }

        void SpekaerTest_Click(object sender, RoutedEventArgs e)
        {
            AudioFilesAndSettings.Play(AudioFilesAndSettings.SPEAKER_TEST_TUNE);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance._WNRingIDSettings.Close();
        }
        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, string> temDictionary = new Dictionary<string, string>();
            temDictionary[SettingsConstants.KEY_RINGID_RECORDER_DEVICE_NUMBER] = SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER.ToString();
            temDictionary[SettingsConstants.KEY_RINGID_PLAYER_DEVICE_NUMBER] = SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER.ToString();
            temDictionary[SettingsConstants.KEY_RINGID_SPEAKER_VOLUME] = SettingsConstants.VALUE_RINGID_SPEAKER_VOLUME.ToString();
            new InsertIntoAllRingUsersSettings().StartProcess(temDictionary);

            RingIDViewModel.Instance._WNRingIDSettings.Close();
        }
        private void OnRecordingStopped(object sender, StoppedEventArgs e)
        {
            capture.Dispose();
            capture = null;
        }
        #endregion Event Trigger

        #region "Public Methods"

        #endregion "Public methods"

        #region Private methods

        private void initAudioDeviceList()
        {
            MicrophoneList = new ObservableCollection<AudioComboboxItem>();
            var enumerator = new MMDeviceEnumerator();
            var endPoints = enumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active);
            int mircophnIndex = 0;
            foreach (var endPoint in endPoints)
            {
                var comboItem = new AudioComboboxItem();
                comboItem.DevieFriendLyName = endPoint.FriendlyName;
                comboItem.DeviceIndex = mircophnIndex;
                comboItem.MmDevice = endPoint;
                MicrophoneList.Add(comboItem);
                mircophnIndex++;
            }

            SpeakerList = new ObservableCollection<AudioComboboxItem>();
            var spekers = enumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active);
            int spekerIndex = 0;
            foreach (var spkr in spekers)
            {
                var comboItem = new AudioComboboxItem();
                comboItem.DevieFriendLyName = spkr.FriendlyName;
                comboItem.DeviceIndex = spekerIndex;
                comboItem.MmDevice = spkr;
                SpeakerList.Add(comboItem);
                spekerIndex++;
            }
            setDefaultDevice();
        }
        private void setDefaultDevice()
        {
            if (MicrophoneList.Count > 0)
            {
                if (MicrophoneList.Count > 1 && SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER >= 0 && MicrophoneList.Count > SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER)
                {
                    SelectedMircoPhoneComboboxItem = MicrophoneList[SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER];
                }
                else
                {
                    SelectedMircoPhoneComboboxItem = MicrophoneList[0];
                }
            }
            if (SpeakerList.Count > 0)
            {
                if (SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER >= 0 && SpeakerList.Count > 1 && SpeakerList.Count > SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER)
                {
                    SelectedSpeakerComboboxItem = SpeakerList[SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER];
                }
                else
                {
                    SelectedSpeakerComboboxItem = SpeakerList[0];
                }
                SpeakerPeakLabel = SettingsConstants.VALUE_RINGID_SPEAKER_VOLUME;
            }
        }
        private void StartRecording()
        {
            if (SelectedMircoPhoneComboboxItem != null)
            {

                try
                {
                    capture = new WasapiCapture(SelectedMircoPhoneComboboxItem.MmDevice);
                    capture.ShareMode = AudioClientShareMode.Shared;
                    capture.StartRecording();
                    capture.RecordingStopped += OnRecordingStopped;
                    capture.DataAvailable += CaptureOnDataAvailable;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message + "\n" + e.StackTrace);
                }
            }
        }
        private void StopRecording()
        {
            if (capture != null)
            {
                capture.StopRecording();
            }
        }
        private void CaptureOnDataAvailable(object sender, WaveInEventArgs waveInEventArgs)
        {
            UpdateMicroPhonePeakMeter();
        }
        private void UpdateMicroPhonePeakMeter()
        {
            synchronizationContext.Post(s => Peak = SelectedMircoPhoneComboboxItem.MmDevice.AudioMeterInformation
                .MasterPeakValue, null);
        }

        #endregion Private methods

        #region "Combobox Item"
        public class AudioComboboxItem
        {
            public int DeviceIndex
            {
                set;
                get;
            }
            public string DevieFriendLyName
            {
                set;
                get;
            }
            public MMDevice MmDevice
            {
                set;
                get;
            }
        }
        #endregion
    }

}

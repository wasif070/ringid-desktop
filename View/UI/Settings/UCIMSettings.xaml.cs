﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Dictonary;
using View.UI.Chat;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.Utility;
using View.Utility.Chat.Service;
using View.Utility.FriendList;
using View.Utility.Myprofile;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCIMSettings.xaml
    /// </summary>
    public partial class UCIMSettings : UserControl, INotifyPropertyChanged
    {
        // public static UCIMSettings Instance;

        public UCIMSettings()
        {
            // Instance = this;
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCIMSettings_Loaded;
            this.Unloaded += UCIMSettings_Unloaded;
        }

        #region Property
        private bool _IsEnableImSound;
        public bool IsEnableImSound
        {
            get { return _IsEnableImSound; }
            set
            {
                if (_IsEnableImSound == value)
                {
                    return;
                }

                _IsEnableImSound = value;
                this.OnPropertyChanged("IsEnableImSound");
            }
        }

        private bool _IsEnableImNotification;
        public bool IsEnableImNotification
        {
            get { return _IsEnableImNotification; }
            set
            {
                if (_IsEnableImNotification == value)
                {
                    return;
                }
                _IsEnableImNotification = value;
                this.OnPropertyChanged("IsEnableImNotification");
            }
        }

        private bool _IsEnableImPopUp;
        public bool IsEnableImPopUp
        {
            get { return _IsEnableImPopUp; }
            set
            {
                if (_IsEnableImPopUp == value)
                {
                    return;
                }
                _IsEnableImPopUp = value;
                this.OnPropertyChanged("IsEnableImPopUp");
            }
        }

        private bool _IsEnableImAutoDownload;
        public bool IsEnableImAutoDownload
        {
            get { return _IsEnableImAutoDownload; }
            set
            {
                if (_IsEnableImAutoDownload == value)
                {
                    return;
                }
                _IsEnableImAutoDownload = value;
                this.OnPropertyChanged("IsEnableImAutoDownload");
            }
        }

        private bool _SeparateChatView;
        public bool SeparateChatView
        {
            get
            {
                return _SeparateChatView;
            }
            set
            {
                if (_SeparateChatView == value)
                {
                    return;
                }

                _SeparateChatView = value;
                this.OnPropertyChanged("SeparateChatView");
            }
        }

        private bool _IsStickerViewOpen;
        public bool IsStickerViewOpen
        {
            get
            {
                return _IsStickerViewOpen;
            }
            set
            {
                if (_IsStickerViewOpen == value)
                {
                    return;
                }

                _IsStickerViewOpen = value;
                this.OnPropertyChanged("IsStickerViewOpen");
            }
        }

        #endregion

        #region Utility

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void UCIMSettings_Unloaded(object sender, RoutedEventArgs e)
        {
            CbIMSound.Click -= CbImSound_Click;
            CbIMNotification.Click -= CbIMNotification_Click;
            CbIMPopUp.Click += CbIMPopUp_Click;
            CbIMAutoDownload.Click -= CbIMAutoDownload_Click;
            EveryoneCheckbox.Click -= chkAllowIncomingChat_Click;
            FriendsOnlyCheckbox.Click -= chkAllowIncomingChat_Click;
            chkSeparateChatView.Click -= chkSeparateChatView_Click;
            chkStickerViewingOption.Click -= chkStickerViewingOption_Click;
        }
        private void UCIMSettings_Loaded(object sender, RoutedEventArgs e)
        {
            IsEnableImSound = SettingsConstants.VALUE_RINGID_IM_SOUND;
            IsEnableImNotification = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
            IsEnableImPopUp = SettingsConstants.VALUE_RINGID_IM_POPUP;
            IsEnableImAutoDownload = SettingsConstants.VALUE_RINGID_IM_AUTO_DOWNLOAD;
            SeparateChatView = SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW;
            IsStickerViewOpen = SettingsConstants.VALUE_RINGID_STICKER_VIEW;

            CbIMSound.Click += CbImSound_Click;
            CbIMNotification.Click += CbIMNotification_Click;
            CbIMPopUp.Click += CbIMPopUp_Click;
            CbIMAutoDownload.Click += CbIMAutoDownload_Click;
            EveryoneCheckbox.Click += chkAllowIncomingChat_Click;
            FriendsOnlyCheckbox.Click += chkAllowIncomingChat_Click;
            chkSeparateChatView.Click += chkSeparateChatView_Click;
            chkStickerViewingOption.Click += chkStickerViewingOption_Click;
        }

        private void OnImSoundSettingsClick()
        {
            try
            {
                //ContactList
                FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values.ToList().ForEach(P =>
                  {
                      P.ShortInfoModel.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
                  });

                List<UserBasicInfoDTO> basicDtoList = FriendDictionaries.Instance.GetUserBasicInfoDictionaryAsList();
                basicDtoList.ForEach(P =>
                    {
                        P.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
                    });

                new InsertIntoUserBasicInfoTable(basicDtoList).Start();

                // Group 
                List<GroupInfoModel> grpList = RingIDViewModel.Instance.GroupList.Values.ToList();
                foreach (GroupInfoModel model in grpList)
                {
                    model.ImSoundEnabled = IsEnableImSound;
                    GroupInfoDAO.UpdateIMSound(model.GroupID, IsEnableImSound);
                }

                //Room
                List<RoomModel> rmList = RingIDViewModel.Instance.RoomList.Values.ToList();
                foreach (RoomModel model in rmList)
                {
                    model.ImSoundEnabled = IsEnableImSound;
                    RoomDAO.UpdateIMSound(model.RoomID, IsEnableImSound);
                }

                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_IM_SOUND, SettingsConstants.VALUE_RINGID_IM_SOUND.ToString());
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCIMSettings).Name).Error(ex.Message + ex.StackTrace);
            }
        }

        private void OnImNotificationSettingsClick()
        {
            try
            {
                //ContactList
                FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values.ToList().ForEach(P =>
                   {
                       P.ShortInfoModel.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
                   });

                List<UserBasicInfoDTO> basicDtoList = FriendDictionaries.Instance.GetUserBasicInfoDictionaryAsList();
                basicDtoList.ForEach(P =>
                {
                    P.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
                });
                new InsertIntoUserBasicInfoTable(basicDtoList).Start();

                // Group 
                List<GroupInfoModel> grpList = RingIDViewModel.Instance.GroupList.Values.ToList();
                foreach (GroupInfoModel model in grpList)
                {
                    model.ImNotificationEnabled = IsEnableImNotification;
                    GroupInfoDAO.UpdateIMNotification(model.GroupID, IsEnableImNotification);
                }

                //Room
                List<RoomModel> rmList = RingIDViewModel.Instance.RoomList.Values.ToList();
                foreach (RoomModel model in rmList)
                {
                    model.ImNotificationEnabled = IsEnableImNotification;
                    RoomDAO.UpdateIMNotification(model.RoomID, IsEnableImNotification);
                }

                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_IM_NOTIFICATION, SettingsConstants.VALUE_RINGID_IM_NOTIFICATION.ToString());
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCIMSettings).Name).Error(ex.Message + ex.StackTrace);
            }
        }

        private void OnImPopUpSettingsClick()
        {
            try
            {
                //ContactList
                FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values.ToList().ForEach(P =>
                  {
                      P.ShortInfoModel.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
                  });
                List<UserBasicInfoDTO> basicDtoList = FriendDictionaries.Instance.GetUserBasicInfoDictionaryAsList();
                basicDtoList.ForEach(P =>
                {
                    P.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
                });
                new InsertIntoUserBasicInfoTable(basicDtoList).Start();

                // Group 
                List<GroupInfoModel> grpList = RingIDViewModel.Instance.GroupList.Values.ToList();
                foreach (GroupInfoModel model in grpList)
                {
                    model.ImPopupEnabled = IsEnableImPopUp;
                    GroupInfoDAO.UpdateIMPopUp(model.GroupID, IsEnableImPopUp);
                }

                //Room
                List<RoomModel> rmList = RingIDViewModel.Instance.RoomList.Values.ToList();
                foreach (RoomModel model in rmList)
                {
                    model.ImPopupEnabled = IsEnableImPopUp;
                    RoomDAO.UpdateIMPopUp(model.RoomID, IsEnableImPopUp);
                }

                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_IM_POPUP, SettingsConstants.VALUE_RINGID_IM_POPUP.ToString());
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCIMSettings).Name).Error(ex.Message + ex.StackTrace);
            }
        }

        #endregion

        #region EventHandler

        private void CbImSound_Click(object sender, RoutedEventArgs e)
        {
            SettingsConstants.VALUE_RINGID_IM_SOUND = !SettingsConstants.VALUE_RINGID_IM_SOUND;
            IsEnableImSound = SettingsConstants.VALUE_RINGID_IM_SOUND;
            OnImSoundSettingsClick();
        }

        private void CbIMNotification_Click(object sender, RoutedEventArgs e)
        {
            SettingsConstants.VALUE_RINGID_IM_NOTIFICATION = !SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
            IsEnableImNotification = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
            OnImNotificationSettingsClick();
        }

        private void CbIMPopUp_Click(object sender, RoutedEventArgs e)
        {
            SettingsConstants.VALUE_RINGID_IM_POPUP = !SettingsConstants.VALUE_RINGID_IM_POPUP;
            IsEnableImPopUp = SettingsConstants.VALUE_RINGID_IM_POPUP;
            OnImPopUpSettingsClick();
        }

        private void CbIMAutoDownload_Click(object sender, RoutedEventArgs e)
        {
            SettingsConstants.VALUE_RINGID_IM_AUTO_DOWNLOAD = !SettingsConstants.VALUE_RINGID_IM_AUTO_DOWNLOAD;
            IsEnableImAutoDownload = SettingsConstants.VALUE_RINGID_IM_AUTO_DOWNLOAD;
            new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_IM_AUTO_DOWNLOAD, SettingsConstants.VALUE_RINGID_IM_AUTO_DOWNLOAD.ToString());
        }

        private void chkSeparateChatView_Click(object sender, RoutedEventArgs e)
        {
            SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW = !SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW;
            SeparateChatView = SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW;
            new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_SEPARATE_CHAT_VIEW, SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW.ToString());
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW)
                {
                    MiddlePanelSwitcher.pageSwitcher.ClearChatVisitedData();

                    if (UCMiddlePanelSwitcher.View_UCFriendChatCallPanel != null && UCMiddlePanelSwitcher.View_UCFriendChatCallPanel.IsVisible)
                    {
                        RingIDViewModel.Instance.OnFriendCallChatButtonClicked(UCMiddlePanelSwitcher.View_UCFriendChatCallPanel._FriendTableID);
                        MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
                    }
                    else
                    {
                        UCMiddlePanelSwitcher.View_UCFriendChatCallPanel = null;
                    }

                    if (UCMiddlePanelSwitcher.View_UCGroupChatCallPanel != null && UCMiddlePanelSwitcher.View_UCGroupChatCallPanel.IsVisible)
                    {
                        RingIDViewModel.Instance.OnGroupCallChatButtonClicked(UCMiddlePanelSwitcher.View_UCGroupChatCallPanel._GroupID);
                        MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
                    }
                    else
                    {
                        UCMiddlePanelSwitcher.View_UCGroupChatCallPanel = null;
                    }
                }
                else
                {
                    List<UCFriendChatCallPanel> friendChatCallPanelList = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.Values.Where(P => P.Parent != null && P.Parent is WNChatView).ToList();
                    foreach (UCFriendChatCallPanel friendChatCallPanel in friendChatCallPanelList)
                    {
                        if (UCChatLogPanel.Instance != null && UCChatLogPanel.Instance.SelectedContactID.Equals(friendChatCallPanel._FriendTableID.ToString()))
                        {
                            ((WNChatView)friendChatCallPanel.Parent).Close();
                            RingIDViewModel.Instance.OnFriendCallChatButtonClicked(friendChatCallPanel._FriendTableID);
                        }
                        else
                        {
                            ((WNChatView)friendChatCallPanel.Parent).Close();
                        }
                    }

                    List<UCGroupChatCallPanel> groupChatCallPanelList = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.Values.Where(P => P.Parent != null && P.Parent is WNChatView).ToList();
                    foreach (UCGroupChatCallPanel groupChatCallPanel in groupChatCallPanelList)
                    {
                        if (UCChatLogPanel.Instance != null && UCChatLogPanel.Instance.SelectedContactID.Equals(groupChatCallPanel._GroupID.ToString()))
                        {
                            ((WNChatView)groupChatCallPanel.Parent).Close();
                            RingIDViewModel.Instance.OnFriendCallChatButtonClicked(groupChatCallPanel._GroupID);
                        }
                        else
                        {
                            ((WNChatView)groupChatCallPanel.Parent).Close();
                        }
                    }
                }
            }, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
        }

        private void chkStickerViewingOption_Click(object sender, RoutedEventArgs e)
        {
            SettingsConstants.VALUE_RINGID_STICKER_VIEW = !SettingsConstants.VALUE_RINGID_STICKER_VIEW;
            IsStickerViewOpen = SettingsConstants.VALUE_RINGID_STICKER_VIEW;
            if (_IsStickerViewOpen == false)
            {
                RingIDViewModel.Instance.CollapseCommand.Execute(null);
            }
            else
            {
                RingIDViewModel.Instance.ExpandCommand.Execute(null);
            }

            new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_STICKER_VIEW, SettingsConstants.VALUE_RINGID_STICKER_VIEW.ToString());
        }

        private void chkAllowIncomingChat_Click(object sender, RoutedEventArgs e)
        {
            int prevValue = RingIDViewModel.Instance.MyBasicInfoModel.AnonymousChatAccess;
            int currValue = prevValue == StatusConstants.CHAT_CALL_EVERYONE ? StatusConstants.CHAT_CALL_FRIENDS_ONLY : StatusConstants.CHAT_CALL_EVERYONE;
            RingIDViewModel.Instance.MyBasicInfoModel.AnonymousChatAccess = currValue;
            DefaultSettings.userProfile.AnonymousChatAccess = currValue;

            EveryoneCheckbox.IsEnabled = false;
            FriendsOnlyCheckbox.IsEnabled = false;

            ThradChangeAccessSettings request = new ThradChangeAccessSettings(StatusConstants.ANONYMOUS_CHAT, currValue);
            request.StartThread();
            request.OnComplete += (status, erroMessage) =>
            {
                if (!status)
                {
                    RingIDViewModel.Instance.MyBasicInfoModel.AnonymousChatAccess = prevValue;
                    DefaultSettings.userProfile.AnonymousChatAccess = prevValue;
                    UIHelperMethods.ShowFailed(erroMessage);
                    // CustomMessageBox.ShowError(erroMessage, "Failed!");
                }
                else if (RingIDViewModel.Instance.MyBasicInfoModel.AnonymousChatAccess == StatusConstants.CHAT_CALL_FRIENDS_ONLY)
                {
                    List<long> friendIds = ChatService.GetFriendRegisterList();
                    foreach (long friendId in friendIds)
                    {
                        UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(friendId);
                        if (model != null && model.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        {
                            ChatService.UnRegisterFriendChat(friendId, StatusConstants.PRESENCE_ONLINE, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.Mood);
                        }
                    }
                }

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    EveryoneCheckbox.IsEnabled = true;
                    FriendsOnlyCheckbox.IsEnabled = true;
                }, System.Windows.Threading.DispatcherPriority.Send);
            };
        }

        #endregion
    }
}

﻿using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.UI.Feed;
using View.Utility.FriendList;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCGeneralSettings.xaml
    /// </summary>
    public partial class UCGeneralSettings : UserControl, INotifyPropertyChanged
    {
        private UCRingSettings _UCRingSettings;
        public UCGeneralSettings()
        {
            InitializeComponent();
            this.DataContext = this;
            LoadPanel();
            SetSelectedModelValuesForNotificationValidityPopup();
            SetSelectedModelValuesForFeedValidityPopup();
        }
        private void LoadPanel()
        {
            _UCRingSettings = new UCRingSettings();
            RingSettingsPanel.Child = _UCRingSettings;
        }

        public void LoadCheckBoxValues()
        {
            IsEnableAlertSound = SettingsConstants.VALUE_RINGID_ALERT_SOUND;
        }

        private bool _IsEnableAlertSound;
        public bool IsEnableAlertSound
        {
            get { return _IsEnableAlertSound; }
            set
            {
                _IsEnableAlertSound = value;
                OnPropertyChanged("IsEnableAlertSound");
            }
        }
        private string _NotificationValidityText;
        public string NotificationValidityText
        {
            get { return _NotificationValidityText; }
            set
            {
                _NotificationValidityText = value;
                OnPropertyChanged("NotificationValidityText");
            }
        }
        private int _NotificationValidityVal;
        public int NotificationValidityVal
        {
            get { return _NotificationValidityVal; }
            set
            {
                _NotificationValidityVal = value;
                this.OnPropertyChanged("NotificationValidityVal");
            }
        }
        private string _FeedValidityText = AppConstants.UNLIMITED_VALIDITY_STRING;
        public string FeedValidityText
        {
            get { return _FeedValidityText; }
            set
            {
                _FeedValidityText = value;
                OnPropertyChanged("FeedValidityText");
            }
        }
        private int _FeedValidityVal = -1;
        public int FeedValidityVal
        {
            get { return _FeedValidityVal; }
            set
            {
                _FeedValidityVal = value;
                this.OnPropertyChanged("FeedValidityVal");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void CbAlertSound_Click(object sender, RoutedEventArgs e)
        {
            SettingsConstants.VALUE_RINGID_ALERT_SOUND = !SettingsConstants.VALUE_RINGID_ALERT_SOUND;
            IsEnableAlertSound = SettingsConstants.VALUE_RINGID_ALERT_SOUND;
            new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_ALERT_SOUND, SettingsConstants.VALUE_RINGID_ALERT_SOUND.ToString());
        }

        private void notificationValidityBtn_Click(object sender, RoutedEventArgs e)
        {
            ucValidityPopup.Show(notificationValidityBtn, this);
        }

        private void feedValidityBtn_Click(object sender, RoutedEventArgs e)
        {
            ucValidityPopup.Show(feedValidityBtn, this, true);
        }

        public void SetSelectedModelValuesForFeedValidityPopup()
        {
            if (FeedValidityVal != SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY)
            {
                FeedValidityVal = SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY;
                if (FeedValidityVal == AppConstants.DEFAULT_VALIDITY)
                {
                    FeedValidityText = AppConstants.UNLIMITED_VALIDITY_STRING;
                }
                else if (FeedValidityVal == 1)
                {
                    FeedValidityText = FeedValidityVal + " Day";
                }
                else
                {
                    FeedValidityText = FeedValidityVal + " Days";
                }
            }
        }
        public void SetSelectedModelValuesForNotificationValidityPopup()
        {
            NotificationValidityVal = DefaultSettings.userProfile.NotificationValidity;
            if (NotificationValidityVal == 1)
            {
                NotificationValidityText = NotificationValidityVal + " Day";
            }
            else
            {
                NotificationValidityText = NotificationValidityVal + " Days";
            }
        }
    }
}

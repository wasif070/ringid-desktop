﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Models.DAO;
using Models.Entity;
using View.BindingModels;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCCustomizeMessageSettings.xaml
    /// </summary>
    public partial class UCCustomizeMessageSettings : UserControl, INotifyPropertyChanged
    {
        public static UCCustomizeMessageSettings Instance;
        private IList<InstantMessageDTO> MyInstantMsgsList;
        private ObservableCollection<InstantMsgModel> _MyInstantMsgModelList = new ObservableCollection<InstantMsgModel>();
        public ObservableCollection<InstantMsgModel> MyInstantMsgModelList
        {
            get
            {
                return _MyInstantMsgModelList;
            }
            set
            {
                _MyInstantMsgModelList = value;
                this.OnPropertyChanged("MyInstantMsgModelList");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public UCCustomizeMessageSettings()
        {
            Instance = this;
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCCustomizeMessageSettings_Loaded;
            this.Unloaded += UCCustomizeMessageSettings_Unloaded;
            LoadInstantMsgs();
        }

        void UCCustomizeMessageSettings_Unloaded(object sender, RoutedEventArgs e)
        {
            AddButton.Click -= AddButton_Click;
            Scroll.PreviewKeyDown -= Scroll_PreviewKeyDown;
        }
        void UCCustomizeMessageSettings_Loaded(object sender, RoutedEventArgs e)
        {
            AddButton.Click += AddButton_Click;
            Scroll.PreviewKeyDown +=Scroll_PreviewKeyDown;
        }
        
        private void LoadInstantMsgs()
        {
            MyInstantMsgsList = InstantMessagesDAO.Instance.GetInstantMessagesFromDB();
            foreach (InstantMessageDTO inMsg in MyInstantMsgsList)
            {
                InstantMsgModel instantMsgModel = new InstantMsgModel();
                instantMsgModel.Onload(inMsg);
                _MyInstantMsgModelList.Add(instantMsgModel);
            }
            //InstantMsgModel m = new InstantMsgModel { InstantMessage = "Hello here", VisEditDelete = Visibility.Visible };
            //_MyInstantMsgModelList.Add(m);
            //InstantMsgModel m2 = new InstantMsgModel { InstantMessage = "semflnsj ", VisEditDelete = Visibility.Visible };
            //_MyInstantMsgModelList.Add(m2);
            //InstantMsgModel m3 = new InstantMsgModel { InstantMessage = "meshdsh32", VisEditDelete = Visibility.Visible };
            //_MyInstantMsgModelList.Add(m3);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            AddButton.Visibility = Visibility.Collapsed;
            AddInstantMessagePanel.Visibility = Visibility.Visible;
            txtBoxInstantMsg.Focus();
        }
        private bool isMsgExisting(string str)
        {
            IList<InstantMessageDTO> tempList = InstantMessagesDAO.Instance.GetInstantMessagesFromDB();
            foreach (InstantMessageDTO im in tempList)
            {
                if (im.InstantMessage.Equals(str))
                {
                    return true;
                }
            }
            return false;
        }

        private void OkMiniBtn_Click(object sender, RoutedEventArgs e)
        {
            errorLbl.Content = "";
            string str = txtBoxInstantMsg.Text.Trim();
            bool isEmpty = (str.Length < 1);
            bool isLarge = (str.Length > 100);
            bool isExisting = isMsgExisting(str);

            if (!isEmpty && !isExisting && !isLarge)
            {
                InstantMessageDTO temp = new InstantMessageDTO { InstantMessage = str, MsgType = 1 };
                InstantMessagesDAO.Instance.InsertSingleInstantMessagetoDB(temp);
                InstantMsgModel instantMsgModel = new InstantMsgModel();
                instantMsgModel.Onload(temp);
                _MyInstantMsgModelList.Add(instantMsgModel);
                if (_MyInstantMsgModelList.Count >= 20)
                    AddButton.Visibility = Visibility.Collapsed;
                else
                    AddButton.Visibility = Visibility.Visible;
                InstantMsgModel prevlastModel = UCCustomizeMessageSettings.Instance.MyInstantMsgModelList.ElementAt(UCCustomizeMessageSettings.Instance.MyInstantMsgModelList.Count - 2);
                prevlastModel.OnPropertyChangedNotify("CurrentInstance");
                txtBoxInstantMsg.Text = "";
                AddInstantMessagePanel.Visibility = Visibility.Collapsed;
                errorLbl.Content = "";
                Scroll.ScrollToBottom();
            }
            else
            {
                string msg = null;
                if (isEmpty)
                {
                    msg = "Message Cannot be empty";
                }
                else if (isExisting)
                {
                    msg = "Message already Exists";
                }
                else if (isLarge)
                {
                    msg = "Message can be maximum 100 characters";
                }
                errorLbl.Content = msg;
            }
            //
        }

        private void EditCancelMini_Click(object sender, RoutedEventArgs e)
        {
            //
            errorLbl.Content = "";
            txtBoxInstantMsg.Text = "";
            if (_MyInstantMsgModelList.Count >= 20)
                AddButton.Visibility = Visibility.Collapsed;
            else
                AddButton.Visibility = Visibility.Visible;
            AddInstantMessagePanel.Visibility = Visibility.Collapsed;
        }

        private void delete_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            Control control = (Control)sender;
            InstantMsgModel instantMsgModel = (InstantMsgModel)control.DataContext;
            _MyInstantMsgModelList.Remove(instantMsgModel);
            if (_MyInstantMsgModelList.Count >= 20)
                AddButton.Visibility = Visibility.Collapsed;
            else
                AddButton.Visibility = Visibility.Visible;
            InstantMsgModel lastModel = UCCustomizeMessageSettings.Instance.MyInstantMsgModelList.LastOrDefault();
            lastModel.OnPropertyChangedNotify("CurrentInstance");

            InstantMessageDTO temp = new InstantMessageDTO { InstantMessage = instantMsgModel.InstantMessage, MsgType = 1 };
            InstantMessagesDAO.Instance.DeleteSingleInstantMessageFromDB(temp);
        }

        private void Scroll_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }
    }
}

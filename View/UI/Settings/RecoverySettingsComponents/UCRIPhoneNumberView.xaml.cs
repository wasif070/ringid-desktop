﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Settings.RecoverySettingsComponents
{
    /// <summary>
    /// Interaction logic for UCRIPhoneNumberView.xaml
    /// </summary>
    public partial class UCRIPhoneNumberView : UserControl, INotifyPropertyChanged
    {
        private string _FullMobileNumber = string.Empty;
        public string FullMobileNumber { get { return _FullMobileNumber; } set { _FullMobileNumber = value; this.OnPropertyChanged("FullMobileNumber"); } }

        private int _IsMobileNumberVerified = 0;
        public int IsMobileNumberVerified { get { return _IsMobileNumberVerified; } set { _IsMobileNumberVerified = value; this.OnPropertyChanged("IsMobileNumberVerified"); } }
        
        public UCRIPhoneNumberView(int IsMobileNumberVerified = 0, string MobileDialingCode = "", string MobileNumber = "")
        {
            InitializeComponent();
            this.DataContext = this;
            this.IsMobileNumberVerified = IsMobileNumberVerified;
            this.FullMobileNumber = !string.IsNullOrEmpty(MobileDialingCode) && !string.IsNullOrEmpty(MobileNumber) ? string.Format("{0}-{1}", MobileDialingCode, MobileNumber) : NotificationMessages.ABOUT_NOT_AVAILABLE;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

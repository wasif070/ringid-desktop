﻿using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using Models.Utility;
using View.Utility;
using View.Utility.Services;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Settings.RecoverySettingsComponents
{
    /// <summary>
    /// Interaction logic for UCRIEmailEdit.xaml
    /// </summary>
    public partial class UCRIEmailEdit : UserControl, INotifyPropertyChanged
    {
        #region
        private Grid motherGrid;
        #endregion

        #region "Properties"
        private string _EmailAddress = string.Empty;
        public string EmailAddress { get { return _EmailAddress; } set { _EmailAddress = value; this.OnPropertyChanged("EmailAddress"); } }

        private string _Code = string.Empty;
        public string Code { get { return _Code; } set { _Code = value; this.OnPropertyChanged("Code"); } }

        private string _ErrorMessage = string.Empty;
        public string ErrorMessage { get { return _ErrorMessage; } set { _ErrorMessage = value; this.OnPropertyChanged("ErrorMessage"); } }

        private bool _IsFieldsEnabled = true;
        public bool IsFieldsEnabled { get { return _IsFieldsEnabled; } set { _IsFieldsEnabled = value; this.OnPropertyChanged("IsFieldsEnabled"); } }

        private string previousEmail = string.Empty;
        #endregion//Properties

        #region Ctors
        public UCRIEmailEdit(Grid motherGrid1, string email = "")
        {
            InitializeComponent();
            Loaded += UserControlLoaded;
            Unloaded += UserControlUnloaded;
            this.motherGrid = motherGrid1;
            this.DataContext = this;
            this.EmailAddress = this.previousEmail = email;
        }
        #endregion

        #region"Event Handlers"
        private void UserControlLoaded(object sender, RoutedEventArgs e)
        {
            editEmailAddressButton.Click += sendMailCodeClick;
            textBoxPressEmailCode.PreviewTextInput += NumberValidationTextBox;
            verifyEmailCode.Click += doneBtnMailClick;
            cancelButtonEmail.Click += cancelButtonEmail_Click;
        }

        private void UserControlUnloaded(object sender, RoutedEventArgs e)
        {
            this.previousEmail = string.Empty;
            editEmailAddressButton.Click -= sendMailCodeClick;
            textBoxPressEmailCode.PreviewTextInput -= NumberValidationTextBox;
            verifyEmailCode.Click -= doneBtnMailClick;
            cancelButtonEmail.Click -= cancelButtonEmail_Click;
        }

        private void sendMailCodeClick(object sender, RoutedEventArgs e)
        {
            try
            {
                EmailAddress = EmailAddress.Trim();
                if (EmailAddress.Equals(previousEmail) && DefaultSettings.userProfile.IsEmailVerified == 1)
                {
                    UIHelperMethods.ShowMessageWithTimerFromNonThread(motherGrid, NotificationMessages.ABOUT_NO_CHANGE);
                    return;
                }
                if (EmailAddress.Length > 0 && HelperMethodsModel.IsValidEmail(EmailAddress))
                {
                    if ((DefaultSettings.userProfile.IsEmailVerified == 0) || ((DefaultSettings.userProfile.IsEmailVerified == 1) && UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.SURE_WANT_TO, NotificationMessages.BIR_CHANGE_VERIFIED_MAIL_ASK), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Change"))))
                    {
                        Application.Current.Dispatcher.BeginInvoke(() => SendCodeMail());
                    }
                }
                else ErrorMessage = NotificationMessages.BIR_INVALID_EMAIL_ADDRESS;
            }
            finally { }
        }

        private void doneBtnMailClick(object sender, RoutedEventArgs e)
        {
            try
            {
                EmailAddress = EmailAddress.Trim();
                Code = Code.Trim();
                if (EmailAddress.Length > 0 && HelperMethodsModel.IsValidEmail(EmailAddress))
                {
                    if (Code.Length > 0)
                    {
                        if ((DefaultSettings.userProfile.IsEmailVerified == 0) || ((DefaultSettings.userProfile.IsEmailVerified == 1) && UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.SURE_WANT_TO, NotificationMessages.BIR_CHANGE_VERIFIED_MAIL_ASK), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Change"))))
                        {
                            VerifyCodeMail();
                        }
                    }
                    else
                    {
                        WNConfirmationView cv = new WNConfirmationView("Edit failed", NotificationMessages.BIR_VERIFICATION_CODE_REQUIRED, CustomConfirmationDialogButtonOptions.OK, RingIDSettingsWindow);
                        var result = cv.ShowCustomDialog();
                    }
                }
                else ErrorMessage = NotificationMessages.BIR_INVALID_EMAIL_ADDRESS;
            }
            finally { }
        }

        private void cancelButtonEmail_Click(object sender, RoutedEventArgs e)
        {
            ResetViewMode();
        }
        #endregion//"Event Handlers"

        #region "Utility Methods"

        private WNRingIDSettingsWindow RingIDSettingsWindow
        {
            get
            {
                if (motherGrid != null)
                {
                    var obj = motherGrid.Parent;
                    if (obj != null && obj is WNRingIDSettingsWindow) return (WNRingIDSettingsWindow)obj;
                }
                return null;
            }
        }

        public void ResetViewMode()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (RingIDSettingsWindow != null) RingIDSettingsWindow._AccountSettings.ucRecoverySettings.ResetViewMode();
            });
        }

        private void SendCodeMail()
        {
            IsFieldsEnabled = false;
            SendandVerifyAfterReg(EmailAddress, null);
        }

        private void VerifyCodeMail()
        {
            IsFieldsEnabled = false;
            SendandVerifyAfterReg(EmailAddress, Code);
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        public void SendandVerifyAfterReg(string email, string code)
        {
            ThreadVerifyEmailOrMobile thrd = new ThreadVerifyEmailOrMobile();
            thrd.OnVerifyEmailOrMobile += (isSuccess, errorMessge, string2) =>
            {
                if (isSuccess)
                {
                    IsFieldsEnabled = false;
                    IsFieldsEnabled = true;
                    if (!string.IsNullOrEmpty(code))
                    {
                        DefaultSettings.userProfile.Email = email;
                        DefaultSettings.userProfile.IsEmailVerified = 1;
                        RingIDViewModel.Instance.MyBasicInfoModel.Email = email;
                        RingIDViewModel.Instance.MyBasicInfoModel.IsEmailVerified = 1;
                        ResetViewMode();
                    }
                    if (!string.IsNullOrEmpty(errorMessge)) UIHelperMethods.ShowMessageWithTimerFromThread(motherGrid, errorMessge);
                }
                else UIHelperMethods.ShowMessageWithTimerFromThread(motherGrid, errorMessge);
            };
            thrd.StartThread(AppConstants.TYPE_ADD_VERIFY_EMAIL, email, null, null, code, false);
            //new ThradSendandVerifyAfterReg().StartThread(email, code);
        }
        #endregion "Utility Methods"


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

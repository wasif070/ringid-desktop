﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Settings.RecoverySettingsComponents
{
    /// <summary>
    /// Interaction logic for UCRIEmailView.xaml
    /// </summary>
    public partial class UCRIEmailView : UserControl
    {
        private int _IsEmailVerified = 0;
        public int IsEmailVerified { get { return _IsEmailVerified; } set { _IsEmailVerified = value; } }

        private string _EmailAddress = string.Empty;
        public string EmailAddress { get { return _EmailAddress; } set { _EmailAddress = value; } }

        public UCRIEmailView(string emailAddress = "", int isEmailAddressVerified = 0)
        {
            InitializeComponent();
            this.DataContext = this;
            this.EmailAddress = !string.IsNullOrEmpty(emailAddress) ? emailAddress : NotificationMessages.ABOUT_NOT_AVAILABLE;
            this.IsEmailVerified = isEmailAddressVerified;
        }
    }
}

﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.UI.PopUp;
using View.UI.Settings.RecoverySettingsComponents;
using View.UI.SignUp;
using View.UI.SocialMedia;
using View.Utility;
using View.Utility.Services;
using View.ViewModel;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCRecoverySettings.xaml
    /// </summary>
    public partial class UCRecoverySettings : UserControl, INotifyPropertyChanged
    {
        #region"Fields"
        ILog log = LogManager.GetLogger(typeof(UCRecoverySettings).Name);
        public UCRIPhoneNumberView ucRIPhoneNumberView = null;
        public UCRIEmailView ucRIEmailView = null;
        public UCRIEmailEdit ucRIEmailEdit = null;
        private Grid motherGrid;
        #endregion

        #region Properties
        private UCWaitForDigitVarification WaitForDigitVarification { get; set; }
        private UCFBAuthentication FBAuthentication { get; set; }
        private UCTwitterUI TwitterUI { get; set; }

        private UserBasicInfoModel myBasicInfoModel;
        public UserBasicInfoModel MyBasicInfoModel
        {
            get { return myBasicInfoModel; }
            set
            { myBasicInfoModel = value; this.OnPropertyChanged("MyBasicInfoModel"); }
        }

        #endregion
        #region"Ctrors"
        public UCRecoverySettings(Grid motherGrid1)
        {
            InitializeComponent();
            this.motherGrid = motherGrid1;
            this.DataContext = this;
            Loaded += UCRecoverySettings_Loaded;
            Unloaded += UCRecoverySettings_Unloaded;
            // LoadData();
            if (phoneNumber.Child == null)
            {
                ucRIPhoneNumberView = new UCRIPhoneNumberView(DefaultSettings.userProfile.IsMobileNumberVerified, DefaultSettings.userProfile.MobileDialingCode, DefaultSettings.userProfile.MobileNumber);
                phoneNumber.Child = ucRIPhoneNumberView;
            }
            if (ViewEmailAddressHolder.Child == null)
            {
                ucRIEmailView = new UCRIEmailView(DefaultSettings.userProfile.Email, DefaultSettings.userProfile.IsEmailVerified);
                ViewEmailAddressHolder.Child = ucRIEmailView;
            }
            MyBasicInfoModel = RingIDViewModel.Instance.MyBasicInfoModel;
        }
        #endregion"Ctors"

        #region Commands
        private ICommand _SocialMediaClickCommand;
        public ICommand SocialMediaClickCommand
        {
            get
            {
                _SocialMediaClickCommand = _SocialMediaClickCommand ?? new RelayCommand((param) => OnSocialMediaAddClicked(param));
                return _SocialMediaClickCommand;
            }
        }

        public void OnSocialMediaAddClicked(object param)
        {
            if (param != null)
            {
                int socialMediaParameter = Convert.ToInt32(param);
                switch (socialMediaParameter)
                {
                    case 1:
                        showFBWebSDK();
                        break;
                    case 2:
                        showTwitterWebSDK();
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        #region EVENT HANDLERS

        void UCRecoverySettings_Unloaded(object sender, RoutedEventArgs e)
        {
            editPhoneNumber.MouseEnter -= editPhoneNumber_MouseEnter;
            editPhoneNumber.MouseLeave -= editPhoneNumber_MouseLeave;
            editButton.Click -= editMobileBtnClick;
            addButton.Click -= editMobileBtnClick;
            EmailAddressPanel.MouseEnter -= EmailAddressPanel_MouseEnter;
            EmailAddressPanel.MouseLeave -= EmailAddressPanel_MouseLeave;
            emailEditButton.Click -= editMailBtnClick;
            emailAddButton.Click -= editMailBtnClick;
            editFacebook.MouseEnter -= editFacebook_MouseEnter;
            editFacebook.MouseLeave -= editFacebook_MouseLeave;
            editTwitter.MouseEnter -= editTwitter_MouseEnter;
            editTwitter.MouseLeave -= editTwitter_MouseLeave;
        }

        void UCRecoverySettings_Loaded(object sender, RoutedEventArgs e)
        {
            editPhoneNumber.MouseEnter += editPhoneNumber_MouseEnter;
            editPhoneNumber.MouseLeave += editPhoneNumber_MouseLeave;
            editButton.Click += editMobileBtnClick;
            addButton.Click += editMobileBtnClick;
            EmailAddressPanel.MouseEnter += EmailAddressPanel_MouseEnter;
            EmailAddressPanel.MouseLeave += EmailAddressPanel_MouseLeave;
            emailEditButton.Click += editMailBtnClick;
            emailAddButton.Click += editMailBtnClick;
            editFacebook.MouseEnter += editFacebook_MouseEnter;
            editFacebook.MouseLeave += editFacebook_MouseLeave;
            editTwitter.MouseEnter += editTwitter_MouseEnter;
            editTwitter.MouseLeave += editTwitter_MouseLeave;
        }

        private void editPhoneNumber_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(DefaultSettings.userProfile.MobileNumber) && !string.IsNullOrEmpty(DefaultSettings.userProfile.MobileDialingCode)) this.editButton.Visibility = Visibility.Visible;
                else this.addButton.Visibility = Visibility.Visible;
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void editPhoneNumber_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                this.editButton.Visibility = Visibility.Collapsed;
                this.addButton.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void EmailAddressPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(DefaultSettings.userProfile.Email)) this.emailEditButton.Visibility = Visibility.Visible;
                else this.emailAddButton.Visibility = Visibility.Visible;
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void EmailAddressPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                this.emailEditButton.Visibility = Visibility.Collapsed;
                this.emailAddButton.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void editMobileBtnClick(object sender, RoutedEventArgs e)
        {
            showDigitsWaitingUI();
        }

        private void editMailBtnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ucRIEmailEdit == null) ucRIEmailEdit = new UCRIEmailEdit(motherGrid, MyBasicInfoModel.Email);
                ViewEmailAddressPanel.Visibility = Visibility.Collapsed;
                EditEmailAddressHolder.Child = ucRIEmailEdit;
                EmailAddressEditMode = Visibility.Hidden;
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void editFacebook_MouseLeave(object sender, MouseEventArgs e)
        {
            this.facebookAddButton.Visibility = this.facebookEditButton.Visibility = Visibility.Collapsed;
        }

        private void editFacebook_MouseEnter(object sender, MouseEventArgs e)
        {
            this.facebookAddButton.Visibility = DefaultSettings.userProfile.FacebookValidated == 0 ? Visibility.Visible : Visibility.Collapsed;
            this.facebookEditButton.Visibility = DefaultSettings.userProfile.FacebookValidated == 0 ? Visibility.Collapsed : Visibility.Visible;
        }

        private void editTwitter_MouseEnter(object sender, MouseEventArgs e)
        {
            this.twitterAddButton.Visibility = DefaultSettings.userProfile.TwitterValidated == 0 ? Visibility.Visible : Visibility.Collapsed;
            this.twitterEditButton.Visibility = DefaultSettings.userProfile.TwitterValidated == 0 ? Visibility.Collapsed : Visibility.Visible;
        }

        private void editTwitter_MouseLeave(object sender, MouseEventArgs e)
        {
            this.twitterAddButton.Visibility = twitterEditButton.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region "Utility Methods"

        public void LoadVerifiedMobileNumberFromDigits(string mbl, string mblDc)
        {
            try
            {
                DefaultSettings.userProfile.MobileNumber = RingIDViewModel.Instance.MyBasicInfoModel.MobilePhone = mbl;
                DefaultSettings.userProfile.MobileDialingCode = RingIDViewModel.Instance.MyBasicInfoModel.MobilePhoneDialingCode = mblDc;
                DefaultSettings.userProfile.IsMobileNumberVerified = RingIDViewModel.Instance.MyBasicInfoModel.IsMobileNumberVerified = 1;
                if (ucRIPhoneNumberView != null)
                {
                    ucRIPhoneNumberView.FullMobileNumber = mblDc + "-" + mbl;
                    ucRIPhoneNumberView.IsMobileNumberVerified = 1;
                }
            }
            catch (Exception) { }
        }

        public void ResetViewMode()
        {
            ucRIEmailView = new UCRIEmailView(DefaultSettings.userProfile.Email, DefaultSettings.userProfile.IsEmailVerified);
            ucRIEmailEdit = null;
            EditEmailAddressHolder.Child = null;
            ViewEmailAddressPanel.Visibility = Visibility.Visible;
            ViewEmailAddressHolder.Child = ucRIEmailView;
            EmailAddressEditMode = Visibility.Visible;
        }

        private void showDigitsWaitingUI()
        {
            if (WaitForDigitVarification == null)
            {
                WaitForDigitVarification = new UCWaitForDigitVarification(3, DefaultSettings.LOGIN_RING_ID);
                WaitForDigitVarification.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 0.0);//background Color Transparent Black
                WaitForDigitVarification.SetParent(this.motherGrid);
                WaitForDigitVarification.OnCompletedDigitsVarifications += (successfullyVerified, mbl, mblDc, previousRingId) =>
                {
                    WaitForDigitVarification = null;
                    if (successfullyVerified)
                    {
                        ThreadVerifyEmailOrMobile thrd = new ThreadVerifyEmailOrMobile();
                        thrd.OnVerifyEmailOrMobile += (isSuccess, errorMessge, string2) =>
                        {
                            if (isSuccess)
                            {
                                LoadVerifiedMobileNumberFromDigits(mbl, mblDc);
                                if (!string.IsNullOrEmpty(errorMessge)) UIHelperMethods.ShowMessageWithTimerFromThread(motherGrid, errorMessge);
                            }
                            else UIHelperMethods.ShowMessageWithTimerFromThread(motherGrid, errorMessge);
                        };
                        thrd.StartThread(AppConstants.TYPE_ACTION_VERIFY_MY_NUMBER, null, mbl, mblDc, string.Empty, true);
                    }
                    else UIHelperMethods.ShowMessageWithTimerFromThread(motherGrid, NotificationMessages.FAILDED_TRY_AGAIN);
                };
                WaitForDigitVarification.Show();
            }
        }

        private void showFBWebSDK()
        {
            if (FBAuthentication == null)
            {
                FBAuthentication = new UCFBAuthentication(SettingsConstants.TYPE_FROM_PROFILE);
                FBAuthentication.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                FBAuthentication.SetParent(motherGrid);
                FBAuthentication.OnRemovedUserControl += () =>
                {
                    FBAuthentication = null;
                };
                FBAuthentication.OnFBAuthenticationCompleted += (typeToSwitch, fbName, fbProfileImage, fbId, accessToken, ringID, message) =>
                {
                    if (string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(fbId))
                    {
                        DefaultSettings.userProfile.SocialMediaId = fbId;
                        DefaultSettings.userProfile.FacebookValidated = 1;
                        MyBasicInfoModel.FacebookValidated = 1;
                        ResetRecoveryPanelView();
                        UIHelperMethods.ShowMessageWithTimerFromNonThread(motherGrid, NotificationMessages.FB_ADDED_SUCCESS_MESSAGE);
                    }
                    else if (!string.IsNullOrEmpty(message)) UIHelperMethods.ShowMessageWithTimerFromNonThread(motherGrid, message);
                    else UIHelperMethods.ShowMessageWithTimerFromNonThread(motherGrid, NotificationMessages.FAILDED_TRY_AGAIN);
                };
                FBAuthentication.Show();
            }
        }

        private void showTwitterWebSDK()
        {
            if (TwitterUI == null)
            {
                TwitterUI = new UCTwitterUI(SettingsConstants.TYPE_FROM_PROFILE);
                TwitterUI.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                TwitterUI.SetParent(motherGrid);
                TwitterUI.OnRemovedUserControl += () =>
                {
                    TwitterUI = null;
                };
                TwitterUI.OnThreadCompleted += (typeToSwitch, screenName, profileImage, socialMediaID, inputToken, ringID, message) =>
                {
                    if (string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(socialMediaID))
                    {
                        DefaultSettings.userProfile.SocialMediaId = socialMediaID;
                        DefaultSettings.userProfile.TwitterValidated = 1;
                        MyBasicInfoModel.TwitterValidated = 1;
                        ResetRecoveryPanelView();
                        UIHelperMethods.ShowMessageWithTimerFromNonThread(motherGrid, NotificationMessages.TW_ADDED_SUCCESS_MESSAGE);
                    }
                    else if (!string.IsNullOrEmpty(message)) UIHelperMethods.ShowMessageWithTimerFromNonThread(motherGrid, message);
                    else UIHelperMethods.ShowMessageWithTimerFromNonThread(motherGrid, NotificationMessages.FAILDED_TRY_AGAIN);
                };
                TwitterUI.Show();
            }
        }

        private void ResetRecoveryPanelView()
        {
            if (RingIDSettingsWindow != null)
            {
                RingIDSettingsWindow._AccountSettings.socialMediaPanelBorder.Child = null;
                RingIDSettingsWindow._AccountSettings.socialMediaPanelBorder.Visibility = Visibility.Collapsed;
                RingIDSettingsWindow._AccountSettings.accountSettingsPanelBorder.Visibility = Visibility.Visible;
            }
        }

        private WNRingIDSettingsWindow RingIDSettingsWindow
        {
            get
            {
                if (motherGrid != null)
                {
                    var obj = motherGrid.Parent;
                    if (obj != null && obj is WNRingIDSettingsWindow) return (WNRingIDSettingsWindow)obj;
                }
                return null;
            }
        }
        #endregion "Utility methods"

        #region Dependency Properties
        public Visibility PhoneNumberEditMode
        {
            get { return (Visibility)GetValue(PhoneNumberEditModeProperty); }
            set { SetValue(PhoneNumberEditModeProperty, value); }
        }
        public static readonly DependencyProperty PhoneNumberEditModeProperty =
            DependencyProperty.Register("PhoneNumberEditMode", typeof(Visibility), typeof(UCRecoverySettings), new PropertyMetadata(Visibility.Visible));

        public Visibility EmailAddressEditMode
        {
            get { return (Visibility)GetValue(EmailAddressEditModeProperty); }
            set { SetValue(EmailAddressEditModeProperty, value); }
        }
        public static readonly DependencyProperty EmailAddressEditModeProperty =
            DependencyProperty.Register("EmailAddressEditMode", typeof(Visibility), typeof(UCRecoverySettings), new PropertyMetadata(Visibility.Visible));
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion//INotifyPropertyChanged Members
    }
}

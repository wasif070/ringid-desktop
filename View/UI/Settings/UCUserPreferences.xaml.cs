﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.Utility;
using View.Utility.Myprofile;

namespace View.UI.Settings
{
    /// <summary>
    /// Interaction logic for UCUserPreferences.xaml
    /// </summary>
    public partial class UCUserPreferences : UserControl, INotifyPropertyChanged
    {
        public UCUserPreferences()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        #region Access Settings

        #region Utility Methods

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion Utility Methods

        private ICommand _ChangeSettingsCommand;
        public ICommand ChangeSettingsCommand
        {
            get
            {
                if (_ChangeSettingsCommand == null)
                {
                    _ChangeSettingsCommand = new RelayCommand(param => OnChangeSettingsClicked(param));
                }
                return _ChangeSettingsCommand;
            }
        }
        private void OnChangeSettingsClicked(object param)
        {
            int value = (int)param;
            if (value == StatusConstants.CALL_ACCESS)
            {
                new ThradChangeAccessSettings(value, (bool)this.CallAccessCheckbox.IsChecked ? 1 : 0).StartThread();
            }
            else if (value == StatusConstants.CHAT_ACCESS)
            {
                new ThradChangeAccessSettings(value, (bool)this.ChatAccessCheckbox.IsChecked ? 1 : 0).StartThread();
            }
            else if (value == StatusConstants.FEED_ACCESS)
            {
                new ThradChangeAccessSettings(value, (bool)this.FeedAccessCheckbox.IsChecked ? 1 : 0).StartThread();
            }
            //else if (value == StatusConstants.ANONYMOUS_CALL)
            //{
            //    new ChangeAccessSettings(value, (bool)this.AnonymousCallCheckbox.IsChecked ? 1 : 0);
            //}
        }
        #endregion

        #region Call Settings

        private int isEveryone = 0;
        private void Checkbox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Name.Equals("EveryoneCheckbox"))
            {
                if (cb.IsChecked == true)
                {
                    isEveryone = 1;
                }
                else
                {
                    isEveryone = 0;
                }
            }

            else if (cb.Name.Equals("FriendsOnlyCheckbox"))
            {
                if (cb.IsChecked == true)
                {
                    isEveryone = 0;
                }
                else
                {
                    isEveryone = 1;
                }
            }
            new ThradChangeAccessSettings(StatusConstants.ANONYMOUS_CALL, isEveryone).StartThread();
        }

        #endregion
    }
}

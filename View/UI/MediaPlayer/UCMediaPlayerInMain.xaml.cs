﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Models.Constants;
using Models.DAO;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.RingPlayer;
using View.ViewModel;
using System.Windows;

namespace View.UI.MediaPlayer
{
    /// <summary>
    /// Interaction logic for UCMediaPlayerInMain.xaml
    /// </summary>
    public partial class UCMediaPlayerInMain : PopUpBaseControl, INotifyPropertyChanged
    {
        #region "Fields"
        System.Windows.Controls.Grid motherGrid;
        System.Windows.Controls.Grid parentOfThisControl;
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(UCMediaPlayerInMain).Name);
        private int owinServerPort;//2260
        private OwinServerSetup owinServer;
        public delegate void ClosedMediaPlayer();
        public event ClosedMediaPlayer OnClosedMediaPlayer;
        #endregion"Fields"

        #region "Ctors"
        public UCMediaPlayerInMain(VMPlayer dataModel)
        {
            InitializeComponent();
            this.DataModel = dataModel;
            this.DataContext = this;
            motherGrid = _MotherPanel;
            MediaPlayListViwer = new UCMediaPlayList(DataModel, motherGrid);
            _RightPanel.Children.Add(MediaPlayListViwer);
            UserProfileLikeCommentNumber = new UCUserProfileLikeCommentNumber(DataModel, motherGrid);
            _LikePanel.Children.Add(UserProfileLikeCommentNumber);
        }
        #endregion"Ctors"

        #region "Properties"

        private UCUserProfileLikeCommentNumber UserProfileLikeCommentNumber { get; set; }
        public UCMediaPlayList MediaPlayListViwer { set; get; }
        public UCMediaOldAndNewComments CommentsAndNewComment { get; set; }
        public UCOnlyMediaPlayer OnlyPlayerView { get; set; }
        private VMPlayer dataModel;
        public VMPlayer DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private bool showLikeComment = true;
        public bool ShowLikeComment
        {
            get { return showLikeComment; }
            set { showLikeComment = value; OnPropertyChanged("ShowLikeComment"); }
        }

        private bool showFloatingPanel = false;
        public bool ShowFloatingPanel
        {
            get { return showFloatingPanel; }
            set { showFloatingPanel = value; OnPropertyChanged("ShowFloatingPanel"); }
        }
        #endregion "Properties"

        #region "Commands"

        private ICommand loadedMainControl;
        public ICommand LoadedMainControl
        {
            get
            {
                if (loadedMainControl == null) loadedMainControl = new RelayCommand(param => OnLoadedMainControl());
                return loadedMainControl;
            }
        }
        private void OnLoadedMainControl()
        {
            try
            {
                GrabKeyBoardFocus();
                AddOnlyMediaPlaeryInGrid();
                AddCommentPanel();
<<<<<<< HEAD
                scroll.ScrollChanged += OnScrollScrollChangedCommand;
=======
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
            }
            finally { }
        }

        private ICommand unLoadedMainControl;
        public ICommand UnLoadedMainControl
        {
            get
            {
                if (unLoadedMainControl == null) unLoadedMainControl = new RelayCommand(param => OnUnLoadedMainControl());
                return unLoadedMainControl;
            }
        }
        private void OnUnLoadedMainControl()
        {
            Hide();
            this.Focusable = false;
            if (MediaPlayListViwer != null) HelperMethods.RemoveAUserControlFromAnotherPanel(MediaPlayListViwer);
            if (UserProfileLikeCommentNumber != null) HelperMethods.RemoveAUserControlFromAnotherPanel(UserProfileLikeCommentNumber);
<<<<<<< HEAD
            scroll.ScrollChanged -= OnScrollScrollChangedCommand;
=======
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        }

        private ICommand exit;
        public ICommand Exit
        {
            get
            {
                if (exit == null) exit = new RelayCommand(param => OnExit());
                return exit;
            }
        }
        public void OnExit()
        {
            CloseMediaPlayer();
        }

        private ICommand previousCommand;
        public ICommand PreviousCommand
        {
            get
            {
                if (previousCommand == null) previousCommand = new RelayCommand(param => OnPreviousCommand());
                return previousCommand;
            }
        }
        public void OnPreviousCommand()
        {
            if (DataModel.PreviousButtonEnable)
                if (DataModel.PlayingIndex < DataModel.MediaList.Count && DataModel.PlayingIndex > 0)
                {
                    DataModel.PlayingIndex--;
                    ChangeSingleMedia(DataModel.PlayingIndex);
                    playMediaThread();
                }
        }

        private ICommand nextCommand;
        public ICommand NextCommand
        {
            get
            {
                if (nextCommand == null) nextCommand = new RelayCommand(param => OnNextCommand());
                return nextCommand;
            }
        }
        public void OnNextCommand()
        {
            if (DataModel.NextButtonEnable)
                if (DataModel.PlayingIndex < DataModel.MediaList.Count - 1)
                {
                    DataModel.PlayingIndex++;
                    ChangeSingleMedia(DataModel.PlayingIndex);
                    playMediaThread();
                }
        }

        public void OnSingleMediaClickCommand(object param)
        {
            if (param is SingleMediaModel)
            {
                int idx = DataModel.MediaList.IndexOf((SingleMediaModel)param);
                if (idx >= 0 && idx < DataModel.MediaList.Count) PlayNextMedia(idx);
            }
        }

        private ICommand spacePressedCommand;
        public ICommand SpacePressedCommand
        {
            get
            {
                if (spacePressedCommand == null) spacePressedCommand = new RelayCommand(param => OnSpacePressed());
                return spacePressedCommand;
            }
        }
        private void OnSpacePressed()
        {
            DataModel.OnSingleClickOnScreen();
        }

<<<<<<< HEAD
        //private ICommand _ScrollScrollChangedCommand;
        //public ICommand ScrollScrollChangedCommand
        //{
        //    get
        //    {
        //        if (_ScrollScrollChangedCommand == null) _ScrollScrollChangedCommand = new RelayCommand(param => OnScrollScrollChangedCommand(param));
        //        return _ScrollScrollChangedCommand;
        //    }
        //}
        private void OnScrollScrollChangedCommand(object param, ScrollChangedEventArgs e)
        {
            if (e.VerticalOffset > 250)
                ShowFloatingPanel = true;
            else ShowFloatingPanel = false;
=======
        private ICommand _ScrollScrollChangedCommand;
        public ICommand ScrollScrollChangedCommand
        {
            get
            {
                if (_ScrollScrollChangedCommand == null) _ScrollScrollChangedCommand = new RelayCommand(param => OnScrollScrollChangedCommand(param));
                return _ScrollScrollChangedCommand;
            }
        }
        private void OnScrollScrollChangedCommand(object param)
        {
            if (param is ScrollChangedEventArgs)
            {
                ScrollChangedEventArgs e = (ScrollChangedEventArgs)param;
                if (e.VerticalOffset > 250)
                    ShowFloatingPanel = true;
                else ShowFloatingPanel = false;
            }
        }

        private ICommand _ScrollToTopCommand;
        public ICommand ScrollToTopCommand
        {
            get
            {
                if (_ScrollToTopCommand == null) _ScrollToTopCommand = new RelayCommand(param => OnScrollToTopCommand());
                return _ScrollToTopCommand;
            }
        }
        public void OnScrollToTopCommand()
        {
            scroll.ScrollToTop();
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        }
        #endregion "Commands"

        #region "Utility Methods"

        public void GrabKeyBoardFocus()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }
        public void AddMediaIntoMediaList(Guid albumId, SingleMediaModel contentModel)
        {
            if (DataModel.SingleMediaModel.AlbumId == albumId && !DataModel.MediaList.Any(P => P.ContentId == contentModel.ContentId)) DataModel.MediaList.InvokeAdd(contentModel);
        }

        public void SetParentGrid(System.Windows.Controls.Grid parentGrid)
        {
            if (parentGrid == null) parentGrid = UCGuiRingID.Instance.MotherPanel;
            parentOfThisControl = parentGrid;
        }

        public void RemoveChildrenFromCommentPanel()
        {
            _CommentsAndNewComments.Children.Clear();
        }

        public void AddCommentPanel(bool firstLoad = false)
        {
            RemoveChildrenFromCommentPanel();
            if (DataModel != null && DataModel.SingleMediaModel.ContentId != Guid.Empty && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                CommentsAndNewComment = new UCMediaOldAndNewComments(DataModel, motherGrid);
                CommentsAndNewComment.AddNewCommentUserControl();
                _CommentsAndNewComments.Children.Add(CommentsAndNewComment);
            }
        }

        public void AddOnlyMediaPlaeryInGrid()
        {
            if (OnlyPlayerView != null) HelperMethods.RemoveAUserControlFromAnotherPanel(OnlyPlayerView);
            if (OnlyPlayerView == null) OnlyPlayerView = new UCOnlyMediaPlayer(DataModel, motherGrid);
            OnlyPlayerView.OnCurrentMediaEnded += () => { autoPlayNext(); };
            OnlyPlayerView.OnLoadedMediaPlayer += () =>
            {
                if (DataModel.PlayingState == ViewConstants.MEDIA_PLAYING && !DataModel.IsLoadded)
                {
                    playMediaThread();
                    DataModel.IsLoadded = true;
                }
            };
            _PlayerContainer.Children.Add(OnlyPlayerView);
        }

        private string makeHDurl(string normalUrl)
        {
            StringBuilder builder = new StringBuilder(normalUrl);
            builder.Insert(normalUrl.LastIndexOf('/') + 1, "h");
            return ServerAndPortSettings.GetVODServerResourceURL + builder.ToString();
        }

        private string isHDAvailable(string normalUrl)
        {
            string hdUrl = makeHDurl(normalUrl);
            string ringMediaUrlHD = Models.Utility.HelperMethodsModel.URLExists(ServerAndPortSettings.GetVODServerResourceURL + hdUrl) ? hdUrl : string.Empty;
            return ringMediaUrlHD;
        }

        public void ChangeSingleMedia(int index, bool firstLoad = false)
        {
            try
            {
                DataModel.ChangeSelectedMedia(index);
                if (DataModel.SingleMediaModel.ContentId != Guid.Empty)
                {
                    DataModel.IsBuffering = true;
                    DataModel.IsStartedNewMedia = true;
                    RingIDViewModel.Instance.MediaComments.Clear();
                    DataModel.CommentList = RingIDViewModel.Instance.MediaComments;

                    ThreadStart detailsOfmedia = delegate
                    {
                        SingleMediaModel clickedMedia = DataModel.SingleMediaModel;
                        SingleMediaModel modelINPlaylist = RingIDViewModel.Instance.MyRecentPlayList.Where(P => P.ContentId == clickedMedia.ContentId).FirstOrDefault();
                        if (modelINPlaylist == null)
                        {
                            RingIDViewModel.Instance.MyRecentPlayList.InvokeInsert(0, clickedMedia);
                            if (RingIDViewModel.Instance.MyRecentPlayList.Count > 20)
                            {
                                SingleMediaModel lastModel = RingIDViewModel.Instance.MyRecentPlayList.LastOrDefault();
                                RingIDViewModel.Instance.MyRecentPlayList.InvokeRemove(lastModel);
                                MediaDAO.Instance.DeleteFromRecentMediasTable(lastModel.ContentId);
                            }
                        }
                        else
                        {
                            RingIDViewModel.Instance.MyRecentPlayList.InvokeRemove(modelINPlaylist);
                            MediaDAO.Instance.DeleteFromRecentMediasTable(modelINPlaylist.ContentId);
                            RingIDViewModel.Instance.MyRecentPlayList.InvokeInsert(0, clickedMedia);
                        }
                        MediaDAO.Instance.InsertIntoRecentMediasTable(DataModel.SingleMediaModel.ContentId, clickedMedia.MediaOwner.UserTableID, clickedMedia.Title, clickedMedia.Artist, clickedMedia.StreamUrl, clickedMedia.ThumbUrl, clickedMedia.Duration, clickedMedia.ThumbImageWidth, clickedMedia.ThumbImageHeight, clickedMedia.MediaType, clickedMedia.AlbumId, clickedMedia.AlbumName, clickedMedia.AccessCount, clickedMedia.LastPlayedTime);
                        bool result = false;
                        if (DefaultSettings.IsInternetAvailable)
                        {
                            //  resetMediaUriToPlay();
                            result = new MediaRequests().DetailsOfMedia(DataModel.SingleMediaModel.ContentId, DataModel.SingleMediaModel.MediaOwner.UserTableID, DataModel.SingleMediaModel.ChannelId);
                            if (result && DataModel != null)
                            {
                                if (DataModel.RingMediaUri == null)
                                {
                                    OnlyPlayerView.IsThumbViwe = true;
                                    Uri ringMediaUri = setMediaUrlInServer(clickedMedia.StreamUrl);
                                    if (ringMediaUri != null && DataModel.SingleMediaModel.ContentId == clickedMedia.ContentId)
                                    {
                                        DataModel.RingMediaUri = ringMediaUri;
                                        System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate { playCurrentMedia(); });
                                    }
                                }
                                if (DataModel.MediaList.Count < 10) MediaPlayListViwer.LoadMoreMedia();
                                if (DataModel.SingleMediaModel.LikeCommentShare != null && DataModel.SingleMediaModel.LikeCommentShare.NumberOfComments <= 0) DataModel.IsStartedNewMedia = false;
                                else
                                {
                                    bool commentsReq = new MediaRequests().CommentReqeust(DataModel.NewsFeedID, DataModel.SingleMediaModel.ContentId, Guid.Empty, 0, 0);
                                    if (DataModel != null) DataModel.IsStartedNewMedia = false;
                                }
                            }
                            else
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UIHelperMethods.ShowMessageWithTimerFromNonThread(motherGrid, "Can not play this media!");
                                    autoPlayNext();
                                }, DispatcherPriority.Send);
                            }
                        }
                        else
                        {
                            DataModel.IsStartedNewMedia = false;
                            UIHelperMethods.ShowMessageWithTimerFromThread(motherGrid, NotificationMessages.INTERNET_UNAVAILABLE);
                        }
                        if (!result && DataModel != null && DataModel.MediaList.Count < 10) MediaPlayListViwer.LoadMoreMedia();
                    };
                    ShowLikeComment = true;
                    new Thread(detailsOfmedia).Start();
                    AddCommentPanel(firstLoad);
                }
                else
                {
                    ShowLikeComment = false;
                    ThreadStart loadMore = delegate { MediaPlayListViwer.AddFromLocalLists(); };
                    new Thread(loadMore).Start();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace);
            }
        }

        public void PlayNextMedia(int index)
        {
            ChangeSingleMedia(index);
            playMediaThread();
        }

        private void playMediaThread()
        {
            setNextMediaToPlay();
            //playCurrentMedia();
            Uri ringMediaUri = null;
            if (DataModel.SingleMediaModel.IsFromLocalDirectory) ringMediaUri = new Uri(DataModel.SingleMediaModel.StreamUrl, UriKind.Relative);
            else ringMediaUri = setDownoadedMediaURL(DataModel.SingleMediaModel);
            if (ringMediaUri != null)
            {
                DataModel.RingMediaUri = ringMediaUri;
                playCurrentMedia();
            }
            else if (!DataModel.SingleMediaModel.IsFromLocalDirectory && DataModel.SingleMediaModel.ContentId == Guid.Empty && !string.IsNullOrEmpty(DataModel.SingleMediaModel.StreamUrl))
            {
                if (DataModel.SingleMediaModel.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                    OnlyPlayerView.IsThumbViwe = true;
                DataModel.RingMediaUri = setMediaUrlInServer(DataModel.SingleMediaModel.StreamUrl);
                playCurrentMedia();
            }
            else DataModel.RingMediaUri = null;
        }

        private void playCurrentMedia()
        {
            if (DataModel.RingMediaUri != null)
            {
                DataModel.PlayingState = ViewConstants.MEDIA_PLAYING;
                DataModel.Play();
            }
        }

        public void SetCurrentUriForOwin()
        {
            OwinServerSetup.CurrentURi = (ServerAndPortSettings.GetVODServerResourceURL + DataModel.SingleMediaModel.StreamUrl).Replace("http:", "https:");
        }

        public void SetOwinServer()
        {
            if (owinServerPort <= 0) owinServerPort = HelperMethods.GetAvailablePortForMemoryServer();
            if (owinServer == null) owinServer = new OwinServerSetup();
            owinServer.StartOwinServer(owinServerPort);
        }

        private void setNextMediaToPlay()
        {
            DataModel.SeekBarValue = 0;
            DataModel.ElapsedTime = 0;
            //  OwinServerSetup.CurrentURi = string.Empty;
            //   SingleMediaModel clickedMedia = DataModel.SingleMediaModel;
            //resetMediaUriToPlay();
        }

        private Uri setDownoadedMediaURL(SingleMediaModel clickedMedia)
        {
            Uri ringMediaUri = null;
            if (RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == clickedMedia.ContentId))
            {
                string fname = HelperMethods.GetDownloadedFilePathFromStreamURL(clickedMedia.StreamUrl, clickedMedia.ContentId);
                if (System.IO.File.Exists(fname)) ringMediaUri = new Uri(fname);
                else
                {
                    ringMediaUri = new Uri(ServerAndPortSettings.GetVODServerResourceURL + clickedMedia.StreamUrl);
                    clickedMedia.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                    clickedMedia.DownloadProgress = 0;
                    Models.DAO.MediaDAO.Instance.DeleteFromDownloadedMediasTable(clickedMedia.ContentId);
                }
            }
            return ringMediaUri;
        }

        public Uri setMediaUrlInServer(string streamUrl)
        {
            Uri ringMediaUri = null;
            if (DataModel.SingleMediaModel.IsFromLocalDirectory) ringMediaUri = new Uri(streamUrl, UriKind.Relative);
            else if (ServerAndPortSettings.GetVODServerResourceURL != null)
            {
                if (owinServerPort <= 0) SetOwinServer();
                SetCurrentUriForOwin();
                ringMediaUri = new Uri("http://localhost:" + owinServerPort + "/" + streamUrl);
            }
            return ringMediaUri;
        }
        /// <summary>
        /// if Single media then reapeat this after given interval
        /// else 
        /// Automatically play next media after given time duration(if media content is same) 
        /// here given duration=5sec
        /// </summary>
        private void autoPlayNext()
        {
            if (DataModel != null)
            {
                DataModel.Close();
                DataModel.IsBuffering = true;
                DataModel.RingMediaUri = null;
                DataModel.MediaElementPosition = TimeSpan.Zero;
                DataModel.SeekBarValue = 0;
                Guid lastPlayedContentId = DataModel.SingleMediaModel.ContentId;
                if (DataModel.MediaList.Count > 1)
                {
                    int nextindex = 0;
                    if (DataModel.PlayingIndex + 1 == DataModel.MediaList.Count) PlayNextMedia(nextindex);
                    else nextindex = DataModel.PlayingIndex + 1;
                    ChangeSingleMedia(nextindex);
                    lastPlayedContentId = DataModel.SingleMediaModel.ContentId;
                    setNextMediaToPlay();
                }
                DispatcherTimer playNextmedia = new DispatcherTimer();
                playNextmedia.Interval = TimeSpan.FromSeconds(3);
                playNextmedia.Tick += new EventHandler(delegate(object s, EventArgs ev)
                    {
                        if (DataModel != null
                            && DataModel.SingleMediaModel.ContentId == lastPlayedContentId
                            && DataModel.MediaElementPosition.TotalMilliseconds == 0
                            && DataModel.PlayingState == ViewConstants.MEDIA_PAUSED
                            && DataModel.IsBuffering)
                        {
                            Uri ringMediaUri = null;
                            if (DataModel.SingleMediaModel.IsFromLocalDirectory) ringMediaUri = new Uri(DataModel.SingleMediaModel.StreamUrl, UriKind.Relative);
                            else if (ringMediaUri != null) ringMediaUri = setDownoadedMediaURL(DataModel.SingleMediaModel);
                            if (ringMediaUri != null) DataModel.RingMediaUri = ringMediaUri;
                            playCurrentMedia();
                        }
                        playNextmedia.Stop();
                    });
                playNextmedia.Start();
            }
        }
        public void ResetAllData()
        {
            if (DataModel != null)
            {
                DataModel.Close();
                DataModel = null;
            }
            if (OnlyPlayerView != null)
            {
                if (OnlyPlayerView.SeparatePlayer != null) OnlyPlayerView.SeparatePlayer.CloseThisWindow();
                HelperMethods.RemoveAUserControlFromAnotherPanel(OnlyPlayerView);
                OnlyPlayerView = null;
            }
            ViewConstants.ContentID = Guid.Empty;
            ViewConstants.AlbumID = Guid.Empty;
        }
        public void CloseMediaPlayer()
        {
            try
            {
<<<<<<< HEAD
                scroll.ScrollChanged -= OnScrollScrollChangedCommand;
=======
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                ResetAllData();
                Hide();
            }
            finally
            {
                if (OnClosedMediaPlayer != null) OnClosedMediaPlayer();
            }
        }

        public void ShowPlayerInMainView(bool firstTime = false)
        {
            DataModel.PlayerViewType = ViewConstants.POPUP;
            SetParent(parentOfThisControl);
            if (!firstTime) parentOfThisControl.Children.Remove(this);
            Focusable = true;
            Panel.SetZIndex(this, ZIndexConstants.BasicMediaViewWrapper);
            Show();
        }

        public void RemovePlayerFromGuiRingID()
        {
            HelperMethods.RemoveAUserControlFromAnotherPanel(this);
        }

        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using View.Utility;
using View.Utility.RingPlayer;

namespace View.UI.MediaPlayer
{
    /// <summary>
    /// Interaction logic for UCMediaProgressBarAndVolumn.xaml
    /// </summary>
    public partial class UCMediaProgressBarAndVolumn : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private Grid motherPanel = null;
        Thumb progressBarThumb;
        #endregion "Fields"

        #region "Ctors"
        public UCMediaProgressBarAndVolumn(VMPlayer DataModel, Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            this.DataModel = DataModel;
            this.motherPanel = motherGrid;
        }
        #endregion "Ctors"

        #region "Properties"
        private VMPlayer dataModel;
        public VMPlayer DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion "Properties"

        #region"Commands"

        private ICommand loadedMainControl;
        public ICommand LoadedMainControl
        {
            get
            {
                if (loadedMainControl == null) loadedMainControl = new RelayCommand(param => OnLoadedMainControl());
                return loadedMainControl;
            }
        }
        private void OnLoadedMainControl()
        {
            var slider = (Slider)_PlayerProgressBar;
            var track = (System.Windows.Controls.Primitives.Track)slider.Template.FindName("PART_Track", slider);
            if (track != null)
            {
                progressBarThumb = track.Thumb;
                progressBarThumb.DragStarted += progressBarThumb_DragStarted;
                progressBarThumb.DragCompleted += progressBarThumb_DragCompleted;
                progressBarThumb.MouseEnter += progressBarThumb_MouseEnter;
            }
            _PlayerProgressBar.ValueChanged += _PlayerProgressBar_ValueChanged;
            _SliderVolumn.ValueChanged += _SliderVolumn_ValueChanged;
            _SliderVolumn.Value = DataModel.MediaElementVolume * 100;
            DataModel.VolmunInPercentage = (long)(DataModel.MediaElementVolume * 100) + "%";
        }

        private ICommand unLoadedMainControl;
        public ICommand UnLoadedMainControl
        {
            get
            {
                if (unLoadedMainControl == null) unLoadedMainControl = new RelayCommand(param => OnUnLoadedMainControl());
                return unLoadedMainControl;
            }
        }
        private void OnUnLoadedMainControl()
        {
            if (progressBarThumb != null)
            {
                progressBarThumb.DragStarted -= progressBarThumb_DragStarted;
                progressBarThumb.DragCompleted -= progressBarThumb_DragCompleted;
                progressBarThumb.MouseEnter -= progressBarThumb_MouseEnter;
            }
            _PlayerProgressBar.ValueChanged -= _PlayerProgressBar_ValueChanged;
            _SliderVolumn.ValueChanged -= _SliderVolumn_ValueChanged;
        }

        private ICommand previousCommand;
        public ICommand PreviousCommand
        {
            get
            {
                if (previousCommand == null) previousCommand = new RelayCommand(param => OnPreviousCommand(param));
                return previousCommand;
            }
        }
        public void OnPreviousCommand(object param)
        {
            if (MediaPlayerInMain != null) MediaPlayerInMain.OnPreviousCommand();
        }

        private ICommand nextCommand;
        public ICommand NextCommand
        {
            get
            {
                if (nextCommand == null) nextCommand = new RelayCommand(param => OnNextCommand(param));
                return nextCommand;
            }
        }
        public void OnNextCommand(object param)
        {
            if (MediaPlayerInMain != null) MediaPlayerInMain.OnNextCommand();
        }

        private ICommand playCommand;
        public ICommand PlayCommand
        {
            get
            {
                if (playCommand == null) playCommand = new RelayCommand(param => DataModel.Play());
                return playCommand;
            }
        }

        private ICommand pauseCommand;
        public ICommand PauseCommand
        {
            get
            {
                if (pauseCommand == null) pauseCommand = new RelayCommand(param => DataModel.Paused());
                return pauseCommand;
            }
        }

        #endregion"Commands"

        #region "Event Triggers"


        void _PlayerProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            DataModel.ElapsedTime = (double)e.NewValue;
        }

        void _SliderVolumn_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            DataModel.MediaElementVolume = (double)e.NewValue / 100;
            DataModel.VolmunInPercentage = (long)e.NewValue + "%";
        }

        void progressBarThumb_MouseEnter(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                MouseButtonEventArgs args = new MouseButtonEventArgs(
                    e.MouseDevice, e.Timestamp, MouseButton.Left);
                args.RoutedEvent = MouseLeftButtonDownEvent;
                (sender as Thumb).RaiseEvent(args);
            }
        }

        void progressBarThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            DataModel.IsSliderDragging = false;
            DataModel.MediaElementPosition = TimeSpan.FromSeconds(DataModel.SeekBarValue);
        }

        void progressBarThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            DataModel.IsSliderDragging = true;
        }
        #endregion//"Event Triggers"

        #region "Utility Methods"
        private UCMediaPlayerInMain MediaPlayerInMain
        {
            get
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                if (obj != null && obj is UCMediaPlayerInMain)
                {
                    return (UCMediaPlayerInMain)obj;
                }
                return null;
            }
        }
        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

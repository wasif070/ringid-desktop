﻿using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using View.BindingModels;
using View.UI.PopUp;
using View.UI.PopUp.MediaSendViaChat;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using System.Threading;
using System.Windows;
using View.Constants;
using System;
using Newtonsoft.Json.Linq;
using View.Utility.DataContainer;
using View.UI.SocialMedia;
using System.Linq;
using Models.DAO;

namespace View.UI.MediaPlayer
{
    /// <summary>
    /// Interaction logic for UCUserProfileLikeCommentNumber.xaml
    /// </summary>
    public partial class UCUserProfileLikeCommentNumber : UserControl
    {
        #region "Fields"
        private Grid motherPanel = null;
        UCFBShare fbShare;
        #endregion "Fields"

        #region "Ctors"
        public UCUserProfileLikeCommentNumber(VMPlayer DataModel, Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            this.DataModel = DataModel;
            this.motherPanel = motherGrid;
        }
        #endregion "Ctors"

        #region "Properties"
        private UCLikeList LikeListView { get; set; }
        private VMPlayer dataModel;
        public VMPlayer DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion "Properties"

        #region "Commands and Command Methods"

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {

        }

        private ICommand likeCommand;
        public ICommand LikeCommand
        {
            get
            {
                if (likeCommand == null) likeCommand = new RelayCommand(param => OnLikeCommand(param));
                return likeCommand;
            }
        }
        private void OnLikeCommand(object param)
        {
            SingleMediaModel clickedFeedMediaInfo = DataModel.SingleMediaModel;
            if (clickedFeedMediaInfo.LikeCommentShare != null)
            {
                JObject pakToSend = new JObject();
                bool ImageIdToContentId = false;
                Guid NewsfeedId = clickedFeedMediaInfo.NewsFeedId;
                FeedModel feedModel = null;
                if (NewsfeedId != Guid.Empty)
                    FeedDataContainer.Instance.FeedModels.TryGetValue(NewsfeedId, out feedModel);
                Guid ContentId = clickedFeedMediaInfo.ContentId;
                Guid AlbumId = clickedFeedMediaInfo.AlbumId;
                bool Liked = (clickedFeedMediaInfo.LikeCommentShare.ILike == 0) ? true : false;
                try
                {
                    int liked = (clickedFeedMediaInfo.LikeCommentShare.ILike == 0) ? 1 : 0;
                    if (clickedFeedMediaInfo.LikeCommentShare.ILike == 0)
                    {
                        ShowLikeAnimation();
                        clickedFeedMediaInfo.LikeCommentShare.ILike = 1;
                        clickedFeedMediaInfo.LikeCommentShare.NumberOfLikes = clickedFeedMediaInfo.LikeCommentShare.NumberOfLikes + 1;
                    }
                    else
                    {
                        clickedFeedMediaInfo.LikeCommentShare.ILike = 0;
                        if (clickedFeedMediaInfo.LikeCommentShare.NumberOfLikes > 0) clickedFeedMediaInfo.LikeCommentShare.NumberOfLikes = clickedFeedMediaInfo.LikeCommentShare.NumberOfLikes - 1;
                        if (feedModel != null && feedModel.SingleMediaFeedModel != null && (feedModel.BookPostType != 3 || feedModel.BookPostType != 6 || feedModel.BookPostType != 9)
                               && feedModel.ActivityType == AppConstants.NEWSFEED_LIKED)
                        {
                            feedModel.Activist = null;
                            feedModel.ActivityType = 0;
                        }
                    }
                    bool fromFeed = clickedFeedMediaInfo.PlayedFromFeed ? true : false;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_LIKE_STATUS;
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_MULTIMEDIA;
                    pakToSend[JsonKeys.Liked] = Liked ? 1 : 0;
                    pakToSend[JsonKeys.ContentId] = ContentId;
                    pakToSend[JsonKeys.MediaType] = clickedFeedMediaInfo.MediaType;
                    if (fromFeed)
                    {
                        if (feedModel != null)
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            pakToSend[JsonKeys.NewsfeedId] = NewsfeedId;
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            pakToSend[JsonKeys.WallOwnerType] = feedModel.WallOrContentOwner.ProfileType;
                        }
                    }
                    else
                    {
                        pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                        pakToSend[JsonKeys.AlbumId] = AlbumId;
                    }
                    new ThreadLikeUnlikeAny().StartThread(pakToSend, ImageIdToContentId);
                }
                finally { }
            }
        }

        private ICommand numberofLikeCommand;
        public ICommand NumberofLikeCommand
        {
            get
            {
                if (numberofLikeCommand == null) numberofLikeCommand = new RelayCommand(param => OnNumberofLikeCommand(DataModel.SingleMediaModel));
                return numberofLikeCommand;
            }
        }
        private void OnNumberofLikeCommand(SingleMediaModel model)
        {
            if (model != null && model.ContentId != Guid.Empty && model.LikeCommentShare != null) ShowLikePopup(model.ContentId, model.NewsFeedId, model.LikeCommentShare.NumberOfLikes);
        }

        private ICommand addToAlbum;
        public ICommand AddToAlbum
        {
            get
            {
                if (addToAlbum == null) addToAlbum = new RelayCommand(param => OnAddToAlbum(DataModel.SingleMediaModel));
                return addToAlbum;
            }
        }

        private void OnAddToAlbum(SingleMediaModel model)
        {
            try
            {
                if (model != null && model.ContentId != Guid.Empty)
                {
                    UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(motherPanel);

                    UCDownloadOrAddToAlbumPopUp.Instance.OnRemovedUserControl += () =>
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance = null;
                        if (MediaPlayerInMain != null && MediaPlayerInMain.IsVisible) MediaPlayerInMain.GrabKeyBoardFocus();
                    };
                    if (model.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                    {
                        if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                        {
                            UCDownloadOrAddToAlbumPopUp.Instance.Show();
                            UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(model, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                        }
                        else if (DefaultSettings.IsInternetAvailable)
                        {
                            UCDownloadOrAddToAlbumPopUp.Instance.Show();
                            UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(model, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                            SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_AUDIO, Guid.Empty);
                        }
                    }
                    else if (model.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                    {
                        if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                        {
                            UCDownloadOrAddToAlbumPopUp.Instance.Show();
                            UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(model, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                        }
                        else if (DefaultSettings.IsInternetAvailable)
                        {
                            UCDownloadOrAddToAlbumPopUp.Instance.Show();
                            UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(model, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                            SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_VIDEO, Guid.Empty);
                        }
                    }
                }
            }
            finally { }
        }

        private ICommand mediaSendViaChat;
        public ICommand MediaSendViaChat
        {
            get
            {
                if (mediaSendViaChat == null) mediaSendViaChat = new RelayCommand(param => OnMediaSendViaChat(DataModel.SingleMediaModel));
                return mediaSendViaChat;
            }
        }
        private void OnMediaSendViaChat(SingleMediaModel model)
        {
            UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(motherPanel);
            ucMediaShareToFriendPopup.Show();
            ucMediaShareToFriendPopup.SetSingleMedia(model);
            ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                if (obj != null && obj is UCMediaPlayerInMain)
                {
                    UCMediaPlayerInMain imageViewInMain1 = (UCMediaPlayerInMain)obj;
                    imageViewInMain1.GrabKeyBoardFocus();
                }
            };
        }

        private ICommand _AddToMyChannel;
        public ICommand AddToMyChannel
        {
            get
            {
                if (_AddToMyChannel == null) _AddToMyChannel = new RelayCommand(param => OnAddToMyChannel(DataModel.SingleMediaModel));
                return _AddToMyChannel;
            }
        }
        private void OnAddToMyChannel(SingleMediaModel model)
        {
            if (UCAddToMyChannelView.Instance != null)
                motherPanel.Children.Remove(UCAddToMyChannelView.Instance);
            UCAddToMyChannelView.Instance = new UCAddToMyChannelView(motherPanel);
            UCAddToMyChannelView.Instance.Show();
            UCAddToMyChannelView.Instance.ShowChannelView(model);
            UCAddToMyChannelView.Instance.OnRemovedUserControl += () =>
            {
                UCAddToMyChannelView.Instance = null;
                var obj = ((Border)(motherPanel.Parent)).Parent;
                if (obj != null && obj is UCMediaPlayerInMain)
                {
                    UCMediaPlayerInMain imageViewInMain1 = (UCMediaPlayerInMain)obj;
                    imageViewInMain1.GrabKeyBoardFocus();
                }
            };
        }
        private ICommand mediaSendViaFacebook;
        public ICommand MediaSendViaFacebook
        {
            get
            {
                if (mediaSendViaFacebook == null) mediaSendViaFacebook = new RelayCommand(param => OnMediaSendViaFacebook(DataModel.SingleMediaModel));
                return mediaSendViaFacebook;
            }
        }

        private void OnMediaSendViaFacebook(SingleMediaModel model)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && model != null && !string.IsNullOrEmpty(model.StreamUrl))
            {
                try
                {
                    //string fbShareLink = "https://www.facebook.com/sharer/sharer.php?u=" + MediaUtility.MakeEncriptedURL(model.MediaOwner.UserIdentity, model.ContentId);
                    //Process myProcess = new Process();
                    //myProcess.StartInfo.UseShellExecute = true;
                    //myProcess.StartInfo.FileName = fbShareLink;
                    //myProcess.Start();
                    if (fbShare == null)
                    {
                        string encyptedUrl = MediaUtility.MakeEncriptedURL(model.MediaOwner.UserTableID, model.ContentId);
                        fbShare = new UCFBShare(encyptedUrl, model.MediaType, true);
                        fbShare.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                        fbShare.SetParent(motherPanel);
                        fbShare.OnRemovedUserControl += () =>
                        {
                            fbShare = null;
                        };
                        fbShare.Show();
                    }
                }
                finally { }
            }
            else UIHelperMethods.ShowWarning("Media link can not be shared to facebook right now!", "Facebook share");
            // { CustomMessageBox.ShowError("Media link can not be shared to facebook right now!"); }
        }

        private ICommand mediaCopyLink;
        public ICommand MediaCopyLink
        {
            get
            {
                if (mediaCopyLink == null) mediaCopyLink = new RelayCommand(param => OnMediaCopyLink(DataModel.SingleMediaModel));
                return mediaCopyLink;
            }
        }

        private void OnMediaCopyLink(SingleMediaModel model)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && model != null && !string.IsNullOrEmpty(model.StreamUrl))
            {
                System.Windows.Clipboard.SetText(MediaUtility.MakeEncriptedURL(model.MediaOwner.UserIdentity, model.ContentId));
                UIHelperMethods.ShowMessageWithTimerFromNonThread(motherPanel, "Media link copied to clipboard!");
            }
            else { UIHelperMethods.ShowWarning("Media link can not be copied right now!", "Media copy"); }
        }

        private ICommand reportMedia;
        public ICommand ReportMedia
        {
            get
            {
                if (reportMedia == null) reportMedia = new RelayCommand(param => OnReportMedia(DataModel.SingleMediaModel));
                return reportMedia;
            }
        }

        private void OnReportMedia(SingleMediaModel model)
        {
            if (model != null) MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(model.ContentId, StatusConstants.SPAM_MEDIA_CONTENT, model.MediaOwner.UserTableID, motherPanel);
        }

        private ICommand exitMediaPlayer;
        public ICommand ExitMediaPlayer
        {
            get
            {
                if (exitMediaPlayer == null) exitMediaPlayer = new RelayCommand(param => OnExitMediaPlayer());
                return exitMediaPlayer;
            }
        }
        private void OnExitMediaPlayer()
        {
            if (MediaPlayerInMain != null) MediaPlayerInMain.OnExit();
        }

        private ICommand _MediaDownloadCommand;
        public ICommand MediaDownloadCommand
        {
            get
            {
                if (_MediaDownloadCommand == null) _MediaDownloadCommand = new RelayCommand(param => OnMediaDownloadCommand(param));
                return _MediaDownloadCommand;
            }
        }
        private void OnMediaDownloadCommand(Object parameter)
        {
            try
            {
                if (parameter is SingleMediaModel)
                {
                    SingleMediaModel singleMediaModel = (SingleMediaModel)parameter;
                    if (singleMediaModel.DownloadState == StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE)
                    {
                        //MakeDownloaderStart(singleMediaModel);
                        if (UCDownloadPopup.Instance != null)
                            motherPanel.Children.Remove(UCDownloadPopup.Instance);
                        UCDownloadPopup.Instance = new UCDownloadPopup(motherPanel);
                        UCDownloadPopup.Instance.Show();
                        UCDownloadPopup.Instance.ShowPopup(singleMediaModel, downloadBtn, () =>
                        {
                            return 0;
                        });
                        UCDownloadPopup.Instance.OnRemovedUserControl += () =>
                        {
                            UCDownloadPopup.Instance = null;
                            if (MediaPlayerInMain != null && UCDownloadOrAddToAlbumPopUp.Instance == null) MediaPlayerInMain.GrabKeyBoardFocus();
                        };
                    }
                    else if (singleMediaModel.DownloadState == StatusConstants.MEDIA_DOWNLOAD_PAUSE_STATE)
                    {
                        CustomFileDownloader fileDownloader = null;
                        if (!MediaDataContainer.Instance.CurrentDownloads.TryGetValue(singleMediaModel.ContentId, out fileDownloader))
                        {
                            fileDownloader = new CustomFileDownloader(singleMediaModel);
                            MediaDataContainer.Instance.CurrentDownloads[singleMediaModel.ContentId] = fileDownloader;
                        }
                        if (!RingIDViewModel.Instance.UploadingModelList.Any(p => p.ContentId == singleMediaModel.ContentId))
                        {
                            UploadingModel _uploadingModel = new UploadingModel();
                            _uploadingModel.IsDownload = true;
                            _uploadingModel.ContentId = singleMediaModel.ContentId;
                            _uploadingModel.UploadingContent = singleMediaModel.MediaType == 1 ? "1 audio is downloading..." : "1 video is downloading...";
                            RingIDViewModel.Instance.UploadingModelList.Add(_uploadingModel);
                        }
                        if (!fileDownloader.CanResume)
                        {
                            FileDownloader.FileInfo file = fileDownloader.Files[0];
                            if (System.IO.File.Exists(fileDownloader.LocalDirectory + "\\" + file.Name))
                            {
                                long length = new System.IO.FileInfo(fileDownloader.LocalDirectory + "\\" + file.Name).Length;
                                fileDownloader.Start(length);
                            }
                            else
                            {
                                UIHelperMethods.ShowWarning(NotificationMessages.PAUSED_MEDIA_FILE_NOT_FOUND);
                                //singleMediaModel.DeleteDownloadMedia(singleMediaModel.ContentId);
                                singleMediaModel.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                                singleMediaModel.DownloadProgress = 0;
                                MediaDAO.Instance.DeleteFromDownloadedMediasTable(singleMediaModel.ContentId);
                                //TODO delete downloaded files from PC
                            }
                        }
                        else fileDownloader.Resume();
                    }
                    else if (singleMediaModel.DownloadState == StatusConstants.MEDIA_DOWNLOADING_STATE)
                    {
                        //Open popup for Pause/Cancel options
                        MainSwitcher.PopupController.FeedDownloadPauseCancelPopup.Show(singleMediaModel, downloadBtn);
                    }
                }
            }
            catch (System.Exception ex)
            { 
                //log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); 
            }
        }
        #endregion

        #region "Utility Methods"
        private UCMediaPlayerInMain MediaPlayerInMain
        {
            get
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                if (obj != null && obj is UCMediaPlayerInMain)
                {
                    return (UCMediaPlayerInMain)obj;
                }
                return null;
            }
        }

        //private string makeMediaUrl(SingleMediaModel model)
        //{
        //    EncryptForShare encrypt = new EncryptForShare();
        //    byte[] ip = encrypt.ipToByteArray(ServerAndPortSettings.AUTH_SERVER_IP);
        //    byte[] port = encrypt.intToByteArray(ServerAndPortSettings.COMMUNICATION_PORT);
        //    byte[] mediaId = System.Text.Encoding.UTF8.GetBytes(model.ContentId.ToString());//"1658975684256".getBytes();
        //    byte[] finalByte = new byte[ip.Length + port.Length + mediaId.Length];
        //    System.Buffer.BlockCopy(ip, 0, finalByte, 0, ip.Length);
        //    System.Buffer.BlockCopy(port, 0, finalByte, ip.Length, port.Length);
        //    System.Buffer.BlockCopy(mediaId, 0, finalByte, ip.Length + port.Length, mediaId.Length);
        //    return ServerAndPortSettings.MediaShareBase + encrypt.bytesToHex(encrypt.encrypt(finalByte));
        //}

        public void ShowLikePopup(Guid contentID, Guid nfID, long numerOflike)
        {
            if (LikeListView != null)
                motherPanel.Children.Remove(LikeListView);
            LikeListView = new UCLikeList(motherPanel);
            LikeListView.ContentId = contentID;
            LikeListView.NewsfeedId = nfID;
            LikeListView.NumberOfLike = numerOflike;
            LikeListView.Show();
            LikeListView.SendLikeListRequest();
            LikeListView.OnRemovedUserControl += () =>
            {
                LikeListView = null;
                if (MediaPlayerInMain != null) MediaPlayerInMain.GrabKeyBoardFocus();
            };
        }

        public bool HideLikePopup()
        {
            if (LikeListView != null)
            {
                LikeListView.LikeList.Clear();
                LikeListView.Hide();
                return true;
            }
            LikeListView = null;
            return false;
        }

        private void likeHide()
        {
            Thread.Sleep(2000);
            Application.Current.Dispatcher.Invoke(() =>
            {
                DataModel.LikeAnimationloader = null;
                likeAnimationGrid.Visibility = Visibility.Collapsed;
            });
        }

        private void ShowLikeAnimation()
        {
            if (DataModel.LikeAnimationloader == null)
            {
                DataModel.LikeAnimationloader = ImageLocation.LOADER_LIKE_ANIMATION;
                likeAnimationGrid.Visibility = Visibility.Visible;
                Thread t = new Thread(likeHide);
                t.Start();
            }
            else
            {
                DataModel.LikeAnimationloader = null;
                likeAnimationGrid.Visibility = Visibility.Collapsed;
            }
        }
        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

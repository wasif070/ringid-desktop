﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using View.Utility;
using View.Utility.RingPlayer;

namespace View.UI.MediaPlayer
{
    /// <summary>
    /// Interaction logic for WNSaparatePlayer.xaml
    /// </summary>
    public partial class WNSeparatePlayer : Window, INotifyPropertyChanged
    {
        #region "Fields"
        public delegate void WindowClosed();
        public event WindowClosed OnWindowClosed;
        public bool IsNormalExitingPlayer = false;
        #endregion

        #region "Ctors"
        public WNSeparatePlayer(VMPlayer dataModel)
        {
            SizeChanged += (o, e) =>
           {
               var r = SystemParameters.WorkArea;
               Left = r.Right - ActualWidth;
               Top = r.Bottom - ActualHeight;
           };
            InitializeComponent();
            ResizeMode = ResizeMode.NoResize;
            this.Closed += WNSmallMediaPlayer_Closed;
            this.MouseDown += Window_MouseDown;
            this.StateChanged += new EventHandler(WNMediaSeparateScreen_StateChanged);
            this.DataModel = dataModel;
            this.DataContext = this;
        }


        #endregion "Ctors"

        #region "Event Trigger"

        void WNSmallMediaPlayer_Closed(object sender, EventArgs e)
        {
            this.MouseDown -= Window_MouseDown;
            this.StateChanged -= new EventHandler(WNMediaSeparateScreen_StateChanged);
            if (OnWindowClosed != null)
                OnWindowClosed();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) DragMove();
        }

        void WNMediaSeparateScreen_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized) this.Topmost = false;
            else if (this.WindowState == WindowState.Maximized)
            {
                this.Topmost = true;
                this.Activate();
                this.Focus();
            }
            else if (this.WindowState == WindowState.Normal) this.Topmost = false;
        }

        #endregion "Event Trigger"

        #region "Properties"
        private VMPlayer dataModel;
        public VMPlayer DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion""

        #region "Utiity Methods"
        public void ShowThisWindow(UserControl userPlayer)
        {
            this.Show();
            Keyboard.Focus(this);
            _PlayerContainer.Children.Add(userPlayer);
        }

        public void CloseThisWindow()
        {
            IsNormalExitingPlayer = true;
            this.Close();
        }
        #endregion"Utiity Methods"

        #region "ICommand"

        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        public void OnEscapeCommand()
        {
            if (this.WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
                DataModel.PlayerViewType = ViewConstants.SMALL_SCREEN;
            }
            else if (this.WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Minimized;
                DataModel.PlayerViewType = ViewConstants.FULL_SCREEN;
            }
        }
        private ICommand spacePressedCommand;
        public ICommand SpacePressedCommand
        {
            get
            {
                if (spacePressedCommand == null) spacePressedCommand = new RelayCommand(param => OnSpacePressed());
                return spacePressedCommand;
            }
        }

        private void OnSpacePressed()
        {
            DataModel.OnSingleClickOnScreen();
        }

        #endregion"ICommand"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Models.Constants;
using View.BindingModels;
using View.Utility;
using View.Utility.Feed;
using View.Utility.RingPlayer;

namespace View.UI.MediaPlayer
{
    /// <summary>
    /// Interaction logic for UCOnlyMediaPlayer.xaml
    /// </summary>
    public partial class UCOnlyMediaPlayer : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private Grid motherPanel = null;
        private DispatcherTimer mediaPositionChangeTimer;
        private double previousTime;
        public delegate void CurrentMediaEnded();
        public event CurrentMediaEnded OnCurrentMediaEnded;
        public delegate void LoadedMediaPlayer();
        public event LoadedMediaPlayer OnLoadedMediaPlayer;
        #endregion "Fields"

        #region "Ctors"
        public UCOnlyMediaPlayer(VMPlayer DataModel, Grid motherPanel)
        {
            InitializeComponent();
            this.DataModel = DataModel;
            this.DataContext = this;
            this.motherPanel = motherPanel;
        }
        #endregion "Ctors"

        #region "Properties"
        private VMPlayer dataModel;
        public VMPlayer DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private bool _IsThumbViwe = true;
        public bool IsThumbViwe
        {
            get { return _IsThumbViwe; }
            set { _IsThumbViwe = value; OnPropertyChanged("IsThumbViwe"); }
        }
        public WNSeparatePlayer SeparatePlayer { get; set; }
        private UCMediaProgressBarAndVolumn PlayerButtonsAndSliders { get; set; }
        #endregion "Properties"

        #region "Commands and Command Methods"

        private ICommand loadedMainControl;
        public ICommand LoadedMainControl
        {
            get
            {
                if (loadedMainControl == null) loadedMainControl = new RelayCommand(param => OnLoadedMainControl());
                return loadedMainControl;
            }
        }
        private void OnLoadedMainControl()
        {
            _MediaElement.MediaOpened += _MediaElement_MediaOpened;
            _MediaElement.MediaEnded += _MediaElement_MediaEnded;
            _MediaElement.BufferingStarted += _MediaElement_BufferingStarted;
            _MediaElement.BufferingEnded += _MediaElement_BufferingEnded;
            _MediaElement.MediaFailed += _MediaElement_MediaFailed;
            _ButtonsAndSlider.Child = null;
            PlayerButtonsAndSliders = new UCMediaProgressBarAndVolumn(DataModel, motherPanel);
            _ButtonsAndSlider.Child = PlayerButtonsAndSliders;
            DataModel.PlayRequested += (d, es) =>
            {
                try
                {
                    DataModel.PlayingState = ViewConstants.MEDIA_PLAYING;
                    this._MediaElement.Play();

                    if (mediaPositionChangeTimer == null)
                    {
                        mediaPositionChangeTimer = new DispatcherTimer();
                        mediaPositionChangeTimer.Interval = TimeSpan.FromSeconds(0.5);
                        mediaPositionChangeTimer.Tick += new EventHandler(mediaPositionChangeTimer_Tick);
                    }
                    if (!mediaPositionChangeTimer.IsEnabled)
                        mediaPositionChangeTimer.Start();
                }
                catch (Exception ex) 
                {
                }
            };
            DataModel.PauseRequested += ((sdr, ex) =>
            {
                DataModel.PlayingState = ViewConstants.MEDIA_PAUSED;
                _MediaElement.Pause();
            });
            DataModel.CloseRequested += ((sdr, ex) =>
            {
                _MediaElement.Close();
                if (mediaPositionChangeTimer != null)
                    mediaPositionChangeTimer.Stop();
                DataModel.PlayingState = ViewConstants.MEDIA_PAUSED;
            });

            if (OnLoadedMediaPlayer != null) OnLoadedMediaPlayer();
        }

        private ICommand unLoadedMainControl;
        public ICommand UnLoadedMainControl
        {
            get
            {
                if (unLoadedMainControl == null) unLoadedMainControl = new RelayCommand(param => OnUnLoadedMainControl());
                return unLoadedMainControl;
            }
        }
        private void OnUnLoadedMainControl()
        {
            _MediaElement.MediaOpened -= _MediaElement_MediaOpened;
            _MediaElement.MediaEnded -= _MediaElement_MediaEnded;
            _MediaElement.BufferingStarted -= _MediaElement_BufferingStarted;
            _MediaElement.BufferingEnded -= _MediaElement_BufferingEnded;
            _MediaElement.MediaFailed -= _MediaElement_MediaFailed;
        }

        private ICommand mouseEnterCommand;
        public ICommand MouseEnterCommand
        {
            get
            {
                if (mouseEnterCommand == null) mouseEnterCommand = new RelayCommand(param => OnMouseEnterCommand());
                return mouseEnterCommand;
            }
        }
        private void OnMouseEnterCommand()
        {
            _ButtonsAndSlider.Visibility = Visibility.Visible;
            _TopPanel.Visibility = Visibility.Visible;
        }

        private ICommand mouseLeaveCommand;
        public ICommand MouseLeaveCommand
        {
            get
            {
                if (mouseLeaveCommand == null) mouseLeaveCommand = new RelayCommand(param => OnMouseLeaveCommand());
                return mouseLeaveCommand;
            }
        }
        private void OnMouseLeaveCommand()
        {
            if (!_ButtonsAndSlider.IsMouseOver) _ButtonsAndSlider.Visibility = Visibility.Hidden;
            if (!_TopPanel.IsMouseOver) _TopPanel.Visibility = Visibility.Hidden;
        }

        private ICommand goToFullScreen;
        public ICommand GoToFullScreen
        {
            get
            {
                if (goToFullScreen == null) goToFullScreen = new RelayCommand(param => OnGoToFullScreen());
                return goToFullScreen;
            }
        }
        public void OnGoToFullScreen()
        {
            DataModel.PlayerViewType = ViewConstants.FULL_SCREEN;
            if (MediaPlayerInMain != null) { MediaPlayerInMain.RemovePlayerFromGuiRingID(); }
            showSeparateScreen();
            SeparatePlayer.WindowState = WindowState.Maximized;
        }

        private ICommand goToSmallScreen;
        public ICommand GoToSmallScreen
        {
            get
            {
                if (goToSmallScreen == null) goToSmallScreen = new RelayCommand(param => OnGoToSmallScreen());
                return goToSmallScreen;
            }
        }
        public void OnGoToSmallScreen()
        {
            DataModel.PlayerViewType = ViewConstants.SMALL_SCREEN;
            if (MediaPlayerInMain != null) { MediaPlayerInMain.RemovePlayerFromGuiRingID(); }
            showSeparateScreen();
            SeparatePlayer.WindowState = WindowState.Normal;
        }

        private ICommand minimizeSmallPlayer;
        public ICommand MinimizeSmallPlayer
        {
            get
            {
                if (minimizeSmallPlayer == null) minimizeSmallPlayer = new RelayCommand(param => OnMinimizeSmallPlayer());
                return minimizeSmallPlayer;
            }
            set { minimizeSmallPlayer = value; }
        }
        private void OnMinimizeSmallPlayer()
        {
            if (SeparatePlayer != null) SeparatePlayer.WindowState = WindowState.Minimized;
        }

        private ICommand showPlayerInPopUP;
        public ICommand ShowPlayerInPopUP
        {
            get
            {
                if (showPlayerInPopUP == null) showPlayerInPopUP = new RelayCommand(param => OnShowPlayerInPopUP());
                return showPlayerInPopUP;
            }
        }
        private void OnShowPlayerInPopUP()
        {
            if (SeparatePlayer != null)
            {
                SeparatePlayer.CloseThisWindow();
                SeparatePlayer = null;
            }
            if (MediaPlayerInMain != null) { MediaPlayerInMain.ShowPlayerInMainView(); }
        }

        private ICommand doubleClickOnScreen;
        public ICommand DoubleClickOnScreen
        {
            get
            {
                if (doubleClickOnScreen == null) doubleClickOnScreen = new RelayCommand(param => OnDoubleClickOnScreen());
                return doubleClickOnScreen;
            }
        }
        private void OnDoubleClickOnScreen()
        {
            if (DataModel.PlayerViewType == ViewConstants.FULL_SCREEN) OnGoToSmallScreen();
            else OnGoToFullScreen();
        }

        private ICommand singleClickOnScreen;
        public ICommand SingleClickOnScreen
        {
            get
            {
                if (singleClickOnScreen == null) singleClickOnScreen = new RelayCommand(param => OnSingleClickOnScreen());
                return singleClickOnScreen;
            }
        }

        private void OnSingleClickOnScreen()
        {
            DataModel.OnSingleClickOnScreen();
        }

        private ICommand exitMediaPlayer;
        public ICommand ExitMediaPlayer
        {
            get
            {
                if (exitMediaPlayer == null) exitMediaPlayer = new RelayCommand(param => OnExitMediaPlayer());
                return exitMediaPlayer;
            }
        }
        private void OnExitMediaPlayer()
        {
            if (SeparatePlayer != null) SeparatePlayer.CloseThisWindow();
            if (MediaPlayerInMain != null) MediaPlayerInMain.OnExit();
        }

        #endregion

        #region "Event Trigger"

        void _MediaElement_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            //_MediaElement.Pause();
        }

        void _MediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            IsThumbViwe = false;
            onOpenAMedia(DataModel.SingleMediaModel);
        }

        void _MediaElement_BufferingEnded(object sender, RoutedEventArgs e)
        {
            if (DataModel != null) DataModel.IsBuffering = false;
        }

        void _MediaElement_BufferingStarted(object sender, RoutedEventArgs e)
        {
            if (DataModel != null) DataModel.IsBuffering = true;
        }

        void _MediaElement_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (OnCurrentMediaEnded != null) OnCurrentMediaEnded();
        }

        private void mediaPositionChangeTimer_Tick(object sender, EventArgs e)
        {
            MediaState state = GetMediaState(_MediaElement);
            if (previousTime == _MediaElement.Position.TotalSeconds) DataModel.IsBuffering = true;
            else if (!DataModel.IsSliderDragging)
            {
                previousTime = _MediaElement.Position.TotalSeconds;
                DataModel.IsBuffering = false;
                switch (state)
                {
                    case MediaState.Close:
                        break;
                    case MediaState.Manual:
                        break;
                    case MediaState.Pause:
                        DataModel.PlayingState = ViewConstants.MEDIA_PAUSED;
                        break;
                    case MediaState.Play:
                        DataModel.SeekBarValue = _MediaElement.Position.TotalSeconds;
                        DataModel.PlayingState = ViewConstants.MEDIA_PLAYING;
                        break;
                    case MediaState.Stop:
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion "Event Trigger"

        #region "Private methods"

        private UCMediaPlayerInMain MediaPlayerInMain
        {
            get
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                if (obj != null && obj is UCMediaPlayerInMain) return (UCMediaPlayerInMain)obj;
                return null;
            }
        }

        private void showSeparateScreen()
        {
            if (SeparatePlayer == null)
            {
                SeparatePlayer = new WNSeparatePlayer(DataModel);
                SeparatePlayer.OnWindowClosed += () =>
                {
                    if (!SeparatePlayer.IsNormalExitingPlayer && MediaPlayerInMain != null) MediaPlayerInMain.OnExit();
                    SeparatePlayer = null;
                };
            }
            HelperMethods.RemoveAUserControlFromAnotherPanel(this);
            SeparatePlayer.ShowThisWindow(this);
        }

        private MediaState GetMediaState(MediaElement myMedia)
        {
            FieldInfo hlp = typeof(MediaElement).GetField("_helper", BindingFlags.NonPublic | BindingFlags.Instance);
            object helperObject = hlp.GetValue(myMedia);
            FieldInfo stateField = helperObject.GetType().GetField("_currentState", BindingFlags.NonPublic | BindingFlags.Instance);
            MediaState state = (MediaState)stateField.GetValue(helperObject);
            return state;
        }

        private void onOpenAMedia(SingleMediaModel singleMediaModel)
        {
            if (singleMediaModel.ContentId != Guid.Empty)
            {
                singleMediaModel.LastPlayedTime = Models.Utility.ModelUtility.CurrentTimeMillis();
                if (DefaultSettings.IsInternetAvailable && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                    new ThradIncreaseMediaViewCount().StartThread(singleMediaModel.MediaOwner.UserTableID, singleMediaModel.MediaType, singleMediaModel.AlbumId, singleMediaModel.ContentId);
            }
        }

        #endregion "Private methods"

        #region "Utility Methods"

        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
﻿using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using Models.Constants;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility;
using View.Utility.Auth;
using View.Utility.RingPlayer;
using View.ViewModel;
using System;

namespace View.UI.MediaPlayer
{
    /// <summary>
    /// Interaction logic for UCMediaPlayList.xaml
    /// </summary>
    public partial class UCMediaPlayList : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private Grid motherPanel = null;
        #endregion "Fields"

        #region "Ctors"
        public UCMediaPlayList(VMPlayer DataModel, Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            this.DataModel = DataModel;
            this.motherPanel = motherGrid;
        }
        #endregion "Ctors"

        #region "Properties"
        private VMPlayer dataModel;
        public VMPlayer DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion "Properties"

        #region "Commands and Command Methods"

        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private void OnLoadedControl()
        {

        }

        private ICommand showMoreMediaClicked;
        public ICommand ShowMoreMediaClicked
        {
            get
            {
                if (showMoreMediaClicked == null) showMoreMediaClicked = new RelayCommand(param => OnShowMoreMedia());
                return showMoreMediaClicked;
            }
        }
        private void OnShowMoreMedia()
        {
            LoadMoreMedia();
        }

        private ICommand singleMediaModelClicked;
        public ICommand SingleMediaModelClicked
        {
            get
            {
                if (singleMediaModelClicked == null) singleMediaModelClicked = new RelayCommand(param => OnSingleMediaClickCommand(param));
                return singleMediaModelClicked;
            }
        }
        public void OnSingleMediaClickCommand(object param)
        {
            if (MediaPlayerInMain != null) MediaPlayerInMain.OnSingleMediaClickCommand(param);
        }
        private ICommand exitMediaPlayer;
        public ICommand ExitMediaPlayer
        {
            get
            {
                if (exitMediaPlayer == null) exitMediaPlayer = new RelayCommand(param => OnExitMediaPlayer());
                return exitMediaPlayer;
            }
        }
        private void OnExitMediaPlayer()
        {
            if (MediaPlayerInMain != null) MediaPlayerInMain.OnExit();
        }

        #endregion

        #region "Utility Methods"

        private UCMediaPlayerInMain MediaPlayerInMain
        {
            get
            {
                var obj = ((Border)(motherPanel.Parent)).Parent;
                if (obj != null && obj is UCMediaPlayerInMain)
                {
                    return (UCMediaPlayerInMain)obj;
                }
                return null;
            }
        }
        /// <summary>
        /// Load More media from
        /// 1.running album
        /// 2. and then recent
        /// 3. and then  MediaDataContainer.Instance.ContentModels
        /// </summary>
        public void LoadMoreMedia()
        {
            if (!DataModel.IsPlaylistLoading)
            {
                bool success = false;
                string message = string.Empty;
                new Thread(() =>
                {
                    try
                    {
                        DataModel.IsPlaylistLoading = true;
                        if (DefaultSettings.IsInternetAvailable && DataModel.SingleMediaModel.AlbumId != Guid.Empty && DataModel.NewsFeedID == Guid.Empty)
                        {
                            JObject obj = new JObject();
                            obj[JsonKeys.UserTableID] = DataModel.SingleMediaModel.UserTableID;
                            obj[JsonKeys.MediaType] = DataModel.SingleMediaModel.MediaType;
                            obj[JsonKeys.AlbumId] = DataModel.SingleMediaModel.AlbumId;
                            //obj[JsonKeys.StartLimit] = DataModel.MediaList.Count;
                            obj[JsonKeys.AlbumName] = DataModel.SingleMediaModel.AlbumName;
                            obj[JsonKeys.Scroll] = 2;
                            obj[JsonKeys.PivotUUID] = (DataModel.MediaList.Count > 0) ? DataModel.MediaList.Min(item => item.ContentId) : Guid.Empty;
                            SendDataToServer.AuthRequestMethod(obj, AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST, AppConstants.REQUEST_TYPE_REQUEST, out success, out message);
                        }
                        if (!success || DataModel.MediaList.Count < 10) AddFromLocalLists();
                    }
                    finally
                    {
                        DataModel.IsPlaylistLoading = false;
                    }
                }).Start();
            }
        }
        /// <summary>
        /// Add 10 SingleMedia From Recent Play list or MediaDataContainer.Instance.ContentModels
        /// </summary>
        public void AddFromLocalLists()
        {
            int i = 0;
            try
            {
                if (!DataModel.IsAddedAllFromRecentPlayList)
                {
                    lock (RingIDViewModel.Instance.MyRecentPlayList)
                    {
                        foreach (SingleMediaModel model in RingIDViewModel.Instance.MyRecentPlayList.ToList())
                        {
                            System.Threading.Thread.Sleep(10);
                            if (!DataModel.MediaList.Any(P => P.ContentId == model.ContentId))
                            {
                                DataModel.MediaList.InvokeAdd(model);
                                i++;
                                if (i == 10) break;
                            }
                        }
                        if (i == 0) DataModel.IsAddedAllFromRecentPlayList = true;
                    }
                    if (DataModel.MediaList.Count < 10) mediaFromMainDictionary();
                }
                else mediaFromMainDictionary();
            }
            finally
            {
                DataModel.PlayingIndex = DataModel.PlayingIndex;// for enable disable buttons
            }
        }

        private void mediaFromMainDictionary()
        {
            int i = 0;
            foreach (var modlePair in View.Utility.DataContainer.MediaDataContainer.Instance.ContentModels.ToList())
            {
                System.Threading.Thread.Sleep(10);
                if (!DataModel.MediaList.Any(P => P.ContentId == modlePair.Key))
                {
                    DataModel.MediaList.InvokeAdd(modlePair.Value);
                    i++;
                    if (i == 10) break;
                }
            }
            if (i == 0) DataModel.ShowLoadMore = false;
        }

        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

﻿using Models.Constants;
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using View.UI.Comment;
using View.Utility;
using View.Utility.RingPlayer;

namespace View.UI.MediaPlayer
{
    /// <summary>
    /// Interaction logic for UCMediaOldAndNewComments.xaml
    /// </summary>
    public partial class UCMediaOldAndNewComments : UserControl
    {
        #region "Fields"
        private Grid _motherPanel = null;
        #endregion "Fields"

        #region "Ctors"
        public UCMediaOldAndNewComments(VMPlayer DataModel, Grid motherGrid)
        {
            InitializeComponent();
            this.DataContext = this;
            this.DataModel = DataModel;
            this.motherPanel = motherGrid;
        }
        #endregion "Ctors"

        #region "Properties"
        public UCNewCommentPanel NewCommentPanel { get; set; }
        private VMPlayer dataModel;
        public VMPlayer DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        public Grid motherPanel
        {
            get
            {
                return _motherPanel;
            }
            set
            {
                if (_motherPanel == value)
                {
                    return;
                }
                _motherPanel = value;
                OnPropertyChanged("motherPanel");
            }
        }
        #endregion "Properties"

        #region "Commands and Command Methods"
        private ICommand showMoreComments;
        public ICommand ShowMoreComments
        {
            get
            {
                if (showMoreComments == null) showMoreComments = new RelayCommand(param => OnShowMoreComments(param));
                return showMoreComments;
            }
        }
        private void OnShowMoreComments(object param)
        {
            if (!DataModel.IsLoadingComments && DataModel.SingleMediaModel.ContentId != Guid.Empty)
            {
                DataModel.IsLoadingComments = true;
                ThreadStart mediaMoreComments = delegate
                {
                    long time = 0;
                    int scroll = 0;
                    Guid pvtUUID = Guid.Empty;
                    if (DataModel.CommentList.Count > 0)
                    {
                        time = DataModel.CommentList.ElementAt(DataModel.CommentList.Count - 1).Time;
                        pvtUUID = DataModel.CommentList.ElementAt(DataModel.CommentList.Count - 1).CommentId;
                    }
                    if (time > 0)
                    {
                        scroll = StatusConstants.SCROLL_OLDER;
                    }
                    bool commentsReq = new MediaRequests().CommentReqeust(DataModel.NewsFeedID, DataModel.SingleMediaModel.ContentId, pvtUUID, time, scroll);// scl =1 for previous scroll
                    DataModel.IsLoadingComments = false;
                };
                new Thread(mediaMoreComments).Start();
            }
        }

        #endregion

        #region "Utility Methods"
        public void AddNewCommentUserControl()
        {
            try
            {
                if (NewCommentPanel != null)
                {
                    _NewCommentPanel.Child = null;
                }
                NewCommentPanel = new UCNewCommentPanel();
                NewCommentPanel.motherPanel = motherPanel;
                NewCommentPanel.SetNeedToFocus(false);
                _NewCommentPanel.Child = NewCommentPanel;
                ChangeSingleModel(DataModel.SingleMediaModel.ContentId, DataModel.NewsFeedID);
            }
            catch (Exception)
            {
            }
        }
        private void ChangeSingleModel(Guid ContentID, Guid newsfeedId)
        {
            if (NewCommentPanel != null)
            {
                NewCommentPanel.newsfeedId = newsfeedId;
                NewCommentPanel.imageId = Guid.Empty;
                NewCommentPanel.contentId = ContentID;
            }
        }
        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

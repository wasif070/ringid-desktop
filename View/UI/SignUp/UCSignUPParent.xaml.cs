﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using View.Constants;
using View.UI.PopUp;
using View.UI.RecoverPassword;
using View.UI.SocialMedia;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.SignUp
{
    /// <summary>
    /// Interaction logic for UCSignUPParent.xaml
    /// </summary>
    public partial class UCSignUPParent : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCSignUPParent).Name);
        Grid motherPanel = null;
        Border signUpBorder = null;
        public event DelegateNoParam OnBackButtonClicked;
        #endregion

        #region"Ctors"
        public UCSignUPParent(VMRingIDMainWindow model, Grid motherGrid, Border signUpBorder)
        {
            InitializeComponent();
            this.DataModel = model;
            this.motherPanel = motherGrid;
            this.signUpBorder = signUpBorder;
            this.DataContext = this;
        }
        #endregion"Ctors"

        #region "Properties"
        private UCSignUPWithMail SignUPWithMail { get; set; }
        private UCFBAuthentication FBAuthentication { get; set; }
        private UCWaitForDigitVarification WaitForDigitVarification { get; set; }
        private UCTwitterUI TwitterUI { get; set; }
        private UCFullNamePassword FullNamePassword { get; set; }
        private UCSignUPSuccess SignUPSuccess { get; set; }
        private UCRecoverPasswordParent RecoverPasswordParent { get; set; }
        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private string typeText;
        public string TypeText
        {
            get { return this.typeText; }
            set { this.typeText = value; this.OnPropertyChanged("TypeText"); }
        }

        private int btnShowType;
        public int BtnShowType
        {
            get { return btnShowType; }
            set
            {
                if (value == btnShowType) return;
                btnShowType = value; OnPropertyChanged("BtnShowType");
            }
        }

        #endregion

        #region "ICommands and Command Methods"

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            getFocusOnUserControl();
            if (DataModel.LoginType == SettingsConstants.EMAIL_LOGIN)
            {
                expanEmailPanel();
            }
        }

        private ICommand expanCollaspViewCommand;
        public ICommand ExpanCollaspViewCommand
        {
            get
            {
                if (expanCollaspViewCommand == null) expanCollaspViewCommand = new RelayCommand(param => OnExpanCollaspView(param));
                return expanCollaspViewCommand;
            }
        }
        private void OnExpanCollaspView(object param)
        {
            int parseType = Convert.ToInt32(param);
            DataModel.LoginType = parseType;
            if (parseType == SettingsConstants.EMAIL_LOGIN)
            {
                expanEmailPanel();
            }
            else if (parseType == SettingsConstants.MOBILE_LOGIN)
            {
                if (WaitForDigitVarification == null)
                {
                    WaitForDigitVarification = new UCWaitForDigitVarification();
                    WaitForDigitVarification.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 0.0);//background Color Transparent Black
                    WaitForDigitVarification.SetParent(motherPanel);
                    WaitForDigitVarification.OnCompletedDigitsVarifications += (successfullyVerified, mbl, mblDc, previousRingId) =>
                    {
                        WaitForDigitVarification = null;
                        if (successfullyVerified)
                        {
                            if (string.IsNullOrEmpty(previousRingId))
                            {
                                DataModel.RingID = DefaultSettings.VALUE_NEW_USER_NAME.ToString();
                                loadFullNameProfilePanel(SettingsConstants.MOBILE_LOGIN, null, null);
                            }
                            else
                            {
                                //MessageBoxResult result = CustomMessageBox.ShowWarning("You have another ringID account associated with this number! Press OK to Recover this account or Cancel to Continue stop.", "", true);
                                //if (result == MessageBoxResult.OK)
                                //{
                                WNConfirmationView cv = new WNConfirmationView("Leave confirmation!", "You have another ringID account associated with this number! Press \"Recover\" to recover this account or \"Continue\" to continue sign up.", CustomConfirmationDialogButtonOptions.YesNoCancel, new[] { "Recover", "Continue", "Cancel" });
                                var result = cv.ShowCustomDialog();
                                if (result == ConfirmationDialogResult.Yes)
                                {
                                    List<string> sgnss = new List<string>();
                                    sgnss.Add(mblDc + "-" + mbl);
                                    sgnss.Add(previousRingId);
                                    loadRecovaryPanel(sgnss);
                                }
                                else if (result == ConfirmationDialogResult.No)
                                {
                                    DataModel.RingID = DefaultSettings.VALUE_NEW_USER_NAME.ToString();
                                    loadFullNameProfilePanel(SettingsConstants.MOBILE_LOGIN, null, null);
                                }
                            }
                        }
                        else DataModel.ShowErrorMessage(NotificationMessages.FAILDED_TRY_AGAIN);
                    };
                    WaitForDigitVarification.Show();
                }
            }
            else if (parseType == SettingsConstants.FACEBOOK_LOGIN)
            {
                if (FBAuthentication == null)
                {
                    FBAuthentication = new UCFBAuthentication(SettingsConstants.TYPE_FROM_SIGNUP);
                    FBAuthentication.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                    FBAuthentication.SetParent(motherPanel);
                    FBAuthentication.OnRemovedUserControl += () =>
                    {
                        getFocusOnUserControl();
                        FBAuthentication = null;
                    };
                    FBAuthentication.OnFBAuthenticationCompleted += (typeToSwitch, fbName, fbProfileImage, fbId, accessToken, ringID, message) => onSocialMediaCallBack(parseType, typeToSwitch, fbName, fbProfileImage, fbId, accessToken, ringID, message);
                    FBAuthentication.Show();
                }
            }
            else if (parseType == SettingsConstants.TWITTER_LOGIN)
            {
                if (TwitterUI == null)
                {
                    TwitterUI = new UCTwitterUI(SettingsConstants.TYPE_FROM_SIGNUP);
                    TwitterUI.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                    TwitterUI.SetParent(motherPanel);
                    TwitterUI.OnRemovedUserControl += () =>
                    {
                        getFocusOnUserControl();
                        TwitterUI = null;
                    };
                    TwitterUI.OnThreadCompleted += (typeToSwitch, screenName, profileImage, socialMediaID, inputToken, ringID, message) => onSocialMediaCallBack(parseType, typeToSwitch, screenName, profileImage, socialMediaID, inputToken, ringID, message);
                    TwitterUI.Show();
                }
            }
        }

        private ICommand backButtonCommand;
        public ICommand BackButtonCommand
        {
            get
            {
                if (backButtonCommand == null) backButtonCommand = new RelayCommand(param => OnBackButton(param));
                return backButtonCommand;
            }
        }
        private void OnBackButton(object param)
        {
            OnBackButtonClicked();
        }

        private ICommand enterKeyCommand;
        public ICommand EnterKeyCommand
        {
            get
            {
                if (enterKeyCommand == null) enterKeyCommand = new RelayCommand(param => OnEnterKeyCommand(param));
                return enterKeyCommand;
            }
        }
        private void OnEnterKeyCommand(object param)
        {
            DataModel.LoginType = SettingsConstants.EMAIL_LOGIN;
            expanEmailPanel();
        }
        #endregion"ICommands and Command Methods"

        #region "Utility Methods"

        private void expanEmailPanel()
        {
            if (_SendCodeEmailBorderPanel.Child == null)
            {
                if (SignUPWithMail == null)
                {
                    SignUPWithMail = new UCSignUPWithMail(DataModel);
                    SignUPWithMail.IsEnableEmailEdit = true;
                    SignUPWithMail.RecoveryByEmail = false;
                    SignUPWithMail.OnBackButtonClicked += (success, string1, string2) =>
                    {
                        if (!success)
                        {
                            BtnShowType = 0;
                            _SendCodeEmailBorderPanel.Child = null;
                            SignUPWithMail = null;
                            getFocusOnUserControl();
                            DataModel.ErrorText = string.Empty;
                            DataModel.IsComponentEnabled = true;
                        }
                        else loadFullNameProfilePanel(SettingsConstants.EMAIL_LOGIN, null, null);
                    };
                }
                _SendCodeEmailBorderPanel.Child = SignUPWithMail;
                BtnShowType = 2;
            }
            else
            {
                BtnShowType = 0;
                _SendCodeEmailBorderPanel.Child = null;
            }
        }

        private void onSignUPSuccessOfFaild(int typeToSwitch, int loginType, string fullName, string profileImage, string token, string socialMediaID, string messag = null)
        {
            DataModel.RingID = DefaultSettings.VALUE_NEW_USER_NAME.ToString();
            DataModel.FullName = fullName;
            DataModel.ProfileImageUrl = profileImage;
            if (typeToSwitch == WelcomePanelConstants.TypeSignupNamePassword) loadFullNameProfilePanel(loginType, token, socialMediaID);
            else if (typeToSwitch == WelcomePanelConstants.TypeSigninSuccess) Application.Current.Dispatcher.Invoke(() => { addSignUPSuccess(); });
            else if (!string.IsNullOrEmpty(messag))
            {
                DataModel.ErrorMessageType = 1;
                DataModel.ErrorText = messag;
            }
        }

        private void onSocialMediaCallBack(int type, int typeToSwitch, string screenName, string profileImage, string socialMediaID, string inputToken, string ringID, string messag = null)
        {
            if (typeToSwitch > 0 && typeToSwitch == WelcomePanelConstants.TypeSigninSuccess)
            {
                UIHelperMethods.ShowMessageWithTimerFromNonThread(motherPanel, NotificationMessages.ALREADY_HAVE_A_RINGID);
                UIHelperMethods.ShowMainUIAfterLogin();
            }
            else if (typeToSwitch > 0 && typeToSwitch != WelcomePanelConstants.TypeSigninFaild)
            {
                onSignUPSuccessOfFaild(typeToSwitch, type, screenName, profileImage, inputToken, socialMediaID, messag);
                if (!string.IsNullOrEmpty(messag)) DataModel.ShowErrorMessage(messag);
            }
            else UIHelperMethods.ShowMessageWithTimerFromNonThread(motherPanel, NotificationMessages.FAILDED_TRY_AGAIN);
        }

        private void loadFullNameProfilePanel(int logintype, string accessToken = null, string socialMediaID = null)
        {
            DataModel.Password = string.Empty;
            FullNamePassword = new UCFullNamePassword(DataModel, accessToken, socialMediaID);
            FullNamePassword.OnCancelButtonClicked += () =>
            {
                if (signUpBorder != null)
                {
                    signUpBorder.Child = null;
                    signUpBorder.Child = this;
                    getFocusOnUserControl();
                }
            };
            if (signUpBorder != null)
            {
                signUpBorder.Child = null;
                signUpBorder.Child = FullNamePassword;
                DataModel.LoginType = logintype;
            }
        }

        private void addSignUPSuccess()
        {
            if (SignUPSuccess == null) SignUPSuccess = new UCSignUPSuccess(DataModel, WelcomePanelConstants.TypeSignup);
            SignUPSuccess.ChangeProfileImage();
            Border motherBorder = null;// (Border)obj;
            var obj = this.Parent;
            if (obj is Border) motherBorder = (Border)obj;
            if (motherBorder != null)
            {
                motherBorder.Child = null;
                motherBorder.Child = SignUPSuccess;
            }
        }

        public void loadRecovaryPanel(List<string> sgnss)
        {
            if (signUpBorder != null)
            {
                if (RecoverPasswordParent == null)
                {
                    RecoverPasswordParent = new UCRecoverPasswordParent(DataModel, motherPanel, signUpBorder);
                    RecoverPasswordParent.OnBackButtonClicked += () =>
                    {
                        signUpBorder.Child = null;
                        signUpBorder.Child = this;
                    };
                }
                RecoverPasswordParent.LoadData(sgnss);
                signUpBorder.Child = RecoverPasswordParent;
            }
        }

        private void getFocusOnUserControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            this.FocusVisualStyle = null;
        }

        #endregion "Utility Methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

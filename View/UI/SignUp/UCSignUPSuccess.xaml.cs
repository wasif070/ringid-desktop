﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.ViewModel;

namespace View.UI.SignUp
{
    /// <summary>
    /// Interaction logic for UCSignUPSuccess.xaml
    /// </summary>
    public partial class UCSignUPSuccess : UserControl, INotifyPropertyChanged
    {
        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(UCSignUPSuccess).Name);
        int susscessType;
        #endregion "Private Fields"

        #region "Constructors"
        public UCSignUPSuccess(VMRingIDMainWindow dataModel, int successType)
        {
            InitializeComponent();
            this.DataModel = dataModel;
            this.DataContext = this;
            this.susscessType = successType;
            if (susscessType == WelcomePanelConstants.TypeForgotPassword)
            {
                TextType = "Recovery by";
            }
            if (RingIDViewModel.Instance.MyBasicInfoModel == null)
            {
                RingIDViewModel.Instance.MyBasicInfoModel = new UserBasicInfoModel();
            }
            MyBasicInfoModel = RingIDViewModel.Instance.MyBasicInfoModel;
        }
        #endregion "Constructors"

        #region "Properties"

        private UserBasicInfoModel myBasicInfoModel;
        public UserBasicInfoModel MyBasicInfoModel
        {
            get { return myBasicInfoModel; }
            set { myBasicInfoModel = value; this.OnPropertyChanged("MyBasicInfoModel"); }
        }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private string mailOrMobileText;
        public string MailOrMobileText
        {
            get { return mailOrMobileText; }
            set { mailOrMobileText = value; OnPropertyChanged("MailOrMobileText"); }
        }

        private string loginID;
        public string LoginID
        {
            get { return loginID; }
            set { loginID = value; OnPropertyChanged("LoginID"); }
        }

        private string password = null;
        public string Password
        {
            get { return password; }
            set { password = value; OnPropertyChanged("Password"); }
        }

        private string textType = "Signup Type";
        public string TextType
        {
            get { return textType; }
            set { textType = value; OnPropertyChanged("TextType"); }
        }


        private bool isCheckedEncryptePassword = true;
        public bool IsCheckedEncryptePassword
        {
            get { return isCheckedEncryptePassword; }
            set
            {
                if (value == isCheckedEncryptePassword) return;
                isCheckedEncryptePassword = value;
                onEncryptPassword(isCheckedEncryptePassword);
                OnPropertyChanged("IsCheckedEncryptePassword");
            }
        }
        private ImageSource profieImageSource = null;
        public ImageSource ProfieImageSource
        {
            get { return profieImageSource; }
            set { profieImageSource = value; OnPropertyChanged("ProfieImageSource"); }
        }

        #endregion "Properties"

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            Password = makeEncriptedPassword(DataModel.Password);
            if (!string.IsNullOrEmpty(DataModel.FullName) && string.IsNullOrEmpty(MyBasicInfoModel.ShortInfoModel.FullName))
                MyBasicInfoModel.ShortInfoModel.FullName = DataModel.FullName;
            if (!string.IsNullOrEmpty(DataModel.ProfileImageUrl) && string.IsNullOrEmpty(MyBasicInfoModel.ShortInfoModel.ProfileImage))
                MyBasicInfoModel.ShortInfoModel.ProfileImage = DataModel.ProfileImageUrl;
            if (DataModel.UserTableID > 0) MyBasicInfoModel.ShortInfoModel.UserTableID = DataModel.UserTableID;
            LoadData();
        }

        private ICommand doneCommand;
        public ICommand DoneCommand
        {
            get
            {
                if (doneCommand == null) doneCommand = new RelayCommand(param => OnDoneCommand(param));
                return doneCommand;
            }
        }
        private void OnDoneCommand(object param)
        {
            UIHelperMethods.ShowMainUIAfterLogin();
        }

        #endregion"ICommands and Command Methods"

        #region "Methods"

        public void ChangeProfileImage()
        {
            ProfieImageSource = ImageUtility.GetImageSoruceFromBasicInfo(MyBasicInfoModel, ImageUtility.IMG_THUMB, 150, 150);
        }

        private void onEncryptPassword(bool isChecked)
        {
            if (isChecked == true)
            {
                Password = makeEncriptedPassword(DataModel.Password);
            }
            else
            {
                Password = DataModel.Password;
            }
        }

        private string makeEncriptedPassword(string rawPassword)
        {
            string _getPassword = null;
            if (!string.IsNullOrEmpty(rawPassword)) for (int i = 0; i < rawPassword.Length; i++) _getPassword += "*";
            return _getPassword;
        }

        private void LoadData()
        {
            int logintype = DataModel.LoginType;
            if (logintype == SettingsConstants.MOBILE_LOGIN)
            {
                MailOrMobileText = ControlTexts.TEXT_PHONE_NUMBER;
                LoginID = DataModel.PhoneNumberWithCode;
            }
            else if (logintype == SettingsConstants.EMAIL_LOGIN)
            {
                MailOrMobileText = ControlTexts.TEXT_EMAIL;
                LoginID = DataModel.EmailID;
            }
            else if (logintype == SettingsConstants.FACEBOOK_LOGIN)
            {
                MailOrMobileText = ControlTexts.TEXT_FACEBOOK;
            }
            else if (logintype == SettingsConstants.TWITTER_LOGIN)
            {
                MailOrMobileText = ControlTexts.TEXT_TWITTER;
            }
            Password = makeEncriptedPassword(DataModel.Password);
        }

        private void DoneButtonClick()
        {
            //MainSwitcher.MainController().MainUIController().SwitchToGuiRingID();
        }

        private void OnEncryptPassword(bool isChecked)
        {
            if (isChecked == true) Password = makeEncriptedPassword(DataModel.Password);
            else
            {
                Password = DataModel.Password;
                //this.showPassword.Margin = new Thickness(5, 10, 9, 0);
                //this.showPassword.FontSize = 15;
            }
        }


        #endregion "methods"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

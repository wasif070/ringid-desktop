﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.Utility;
using View.Utility;
using View.Utility.SignInSignUP;
using View.ViewModel;

namespace View.UI.SignUp
{
    /// <summary>
    /// Interaction logic for UCSignUPWithMail.xaml
    /// </summary>
    public partial class UCSignUPWithMail : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCSignUPWithMail).Name);
        public event DelegateBoolStringString OnBackButtonClicked;
        //Border signUpBorder = null;
        public bool RecoveryByEmail = false;
        private GetOrVerifyPassCodeForMail passCodeReqeust;
        #endregion

        #region"Ctors"
        public UCSignUPWithMail(VMRingIDMainWindow model)
        {
            InitializeComponent();
            this.DataModel = model;
            this.DataContext = this;
        }

        #endregion"Ctors"

        #region "Properties"
        private UCFullNamePassword FullNamePassword { get; set; }
        private UCSignUPParent SignUPParent { get; set; }
        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private string passCode;
        public string PassCode
        {
            get { return this.passCode; }
            set { this.passCode = value; this.OnPropertyChanged("PassCode"); }
        }

        private bool isEnableEmailEdit = true;
        public bool IsEnableEmailEdit
        {
            get { return this.isEnableEmailEdit; }
            set { this.isEnableEmailEdit = value; this.OnPropertyChanged("IsEnableEmailEdit"); }
        }

        private bool isFocusedEmail;
        public bool IsFocusedEmail
        {
            get { return isFocusedEmail; }
            set { isFocusedEmail = value; OnPropertyChanged("IsFocusedEmail"); }
        }

        private bool isFocusedPasscode;
        public bool IsFocusedPasscode
        {
            get { return isFocusedPasscode; }
            set { isFocusedPasscode = value; OnPropertyChanged("IsFocusedPasscode"); }
        }

        private bool isCancelClicked = false;
        public bool IsCancelClicked
        {
            get { return isCancelClicked; }
            set { isCancelClicked = value; OnPropertyChanged("IsCancelClicked"); }
        }

        private Visibility signUpButtonVisibility = Visibility.Visible;
        public Visibility SignUpButtonVisibility
        {
            get { return signUpButtonVisibility; }
            set { signUpButtonVisibility = value; OnPropertyChanged("SignUpButtonVisibility"); }
        }

        private Visibility cancelButtonVisibility = Visibility.Collapsed;
        public Visibility CancelButtonVisibility
        {
            get { return cancelButtonVisibility; }
            set
            {
                cancelButtonVisibility = value;
                if (value == Visibility.Visible) SignUpButtonVisibility = Visibility.Collapsed;
                else SignUpButtonVisibility = Visibility.Visible;
                OnPropertyChanged("CancelButtonVisibility");
            }
        }

        #endregion

        #region "ICommands and Command Methods"

        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            PassCode = string.Empty;
            if (RecoveryByEmail)
            {
                IsEnableEmailEdit = false;
                _SendBtn.Focus();
                Keyboard.Focus(_SendBtn);
                _SendBtn.FocusVisualStyle = null;
            }
            else
            {
                focusOnEmail();
                IsEnableEmailEdit = true;
            }
        }

        private ICommand backButtonCommand;
        public ICommand BackButtonCommand
        {
            get
            {
                if (backButtonCommand == null) backButtonCommand = new RelayCommand(param => OnBackButton(param));
                return backButtonCommand;
            }
        }
        private void OnBackButton(object param)
        {
            if (DataModel.IsComponentEnabled)
            {
                DataModel.ErrorText = string.Empty;
                DataModel.PleaseWaitLoader = false;
                DataModel.IsComponentEnabled = true;
                if (OnBackButtonClicked != null) OnBackButtonClicked(false, null, null);
            }
        }


        private ICommand enterKeyCommand;
        public ICommand EnterKeyCommand
        {
            get
            {
                if (enterKeyCommand == null) enterKeyCommand = new RelayCommand(param => OnEnterKeyCommand(param));
                return enterKeyCommand;
            }
        }
        private void OnEnterKeyCommand(object param)
        {
            Console.WriteLine(" Signup with mail Enter Key");
        }

        private ICommand sendEmailCommand;
        public ICommand SendEmailCommand
        {
            get
            {
                if (sendEmailCommand == null) sendEmailCommand = new RelayCommand(param => OnSendEmailCommand(param));
                return sendEmailCommand;
            }
        }
        private void OnSendEmailCommand(object param)
        {
            string email = DataModel.EmailID.Trim();
            if (email.Length > 0 && HelperMethodsModel.IsValidEmail(email)) getCodeForEmail();
            else
            {
                DataModel.IsComponentEnabled = true;
                DataModel.ErrorMessageType = SettingsConstants.EMAIL_LOGIN;
                DataModel.ErrorText = NotificationMessages.BIR_INVALID_EMAIL_ADDRESS;
            }
        }

        private ICommand sendCodeCommand;
        public ICommand SendCodeCommand
        {
            get
            {
                if (sendCodeCommand == null) sendCodeCommand = new RelayCommand(param => OnSendCodeCommand(param));
                return sendCodeCommand;
            }
        }
        private void OnSendCodeCommand(object param)
        {
            string validationMsg = HelperMethodsModel.RingidorMailorMobileValidationMsg(DataModel.EmailID, SettingsConstants.EMAIL_LOGIN);
            if (string.IsNullOrEmpty(validationMsg))
            {
                if (PassCode.Length > 0) verifyCode();
                else
                {
                    focusOnPassCode();
                    showErrorMessage("Verification Code Required!");
                }
            }
            else
            {
                focusOnEmail();
                showErrorMessage(validationMsg);
            }
        }

        private ICommand cancelReqeustCommand;
        public ICommand CancelReqeustCommand
        {
            get
            {
                if (cancelReqeustCommand == null) cancelReqeustCommand = new RelayCommand(param => OnCancelReqeustCommand());
                return cancelReqeustCommand;
            }
        }
        private void OnCancelReqeustCommand()
        {
            IsCancelClicked = true;
            if (passCodeReqeust != null)
                passCodeReqeust.StopThread();
        }

        #endregion"ICommands and Command Methods"

        #region "Utility Methods"

        private void showErrorMessage(string msg)
        {
            DataModel.ErrorText = msg;
            DataModel.ErrorMessageType = 0;
            DataModel.IsComponentEnabled = true;
        }

        private void showSuccessMessage(string msg)
        {
            DataModel.ErrorText = msg;
            DataModel.ErrorMessageType = SettingsConstants.EMAIL_LOGIN;
            DataModel.IsComponentEnabled = true;
        }

        private void getFocusOnUserControl()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
            this.FocusVisualStyle = null;
        }

        private void focusOnPassCode()
        {
            IsFocusedPasscode = false;
            IsFocusedPasscode = true;
        }

        private void focusOnEmail()
        {
            IsFocusedEmail = false;
            IsFocusedEmail = true;
        }

        #endregion "Utility methods"

        #region"Server requests"

        private void initServerRequets()
        {
            DataModel.PleaseWaitLoader = true;
            DataModel.IsComponentEnabled = false;
            IsEnableEmailEdit = false;
            CancelButtonVisibility = Visibility.Visible;
            DataModel.ErrorMessageType = SettingsConstants.EMAIL_LOGIN;
            if (passCodeReqeust == null)
            {
                passCodeReqeust = new GetOrVerifyPassCodeForMail();
                passCodeReqeust.OnEmailCodeFeedBack += (issucsess, code, msg) => OnEmailCodeFeedBack(issucsess, code, msg);
            }
        }

        public void getCodeForEmail()
        {
            initServerRequets();
            if (RecoveryByEmail) passCodeReqeust.StartThread(passCodeReqeust.SendCodeRecovery, DataModel.EmailID, 0);
            else passCodeReqeust.StartThread(passCodeReqeust.SendCodeSignup, DataModel.EmailID, DefaultSettings.VALUE_NEW_USER_NAME);
        }

        public void verifyCode()
        {
            initServerRequets();
            try
            {
                if (RecoveryByEmail) passCodeReqeust.StartThread(passCodeReqeust.VerifyCodeRecovery, DataModel.EmailID, long.Parse(DataModel.RingID), PassCode);
                else passCodeReqeust.StartThread(passCodeReqeust.VerifyCodeSignup, DataModel.EmailID, DefaultSettings.VALUE_NEW_USER_NAME, PassCode);
            }
            finally { }
        }

        private void OnEmailCodeFeedBack(bool isSuccess, string code, string msg)
        {
            DataModel.PleaseWaitLoader = false;
            CancelButtonVisibility = Visibility.Collapsed;
            DataModel.IsComponentEnabled = true;
            if (IsCancelClicked)
            {
                IsCancelClicked = false;
                DataModel.ErrorText = "Canceled!";
            }
            else
            {
                if (passCodeReqeust.ActionToDo == passCodeReqeust.SendCodeSignup)
                {
                    if (isSuccess && !string.IsNullOrEmpty(msg))
                    {
                        showSuccessMessage(msg);
                        IsEnableEmailEdit = false;
                        focusOnPassCode();
                    }
                    else if (!string.IsNullOrEmpty(msg))
                    {
                        showErrorMessage(msg);
                        focusOnEmail();
                    }
                    else
                    {
                        showErrorMessage(NotificationMessages.FAILED_TO_RESET_PASSWORD);
                        focusOnEmail();
                    }
                }
                else if (passCodeReqeust.ActionToDo == passCodeReqeust.VerifyCodeSignup)
                {
                    if (isSuccess)
                    {
                        IsEnableEmailEdit = true;
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            DataModel.ErrorText = string.Empty;
                            DataModel.RingID = DefaultSettings.VALUE_NEW_USER_NAME.ToString();
                            if (OnBackButtonClicked != null) OnBackButtonClicked(true, null, null);
                        });
                    }
                    else
                    {
                        showErrorMessage(msg);
                        focusOnPassCode();
                    }
                }
                else if (passCodeReqeust.ActionToDo == passCodeReqeust.SendCodeRecovery)
                {
                    if (isSuccess)
                    {
                        showSuccessMessage(msg);
                        focusOnPassCode();
                    }
                    else
                    {
                        showErrorMessage(msg);
                        focusOnEmail();
                    }
                }
                else if (passCodeReqeust.ActionToDo == passCodeReqeust.VerifyCodeRecovery)
                {
                    if (isSuccess)
                    {
                        DataModel.ErrorText = string.Empty;
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (OnBackButtonClicked != null)
                                OnBackButtonClicked(true, null, null);
                        });
                    }
                    else
                    {
                        focusOnPassCode();
                        showErrorMessage(msg);
                    }
                }
            }
            DataModel.PleaseWaitLoader = false;
            DataModel.IsComponentEnabled = true;
            IsEnableEmailEdit = true;
        }
        #endregion"Server Requests"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

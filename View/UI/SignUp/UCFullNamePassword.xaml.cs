﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Auth.Service.Images;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Images;
using View.Utility.SignInSignUP;
using View.ViewModel;

namespace View.UI.SignUp
{
    /// <summary>
    /// Interaction logic for UCFullNamePassword.xaml
    /// </summary>
    public partial class UCFullNamePassword : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(UCFullNamePassword).Name);
        public event DelegateNoParam OnCancelButtonClicked;
        private string socialMediaID;
        private string asccessOrInputToken;
        #endregion

        #region"Ctors"
        public UCFullNamePassword(VMRingIDMainWindow model, string accessToken = null, string socialMediaID = null)
        {
            InitializeComponent();
            this.DataModel = model;
            this.DataContext = this;
            this.socialMediaID = socialMediaID;
            this.asccessOrInputToken = accessToken;
        }
        #endregion"Ctors"

        #region "Properties"
        private View.UI.PopUp.UCCountryCodePopup CountryCodePopup { get; set; }
        private UCSignUPSuccess SignUPSuccess { get; set; }

        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private CountryCodeModel selectedCountryModel;
        public CountryCodeModel SelectedCountryModel
        {
            get { return selectedCountryModel; }
            set
            {
                if (value == selectedCountryModel) return;
                selectedCountryModel = value;
                DataModel.CountryCode = selectedCountryModel.CountryCode;
                OnPropertyChanged("SelectedCountryModel");
            }
        }

        private string retypePassword;
        public string RetypePassword
        {
            get { return this.retypePassword; }
            set { this.retypePassword = value; this.OnPropertyChanged("RetypePassword"); }
        }

        private Visibility pleaseWaitLoaderVisibility = Visibility.Collapsed;
        public Visibility PleaseWaitLoaderVisibility
        {
            get { return this.pleaseWaitLoaderVisibility; }
            set
            {
                this.pleaseWaitLoaderVisibility = value;
                if (this.pleaseWaitLoaderVisibility == Visibility.Visible) DataModel.ErrorText = NotificationMessages.PLEASE_WAIT;
                ChangeLoaderAnimation(this.pleaseWaitLoaderVisibility);
                this.OnPropertyChanged("PleaseWaitLoaderVisibility");
            }
        }

        private Visibility capsLockVisibility = Visibility.Hidden;
        public Visibility CapsLockVisibility
        {
            get { return this.capsLockVisibility; }
            set { this.capsLockVisibility = value; this.OnPropertyChanged("CapsLockVisibility"); }
        }

        #endregion

        #region "ICommands and Command Methods"
        private ICommand loadedUserConrol;
        public ICommand LoadedUserConrol
        {
            get
            {
                if (loadedUserConrol == null) loadedUserConrol = new RelayCommand(param => OnLoadedUserConrol());
                return loadedUserConrol;
            }
        }
        public void OnLoadedUserConrol()
        {
            getFocusOnThis();
            SelectedCountryModel = HelperMethods.GetCountryCodeModelFromCountry();
            if (DataModel.LoginType == SettingsConstants.MOBILE_LOGIN)
            {
                mblDataContainerBorder.Visibility = Visibility.Collapsed;
                _TextBoxUserName.Focus();
            }
            else
            {
                mblDataContainerBorder.Visibility = Visibility.Visible;
                _TextBoxUserPhone.Focus();
            }
            OnCapsLockCommand();
        }
        //CapsLockCommand
        private ICommand capsLockCommand;
        public ICommand CapsLockCommand
        {
            get
            {
                if (capsLockCommand == null) capsLockCommand = new RelayCommand(param => OnCapsLockCommand());
                return capsLockCommand;
            }
        }
        private void OnCapsLockCommand()
        {
            if (Console.CapsLock) CapsLockVisibility = Visibility.Visible;
            else CapsLockVisibility = Visibility.Hidden;
        }

        private ICommand cancelButtonCommand;
        public ICommand CancelButtonCommand
        {
            get
            {
                if (cancelButtonCommand == null) cancelButtonCommand = new RelayCommand(param => OnCancelButtonCommand(param));
                return cancelButtonCommand;
            }
        }
        private void OnCancelButtonCommand(object param)
        {
            DataModel.ErrorText = string.Empty;
            if (OnCancelButtonClicked != null) OnCancelButtonClicked();
        }

        private ICommand doneButtonCommand;
        public ICommand VerifyOrDoneButtonCommand
        {
            get
            {
                if (doneButtonCommand == null) doneButtonCommand = new RelayCommand(param => OnVerifyOrDoneButton(param));
                return doneButtonCommand;
            }
        }
        private void OnVerifyOrDoneButton(object param)
        {
            string validationMsg = string.Empty;
            if (DataModel.LoginType != SettingsConstants.MOBILE_LOGIN) validationMsg = HelperMethodsModel.PhoneNumberValidationMsg(DataModel.PhoneNumber);
            if (string.IsNullOrEmpty(validationMsg)) validationMsg = HelperMethodsModel.NamePasswordValidationMsg(DataModel.FullName, DataModel.Password, RetypePassword);
            if (string.IsNullOrEmpty(validationMsg))
            {
                DataModel.ErrorText = string.Empty;
                doSignIn();
            }
            else
            {
                DataModel.ErrorText = validationMsg;
                DataModel.ErrorMessageType = DataModel.LoginType;
            }
        }

        private ICommand countryPopUpCommand;
        public ICommand CountryPopUpCommand
        {
            get
            {
                if (countryPopUpCommand == null) countryPopUpCommand = new RelayCommand(param => OnCountryPopUpCommand(param));
                return countryPopUpCommand;
            }
        }
        private void OnCountryPopUpCommand(object param)
        {
            if (DataModel.IsComponentEnabled)
            {
                if (CountryCodePopup != null) _CountryCodePopupGrid.Children.Remove(CountryCodePopup);
                if (CountryCodePopup == null)
                {
                    CountryCodePopup = new PopUp.UCCountryCodePopup();
                    CountryCodePopup.OnCountryModelChanged += (selectedCountry) =>
                    {
                        SelectedCountryModel = selectedCountry;
                    };
                    CountryCodePopup.OnHide += () =>
                    {
                        _CountryCodePopupGrid.Children.Remove(CountryCodePopup);
                        getFocusOnThis();
                    };
                }
                CountryCodePopup.SelectedCountryModel = SelectedCountryModel;
                CountryCodePopup.ShowThis((UIElement)param);
                _CountryCodePopupGrid.Children.Add(CountryCodePopup);
            }
        }

        #endregion"ICommands and Command Methods"

        #region"Utility Methods"

        private void getFocusOnThis()
        {
            this.Focusable = true;
            Keyboard.Focus(this);
        }

        private void ChangeLoaderAnimation(Visibility visibility)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (visibility == Visibility.Visible)
                {
                    GIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_SIGN_IN));
                    GIFCtrl.Visibility = Visibility.Visible;
                }
                else
                {
                    if (GIFCtrl != null && GIFCtrl.IsRunning()) this.GIFCtrl.StopAnimate();
                    GIFCtrl.Visibility = Visibility.Collapsed;
                }
            });
        }

        private void showErrorMessage(string msg)
        {
            PleaseWaitLoaderVisibility = Visibility.Collapsed;
            DataModel.ErrorText = msg;
            DataModel.ErrorMessageType = 0;
        }

        #endregion"Utility Methods"

        #region "Server Communications"

        private void doSignIn()
        {
            ThreadStart start = delegate
            {
                PleaseWaitLoaderVisibility = Visibility.Visible;
                DataModel.ErrorMessageType = DataModel.LoginType;
                DataModel.IsComponentEnabled = false;
                try
                {
                    string addProfileDetails = addProfileDetailsReqeusts(DataModel.LoginType, DataModel.CountryCode, DataModel.PhoneNumber, asccessOrInputToken, socialMediaID);
                    if (string.IsNullOrEmpty(addProfileDetails))
                    {
                        SignedInInfoDTO signindto = sendSignInReqeusts(DataModel.LoginType, DataModel.RingID, DataModel.Password, DataModel.EmailID, DataModel.CountryCode, DataModel.PhoneNumber, socialMediaID, asccessOrInputToken);
                        if (signindto.isSuccess)
                        {
                            if (!string.IsNullOrEmpty(DataModel.ProfileImageUrl)) setProfileImageForSocialMedia(DataModel.ProfileImageUrl);
                            ChangeLoaderAnimation(Visibility.Collapsed);
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (SignUPSuccess == null) SignUPSuccess = new UCSignUPSuccess(DataModel, WelcomePanelConstants.TypeSignup);
                                SignUPSuccess.ChangeProfileImage();
                                Border motherBorder = null;// (Border)obj;
                                var obj = this.Parent;
                                if (obj is Border) motherBorder = (Border)obj;
                                if (motherBorder != null)
                                {
                                    motherBorder.Child = null;
                                    motherBorder.Child = SignUPSuccess;
                                }
                            });
                        }
                        else showErrorMessage(NotificationMessages.PLEASE_ENTER_DETAILS_CORRECTLY);
                    }
                    else showErrorMessage(addProfileDetails);
                }
                finally { }
                PleaseWaitLoaderVisibility = Visibility.Collapsed;
                DataModel.IsComponentEnabled = true;
            };
            new System.Threading.Thread(start).Start();
        }

        private string addProfileDetailsReqeusts(int loginType, string countryCode, string mobileNumber, string accessToken = null, string socialMediaID = null)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserIdentity] = DefaultSettings.VALUE_NEW_USER_NAME;
            pakToSend[JsonKeys.AppType] = SettingsConstants.APP_TYPE_RINGID_FULL;
            if (countryCode.Equals("+880") && mobileNumber[0] == '0') mobileNumber = mobileNumber.Substring(1);
            pakToSend[JsonKeys.MobilePhone] = mobileNumber;
            pakToSend[JsonKeys.DialingCode] = countryCode;
            pakToSend[JsonKeys.Name] = DataModel.FullName;
            pakToSend[JsonKeys.Password] = DataModel.Password;
            pakToSend[JsonKeys.IsDigits] = 1;
            pakToSend[JsonKeys.IsPickedMobileNumber] = 0;
            pakToSend[JsonKeys.DeviceId] = HelperMethods.GetGUID();
            pakToSend[JsonKeys.Version] = AppConfig.VERSION_PC;
            pakToSend[JsonKeys.Device] = AppConstants.PLATFORM_DESKTOP;
            if (loginType == SettingsConstants.FACEBOOK_LOGIN)
            {
                pakToSend[JsonKeys.Facebook] = socialMediaID;
                pakToSend[JsonKeys.InputToken] = accessToken;
            }
            else if (loginType == SettingsConstants.TWITTER_LOGIN)
            {
                pakToSend[JsonKeys.Twitter] = socialMediaID;
                pakToSend[JsonKeys.InputToken] = accessToken;
            }
            bool SC = false;
            string MS = string.Empty;
            (new AuthRequestWithMessageNoSession(pakToSend, AppConstants.TYPE_ADD_PROFILE_DETAILS, AppConstants.REQUEST_TYPE_AUTH)).Run(out SC, out MS);
            return MS;
        }

        private void setProfileImageForSocialMedia(string imageUrl)
        {
            System.Drawing.Bitmap ProfileImageFromSocialMedia = null;
            System.Net.WebRequest httpWebRequest = null;
            try
            {
                httpWebRequest = System.Net.WebRequest.Create(imageUrl);
                using (System.Net.WebResponse response = httpWebRequest.GetResponse())
                using (System.IO.Stream responseStream = response.GetResponseStream()) ProfileImageFromSocialMedia = new System.Drawing.Bitmap(responseStream);
                if (ProfileImageFromSocialMedia != null)
                {
                    byte[] postData = ImageUtility.ReduceQualityAndSize(ProfileImageFromSocialMedia, RingIDSettings.PROFILE_PIC_SCALABLE_WIDTH, 85, SettingsConstants.TYPE_PROFILE_IMAGE);
                    if (postData != null)
                    {
                        string response = ProfileCoverImageUpload.Instance.UploadToImageServerWebRequest(postData, SettingsConstants.TYPE_PROFILE_IMAGE, 0, 0, RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT, RingIDSettings.PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT);
                        if (!string.IsNullOrEmpty(response))
                        {
                            int reasonCode = ImageUploadHelper.SendProfileCoverImagePostRequestToAuth(response, (int)ProfileImageFromSocialMedia.Width, (int)ProfileImageFromSocialMedia.Height, SettingsConstants.TYPE_PROFILE_IMAGE, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER, 0, 0, Guid.Empty, 0);
                        }
                    }
                }
            }
            catch (Exception e) { log.Error("SetProfileImageForSocialMedia==>" + e.Message + "\n" + e.StackTrace); }
            finally
            {
                if (httpWebRequest != null)
                {
                    httpWebRequest.Abort();
                    httpWebRequest = null;
                }
                if (ProfileImageFromSocialMedia != null) ProfileImageFromSocialMedia.Dispose();
            }
        }

        private SignedInInfoDTO sendSignInReqeusts(int loginType, string ringId, string password, string email, string mobileCode, string phoneNumber, string socialMediaId, string inputToken)
        {
            try
            {
                new DatabaseActivityDAO().DeleteNewUserIDfromDB();
                DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN = 1;
                DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
                DefaultSettings.VALUE_LOGIN_AUTO_START = 1;
                DefaultSettings.VALUE_LOGIN_USER_INFO = null;
                return new SigninRequest().FetchPortsAndSignIn(loginType, ringId, password, email, mobileCode, phoneNumber, socialMediaId, inputToken);
            }
            catch (Exception) { return null; }
        }

        #endregion "Server Communications"

        #region "INotifyPropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
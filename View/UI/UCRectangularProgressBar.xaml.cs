﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for UCRectangularProgressBar.xaml
    /// </summary>
    public partial class UCRectangularProgressBar : UserControl
    {
        public UCRectangularProgressBar()
        {
            InitializeComponent();
            Angle = ((double)Percentage * 360) / 100;
            RenderArc();
        }

        public Brush FillColor
        {
            get { return (Brush)GetValue(FillColorProperty); }
            set { SetValue(FillColorProperty, value); }
        }

        public double Percentage
        {
            get { return (double)GetValue(PercentageProperty); }
            set { SetValue(PercentageProperty, value); }
        }

        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        public int RecWidth
        {
            get { return (int)GetValue(RecWidthProperty); }
            set { SetValue(RecWidthProperty, value); }
        }

        public int RecHeight
        {
            get { return (int)GetValue(RecHeightProperty); }
            set { SetValue(RecHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Percentage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RecWidthProperty =
            DependencyProperty.Register("RecWidth", typeof(int), typeof(UCRectangularProgressBar), new PropertyMetadata(0, new PropertyChangedCallback(OnPropertyChanged)));

        // Using a DependencyProperty as the backing store for Percentage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RecHeightProperty =
            DependencyProperty.Register("RecHeight", typeof(int), typeof(UCRectangularProgressBar), new PropertyMetadata(0, new PropertyChangedCallback(OnPropertyChanged)));

        // Using a DependencyProperty as the backing store for Percentage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PercentageProperty =
            DependencyProperty.Register("Percentage", typeof(double), typeof(UCRectangularProgressBar), new PropertyMetadata(65d, new PropertyChangedCallback(OnPercentageChanged)));

        // Using a DependencyProperty as the backing store for SegmentColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FillColorProperty =
            DependencyProperty.Register("FillColor", typeof(Brush), typeof(UCRectangularProgressBar), new PropertyMetadata(new SolidColorBrush(Colors.Red), new PropertyChangedCallback(OnColorChanged)));

        // les animUsing a DependencyProperty as the backing store for Angle.  This enabation, styling, binding, etc...
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(UCRectangularProgressBar), new PropertyMetadata(120d, new PropertyChangedCallback(OnPropertyChanged)));

        private static void OnColorChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            UCRectangularProgressBar rect = sender as UCRectangularProgressBar;
            rect.pathRoot.Fill = (SolidColorBrush)args.NewValue;
        }

        private static void OnPercentageChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            UCRectangularProgressBar rect = sender as UCRectangularProgressBar;
            rect.Angle = ((double)(rect.Percentage > 100 ? 100 : rect.Percentage) * 360) / 100;
        }

        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            UCRectangularProgressBar rect = sender as UCRectangularProgressBar;
            rect.RenderArc();
        }

        public void RenderArc()
        {
            pathRoot.Width = RecWidth;
            pathRoot.Height = RecHeight;

            double diagonal = Math.Sqrt(pathRoot.Width * pathRoot.Width + pathRoot.Height * pathRoot.Height) / 2;

            Point recPoint1 = new Point(0, 0);
            Point recPoint2 = new Point(pathRoot.Width, 0);
            Point recPoint3 = new Point(pathRoot.Width, pathRoot.Height);
            Point recPoint4 = new Point(0, pathRoot.Height);

            Point startPoint = new Point(pathRoot.Width / 2, pathRoot.Height / 2);
            Point endPoint = ComputeCartesianCoordinate(Angle, diagonal);
            endPoint.X += pathRoot.Width / 2;
            endPoint.Y += pathRoot.Height / 2;

            pathFigure.StartPoint = startPoint;
            pathFigure.Segments.Clear();
            pathFigure.Segments.Add(new LineSegment { Point = new Point(pathRoot.Width / 2, 0) });

            bool isIntersect = false;
            Point intersectPoint = new Point();

            if (Angle <= 90.0)
            {
                FindIntersectionPoint(recPoint1, recPoint2, startPoint, endPoint, out isIntersect, out intersectPoint);
                if (isIntersect && intersectPoint.X <= pathRoot.Width) endPoint = intersectPoint;
                else
                {
                    FindIntersectionPoint(recPoint2, recPoint3, startPoint, endPoint, out isIntersect, out intersectPoint);
                    if (isIntersect) endPoint = intersectPoint;
                }

                if (endPoint.Y > 0)
                {
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint2 });
                    pathFigure.Segments.Add(new LineSegment { Point = endPoint });
                }
                else pathFigure.Segments.Add(new LineSegment { Point = endPoint });
            }
            else if (Angle <= 180)
            {
                FindIntersectionPoint(recPoint2, recPoint3, startPoint, endPoint, out isIntersect, out intersectPoint);
                if (isIntersect && intersectPoint.Y <= pathRoot.Height) endPoint = intersectPoint;
                else
                {
                    FindIntersectionPoint(recPoint3, recPoint4, startPoint, endPoint, out isIntersect, out intersectPoint);
                    if (isIntersect) endPoint = intersectPoint;
                }

                if (endPoint.X < pathRoot.Width)
                {
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint2 });
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint3 });
                    pathFigure.Segments.Add(new LineSegment { Point = endPoint });
                }
                else
                {
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint2 });
                    pathFigure.Segments.Add(new LineSegment { Point = endPoint });
                }
            }
            else if (Angle <= 270)
            {
                FindIntersectionPoint(recPoint3, recPoint4, startPoint, endPoint, out isIntersect, out intersectPoint);
                if (isIntersect && intersectPoint.X >= 0) endPoint = intersectPoint;
                else
                {
                    FindIntersectionPoint(recPoint4, recPoint1, startPoint, endPoint, out isIntersect, out intersectPoint);
                    if (isIntersect) endPoint = intersectPoint;
                }

                if (endPoint.Y < pathRoot.Height)
                {
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint2 });
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint3 });
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint4 });
                    pathFigure.Segments.Add(new LineSegment { Point = endPoint });
                }
                else
                {
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint2 });
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint3 });
                    pathFigure.Segments.Add(new LineSegment { Point = endPoint });
                }
            }
            else
            {
                FindIntersectionPoint(recPoint4, recPoint1, startPoint, endPoint, out isIntersect, out intersectPoint);
                if (isIntersect && intersectPoint.Y >= 0) endPoint = intersectPoint;
                else
                {
                    FindIntersectionPoint(recPoint1, recPoint2, startPoint, endPoint, out isIntersect, out intersectPoint);
                    if (isIntersect) endPoint = intersectPoint;
                }

                if (endPoint.X > 0)
                {
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint2 });
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint3 });
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint4 });
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint1 });
                    pathFigure.Segments.Add(new LineSegment { Point = endPoint });
                }
                else
                {
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint2 });
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint3 });
                    pathFigure.Segments.Add(new LineSegment { Point = recPoint4 });
                    pathFigure.Segments.Add(new LineSegment { Point = endPoint });
                }
            }
        }

        private Point ComputeCartesianCoordinate(double angle, double radius)
        {
            // convert to radians
            double angleRad = (Math.PI / 180.0) * (angle - 90);
            double x = radius * Math.Cos(angleRad);
            double y = radius * Math.Sin(angleRad);
            return new Point(x, y);
        }


        private void FindIntersectionPoint(Point p1, Point p2, Point p3, Point p4, out bool lines_intersect, out Point intersection)
        {
            double dx12 = p2.X - p1.X;
            double dy12 = p2.Y - p1.Y;
            double dx34 = p4.X - p3.X;
            double dy34 = p4.Y - p3.Y;
            double denominator = (dy12 * dx34 - dx12 * dy34);
            double t1 = ((p1.X - p3.X) * dy34 + (p3.Y - p1.Y) * dx34) / denominator;
            if (double.IsInfinity(t1))
            {
                lines_intersect = false;
                intersection = new Point(float.NaN, float.NaN);
                return;
            }
            lines_intersect = true;
            intersection = new Point(p1.X + dx12 * t1, p1.Y + dy12 * t1);
        }
    }
}

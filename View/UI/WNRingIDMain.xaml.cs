﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shell;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using View.Constants;
using View.UI.Chat;
using View.UI.Feed;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for WNRingIDMain.xaml
    /// </summary>
    public partial class WNRingIDMain : Window, INotifyPropertyChanged
    {
        #region "Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(WNRingIDMain).Name);
        private long _SystemTimeChanged = 0;
        private System.Timers.Timer _ActivateTimer = null;
        System.Windows.Forms.NotifyIcon systemTrayICon;
        System.Windows.Forms.ContextMenu contextMenu1;
        UCMainStartUpLoader RingIDLogoLoader;
        UCMainMenuBar menuTopBar;
        #endregion "Fields"

        #region "Ctors"
        public WNRingIDMain()
        {
            InitializeComponent();
            DataModel = new VMRingIDMainWindow();
            this.DataContext = this;
            WindowWidth = RingIDSettings.FRAME_DEFAULT_WIDTH;
            WindowHeight = RingIDSettings.FRAME_DEFAULT_HEIGHT;
            WindowMinWidth = WindowWidth;
            WindowMinHeight = RingIDSettings.FRAME_DEFAULT_HEIGHT;
            Timeline.DesiredFrameRateProperty.OverrideMetadata(typeof(Timeline), new FrameworkPropertyMetadata { DefaultValue = 25 });
            this.Left = (SystemParameters.PrimaryScreenWidth / 2) - (WindowWidth / 2);
            this.Top = (SystemParameters.PrimaryScreenHeight / 2) - (WindowHeight / 2) - 20;

            SystemEvents.PowerModeChanged += new PowerModeChangedEventHandler(SystemEvents_PowerModeChanged);
            SystemEvents.TimeChanged += SystemEvents_TimeChanged;
            //this.PreviewMouseWheel += WNMainSwitcher_PreviewMouseWheel;
            this.Closing += DataWindow_Closing;
            this.Loaded += WNRingIDMain_Loaded;
        }

        #endregion

        #region "Properties"

        public static WNRingIDMain WinRingIDMain { get; set; }
        private VMRingIDMainWindow dataModel;
        public VMRingIDMainWindow DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private ResizeMode resizeMode = System.Windows.ResizeMode.CanMinimize;
        public ResizeMode MainResizeMode
        {
            get { return resizeMode; }
            set { resizeMode = value; this.OnPropertyChanged("MainResizeMode"); }
        }

        private int windowWidth;
        public int WindowWidth
        {
            get { return windowWidth; }
            set { windowWidth = value; this.OnPropertyChanged("WindowWidth"); }
        }

        private int windowHeight;
        public int WindowHeight
        {
            get { return this.windowHeight; }
            set { this.windowHeight = value; this.OnPropertyChanged("WindowHeight"); }
        }

        private int windowMinWidth;
        public int WindowMinWidth
        {
            get { return this.windowMinWidth; }
            set { this.windowMinWidth = value; this.OnPropertyChanged("WindowMinWidth"); }
        }

        private int windowMinHeight;
        public int WindowMinHeight
        {
            get { return this.windowMinHeight; }
            set { this.windowMinHeight = value; this.OnPropertyChanged("WindowMinHeight"); }
        }

        #endregion "Properties"

        #region Utility Method

        private void CreateCustomJumpList()
        {
            try
            {
                string cmdPath = Assembly.GetEntryAssembly().Location;
                JumpTask jumpTask = new JumpTask();
                jumpTask.ApplicationPath = cmdPath;
                jumpTask.IconResourcePath = cmdPath;
                jumpTask.Title = "Quit ringID";
                jumpTask.Arguments = StartUpConstatns.ARGUMENT_EXIT_FROM_ANOTHER_PROCESS;
                JumpList jumpList = new JumpList();
                jumpList.JumpItems.Add(jumpTask);
                JumpList.SetJumpList(App.Current, jumpList);
            }
            catch (Exception ex) { log.Error("Error: CreateJumpList() => " + ex.Message + "\n" + ex.StackTrace); }
        }

        public void AddActivateEvent()
        {
            this.Activated -= WNMainSwitcher_Activated;
            this.Activated += WNMainSwitcher_Activated;
        }

        public void RemoveActivateEvent()
        {
            this.Activated -= WNMainSwitcher_Activated;
        }

        public void HideTrayIcon()
        {
            if (systemTrayICon != null) systemTrayICon.Visible = false;
        }

        public void SetRingIDasToolTipText()
        {
            try
            {
                if (systemTrayICon != null)
                {
                    if (DefaultSettings.LOGIN_RING_ID > 0)
                    {
                        string str = (DefaultSettings.LOGIN_RING_ID + "").Substring(2);
                        systemTrayICon.Text = "" + RingIDSettings.APP_NAME + " (" + str + ")";
                    }
                    else systemTrayICon.Text = RingIDSettings.APP_NAME;
                }
            }
            catch (Exception ex) { log.Error(ex.StackTrace + ex.Message); }
        }

        public void MinimizeToTray()
        {
            Application.Current.Dispatcher.Invoke((Action)delegate { this.WindowState = WindowState.Minimized; });
        }

        public void AddSystemTray()
        {
            try
            {
                if (systemTrayICon == null)
                {
                    systemTrayICon = new System.Windows.Forms.NotifyIcon();
                    systemTrayICon.Icon = ImageUtility.GetIcon(ImageLocation.APP_ICON);
                    systemTrayICon.Visible = true;
                    systemTrayICon.DoubleClick += delegate(object sender, EventArgs args) { ShowAndActivateWindow(); };
                    systemTrayICon.MouseDown += delegate(object sender, System.Windows.Forms.MouseEventArgs e)
                    {
                        if (e.Button == System.Windows.Forms.MouseButtons.Left) ShowAndActivateWindow();
                        else if (e.Button == System.Windows.Forms.MouseButtons.Right)
                        {
                            if (contextMenu1 == null)
                            {
                                contextMenu1 = new System.Windows.Forms.ContextMenu();
                                System.Windows.Forms.MenuItem openItem = new System.Windows.Forms.MenuItem("&Open");
                                openItem.Click += openItem_Click;
                                System.Windows.Forms.MenuItem exitItem = new System.Windows.Forms.MenuItem("E&xit");
                                exitItem.Click += ExitItem_Click;
                                contextMenu1.MenuItems.Add(openItem);
                                contextMenu1.MenuItems.Add(exitItem);
                            }
                            systemTrayICon.ContextMenu = contextMenu1;
                        }
                    };
                    SetRingIDasToolTipText();
                }
            }
            finally { }
        }

        public void ShowAndActivateWindow()
        {
            this.Visibility = Visibility.Visible;
            this.Show();
            this.Activate();
            this.WindowState = WindowState.Normal;
        }

        public void ShowWindow(string[] args)
        {
            DataModel.PassedArguments = args;
            ShowAndActivateWindow();
        }

        private void CreateAppExitOrOpenOptions()
        {
            CreateCustomJumpList();
            AddSystemTray();
        }

        public void ForceActiveWindow()
        {
            if (!IsActive) Activate();
            Show();
        }

        public void FocusWindow()
        {
            try
            {
                if (!IsActive)
                {
                    this.WindowState = this.WindowState == WindowState.Minimized ? WindowState.Normal : this.WindowState;
                    this.Activate();
                    this.Topmost = true;  // important
                    this.Topmost = false; // important
                    this.Focus();
                }
            }
            catch (Exception ex) { log.Error(ex.StackTrace + " " + ex.Message); }
        }

        public void SetButtonExpandVisibility(Button BtnExpand)
        {
            if (this.WindowState == WindowState.Maximized) BtnExpand.Visibility = Visibility.Hidden;
            else if (WindowState == WindowState.Normal) BtnExpand.Visibility = Visibility.Visible;
        }

        public void MaximizeTheWindow(bool isMaximize = false)
        {
            if (isMaximize) { WindowState = WindowState.Maximized; }
            else
            {
                if (WindowState == WindowState.Maximized) WindowState = WindowState.Normal;
                else if (WindowState == WindowState.Normal) WindowState = WindowState.Maximized;
            }
        }

        #endregion Utility Method

        #region "Event Triggers"

        void WNRingIDMain_Loaded(object sender, RoutedEventArgs e)
        {
            CreateAppExitOrOpenOptions();
            if (menuTopBar == null) menuTopBar = new UCMainMenuBar(_WindowFirstGrid, DataModel);
            _MenuBarBorder.Child = menuTopBar;
            RingIDLogoLoader = new UCMainStartUpLoader(DataModel, _WindowFirstGrid, _RingIDBodyGrid);
            RingIDLogoLoader.StartLoader();
            Navigate(RingIDLogoLoader);
        }

        private void WNMainSwitcher_Activated(object sender, EventArgs args)
        {
            try
            {
                if (_ActivateTimer == null)
                {
                    _ActivateTimer = new System.Timers.Timer { AutoReset = false };
                    _ActivateTimer.Interval = 2000;
                    _ActivateTimer.Elapsed += (o, e) =>
                    {
                        _ActivateTimer.Stop();
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            if (this.IsActive)
                            {
                                ChatHelpers.TriggerChatActivateEvent(UCMiddlePanelSwitcher.View_UCFriendChatCallPanel);
                                ChatHelpers.TriggerChatActivateEvent(UCMiddlePanelSwitcher.View_UCGroupChatCallPanel);
                                ChatHelpers.TriggerChatActivateEvent(UCMiddlePanelSwitcher.View_UCRoomChatCallPanel);
                            }
                        });
                    };
                }
                _ActivateTimer.Stop();
                _ActivateTimer.Start();
            }
            catch (Exception ex) { log.Error(ex.StackTrace + ex.Message); }
        }

        private void SystemEvents_PowerModeChanged(object s, PowerModeChangedEventArgs e)
        {
            switch (e.Mode)
            {
                case PowerModes.Resume:
                    log.Info("==============================> Resume Mode");
                    MainSwitcher.ThreadManager().StartNetworkThread();
                    break;
                case PowerModes.Suspend:
                    log.Info("==============================> Suspend Mode");
                    MainSwitcher.ThreadManager().StartNetworkThread();
                    break;
            }
        }

        void WNMainSwitcher_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)) e.Cancel = false;
                else
                {
                    if (WNLocationViewer.Instance != null) WNLocationViewer.Instance.CloseWindow();
                    //MessageBoxResult result = CustomMessageBox.ShowQuestion(NotificationMessages.EXIT_NOTIFICAITON, RingIDSettings.APP_NAME);
                    //if (result == MessageBoxResult.Yes) 
                    bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.SURE_WANT_TO, "exit"), "Exit confirmation!");
                    if (isTrue)
                        e.Cancel = false;
                    else e.Cancel = true;
                }
            }
            catch (Exception ex) { log.Error(ex.StackTrace + ex.Message); }
        }

        private void DataWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (WNLocationViewer.Instance != null) WNLocationViewer.Instance.CloseWindow();
            e.Cancel = true;
            MinimizeToTray();
        }

        private void WNMainSwitcher_Closed(object sender, EventArgs e)
        {
            try
            {
                if (MainSwitcher.CallController.ViewModelCallInfo != null)
                {
                    MainSwitcher.CallController.ViewModelCallInfo.IsNeedToSendEndSignal = true;
                    MainSwitcher.CallController.IsCallEnded = true;
                }
                if (UCGuiRingID.Instance == null)
                {
                    HideTrayIcon();
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }
            catch (Exception ex) { log.Error(ex.StackTrace + ex.Message); }
        }

        private void ExitItem_Click(object sender, EventArgs e)
        {
            //MessageBoxResult result = CustomMessageBox.ShowQuestion(NotificationMessages.EXIT_NOTIFICAITON, RingIDSettings.APP_NAME);
            //if (result == MessageBoxResult.Yes)
            //{
            bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.SURE_WANT_TO, "exit"), "Exit confirmation!");
            if (isTrue)
            {
                try
                {
                    systemTrayICon.Visible = false;
                    GC.Collect();
                    if (menuTopBar != null) menuTopBar.ShowSignoutLoader(true);
                }
                catch (Exception ex) { log.Error(ex.StackTrace + ex.Message); }
                finally { System.Diagnostics.Process.GetCurrentProcess().Kill(); }
            }
        }

        private void openItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Show();
                this.Activate();
                this.WindowState = WindowState.Normal;
            }
            catch (Exception ex) { log.Error(ex.StackTrace + ex.Message); }
        }

        private void SystemEvents_TimeChanged(object sender, EventArgs e)
        {
            _SystemTimeChanged += 1;
            if (_SystemTimeChanged % 2 == 1) ChatService.RequestOffline(SettingsConstants.VALUE_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT);
        }
        #endregion //"Event Triggers"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region"Navigate"
        public void Navigate(System.Windows.Controls.UserControl nextPage)
        {
            _RingIDBodyGrid.Child = null;
            _RingIDBodyGrid.Child = nextPage;
        }
        #endregion
    }
}

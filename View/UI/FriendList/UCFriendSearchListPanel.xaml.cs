﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Auth;
using View.Utility.FriendProfile;
using View.UI.PopUp;

namespace View.UI.FriendList
{
    /// <summary>
    /// Interaction logic for UCFriendSearchListPanel.xaml
    /// </summary>
    public partial class UCFriendSearchListPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendSearchListPanel).Name);

        public UCFriendSearchListPanel()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Property

        private long _SelectedFriendUserTableId = 0;
        public long SelectedFriendUserTableId
        {
            get { return _SelectedFriendUserTableId; }
            set { _SelectedFriendUserTableId = value; }
        }

        private ImageSource _Loader;
        public ImageSource Loader
        {
            get
            {
                return _Loader;
            }
            set
            {
                if (value == _Loader)
                    return;
                _Loader = value;
                OnPropertyChanged("Loader");
            }
        }

        private bool _NoResultsFound = false;
        public bool NoResultsFound
        {
            get
            {
                return _NoResultsFound;
            }
            set
            {
                if (value == _NoResultsFound)
                    return;
                if (string.IsNullOrWhiteSpace(ThreadLeftSearchFriend.searchParm)) { _NoResultsFound = false; }
                else { _NoResultsFound = value; }
                OnPropertyChanged("NoResultsFound");
            }
        }

        private ICommand _OpenPopupCommand;
        public ICommand OpenPopupCommand
        {
            get
            {
                if (_OpenPopupCommand == null)
                    _OpenPopupCommand = new RelayCommand(param => OpenPopupExecute(param));
                return _OpenPopupCommand;
            }
            set
            {
                _OpenPopupCommand = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Property

        public void ShowSearchingLoader(bool isSearching)
        {
            //Application.Current.Dispatcher.BeginInvoke(() =>
            //{
            //    if (isSearching)
            //    {
            //        LoaderSmall = ImageObjects.LOADER_SMALL;
            //    }
            //    else
            //    {
            //        if (ImageObjects.LOADER_SMALL != null)
            //            GC.SuppressFinalize(ImageObjects.LOADER_SMALL);
            //        if (LoaderSmall != null)
            //            GC.SuppressFinalize(LoaderSmall);
            //        ImageObjects.LOADER_SMALL = null;
            //        LoaderSmall = null;
            //    }
            //}, System.Windows.Threading.DispatcherPriority.Send);
        }
        #region Utility Method

        public void OpenPopupExecute(object parameter)
        {
            try
            {
                Button control = (Button)parameter;
                UserBasicInfoModel model = (UserBasicInfoModel)control.Tag;
                if (model.NumberOfMutualFriends > 0)
                {
                    if (UCMutualFriendsPopup.Instance != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(UCMutualFriendsPopup.Instance);
                    UCMutualFriendsPopup.Instance = new UCMutualFriendsPopup();
                    UCMutualFriendsPopup.Instance.SetParent(UCGuiRingID.Instance.MotherPanel);
                    UCMutualFriendsPopup.Instance.Show();
                    UCMutualFriendsPopup.Instance.ShowPopup(control, model.ShortInfoModel.UserTableID, model.ShortInfoModel.FullName);
                    UCMutualFriendsPopup.Instance.OnRemovedUserControl += () =>
                    {
                        UCMutualFriendsPopup.Instance.MutualList.Clear();
                        UCMutualFriendsPopup.Instance = null;
                    };

                    SendDataToServer.SendMutualFreindRequest(model.ShortInfoModel.UserTableID);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void Control_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;

                if (SelectedFriendUserTableId.Equals(model.ShortInfoModel.UserTableID))
                {
                    SelectedFriendUserTableId = 0;
                    model.ShortInfoModel.OnPropertyChanged("UserTableID");
                }
                else
                {
                    long tempUTId = SelectedFriendUserTableId;
                    SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                    model.ShortInfoModel.OnPropertyChanged("UserTableID");

                    UserBasicInfoModel tempModel = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(tempUTId);
                    if (tempModel != null)
                    {
                        tempModel.ShortInfoModel.OnPropertyChanged("UserTableID");
                    }
                }
                DragDrop.DoDragDrop(control, model, DragDropEffects.Copy);
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                ChangeOpenStatus();
            }
            catch (Exception ex)
            {

                log.Error("Error: MainContainer_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void MainContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                ChangeOpenStatus();
            }
            catch (Exception ex)
            {

                log.Error("Error: MainContainer_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private DispatcherTimer _ResizeTimer = null;
        public void ChangeOpenStatus()
        {
            try
            {
                if (_ResizeTimer == null)
                {
                    _ResizeTimer = new DispatcherTimer();
                    _ResizeTimer.Interval += TimeSpan.FromSeconds(1);
                    _ResizeTimer.Tick += (o, e) =>
                    {
                        List<object> list = HelperMethods.ChangeViewPortOpenedProperty(Scroll, FrienditmsControl);                        
                        if (list.Count > 0)
                        {
                            list = list.Where(P => ((UserBasicInfoModel)P).ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED).ToList();
                            if (list.Count > 0) new ThreadFriendPresenceInfo(list.Select(P => ((UserBasicInfoModel)P).ShortInfoModel.UserTableID).ToList());
                        }

                        _ResizeTimer.Stop();
                        _ResizeTimer.Interval = TimeSpan.FromSeconds(0.2);
                    };
                }

                _ResizeTimer.Stop();
                _ResizeTimer.Start();
            }
            catch (Exception ex)
            {

                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        #endregion Utility Method


        private void JumpTabOfFriendsProfile(long utId, int index)
        {
            dynamic state = new ExpandoObject();
            state.UtID = utId;
            state.type = "profile";
            state.TabIndx = index;
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeFriendProfile, state);
           UIHelperMethods.FocusMainWindow();
        }

        private void mnuAbout_Click(object sender, RoutedEventArgs e)
        {
            Control cntrl = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
            JumpTabOfFriendsProfile(model.ShortInfoModel.UserTableID, 1);
        }

        private void mnuPhotos_Click(object sender, RoutedEventArgs e)
        {
            Control cntrl = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
            JumpTabOfFriendsProfile(model.ShortInfoModel.UserTableID, 2);
        }

        private void mnuFriends_Click(object sender, RoutedEventArgs e)
        {
            Control cntrl = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
            JumpTabOfFriendsProfile(model.ShortInfoModel.UserTableID, 3);
        }

        private void mnuMV_Click(object sender, RoutedEventArgs e)
        {
            Control cntrl = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
            JumpTabOfFriendsProfile(model.ShortInfoModel.UserTableID, 4);
        }

        private void mnuFavourite_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Control cntrl = (Control)sender;
                UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
                new AddToFavouriteFriend().StartProcess(model);
            }
            catch (Exception ex)
            {
                log.Error("Error: mnuFavourite_Click() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideShowMorePanel()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                showMorePanel.Visibility = Visibility.Collapsed;
            }, System.Windows.Threading.DispatcherPriority.Send);
        }
        public void ShowLoader(bool show)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (!string.IsNullOrWhiteSpace(ThreadLeftSearchFriend.searchParm) && RingIDViewModel.Instance.TempFriendSearchList.Count > 0)
                {
                    if (show)
                    {
                        showMoreLoaderBorder.Visibility = Visibility.Visible;
                        Loader = ImageObjects.LOADER_FEED;
                    }
                    else
                    {
                        showMoreLoaderBorder.Visibility = Visibility.Collapsed;
                        showMorePanel.Visibility = Visibility.Visible;
                        showMoreButtonPanel.Visibility = Visibility.Visible;
                        if (ImageObjects.LOADER_FEED != null)
                            GC.SuppressFinalize(ImageObjects.LOADER_FEED);
                        if (Loader != null)
                            GC.SuppressFinalize(Loader);
                        ImageObjects.LOADER_FEED = null;
                        Loader = null;
                    }
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }
        private void ShowMore_PanelClick(object sender, RoutedEventArgs e)
        {
            ShowLoader(true);
            showMoreButtonPanel.Visibility = Visibility.Collapsed;
            ThreadLeftSearchFriend.SearchForShowMore = true;
            ThreadLeftSearchFriend.friendCountInOneRequest = 0;
            //SearchFriend.processingList = false;
            ThreadLeftSearchFriend.searchParm = UCGuiRingID.Instance.SearchTermTextBox.Text.ToLower();
            //  SearchFriend.sendSearchRequest();
            //if (!ThreadLeftSearchFriend.runningSearchFriendThread)
            //{
            //    (new ThreadLeftSearchFriend(ThreadLeftSearchFriend.SearchFromLeftPanel, StatusConstants.SEARCH_BY_ALL)).StartThread();
            //}
            //if (!ThreadLeftSearchFriend.runningSearchFriendThread)
            //{
            //    (new ThreadLeftSearchFriend(ThreadLeftSearchFriend.SearchFromLeftPanel, StatusConstants.SEARCH_BY_ALL)).StartThread();
            //}
            MainSwitcher.ThreadManager().LeftSearchFriend.StartThread(ThreadLeftSearchFriend.SearchFromLeftPanel, StatusConstants.SEARCH_BY_ALL);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            showMoreButtonPanel.Click += ShowMore_PanelClick;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            showMoreButtonPanel.Click -= ShowMore_PanelClick;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Auth;
using View.Utility.FriendList;
using View.Utility.FriendProfile;
using View.Utility.Suggestion;
using View.ViewModel;

namespace View.UI.FriendList
{
    /// <summary>
    /// Interaction logic for UCFriendList.xaml
    /// </summary>
    public partial class UCFriendList : UserControl, INotifyPropertyChanged
    {
        #region "Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendList).Name);
        public static UCFriendList Instance;
        private ThrdAddNonFavoriteFriend thrdAddNonFavoriteFriend;
        private DispatcherTimer _ResizeTimer = null;
        #endregion //"Fields"

        #region Constructor
        public UCFriendList()
        {
            InitializeComponent();
            Instance = this;
            this.DataContext = this;
        }
        #endregion Constructor

        #region Property

        private long selectedFriendUserTableId = 0;
        public long SelectedFriendUserTableId
        {
            get { return selectedFriendUserTableId; }
            set { selectedFriendUserTableId = value; }
        }

        private FriendFilterEnum filterType = FriendFilterEnum.TypeAllFriends;
        public FriendFilterEnum FilterType
        {
            get { return filterType; }
            set { filterType = value; OnPropertyChanged("FilterType"); }
        }

        private int selectedFilterType;
        public int SelectedFilterType
        {
            get { return selectedFilterType; }
            set
            {
                if (selectedFilterType == value) return;
                selectedFilterType = value; OnPropertyChanged("SelectedFilterType");
            }
        }

        private int totalFriend;
        public int TotalFriend
        {
            get { return totalFriend; }
            set
            {
                if (value == totalFriend) return;
                totalFriend = value; OnPropertyChanged("TotalFriend");
            }
        }

        private int totalFavFriend = 0;
        public int TotalFavFriend
        {
            get { return totalFavFriend; }
            set
            {
                totalFavFriend = value;
                OnPropertyChanged("TotalFavFriend");
            }
        }

        private int totalTopFriend = 0;
        public int TotalTopFriend
        {
            get { return totalTopFriend; }
            set
            {
                totalTopFriend = value;
                OnPropertyChanged("TotalTopFriend");
            }
        }

        private int totalNonFavFriend = 0;
        public int TotalNonFavFriend
        {
            get { return totalNonFavFriend; }
            set { totalNonFavFriend = value; OnPropertyChanged("TotalNonFavFriend"); }
        }

        private int selectedFriendType;
        public int SelectedFriendType
        {
            get { return selectedFriendType; }
            set { selectedFriendType = value; OnPropertyChanged("SelectedFriendType"); }
        }
        #endregion Property

        #region "Command and command methods"

        private ICommand filterCommand;
        public ICommand FilterCommand
        {
            get
            {
                if (filterCommand == null) filterCommand = new RelayCommand(param => OnFilterSelection(param));
                return filterCommand;
            }
        }
        private void OnFilterSelection(object param)
        {
            SelectedFilterType = Int32.Parse(param.ToString());
            FilterType = (FriendFilterEnum)Int32.Parse(param.ToString());

        }

        private ICommand openPopupCommand;
        public ICommand OpenPopupCommand
        {
            get
            {
                if (openPopupCommand == null) openPopupCommand = new RelayCommand(param => openPopupExecute(param));
                return openPopupCommand;
            }
            set { openPopupCommand = value; }
        }
        public void openPopupExecute(object parameter)
        {
            try
            {
                Button control = (Button)parameter;
                UserBasicInfoModel model = (UserBasicInfoModel)control.Tag;
                if (model.NumberOfMutualFriends > 0)
                {
                    if (UCMutualFriendsPopup.Instance != null)
                        mainPanel.Children.Remove(UCMutualFriendsPopup.Instance);
                    UCMutualFriendsPopup.Instance = new UCMutualFriendsPopup();
                    UCMutualFriendsPopup.Instance.SetParent(mainPanel);
                    UCMutualFriendsPopup.Instance.Show();
                    UCMutualFriendsPopup.Instance.ShowPopup(control, model.ShortInfoModel.UserTableID, model.ShortInfoModel.FullName);
                    UCMutualFriendsPopup.Instance.OnRemovedUserControl += () =>
                    {
                        UCMutualFriendsPopup.Instance.MutualList.Clear();
                        UCMutualFriendsPopup.Instance = null;
                    };
                    SendDataToServer.SendMutualFreindRequest(model.ShortInfoModel.UserTableID);
                }
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private ICommand addFriendButtonCommand;
        public ICommand AddFriendButtonCommand
        {
            get
            {
                if (addFriendButtonCommand == null) addFriendButtonCommand = new RelayCommand(param => OnAddFriendButtonClicked(param));
                return addFriendButtonCommand;
            }
        }
        public void OnAddFriendButtonClicked(object parameter)
        {
            try
            {
                UserBasicInfoModel model = (UserBasicInfoModel)parameter;
                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(model.ShortInfoModel.UserTableID);

                int accessType = -1;
                if (!HelperMethods.HasFriendChatPermission(model, blockModel, out accessType))
                {
                    if (HelperMethods.ShowBlockWarning(model, accessType)) return;
                }
                HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, model, blockModel, (status) =>
                {
                    if (status == false) return 0;
                    SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                    new ThrdAddFriend().StartProcess(model, true);
                    return 1;
                }, true);
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private ICommand removeButtonCommand;
        public ICommand RemoveButtonCommand
        {
            get
            {
                if (removeButtonCommand == null) removeButtonCommand = new RelayCommand(param => OnRemoveButtonClicked(param));
                return removeButtonCommand;
            }
        }
        public void OnRemoveButtonClicked(object parameter)
        {
            try
            {
                UserBasicInfoModel model = (UserBasicInfoModel)parameter;
                SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                string msg = string.Format(NotificationMessages.REMOVE_FROM_SUGGESTION, model.ShortInfoModel.FullName);
                // MessageBoxResult result = CustomMessageBox.ShowQuestion(msg);
                bool isTrue = UIHelperMethods.ShowQuestion(msg, "Remove confirmations");
                if (isTrue) AcceptRejectRequest(model, false);
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        #endregion //"Command and command methods"

        #region Utility Method
        private void AcceptRejectRequest(UserBasicInfoModel _model, bool isAccept)
        {
            _model.VisibilityModel.ShowActionButton = false;
            _model.VisibilityModel.ShowLoading = Visibility.Visible;
            new RemoveFromSuggestion(_model, true);
        }

        public void ShowNextFriendAfterAddOrRemove()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (RingIDViewModel.Instance.SuggestionFriendList.Where(p => p.VisibilityModel.IsVisibleInPeopleUmayKnow == Visibility.Visible).ToList().Count < 5)
                {
                    UserBasicInfoModel suggestionModel = RingIDViewModel.Instance.SuggestionFriendList.Where(P => P.VisibilityModel.IsVisibleInSuggestion == Visibility.Visible).FirstOrDefault();
                    if (suggestionModel != null)
                    {
                        suggestionModel.VisibilityModel.IsVisibleInSuggestion = Visibility.Collapsed;
                        suggestionModel.VisibilityModel.IsVisibleInPeopleUmayKnow = Visibility.Visible;
                    }
                    else
                    {
                        foreach (KeyValuePair<long, bool> obj in FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
                        {
                            if (obj.Value == false)
                            {
                                List<long> list = new List<long>();
                                list.Add(obj.Key);
                                (new ThreadSuggestionUsersDetailsRequest()).StartThread(list);
                                break;
                            }
                        }
                    }
                }
            });
        }

        private void startThrdAddNonFavoriteFriend()
        {
            if (thrdAddNonFavoriteFriend == null) thrdAddNonFavoriteFriend = new ThrdAddNonFavoriteFriend();
            thrdAddNonFavoriteFriend.StartThread(10);
        }

        public void ChangeOpenStatus()
        {
            try
            {
                if (_ResizeTimer == null)
                {
                    _ResizeTimer = new DispatcherTimer();
                    _ResizeTimer.Interval += TimeSpan.FromSeconds(1);
                    _ResizeTimer.Tick += (o, e) =>
                    {
                        List<object> recentlyAddedList = HelperMethods.ChangeViewPortOpenedProperty(Scroll, RecentlyAddedFriend);
                        List<object> favouriteList = HelperMethods.ChangeViewPortOpenedProperty(Scroll, FavouriteFriend);
                        List<object> topList = HelperMethods.ChangeViewPortOpenedProperty(Scroll, TopFriend);
                        List<object> nonFavouriteList = HelperMethods.ChangeViewPortOpenedProperty(Scroll, NonFavouriteFriend);
                        List<object> specialList = HelperMethods.ChangeViewPortOpenedProperty(Scroll, SpecialFriend);
                        HelperMethods.ChangeViewPortOpenedProperty(Scroll, pplUMayKnow);

                        if (recentlyAddedList.Count > 0) new ThreadFriendPresenceInfo(recentlyAddedList.Select(P => ((UserBasicInfoModel)P).ShortInfoModel.UserTableID).ToList());
                        if (favouriteList.Count > 0) new ThreadFriendPresenceInfo(favouriteList.Select(P => ((UserBasicInfoModel)P).ShortInfoModel.UserTableID).ToList());
                        if (topList.Count > 0) new ThreadFriendPresenceInfo(topList.Select(P => ((UserBasicInfoModel)P).ShortInfoModel.UserTableID).ToList());
                        if (nonFavouriteList.Count > 0) new ThreadFriendPresenceInfo(nonFavouriteList.Select(P => ((UserBasicInfoModel)P).ShortInfoModel.UserTableID).ToList());
                        if (specialList.Count > 0) new ThreadFriendPresenceInfo(specialList.Select(P => ((UserBasicInfoModel)P).ShortInfoModel.UserTableID).ToList());

                        if (this.Scroll.VerticalOffset > 500
                            && this.Scroll.VerticalOffset > Scroll.ScrollableHeight - 100
                            && !RingIDViewModel.Instance.WinDataModel.IsLoadingCotactList)
                            startThrdAddNonFavoriteFriend();
                        _ResizeTimer.Stop();
                        _ResizeTimer.Interval = TimeSpan.FromSeconds(0.2);
                    };
                }
                _ResizeTimer.Stop();
                _ResizeTimer.Start();
            }
            catch (Exception ex) { log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        public void GetFriendCount()
        {
            if (FilterType == FriendFilterEnum.TypeFav) TotalFriend = RingIDViewModel.Instance.FavouriteFriendList.Count;
            else if (FilterType == FriendFilterEnum.TypeTop) TotalFriend = RingIDViewModel.Instance.TopFriendList.Count;
            else if (FilterType == FriendFilterEnum.TypeRecentlyAdded) TotalFriend = RingIDViewModel.Instance.RecentlyAddedFriendList.Count;
            else if (FilterType == FriendFilterEnum.Official) TotalFriend = RingIDViewModel.Instance.SpecialFriendList.Count;
            else FriendListController.Instance.FriendDataContainer.TotalFriends();
        }

        private void JumpTabOfFriendsProfile(long utId, int index)
        {
            dynamic state = new ExpandoObject();
            state.UtID = utId;
            state.type = "profile";
            state.TabIndx = index;
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeFriendProfile, state);
            UIHelperMethods.FocusMainWindow();
        }

        #endregion Utility Method

        #region "Event Triggers"

        private void Control_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Control control = (Control)sender;
                int friendType = int.Parse(control.Tag.ToString());
                UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
                if (SelectedFriendUserTableId.Equals(model.ShortInfoModel.UserTableID) && SelectedFriendType == friendType)
                {
                    SelectedFriendUserTableId = 0;
                    SelectedFriendType = 0;
                    model.ShortInfoModel.OnPropertyChanged("UserTableID");
                }
                else
                {
                    long tempUTId = SelectedFriendUserTableId;
                    SelectedFriendUserTableId = model.ShortInfoModel.UserTableID;
                    SelectedFriendType = friendType;
                    model.ShortInfoModel.OnPropertyChanged("UserTableID");

                    UserBasicInfoModel tempModel = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(tempUTId);
                    if (tempModel != null)
                    {
                        tempModel.ShortInfoModel.OnPropertyChanged("UserTableID");
                    }
                }
                if (RingIDViewModel.Instance.RecentlyAddedFriendList.Contains(model) && model.ContactAddedReadUnread == SettingsConstants.RECENT_FRND_UNREAD) // Unread
                {
                    model.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_READ; // Read
                    UserBasicInfoDTO userDto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(model.ShortInfoModel.UserTableID);
                    if (userDto != null)
                    {
                        List<UserBasicInfoDTO> basicInfos = new List<UserBasicInfoDTO>();
                        userDto.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_READ;
                        basicInfos.Add(userDto);
                        new InsertIntoUserBasicInfoTable(basicInfos).Start();
                    }
                }
                DragDrop.DoDragDrop(control, model, DragDropEffects.Copy);
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void Control_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Control control = (Control)sender;
            UserBasicInfoModel userBasicInfoModel = (UserBasicInfoModel)control.DataContext;
        }

        private void MainContainer_SizeChanged(object sender, SizeChangedEventArgs args)
        {
            try { ChangeOpenStatus(); }
            catch (Exception ex) { log.Error("Error: MainContainer_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            try { if (Math.Abs(e.VerticalChange) > 0) ChangeOpenStatus(); }
            catch (Exception ex) { log.Error("Error: Scroll_ScrollChanged() ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        private void SpecialFriend_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Control cntrl = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
            RingIDViewModel.Instance.OnFriendCallChatButtonClicked(model.ShortInfoModel.UserTableID);
        }

        private void mnuAbout_Click(object sender, RoutedEventArgs e)
        {
            Control cntrl = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
            JumpTabOfFriendsProfile(model.ShortInfoModel.UserTableID, 1);
        }

        private void mnuPhotos_Click(object sender, RoutedEventArgs e)
        {
            Control cntrl = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
            JumpTabOfFriendsProfile(model.ShortInfoModel.UserTableID, 2);
        }

        private void mnuFriends_Click(object sender, RoutedEventArgs e)
        {
            Control cntrl = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
            JumpTabOfFriendsProfile(model.ShortInfoModel.UserTableID, 3);
        }

        private void mnuMV_Click(object sender, RoutedEventArgs e)
        {
            Control cntrl = (Control)sender;
            UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
            JumpTabOfFriendsProfile(model.ShortInfoModel.UserTableID, 4);
        }

        private void mnuFavourite_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Control cntrl = (Control)sender;
                UserBasicInfoModel model = (UserBasicInfoModel)cntrl.DataContext;
                new AddToFavouriteFriend().StartProcess(model);
            }
            catch (Exception ex) { log.Error("Error: mnuFavourite_Click() ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        #endregion //"Event Triggers"

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion//INotifyPropertyChanged Members
    }
}

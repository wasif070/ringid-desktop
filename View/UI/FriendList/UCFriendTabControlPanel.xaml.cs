﻿using Auth.utility;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.UI.Call;
using View.UI.Chat;
using View.UI.Notification;
using View.Utility.Notification;
using View.Utility;

namespace View.UI.FriendList
{
    /// <summary>
    /// Interaction logic for UCFriendTabControlPanel.xaml
    /// </summary>
    public partial class UCFriendTabControlPanel : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCFriendTabControlPanel).Name);

        public string tabItem;
        public static UCFriendTabControlPanel Instance = null;

        public UCFriendTabControlPanel()
        {
            Instance = this;
            InitializeComponent();
        }

        private void LeftSideTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                tabItem = ((sender as TabControl).SelectedItem as TabItem).Name as string;
                switch (tabItem)
                {
                    case "FriendListTab":
                        break;

                    case "CallLogTab":
                        if (UCCallLog.Instance == null)
                        {
                            _CallLogPanel.Children.Add(new UCCallLog());
                            UCCallLog.Instance.LoadCallLogs();
                        }
                        break;

                    case "ChatLogTab":
                        if (UCChatLogPanel.Instance == null)
                        {
                            _ChatLogPanel.Children.Add(new UCChatLogPanel());
                            UCChatLogPanel.Instance.LoadChatLogs();
                        }
                        break;

                    case "AllNotificationTab":
                        if (UCAllNotification.Instance == null)
                        {
                            _NotificationPanel.Children.Add(new UCAllNotification());
                            //UCAllNotification.Instance.LoadShowMoreData();
                        }
                        else if (VMNotification.Instance.NotificationModelCount == 0)
                        {
                            VMNotification.Instance.LoadData();
                        }

                        MainSwitcher.AuthSignalHandler().notificationSignalHandler.ClearAllNotificationCount();

                        break;

                }
            }
            catch (Exception ex)
            {
                log.Error("Error: TabControl_SelectionChanged() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void SelectLeftSideTabControl(int index = 0)
        {
            if (LeftSideTabControl.SelectedIndex != index)
            {
                LeftSideTabControl.SelectedIndex = index;
            }
        }

        private void AllNotificationTab_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MainSwitcher.AuthSignalHandler().notificationSignalHandler.ClearAllNotificationCount();
        }
    }
}

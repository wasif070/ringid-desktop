﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.ViewModel;

namespace View.UI.Followinglist
{
    /// <summary>
    /// Interaction logic for UCFollowingMainPanel.xaml
    /// </summary>
    public partial class UCFollowingMainPanel : UserControl, INotifyPropertyChanged
    {
        public UCFollowingMainPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            ChangeTab(0);
            this.Loaded += UserControl_Loaded;
            this.Unloaded += UserControl_Unloaded;
        }

        #region "PropertyChanged"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region "Property"
        private CustomObservableCollection _ViewCollection;
        public CustomObservableCollection ViewCollection
        {
            get { return _ViewCollection; }
            set
            {
                _ViewCollection = value;
                this.OnPropertyChanged("ViewCollection");
            }
        }
        #endregion

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChangeTab(FollowingControl.SelectedIndex);
        }

        private void ChangeTab(int idx)
        {
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += (s, ex) =>
            {
                switch (idx)
                {
                    case 0:
                        //ViewCollection = RingIDViewModel.Instance.SavedFeeds;
                        ScrlViewer.SetScrollValues(ViewCollection, FeedDataContainer.Instance.SavedFeedsAllSortedIds, 100, AppConstants.TYPE_ALL_SAVED_FEEDS);
                        if (!AllReq)
                        {
                            AllReq = true;
                            DefaultSettings.SAVEDALL_STARTPKT = Auth.utility.SendToServer.GetRanDomPacketID();
                            ScrlViewer.RequestFeeds(0, 2, 0, DefaultSettings.SAVEDALL_STARTPKT);
                        }
                        break;
                    default:
                        break;
                }
                timer.Stop();
            };
            timer.Stop();
            timer.Start();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.FollowingSelection = true;
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.FollowingSelection = true;
        }

        DispatcherTimer timer = new DispatcherTimer();
        bool AllReq = false, PortalReq = false, PageReq = false, MediaReq = false, CelebReq = false;
    }
}

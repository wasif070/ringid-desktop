﻿using Auth.Service.RingMarket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.ViewModel;

namespace View.UI.RingMarket.MenuMarketSticker
{
    /// <summary>
    /// Interaction logic for UCMenuStickerWrapper.xaml
    /// </summary>
    public partial class UCMenuStickerWrapper : UserControl
    {
        public static UCMenuStickerWrapper Instance;

        public UCStickerMarketInitPanel _UCStickerMarketInitPanel;
        public UCMarketStickerDetailsViewPanel _UCStickerDetailsViewPanel;
        public UCStickerMarketDownloadView _UCStickerMarketDownloadView;

        public UCMenuStickerWrapper()
        {
            InitializeComponent();
            Instance = this;
            this.Loaded += UCMenuStickerWrapper_Loaded;
            this.Unloaded += UCMenuStickerWrapper_Unloaded;
            ShowStickerInitView();
        }

        #region Utility

        public void ShowStickerInitView()
        {
            if (_UCStickerMarketInitPanel == null)
            {
                _UCStickerMarketInitPanel = new UCStickerMarketInitPanel();
            }
            Parent.Child = _UCStickerMarketInitPanel;
        }

        public void ShowStickerDetailsView()
        {
            if (_UCStickerDetailsViewPanel == null)
            {
                _UCStickerDetailsViewPanel = new UCMarketStickerDetailsViewPanel();
            }
            Parent.Child = _UCStickerDetailsViewPanel;
        }

        public void ShowStickerDownloadView()
        {
            if (_UCStickerMarketDownloadView == null)
            {
                _UCStickerMarketDownloadView = new UCStickerMarketDownloadView();
            }
            Parent.Child = _UCStickerMarketDownloadView;
        }



        #endregion

        #region EventHandler

        private void UCMenuStickerWrapper_Loaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.StickerMarketButtonSelection = true;
        }

        private void UCMenuStickerWrapper_Unloaded(object sender, RoutedEventArgs e)
        {
            RingIDViewModel.Instance.StickerMarketButtonSelection = false;
            this._UCStickerMarketInitPanel.TabType = 0;
            if (_UCStickerDetailsViewPanel != null)
            {
                _UCStickerDetailsViewPanel.MarketStickerDetailList.Clear();
                _UCStickerDetailsViewPanel.MarketStickerSlidingBannerList.Clear();
                GC.SuppressFinalize(_UCStickerDetailsViewPanel.MarketStickerDetailList);
                GC.SuppressFinalize(_UCStickerDetailsViewPanel.MarketStickerSlidingBannerList);
                GC.Collect();
            }
            if (_UCStickerMarketDownloadView != null)
            {
                _UCStickerMarketDownloadView.SelectedCategoryModel = null;
            }
            //RingIDViewModel.Instance._IsNewStickerClicked = false;
            //ShowStickerInitView();
        }

        #endregion

    }
}

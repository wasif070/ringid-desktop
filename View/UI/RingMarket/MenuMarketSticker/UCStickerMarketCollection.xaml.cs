﻿using Auth.Service.RingMarket;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.RingMarket.MenuMarketSticker;
using View.Utility;
using View.Utility.RingMarket;
using View.ViewModel;



namespace View.UI.RingMarket
{
    /// Interaction logic for UCStickerMarketCollection.xaml
    /// </summary>
    public partial class UCStickerMarketCollection : UserControl, INotifyPropertyChanged
    {
        private Thread _CollectionThread;
        private int _Type_Collection = 11;

        private bool _IsInitialize = false;
        private bool BOTTOM_LOADING = false;
        public bool _StickerMarketCollectionLoaded = false;
        int _ItemsNeedToLoad;
        private const double _SingleItemsMaxWidth = 630;
        private const double _SingleItemsMaxHeight = 170;
        private ICommand _StickerDetailsCommand;
        private ScrollViewer _myScroll = null;
        private Thread ScrollThread = null;

        public UCStickerMarketCollection(ScrollViewer _ParentScroll)
        {
            InitializeComponent();
            this.DataContext = this;
            this.InitData();
            this._myScroll = _ParentScroll;
            this._myScroll.ScrollChanged += _myScroll_ScrollChanged;
        }

        #region Utility

        public void InitData()
        {
            var screen = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            double screenHeight = screen.Height;
            _ItemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - CollectionModelList.Count;
        }

        public void LoadCollectionWithCategory()
        {
            if (_StickerMarketCollectionLoaded == false && !(_CollectionThread != null && _CollectionThread.IsAlive))
            {
                _CollectionThread = new System.Threading.Thread(RunCollectionThHread);
                _CollectionThread.Start();
            }
        }

        private void RunCollectionThHread()
        {
            List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

            foreach (MarketStickerCollectionDTO collectionDto in StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY.Values)
            {
                if (CollectionModelList.Where(P => P.StickerCollectionID == collectionDto.sClId).FirstOrDefault() == null)
                {
                    MarketStickerCollectionModel collectionModel = new MarketStickerCollectionModel(collectionDto);
                    CollectionModelList.InvokeAdd(collectionModel);
                    System.Threading.Thread.Sleep(10);
                    foreach (int categoryId in collectionDto.catIds)
                    {
                        MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == categoryId).FirstOrDefault();
                        if (ctgModel == null)
                        {
                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == categoryId).FirstOrDefault();
                            if (ctgDTO != null)
                            {
                                ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }
                        }
                        if (ctgModel != null)
                        {
                            if (collectionModel.CategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                            {
                                collectionModel.CategoryList.InvokeAdd(ctgModel);
                                System.Threading.Thread.Sleep(5);
                            }
                        }
                    }
                }
            }

            this.LoadMoreInitDataIfNeeds();
            _StickerMarketCollectionLoaded = true;
        }

        public void LoadMoreInitDataIfNeeds()
        {
            try
            {
                if (_IsInitialize == false)
                {
                    int collectionLimit = _ItemsNeedToLoad;
                    int categoryLimit = 4, stickerType = StickerMarketConstants.STICKER_TYPE_COLLECTION;
                    this.GetMoreCollections(CollectionModelList.Count, categoryLimit, collectionLimit, stickerType);
                    _IsInitialize = true;
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketCollection).Name).Error("Error: LoadInitDataIfNeeds() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void GetMoreCollections(int startIndex, int categoryLimit, int collectionLimit, int stickerType)
        {
            MarketStickerDTO marketStickerDTO = RingMarketStickerService.Instance.GetMoreStickerCollections(startIndex, categoryLimit, collectionLimit, stickerType);

            if (marketStickerDTO != null && marketStickerDTO.sucs == true)
            {
                if (marketStickerDTO.colList != null && marketStickerDTO.colList.Count > 0)
                {
                    List<MarketStickerCategoryModel> modelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

                    foreach (MarketStickerCollectionDTO collDTO in marketStickerDTO.colList)
                    {
                        MarketStickerCollectionModel collModel = CollectionModelList.Where(P => P.StickerCollectionID == collDTO.sClId).FirstOrDefault();
                        if (collModel == null)
                        {
                            collModel = new MarketStickerCollectionModel(collDTO);
                            CollectionModelList.InvokeAdd(collModel);
                        }

                        if (collDTO.catIds != null && collDTO.catIds.Count > 0)
                        {
                            foreach (int categoryId in collDTO.catIds)
                            {
                                MarketStickerCategoryDTO ctgDTO = marketStickerDTO.catList.Where(P => P.sCtId == categoryId).FirstOrDefault();
                                if (ctgDTO != null)
                                {
                                    MarketStickerCategoryModel ctgModel = modelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                                    if (ctgModel == null)
                                    {
                                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                    }

                                    if (collModel.CategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                                    {
                                        collModel.CategoryList.InvokeAdd(ctgModel);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.BOTTOM_LOADING = false;
        }

        private void LoadCollectionWithinScroll()
        {
            int startIndex = CollectionModelList.Count, categoryLimit = 4, collectionLimit =  5, stickerType = StickerMarketConstants.STICKER_TYPE_COLLECTION;
            this.GetMoreCollections(startIndex, categoryLimit, collectionLimit, stickerType);
            this.BOTTOM_LOADING = false;
        }

        private void OnStickerDetailsCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)param;
            UCMenuStickerWrapper.Instance.ShowStickerDownloadView();
            UCMenuStickerWrapper.Instance._UCStickerMarketDownloadView.LoadSelectedSticker(ctgModel, false);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region EventHander

        private void _myScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (this._IsInitialize && this.IsVisible)
            {
                var scrollViewer = (ScrollViewer)sender;

                if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                }

                else if (e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight && BOTTOM_LOADING == false)
                {
                    this.BOTTOM_LOADING = true;
                    if (!(ScrollThread != null && ScrollThread.IsAlive))
                    {
                        ScrollThread = new Thread(new ThreadStart(LoadCollectionWithinScroll));
                        ScrollThread.Start();
                    }
                }
            }
        }

        private void BtnTopSeeAll_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MarketStickerCollectionModel model = (MarketStickerCollectionModel)((Button)sender).DataContext;
            UCMenuStickerWrapper.Instance.ShowStickerDetailsView();
            UCMenuStickerWrapper.Instance._UCStickerDetailsViewPanel.LoadShowMoreData(_Type_Collection, model.StickerCollectionID, model.StickerCollectionName);
        }

        private void Grid_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)((Grid)sender).DataContext;
            UCMenuStickerWrapper.Instance.ShowStickerDownloadView();
            UCMenuStickerWrapper.Instance._UCStickerMarketDownloadView.LoadSelectedSticker(ctgModel, false);
        }

        #endregion

        #region Property

        private ObservableCollection<MarketStickerCollectionModel> _CollectionModelList = new ObservableCollection<MarketStickerCollectionModel>();
        public ObservableCollection<MarketStickerCollectionModel> CollectionModelList
        {
            get { return _CollectionModelList; }
            set
            {
                if (value == _CollectionModelList)
                    return;

                _CollectionModelList = value;
                this.OnPropertyChanged("CollectionModelList");
            }
        }

        #endregion

        #region Command

        public ICommand StickerDetailsCommand
        {
            get
            {
                if (_StickerDetailsCommand == null)
                {
                    _StickerDetailsCommand = new RelayCommand(param => OnStickerDetailsCommand(param));
                }
                return _StickerDetailsCommand;
            }
        }

        #endregion
    }
}

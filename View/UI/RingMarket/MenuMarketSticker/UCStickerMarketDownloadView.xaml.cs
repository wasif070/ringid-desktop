<<<<<<< HEAD
﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Auth.Service.RingMarket;
using log4net;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.RingMarket;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.UI.PopUp.MediaSendViaChat;
using Models.Constants;

namespace View.UI.RingMarket.MenuMarketSticker
{
    /// <summary>
    /// Interaction logic for UCStickerMarketDownloadView.xaml
    /// </summary>
    public partial class UCStickerMarketDownloadView : UserControl, INotifyPropertyChanged
    {
        public bool _IsFromDetailsView = false;
        private readonly ILog log = LogManager.GetLogger(typeof(UCStickerMarketDownloadView).Name);
        public UCStickerMarketDownloadView()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region utility

        public void LoadSelectedSticker(MarketStickerCategoryModel ctgModel, bool isFromDetailsView)
        {
            this.SelectedCategoryModel = ctgModel;
            this._IsFromDetailsView = isFromDetailsView;

            if (ctgModel.Downloaded == true)
            {
                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgModel.StickerCategoryID).FirstOrDefault();
                RingMarketStickerLoadUtility.LaodAndDownLoadStickerImages(ctgModel, ctgDTO);
            }

            if (ctgModel.IsNew && ctgModel.IsNewStickerSeen == false && StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Contains(ctgModel.StickerCategoryID))
            {
                ctgModel.IsNewStickerSeen = true;
                RingIDViewModel.Instance.NewStickerNotificationCounter = RingIDViewModel.Instance.NewStickerNotificationCounter - 1;

                List<MarketStickerCategoryDTO> ctgDTOList = new List<MarketStickerCategoryDTO>();
                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.IsNewStickerSeen = true;
                    ctgDTOList.Add(ctgDTO);
                    new InsertIntoRingMarketStickerTable(ctgDTOList);
                }
            }
        }

        private void OnDownloadCommand()
        {
            if (SelectedCategoryModel.Downloaded == false) // do download
            {
                SelectedCategoryModel.DownloadPercentage = 0;
                SelectedCategoryModel.IsDownloadRunning = true;

                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(SelectedCategoryModel.StickerCategoryID, out ctgDTO))
                {
                    RingMarketStickerLoadUtility.DownLoadStickerImages(ctgDTO, (isNointernet) =>
                    {
                        if (isNointernet)
                        {
                            SelectedCategoryModel.DownloadPercentage = 0;
                            SelectedCategoryModel.IsDownloadRunning = false;
                            UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                        }
                        return 0;
                    });
                }
            }
            else   // Remove Sticker
            {
                if (SelectedCategoryModel.IsDefault)
                {
                    UIHelperMethods.ShowWarning("Can't remove default sticker!", "Remove sticker");
                    //  CustomMessageBox.ShowError("Can't Remove Default Sticker!");
                    return;
                }

                MarketStickerCategoryDTO ctgDTO = null;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(SelectedCategoryModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.Downloaded = false;
                    List<MarketStickerCategoryDTO> ctgDTOs = new List<MarketStickerCategoryDTO>();
                    ctgDTOs.Add(ctgDTO);
                    RingMarketStickerLoadUtility.LoadMarketStickerRecentCategorys(ctgDTOs);
                    RingMarkerStickerDAO.UpdateStickerCategoryDownloadStatus(ctgDTO.sCtId, ctgDTO.Downloaded);
                }

                SelectedCategoryModel.Downloaded = false;
                SelectedCategoryModel.IsDownloadRunning = false;
                SelectedCategoryModel.DownloadPercentage = 0;
                SelectedCategoryModel.OnPropertyChanged("DetailImage");
            }
        }

        private void OnBackCommand()
        {
            if (_IsFromDetailsView == false)
            {
                UCMenuStickerWrapper.Instance.ShowStickerInitView();
            }
            else
            {
                UCMenuStickerWrapper.Instance.ShowStickerDetailsView();
                UCMenuStickerWrapper.Instance._UCStickerDetailsViewPanel.CallBackFromDownloadView();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property


        private MarketStickerCategoryModel _SelectedCategoryModel;
        public MarketStickerCategoryModel SelectedCategoryModel
        {
            get { return _SelectedCategoryModel; }
            set
            {
                _SelectedCategoryModel = value;
                OnPropertyChanged("SelectedCategoryModel");
            }
        }

        private bool _IsContextMenuOpening;
        public bool IsContextMenuOpening
        {
            get { return _IsContextMenuOpening; }
            set
            {
                _IsContextMenuOpening = value;
                OnPropertyChanged("IsContextMenuOpening");
            }
        }

        #endregion

        #region EventHandler

        private void MenuSendviaChat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)((MenuItem)sender).DataContext;
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetStickerImage(imgModel);
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + "==>" + ex.Message);
            }
        }

        private void Button_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            Button button = (Button)sender;
            MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)(button.DataContext);
            imgModel.SingleStickerImageFocusable = true;
        }

        private void Button_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            Button button = (Button)sender;
            MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)(button.DataContext);
            imgModel.SingleStickerImageFocusable = false;
        }

        private void MenuItmDownload_Click(object sender, RoutedEventArgs e)
        {
            this.OnDownloadCommand();
        }

        private void ContextMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            IsContextMenuOpening = true;
        }

        private void ContextMenu_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            IsContextMenuOpening = false;
        }

        #endregion

        #region Command

        private ICommand _DownloadCommand;
        public ICommand DownloadCommand
        {
            get
            {
                if (_DownloadCommand == null)
                {
                    _DownloadCommand = new RelayCommand(param => OnDownloadCommand());
                }
                return _DownloadCommand;
            }
        }

        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommand());
                }
                return _BackCommand;
            }
        }

        #endregion
    }
}
=======
﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Auth.Service.RingMarket;
using log4net;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.RingMarket;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.UI.PopUp.MediaSendViaChat;
using Models.Constants;

namespace View.UI.RingMarket.MenuMarketSticker
{
    /// <summary>
    /// Interaction logic for UCStickerMarketDownloadView.xaml
    /// </summary>
    public partial class UCStickerMarketDownloadView : UserControl, INotifyPropertyChanged
    {
        public bool _IsFromDetailsView = false;
        private readonly ILog log = LogManager.GetLogger(typeof(UCStickerMarketDownloadView).Name);
        public UCStickerMarketDownloadView()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region utility

        public void LoadSelectedSticker(MarketStickerCategoryModel ctgModel, bool isFromDetailsView)
        {
            this.SelectedCategoryModel = ctgModel;
            this._IsFromDetailsView = isFromDetailsView;

            if (ctgModel.Downloaded == true)
            {
                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgModel.StickerCategoryID).FirstOrDefault();
                RingMarketStickerLoadUtility.DownLoadStickerImages(ctgModel, ctgDTO, (isNointernet) =>
                {
                    if (isNointernet)
                    {
                        ctgModel.DownloadPercentage = 0;
                        ctgModel.IsDownloadRunning = false;
                        UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                    }
                    return 0;
                });
            }

            if (ctgModel.IsNew && ctgModel.IsNewStickerSeen == false && StickerDictionaries.Instance.TEMP_NEW_STICKER_CATEGORY_IDS.Contains(ctgModel.StickerCategoryID))
            {
                ctgModel.IsNewStickerSeen = true;
                RingIDViewModel.Instance.NewStickerNotificationCounter = RingIDViewModel.Instance.NewStickerNotificationCounter - 1;

                List<MarketStickerCategoryDTO> ctgDTOList = new List<MarketStickerCategoryDTO>();
                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.IsNewStickerSeen = true;
                    ctgDTOList.Add(ctgDTO);
                    new InsertIntoRingMarketStickerTable(ctgDTOList);
                }
            }
        }

        private void OnDownloadCommand()
        {
            if (SelectedCategoryModel.Downloaded == false) // do download
            {
                SelectedCategoryModel.DownloadPercentage = 0;
                SelectedCategoryModel.IsDownloadRunning = true;

                MarketStickerCategoryDTO ctgDTO;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(SelectedCategoryModel.StickerCategoryID, out ctgDTO))
                {
                    RingMarketStickerLoadUtility.DownLoadStickerImages(SelectedCategoryModel, ctgDTO, (isNointernet) =>
                    {
                        if (isNointernet)
                        {
                            SelectedCategoryModel.DownloadPercentage = 0;
                            SelectedCategoryModel.IsDownloadRunning = false;
                            UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                        }
                        return 0;
                    });
                }
            }
            else   // Remove Sticker
            {
                if (SelectedCategoryModel.IsDefault)
                {
                    UIHelperMethods.ShowWarning("Can't remove default sticker!", "Remove sticker");
                    //  CustomMessageBox.ShowError("Can't Remove Default Sticker!");
                    return;
                }

                MarketStickerCategoryDTO ctgDTO = null;
                if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(SelectedCategoryModel.StickerCategoryID, out ctgDTO))
                {
                    ctgDTO.Downloaded = false;
                    List<MarketStickerCategoryDTO> ctgDTOs = new List<MarketStickerCategoryDTO>();
                    ctgDTOs.Add(ctgDTO);
                    RingMarketStickerLoadUtility.LoadMarketStickerRecentCategorys(ctgDTOs);
                    RingMarkerStickerDAO.UpdateStickerCategoryDownloadStatus(ctgDTO.sCtId, ctgDTO.Downloaded);
                }

                SelectedCategoryModel.Downloaded = false;
                SelectedCategoryModel.IsDownloadRunning = false;
                SelectedCategoryModel.DownloadPercentage = 0;
                SelectedCategoryModel.OnPropertyChanged("DetailImage");
            }
        }

        private void OnBackCommand()
        {
            if (_IsFromDetailsView == false)
            {
                UCMenuStickerWrapper.Instance.ShowStickerInitView();
            }
            else
            {
                UCMenuStickerWrapper.Instance.ShowStickerDetailsView();
                UCMenuStickerWrapper.Instance._UCStickerDetailsViewPanel.CallBackFromDownloadView();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property


        private MarketStickerCategoryModel _SelectedCategoryModel;
        public MarketStickerCategoryModel SelectedCategoryModel
        {
            get { return _SelectedCategoryModel; }
            set
            {
                _SelectedCategoryModel = value;
                OnPropertyChanged("SelectedCategoryModel");
            }
        }

        private bool _IsContextMenuOpening;
        public bool IsContextMenuOpening
        {
            get { return _IsContextMenuOpening; }
            set
            {
                _IsContextMenuOpening = value;
                OnPropertyChanged("IsContextMenuOpening");
            }
        }

        #endregion

        #region EventHandler

        private void MenuSendviaChat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)((MenuItem)sender).DataContext;
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetStickerImage(imgModel);
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + "==>" + ex.Message);
            }
        }

        private void Button_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            Button button = (Button)sender;
            MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)(button.DataContext);
            imgModel.SingleStickerImageFocusable = true;
        }

        private void Button_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            Button button = (Button)sender;
            MarkertStickerImagesModel imgModel = (MarkertStickerImagesModel)(button.DataContext);
            imgModel.SingleStickerImageFocusable = false;
        }

        private void MenuItmDownload_Click(object sender, RoutedEventArgs e)
        {
            this.OnDownloadCommand();
        }

        private void ContextMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            IsContextMenuOpening = true;
        }

        private void ContextMenu_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            IsContextMenuOpening = false;
        }

        #endregion

        #region Command

        private ICommand _DownloadCommand;
        public ICommand DownloadCommand
        {
            get
            {
                if (_DownloadCommand == null)
                {
                    _DownloadCommand = new RelayCommand(param => OnDownloadCommand());
                }
                return _DownloadCommand;
            }
        }

        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommand());
                }
                return _BackCommand;
            }
        }

        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

<<<<<<< HEAD
﻿using Auth.Service.RingMarket;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Effects;
using View.BindingModels;
using View.Constants;
using View.UI.RingMarket.MenuMarketSticker;
using View.Utility;
using View.Utility.RingMarket;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for UCStickerMarketHome.xaml
    /// </summary>
    public partial class UCStickerMarketHome : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStickerMarketHome).Name);

        private const int Type_Popular = 5;
        private const int Type_New = 3;
        private const int Type_All = 2;
        private const int Type_Free = 1;

        public bool _StickerMarketHomeLoaded = false;
        public bool _StickerMarketContinentLoaded = false;
        public bool _AllCollectionLoaded = false;
        private Thread _HomeThread = null;
        private ICommand _StickerDetailsCommand;

        private ObservableCollection<MarketStickerCategoryModel> _StickerAllCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        private ObservableCollection<MarketStickerCategoryModel> _StickerPopularCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        private ObservableCollection<MarketStickerCategoryModel> _StickerNewCategoryList = new ObservableCollection<MarketStickerCategoryModel>();

        public UCStickerMarketHome()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCStickerMarketHome_Loaded;
            this.Unloaded += UCStickerMarketHome_Unloaded;
        }

        #region Utility

        public void LoadHomeCategory()
        {

            if (_StickerMarketHomeLoaded == false && !(_HomeThread != null && _HomeThread.IsAlive))
            {
                _HomeThread = new System.Threading.Thread(RunHomeThread);
                _HomeThread.Start();
            }
        }

        private void RunHomeThread()
        {
            try
            {
                List<MarketStickerCategoryModel> ctgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                List<MarketStickerCategoryDTO> ctgDTOList = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.ToList();
                if (ctgDTOList.Count > 0)
                {
                    foreach (int prlCtgId in StickerDictionaries.Instance.MARKET_STICKER_POPULAR_LIST)
                    {
                        MarketStickerCategoryDTO ctgDTO = ctgDTOList.Where(P => P.sCtId == prlCtgId).FirstOrDefault();
                        if (ctgDTO != null)
                        {
                            MarketStickerCategoryModel model = ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                            if (model == null)
                            {
                                model = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }
                            if (StickerPopularCategoryList.Where(P => P.StickerCategoryID == model.StickerCategoryID).FirstOrDefault() == null)
                            {
                                StickerPopularCategoryList.InvokeAdd(model);
                                if (StickerPopularCategoryList.Count >= 2)
                                {
                                    break;
                                }
                                System.Threading.Thread.Sleep(10);
                            }
                        }
                    }

                    foreach (int newCtgId in StickerDictionaries.Instance.MARKET_STICKER_NEW_LIST)
                    {
                        MarketStickerCategoryDTO ctgDTO = ctgDTOList.Where(P => P.sCtId == newCtgId).FirstOrDefault();
                        if (ctgDTO != null)
                        {
                            MarketStickerCategoryModel model = ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                            if (model == null)
                            {
                                model = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }
                            if (StickerNewCategoryList.Where(P => P.StickerCategoryID == model.StickerCategoryID).FirstOrDefault() == null)
                            {
                                StickerNewCategoryList.InvokeAdd(model);
                                if (StickerNewCategoryList.Count >= 2)
                                {
                                    break;
                                }
                                System.Threading.Thread.Sleep(10);
                            }
                        }
                    }

                    foreach (int allCtgId in StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST)
                    {
                        MarketStickerCategoryDTO ctgDTO = ctgDTOList.Where(P => P.sCtId == allCtgId).FirstOrDefault();
                        if (ctgDTO != null)
                        {
                            MarketStickerCategoryModel model = ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                            if (model == null)
                            {
                                model = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }
                            if (StickerAllCategoryList.Where(P => P.StickerCategoryID == model.StickerCategoryID).FirstOrDefault() == null)
                            {
                                StickerAllCategoryList.InvokeAdd(model);
                                if (StickerAllCategoryList.Count >= 4)
                                {
                                    break;
                                }
                                System.Threading.Thread.Sleep(10);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
            }
            finally
            {
                _StickerMarketHomeLoaded = true;
            }
        }

        private void OnStickerDetailsCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)param;
            UCMenuStickerWrapper.Instance.ShowStickerDownloadView();
            UCMenuStickerWrapper.Instance._UCStickerMarketDownloadView.LoadSelectedSticker(ctgModel, false);
        }

        #endregion

        #region Onproperty

        public ObservableCollection<MarketStickerCategoryModel> StickerPopularCategoryList
        {
            get
            {
                return _StickerPopularCategoryList;
            }
            set
            {
                _StickerPopularCategoryList = value;
                this.OnPropertyChanged("StickerPopularCategoryList");
            }
        }

        public ObservableCollection<MarketStickerCategoryModel> StickerNewCategoryList
        {
            get
            {
                return _StickerNewCategoryList;
            }
            set
            {
                _StickerNewCategoryList = value;
                this.OnPropertyChanged("StickerNewCategoryList");
            }
        }

        public ObservableCollection<MarketStickerCategoryModel> StickerAllCategoryList
        {
            get
            {
                return _StickerAllCategoryList;
            }
            set
            {
                _StickerAllCategoryList = value;
                this.OnPropertyChanged("StickerAllCategoryList");
            }
        }

        private int _SelectedCategory;
        public int SelectedCategory
        {
            get { return _SelectedCategory; }
            set
            {
                if (_SelectedCategory == value) return;
                _SelectedCategory = value;
                OnPropertyChanged("SelectedCategory");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Command

        #endregion

        #region EventHandler

        private void UCStickerMarketHome_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            BtnNewSeeAll.Click -= BtnNewSeeAll_Click;
            BtnPopularSeeMore.Click -= BtnPopularSeeMore_Click;
            BtnAllSeeMore.Click -= BtnAllSeeMore_Click;
        }

        private void UCStickerMarketHome_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            BtnNewSeeAll.Click += BtnNewSeeAll_Click;
            BtnPopularSeeMore.Click += BtnPopularSeeMore_Click;
            BtnAllSeeMore.Click += BtnAllSeeMore_Click;
        }

        private void BtnNewSeeAll_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            UCMenuStickerWrapper.Instance.ShowStickerDetailsView();
            UCMenuStickerWrapper.Instance._UCStickerDetailsViewPanel.LoadShowMoreData(Type_New);
        }

        private void BtnAllSeeMore_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            UCMenuStickerWrapper.Instance.ShowStickerDetailsView();
            UCMenuStickerWrapper.Instance._UCStickerDetailsViewPanel.LoadShowMoreData(Type_All);
        }

        private void BtnPopularSeeMore_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            UCMenuStickerWrapper.Instance.ShowStickerDetailsView();
            UCMenuStickerWrapper.Instance._UCStickerDetailsViewPanel.LoadShowMoreData(Type_Popular);
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Grid).Effect = (DropShadowEffect)this.Resources["MouseEnteredShadow"];
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Grid).Effect = (DropShadowEffect)this.Resources["DefaultShadow"];
        }
        
        #endregion

        #region Command

        public ICommand StickerDetailsCommand
        {
            get
            {
                if (_StickerDetailsCommand == null)
                {
                    _StickerDetailsCommand = new RelayCommand(param => OnStickerDetailsCommand(param));
                }
                return _StickerDetailsCommand;
            }
        }

        #endregion
    }
}
=======
﻿using Auth.Service.RingMarket;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Effects;
using View.BindingModels;
using View.Constants;
using View.UI.RingMarket.MenuMarketSticker;
using View.Utility;
using View.Utility.RingMarket;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for UCStickerMarketHome.xaml
    /// </summary>
    public partial class UCStickerMarketHome : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStickerMarketHome).Name);

        private const int Type_Popular = 5;
        private const int Type_New = 3;
        private const int Type_All = 2;
        private const int Type_Free = 1;

        public bool _StickerMarketHomeLoaded = false;
        public bool _StickerMarketContinentLoaded = false;
        public bool _AllCollectionLoaded = false;
        private Thread _HomeThread = null;
        private ICommand _StickerDetailsCommand;
        private ICommand _SeeMoreCommand;

        private ObservableCollection<MarketStickerDynamicCategoryModel> _DynamicStickerList = new ObservableCollection<MarketStickerDynamicCategoryModel>();
        private ObservableCollection<MarketStickerCategoryModel> _StickerAllCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        private ObservableCollection<MarketStickerCategoryModel> _StickerPopularCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        private ObservableCollection<MarketStickerCategoryModel> _StickerNewCategoryList = new ObservableCollection<MarketStickerCategoryModel>();

        public UCStickerMarketHome()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #region Utility

        public void LoadHomeCategory()
        {
            if (_StickerMarketHomeLoaded == false && !(_HomeThread != null && _HomeThread.IsAlive))
            {
                _HomeThread = new System.Threading.Thread(RunHomeThread);
                _HomeThread.Start();
            }
        }

        public void RunHomeThread()
        {
            try
            {
                List<MarketStickerCategoryModel> ctgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                foreach (MarketStickerDynamicCategoryDTO dynamicCtgDTO in StickerDictionaries.Instance.MARKET_STICKER_DYNAMIC_CATEGORY_DICTIONARY)
                {
                    MarketStickerDynamicCategoryModel dynamicCtgModel = new MarketStickerDynamicCategoryModel(dynamicCtgDTO);
                    foreach (int ctgId in dynamicCtgDTO.catIds)
                    {
                        MarketStickerCategoryDTO ctgDTO = null;
                        if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(ctgId, out ctgDTO))
                        {
                            MarketStickerCategoryModel model = ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                            if (model == null)
                            {
                                model = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }
                            if (dynamicCtgModel.DynamicCategoryList.Where(P => P.StickerCategoryID == model.StickerCategoryID).FirstOrDefault() == null)
                            {
                                dynamicCtgModel.DynamicCategoryList.Add(model);
                                if ((dynamicCtgModel.DynamicCategoryShape == StickerMarketConstants.DynamicStickerShapeRectangle && dynamicCtgModel.DynamicCategoryList.Count >= 2)
                                    || (dynamicCtgModel.DynamicCategoryShape == StickerMarketConstants.DynamicStickerShapeSquare && dynamicCtgModel.DynamicCategoryList.Count >= 4))
                                {
                                    break;
                                }
                                System.Threading.Thread.Sleep(10);
                            }
                        }
                    }
                    if (DynamicStickerList.Where(P => P.DynamicCategoryId == dynamicCtgModel.DynamicCategoryId).FirstOrDefault() == null)
                    {
                        DynamicStickerList.InvokeAdd(dynamicCtgModel);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
            }
            finally
            {
                _StickerMarketHomeLoaded = true;
            }
        }

        private void OnStickerDetailsCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)param;
            UCMenuStickerWrapper.Instance.ShowStickerDownloadView();
            UCMenuStickerWrapper.Instance._UCStickerMarketDownloadView.LoadSelectedSticker(ctgModel, false);
        }

        private void OnSeeMoreCommand(object param)
        {
            int dynamicCtgId = Int32.Parse(param.ToString());
            UCMenuStickerWrapper.Instance.ShowStickerDetailsView();
            UCMenuStickerWrapper.Instance._UCStickerDetailsViewPanel.LoadShowMoreData(StickerMarketConstants.STICKER_TYPE_DYNAMIC, 0, "", dynamicCtgId);
        }

        #endregion

        #region Onproperty

        public ObservableCollection<MarketStickerDynamicCategoryModel> DynamicStickerList
        {
            get
            {
                return _DynamicStickerList;
            }
            set
            {
                _DynamicStickerList = value;
                this.OnPropertyChanged("DynamicStickerList");
            }
        }

        public ObservableCollection<MarketStickerCategoryModel> StickerPopularCategoryList
        {
            get
            {
                return _StickerPopularCategoryList;
            }
            set
            {
                _StickerPopularCategoryList = value;
                this.OnPropertyChanged("StickerPopularCategoryList");
            }
        }

        public ObservableCollection<MarketStickerCategoryModel> StickerNewCategoryList
        {
            get
            {
                return _StickerNewCategoryList;
            }
            set
            {
                _StickerNewCategoryList = value;
                this.OnPropertyChanged("StickerNewCategoryList");
            }
        }

        public ObservableCollection<MarketStickerCategoryModel> StickerAllCategoryList
        {
            get
            {
                return _StickerAllCategoryList;
            }
            set
            {
                _StickerAllCategoryList = value;
                this.OnPropertyChanged("StickerAllCategoryList");
            }
        }

        private int _SelectedCategory;
        public int SelectedCategory
        {
            get { return _SelectedCategory; }
            set
            {
                if (_SelectedCategory == value) return;
                _SelectedCategory = value;
                OnPropertyChanged("SelectedCategory");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region EventHandler

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            (sender as Grid).Effect = (DropShadowEffect)this.Resources["MouseEnteredShadow"];
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            (sender as Grid).Effect = (DropShadowEffect)this.Resources["DefaultShadow"];
        }

        #endregion

        #region Command

        public ICommand StickerDetailsCommand
        {
            get
            {
                if (_StickerDetailsCommand == null)
                {
                    _StickerDetailsCommand = new RelayCommand(param => OnStickerDetailsCommand(param));
                }
                return _StickerDetailsCommand;
            }
        }

        public ICommand SeeMoreCommand
        {
            get
            {
                if (_SeeMoreCommand == null)
                {
                    _SeeMoreCommand = new RelayCommand(param => OnSeeMoreCommand(param));
                }
                return _SeeMoreCommand;
            }
        }

        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

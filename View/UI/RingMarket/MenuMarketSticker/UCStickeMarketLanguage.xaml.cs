﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.RingMarket.MenuMarketSticker;
using View.ViewModel;
using View.Utility;
using System.Threading;
using Models.Stores;
using Auth.Service.RingMarket;
using View.Constants;
using View.Utility.RingMarket;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for UCStickeMarketLanguage.xaml
    /// </summary>
    public partial class UCStickeMarketLanguage : UserControl, INotifyPropertyChanged
    {
        public bool _StickerMarketLanguageLoaded = false;
        private bool _IsInitialize = false;
        private const double _SingleItemsMaxHeight = 170;
        private bool BOTTOM_LOADING = false;

        private int _Type_Language = 21;
        int _ItemsNeedToLoad;
        private Thread _LanguageThread = null;
        private ScrollViewer _myScroll = null;
        private Thread ScrollThread = null;
        private ICommand _StickerDetailsCommand;

        public UCStickeMarketLanguage(ScrollViewer _ParentScroll)
        {
            InitializeComponent();
            this.DataContext = this;
            this.InitData();
            this._myScroll = _ParentScroll;
            this._myScroll.ScrollChanged += _myScroll_ScrollChanged;

        }

        #region Utility

        public void InitData()
        {
            var screen = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
            double screenHeight = screen.Height;
            _ItemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - LanguageModelList.Count;
        }

        public void LoadLanguageWithCategory()
        {
            if (_StickerMarketLanguageLoaded == false && !(_LanguageThread != null && _LanguageThread.IsAlive))
            {
                _LanguageThread = new System.Threading.Thread(RunLanguageThread);
                _LanguageThread.Start();
            }
        }

        private void RunLanguageThread()
        {
            List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

            foreach (MarketStickerLanguageyDTO langDTO in StickerDictionaries.Instance.STICKER_COUNTRYLIST)
            {
                if (LanguageModelList.Where(P => P.CountryName == langDTO.name).FirstOrDefault() == null)
                {
                    MarketStickerLanguageModel langModel = new MarketStickerLanguageModel(langDTO);
                    LanguageModelList.InvokeAdd(langModel);
                    System.Threading.Thread.Sleep(10);
                    foreach (int categoryId in langDTO.catIds)
                    {
                        MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == categoryId).FirstOrDefault();
                        if (ctgModel == null)
                        {
                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == categoryId).FirstOrDefault();
                            if (ctgDTO != null)
                            {
                                ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }
                        }
                        if (ctgModel != null)
                        {
                            if (langModel.CategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                            {
                                langModel.CategoryList.InvokeAdd(ctgModel);
                                System.Threading.Thread.Sleep(5);
                            }
                        }
                    }
                }
            }

            this.LoadMoreInitDataIfNeeds();
            _StickerMarketLanguageLoaded = true;
        }

        public void LoadMoreInitDataIfNeeds()
        {
            try
            {
                if (_IsInitialize == false && LanguageModelList.Count < _ItemsNeedToLoad)
                {
                    int collectionLimit = _ItemsNeedToLoad;
                    int categoryLimit = 4, stickerType = StickerMarketConstants.STICKER_TYPE_LANGUAGE;
                    this.GetMoreLanguages(LanguageModelList.Count, categoryLimit, collectionLimit, stickerType);
                    _IsInitialize = true;
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketCollection).Name).Error("Error: LoadInitDataIfNeeds() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void GetMoreLanguages(int startIndex, int categoryLimit, int languageLimit, int stickerType)
        {
            MarketStickerDTO marketStickerDTO = RingMarketStickerService.Instance.GetMoreStickerLanguages(startIndex, categoryLimit, languageLimit, stickerType);

            if (marketStickerDTO != null && marketStickerDTO.sucs == true)
            {
                if (marketStickerDTO.langLst != null && marketStickerDTO.langLst.Count > 0)
                {
                    List<MarketStickerCategoryModel> modelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

                    foreach (MarketStickerLanguageyDTO langDTO in marketStickerDTO.langLst)
                    {
                        MarketStickerLanguageModel langModel = LanguageModelList.Where(P => P.CountryName == langDTO.name).FirstOrDefault();
                        if (langModel == null)
                        {
                            langModel = new MarketStickerLanguageModel(langDTO);
                            LanguageModelList.InvokeAdd(langModel);
                        }

                        if (langDTO.catIds != null && langDTO.catIds.Count > 0)
                        {
                            foreach (int categoryId in langDTO.catIds)
                            {
                                MarketStickerCategoryDTO ctgDTO = marketStickerDTO.catList.Where(P => P.sCtId == categoryId).FirstOrDefault();
                                if (ctgDTO != null)
                                {
                                    MarketStickerCategoryModel ctgModel = modelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                                    if (ctgModel == null)
                                    {
                                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                    }

                                    if (langModel.CategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                                    {
                                        langModel.CategoryList.InvokeAdd(ctgModel);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.BOTTOM_LOADING = false;
        }

        private void LoadLanguageWithinScroll()
        {
            int startIndex = LanguageModelList.Count, categoryLimit = 4, languageLimit = 5, stickerType = StickerMarketConstants.STICKER_TYPE_LANGUAGE;
            this.GetMoreLanguages(startIndex, categoryLimit, languageLimit, stickerType);
            this.BOTTOM_LOADING = false;
        }

        private void OnStickerDetailsCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)param;
            UCMenuStickerWrapper.Instance.ShowStickerDownloadView();
            UCMenuStickerWrapper.Instance._UCStickerMarketDownloadView.LoadSelectedSticker(ctgModel, false);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Property

        private ObservableCollection<MarketStickerLanguageModel> _LanguageModelList = new ObservableCollection<MarketStickerLanguageModel>();
        public ObservableCollection<MarketStickerLanguageModel> LanguageModelList
        {
            get { return _LanguageModelList; }
            set
            {
                if (value == _LanguageModelList)
                    return;

                _LanguageModelList = value;
                this.OnPropertyChanged("LanguageModelList");
            }
        }

        #endregion

        #region EventHandler

        private void BtnTopSeeAll_Click(object sender, RoutedEventArgs e)
        {
            MarketStickerLanguageModel model = (MarketStickerLanguageModel)((Button)sender).DataContext;
            UCMenuStickerWrapper.Instance.ShowStickerDetailsView();
            UCMenuStickerWrapper.Instance._UCStickerDetailsViewPanel.LoadShowMoreData(_Type_Language, 0, model.CountryName);
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)((Grid)sender).DataContext;
            UCMenuStickerWrapper.Instance.ShowStickerDownloadView();
            UCMenuStickerWrapper.Instance._UCStickerMarketDownloadView.LoadSelectedSticker(ctgModel, false);
        }

        private void _myScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (this._IsInitialize && this.IsVisible)
            {
                var scrollViewer = (ScrollViewer)sender;

                if (e.VerticalChange == 0 && scrollViewer.ScrollableHeight > 50 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    scrollViewer.ScrollToVerticalOffset(scrollViewer.ScrollableHeight - 50);
                }

                else if (e.VerticalChange > 0 && scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight && BOTTOM_LOADING == false)
                {
                    this.BOTTOM_LOADING = true;
                    if (!(ScrollThread != null && ScrollThread.IsAlive))
                    {
                        ScrollThread = new Thread(new ThreadStart(LoadLanguageWithinScroll));
                        ScrollThread.Start();
                    }
                }
            }
        }

        #endregion

        #region Command

        public ICommand StickerDetailsCommand
        {
            get
            {
                if (_StickerDetailsCommand == null)
                {
                    _StickerDetailsCommand = new RelayCommand(param => OnStickerDetailsCommand(param));
                }
                return _StickerDetailsCommand;
            }
        }

        #endregion
    }
}

﻿using System;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using View.Utility;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for UCStickerMarketInitPanel.xaml
    /// </summary>
    public partial class UCStickerMarketInitPanel : UserControl, INotifyPropertyChanged
    {
        public static UCStickerMarketHome ViewStickerHome = null;
        public static UCStickerMarketCollection ViewStickerCollection = null;
        public static UCStickeMarketLanguage ViewStickerLanguage = null;

        public UCStickerMarketInitPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCStickerMarketInitPanel_Loaded;
            SwitchToTab(_TabType);
        }

        private void UCStickerMarketInitPanel_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            myScrlViewer.ScrollToVerticalOffset(0);
        }

        #region EventHandler

        private void SwitchToTab(int SelectedIndex)
        {
            switch (TabType)
            {
                case 0:
                    if (ViewStickerHome == null)
                    {
                        ViewStickerHome = new UCStickerMarketHome();
                    }

                    BdrStickerMarket.Child = ViewStickerHome;
                    ViewStickerHome.LoadHomeCategory();
                    break;

                case 1:
                    if (ViewStickerCollection == null)
                    {
                        ViewStickerCollection = new UCStickerMarketCollection(myScrlViewer);
                    }

                    BdrStickerMarket.Child = ViewStickerCollection;
                    ViewStickerCollection.LoadCollectionWithCategory();
                    break;

                case 2:
                    if (ViewStickerLanguage == null)
                    {
                        ViewStickerLanguage = new UCStickeMarketLanguage(myScrlViewer);
                    }

                    BdrStickerMarket.Child = ViewStickerLanguage;
                    ViewStickerLanguage.LoadLanguageWithCategory();
                    break;
            }
        }

        #endregion


        private int _TabType = 0;
        public int TabType
        {
            get { return _TabType; }
            set
            {
                _TabType = value;
                OnPropertyChanged("TabType");
            }
        }

        #region "PropertyChanged"

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region "ICommand"
        private ICommand _BtnTabCommand;
        public ICommand BtnTabCommand
        {
            get
            {
                if (_BtnTabCommand == null)
                {
                    _BtnTabCommand = new RelayCommand(param => OnBtnTabCommandClicked(param));
                }
                return _BtnTabCommand;
            }
        }

        public void OnBtnTabCommandClicked(object parameter)
        {
            TabType = Convert.ToInt32(parameter);
            SwitchToTab(_TabType);
        }

        #endregion "ICommand"
    }
}

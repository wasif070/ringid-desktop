<<<<<<< HEAD
﻿using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Animation;
using View.BindingModels;
using View.ViewModel;
using View.Utility;
using Auth.Service.RingMarket;
using View.Constants;
using Models.Entity;
using View.UI.RingMarket.MenuMarketSticker;
using View.Utility.RingMarket;
using View.Utility.WPFMessageBox;
using Models.DAO;
using Models.Constants;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for UCMarketStickerDetailsViewPanel.xaml
    /// </summary>
    public partial class UCMarketStickerDetailsViewPanel : UserControl, INotifyPropertyChanged
    {
        private VirtualizingStackPanel _ImageItemContainer = null;
        private Thread ScrollThread = null;

        private string _CollectionOrLanguageName;
        private bool _IS_LOADING = false;
        private bool _IsFirstTimePrev = true;
        private bool _IsFirstTimeNext = true;

        private int _CurrentLeftMargin = 0;
        private int _NextPrevCount = 0;
        private int _TotalBannerCount = 0;
        private int _NoOfSwappingCount = 0;
        private int _CollectionId = 0;
        private const double _SingleItemsMaxWidth = 630;
        private const double _SingleItemsMaxHeight = 100;

        public int StickerType;
        public System.Timers.Timer _Timer;

        public UCMarketStickerDetailsViewPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCStickerDetailsViewPanel_Loaded;
            this.Unloaded += UCStickerDetailsViewPanel_Unloaded;
        }

        #region Utility

        public void LoadShowMoreData(int stickerType, int collectionId = 0, string collectionOrLanguageName = "")
        {
            try
            {
                this.StickerType = stickerType;
                this._CollectionId = collectionId;
                this._CollectionOrLanguageName = collectionOrLanguageName;

                this.SetInitData();
                this.FetchMoreCategoriesOfSameType((status) =>
                {
                    if (MarketStickerSlidingBannerList.Count > 1)
                    {
                        SetNextPrevoiusButtonVisibility();
                        StartBannerImageSliding();
                    }

                    return 0;
                });
            }

            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: LoadShowMoreData() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetInitData()
        {
            myScrlViewer.ScrollToVerticalOffset(0);
            _MarketStickerDetailList.Clear();
            _MarketStickerSlidingBannerList.Clear();
            ImagesListBox.ItemsSource = MarketStickerSlidingBannerList;
            IsLeftArrowVisible = Visibility.Hidden;
            IsRightArrowVisible = Visibility.Hidden;
            _MarketStickerDetailList.CollectionChanged += _MarketStickerDetailList_CollectionChanged;
        }

        private void FetchMoreCategoriesOfSameType(Func<bool, int> Oncomplete)
        {
            new Thread(() =>
            {
                List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                var screen = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                double screenWidth = screen.Width;
                double screenHeight = screen.Height;
                int itemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - MarketStickerDetailList.Count;

                if (StickerType == StickerMarketConstants.STICKER_TYPE_ALL || StickerType == StickerMarketConstants.STICKER_TYPE_NEW || StickerType == StickerMarketConstants.STICKER_TYPE_POPULAR)
                {
                    this._TotalBannerCount = StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[StickerType].Count;

                    if (StickerType == StickerMarketConstants.STICKER_TYPE_ALL)
                    {
                        StickerName = StickerMarketConstants.ALL_CATEGORY;
                        this.LoadAllCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), itemsNeedToLoad);
                    }

                    else if (StickerType == StickerMarketConstants.STICKER_TYPE_NEW)
                    {
                        StickerName = StickerMarketConstants.NEW_CATEGORY;
                        this.LoadNewCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), itemsNeedToLoad);
                    }

                    else if (StickerType == StickerMarketConstants.STICKER_TYPE_POPULAR)
                    {
                        StickerName = StickerMarketConstants.POPULAR_CATEGORY;
                        this.LoadPopularCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), itemsNeedToLoad);
                    }

                    int StartIndex = MarketStickerDetailList.Count;
                    int ItemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - StartIndex;
                    if (ItemsNeedToLoad > 0)
                    {
                        int CategoryLimit = (StartIndex + ItemsNeedToLoad);
                        int CollectionLimit = 0;
                        this.LoadMoreAllNewOrPopuplarCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), StartIndex, CategoryLimit, CollectionLimit, StickerType);
                    }
                }

                else if (StickerType == StickerMarketConstants.STICKER_CATEGORIES_IN_A_COLLECTION)
                {
                    if (this._CollectionId > 0)
                    {
                        this.StickerName = _CollectionOrLanguageName;
                        this.LoadAllCategoriesInACollection(RingIDViewModel.Instance.MarketStickerCategoryList.ToList());

                        int StartIndex = MarketStickerDetailList.Count;
                        int ItemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - StartIndex;
                        if (ItemsNeedToLoad > 0)
                        {
                            int CategoryLimit = (StartIndex + ItemsNeedToLoad);
                            this.LoadMoreCategoriesInACollection(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), StartIndex, CategoryLimit, StickerType, _CollectionId);
                        }
                    }
                }

                else if (StickerType == StickerMarketConstants.STICKER_CATEGORIES_IN_A_LANGUAGE)
                {
                    this.StickerName = _CollectionOrLanguageName;
                    this.LoadAllCategoriesInALanguage(tempCtgModelList);

                    int StartIndex = MarketStickerDetailList.Count;
                    int ItemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - StartIndex;
                    if (ItemsNeedToLoad > 0)
                    {
                        int CategoryLimit = (StartIndex + ItemsNeedToLoad);
                        this.LoadMoreCategoriesInALanguage(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), StartIndex, CategoryLimit, StickerType, _CollectionOrLanguageName);
                    }
                }

                Oncomplete(true);
            }).Start();
        }

        private void LoadPopularCategories(List<MarketStickerCategoryModel> tempCtgModelList, int limit)
        {
            List<MarketStickerCategoryDTO> categoryDtos = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => StickerDictionaries.Instance.MARKET_STICKER_POPULAR_LIST.Contains(P.sCtId)).OrderBy(P => P.Rnk).ToList();
            if (categoryDtos != null && categoryDtos.Count > 0)
            {
                foreach (MarketStickerCategoryDTO ctgDTO in categoryDtos)
                {
                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                    }

                    if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                    {
                        MarketStickerDetailList.InvokeAdd(ctgModel);
                        if (MarketStickerDetailList.Count >= limit)
                        {
                            break;
                        }
                        Thread.Sleep(10);
                    }
                }
            }
        }

        private void LoadNewCategories(List<MarketStickerCategoryModel> tempCtgModelList, int limit)
        {
            List<MarketStickerCategoryDTO> categoryDtos = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => StickerDictionaries.Instance.MARKET_STICKER_NEW_LIST.Contains(P.sCtId)).OrderBy(P => P.Rnk).ToList();

            if (categoryDtos != null && categoryDtos.Count > 0)
            {
                foreach (MarketStickerCategoryDTO ctgDTO in categoryDtos)
                {
                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                    }

                    if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                    {
                        MarketStickerDetailList.InvokeAdd(ctgModel);
                        if (MarketStickerDetailList.Count >= limit)
                        {
                            break;
                        }
                        Thread.Sleep(10);
                    }
                }
            }
        }

        private void LoadAllCategories(List<MarketStickerCategoryModel> tempCtgModelList, int limit)
        {
            List<MarketStickerCategoryDTO> categoryDtos = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST.Contains(P.sCtId)).OrderBy(P => P.Rnk).ToList();
            if (categoryDtos != null && categoryDtos.Count > 0)
            {
                foreach (MarketStickerCategoryDTO ctgDTO in categoryDtos)
                {
                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                    }

                    if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                    {
                        MarketStickerDetailList.InvokeAdd(ctgModel);
                        if (MarketStickerDetailList.Count >= limit)
                        {
                            break;
                        }
                        Thread.Sleep(10);
                    }
                }
            }
        }

        private void LoadMoreAllNewOrPopuplarCategories(List<MarketStickerCategoryModel> tempCtgModelList, int StartIndex, int CategoryLimit, int CollectionLimit, int StickerType)
        {
            List<MarketStickerCategoryDTO> categoryDTOs = RingMarketStickerService.Instance.GetMoreStickerCategoriesOfSameType(StartIndex, CategoryLimit, CollectionLimit, StickerType);
            if (categoryDTOs != null && categoryDTOs.Count > 0)
            {
                int limit = MarketStickerDetailList.Count + CategoryLimit;
                categoryDTOs = categoryDTOs.OrderBy(P => P.Rnk).ToList();
                foreach (MarketStickerCategoryDTO ctgDTO in categoryDTOs)
                {
                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                    }

                    if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                    {
                        MarketStickerDetailList.InvokeAdd(ctgModel);
                        if (MarketStickerDetailList.Count >= limit)
                        {
                            break;
                        }
                        Thread.Sleep(10);
                    }
                }
            }
        }

        private void LoadAllCategoriesInACollection(List<MarketStickerCategoryModel> tempCtgModelList)
        {
            MarketStickerCollectionDTO collectionDTO = StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY.Values.Where(P => P.sClId == this._CollectionId).FirstOrDefault();

            List<MarketStickerCategoryDTO> categoryDtos = new List<MarketStickerCategoryDTO>();
            if (collectionDTO != null)
            {
                foreach (int ctgId in collectionDTO.catIds)
                {
                    MarketStickerCategoryDTO dto = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgId).FirstOrDefault();
                    if (dto != null)
                    {
                        categoryDtos.Add(dto);
                    }
                }

                if (categoryDtos.Count > 0)
                {
                    foreach (MarketStickerCategoryDTO ctgDTO in categoryDtos)
                    {
                        MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                        if (ctgModel == null)
                        {
                            ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                        }

                        if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                        {
                            MarketStickerDetailList.InvokeAdd(ctgModel);
                            Thread.Sleep(10);
                        }
                    }
                }
            }
        }

        private void LoadMoreCategoriesInACollection(List<MarketStickerCategoryModel> tempCtgModelList, int StartIndex, int CategoryLimit, int Sticker_Type, int CollectionId)
        {
            List<MarketStickerCategoryDTO> categoriesDTOs = RingMarketStickerService.Instance.GetMoreStickerCategoriesInACollection(StartIndex, CategoryLimit, Sticker_Type, CollectionId);
            if (categoriesDTOs != null && categoriesDTOs.Count > 0)
            {
                foreach (MarketStickerCategoryDTO ctgDTO in categoriesDTOs)
                {
                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO); ;
                    }

                    if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                    {
                        MarketStickerDetailList.InvokeAdd(ctgModel);
                        Thread.Sleep(10);
                    }
                }
            }
        }

        private void LoadAllCategoriesInALanguage(List<MarketStickerCategoryModel> tempCtgModelList)
        {
            MarketStickerLanguageyDTO langDTO = StickerDictionaries.Instance.STICKER_COUNTRYLIST.Where(P => P.name == this._CollectionOrLanguageName).FirstOrDefault();
            List<MarketStickerCategoryDTO> categoryDtos = new List<MarketStickerCategoryDTO>();
            if (langDTO != null)
            {
                categoryDtos = new List<MarketStickerCategoryDTO>();
                foreach (int ctgId in langDTO.catIds)
                {
                    MarketStickerCategoryDTO dto = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgId).FirstOrDefault();
                    if (dto != null)
                    {
                        categoryDtos.Add(dto);
                    }
                }

                if (categoryDtos.Count > 0)
                {
                    foreach (MarketStickerCategoryDTO ctgDTO in categoryDtos)
                    {
                        MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                        if (ctgModel == null)
                        {
                            ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                        }

                        if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                        {
                            MarketStickerDetailList.InvokeAdd(ctgModel);
                            Thread.Sleep(10);
                        }
                    }
                }
            }
        }

        private void LoadMoreCategoriesInALanguage(List<MarketStickerCategoryModel> tempCtgModelList, int StartIndex, int CategoryLimit, int Sticker_Type, string LanguageName)
        {
            List<MarketStickerCategoryDTO> categoriesDTOs = RingMarketStickerService.Instance.GetMoreStickerCategoriesInALanguage(StartIndex, CategoryLimit, Sticker_Type, LanguageName);
            if (categoriesDTOs != null && categoriesDTOs.Count > 0)
            {
                foreach (MarketStickerCategoryDTO ctgDTO in categoriesDTOs)
                {
                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                    }

                    if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                    {
                        MarketStickerDetailList.InvokeAdd(ctgModel);
                        Thread.Sleep(10);
                    }
                }
            }
        }

        public void StartBannerImageSliding()
        {
            _Timer = new System.Timers.Timer();
            _Timer.Interval = 2500;
            _Timer.Elapsed += timer_Elapsed;
            _Timer.Start();
        }

        public void CallBackFromDownloadView()
        {
            ImagesListBox.ItemsSource = _MarketStickerSlidingBannerList;
            if (MarketStickerSlidingBannerList.Count > 1)
            {
                StartBannerImageSliding();
            }
        }

        private void OnStickerDownloadCommand(object parameter)
        {
            if (parameter != null && parameter is MarketStickerCategoryModel)
            {
                MarketStickerCategoryModel marketCtgModel = (MarketStickerCategoryModel)parameter;
                if (marketCtgModel != null && marketCtgModel.IsDownloadRunning == false)
                {
                    if (marketCtgModel.Downloaded == false) // do download
                    {
                        marketCtgModel.DownloadPercentage = 0;
                        marketCtgModel.IsDownloadRunning = true;

                        MarketStickerCategoryDTO ctgDTO;
                        if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgModel.StickerCategoryID, out ctgDTO))
                        {
                            RingMarketStickerLoadUtility.DownLoadStickerImages(ctgDTO, (isNointernet) =>
                            {
                                if (isNointernet)
                                {
                                    marketCtgModel.DownloadPercentage = 0;
                                    marketCtgModel.IsDownloadRunning = false;
                                    UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                                }
                                return 0;
                            });
                        }
                    }
                    else   // Remove Sticker
                    {
                        if (marketCtgModel.IsDefault)
                        {
                            UIHelperMethods.ShowWarning("You can't remove default sticker!", "Remove sticker");
                            return;
                        }

                        MarketStickerCategoryDTO ctgDTO = null;
                        if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgModel.StickerCategoryID, out ctgDTO))
                        {
                            ctgDTO.Downloaded = false;
                            List<MarketStickerCategoryDTO> ctgDTOs = new List<MarketStickerCategoryDTO>();
                            ctgDTOs.Add(ctgDTO);
                            RingMarketStickerLoadUtility.LoadMarketStickerRecentCategorys(ctgDTOs);
                            RingMarkerStickerDAO.UpdateStickerCategoryDownloadStatus(ctgDTO.sCtId, ctgDTO.Downloaded);
                        }

                        marketCtgModel.Downloaded = false;
                        marketCtgModel.IsDownloadRunning = false;
                        marketCtgModel.DownloadPercentage = 0;
                        marketCtgModel.OnPropertyChanged("DetailImage");
                    }
                }
            }
        }

        private void LoadCategoryOrCollectionByScroll()
        {
            if (_CollectionId != 0)
            {
                this.LoadMoreCategoriesInACollection(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), MarketStickerDetailList.Count, 8, StickerType, _CollectionId);
            }
            else if (!string.IsNullOrEmpty(_CollectionOrLanguageName))
            {
                this.LoadMoreCategoriesInALanguage(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), MarketStickerDetailList.Count, 8, StickerType, _CollectionOrLanguageName);
            }
            else
            {
                int limit = MarketStickerDetailList.Count + 8;
                if (StickerType == StickerMarketConstants.STICKER_TYPE_ALL)
                {
                    this.LoadAllCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), limit);
                }

                else if (StickerType == StickerMarketConstants.STICKER_TYPE_NEW)
                {
                    this.LoadNewCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), limit);
                }
                else if (StickerType == StickerMarketConstants.STICKER_TYPE_NEW)
                {
                    LoadPopularCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), limit);
                }
                if (MarketStickerDetailList.Count < limit)
                {
                    this.LoadMoreAllNewOrPopuplarCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), MarketStickerDetailList.Count, 8, 0, StickerType);
                }

            }

            _IS_LOADING = false;
        }

        private void ResetData()
        {
            if (_Timer != null)
            {
                _Timer.Stop();
                _Timer.Enabled = false;
                _Timer = null;
            }

            ImagesListBox.ItemsSource = null;
            AnimationOnArrowClicked(0, 0);

            _NoOfSwappingCount = 0;
            _NextPrevCount = 0;
            _CurrentLeftMargin = 0;
            _IsFirstTimeNext = true;
            _IsFirstTimePrev = true;
            IsLeftArrowVisible = Visibility.Hidden;
            IsRightArrowVisible = Visibility.Hidden;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region EventHandler

        private void UCStickerDetailsViewPanel_Loaded(object sender, RoutedEventArgs e)
        {
            ImagesListBox.ItemsSource = _MarketStickerSlidingBannerList;
            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
        }

        private void UCStickerDetailsViewPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
            this.ResetData();
        }

        private void myScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (_IS_LOADING == false && e.VerticalOffset > 0 && e.VerticalOffset >= (myScrlViewer.ScrollableHeight - 200))
            {
                _IS_LOADING = true;
                if (!(ScrollThread != null && ScrollThread.IsAlive))
                {
                    ScrollThread = new Thread(new ThreadStart(LoadCategoryOrCollectionByScroll));
                    ScrollThread.Start();
                }
            }
        }

        private void OnBackToPreviousUI()
        {
            UCMenuStickerWrapper.Instance.ShowStickerInitView();
        }

        private void OnDetailsViewCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)param;
            UCMenuStickerWrapper.Instance.ShowStickerDownloadView();
            UCMenuStickerWrapper.Instance._UCStickerMarketDownloadView.LoadSelectedSticker(ctgModel, true);
        }

        private void OnPrevious(object param)
        {
            try
            {
                int target = 0;
                _CurrentLeftMargin = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNext(object param)
        {
            try
            {
                //int target = _CurrentLeftMargin - (int)pnlImageContainerWrapper.ActualWidth;
                int target = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(_CurrentLeftMargin, 0, 0, 0);
            MarginAnimation.To = new Thickness(target, 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
            storyboard.Begin(_ImageItemContainer);
        }

        private void RightArrowPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_Timer != null)
                {
                    _Timer.Stop();
                    OnNextCommand();
                    _Timer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LeftArrowPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_Timer != null)
                {
                    _Timer.Stop();
                    OnPreviousCommand();
                    _Timer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    OnNextCommand();
                }
                catch (Exception ex)
                {
                    log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: timer_Elapsed() => " + ex.Message + "\n" + ex.StackTrace);
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void OnNextCommand()
        {
            //Hints: For AutoSlide/Next, Viewport is 0 to -630.
            try
            {
                if (_Timer != null)
                {
                    _Timer.Stop();
                }

                if (_IsFirstTimeNext == false)
                {
                    int firstIndex = 0, middleIndex = 1, lastIndex = 2;
                    MarketStickerSlidingBannerList.InvokeMove(firstIndex, middleIndex);

                    if (MarketStickerSlidingBannerList.Count > 2)
                    {
                        MarketStickerSlidingBannerList.InvokeMove(middleIndex, lastIndex);
                        MarketStickerSlidingBannerList.RemoveAt(lastIndex);
                        if (StickerType == StickerMarketConstants.STICKER_TYPE_ALL || StickerType == StickerMarketConstants.STICKER_TYPE_NEW || StickerType == StickerMarketConstants.STICKER_TYPE_POPULAR)
                        {
                            int nextLoadingIndex = (_NextPrevCount + lastIndex) % _TotalBannerCount;
                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[StickerType].ElementAt(nextLoadingIndex);
                            if (ctgDTO != null)
                            {
                                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                                if (ctgModel == null)
                                {
                                    ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                }
                                MarketStickerSlidingBannerList.InvokeInsert(lastIndex, ctgModel);

                                _NoOfSwappingCount = _NoOfSwappingCount == _TotalBannerCount - 1 ? 0 : _NoOfSwappingCount + 1;
                            }
                        }
                        else
                        {
                            int nextLoadingIndex = (_NextPrevCount + lastIndex) % MarketStickerDetailList.Count;
                            MarketStickerCategoryModel ctgModel = MarketStickerDetailList.ElementAt(nextLoadingIndex);
                            MarketStickerSlidingBannerList.InvokeInsert(lastIndex, ctgModel);

                            _NoOfSwappingCount = _NoOfSwappingCount == MarketStickerDetailList.Count - 1 ? 0 : _NoOfSwappingCount + 1;
                        }
                    }
                }

                _IsFirstTimeNext = false; // firstTime no swapping make, just changing viewport from 0 to -630.
                _IsFirstTimePrev = true;

                _NextPrevCount++;
                OnNext(null);
                SetNextPrevoiusButtonVisibility();

                if (_Timer != null)
                {
                    _Timer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: nextBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPreviousCommand()
        {
            //Hints: For Previous, Viewport is -630 to 0.

            try
            {
                if (_Timer != null)
                {
                    _Timer.Stop();
                }

                int firstIndex = 0, middleIndex = 1, lastIndex = 2;
                if (_IsFirstTimePrev == false)
                {
                    if (MarketStickerSlidingBannerList.Count > 2)
                    {
                        MarketStickerSlidingBannerList.InvokeMove(middleIndex, lastIndex);
                        MarketStickerSlidingBannerList.InvokeMove(firstIndex, middleIndex);
                        MarketStickerSlidingBannerList.RemoveAt(firstIndex);
                        MarketStickerCategoryModel ctgModel = null;
                        if (StickerType == StickerMarketConstants.STICKER_TYPE_ALL || StickerType == StickerMarketConstants.STICKER_TYPE_NEW || StickerType == StickerMarketConstants.STICKER_TYPE_POPULAR)
                        {
                            MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[StickerType].ElementAt(_NoOfSwappingCount);
                            if (ctgDTO != null)
                            {
                                ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                                if (ctgModel == null)
                                {
                                    ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                }
                            }
                            MarketStickerSlidingBannerList.InvokeInsert(firstIndex, ctgModel);
                        }
                        else
                        {
                            ctgModel = MarketStickerDetailList.ElementAt(_NoOfSwappingCount);
                            MarketStickerSlidingBannerList.InvokeInsert(firstIndex, ctgModel);
                            _NoOfSwappingCount = _NoOfSwappingCount == MarketStickerDetailList.Count - 1 ? 0 : _NoOfSwappingCount + 1;
                        }
                    }
                }

                _IsFirstTimeNext = true;
                _IsFirstTimePrev = false;

                OnPrevious(null);
                _NextPrevCount--;

                if (_NoOfSwappingCount > 0)
                {
                    _NoOfSwappingCount = _NoOfSwappingCount - 1;
                }

                SetNextPrevoiusButtonVisibility();

                if (_Timer != null)
                {
                    _Timer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: prevBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetNextPrevoiusButtonVisibility()
        {
            if (StickerType == StickerMarketConstants.STICKER_TYPE_ALL || StickerType == StickerMarketConstants.STICKER_TYPE_NEW || StickerType == StickerMarketConstants.STICKER_TYPE_POPULAR)
            {
                if (_NextPrevCount == _TotalBannerCount)
                {
                    _NextPrevCount = 0;
                }

                IsLeftArrowVisible = _NextPrevCount > 0 ? Visibility.Visible : Visibility.Collapsed;
                IsRightArrowVisible = _NextPrevCount < _TotalBannerCount - 1 ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                if (_NextPrevCount == MarketStickerDetailList.Count)
                {
                    _NextPrevCount = 0;
                }

                IsLeftArrowVisible = _NextPrevCount > 0 ? Visibility.Visible : Visibility.Collapsed;
                IsRightArrowVisible = _NextPrevCount < MarketStickerDetailList.Count - 1 ? Visibility.Visible : Visibility.Collapsed;

            }
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
            eventArg.RoutedEvent = UIElement.MouseWheelEvent;
            eventArg.Source = sender;
            var parent = ((Control)sender).Parent as UIElement;
            parent.RaiseEvent(eventArg);
        }

        private void HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as VirtualizingStackPanel;
        }

        #endregion

        #region Property

        private ObservableCollection<MarketStickerCategoryModel> _MarketStickerDetailList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> MarketStickerDetailList
        {
            get { return _MarketStickerDetailList; }
            set
            {
                if (_MarketStickerDetailList == value)
                {
                    return;
                }
                _MarketStickerDetailList = value;
                OnPropertyChanged("MarketStickerDetailList");
            }
        }

        private void _MarketStickerDetailList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add && MarketStickerSlidingBannerList.Count < 3)
            {
                if (StickerType == StickerMarketConstants.STICKER_TYPE_ALL || StickerType == StickerMarketConstants.STICKER_TYPE_NEW || StickerType == StickerMarketConstants.STICKER_TYPE_POPULAR)
                {
                    List<MarketStickerCategoryDTO> detailList = StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[StickerType];
                    foreach (MarketStickerCategoryDTO ctgDTO in detailList)
                    {
                        if (!MarketStickerSlidingBannerList.Any(P => P.StickerCategoryID == ctgDTO.sCtId))
                        {
                            MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                            if (ctgModel == null)
                            {
                                ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }

                            MarketStickerSlidingBannerList.InvokeAdd(ctgModel);
                            if (MarketStickerSlidingBannerList.Count == 3)
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    List<MarketStickerCategoryModel> detailList = MarketStickerDetailList.ToList();
                    foreach (MarketStickerCategoryModel ctgModel in detailList)
                    {
                        if (!MarketStickerSlidingBannerList.Any(P => P.StickerCategoryID == ctgModel.StickerCategoryID))
                        {
                            MarketStickerSlidingBannerList.InvokeAdd(ctgModel);
                            if (MarketStickerSlidingBannerList.Count == 3)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        private ObservableCollection<MarketStickerCategoryModel> _MarketStickerSlidingBannerList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> MarketStickerSlidingBannerList
        {
            get { return _MarketStickerSlidingBannerList; }
            set
            {
                if (_MarketStickerSlidingBannerList == value)
                {
                    return;
                }

                _MarketStickerSlidingBannerList = value;
                OnPropertyChanged("MarketStickerSlidingBannerList");
            }
        }

        private string _StickerName = string.Empty;
        public string StickerName
        {
            get { return _StickerName; }
            set
            {
                if (_StickerName == value)
                {
                    return;
                }

                _StickerName = value;
                OnPropertyChanged("StickerName");
            }
        }

        private Visibility _IsLeftArrowVisible = Visibility.Visible;
        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                if (value == _IsLeftArrowVisible) { return; }
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        private Visibility _IsRightArrowVisible = Visibility.Visible;
        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                if (value == _IsRightArrowVisible) { return; }
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }

        #endregion

        #region Command

        private ICommand _DownloadCommand;
        public ICommand DownloadCommand
        {
            get
            {
                if (_DownloadCommand == null)
                {
                    _DownloadCommand = new RelayCommand(param => OnStickerDownloadCommand(param));
                }
                return _DownloadCommand;
            }
        }

        private ICommand _DetailsCommand;
        public ICommand DetailsCommand
        {
            get
            {
                if (_DetailsCommand == null)
                {
                    _DetailsCommand = new RelayCommand(param => OnDetailsViewCommand(param));
                }
                return _DetailsCommand;
            }
        }

        private ICommand _BackToPrevoiousUI;
        public ICommand BackToPrevoiousUI
        {
            get
            {
                if (_BackToPrevoiousUI == null)
                {
                    _BackToPrevoiousUI = new RelayCommand(param => OnBackToPreviousUI());
                }
                return _BackToPrevoiousUI;
            }
        }

        private ICommand _NextCommand;
        public ICommand NextCommand
        {
            get
            {
                if (_NextCommand == null)
                {
                    _NextCommand = new RelayCommand(param => OnNextCommand());
                }
                return _NextCommand;
            }
        }

        private ICommand _PrevCommand;
        public ICommand PrevCommand
        {
            get
            {
                if (_PrevCommand == null)
                {
                    _PrevCommand = new RelayCommand(param => OnPreviousCommand());
                }
                return _PrevCommand;
            }
        }

        #endregion
    }
}
=======
﻿using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Animation;
using View.BindingModels;
using View.ViewModel;
using View.Utility;
using Auth.Service.RingMarket;
using View.Constants;
using Models.Entity;
using View.UI.RingMarket.MenuMarketSticker;
using View.Utility.RingMarket;
using View.Utility.WPFMessageBox;
using Models.DAO;
using Models.Constants;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for UCMarketStickerDetailsViewPanel.xaml
    /// </summary>
    public partial class UCMarketStickerDetailsViewPanel : UserControl, INotifyPropertyChanged
    {
        private VirtualizingStackPanel _ImageItemContainer = null;
        private Thread ScrollThread = null;

        private string _CollectionOrLanguageName;
        private bool _IS_LOADING = false;
        private bool _IsFirstTimePrev = true;
        private bool _IsFirstTimeNext = true;

        private int _CurrentLeftMargin = 0;
        private int _NextPrevCount = 0;
        private int _TotalBannerCount = 0;
        private int _NoOfSwappingCount = 0;
        private int _CollectionId = 0;
        private const double _SingleItemsMaxWidth = 630;
        private const double _SingleItemsMaxHeight = 100;
        private int _DynamicCategoryId;
        public int StickerType;
        public System.Timers.Timer _Timer;

        public UCMarketStickerDetailsViewPanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += UCStickerDetailsViewPanel_Loaded;
            this.Unloaded += UCStickerDetailsViewPanel_Unloaded;
        }

        #region Utility

        public void LoadShowMoreData(int stickerType, int collectionId = 0, string collectionOrLanguageName = "", int dynamicCategoryId = 0)
        {
            try
            {
                this.StickerType = stickerType;
                this._CollectionId = collectionId;
                this._CollectionOrLanguageName = collectionOrLanguageName;
                this._DynamicCategoryId = dynamicCategoryId;
                this.SetInitData();
                this.FetchMoreCategoriesOfSameType((status) =>
                {
                    if (MarketStickerSlidingBannerList.Count > 1)
                    {
                        SetNextPrevoiusButtonVisibility();
                        StartBannerImageSliding();
                    }

                    return 0;
                });
            }

            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: LoadShowMoreData() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetInitData()
        {
            myScrlViewer.ScrollToVerticalOffset(0);
            _MarketStickerDetailList.Clear();
            _MarketStickerSlidingBannerList.Clear();
            ImagesListBox.ItemsSource = MarketStickerSlidingBannerList;
            IsLeftArrowVisible = Visibility.Hidden;
            IsRightArrowVisible = Visibility.Hidden;
            _MarketStickerDetailList.CollectionChanged += _MarketStickerDetailList_CollectionChanged;
        }

        private void FetchMoreCategoriesOfSameType(Func<bool, int> Oncomplete)
        {
            new Thread(() =>
            {
                List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                var screen = System.Windows.Forms.Screen.PrimaryScreen.Bounds;
                double screenWidth = screen.Width;
                double screenHeight = screen.Height;
                int itemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - MarketStickerDetailList.Count;

                if (StickerType == StickerMarketConstants.STICKER_TYPE_DYNAMIC)
                {
                    MarketStickerDynamicCategoryDTO dynamicCtgDTO = StickerDictionaries.Instance.MARKET_STICKER_DYNAMIC_CATEGORY_DICTIONARY.Where(P => P.id == _DynamicCategoryId).FirstOrDefault();
                    if (dynamicCtgDTO != null)
                    {
                        StickerName = dynamicCtgDTO.name;
                        LoadDynamicCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), dynamicCtgDTO, itemsNeedToLoad);
                    }
                     List<MarketStickerCategoryDTO> ctaDTOList = StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[_DynamicCategoryId];
                     if (ctaDTOList != null)
                     {
                         this._TotalBannerCount = ctaDTOList.Count;
                     }

                    int StartIndex = MarketStickerDetailList.Count;
                    int ItemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - StartIndex;
                    if (ItemsNeedToLoad > 0)
                    {
                        int CategoryLimit = (StartIndex + ItemsNeedToLoad);
                        int CollectionLimit = 0;
                        this.LoadMoreDynamicCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), StartIndex, CategoryLimit, CollectionLimit, StickerType, _DynamicCategoryId);
                    }
                }

                else if (StickerType == StickerMarketConstants.STICKER_CATEGORIES_IN_A_COLLECTION)
                {
                    if (this._CollectionId > 0)
                    {
                        this.StickerName = _CollectionOrLanguageName;
                        this.LoadAllCategoriesInACollection(RingIDViewModel.Instance.MarketStickerCategoryList.ToList());

                        int StartIndex = MarketStickerDetailList.Count;
                        int ItemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - StartIndex;
                        if (ItemsNeedToLoad > 0)
                        {
                            int CategoryLimit = (StartIndex + ItemsNeedToLoad);
                            this.LoadMoreCategoriesInACollection(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), StartIndex, CategoryLimit, StickerType, _CollectionId);
                        }
                    }
                }

                else if (StickerType == StickerMarketConstants.STICKER_CATEGORIES_IN_A_LANGUAGE)
                {
                    this.StickerName = _CollectionOrLanguageName;
                    this.LoadAllCategoriesInALanguage(tempCtgModelList);

                    int StartIndex = MarketStickerDetailList.Count;
                    int ItemsNeedToLoad = (int)(screenHeight / _SingleItemsMaxHeight) - StartIndex;
                    if (ItemsNeedToLoad > 0)
                    {
                        int CategoryLimit = (StartIndex + ItemsNeedToLoad);
                        this.LoadMoreCategoriesInALanguage(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), StartIndex, CategoryLimit, StickerType, _CollectionOrLanguageName);
                    }
                }

                Oncomplete(true);
            }).Start();
        }

        private void LoadDynamicCategories(List<MarketStickerCategoryModel> tempCtgModelList, MarketStickerDynamicCategoryDTO dynamicCtgDTO, int limit)
        {
            if (dynamicCtgDTO.catIds != null)
            {
                List<MarketStickerCategoryDTO> categoryDtos = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => dynamicCtgDTO.catIds.Contains(P.sCtId)).OrderBy(P => P.Rnk).ToList();
                if (categoryDtos != null)
                {
                    foreach (MarketStickerCategoryDTO ctgDTO in categoryDtos)
                    {
                        MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                        if (ctgModel == null)
                        {
                            ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                        }

                        if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                        {
                            MarketStickerDetailList.InvokeAdd(ctgModel);
                            if (MarketStickerDetailList.Count >= limit)
                            {
                                break;
                            }
                            Thread.Sleep(10);
                        }
                    }
                }
            }
        }

        private void LoadMoreDynamicCategories(List<MarketStickerCategoryModel> tempCtgModelList, int StartIndex, int CategoryLimit, int CollectionLimit, int StickerType, int dynamiCategoryId)
        {
            List<MarketStickerCategoryDTO> categoryDTOs = RingMarketStickerService.Instance.GetMoreDynamicStickerCategories(StartIndex, CategoryLimit, CollectionLimit, StickerType, dynamiCategoryId);
            if (categoryDTOs != null && categoryDTOs.Count > 0)
            {
                int limit = MarketStickerDetailList.Count + CategoryLimit;
                categoryDTOs = categoryDTOs.OrderBy(P => P.Rnk).ToList();
                foreach (MarketStickerCategoryDTO ctgDTO in categoryDTOs)
                {
                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                    }

                    if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                    {
                        MarketStickerDetailList.InvokeAdd(ctgModel);
                        if (MarketStickerDetailList.Count >= limit)
                        {
                            break;
                        }
                        Thread.Sleep(10);
                    }
                }
            }
        }

        private void LoadAllCategoriesInACollection(List<MarketStickerCategoryModel> tempCtgModelList)
        {
            MarketStickerCollectionDTO collectionDTO = StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY.Values.Where(P => P.sClId == this._CollectionId).FirstOrDefault();
            List<MarketStickerCategoryDTO> categoryDtos = new List<MarketStickerCategoryDTO>();
            if (collectionDTO != null)
            {
                foreach (int ctgId in collectionDTO.catIds)
                {
                    MarketStickerCategoryDTO dto = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgId).FirstOrDefault();
                    if (dto != null)
                    {
                        categoryDtos.Add(dto);
                    }
                }

                if (categoryDtos.Count > 0)
                {
                    foreach (MarketStickerCategoryDTO ctgDTO in categoryDtos)
                    {
                        MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                        if (ctgModel == null)
                        {
                            ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                        }

                        if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                        {
                            MarketStickerDetailList.InvokeAdd(ctgModel);
                            Thread.Sleep(10);
                        }
                    }
                }
            }
        }

        private void LoadMoreCategoriesInACollection(List<MarketStickerCategoryModel> tempCtgModelList, int StartIndex, int CategoryLimit, int Sticker_Type, int CollectionId)
        {
            List<MarketStickerCategoryDTO> categoriesDTOs = RingMarketStickerService.Instance.GetMoreStickerCategoriesInACollection(StartIndex, CategoryLimit, Sticker_Type, CollectionId);
            if (categoriesDTOs != null && categoriesDTOs.Count > 0)
            {
                foreach (MarketStickerCategoryDTO ctgDTO in categoriesDTOs)
                {
                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO); ;
                    }

                    if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                    {
                        MarketStickerDetailList.InvokeAdd(ctgModel);
                        Thread.Sleep(10);
                    }
                }
            }
        }

        private void LoadAllCategoriesInALanguage(List<MarketStickerCategoryModel> tempCtgModelList)
        {
            MarketStickerLanguageyDTO langDTO = StickerDictionaries.Instance.STICKER_COUNTRYLIST.Where(P => P.name == this._CollectionOrLanguageName).FirstOrDefault();
            List<MarketStickerCategoryDTO> categoryDtos = new List<MarketStickerCategoryDTO>();
            if (langDTO != null)
            {
                categoryDtos = new List<MarketStickerCategoryDTO>();
                foreach (int ctgId in langDTO.catIds)
                {
                    MarketStickerCategoryDTO dto = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == ctgId).FirstOrDefault();
                    if (dto != null)
                    {
                        categoryDtos.Add(dto);
                    }
                }

                if (categoryDtos.Count > 0)
                {
                    foreach (MarketStickerCategoryDTO ctgDTO in categoryDtos)
                    {
                        MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                        if (ctgModel == null)
                        {
                            ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                        }

                        if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                        {
                            MarketStickerDetailList.InvokeAdd(ctgModel);
                            Thread.Sleep(10);
                        }
                    }
                }
            }
        }

        private void LoadMoreCategoriesInALanguage(List<MarketStickerCategoryModel> tempCtgModelList, int StartIndex, int CategoryLimit, int Sticker_Type, string LanguageName)
        {
            List<MarketStickerCategoryDTO> categoriesDTOs = RingMarketStickerService.Instance.GetMoreStickerCategoriesInALanguage(StartIndex, CategoryLimit, Sticker_Type, LanguageName);
            if (categoriesDTOs != null && categoriesDTOs.Count > 0)
            {
                foreach (MarketStickerCategoryDTO ctgDTO in categoriesDTOs)
                {
                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                    }

                    if (MarketStickerDetailList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                    {
                        MarketStickerDetailList.InvokeAdd(ctgModel);
                        Thread.Sleep(10);
                    }
                }
            }
        }

        public void StartBannerImageSliding()
        {
            _Timer = new System.Timers.Timer();
            _Timer.Interval = 2500;
            _Timer.Elapsed += timer_Elapsed;
            _Timer.Start();
        }

        public void CallBackFromDownloadView()
        {
            ImagesListBox.ItemsSource = _MarketStickerSlidingBannerList;
            if (MarketStickerSlidingBannerList.Count > 1)
            {
                StartBannerImageSliding();
            }
        }

        private void OnStickerDownloadCommand(object parameter)
        {
            if (parameter != null && parameter is MarketStickerCategoryModel)
            {
                MarketStickerCategoryModel marketCtgModel = (MarketStickerCategoryModel)parameter;
                if (marketCtgModel != null && marketCtgModel.IsDownloadRunning == false)
                {
                    if (marketCtgModel.Downloaded == false) // do download
                    {
                        marketCtgModel.DownloadPercentage = 0;
                        marketCtgModel.IsDownloadRunning = true;

                        MarketStickerCategoryDTO ctgDTO;
                        if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgModel.StickerCategoryID, out ctgDTO))
                        {
                            RingMarketStickerLoadUtility.DownLoadStickerImages(marketCtgModel, ctgDTO, (isNointernet) =>
                            {
                                if (isNointernet)
                                {
                                    marketCtgModel.DownloadPercentage = 0;
                                    marketCtgModel.IsDownloadRunning = false;
                                    UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                                }
                                return 0;
                            });
                        }
                    }
                    else   // Remove Sticker
                    {
                        if (marketCtgModel.IsDefault)
                        {
                            UIHelperMethods.ShowWarning("You can't remove default sticker!", "Remove sticker");
                            return;
                        }

                        MarketStickerCategoryDTO ctgDTO = null;
                        if (StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(marketCtgModel.StickerCategoryID, out ctgDTO))
                        {
                            ctgDTO.Downloaded = false;
                            List<MarketStickerCategoryDTO> ctgDTOs = new List<MarketStickerCategoryDTO>();
                            ctgDTOs.Add(ctgDTO);
                            RingMarketStickerLoadUtility.LoadMarketStickerRecentCategorys(ctgDTOs);
                            RingMarkerStickerDAO.UpdateStickerCategoryDownloadStatus(ctgDTO.sCtId, ctgDTO.Downloaded);
                        }

                        marketCtgModel.Downloaded = false;
                        marketCtgModel.IsDownloadRunning = false;
                        marketCtgModel.DownloadPercentage = 0;
                        marketCtgModel.OnPropertyChanged("DetailImage");
                    }
                }
            }
        }

        private void LoadCategoryOrCollectionByScroll()
        {
            if (_CollectionId != 0)
            {
                this.LoadMoreCategoriesInACollection(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), MarketStickerDetailList.Count, 8, StickerType, _CollectionId);
            }
            else if (!string.IsNullOrEmpty(_CollectionOrLanguageName))
            {
                this.LoadMoreCategoriesInALanguage(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), MarketStickerDetailList.Count, 8, StickerType, _CollectionOrLanguageName);
            }
            else
            {
                int limit = MarketStickerDetailList.Count + 8;
                //if (StickerType == StickerMarketConstants.STICKER_TYPE_ALL)
                //{
                //    this.LoadAllCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), limit);
                //}

                //else if (StickerType == StickerMarketConstants.STICKER_TYPE_NEW)
                //{
                //    this.LoadNewCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), limit);
                //}
                //else if (StickerType == StickerMarketConstants.STICKER_TYPE_NEW)
                //{
                //    LoadPopularCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), limit);
                //}
                MarketStickerDynamicCategoryDTO dynamicCtgDTO = StickerDictionaries.Instance.MARKET_STICKER_DYNAMIC_CATEGORY_DICTIONARY.Where(P => P.id == _DynamicCategoryId).FirstOrDefault();
                if (dynamicCtgDTO != null)
                {
                    LoadDynamicCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), dynamicCtgDTO, limit);
                }
                if (MarketStickerDetailList.Count < limit)
                {
                    this.LoadMoreDynamicCategories(RingIDViewModel.Instance.MarketStickerCategoryList.ToList(), MarketStickerDetailList.Count, 8, 0, StickerType, _DynamicCategoryId);
                }
            }

            _IS_LOADING = false;
        }

        private void ResetData()
        {
            if (_Timer != null)
            {
                _Timer.Stop();
                _Timer.Enabled = false;
                _Timer = null;
            }

            ImagesListBox.ItemsSource = null;
            AnimationOnArrowClicked(0, 0);

            _NoOfSwappingCount = 0;
            _NextPrevCount = 0;
            _CurrentLeftMargin = 0;
            _IsFirstTimeNext = true;
            _IsFirstTimePrev = true;
            IsLeftArrowVisible = Visibility.Hidden;
            IsRightArrowVisible = Visibility.Hidden;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region EventHandler

        private void UCStickerDetailsViewPanel_Loaded(object sender, RoutedEventArgs e)
        {
            ImagesListBox.ItemsSource = _MarketStickerSlidingBannerList;
            myScrlViewer.ScrollChanged += myScrlViewer_ScrollChanged;
        }

        private void UCStickerDetailsViewPanel_Unloaded(object sender, RoutedEventArgs e)
        {
            myScrlViewer.ScrollChanged -= myScrlViewer_ScrollChanged;
            this.ResetData();
        }

        private void myScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (_IS_LOADING == false && e.VerticalOffset > 0 && e.VerticalOffset >= (myScrlViewer.ScrollableHeight - 200))
            {
                _IS_LOADING = true;
                if (!(ScrollThread != null && ScrollThread.IsAlive))
                {
                    ScrollThread = new Thread(new ThreadStart(LoadCategoryOrCollectionByScroll));
                    ScrollThread.Start();
                }
            }
        }

        private void OnBackToPreviousUI()
        {
            UCMenuStickerWrapper.Instance.ShowStickerInitView();
        }

        private void OnDetailsViewCommand(object param)
        {
            MarketStickerCategoryModel ctgModel = (MarketStickerCategoryModel)param;
            UCMenuStickerWrapper.Instance.ShowStickerDownloadView();
            UCMenuStickerWrapper.Instance._UCStickerMarketDownloadView.LoadSelectedSticker(ctgModel, true);
        }

        private void OnPrevious(object param)
        {
            try
            {
                int target = 0;
                _CurrentLeftMargin = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: OnPrevious() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNext(object param)
        {
            try
            {
                //int target = _CurrentLeftMargin - (int)pnlImageContainerWrapper.ActualWidth;
                int target = -630;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = 0;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(_CurrentLeftMargin, 0, 0, 0);
            MarginAnimation.To = new Thickness(target, 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
            storyboard.Begin(_ImageItemContainer);
        }

        private void RightArrowPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_Timer != null)
                {
                    _Timer.Stop();
                    OnNextCommand();
                    _Timer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LeftArrowPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_Timer != null)
                {
                    _Timer.Stop();
                    OnPreviousCommand();
                    _Timer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    OnNextCommand();
                }
                catch (Exception ex)
                {
                    log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: timer_Elapsed() => " + ex.Message + "\n" + ex.StackTrace);
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void OnNextCommand()
        {
            //Hints: For AutoSlide/Next, Viewport is 0 to -630.
            try
            {
                if (_Timer != null)
                {
                    _Timer.Stop();
                }

                if (_IsFirstTimeNext == false)
                {
                    int firstIndex = 0, middleIndex = 1, lastIndex = 2;
                    MarketStickerSlidingBannerList.InvokeMove(firstIndex, middleIndex);

                    if (MarketStickerSlidingBannerList.Count > 2)
                    {
                        MarketStickerSlidingBannerList.InvokeMove(middleIndex, lastIndex);
                        MarketStickerSlidingBannerList.RemoveAt(lastIndex);
                        if (StickerType == StickerMarketConstants.STICKER_TYPE_DYNAMIC)
                        {
                            int nextLoadingIndex = (_NextPrevCount + lastIndex) % _TotalBannerCount;
                            List<MarketStickerCategoryDTO> ctaDTOList = StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[_DynamicCategoryId];
                            if (ctaDTOList != null)
                            {
                                MarketStickerCategoryDTO ctgDTO = ctaDTOList.ElementAt(_NoOfSwappingCount);
                                if (ctgDTO != null)
                                {
                                    MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                                    if (ctgModel == null)
                                    {
                                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                    }
                                    MarketStickerSlidingBannerList.InvokeInsert(lastIndex, ctgModel);

                                    _NoOfSwappingCount = _NoOfSwappingCount == _TotalBannerCount - 1 ? 0 : _NoOfSwappingCount + 1;
                                }
                            }
                        }
                        else
                        {
                            int nextLoadingIndex = (_NextPrevCount + lastIndex) % MarketStickerDetailList.Count;
                            MarketStickerCategoryModel ctgModel = MarketStickerDetailList.ElementAt(nextLoadingIndex);
                            MarketStickerSlidingBannerList.InvokeInsert(lastIndex, ctgModel);

                            _NoOfSwappingCount = _NoOfSwappingCount == MarketStickerDetailList.Count - 1 ? 0 : _NoOfSwappingCount + 1;
                        }
                    }
                }

                _IsFirstTimeNext = false; // firstTime no swapping make, just changing viewport from 0 to -630.
                _IsFirstTimePrev = true;

                _NextPrevCount++;
                OnNext(null);
                SetNextPrevoiusButtonVisibility();

                if (_Timer != null)
                {
                    _Timer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: nextBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnPreviousCommand()
        {
            //Hints: For Previous, Viewport is -630 to 0.

            try
            {
                if (_Timer != null)
                {
                    _Timer.Stop();
                }

                int firstIndex = 0, middleIndex = 1, lastIndex = 2;
                if (_IsFirstTimePrev == false)
                {
                    if (MarketStickerSlidingBannerList.Count > 2)
                    {
                        MarketStickerSlidingBannerList.InvokeMove(middleIndex, lastIndex);
                        MarketStickerSlidingBannerList.InvokeMove(firstIndex, middleIndex);
                        MarketStickerSlidingBannerList.RemoveAt(firstIndex);
                        MarketStickerCategoryModel ctgModel = null;
                        if (StickerType == StickerMarketConstants.STICKER_TYPE_DYNAMIC)
                        {
                            List<MarketStickerCategoryDTO> ctaDTOList = StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[_DynamicCategoryId];
                            if (ctaDTOList != null)
                            {
                                MarketStickerCategoryDTO ctgDTO = ctaDTOList.ElementAt(_NoOfSwappingCount);
                                if (ctgDTO != null)
                                {
                                    ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                                    if (ctgModel == null)
                                    {
                                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                    }
                                }
                                MarketStickerSlidingBannerList.InvokeInsert(firstIndex, ctgModel);
                            }
                        }
                        else
                        {
                            ctgModel = MarketStickerDetailList.ElementAt(_NoOfSwappingCount);
                            MarketStickerSlidingBannerList.InvokeInsert(firstIndex, ctgModel);
                            _NoOfSwappingCount = _NoOfSwappingCount == MarketStickerDetailList.Count - 1 ? 0 : _NoOfSwappingCount + 1;
                        }
                    }
                }

                _IsFirstTimeNext = true;
                _IsFirstTimePrev = false;

                OnPrevious(null);
                _NextPrevCount--;

                if (_NoOfSwappingCount > 0)
                {
                    _NoOfSwappingCount = _NoOfSwappingCount - 1;
                }

                SetNextPrevoiusButtonVisibility();

                if (_Timer != null)
                {
                    _Timer.Start();
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(UCStickerMarketInitPanel).Name).Error("Error: prevBtn_Click() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SetNextPrevoiusButtonVisibility()
        {
            if (StickerType == StickerMarketConstants.STICKER_TYPE_DYNAMIC)
            {
                if (_NextPrevCount == _TotalBannerCount)
                {
                    _NextPrevCount = 0;
                }

                IsLeftArrowVisible = _NextPrevCount > 0 ? Visibility.Visible : Visibility.Collapsed;
                IsRightArrowVisible = _NextPrevCount < _TotalBannerCount - 1 ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                if (_NextPrevCount == MarketStickerDetailList.Count)
                {
                    _NextPrevCount = 0;
                }

                IsLeftArrowVisible = _NextPrevCount > 0 ? Visibility.Visible : Visibility.Collapsed;
                IsRightArrowVisible = _NextPrevCount < MarketStickerDetailList.Count - 1 ? Visibility.Visible : Visibility.Collapsed;

            }
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
            eventArg.RoutedEvent = UIElement.MouseWheelEvent;
            eventArg.Source = sender;
            var parent = ((Control)sender).Parent as UIElement;
            parent.RaiseEvent(eventArg);
        }

        private void HeaderPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as VirtualizingStackPanel;
        }

        #endregion

        #region Property

        private ObservableCollection<MarketStickerCategoryModel> _MarketStickerDetailList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> MarketStickerDetailList
        {
            get { return _MarketStickerDetailList; }
            set
            {
                if (_MarketStickerDetailList == value)
                {
                    return;
                }
                _MarketStickerDetailList = value;
                OnPropertyChanged("MarketStickerDetailList");
            }
        }

        private void _MarketStickerDetailList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add && MarketStickerSlidingBannerList.Count < 3)
            {
                if (StickerType == StickerMarketConstants.STICKER_TYPE_DYNAMIC)
                {
                    List<MarketStickerCategoryDTO> detailList = StickerDictionaries.Instance.MARKET_STICKER_BANNER_DICTIONARY[_DynamicCategoryId];
                    foreach (MarketStickerCategoryDTO ctgDTO in detailList)
                    {
                        if (!MarketStickerSlidingBannerList.Any(P => P.StickerCategoryID == ctgDTO.sCtId))
                        {
                            MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                            if (ctgModel == null)
                            {
                                ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            }

                            MarketStickerSlidingBannerList.InvokeAdd(ctgModel);
                            if (MarketStickerSlidingBannerList.Count == 3)
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    List<MarketStickerCategoryModel> detailList = MarketStickerDetailList.ToList();
                    foreach (MarketStickerCategoryModel ctgModel in detailList)
                    {
                        if (!MarketStickerSlidingBannerList.Any(P => P.StickerCategoryID == ctgModel.StickerCategoryID))
                        {
                            MarketStickerSlidingBannerList.InvokeAdd(ctgModel);
                            if (MarketStickerSlidingBannerList.Count == 3)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        private ObservableCollection<MarketStickerCategoryModel> _MarketStickerSlidingBannerList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> MarketStickerSlidingBannerList
        {
            get { return _MarketStickerSlidingBannerList; }
            set
            {
                if (_MarketStickerSlidingBannerList == value)
                {
                    return;
                }

                _MarketStickerSlidingBannerList = value;
                OnPropertyChanged("MarketStickerSlidingBannerList");
            }
        }

        private string _StickerName = string.Empty;
        public string StickerName
        {
            get { return _StickerName; }
            set
            {
                if (_StickerName == value)
                {
                    return;
                }

                _StickerName = value;
                OnPropertyChanged("StickerName");
            }
        }

        private Visibility _IsLeftArrowVisible = Visibility.Visible;
        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                if (value == _IsLeftArrowVisible) { return; }
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        private Visibility _IsRightArrowVisible = Visibility.Visible;
        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                if (value == _IsRightArrowVisible) { return; }
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }

        #endregion

        #region Command

        private ICommand _DownloadCommand;
        public ICommand DownloadCommand
        {
            get
            {
                if (_DownloadCommand == null)
                {
                    _DownloadCommand = new RelayCommand(param => OnStickerDownloadCommand(param));
                }
                return _DownloadCommand;
            }
        }

        private ICommand _DetailsCommand;
        public ICommand DetailsCommand
        {
            get
            {
                if (_DetailsCommand == null)
                {
                    _DetailsCommand = new RelayCommand(param => OnDetailsViewCommand(param));
                }
                return _DetailsCommand;
            }
        }

        private ICommand _BackToPrevoiousUI;
        public ICommand BackToPrevoiousUI
        {
            get
            {
                if (_BackToPrevoiousUI == null)
                {
                    _BackToPrevoiousUI = new RelayCommand(param => OnBackToPreviousUI());
                }
                return _BackToPrevoiousUI;
            }
        }

        private ICommand _NextCommand;
        public ICommand NextCommand
        {
            get
            {
                if (_NextCommand == null)
                {
                    _NextCommand = new RelayCommand(param => OnNextCommand());
                }
                return _NextCommand;
            }
        }

        private ICommand _PrevCommand;
        public ICommand PrevCommand
        {
            get
            {
                if (_PrevCommand == null)
                {
                    _PrevCommand = new RelayCommand(param => OnPreviousCommand());
                }
                return _PrevCommand;
            }
        }

        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

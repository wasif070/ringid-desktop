﻿using Auth.Service.RingMarket;
using imsdkwrapper;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
using View.UI.Chat;
using View.Utility;
using View.Utility.RingMarket;
using View.ViewModel;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for UCRingMarketCategoryPanel.xaml
    /// </summary>
    public partial class UCRingChatStickerCategoryPanel : UserControl, ISwitchable, INotifyPropertyChanged
    {

        private MarketStickerCategoryModel _CategoryModel;
        private ICommand _StickerImageClickedCommand;

        public UCRingChatStickerCategoryPanel(MarketStickerCategoryModel model)
        {
            InitializeComponent();
            this.CategoryModel = model;
            this.DataContext = this;
        }

        public bool UtilizeState(object state)
        {
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void Scroll_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                Scroll.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                Scroll.ScrollToEnd();
                e.Handled = true;
            }
        }

        public MarketStickerCategoryModel CategoryModel
        {
            get { return _CategoryModel; }
            set
            {
                _CategoryModel = value;
                OnPropertyChanged("CategoryModel");
            }
        }
       
        public void OnStickerImageClickedClicked(object parameter)
        {
            if (parameter != null && parameter is MarkertStickerImagesModel)
            {
                MarkertStickerImagesModel imageModel = (MarkertStickerImagesModel)parameter;
                if (UCMiddlePanelSwitcher.View_UCFriendChatCallPanel != null
                    && UCMiddlePanelSwitcher.View_UCFriendChatCallPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCFriendChatCallPanel.BuildAndSendChatPacket(imageModel.ToString(), (int)MessageType.DOWNLOAD_STICKER_MESSAGE, imageModel);
                }
                else if (UCMiddlePanelSwitcher.View_UCGroupChatCallPanel != null
                    && UCMiddlePanelSwitcher.View_UCGroupChatCallPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCGroupChatCallPanel.BuildAndSendChatPacket(imageModel.ToString(), (int)MessageType.DOWNLOAD_STICKER_MESSAGE, imageModel);
                }
                else if (UCMiddlePanelSwitcher.View_UCRoomChatCallPanel != null
                    && UCMiddlePanelSwitcher.View_UCRoomChatCallPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCRoomChatCallPanel.BuildAndSendChatPacket(imageModel.ToString(), (int)MessageType.DOWNLOAD_STICKER_MESSAGE, imageModel);
                }
            }
        }
        public bool CanStickerImageClickedClicked(object parameter)
        {
            return (UCMiddlePanelSwitcher.View_UCFriendChatCallPanel != null
                    && UCMiddlePanelSwitcher.View_UCFriendChatCallPanel.IsVisible)
                    ||
                    (UCMiddlePanelSwitcher.View_UCGroupChatCallPanel != null
                    && UCMiddlePanelSwitcher.View_UCGroupChatCallPanel.IsVisible)
                    ||
                    (UCMiddlePanelSwitcher.View_UCRoomChatCallPanel != null
                    && UCMiddlePanelSwitcher.View_UCRoomChatCallPanel.IsVisible);
        }

        public ICommand StickerImageClickedCommand
        {
            get
            {
                if (_StickerImageClickedCommand == null)
                {
                    _StickerImageClickedCommand = new RelayCommand(param => OnStickerImageClickedClicked(param), param => CanStickerImageClickedClicked(param));
                }
                return _StickerImageClickedCommand;
            }
        }

    }
}

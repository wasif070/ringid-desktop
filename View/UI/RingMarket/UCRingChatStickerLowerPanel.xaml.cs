<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Models.Utility;
using View.Dictonary;
using View.Utility.RingMarket;
using log4net;
using System.ComponentModel;
using View.Utility;
using System.Windows.Media.Animation;
using View.BindingModels;
using View.ViewModel;
using Auth.Service.RingMarket;
using View.Constants;
using System.Collections.ObjectModel;
using System.Threading;
using Models.Entity;
using Models.Stores;
using View.UI.PopUp;
using Models.Constants;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for UCRingChatStickerLowerPanel.xaml
    /// </summary>
    public partial class UCRingChatStickerLowerPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCRingChatStickerLowerPanel).Name);

        public static UCRingChatStickerLowerPanel Instance = null;
        private ScrollViewer _Scroll = null;
        private Storyboard _Storyboard;
        private MarketStickerCategoryModel _SelectedStickerModel;

        private bool _IsFirstTime = false;
        private bool _IS_LOADING { get; set; }
        private bool _IsCategoryListOpened = true;
        private string _StickerFocused = String.Empty;
        private double PreVerticalChange = 0.0;

        private ICommand _MoreStickerCommand;
        private Thread ScrollThread = null;

        public UCRingChatStickerLowerPanel()
        {
            InitializeComponent();
            Instance = this;
            this.DataContext = this;
            this.OnCategoryListView(0D);
        }

        #region Event Handler

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            if (_Storyboard != null)
            {
                _Storyboard.Completed -= Storyboard_Completed;
                _Storyboard = null;
            }
        }

        private void Sticker_MouseEnter(object sender, MouseEventArgs e)
        {
            if ((FrameworkElement)sender is Grid && ((FrameworkElement)sender).ToolTip != null)
            {
                StickerFocused = ((FrameworkElement)sender).ToolTip.ToString();
            }
        }

        private void Sticker_MouseLeave(object sender, MouseEventArgs e)
        {
            if ((FrameworkElement)sender is Grid && ((FrameworkElement)sender).ToolTip != null)
            {
                StickerFocused = String.Empty;
            }
        }

        #endregion Event Handler

        #region Utility

        public void OnNavigate(object param)
        {
            MarketStickerCategoryModel model = null;
            try
            {
                model = (MarketStickerCategoryModel)param;
                if (pnlCategory.Child != null)
                {
                    MarketStickerCategoryModel prevModel = ((UCRingChatStickerCategoryPanel)pnlCategory.Child).CategoryModel;
                    if (prevModel.StickerCategoryID == model.StickerCategoryID)
                        return;
                    prevModel.IsSelected = false;
                }

                if (model.Downloaded == true)
                {
                    MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == model.StickerCategoryID).FirstOrDefault();
                    RingMarketStickerLoadUtility.LaodAndDownLoadStickerImages(model, ctgDTO);
                }

                UCRingChatStickerCategoryPanel categoryPanel = UIDictionaries.Instance.RING_MARKET_STICKER_DICTIONARY.TryGetValue(model.StickerCategoryID);
                if (categoryPanel == null)
                {
                    categoryPanel = new UCRingChatStickerCategoryPanel(model);
                    UIDictionaries.Instance.RING_MARKET_STICKER_DICTIONARY[model.StickerCategoryID] = categoryPanel;
                }
                else
                {
                    if (model.IsDetailImageDownloading == false)
                    {
                        if (model.Downloaded == false)
                        {
                            model.OnPropertyChanged("DetailImage");
                        }
                    }
                }

                model.IsSelected = true;
                SelectedStickerModel = model;

                pnlCategory.Child = categoryPanel;
                categoryPanel.UtilizeState(param);

                if (IsCategoryListOpened)
                {
                    OnCategoryListView(0D);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: OnNavigate() ==> ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnCategoryListView(object param)
        {
            try
            {
                double delay = 0.25;
                if (param != null && param is double)
                {
                    delay = (double)param;
                }

                if (_Storyboard != null)
                {
                    _Storyboard.Stop();
                    _Storyboard = null;
                }

                IsCategoryListOpened = !IsCategoryListOpened;
                this.AnimationOnArrowClicked(IsCategoryListOpened ? 0 : -240, delay);
                ShowAllCategoryList();
                _IsFirstTime = true;
            }
            catch (Exception ex)
            {

                log.Error("Error: OnCategoryListView() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnCloseClicked(object param)
        {
            try
            {
                RingIDViewModel.Instance.CollapseCommand.Execute(null);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCloseClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowAllCategoryList()
        {
            if (_IsFirstTime == true)
            {
                _IsFirstTime = false;
                Thread t = new Thread(RunStickerCategory);
                t.Start();
            }
        }

        private void RunStickerCategory()
        {
            int startIndex = 0, categoryLimit = 20, collectionLimit = 0, stickerType = StickerMarketConstants.STICKER_TYPE_ALL;

            RingMarketStickerService.Instance.GetMoreStickerCategoriesOfSameType(startIndex, categoryLimit, collectionLimit, stickerType);
            List<MarketStickerCategoryModel> ctgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

            foreach (MarketStickerCategoryDTO ctgDTO in StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values)
            {
                if (ctgDTO.sCtId > 0)
                {
                    MarketStickerCategoryModel ctgModel = ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                        Thread.Sleep(10);
                    }
                }
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            try
            {
                ThicknessAnimation MarginAnimation = new ThicknessAnimation();
                MarginAnimation.From = new Thickness(0, 0, 0, brdStickerCategory.Margin.Bottom);
                MarginAnimation.To = new Thickness(0, 0, 0, target);
                MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                _Storyboard = new Storyboard();
                _Storyboard.Children.Add(MarginAnimation);
                Storyboard.SetTarget(MarginAnimation, brdStickerCategory);
                Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(StackPanel.MarginProperty));
                _Storyboard.Completed += Storyboard_Completed;
                _Storyboard.Begin(brdStickerCategory);
            }
            catch (Exception ex)
            {
                log.Error("Error: AnimationOnArrowClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowInitLoader(bool isNeedToLoading)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (isNeedToLoading)
                {
                    InitLoader = ImageUtility.GetBitmapImage(View.Constants.ImageLocation.LOADER_DOWNLOAD_SMALL);
                    GC.SuppressFinalize(InitLoader);
                }
                else
                {
                    InitLoader = null;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void OnDownloadClicked(object parameter)
        {
            if (parameter != null && parameter is int)
            {
                int categoryID = (int)parameter;
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (ctgModel != null && ctgModel.Downloaded == false && ctgModel.IsDownloadRunning == false)
                {
                    ctgModel.DownloadPercentage = 0;
                    ctgModel.IsDownloadRunning = true;

                    MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(categoryID);
                    if (ctgDTO != null)
                    {
                        RingMarketStickerLoadUtility.DownLoadStickerImages(ctgDTO, (isNointernet) =>
                        {
                            if (isNointernet)
                            {
                                ctgModel.DownloadPercentage = 0;
                                ctgModel.IsDownloadRunning = false;
                                UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                            }
                            return 0;
                        });
                    }
                }
            }
        }

        private void OnMoreStickerClickedCommand()
        {
            if (MainSwitcher.PopupController.stickerPopUPInComments != null && MainSwitcher.PopupController.stickerPopUPInComments.popupEmoticon.IsOpen)
            {
                MainSwitcher.PopupController.stickerPopUPInComments.popupEmoticon.IsOpen = false;
            }
            UCMoreStickersPopUpInComments moreStickersPopUpInComments = new UCMoreStickersPopUpInComments(UCGuiRingID.Instance.MotherPanel);
            moreStickersPopUpInComments.Show();
            moreStickersPopUpInComments.LoadData();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility

        #region Property

        private ICommand _NavigateCommand;
        public ICommand NavigateCommand
        {
            get
            {
                if (_NavigateCommand == null)
                {
                    _NavigateCommand = new RelayCommand(param => OnNavigate(param));
                }
                return _NavigateCommand;
            }
        }

        private ICommand _CategoryListViewCommand;
        public ICommand CategoryListViewCommand
        {
            get
            {
                if (_CategoryListViewCommand == null)
                {
                    _CategoryListViewCommand = new RelayCommand(param => OnCategoryListView(param));
                }
                return _CategoryListViewCommand;
            }
        }
        private ICommand _StickerDownloadCommand;
        public ICommand StickerDownloadCommand
        {
            get
            {
                if (_StickerDownloadCommand == null)
                {
                    _StickerDownloadCommand = new RelayCommand(param => OnDownloadClicked(param));
                }
                return _StickerDownloadCommand;
            }
        }

        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(param => OnCloseClicked(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand MoreStickerCommand
        {
            get
            {
                if (_MoreStickerCommand == null) _MoreStickerCommand = new RelayCommand(param => OnMoreStickerClickedCommand());
                return _MoreStickerCommand;
            }
        }

        public bool IsCategoryListOpened
        {
            get { return _IsCategoryListOpened; }
            set
            {
                if (_IsCategoryListOpened == value)
                    return;

                _IsCategoryListOpened = value;
                OnPropertyChanged("IsCategoryListOpened");
            }
        }

        public string StickerFocused
        {
            get { return _StickerFocused; }
            set
            {
                _StickerFocused = value;
                this.OnPropertyChanged("StickerFocused");
            }
        }

        private ObservableCollection<MarketStickerCategoryModel> _AllCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> AllCategoryList
        {
            get { return _AllCategoryList; }
            set
            {
                if (_AllCategoryList == value) return;
                _AllCategoryList = value;
                this.OnPropertyChanged("AllCategoryList");
            }
        }

        public MarketStickerCategoryModel SelectedStickerModel
        {
            get { return _SelectedStickerModel; }
            set
            {
                _SelectedStickerModel = value;
                this.OnPropertyChanged("SelectedStickerModel");
            }
        }
        private ImageSource _InitLoader;
        public ImageSource InitLoader
        {
            get { return _InitLoader; }
            set
            {
                _InitLoader = value;
                this.OnPropertyChanged("InitLoader");
            }
        }

        #endregion Property

        #region EventHandler

        private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            _Scroll = (ScrollViewer)sender;
            _Scroll.ScrollChanged += _Scroll_ScrollChanged;
        }

        private void _Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            if (_IS_LOADING == false && e.VerticalChange > 0 && e.VerticalChange != PreVerticalChange && e.VerticalOffset >= (_Scroll.ScrollableHeight - 20))
            {
                _IS_LOADING = true;

                if (!(ScrollThread != null && ScrollThread.IsAlive))
                {
                    ScrollThread = new Thread(new ThreadStart(LoadCategoryWithinScroll));
                    ScrollThread.Start();
                    PreVerticalChange = e.VerticalChange;
                }
            }
        }

        private void LoadCategoryWithinScroll()
        {
            int startIndex = StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST.Count, categoryLimit = 8, collectionLimit = 0, stickerType = StickerMarketConstants.STICKER_TYPE_ALL;
            List<MarketStickerCategoryDTO> categoriesList = RingMarketStickerService.Instance.GetMoreStickerCategoriesOfSameType(startIndex, categoryLimit, collectionLimit, stickerType);

            if (categoriesList != null && categoriesList.Count > 0)
            {
                List<MarketStickerCategoryModel> ctgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                foreach (MarketStickerCategoryDTO ctgDTO in categoriesList)
                {
                    if (ctgDTO.sCtId > 0)
                    {
                        MarketStickerCategoryModel ctgModel = ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                        if (ctgModel == null)
                        {
                            RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            Thread.Sleep(10);
                        }
                    }
                }
            }

            _IS_LOADING = false;
        }

        #endregion
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Models.Utility;
using View.Dictonary;
using View.Utility.RingMarket;
using log4net;
using System.ComponentModel;
using View.Utility;
using System.Windows.Media.Animation;
using View.BindingModels;
using View.ViewModel;
using Auth.Service.RingMarket;
using View.Constants;
using System.Collections.ObjectModel;
using System.Threading;
using Models.Entity;
using Models.Stores;
using View.UI.PopUp;
using Models.Constants;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for UCRingChatStickerLowerPanel.xaml
    /// </summary>
    public partial class UCRingChatStickerLowerPanel : UserControl, INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCRingChatStickerLowerPanel).Name);

        public static UCRingChatStickerLowerPanel Instance = null;
        private ScrollViewer _Scroll = null;
        private Storyboard _Storyboard;
        private MarketStickerCategoryModel _SelectedStickerModel;

        private bool _IsFirstTime = false;
        private bool _IS_LOADING { get; set; }
        private bool _IsCategoryListOpened = true;
        private string _StickerFocused = String.Empty;
        private double PreVerticalChange = 0.0;

        private ICommand _MoreStickerCommand;
        private Thread ScrollThread = null;

        public UCRingChatStickerLowerPanel()
        {
            InitializeComponent();
            Instance = this;
            this.DataContext = this;
            this.OnCategoryListView(0D);
        }

        #region Event Handler

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            if (_Storyboard != null)
            {
                _Storyboard.Completed -= Storyboard_Completed;
                _Storyboard = null;
            }
        }

        private void Sticker_MouseEnter(object sender, MouseEventArgs e)
        {
            if ((FrameworkElement)sender is Grid && ((FrameworkElement)sender).ToolTip != null)
            {
                StickerFocused = ((FrameworkElement)sender).ToolTip.ToString();
            }
        }

        private void Sticker_MouseLeave(object sender, MouseEventArgs e)
        {
            if ((FrameworkElement)sender is Grid && ((FrameworkElement)sender).ToolTip != null)
            {
                StickerFocused = String.Empty;
            }
        }

        #endregion Event Handler

        #region Utility

        public void OnNavigate(object param)
        {
            MarketStickerCategoryModel model = null;
            try
            {
                model = (MarketStickerCategoryModel)param;
                if (pnlCategory.Child != null)
                {
                    MarketStickerCategoryModel prevModel = ((UCRingChatStickerCategoryPanel)pnlCategory.Child).CategoryModel;
                    if (prevModel.StickerCategoryID == model.StickerCategoryID)
                        return;
                    prevModel.IsSelected = false;
                }

                if (model.Downloaded == true)
                {
                    MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == model.StickerCategoryID).FirstOrDefault();
                    RingMarketStickerLoadUtility.DownLoadStickerImages(model, ctgDTO, (isNointernet) =>
                    {
                        if (isNointernet)
                        {
                            model.DownloadPercentage = 0;
                            model.IsDownloadRunning = false;
                            UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                        }
                        return 0;
                    });
                }

                UCRingChatStickerCategoryPanel categoryPanel = UIDictionaries.Instance.RING_MARKET_STICKER_DICTIONARY.TryGetValue(model.StickerCategoryID);
                if (categoryPanel == null)
                {
                    categoryPanel = new UCRingChatStickerCategoryPanel(model);
                    UIDictionaries.Instance.RING_MARKET_STICKER_DICTIONARY[model.StickerCategoryID] = categoryPanel;
                }
                else
                {
                    if (model.IsDetailImageDownloading == false)
                    {
                        if (model.Downloaded == false)
                        {
                            model.OnPropertyChanged("DetailImage");
                        }
                    }
                }

                model.IsSelected = true;
                SelectedStickerModel = model;

                pnlCategory.Child = categoryPanel;
                categoryPanel.UtilizeState(param);

                if (IsCategoryListOpened)
                {
                    OnCategoryListView(0D);
                }
            }
            catch (Exception ex)
            {

                log.Error("Error: OnNavigate() ==> ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnCategoryListView(object param)
        {
            try
            {
                double delay = 0.25;
                if (param != null && param is double)
                {
                    delay = (double)param;
                }

                if (_Storyboard != null)
                {
                    _Storyboard.Stop();
                    _Storyboard = null;
                }

                IsCategoryListOpened = !IsCategoryListOpened;
                this.AnimationOnArrowClicked(IsCategoryListOpened ? 0 : -240, delay);
                ShowAllCategoryList();
                _IsFirstTime = true;
            }
            catch (Exception ex)
            {

                log.Error("Error: OnCategoryListView() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }

        private void OnCloseClicked(object param)
        {
            try
            {
                RingIDViewModel.Instance.CollapseCommand.Execute(null);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCloseClicked() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowAllCategoryList()
        {
            if (_IsFirstTime == true)
            {
                _IsFirstTime = false;
                Thread t = new Thread(RunStickerCategory);
                t.Start();
            }
        }

        private void RunStickerCategory()
        {
            int startIndex = 0, categoryLimit = 20, collectionLimit = 0, stickerType = StickerMarketConstants.STICKER_TYPE_ALL;

            RingMarketStickerService.Instance.GetMoreStickerCategoriesOfSameType(startIndex, categoryLimit, collectionLimit, stickerType);
            List<MarketStickerCategoryModel> ctgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

            foreach (MarketStickerCategoryDTO ctgDTO in StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values)
            {
                if (ctgDTO.sCtId > 0)
                {
                    MarketStickerCategoryModel ctgModel = ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                    if (ctgModel == null)
                    {
                        RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                        Thread.Sleep(10);
                    }
                }
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            try
            {
                ThicknessAnimation MarginAnimation = new ThicknessAnimation();
                MarginAnimation.From = new Thickness(0, 0, 0, brdStickerCategory.Margin.Bottom);
                MarginAnimation.To = new Thickness(0, 0, 0, target);
                MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                _Storyboard = new Storyboard();
                _Storyboard.Children.Add(MarginAnimation);
                Storyboard.SetTarget(MarginAnimation, brdStickerCategory);
                Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(StackPanel.MarginProperty));
                _Storyboard.Completed += Storyboard_Completed;
                _Storyboard.Begin(brdStickerCategory);
            }
            catch (Exception ex)
            {
                log.Error("Error: AnimationOnArrowClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowInitLoader(bool isNeedToLoading)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (isNeedToLoading)
                {
                    InitLoader = ImageUtility.GetBitmapImage(View.Constants.ImageLocation.LOADER_DOWNLOAD_SMALL);
                    GC.SuppressFinalize(InitLoader);
                }
                else
                {
                    InitLoader = null;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void OnDownloadClicked(object parameter)
        {
            if (parameter != null && parameter is int)
            {
                int categoryID = (int)parameter;
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (ctgModel != null && ctgModel.Downloaded == false && ctgModel.IsDownloadRunning == false)
                {
                    ctgModel.DownloadPercentage = 0;
                    ctgModel.IsDownloadRunning = true;

                    MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(categoryID);
                    if (ctgDTO != null)
                    {
                        RingMarketStickerLoadUtility.DownLoadStickerImages(ctgModel, ctgDTO, (isNointernet) =>
                        {
                            if (isNointernet)
                            {
                                ctgModel.DownloadPercentage = 0;
                                ctgModel.IsDownloadRunning = false;
                                UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Sticker image");
                            }
                            return 0;
                        });
                    }
                }
            }
        }

        private void OnMoreStickerClickedCommand()
        {
            if (MainSwitcher.PopupController.stickerPopUPInComments != null && MainSwitcher.PopupController.stickerPopUPInComments.popupEmoticon.IsOpen)
            {
                MainSwitcher.PopupController.stickerPopUPInComments.popupEmoticon.IsOpen = false;
            }
            UCMoreStickersPopUpInComments moreStickersPopUpInComments = new UCMoreStickersPopUpInComments(UCGuiRingID.Instance.MotherPanel);
            moreStickersPopUpInComments.Show();
            moreStickersPopUpInComments.LoadData();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility

        #region Property

        private ICommand _NavigateCommand;
        public ICommand NavigateCommand
        {
            get
            {
                if (_NavigateCommand == null)
                {
                    _NavigateCommand = new RelayCommand(param => OnNavigate(param));
                }
                return _NavigateCommand;
            }
        }

        private ICommand _CategoryListViewCommand;
        public ICommand CategoryListViewCommand
        {
            get
            {
                if (_CategoryListViewCommand == null)
                {
                    _CategoryListViewCommand = new RelayCommand(param => OnCategoryListView(param));
                }
                return _CategoryListViewCommand;
            }
        }
        private ICommand _StickerDownloadCommand;
        public ICommand StickerDownloadCommand
        {
            get
            {
                if (_StickerDownloadCommand == null)
                {
                    _StickerDownloadCommand = new RelayCommand(param => OnDownloadClicked(param));
                }
                return _StickerDownloadCommand;
            }
        }

        private ICommand _CloseCommand;
        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand(param => OnCloseClicked(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand MoreStickerCommand
        {
            get
            {
                if (_MoreStickerCommand == null) _MoreStickerCommand = new RelayCommand(param => OnMoreStickerClickedCommand());
                return _MoreStickerCommand;
            }
        }

        public bool IsCategoryListOpened
        {
            get { return _IsCategoryListOpened; }
            set
            {
                if (_IsCategoryListOpened == value)
                    return;

                _IsCategoryListOpened = value;
                OnPropertyChanged("IsCategoryListOpened");
            }
        }

        public string StickerFocused
        {
            get { return _StickerFocused; }
            set
            {
                _StickerFocused = value;
                this.OnPropertyChanged("StickerFocused");
            }
        }

        private ObservableCollection<MarketStickerCategoryModel> _AllCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> AllCategoryList
        {
            get { return _AllCategoryList; }
            set
            {
                if (_AllCategoryList == value) return;
                _AllCategoryList = value;
                this.OnPropertyChanged("AllCategoryList");
            }
        }

        public MarketStickerCategoryModel SelectedStickerModel
        {
            get { return _SelectedStickerModel; }
            set
            {
                _SelectedStickerModel = value;
                this.OnPropertyChanged("SelectedStickerModel");
            }
        }
        private ImageSource _InitLoader;
        public ImageSource InitLoader
        {
            get { return _InitLoader; }
            set
            {
                _InitLoader = value;
                this.OnPropertyChanged("InitLoader");
            }
        }

        #endregion Property

        #region EventHandler

        private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            _Scroll = (ScrollViewer)sender;
            _Scroll.ScrollChanged += _Scroll_ScrollChanged;
        }

        private void _Scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

            if (_IS_LOADING == false && e.VerticalChange > 0 && e.VerticalChange != PreVerticalChange && e.VerticalOffset >= (_Scroll.ScrollableHeight - 20))
            {
                _IS_LOADING = true;

                if (!(ScrollThread != null && ScrollThread.IsAlive))
                {
                    ScrollThread = new Thread(new ThreadStart(LoadCategoryWithinScroll));
                    ScrollThread.Start();
                    PreVerticalChange = e.VerticalChange;
                }
            }
        }

        private void LoadCategoryWithinScroll()
        {
            int startIndex = StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST.Count, categoryLimit = 8, collectionLimit = 0, stickerType = StickerMarketConstants.STICKER_TYPE_ALL;
            List<MarketStickerCategoryDTO> categoriesList = RingMarketStickerService.Instance.GetMoreStickerCategoriesOfSameType(startIndex, categoryLimit, collectionLimit, stickerType);

            if (categoriesList != null && categoriesList.Count > 0)
            {
                List<MarketStickerCategoryModel> ctgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();
                foreach (MarketStickerCategoryDTO ctgDTO in categoriesList)
                {
                    if (ctgDTO.sCtId > 0)
                    {
                        MarketStickerCategoryModel ctgModel = ctgModelList.Where(P => P.StickerCategoryID == ctgDTO.sCtId).FirstOrDefault();
                        if (ctgModel == null)
                        {
                            RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                            Thread.Sleep(10);
                        }
                    }
                }
            }

            _IS_LOADING = false;
        }

        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace View.UI.RingMarket
{
    /// <summary>
    /// Interaction logic for ProgressAnimation.xaml
    /// </summary>
    public partial class UCProgressAnimation : UserControl
    {
        Storyboard _ProgressAnimation;

        public UCProgressAnimation()
        {
            InitializeComponent();
            this.Visibility = System.Windows.Visibility.Collapsed;
            this._ProgressAnimation = (Storyboard)FindResource("ProgressAnimation");
        }
        
        public void StartAnimation()
        {
            this.Visibility = System.Windows.Visibility.Visible;
            if (_ProgressAnimation != null)
            {
                _ProgressAnimation.Begin();
            }
        }

        public void StopAnimation()
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
            if (_ProgressAnimation != null)
            {
                _ProgressAnimation.Stop();
            }
        }

        public bool ShowAnimation
        {
            get { return (bool)GetValue(ShowAnimationProperty); }
            set { SetValue(ShowAnimationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowAnimationProperty = DependencyProperty.Register("ShowAnimation", typeof(bool), typeof(UCProgressAnimation), new PropertyMetadata(false, (s, e) => 
        {
            if (s != null)
            {
                UCProgressAnimation panel = (UCProgressAnimation)s;
                bool state = (bool)e.NewValue;
                if (state)
                {
                    panel.StartAnimation();
                }
                else
                {
                    panel.StopAnimation();
                }
            }
        }));


    }
}

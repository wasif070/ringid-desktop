﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Auth;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using Models.DAO;
using View.Utility.DataContainer;
using System.Windows.Input;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for UCDownloadMediaFile.xaml
    /// </summary>
    public partial class UCDownloadMediaFile : UserControl, INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UCDownloadMediaFile).Name);
        private SingleMediaModel _PlayingMedia;
        public SingleMediaModel PlayingMedia
        {
            get { return _PlayingMedia; }
            set
            {
                if (value == _PlayingMedia)
                    return;
                _PlayingMedia = value;
                this.OnPropertyChanged("PlayingMedia");
            }
        }
        #region "Properties"
        private SingleMediaModel _singleMediaModel;
        public SingleMediaModel singleMediaModel
        {
            get { return _singleMediaModel; }
            set
            {
                if (value == _singleMediaModel)
                    return;
                _singleMediaModel = value;
                this.OnPropertyChanged("singleMediaModel");
            }
        }

        private bool _shareOptionsPopup;
        public bool ShareOptionsPopup
        {
            get { return _shareOptionsPopup; }
            set
            {
                if (value == _shareOptionsPopup)
                    return;
                _shareOptionsPopup = value;
                this.OnPropertyChanged("ShareOptionsPopup");
            }
        }

        #endregion"Properties"

        public UCDownloadMediaFile()
        {
            InitializeComponent();
        }
        public static readonly DependencyProperty ArgumentProperty = DependencyProperty.Register("Argument", typeof(object), typeof(UCDownloadMediaFile), new PropertyMetadata((s, e) =>
        {
            if (e.NewValue != null && e.NewValue is SingleMediaModel)
            {
                UCDownloadMediaFile panel = (UCDownloadMediaFile)s;
                panel.singleMediaModel = (SingleMediaModel)e.NewValue;
                panel.singleMediaModel.OnPropertyChanged("CurrentInstance");
                panel.DataContext = panel;
            }
        }
        ));
        public object Argument
        {
            get { return (object)GetValue(ArgumentProperty); }
            set
            {
                SetValue(ArgumentProperty, value);
            }
        }

        private void AddToAlbum_Click(object sender, RoutedEventArgs e)
        {
            AddOrDownload(false, (Button)sender);
        }


        public void AddOrDownload(bool isDownload, Control cntrl = null)
        {
            UCDownloadOrAddToAlbumPopUp ucDownloadOrAddToAlbumPopUp = new UCDownloadOrAddToAlbumPopUp(UCGuiRingID.Instance.MotherPanel);

            ucDownloadOrAddToAlbumPopUp.OnRemovedUserControl += () =>
            {
                ucDownloadOrAddToAlbumPopUp = null;
            };
            if (singleMediaModel.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
            {
                if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                {
                    ucDownloadOrAddToAlbumPopUp.Show();
                    ucDownloadOrAddToAlbumPopUp.ShowPopup(singleMediaModel, SettingsConstants.MEDIA_TYPE_AUDIO, isDownload, false, () => { return 0; });
                }
                else
                {
                    ucDownloadOrAddToAlbumPopUp.Show();
                    ucDownloadOrAddToAlbumPopUp.ShowPopup(singleMediaModel, SettingsConstants.MEDIA_TYPE_AUDIO, isDownload, false, () => { return 0; });
                    SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_AUDIO, System.Guid.Empty); //TODO
                }
            }
            else if (singleMediaModel.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
            {
                if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                {
                    ucDownloadOrAddToAlbumPopUp.Show();
                    ucDownloadOrAddToAlbumPopUp.ShowPopup(singleMediaModel, SettingsConstants.MEDIA_TYPE_VIDEO, isDownload, false, () => { return 0; });
                }
                else
                {
                    ucDownloadOrAddToAlbumPopUp.Show();
                    ucDownloadOrAddToAlbumPopUp.ShowPopup(singleMediaModel, SettingsConstants.MEDIA_TYPE_VIDEO, isDownload, false, () => { return 0; });
                    SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_VIDEO, System.Guid.Empty); //TODO
                }
            }
        }

        CustomFileDownloader fileDownloader = null;
        public void MakeDownloaderStart()
        {
            //if (AddOrDownloadToNewAlbum.Trim().Equals(NotificationMessages.DOWNLOAD_TO_NEW_ALBUM)
            //    || AddOrDownloadToExistingAlbum.Trim().Equals(NotificationMessages.DOWNLOAD_TO_EXISTING_ALBUM))
            //{
            //popupAddToAlbum.IsOpen = false;

            UploadingModel _uploadingModel = new UploadingModel();
            _uploadingModel.IsDownload = true;
            _uploadingModel.ContentId = this.PlayingMedia.ContentId;
            _uploadingModel.UploadingContent = this.PlayingMedia.MediaType == 1 ? "1 audio is downloading..." : "1 video is downloading...";
            RingIDViewModel.Instance.UploadingModelList.Add(_uploadingModel);

            if (System.IO.File.Exists(HelperMethods.GetDownloadedFilePathFromStreamURL(PlayingMedia.StreamUrl, PlayingMedia.ContentId)))
            {
                //this.PlayingMedia.DeleteDownloadMedia(PlayingMedia.ContentId);
                PlayingMedia.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                PlayingMedia.DownloadProgress = 0;
                MediaDAO.Instance.DeleteFromDownloadedMediasTable(PlayingMedia.ContentId);
                //TODO delete downloaded files from PC
            }

            if (!MediaDataContainer.Instance.CurrentDownloads.TryGetValue(PlayingMedia.ContentId, out fileDownloader))
            {
                fileDownloader = new CustomFileDownloader(PlayingMedia);
                MediaDataContainer.Instance.CurrentDownloads[PlayingMedia.ContentId] = fileDownloader;
            }
            if (fileDownloader.Files == null || (this.fileDownloader.Files != null && this.fileDownloader.Files.Count == 0))
            {
                this.fileDownloader.Files.Add(new FileDownloader.FileInfo(ServerAndPortSettings.GetVODServerResourceURL + this.PlayingMedia.StreamUrl, this.PlayingMedia.ContentId.ToString()));
            }
            this.fileDownloader.Start();
            //}
        }

        #region Property
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private UCMediaShareMoreOptions ucMediaShareMoreOptions { get; set; }
        #endregion

        private void ShowOptionsPopup(int type)
        {
            if (ucMediaShareMoreOptions != null)
                mainPanel.Children.Remove(ucMediaShareMoreOptions);
            ucMediaShareMoreOptions = new UCMediaShareMoreOptions(mainPanel);
            ucMediaShareMoreOptions.Show();
            ucMediaShareMoreOptions.ShowOptionsPopup(singleMediaModel, shareMedia, singleMediaModel.MediaType, type);
            ucMediaShareMoreOptions.OnRemovedUserControl += () =>
            {
                ShareOptionsPopup = false;
                ucMediaShareMoreOptions = null;
                //if (ImageViewInMain != null)
                //{
                //    ImageViewInMain.GrabKeyboardFocus();
                //}
            };
        }

        #region ICommands
        private ICommand downloadBtnCommand;
        public ICommand DownloadBtnCommand
        {
            get
            {
                if (downloadBtnCommand == null) downloadBtnCommand = new RelayCommand(param => OnDownloadBtnCommand());
                return downloadBtnCommand;
            }
        }
        private void OnDownloadBtnCommand()
        {
            try
            {
                if (singleMediaModel.DownloadState == StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE)
                {
                    this.PlayingMedia = singleMediaModel;
                    if (UCDownloadPopup.Instance != null)
                    {
                        if(UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                            UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(UCDownloadPopup.Instance);
                        else UCGuiRingID.Instance.MotherPanel.Children.Remove(UCDownloadPopup.Instance);
                    }
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCDownloadPopup.Instance = new UCDownloadPopup(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                    else UCDownloadPopup.Instance = new UCDownloadPopup(UCGuiRingID.Instance.MotherPanel);
                    UCDownloadPopup.Instance.Show();
                    UCDownloadPopup.Instance.ShowPopup(PlayingMedia, mainPanel, () =>
                    {
                        //MakeDownloaderStart();
                        return 0;
                    });
                    UCDownloadPopup.Instance.OnRemovedUserControl += () =>
                    {
                        UCDownloadPopup.Instance = null;
                    };
                    //MakeDownloaderStart();
                    //if (singleMediaModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                    //{
                    //    //if (DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp == null)
                    //    //    DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp = new UCDownloadOrAddToAlbumPopUp();
                    //    //DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp.PlayingMedia = singleMediaModel;
                    //    //DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp.MakeDownloaderStart();
                    //    this.PlayingMedia = singleMediaModel;
                    //    MakeDownloaderStart();
                    //}
                    //else
                    //{
                    //    //AddOrDownload(true, (Button)sender);
                    //}
                }
                else if (singleMediaModel.DownloadState == StatusConstants.MEDIA_DOWNLOAD_PAUSE_STATE)
                {
                    CustomFileDownloader fileDownloader = null;
                    if (!MediaDataContainer.Instance.CurrentDownloads.TryGetValue(singleMediaModel.ContentId, out fileDownloader))
                    {
                        fileDownloader = new CustomFileDownloader(singleMediaModel);
                        MediaDataContainer.Instance.CurrentDownloads[singleMediaModel.ContentId] = fileDownloader;
                    }
                    if (!RingIDViewModel.Instance.UploadingModelList.Any(p => p.ContentId == singleMediaModel.ContentId))
                    {
                        UploadingModel _uploadingModel = new UploadingModel();
                        _uploadingModel.IsDownload = true;
                        _uploadingModel.ContentId = singleMediaModel.ContentId;
                        _uploadingModel.UploadingContent = singleMediaModel.MediaType == 1 ? "1 audio is downloading..." : "1 video is downloading...";
                        RingIDViewModel.Instance.UploadingModelList.Add(_uploadingModel);
                    }
                    if (!fileDownloader.CanResume)
                    {
                        FileDownloader.FileInfo file = fileDownloader.Files[0];
                        if (System.IO.File.Exists(fileDownloader.LocalDirectory + "\\" + file.Name))
                        {
                            long length = new System.IO.FileInfo(fileDownloader.LocalDirectory + "\\" + file.Name).Length;
                            fileDownloader.Start(length);
                        }
                        else
                        {
                            //CustomMessageBox.ShowError("Paused Media file not found");
                            UIHelperMethods.ShowWarning("Paused Media file not found", "Media not found");
                            //singleMediaModel.DeleteDownloadMedia(singleMediaModel.ContentId);
                            singleMediaModel.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                            singleMediaModel.DownloadProgress = 0;
                            MediaDAO.Instance.DeleteFromDownloadedMediasTable(singleMediaModel.ContentId);
                            //TODO delete downloaded files from PC
                        }
                    }
                    else
                    {
                        fileDownloader.Resume();
                    }
                }
                else if (singleMediaModel.DownloadState == StatusConstants.MEDIA_DOWNLOADING_STATE)
                {
                    //Open popup for Pause/Cancel options
                    MainSwitcher.PopupController.FeedDownloadPauseCancelPopup.Show(singleMediaModel, DownloadBtn);
                }
            }
            catch (System.Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private ICommand shareMediaCommand;
        public ICommand ShareMediaCommand
        {
            get
            {
                if (shareMediaCommand == null) shareMediaCommand = new RelayCommand(param => OnShareMediaCommand());
                return shareMediaCommand;
            }
        }
        public void OnShareMediaCommand()
        {
            int type = 0;
            ShareOptionsPopup = true;
            if (singleMediaModel.MediaOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                type = 1;
            else if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.IsVisible
                && UCMeidaCloudDownloadPopup.Instance != null
                && UCMeidaCloudDownloadPopup.Instance.IsVisible)
                type = 2;
            else if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.IsVisible
                && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.tabContainerBorder.IsVisible
                && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.TabType == 4)
                type = 3;
            else
                type = -1;

            ShowOptionsPopup(type);
        }
        #endregion
    }
}

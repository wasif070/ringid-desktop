﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Utility;
using View.Utility.Stream;
using View.ViewModel;
using System.Linq;
using System;
using Models.Entity;
using System.Windows.Data;
using View.Constants;
using Models.Constants;
using View.Utility.Feed;
using log4net;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Threading.Tasks;
using View.UI.StreamAndChannel;
using View.Utility.Channel;
using System.Diagnostics;
using View.Utility.StreamAndChannel;
using View.Converter;
namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamHomePanel.xaml
    /// </summary>
    public partial class UCStreamHomePanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamHomePanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private string _SearchText = String.Empty;
        private int _CategoryID = 0;
        private bool _IsArrowEnabled = true;
        private ICommand _ShowMoreCommand;
        private ICommand _ArrowClickCommand;
        private Storyboard _SliderStoryboard = null;
        private DispatcherTimer _SliderResizeTimer = null;
        private DispatcherTimer _SliderAnimationTimer = null;
        private DispatcherTimer _WindowResizeTimer = null;
        private DispatcherTimer _ScrollChangeTimer = null;
        private DispatcherTimer _FilterTimer = null;

        private bool _IS_AT_TOP = true;
        private bool _IS_AT_BOTTOM = false;

        #region Constructor

        static UCStreamHomePanel()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamHomePanel()
        {
            InitializeComponent();
            this.DataContext = this;
            this.srvFeatureList.PreviewKeyDown += SliderControl_PreviewKeyDown;
            this.itcFeatureList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamFeatureList"), Source = RingIDSettings });
            this.itcStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList"), Source = RingIDSettings });
        }

        #endregion Constructor

        #region Event Handler

        private static void OnLoadingStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d != null && e.NewValue != null)
                {
                    UCStreamHomePanel panel = ((UCStreamHomePanel)d);
                    int loadingStatus = (int)e.NewValue;

                    switch (loadingStatus)
                    {
                        case StatusConstants.NO_DATA:
                            log.Debug("*********************    NO_DATA                 *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.NO_DATA_AND_LOADING:
                            log.Debug("*********************    NO_DATA_AND_LOADING     *********************");
                            panel.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            break;
                        case StatusConstants.HAS_DATA:
                            log.Debug("*********************    HAS_DATA                *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.HAS_DATA_AND_LOADING:
                            log.Debug("*********************    HAS_DATA_AND_LOADING    *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLoadingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int featureCount = this.itcFeatureList.Items.Count;
            int recentCount = this.itcStreamList.Items.Count;
            int topLimit = featureCount > 0 ? (recentCount < 4 ? 150 : 400) : (recentCount < 4 ? 150 : 300);
            int bottomLimit = recentCount > 5 ? 400 : (recentCount > 3 ? 200 : 140);

            if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
            {
                if (!StreamViewModel.Instance.LoadStatusModel.IsRecentLoading && e.ExtentHeight > e.ViewportHeight)
                {
                    if (e.VerticalChange < 0 && e.VerticalOffset <= topLimit && this._IS_AT_TOP == false)
                    {
                        //Debug.WriteLine("IS_AT_TOP");
                        StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);
                    }
                    else if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_AT_BOTTOM == false)
                    {
                        //Debug.WriteLine("IS_AT_BOTTOM");
                        StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                    }
                }
                this.ChangeScrollOpenStatusOnScroll();
            }

            if (e.ExtentHeight > e.ViewportHeight)
            {
                this._IS_AT_TOP = e.VerticalOffset <= topLimit;
                this._IS_AT_BOTTOM = (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit);
            }
            else
            {
                this._IS_AT_TOP = true;
                this._IS_AT_BOTTOM = false;
            }
        }

        public void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.ChangeScrollOpenStatusOnResize();
        }

        public void SearchBox_TextChanged(object sender, TextChangedEventArgs args)
        {
            this.OnFilterData();
        }

        public void Category_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count > 0)
            {
                this.OnFilterData();
            }
        }

        public void Reload_Click(object sender, RoutedEventArgs e)
        {
            this.OnFilterData();
        }

        private void SliderControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home || e.Key == Key.End || e.Key == Key.Left || e.Key == Key.Right)
            {
                e.Handled = true;
            }
        }

        private void SliderStoryboard_Completed(object sender, EventArgs e)
        {
            if (this._SliderStoryboard != null)
            {
                this._SliderStoryboard.Completed -= SliderStoryboard_Completed;
                this._SliderStoryboard = null;
            }
            this.ChangeSliderOpenStatusOnAnimation();
        }

        private void SliderControl_SizeChanged(object sender, SizeChangedEventArgs args)
        {
            try
            {
                if (sender.Equals(this.itcFeatureList) && Math.Abs(this.itcFeatureList.Margin.Left) >= this.itcFeatureList.ActualWidth)
                {
                    this.OnSliderAnimation(this.itcFeatureList, 0, 0.0D);
                    return;
                }
                this.ChangeSliderOpenStatusOnResize();
            }
            catch (Exception ex)
            {
                log.Error("Error: SliderControl_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_TOP_SCROLL);
            StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);

            this.itcFeatureList.SizeChanged += SliderControl_SizeChanged;
            this.itcStreamList.SizeChanged += ScrlViewer_SizeChanged;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.ChangeSliderOpenStatus();
                this.ChangeScrollOpenStatus();
            }, DispatcherPriority.ApplicationIdle);

            MultiBinding loadingStatusBinding = new MultiBinding { Converter = new LoadingStatusConverter() };
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamFeatureList.Count"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList.Count"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsFeaturedLoading"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsRecentLoading"), Source = RingIDSettings });
            this.SetBinding(UCStreamHomePanel.LoadingStatusProperty, loadingStatusBinding);
        }

        public void ReleaseViewer()
        {
            if (this._FilterTimer != null) this._FilterTimer.Stop();
            if (this._SliderResizeTimer != null) this._SliderResizeTimer.Stop();
            if (this._SliderAnimationTimer != null) this._SliderAnimationTimer.Stop();
            if (this._WindowResizeTimer != null) this._WindowResizeTimer.Stop();
            if (this._ScrollChangeTimer != null) this._ScrollChangeTimer.Stop();

            this.itcFeatureList.SizeChanged -= SliderControl_SizeChanged;
            this.itcStreamList.SizeChanged -= ScrlViewer_SizeChanged;
            this.ClearValue(UCStreamHomePanel.LoadingStatusProperty);

            Task.Factory.StartNew(() =>
            {
                StreamViewModel.Instance.StreamFeatureList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                StreamViewModel.Instance.StreamRecentLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
            });

            this.IsArrowEnabled = true;
            this._IS_AT_TOP = true;
            this._IS_AT_BOTTOM = false;
        }

        private void ResetDataListByFilter()
        {
            this.itcFeatureList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcStreamList.ClearValue(ItemsControl.ItemsSourceProperty);
            StreamViewModel.Instance.StreamFeatureList.Clear();
            StreamViewModel.Instance.StreamRecentLiveList.Clear();
            HelperMethods.RemoveFromFeaturedStreamAndChannelList(SettingsConstants.TYPE_STREAM);
            OnSliderAnimation(itcFeatureList, 0, 0);
            this.itcFeatureList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamFeatureList"), Source = RingIDSettings });
            this.itcStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList"), Source = RingIDSettings });
        }

        private void OnSliderArrowClick(object param)
        {
            try
            {
                int type = Int32.Parse(param.ToString());
                bool isNext = type % 2 == 0;

                switch (type)
                {
                    case 1:
                    case 2:
                        {
                            double targetValue = 0.0D;
                            if (isNext)
                            {
                                double remaining = this.itcFeatureList.ActualWidth - Math.Abs(this.itcFeatureList.Margin.Left) + this.srvFeatureList.ActualWidth;
                                if (remaining < this.srvFeatureList.ActualWidth)
                                {
                                    targetValue = this.itcFeatureList.Margin.Left - remaining;
                                    StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                }
                                else
                                {
                                    targetValue = this.itcFeatureList.Margin.Left - this.srvFeatureList.ActualWidth;
                                    if (Math.Abs(targetValue) + this.srvFeatureList.ActualWidth >= this.itcFeatureList.ActualWidth)
                                    {
                                        StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                    }
                                }
                            }
                            else
                            {
                                if (Math.Abs(this.itcFeatureList.Margin.Left) <= this.srvFeatureList.ActualWidth)
                                {
                                    targetValue = 0;
                                    StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_TOP_SCROLL);
                                }
                                else
                                {
                                    targetValue = this.itcFeatureList.Margin.Left + this.srvFeatureList.ActualWidth;

                                }
                            }
                            OnSliderAnimation(this.itcFeatureList, targetValue, 0.35D);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSliderArrowClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnSliderAnimation(FrameworkElement targetElement, double targetValue, double delay)
        {
            try
            {
                this.IsArrowEnabled = false;
                double currentValue = targetElement.Margin.Left;
                ThicknessAnimation marginAnimation = new ThicknessAnimation();
                marginAnimation.From = new Thickness(currentValue, 0, 0, 0);
                marginAnimation.To = new Thickness(targetValue, 0, 0, 0);
                marginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                this._SliderStoryboard = new Storyboard();
                this._SliderStoryboard.Children.Add(marginAnimation);
                Storyboard.SetTarget(marginAnimation, targetElement);
                Storyboard.SetTargetProperty(marginAnimation, new PropertyPath(ItemsControl.MarginProperty));
                this._SliderStoryboard.Completed += this.SliderStoryboard_Completed;
                this._SliderStoryboard.Begin(targetElement);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSliderAnimation() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeSliderOpenStatusOnResize()
        {
            try
            {
                if (this._SliderResizeTimer == null)
                {
                    this._SliderResizeTimer = new DispatcherTimer();
                    this._SliderResizeTimer.Interval = TimeSpan.FromMilliseconds(400);
                    this._SliderResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeSliderOpenStatus();
                        this._SliderResizeTimer.Stop();
                    };
                }
                this._SliderResizeTimer.Stop();
                if (this._SliderAnimationTimer == null || this._SliderAnimationTimer.IsEnabled == false)
                {
                    this._SliderResizeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeSliderOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeSliderOpenStatusOnAnimation()
        {
            try
            {
                if (this._SliderAnimationTimer == null)
                {
                    this._SliderAnimationTimer = new DispatcherTimer();
                    this._SliderAnimationTimer.Interval = TimeSpan.FromMilliseconds(100);
                    this._SliderAnimationTimer.Tick += (o, e) =>
                    {
                        this.ChangeSliderOpenStatus();
                        this._SliderAnimationTimer.Stop();
                        this.IsArrowEnabled = true;
                    };
                }
                if (this._SliderResizeTimer != null && this._SliderResizeTimer.IsEnabled)
                {
                    this._SliderResizeTimer.Stop();
                }
                this._SliderAnimationTimer.Stop();
                this._SliderAnimationTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeSliderOpenStatusOnAnimation() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeSliderOpenStatus()
        {
            try
            {
                int featureIndex = (int)(Math.Abs(itcFeatureList.Margin.Left) / 155);
                if (featureIndex < this.itcFeatureList.Items.Count)
                {
                    int firstIndex = featureIndex - 8 < 0 ? 0 : featureIndex - 8;
                    int read = featureIndex > 8 ? 0 : 8 - featureIndex;
                    for (int idx = firstIndex; idx < this.itcFeatureList.Items.Count && read < 20; idx++, read++)
                    {
                        if (idx >= (featureIndex - 4) && idx < (featureIndex + 4 + 4))
                        {
                            ((StreamModel)this.itcFeatureList.Items[idx]).IsViewOpened = true;
                        }
                        else
                        {
                            ((StreamModel)this.itcFeatureList.Items[idx]).IsViewOpened = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeSliderOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnResize()
        {
            try
            {
                if (this._WindowResizeTimer == null)
                {
                    this._WindowResizeTimer = new DispatcherTimer();
                    this._WindowResizeTimer.Interval = TimeSpan.FromMilliseconds(350);
                    this._WindowResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._WindowResizeTimer.Stop();
                    };
                }

                if (this._ScrollChangeTimer != null && this._ScrollChangeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Stop();
                }
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnScroll()
        {
            try
            {
                if (this._ScrollChangeTimer == null)
                {
                    this._ScrollChangeTimer = new DispatcherTimer();
                    this._ScrollChangeTimer.Interval = TimeSpan.FromMilliseconds(30);
                    this._ScrollChangeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._ScrollChangeTimer.Stop();
                    };
                }
                this._ScrollChangeTimer.Stop();
                if (this._WindowResizeTimer == null || !this._WindowResizeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnScroll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatus()
        {
            try
            {
                UCStreamAndChannelMainPanel parent = HelperMethods.FindVisualParent<UCStreamAndChannelMainPanel>(this);
                if (parent != null)
                {
                    int paddingTop = 45 + 45 + 8 + (itcFeatureList.Items.Count > 0 ? 25 + 145 : 0) + 5;
                    StreamHelpers.ChangeStreamViewPortOpenedProperty(parent.ScrlViewer, itcStreamList, paddingTop);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnFilterData()
        {
            try
            {
                this.ResetDataListByFilter();

                if (this._FilterTimer == null)
                {
                    this._FilterTimer = new DispatcherTimer();
                    this._FilterTimer.Interval = TimeSpan.FromMilliseconds(800);
                    this._FilterTimer.Tick += (o, e) =>
                    {
                        StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_TOP_SCROLL);
                        StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);
                        this._FilterTimer.Stop();
                    };
                }

                this._FilterTimer.Stop();
                this._FilterTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnFilterData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnShowMoreClick(object param)
        {
            try
            {
                if (param == null) return;

                ParamModel paramModel = new ParamModel();
                paramModel.Title = "Featured Lives";
                paramModel.ActionType = (int)param;
                paramModel.Param = null;
                paramModel.PrevParam = null;
                paramModel.PrevViewType = StreamAndChannelConstants.TypeStreamAndChannelMainPanel;
                StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel, paramModel);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnShowMoreClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {

        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty LoadingStatusProperty = DependencyProperty.Register("LoadingStatus", typeof(int), typeof(UCStreamHomePanel), new PropertyMetadata(StatusConstants.NO_DATA, OnLoadingStatusChanged));

        public int LoadingStatus
        {
            get { return (int)GetValue(LoadingStatusProperty); }
            set
            {
                SetValue(LoadingStatusProperty, value);
            }
        }

        public ICommand ShowMoreCommand
        {
            get
            {
                if (_ShowMoreCommand == null)
                {
                    _ShowMoreCommand = new RelayCommand((param) => OnShowMoreClick(param));
                }
                return _ShowMoreCommand;
            }
        }

        public ICommand ArrowClickCommand
        {
            get
            {
                if (_ArrowClickCommand == null)
                {
                    _ArrowClickCommand = new RelayCommand((param) => OnSliderArrowClick(param));
                }
                return _ArrowClickCommand;
            }
        }

        public bool IsArrowEnabled
        {
            get { return _IsArrowEnabled; }
            set
            {
                if (_IsArrowEnabled == value)
                    return;

                _IsArrowEnabled = value;
                this.OnPropertyChanged("IsArrowEnabled");
            }
        }

        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                if (_SearchText == value)
                    return;

                _SearchText = value;
                this.OnPropertyChanged("SearchText");
            }
        }

        public int CategoryID
        {
            get { return _CategoryID; }
            set
            {
                if (_CategoryID == value)
                    return;

                _CategoryID = value;
                this.OnPropertyChanged("CategoryID");
            }
        }

        #endregion Property

    }

}

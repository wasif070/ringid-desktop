﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility;
using View.Utility.Stream;
using View.Utility.WPFMessageBox;
using System.Management;
using System.Windows.Media.Animation;
using View.Utility.Stream.Utils;
using View.ViewModel;
using System.Windows.Interop;
using View.UI.StreamAndChannel;
using View.Utility.Call;
using callsdkwrapper;
using View.Utility.Chat.Service;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamLiveNowViewer.xaml
    /// </summary>
    public partial class UCStreamLiveSourceViewer : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamLiveSourceViewer).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;
        private Func<int> _OnClose = null;

        private bool _IS_INITIALIZED = false;
        private ICommand _CancelCommand;
        private ICommand _GoLiveCommand;
        private ICommand _ChangeSourceCommand;
        private ICommand _ShowAppsCommand;
        private ICommand _SelectAppCommand;
        private ICommand _ArrowClickCommand;
        private bool _IsPopupView = false;
        private bool _IsSourceOpened = false;
        private bool _IsAppListOpened = false;
        private int _SelectedAppHandler = 0;
        private bool _IsLeftArrowVisible = false;
        private bool _IsRightArrowVisible = false;
        private bool _IsArrowEnabled = true;
        private Storyboard _AppListStoryboard = null;

        private ObservableDictionary<int, AppInfoModel> _AppList = new ObservableDictionary<int, AppInfoModel>();

        #region Constructor

        static UCStreamLiveSourceViewer()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamLiveSourceViewer()
        {
            InitializeComponent();
            this.DataContext = null;
            this.IsPopupView = false;
        }

        public UCStreamLiveSourceViewer(Func<int> onClose)
        {
            InitializeComponent();
            this.DataContext = null;
            this.IsPopupView = true;
            this._OnClose = onClose;
        }

        #endregion Event Handler

        #region Event Handler

        private void AppListStoryboard_Completed(object sender, EventArgs e)
        {
            if (this._AppListStoryboard != null)
            {
                this._AppListStoryboard.Completed -= AppListStoryboard_Completed;
                this._AppListStoryboard.Stop();
                this._AppListStoryboard = null;
            }
            this.IsArrowEnabled = true;
        }

        private void ScrollViewer_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home || e.Key == Key.End || e.Key == Key.Left || e.Key == Key.Right)
            {
                e.Handled = true;
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            if (this._IS_INITIALIZED == false)
            {
                this._IS_INITIALIZED = true;
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this.DataContext = this;
                    this.imgStreamPreview.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRenderModel.RenderSource"), Source = RingIDSettings });

                    if (this.IsPopupView)
                    {
                        if (StreamViewModel.Instance.VideoSourceType == StreamConstants.APP_SCREEN)
                        {
                            OnShowAppsClick(StreamViewModel.Instance.StreamingChannel.DeviceInfo);
                        }
                        this.IsSourceOpened = true;
                    }
                    else
                    {
                        StreamViewModel.Instance.StreamInfoModel.StartTime = 0;
                        Guid streamId = StreamViewModel.Instance.StreamInfoModel.StreamID;
                        new ThrdInitializeLiveStream(streamId, (serverDTO) =>
                        {
                            if (this._IS_INITIALIZED && StreamViewModel.Instance.StreamInfoModel != null && streamId.Equals(StreamViewModel.Instance.StreamInfoModel.StreamID) && serverDTO != null && !String.IsNullOrWhiteSpace(serverDTO.ServerIP) && serverDTO.RegisterPort > 0)
                            {
                                StreamViewModel.Instance.StreamInfoModel.StartTime = ChatService.GetServerTime();
                                StreamViewModel.Instance.StreamInfoModel.PublisherServerIP = serverDTO.ServerIP;
                                StreamViewModel.Instance.StreamInfoModel.PublisherServerPort = serverDTO.RegisterPort;
#if CHAT_LOG
                                log.Debug("AUTH::STREAM::RESPONSE".PadRight(58, ' ') + "==>   publisherID = " + StreamViewModel.Instance.StreamInfoModel.UserTableID + ", streamID = " + StreamViewModel.Instance.StreamInfoModel.StreamID + ", userName = " + StreamViewModel.Instance.StreamInfoModel.UserName + ", startTime = " + StreamViewModel.Instance.StreamInfoModel.StartTime + ", endTime = " + StreamViewModel.Instance.StreamInfoModel.EndTime + ", publisherServerIP = " + StreamViewModel.Instance.StreamInfoModel.PublisherServerIP + ", publisherServerPort = " + StreamViewModel.Instance.StreamInfoModel.PublisherServerPort);
#endif
                                CallHelperMethods.PublisherRegister(
                                    StreamViewModel.Instance.StreamInfoModel.StreamID.ToString(),
                                    StreamViewModel.Instance.StreamInfoModel.PublisherServerIP,
                                    StreamViewModel.Instance.StreamInfoModel.PublisherServerPort,
                                    RegType.LiveStream);
                            }
                            return 0;
                        }).Start();
                    }
                }, DispatcherPriority.ApplicationIdle);
            }
        }

        private void OnCancelClick(object param)
        {
            if (this.IsPopupView)
            {
                if (this._OnClose != null)
                {
                    this._OnClose();
                }
            }
            else
            {
                UCStreamAndChannelViewer.Instance.Dispose();
            }
        }

        private void OnGoLiveClick(object param)
        {
            if (StreamViewModel.Instance.VideoSourceType > 0)
            {
                UCStreamLiveWrapper ucStreamLiveWrapper = StreamHelpers.GetCurruentStreamLiveWrapper();
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    ucStreamLiveWrapper.OpenLiveNowViewer();
                }, DispatcherPriority.ApplicationIdle);
            }
            else
            {
                UIHelperMethods.ShowWarning("You must have to select a media to go live", "Go live with");
                //CustomMessageBox.ShowWarning("You must have to select a media to go live", "Go Live With");
            }
        }

        private void OnChangeSourceClick(object param)
        {
            try
            {
                int sourceType = (int)param;
                this.IsAppListOpened = false;
                this.SelectedAppHandler = 0;
                this.ScrollViewer.PreviewKeyDown -= ScrollViewer_PreviewKeyDown;
                this.itcAppList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                this.itcAppList.ItemsSource = null;
                this.AppList.Clear();
                this.OnAppListAnimation(0, 0);

                StreamViewModel.Instance.InitStreamingSourceInfo(StreamConstants.MICROPHONE_SPEAKER_MIX, sourceType, null);
                this.IsSourceOpened = StreamViewModel.Instance.SetStreamingChannelSource();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnChangeSourceClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnShowAppsClick(object param)
        {
            try
            {
                string prevDeviceKey = "0";
                if (param != null && param is VideoDeviceInfo)
                {
                    prevDeviceKey = ((VideoDeviceInfo)param).Key;
                }

                this.IsSourceOpened = false;
                this.ScrollViewer.PreviewKeyDown += ScrollViewer_PreviewKeyDown;
                this.itcAppList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
                this.itcAppList.ItemsSource = null;
                this.AppList.Clear();
                this.OnAppListAnimation(0, 0);
                this.IsAppListOpened = true;

                IntPtr currentHandler = new WindowInteropHelper(Application.Current.MainWindow).EnsureHandle();
                Process[] processlist = Process.GetProcesses().Where(p => p.MainWindowHandle != IntPtr.Zero && p.MainWindowHandle != currentHandler && p.ProcessName != "explorer").ToArray();
                int count = 0;
                foreach (Process process in processlist)
                {
                    int hdw = process.MainWindowHandle.ToInt32();
                    if (hdw > 0 && !String.IsNullOrEmpty(process.MainWindowTitle))
                    {
                        AppInfoModel model = new AppInfoModel();
                        model.AppHandler = hdw;
                        model.AppName = process.ProcessName;
                        model.AppTitle = process.MainWindowTitle;
                        model.AppCaption = model.AppName + ": " + model.AppTitle;

                        //Application.Current.Dispatcher.BeginInvoke(() =>
                        //{
                        this.AppList[process.MainWindowHandle.ToInt32()] = model;
                        //}, DispatcherPriority.Send);

                        if (model.AppHandler.ToString().Equals(prevDeviceKey))
                        {
                            this.SelectedAppHandler = model.AppHandler;
                        }
                        count++;
                    }
                }

                this.itcAppList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("AppList") });
                this.IsLeftArrowVisible = false;
                this.IsRightArrowVisible = 0 > -((((double)count / 6) - 1) * ScrollViewerContainer.Width);

                if (this.SelectedAppHandler == 0 && this.AppList.Count > 0)
                {
                    this.OnSelectAppClick(this.AppList.FirstOrDefault().Value);
                }

            }
            catch (Exception ex)
            {
                log.Error("Error: OnShowAppsClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnSelectAppClick(object param)
        {
            try
            {
                AppInfoModel appInfo = (AppInfoModel)param;
                this.SelectedAppHandler = appInfo.AppHandler;
                StreamViewModel.Instance.InitStreamingSourceInfo(StreamConstants.MICROPHONE_SPEAKER_MIX, StreamConstants.APP_SCREEN, appInfo);

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this.IsSourceOpened = StreamViewModel.Instance.SetStreamingChannelSource();
                }, DispatcherPriority.ApplicationIdle);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSelectAppClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnArrowClick(object param)
        {
            try
            {
                if ((bool)param)
                {
                    this.OnAppListAnimation(this.itcAppList.Margin.Left + ScrollViewerContainer.Width, 0.35);
                }
                else
                {
                    this.OnAppListAnimation(this.itcAppList.Margin.Left - ScrollViewerContainer.Width, 0.35);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnArrowClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnAppListAnimation(double targetValue, double delay)
        {
            try
            {
                this.IsArrowEnabled = false;
                ThicknessAnimation marginAnimation = new ThicknessAnimation();
                marginAnimation.From = new Thickness(this.itcAppList.Margin.Left, 0, 0, 0);
                marginAnimation.To = new Thickness(targetValue, 0, 0, 0);
                marginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                this._AppListStoryboard = new Storyboard();
                this._AppListStoryboard.Children.Add(marginAnimation);
                Storyboard.SetTarget(marginAnimation, this.itcAppList);
                Storyboard.SetTargetProperty(marginAnimation, new PropertyPath(ItemsControl.MarginProperty));
                this._AppListStoryboard.Completed += this.AppListStoryboard_Completed;
                this._AppListStoryboard.Begin(this.itcAppList);

                this.IsLeftArrowVisible = targetValue < 0;
                this.IsRightArrowVisible = targetValue > -((((double)this.AppList.Count / 6) - 1) * ScrollViewerContainer.Width);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnArrowClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this._IS_INITIALIZED = false;
            this.IsSourceOpened = false;
            this.imgStreamPreview.ClearValue(Image.SourceProperty);
            this.imgStreamPreview.Source = null;
            if (this._AppListStoryboard != null)
            {
                this._AppListStoryboard.Completed -= AppListStoryboard_Completed;
                this._AppListStoryboard.Stop();
                this._AppListStoryboard = null;
            }
            this.IsLeftArrowVisible = false;
            this.IsRightArrowVisible = false;
            this.ScrollViewer.PreviewKeyDown -= ScrollViewer_PreviewKeyDown;
            this.itcAppList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
            this.itcAppList.ItemsSource = null;
            this.AppList.Clear();
            this.DataContext = null;
            this.IsPopupView = false;
            this.IsSourceOpened = false;
            this.IsAppListOpened = false;
            this.SelectedAppHandler = 0;
            this._OnClose = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand((param) => OnCancelClick(param));
                }
                return _CancelCommand;
            }
        }

        public ICommand GoLiveCommand
        {
            get
            {
                if (_GoLiveCommand == null)
                {
                    _GoLiveCommand = new RelayCommand((param) => OnGoLiveClick(param));
                }
                return _GoLiveCommand;
            }
        }

        public ICommand ChangeSourceCommand
        {
            get
            {
                if (_ChangeSourceCommand == null)
                {
                    _ChangeSourceCommand = new RelayCommand((param) => OnChangeSourceClick(param));
                }
                return _ChangeSourceCommand;
            }
        }

        public ICommand ShowAppsCommand
        {
            get
            {
                if (_ShowAppsCommand == null)
                {
                    _ShowAppsCommand = new RelayCommand((param) => OnShowAppsClick(param));
                }
                return _ShowAppsCommand;
            }
        }

        public ICommand SelectAppCommand
        {
            get
            {
                if (_SelectAppCommand == null)
                {
                    _SelectAppCommand = new RelayCommand((param) => OnSelectAppClick(param));
                }
                return _SelectAppCommand;
            }
        }

        public ICommand ArrowClickCommand
        {
            get
            {
                if (_ArrowClickCommand == null)
                {
                    _ArrowClickCommand = new RelayCommand((param) => OnArrowClick(param));
                }
                return _ArrowClickCommand;
            }
        }

        public ObservableDictionary<int, AppInfoModel> AppList
        {
            get { return _AppList; }
            set { _AppList = value; }
        }

        public bool IsPopupView
        {
            get { return _IsPopupView; }
            set
            {
                if (_IsPopupView == value)
                    return;

                _IsPopupView = value;
                this.OnPropertyChanged("IsPopupView");
            }
        }

        public bool IsSourceOpened
        {
            get { return _IsSourceOpened; }
            set
            {
                if (_IsSourceOpened == value)
                    return;

                _IsSourceOpened = value;
                this.OnPropertyChanged("IsSourceOpened");
            }
        }

        public int SelectedAppHandler
        {
            get { return _SelectedAppHandler; }
            set
            {
                if (_SelectedAppHandler == value)
                    return;

                _SelectedAppHandler = value;
                this.OnPropertyChanged("SelectedAppHandler");
            }
        }

        public bool IsAppListOpened
        {
            get { return _IsAppListOpened; }
            set
            {
                if (_IsAppListOpened == value)
                    return;

                _IsAppListOpened = value;
                this.OnPropertyChanged("IsAppListOpened");
            }
        }

        public bool IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                if (_IsLeftArrowVisible == value)
                    return;

                _IsLeftArrowVisible = value;
                this.OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        public bool IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                if (_IsRightArrowVisible == value)
                    return;

                _IsRightArrowVisible = value;
                this.OnPropertyChanged("IsRightArrowVisible");
            }
        }

        public bool IsArrowEnabled
        {
            get { return _IsArrowEnabled; }
            set
            {
                if (_IsArrowEnabled == value)
                    return;

                _IsArrowEnabled = value;
                this.OnPropertyChanged("IsArrowEnabled");
            }
        }

        #endregion Property
    }

}

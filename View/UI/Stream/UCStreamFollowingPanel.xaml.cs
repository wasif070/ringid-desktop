﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.Constants;
using View.Converter;
using View.Utility;
using View.Utility.Stream;
using View.ViewModel;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamFollowingPanel.xaml
    /// </summary>
    public partial class UCStreamFollowingPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamFollowingPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private ICommand _BackCommand;

        #region Constructor

        static UCStreamFollowingPanel()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamFollowingPanel()
        {
            InitializeComponent();
            this.DataContext = null;
        }

        #endregion Constructor

        #region Event Handler

        private static void OnLoadingStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d != null && e.NewValue != null)
                {
                    UCStreamFollowingPanel panel = ((UCStreamFollowingPanel)d);
                    int loadingStatus = (int)e.NewValue;

                    switch (loadingStatus)
                    {
                        case StatusConstants.NO_DATA:
                            log.Debug("*********************    NO_DATA                 *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.NO_DATA_AND_LOADING:
                            log.Debug("*********************    NO_DATA_AND_LOADING     *********************");
                            panel.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            break;
                        case StatusConstants.HAS_DATA:
                            log.Debug("*********************    HAS_DATA                *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.HAS_DATA_AND_LOADING:
                            log.Debug("*********************    HAS_DATA_AND_LOADING    *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLoadingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            StreamHelpers.LoadFollowingStreamList(StreamConstants.STREAM_TOP_SCROLL);
            StreamHelpers.LoadCategoryWiseStreamCount();

            this.DataContext = this;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.itcFollowingList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamFollowingList"), Source = RingIDSettings });
                this.itcCategoryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamCountByCategoryList"), Source = RingIDSettings });
            });

            MultiBinding loadingStatusBinding = new MultiBinding { Converter = new LoadingStatusConverter() };
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamFollowingList.Count"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamCountByCategoryList.Count"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsFollowingLoading"), Source = RingIDSettings });
            loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsCategoryWiseCountLoading"), Source = RingIDSettings });
            this.SetBinding(UCStreamFollowingPanel.LoadingStatusProperty, loadingStatusBinding);
        }

        public void ReleaseViewer()
        {
            
        }

        private void OnBackClick(object param)
        {
            try
            {
                RingIDViewModel.Instance.OnStreamAndChannelCommand(true);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnBackClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.ClearValue(UCStreamFollowingPanel.LoadingStatusProperty);

            Task.Factory.StartNew(() =>
            {
                lock (StreamHelpers._PROCESS_SYNC_LOCK)
                {
                    StreamViewModel.Instance.StreamFollowingList.ToList().ForEach(P => P.IsViewOpened = false);
                    StreamViewModel.Instance.StreamCountByCategoryList.ToList().ForEach(P => { P.FollowingCount = 0; P.IsExpanded = false; });
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        StreamViewModel.Instance.StreamFollowingList.Clear();
                        StreamViewModel.Instance.StreamCountByCategoryList.Clear();
                    }, DispatcherPriority.Send);
                }
            });

            this.itcFollowingList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcFollowingList.ItemsSource = null;
            this.itcCategoryList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcCategoryList.ItemsSource = null;
            this.DataContext = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty LoadingStatusProperty = DependencyProperty.Register("LoadingStatus", typeof(int), typeof(UCStreamFollowingPanel), new PropertyMetadata(StatusConstants.NO_DATA, OnLoadingStatusChanged));

        public int LoadingStatus
        {
            get { return (int)GetValue(LoadingStatusProperty); }
            set
            {
                SetValue(LoadingStatusProperty, value);
            }
        }

        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand((param) => OnBackClick(param));
                }
                return _BackCommand;
            }
        }

        #endregion Property
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Converter;
using View.UI.StreamAndChannel;
using View.Utility;
using View.Utility.Stream;
using View.Utility.StreamAndChannel;
using View.ViewModel;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamDiscoveryPanel.xaml
    /// </summary>
    public partial class UCStreamDiscoveryPanel : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamDiscoveryPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private ObservableCollection<CountryCodeModel> _CountryList = new ObservableCollection<CountryCodeModel>();

        private string _PrevSearchText = null;
        private bool _IsArrowEnabled = true;
        private bool _IsSearchMode = false;
        private bool _IsCountrySelectMode = false;
        private ICommand _ShowMoreCommand;
        private ICommand _ClearSearchCommand;
        private ICommand _ArrowClickCommand;
        private ICommand _BackCommand;
        private Storyboard _SliderStoryboard = null;
        private DispatcherTimer _SliderResizeTimer = null;
        private DispatcherTimer _SliderAnimationTimer = null;
        private DispatcherTimer _WindowResizeTimer = null;
        private DispatcherTimer _ScrollChangeTimer = null;
        private DispatcherTimer _FilterTimer = null;

        private bool _IS_AT_TOP = true;
        private bool _IS_AT_BOTTOM = false;

        private UCStreamAndChannelCountryListPanel _CountryListPanel = null;

        #region Constructor

        static UCStreamDiscoveryPanel()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamDiscoveryPanel()
        {
            InitializeComponent();
            CountryList.Add(new CountryCodeModel("Canada", "+1"));
            CountryList.Add(new CountryCodeModel("United States", "+1"));
            CountryList.Add(new CountryCodeModel("United Kingdom", "+44"));
            CountryList.Add(new CountryCodeModel("Bangladesh", "+880"));
            CountryList.Add(new CountryCodeModel("India", "+91"));
            CountryList.Add(new CountryCodeModel("Pakistan", "+92"));
            CountryList.Add(new CountryCodeModel("Australia", "+61"));
            CountryList.Add(new CountryCodeModel("Iran", "+98"));
            this.DataContext = null;
        }

        #endregion Constructor

        #region Event Handler

        private static void OnLoadingStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (d != null && e.NewValue != null)
                {
                    UCStreamDiscoveryPanel panel = ((UCStreamDiscoveryPanel)d);
                    int loadingStatus = (int)e.NewValue;

                    switch (loadingStatus)
                    {
                        case StatusConstants.NO_DATA:
                            log.Debug("*********************    NO_DATA                 *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.NO_DATA_AND_LOADING:
                            log.Debug("*********************    NO_DATA_AND_LOADING     *********************");
                            panel.InitGIFCtrl.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_FEED_CYCLE));
                            break;
                        case StatusConstants.HAS_DATA:
                            log.Debug("*********************    HAS_DATA                *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                        case StatusConstants.HAS_DATA_AND_LOADING:
                            log.Debug("*********************    HAS_DATA_AND_LOADING    *********************");
                            if (panel.InitGIFCtrl != null && panel.InitGIFCtrl.IsRunning())
                            {
                                panel.InitGIFCtrl.StopAnimate();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLoadingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ScrlViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            int topLimit = 0;
            int bottomLimit = 0;

            if (this.IsSearchMode)
            {
                int seachCount = this.itcSearchStreamList.Items.Count;
                topLimit = seachCount > 5 ? 300 : 150;
                bottomLimit = seachCount > 5 ? 400 : 100;

                if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
                {
                    if (!StreamViewModel.Instance.LoadStatusModel.IsSearchLoading && e.ExtentHeight > e.ViewportHeight)
                    {
                        if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_AT_BOTTOM == false)
                        {
                            Debug.WriteLine("IS_AT_BOTTOM");
                            StreamHelpers.LoadSearchStreamList(StreamConstants.STREAM_BOTTOM_SCROLL, TxtSearch.Text, null, null);
                        }
                    }
                    this.ChangeScrollOpenStatusOnScroll();
                }
            }
            else
            {
                int mostViewedCount = this.itcMostViewedStreamList.Items.Count;
                topLimit = (51 + (int)bdrMostViewLiveList.ActualHeight + (int)bdrNearByLiveList.ActualHeight + (int)bdrCountryList.ActualHeight + (int)bdrCategoryList.ActualHeight + (mostViewedCount > 0 ? 50 : 0) + (155 /* Extra */)) / 2;
                bottomLimit = (topLimit > 200 && mostViewedCount > 16) || mostViewedCount > 20 ? 200 : 70;

                if (e.ExtentHeightChange == 0 && e.ViewportHeightChange == 0)
                {
                    if (!StreamViewModel.Instance.LoadStatusModel.IsMostViewLoading && e.ExtentHeight > e.ViewportHeight)
                    {
                        if (e.VerticalChange < 0 && e.VerticalOffset <= topLimit && this._IS_AT_TOP == false)
                        {
                            Debug.WriteLine("IS_AT_TOP");
                            StreamHelpers.LoadMostViewStreamList(true, 12);
                        }
                        else if (e.VerticalChange > 0 && (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit) && this._IS_AT_BOTTOM == false)
                        {
                            Debug.WriteLine("IS_AT_BOTTOM");
                            StreamHelpers.LoadMostViewStreamList(false, 12);
                        }
                    }
                    this.ChangeScrollOpenStatusOnScroll();
                }
            }

            if (e.ExtentHeight > e.ViewportHeight)
            {
                this._IS_AT_TOP = e.VerticalOffset <= topLimit;
                this._IS_AT_BOTTOM = (e.ExtentHeight - e.VerticalOffset) <= (e.ViewportHeight + bottomLimit);
            }
            else
            {
                this._IS_AT_TOP = true;
                this._IS_AT_BOTTOM = false;
            }
        }

        private void ScrlViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.ChangeScrollOpenStatusOnResize();
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs args)
        {
            try
            {
                this.IsSearchMode = TxtSearch.Text.Trim().Length > 0;
                this.ResetDataOnSearching(!this.IsSearchMode || String.IsNullOrWhiteSpace(this._PrevSearchText) || !this._PrevSearchText.ToLower().Contains(TxtSearch.Text.ToLower()));

                if (this._FilterTimer == null)
                {
                    this._FilterTimer = new DispatcherTimer();
                    this._FilterTimer.Interval = TimeSpan.FromMilliseconds(800);
                    this._FilterTimer.Tick += (o, e) =>
                    {
                        this._PrevSearchText = TxtSearch.Text;
                        this.ResetDataOnSearchComplete();
                        this._FilterTimer.Stop();
                    };
                }

                this._FilterTimer.Stop();
                this._FilterTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: SearchBox_TextChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SliderControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home || e.Key == Key.End || e.Key == Key.Left || e.Key == Key.Right)
            {
                e.Handled = true;
            }
        }

        private void SliderStoryboard_Completed(object sender, EventArgs e)
        {
            if (this._SliderStoryboard != null)
            {
                this._SliderStoryboard.Completed -= SliderStoryboard_Completed;
                this._SliderStoryboard = null;
            }
            this.ChangeSliderOpenStatusOnAnimation();
        }

        private void SliderControl_SizeChanged(object sender, SizeChangedEventArgs args)
        {
            try
            {
                if (sender.Equals(this.itcRecentStreamList) && Math.Abs(this.itcRecentStreamList.Margin.Left) >= this.itcRecentStreamList.ActualWidth)
                {
                    this.OnSliderAnimation(this.itcRecentStreamList, 0, 0.0D);
                    return;
                }
                if (sender.Equals(this.itcNearbyStreamList) && Math.Abs(this.itcNearbyStreamList.Margin.Left) >= this.itcNearbyStreamList.ActualWidth)
                {
                    this.OnSliderAnimation(this.itcNearbyStreamList, 0, 0.0D);
                    return;
                }
                this.ChangeSliderOpenStatusOnResize();
            }
            catch (Exception ex)
            {
                log.Error("Error: SliderControl_SizeChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer(object param)
        {
            StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);
            StreamHelpers.LoadNearByStreamList(StreamConstants.STREAM_TOP_SCROLL);
            StreamHelpers.LoadMostViewStreamList(true, 16);

            this.DataContext = this;
            this.TxtSearch.TextChanged += SearchBox_TextChanged;
            this.itcRecentStreamList.SizeChanged += SliderControl_SizeChanged;
            this.itcRecentStreamList.PreviewKeyDown += SliderControl_PreviewKeyDown;
            this.itcNearbyStreamList.SizeChanged += SliderControl_SizeChanged;
            this.itcNearbyStreamList.PreviewKeyDown += SliderControl_PreviewKeyDown;
            this.itcMostViewedStreamList.SizeChanged += ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged += ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged += ScrlViewer_SizeChanged;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.itcCountryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("CountryList") });
                this.itcCategoryList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamCategoryList"), Source = RingIDSettings });
                this.itcRecentStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRecentLiveList"), Source = RingIDSettings });
                this.itcNearbyStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamNearByLiveList"), Source = RingIDSettings });
                this.itcMostViewedStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamMostViewLiveList"), Source = RingIDSettings });

                if (param != null && param is ExpandoObject)
                {
                    this.ShowCountrySelectPreview(param);
                }
                else
                {
                    this.ChangeSliderOpenStatus();
                    this.ChangeScrollOpenStatus();
                }
            });
        }

        private void ResetDataOnSearching(bool doClear)
        {
            this.itcRecentStreamList.SizeChanged -= SliderControl_SizeChanged;
            this.itcRecentStreamList.PreviewKeyDown -= SliderControl_PreviewKeyDown;
            this.itcNearbyStreamList.SizeChanged -= SliderControl_SizeChanged;
            this.itcNearbyStreamList.PreviewKeyDown -= SliderControl_PreviewKeyDown;
            this.itcMostViewedStreamList.SizeChanged -= ScrlViewer_SizeChanged;
            this.itcSearchStreamList.SizeChanged -= ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged -= ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged -= ScrlViewer_SizeChanged;

            Task.Factory.StartNew(() =>
            {
                StreamViewModel.Instance.StreamRecentLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                StreamViewModel.Instance.StreamNearByLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                StreamViewModel.Instance.StreamMostViewLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                if (doClear) StreamViewModel.Instance.StreamSearchList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
            });

            if (doClear)
            {
                this.ClearValue(UCStreamDiscoveryPanel.LoadingStatusProperty);
                this.itcSearchStreamList.ClearValue(ItemsControl.ItemsSourceProperty);
                this.itcSearchStreamList.ItemsSource = null;
                StreamViewModel.Instance.StreamSearchList.Clear();
            }
        }

        private void ResetDataOnSearchComplete()
        {
            if (this.IsSearchMode)
            {
                this.itcRecentStreamList.SizeChanged -= SliderControl_SizeChanged;
                this.itcRecentStreamList.PreviewKeyDown -= SliderControl_PreviewKeyDown;
                this.itcNearbyStreamList.SizeChanged -= SliderControl_SizeChanged;
                this.itcNearbyStreamList.PreviewKeyDown -= SliderControl_PreviewKeyDown;
                this.itcMostViewedStreamList.SizeChanged -= ScrlViewer_SizeChanged;
                this.itcSearchStreamList.SizeChanged += ScrlViewer_SizeChanged;
                this.ScrlViewer.ScrollChanged += ScrlViewer_ScrollChanged;
                this.ScrlViewer.SizeChanged += ScrlViewer_SizeChanged;

                StreamHelpers.LoadSearchStreamList(StreamConstants.STREAM_TOP_SCROLL, TxtSearch.Text, null, null);
                this.itcSearchStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamSearchList"), Source = RingIDSettings });

                MultiBinding loadingStatusBinding = new MultiBinding { Converter = new LoadingStatusConverter() };
                loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.StreamSearchList.Count"), Source = RingIDSettings });
                loadingStatusBinding.Bindings.Add(new Binding { Path = new PropertyPath("StreamViewModel.LoadStatusModel.IsSearchLoading"), Source = RingIDSettings });
                this.SetBinding(UCStreamDiscoveryPanel.LoadingStatusProperty, loadingStatusBinding);
            }
            else
            {
                this.itcSearchStreamList.SizeChanged -= ScrlViewer_SizeChanged;
                this.itcMostViewedStreamList.SizeChanged += ScrlViewer_SizeChanged;
                this.ScrlViewer.ScrollChanged += ScrlViewer_ScrollChanged;
                this.ScrlViewer.SizeChanged += ScrlViewer_SizeChanged;
                this.ChangeSliderOpenStatus();
                this.ChangeScrollOpenStatus();
            }
        }

        private void OnSliderArrowClick(object param)
        {
            try
            {
                int type = Int32.Parse(param.ToString());
                bool isNext = type % 2 == 0;

                switch (type)
                {
                    case 1:
                    case 2:
                        {
                            double targetValue = 0.0D;
                            if (isNext)
                            {
                                double remaining = this.itcRecentStreamList.ActualWidth - Math.Abs(this.itcRecentStreamList.Margin.Left) + this.srvRecentStreamList.ActualWidth;
                                if (remaining < this.srvRecentStreamList.ActualWidth)
                                {
                                    targetValue = this.itcRecentStreamList.Margin.Left - remaining;
                                    StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                }
                                else
                                {
                                    targetValue = this.itcRecentStreamList.Margin.Left - this.srvRecentStreamList.ActualWidth;
                                    if (Math.Abs(targetValue) + this.srvRecentStreamList.ActualWidth >= this.itcRecentStreamList.ActualWidth)
                                    {
                                        StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                    }
                                }
                            }
                            else
                            {
                                if (Math.Abs(this.itcRecentStreamList.Margin.Left) <= this.srvRecentStreamList.ActualWidth)
                                {
                                    targetValue = 0;
                                    StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);
                                }
                                else
                                {
                                    targetValue = this.itcRecentStreamList.Margin.Left + this.srvRecentStreamList.ActualWidth;

                                }
                            }
                            OnSliderAnimation(this.itcRecentStreamList, targetValue, 0.35D);
                        }
                        break;
                    case 3:
                    case 4:
                        {
                            double targetValue = 0.0D;
                            if (isNext)
                            {
                                double remaining = this.itcNearbyStreamList.ActualWidth - Math.Abs(this.itcNearbyStreamList.Margin.Left) + this.srvNearbyStreamList.ActualWidth;
                                if (remaining < this.srvNearbyStreamList.ActualWidth)
                                {
                                    targetValue = this.itcNearbyStreamList.Margin.Left - remaining;
                                    StreamHelpers.LoadNearByStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                }
                                else
                                {
                                    targetValue = this.itcNearbyStreamList.Margin.Left - this.srvNearbyStreamList.ActualWidth;
                                    if (Math.Abs(targetValue) + this.srvNearbyStreamList.ActualWidth >= this.itcNearbyStreamList.ActualWidth)
                                    {
                                        StreamHelpers.LoadNearByStreamList(StreamConstants.STREAM_BOTTOM_SCROLL);
                                    }
                                }
                            }
                            else
                            {
                                if (Math.Abs(this.itcNearbyStreamList.Margin.Left) <= this.srvNearbyStreamList.ActualWidth)
                                {
                                    targetValue = 0;
                                    StreamHelpers.LoadNearByStreamList(StreamConstants.STREAM_TOP_SCROLL);
                                }
                                else
                                {
                                    targetValue = this.itcNearbyStreamList.Margin.Left + this.srvNearbyStreamList.ActualWidth;

                                }
                            }
                            OnSliderAnimation(this.itcNearbyStreamList, targetValue, 0.35D);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSliderArrowClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnSliderAnimation(FrameworkElement targetElement, double targetValue, double delay)
        {
            try
            {
                this.IsArrowEnabled = false;
                double currentValue = targetElement.Margin.Left;
                ThicknessAnimation marginAnimation = new ThicknessAnimation();
                marginAnimation.From = new Thickness(currentValue, 0, 0, 0);
                marginAnimation.To = new Thickness(targetValue, 0, 0, 0);
                marginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                this._SliderStoryboard = new Storyboard();
                this._SliderStoryboard.Children.Add(marginAnimation);
                Storyboard.SetTarget(marginAnimation, targetElement);
                Storyboard.SetTargetProperty(marginAnimation, new PropertyPath(ItemsControl.MarginProperty));
                this._SliderStoryboard.Completed += this.SliderStoryboard_Completed;
                this._SliderStoryboard.Begin(targetElement);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSliderAnimation() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeSliderOpenStatusOnResize()
        {
            try
            {
                if (this._SliderResizeTimer == null)
                {
                    this._SliderResizeTimer = new DispatcherTimer();
                    this._SliderResizeTimer.Interval = TimeSpan.FromMilliseconds(400);
                    this._SliderResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeSliderOpenStatus();
                        this._SliderResizeTimer.Stop();
                    };
                }
                this._SliderResizeTimer.Stop();
                if (this._SliderAnimationTimer == null || this._SliderAnimationTimer.IsEnabled == false)
                {
                    this._SliderResizeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeSliderOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeSliderOpenStatusOnAnimation()
        {
            try
            {
                if (this._SliderAnimationTimer == null)
                {
                    this._SliderAnimationTimer = new DispatcherTimer();
                    this._SliderAnimationTimer.Interval = TimeSpan.FromMilliseconds(100);
                    this._SliderAnimationTimer.Tick += (o, e) =>
                    {
                        this.ChangeSliderOpenStatus();
                        this._SliderAnimationTimer.Stop();
                        this.IsArrowEnabled = true;
                    };
                }
                if (this._SliderResizeTimer != null && this._SliderResizeTimer.IsEnabled)
                {
                    this._SliderResizeTimer.Stop();
                }
                this._SliderAnimationTimer.Stop();
                this._SliderAnimationTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeSliderOpenStatusOnAnimation() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeSliderOpenStatus()
        {
            try
            {
                int recentIndex = (int)(Math.Abs(itcRecentStreamList.Margin.Left) / 155);
                if (recentIndex < this.itcRecentStreamList.Items.Count)
                {
                    int firstIndex = recentIndex - 8 < 0 ? 0 : recentIndex - 8;
                    int read = recentIndex > 8 ? 0 : 8 - recentIndex;
                    for (int idx = firstIndex; idx < this.itcRecentStreamList.Items.Count && read < 20; idx++, read++)
                    {
                        if (idx >= (recentIndex - 4) && idx < (recentIndex + 4 + 4))
                        {
                            ((StreamModel)this.itcRecentStreamList.Items[idx]).IsViewOpened = true;
                        }
                        else
                        {
                            ((StreamModel)this.itcRecentStreamList.Items[idx]).IsViewOpened = false;
                        }
                    }
                }

                int nearByIndex = (int)(Math.Abs(itcNearbyStreamList.Margin.Left) / 155);
                if (nearByIndex < this.itcNearbyStreamList.Items.Count)
                {
                    int firstIndex = nearByIndex - 8 < 0 ? 0 : nearByIndex - 8;
                    int read = nearByIndex > 8 ? 0 : 8 - nearByIndex;
                    for (int idx = firstIndex; idx < this.itcNearbyStreamList.Items.Count && read < 20; idx++, read++)
                    {
                        if (idx >= (nearByIndex - 4) && idx < (nearByIndex + 4 + 4))
                        {
                            ((StreamModel)this.itcNearbyStreamList.Items[idx]).IsViewOpened = true;
                        }
                        else
                        {
                            ((StreamModel)this.itcNearbyStreamList.Items[idx]).IsViewOpened = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeSliderOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnResize()
        {
            try
            {
                if (this._WindowResizeTimer == null)
                {
                    this._WindowResizeTimer = new DispatcherTimer();
                    this._WindowResizeTimer.Interval = TimeSpan.FromMilliseconds(350);
                    this._WindowResizeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._WindowResizeTimer.Stop();
                    };
                }

                if (this._ScrollChangeTimer != null && this._ScrollChangeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Stop();
                }
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnResize() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatusOnScroll()
        {
            try
            {
                if (this._ScrollChangeTimer == null)
                {
                    this._ScrollChangeTimer = new DispatcherTimer();
                    this._ScrollChangeTimer.Interval = TimeSpan.FromMilliseconds(30);
                    this._ScrollChangeTimer.Tick += (o, e) =>
                    {
                        this.ChangeScrollOpenStatus();
                        this._ScrollChangeTimer.Stop();
                    };
                }
                this._ScrollChangeTimer.Stop();
                if (this._WindowResizeTimer == null || !this._WindowResizeTimer.IsEnabled)
                {
                    this._ScrollChangeTimer.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatusOnScroll() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChangeScrollOpenStatus()
        {
            try
            {
                if (this.IsSearchMode)
                {
                    int paddingTop = 51;
                    StreamHelpers.ChangeStreamViewPortOpenedProperty(this.ScrlViewer, this.itcSearchStreamList, paddingTop);
                }
                else
                {
                    int mostViewedCount = this.itcMostViewedStreamList.Items.Count;
                    int paddingTop = 51 + (int)bdrRecentLiveList.ActualHeight + (int)bdrNearByLiveList.ActualHeight + (int)bdrCountryList.ActualHeight + (int)bdrCategoryList.ActualHeight + (mostViewedCount > 0 ? 50 : 0);
                    StreamHelpers.ChangeStreamViewPortOpenedProperty(this.ScrlViewer, this.itcMostViewedStreamList, paddingTop, 4);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeScrollOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnClearSearchClick(object param)
        {
            this.TxtSearch.Text = String.Empty;
        }

        private void OnShowMoreClick(object param)
        {
            try
            {
                if (param == null)
                {
                    ShowCountrySelectPreview(null);
                }
                else
                {
                    ParamModel paramModel = new ParamModel();
                    paramModel.Title = (int)param == AppConstants.TYPE_GET_RECENT_STREAMS ? "New Lives" : "Nearby Lives";
                    paramModel.ActionType = (int)param;
                    paramModel.Param = null;
                    paramModel.PrevParam = null;
                    paramModel.PrevViewType = StreamAndChannelConstants.TypeStreamDiscoveryPanel;
                    StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel, paramModel);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnShowMoreClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnBackClick(object param)
        {
            try
            {
                RingIDViewModel.Instance.OnStreamAndChannelCommand(true);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnBackClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowCountrySelectPreview(dynamic param)
        {
            try
            {
                this._CountryListPanel = new UCStreamAndChannelCountryListPanel();
                this._CountryListPanel.OnCountrySelect += (CountryCodeModel countryModel, string searchParam) =>
                {
                    if (countryModel != null)
                    {
                        dynamic prevParam = new ExpandoObject();
                        prevParam.SearchParam = searchParam;

                        ParamModel paramModel = new ParamModel();
                        paramModel.Title = countryModel.CountryName + " - Lives";
                        paramModel.ActionType = AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS;
                        paramModel.Param = countryModel;
                        paramModel.PrevParam = prevParam;
                        paramModel.PrevViewType = StreamAndChannelConstants.TypeStreamDiscoveryPanel;
                        StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel, paramModel);
                    }
                    this.HideCountrySelectPreview(true);
                };
                this.pnlCountrySelectContainer.Child = this._CountryListPanel;
                this.IsCountrySelectMode = true;

                this._CountryListPanel.InilializeViewer(param != null ? param.SearchParam : null);
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowCountrySelectPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideCountrySelectPreview(bool isRefresh = false)
        {
            try
            {
                if (this._CountryListPanel != null)
                {
                    this._CountryListPanel.Dispose();
                    this._CountryListPanel = null;
                }
                this.IsCountrySelectMode = false;
                this.pnlCountrySelectContainer.Child = null;
                if (isRefresh)
                {
                    this.ChangeSliderOpenStatus();
                    this.ChangeScrollOpenStatus();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: HideCountrySelectPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            if (this._SliderResizeTimer != null)
            {
                this._SliderResizeTimer.Stop();
                this._SliderResizeTimer = null;
            }
            if (this._SliderAnimationTimer != null)
            {
                this._SliderAnimationTimer.Stop();
                this._SliderAnimationTimer = null;
            }
            if (this._WindowResizeTimer != null)
            {
                this._WindowResizeTimer.Stop();
                this._WindowResizeTimer = null;
            }
            if (this._ScrollChangeTimer != null)
            {
                this._ScrollChangeTimer.Stop();
                this._ScrollChangeTimer = null;
            }
            if (this._FilterTimer != null)
            {
                this._FilterTimer.Stop();
                this._FilterTimer = null;
            }
            this.TxtSearch.TextChanged -= SearchBox_TextChanged;
            this.itcRecentStreamList.SizeChanged -= SliderControl_SizeChanged;
            this.itcRecentStreamList.PreviewKeyDown -= SliderControl_PreviewKeyDown;
            this.itcNearbyStreamList.SizeChanged -= SliderControl_SizeChanged;
            this.itcNearbyStreamList.PreviewKeyDown -= SliderControl_PreviewKeyDown;
            this.itcMostViewedStreamList.SizeChanged -= ScrlViewer_SizeChanged;
            this.itcSearchStreamList.SizeChanged -= ScrlViewer_SizeChanged;
            this.ScrlViewer.ScrollChanged -= ScrlViewer_ScrollChanged;
            this.ScrlViewer.SizeChanged -= ScrlViewer_SizeChanged;
            this.ClearValue(UCStreamDiscoveryPanel.LoadingStatusProperty);

            Task.Factory.StartNew(() =>
            {
                StreamViewModel.Instance.StreamRecentLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                StreamViewModel.Instance.StreamNearByLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                StreamViewModel.Instance.StreamMostViewLiveList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
                StreamViewModel.Instance.StreamSearchList.Where(P => P.IsViewOpened).ToList().ForEach(P => P.IsViewOpened = false);
            });

            this.itcRecentStreamList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcRecentStreamList.ItemsSource = null;
            this.itcNearbyStreamList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcNearbyStreamList.ItemsSource = null;
            this.itcMostViewedStreamList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcMostViewedStreamList.ItemsSource = null;
            this.itcSearchStreamList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcSearchStreamList.ItemsSource = null;
            this.itcCountryList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcCountryList.ItemsSource = null;
            this.itcCategoryList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcCategoryList.ItemsSource = null;
            StreamViewModel.Instance.StreamSearchList.Clear();
            this.CountryList.Clear();
            this.DataContext = null;
            this.CountryList = null;
            this.IsSearchMode = false;
            this._PrevSearchText = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty LoadingStatusProperty = DependencyProperty.Register("LoadingStatus", typeof(int), typeof(UCStreamDiscoveryPanel), new PropertyMetadata(StatusConstants.NO_DATA, OnLoadingStatusChanged));

        public int LoadingStatus
        {
            get { return (int)GetValue(LoadingStatusProperty); }
            set
            {
                SetValue(LoadingStatusProperty, value);
            }
        }

        public ICommand ShowMoreCommand
        {
            get
            {
                if (_ShowMoreCommand == null)
                {
                    _ShowMoreCommand = new RelayCommand((param) => OnShowMoreClick(param));
                }
                return _ShowMoreCommand;
            }
        }

        public ICommand ClearSearchCommand
        {
            get
            {
                if (_ClearSearchCommand == null)
                {
                    _ClearSearchCommand = new RelayCommand((param) => OnClearSearchClick(param));
                }
                return _ClearSearchCommand;
            }
        }

        public ICommand ArrowClickCommand
        {
            get
            {
                if (_ArrowClickCommand == null)
                {
                    _ArrowClickCommand = new RelayCommand((param) => OnSliderArrowClick(param));
                }
                return _ArrowClickCommand;
            }
        }

        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand((param) => OnBackClick(param));
                }
                return _BackCommand;
            }
        }

        public bool IsArrowEnabled
        {
            get { return _IsArrowEnabled; }
            set
            {
                if (_IsArrowEnabled == value)
                    return;

                _IsArrowEnabled = value;
                this.OnPropertyChanged("IsArrowEnabled");
            }
        }

        public bool IsSearchMode
        {
            get { return _IsSearchMode; }
            set
            {
                if (_IsSearchMode == value)
                    return;

                _IsSearchMode = value;
                this.OnPropertyChanged("IsSearchMode");
            }
        }

        public bool IsCountrySelectMode
        {
            get { return _IsCountrySelectMode; }
            set
            {
                if (_IsCountrySelectMode == value)
                    return;

                _IsCountrySelectMode = value;
                this.OnPropertyChanged("IsCountrySelectMode");
            }
        }

        public ObservableCollection<CountryCodeModel> CountryList
        {
            get { return _CountryList; }
            set
            {
                _CountryList = value;
                OnPropertyChanged("CountryList");
            }
        }

        #endregion Property
    }
}

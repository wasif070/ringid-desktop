﻿using callsdkwrapper;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.StreamAndChannel;
using View.Utility;
using View.Utility.Call;
using View.Utility.Chat.Service;
using View.Utility.Stream;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamLiveNowViewer.xaml
    /// </summary>
    public partial class UCStreamLiveNowViewer : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamLiveNowViewer).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private bool _IS_INITIALIZED = false;
        private ICommand _SelectCategoryCommand;
        private ICommand _GoLiveCommand;
        private ICommand _CancelCommand;

        #region Constructor

        static UCStreamLiveNowViewer()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamLiveNowViewer()
        {
            InitializeComponent();
            this.DataContext = null;
            if (StreamViewModel.Instance.StreamCategoryList == null || StreamViewModel.Instance.StreamCategoryList.Count == 0)
            {
                new ThrdGetStreamCategoryList(String.Empty, null).Start(); ;
            }
        }

        #endregion Event Handler

        #region Event Handler

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            if (this._IS_INITIALIZED == false)
            {
                this._IS_INITIALIZED = true;
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this.DataContext = this;
                    this.txtTitle.Focus();
                    this.itcCtgList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamCategoryList"), Source = RingIDSettings });
                }, DispatcherPriority.ApplicationIdle);
            }
        }

        private void OnSelectCategoryClick(object param)
        {
            StreamCategoryModel model = (StreamCategoryModel)param;
            model.IsChecked = !model.IsChecked;
        }

        private void OnGoLiveClick(object param)
        {
            if (StreamViewModel.Instance.StreamInfoModel.CategoryList == null)
            {
                StreamViewModel.Instance.StreamInfoModel.CategoryList = new ObservableCollection<StreamCategoryModel>();
            }
            else
            {
                StreamViewModel.Instance.StreamInfoModel.CategoryList.Clear();
            }

            foreach (StreamCategoryModel model in StreamViewModel.Instance.StreamCategoryList.ToList())
            {
                if (model.IsChecked)
                {
                    StreamViewModel.Instance.StreamInfoModel.CategoryList.Add(model);
                }
            }

            if (StreamViewModel.Instance.StreamInfoModel.CategoryList.Count == 0)
            {
                StreamCategoryModel model = StreamViewModel.Instance.StreamCategoryList.OrderBy(P => P.CategoryID).FirstOrDefault();
                if (model != null)
                {
                    StreamViewModel.Instance.StreamInfoModel.CategoryList.Add(model);
                }
            }

            StreamViewModel.Instance.StreamInfoModel.Title = txtTitle.Text;
            UCStreamLiveWrapper ucStreamLiveWrapper = StreamHelpers.GetCurruentStreamLiveWrapper();
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                ucStreamLiveWrapper.OpenViewer();
            }, DispatcherPriority.ApplicationIdle);
        }

        private void OnCancelClick(object param)
        {
            try
            {
                UCStreamAndChannelViewer.Instance.Dispose();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCancelClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this._IS_INITIALIZED = false;
            this.itcCtgList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
            this.itcCtgList.ItemsSource = null;
            StreamViewModel.Instance.StreamCategoryList.ToList().ForEach(i => i.IsChecked = false);
            this.DataContext = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand SelectCategoryCommand
        {
            get
            {
                if (_SelectCategoryCommand == null)
                {
                    _SelectCategoryCommand = new RelayCommand((param) => OnSelectCategoryClick(param));
                }
                return _SelectCategoryCommand;
            }
        }

        public ICommand GoLiveCommand
        {
            get
            {
                if (_GoLiveCommand == null)
                {
                    _GoLiveCommand = new RelayCommand((param) => OnGoLiveClick(param));
                }
                return _GoLiveCommand;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand((param) => OnCancelClick(param));
                }
                return _CancelCommand;
            }
        }

        #endregion Property


    }
}

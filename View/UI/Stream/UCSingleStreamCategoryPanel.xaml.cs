﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;
using View.Utility;
using View.Utility.Stream;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCSingleStreamCategoryPanel.xaml
    /// </summary>
    public partial class UCSingleStreamCategoryPanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleStreamCategoryPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private bool disposed = false;

        public UCSingleStreamCategoryPanel()
        {
            InitializeComponent();
        }

        ~UCSingleStreamCategoryPanel()
        {
           Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleStreamCategoryPanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        public static readonly DependencyProperty IsExpandedProperty = DependencyProperty.Register("IsExpanded", typeof(bool), typeof(UCSingleStreamCategoryPanel), new PropertyMetadata(false, OnExpandChanged));

        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set
            {
                SetValue(IsExpandedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleStreamCategoryPanel panel = ((UCSingleStreamCategoryPanel)o);
                object data = panel.DataContext;

                if (e.NewValue != null && (bool)e.NewValue)
                {
                    panel.BindPreview((StreamCategoryModel)data);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void OnExpandChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleStreamCategoryPanel panel = ((UCSingleStreamCategoryPanel)o);
                object data = panel.DataContext;

                if (e.NewValue != null && (bool)e.NewValue && panel.IsOpened)
                {
                    panel.BindStreamList((StreamCategoryModel)data);
                }
                else
                {
                    panel.ClearStreamList();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindPreview(StreamCategoryModel model)
        {
            brdControl.SetBinding(Border.BackgroundProperty, new Binding { Path = new PropertyPath("CategoryID"), Converter = new IntToBackgroundColor() });

            MultiBinding nameBinding = new MultiBinding { StringFormat = "{0} ({1})" };
            nameBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("CategoryName"),
            });
            nameBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("FollowingCount"),
            });
            txtName.SetBinding(TextBlock.TextProperty, nameBinding);

            MouseBinding mouseBinding = new MouseBinding();
            mouseBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
            mouseBinding.Command = StreamViewModel.Instance.ExpandCollapseCategoryCommand;
            mouseBinding.CommandParameter = model;
            brdControl.InputBindings.Add(mouseBinding);
        }

        private void ClearPreview()
        {
            brdControl.ClearValue(Border.BackgroundProperty);
            txtName.ClearValue(TextBlock.TextProperty);
            brdControl.InputBindings.Clear();
        }

        private void BindStreamList(StreamCategoryModel model)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.itcStreamList.SetBinding(ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("StreamList") });
            });
        }

        private void ClearStreamList()
        {
            this.itcStreamList.ClearValue(ItemsControl.ItemsSourceProperty);
            this.itcStreamList.ItemsSource = null;
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    if (disposing)
                    {
                        this.ClearPreview();
                        this.ClearStreamList();
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

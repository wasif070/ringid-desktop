﻿using callsdkwrapper;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.Converter;
using View.Utility;
using View.Utility.Call;
using View.Utility.Stream;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamLiveCallViewer.xaml
    /// </summary>
    public partial class UCStreamLiveCallViewer : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamLiveCallViewer).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        public delegate void CloseHandler();
        public event CloseHandler OnClose;

        private ICommand _ActionCommand;

        #region Constructor

        static UCStreamLiveCallViewer()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamLiveCallViewer()
        {
            InitializeComponent();
            this.DataContext = null;
        }

        #endregion Constructor

        #region Event Handler

        static void OnCallStatusChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCStreamLiveCallViewer panel = ((UCStreamLiveCallViewer)o);
                panel.BindPreview(e.NewValue != null ? (int)e.NewValue : StreamConstants.STREAM_CALL_DISCONNECTED);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            this.DataContext = this;
            this.SetBinding(UCStreamLiveCallViewer.CallStatusProperty, new Binding { Path = new PropertyPath("StreamViewModel.CallStatus"), Source = RingIDSettings });
        }

        private void BindPreview(int callStatus)
        {
            try
            {
                log.Debug(">>>>>>>>>>>>>>>>>> CALL VIEWER = " + callStatus);
                txtCallingFullName.ClearValue(TextBlock.TextProperty);
                txtIncomingFullName.ClearValue(TextBlock.TextProperty);
                txtConnectedFullName.ClearValue(TextBlock.TextProperty);
                txtConnectedTimer.ClearValue(TextBlock.TextProperty);
                imgCallingProfile.ClearValue(Image.SourceProperty);
                imgCallingProfile.Source = null;
                imgConnectedProfile.ClearValue(Image.SourceProperty);
                imgConnectedProfile.Source = null;

                if (callStatus == StreamConstants.STREAM_CALL_OUTGOING)
                {
                    txtCallingFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("StreamViewModel.CallerInfoModel.UserName"), Source = RingIDSettings });
                    MultiBinding sourceBinding = new MultiBinding { Converter = new StreamProfileImageConverter(), ConverterParameter = ImageUtility.IMG_THUMB };
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("StreamViewModel.CallerInfoModel"),
                        Source = RingIDSettings
                    });
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("StreamViewModel.CallerInfoModel.ProfileImage"),
                        Source = RingIDSettings
                    });
                    imgCallingProfile.SetBinding(Image.SourceProperty, sourceBinding);
                }
                else if (callStatus == StreamConstants.STREAM_CALL_INCOMING)
                {
                    txtIncomingFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("StreamViewModel.CallerInfoModel.UserName"), Source = RingIDSettings });
                }
                else if (callStatus == StreamConstants.STREAM_CALL_CONNECTED)
                {
                    txtConnectedFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("StreamViewModel.CallerInfoModel.UserName"), Source = RingIDSettings });
                    txtConnectedTimer.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("StreamViewModel.CallDuration"), Converter = new ChannelCallDurationConverter(), Source = RingIDSettings, StringFormat = "hh\\:mm\\:ss" });
                    MultiBinding sourceBinding = new MultiBinding { Converter = new StreamProfileImageConverter(), ConverterParameter = ImageUtility.IMG_THUMB };
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("StreamViewModel.CallerInfoModel"),
                        Source = RingIDSettings
                    });
                    sourceBinding.Bindings.Add(new Binding
                    {
                        Path = new PropertyPath("StreamViewModel.CallerInfoModel.ProfileImage"),
                        Source = RingIDSettings
                    });
                    imgConnectedProfile.SetBinding(Image.SourceProperty, sourceBinding);
                }
                else if (callStatus == StreamConstants.STREAM_CALL_DISCONNECTED)
                {
                    this.ClearValue(UCStreamLiveCallViewer.CallStatusProperty);
                    if (this.OnClose != null) this.OnClose();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: BindPreview => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnActionCommand(object param)
        {
            try
            {
                int type = Int32.Parse(param.ToString());
                switch (type)
                {
                    case 0://BYE
                        CallHelperMethods.ByeLiveCall(StreamViewModel.Instance.CallerInfoModel.UserTableID);
                        StreamViewModel.Instance.DestroyCallChannel();
                        StreamViewModel.Instance.DestroyCallInfo();
                        break;
                    case 1://BUSY
                        CallHelperMethods.BusyLiveCall(StreamViewModel.Instance.CallerInfoModel.UserTableID);
                        StreamViewModel.Instance.DestroyCallChannel();
                        StreamViewModel.Instance.DestroyCallInfo();
                        break;
                    case 2://ANSWER VIDEO
                        if (CallHelperMethods.AnswerLiveCall(StreamViewModel.Instance.CallerInfoModel.UserTableID, StreamViewModel.Instance.CallType)) 
                        {
                            StreamViewModel.Instance.CallType = CallMediaType.Video;
                            StreamViewModel.Instance.CallStatus = StreamConstants.STREAM_CALL_CONNECTED;
                        }
                        else
                        {
                            StreamViewModel.Instance.CallStatus = StreamConstants.STREAM_CALL_DISCONNECTED;
                        }
                        break;
                    case 3://ANSWER AUDIO
                        if (CallHelperMethods.AnswerLiveCall(StreamViewModel.Instance.CallerInfoModel.UserTableID, StreamViewModel.Instance.CallType))
                        {
                            StreamViewModel.Instance.CallType = CallMediaType.Voice;
                            StreamViewModel.Instance.CallStatus = StreamConstants.STREAM_CALL_CONNECTED;
                        }
                        else
                        {
                            StreamViewModel.Instance.CallStatus = StreamConstants.STREAM_CALL_DISCONNECTED;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnActionCommand() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this.ClearValue(UCStreamLiveCallViewer.CallStatusProperty);
            txtCallingFullName.ClearValue(TextBlock.TextProperty);
            txtIncomingFullName.ClearValue(TextBlock.TextProperty);
            txtConnectedFullName.ClearValue(TextBlock.TextProperty);
            txtConnectedTimer.ClearValue(TextBlock.TextProperty);
            imgCallingProfile.ClearValue(Image.SourceProperty);
            imgCallingProfile.Source = null;
            imgConnectedProfile.ClearValue(Image.SourceProperty);
            imgConnectedProfile.Source = null;
            this.DataContext = null;
            this.OnClose = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public static readonly DependencyProperty CallStatusProperty = DependencyProperty.Register("CallStatus", typeof(int), typeof(UCStreamLiveCallViewer), new PropertyMetadata(StreamConstants.STREAM_CALL_DISCONNECTED, OnCallStatusChanged));

        public int CallStatus
        {
            get { return (int)GetValue(CallStatusProperty); }
            set
            {
                SetValue(CallStatusProperty, value);
            }
        }

        public ICommand ActionCommand
        {
            get
            {
                if (_ActionCommand == null)
                {
                    _ActionCommand = new RelayCommand((param) => OnActionCommand(param));
                }
                return _ActionCommand;
            }
        }

        #endregion Property
    }
}

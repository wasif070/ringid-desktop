﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCBubbleAnimation.xaml
    /// </summary>
    public partial class UCBubbleAnimation : UserControl
    {

        private const int SIZE = 10;
        private const double DURATION = 5000;

        private static readonly double[] SIZE_ARRAY = new double[SIZE] { 60, 60, 40, 40, 25, 25, 40, 25, 60, 40 };
        private static readonly double[] OPACITY_ARRAY = { 0.90, 0.80, 0.75, 0.70, 0.60, 0.50, 0.45, 0.40, 0.35, 0.30, 0.25, 0.15, 0.07, 0.00 };
        private static readonly double[,] MARGIN_ARRAY = new double[SIZE, 32] 
        { 
            {000,000 , 035,000 , 070,010 , 100,018 , 140,025 , 170,037 , 200,050 , 230,070 , 260,100 , 290,120 , 320,140 , 350,160 , 370,180 , 390,200 , 420,230 , 450,250}, 
            {000,000 , 035,015 , 070,030 , 100,050 , 130,070 , 160,080 , 190,090 , 220,110 , 250,120 , 270,130 , 290,160 , 320,190 , 350,220 , 370,250 , 400,280 , 420,310},  
            {000,000 , 035,010 , 070,020 , 100,030 , 130,050 , 140,080 , 170,100 , 190,130 , 210,160 , 240,180 , 270,210 , 300,230 , 330,240 , 360,260 , 400,290 , 440,310},
            {000,000 , 035,020 , 070,040 , 100,060 , 130,090 , 180,110 , 190,130 , 210,150 , 220,190 , 220,210 , 230,240 , 220,270 , 210,300 , 195,330 , 180,360 , 170,380},
            {000,000 , 035,030 , 070,060 , 100,090 , 120,100 , 150,130 , 170,150 , 180,170 , 200,190 , 220,220 , 230,240 , 245,265 , 260,290 , 270,320 , 280,355 , 280,400},
            {000,000 , 050,080 , 080,120 , 100,150 , 130,180 , 150,200 , 170,220 , 200,240 , 230,260 , 260,280 , 290,300 , 320,310 , 360,330 , 380,335 , 420,335 , 460,340},
            {000,000 , 010,030 , 020,070 , 030,100 , 040,120 , 045,160 , 050,180 , 070,210 , 080,240 , 110,260 , 130,280 , 170,300 , 190,320 , 220,340 , 250,365 , 240,390},
            {000,000 , 003,030 , 005,070 , 010,100 , 015,120 , 017,160 , 020,170 , 030,200 , 045,230 , 060,260 , 070,280 , 090,305 , 100,330 , 120,360 , 140,380 , 160,400},
            {000,000 , 035,000 , 070,010 , 080,100 , 090,130 , 095,160 , 100,190 , 100,220 , 095,250 , 090,270 , 080,290 , 085,320 , 070,340 , 065,360 , 050,380 , 030,400},
            {000,000 , 030,030 , 060,070 , 090,100 , 100,120 , 120,150 , 140,170 , 150,200 , 150,225 , 150,250 , 160,270 , 160,300 , 160,330 , 170,350 , 170,375 , 170,400},
        };
        private static readonly SolidColorBrush[] COLOR_ARRAY = new SolidColorBrush[SIZE] { Brushes.White, Brushes.Purple, Brushes.White, Brushes.Orange, Brushes.Purple, Brushes.Orange, Brushes.White, Brushes.White, Brushes.Orange, Brushes.Purple };

        private static readonly double[] DATA_ARRAY = new double[] { 0.51, 0.10, 0.20, 0.10, 0.50, 0.20, 0.60, 0.50, 0.80, 0.70, 0.90, 0.50, 0.20, 0.49, 0.10 };
        private const String DATA_STRING = "M {0},{1} A {2},{2} 0 0 0 {3},{4} C {5},{6} {7},{8} {7},{8} C {7},{8} {9},{9} {10},{11} A {12},{12} 0 0 0 {13},{14}";
        private Random _Random = null;
        private List<Storyboard> StoryboardList = new List<Storyboard>();

        public UCBubbleAnimation()
        {
            InitializeComponent();
            this._Random = new Random();
            this.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void AddBubble()
        {
            int randomNumber = this._Random.Next(SIZE);

            System.Windows.Shapes.Path path = new System.Windows.Shapes.Path();
            path.Fill = COLOR_ARRAY[randomNumber];
            path.StrokeThickness = 3;
            path.Stroke = COLOR_ARRAY[randomNumber];
            path.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            path.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            path.Data = Geometry.Parse(String.Format(DATA_STRING, DATA_ARRAY[0] * SIZE_ARRAY[randomNumber], DATA_ARRAY[1] * SIZE_ARRAY[randomNumber], DATA_ARRAY[2] * SIZE_ARRAY[randomNumber], DATA_ARRAY[3] * SIZE_ARRAY[randomNumber], DATA_ARRAY[4] * SIZE_ARRAY[randomNumber], DATA_ARRAY[5] * SIZE_ARRAY[randomNumber], DATA_ARRAY[6] * SIZE_ARRAY[randomNumber], DATA_ARRAY[7] * SIZE_ARRAY[randomNumber], DATA_ARRAY[8] * SIZE_ARRAY[randomNumber], DATA_ARRAY[9] * SIZE_ARRAY[randomNumber], DATA_ARRAY[10] * SIZE_ARRAY[randomNumber], DATA_ARRAY[11] * SIZE_ARRAY[randomNumber], DATA_ARRAY[12] * SIZE_ARRAY[randomNumber], DATA_ARRAY[13] * SIZE_ARRAY[randomNumber], DATA_ARRAY[14] * SIZE_ARRAY[randomNumber]));
            container.Children.Add(path);
            this.Visibility = System.Windows.Visibility.Visible;

            int size = MARGIN_ARRAY.Length / SIZE;
            double icnTime = DURATION / (double)size;
            double time = 0;

            ThicknessAnimationUsingKeyFrames marginAnimation = new ThicknessAnimationUsingKeyFrames();
            marginAnimation.BeginTime = TimeSpan.FromMilliseconds(0);
            for (int idx = 0; idx < size; idx += 2, time += icnTime)
            {
                marginAnimation.KeyFrames.Add(new SplineThicknessKeyFrame { KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(time)), Value = new Thickness(0, 0, MARGIN_ARRAY[randomNumber, idx], MARGIN_ARRAY[randomNumber, idx + 1]) });
            }
            Storyboard.SetTarget(marginAnimation, path);
            Storyboard.SetTargetProperty(marginAnimation, new PropertyPath(Path.MarginProperty));

            size = OPACITY_ARRAY.Length;
            icnTime = DURATION / (double)OPACITY_ARRAY.Length;
            time = 0;

            DoubleAnimationUsingKeyFrames opacityAnimation = new DoubleAnimationUsingKeyFrames();
            opacityAnimation.BeginTime = TimeSpan.FromMilliseconds(0);
            for (int idx = 0; idx < size; idx++, time += icnTime)
            {
                opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(time)), Value = OPACITY_ARRAY[idx] });
            }
            Storyboard.SetTarget(opacityAnimation, path);
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath(Path.OpacityProperty));

            Storyboard storyBoard = new Storyboard();
            storyBoard.Children.Add(opacityAnimation);
            storyBoard.Children.Add(marginAnimation);
            storyBoard.Completed += (s, e) => Animation_Completed(storyBoard, path);
            storyBoard.Begin();
            StoryboardList.Add(storyBoard);
        }

        public void Close()
        {
            foreach (Storyboard sb in this.StoryboardList.ToList())
            {
                sb.Stop();
            }
            this.StoryboardList.Clear();
            this.container.Children.Clear();
        }

        private void Animation_Completed(Storyboard storyboard, System.Windows.Shapes.Path path)
        {
            StoryboardList.Remove(storyboard);
            storyboard.Children.Clear();
            storyboard = null;
            this.container.Children.Remove(path);

            if (this.container.Children.Count == 0)
            {
                this.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

    }
}

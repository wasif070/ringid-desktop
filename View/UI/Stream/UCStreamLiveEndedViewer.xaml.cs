﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.StreamAndChannel;
using View.Utility;
using View.Utility.Stream;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamLiveEndedViewer.xaml
    /// </summary>
    public partial class UCStreamLiveEndedViewer : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamLiveEndedViewer).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _IS_INITIALIZED = false;

        private ICommand _HomeCommand;

        #region Constructor

        public UCStreamLiveEndedViewer()
        {
            InitializeComponent();
            this.DataContext = null;
        }

        #endregion Event Handler

        #region Event Handler

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer(bool detailsRequest)
        {
            if (this._IS_INITIALIZED == false)
            {
                this._IS_INITIALIZED = true;
                this.DataContext = this;

                if (detailsRequest)
                {
                    Guid streamID = StreamViewModel.Instance.StreamInfoModel.StreamID;
                    long utId = StreamViewModel.Instance.StreamInfoModel.UserTableID;
                    new ThrdStreamDetails(streamID, (streamDTO, streamUserDTO) =>
                    {
                        if (this._IS_INITIALIZED)
                        {
                            if (streamDTO != null && StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.StreamID.Equals(streamID))
                            {
                                if (StreamViewModel.Instance.StreamInfoModel.EndTime > 0 && streamDTO.EndTime == 0)
                                {
                                    streamDTO.EndTime = StreamViewModel.Instance.StreamInfoModel.EndTime;
                                }
                                StreamViewModel.Instance.StreamInfoModel.LoadData(streamDTO);
                            }
                            if (streamUserDTO != null && StreamViewModel.Instance.StreamUserInfoModel != null && StreamViewModel.Instance.StreamUserInfoModel.UserTableID == utId)
                            {
                                StreamViewModel.Instance.StreamUserInfoModel.LoadData(streamUserDTO);
                            }
                        }
                        return 0;
                    }).Start();
                }
            }
        }

        private void OnHomeClick(object param)
        {
            try
            {
                UCStreamAndChannelViewer.Instance.Dispose();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnHomeClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            this._IS_INITIALIZED = false;
            this.DataContext = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public ICommand HomeCommand
        {
            get
            {
                if (_HomeCommand == null)
                {
                    _HomeCommand = new RelayCommand((param) => OnHomeClick(param));
                }
                return _HomeCommand;
            }
        }

        #endregion Property


    }
}

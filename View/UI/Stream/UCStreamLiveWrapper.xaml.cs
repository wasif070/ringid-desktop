﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility.Call;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.Stream;
using View.ViewModel;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamLiveWrapper.xaml
    /// </summary>
    public partial class UCStreamLiveWrapper : UserControl, INotifyPropertyChanged, IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamLiveWrapper).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        public UCStreamLiveSourceViewer _StreamLiveSourceViewer = null;
        public UCStreamLiveNowViewer _StreamLiveNowViewer = null;
        public UCStreamLiveViewer _StreamLiveViewer = null;
        public UCStreamLiveEndedViewer _StreamLiveEndedViewer = null;

        private bool _IS_INITIALIZED = false;
        private StreamModel _StreamModel = null;
        private StreamUserModel _StreamUserModel = null;
        private bool _IsSourceView = true;

        #region Constructor

        static UCStreamLiveWrapper()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamLiveWrapper(StreamModel streamModel)
        {
            InitializeComponent();
            this._StreamModel = streamModel;
            this.DataContext = this;
        }    

        #endregion Constructor

        #region Event Handler


        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            if (this.IS_INITIALIZED == false)
            {
                this.IS_INITIALIZED = true;
                int channelType = 0;

                if (this._StreamModel != null)
                {
                    channelType = StreamConstants.CHANNEL_IN;
                    this._StreamModel = new StreamModel
                    {
                        StreamID = this._StreamModel.StreamID,
                        UserTableID = this._StreamModel.UserTableID,
                        UserName = this._StreamModel.UserName,
                        ProfileImage = this._StreamModel.ProfileImage,
                        Title = this._StreamModel.Title,
                        ViewCount = this._StreamModel.ViewCount,
                        UserType = this._StreamModel.UserType,
                        StartTime = this._StreamModel.StartTime,
                        PublisherServerIP = this._StreamModel.PublisherServerIP,
                        PublisherServerPort = this._StreamModel.PublisherServerPort,
                        ViewerServerIP = this._StreamModel.ViewerServerIP,
                        ViewerServerPort = this._StreamModel.ViewerServerPort
                    };

                    this._StreamUserModel = new StreamUserModel
                    {
                        UserTableID = this._StreamModel.UserTableID,
                        UserName = this._StreamModel.UserName,
                        ProfileImage = this._StreamModel.ProfileImage
                    };
                }
                else
                {
                    channelType = StreamConstants.CHANNEL_OUT;
                    this._StreamModel = new StreamModel 
                    {
                        StreamID = new Guid(PacketIDGenerator.GeneratedPacketIDByFixTimeAndIdentity(ChatService.GetServerTime(), DefaultSettings.LOGIN_TABLE_ID)), 
                        UserTableID = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.UserTableID,
                        UserName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName,
                        ProfileImage = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage,
                        ChatOn = true, 
                        GiftOn = true
                    };

                    this._StreamUserModel = new StreamUserModel
                    {
                        UserTableID = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.UserTableID,
                        UserName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName,
                        ProfileImage = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage
                    };
                }

                StreamViewModel.Instance.InitStreamingInfo(this._StreamModel, this._StreamUserModel, channelType);

                if (channelType == StreamConstants.CHANNEL_OUT)
                {
                    StreamViewModel.Instance.InitStreamingChannel();
                    this.imgStream.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRenderModel.RenderSource"), Source = RingIDSettings });
                    this.imgCall.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.CallRenderModel.RenderSource"), Source = RingIDSettings });
                    this.OpenLiveSourceViewer();
                }
                else if (this._StreamModel.EndTime == 0)
                {
                    StreamViewModel.Instance.InitStreamingChannel();
                    this.imgStream.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRenderModel.RenderSource"), Source = RingIDSettings });
                    this.imgCall.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.CallRenderModel.RenderSource"), Source = RingIDSettings });
                    this.OpenViewer();
                }
                else
                {
                    this.OpenLiveEndedViewer();
                }
            }
        }

        public void OpenLiveSourceViewer()
        {
            this.IsSourceView = true;
            this._StreamLiveSourceViewer = new UCStreamLiveSourceViewer();
            this.pnlMainViewer.Child = this._StreamLiveSourceViewer;
            this._StreamLiveSourceViewer.InilializeViewer();
        }

        public void OpenLiveNowViewer()
        {
            this.IsSourceView = false;
            this._StreamLiveNowViewer = new UCStreamLiveNowViewer();
            this.pnlMainViewer.Child = this._StreamLiveNowViewer;
            this._StreamLiveNowViewer.InilializeViewer();

            if (this._StreamLiveSourceViewer != null)
            {
                this._StreamLiveSourceViewer.Dispose();
                this._StreamLiveSourceViewer = null;
            }
        }

        public void OpenViewer()
        {
            this.IsSourceView = false;
            this._StreamLiveViewer = new UCStreamLiveViewer();
            this.pnlMainViewer.Child = this._StreamLiveViewer;
            this._StreamLiveViewer.InilializeViewer();

            if (this._StreamLiveNowViewer != null)
            {
                this._StreamLiveNowViewer.Dispose();
                this._StreamLiveNowViewer = null;
            }
        }

        public void OpenLiveEndedViewer(bool detailsRequest = true)
        {
            this.IsSourceView = false;
            this._StreamLiveEndedViewer = new UCStreamLiveEndedViewer();
            this.pnlMainViewer.Child = this._StreamLiveEndedViewer;
            this._StreamLiveEndedViewer.InilializeViewer(detailsRequest);

            if (this._StreamLiveViewer != null)
            {
                this._StreamLiveViewer.Dispose();
                this._StreamLiveViewer = null;
            }

            this.imgStream.ClearValue(Image.SourceProperty);
            this.imgStream.Source = null;

            StreamViewModel.Instance.DestroyStreamingChannel();
        }

        public void ShowWindowPreview()
        {
            if (this._StreamLiveViewer != null)
            {
                this._StreamLiveViewer.ShowWindowPreview();
            }

            this.imgStream.ClearValue(Image.SourceProperty);
            this.imgStream.Source = null;
            this.imgCall.ClearValue(Image.SourceProperty);
            this.imgCall.Source = null;
        }

        public void HideWindowPreview()
        {
            this.imgStream.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamRenderModel.RenderSource"), Source = RingIDSettings });
            this.imgCall.SetBinding(Image.SourceProperty, new Binding { Path = new PropertyPath("StreamViewModel.CallRenderModel.RenderSource"), Source = RingIDSettings });

            if (this._StreamLiveViewer != null)
            {
                this._StreamLiveViewer.HideWindowPreview();
            }
        }

        public void Dispose()
        {
            this.IS_INITIALIZED = false;
            this.DataContext = null;
            this.pnlMainViewer.Child = null;

            if (this._StreamLiveSourceViewer != null)
            {
                this._StreamLiveSourceViewer.Dispose();
                this._StreamLiveSourceViewer = null;
            }
            if (this._StreamLiveNowViewer != null)
            {
                this._StreamLiveNowViewer.Dispose();
                this._StreamLiveNowViewer = null;
            }
            if (this._StreamLiveViewer != null)
            {
                this._StreamLiveViewer.Dispose();
                this._StreamLiveViewer = null;
            }
            else
            {
                if (StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamUserInfoModel != null)
                {
                    CallHelperMethods.PublisherUnregister(
                        StreamViewModel.Instance.StreamInfoModel.StreamID.ToString(),
                        StreamViewModel.Instance.StreamInfoModel.ViewCount,
                        StreamViewModel.Instance.StreamUserInfoModel.CoinCount,
                        StreamViewModel.Instance.StreamUserInfoModel.FollowerCount);
                }
            }
            if (this._StreamLiveEndedViewer != null)
            {
                this._StreamLiveEndedViewer.Dispose();
                this._StreamLiveEndedViewer = null;
            }

            this.imgStream.ClearValue(Image.SourceProperty);
            this.imgStream.Source = null;
            this.imgCall.ClearValue(Image.SourceProperty);
            this.imgCall.Source = null;

            StreamViewModel.Instance.DestroyStreamingChannel();
            StreamViewModel.Instance.DestroyStreamingInfo();

            this._StreamModel = null;
            this._StreamUserModel = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Property

        public bool IS_INITIALIZED
        {
            get { return _IS_INITIALIZED; }
            private set { _IS_INITIALIZED = value; }
        }

        public StreamModel StreamInfoModel
        {
            get { return _StreamModel; }
        }

        public bool IsSourceView
        {
            get { return _IsSourceView; }
            set
            {
                if (_IsSourceView == value)
                    return;

                _IsSourceView = value;
                this.OnPropertyChanged("IsSourceView");
            }
        }

        #endregion Property

    }
}

﻿using log4net;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.UI.PopUp.Stream;
using View.Utility;
using View.Utility.Stream;
using View.Utility.Wallet;
using View.ViewModel;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamGiftSlideShow.xaml
    /// </summary>
    public partial class UCStreamGiftSlideShow : UserControl, INotifyPropertyChanged, IDisposable
    {
        private ILog log = log4net.LogManager.GetLogger(typeof(UCStreamGiftSlideShow).Name);

        public delegate void OnSendHandler(WalletGiftProductModel productModel);
        public event OnSendHandler OnSend;

        private ICommand _SendCommand;
        private ICommand _CloseCommand;
        private ICommand _PrevoiusCommand;
        private ICommand _NextCommand;
        private WrapPanel _ImageItemContainer;
        private int _CurrentLeftMargin = 0;
        private WalletGiftProductModel selectedModel;

        #region Constructor

        public UCStreamGiftSlideShow()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Unloaded += (o, e) =>
            {
                if (selectedModel != null)
                {
                    selectedModel.IsSelected = false;
                }
            };
        }

        #endregion Constructor

        #region Command

        public ICommand SendCommand
        {
            get
            {
                if (_SendCommand == null)
                {
                    _SendCommand = new RelayCommand(param => OnSendClicked(param));

                }
                return _SendCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_CloseCommand == null)
                {
                    _CloseCommand = new RelayCommand((param) => OnCloseClicked(param));
                }
                return _CloseCommand;
            }
        }

        public ICommand PreviousCommand
        {
            get
            {
                if (_PrevoiusCommand == null)
                {
                    _PrevoiusCommand = new RelayCommand((param) => OnPreviousCommand());
                }
                return _PrevoiusCommand;
            }
        }

        public ICommand NextCommand
        {
            get
            {
                if (_NextCommand == null)
                {
                    _NextCommand = new RelayCommand((param) => OnNextCommand());
                }
                return _NextCommand;
            }
        }

        private ICommand _GiftClickedCommand;
        public ICommand GiftClickedCommand
        {
            get
            {
                if (_GiftClickedCommand == null)
                {
                    _GiftClickedCommand = new RelayCommand((param) => OnGiftClickedCommand(param));
                }
                return _GiftClickedCommand;
            }
        }



        #endregion Command

        #region Property

        private Visibility _IsLeftArrowVisible = Visibility.Hidden;
        public Visibility IsLeftArrowVisible
        {
            get { return _IsLeftArrowVisible; }
            set
            {
                if (value == _IsLeftArrowVisible) { return; }
                _IsLeftArrowVisible = value;
                OnPropertyChanged("IsLeftArrowVisible");
            }
        }

        private Visibility _IsRightArrowVisible = Visibility.Hidden;
        public Visibility IsRightArrowVisible
        {
            get { return _IsRightArrowVisible; }
            set
            {
                if (value == _IsRightArrowVisible) { return; }
                _IsRightArrowVisible = value;
                OnPropertyChanged("IsRightArrowVisible");
            }
        }

        private bool _IsSendButtonEnabled;
        public bool IsSendButtonEnabled
        {
            get { return _IsSendButtonEnabled; }
            set
            {
                if (value == _IsSendButtonEnabled) { return; }
                _IsSendButtonEnabled = value;
                OnPropertyChanged("IsSendButtonEnabled");
            }
        }

        #endregion

        #region Utility Methods

        public void InilializeViewer()
        {
            if (WalletViewModel.Instance.GiftProductsList.Count == 0)
            {
                new Utility.Wallet.ThreadGetGiftProducts().StartGetGifts();
            }
            //if (WalletViewModel.Instance.MyCoinStat.Quantity == 0)
            {
                new ThreadGetWalletInformation().StartThread();
            }
        }

        private void OnSendClicked(object param)
        {
            string value = (string)param;
            if (selectedModel != null && selectedModel.ProductPriceCoinQuantity > WalletViewModel.Instance.MyCoinStat.Quantity)
            {
                Grid parent = UCGuiRingID.Instance.MotherPanel;
                UCStreamLiveViewer viewer = StreamHelpers.GetCurruentStreamLiveViewer();
                if (viewer != null)
                {
                    parent = viewer.gridChatViewWrapper;
                }
                UCRechargePopUp rechargePopUp = new UCRechargePopUp();
                parent.Children.Add(rechargePopUp);
                rechargePopUp.onClosing += (isRechargeCliked) =>
                {
                    parent.Children.Remove(rechargePopUp);
                    if (isRechargeCliked)
                    {
                        //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowBuyCoinPanel();
                        Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(Models.Constants.WalletConstants.PU_COIN_RECHARGE_METHODS);
                    }
                };
            }
            else
            {
                if (selectedModel != null && OnSend != null)
                {
                    OnSend(selectedModel);
                }
            }
        }

        private void OnPreviousCommand()
        {
            try
            {
                int target = _CurrentLeftMargin + (int)pnlImageContainerWrapper.ActualWidth;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = target;
                this.SetNextPrevoiusButtonVisibility();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPreviousCommand() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnNextCommand()
        {
            try
            {
                int target = _CurrentLeftMargin - (int)pnlImageContainerWrapper.ActualWidth;
                AnimationOnArrowClicked(target, 0.35);
                _CurrentLeftMargin = target;
                this.SetNextPrevoiusButtonVisibility();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnNextCommand() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void AnimationOnArrowClicked(int target, double delay)
        {
            ThicknessAnimation MarginAnimation = new ThicknessAnimation();
            MarginAnimation.From = new Thickness(_CurrentLeftMargin, 0, 0, 0);
            MarginAnimation.To = new Thickness(target, 0, 0, 0);
            MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(MarginAnimation);
            Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
            Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
            storyboard.Begin(_ImageItemContainer);
        }

        private void SetNextPrevoiusButtonVisibility()
        {
            IsLeftArrowVisible = this._CurrentLeftMargin < 0 ? Visibility.Visible : Visibility.Hidden;
            IsRightArrowVisible = _CurrentLeftMargin > ((((float)itemsCntrl.Items.Count / 18) - 1) * pnlImageContainerWrapper.ActualWidth) * -1 ? Visibility.Visible : Visibility.Hidden;
        }

        private void OnCloseClicked(object param)
        {
            if (OnSend != null)
            {
                OnSend(null);
            }
        }

        public void Dispose()
        {
            this.DataContext = null;
            this.OnSend = null;
        }

        
        private void OnGiftClickedCommand(object param)
        {
            if (param is WalletGiftProductModel)
            {
                WalletGiftProductModel wgModel = (WalletGiftProductModel)param;
                if (selectedModel != null && selectedModel != wgModel)
                {
                    selectedModel.IsSelected = false;
                }

                wgModel.IsSelected = !wgModel.IsSelected;
                selectedModel = wgModel;
                IsSendButtonEnabled = wgModel.IsSelected ? true : false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region EventHandler

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
            eventArg.RoutedEvent = UIElement.MouseWheelEvent;
            eventArg.Source = sender;
            var parent = ((Control)sender).Parent as UIElement;
            parent.RaiseEvent(eventArg);
        }

        private void WrapPanel_Loaded(object sender, RoutedEventArgs e)
        {
            _ImageItemContainer = sender as WrapPanel;
            _ImageItemContainer.SizeChanged += (o, ee) =>
            {
                this.SetNextPrevoiusButtonVisibility();
            };
        }

        #endregion EventHandler

    }
}

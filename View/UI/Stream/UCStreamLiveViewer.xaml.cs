﻿using callsdkwrapper;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp.Stream;
using View.UI.StreamAndChannel;
using View.Utility;
using View.Utility.Call;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.GIF;
using View.Utility.Stream;
using View.Utility.Wallet;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCStreamLiveViewer.xaml
    /// </summary>
    public partial class UCStreamLiveViewer : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCStreamLiveViewer).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        private static object RingIDSettings = null;

        private UCStreamGiftSlideShow _GiftSlideShowPanel = null;
        private UCStreamLiveSourceViewer _StreamLiveSourcePanel = null;
        private UCStreamLiveCallViewer _StreamLiveCallPanel = null;
        public WNStreamLivePreview _StreamLivePreview = null;
        private Storyboard _CountDownStoryboard = null;
        private Storyboard _GiftViewStoryboard = null;

        public bool _IS_INITIALIZED = false;
        private bool _IsGiftPreviewMode = false;
        private bool _IsSeperateWindowMode = false;
        private bool _IsSourcePreviewMode = false;
        private bool _IsCallPreviewMode = false;
        private bool _IsGiftPreviewOpen = false;
        private long _PrevTableId = 0;
        private int? _CountDown;

        private ICommand _SendCommand;
        private ICommand _GiftCommand;
        private ICommand _StreamLikeCommand;
        private ICommand _ShareCommand;
        private ICommand _CancelCommand;
        private ICommand _SourceCommand;
        private ICommand _StreamContributionListCommand;
        private ICommand _PopUpCloseCommand;

        private ObservableCollection<StreamUserModel> _ViewerList = new ObservableCollection<StreamUserModel>();
        private ObservableCollection<MessageModel> _MessageList = new ObservableCollection<MessageModel>();
        private WalletGiftProductModel _GiftProductModel = new WalletGiftProductModel();

        #region Constructor

        static UCStreamLiveViewer()
        {
            if (Application.Current != null)
            {
                RingIDSettings = Application.Current.FindResource("RingIDSettings");
            }
        }

        public UCStreamLiveViewer()
        {
            InitializeComponent();
            this.DataContext = null;
            this.ViewerList = StreamViewModel.Instance.GetViewerListByPublisherID(StreamViewModel.Instance.StreamInfoModel.UserTableID);
            this.MessageList = StreamViewModel.Instance.GetMessageListByPublisherID(StreamViewModel.Instance.StreamInfoModel.UserTableID);
        }

        #endregion Constructor

        #region Event Handler

        static void OnStreamingStatusChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (o != null && e.NewValue != null)
                {
                    UCStreamLiveViewer panel = ((UCStreamLiveViewer)o);
                    int streamingStatus = (int)e.NewValue;

                    switch (streamingStatus)
                    {
                        case StreamConstants.STREAMING_CONNECTED:
                            log.Debug("*********************    STREAMING_CONNECTED      *********************");
                            panel.statusAnimation.StopAnimation();
                            StreamViewModel.Instance.StopStreamingInturruptTimer();
                            break;
                        case StreamConstants.STREAMING_CONNECTING:
                            log.Debug("*********************    STREAMING_CONNECTING     *********************");
                            panel.statusAnimation.StartAnimation();
                            break;
                        case StreamConstants.STREAMING_RECONNECTING:
                            log.Debug("*********************    STREAMING_RECONNECTING     *********************");
                            panel.statusAnimation.StartAnimation();
                            break;
                        case StreamConstants.STREAMING_POOR_NERWORK:
                            log.Debug("*********************    STREAMING_POOR_NERWORK   *********************");
                            panel.statusAnimation.StopAnimation();
                            break;
                        case StreamConstants.STREAMING_INTERRUPTED:
                            log.Debug("*********************    STREAMING_INTERRUPTED    *********************");
                            panel.statusAnimation.StopAnimation();
                            break;
                        case StreamConstants.STREAMING_NO_DATA:
                            log.Debug("*********************    STREAMING_NO_DATA        *********************");
                            panel.statusAnimation.StopAnimation();
                            StreamViewModel.Instance.StartStreamingInturruptTimer();
                            break;
                        case StreamConstants.STREAMING_FINISHED:
                            log.Debug("*********************    STREAMING_FINISHED       *********************");
                            panel.statusAnimation.StopAnimation();
                            StreamViewModel.Instance.StopStreamingInturruptTimer();
                            panel.ClearValue(UCStreamLiveViewer.StreamingStatusProperty);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnStreamingStatusChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CountDownStoryboard_Completed(object sender, EventArgs e)
        {
            if (this.CountDown == null) return;

            if (this.CountDown == 1)
            {
                this.CountDown = null;
                this._CountDownStoryboard.Completed -= CountDownStoryboard_Completed;
                this._CountDownStoryboard = null;
            }
            else
            {
                this._CountDownStoryboard.Begin();
                this.CountDown--;
            }
        }

        private void HideGiftViewStoryboar_Completed(object sender, EventArgs e)
        {
            if (this._GiftViewStoryboard != null)
            {
                this._GiftViewStoryboard.Completed -= HideGiftViewStoryboar_Completed;
                this._GiftViewStoryboard = null;
            }

            if (this._GiftSlideShowPanel != null)
            {
                this._GiftSlideShowPanel.Dispose();
                this._GiftSlideShowPanel = null;
            }
            this.IsGiftPreviewMode = false;
        }

        private void txtMsg_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (txtMsg.Text.Length > 4 && txtMsg.Text.Length % 5 == 0)
                {
                    ChatService.TypingStreamChat(StreamViewModel.Instance.StreamInfoModel.UserTableID);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: txtMsg_TextChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void txtMsg_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Return)
                {
                    if (!Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                    {
                        e.Handled = true;
                        OnSendClick(null);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: txtMsg_KeyDown() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void srvViewerList_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                srvViewerList.ScrollToHorizontalOffset(srvViewerList.HorizontalOffset + 110 > srvViewerList.ExtentWidth - srvViewerList.ViewportWidth ? srvViewerList.ExtentWidth - srvViewerList.ViewportWidth : srvViewerList.HorizontalOffset + 110);
            }
            else if (e.Delta < 0)
            {
                srvViewerList.ScrollToHorizontalOffset(srvViewerList.HorizontalOffset - 110 < 0 ? 0 : srvViewerList.HorizontalOffset - 110);
            }
        }

        #endregion Event Handler

        #region Utility Methods

        public void InilializeViewer()
        {
            if (this._IS_INITIALIZED == false)
            {
                this._IS_INITIALIZED = true;
                this.DataContext = this;

                if (StreamViewModel.Instance.ChannelType == StreamConstants.CHANNEL_OUT)
                {
                    DoubleAnimationUsingKeyFrames doubleAnimation = new DoubleAnimationUsingKeyFrames();
                    doubleAnimation.BeginTime = TimeSpan.FromMilliseconds(0);
                    doubleAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(0)), Value = 0 });
                    doubleAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(200)), Value = 0 });
                    for (int time = 0, size = 200; time <= 1000; time++)
                    {
                        doubleAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(time)), Value = size });
                        if (size <= 30)
                        {
                            size = 30;
                        }
                        else if (time % 5 == 0)
                        {
                            size--;
                        }
                    }
                    Storyboard.SetTarget(doubleAnimation, lblCountDown);
                    Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(TextBlock.FontSizeProperty));

                    this.CountDown = 3;
                    this._CountDownStoryboard = new Storyboard();
                    this._CountDownStoryboard.Children.Add(doubleAnimation);
                    this._CountDownStoryboard.Completed += CountDownStoryboard_Completed;
                    this._CountDownStoryboard.Begin();

                    ChatService.StartLiveStream(StreamViewModel.Instance.StreamInfoModel, (status, errormsg, reasonCode) =>
                    {
                        if (!status && _IS_INITIALIZED)
                        {
                            StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_FINISHED;

                            Application.Current.Dispatcher.BeginInvoke(() =>
                            {
                                if (this._CountDownStoryboard != null)
                                {
                                    this.CountDown = null;
                                    this._CountDownStoryboard.Stop();
                                    this._CountDownStoryboard.Completed -= CountDownStoryboard_Completed;
                                    this._CountDownStoryboard = null;
                                }

                                UIHelperMethods.ShowWarning(errormsg + " Please try again.", "Live Failed");
                                // CustomMessageBox.ShowError(errormsg + " Please try again.", "Live Failed");
                                UCStreamAndChannelViewer.Instance.Dispose();
                            });
                        }
                        return 0;
                    });

                    long utId = StreamViewModel.Instance.StreamInfoModel.UserTableID;
                    new ThrdStreamUserDetails(utId, (streamUserDTO) =>
                    {
                        if (this._IS_INITIALIZED)
                        {
                            if (streamUserDTO != null && StreamViewModel.Instance.StreamUserInfoModel != null && StreamViewModel.Instance.StreamUserInfoModel.UserTableID == utId)
                            {
                                StreamViewModel.Instance.StreamUserInfoModel.LoadData(streamUserDTO);
                            }
                        }
                        return 0;
                    }).Start();

                    StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_CONNECTED;
                    this.SetBinding(UCStreamLiveViewer.StreamingStatusProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamingStatus"), Source = RingIDSettings });
                }
                else
                {
                    StreamViewModel.Instance.InitStreamingSourceInfo(StreamConstants.SPEAKER, StreamConstants.MONITOR_SCREEN, null);
                    StreamViewModel.Instance.SetStreamingChannelSource();

                    bool isViewerRegisterSend = false;
                    if (!String.IsNullOrWhiteSpace(StreamViewModel.Instance.StreamInfoModel.PublisherServerIP) && !String.IsNullOrWhiteSpace(StreamViewModel.Instance.StreamInfoModel.ViewerServerIP))
                    {
#if CHAT_LOG
                        log.Debug("AUTH::STREAM::RESPONSE".PadRight(65, ' ') + "==>   publisherID = " + StreamViewModel.Instance.StreamInfoModel.UserTableID + ", streamID = " + StreamViewModel.Instance.StreamInfoModel.StreamID + ", userName = " + StreamViewModel.Instance.StreamInfoModel.UserName + ", profileImage = " + StreamViewModel.Instance.StreamInfoModel.ProfileImage + ", title = " + StreamViewModel.Instance.StreamInfoModel.Title + ", startTime = " + StreamViewModel.Instance.StreamInfoModel.StartTime + ", endTime = " + StreamViewModel.Instance.StreamInfoModel.EndTime + ", chatServerIP = " + StreamViewModel.Instance.StreamInfoModel.ChatServerIP + ", chatServerPort = " + StreamViewModel.Instance.StreamInfoModel.ChatServerPort + ", publisherServerIP = " + StreamViewModel.Instance.StreamInfoModel.PublisherServerIP + ", publisherServerPort = " + StreamViewModel.Instance.StreamInfoModel.PublisherServerPort + ", viewerServerIP = " + StreamViewModel.Instance.StreamInfoModel.ViewerServerIP + ", viewerServerPort = " + StreamViewModel.Instance.StreamInfoModel.ViewerServerPort);
#endif
                        isViewerRegisterSend = true;
                        CallHelperMethods.ViewerRegisterForLive(
                            StreamViewModel.Instance.StreamInfoModel.UserTableID,
                            StreamViewModel.Instance.StreamInfoModel.PublisherServerIP,
                            StreamViewModel.Instance.StreamInfoModel.PublisherServerPort,
                            StreamViewModel.Instance.StreamInfoModel.ViewerServerIP,
                            StreamViewModel.Instance.StreamInfoModel.ViewerServerPort,
                            RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName,
                            RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage,
                            StreamViewModel.Instance.StreamInfoModel.StreamID.ToString());
                    }

                    Guid streamID = StreamViewModel.Instance.StreamInfoModel.StreamID;
                    long utId = StreamViewModel.Instance.StreamInfoModel.UserTableID;
                    new ThrdStreamDetails(streamID, (streamDTO, streamUserDTO) =>
                    {
                        if (this._IS_INITIALIZED)
                        {
                            if (streamDTO != null && StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.StreamID.Equals(streamID))
                            {
                                StreamViewModel.Instance.StreamInfoModel.LoadData(streamDTO);
                            }
                            if (streamUserDTO != null && StreamViewModel.Instance.StreamUserInfoModel != null && StreamViewModel.Instance.StreamUserInfoModel.UserTableID == utId)
                            {
                                StreamViewModel.Instance.StreamUserInfoModel.LoadData(streamUserDTO);
                            }

                            if (streamDTO == null || streamDTO.EndTime > 0)
                            {
                                Application.Current.Dispatcher.BeginInvoke(() =>
                                {
                                    this._IS_INITIALIZED = false;
                                    UCStreamLiveWrapper ucStreamLiveWrapper = StreamHelpers.GetCurruentStreamLiveWrapper();
                                    if (ucStreamLiveWrapper != null)
                                    {
                                        ucStreamLiveWrapper.OpenLiveEndedViewer(false);
                                    }
                                }, DispatcherPriority.ApplicationIdle);
                            }
                            else
                            {
                                if (!isViewerRegisterSend)
                                {
#if CHAT_LOG
                                    log.Debug("AUTH::STREAM::RESPONSE".PadRight(65, ' ') + "==>   publisherID = " + StreamViewModel.Instance.StreamInfoModel.UserTableID + ", streamID = " + StreamViewModel.Instance.StreamInfoModel.StreamID + ", userName = " + StreamViewModel.Instance.StreamInfoModel.UserName + ", profileImage = " + StreamViewModel.Instance.StreamInfoModel.ProfileImage + ", title = " + StreamViewModel.Instance.StreamInfoModel.Title + ", startTime = " + StreamViewModel.Instance.StreamInfoModel.StartTime + ", endTime = " + StreamViewModel.Instance.StreamInfoModel.EndTime + ", chatServerIP = " + StreamViewModel.Instance.StreamInfoModel.ChatServerIP + ", chatServerPort = " + StreamViewModel.Instance.StreamInfoModel.ChatServerPort + ", publisherServerIP = " + StreamViewModel.Instance.StreamInfoModel.PublisherServerIP + ", publisherServerPort = " + StreamViewModel.Instance.StreamInfoModel.PublisherServerPort + ", viewerServerIP = " + StreamViewModel.Instance.StreamInfoModel.ViewerServerIP + ", viewerServerPort = " + StreamViewModel.Instance.StreamInfoModel.ViewerServerPort);
#endif
                                    CallHelperMethods.ViewerRegisterForLive(
                                        StreamViewModel.Instance.StreamInfoModel.UserTableID,
                                        StreamViewModel.Instance.StreamInfoModel.PublisherServerIP,
                                        StreamViewModel.Instance.StreamInfoModel.PublisherServerPort,
                                        StreamViewModel.Instance.StreamInfoModel.ViewerServerIP,
                                        StreamViewModel.Instance.StreamInfoModel.ViewerServerPort,
                                        RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName,
                                        RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage,
                                        StreamViewModel.Instance.StreamInfoModel.StreamID.ToString());
                                }

                                StreamViewModel.Instance.StreamInfoModel.ChatOn = !String.IsNullOrWhiteSpace(StreamViewModel.Instance.StreamInfoModel.ChatServerIP) && StreamViewModel.Instance.StreamInfoModel.ChatServerPort > 0;
                                if (StreamViewModel.Instance.StreamInfoModel.ChatOn)
                                {
                                    ChatService.RegisterStreamChat(
                                        StreamViewModel.Instance.StreamInfoModel.UserTableID,
                                        StreamViewModel.Instance.StreamInfoModel.StreamID.ToString(),
                                        StreamViewModel.Instance.StreamInfoModel.ChatServerIP,
                                        StreamViewModel.Instance.StreamInfoModel.ChatServerPort);
                                }
                            }
                        }
                        return 0;
                    }).Start();

                    StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_CONNECTING;
                    this.SetBinding(UCStreamLiveViewer.StreamingStatusProperty, new Binding { Path = new PropertyPath("StreamViewModel.StreamingStatus"), Source = RingIDSettings });
                }

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this.itcViewerList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("ViewerList") });
                    this.itcMessageList.SetBinding(System.Windows.Controls.ItemsControl.ItemsSourceProperty, new Binding { Path = new PropertyPath("MessageList") });
                }, DispatcherPriority.ApplicationIdle);

                this.txtMsg.KeyDown += txtMsg_KeyDown;
                this.txtMsg.TextChanged += txtMsg_TextChanged;
                this.srvViewerList.PreviewMouseWheel += srvViewerList_PreviewMouseWheel;
            }
        }

        private void OnSendClick(object param)
        {
            try
            {
                if (txtMsg.Text.Trim().Length > 0)
                {
                    MessageDTO messageDTO = new MessageDTO();
                    messageDTO.PublisherID = StreamViewModel.Instance.StreamInfoModel.UserTableID;
                    messageDTO.SenderTableID = DefaultSettings.LOGIN_TABLE_ID;
                    PacketTimeID packet = ChatService.GeneratePacketID();
                    messageDTO.PacketID = packet.PacketID;
                    messageDTO.StreamMsgDTO = new StreamMessageDTO { MessageType = StreamConstants.PLAIN_MESSAGE };
                    messageDTO.Message = txtMsg.Text;
                    messageDTO.MessageDate = packet.Time;
                    messageDTO.FullName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                    messageDTO.Status = ChatConstants.STATUS_SENDING;
                    StreamMsgLoadUtility.LoadMessageData(messageDTO);
                    ChatService.SendStreamChat(messageDTO.PacketID, messageDTO.PublisherID, messageDTO.MessageType, messageDTO.Message, messageDTO.MessageDate);
                    txtMsg.Text = String.Empty;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnSendClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnShareClick(object param)
        {
            popupStreamShare.IsOpen = true;
            popupStreamShare.Child.IsVisibleChanged += (o, e) =>
            {
                if ((bool)e.NewValue == true)
                {
                    popupStreamShare.Child.Focusable = true;
                    Keyboard.Focus(popupStreamShare.Child);
                }
                else
                {

                }
            };
        }

        private void OnCancelClick(object param)
        {
            if (StreamViewModel.Instance.ChannelType == StreamConstants.CHANNEL_OUT)
            {
                WNConfirmationView cv = new WNConfirmationView(string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Quit Live"), string.Format(NotificationMessages.SURE_WANT_TO, "quit live broadcasting"), CustomConfirmationDialogButtonOptions.YesNo, new[] { "Yes", "Cancel" });
                var result = cv.ShowCustomDialog();
                if (result == ConfirmationDialogResult.Yes)
                {
                    new ThrdEndLiveStream(StreamViewModel.Instance.StreamInfoModel.StreamID).Start();
                    StreamViewModel.Instance.StreamInfoModel.EndTime = ChatService.GetServerTime();

                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        this._IS_INITIALIZED = false;
                        UCStreamLiveWrapper ucStreamLiveWrapper = StreamHelpers.GetCurruentStreamLiveWrapper();
                        if (ucStreamLiveWrapper != null)
                        {
                            ucStreamLiveWrapper.OpenLiveEndedViewer();
                        }
                    }, DispatcherPriority.ApplicationIdle);
                }
            }
            else
            {
                UCStreamAndChannelViewer.Instance.Dispose();
            }
        }

        private void OnPopUpCloseCommand(object param)
        {
            popupStreamShare.IsOpen = false;
        }

        private void OnStreamContributionListCommand(object param)
        {
            UCContributorList contributionList = new UCContributorList();
            contributionList.ShowProfileInfo(gridChatViewWrapper);
            if (StreamViewModel.Instance.StreamUserInfoModel != null && StreamViewModel.Instance.StreamUserInfoModel.UserTableID > 0)
            {
                if (_PrevTableId != StreamViewModel.Instance.StreamUserInfoModel.UserTableID)
                {
                    StreamViewModel.Instance.SreamContributorList.Clear();
                }
                _PrevTableId = StreamViewModel.Instance.StreamUserInfoModel.UserTableID;
                new ThreadGetTopContributorsList().StartGetTopContributors(StreamViewModel.Instance.StreamUserInfoModel.UserTableID);
            }
            else
            {
                log.Info("Failed to fetch StreamContributionListCommand... => ");
            }
        }

        public void OnPublisherNotAvailable()
        {
            if (StreamViewModel.Instance.StreamInfoModel.EndTime == 0)
            {
                new ThrdEndLiveStream(StreamViewModel.Instance.StreamInfoModel.StreamID).Start();
                StreamViewModel.Instance.StreamInfoModel.EndTime = ChatService.GetServerTime();
            }

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this._IS_INITIALIZED = false;
                UCStreamLiveWrapper ucStreamLiveWrapper = StreamHelpers.GetCurruentStreamLiveWrapper();
                if (ucStreamLiveWrapper != null)
                {
                    ucStreamLiveWrapper.OpenLiveEndedViewer();
                }
            }, DispatcherPriority.ApplicationIdle);
        }

        public void OnInternetNotAvailable()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {

                WNConfirmationView cv = new WNConfirmationView("Poor network", "No internet! Please check your network connectivity?", CustomConfirmationDialogButtonOptions.YesNo, new[] { "Retry", "Ok" });
                var result = cv.ShowCustomDialog();
                if (result == ConfirmationDialogResult.No)
                {
                    if (StreamViewModel.Instance.ChannelType == StreamConstants.CHANNEL_OUT)
                    {
                        if (StreamViewModel.Instance.StreamInfoModel.EndTime == 0)
                        {
                            new ThrdEndLiveStream(StreamViewModel.Instance.StreamInfoModel.StreamID).Start();
                            StreamViewModel.Instance.StreamInfoModel.EndTime = ChatService.GetServerTime();
                        }
                    }

                    this._IS_INITIALIZED = false;
                    UCStreamLiveWrapper ucStreamLiveWrapper = StreamHelpers.GetCurruentStreamLiveWrapper();
                    if (ucStreamLiveWrapper != null)
                    {
                        ucStreamLiveWrapper.OpenLiveEndedViewer();
                    }
                }
            }, DispatcherPriority.ApplicationIdle);
        }

        private void AnimationOnShowGiftPreview(int target, double delay)
        {
            try
            {
                ThicknessAnimation MarginAnimation = new ThicknessAnimation();
                MarginAnimation.From = new Thickness(0, 0, 0, pnlGiftPreviewContainer.Margin.Bottom);
                MarginAnimation.To = new Thickness(0, 0, 0, target);
                MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));

                _GiftViewStoryboard = new Storyboard();
                _GiftViewStoryboard.Children.Add(MarginAnimation);
                Storyboard.SetTarget(MarginAnimation, pnlGiftPreviewContainer);
                Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(StackPanel.MarginProperty));
                if (target == -200)
                {
                    _GiftViewStoryboard.Completed += HideGiftViewStoryboar_Completed;
                }
                _GiftViewStoryboard.Begin(pnlGiftPreviewContainer);
            }
            catch (Exception ex)
            {
                log.Error("Error: AnimationOnArrowClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void MoveMessageListToBottom(MessageDTO newDTO)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (newDTO.SenderTableID == DefaultSettings.LOGIN_TABLE_ID || MessageList.Count <= 3 || (srvMessageList.ExtentHeight - srvMessageList.VerticalOffset) < (srvMessageList.ViewportHeight + 70))
                    {
                        this.srvMessageList.ScrollToBottom();
                    }
                });
            }
            catch (Exception ex)
            {
                log.Error("Error: MoveMessageListToBottom() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void MoveViewerListToLeftEnd()
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this.srvViewerList.ScrollToLeftEnd();
                });
            }
            catch (Exception ex)
            {
                log.Error("Error: MoveViewerListToLeftEnd() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void InitGiftGif(WalletGiftProductModel giftProductModel)
        {
            if (giftProductModel != null)
            {
                string imageName = giftProductModel.ProductName + StreamConstants.STREAM_GIFT_EXTENSION_GIF;
                string imagePath = View.Constants.RingIDSettings.STREAM_GIFT_FOLDER + System.IO.Path.DirectorySeparatorChar + imageName;
                giftProductModel.GiftFilePath = imagePath;

                if (System.IO.File.Exists(giftProductModel.GiftFilePath))
                {
                    StreamGiftSendReceive.Instance.StartThread(giftProductModel);
                }
                else
                {
                    new ThrdDownloadGift(giftProductModel, View.Constants.RingIDSettings.STREAM_GIFT_FOLDER, ".gif", (giftproduct) =>
                    {
                        if (giftproduct != null && System.IO.File.Exists(giftproduct.GiftFilePath))
                        {
                            StreamGiftSendReceive.Instance.StartThread(giftProductModel);
                        }
                        return 0;
                    }).StartThread();
                }
            }
        }

        public void Dispose()
        {
            this._IS_INITIALIZED = false;

            this.HideWindowPreview();
            this.HideCallPreview();

            if (StreamViewModel.Instance.CallerInfoModel != null)
            {
                switch (StreamViewModel.Instance.CallStatus)
                {
                    case StreamConstants.STREAM_CALL_INCOMING:
                        CallHelperMethods.BusyLiveCall(StreamViewModel.Instance.CallerInfoModel.UserTableID);
                        break;
                    case StreamConstants.STREAM_CALL_OUTGOING:
                    case StreamConstants.STREAM_CALL_CONNECTED:
                        CallHelperMethods.ByeLiveCall(StreamViewModel.Instance.CallerInfoModel.UserTableID);
                        break;
                }
            }

            StreamViewModel.Instance.DestroyCallChannel();
            StreamViewModel.Instance.DestroyCallInfo();

            this.txtMsg.KeyDown -= txtMsg_KeyDown;
            this.txtMsg.TextChanged -= txtMsg_TextChanged;
            this.srvViewerList.PreviewMouseWheel -= srvViewerList_PreviewMouseWheel;
            this.HideGiftPreview();

            if (StreamViewModel.Instance.ChannelType == StreamConstants.CHANNEL_OUT)
            {
                CallHelperMethods.PublisherUnregister(
                    StreamViewModel.Instance.StreamInfoModel.StreamID.ToString(),
                    StreamViewModel.Instance.StreamInfoModel.ViewCount,
                    StreamViewModel.Instance.StreamUserInfoModel.CoinCount,
                    StreamViewModel.Instance.StreamUserInfoModel.FollowerCount);
            }
            else
            {
                CallHelperMethods.ViewerUnregister(StreamViewModel.Instance.StreamInfoModel.UserTableID);
            }
            ChatService.UnRegisterStreamChat(StreamViewModel.Instance.StreamInfoModel.UserTableID, StatusConstants.PRESENCE_ONLINE, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.Mood);

            if (this._CountDownStoryboard != null)
            {
                this._CountDownStoryboard.Completed -= CountDownStoryboard_Completed;
                this._CountDownStoryboard.Stop();
                this._CountDownStoryboard = null;
            }

            this.bubbleAnimation.Close();

            this.ClearValue(UCStreamLiveViewer.StreamingStatusProperty);
            this.itcViewerList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
            this.itcViewerList.ItemsSource = null;
            this.itcMessageList.ClearValue(System.Windows.Controls.ItemsControl.ItemsSourceProperty);
            this.itcMessageList.ItemsSource = null;
            this.ViewerList.Clear();
            this.MessageList.Clear();
            StreamViewModel.Instance.ViewerList.TryRemove(StreamViewModel.Instance.StreamInfoModel.UserTableID);
            StreamViewModel.Instance.MessageList.TryRemove(StreamViewModel.Instance.StreamInfoModel.UserTableID);
            this.DataContext = null;
            this.CountDown = null;
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region Popup

        public void ShowWindowPreview()
        {
            this.IsSeperateWindowMode = true;
            if (this._StreamLivePreview == null)
            {
                this._StreamLivePreview = new WNStreamLivePreview();
                this._StreamLivePreview.ShowWindow(IsCallPreviewMode);
            }
        }

        public void HideWindowPreview()
        {
            this.IsSeperateWindowMode = false;
            if (this._StreamLivePreview != null)
            {
                this._StreamLivePreview.CloseWindow();
                this._StreamLivePreview = null;
            }
        }

        public void ShowCallPreview()
        {
            try
            {
                this._StreamLiveCallPanel = new UCStreamLiveCallViewer();
                this._StreamLiveCallPanel.OnClose += () =>
                {
                    HideCallPreview();
                };
                this.pnlCallPreviewContainer.Child = this._StreamLiveCallPanel;
                this.IsCallPreviewMode = true;

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this._StreamLiveCallPanel.InilializeViewer();
                });
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowCallPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void HideCallPreview()
        {
            try
            {
                if (this._StreamLiveCallPanel != null)
                {
                    this._StreamLiveCallPanel.Dispose();
                    this._StreamLiveCallPanel = null;
                }
                this.IsCallPreviewMode = false;
            }
            catch (Exception ex)
            {
                log.Error("Error: HideCallPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnStreamLikeCommand()
        {
            if (StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
            {
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.PublisherID = StreamViewModel.Instance.StreamInfoModel.UserTableID;
                messageDTO.Message = StreamViewModel.Instance.StreamInfoModel.ProfileImage;
                messageDTO.StreamMsgDTO = new StreamMessageDTO { MessageType = StreamConstants.LIKE_MESSAGE };
                messageDTO.FullName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                PacketTimeID packet = ChatService.GeneratePacketID();
                messageDTO.PacketID = packet.PacketID;
                messageDTO.MessageDate = packet.Time;
                StreamMsgLoadUtility.LoadMessageData(messageDTO);

                this.bubbleAnimation.AddBubble();

                new ThrdUpdateStreamingLikeCount(StreamViewModel.Instance.StreamInfoModel.StreamID, (isSucces, message, likeCount) =>
                {
                    if (isSucces)
                    {
                        if (likeCount > StreamViewModel.Instance.StreamInfoModel.LikeCount)
                        {
                            StreamViewModel.Instance.StreamInfoModel.LikeCount = likeCount;
                        }

                        CallHelperMethods.SendLike(StreamViewModel.Instance.StreamInfoModel.UserTableID, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage, (int)likeCount);
                    }
                    return 0;
                }).Start();
            }
        }

        private void ShowGiftPreview(object param)
        {
            try
            {
                /* hide if already running*/
                if (this._GiftViewStoryboard != null)
                {
                    this._GiftViewStoryboard.Completed -= HideGiftViewStoryboar_Completed;
                    this._GiftViewStoryboard.Stop();
                    this._GiftViewStoryboard = null;
                }
                /* hide if already running*/

                if (this._GiftSlideShowPanel != null)
                {
                    this._GiftSlideShowPanel.Dispose();
                    this._GiftSlideShowPanel = null;
                }

                this._GiftSlideShowPanel = new UCStreamGiftSlideShow();
                this._GiftSlideShowPanel.OnSend += (productModel) =>
                {
                    if (productModel != null && StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
                    {

                        MessageDTO messageDTO = new MessageDTO();
                        messageDTO.PublisherID = StreamViewModel.Instance.StreamInfoModel.UserTableID;
                        messageDTO.Message = productModel.ProductID.ToString();
                        messageDTO.StreamMsgDTO = new StreamMessageDTO { MessageType = StreamConstants.GIFT_MESSAGE };
                        messageDTO.FullName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                        messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                        PacketTimeID packet = ChatService.GeneratePacketID();
                        messageDTO.PacketID = packet.PacketID;
                        messageDTO.MessageDate = packet.Time;
                        StreamMsgLoadUtility.LoadMessageData(messageDTO);

                        productModel.GiftSenderTableId = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.UserTableID;
                        productModel.GiftSenderName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                        productModel.GiftSenderProfileImage = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage;
                        List<WalletGiftProductModel> selectedGiftList = new List<WalletGiftProductModel>();
                        selectedGiftList.Add(productModel);
                        if (!(StreamGiftSendReceive._Instance != null && StreamGiftSendReceive._Instance._IsReady == false))
                        {
                            StreamGiftSendReceive.Instance.AddGift(productModel);
                            this.GiftProductModel = productModel;
                            this.InitGiftGif(productModel);
                        }

                        if (StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
                        {
                            new ThreadSendGift().StartSendGift(StreamViewModel.Instance.StreamInfoModel.UserTableID, selectedGiftList, (isSucces, message) =>
                            {
                                if (isSucces)
                                {
                                    CallHelperMethods.SendGifts(StreamViewModel.Instance.StreamInfoModel.UserTableID, (long)productModel.ProductID, productModel.ProductName, (int)productModel.ProductPriceCoinQuantity, productModel.ProductUrl, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage);
                                }
                                else if (!string.IsNullOrEmpty(message))
                                {
                                    UIHelperMethods.ShowWarning(message, "Gift preview");
                                    //CustomMessageBox.ShowError(message);
                                }
                                return 0;
                            });
                        }
                    }
                    this.HideGiftPreview(0.35);
                };
                this.pnlGiftPreviewContainer.Child = this._GiftSlideShowPanel;
                this.IsGiftPreviewMode = true;
                this.AnimationOnShowGiftPreview(0, 0.35);

                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this._GiftSlideShowPanel.InilializeViewer();
                });
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowGiftPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void HideGiftPreview(double delay = 0)
        {
            try
            {
                /* hide if already running*/
                if (this._GiftViewStoryboard != null)
                {
                    this._GiftViewStoryboard.Completed -= HideGiftViewStoryboar_Completed;
                    this._GiftViewStoryboard.Stop();
                    this._GiftViewStoryboard = null;
                }
                /* hide if already running*/

                this.AnimationOnShowGiftPreview(-200, delay);
            }
            catch (Exception ex)
            {
                log.Error("Error: HideGiftPreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ShowSourcePreview(object param)
        {
            try
            {
                this._StreamLiveSourcePanel = new UCStreamLiveSourceViewer(() =>
                {
                    HideSourcePreview();
                    return 0;
                });
                this._StreamLiveSourcePanel.InilializeViewer();
                this.pnlSourcePreviewContainer.Child = this._StreamLiveSourcePanel;
                this.IsSourcePreviewMode = true;
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowSourcePreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void HideSourcePreview()
        {
            try
            {
                if (this._StreamLiveSourcePanel != null)
                {
                    this._StreamLiveSourcePanel.Dispose();
                    this._StreamLiveSourcePanel = null;
                }
                this.IsSourcePreviewMode = false;
            }
            catch (Exception ex)
            {
                log.Error("Error: HideSourcePreview() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowGiftImage(string giftFilePath)
        {
            IsGiftPreviewOpen = true;
            if (!string.IsNullOrEmpty(giftFilePath))
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    BitmapImage _LoadingIcon = ImageUtility.GetBitmapImageOfDynamicResource(giftFilePath);
                    imgGift.Source = _LoadingIcon;
                    ImageBehavior.SetAnimatedSource(imgGift, _LoadingIcon);
                }, DispatcherPriority.Send);
            }
        }

        public void HideGiftImage()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                IsGiftPreviewOpen = false;
                imgGift.Source = null;
                ImageBehavior.SetAnimatedSource(imgGift, null);
            }, DispatcherPriority.Send);
        }

        #endregion Popup

        #region Command

        public ICommand SendCommand
        {
            get
            {
                if (_SendCommand == null)
                {
                    _SendCommand = new RelayCommand((param) => OnSendClick(param));
                }
                return _SendCommand;
            }
        }

        public ICommand GiftCommand
        {
            get
            {
                if (_GiftCommand == null)
                {
                    _GiftCommand = new RelayCommand((param) => ShowGiftPreview(param));
                }
                return _GiftCommand;
            }
        }

        public ICommand StreamLikeCommand
        {
            get
            {
                if (_StreamLikeCommand == null)
                {
                    _StreamLikeCommand = new RelayCommand(param => OnStreamLikeCommand());
                }
                return _StreamLikeCommand;
            }
        }

        public ICommand ShareCommand
        {
            get
            {
                if (_ShareCommand == null)
                {
                    _ShareCommand = new RelayCommand((param) => OnShareClick(param));
                }
                return _ShareCommand;
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new RelayCommand((param) => OnCancelClick(param));
                }
                return _CancelCommand;
            }
        }

        public ICommand SourceCommand
        {
            get
            {
                if (_SourceCommand == null)
                {
                    _SourceCommand = new RelayCommand((param) => ShowSourcePreview(param));
                }
                return _SourceCommand;
            }
        }

        public ICommand StreamContributionListCommand
        {
            get
            {
                if (_StreamContributionListCommand == null)
                {
                    _StreamContributionListCommand = new RelayCommand(param => OnStreamContributionListCommand(param));
                }
                return _StreamContributionListCommand;
            }
        }

        public ICommand PopUpCloseCommand
        {
            get
            {
                if (_PopUpCloseCommand == null)
                {
                    _PopUpCloseCommand = new RelayCommand(param => OnPopUpCloseCommand(param));
                }
                return _PopUpCloseCommand;
            }
        }

        #endregion

        #region Property

        public static readonly DependencyProperty StreamingStatusProperty = DependencyProperty.Register("StreamingStatus", typeof(int), typeof(UCStreamLiveViewer), new PropertyMetadata(StreamConstants.STREAMING_FINISHED, OnStreamingStatusChanged));

        public bool StreamingStatus
        {
            get { return (bool)GetValue(StreamingStatusProperty); }
            set
            {
                SetValue(StreamingStatusProperty, value);
            }
        }

        public ObservableCollection<StreamUserModel> ViewerList
        {
            get { return _ViewerList; }
            set
            {
                _ViewerList = value;
                this.OnPropertyChanged("ViewerList");
            }
        }

        public ObservableCollection<MessageModel> MessageList
        {
            get { return _MessageList; }
            set
            {
                _MessageList = value;
                this.OnPropertyChanged("MessageList");
            }
        }

        public int? CountDown
        {
            get { return _CountDown; }
            set
            {
                if (_CountDown == value)
                    return;

                _CountDown = value;
                this.OnPropertyChanged("CountDown");
            }
        }

        public bool IsGiftPreviewMode
        {
            get { return _IsGiftPreviewMode; }
            set
            {
                if (_IsGiftPreviewMode == value)
                    return;
                _IsGiftPreviewMode = value;
                this.OnPropertyChanged("IsGiftPreviewMode");
            }
        }

        public bool IsSourcePreviewMode
        {
            get { return _IsSourcePreviewMode; }
            set
            {
                if (_IsSourcePreviewMode == value)
                    return;
                _IsSourcePreviewMode = value;
                this.OnPropertyChanged("IsSourcePreviewMode");
            }
        }

        public bool IsCallPreviewMode
        {
            get { return _IsCallPreviewMode; }
            set
            {
                if (_IsCallPreviewMode == value)
                    return;
                _IsCallPreviewMode = value;
                this.OnPropertyChanged("IsCallPreviewMode");
            }
        }

        public bool IsSeperateWindowMode
        {
            get { return _IsSeperateWindowMode; }
            set
            {
                if (_IsSeperateWindowMode == value)
                    return;
                _IsSeperateWindowMode = value;
                this.OnPropertyChanged("IsSeperateWindowMode");
            }
        }

        public bool IsGiftPreviewOpen
        {
            get { return _IsGiftPreviewOpen; }
            set
            {
                if (_IsGiftPreviewOpen == value)
                    return;
                _IsGiftPreviewOpen = value;
                this.OnPropertyChanged("IsGiftPreviewOpen");
            }
        }

        public WalletGiftProductModel GiftProductModel
        {
            get { return _GiftProductModel; }
            set
            {
                _GiftProductModel = value;
                this.OnPropertyChanged("GiftProductModel");
            }
        }

        #endregion Property

    }
}

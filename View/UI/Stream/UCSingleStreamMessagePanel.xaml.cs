﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCSingleStreamMessagePanel.xaml
    /// </summary>
    public partial class UCSingleStreamMessagePanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleStreamMessagePanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private bool disposed = false;

        public UCSingleStreamMessagePanel()
        {
            InitializeComponent();
        }

        ~UCSingleStreamMessagePanel()
        {
            Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleStreamMessagePanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleStreamMessagePanel panel = ((UCSingleStreamMessagePanel)o);
                object data = panel.DataContext;

                if (e.NewValue != null && (bool)e.NewValue)
                {
                    panel.BindPreview((MessageModel)data);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindPreview(MessageModel msgModel)
        {
            if (msgModel.StreamMsgModel.MessageType == StreamConstants.PLAIN_MESSAGE)
            {
                tbPlainMsgFullName.SetBinding(Run.TextProperty, new Binding { Path = new PropertyPath("FullName") });
                tbPlainMessage.SetBinding(Run.TextProperty, new Binding { Path = new PropertyPath("Message") });
            }

            else if (msgModel.StreamMsgModel.MessageType == StreamConstants.LIKE_MESSAGE)
            {
                tbLikeMsgFullName.SetBinding(Run.TextProperty, new Binding { Path = new PropertyPath("FullName") });

            }
            else if (msgModel.StreamMsgModel.MessageType == StreamConstants.GIFT_MESSAGE)
            {
                tbGiftMsgFullName.SetBinding(Run.TextProperty, new Binding { Path = new PropertyPath("FullName") });
                MultiBinding imageBinding = new MultiBinding();
                imageBinding.Converter = new StreamGiftImageConverter();
                imageBinding.ConverterParameter = StreamConstants.STREAM_GIFT_EXTENSION_PNG;
                imageBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath(""),
                });
                imageBinding.Bindings.Add(new Binding
                {
                    Path = new PropertyPath("Message"),
                });
                imgGift.SetBinding(Image.SourceProperty, imageBinding);
            }

            else if (msgModel.StreamMsgModel.MessageType == StreamConstants.FOLLOW_MESSAGE)
            {
                tbFollowMsgFullName.SetBinding(Run.TextProperty, new Binding { Path = new PropertyPath("FullName") });
            }
            else if (msgModel.StreamMsgModel.MessageType == StreamConstants.SHARE_MESSAGE)
            {
                tbPlainMsgFullName.SetBinding(Run.TextProperty, new Binding { Path = new PropertyPath("FullName") });
                tbShareMessage.SetBinding(Run.TextProperty, new Binding { Path = new PropertyPath("Message") });
            }
        }

        private void ClearPreview()
        {
            tbPlainMsgFullName.ClearValue(Run.TextProperty);
            tbPlainMessage.ClearValue(Run.TextProperty);
            tbLikeMsgFullName.ClearValue(Run.TextProperty);
            tbGiftMsgFullName.ClearValue(Run.TextProperty);
            imgGift.ClearValue(Image.SourceProperty);
            tbFollowMsgFullName.ClearValue(Run.TextProperty);
            tbPlainMsgFullName.ClearValue(Run.TextProperty);
            tbShareMessage.ClearValue(Run.TextProperty);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    if (disposing)
                    {
                        this.ClearPreview();
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.BindingModels;
using View.Converter;
using View.Utility;
using View.Utility.Stream;

namespace View.UI.Stream
{
    /// <summary>
    /// Interaction logic for UCSingleStreamPanel.xaml
    /// </summary>
    public partial class UCSingleStreamPanel : UserControl, INotifyPropertyChanged, IDisposable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(UCSingleStreamPanel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private bool disposed = false;

        public UCSingleStreamPanel()
        {
            InitializeComponent();
        }

        ~UCSingleStreamPanel()
        {
           Dispose(false);
        }

        #region Property

        public static readonly DependencyProperty IsThumbViewProperty = DependencyProperty.Register("IsThumbView", typeof(bool), typeof(UCSingleStreamPanel), new PropertyMetadata(false));

        public bool IsThumbView
        {
            get { return (bool)GetValue(IsThumbViewProperty); }
            set
            {
                SetValue(IsThumbViewProperty, value);
            }
        }

        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(UCSingleStreamPanel), new PropertyMetadata(false, OnOpenChanged));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set
            {
                SetValue(IsOpenedProperty, value);
            }
        }

        #endregion Property

        #region Event Handler

        static void OnOpenChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UCSingleStreamPanel panel = ((UCSingleStreamPanel)o);
                object data = panel.DataContext;

                if (e.NewValue != null && (bool)e.NewValue)
                {
                    panel.BindPreview((StreamModel)data);
                }
                else
                {
                    panel.Dispose(true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnOpenChanged => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Event Handler

        #region Utility Method

        private void BindPreview(StreamModel model)
        {
            MultiBinding sourceBinding = new MultiBinding { Converter = new StreamProfileImageConverter(), ConverterParameter = this.IsThumbView ? ImageUtility.IMG_CROP : ImageUtility.IMG_600 };
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath(""),
            });
            sourceBinding.Bindings.Add(new Binding
            {
                Path = new PropertyPath("ProfileImage"),
            });
            imgControl.SetBinding(Image.SourceProperty, sourceBinding);

            if (this.IsThumbView)
            {
                lblFullNameThumb.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("UserName") });
            }
            else
            {
                lblTitle.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Title") });
                lblFullName.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("UserName") });
                lblLocation.SetBinding(TextBlock.TextProperty, new Binding { Path = new PropertyPath("Country") });
            }

            MouseBinding mouseBinding = new MouseBinding();
            mouseBinding.Gesture = new MouseGesture(MouseAction.LeftClick);
            mouseBinding.Command = StreamViewModel.Instance.StreamViewCommand;
            mouseBinding.CommandParameter = model;
            imgControl.InputBindings.Add(mouseBinding);
        }

        private void ClearPreview()
        {
            imgControl.ClearValue(Image.SourceProperty);
            imgControl.Source = null;
            imgControl.InputBindings.Clear();
            lblTitle.ClearValue(TextBlock.TextProperty);
            lblFullName.ClearValue(TextBlock.TextProperty);
            lblFullNameThumb.ClearValue(TextBlock.TextProperty);
            lblLocation.ClearValue(TextBlock.TextProperty);
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    // Release unmanaged resources.
                    if (disposing)
                    {
                        this.ClearPreview();
                        this.imgControl = null;
                    }
                    disposed = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispose() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

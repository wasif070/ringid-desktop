﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI.AddFriend;
using View.UI.Celebrities;
using View.UI.Channel;
using View.UI.Chat;
using View.UI.Circle;
using View.UI.Dialer;
using View.UI.Feed;
using View.UI.Followinglist;
using View.UI.Group;
using View.UI.MediaCloud;
//using View.UI.MediaFeed;
//using View.UI.MusicAndVideo;
using View.UI.NewsPortal;
using View.UI.Pages;
using View.UI.Profile.CelebrityProfile;
using View.UI.Profile.FriendProfile;
using View.UI.Profile.MusicPageProfile;
using View.UI.Profile.MyProfile;
using View.UI.Profile.NewsPortalProfile;
using View.UI.Profile.PageProfile;
using View.UI.RingMarket.MenuMarketSticker;
using View.UI.Room;
using View.UI.SavedFeed;
using View.UI.StreamAndChannel;
using View.UI.Wallet;
using View.Utility;
using View.Utility.Circle;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.FriendProfile;
using View.Utility.Myprofile;
using View.ViewModel;

namespace View.UI
{
    /// <summary>
    /// Interaction logic for MainMiddleControl.xaml
    /// </summary>
    public partial class UCMiddlePanelSwitcher : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCMiddlePanelSwitcher).Name);
        private ThrdCheckForUpdates thrdCheckForUpdates;

        #region Properties

        public int PreviousType { get; set; }
        public static UCAllFeeds View_UCAllFeeds { get; set; }
        public static UCMyProfile View_UCMyProfile { get; set; }
        public static UCFriendChatCallPanel View_UCFriendChatCallPanel { get; set; }
        public static UCFriendProfile View_UCFriendProfilePanel { get; set; }
        public static UCFriendChatSettings View_UCFriendChatSettingsPanel { get; set; }
        public static UCGroupChatCallPanel View_UCGroupChatCallPanel { get; set; }
        public static UCGroupChatSettings View_UCGroupChatSettingsPanel { get; set; }
        public static UCDialer View_UCDialer { get; set; }
        public static UCAddFriendMainPanel View_UCAddFriendMainPanel { get; set; }
        public static UCMyCoverPhotoChange View_UCMyCoverPictureChange { get; set; }
        public static UCCircleSwitcher View_UCCircleSwitcher { get; set; }
        public static UCCircleInitPanel View_UCCircleInitPanel { get; set; }
        public static UCCreateCirclePanel View_UCCreateCirclePanel { get; set; }
        public static UCCirclePanel View_UCCirclePanel { get; set; }
        public static UCMediaCloudMainPanel View_UCMediaCloudMainPanel { get; set; }
        public static UCMediaCloudSearchMainPanel _UCMediaCloudSearch { get; set; }
        private List<ProfilesVisitedDTO> ProfilesVisited = new List<ProfilesVisitedDTO>();
        public static UCMenuStickerWrapper View_MenuStickerWrapper { get; set; }
        public static UCMenuGroupList View_UCMenuGroupList { get; set; }
        public static UCRoomListPanel View_UCRoomListPanel { get; set; }
        public static UCRoomChatCallPanel View_UCRoomChatCallPanel { get; set; }
        public static UCRoomChatSettings View_UCRoomChatSettingsPanel { get; set; }
        public static UCNewsPortalMainPanel View_UCNewsPortalMainPanel { get; set; }
        public static UCNewsPortalSearchPanel View_UCNewsPortalSearchPanel { get; set; }
        public static UCPagesMainPanel View_UCPagesMainPanel { get; set; }
        public static UCPagesSearchPanel View_UCPagesSearchPanel { get; set; }
        public static UCCelebritiesMainPanel View_UCCelebritiesMainPanel { get; set; }
        public static UCCelebritiesSearchPanel _UCCelebritiesSearchPanel { get; set; }
        public static UCSavedFeedPanel View_UCSavedFeedPanel { get; set; }
        public static UCFollowingMainPanel View_UCFollowingMainPanel { get; set; }
        public static UCNewsPortalProfile View_UCNewsPortalProfile;
        public static UCPageProfile View_UCPageProfile;
        public static UCMediaPageProfile View_MediaPageProfile;
        public static UCCelebrityProfile View_CelebrityProfile;
        public static UCStreamAndChannelWrapper View_UCStreamAndChannel { get; set; }
        public static UCMyChannelWrapper View_UCMyChannel { get; set; }
        public static UCMyProfile InstanceOfMyProfile
        {
            get
            {
                if (View_UCMyProfile == null) View_UCMyProfile = new UCMyProfile();
                return View_UCMyProfile;
            }
        }
        public static UCMyCoverPhotoChange InstanceOfMyCoverPicture
        {
            get
            {
                if (View_UCMyCoverPictureChange == null) View_UCMyCoverPictureChange = new UCMyCoverPhotoChange();
                return View_UCMyCoverPictureChange;
            }
        }
        private UCCircleSwitcher GetCircleSwitcher
        {
            get
            {
                if (View_UCCircleSwitcher == null) View_UCCircleSwitcher = new UCCircleSwitcher();
                return View_UCCircleSwitcher;
            }
        }
        public static UCWalletMainPanel View_UCWalletMainPanel { get; set; }
        private CallerDTO CallData
        {
            get { return MainSwitcher.CallController.CallData; }
        }

        #endregion "Properties"

        #region Constructor

        public UCMiddlePanelSwitcher()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion Constructor

        #region "Commands and Command Methods"

        private ICommand loadedMainControl;
        public ICommand LoadedThis
        {
            get
            {
                if (loadedMainControl == null) loadedMainControl = new RelayCommand(param => OnLoadedMainControl());
                return loadedMainControl;
            }
        }
        private void OnLoadedMainControl()
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeAllFeeds);
            if (thrdCheckForUpdates == null) thrdCheckForUpdates = new ThrdCheckForUpdates();
            thrdCheckForUpdates.StartChecking(false);
        }
        #endregion

        #region Utility Method

        public void InsertTabIndexInProfilesVisited(int idx, long frndId, long circleId)
        {
            ProfilesVisitedDTO dto = new ProfilesVisitedDTO();
            if (frndId > 0)
            {
                dto.Type = MiddlePanelConstants.TypeFriendProfile;
                dto.TabIndx = idx;
                dto.ID = frndId;
            }
            else if (circleId > 0)
            {
                dto.Type = MiddlePanelConstants.TypeCirclePanel;
                dto.TabIndx = idx;
                dto.ID = circleId;
            }
            else if (frndId < 0)
            {
                dto.Type = MiddlePanelConstants.TypeMyProfile;
                dto.TabIndx = idx;
            }
            else
            {
                dto.Type = MiddlePanelConstants.TypeMediaFeed;
            }
            //TODO celeb tabs
            ProfilesVisitedDTO lastDto = ProfilesVisited.LastOrDefault();
            if (lastDto.Type == dto.Type && lastDto.TabIndx == dto.TabIndx && lastDto.ID == dto.ID) return;
            ProfilesVisited.Add(dto);
        }

        public void InsertTabIndexSearchVisited(int type, int idx)
        {
            ProfilesVisitedDTO dto = new ProfilesVisitedDTO();
            dto.TabIndx = idx;
            dto.Type = type;
            ProfilesVisitedDTO lastDto = ProfilesVisited.LastOrDefault();
            if (lastDto.Type == dto.Type && lastDto.TabIndx == dto.TabIndx && lastDto.ID == dto.ID) return;
            ProfilesVisited.Add(dto);
        }

        public void ClearChatVisitedData()
        {
            try
            {
                List<ProfilesVisitedDTO> tempList = ProfilesVisited.Where(P => P.Type == MiddlePanelConstants.TypeFriendChatCall || P.Type == MiddlePanelConstants.TypeGroupChatCall || P.Type == MiddlePanelConstants.TypeRoomChat).ToList();
                foreach (ProfilesVisitedDTO dto in tempList) ProfilesVisited.Remove(dto);
                if (RingIDViewModel.Instance.SelectedMainMiddlePanel == MiddlePanelConstants.TypeFriendChatCall || RingIDViewModel.Instance.SelectedMainMiddlePanel == MiddlePanelConstants.TypeGroupChatCall || RingIDViewModel.Instance.SelectedMainMiddlePanel == MiddlePanelConstants.TypeRoomChat)
                {
                    if (ProfilesVisited.Count > 0) RingIDViewModel.Instance.SelectedMainMiddlePanel = ProfilesVisited[ProfilesVisited.Count - 1].Type;
                    else RingIDViewModel.Instance.SelectedMainMiddlePanel = MiddlePanelConstants.NULL;
                }
            }
            finally { }
        }

        public void SwitchToPreviousUI()
        {
            try
            {
                if (ProfilesVisited.Count > 1)
                {
                    ProfilesVisitedDTO current = ProfilesVisited.LastOrDefault();
                    ProfilesVisited.Remove(current); //remove current
                    ProfilesVisitedDTO lastVisted = ProfilesVisited.LastOrDefault();
                    if (lastVisted.ID == current.ID && lastVisted.TabIndx == current.TabIndx && lastVisted.Type == current.Type) SwitchToPreviousUI();
                    else
                    {
                        RingIDViewModel.Instance.SelectedMainMiddlePanel = lastVisted.Type;
                        switch (lastVisted.Type)
                        {
                            case MiddlePanelConstants.TypeAllFeeds:
                                AddUserControl(View_UCAllFeeds);
                                break;
                            case MiddlePanelConstants.TypeMyProfile:
                                LoadMyProfile(lastVisted.TabIndx);
                                break;
                            case MiddlePanelConstants.TypeFriendChatCall:
                                AddUserControl(View_UCFriendChatCallPanel);
                                break;
                            case MiddlePanelConstants.TypeFriendProfile:
                                dynamic state = new ExpandoObject();
                                state.UtID = lastVisted.ID;
                                if (lastVisted.TabIndx != 5) state.type = "profile";
                                else state.type = "info";
                                state.TabIndx = lastVisted.TabIndx;
                                LoadFriendProfilePanel(state);
                                break;
                            case MiddlePanelConstants.TypeFriendChatSettings:
                                AddUserControl(View_UCFriendChatSettingsPanel);
                                break;
                            case MiddlePanelConstants.TypeGroupChatCall:
                                AddUserControl(View_UCGroupChatCallPanel);
                                break;
                            case MiddlePanelConstants.TypeGroupChatSettings:
                                AddUserControl(View_UCGroupChatSettingsPanel);
                                break;
                            case MiddlePanelConstants.TypeRoomList:
                                AddUserControl(View_UCRoomListPanel);
                                break;
                            case MiddlePanelConstants.TypeRoomChat:
                                AddUserControl(View_UCRoomChatCallPanel);
                                break;
                            case MiddlePanelConstants.TypeRoomChatSettings:
                                AddUserControl(View_UCRoomChatSettingsPanel);
                                break;
                            case MiddlePanelConstants.TypeAddFriend:
                                AddUserControl(View_UCAddFriendMainPanel);
                                break;
                            case MiddlePanelConstants.TypeDialer:
                                AddUserControl(View_UCDialer);
                                break;
                            case MiddlePanelConstants.TypeCircleMain:
                                LoadCirCleSwitcher(MiddlePanelConstants.TypeCircleMain, null, lastVisted.TabIndx);
                                break;
                            case MiddlePanelConstants.TypeCreateCirclePanel:
                                LoadCirCleSwitcher(MiddlePanelConstants.TypeCreateCirclePanel, null);
                                break;
                            case MiddlePanelConstants.TypeCirclePanel:
                                LoadCirCleSwitcher(MiddlePanelConstants.TypeCirclePanel, lastVisted.ID, lastVisted.TabIndx);
                                AddUserControl(GetCircleSwitcher);
                                break;
                            case MiddlePanelConstants.TypeMyCoverPictureChange:
                                AddUserControl(View_UCMyCoverPictureChange);
                                break;
                            case MiddlePanelConstants.TypeSingleFeedDetails:
                                break;
                            case MiddlePanelConstants.TypeMusicAndVideo:
                                break;
                            case MiddlePanelConstants.TypeMediaFeed:
                                break;
                            case MiddlePanelConstants.TypeNewsPortalProfile:
                                NewsPortalModel newsPortalModel = null;
                                if (UserProfilesContainer.Instance.NewsPortalModels.TryGetValue(lastVisted.ID, out newsPortalModel))
                                    LoadNewsPortalProfilePanel(newsPortalModel);
                                break;
                            case MiddlePanelConstants.TypePageProfile:
                                PageInfoModel pageInfoModel = null;
                                if (UserProfilesContainer.Instance.PageModels.TryGetValue(lastVisted.ID, out pageInfoModel))
                                    LoadPageProfilePanel(pageInfoModel);
                                break;
                            case MiddlePanelConstants.TypeMediaCloudProfile:
                                MusicPageModel musicPageModel = null;
                                if (UserProfilesContainer.Instance.MediaPageModels.TryGetValue(lastVisted.ID, out musicPageModel))
                                    LoadMediaCloudProfilePanel(musicPageModel);
                                break;
                            case MiddlePanelConstants.TypeCelebrityProfile:
                                CelebrityModel celebModel = null;
                                if (UserProfilesContainer.Instance.CelebrityModels.TryGetValue(lastVisted.ID, out celebModel))
                                    LoadCelebrityProfilePanel(celebModel);
                                break;
                            case MiddlePanelConstants.TypeMediaCloud:
                                LoadMediaCloud(lastVisted.TabIndx);
                                break;
                            case MiddlePanelConstants.TypeMediaCloudSearch:
                                LoadMediaCloudSearch(lastVisted.TabIndx);
                                break;
                            case MiddlePanelConstants.TypeNewsPortalSearch:
                                LoadNewsPortalSearch();
                                break;
                            case MiddlePanelConstants.TypePagesSearch:
                                LoadPageSearch();
                                break;
                            case MiddlePanelConstants.TypeCelebritySearch:
                                LoadCelebritySearch();
                                break;
                            case MiddlePanelConstants.TypeNewsPortal:
                                LoadNews(lastVisted.TabIndx);
                                break;
                            case MiddlePanelConstants.TypePages:
                                LoadPages(lastVisted.TabIndx);
                                break;
                            case MiddlePanelConstants.TypeCelebrity:
                                LoadCelebrityFeeds(lastVisted.TabIndx);
                                break;
                            case MiddlePanelConstants.TypeStreamAndChannel:
                                AddUserControl(View_UCStreamAndChannel);
                                break;
                            case MiddlePanelConstants.TypeMyChannel:
                                AddUserControl(View_UCMyChannel);
                                break;
                            case MiddlePanelConstants.TypeSavedFeed:
                                LoadSavedFeed(lastVisted.TabIndx);
                                break;
                            case MiddlePanelConstants.TypeWallet:
                                LoadWallet();
                                break;
                            default:
                                AddUserControl(View_UCAllFeeds);
                                break;
                        }
                    }
                }
                else
                {
                    RingIDViewModel.Instance.SelectedMainMiddlePanel = MiddlePanelConstants.TypeAllFeeds;
                    AddUserControl(View_UCAllFeeds);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: NavigateToPreviousPage() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Navigate(int type, object state = null)
        {
            try
            {
                if (UCMiddlePanelSwitcher.View_UCCreateCirclePanel != null && UCMiddlePanelSwitcher.View_UCCreateCirclePanel._IsCreateCircleContinuing && VMCircle.Instance.DragListForNewCircle.Count > 0)
                    if (!UCMiddlePanelSwitcher.View_UCCreateCirclePanel.ConfirmCircleCreateLeave()) return;
                if (type != MiddlePanelConstants.TypeCirclePanel && type != MiddlePanelConstants.TypeMyProfile && type != MiddlePanelConstants.TypeFriendProfile && type != MiddlePanelConstants.TypeCelebrityProfile && type != MiddlePanelConstants.TypeNewsPortalProfile && type != MiddlePanelConstants.TypePageProfile && type != MiddlePanelConstants.TypeMediaCloudProfile
                    && type != MiddlePanelConstants.TypeFriendChatCall && type != MiddlePanelConstants.TypeGroupChatSettings && type != MiddlePanelConstants.TypeGroupChatCall && type != MiddlePanelConstants.TypeRoomChat
                    && type != MiddlePanelConstants.TypeNewsPortalSearch && type != MiddlePanelConstants.TypePagesSearch && type != MiddlePanelConstants.TypeCelebritySearch && type != MiddlePanelConstants.TypeMediaCloudSearch
                    && type != MiddlePanelConstants.TypeNewsPortal && type != MiddlePanelConstants.TypePages && type != MiddlePanelConstants.TypeCelebrity && type != MiddlePanelConstants.TypeMediaCloud
                    && type != MiddlePanelConstants.TypeCircleMain && type != MiddlePanelConstants.TypeCirclePanel && type != MiddlePanelConstants.TypeCreateCirclePanel && type != MiddlePanelConstants.TypeSavedFeed)
                {
                    ProfilesVisited.Clear();
                    ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                }
                switch (type)
                {
                    case MiddlePanelConstants.TypeAllFeeds:
                        LoadAllFeeds(state);
                        break;
                    case MiddlePanelConstants.TypeMyProfile:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        state = 0;//wall
                        LoadMyProfile(state);
                        break;
                    case MiddlePanelConstants.TypeFriendChatCall:
                        if (SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW)
                        {
                            LoadFriendChatCallPanelInWindow(state);
                            return;
                        }
                        else
                        {
                            ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                            LoadFriendChatCallPanel(state);
                        }
                        break;
                    case MiddlePanelConstants.TypeFriendProfile:
                        dynamic uObj = (ExpandoObject)state;
                        long utID = (long)uObj.UtID;
                        int idx = (int)uObj.TabIndx;
                        ProfilesVisited.Add(new ProfilesVisitedDTO { ID = utID, Type = type, TabIndx = idx });
                        LoadFriendProfilePanel(state);
                        break;
                    case MiddlePanelConstants.TypeFriendChatSettings:
                        LoadFriendChatSettings(state);
                        break;
                    case MiddlePanelConstants.TypeGroupChatCall:
                        if (SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW)
                        {
                            LoadGroupChatCallPanelInWindow(state);
                            return;
                        }
                        else
                        {
                            ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                            LoadGroupChatCallPanel(state);
                        }
                        break;
                    case MiddlePanelConstants.TypeGroupChatSettings:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadGroupChatSettingsPanel(state);
                        break;
                    case MiddlePanelConstants.TypeRoomChat:
                        if (SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW)
                        {
                            LoadRoomChatCallPanelInWindow(state);
                            return;
                        }
                        else
                        {
                            ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                            LoadRoomChatCallPanel(state);
                        }
                        break;
                    case MiddlePanelConstants.TypeRoomChatSettings:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadRoomChatSettingsPanel(state);
                        break;

                    case MiddlePanelConstants.TypeAddFriend:
                        LoadAddFriend(state);
                        break;
                    case MiddlePanelConstants.TypeDialer:
                        LoadDialer(state);
                        break;
                    case MiddlePanelConstants.TypeCircleMain:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadCirCleSwitcher(type, state);
                        break;
                    case MiddlePanelConstants.TypeMyCoverPictureChange:
                        LoadMyCoverPictureChange(state);
                        break;
                    case MiddlePanelConstants.TypeCreateCirclePanel:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadCirCleSwitcher(type, state);
                        break;
                    case MiddlePanelConstants.TypeCirclePanel:
                        long cid = long.Parse(state.ToString());
                        ProfilesVisited.Add(new ProfilesVisitedDTO { ID = cid, Type = type });
                        LoadCirCleSwitcher(type, state);
                        break;
                    case MiddlePanelConstants.TypeSingleFeedDetails:
                        LoadSingleFeedDetails(state);
                        break;
                    case MiddlePanelConstants.TypeCallMainUI:
                        LoadCallControlContainer(state);
                        break;
                    case MiddlePanelConstants.TypeMusicAndVideo:
                        LoadMusicAndVideo(state);
                        break;
                    case MiddlePanelConstants.TypeMediaCloud:
                        bool isFromChannel = true;
                        if (!(state != null && Int32.Parse(state.ToString()) == MiddlePanelConstants.TypeMyChannel))
                        {
                            isFromChannel = false;
                            ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        }
                        LoadMediaCloud(0, isFromChannel);
                        break;
                    case MiddlePanelConstants.TypeMediaFeed:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadMediaFeed(state);
                        break;
                    case MiddlePanelConstants.TypeStickerMarket:
                        LoadMenuStickers();
                        break;
                    case MiddlePanelConstants.TypeGroupList:
                        LoadGroupList();
                        break;
                    case MiddlePanelConstants.TypeRoomList:
                        LoadRoomList();
                        break;
                    case MiddlePanelConstants.TypeNewsPortal:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadNews();
                        break;
                    case MiddlePanelConstants.TypeNewsPortalProfile:
                        if (state is NewsPortalModel)
                        {
                            NewsPortalModel newsPortalModel = (NewsPortalModel)state;
                            ProfilesVisited.Add(new ProfilesVisitedDTO { ID = newsPortalModel.PortalId, Type = type });
                        }
                        LoadNewsPortalProfilePanel(state);
                        break;
                    case MiddlePanelConstants.TypePageProfile:
                        if (state is PageInfoModel)
                        {
                            PageInfoModel pageInfomodel = (PageInfoModel)state;
                            ProfilesVisited.Add(new ProfilesVisitedDTO { ID = pageInfomodel.PageId, Type = type });
                        }
                        LoadPageProfilePanel(state);
                        break;
                    case MiddlePanelConstants.TypePages:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadPages();
                        break;
                    case MiddlePanelConstants.TypeSavedFeed:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadSavedFeed();
                        break;
                    case MiddlePanelConstants.TypeCelebrity:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadCelebrityFeeds();
                        break;
                    case MiddlePanelConstants.TypeCelebrityProfile:
                        if (state is CelebrityModel)
                        {
                            CelebrityModel celebrityInfomodel = (CelebrityModel)state;
                            ProfilesVisited.Add(new ProfilesVisitedDTO { ID = celebrityInfomodel.UserTableID, Type = type });
                        }
                        LoadCelebrityProfilePanel(state);
                        break;
                    case MiddlePanelConstants.TypeMediaCloudProfile:
                        if (state is MusicPageModel)
                        {
                            MusicPageModel musicPageModel = (MusicPageModel)state;
                            ProfilesVisited.Add(new ProfilesVisitedDTO { ID = musicPageModel.MusicPageId, Type = type });
                        }
                        LoadMediaCloudProfilePanel(state);
                        break;
                    case MiddlePanelConstants.TypeFollowingList:
                        LoadFollowingList();
                        break;
                    case MiddlePanelConstants.TypeWallet:
                        LoadWallet();
                        break;
                    case MiddlePanelConstants.TypeStreamAndChannel:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadStreamAndChannelPanel(state);
                        break;
                    case MiddlePanelConstants.TypeMyChannel:
                        ProfilesVisited.Add(new ProfilesVisitedDTO { Type = type });
                        LoadMyChannelPanel(state);
                        break;
                }
                RingIDViewModel.Instance.SelectedMainMiddlePanel = type;
            }
            catch (Exception ex)
            {
                log.Error("Error: Navigate() ==> UI TYPE ==> " + type + " ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void LoadAllFeeds(object state)
        {
            if (View_UCAllFeeds == null) View_UCAllFeeds = new UCAllFeeds();
            AddUserControl(View_UCAllFeeds);
            if (View_UCAllFeeds.scroll != null) View_UCAllFeeds.scroll.ScrollToVerticalOffset(0);
        }

        private void LoadMyProfile(object state)
        {
            if (View_UCMyProfile == null) View_UCMyProfile = new UCMyProfile();
            if (View_UCMyProfile._UCMyProfilePhotos != null)
            {
                View.Utility.Auth.SendDataToServer.ListOfImageAlbumsOfUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_IMAGE, Guid.Empty);
            }
            View_UCMyProfile.ShowMyFriendPreviewImages();
            View_UCMyProfile.ShowPost(state);
            AddUserControl(View_UCMyProfile);
            if (!DefaultSettings.IS_MY_PROFILE_VALUES_LOADED) new ThradMyProfileDetails().StartThread();
            else
            {
                if (View_UCMyProfile._UCMyProfileAbout != null) View_UCMyProfile._UCMyProfileAbout.ShowMyBasicInfos();
            }
            if (View_UCMyProfile._UCMyNewsFeeds != null && View_UCMyProfile._UCMyNewsFeeds.NewStatusView != null) View_UCMyProfile._UCMyNewsFeeds.NewStatusViewModel.SetSelectedModelValuesForValidityPopup();
        }

        private void LoadMyCoverPictureChange(object state)
        {
            if (View_UCMyCoverPictureChange == null) View_UCMyCoverPictureChange = new UCMyCoverPhotoChange();
            else View_UCMyCoverPictureChange.ResetAll();
            AddUserControl(View_UCMyCoverPictureChange);
        }

        private void LoadCircleList(object state, int CirclePanelIdx)
        {
            if (View_UCCircleInitPanel == null) View_UCCircleInitPanel = new UCCircleInitPanel();
            else View_UCCircleInitPanel.SwitchToTab(CirclePanelIdx);
            GetCircleSwitcher.Content = View_UCCircleInitPanel;
        }

        private void LoadCirCleSwitcher(int type, object state, int CirclePanelIdx = 0)
        {
            switch (type)
            {
                case MiddlePanelConstants.TypeCircleMain:
                    LoadCircleList(state, CirclePanelIdx);
                    AddUserControl(GetCircleSwitcher);
                    break;
                case MiddlePanelConstants.TypeCreateCirclePanel:
                    LoadCreateCircle(state);
                    break;
                case MiddlePanelConstants.TypeCirclePanel:
                    LoadCirclePanel(state, CirclePanelIdx);
                    break;
            }
        }

        private void LoadCreateCircle(object state)
        {
            if (View_UCCreateCirclePanel == null) View_UCCreateCirclePanel = new UCCreateCirclePanel();
            else View_UCCreateCirclePanel.ClearCreateCirclePanel();
            GetCircleSwitcher.Content = View_UCCreateCirclePanel;
        }

        private void LoadCirclePanel(object state, int CirclePanelIdx)
        {
            if (!DefaultSettings.IsInternetAvailable)
            {
                UIHelperMethods.ShowInformation(NotificationMessages.INTERNET_UNAVAILABLE, "Internet problem!");
                return;
            }
            long circleId = System.Convert.ToInt64(state);
            if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.Count > 0)
            {
                CircleModel model;
                if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out model)) AccessCircle(circleId, model, CirclePanelIdx);
                else //CustomMessageBox.ShowInformation("You are not a member of this circle!");
                    UIHelperMethods.ShowInformation("You are not a member of this circle!", "Circle load");
            }
        }

        private void AccessCircle(long circleId, CircleModel model, int CirclePanelIdx)
        {
            try
            {
                View_UCCirclePanel = new UCCirclePanel(model);
                GetCircleSwitcher.Content = View_UCCirclePanel;
                AddUserControl(GetCircleSwitcher);
                if (View_UCCirclePanel._UCCircleMembersPanel != null)
                {
                    View_UCCirclePanel._UCCircleMembersPanel.textBoxSearch.Text = string.Empty;
                    View.Utility.Circle.VMCircle.Instance.SearchString = string.Empty;
                    View.Utility.Circle.VMCircle.Instance.ClearSearchRecords();
                }
                View_UCCirclePanel.ChangeEventHandler(false);
                View_UCCirclePanel.TabList.SelectedIndex = CirclePanelIdx;
                View_UCCirclePanel.ChangeEventHandler();
                if (CirclePanelIdx == 0) View_UCCirclePanel.ShowFeedsTab();
                else View_UCCirclePanel.ShowMembersTab();
                if (View_UCCirclePanel._UCCircleNewsFeeds != null && View_UCCirclePanel._UCCircleNewsFeeds.NewStatusView != null) View_UCCirclePanel._UCCircleNewsFeeds.NewStatusViewModel.SetSelectedModelValuesForValidityPopup();
                View_UCCirclePanel.myScrlViewer.ScrollToTop();
            }
            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        private void LoadDialer(object state)
        {
            if (View_UCDialer == null) View_UCDialer = new UCDialer();
            AddUserControl(View_UCDialer);
        }

        private void LoadAddFriend(object state)
        {
            if (View_UCAddFriendMainPanel == null) View_UCAddFriendMainPanel = new UCAddFriendMainPanel();
            int incomingCount = FriendListController.Instance.FriendDataContainer.IncommingListCount();
            if (incomingCount > 0)
            {
                View_UCAddFriendMainPanel.IncomingRequestCount = FriendListController.Instance.FriendDataContainer.IncommingListCount();
                View_UCAddFriendMainPanel.IncomingRequestCounterSize = HelperMethods.CalculateCircleSizeOfAddFriendNotifactionCounter(incomingCount);
            }
            AddUserControl(View_UCAddFriendMainPanel);

            if (AppConstants.ADD_FRIEND_NOTIFICATION_COUNT > 0)
            {
                View_UCAddFriendMainPanel.TabControl.SelectedIndex = 1; //Pending tab
                View_UCAddFriendMainPanel.LoadPendingData();
            }
            else View_UCAddFriendMainPanel.TabControl.SelectedIndex = 0; //Search tab
            FriendListLoadUtility.ClearAddFriendsNotificationCount();
        }

        private void LoadSingleFeedDetails(object state)
        {
        }

        private void LoadCallControlContainer(object callerDto)
        {
            if (this.Content != MainSwitcher.CallController.CallUiInMainWindow) AddUserControl(MainSwitcher.CallController.CallUiInMainWindow);
        }

        private void LoadMusicAndVideo(object state)
        {
        }

        private void LoadMediaCloud(int tabIdx = 0, bool isFromChannel = false)
        {
            if (View_UCMediaCloudMainPanel == null) View_UCMediaCloudMainPanel = new UCMediaCloudMainPanel(isFromChannel);
            else
            {
                View_UCMediaCloudMainPanel.tabContainerBorder.Visibility = Visibility.Visible;
                View_UCMediaCloudMainPanel.searchContentsBdr.Visibility = Visibility.Collapsed;
                View_UCMediaCloudMainPanel.IsTabView = true;
                View_UCMediaCloudMainPanel.SwitchToTab(tabIdx);
            }
            AddUserControl(View_UCMediaCloudMainPanel);
        }

        private void LoadMediaCloudSearch(int tabIdx = 0)
        {
            if (_UCMediaCloudSearch != null)
            {
                View_UCMediaCloudMainPanel.tabContainerBorder.Visibility = Visibility.Collapsed;
                View_UCMediaCloudMainPanel.searchContentsBdr.Visibility = Visibility.Visible;
                View_UCMediaCloudMainPanel.IsTabView = false;
                _UCMediaCloudSearch.LoadUI(tabIdx);
            }
            AddUserControl(View_UCMediaCloudMainPanel);
        }

        private void LoadCelebritySearch()
        {
            if (_UCCelebritiesSearchPanel != null)
            {
                View_UCCelebritiesMainPanel.tabContainerBorder.Visibility = Visibility.Collapsed;
                View_UCCelebritiesMainPanel.searchContentsBdr.Visibility = Visibility.Visible;
                View_UCCelebritiesMainPanel.IsTabView = false;
            }
            AddUserControl(View_UCCelebritiesMainPanel);
        }

        private void LoadPageSearch()
        {
            try
            {
                if (View_UCPagesSearchPanel != null)
                {
                    View_UCPagesMainPanel.tabContainerBorder.Visibility = Visibility.Collapsed;
                    View_UCPagesMainPanel.searchContentsBdr.Visibility = Visibility.Visible;
                    View_UCPagesMainPanel.IsTabView = false;
                }
                AddUserControl(View_UCPagesMainPanel);
            }
            finally { }
        }

        private void LoadNewsPortalSearch()
        {
            if (View_UCNewsPortalSearchPanel != null)
            {
                View_UCNewsPortalMainPanel.tabContainerBorder.Visibility = Visibility.Collapsed;
                View_UCNewsPortalMainPanel.searchContentsBdr.Visibility = Visibility.Visible;
                View_UCNewsPortalMainPanel.IsTabView = false;
            }
            AddUserControl(View_UCNewsPortalMainPanel);
        }

        private void LoadMediaFeed(object state)
        {
        }

        private void LoadWallet()
        {
            if (View_UCWalletMainPanel == null) View_UCWalletMainPanel = new UCWalletMainPanel();
            AddUserControl(View_UCWalletMainPanel);
        }

        private void LoadFriendChatCallPanel(object state)
        {
            long userTableId = (long)state;
            UserBasicInfoModel basicInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(userTableId);
            if (View_UCFriendChatCallPanel != null && View_UCFriendChatCallPanel._FriendTableID == userTableId)
            {
                bool updatePresence = !View_UCFriendChatCallPanel.IsVisible;
                AddUserControl(View_UCFriendChatCallPanel);
                HelperMethods.InitFriendChatCallPanel(View_UCFriendChatCallPanel, updatePresence);
                return;
            }

            UCFriendChatCallPanel ucFriendChatCallPanel = null;
            if (!UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(userTableId, out ucFriendChatCallPanel))
            {
                ucFriendChatCallPanel = new UCFriendChatCallPanel(basicInfoModel);
                UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY[userTableId] = ucFriendChatCallPanel;
            }

            AddUserControl(ucFriendChatCallPanel);
            View_UCFriendChatCallPanel = ucFriendChatCallPanel;
            HelperMethods.InitFriendChatCallPanel(ucFriendChatCallPanel, true);
        }

        private void LoadFriendChatCallPanelInWindow(object state)
        {
            long userTableId = (long)state;
            UserBasicInfoModel basicInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(userTableId);

            UCFriendChatCallPanel ucFriendChatCallPanel = null;
            if (!UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(userTableId, out ucFriendChatCallPanel))
            {
                ucFriendChatCallPanel = new UCFriendChatCallPanel(basicInfoModel);
                UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY[userTableId] = ucFriendChatCallPanel;
            }
            if (ucFriendChatCallPanel.Parent != null)
            {
                if (ucFriendChatCallPanel.Parent is WNChatView)
                {
                    ((WNChatView)ucFriendChatCallPanel.Parent).ShowWindow();
                    HelperMethods.InitFriendChatCallPanel(ucFriendChatCallPanel);
                }
                else
                {
                    new WNChatView(ucFriendChatCallPanel).ShowWindow();
                    HelperMethods.InitFriendChatCallPanel(ucFriendChatCallPanel, true);
                }
            }
            else
            {
                new WNChatView(ucFriendChatCallPanel).ShowWindow();
                HelperMethods.InitFriendChatCallPanel(ucFriendChatCallPanel, true);
            }
            View_UCFriendChatCallPanel = null;

            if (RingIDViewModel.Instance.SelectedMainMiddlePanel == MiddlePanelConstants.TypeCallMainUI) SwitchToPreviousUI();
        }

        private void LoadFriendProfilePanel(object state)
        {
            int tabIndex = 0;
            long utID = ((dynamic)state).UtID;
            string pType = ((dynamic)state).type;
            try
            {
                tabIndex = ((dynamic)state).TabIndx;
            }
            catch (Exception) { }

            HelperMethods.ConvertUtIdToSwitchFriendProfile(pType, utID, (type, userTableID, alreadyRequestedNF) =>
            {
                try
                {
                    if (View_UCFriendProfilePanel != null)
                    {
                        if (View_UCFriendProfilePanel._FriendTableID == userTableID)
                        {
                            if (type == "profile")
                            {
                                View_UCFriendProfilePanel.ProfileLoading();
                                View_UCFriendProfilePanel.RequestForPreviewImageOfFeedContact();
                                View_UCFriendProfilePanel.ShowSelectedTab(tabIndex);
                            }
                            else if (type == "info") View_UCFriendProfilePanel.InfoLoading();
                            if (RingIDViewModel.Instance.SelectedMainMiddlePanel != MiddlePanelConstants.TypeFriendProfile) new ThreadFriendPresenceInfo(userTableID);
                            AddUserControl(View_UCFriendProfilePanel);
                            return false;
                        }
                        else HelperMethods.ClearUIOfPreviousFriend(View_UCFriendProfilePanel);
                    }

                    UserBasicInfoModel basicInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(userTableID);
                    View_UCFriendProfilePanel = new UCFriendProfile(basicInfoModel);
                    UserBasicInfoDTO basicInfoDTO = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userTableID);
                    if (basicInfoDTO != null && basicInfoDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                    {
                        ThradFriendDetailsInfoRequest request = new ThradFriendDetailsInfoRequest();
                        request.OnSuccess += (userDTO) =>
                        {
                            basicInfoModel.ShortInfoModel.IsProfileHidden = userDTO.IsProfileHidden;
                            new ThreadFriendPresenceInfo(userTableID);
                            if (type == "profile") View.Utility.Auth.SendDataToServer.SendAlbumRequest(userTableID, userDTO.AlbumId, Guid.Empty, 10);
                        };
                        request.StartThread(basicInfoModel.ShortInfoModel.UserTableID);
                    }
                    else if ((basicInfoDTO == null || basicInfoDTO.FriendShipStatus == 0) && alreadyRequestedNF == false)
                    {
                        ThradUnknownProfileInfoRequest request = new ThradUnknownProfileInfoRequest();
                        request.OnSuccess += (userDTO) => { new ThreadFriendPresenceInfo(userTableID); };
                        request.StartThread(userTableID, true);
                    }
                    else
                    {
                        new ThreadFriendPresenceInfo(userTableID);
                    }

                    if (type == "profile")
                    {
                        View_UCFriendProfilePanel.ProfileLoading();
                        View_UCFriendProfilePanel.RequestForPreviewImageOfFeedContact();
                        View_UCFriendProfilePanel.ShowSelectedTab(tabIndex);
                    }
                    else if (type == "info") View_UCFriendProfilePanel.InfoLoading();
                    AddUserControl(View_UCFriendProfilePanel);
                    if (View_UCFriendProfilePanel._UCFriendNewsFeeds != null && View_UCFriendProfilePanel._UCFriendNewsFeeds.NewStatusView != null)
                        View_UCFriendProfilePanel._UCFriendNewsFeeds.NewStatusViewModel.SetSelectedModelValuesForValidityPopup();

                }
                catch (Exception ex) { log.Error(ex.StackTrace + ex.Message); }
                return true;
            });
        }

        private void LoadFriendChatSettings(object state)
        {
            long userTableId = (long)state;
            UserBasicInfoModel basicInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableId);

            if (View_UCFriendChatSettingsPanel != null && View_UCFriendChatSettingsPanel._FriendTableID == userTableId)
            {
                AddUserControl(View_UCFriendChatSettingsPanel);
                return;
            }

            UCFriendChatSettings ucFriendChatSettingsPanel = null;
            if (!UIDictionaries.Instance.FRIEND_CHAT_SETTINGS_DICTIONARY.TryGetValue(userTableId, out ucFriendChatSettingsPanel))
            {
                ucFriendChatSettingsPanel = new UCFriendChatSettings(basicInfoModel);
                UIDictionaries.Instance.FRIEND_CHAT_SETTINGS_DICTIONARY[userTableId] = ucFriendChatSettingsPanel;
            }

            AddUserControl(ucFriendChatSettingsPanel);
            View_UCFriendChatSettingsPanel = ucFriendChatSettingsPanel;
            ucFriendChatSettingsPanel.LoadInitInformation();
        }

        private void LoadGroupChatCallPanel(object state)
        {
            long groupID = (long)state;
            GroupInfoModel groupInfoModel = HelperMethods.GetModelToSwtichGroupProfile(groupID);

            if (View_UCGroupChatCallPanel != null && View_UCGroupChatCallPanel._GroupID == groupID)
            {
                AddUserControl(View_UCGroupChatCallPanel);
                HelperMethods.InitGroupChatCallPanel(View_UCGroupChatCallPanel);
                return;
            }

            UCGroupChatCallPanel ucGroupChatCallPanel = null;
            if (!UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(groupID, out ucGroupChatCallPanel))
            {
                ucGroupChatCallPanel = new UCGroupChatCallPanel(groupInfoModel);
                UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY[groupID] = ucGroupChatCallPanel;
            }

            AddUserControl(ucGroupChatCallPanel);
            View_UCGroupChatCallPanel = ucGroupChatCallPanel;
            HelperMethods.InitGroupChatCallPanel(ucGroupChatCallPanel);
        }

        private void LoadGroupChatCallPanelInWindow(object state)
        {
            long groupID = (long)state;
            GroupInfoModel groupInfoModel = HelperMethods.GetModelToSwtichGroupProfile(groupID);

            UCGroupChatCallPanel ucGroupChatCallPanel = null;
            if (!UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(groupID, out ucGroupChatCallPanel))
            {
                ucGroupChatCallPanel = new UCGroupChatCallPanel(groupInfoModel);
                UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY[groupID] = ucGroupChatCallPanel;
            }

            if (ucGroupChatCallPanel.Parent != null)
            {
                if (ucGroupChatCallPanel.Parent is WNChatView)
                {
                    ((WNChatView)ucGroupChatCallPanel.Parent).ShowWindow();
                    HelperMethods.InitGroupChatCallPanel(ucGroupChatCallPanel);
                }
                else
                {
                    new WNChatView(ucGroupChatCallPanel).ShowWindow();
                    HelperMethods.InitGroupChatCallPanel(ucGroupChatCallPanel);
                }
            }
            else
            {
                new WNChatView(ucGroupChatCallPanel).ShowWindow();
                HelperMethods.InitGroupChatCallPanel(ucGroupChatCallPanel);
            }
            View_UCGroupChatCallPanel = null;
        }

        private void LoadGroupChatSettingsPanel(object state)
        {
            long groupID = (long)state;
            GroupInfoModel groupInfoModel = HelperMethods.GetModelToSwtichGroupProfile(groupID);
            if (View_UCGroupChatSettingsPanel != null && View_UCGroupChatSettingsPanel._GroupID == groupID)
            {
                View_UCGroupChatSettingsPanel.LoadInitInformation();
                AddUserControl(View_UCGroupChatSettingsPanel);
                return;
            }
            UCGroupChatSettings ucGroupChatSettingsPanel = null;
            if (!UIDictionaries.Instance.GROUP_CHAT_SETTINGS_DICTIONARY.TryGetValue(groupID, out ucGroupChatSettingsPanel))
            {
                ucGroupChatSettingsPanel = new UCGroupChatSettings(groupInfoModel);
                UIDictionaries.Instance.GROUP_CHAT_SETTINGS_DICTIONARY[groupID] = ucGroupChatSettingsPanel;
            }
            View_UCGroupChatSettingsPanel = ucGroupChatSettingsPanel;
            AddUserControl(View_UCGroupChatSettingsPanel);
            ucGroupChatSettingsPanel.LoadInitInformation();
        }

        private void LoadRoomChatCallPanel(object state)
        {
            string roomID = (string)state;
            RoomModel roomModel = HelperMethods.GetModelToSwtichRoomProfile(roomID);

            if (View_UCRoomChatCallPanel != null && View_UCRoomChatCallPanel._RoomID.Equals(roomID))
            {
                AddUserControl(View_UCRoomChatCallPanel);
                HelperMethods.InitRoomChatCallPanel(View_UCRoomChatCallPanel);
                return;
            }
            UCRoomChatCallPanel ucRoomChatCallPanel = null;
            if (!UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(roomID, out ucRoomChatCallPanel))
            {
                ucRoomChatCallPanel = new UCRoomChatCallPanel(roomModel);
                UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY[roomID] = ucRoomChatCallPanel;
            }
            AddUserControl(ucRoomChatCallPanel);
            View_UCRoomChatCallPanel = ucRoomChatCallPanel;
            HelperMethods.InitRoomChatCallPanel(ucRoomChatCallPanel);
        }

        private void LoadRoomChatCallPanelInWindow(object state)
        {
            string roomID = (string)state;
            RoomModel roomModel = HelperMethods.GetModelToSwtichRoomProfile(roomID);
            UCRoomChatCallPanel ucRoomChatCallPanel = null;
            if (!UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(roomID, out ucRoomChatCallPanel))
            {
                ucRoomChatCallPanel = new UCRoomChatCallPanel(roomModel);
                UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY[roomID] = ucRoomChatCallPanel;
            }
            if (ucRoomChatCallPanel.Parent != null)
            {
                if (ucRoomChatCallPanel.Parent is WNChatView)
                {
                    ((WNChatView)ucRoomChatCallPanel.Parent).ShowWindow();
                    HelperMethods.InitRoomChatCallPanel(ucRoomChatCallPanel);
                }
                else
                {
                    new WNChatView(ucRoomChatCallPanel).ShowWindow();
                    HelperMethods.InitRoomChatCallPanel(ucRoomChatCallPanel);
                }
            }
            else
            {
                new WNChatView(ucRoomChatCallPanel).ShowWindow();
                HelperMethods.InitRoomChatCallPanel(ucRoomChatCallPanel);
            }
            View_UCGroupChatCallPanel = null;
        }

        private void LoadRoomChatSettingsPanel(object state)
        {
            string roomID = (string)state;
            RoomModel roomModel = HelperMethods.GetModelToSwtichRoomProfile(roomID);

            if (View_UCRoomChatSettingsPanel != null && View_UCRoomChatSettingsPanel._RoomID.Equals(roomID))
            {
                View_UCRoomChatSettingsPanel.LoadInitInformation();
                AddUserControl(View_UCRoomChatSettingsPanel);
                return;
            }
            UCRoomChatSettings ucRoomChatSettingsPanel = null;
            if (!UIDictionaries.Instance.ROOM_CHAT_SETTINGS_DICTIONARY.TryGetValue(roomID, out ucRoomChatSettingsPanel))
            {
                ucRoomChatSettingsPanel = new UCRoomChatSettings(roomModel);
                UIDictionaries.Instance.ROOM_CHAT_SETTINGS_DICTIONARY[roomID] = ucRoomChatSettingsPanel;
            }
            View_UCRoomChatSettingsPanel = ucRoomChatSettingsPanel;
            AddUserControl(View_UCRoomChatSettingsPanel);
            ucRoomChatSettingsPanel.LoadInitInformation();
        }

        private void LoadNewsPortalProfilePanel(object state)
        {
            if (state is NewsPortalModel)
            {
                NewsPortalModel model = (NewsPortalModel)state;
                if (View_UCNewsPortalProfile == null || (View_UCNewsPortalProfile.NewsPortalModel != null && View_UCNewsPortalProfile.NewsPortalModel.UserTableID != model.UserTableID))
                {
                    View_UCNewsPortalProfile = new UCNewsPortalProfile(model);
<<<<<<< HEAD
                    ThreadChannelInfoRequest request = new ThreadChannelInfoRequest(model.PortalId, model.UserTableID, model.UserIdentity, SettingsConstants.PROFILE_TYPE_NEWSPORTAL);
                    request.OnSuccess += (jObj) =>
                    {
                        if (jObj != null) model.LoadData(jObj);
                    };
                    request.Start();
                }
=======
                }
                ThreadChannelInfoRequest request = new ThreadChannelInfoRequest(model.PortalId, model.UserTableID, model.UserIdentity, SettingsConstants.PROFILE_TYPE_NEWSPORTAL);
                request.OnSuccess += (jObj) =>
                {
                    if (jObj != null) View_UCNewsPortalProfile.NewsPortalModel.LoadData(jObj);
                };
                request.Start();
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                AddUserControl(View_UCNewsPortalProfile);
            }
        }

        private void LoadPageProfilePanel(object state)
        {
            if (state is PageInfoModel)
            {
                PageInfoModel model = (PageInfoModel)state;
                if (View_UCPageProfile == null || (View_UCPageProfile.PageModel != null && View_UCPageProfile.PageModel.PageId != model.PageId))
                {
                    View_UCPageProfile = new UCPageProfile(model);
                    ThreadChannelInfoRequest request = new ThreadChannelInfoRequest(model.PageId, model.UserTableID, model.UserIdentity, SettingsConstants.PROFILE_TYPE_PAGES);
                    request.OnSuccess += (jObj) =>
                    {
                        if (jObj != null) model.LoadData(jObj);
                    };
                    request.Start();
                }
                AddUserControl(View_UCPageProfile);
            }
        }

        private void LoadMediaCloudProfilePanel(object state)
        {
            if (state is MusicPageModel)
            {
                MusicPageModel model = (MusicPageModel)state;
                if (View_MediaPageProfile == null || (View_MediaPageProfile.MediaPageModel != null && View_MediaPageProfile.MediaPageModel.UserTableID != model.UserTableID))
                {
                    View_MediaPageProfile = new UCMediaPageProfile(model);
<<<<<<< HEAD
                    ThreadChannelInfoRequest request = new ThreadChannelInfoRequest(model.MusicPageId, model.UserTableID, model.UserIdentity, SettingsConstants.PROFILE_TYPE_MUSICPAGE);
                    request.OnSuccess += (jObj) =>
                    {
                        if (jObj != null) model.LoadData(jObj);
                    };
                    request.Start();
                }
=======
                }
                ThreadChannelInfoRequest request = new ThreadChannelInfoRequest(model.MusicPageId, model.UserTableID, model.UserIdentity, SettingsConstants.PROFILE_TYPE_MUSICPAGE);
                request.OnSuccess += (jObj) =>
                {
                    if (jObj != null) View_MediaPageProfile.MediaPageModel.LoadData(jObj);
                };
                request.Start();
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                AddUserControl(View_MediaPageProfile);
            }
        }

        private void LoadCelebrityProfilePanel(object state)
        {
            if (state is CelebrityModel)
            {
                CelebrityModel model = (CelebrityModel)state;
                if (View_CelebrityProfile == null || (View_CelebrityProfile.CelebrityUserTableID != model.UserTableID))
                {
                    if (View_CelebrityProfile != null)
                    {
                        UCCelebrityProfile PreviousProfile = View_CelebrityProfile;
                        View_CelebrityProfile = new UCCelebrityProfile(model);
                        PreviousProfile.RemoveAllPhotoModel();
                    }
                    else View_CelebrityProfile = new UCCelebrityProfile(model);
                    ThreadChannelInfoRequest request = new ThreadChannelInfoRequest(0, model.UserTableID, model.UserIdentity, SettingsConstants.PROFILE_TYPE_CELEBRITY);
                    request.OnSuccess += (jObj) =>
                    {
                        if (jObj != null) model.LoadData(jObj);
                    };
                    request.Start();
                    View_CelebrityProfile.RequestPhotosToServer();
                }
                AddUserControl(View_CelebrityProfile);
            }
        }

        private void LoadMenuStickers()
        {
            if (View_MenuStickerWrapper == null) View_MenuStickerWrapper = new UCMenuStickerWrapper();
            AddUserControl(View_MenuStickerWrapper);
        }

        private void LoadGroupList()
        {
            if (View_UCMenuGroupList == null) View_UCMenuGroupList = new UCMenuGroupList();
            AddUserControl(View_UCMenuGroupList);
        }

        private void LoadRoomList()
        {
            if (View_UCRoomListPanel == null) View_UCRoomListPanel = new UCRoomListPanel();
            AddUserControl(View_UCRoomListPanel);
            View_UCRoomListPanel.LoadInitData();
        }

        private void LoadNews(int tabIdx = 0)
        {
            if (View_UCNewsPortalMainPanel == null) View_UCNewsPortalMainPanel = new UCNewsPortalMainPanel();
            else View_UCNewsPortalMainPanel.SwitchToTab(tabIdx);
            AddUserControl(View_UCNewsPortalMainPanel);
        }

        private void LoadPages(int tabIdx = 0)
        {
            if (View_UCPagesMainPanel == null) View_UCPagesMainPanel = new UCPagesMainPanel();
            else View_UCPagesMainPanel.SwitchToTab(tabIdx);
            AddUserControl(View_UCPagesMainPanel);
        }

        private void LoadSavedFeed(int tabIdx = 0)
        {
            if (View_UCSavedFeedPanel == null) View_UCSavedFeedPanel = new UCSavedFeedPanel();
            else View_UCSavedFeedPanel.SwitchToTab(tabIdx);
            AddUserControl(View_UCSavedFeedPanel);
        }

        private void LoadFollowingList()
        {
            if (View_UCFollowingMainPanel == null) View_UCFollowingMainPanel = new UCFollowingMainPanel();
            AddUserControl(View_UCFollowingMainPanel);
        }

        private void LoadCelebrityFeeds(int tabIdx = 0)
        {
            if (View_UCCelebritiesMainPanel == null) View_UCCelebritiesMainPanel = new UCCelebritiesMainPanel();
            else View_UCCelebritiesMainPanel.SwitchToTab(tabIdx);
            AddUserControl(View_UCCelebritiesMainPanel);
        }

        private void LoadStreamAndChannelPanel(object state)
        {
            if (View_UCStreamAndChannel == null)
            {
                View_UCStreamAndChannel = new UCStreamAndChannelWrapper();
                View_UCStreamAndChannel.Navigate(StreamAndChannelConstants.TypeStreamAndChannelMainPanel);
            }
            else if (state != null && (bool)state)
            {
                View_UCStreamAndChannel.Navigate(StreamAndChannelConstants.TypeStreamAndChannelMainPanel);
            }

            AddUserControl(View_UCStreamAndChannel);
        }

        private void LoadMyChannelPanel(object state)
        {
            if (View_UCMyChannel == null)
            {
                View_UCMyChannel = new UCMyChannelWrapper();
                View_UCMyChannel.Navigate(MyChannelConstants.TypeMyChannelAndFollowingPanel);
            }
            else if (state != null && (bool)state)
            {
                View_UCMyChannel.Navigate(MyChannelConstants.TypeMyChannelAndFollowingPanel);
            }
            AddUserControl(View_UCMyChannel);
        }

        public void AddUserControl(UserControl control)
        {
            if (control != null && control.IsVisible == false)
                this.Content = control;
            //pageTransitionControl.ShowPage(control);
        }

        #endregion Utility Method

        #region "Event Trigger"

        //void UCMiddlePanelSwitcher_Unloaded(object sender, RoutedEventArgs e)
        //{
        //    this.Loaded -= UCMiddlePanelSwitcher_Loaded;
        //    this.Unloaded -= UCMiddlePanelSwitcher_Unloaded;
        //}

        //void UCMiddlePanelSwitcher_Loaded(object sender, RoutedEventArgs e)
        //{
        //    Console.WriteLine("UCMiddlePanelSwitcher_Loaded**********************");
        //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeAllFeeds);
        //    if (thrdCheckForUpdates == null) thrdCheckForUpdates = new ThrdCheckForUpdates();
        //    thrdCheckForUpdates.StartChecking(false);
        //}

        #endregion "Event Trigger"
        //#region INotifyPropertyChanged Members

        //public event PropertyChangedEventHandler PropertyChanged;
        //public virtual void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //}
        //#endregion
    }
}

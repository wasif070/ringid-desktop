﻿
namespace View.ViewModel
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using View.Constants;
    using View.Utility;

    public enum ConfirmationDialogResult
    {
        OK,
        Cancel,
        Yes,
        No
    }

    public enum ConfirmationDialogType
    {
        WARNING,
        FAILD,
        QUESTIONS,
        INFO
    }

    public enum CustomConfirmationDialogButtonOptions
    {
        OK,
        OKCancel,
        YesNo,
        YesNoCancel
    }

    internal class ConfirmationViewModel : BaseViewModel
    {
        string[] customButtonText = null;
        string TEXT_OK = "Ok";
        string TEXT_CANCEL = "Cancel";
        string TEXT_NO = "No";
        string TEXT_YES = "Yes";
        #region Properties

        private string confirmTitle;
        public string ConfirmTitle
        {
            get { return confirmTitle; }
            set { confirmTitle = value; OnPropertyChanged("ConfirmTitle"); }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; OnPropertyChanged("Message"); }
        }

        private string description = string.Empty;
        public string Description
        {
            get { return description; }
            set { description = value; OnPropertyChanged("Description"); }
        }

        private string iconSource = null;

        public string IconSource
        {
            get { return iconSource; }
            set { iconSource = value; OnPropertyChanged("IconSource"); }
        }

        /// <summary>
        /// Boolean result of optional checkbox
        /// </summary>
        private bool checkBoxValue;
        public bool CheckBoxValue
        {
            get { return checkBoxValue; }
            set { checkBoxValue = value; }
        }

        /// <summary>
        /// Checkbox caption
        /// </summary>
        private string checkBoxString = null;
        public string CheckBoxString
        {
            get { return checkBoxString; }
            set { checkBoxString = value; OnPropertyChanged("CheckBoxString"); }
        }

        /// <summary>
        /// Font Size
        /// </summary>
        private int fontSize;
        public int FontSize
        {
            get { return fontSize; }
            set { fontSize = value; OnPropertyChanged("FontSize"); }
        }

        /// <summary>
        /// Button stack panel
        /// </summary>
        public StackPanel ButtonStackPanel;

        /// <summary>
        /// Results from dialog
        /// </summary>
        public ConfirmationDialogResult Result { get; set; }

        /// <summary>
        /// Close Action
        /// </summary>
        public Action CloseAction { get; set; }

        #endregion

        #region Commands

        private RelayCommand okCommand;
        public RelayCommand OKCommand
        {
            get
            {
                if (okCommand == null) okCommand = new RelayCommand(param => OKExecute());
                return okCommand;
            }
        }

        private RelayCommand yesCommand;
        public RelayCommand YesCommand
        {
            get
            {
                if (yesCommand == null) yesCommand = new RelayCommand(param => YesExecute());
                return yesCommand;
            }
        }

        private RelayCommand noCommand;
        public RelayCommand NoCommand
        {
            get
            {
                if (noCommand == null) noCommand = new RelayCommand(param => NoExecute());
                return noCommand;
            }
        }

        private RelayCommand cancelCommand;
        public RelayCommand CancelCommand
        {
            get
            {
                if (cancelCommand == null) cancelCommand = new RelayCommand(param => CancelExecute());
                return cancelCommand;
            }
        }

        #endregion

        #region Constructors

        public ConfirmationViewModel(StackPanel buttonPanel)
        {
            Message = "Are you sure?";
            FontSize = 32;
            BuildButtonList(buttonPanel, CustomConfirmationDialogButtonOptions.OK);
        }

        /// <summary>
        /// Show error or warning
        /// </summary>
        /// <param name="buttonPanel"></param>
        /// <param name="message"></param>
        public ConfirmationViewModel(StackPanel buttonPanel, string message)
            : this(buttonPanel)
        {
            Message = message;
            BuildButtonList(buttonPanel, CustomConfirmationDialogButtonOptions.OK);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buttonPanel"></param>
        /// <param name="message"></param>
        /// <param name="options"></param>
        public ConfirmationViewModel(StackPanel buttonPanel, string message, CustomConfirmationDialogButtonOptions options)
            : this(buttonPanel, message)
        {
            BuildButtonList(buttonPanel, options);
        }

        /// <summary>
        /// Show confirmation messge and get a result
        /// </summary>
        /// <param name="buttonPanel"></param>
        /// <param name="message"></param>
        /// <param name="options"></param>
        /// <param name="customButtonText"></param>
        public ConfirmationViewModel(StackPanel buttonPanel, string message, CustomConfirmationDialogButtonOptions options, string[] customButtonText)
            : this(buttonPanel, message)
        {
            this.customButtonText = customButtonText;
            BuildButtonList(buttonPanel, options);
        }


        #endregion

        #region Methods

        /// <summary>
        /// OK - Execute
        /// Executed once employee confirms OK
        /// </summary>
        internal void OKExecute()
        {
            // Dialog result
            Result = ConfirmationDialogResult.OK;
            CloseAction();
        }

        /// <summary>
        /// Yes - Execute
        /// </summary>
        internal void YesExecute()
        {
            // Dialog result
            Result = ConfirmationDialogResult.Yes;
            CloseAction();
        }

        /// <summary>
        /// No - execute
        /// </summary>
        internal void NoExecute()
        {
            // Dialog result
            Result = ConfirmationDialogResult.No;
            CloseAction();
        }

        /// <summary>
        /// Cancel - Execute
        /// Executed once employee confirms Cancel
        /// </summary>
        internal void CancelExecute()
        {
            // Dialog result
            Result = ConfirmationDialogResult.Cancel;
            CloseAction();
        }

        public void BuildButtonList(StackPanel buttonPanel, CustomConfirmationDialogButtonOptions options)
        {
            buttonPanel.Children.Clear();
            Button button;
            if (customButtonText != null)
            {
                if (customButtonText.Length == 1) TEXT_YES = customButtonText[0];
                else if (customButtonText.Length == 2)
                {
                    TEXT_YES = customButtonText[0];
                    TEXT_NO = customButtonText[1];
                }
                else if (customButtonText.Length == 3)
                {
                    TEXT_YES = customButtonText[0];
                    TEXT_NO = customButtonText[1];
                    TEXT_CANCEL = customButtonText[2];
                }
            }
            Thickness margin = new Thickness(0, 0, 10, 0);
            switch (options)
            {
                case CustomConfirmationDialogButtonOptions.OK:
                    IconSource = ImageLocation.CONFIRMATIONS_INFO;
                    button = new Button();
                    button.Content = TEXT_OK;
                    button.Command = OKCommand;
                    button.Style = (Style)Application.Current.Resources["Button"];
                    buttonPanel.Children.Add(button);
                    break;

                case CustomConfirmationDialogButtonOptions.OKCancel:
                    IconSource = ImageLocation.CONFIRMATIONS_QUESTION;
                    button = new Button();
                    button.Content = TEXT_OK;
                    button.Command = OKCommand;
                    button.Style = (Style)Application.Current.Resources["Button"];
                    button.Margin = margin;
                    buttonPanel.Children.Add(button);

                    button = new Button();
                    button.Content = TEXT_CANCEL;
                    button.Command = CancelCommand;
                    button.Style = (Style)Application.Current.Resources["Button"];
                    buttonPanel.Children.Add(button);
                    break;

                case CustomConfirmationDialogButtonOptions.YesNo:
                    IconSource = ImageLocation.CONFIRMATIONS_QUESTION;
                    button = new Button();
                    button.Content = TEXT_YES;
                    button.Command = YesCommand;
                    button.Style = (Style)Application.Current.Resources["Button"];
                    button.Margin = margin;
                    buttonPanel.Children.Add(button);

                    button = new Button();
                    button.Content = TEXT_NO;
                    button.Command = NoCommand;
                    button.Style = (Style)Application.Current.Resources["Button"];
                    buttonPanel.Children.Add(button);
                    break;

                case CustomConfirmationDialogButtonOptions.YesNoCancel:
                    IconSource = ImageLocation.CONFIRMATIONS_QUESTION;
                    button = new Button();
                    button.Content = TEXT_YES;
                    button.Command = YesCommand;
                    button.Style = (Style)Application.Current.Resources["Button"];
                    button.Margin = margin;
                    buttonPanel.Children.Add(button);

                    button = new Button();
                    button.Content = TEXT_NO;
                    button.Command = NoCommand;
                    button.Style = (Style)Application.Current.Resources["Button"];
                    button.Margin = margin;
                    buttonPanel.Children.Add(button);

                    button = new Button();
                    button.Content = TEXT_CANCEL;
                    button.Command = CancelCommand;
                    button.Style = (Style)Application.Current.Resources["Button"];
                    buttonPanel.Children.Add(button);
                    break;
                default:
                    break;
            }
        }

        #endregion

    }
}

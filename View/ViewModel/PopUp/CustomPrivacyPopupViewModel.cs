﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.WPFMessageBox;
using View.ViewModel.NewStatus;

namespace View.ViewModel.PopUp
{
    public class CustomPrivacyPopupViewModel : BaseViewModel
    {
        #region Private Fields

        NewStatusViewModel friendPrivacyNewStatusViewModel;

        #endregion

        #region Constructor

        public CustomPrivacyPopupViewModel()
        {
            FriendListToShowOrHide.Add(PlaceHolderShortInfoModel);
        }

        #endregion

        #region Private Fields and Public Properties

        private bool _isDisableShadeOn = true;
        public bool IsDisableShadeOn
        {
            get
            {
                return _isDisableShadeOn;
            }
            set
            {
                SetProperty(ref _isDisableShadeOn, value, "IsDisableShadeOn");
            }
        }

        public CustomPrivacyPopupView customPrivacyPopupView
        {
            get
            {
                return MainSwitcher.PopupController.customPrivacyPopupView;
            }
        }

        #endregion

        #region Private Fields And Their Public Properties;

        private UserBasicInfoModel _placeHolderShortInfoModel = new UserBasicInfoModel { PlaceHolderType = 0 };
        public UserBasicInfoModel PlaceHolderShortInfoModel
        {
            get
            {
                return _placeHolderShortInfoModel;
            }
            set
            {
                SetProperty(ref _placeHolderShortInfoModel, value, "PlaceHolderShortInfoModel");

            }
        }

        private ObservableCollection<UserBasicInfoModel> _friendListToShowOrHide = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> FriendListToShowOrHide
        {
            get
            {
                return _friendListToShowOrHide;
            }
            set
            {
                SetProperty(ref _friendListToShowOrHide, value, "FriendListToShowOrHide");
            }
        }

        #endregion

        #region UI Text

        private string _customPrivacyTextBlockText = "Custom Privacy";
        public string CustomPrivacyTextBlockText
        {
            get
            {
                return _customPrivacyTextBlockText;
            }
            set
            {
                SetProperty(ref _customPrivacyTextBlockText, value, "CustomPrivacyTextBlockText");
            }
        }

        #endregion

        #region UI Visibility

        private Visibility _friendPrivacyPopupMainGridVisibility;
        public Visibility FriendPrivacyPopupMainGridVisibility
        {
            get
            {
                return _friendPrivacyPopupMainGridVisibility;
            }
            set
            {
                SetProperty(ref _friendPrivacyPopupMainGridVisibility, value, "FriendPrivacyPopupMainGridVisibility");
            }
        }

        private Visibility _friendPrivacyTypingMessageTextBlockVisibility = Visibility.Visible;
        public Visibility FriendPrivacyTypingMessageTextBlockVisibility
        {
            get
            {
                return _friendPrivacyTypingMessageTextBlockVisibility;
            }
            set
            {
                SetProperty(ref _friendPrivacyTypingMessageTextBlockVisibility, value, "FriendPrivacyTypingMessageTextBlockVisibility");
            }
        }

        #endregion

        #region IsChecked

        private bool _isFriendsToHideChecked = false;
        public bool IsFriendsToHideChecked
        {
            get
            {
                return _isFriendsToHideChecked;
            }
            set
            {
                SetProperty(ref _isFriendsToHideChecked, value, "IsFriendsToHideChecked");
            }
        }

        private bool _isFriendsToShowChecked = false;
        public bool IsFriendsToShowChecked
        {
            get
            {
                return _isFriendsToShowChecked;
            }
            set
            {
                SetProperty(ref _isFriendsToShowChecked, value, "IsFriendsToShowChecked");
            }
        }

        private bool _isFriendsOfFriendsChecked = true;
        public bool IsFriendsOfFriendsChecked
        {
            get
            {
                return _isFriendsOfFriendsChecked;
            }
            set
            {
                SetProperty(ref _isFriendsOfFriendsChecked, value, "IsFriendsOfFriendsChecked");
            }
        }

        #endregion

        #region IsEnabled

        private bool _isFriendsOfFriendsCheckBoxEnabled = true;
        public bool IsFriendsOfFriendsCheckBoxEnabled
        {
            get
            {
                return _isFriendsOfFriendsCheckBoxEnabled;
            }
            set
            {
                SetProperty(ref _isFriendsOfFriendsCheckBoxEnabled, value, "IsFriendsOfFriendsCheckBoxEnabled");
            }
        }

        private bool _isFriendsToShowCheckBoxEnabled = true;
        public bool IsFriendsToShowCheckBoxEnabled
        {
            get
            {
                return _isFriendsToShowCheckBoxEnabled;
            }
            set
            {
                SetProperty(ref _isFriendsToShowCheckBoxEnabled, value, "IsFriendsToShowCheckBoxEnabled");
            }
        }

        private bool _isFriendsToHideCheckBoxEnabled = true;
        public bool IsFriendsToHideCheckBoxEnabled
        {
            get
            {
                return _isFriendsToHideCheckBoxEnabled;
            }
            set
            {
                SetProperty(ref _isFriendsToHideCheckBoxEnabled, value, "IsFriendsToHideCheckBoxEnabled");
            }
        }

        #endregion

        #region ICommand Region

        private ICommand _friendPrivacyPopupMainGridClickCommand;
        public ICommand FriendPrivacyPopupMainGridClickCommand
        {
            get
            {
                _friendPrivacyPopupMainGridClickCommand = _friendPrivacyPopupMainGridClickCommand ?? new RelayCommand(param => onFriendPrivacyPopupMainGridClick(param));
                return _friendPrivacyPopupMainGridClickCommand;
            }
        }

        private ICommand _childGridClickCommand;
        public ICommand ChildGridClickCommand
        {
            get
            {
                _childGridClickCommand = _childGridClickCommand ?? new RelayCommand(param => onChildGridClickCommand(param));
                return _childGridClickCommand;
            }
        }

        private ICommand _cancelButtonClickCommand;
        public ICommand CancelButtonClickCommand
        {
            get
            {
                _cancelButtonClickCommand = _cancelButtonClickCommand ?? new RelayCommand(param => onCancelButtonClick(param));
                return _cancelButtonClickCommand;
            }
        }

        private ICommand _addFriendTextBoxTextChangedEvent;
        public ICommand AddFriendTextBoxTextChangedEvent
        {
            get
            {
                _addFriendTextBoxTextChangedEvent = _addFriendTextBoxTextChangedEvent ?? new RelayCommand(param => onAddFriendTextBoxTextChanged(param));
                return _addFriendTextBoxTextChangedEvent;
            }
        }

        private ICommand _addFriendTextBoxPreviewKeyDownEvent;
        public ICommand AddFriendTextBoxPreviewKeyDownEvent
        {
            get
            {
                _addFriendTextBoxPreviewKeyDownEvent = _addFriendTextBoxPreviewKeyDownEvent ?? new RelayCommand(param => onAddFriendTextBoxPreviewKeyDown(param));
                return _addFriendTextBoxPreviewKeyDownEvent;
            }
        }

        private ICommand _removeAddedFriendButtonClickCommand;
        public ICommand RemoveAddedFriendButtonClickCommand
        {
            get
            {
                _removeAddedFriendButtonClickCommand = _removeAddedFriendButtonClickCommand ?? new RelayCommand(param => onRemoveAddedFriendButtonClick(param));
                return _removeAddedFriendButtonClickCommand;
            }
        }

        private ICommand _submitButtonClickCommand;
        public ICommand SubmitButtonClickCommand
        {
            get
            {
                _submitButtonClickCommand = _submitButtonClickCommand ?? new RelayCommand(param => onSubmitButtonClick(param));
                return _submitButtonClickCommand;
            }
        }

        private ICommand _friendsOfFriendsCheckCommand;
        public ICommand FriendsOfFriendsCheckCommand
        {
            get
            {
                _friendsOfFriendsCheckCommand = _friendsOfFriendsCheckCommand ?? new RelayCommand(param => onFriendsOfFriendsCheck(param));
                return _friendsOfFriendsCheckCommand;
            }
        }

        private ICommand _friendsToShowCheckedCommand;
        public ICommand FriendsToShowCheckedCommand
        {
            get
            {
                _friendsToShowCheckedCommand = _friendsToShowCheckedCommand ?? new RelayCommand(param => onFriendsToShowChecked(param));
                return _friendsToShowCheckedCommand;
            }
        }

        private ICommand _friendsToHideCheckedCommand;
        public ICommand FriendsToHideCheckedCommand
        {
            get
            {
                _friendsToHideCheckedCommand = _friendsToHideCheckedCommand ?? new RelayCommand(param => onFriendsToHideChecked(param));
                return _friendsToHideCheckedCommand;
            }
        }

        private ICommand _friendPrivacyWrapPanelMouseLeftButtonUpCommand;
        public ICommand FriendPrivacyWrapPanelMouseLeftButtonUpCommand
        {
            get
            {
                _friendPrivacyWrapPanelMouseLeftButtonUpCommand = _friendPrivacyWrapPanelMouseLeftButtonUpCommand ?? new RelayCommand(param => onFriendPrivacyWrapPanelMouseLeftButtonUp(param));
                return _friendPrivacyWrapPanelMouseLeftButtonUpCommand;
            }
        }
        private ICommand loadedControl;
        public ICommand LoadedControl
        {
            get
            {
                if (loadedControl == null) loadedControl = new RelayCommand(param => OnLoadedControl());
                return loadedControl;
            }
        }
        private ICommand escapCommand;
        public ICommand EscapeCommand
        {
            get
            {
                if (escapCommand == null) escapCommand = new RelayCommand(param => OnEscapeCommand());
                return escapCommand;
            }
        }
        #endregion

        #region ICommand Executable Methods

        private void OnLoadedControl()
        {
            customPrivacyPopupView.Focusable = true;
            Keyboard.Focus(customPrivacyPopupView);
        }

        public void OnEscapeCommand()
        {
            customPrivacyPopupView.Hide();
            HideCustomPrivacy();
        }

        private void onFriendPrivacyPopupMainGridClick(object param)
        {
            customPrivacyPopupView.Hide();
            HideCustomPrivacy();
        }

        private void onChildGridClickCommand(object param)
        {
            //do nothing
        }

        private void onCancelButtonClick(object param)
        {
            customPrivacyPopupView.Hide();
            HideCustomPrivacy();
            FriendListToShowOrHide.Clear();
            friendPrivacyNewStatusViewModel.PrivacyValue = 25;
        }

        private void onAddFriendTextBoxTextChanged(object param)
        {
            if (param is TextBox)
            {
                TextBox addFriendTextBox = (TextBox)param;

                if (addFriendTextBox.Text.Length > 0)
                {
                    FriendPrivacyTypingMessageTextBlockVisibility = Visibility.Collapsed;
                    if (UCAlphaTagPopUp.Instance == null)
                    {
                        UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
                    }
                    if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
                    {
                        Grid g = (Grid)UCAlphaTagPopUp.Instance.Parent;
                        g.Children.Remove(UCAlphaTagPopUp.Instance);
                    }
                    Grid richGrid = (Grid)addFriendTextBox.Parent;
                    richGrid.Children.Add(UCAlphaTagPopUp.Instance);
                    UCAlphaTagPopUp.Instance.InitializePopUpLocation(addFriendTextBox, this);

                    string searchString = addFriendTextBox.Text;
                    UCAlphaTagPopUp.Instance.ViewSearchFriends(searchString);
                }
                else
                {
                    if (UCAlphaTagPopUp.Instance != null)
                    {
                        UCAlphaTagPopUp.Instance.CloseFriendTagPopUp();
                    }
                }
            }
        }

        private void onAddFriendTextBoxPreviewKeyDown(object param)
        {
            if (param is KeyEventArgs)
            {
                KeyEventArgs pressedKeyEvent = (KeyEventArgs)param;
                TextBox textBox = (TextBox)pressedKeyEvent.KeyboardDevice.FocusedElement;

                if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
                {
                    if (pressedKeyEvent.Key == Key.Down)
                    {
                        pressedKeyEvent.Handled = true;
                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Up)
                    {
                        pressedKeyEvent.Handled = true;
                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Enter || pressedKeyEvent.Key == Key.Tab)
                    {
                        pressedKeyEvent.Handled = true;
                        AddFriendToCollection((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
                        UCAlphaTagPopUp.Instance.CloseFriendTagPopUp();
                        textBox.Clear();
                    }
                }
            }
        }

        private void onRemoveAddedFriendButtonClick(object param)
        {
            if (param is Control)
            {
                Control control = (Control)param;
                UserBasicInfoModel model = (UserBasicInfoModel)control.DataContext;
                FriendListToShowOrHide.Remove(model);
            }
        }

        private void onSubmitButtonClick(object param)
        {
            if (friendPrivacyNewStatusViewModel.PrivacyValue != AppConstants.PRIVACY_FRIENDSOFFRIENDS)
            {
                if (FriendListToShowOrHide.Count == 1) //for one placeholder model
                {
                    friendPrivacyNewStatusViewModel.PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                    //CustomMessageBox.ShowInformation("No Friends Have Been Selected");
                    UIHelperMethods.ShowInformation("Please select one or more contact.", "Action failed!");
                }
                friendPrivacyNewStatusViewModel.FriendsToHideOrShow = FriendListToShowOrHide;
            }
            customPrivacyPopupView.Hide();
            HideCustomPrivacy();
        }

        private void onFriendsOfFriendsCheck(object param)
        {
            toggleRadioButtons(true, false, false);
            IsDisableShadeOn = true;
            friendPrivacyNewStatusViewModel.PrivacyValue = AppConstants.PRIVACY_FRIENDSOFFRIENDS;
        }

        private void onFriendsToShowChecked(object param)
        {
            toggleRadioButtons(false, true, false);
            IsDisableShadeOn = false;
            friendPrivacyNewStatusViewModel.PrivacyValue = AppConstants.PRIVACY_FRIENDSTOSHOW;
        }

        private void onFriendsToHideChecked(object param)
        {
            toggleRadioButtons(false, false, true);
            IsDisableShadeOn = false;
            friendPrivacyNewStatusViewModel.PrivacyValue = AppConstants.PRIVACY_FRIENDSTOHIDE;
        }

        private void onFriendPrivacyWrapPanelMouseLeftButtonUp(object param)
        {
            if (param is WrapPanel)
            {
                TextBox wrapPanelChildTextBox = HelperMethods.FindVisualChild<TextBox>((WrapPanel)param);

                if (wrapPanelChildTextBox != null)
                {
                    wrapPanelChildTextBox.Focusable = true;
                    wrapPanelChildTextBox.Focus();
                }
            }
        }

        #endregion

        #region Public Methods

        public void ShowCustomPrivacy(NewStatusViewModel newStatusViewModel)
        {
            friendPrivacyNewStatusViewModel = newStatusViewModel;
            FriendListToShowOrHide = friendPrivacyNewStatusViewModel.FriendsToHideOrShow;
            if (FriendListToShowOrHide.Count == 0)
            {
                FriendListToShowOrHide.Add(PlaceHolderShortInfoModel);
            }

            if (friendPrivacyNewStatusViewModel.PrivacyValue == AppConstants.PRIVACY_PUBLIC || friendPrivacyNewStatusViewModel.PrivacyValue == AppConstants.PRIVACY_FRIENDSOFFRIENDS)
            {
                IsFriendsOfFriendsChecked = true;
                IsFriendsToHideChecked = false;
                IsFriendsToShowChecked = false;
            }

            friendPrivacyNewStatusViewModel.PrivacyValue = AppConstants.PRIVACY_FRIENDSOFFRIENDS;
            if (friendPrivacyNewStatusViewModel.NewStatusImageUpload.Count > 1 || friendPrivacyNewStatusViewModel.ImageAlbumTitleTextBoxText.Trim().Length > 0
                || friendPrivacyNewStatusViewModel.NewStatusMusicUpload.Count > 1 || friendPrivacyNewStatusViewModel.AudioAlbumTitleTextBoxText.Trim().Length > 0
                || friendPrivacyNewStatusViewModel.NewStatusVideoUpload.Count > 1 || friendPrivacyNewStatusViewModel.VideoAlbumTitleTextBoxText.Trim().Length > 0)
            {
                IsFriendsToHideCheckBoxEnabled = false;
                IsFriendsToShowCheckBoxEnabled = false;
            }
            else
            {
                IsFriendsToShowCheckBoxEnabled = true;
                IsFriendsToHideCheckBoxEnabled = true;
            }
            FriendPrivacyPopupMainGridVisibility = Visibility.Visible;
        }

        public void HideCustomPrivacy()
        {
            FriendPrivacyPopupMainGridVisibility = Visibility.Hidden;
        }

        public void AddFriendToCollection(UserBasicInfoModel model)
        {
            if (FriendListToShowOrHide.Count - 2 >= 0)
            {
                FriendListToShowOrHide.Insert(FriendListToShowOrHide.Count - 1, model);
            }
            else
            {
                FriendListToShowOrHide.Insert(0, model);
            }
        }

        #endregion

        #region Private Methods

        private void toggleRadioButtons(bool friendsOfFriends, bool friendsToShow, bool friendsToHide)
        {
            IsFriendsOfFriendsChecked = friendsOfFriends;
            IsFriendsToShowChecked = friendsToShow;
            IsFriendsToHideChecked = friendsToHide;
        }

        #endregion
    }
}

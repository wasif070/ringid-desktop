<<<<<<< HEAD
﻿using log4net;
using MediaInfoDotNet;
using Microsoft.Win32;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Feed;
using View.UI.PopUp;
using View.UI.Profile.MyProfile;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;

namespace View.ViewModel.NewStatus
{
    public class NewStatusViewModel : BaseViewModel
    {

        #region Private Fields

        private readonly ILog log = LogManager.GetLogger(typeof(NewStatusViewModel).Name);
        private DispatcherTimer timer;
        private string LastMapSearchText = "";
        private TextBox locationTextBox;
        private bool prevState = true;
        List<string> FilesDragged = new List<string>();

        #endregion

        #region Public Fields

        public UCMediaCloudUploadPopup ucMediaCloudUploadPopup = null;
        public string StatusText;
        public string StatusTextWithoutTags;
        public string InputUrl = "";
        public int PreviewLnkType = 0;
        public List<string> AllImagesURLS = new List<string>();
        public int SelectedImageIndex = 0;
        public long SelectedDoingId;
        public List<long> TaggedUtids = null;
        public List<long> FriendsToShowOrHideUtIds = new List<long>();
        public JArray StatusTagsJArray = null;
        public RichTextFeedEdit richTextFeedEditFromViewModel;
        public int FeedWallType;
        public long FriendUtIdOrCircleId;

        #endregion

        #region Constructor

        public NewStatusViewModel()
        {
            NewStatusHashTag.Add(PlaceHolderHashTagModel);
            SetSelectedModelValuesForValidityPopup();
        }

        #endregion

        #region Observable Collections

        private ObservableCollection<ImageUploaderModel> _newStatusImageUpload = new ObservableCollection<ImageUploaderModel>();
        public ObservableCollection<ImageUploaderModel> NewStatusImageUpload
        {
            get
            {
                return _newStatusImageUpload;
            }
            set
            {
                SetProperty(ref _newStatusImageUpload, value, "NewStatusImageUpload");
            }
        }

        private ObservableCollection<HashTagModel> _newStatusHashTag = new ObservableCollection<HashTagModel>();
        public ObservableCollection<HashTagModel> NewStatusHashTag
        {
            get
            {
                return _newStatusHashTag;
            }
            set
            {
                SetProperty(ref _newStatusHashTag, value, "NewStatusHashTag");
            }
        }

        private ObservableCollection<FeedVideoUploaderModel> _newStatusVideoUpload = new ObservableCollection<FeedVideoUploaderModel>();
        public ObservableCollection<FeedVideoUploaderModel> NewStatusVideoUpload
        {
            get
            {
                return _newStatusVideoUpload;
            }
            set
            {
                SetProperty(ref _newStatusVideoUpload, value, "NewStatusVideoUpload");
            }
        }


        private ObservableCollection<MusicUploaderModel> _newStatusMusicUpload = new ObservableCollection<MusicUploaderModel>();
        public ObservableCollection<MusicUploaderModel> NewStatusMusicUpload
        {
            get
            {
                return _newStatusMusicUpload;
            }
            set
            {
                SetProperty(ref _newStatusMusicUpload, value, "NewStatusMusicUpload");
            }
        }

        private ObservableCollection<UserShortInfoModel> _taggedFriends;
        public ObservableCollection<UserShortInfoModel> TaggedFriends
        {
            get
            {
                return _taggedFriends;
            }
            set
            {
                SetProperty(ref _taggedFriends, value, "TaggedFriends");
            }
        }

        private ObservableCollection<UserBasicInfoModel> _friendsToHideOrShow = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> FriendsToHideOrShow
        {
            get
            {
                return _friendsToHideOrShow;
            }
            set
            {
                SetProperty(ref _friendsToHideOrShow, value, "FriendsToHideOrShow");
            }
        }

        #endregion

        #region Properties

        private UCNewStatusFeelingPopup ucNewStatusFeelingPopup { get; set; }

        private UCFeedOptionsPopup ucFeedOptionsPopup { get; set; }

        private UCNewStatusValidityPopup ucNewStatusValidityPopup { get; set; }

        private UCLocationPopUp LocationPopUp { get; set; }

        private UCNewStatusPrivacyPopup ucNewStatusPrivacyPopup { get; set; }

        public UCHashTagPopUp HashTagPopUp
        {
            get
            {
                return MainSwitcher.PopupController.HashTagPopUp;
            }
        }

        private UCNewStatusTagPopup ucNewStatusTagPopup { get; set; }

        #endregion

        #region Private Fields and their Public Properties

        private string _previewDomain = "";
        public string PreviewDomain
        {
            get
            {
                return _previewDomain;
            }
            set
            {
                SetProperty(ref _previewDomain, value, "PreviewDomain");
            }
        }

        private string _previewDesc = "";
        public string PreviewDesc
        {
            get
            {
                return _previewDesc;
            }
            set
            {
                SetProperty(ref _previewDesc, value, "PreviewDesc");
            }
        }

        private string _previewUrl = "";
        public string PreviewUrl
        {
            get
            {
                return _previewUrl;
            }
            set
            {
                SetProperty(ref _previewUrl, value, "PreviewUrl");
            }
        }

        private string _previewImgUrl = "";
        public string PreviewImgUrl
        {
            get
            {
                return _previewImgUrl;
            }
            set
            {
                SetProperty(ref _previewImgUrl, value, "PreviewImgUrl");
            }
        }

        private string _previewTitle = "";
        public string PreviewTitle
        {
            get
            {
                return _previewTitle;
            }
            set
            {
                SetProperty(ref _previewTitle, value, "PreviewTitle");
            }
        }

        private string _uploadingText;
        public string UploadingText
        {
            get
            {
                return _uploadingText;
            }
            set
            {
                SetProperty(ref _uploadingText, value, "UploadingText");
            }
        }

        private HashTagModel _placeHolderHashTagModel = new HashTagModel { TagType = 0 };
        public HashTagModel PlaceHolderHashTagModel
        {
            get
            {
                return _placeHolderHashTagModel;
            }
            set
            {
                SetProperty(ref _placeHolderHashTagModel, value, "PlaceHolderHashTagModel");
            }
        }

        private DoingModel _selectedDoingModel = null;
        public DoingModel SelectedDoingModel
        {
            get
            {
                return _selectedDoingModel;
            }
            set
            {
                SetProperty(ref _selectedDoingModel, value, "SelectedDoingModel");
            }
        }

        private bool _emoPopup;
        public bool EmoPopup
        {
            get
            {
                return _emoPopup;
            }
            set
            {
                SetProperty(ref _emoPopup, value, "EmoPopup");
            }
        }

        private bool _tagPopup;
        public bool TagPopup
        {
            get
            {
                return _tagPopup;
            }
            set
            {
                SetProperty(ref _tagPopup, value, "TagPopup");
            }
        }

        private LocationModel _selectedLocationModel = null;
        public LocationModel SelectedLocationModel
        {
            get
            {
                return _selectedLocationModel;
            }
            set
            {
                SetProperty(ref _selectedLocationModel, value, "SelectedLocationModel");
            }
        }

        private bool _privacyPopup;
        public bool PrivacyPopup
        {
            get
            {
                return _privacyPopup;
            }
            set
            {
                SetProperty(ref _privacyPopup, value, "PrivacyPopup");
            }
        }

        /// <summary>
        /// PRIVACY_ONLY_ME = 1	Only me. None will see this content except me.
        /// PRIVACY_SP_FRIEND2SHOW = 5	This content is visibl only to specific friends.
        /// PRIVACY_SP_FRIEND2HIDE = 10	This content is kept hidden from specific friends.
        /// PRIVACY_FRIEND = 15	Only friends can see this content.
        /// PRIVACY_FOF = 20	Friends of friends can see this content.
        /// PRIVACY_PUBLIC = 25	Anyone can see this content.
        /// </summary>
        private int _privacyValue = AppConstants.PRIVACY_PUBLIC;
        public int PrivacyValue
        {
            get
            {
                return _privacyValue;
            }
            set
            {
                SetProperty(ref _privacyValue, value, "PrivacyValue");
            }
        }

        private ImageSource _postLoader;
        public ImageSource PostLoader
        {
            get
            {
                return _postLoader;
            }
            set
            {
                SetProperty(ref _postLoader, value, "PostLoader");
            }
        }

        private bool _feedViewPopup;
        public bool FeedViewPopup
        {
            get
            {
                return _feedViewPopup;
            }
            set
            {
                SetProperty(ref _feedViewPopup, value, "FeedViewPopup");
            }
        }

        private bool _optionBool = false;
        public bool OptionBool
        {
            get
            {
                return _optionBool;
            }
            set
            {
                SetProperty(ref _optionBool, value, "OptionBool");
            }
        }

        private bool _isDragShadeOn = false;
        public bool IsDragShadeOn
        {
            get
            {
                return _isDragShadeOn;
            }
            set
            {
                _isDragShadeOn = value;
                this.OnPropertyChanged("IsDragShadeOn");
            }
        }

        private int _validityValue = AppConstants.DEFAULT_VALIDITY;
        public int ValidityValue
        {
            get
            {
                return _validityValue;
            }
            set
            {
                SetProperty(ref _validityValue, value, "ValidityValue");
            }
        }

        private bool _isLocationSet = false;
        public bool IsLocationSet
        {
            get
            {
                return _isLocationSet;
            }
            set
            {
                SetProperty(ref _isLocationSet, value, "IsLocationSet");
            }
        }

        private bool _isImageAlbumSelectedFromExisting = false;
        public bool IsImageAlbumSelectedFromExisting
        {
            get
            {
                return _isImageAlbumSelectedFromExisting;
            }
            set
            {
                SetProperty(ref _isImageAlbumSelectedFromExisting, value, "IsImageAlbumSelectedFromExisting");
            }
        }

        private bool _isAudioAlbumSelectedFromExisting = false;
        public bool IsAudioAlbumSelectedFromExisting
        {
            get
            {
                return _isAudioAlbumSelectedFromExisting;
            }
            set
            {
                SetProperty(ref _isAudioAlbumSelectedFromExisting, value, "IsAudioAlbumSelectedFromExisting");
            }
        }

        private bool _isVideoAlbumSelectedFromExisting = false;
        public bool IsVideoAlbumSelectedFromExisting
        {
            get
            {
                return _isVideoAlbumSelectedFromExisting;
            }
            set
            {
                SetProperty(ref _isVideoAlbumSelectedFromExisting, value, "IsVideoAlbumSelectedFromExisting");
            }
        }

        #endregion

        #region UI Visibility

        private Visibility _locationMainBorderPanelVisibility = Visibility.Collapsed;
        public Visibility LocationMainBorderPanelVisibility
        {
            get
            {
                return _locationMainBorderPanelVisibility;
            }
            set
            {
                SetProperty(ref _locationMainBorderPanelVisibility, value, "LocationMainBorderPanelVisibility");
            }
        }

        private Visibility _feelingDoingButtonContainerVisibility = Visibility.Collapsed;
        public Visibility FeelingDoingButtonContainerVisibility
        {
            get
            {
                return _feelingDoingButtonContainerVisibility;
            }
            set
            {
                SetProperty(ref _feelingDoingButtonContainerVisibility, value, "FeelingDoingButtonContainerVisibility");
            }
        }

        private Visibility _tagListHolderVisibility = Visibility.Collapsed;
        public Visibility TagListHolderVisibility
        {
            get
            {
                return _tagListHolderVisibility;
            }
            set
            {
                SetProperty(ref _tagListHolderVisibility, value, "TagListHolderVisibility");
            }
        }

        private Visibility _prevNextVisibility = Visibility.Collapsed;
        public Visibility PrevNextVisibility
        {
            get
            {
                return _prevNextVisibility;
            }
            set
            {
                SetProperty(ref _prevNextVisibility, value, "PrevNextVisibility");
            }
        }

        private Visibility _previewCancelButtonVisibility = Visibility.Collapsed;
        public Visibility PreviewCancelButtonVisibility
        {
            get
            {
                return _previewCancelButtonVisibility;
            }
            set
            {
                SetProperty(ref _previewCancelButtonVisibility, value, "PreviewCancelButtonVisibility");
            }
        }

        private Visibility _statusImagePanelVisibility = Visibility.Collapsed;
        public Visibility StatusImagePanelVisibility
        {
            get
            {
                return _statusImagePanelVisibility;
            }
            set
            {
                SetProperty(ref _statusImagePanelVisibility, value, "StatusImagePanelVisibility");
            }
        }

        private Visibility _audioPanelVisibility = Visibility.Collapsed;
        public Visibility AudioPanelVisibility
        {
            get
            {
                return _audioPanelVisibility;
            }
            set
            {
                SetProperty(ref _audioPanelVisibility, value, "AudioPanelVisibility");
            }
        }

        private Visibility _videoPanelVisibility = Visibility.Collapsed;
        public Visibility VideoPanelVisibility
        {
            get
            {
                return _videoPanelVisibility;
            }
            set
            {
                SetProperty(ref _videoPanelVisibility, value, "VideoPanelVisibility");
            }
        }

        private Visibility _writeSomethingTextBlockVisibility = Visibility.Visible;
        public Visibility WriteSomethingTextBlockVisibility
        {
            get
            {
                return _writeSomethingTextBlockVisibility;
            }
            set
            {
                SetProperty(ref _writeSomethingTextBlockVisibility, value, "WriteSomethingTextBlockVisibility");
            }
        }

        private Visibility _privacyButtonVisibility = Visibility.Visible;
        public Visibility PrivacyButtonVisibility
        {
            get
            {
                return _privacyButtonVisibility;
            }
            set
            {
                SetProperty(ref _privacyButtonVisibility, value, "PrivacyButtonVisibility");
            }
        }

        private Visibility _newStatusTagFriendButtonVisibility = Visibility.Visible;
        public Visibility NewStatusTagFriendButtonVisibility
        {
            get
            {
                return _newStatusTagFriendButtonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusTagFriendButtonVisibility, value, "NewStatusTagFriendButtonVisibility");
            }
        }

        private Visibility _optionsButtonVisibility = Visibility.Visible;
        public Visibility OptionsButtonVisibility
        {
            get
            {
                return _optionsButtonVisibility;
            }
            set
            {
                SetProperty(ref _optionsButtonVisibility, value, "OptionsButtonVisibility");
            }
        }

        private Visibility _bottomLeftStackPanelVisibility = Visibility.Visible;
        public Visibility BottomLeftStackPanelVisibility
        {
            get
            {
                return _bottomLeftStackPanelVisibility;
            }
            set
            {
                SetProperty(ref _bottomLeftStackPanelVisibility, value, "BottomLeftStackPanelVisibility");
            }
        }

        private Visibility _newStatusLocationButtonVisibility = Visibility.Visible;
        public Visibility NewStatusLocationButtonVisibility
        {
            get
            {
                return _newStatusLocationButtonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusLocationButtonVisibility, value, "NewStatusLocationButtonVisibility");
            }
        }

        private Visibility _newStatusEmoticonVisibility = Visibility.Visible;
        public Visibility NewStatusEmoticonVisibility
        {
            get
            {
                return _newStatusEmoticonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusEmoticonVisibility, value, "NewStatusEmoticonVisibility");
            }
        }

        private Visibility _newStatusMusicButtonVisibility = Visibility.Visible;
        public Visibility NewStatusMusicButtonVisibility
        {
            get
            {
                return _newStatusMusicButtonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusMusicButtonVisibility, value, "NewStatusMusicButtonVisibility");
            }
        }

        private Visibility _newStatusVideoButtonVisibility = Visibility.Visible;
        public Visibility NewStatusVideoButtonVisibility
        {
            get
            {
                return _newStatusVideoButtonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusVideoButtonVisibility, value, "NewStatusVideoButtonVisibility");
            }
        }

        private Visibility _uploadPhotoButtonVisibility = Visibility.Visible;
        public Visibility UploadPhotoButtonVisibility
        {
            get
            {
                return _uploadPhotoButtonVisibility;
            }
            set
            {
                SetProperty(ref _uploadPhotoButtonVisibility, value, "UploadPhotoButtonVisibility");
            }
        }

        private Visibility _audioTitlePanelVisibility = Visibility.Visible;
        public Visibility AudioTitlePanelVisibility
        {
            get
            {
                return _audioTitlePanelVisibility;
            }
            set
            {
                SetProperty(ref _audioTitlePanelVisibility, value, "AudioTitlePanelVisibility");
            }
        }

        private Visibility _audioHashTagGridPanelVisibility = Visibility.Visible;
        public Visibility AudioHashTagGridPanelVisibility
        {
            get
            {
                return _audioHashTagGridPanelVisibility;
            }
            set
            {
                SetProperty(ref  _audioHashTagGridPanelVisibility, value, "AudioHashTagGridPanelVisibility");
            }
        }

        private Visibility _videoTitlePanelVisibility = Visibility.Visible;
        public Visibility VideoTitlePanelVisibility
        {
            get
            {
                return _videoTitlePanelVisibility;
            }
            set
            {
                SetProperty(ref _videoTitlePanelVisibility, value, "VideoTitlePanelVisibility");
            }
        }

        private Visibility _videoAddHAshTagGridPanelVisibility = Visibility.Visible;
        public Visibility VideoAddHAshTagGridPanelVisibility
        {
            get
            {
                return _videoAddHAshTagGridPanelVisibility;
            }
            set
            {
                SetProperty(ref _videoAddHAshTagGridPanelVisibility, value, "VideoAddHAshTagGridPanelVisibility");
            }
        }

        private Visibility _imageTitlePanelVisibility = Visibility.Visible;
        public Visibility ImageTitlePanelVisibility
        {
            get
            {
                return _imageTitlePanelVisibility;
            }
            set
            {
                SetProperty(ref _imageTitlePanelVisibility, value, "ImageTitlePanelVisibility");
            }
        }

        private Visibility _selectedImageAlbumCrossButtonVisibility = Visibility.Collapsed;
        public Visibility SelectedImageAlbumCrossButtonVisibility
        {
            get
            {
                return _selectedImageAlbumCrossButtonVisibility;
            }
            set
            {
                SetProperty(ref _selectedImageAlbumCrossButtonVisibility, value, "SelectedImageAlbumCrossButtonVisibility");
            }
        }

        private Visibility _addToImageAlbumButtonVisibility = Visibility.Visible;
        public Visibility AddToImageAlbumButtonVisibility
        {
            get
            {
                return _addToImageAlbumButtonVisibility;
            }
            set
            {
                SetProperty(ref _addToImageAlbumButtonVisibility, value, "AddToImageAlbumButtonVisibility");
            }
        }

        private Visibility _selectedAudioAlbumCrossButtonVisibility = Visibility.Collapsed;
        public Visibility SelectedAudioAlbumCrossButtonVisibility
        {
            get
            {
                return _selectedAudioAlbumCrossButtonVisibility;
            }
            set
            {
                SetProperty(ref _selectedAudioAlbumCrossButtonVisibility, value, "SelectedAudioAlbumCrossButtonVisibility");
            }
        }

        private Visibility _addtoAudioAlbumButtonVisibility = Visibility.Visible;
        public Visibility AddtoAudioAlbumButtonVisibility
        {
            get
            {
                return _addtoAudioAlbumButtonVisibility;
            }
            set
            {
                SetProperty(ref _addtoAudioAlbumButtonVisibility, value, "AddtoAudioAlbumButtonVisibility");
            }
        }

        private Visibility _selectedVideoAlbumCrossButtonVisibility = Visibility.Collapsed;
        public Visibility SelectedVideoAlbumCrossButtonVisibility
        {
            get
            {
                return _selectedVideoAlbumCrossButtonVisibility;
            }
            set
            {
                SetProperty(ref _selectedVideoAlbumCrossButtonVisibility, value, "SelectedVideoAlbumCrossButtonVisibility");
            }
        }

        private Visibility _addtoVideoAlbumButtonVisibility = Visibility.Visible;
        public Visibility AddtoVideoAlbumButtonVisibility
        {
            get
            {
                return _addtoVideoAlbumButtonVisibility;
            }
            set
            {
                SetProperty(ref _addtoVideoAlbumButtonVisibility, value, "AddtoVideoAlbumButtonVisibility");
            }
        }

        #endregion

        #region UI IsOpen

        private bool _isPopupUploadPhotoOpen = false;
        public bool IsPopupUploadPhotoOpen
        {
            get
            {
                return _isPopupUploadPhotoOpen;
            }
            set
            {
                SetProperty(ref _isPopupUploadPhotoOpen, value, "IsPopupUploadPhotoOpen");
            }
        }

        private bool _isPopupUploadVideoOpen = false;
        public bool IsPopupUploadVideoOpen
        {
            get
            {
                return _isPopupUploadVideoOpen;
            }
            set
            {
                SetProperty(ref _isPopupUploadVideoOpen, value, "IsPopupUploadVideoOpen");
            }
        }

        #endregion

        #region UI IsEnabled

        private bool _isRtbNewStatusAreaEnabled = true;
        public bool IsRtbNewStatusAreaEnabled
        {
            get
            {
                return _isRtbNewStatusAreaEnabled;
            }
            set
            {
                SetProperty(ref _isRtbNewStatusAreaEnabled, value, "IsRtbNewStatusAreaEnabled");
            }
        }

        private bool _isPostButtonEnabled = true;
        public bool IsPostButtonEnabled
        {
            get
            {
                return _isPostButtonEnabled;
            }
            set
            {
                SetProperty(ref _isPostButtonEnabled, value, "IsPostButtonEnabled");
            }
        }

        private bool _hashTagBoxEnabled = true;
        public bool HashTagBoxEnabled
        {
            get
            {
                return _hashTagBoxEnabled;
            }
            set
            {
                SetProperty(ref _hashTagBoxEnabled, value, "HashTagBoxEnabled");
            }
        }

        private bool _isPreviewCancelButtonEnabled = true;
        public bool IsPreviewCancelButtonEnabled
        {
            get
            {
                return _isPreviewCancelButtonEnabled;
            }
            set
            {
                SetProperty(ref _isPreviewCancelButtonEnabled, value, "IsPreviewCancelButtonEnabled");
            }
        }

        private bool _isAudioAlbumTitleTextBoxEnabled = true;
        public bool IsAudioAlbumTitleTextBoxEnabled
        {
            get
            {
                return _isAudioAlbumTitleTextBoxEnabled;
            }
            set
            {
                SetProperty(ref _isAudioAlbumTitleTextBoxEnabled, value, "IsAudioAlbumTitleTextBoxEnabled");
            }
        }

        private bool _isVideoAlbumTitleTextBoxEnabled = true;
        public bool IsVideoAlbumTitleTextBoxEnabled
        {
            get
            {
                return _isVideoAlbumTitleTextBoxEnabled;
            }
            set
            {
                SetProperty(ref _isVideoAlbumTitleTextBoxEnabled, value, "IsVideoAlbumTitleTextBoxEnabled");
            }

        }

        private bool _isAddtoAudioAlbumButtonEnabled = true;
        public bool IsAddtoAudioAlbumButtonEnabled
        {
            get
            {
                return _isAddtoAudioAlbumButtonEnabled;
            }
            set
            {
                SetProperty(ref _isAddtoAudioAlbumButtonEnabled, value, "IsAddtoAudioAlbumButtonEnabled");
            }

        }

        private bool _isAddtoVideoAlbumButtonEnabled = true;
        public bool IsAddtoVideoAlbumButtonEnabled
        {
            get
            {
                return _isAddtoVideoAlbumButtonEnabled;
            }
            set
            {
                SetProperty(ref _isAddtoVideoAlbumButtonEnabled, value, "IsAddtoVideoAlbumButtonEnabled");
            }

        }

        private bool _isUploadPhotoButtonEnabled = true;
        public bool IsUploadPhotoButtonEnabled
        {
            get
            {
                return _isUploadPhotoButtonEnabled;
            }
            set
            {
                SetProperty(ref _isUploadPhotoButtonEnabled, value, "IsUploadPhotoButtonEnabled");
            }

        }

        private bool _isNewStatusEmoticonEnabled = true;
        public bool IsNewStatusEmoticonEnabled
        {
            get
            {
                return _isNewStatusEmoticonEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusEmoticonEnabled, value, "IsNewStatusEmoticonEnabled");
            }

        }

        private bool _isNewStatusTagFriendEnabled = true;
        public bool IsNewStatusTagFriendEnabled
        {
            get
            {
                return _isNewStatusTagFriendEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusTagFriendEnabled, value, "IsNewStatusTagFriendEnabled");
            }

        }

        private bool _isNewStatusMusicEnabled = true;
        public bool IsNewStatusMusicEnabled
        {
            get
            {
                return _isNewStatusMusicEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusMusicEnabled, value, "IsNewStatusMusicEnabled");
            }

        }

        private bool _isNewStatusVideoButtonEnabled = true;
        public bool IsNewStatusVideoButtonEnabled
        {
            get
            {
                return _isNewStatusVideoButtonEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusVideoButtonEnabled, value, "IsNewStatusVideoButtonEnabled");
            }

        }

        private bool _isNewStatusLocationButtonEnabled = true;
        public bool IsNewStatusLocationButtonEnabled
        {
            get
            {
                return _isNewStatusLocationButtonEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusLocationButtonEnabled, value, "IsNewStatusLocationButtonEnabled");
            }

        }

        private bool _isPrivacyButtonEnabled = true;
        public bool IsPrivacyButtonEnabled
        {
            get
            {
                return _isPrivacyButtonEnabled;
            }
            set
            {
                SetProperty(ref _isPrivacyButtonEnabled, value, "IsPrivacyButtonEnabled");
            }

        }

        private bool _isValidityButtonEnabled = true;
        public bool IsValidityButtonEnabled
        {
            get
            {
                return _isValidityButtonEnabled;
            }
            set
            {
                SetProperty(ref _isValidityButtonEnabled, value, "IsValidityButtonEnabled");
            }

        }

        private bool _isLinkPreviewImagePreviousButtonEnabled = false;
        public bool IsLinkPreviewImagePreviousButtonEnabled
        {
            get
            {
                return _isLinkPreviewImagePreviousButtonEnabled;
            }
            set
            {
                SetProperty(ref _isLinkPreviewImagePreviousButtonEnabled, value, "IsLinkPreviewImagePreviousButtonEnabled");
            }
        }

        private bool _isLinkPreviewImageNextButtonEnabled = true;
        public bool IsLinkPreviewImageNextButtonEnabled
        {
            get
            {
                return _isLinkPreviewImageNextButtonEnabled;
            }
            set
            {
                SetProperty(ref _isLinkPreviewImageNextButtonEnabled, value, "IsLinkPreviewImageNextButtonEnabled");
            }
        }

        private bool _isImageAlbumTitleTextBoxEnabled = true;
        public bool IsImageAlbumTitleTextBoxEnabled
        {
            get
            {
                return _isImageAlbumTitleTextBoxEnabled;
            }
            set
            {
                SetProperty(ref _isImageAlbumTitleTextBoxEnabled, value, "IsImageAlbumTitleTextBoxEnabled");
            }
        }

        private bool _isAddtoImageAlbumButtonEnabled = true;
        public bool IsAddtoImageAlbumButtonEnabled
        {
            get
            {
                return _isAddtoImageAlbumButtonEnabled;
            }
            set
            {
                SetProperty(ref _isAddtoImageAlbumButtonEnabled, value, "IsAddtoImageAlbumButtonEnabled");
            }
        }

        private bool _isAudioAddHashTagWrapPanelEnabled = true;
        public bool IsAudioAddHashTagWrapPanelEnabled
        {
            get
            {
                return _isAudioAddHashTagWrapPanelEnabled;
            }
            set
            {
                SetProperty(ref _isAudioAddHashTagWrapPanelEnabled, value, "IsAudioAddHashTagWrapPanelEnabled");
            }
        }

        private bool _isVideoAddHashTagGridPanelEnabled = true;
        public bool IsVideoAddHashTagGridPanelEnabled
        {
            get
            {
                return _isVideoAddHashTagGridPanelEnabled;
            }
            set
            {
                SetProperty(ref _isVideoAddHashTagGridPanelEnabled, value, "IsVideoAddHashTagGridPanelEnabled");
            }
        }

        private bool _isSelectedImageAlbumCrossButtonEnabled = true;
        public bool IsSelectedImageAlbumCrossButtonEnabled
        {
            get
            {
                return _isSelectedImageAlbumCrossButtonEnabled;
            }
            set
            {
                SetProperty(ref _isSelectedImageAlbumCrossButtonEnabled, value, "IsSelectedImageAlbumCrossButtonEnabled");
            }
        }

        private bool _isSelectedAudioAlbumCrossButtonEnabled = true;
        public bool IsSelectedAudioAlbumCrossButtonEnabled
        {
            get
            {
                return _isSelectedAudioAlbumCrossButtonEnabled;
            }
            set
            {
                SetProperty(ref _isSelectedAudioAlbumCrossButtonEnabled, value, "IsSelectedAudioAlbumCrossButtonEnabled");
            }
        }

        private bool _isSelectedVideoAlbumCrossButtonEnabled = true;
        public bool IsSelectedVideoAlbumCrossButtonEnabled
        {
            get
            {
                return _isSelectedVideoAlbumCrossButtonEnabled;
            }
            set
            {
                SetProperty(ref _isSelectedVideoAlbumCrossButtonEnabled, value, "IsSelectedVideoAlbumCrossButtonEnabled");
            }
        }

        #endregion

        #region UI Text

        private string _locationTextBoxText;
        public string LocationTextBoxText
        {
            get
            {
                return _locationTextBoxText;
            }
            set
            {
                SetProperty(ref _locationTextBoxText, value, "LocationTextBoxText");
            }
        }

        private string _writeOnText;
        public string WriteOnText
        {
            get
            {
                return _writeOnText;
            }
            set
            {
                SetProperty(ref _writeOnText, value, "WriteOnText");
            }
        }

        private string _videoAlbumTitleTextBoxText = "";
        public string VideoAlbumTitleTextBoxText
        {
            get
            {
                return _videoAlbumTitleTextBoxText;
            }
            set
            {
                SetProperty(ref _videoAlbumTitleTextBoxText, value, "VideoAlbumTitleTextBoxText");
            }
        }

        private string _audioAlbumTitleTextBoxText = "";
        public string AudioAlbumTitleTextBoxText
        {
            get
            {
                return _audioAlbumTitleTextBoxText;
            }
            set
            {
                SetProperty(ref _audioAlbumTitleTextBoxText, value, "AudioAlbumTitleTextBoxText");
            }
        }

        private string _writePostText = "";
        public string WritePostText
        {
            get
            {
                return _writePostText;
            }
            set
            {
                SetProperty(ref _writePostText, value, "WritePostText");
            }
        }

        private string _validityToolTipText = "Post Validity Unlimited";
        public string ValidityToolTipText
        {
            get
            {
                return _validityToolTipText;
            }
            set
            {
                SetProperty(ref _validityToolTipText, value, "ValidityToolTipText");
            }
        }

        private string _imageAlbumTitleTextBoxText = "";
        public string ImageAlbumTitleTextBoxText
        {
            get
            {
                return _imageAlbumTitleTextBoxText;
            }
            set
            {
                SetProperty(ref _imageAlbumTitleTextBoxText, value, "ImageAlbumTitleTextBoxText");
            }
        }

        #endregion

        #region UI IsChecked

        private bool _updateLinkImage;
        public bool UpdateLinkImage
        {
            get
            {
                return _updateLinkImage;
            }
            set
            {
                SetProperty(ref _updateLinkImage, value, "UpdateLinkImage");
            }
        }


        #endregion

        #region ICommand Region

        private ICommand _optionsButtonClickCommand;
        public ICommand OptionsButtonClickCommand
        {
            get
            {
                _optionsButtonClickCommand = _optionsButtonClickCommand ?? new RelayCommand(param => onOptionsButtonClick(param));
                return _optionsButtonClickCommand;
            }
        }

        private ICommand _validityButtonClickCommand;
        public ICommand ValidityButtonClickCommand
        {
            get
            {
                _validityButtonClickCommand = _validityButtonClickCommand ?? new RelayCommand(param => onValidityButtonClick(param));
                return _validityButtonClickCommand;
            }
        }

        private ICommand _postButtonClickCommand;
        public ICommand PostButtonClickCommand
        {
            get
            {
                _postButtonClickCommand = _postButtonClickCommand ?? new RelayCommand(param => onPostButtonClick(param));
                return _postButtonClickCommand;
            }
        }

        private ICommand _uploadPhotoButtonClickCommand;
        public ICommand UploadPhotoButtonClickCommand
        {
            get
            {
                _uploadPhotoButtonClickCommand = _uploadPhotoButtonClickCommand ?? new RelayCommand(param => onUploadPhotoButtonClick(param));
                return _uploadPhotoButtonClickCommand;
            }
        }

        private ICommand _newStatusVideoButtonClickCommand;
        public ICommand NewStatusVideoButtonClickCommand
        {
            get
            {
                _newStatusVideoButtonClickCommand = _newStatusVideoButtonClickCommand ?? new RelayCommand(param => onNewStatusVideoButtonClick(param));
                return _newStatusVideoButtonClickCommand;
            }
        }

        private ICommand _takePhotoClickCommand;
        public ICommand TakePhotoClickCommand
        {
            get
            {
                _takePhotoClickCommand = _takePhotoClickCommand ?? new RelayCommand(param => onTakePhotoClick(param));
                return _takePhotoClickCommand;
            }
        }

        private ICommand _uploadFromComputerClickCommand;
        public ICommand UploadFromComputerClickCommand
        {
            get
            {
                _uploadFromComputerClickCommand = _uploadFromComputerClickCommand ?? new RelayCommand(param => onUploadFromComputerClick(param));
                return _uploadFromComputerClickCommand;
            }
        }

        private ICommand _chooseFromAlbumClickCommand;
        public ICommand ChooseFromAlbumClickCommand
        {
            get
            {
                _chooseFromAlbumClickCommand = _chooseFromAlbumClickCommand ?? new RelayCommand(param => onChooseFromAlbumClick(param));
                return _chooseFromAlbumClickCommand;
            }
        }

        private ICommand _newStatusEmoticonClickCommand;
        public ICommand NewStatusEmoticonClickCommand
        {
            get
            {
                _newStatusEmoticonClickCommand = _newStatusEmoticonClickCommand ?? new RelayCommand(param => onNewStatusEmoticonClick(param));
                return _newStatusEmoticonClickCommand;
            }
        }

        private ICommand _newStatusTagFriendClickCommand;
        public ICommand NewStatusTagFriendClickCommand
        {
            get
            {
                _newStatusTagFriendClickCommand = _newStatusTagFriendClickCommand ?? new RelayCommand(param => onNewsStatusTagFriendClick(param));
                return _newStatusTagFriendClickCommand;
            }
        }

        private ICommand _removeFriendTagClickCommand;
        public ICommand RemoveFriendTagClickCommand
        {
            get
            {
                _removeFriendTagClickCommand = _removeFriendTagClickCommand ?? new RelayCommand(param => onRemoveFriendTagClick(param));
                return _removeFriendTagClickCommand;
            }
        }

        private ICommand _newStatusLocationButtonClickCommand;
        public ICommand NewStatusLocationButtonClickCommand
        {
            get
            {
                _newStatusLocationButtonClickCommand = _newStatusLocationButtonClickCommand ?? new RelayCommand(param => onNewStatusLocationButtonClick(param));
                return _newStatusLocationButtonClickCommand;
            }
        }

        private ICommand _locationTextBoxLostFocusCommand;
        public ICommand LocationTextBoxLostFocusCommand
        {
            get
            {
                _locationTextBoxLostFocusCommand = _locationTextBoxLostFocusCommand ?? new RelayCommand(param => onLocationTextBoxLostFocus(param));
                return _locationTextBoxLostFocusCommand;
            }
        }

        private ICommand _locationTextBoxTextChangedCommand;
        public ICommand LocationTextBoxTextChangedCommand
        {
            get
            {
                _locationTextBoxTextChangedCommand = _locationTextBoxTextChangedCommand ?? new RelayCommand(param => onLocationTextBoxTextChanged(param));
                return _locationTextBoxTextChangedCommand;
            }
        }

        private ICommand _locationTextBoxPreviewKeyDownCommand;
        public ICommand LocationTextBoxPreviewKeyDownCommand
        {
            get
            {
                _locationTextBoxPreviewKeyDownCommand = _locationTextBoxPreviewKeyDownCommand ?? new RelayCommand(param => onLocationTextBoxPreviewKeyDown(param));
                return _locationTextBoxPreviewKeyDownCommand;
            }
        }

        private ICommand _textLocationCrossButtonClickCommand;
        public ICommand TextLocationCrossButtonClickCommand
        {
            get
            {
                _textLocationCrossButtonClickCommand = _textLocationCrossButtonClickCommand ?? new RelayCommand(param => onTextLocationCrossButtonClick(param));
                return _textLocationCrossButtonClickCommand;
            }
        }

        private ICommand _privacyButtonClickCommand;
        public ICommand PrivacyButtonClickCommand
        {
            get
            {
                _privacyButtonClickCommand = _privacyButtonClickCommand ?? new RelayCommand(param => onPrivacyButtonClick(param));
                return _privacyButtonClickCommand;
            }
        }

        private ICommand _previewCancelButtonClickCommand;
        public ICommand PreviewCancelButtonClickCommand
        {
            get
            {
                _previewCancelButtonClickCommand = _previewCancelButtonClickCommand ?? new RelayCommand(param => onPreviewCancelButtonClick(param));
                return _previewCancelButtonClickCommand;
            }
        }

        private ICommand _rtbNewStatusAreaTextChangedCommand;
        public ICommand RtbNewStatusAreaTextChangedCommand
        {
            get
            {
                _rtbNewStatusAreaTextChangedCommand = _rtbNewStatusAreaTextChangedCommand ?? new RelayCommand(param => onRtbNewStatusAreaTextChanged(param));
                return _rtbNewStatusAreaTextChangedCommand;
            }
        }

        private ICommand _rtbNewStatusAreaPreviewKeyDownCommand;
        public ICommand RtbNewStatusAreaPreviewKeyDownCommand
        {
            get
            {
                _rtbNewStatusAreaPreviewKeyDownCommand = _rtbNewStatusAreaPreviewKeyDownCommand ?? new RelayCommand(param => onRtbNewStatusAreaPreviewKeyDown(param));
                return _rtbNewStatusAreaPreviewKeyDownCommand;
            }
        }

        private ICommand _rtbNewStatusAreaPreviewMouseLeftButtonDownCommand;
        public ICommand RtbNewStatusAreaPreviewMouseLeftButtonDownCommand
        {
            get
            {
                _rtbNewStatusAreaPreviewMouseLeftButtonDownCommand = _rtbNewStatusAreaPreviewMouseLeftButtonDownCommand ?? new RelayCommand(param => onRtbNewStatusAreaPreviewMouseLeftButtonDown(param));
                return _rtbNewStatusAreaPreviewMouseLeftButtonDownCommand;
            }
        }

        private ICommand _newStatusMusicClickCommand;
        public ICommand NewStatusMusicClickCommand
        {
            get
            {
                _newStatusMusicClickCommand = _newStatusMusicClickCommand ?? new RelayCommand(param => onNewStatusMusicClick(param));
                return _newStatusMusicClickCommand;
            }
        }

        private ICommand _addtoAudioAlbumButtonClickCommand;
        public ICommand AddtoAudioAlbumButtonClickCommand
        {
            get
            {
                _addtoAudioAlbumButtonClickCommand = _addtoAudioAlbumButtonClickCommand ?? new RelayCommand(param => onAddtoAudioAlbumButtonClick(param));
                return _addtoAudioAlbumButtonClickCommand;

            }
        }

        private ICommand _borderMouseEnterEvent;
        public ICommand BorderMouseEnterEvent
        {
            get
            {
                _borderMouseEnterEvent = _borderMouseEnterEvent ?? new RelayCommand(param => onBorderMouseEnter(param));
                return _borderMouseEnterEvent;
            }
        }

        private ICommand _borderMouseLeaveEvent;
        public ICommand BorderMouseLeaveEvent
        {
            get
            {
                _borderMouseLeaveEvent = _borderMouseLeaveEvent ?? new RelayCommand(param => onBorderMouseLeave(param));
                return _borderMouseLeaveEvent;
            }
        }

        private ICommand _removeHashtagButtonClickCommand;
        public ICommand RemoveHashtagButtonClickCommand
        {
            get
            {
                _removeHashtagButtonClickCommand = _removeHashtagButtonClickCommand ?? new RelayCommand(param => onRemoveHashtagButtonClick(param));
                return _removeHashtagButtonClickCommand;
            }
        }

        private ICommand _audioHashTagWrapPanelMouseLeftButtonUpCommand;
        public ICommand AudioHashTagWrapPanelMouseLeftButtonUpCommand
        {
            get
            {
                _audioHashTagWrapPanelMouseLeftButtonUpCommand = _audioHashTagWrapPanelMouseLeftButtonUpCommand ?? new RelayCommand(param => onHashTagWrapPanelMouseLeftButtonUp(param));
                return _audioHashTagWrapPanelMouseLeftButtonUpCommand;
            }
        }

        private ICommand _audioAddHashTagTextBoxTextChangedEvent;
        public ICommand AudioAddHashTagTextBoxTextChangedEvent
        {
            get
            {
                _audioAddHashTagTextBoxTextChangedEvent = _audioAddHashTagTextBoxTextChangedEvent ?? new RelayCommand(param => onAudioAddHashTagTextBoxTextChanged(param));
                return _audioAddHashTagTextBoxTextChangedEvent;
            }
        }

        private ICommand _audioAddHashTagTextBoxPreviewKeyDownCommand;
        public ICommand AudioAddHashTagTextBoxPreviewKeyDownCommand
        {
            get
            {
                _audioAddHashTagTextBoxPreviewKeyDownCommand = _audioAddHashTagTextBoxPreviewKeyDownCommand ?? new RelayCommand(param => onAudioAddHashTagTextBoxPreviewKeyDown(param));
                return _audioAddHashTagTextBoxPreviewKeyDownCommand;
            }
        }

        private ICommand _videoHashTagWrapPanelMouseLeftButtonUpCommand;
        public ICommand VideoHashTagWrapPanelMouseLeftButtonUpCommand
        {
            get
            {
                _videoHashTagWrapPanelMouseLeftButtonUpCommand = _videoHashTagWrapPanelMouseLeftButtonUpCommand ?? new RelayCommand(param => onHashTagWrapPanelMouseLeftButtonUp(param));
                return _videoHashTagWrapPanelMouseLeftButtonUpCommand;
            }
        }

        private ICommand _addtoVideoAlbumButtonClickCommand;
        public ICommand AddtoVideoAlbumButtonClickCommand
        {
            get
            {
                _addtoVideoAlbumButtonClickCommand = _addtoVideoAlbumButtonClickCommand ?? new RelayCommand(param => onAddtoVideoAlbumButtonClick(param));
                return _addtoVideoAlbumButtonClickCommand;
            }
        }

        private ICommand _recordVideoClickCommand;
        public ICommand RecordVideoClickCommand
        {
            get
            {
                _recordVideoClickCommand = _recordVideoClickCommand ?? new RelayCommand(param => onRecordVideoClick(param));
                return _recordVideoClickCommand;
            }
        }

        private ICommand _uploadVideoFromComputerClickCommand;
        public ICommand UploadVideoFromComputerClickCommand
        {
            get
            {
                _uploadVideoFromComputerClickCommand = _uploadVideoFromComputerClickCommand ?? new RelayCommand(param => onUploadVideoFromComputerClick(param));
                return _uploadVideoFromComputerClickCommand;
            }
        }

        private ICommand _linkImageThumbnailCheckBoxCheckedCommand;
        public ICommand LinkImageThumbnailCheckBoxCheckedCommand
        {
            get
            {
                _linkImageThumbnailCheckBoxCheckedCommand = _linkImageThumbnailCheckBoxCheckedCommand ?? new RelayCommand(param => onLinkImageThumbnailCheckBoxChecked(param));
                return _linkImageThumbnailCheckBoxCheckedCommand;
            }
        }

        private ICommand _linkPreviewImagePreviousButtonClickCommand;
        public ICommand LinkPreviewImagePreviousButtonClickCommand
        {
            get
            {
                _linkPreviewImagePreviousButtonClickCommand = _linkPreviewImagePreviousButtonClickCommand ?? new RelayCommand(param => onLinkPreviewImagePreviousButtonClick(param));
                return _linkPreviewImagePreviousButtonClickCommand;
            }
        }

        private ICommand _linkPreviewImageNextButtonClickCommand;
        public ICommand LinkPreviewImageNextButtonClickCommand
        {
            get
            {
                _linkPreviewImageNextButtonClickCommand = _linkPreviewImageNextButtonClickCommand ?? new RelayCommand(param => onLinkPreviewImageNextButtonClick(param));
                return _linkPreviewImageNextButtonClickCommand;
            }
        }

        private ICommand _cancelLinkPreviewButtonClickCommand;
        public ICommand CancelLinkPreviewButtonClickCommand
        {
            get
            {
                _cancelLinkPreviewButtonClickCommand = _cancelLinkPreviewButtonClickCommand ?? new RelayCommand(param => onCancelLinkPreviewButtonClick(param));
                return _cancelLinkPreviewButtonClickCommand;
            }
        }

        private ICommand _feelingDoingCancelButtonClickCommand;
        public ICommand FeelingDoingCancelButtonClickCommand
        {
            get
            {
                _feelingDoingCancelButtonClickCommand = _feelingDoingCancelButtonClickCommand ?? new RelayCommand(param => onFeelingDoingCancelButtonClick(param));
                return _feelingDoingCancelButtonClickCommand;
            }
        }

        private ICommand _rtbNewStatusAreaDragOverEventCommand;
        public ICommand RtbNewStatusAreaDragOverEventCommand
        {
            get
            {
                _rtbNewStatusAreaDragOverEventCommand = _rtbNewStatusAreaDragOverEventCommand ?? new RelayCommand(param => onRtbNewStatusAreaDragOverEvent(param));
                return _rtbNewStatusAreaDragOverEventCommand;
            }
        }

        private ICommand _rtbNewStatusAreaDropEventCommand;
        public ICommand RtbNewStatusAreaDropEventCommand
        {
            get
            {
                _rtbNewStatusAreaDropEventCommand = _rtbNewStatusAreaDropEventCommand ?? new RelayCommand(param => onRtbNewStatusAreaDropEvent(param));
                return _rtbNewStatusAreaDropEventCommand;
            }
        }

        private ICommand _rtbNewStatusAreaPreviewDragLeaveEventCommand;
        private ICommand RtbNewStatusAreaPreviewDragLeaveEventCommand
        {
            get
            {
                _rtbNewStatusAreaPreviewDragLeaveEventCommand = _rtbNewStatusAreaPreviewDragLeaveEventCommand ?? new RelayCommand(param => onRtbNewStatusAreaPreviewDragLeaveEvent(param));
                return _rtbNewStatusAreaPreviewDragLeaveEventCommand;
            }
        }

        private ICommand _addtoImageAlbumButtonClickCommand;
        public ICommand AddtoImageAlbumButtonClickCommand
        {
            get
            {
                _addtoImageAlbumButtonClickCommand = _addtoImageAlbumButtonClickCommand ?? new RelayCommand(param => onAddtoImageAlbumButtonClick(param));
                return _addtoImageAlbumButtonClickCommand;
            }
        }

        private ICommand _selectedImageAlbumCrossButtonClickCommand;
        public ICommand SelectedImageAlbumCrossButtonClickCommand
        {
            get
            {
                _selectedImageAlbumCrossButtonClickCommand = _selectedImageAlbumCrossButtonClickCommand ?? new RelayCommand(param => onSelectedImageAlbumCrossButtonClick(param));
                return _selectedImageAlbumCrossButtonClickCommand;
            }
        }

        private ICommand _selectedAudioAlbumCrossButtonClickCommand;
        public ICommand SelectedAudioAlbumCrossButtonClickCommand
        {
            get
            {
                _selectedAudioAlbumCrossButtonClickCommand = _selectedAudioAlbumCrossButtonClickCommand ?? new RelayCommand(param => onSelectedAudioAlbumCrossButtonClick(param));
                return _selectedAudioAlbumCrossButtonClickCommand;
            }
        }

        private ICommand _selectedVideoAlbumCrossButtonClickCommand;
        public ICommand SelectedVideoAlbumCrossButtonClickCommand
        {
            get
            {
                _selectedVideoAlbumCrossButtonClickCommand = _selectedVideoAlbumCrossButtonClickCommand ?? new RelayCommand(param => onSelectedVideoAlbumCrossButtonClick(param));
                return _selectedVideoAlbumCrossButtonClickCommand;
            }
        }

        private ICommand _audioAlbumTitleTextBoxTextChangedEvent;
        public ICommand AudioAlbumTitleTextBoxTextChangedEvent
        {
            get
            {
                _audioAlbumTitleTextBoxTextChangedEvent = _audioAlbumTitleTextBoxTextChangedEvent ?? new RelayCommand(param => onAudioAlbumTitleTextBoxTextChanged(param));
                return _audioAlbumTitleTextBoxTextChangedEvent;
            }
        }

        private ICommand _imageAlbumTitleTextBoxTextChangedEvent;
        public ICommand ImageAlbumTitleTextBoxTextChangedEvent
        {
            get
            {
                _imageAlbumTitleTextBoxTextChangedEvent = _imageAlbumTitleTextBoxTextChangedEvent ?? new RelayCommand(param => onImageAlbumTitleTextBoxTextChanged(param));
                return _imageAlbumTitleTextBoxTextChangedEvent;
            }
        }

        private ICommand _videoAlbumTitleTextBoxTextChangedEvent;
        public ICommand VideoAlbumTitleTextBoxTextChangedEvent
        {
            get
            {
                _videoAlbumTitleTextBoxTextChangedEvent = _videoAlbumTitleTextBoxTextChangedEvent ?? new RelayCommand(param => onVideoAlbumTitleTextBoxTextChanged(param));
                return _videoAlbumTitleTextBoxTextChangedEvent;
            }
        }

        #endregion

        #region Location Popup

        private void locationPopupView()
        {
            if (LocationPopUp != null)
            {
                UCGuiRingID.Instance.MotherPanel.Children.Remove(LocationPopUp);
            }
            LocationPopUp = new UCLocationPopUp(UCGuiRingID.Instance.MotherPanel);

            LocationPopUp.OnRemovedUserControl += () =>
            {
                LocationPopUp = null;
            };
        }

        #endregion

        #region ICommand Executable Methods

        private void onOptionsButtonClick(object param)
        {
            if (param is Button)
            {
                Button validityButton = param as Button;
                FeedViewPopup = true;
                if (ucFeedOptionsPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucFeedOptionsPopup);
                }
                ucFeedOptionsPopup = new UCFeedOptionsPopup();
                ucFeedOptionsPopup.SetParent(UCGuiRingID.Instance.MotherPanel);
                ucFeedOptionsPopup.Show();
                ucFeedOptionsPopup.ShowPopup(validityButton, this);
                ucFeedOptionsPopup.OnRemovedUserControl += () =>
                {
                    FeedViewPopup = false;
                    ucFeedOptionsPopup = null;
                };
            }
        }

        private void onValidityButtonClick(object param)
        {
            if (param is Button)
            {
                Button validityButton = param as Button;

                if (ucNewStatusValidityPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNewStatusValidityPopup);
                }
                ucNewStatusValidityPopup = new UCNewStatusValidityPopup(UCGuiRingID.Instance.MotherPanel);
                ucNewStatusValidityPopup.Show();
                ucNewStatusValidityPopup.OnRemovedUserControl += () =>
                {
                    ucNewStatusValidityPopup = null;
                };

                if (MainSwitcher.PopupController.EditStatusCommentView != null && MainSwitcher.PopupController.EditStatusCommentView.IsVisible)
                {
                    ucNewStatusValidityPopup.ShowPopup(validityButton, this, MainSwitcher.PopupController.EditStatusCommentView.FeedModel.Validity);
                }
                else
                {
                    ucNewStatusValidityPopup.ShowPopup(validityButton, this);
                }
            }
        }

        private void onPostButtonClick(object param)
        {
            if (param is object[])
            {
                var multipleParameter = (object[])param;
                NewStatusView newStatusView = (NewStatusView)multipleParameter[0];
                RichTextFeedEdit RtbNewStatusArea = (RichTextFeedEdit)multipleParameter[1];
                richTextFeedEditFromViewModel = RtbNewStatusArea;
                SetFeedWallType(newStatusView);
                if (PrivacyValue != 5 && PrivacyValue != 10)
                {
                    FriendsToHideOrShow.Clear();
                }

                if (DefaultSettings.IsInternetAvailable)
                {
                    if (MainSwitcher.PopupController.EditStatusCommentView != null
                    && MainSwitcher.PopupController.EditStatusCommentView.IsVisible)
                    {
                        MainSwitcher.PopupController.EditStatusCommentView.ActionEditFeed();
                        return;
                    }
                    if (ucMediaCloudUploadPopup != null)
                    {
                        ucMediaCloudUploadPopup.LoadStatesInUI(2);
                    }
                    StatusText = RtbNewStatusArea.Text.Trim();
                    if (string.IsNullOrWhiteSpace(StatusText))
                    {
                        StatusText = "";
                    }
                    RtbNewStatusArea.SetStatusandTags();
                    if (RtbNewStatusArea.TagsJArray != null)
                    {
                        StatusTagsJArray = RtbNewStatusArea.TagsJArray;
                        StatusTextWithoutTags = RtbNewStatusArea.StringWithoutTags;
                    }
                    else
                    {
                        StatusTagsJArray = null;
                        StatusTextWithoutTags = StatusText;
                    }

                    if (FriendsToHideOrShow.Count > 0)
                    {
                        foreach (var friends in FriendsToHideOrShow)
                        {
                            FriendsToShowOrHideUtIds.Add(friends.ShortInfoModel.UserTableID);
                        }
                    }

                    if (NewStatusImageUpload.Count > 0)
                    {
                        bool tooLargeCaption = false;
                        foreach (ImageUploaderModel model in NewStatusImageUpload)
                        {
                            if (model.ImageCaption.Length > 250)
                            {
                                tooLargeCaption = true;
                                break;
                            }
                        }
                        if (tooLargeCaption)
                        {
                            //CustomMessageBox.ShowInformation("An Image Caption can be maximum 250 Characters!");
                            UIHelperMethods.ShowInformation("An Image Caption can be maximum 250 Characters!", "Max characters!");
                        }
                        else
                        {
                            new ImageUploader(this);
                        }

                    }
                    else if (NewStatusMusicUpload.Count > 0)
                    {
                        bool isAllAudioHaveTitle = true;
                        foreach (MusicUploaderModel model in NewStatusMusicUpload)
                        {
                            if (string.IsNullOrEmpty(model.AudioTitle))
                            {
                                isAllAudioHaveTitle = false;
                                break;
                            }
                        }
                        if (!isAllAudioHaveTitle)
                        {
                            UIHelperMethods.ShowInformation("Please choose a name for every music!", "Need name!");
                            if (ucMediaCloudUploadPopup != null) ucMediaCloudUploadPopup.LoadStatesInUI(1);
                        }
                        else if (!isSelectedMediaFilesLessThan500MB(SettingsConstants.MEDIA_TYPE_AUDIO))
                        {
                            UIHelperMethods.ShowInformation("Maximum limit for uploading is 500MB!", "Max limit!");
                            if (ucMediaCloudUploadPopup != null) ucMediaCloudUploadPopup.LoadStatesInUI(1);
                        }
                        else
                        {
                            new MusicUploader(this);
                        }
                    }
                    else if (NewStatusVideoUpload.Count > 0)
                    {
                        bool isAllVideoHaveTitle = true;
                        foreach (FeedVideoUploaderModel model in NewStatusVideoUpload)
                        {
                            if (string.IsNullOrEmpty(model.VideoTitle))
                            {
                                isAllVideoHaveTitle = false;
                                break;
                            }
                        }
                        if (!isAllVideoHaveTitle)
                        {
                            UIHelperMethods.ShowInformation("Please choose a name for every video!", "Need name!");
                            //  CustomMessageBox.ShowInformation("Please Choose a name for Every Video!");
                            if (ucMediaCloudUploadPopup != null) ucMediaCloudUploadPopup.LoadStatesInUI(1);
                        }
                        else if (!isSelectedMediaFilesLessThan500MB(SettingsConstants.MEDIA_TYPE_VIDEO))
                        {
                            UIHelperMethods.ShowInformation("Maximum limit for uploading is 500MB!", "Max limit!");
                            //  CustomMessageBox.ShowInformation("Maximum Limit for uploading is 500MB!");
                            if (ucMediaCloudUploadPopup != null) ucMediaCloudUploadPopup.LoadStatesInUI(1);
                        }
                        else
                        {
                            new VideoUploader(this);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(StatusText) && SelectedLocationModel == null && (TaggedFriends == null || TaggedFriends.Count < 1) && (SelectedDoingModel == null && SelectedDoingId == 0))
                        {
                            UIHelperMethods.ShowInformation(NotificationMessages.NOTHING_TO_POST, "Empty post!");
                        }
                        else
                        {
                            if ((InputUrl.Length == StatusText.Length) && InputUrl.Equals(StatusText))
                            {
                                StatusTextWithoutTags = "";
                            }
                            EnablePostbutton(false);
                            new ThreadAddFeed().StartThread(this);
                        }
                    }
                }
                else
                {
                    UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Post failed!");
                }
            }

        }

        private void onUploadPhotoButtonClick(object param)
        {
            if (param is NewStatusView)
            {
                NewStatusView newStatusView = (NewStatusView)param;
                SetFeedWallType(newStatusView);
            }
            if (LocationMainBorderPanelVisibility == Visibility.Visible)
            {
                LocationMainBorderPanelVisibility = Visibility.Collapsed;
            }
            if (FeelingDoingButtonContainerVisibility == Visibility.Visible)
            {
                FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
            }
            IsImageAlbumTitleTextBoxEnabled = true;
            AddToImageAlbumButtonVisibility = Visibility.Visible;
            SelectedImageAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsPrivacyButtonEnabled = true;
            IsPopupUploadPhotoOpen = true;
        }

        private void onNewStatusVideoButtonClick(object param)
        {
            LocationMainBorderPanelVisibility = Visibility.Collapsed;
            TagListHolderVisibility = Visibility.Collapsed;
            FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
            IsVideoAlbumTitleTextBoxEnabled = true;
            AddtoVideoAlbumButtonVisibility = Visibility.Visible;
            SelectedVideoAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsPopupUploadVideoOpen = true;
        }

        private void onTakePhotoClick(object param)
        {
            IsPopupUploadPhotoOpen = false;
            View.UI.WNWebcamCapture.Instance.ShowWindow((f) =>
            {
                try
                {
                    ImageUploaderModel model = new ImageUploaderModel();
                    model.FilePath = f;
                    NewStatusImageUpload.Add(model);
                    AudioAlbumTitleTextBoxText = "";
                    VideoAlbumTitleTextBoxText = "";
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        NewStatusMusicUpload.Clear();
                    }
                    if (NewStatusVideoUpload.Count > 0)
                    {
                        NewStatusVideoUpload.Clear();
                    }
                    if (NewStatusHashTag.Count > 0)
                    {
                        ClearNewStatusHashTagsExceptPlaceHolder();
                    }
                    if (NewStatusImageUpload.Count > 1 || ImageAlbumTitleTextBoxText.Trim().Length > 0)
                    {
                        if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                        {
                            PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                        }
                    }
                    UploadingText = "Selected Image(s): " + NewStatusImageUpload.Count;
                    this.StatusImagePanelVisibility = Visibility.Visible;
                    AudioPanelVisibility = Visibility.Collapsed;
                    VideoPanelVisibility = Visibility.Collapsed;
                    PreviewCancelButtonVisibility = Visibility.Visible;
                    ActionRemoveLinkPreview();
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            });
        }

        private void onUploadFromComputerClick(object param)
        {
            IsPopupUploadPhotoOpen = false;
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            op.Multiselect = true;
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if ((bool)op.ShowDialog())
            {
                foreach (string filename in op.FileNames)
                {
                    try
                    {
                        MediaFile mf = new MediaFile(filename);
                        if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit500MBinBytes)
                        { 
                            anyMorethan500MB = true; 
                            continue; 
                        }
                        int formatChecked = ImageUtility.GetFileImageTypeFromHeader(filename);
                        if (formatChecked != 1 && formatChecked != 2)
                        { 
                            anyCorrupted = true; continue; 
                        }
                        ImageUploaderModel model = new ImageUploaderModel();
                        model.FileSize = mf.size;
                        model.FilePath = filename;
                        NewStatusImageUpload.Add(model);
                        UploadingText = "Selected Image(s): " + NewStatusImageUpload.Count;
                    }
                    catch (Exception) { anyCorrupted = true; }
                }
                if (NewStatusImageUpload.Count > 0)
                {
                    AudioAlbumTitleTextBoxText = "";
                    VideoAlbumTitleTextBoxText = "";
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        NewStatusMusicUpload.Clear();
                    }
                    if (NewStatusVideoUpload.Count > 0)
                    {
                        NewStatusVideoUpload.Clear();
                    }
                    if (NewStatusHashTag.Count > 0)
                    {
                        ClearNewStatusHashTagsExceptPlaceHolder();
                    }
                    if (NewStatusImageUpload.Count > 1 || ImageAlbumTitleTextBoxText.Trim().Length > 0)
                    {
                        if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                        {
                            PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                        }
                    }
                    AudioPanelVisibility = Visibility.Collapsed;
                    VideoPanelVisibility = Visibility.Collapsed;
                    ActionRemoveLinkPreview();
                    StatusImagePanelVisibility = Visibility.Visible;
                    PreviewCancelButtonVisibility = Visibility.Visible;
                }
            }
            if (anyCorrupted)
            {
                UIHelperMethods.ShowWarning("One or more files are corrupted !");
            }
            if (anyMorethan500MB)
            {
                UIHelperMethods.ShowFailed("One or more files are greater than Maximum file Limit (500MB) !");
            }
        }

        private void onChooseFromAlbumClick(object param)
        {
            IsPopupUploadPhotoOpen = false;
            SetStatusImagePanelVisibility();
            MainSwitcher.PopupController.PhotoSelectionView.Show();
            MainSwitcher.PopupController.PhotoSelectionView.SetPhotoType(MiddlePanelConstants.TypeMyAlbum, this);
        }

        private void onNewStatusEmoticonClick(object param)
        {
            if (param is Button)
            {
                Button newStatusEmoticonButton = (Button)param;
                if (LocationMainBorderPanelVisibility == Visibility.Visible)
                {
                    LocationMainBorderPanelVisibility = Visibility.Collapsed;
                }
                if (TagListHolderVisibility == Visibility.Visible)
                {
                    TagListHolderVisibility = Visibility.Collapsed;
                }
                if (FeelingDoingButtonContainerVisibility == Visibility.Visible)
                {
                    FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
                }
                else
                {
                    if (SelectedDoingModel != null)
                    {
                        FeelingDoingButtonContainerVisibility = Visibility.Visible;
                    }
                }
                EmoPopup = true;
                if (ucNewStatusFeelingPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNewStatusFeelingPopup);
                }
                if (ucNewStatusFeelingPopup == null)
                {
                    ucNewStatusFeelingPopup = new UCNewStatusFeelingPopup(UCGuiRingID.Instance.MotherPanel);
                }
                ucNewStatusFeelingPopup.Show();
                ucNewStatusFeelingPopup.ShowFeelingPopup(newStatusEmoticonButton, this);
                ucNewStatusFeelingPopup.OnRemovedUserControl += () =>
                {
                    EmoPopup = false;
                };
                RingIDViewModel.Instance.LoadDoingModels();
            }
        }

        private void onNewsStatusTagFriendClick(object param)
        {
            if (param is Button)
            {
                Button NewStatusTagFriendButton = (Button)param;

                if (LocationMainBorderPanelVisibility == Visibility.Visible)
                {
                    LocationMainBorderPanelVisibility = Visibility.Collapsed;
                }
                if (FeelingDoingButtonContainerVisibility == Visibility.Visible)
                {
                    FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
                }
                if (TaggedFriends != null && TaggedFriends.Count > 0 && TagListHolderVisibility == Visibility.Collapsed)
                {
                    TagListHolderVisibility = Visibility.Visible;
                }
                else
                {
                    TagListHolderVisibility = Visibility.Collapsed;
                }
                TagPopup = true;
                if (ucNewStatusTagPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNewStatusTagPopup);
                }
                ucNewStatusTagPopup = new UCNewStatusTagPopup(UCGuiRingID.Instance.MotherPanel);
                ucNewStatusTagPopup.Show();
                ucNewStatusTagPopup.ShowTagPopup(NewStatusTagFriendButton, this);
                ucNewStatusTagPopup.OnRemovedUserControl += () =>
                {
                    ucNewStatusTagPopup = null;
                    TagPopup = false;
                };
            }
        }

        private void onRemoveFriendTagClick(object param)
        {
            if (param is Button)
            {
                Button button = (Button)param;
                UserShortInfoModel model = (UserShortInfoModel)button.DataContext;
                model.IsVisibleInTagList = false;
                TaggedFriends.Remove(model);
                OnPropertyChanged("TaggedFriends");
                if (TaggedUtids != null && TaggedUtids.Count > 0)
                {
                    TaggedUtids.Remove(model.UserTableID);
                }
                if (TaggedFriends.Count == 0 && TagListHolderVisibility == Visibility.Visible)
                {
                    TagListHolderVisibility = Visibility.Collapsed;
                }
            }
        }

        private void onNewStatusLocationButtonClick(object param)
        {
            if (param is TextBox)
            {
                locationTextBox = (TextBox)param;
                if (LocationMainBorderPanelVisibility == Visibility.Visible)
                {
                    LocationMainBorderPanelVisibility = Visibility.Collapsed;
                    locationTextBox.Text = String.Empty;
                }
                else
                {
                    LocationMainBorderPanelVisibility = Visibility.Visible;
                    if (SelectedLocationModel == null)
                    {
                        locationTextBox.Text = String.Empty;
                    }
                    else
                    {
                        locationTextBox.Text = SelectedLocationModel.LocationName;
                        IsLocationSet = true;
                    }
                    locationTextBox.Focus();
                    locationTextBox.SelectionStart = locationTextBox.Text.Length;
                }

                if (FeelingDoingButtonContainerVisibility == Visibility.Visible)
                {
                    FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
                }
                if (TagListHolderVisibility == Visibility.Visible)
                {
                    TagListHolderVisibility = Visibility.Collapsed;
                }
            }
        }

        private void onLocationTextBoxLostFocus(object param)
        {
            if (SelectedLocationModel != null)
            {
                IsLocationSet = true;
                LocationTextBoxText = SelectedLocationModel.LocationName;
            }
            if (LocationPopUp != null)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
            }
            if (timer != null && timer.IsEnabled)
            {
                timer.Stop();
            }
        }

        private void onLocationTextBoxTextChanged(object param)
        {
            try
            {
                locationTextBox = (TextBox)param;
                if (IsLocationSet && (!string.Equals(locationTextBox.Text, LocationTextBoxText)))
                {
                    IsLocationSet = false;
                }
                if (locationTextBox.Text.Trim().Length > 0)
                {
                    if (timer == null)
                    {
                        askSuggestions();
                        timer = new DispatcherTimer(DispatcherPriority.Background);
                        timer.Interval = TimeSpan.FromSeconds(1); //100ms
                        timer.Tick += lookUp;
                        timer.Start();
                    }
                    else if (!timer.IsEnabled)
                    {
                        timer.Start();
                    }
                }
                else if (locationTextBox.Text.Trim().Length == 0)
                {
                    if (LocationPopUp != null)
                    {
                        LocationPopUp.SelectedLocationModel = null;
                        LocationPopUp.popupLocation.IsOpen = false;
                    }
                    timer.Stop();
                }
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void onLocationTextBoxPreviewKeyDown(object param)
        {
            if (LocationPopUp != null && LocationPopUp.popupLocation.IsOpen == true)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
                LocationPopUp.popupLocation.IsOpen = false;
            }
        }

        private void onTextLocationCrossButtonClick(object param)
        {
            SelectedLocationModel = null;
            locationTextBox.Text = "";
            IsLocationSet = false;
            locationTextBox.Focus();
            locationTextBox.SelectionStart = locationTextBox.Text.Length;
        }

        private void onPrivacyButtonClick(object param)
        {
            if (param is Button)
            {
                Button privacyButton = (Button)param;
                PrivacyPopup = true;
                if (ucNewStatusPrivacyPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNewStatusPrivacyPopup);
                }
                ucNewStatusPrivacyPopup = new UCNewStatusPrivacyPopup(UCGuiRingID.Instance.MotherPanel);
                ucNewStatusPrivacyPopup.Show();
                ucNewStatusPrivacyPopup.ShowPopUp(privacyButton, this);
                ucNewStatusPrivacyPopup.OnRemovedUserControl += () =>
                {
                    ucNewStatusPrivacyPopup = null;
                    PrivacyPopup = false;
                };
            }
        }

        private void onPreviewCancelButtonClick(object param)
        {
            if (MainSwitcher.PopupController.EditStatusCommentView != null
               && MainSwitcher.PopupController.EditStatusCommentView.IsVisible) MainSwitcher.PopupController.EditStatusCommentView.Hide();
            else
            {
                PreviewCancelButtonVisibility = Visibility.Collapsed;
                StatusImagePanelVisibility = Visibility.Collapsed;
                AudioPanelVisibility = Visibility.Collapsed;
                VideoPanelVisibility = Visibility.Collapsed;
                IsPrivacyButtonEnabled = true;
                AudioAlbumTitleTextBoxText = "";
                VideoAlbumTitleTextBoxText = "";
                ImageAlbumTitleTextBoxText = "";
                if (NewStatusImageUpload.Count > 0)
                {
                    NewStatusImageUpload.Clear();
                }
                if (NewStatusMusicUpload.Count > 0)
                {
                    NewStatusMusicUpload.Clear();
                }
                if (NewStatusVideoUpload.Count > 0)
                {
                    NewStatusVideoUpload.Clear();
                    if (UCVideoRecorderPreviewPanel.Instance != null)
                    {
                        UCVideoRecorderPreviewPanel.Instance.DeleteFilesInDirectory();
                        UCVideoRecorderPreviewPanel.Instance.CloseAll();
                        UCVideoRecorderPreviewPanel.Instance.Hide();
                    }
                }
                if (NewStatusHashTag.Count > 0)
                {
                    ClearNewStatusHashTagsExceptPlaceHolder();
                }
                HelperMethods.ClearBitMapImagesDictionary();
                ActionRemoveLinkPreview();
                if (ucMediaCloudUploadPopup != null)
                {
                    ucMediaCloudUploadPopup.LoadStatesInUI(0);
                }
            }
        }

        private void onRtbNewStatusAreaTextChanged(object param)
        {
            if (param is RichTextFeedEdit)
            {
                RichTextFeedEdit richTextFeedEditNewStatus = (RichTextFeedEdit)param;
                string statusText = richTextFeedEditNewStatus.Text;
                if (statusText.Length > 0)
                {

                    WriteSomethingTextBlockVisibility = Visibility.Collapsed;
                    if (statusText.EndsWith("@"))
                    {
                        richTextFeedEditNewStatus.AlphaStarts = statusText.Length;
                        if (UCAlphaTagPopUp.Instance == null)
                        {
                            UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
                        }
                        if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
                        {
                            Grid g = (Grid)UCAlphaTagPopUp.Instance.Parent;
                            g.Children.Remove(UCAlphaTagPopUp.Instance);
                        }
                        Grid richGrid = (Grid)richTextFeedEditNewStatus.Parent;
                        richGrid.Children.Add(UCAlphaTagPopUp.Instance);
                        UCAlphaTagPopUp.Instance.InitializePopUpLocation(richTextFeedEditNewStatus);
                    }
                    else if (richTextFeedEditNewStatus.AlphaStarts >= 0 && richTextFeedEditNewStatus.Text.Length > richTextFeedEditNewStatus.AlphaStarts)
                    {
                        string searchString = richTextFeedEditNewStatus.Text.Substring(richTextFeedEditNewStatus.AlphaStarts);
                        UCAlphaTagPopUp.Instance.ViewSearchFriends(searchString);
                    }
                }
                else
                {
                    WriteSomethingTextBlockVisibility = Visibility.Visible;
                    richTextFeedEditNewStatus.AlphaStarts = -1;
                    richTextFeedEditNewStatus._AddedFriendsUtid.Clear();
                }
            }
        }

        private void onRtbNewStatusAreaPreviewKeyDown(object param)
        {
            if (param is KeyEventArgs)
            {
                KeyEventArgs pressedKeyEvent = (KeyEventArgs)param;
                RichTextFeedEdit richTextFeedEdit = (RichTextFeedEdit)pressedKeyEvent.KeyboardDevice.FocusedElement;
                if (pressedKeyEvent.Key == Key.Enter && (UCAlphaTagPopUp.Instance == null || (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen == false)))
                {
                    richTextFeedEdit.AppendText("\n");
                    pressedKeyEvent.Handled = true;
                }

                if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
                {
                    if (pressedKeyEvent.Key == Key.Down)
                    {
                        pressedKeyEvent.Handled = true;
                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Up)
                    {
                        pressedKeyEvent.Handled = true;

                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Enter || pressedKeyEvent.Key == Key.Tab)
                    {
                        pressedKeyEvent.Handled = true;
                        UCAlphaTagPopUp.Instance.SelectFriendTag((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
                    }
                }

            }
        }

        private void onRtbNewStatusAreaPreviewMouseLeftButtonDown(object param)
        {
            if (LocationMainBorderPanelVisibility == Visibility.Visible)
            {
                LocationMainBorderPanelVisibility = Visibility.Collapsed;
            }
            if (TagListHolderVisibility == Visibility.Visible)
            {
                TagListHolderVisibility = Visibility.Collapsed;
            }
        }

        private void onNewStatusMusicClick(object param)
        {
            IsAudioAlbumTitleTextBoxEnabled = true;
            AddtoAudioAlbumButtonVisibility = Visibility.Visible;
            SelectedAudioAlbumCrossButtonVisibility = Visibility.Collapsed;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Music File";
            openFileDialog.Filter = "All supported Music files|*.mp3|MP3 files(*.mp3)|*.mp3";
            openFileDialog.Multiselect = true;
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length <= 5 && (openFileDialog.FileNames.Length + NewStatusMusicUpload.Count) <= 5) // When 5 audio select with cntrl--> openFileDialog.FileNames.Length <= 5
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);
                            if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit500MBinBytes)
                            {
                                anyMorethan500MB = true;
                                continue;
                            }
                            if (mf.Audio == null || mf.Audio.Count == 0 || mf.Video.Count > 0 || !mf.format.Equals("MPEG Audio"))
                            {
                                anyCorrupted = true;
                                continue;
                            }
                            MusicUploaderModel model = new MusicUploaderModel();
                            model.FileSize = mf.size;
                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                                if (file.Tag != null && file.Tag.Pictures.Any())
                                {
                                    if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null)
                                    {
                                        model.IsImageFound = true;
                                    }
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                    {
                                        model.AudioArtist = file.Tag.Performers[0];
                                    }
                                    else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                    {
                                        model.AudioArtist = file.Tag.AlbumArtists[0];
                                    }
                                    model.AudioTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception) { }
                            if (string.IsNullOrEmpty(model.AudioTitle))
                            {
                                model.AudioTitle = Path.GetFileNameWithoutExtension(filename);
                            }
                            model.AudioDuration = (long)(mf.duration / 1000);
                            if (model.AudioDuration == 0)
                                model.AudioDuration = HelperMethods.MediaPlayerDuration(filename);
                            model.FilePath = filename;
                            NewStatusMusicUpload.Add(model);
                            UploadingText = "";
                        }
                        catch (Exception)
                        {
                            anyCorrupted = true;
                        }
                    }
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        VideoAlbumTitleTextBoxText = "";
                        ImageAlbumTitleTextBoxText = "";
                        if (NewStatusImageUpload.Count > 0)
                        {
                            NewStatusImageUpload.Clear();
                        }
                        if (NewStatusVideoUpload.Count > 0)
                        {
                            NewStatusVideoUpload.Clear();
                        }
                        if (NewStatusHashTag.Count > 0)
                        {
                            ClearNewStatusHashTagsExceptPlaceHolder();
                        }
                        if (NewStatusMusicUpload.Count > 1 || AudioAlbumTitleTextBoxText.Trim().Length > 0)
                        {
                            if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                            {
                                PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                            }
                        }
                        StatusImagePanelVisibility = Visibility.Collapsed;
                        VideoPanelVisibility = Visibility.Collapsed;
                        ActionRemoveLinkPreview();
                        AudioPanelVisibility = Visibility.Visible;
                        PreviewCancelButtonVisibility = Visibility.Visible;
                    }
                }
                else
                {
                    UIHelperMethods.ShowFailed("Can't upload more than 5 music at a time !");
                }
            }
            if (anyCorrupted)
            {
                UIHelperMethods.ShowWarning("One or more files are corrupted or invalid format!");
            }
            if (anyMorethan500MB)
            {
                UIHelperMethods.ShowFailed("One or more files are greater than Maximum file Limit (250MB) !", "Max media size");
            }
        }
        private void showMediaAlbumPopup(UIElement txtBox, int type)
        {
            if (UCMediaContentAlbumPopUp.Instance == null)
            {
                UCMediaContentAlbumPopUp.Instance = new UCMediaContentAlbumPopUp(UCGuiRingID.Instance.MotherPanel);
            }
            UCMediaContentAlbumPopUp.Instance.Show();
            UCMediaContentAlbumPopUp.Instance.ShowMediaPopup(txtBox, type, this);
            UCMediaContentAlbumPopUp.Instance.OnRemovedUserControl += () =>
            {
                UCMediaContentAlbumPopUp.Instance = null;
            };
        }
        private void onAddtoAudioAlbumButtonClick(object param)
        {
            if (param is TextBox)
            {
                TextBox audioAlbumTitleTextBox = (TextBox)param;

                if (DefaultSettings.MY_AUDIO_ALBUMS_COUNT == 0)
                {
                    UIHelperMethods.ShowFailed("You have No existing Audio Albums!", "No Albums!");
                }
                else if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                {
                    showMediaAlbumPopup(audioAlbumTitleTextBox, SettingsConstants.MEDIA_TYPE_AUDIO);
                }
                else if (DefaultSettings.IsInternetAvailable)
                {
                    showMediaAlbumPopup(audioAlbumTitleTextBox, SettingsConstants.MEDIA_TYPE_AUDIO);
                    SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_AUDIO, Guid.Empty);
                }
            }
        }

        private void onBorderMouseEnter(object param)
        {
            if (param is object[])
            {
                var wrapPanelsCommandParameters = (object[])param;
                WrapPanel audioAddHashTagWrapPanel = (WrapPanel)wrapPanelsCommandParameters[0];
                WrapPanel videoAddHashTagWrapPanel = (WrapPanel)wrapPanelsCommandParameters[1];
                if (AudioPanelVisibility == Visibility.Visible)
                {
                    audioAddHashTagWrapPanel.Cursor = Cursors.Arrow;
                }
                else if (VideoPanelVisibility == Visibility.Visible)
                {
                    videoAddHashTagWrapPanel.Cursor = Cursors.Arrow;
                }
            }
        }

        private void onBorderMouseLeave(object param)
        {
            if (param is object[])
            {
                var wrapPanelsCommandParameters = (object[])param;
                WrapPanel audioAddHashTagWrapPanel = (WrapPanel)wrapPanelsCommandParameters[0];
                WrapPanel videoAddHashTagWrapPanel = (WrapPanel)wrapPanelsCommandParameters[1];
                if (AudioPanelVisibility == Visibility.Visible)
                {
                    audioAddHashTagWrapPanel.Cursor = Cursors.IBeam;
                }
                else if (VideoPanelVisibility == Visibility.Visible)
                {
                    videoAddHashTagWrapPanel.Cursor = Cursors.IBeam;
                }
            }
        }

        private void onRemoveHashtagButtonClick(object param)
        {
            if (param is Control)
            {
                Control control = (Control)param;
                HashTagModel model = (HashTagModel)control.DataContext;
                NewStatusHashTag.Remove(model);
            }
        }

        private void onHashTagWrapPanelMouseLeftButtonUp(object param)
        {
            if (param is WrapPanel)
            {
                HashTagBoxEnabled = true;
                TextBox wrapPanelChildTextBox = HelperMethods.FindVisualChild<TextBox>((WrapPanel)param);

                if (wrapPanelChildTextBox != null)
                {
                    wrapPanelChildTextBox.Focusable = true;
                    wrapPanelChildTextBox.Focus();
                }
            }
        }

        private void onAudioAddHashTagTextBoxTextChanged(object param)
        {
            if (param is TextBox)
            {
                TextBox AudioAddHashTagTextBox = (TextBox)param;
                if (AudioAddHashTagTextBox.Text.Trim().Length > 0)
                {
                    if (NewStatusHashTag.Count > 5)
                    {
                        AudioAddHashTagTextBox.Text = string.Empty;
                    }
                    else if (AudioAddHashTagTextBox.Parent != null && AudioAddHashTagTextBox.Parent is Grid)
                    {
                        Grid AudioAddHashTagTextBoxParentGrid = (Grid)AudioAddHashTagTextBox.Parent;
                        HashTagPopUp.Show(this, AudioAddHashTagTextBox, AudioAddHashTagTextBoxParentGrid);
                    }
                }
                else
                {
                    HashTagPopUp.CloseHashTagPopUp();
                }
            }
        }

        private void onAudioAddHashTagTextBoxPreviewKeyDown(object param)
        {
            if (param is KeyEventArgs)
            {
                KeyEventArgs pressedKeyEvent = (KeyEventArgs)param;
                TextBox AudioAddHashTagTextBox = (TextBox)pressedKeyEvent.KeyboardDevice.FocusedElement;
                if (HashTagPopUp != null && HashTagPopUp.popupHashTag.IsOpen && HashTagPopUp.TagList.Items.Count > 0)
                {
                    if (pressedKeyEvent.Key == Key.Down)
                    {
                        pressedKeyEvent.Handled = true;
                        if (HashTagPopUp.TagList.SelectedIndex < HashTagPopUp.TagList.Items.Count - 1)
                        {
                            HashTagPopUp.TagList.SelectedIndex++;
                        }
                    }
                    else if (pressedKeyEvent.Key == Key.Up)
                    {
                        pressedKeyEvent.Handled = true;

                        if (HashTagPopUp.TagList.SelectedIndex > 0)
                        {
                            HashTagPopUp.TagList.SelectedIndex--;
                        }
                    }
                    else if (pressedKeyEvent.Key == Key.Enter || pressedKeyEvent.Key == Key.Tab)
                    {
                        pressedKeyEvent.Handled = true;

                        if (NewStatusHashTag.Count - 2 >= 0)
                        {
                            NewStatusHashTag.Insert(NewStatusHashTag.Count - 1, (HashTagModel)HashTagPopUp.TagList.SelectedItem);
                        }
                        else
                        {
                            NewStatusHashTag.Insert(0, (HashTagModel)HashTagPopUp.TagList.SelectedItem);
                        }
                        AudioAddHashTagTextBox.Text = string.Empty;
                        AudioAddHashTagTextBox.Focus();
                        HashTagPopUp.CloseHashTagPopUp();
                    }
                }
                if (pressedKeyEvent.Key == Key.Space)
                {
                    pressedKeyEvent.Handled = true;
                    string newTag = AudioAddHashTagTextBox.Text.Trim();
                    if (newTag.Length > 0 && (NewStatusHashTag.Count == 0 || !NewStatusHashTag.Any(P => P.HashTagSearchKey.Equals(newTag, StringComparison.InvariantCultureIgnoreCase))))
                    {
                        HashTagModel model = null;
                        if (HashTagPopUp.SearchHashTag.Count > 0)
                        {
                            model = HashTagPopUp.SearchHashTag.Where(P => P.HashTagSearchKey.Equals(newTag, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                            if (model != null)
                            {
                                model.HashTagSearchKey = newTag;
                            }
                        }
                        if (model == null && MediaDictionaries.Instance.HashTagSuggestions.Count > 0)
                        {
                            HashTagDTO dto = MediaDictionaries.Instance.HashTagSuggestions.Where(P => P.HashTagSearchKey.Equals(newTag, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                            if (dto != null)
                            {
                                model = new HashTagModel();
                                model.LoadData(dto);
                                model.HashTagSearchKey = newTag;
                            }
                        }
                        if (model == null)
                        {
                            HashTagDTO dto = new HashTagDTO { HashTagSearchKey = newTag, HashTagID = 0 };
                            if (!MediaDictionaries.Instance.HashTagSuggestions.Any(p => p.HashTagSearchKey.Equals(dto.HashTagSearchKey, StringComparison.InvariantCultureIgnoreCase)))
                            {
                                MediaDictionaries.Instance.HashTagSuggestions.Add(dto);
                            }
                            model = new HashTagModel { HashTagSearchKey = newTag, HashTagId = 0 };
                        }
                        if (NewStatusHashTag.Count - 2 >= 0)
                        {
                            NewStatusHashTag.Insert(NewStatusHashTag.Count - 1, model);
                        }
                        else
                        {
                            NewStatusHashTag.Insert(0, model);
                        }
                        AudioAddHashTagTextBox.Text = string.Empty;
                        AudioAddHashTagTextBox.Focus();
                        HashTagPopUp.CloseHashTagPopUp();
                    }
                }
                if (pressedKeyEvent.Key == Key.Back && AudioAddHashTagTextBox.Text.Length == 0)
                {
                    if (NewStatusHashTag.Count - 2 >= 0)
                    {
                        NewStatusHashTag.RemoveAt(NewStatusHashTag.Count - 2);
                    }
                }
            }
        }

        private void onAddtoVideoAlbumButtonClick(object param)
        {
            if (param is TextBox)
            {
                TextBox videoAlbumTitleTextBox = (TextBox)param;
                if (DefaultSettings.MY_VIDEO_ALBUMS_COUNT == 0)
                {
                    UIHelperMethods.ShowFailed("You have No existing video albums!", "No albums!");
                }
                else if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                {
                    showMediaAlbumPopup(videoAlbumTitleTextBox, SettingsConstants.MEDIA_TYPE_VIDEO);
                }
                else if (DefaultSettings.IsInternetAvailable)
                {
                    showMediaAlbumPopup(videoAlbumTitleTextBox, SettingsConstants.MEDIA_TYPE_VIDEO);
                    SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_VIDEO, Guid.Empty);
                }
            }
        }

        private void onRecordVideoClick(object param)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (UCVideoRecorderPreviewPanel.Instance != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(UCVideoRecorderPreviewPanel.Instance);
                }
                UCVideoRecorderPreviewPanel.Instance = new UCVideoRecorderPreviewPanel(UCGuiRingID.Instance.MotherPanel);
                IsPopupUploadVideoOpen = false;
                UCVideoRecorderPreviewPanel.Instance.Show();
                UCVideoRecorderPreviewPanel.Instance.ShowRecorder();
                UCVideoRecorderPreviewPanel.Instance.OnRemovedUserControl += () =>
                {
                    UCVideoRecorderPreviewPanel.Instance = null;
                };
                UCVideoRecorderPreviewPanel.Instance.OnVideoAddedToUploadList += (fileName, duration) =>
                {
                    FeedVideoUploaderModel model = new FeedVideoUploaderModel 
                    { 
                        IsRecorded = true, 
                        FilePath = fileName, 
                        VideoDuration = duration, 
                        VideoTitle = Path.GetFileNameWithoutExtension(fileName) 
                    };
                    new SetArtImageFromVideo(model);
                    NewStatusVideoUpload.Add(model);
                    AudioAlbumTitleTextBoxText = "";
                    ImageAlbumTitleTextBoxText = "";
                    if (NewStatusImageUpload.Count > 0)
                    {
                        NewStatusImageUpload.Clear();
                    }
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        NewStatusMusicUpload.Clear();
                    }
                    if (NewStatusHashTag.Count > 0)
                    {
                        ClearNewStatusHashTagsExceptPlaceHolder();
                    }
                    if (NewStatusVideoUpload.Count > 1 || VideoAlbumTitleTextBoxText.Trim().Length > 0)
                    {
                        if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                        {
                            PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                        }
                    }
                    AudioPanelVisibility = Visibility.Collapsed;
                    StatusImagePanelVisibility = Visibility.Collapsed;
                    ActionRemoveLinkPreview();
                    VideoPanelVisibility = Visibility.Visible;
                    PreviewCancelButtonVisibility = Visibility.Visible;
                };
            });
        }

        private void onUploadVideoFromComputerClick(object param)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "All supported Video files|*.mp4|MP4 files(*.mp4)|*.mp4";
            bool anyCorrupted = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length <= 5 && (openFileDialog.FileNames.Length + NewStatusVideoUpload.Count) <= 5) // When 5 video select with cntrl--> openFileDialog.FileNames.Length <= 5
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);

                            if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264"))
                            {
                                anyCorrupted = true;
                                continue;
                            }

                            FeedVideoUploaderModel model = new FeedVideoUploaderModel();
                            model.FileSize = mf.size;
                            model.FilePath = filename;
                            NewStatusVideoUpload.Add(model);
                            TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                            if (file.Tag != null && file.Tag.Pictures.Any())
                            {
                                if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                {
                                    model.VideoArtist = file.Tag.Performers[0];
                                }
                                else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                {
                                    model.VideoArtist = file.Tag.AlbumArtists[0];
                                }
                                model.VideoTitle = file.Tag.Title;
                            }
                            UploadingText = "";
                            model.VideoTitle = Path.GetFileNameWithoutExtension(filename);
                            model.VideoDuration = (long)(mf.duration / 1000);
                            if (model.VideoDuration == 0)
                            {
                                model.VideoDuration = HelperMethods.MediaPlayerDuration(filename);
                            }
                            new SetArtImageFromVideo(model);//120*90

                        }
                        catch (Exception)
                        {
                            anyCorrupted = true;
                        }
                    }
                    if (NewStatusVideoUpload.Count > 0)
                    {
                        AudioAlbumTitleTextBoxText = "";
                        ImageAlbumTitleTextBoxText = "";
                        if (NewStatusImageUpload.Count > 0)
                        {
                            NewStatusImageUpload.Clear();
                        }
                        if (NewStatusMusicUpload.Count > 0)
                        {
                            NewStatusMusicUpload.Clear();
                        }
                        if (NewStatusHashTag.Count > 0)
                        {
                            ClearNewStatusHashTagsExceptPlaceHolder();
                        }
                        if (NewStatusVideoUpload.Count > 1 || VideoAlbumTitleTextBoxText.Trim().Length > 0)
                        {
                            if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                            {
                                PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                            }
                        }
                        StatusImagePanelVisibility = Visibility.Collapsed;
                        AudioPanelVisibility = Visibility.Collapsed;
                        ActionRemoveLinkPreview();
                        VideoPanelVisibility = Visibility.Visible;
                        PreviewCancelButtonVisibility = Visibility.Visible;
                    }
                }
                else
                {
                    UIHelperMethods.ShowFailed("Can't upload more than 5 videos at a time !");
                }
            }
            if (anyCorrupted)
            {
                UIHelperMethods.ShowWarning("One or more files are corrupted or Invalid format!");
            }
        }

        private void onLinkImageThumbnailCheckBoxChecked(object param)
        {
            PreviewImgUrl = "";
        }

        private void onLinkPreviewImagePreviousButtonClick(object param)
        {
            SelectedImageIndex--;
            if (SelectedImageIndex > -1)
            {
                PreviewImgUrl = AllImagesURLS.ElementAt(SelectedImageIndex);
            }
            if (SelectedImageIndex - 1 < 0)
            {
                IsLinkPreviewImagePreviousButtonEnabled = false;
            }
            IsLinkPreviewImageNextButtonEnabled = true;
        }

        private void onLinkPreviewImageNextButtonClick(object param)
        {
            SelectedImageIndex++;
            PreviewImgUrl = AllImagesURLS.ElementAt(SelectedImageIndex);
            if (SelectedImageIndex + 2 > AllImagesURLS.Count)
            {
                IsLinkPreviewImageNextButtonEnabled = false;
            }
            IsLinkPreviewImagePreviousButtonEnabled = true;
        }

        private void onCancelLinkPreviewButtonClick(object param)
        {
            ActionRemoveLinkPreview();
        }

        private void onRtbNewStatusAreaDragOverEvent(object param)
        {
            if (param is DragEventArgs)
            {
                DragEventArgs rtbNewStatusAreaDragOverEvent = (DragEventArgs)param;
                checkDragFiles(rtbNewStatusAreaDragOverEvent);
                if (FilesDragged.Count == 0)
                {
                    rtbNewStatusAreaDragOverEvent.Effects = DragDropEffects.None;
                }
                else if (!rtbNewStatusAreaDragOverEvent.Handled)
                {
                    IsDragShadeOn = true;
                    rtbNewStatusAreaDragOverEvent.Handled = true;
                }
            }
        }

        private void onRtbNewStatusAreaDropEvent(object param)
        {
            if (param is DragEventArgs)
            {
                DragEventArgs rtbNewStatusAreaDropEvent = (DragEventArgs)param;
                IsDragShadeOn = false;
                CheckDroppedFiles(rtbNewStatusAreaDropEvent);
            }
        }

        private void onRtbNewStatusAreaPreviewDragLeaveEvent(object param)
        {
            if (param is DragEventArgs)
            {
                DragEventArgs rtbNewStatusAreaPreviewDragLeaveEvent = (DragEventArgs)param;
                if (IsDragShadeOn)
                {
                    IsDragShadeOn = false;
                }
                rtbNewStatusAreaPreviewDragLeaveEvent.Handled = true;
            }
        }

        public void onRtbNewStatusAreaPreviewDragLeaveEvent(DragEventArgs e)
        {
            if (IsDragShadeOn)
            {
                IsDragShadeOn = false;
            }
            e.Handled = true;
        }

        private void onFeelingDoingCancelButtonClick(object param)
        {
            SelectedDoingModel = null;
            SelectedDoingId = 0;
            FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
        }

        private void onAddtoImageAlbumButtonClick(object param)
        {
            if (param is TextBox)
            {
                TextBox imageAlbumTitleTextBox = (TextBox)param;
                if (DefaultSettings.MY_IMAGE_ALBUMS_COUNT == 0)
                {
                    UIHelperMethods.ShowWarning("You have No existing Image Albums!", "No Albums!");
                }
                else if (RingIDViewModel.Instance.MyImageAlubms.Count == DefaultSettings.MY_IMAGE_ALBUMS_COUNT)
                {
                    if (UCImageAlbumsPopUp.Instance == null)
                    {
                        UCImageAlbumsPopUp.Instance = new UCImageAlbumsPopUp(UCGuiRingID.Instance.MotherPanel);
                    }
                    UCImageAlbumsPopUp.Instance.Show();
                    UCImageAlbumsPopUp.Instance.ShowImagePopup(imageAlbumTitleTextBox, this);
                    UCImageAlbumsPopUp.Instance.OnRemovedUserControl += () =>
                    {
                        UCImageAlbumsPopUp.Instance = null;
                    };
                }
                else if (DefaultSettings.IsInternetAvailable)
                {
                    SendDataToServer.ListOfImageAlbumsOfUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_IMAGE, Guid.Empty);
                    if (UCImageAlbumsPopUp.Instance == null)
                    {
                        UCImageAlbumsPopUp.Instance = new UCImageAlbumsPopUp(UCGuiRingID.Instance.MotherPanel);
                    }
                    UCImageAlbumsPopUp.Instance.Show();
                    UCImageAlbumsPopUp.Instance.ShowImagePopup(imageAlbumTitleTextBox, this);
                    UCImageAlbumsPopUp.Instance.OnRemovedUserControl += () =>
                    {
                        UCImageAlbumsPopUp.Instance = null;
                    };
                }
            }
        }

        private void onSelectedImageAlbumCrossButtonClick(object param)
        {
            ImageAlbumTitleTextBoxText = "";
            AddToImageAlbumButtonVisibility = Visibility.Visible;
            SelectedImageAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsImageAlbumTitleTextBoxEnabled = true;
            IsPrivacyButtonEnabled = true;
            IsImageAlbumSelectedFromExisting = false;
        }

        private void onSelectedAudioAlbumCrossButtonClick(object param)
        {
            AudioAlbumTitleTextBoxText = "";
            AddtoAudioAlbumButtonVisibility = Visibility.Visible;
            SelectedAudioAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsAudioAlbumTitleTextBoxEnabled = true;
            IsPrivacyButtonEnabled = true;
            IsAudioAlbumSelectedFromExisting = false;
        }

        private void onSelectedVideoAlbumCrossButtonClick(object param)
        {
            VideoAlbumTitleTextBoxText = "";
            AddtoVideoAlbumButtonVisibility = Visibility.Visible;
            SelectedVideoAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsVideoAlbumTitleTextBoxEnabled = true;
            IsPrivacyButtonEnabled = true;
            IsVideoAlbumSelectedFromExisting = false;
        }

        private void onAudioAlbumTitleTextBoxTextChanged(object param)
        {
            if (param is TextBox)
            {
                setPrivacyForAlbumTitleChange((TextBox)param);
            }
        }

        private void onImageAlbumTitleTextBoxTextChanged(object param)
        {
            if (param is TextBox)
            {
                setPrivacyForAlbumTitleChange((TextBox)param);
            }
        }

        private void onVideoAlbumTitleTextBoxTextChanged(object param)
        {
            if (param is TextBox)
            {
                setPrivacyForAlbumTitleChange((TextBox)param);
            }
        }

        #endregion

        #region Private Methods

        private void lookUp(object sender, EventArgs e)
        {
            Dispatcher.Invoke(askSuggestions);
        }

        private void askSuggestions()
        {
            if (!locationTextBox.Text.Equals(LastMapSearchText))
            {
                LastMapSearchText = locationTextBox.Text;
                string address = LastMapSearchText.Trim();
                try
                {
                    if (IsLocationSet)
                    {
                        return;
                    }
                    if (LocationPopUp == null)
                    {
                        locationPopupView();
                    }
                    if (NewsFeedDictionaries.Instance.MAP_SEARCH_HISTORY.ContainsKey(address))
                    {
                        LocationPopUp.popupLocation.StaysOpen = true;
                        LocationPopUp.Show();
                        LocationPopUp.ShowLocationPopup(locationTextBox, this);
                        LocationPopUp.LoadLocationsFromHistory(address);
                    }
                    else
                    {
                        if (LocationPopUp != null)
                        {
                            if (LocationPopUp.LoadNewLocationFromMap(address))
                            {
                                LocationPopUp.popupLocation.StaysOpen = true;
                                LocationPopUp.Show();
                                LocationPopUp.ShowLocationPopup(locationTextBox, this);
                            }
                            else
                            {
                                LocationPopUp.popupLocation.StaysOpen = false;
                                LocationPopUp.popupLocation.IsOpen = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message + "\n" + ex.StackTrace);
                }
            }
            locationTextBox.Focus();
            locationTextBox.SelectionStart = locationTextBox.Text.Length;
        }

        private bool isSelectedMediaFilesLessThan500MB(int mediaType)
        {
            long count = 0;
            if (mediaType == 1)
            {
                foreach (var item in NewStatusMusicUpload)
                {
                    count += new FileInfo(item.FilePath).Length;
                }
                return (count < DefaultSettings.MaxFileLimit500MBinBytes);
            }
            else if (mediaType == 2)
            {
                foreach (var item in NewStatusVideoUpload)
                {
                    count += new FileInfo(item.FilePath).Length;
                }
                return (count < DefaultSettings.MaxFileLimit500MBinBytes);
            }
            return false;
        }

        private void checkDragFiles(DragEventArgs e)
        {
            try
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
                {
                    FilesDragged.Clear();
                    string[] fileNames = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                    if (fileNames != null && fileNames.Length > 0)
                    {
                        foreach (string filename in fileNames)
                        {
                            try
                            {
                                if (new FileInfo(filename).Length < DefaultSettings.MaxFileLimit250MBinBytes) FilesDragged.Add(filename);
                            }
                            catch (Exception) { }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CheckDragFiles() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void setPrivacyForAlbumTitleChange(TextBox albumTextBox)
        {
            if (albumTextBox.Text.Trim().Length > 0 && (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW))
            {
                PrivacyValue = AppConstants.PRIVACY_PUBLIC;
            }
        }

        #endregion

        #region Public Methods

        public void SetSelectedModelValuesForValidityPopup(bool isFromEditPopup = false, int validityValue = AppConstants.DEFAULT_VALIDITY)
        {
            ValidityValue = isFromEditPopup ? validityValue : SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY;

            String commonPartOfValidationMessage = "Post Validity";
            if (ValidityValue == AppConstants.DEFAULT_VALIDITY)
            {
                ValidityToolTipText = commonPartOfValidationMessage + " Unlimited";
            }
            else if (ValidityValue == AppConstants.VALIDITY_ONE_DAY)
            {
                ValidityToolTipText = commonPartOfValidationMessage + " " + ValidityValue + " Day";
            }
            else
            {
                ValidityToolTipText = commonPartOfValidationMessage + " " + ValidityValue + " Days";
            }
        }

        public void ActionRemoveLinkPreview()
        {
            PreviewDomain = "";
            PreviewDesc = "";
            PreviewUrl = "";
            InputUrl = "";
            PreviewImgUrl = "";
            PreviewTitle = "";
            PreviewLnkType = 0;
            UpdateLinkImage = false;
            AllImagesURLS.Clear();
            SelectedImageIndex = 0;
            PrevNextVisibility = Visibility.Collapsed;
        }

        public void ClearNewStatusHashTagsExceptPlaceHolder()
        {
            NewStatusHashTag.Clear();
            NewStatusHashTag.Add(PlaceHolderHashTagModel);
        }

        public void SetStatusImagePanelVisibility()
        {
            try
            {
                if (NewStatusImageUpload.Count > 0)
                {

                    AudioAlbumTitleTextBoxText = "";
                    VideoAlbumTitleTextBoxText = "";
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        NewStatusMusicUpload.Clear();
                    }
                    if (NewStatusVideoUpload.Count > 0)
                    {
                        NewStatusVideoUpload.Clear();
                    }
                    if (NewStatusHashTag.Count > 0)
                    {
                        ClearNewStatusHashTagsExceptPlaceHolder();
                    }
                    StatusImagePanelVisibility = Visibility.Visible;
                    AudioPanelVisibility = Visibility.Collapsed;
                    VideoPanelVisibility = Visibility.Collapsed;
                    PreviewCancelButtonVisibility = Visibility.Visible;
                    ActionRemoveLinkPreview();
                }
                else
                {
                    StatusImagePanelVisibility = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void SetPostLoaderandBtn(bool flag)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (!flag)
                {
                    PostLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
                }
                else
                {
                    GC.SuppressFinalize(PostLoader);
                    PostLoader = null;
                }
                IsPostButtonEnabled = flag;
            }));
        }

        public void EnablePostbutton(bool flag)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                try
                {
                    if (flag != prevState)
                    {
                        prevState = flag;
                        if (!flag)
                        {
                            PostLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
                        }
                        else
                        {
                            GC.SuppressFinalize(PostLoader);
                            PostLoader = null;
                        }
                        IsRtbNewStatusAreaEnabled = flag;
                        IsPostButtonEnabled = flag;
                        IsPreviewCancelButtonEnabled = flag;
                        if (NewStatusImageUpload.Count > 0 || NewStatusVideoUpload.Count > 0 || NewStatusMusicUpload.Count > 0)
                        {
                            PreviewCancelButtonVisibility = Visibility.Visible;
                        }
                        else
                        {
                            PreviewCancelButtonVisibility = Visibility.Collapsed;
                        }
                        IsAudioAlbumTitleTextBoxEnabled = flag;
                        IsVideoAlbumTitleTextBoxEnabled = flag;
                        IsImageAlbumTitleTextBoxEnabled = flag;
                        HashTagBoxEnabled = flag;
                        IsAddtoAudioAlbumButtonEnabled = flag;
                        IsAddtoVideoAlbumButtonEnabled = flag;
                        IsAddtoImageAlbumButtonEnabled = flag;
                        IsUploadPhotoButtonEnabled = flag;
                        IsNewStatusEmoticonEnabled = flag;
                        IsNewStatusTagFriendEnabled = flag;
                        IsNewStatusMusicEnabled = flag;
                        IsNewStatusVideoButtonEnabled = flag;
                        IsNewStatusLocationButtonEnabled = flag;
                        IsValidityButtonEnabled = flag;
                        IsAudioAddHashTagWrapPanelEnabled = flag;
                        IsVideoAddHashTagGridPanelEnabled = flag;
                        IsPrivacyButtonEnabled = flag;
                        IsSelectedImageAlbumCrossButtonEnabled = flag;
                        IsSelectedAudioAlbumCrossButtonEnabled = flag;
                        IsSelectedVideoAlbumCrossButtonEnabled = flag;
                        if (NewStatusImageUpload.Count > 0)
                        {
                            foreach (var item in NewStatusImageUpload)
                            {
                                item.CaptionEnabled = flag;
                                item.RemoveEnabled = flag;
                                if (flag) { item.IsUploadedInImageServer = false; UploadingText = "Selected Image(s): " + NewStatusImageUpload.Count; }
                            }
                        }
                            
                        if (NewStatusMusicUpload.Count > 0)
                        {
                            foreach (var item in NewStatusMusicUpload)
                            {
                                item.TitleEnabled = flag;
                                if (flag)
                                {
                                    item.IsUploadedInAudioServer = false;
                                }
                            }
                        }

                        if (NewStatusVideoUpload.Count > 0)
                        {
                            foreach (var item in NewStatusVideoUpload)
                            {
                                item.TitleEnabled = flag;
                                if (flag)
                                {
                                    item.IsUploadedInVideoServer = false;
                                }
                            }
                        }
                            
                        if (NewStatusHashTag.Count > 0)
                        {
                            foreach (var item in NewStatusHashTag)
                            {
                                item.RemoveEnabled = flag;
                            }
                        }
                            
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }));
        }

        public void ActionRemoveLocationFeelingTagStatusImagesMedia()
        {
            SelectedLocationModel = null;
            if (TaggedUtids != null)
            {
                TaggedUtids.Clear();
            }
            if (TaggedFriends != null)
            {
                foreach (var item in TaggedFriends)
                {
                    item.IsVisibleInTagList = false;
                }
                TaggedFriends.Clear();
            }
            LocationTextBoxText = "";
            SelectedDoingModel = null;
            SelectedDoingId = 0;
            richTextFeedEditFromViewModel.Document.Blocks.Clear();
            StatusText = "";
            StatusTextWithoutTags = "";
            StatusTagsJArray = null;
            AudioAlbumTitleTextBoxText = "";
            VideoAlbumTitleTextBoxText = "";
            ImageAlbumTitleTextBoxText = "";
            if (NewStatusImageUpload.Count > 0)
            {
                NewStatusImageUpload.Clear();
            }
            if (NewStatusMusicUpload.Count > 0)
            {
                NewStatusMusicUpload.Clear();
            }
            if (NewStatusVideoUpload.Count > 0)
            {
                NewStatusVideoUpload.Clear();
            }
            if (NewStatusHashTag.Count > 0)
            {
                ClearNewStatusHashTagsExceptPlaceHolder();
            }
            TagListHolderVisibility = Visibility.Collapsed;
            StatusImagePanelVisibility = Visibility.Collapsed;
            VideoPanelVisibility = Visibility.Collapsed;
            AudioPanelVisibility = Visibility.Collapsed;
            PreviewCancelButtonVisibility = Visibility.Collapsed;
            LocationMainBorderPanelVisibility = Visibility.Collapsed;
            FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
        }

        #endregion

        #region Private Methods

        private void SetFeedWallType(NewStatusView newStatusView)
        {
            if (newStatusView.Tag != null)
            {
                if (newStatusView.Tag is string)
                {
                    FeedWallType = Convert.ToInt32(newStatusView.Tag);
                    FriendUtIdOrCircleId = 0;
                }
                else
                {
                    dynamic obj = (ExpandoObject)newStatusView.Tag;
                    FeedWallType = (int)obj.type;
                    FriendUtIdOrCircleId = (long)obj.id;
                }
            }
        }

        private void CheckDroppedFiles(DragEventArgs e)
        {
            if (FilesDragged.Count > 0)
            {
                int currentFiletype = CurrentNewStatusFileType();
                foreach (string filename in FilesDragged)
                {
                    try
                    {
                        int currentAddedFiletype = CurrentAddedFileType(filename);
                        if (currentAddedFiletype == 0)
                        {
                            continue;
                        }
                        if (currentFiletype == 0)
                        {
                            currentFiletype = currentAddedFiletype;
                        }
                        else if (currentFiletype != currentAddedFiletype)
                        {
                            continue;
                        }
                        if (currentFiletype == 1)
                        {
                            ImageUploaderModel model = new ImageUploaderModel();
                            model.FilePath = filename;
                            NewStatusImageUpload.Add(model);
                            UploadingText = "Selected Image(s): " + NewStatusImageUpload.Count;
                            AudioAlbumTitleTextBoxText = "";
                            VideoAlbumTitleTextBoxText = "";
                            if (NewStatusMusicUpload.Count > 0)
                            {
                                NewStatusMusicUpload.Clear();
                            }
                            if (NewStatusVideoUpload.Count > 0)
                            {
                                NewStatusVideoUpload.Clear();
                            }
                            if (NewStatusHashTag.Count > 0)
                            {
                                ClearNewStatusHashTagsExceptPlaceHolder();
                            }
                            AudioPanelVisibility = Visibility.Collapsed;
                            VideoPanelVisibility = Visibility.Collapsed;
                            ActionRemoveLinkPreview();
                            StatusImagePanelVisibility = Visibility.Visible;
                            PreviewCancelButtonVisibility = Visibility.Visible;
                        }
                        else if (currentFiletype == 3 && NewStatusVideoUpload.Count < 5)
                        {
                            FeedVideoUploaderModel model = new FeedVideoUploaderModel();
                            model.FilePath = filename;
                            NewStatusVideoUpload.Add(model);
                            UploadingText = "";
                            model.VideoTitle = Path.GetFileNameWithoutExtension(filename);
                            model.VideoDuration = (long)(new MediaFile(filename).duration / 1000);
                            if (model.VideoDuration == 0)
                            {
                                model.VideoDuration = HelperMethods.MediaPlayerDuration(filename);
                            }
                            new SetArtImageFromVideo(model);//120*90
                            AudioAlbumTitleTextBoxText = "";
                            ImageAlbumTitleTextBoxText = "";
                            if (NewStatusImageUpload.Count > 0)
                            {
                                NewStatusImageUpload.Clear();
                            }
                            if (NewStatusMusicUpload.Count > 0)
                            {
                                NewStatusMusicUpload.Clear();
                            }
                            if (NewStatusHashTag.Count > 0)
                            {
                                ClearNewStatusHashTagsExceptPlaceHolder();
                            }
                            StatusImagePanelVisibility = Visibility.Collapsed;
                            AudioPanelVisibility = Visibility.Collapsed;
                            ActionRemoveLinkPreview();
                            VideoPanelVisibility = Visibility.Visible;
                            PreviewCancelButtonVisibility = Visibility.Visible;
                        }
                        else if (currentFiletype == 2 && NewStatusMusicUpload.Count < 5)
                        {
                            MusicUploaderModel model = new MusicUploaderModel();
                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                                if (file.Tag != null && file.Tag.Pictures.Any())
                                {
                                    if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null)
                                    {
                                        model.IsImageFound = true;
                                    }
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                    {
                                        model.AudioArtist = file.Tag.Performers[0];
                                    }
                                    else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                    {
                                        model.AudioArtist = file.Tag.AlbumArtists[0];
                                    }
                                    model.AudioTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception ex) 
                            {
                                log.Error("Error Message : " + ex.Message + "Stack Trace : " + ex.StackTrace);
                            }
                            if (string.IsNullOrEmpty(model.AudioTitle))
                            {
                                model.AudioTitle = Path.GetFileNameWithoutExtension(filename);
                            }
                            model.AudioDuration = (long)(new MediaFile(filename).duration / 1000);
                            if (model.AudioDuration == 0)
                            {
                                model.AudioDuration = HelperMethods.MediaPlayerDuration(filename);
                            }
                            model.FilePath = filename;
                            NewStatusMusicUpload.Add(model);
                            UploadingText = "";
                            VideoAlbumTitleTextBoxText = "";
                            ImageAlbumTitleTextBoxText = "";
                            if (NewStatusImageUpload.Count > 0)
                            {
                                NewStatusImageUpload.Clear();
                            }
                            if (NewStatusVideoUpload.Count > 0)
                            {
                                NewStatusVideoUpload.Clear();
                            }
                            if (NewStatusHashTag.Count > 0)
                            {
                                ClearNewStatusHashTagsExceptPlaceHolder();
                            }
                            StatusImagePanelVisibility = Visibility.Collapsed;
                            VideoPanelVisibility = Visibility.Collapsed;
                            ActionRemoveLinkPreview();
                            AudioPanelVisibility = Visibility.Visible;
                            PreviewCancelButtonVisibility = Visibility.Visible;
                        }
                    }
                    catch (Exception) { }
                }
                FilesDragged.Clear();
            }
        }

        private int CurrentNewStatusFileType()
        {
            if (NewStatusImageUpload.Count > 0)
            {
                return 1;
            }
            else if (NewStatusMusicUpload.Count > 0)
            {
                return 2;
            }
            else if (NewStatusVideoUpload.Count > 0)
            {
                return 3;
            }
            return 0;
        }

        private int CurrentAddedFileType(string filename)
        {
            try
            {
                int formatChecked = ImageUtility.GetFileImageTypeFromHeader(filename);
                if (formatChecked == 1 || formatChecked == 2)
                {
                    return 1;
                }
                MediaFile mf = new MediaFile(filename);
                if (mf.Audio != null && mf.Audio.Count > 0 && (mf.Video == null || mf.Video.Count == 0) && mf.format.Equals("MPEG Audio"))
                {
                    return 2;
                }
                else if (mf.Video != null && mf.Video.Count > 0 && mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264"))
                {
                    return 3;
                }
            }
            catch (Exception ex) 
            {
                log.Error(" Error message : " + ex.Message + "Stack Trace : " + ex.StackTrace);
            }
            return 0;
        }

        #endregion
    }
}
=======
﻿using log4net;
using MediaInfoDotNet;
using Microsoft.Win32;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Feed;
using View.UI.PopUp;
using View.UI.Profile.MyProfile;
using View.Utility;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;

namespace View.ViewModel.NewStatus
{
    public class NewStatusViewModel : BaseViewModel
    {

        #region Private Fields

        private readonly ILog log = LogManager.GetLogger(typeof(NewStatusViewModel).Name);
        private DispatcherTimer timer;
        private string LastMapSearchText = "";
        private TextBox locationTextBox;
        private bool prevState = true;
        List<string> FilesDragged = new List<string>();

        #endregion

        #region Public Fields

        public UCMediaCloudUploadPopup ucMediaCloudUploadPopup = null;
        public string StatusText;
        public string StatusTextWithoutTags;
        public string InputUrl = "";
        public int PreviewLnkType = 0;
        public List<string> AllImagesURLS = new List<string>();
        public int SelectedImageIndex = 0;
        public long SelectedDoingId;
        public List<long> TaggedUtids = null;
        public List<long> FriendsToShowOrHideUtIds = new List<long>();
        public JArray StatusTagsJArray = null;
        public RichTextFeedEdit richTextFeedEditFromViewModel;
        public int FeedWallType;
        public long FriendUtIdOrCircleId;

        #endregion

        #region Constructor

        public NewStatusViewModel()
        {
            NewStatusHashTag.Add(PlaceHolderHashTagModel);
            SetSelectedModelValuesForValidityPopup();
        }

        #endregion

        #region Observable Collections

        private ObservableCollection<ImageUploaderModel> _newStatusImageUpload = new ObservableCollection<ImageUploaderModel>();
        public ObservableCollection<ImageUploaderModel> NewStatusImageUpload
        {
            get
            {
                return _newStatusImageUpload;
            }
            set
            {
                SetProperty(ref _newStatusImageUpload, value, "NewStatusImageUpload");
            }
        }

        private ObservableCollection<HashTagModel> _newStatusHashTag = new ObservableCollection<HashTagModel>();
        public ObservableCollection<HashTagModel> NewStatusHashTag
        {
            get
            {
                return _newStatusHashTag;
            }
            set
            {
                SetProperty(ref _newStatusHashTag, value, "NewStatusHashTag");
            }
        }

        private ObservableCollection<FeedVideoUploaderModel> _newStatusVideoUpload = new ObservableCollection<FeedVideoUploaderModel>();
        public ObservableCollection<FeedVideoUploaderModel> NewStatusVideoUpload
        {
            get
            {
                return _newStatusVideoUpload;
            }
            set
            {
                SetProperty(ref _newStatusVideoUpload, value, "NewStatusVideoUpload");
            }
        }


        private ObservableCollection<MusicUploaderModel> _newStatusMusicUpload = new ObservableCollection<MusicUploaderModel>();
        public ObservableCollection<MusicUploaderModel> NewStatusMusicUpload
        {
            get
            {
                return _newStatusMusicUpload;
            }
            set
            {
                SetProperty(ref _newStatusMusicUpload, value, "NewStatusMusicUpload");
            }
        }

        private ObservableCollection<UserShortInfoModel> _taggedFriends;
        public ObservableCollection<UserShortInfoModel> TaggedFriends
        {
            get
            {
                return _taggedFriends;
            }
            set
            {
                SetProperty(ref _taggedFriends, value, "TaggedFriends");
            }
        }

        private ObservableCollection<UserBasicInfoModel> _friendsToHideOrShow = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> FriendsToHideOrShow
        {
            get
            {
                return _friendsToHideOrShow;
            }
            set
            {
                SetProperty(ref _friendsToHideOrShow, value, "FriendsToHideOrShow");
            }
        }

        #endregion

        #region Properties

        private UCNewStatusFeelingPopup ucNewStatusFeelingPopup { get; set; }

        private UCFeedOptionsPopup ucFeedOptionsPopup { get; set; }

        private UCNewStatusValidityPopup ucNewStatusValidityPopup { get; set; }

        private UCLocationPopUp LocationPopUp { get; set; }

        private UCNewStatusPrivacyPopup ucNewStatusPrivacyPopup { get; set; }

        public UCHashTagPopUp HashTagPopUp
        {
            get
            {
                return MainSwitcher.PopupController.HashTagPopUp;
            }
        }

        private UCNewStatusTagPopup ucNewStatusTagPopup { get; set; }

        #endregion

        #region Private Fields and their Public Properties

        private string _previewDomain = "";
        public string PreviewDomain
        {
            get
            {
                return _previewDomain;
            }
            set
            {
                SetProperty(ref _previewDomain, value, "PreviewDomain");
            }
        }

        private string _previewDesc = "";
        public string PreviewDesc
        {
            get
            {
                return _previewDesc;
            }
            set
            {
                SetProperty(ref _previewDesc, value, "PreviewDesc");
            }
        }

        private string _previewUrl = "";
        public string PreviewUrl
        {
            get
            {
                return _previewUrl;
            }
            set
            {
                SetProperty(ref _previewUrl, value, "PreviewUrl");
            }
        }

        private string _previewImgUrl = "";
        public string PreviewImgUrl
        {
            get
            {
                return _previewImgUrl;
            }
            set
            {
                SetProperty(ref _previewImgUrl, value, "PreviewImgUrl");
            }
        }

        private string _previewTitle = "";
        public string PreviewTitle
        {
            get
            {
                return _previewTitle;
            }
            set
            {
                SetProperty(ref _previewTitle, value, "PreviewTitle");
            }
        }

        private string _uploadingText;
        public string UploadingText
        {
            get
            {
                return _uploadingText;
            }
            set
            {
                SetProperty(ref _uploadingText, value, "UploadingText");
            }
        }

        private HashTagModel _placeHolderHashTagModel = new HashTagModel { TagType = 0 };
        public HashTagModel PlaceHolderHashTagModel
        {
            get
            {
                return _placeHolderHashTagModel;
            }
            set
            {
                SetProperty(ref _placeHolderHashTagModel, value, "PlaceHolderHashTagModel");
            }
        }

        private DoingModel _selectedDoingModel = null;
        public DoingModel SelectedDoingModel
        {
            get
            {
                return _selectedDoingModel;
            }
            set
            {
                SetProperty(ref _selectedDoingModel, value, "SelectedDoingModel");
            }
        }

        private bool _emoPopup;
        public bool EmoPopup
        {
            get
            {
                return _emoPopup;
            }
            set
            {
                SetProperty(ref _emoPopup, value, "EmoPopup");
            }
        }

        private bool _tagPopup;
        public bool TagPopup
        {
            get
            {
                return _tagPopup;
            }
            set
            {
                SetProperty(ref _tagPopup, value, "TagPopup");
            }
        }

        private LocationModel _selectedLocationModel = null;
        public LocationModel SelectedLocationModel
        {
            get
            {
                return _selectedLocationModel;
            }
            set
            {
                SetProperty(ref _selectedLocationModel, value, "SelectedLocationModel");
            }
        }

        private bool _privacyPopup;
        public bool PrivacyPopup
        {
            get
            {
                return _privacyPopup;
            }
            set
            {
                SetProperty(ref _privacyPopup, value, "PrivacyPopup");
            }
        }

        /// <summary>
        /// PRIVACY_ONLY_ME = 1	Only me. None will see this content except me.
        /// PRIVACY_SP_FRIEND2SHOW = 5	This content is visibl only to specific friends.
        /// PRIVACY_SP_FRIEND2HIDE = 10	This content is kept hidden from specific friends.
        /// PRIVACY_FRIEND = 15	Only friends can see this content.
        /// PRIVACY_FOF = 20	Friends of friends can see this content.
        /// PRIVACY_PUBLIC = 25	Anyone can see this content.
        /// </summary>
        private int _privacyValue = AppConstants.PRIVACY_PUBLIC;
        public int PrivacyValue
        {
            get
            {
                return _privacyValue;
            }
            set
            {
                SetProperty(ref _privacyValue, value, "PrivacyValue");
            }
        }

        private ImageSource _postLoader;
        public ImageSource PostLoader
        {
            get
            {
                return _postLoader;
            }
            set
            {
                SetProperty(ref _postLoader, value, "PostLoader");
            }
        }

        private bool _feedViewPopup;
        public bool FeedViewPopup
        {
            get
            {
                return _feedViewPopup;
            }
            set
            {
                SetProperty(ref _feedViewPopup, value, "FeedViewPopup");
            }
        }

        private bool _optionBool = false;
        public bool OptionBool
        {
            get
            {
                return _optionBool;
            }
            set
            {
                SetProperty(ref _optionBool, value, "OptionBool");
            }
        }

        private bool _isDragShadeOn = false;
        public bool IsDragShadeOn
        {
            get
            {
                return _isDragShadeOn;
            }
            set
            {
                _isDragShadeOn = value;
                this.OnPropertyChanged("IsDragShadeOn");
            }
        }

        private int _validityValue = AppConstants.DEFAULT_VALIDITY;
        public int ValidityValue
        {
            get
            {
                return _validityValue;
            }
            set
            {
                SetProperty(ref _validityValue, value, "ValidityValue");
            }
        }

        private bool _isLocationSet = false;
        public bool IsLocationSet
        {
            get
            {
                return _isLocationSet;
            }
            set
            {
                SetProperty(ref _isLocationSet, value, "IsLocationSet");
            }
        }

        private bool _isImageAlbumSelectedFromExisting = false;
        public bool IsImageAlbumSelectedFromExisting
        {
            get
            {
                return _isImageAlbumSelectedFromExisting;
            }
            set
            {
                SetProperty(ref _isImageAlbumSelectedFromExisting, value, "IsImageAlbumSelectedFromExisting");
            }
        }

        private bool _isAudioAlbumSelectedFromExisting = false;
        public bool IsAudioAlbumSelectedFromExisting
        {
            get
            {
                return _isAudioAlbumSelectedFromExisting;
            }
            set
            {
                SetProperty(ref _isAudioAlbumSelectedFromExisting, value, "IsAudioAlbumSelectedFromExisting");
            }
        }

        private bool _isVideoAlbumSelectedFromExisting = false;
        public bool IsVideoAlbumSelectedFromExisting
        {
            get
            {
                return _isVideoAlbumSelectedFromExisting;
            }
            set
            {
                SetProperty(ref _isVideoAlbumSelectedFromExisting, value, "IsVideoAlbumSelectedFromExisting");
            }
        }

        #endregion

        #region UI Visibility

        private Visibility _locationMainBorderPanelVisibility = Visibility.Collapsed;
        public Visibility LocationMainBorderPanelVisibility
        {
            get
            {
                return _locationMainBorderPanelVisibility;
            }
            set
            {
                SetProperty(ref _locationMainBorderPanelVisibility, value, "LocationMainBorderPanelVisibility");
            }
        }

        private Visibility _feelingDoingButtonContainerVisibility = Visibility.Collapsed;
        public Visibility FeelingDoingButtonContainerVisibility
        {
            get
            {
                return _feelingDoingButtonContainerVisibility;
            }
            set
            {
                SetProperty(ref _feelingDoingButtonContainerVisibility, value, "FeelingDoingButtonContainerVisibility");
            }
        }

        private Visibility _tagListHolderVisibility = Visibility.Collapsed;
        public Visibility TagListHolderVisibility
        {
            get
            {
                return _tagListHolderVisibility;
            }
            set
            {
                SetProperty(ref _tagListHolderVisibility, value, "TagListHolderVisibility");
            }
        }

        private Visibility _prevNextVisibility = Visibility.Collapsed;
        public Visibility PrevNextVisibility
        {
            get
            {
                return _prevNextVisibility;
            }
            set
            {
                SetProperty(ref _prevNextVisibility, value, "PrevNextVisibility");
            }
        }

        private Visibility _previewCancelButtonVisibility = Visibility.Collapsed;
        public Visibility PreviewCancelButtonVisibility
        {
            get
            {
                return _previewCancelButtonVisibility;
            }
            set
            {
                SetProperty(ref _previewCancelButtonVisibility, value, "PreviewCancelButtonVisibility");
            }
        }

        private Visibility _statusImagePanelVisibility = Visibility.Collapsed;
        public Visibility StatusImagePanelVisibility
        {
            get
            {
                return _statusImagePanelVisibility;
            }
            set
            {
                SetProperty(ref _statusImagePanelVisibility, value, "StatusImagePanelVisibility");
            }
        }

        private Visibility _audioPanelVisibility = Visibility.Collapsed;
        public Visibility AudioPanelVisibility
        {
            get
            {
                return _audioPanelVisibility;
            }
            set
            {
                SetProperty(ref _audioPanelVisibility, value, "AudioPanelVisibility");
            }
        }

        private Visibility _videoPanelVisibility = Visibility.Collapsed;
        public Visibility VideoPanelVisibility
        {
            get
            {
                return _videoPanelVisibility;
            }
            set
            {
                SetProperty(ref _videoPanelVisibility, value, "VideoPanelVisibility");
            }
        }

        private Visibility _writeSomethingTextBlockVisibility = Visibility.Visible;
        public Visibility WriteSomethingTextBlockVisibility
        {
            get
            {
                return _writeSomethingTextBlockVisibility;
            }
            set
            {
                SetProperty(ref _writeSomethingTextBlockVisibility, value, "WriteSomethingTextBlockVisibility");
            }
        }

        private Visibility _privacyButtonVisibility = Visibility.Visible;
        public Visibility PrivacyButtonVisibility
        {
            get
            {
                return _privacyButtonVisibility;
            }
            set
            {
                SetProperty(ref _privacyButtonVisibility, value, "PrivacyButtonVisibility");
            }
        }

        private Visibility _newStatusTagFriendButtonVisibility = Visibility.Visible;
        public Visibility NewStatusTagFriendButtonVisibility
        {
            get
            {
                return _newStatusTagFriendButtonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusTagFriendButtonVisibility, value, "NewStatusTagFriendButtonVisibility");
            }
        }

        private Visibility _optionsButtonVisibility = Visibility.Visible;
        public Visibility OptionsButtonVisibility
        {
            get
            {
                return _optionsButtonVisibility;
            }
            set
            {
                SetProperty(ref _optionsButtonVisibility, value, "OptionsButtonVisibility");
            }
        }

        private Visibility _bottomLeftStackPanelVisibility = Visibility.Visible;
        public Visibility BottomLeftStackPanelVisibility
        {
            get
            {
                return _bottomLeftStackPanelVisibility;
            }
            set
            {
                SetProperty(ref _bottomLeftStackPanelVisibility, value, "BottomLeftStackPanelVisibility");
            }
        }

        private Visibility _newStatusLocationButtonVisibility = Visibility.Visible;
        public Visibility NewStatusLocationButtonVisibility
        {
            get
            {
                return _newStatusLocationButtonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusLocationButtonVisibility, value, "NewStatusLocationButtonVisibility");
            }
        }

        private Visibility _newStatusEmoticonVisibility = Visibility.Visible;
        public Visibility NewStatusEmoticonVisibility
        {
            get
            {
                return _newStatusEmoticonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusEmoticonVisibility, value, "NewStatusEmoticonVisibility");
            }
        }

        private Visibility _newStatusMusicButtonVisibility = Visibility.Visible;
        public Visibility NewStatusMusicButtonVisibility
        {
            get
            {
                return _newStatusMusicButtonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusMusicButtonVisibility, value, "NewStatusMusicButtonVisibility");
            }
        }

        private Visibility _newStatusVideoButtonVisibility = Visibility.Visible;
        public Visibility NewStatusVideoButtonVisibility
        {
            get
            {
                return _newStatusVideoButtonVisibility;
            }
            set
            {
                SetProperty(ref _newStatusVideoButtonVisibility, value, "NewStatusVideoButtonVisibility");
            }
        }

        private Visibility _uploadPhotoButtonVisibility = Visibility.Visible;
        public Visibility UploadPhotoButtonVisibility
        {
            get
            {
                return _uploadPhotoButtonVisibility;
            }
            set
            {
                SetProperty(ref _uploadPhotoButtonVisibility, value, "UploadPhotoButtonVisibility");
            }
        }

        private Visibility _audioTitlePanelVisibility = Visibility.Visible;
        public Visibility AudioTitlePanelVisibility
        {
            get
            {
                return _audioTitlePanelVisibility;
            }
            set
            {
                SetProperty(ref _audioTitlePanelVisibility, value, "AudioTitlePanelVisibility");
            }
        }

        private Visibility _audioHashTagGridPanelVisibility = Visibility.Visible;
        public Visibility AudioHashTagGridPanelVisibility
        {
            get
            {
                return _audioHashTagGridPanelVisibility;
            }
            set
            {
                SetProperty(ref  _audioHashTagGridPanelVisibility, value, "AudioHashTagGridPanelVisibility");
            }
        }

        private Visibility _videoTitlePanelVisibility = Visibility.Visible;
        public Visibility VideoTitlePanelVisibility
        {
            get
            {
                return _videoTitlePanelVisibility;
            }
            set
            {
                SetProperty(ref _videoTitlePanelVisibility, value, "VideoTitlePanelVisibility");
            }
        }

        private Visibility _videoAddHAshTagGridPanelVisibility = Visibility.Visible;
        public Visibility VideoAddHAshTagGridPanelVisibility
        {
            get
            {
                return _videoAddHAshTagGridPanelVisibility;
            }
            set
            {
                SetProperty(ref _videoAddHAshTagGridPanelVisibility, value, "VideoAddHAshTagGridPanelVisibility");
            }
        }

        private Visibility _imageTitlePanelVisibility = Visibility.Visible;
        public Visibility ImageTitlePanelVisibility
        {
            get
            {
                return _imageTitlePanelVisibility;
            }
            set
            {
                SetProperty(ref _imageTitlePanelVisibility, value, "ImageTitlePanelVisibility");
            }
        }

        private Visibility _selectedImageAlbumCrossButtonVisibility = Visibility.Collapsed;
        public Visibility SelectedImageAlbumCrossButtonVisibility
        {
            get
            {
                return _selectedImageAlbumCrossButtonVisibility;
            }
            set
            {
                SetProperty(ref _selectedImageAlbumCrossButtonVisibility, value, "SelectedImageAlbumCrossButtonVisibility");
            }
        }

        private Visibility _addToImageAlbumButtonVisibility = Visibility.Visible;
        public Visibility AddToImageAlbumButtonVisibility
        {
            get
            {
                return _addToImageAlbumButtonVisibility;
            }
            set
            {
                SetProperty(ref _addToImageAlbumButtonVisibility, value, "AddToImageAlbumButtonVisibility");
            }
        }

        private Visibility _selectedAudioAlbumCrossButtonVisibility = Visibility.Collapsed;
        public Visibility SelectedAudioAlbumCrossButtonVisibility
        {
            get
            {
                return _selectedAudioAlbumCrossButtonVisibility;
            }
            set
            {
                SetProperty(ref _selectedAudioAlbumCrossButtonVisibility, value, "SelectedAudioAlbumCrossButtonVisibility");
            }
        }

        private Visibility _addtoAudioAlbumButtonVisibility = Visibility.Visible;
        public Visibility AddtoAudioAlbumButtonVisibility
        {
            get
            {
                return _addtoAudioAlbumButtonVisibility;
            }
            set
            {
                SetProperty(ref _addtoAudioAlbumButtonVisibility, value, "AddtoAudioAlbumButtonVisibility");
            }
        }

        private Visibility _selectedVideoAlbumCrossButtonVisibility = Visibility.Collapsed;
        public Visibility SelectedVideoAlbumCrossButtonVisibility
        {
            get
            {
                return _selectedVideoAlbumCrossButtonVisibility;
            }
            set
            {
                SetProperty(ref _selectedVideoAlbumCrossButtonVisibility, value, "SelectedVideoAlbumCrossButtonVisibility");
            }
        }

        private Visibility _addtoVideoAlbumButtonVisibility = Visibility.Visible;
        public Visibility AddtoVideoAlbumButtonVisibility
        {
            get
            {
                return _addtoVideoAlbumButtonVisibility;
            }
            set
            {
                SetProperty(ref _addtoVideoAlbumButtonVisibility, value, "AddtoVideoAlbumButtonVisibility");
            }
        }

        #endregion

        #region UI IsOpen

        private bool _isPopupUploadPhotoOpen = false;
        public bool IsPopupUploadPhotoOpen
        {
            get
            {
                return _isPopupUploadPhotoOpen;
            }
            set
            {
                SetProperty(ref _isPopupUploadPhotoOpen, value, "IsPopupUploadPhotoOpen");
            }
        }

        private bool _isPopupUploadVideoOpen = false;
        public bool IsPopupUploadVideoOpen
        {
            get
            {
                return _isPopupUploadVideoOpen;
            }
            set
            {
                SetProperty(ref _isPopupUploadVideoOpen, value, "IsPopupUploadVideoOpen");
            }
        }

        #endregion

        #region UI IsEnabled

        private bool _isRtbNewStatusAreaEnabled = true;
        public bool IsRtbNewStatusAreaEnabled
        {
            get
            {
                return _isRtbNewStatusAreaEnabled;
            }
            set
            {
                SetProperty(ref _isRtbNewStatusAreaEnabled, value, "IsRtbNewStatusAreaEnabled");
            }
        }

        private bool _isPostButtonEnabled = true;
        public bool IsPostButtonEnabled
        {
            get
            {
                return _isPostButtonEnabled;
            }
            set
            {
                SetProperty(ref _isPostButtonEnabled, value, "IsPostButtonEnabled");
            }
        }

        private bool _hashTagBoxEnabled = true;
        public bool HashTagBoxEnabled
        {
            get
            {
                return _hashTagBoxEnabled;
            }
            set
            {
                SetProperty(ref _hashTagBoxEnabled, value, "HashTagBoxEnabled");
            }
        }

        private bool _isPreviewCancelButtonEnabled = true;
        public bool IsPreviewCancelButtonEnabled
        {
            get
            {
                return _isPreviewCancelButtonEnabled;
            }
            set
            {
                SetProperty(ref _isPreviewCancelButtonEnabled, value, "IsPreviewCancelButtonEnabled");
            }
        }

        private bool _isAudioAlbumTitleTextBoxEnabled = true;
        public bool IsAudioAlbumTitleTextBoxEnabled
        {
            get
            {
                return _isAudioAlbumTitleTextBoxEnabled;
            }
            set
            {
                SetProperty(ref _isAudioAlbumTitleTextBoxEnabled, value, "IsAudioAlbumTitleTextBoxEnabled");
            }
        }

        private bool _isVideoAlbumTitleTextBoxEnabled = true;
        public bool IsVideoAlbumTitleTextBoxEnabled
        {
            get
            {
                return _isVideoAlbumTitleTextBoxEnabled;
            }
            set
            {
                SetProperty(ref _isVideoAlbumTitleTextBoxEnabled, value, "IsVideoAlbumTitleTextBoxEnabled");
            }

        }

        private bool _isAddtoAudioAlbumButtonEnabled = true;
        public bool IsAddtoAudioAlbumButtonEnabled
        {
            get
            {
                return _isAddtoAudioAlbumButtonEnabled;
            }
            set
            {
                SetProperty(ref _isAddtoAudioAlbumButtonEnabled, value, "IsAddtoAudioAlbumButtonEnabled");
            }

        }

        private bool _isAddtoVideoAlbumButtonEnabled = true;
        public bool IsAddtoVideoAlbumButtonEnabled
        {
            get
            {
                return _isAddtoVideoAlbumButtonEnabled;
            }
            set
            {
                SetProperty(ref _isAddtoVideoAlbumButtonEnabled, value, "IsAddtoVideoAlbumButtonEnabled");
            }

        }

        private bool _isUploadPhotoButtonEnabled = true;
        public bool IsUploadPhotoButtonEnabled
        {
            get
            {
                return _isUploadPhotoButtonEnabled;
            }
            set
            {
                SetProperty(ref _isUploadPhotoButtonEnabled, value, "IsUploadPhotoButtonEnabled");
            }

        }

        private bool _isNewStatusEmoticonEnabled = true;
        public bool IsNewStatusEmoticonEnabled
        {
            get
            {
                return _isNewStatusEmoticonEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusEmoticonEnabled, value, "IsNewStatusEmoticonEnabled");
            }

        }

        private bool _isNewStatusTagFriendEnabled = true;
        public bool IsNewStatusTagFriendEnabled
        {
            get
            {
                return _isNewStatusTagFriendEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusTagFriendEnabled, value, "IsNewStatusTagFriendEnabled");
            }

        }

        private bool _isNewStatusMusicEnabled = true;
        public bool IsNewStatusMusicEnabled
        {
            get
            {
                return _isNewStatusMusicEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusMusicEnabled, value, "IsNewStatusMusicEnabled");
            }

        }

        private bool _isNewStatusVideoButtonEnabled = true;
        public bool IsNewStatusVideoButtonEnabled
        {
            get
            {
                return _isNewStatusVideoButtonEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusVideoButtonEnabled, value, "IsNewStatusVideoButtonEnabled");
            }

        }

        private bool _isNewStatusLocationButtonEnabled = true;
        public bool IsNewStatusLocationButtonEnabled
        {
            get
            {
                return _isNewStatusLocationButtonEnabled;
            }
            set
            {
                SetProperty(ref _isNewStatusLocationButtonEnabled, value, "IsNewStatusLocationButtonEnabled");
            }

        }

        private bool _isPrivacyButtonEnabled = true;
        public bool IsPrivacyButtonEnabled
        {
            get
            {
                return _isPrivacyButtonEnabled;
            }
            set
            {
                SetProperty(ref _isPrivacyButtonEnabled, value, "IsPrivacyButtonEnabled");
            }

        }

        private bool _isValidityButtonEnabled = true;
        public bool IsValidityButtonEnabled
        {
            get
            {
                return _isValidityButtonEnabled;
            }
            set
            {
                SetProperty(ref _isValidityButtonEnabled, value, "IsValidityButtonEnabled");
            }

        }

        private bool _isLinkPreviewImagePreviousButtonEnabled = false;
        public bool IsLinkPreviewImagePreviousButtonEnabled
        {
            get
            {
                return _isLinkPreviewImagePreviousButtonEnabled;
            }
            set
            {
                SetProperty(ref _isLinkPreviewImagePreviousButtonEnabled, value, "IsLinkPreviewImagePreviousButtonEnabled");
            }
        }

        private bool _isLinkPreviewImageNextButtonEnabled = true;
        public bool IsLinkPreviewImageNextButtonEnabled
        {
            get
            {
                return _isLinkPreviewImageNextButtonEnabled;
            }
            set
            {
                SetProperty(ref _isLinkPreviewImageNextButtonEnabled, value, "IsLinkPreviewImageNextButtonEnabled");
            }
        }

        private bool _isImageAlbumTitleTextBoxEnabled = true;
        public bool IsImageAlbumTitleTextBoxEnabled
        {
            get
            {
                return _isImageAlbumTitleTextBoxEnabled;
            }
            set
            {
                SetProperty(ref _isImageAlbumTitleTextBoxEnabled, value, "IsImageAlbumTitleTextBoxEnabled");
            }
        }

        private bool _isAddtoImageAlbumButtonEnabled = true;
        public bool IsAddtoImageAlbumButtonEnabled
        {
            get
            {
                return _isAddtoImageAlbumButtonEnabled;
            }
            set
            {
                SetProperty(ref _isAddtoImageAlbumButtonEnabled, value, "IsAddtoImageAlbumButtonEnabled");
            }
        }

        private bool _isAudioAddHashTagWrapPanelEnabled = true;
        public bool IsAudioAddHashTagWrapPanelEnabled
        {
            get
            {
                return _isAudioAddHashTagWrapPanelEnabled;
            }
            set
            {
                SetProperty(ref _isAudioAddHashTagWrapPanelEnabled, value, "IsAudioAddHashTagWrapPanelEnabled");
            }
        }

        private bool _isVideoAddHashTagGridPanelEnabled = true;
        public bool IsVideoAddHashTagGridPanelEnabled
        {
            get
            {
                return _isVideoAddHashTagGridPanelEnabled;
            }
            set
            {
                SetProperty(ref _isVideoAddHashTagGridPanelEnabled, value, "IsVideoAddHashTagGridPanelEnabled");
            }
        }

        private bool _isSelectedImageAlbumCrossButtonEnabled = true;
        public bool IsSelectedImageAlbumCrossButtonEnabled
        {
            get
            {
                return _isSelectedImageAlbumCrossButtonEnabled;
            }
            set
            {
                SetProperty(ref _isSelectedImageAlbumCrossButtonEnabled, value, "IsSelectedImageAlbumCrossButtonEnabled");
            }
        }

        private bool _isSelectedAudioAlbumCrossButtonEnabled = true;
        public bool IsSelectedAudioAlbumCrossButtonEnabled
        {
            get
            {
                return _isSelectedAudioAlbumCrossButtonEnabled;
            }
            set
            {
                SetProperty(ref _isSelectedAudioAlbumCrossButtonEnabled, value, "IsSelectedAudioAlbumCrossButtonEnabled");
            }
        }

        private bool _isSelectedVideoAlbumCrossButtonEnabled = true;
        public bool IsSelectedVideoAlbumCrossButtonEnabled
        {
            get
            {
                return _isSelectedVideoAlbumCrossButtonEnabled;
            }
            set
            {
                SetProperty(ref _isSelectedVideoAlbumCrossButtonEnabled, value, "IsSelectedVideoAlbumCrossButtonEnabled");
            }
        }

        #endregion

        #region UI Text

        private string _locationTextBoxText;
        public string LocationTextBoxText
        {
            get
            {
                return _locationTextBoxText;
            }
            set
            {
                SetProperty(ref _locationTextBoxText, value, "LocationTextBoxText");
            }
        }

        private string _writeOnText;
        public string WriteOnText
        {
            get
            {
                return _writeOnText;
            }
            set
            {
                SetProperty(ref _writeOnText, value, "WriteOnText");
            }
        }

        private string _videoAlbumTitleTextBoxText = "";
        public string VideoAlbumTitleTextBoxText
        {
            get
            {
                return _videoAlbumTitleTextBoxText;
            }
            set
            {
                SetProperty(ref _videoAlbumTitleTextBoxText, value, "VideoAlbumTitleTextBoxText");
            }
        }

        private string _audioAlbumTitleTextBoxText = "";
        public string AudioAlbumTitleTextBoxText
        {
            get
            {
                return _audioAlbumTitleTextBoxText;
            }
            set
            {
                SetProperty(ref _audioAlbumTitleTextBoxText, value, "AudioAlbumTitleTextBoxText");
            }
        }

        private string _writePostText = "";
        public string WritePostText
        {
            get
            {
                return _writePostText;
            }
            set
            {
                SetProperty(ref _writePostText, value, "WritePostText");
            }
        }

        private string _validityToolTipText = "Post Validity Unlimited";
        public string ValidityToolTipText
        {
            get
            {
                return _validityToolTipText;
            }
            set
            {
                SetProperty(ref _validityToolTipText, value, "ValidityToolTipText");
            }
        }

        private string _imageAlbumTitleTextBoxText = "";
        public string ImageAlbumTitleTextBoxText
        {
            get
            {
                return _imageAlbumTitleTextBoxText;
            }
            set
            {
                SetProperty(ref _imageAlbumTitleTextBoxText, value, "ImageAlbumTitleTextBoxText");
            }
        }

        #endregion

        #region UI IsChecked

        private bool _updateLinkImage;
        public bool UpdateLinkImage
        {
            get
            {
                return _updateLinkImage;
            }
            set
            {
                SetProperty(ref _updateLinkImage, value, "UpdateLinkImage");
            }
        }


        #endregion

        #region ICommand Region

        private ICommand _optionsButtonClickCommand;
        public ICommand OptionsButtonClickCommand
        {
            get
            {
                _optionsButtonClickCommand = _optionsButtonClickCommand ?? new RelayCommand(param => onOptionsButtonClick(param));
                return _optionsButtonClickCommand;
            }
        }

        private ICommand _validityButtonClickCommand;
        public ICommand ValidityButtonClickCommand
        {
            get
            {
                _validityButtonClickCommand = _validityButtonClickCommand ?? new RelayCommand(param => onValidityButtonClick(param));
                return _validityButtonClickCommand;
            }
        }

        private ICommand _postButtonClickCommand;
        public ICommand PostButtonClickCommand
        {
            get
            {
                _postButtonClickCommand = _postButtonClickCommand ?? new RelayCommand(param => onPostButtonClick(param));
                return _postButtonClickCommand;
            }
        }

        private ICommand _uploadPhotoButtonClickCommand;
        public ICommand UploadPhotoButtonClickCommand
        {
            get
            {
                _uploadPhotoButtonClickCommand = _uploadPhotoButtonClickCommand ?? new RelayCommand(param => onUploadPhotoButtonClick(param));
                return _uploadPhotoButtonClickCommand;
            }
        }

        private ICommand _newStatusVideoButtonClickCommand;
        public ICommand NewStatusVideoButtonClickCommand
        {
            get
            {
                _newStatusVideoButtonClickCommand = _newStatusVideoButtonClickCommand ?? new RelayCommand(param => onNewStatusVideoButtonClick(param));
                return _newStatusVideoButtonClickCommand;
            }
        }

        private ICommand _takePhotoClickCommand;
        public ICommand TakePhotoClickCommand
        {
            get
            {
                _takePhotoClickCommand = _takePhotoClickCommand ?? new RelayCommand(param => onTakePhotoClick(param));
                return _takePhotoClickCommand;
            }
        }

        private ICommand _uploadFromComputerClickCommand;
        public ICommand UploadFromComputerClickCommand
        {
            get
            {
                _uploadFromComputerClickCommand = _uploadFromComputerClickCommand ?? new RelayCommand(param => onUploadFromComputerClick(param));
                return _uploadFromComputerClickCommand;
            }
        }

        private ICommand _chooseFromAlbumClickCommand;
        public ICommand ChooseFromAlbumClickCommand
        {
            get
            {
                _chooseFromAlbumClickCommand = _chooseFromAlbumClickCommand ?? new RelayCommand(param => onChooseFromAlbumClick(param));
                return _chooseFromAlbumClickCommand;
            }
        }

        private ICommand _newStatusEmoticonClickCommand;
        public ICommand NewStatusEmoticonClickCommand
        {
            get
            {
                _newStatusEmoticonClickCommand = _newStatusEmoticonClickCommand ?? new RelayCommand(param => onNewStatusEmoticonClick(param));
                return _newStatusEmoticonClickCommand;
            }
        }

        private ICommand _newStatusTagFriendClickCommand;
        public ICommand NewStatusTagFriendClickCommand
        {
            get
            {
                _newStatusTagFriendClickCommand = _newStatusTagFriendClickCommand ?? new RelayCommand(param => onNewsStatusTagFriendClick(param));
                return _newStatusTagFriendClickCommand;
            }
        }

        private ICommand _removeFriendTagClickCommand;
        public ICommand RemoveFriendTagClickCommand
        {
            get
            {
                _removeFriendTagClickCommand = _removeFriendTagClickCommand ?? new RelayCommand(param => onRemoveFriendTagClick(param));
                return _removeFriendTagClickCommand;
            }
        }

        private ICommand _newStatusLocationButtonClickCommand;
        public ICommand NewStatusLocationButtonClickCommand
        {
            get
            {
                _newStatusLocationButtonClickCommand = _newStatusLocationButtonClickCommand ?? new RelayCommand(param => onNewStatusLocationButtonClick(param));
                return _newStatusLocationButtonClickCommand;
            }
        }

        private ICommand _locationTextBoxLostFocusCommand;
        public ICommand LocationTextBoxLostFocusCommand
        {
            get
            {
                _locationTextBoxLostFocusCommand = _locationTextBoxLostFocusCommand ?? new RelayCommand(param => onLocationTextBoxLostFocus(param));
                return _locationTextBoxLostFocusCommand;
            }
        }

        private ICommand _locationTextBoxTextChangedCommand;
        public ICommand LocationTextBoxTextChangedCommand
        {
            get
            {
                _locationTextBoxTextChangedCommand = _locationTextBoxTextChangedCommand ?? new RelayCommand(param => onLocationTextBoxTextChanged(param));
                return _locationTextBoxTextChangedCommand;
            }
        }

        private ICommand _locationTextBoxPreviewKeyDownCommand;
        public ICommand LocationTextBoxPreviewKeyDownCommand
        {
            get
            {
                _locationTextBoxPreviewKeyDownCommand = _locationTextBoxPreviewKeyDownCommand ?? new RelayCommand(param => onLocationTextBoxPreviewKeyDown(param));
                return _locationTextBoxPreviewKeyDownCommand;
            }
        }

        private ICommand _textLocationCrossButtonClickCommand;
        public ICommand TextLocationCrossButtonClickCommand
        {
            get
            {
                _textLocationCrossButtonClickCommand = _textLocationCrossButtonClickCommand ?? new RelayCommand(param => onTextLocationCrossButtonClick(param));
                return _textLocationCrossButtonClickCommand;
            }
        }

        private ICommand _privacyButtonClickCommand;
        public ICommand PrivacyButtonClickCommand
        {
            get
            {
                _privacyButtonClickCommand = _privacyButtonClickCommand ?? new RelayCommand(param => onPrivacyButtonClick(param));
                return _privacyButtonClickCommand;
            }
        }

        private ICommand _previewCancelButtonClickCommand;
        public ICommand PreviewCancelButtonClickCommand
        {
            get
            {
                _previewCancelButtonClickCommand = _previewCancelButtonClickCommand ?? new RelayCommand(param => onPreviewCancelButtonClick(param));
                return _previewCancelButtonClickCommand;
            }
        }

        private ICommand _rtbNewStatusAreaTextChangedCommand;
        public ICommand RtbNewStatusAreaTextChangedCommand
        {
            get
            {
                _rtbNewStatusAreaTextChangedCommand = _rtbNewStatusAreaTextChangedCommand ?? new RelayCommand(param => onRtbNewStatusAreaTextChanged(param));
                return _rtbNewStatusAreaTextChangedCommand;
            }
        }

        private ICommand _rtbNewStatusAreaPreviewKeyDownCommand;
        public ICommand RtbNewStatusAreaPreviewKeyDownCommand
        {
            get
            {
                _rtbNewStatusAreaPreviewKeyDownCommand = _rtbNewStatusAreaPreviewKeyDownCommand ?? new RelayCommand(param => onRtbNewStatusAreaPreviewKeyDown(param));
                return _rtbNewStatusAreaPreviewKeyDownCommand;
            }
        }

        private ICommand _rtbNewStatusAreaPreviewMouseLeftButtonDownCommand;
        public ICommand RtbNewStatusAreaPreviewMouseLeftButtonDownCommand
        {
            get
            {
                _rtbNewStatusAreaPreviewMouseLeftButtonDownCommand = _rtbNewStatusAreaPreviewMouseLeftButtonDownCommand ?? new RelayCommand(param => onRtbNewStatusAreaPreviewMouseLeftButtonDown(param));
                return _rtbNewStatusAreaPreviewMouseLeftButtonDownCommand;
            }
        }

        private ICommand _newStatusMusicClickCommand;
        public ICommand NewStatusMusicClickCommand
        {
            get
            {
                _newStatusMusicClickCommand = _newStatusMusicClickCommand ?? new RelayCommand(param => onNewStatusMusicClick(param));
                return _newStatusMusicClickCommand;
            }
        }

        private ICommand _addtoAudioAlbumButtonClickCommand;
        public ICommand AddtoAudioAlbumButtonClickCommand
        {
            get
            {
                _addtoAudioAlbumButtonClickCommand = _addtoAudioAlbumButtonClickCommand ?? new RelayCommand(param => onAddtoAudioAlbumButtonClick(param));
                return _addtoAudioAlbumButtonClickCommand;

            }
        }

        private ICommand _borderMouseEnterEvent;
        public ICommand BorderMouseEnterEvent
        {
            get
            {
                _borderMouseEnterEvent = _borderMouseEnterEvent ?? new RelayCommand(param => onBorderMouseEnter(param));
                return _borderMouseEnterEvent;
            }
        }

        private ICommand _borderMouseLeaveEvent;
        public ICommand BorderMouseLeaveEvent
        {
            get
            {
                _borderMouseLeaveEvent = _borderMouseLeaveEvent ?? new RelayCommand(param => onBorderMouseLeave(param));
                return _borderMouseLeaveEvent;
            }
        }

        private ICommand _removeHashtagButtonClickCommand;
        public ICommand RemoveHashtagButtonClickCommand
        {
            get
            {
                _removeHashtagButtonClickCommand = _removeHashtagButtonClickCommand ?? new RelayCommand(param => onRemoveHashtagButtonClick(param));
                return _removeHashtagButtonClickCommand;
            }
        }

        private ICommand _audioHashTagWrapPanelMouseLeftButtonUpCommand;
        public ICommand AudioHashTagWrapPanelMouseLeftButtonUpCommand
        {
            get
            {
                _audioHashTagWrapPanelMouseLeftButtonUpCommand = _audioHashTagWrapPanelMouseLeftButtonUpCommand ?? new RelayCommand(param => onHashTagWrapPanelMouseLeftButtonUp(param));
                return _audioHashTagWrapPanelMouseLeftButtonUpCommand;
            }
        }

        private ICommand _audioAddHashTagTextBoxTextChangedEvent;
        public ICommand AudioAddHashTagTextBoxTextChangedEvent
        {
            get
            {
                _audioAddHashTagTextBoxTextChangedEvent = _audioAddHashTagTextBoxTextChangedEvent ?? new RelayCommand(param => onAudioAddHashTagTextBoxTextChanged(param));
                return _audioAddHashTagTextBoxTextChangedEvent;
            }
        }

        private ICommand _audioAddHashTagTextBoxPreviewKeyDownCommand;
        public ICommand AudioAddHashTagTextBoxPreviewKeyDownCommand
        {
            get
            {
                _audioAddHashTagTextBoxPreviewKeyDownCommand = _audioAddHashTagTextBoxPreviewKeyDownCommand ?? new RelayCommand(param => onAudioAddHashTagTextBoxPreviewKeyDown(param));
                return _audioAddHashTagTextBoxPreviewKeyDownCommand;
            }
        }

        private ICommand _videoHashTagWrapPanelMouseLeftButtonUpCommand;
        public ICommand VideoHashTagWrapPanelMouseLeftButtonUpCommand
        {
            get
            {
                _videoHashTagWrapPanelMouseLeftButtonUpCommand = _videoHashTagWrapPanelMouseLeftButtonUpCommand ?? new RelayCommand(param => onHashTagWrapPanelMouseLeftButtonUp(param));
                return _videoHashTagWrapPanelMouseLeftButtonUpCommand;
            }
        }

        private ICommand _addtoVideoAlbumButtonClickCommand;
        public ICommand AddtoVideoAlbumButtonClickCommand
        {
            get
            {
                _addtoVideoAlbumButtonClickCommand = _addtoVideoAlbumButtonClickCommand ?? new RelayCommand(param => onAddtoVideoAlbumButtonClick(param));
                return _addtoVideoAlbumButtonClickCommand;
            }
        }

        private ICommand _recordVideoClickCommand;
        public ICommand RecordVideoClickCommand
        {
            get
            {
                _recordVideoClickCommand = _recordVideoClickCommand ?? new RelayCommand(param => onRecordVideoClick(param));
                return _recordVideoClickCommand;
            }
        }

        private ICommand _uploadVideoFromComputerClickCommand;
        public ICommand UploadVideoFromComputerClickCommand
        {
            get
            {
                _uploadVideoFromComputerClickCommand = _uploadVideoFromComputerClickCommand ?? new RelayCommand(param => onUploadVideoFromComputerClick(param));
                return _uploadVideoFromComputerClickCommand;
            }
        }

        private ICommand _linkImageThumbnailCheckBoxCheckedCommand;
        public ICommand LinkImageThumbnailCheckBoxCheckedCommand
        {
            get
            {
                _linkImageThumbnailCheckBoxCheckedCommand = _linkImageThumbnailCheckBoxCheckedCommand ?? new RelayCommand(param => onLinkImageThumbnailCheckBoxChecked(param));
                return _linkImageThumbnailCheckBoxCheckedCommand;
            }
        }

        private ICommand _linkPreviewImagePreviousButtonClickCommand;
        public ICommand LinkPreviewImagePreviousButtonClickCommand
        {
            get
            {
                _linkPreviewImagePreviousButtonClickCommand = _linkPreviewImagePreviousButtonClickCommand ?? new RelayCommand(param => onLinkPreviewImagePreviousButtonClick(param));
                return _linkPreviewImagePreviousButtonClickCommand;
            }
        }

        private ICommand _linkPreviewImageNextButtonClickCommand;
        public ICommand LinkPreviewImageNextButtonClickCommand
        {
            get
            {
                _linkPreviewImageNextButtonClickCommand = _linkPreviewImageNextButtonClickCommand ?? new RelayCommand(param => onLinkPreviewImageNextButtonClick(param));
                return _linkPreviewImageNextButtonClickCommand;
            }
        }

        private ICommand _cancelLinkPreviewButtonClickCommand;
        public ICommand CancelLinkPreviewButtonClickCommand
        {
            get
            {
                _cancelLinkPreviewButtonClickCommand = _cancelLinkPreviewButtonClickCommand ?? new RelayCommand(param => onCancelLinkPreviewButtonClick(param));
                return _cancelLinkPreviewButtonClickCommand;
            }
        }

        private ICommand _feelingDoingCancelButtonClickCommand;
        public ICommand FeelingDoingCancelButtonClickCommand
        {
            get
            {
                _feelingDoingCancelButtonClickCommand = _feelingDoingCancelButtonClickCommand ?? new RelayCommand(param => onFeelingDoingCancelButtonClick(param));
                return _feelingDoingCancelButtonClickCommand;
            }
        }

        private ICommand _rtbNewStatusAreaDragOverEventCommand;
        public ICommand RtbNewStatusAreaDragOverEventCommand
        {
            get
            {
                _rtbNewStatusAreaDragOverEventCommand = _rtbNewStatusAreaDragOverEventCommand ?? new RelayCommand(param => onRtbNewStatusAreaDragOverEvent(param));
                return _rtbNewStatusAreaDragOverEventCommand;
            }
        }

        private ICommand _rtbNewStatusAreaDropEventCommand;
        public ICommand RtbNewStatusAreaDropEventCommand
        {
            get
            {
                _rtbNewStatusAreaDropEventCommand = _rtbNewStatusAreaDropEventCommand ?? new RelayCommand(param => onRtbNewStatusAreaDropEvent(param));
                return _rtbNewStatusAreaDropEventCommand;
            }
        }

        private ICommand _rtbNewStatusAreaPreviewDragLeaveEventCommand;
        private ICommand RtbNewStatusAreaPreviewDragLeaveEventCommand
        {
            get
            {
                _rtbNewStatusAreaPreviewDragLeaveEventCommand = _rtbNewStatusAreaPreviewDragLeaveEventCommand ?? new RelayCommand(param => onRtbNewStatusAreaPreviewDragLeaveEvent(param));
                return _rtbNewStatusAreaPreviewDragLeaveEventCommand;
            }
        }

        private ICommand _addtoImageAlbumButtonClickCommand;
        public ICommand AddtoImageAlbumButtonClickCommand
        {
            get
            {
                _addtoImageAlbumButtonClickCommand = _addtoImageAlbumButtonClickCommand ?? new RelayCommand(param => onAddtoImageAlbumButtonClick(param));
                return _addtoImageAlbumButtonClickCommand;
            }
        }

        private ICommand _selectedImageAlbumCrossButtonClickCommand;
        public ICommand SelectedImageAlbumCrossButtonClickCommand
        {
            get
            {
                _selectedImageAlbumCrossButtonClickCommand = _selectedImageAlbumCrossButtonClickCommand ?? new RelayCommand(param => onSelectedImageAlbumCrossButtonClick(param));
                return _selectedImageAlbumCrossButtonClickCommand;
            }
        }

        private ICommand _selectedAudioAlbumCrossButtonClickCommand;
        public ICommand SelectedAudioAlbumCrossButtonClickCommand
        {
            get
            {
                _selectedAudioAlbumCrossButtonClickCommand = _selectedAudioAlbumCrossButtonClickCommand ?? new RelayCommand(param => onSelectedAudioAlbumCrossButtonClick(param));
                return _selectedAudioAlbumCrossButtonClickCommand;
            }
        }

        private ICommand _selectedVideoAlbumCrossButtonClickCommand;
        public ICommand SelectedVideoAlbumCrossButtonClickCommand
        {
            get
            {
                _selectedVideoAlbumCrossButtonClickCommand = _selectedVideoAlbumCrossButtonClickCommand ?? new RelayCommand(param => onSelectedVideoAlbumCrossButtonClick(param));
                return _selectedVideoAlbumCrossButtonClickCommand;
            }
        }

        private ICommand _audioAlbumTitleTextBoxTextChangedEvent;
        public ICommand AudioAlbumTitleTextBoxTextChangedEvent
        {
            get
            {
                _audioAlbumTitleTextBoxTextChangedEvent = _audioAlbumTitleTextBoxTextChangedEvent ?? new RelayCommand(param => onAudioAlbumTitleTextBoxTextChanged(param));
                return _audioAlbumTitleTextBoxTextChangedEvent;
            }
        }

        private ICommand _imageAlbumTitleTextBoxTextChangedEvent;
        public ICommand ImageAlbumTitleTextBoxTextChangedEvent
        {
            get
            {
                _imageAlbumTitleTextBoxTextChangedEvent = _imageAlbumTitleTextBoxTextChangedEvent ?? new RelayCommand(param => onImageAlbumTitleTextBoxTextChanged(param));
                return _imageAlbumTitleTextBoxTextChangedEvent;
            }
        }

        private ICommand _videoAlbumTitleTextBoxTextChangedEvent;
        public ICommand VideoAlbumTitleTextBoxTextChangedEvent
        {
            get
            {
                _videoAlbumTitleTextBoxTextChangedEvent = _videoAlbumTitleTextBoxTextChangedEvent ?? new RelayCommand(param => onVideoAlbumTitleTextBoxTextChanged(param));
                return _videoAlbumTitleTextBoxTextChangedEvent;
            }
        }

        #endregion

        #region Location Popup

        private void locationPopupView()
        {
            if (LocationPopUp != null)
            {
                UCGuiRingID.Instance.MotherPanel.Children.Remove(LocationPopUp);
            }
            LocationPopUp = new UCLocationPopUp(UCGuiRingID.Instance.MotherPanel);

            LocationPopUp.OnRemovedUserControl += () =>
            {
                LocationPopUp = null;
            };
        }

        #endregion

        #region ICommand Executable Methods

        private void onOptionsButtonClick(object param)
        {
            if (param is Button)
            {
                Button validityButton = param as Button;
                FeedViewPopup = true;
                if (ucFeedOptionsPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucFeedOptionsPopup);
                }
                ucFeedOptionsPopup = new UCFeedOptionsPopup();
                ucFeedOptionsPopup.SetParent(UCGuiRingID.Instance.MotherPanel);
                ucFeedOptionsPopup.Show();
                ucFeedOptionsPopup.ShowPopup(validityButton, this);
                ucFeedOptionsPopup.OnRemovedUserControl += () =>
                {
                    FeedViewPopup = false;
                    ucFeedOptionsPopup = null;
                };
            }
        }

        private void onValidityButtonClick(object param)
        {
            if (param is Button)
            {
                Button validityButton = param as Button;

                if (ucNewStatusValidityPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNewStatusValidityPopup);
                }
                ucNewStatusValidityPopup = new UCNewStatusValidityPopup(UCGuiRingID.Instance.MotherPanel);
                ucNewStatusValidityPopup.Show();
                ucNewStatusValidityPopup.OnRemovedUserControl += () =>
                {
                    ucNewStatusValidityPopup = null;
                };

                if (MainSwitcher.PopupController.EditStatusCommentView != null && MainSwitcher.PopupController.EditStatusCommentView.IsVisible)
                {
                    ucNewStatusValidityPopup.ShowPopup(validityButton, this, MainSwitcher.PopupController.EditStatusCommentView.FeedModel.Validity);
                }
                else
                {
                    ucNewStatusValidityPopup.ShowPopup(validityButton, this);
                }
            }
        }

        private void onPostButtonClick(object param)
        {
            if (param is object[])
            {
                var multipleParameter = (object[])param;
                NewStatusView newStatusView = (NewStatusView)multipleParameter[0];
                RichTextFeedEdit RtbNewStatusArea = (RichTextFeedEdit)multipleParameter[1];
                richTextFeedEditFromViewModel = RtbNewStatusArea;
                SetFeedWallType(newStatusView);
                if (PrivacyValue != 5 && PrivacyValue != 10)
                {
                    FriendsToHideOrShow.Clear();
                }

                if (DefaultSettings.IsInternetAvailable)
                {
                    if (MainSwitcher.PopupController.EditStatusCommentView != null
                    && MainSwitcher.PopupController.EditStatusCommentView.IsVisible)
                    {
                        MainSwitcher.PopupController.EditStatusCommentView.ActionEditFeed();
                        return;
                    }
                    if (ucMediaCloudUploadPopup != null)
                    {
                        ucMediaCloudUploadPopup.LoadStatesInUI(2);
                    }
                    StatusText = RtbNewStatusArea.Text.Trim();
                    if (string.IsNullOrWhiteSpace(StatusText))
                    {
                        StatusText = "";
                    }
                    RtbNewStatusArea.SetStatusandTags();
                    if (RtbNewStatusArea.TagsJArray != null)
                    {
                        StatusTagsJArray = RtbNewStatusArea.TagsJArray;
                        StatusTextWithoutTags = RtbNewStatusArea.StringWithoutTags;
                    }
                    else
                    {
                        StatusTagsJArray = null;
                        StatusTextWithoutTags = StatusText;
                    }

                    if (FriendsToHideOrShow.Count > 0)
                    {
                        foreach (var friends in FriendsToHideOrShow)
                        {
                            FriendsToShowOrHideUtIds.Add(friends.ShortInfoModel.UserTableID);
                        }
                    }

                    if (NewStatusImageUpload.Count > 0)
                    {
                        bool tooLargeCaption = false;
                        foreach (ImageUploaderModel model in NewStatusImageUpload)
                        {
                            if (model.ImageCaption.Length > 250)
                            {
                                tooLargeCaption = true;
                                break;
                            }
                        }
                        if (tooLargeCaption)
                        {
                            UIHelperMethods.ShowInformation("An Image Caption can be maximum 250 Characters!", "Max characters!");
                        }
                        else
                        {
                            new ImageUploader(this);
                        }

                    }
                    else if (NewStatusMusicUpload.Count > 0)
                    {
                        bool isAllAudioHaveTitle = true;
                        foreach (MusicUploaderModel model in NewStatusMusicUpload)
                        {
                            if (string.IsNullOrEmpty(model.AudioTitle))
                            {
                                isAllAudioHaveTitle = false;
                                break;
                            }
                        }
                        if (!isAllAudioHaveTitle)
                        {
                            UIHelperMethods.ShowInformation("Please choose a name for every music!", "Need name!");
                            if (ucMediaCloudUploadPopup != null) ucMediaCloudUploadPopup.LoadStatesInUI(1);
                        }
                        else if (!isSelectedMediaFilesLessThan500MB(SettingsConstants.MEDIA_TYPE_AUDIO))
                        {
                            UIHelperMethods.ShowInformation("Maximum limit for uploading is 500MB!", "Max limit!");
                            if (ucMediaCloudUploadPopup != null)
                            {
                                ucMediaCloudUploadPopup.LoadStatesInUI(1);
                            }
                        }
                        else
                        {
                            new MusicUploader(this);
                        }
                    }
                    else if (NewStatusVideoUpload.Count > 0)
                    {
                        bool isAllVideoHaveTitle = true;
                        foreach (FeedVideoUploaderModel model in NewStatusVideoUpload)
                        {
                            if (string.IsNullOrEmpty(model.VideoTitle))
                            {
                                isAllVideoHaveTitle = false;
                                break;
                            }
                        }
                        if (!isAllVideoHaveTitle)
                        {
                            UIHelperMethods.ShowInformation("Please choose a name for every video!", "Need name!");
                            if (ucMediaCloudUploadPopup != null)
                            {
                                ucMediaCloudUploadPopup.LoadStatesInUI(1);
                            }
                        }
                        else if (!isSelectedMediaFilesLessThan500MB(SettingsConstants.MEDIA_TYPE_VIDEO))
                        {
                            UIHelperMethods.ShowInformation("Maximum limit for uploading is 500MB!", "Max limit!");
                            if (ucMediaCloudUploadPopup != null)
                            {
                                ucMediaCloudUploadPopup.LoadStatesInUI(1);
                            }
                        }
                        else
                        {
                            new VideoUploader(this);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(StatusText) && SelectedLocationModel == null && (TaggedFriends == null || TaggedFriends.Count < 1) && (SelectedDoingModel == null && SelectedDoingId == 0))
                        {
                            UIHelperMethods.ShowInformation(NotificationMessages.NOTHING_TO_POST, "Empty post!");
                        }
                        else
                        {
                            if ((InputUrl.Length == StatusText.Length) && InputUrl.Equals(StatusText))
                            {
                                StatusTextWithoutTags = "";
                            }
                            EnablePostbutton(false);
                            new ThreadAddFeed().StartThread(this);
                        }
                    }
                }
                else
                {
                    UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Post failed!");
                }
            }

        }

        private void onUploadPhotoButtonClick(object param)
        {
            if (param is NewStatusView)
            {
                NewStatusView newStatusView = (NewStatusView)param;
                SetFeedWallType(newStatusView);
            }
            if (LocationMainBorderPanelVisibility == Visibility.Visible)
            {
                LocationMainBorderPanelVisibility = Visibility.Collapsed;
            }
            if (FeelingDoingButtonContainerVisibility == Visibility.Visible)
            {
                FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
            }
            IsImageAlbumTitleTextBoxEnabled = true;
            AddToImageAlbumButtonVisibility = Visibility.Visible;
            SelectedImageAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsPrivacyButtonEnabled = true;
            IsPopupUploadPhotoOpen = true;
        }

        private void onNewStatusVideoButtonClick(object param)
        {
            LocationMainBorderPanelVisibility = Visibility.Collapsed;
            TagListHolderVisibility = Visibility.Collapsed;
            FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
            IsVideoAlbumTitleTextBoxEnabled = true;
            AddtoVideoAlbumButtonVisibility = Visibility.Visible;
            SelectedVideoAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsPopupUploadVideoOpen = true;
        }

        private void onTakePhotoClick(object param)
        {
            IsPopupUploadPhotoOpen = false;
            View.UI.WNWebcamCapture.Instance.ShowWindow((f) =>
            {
                try
                {
                    ImageUploaderModel model = new ImageUploaderModel();
                    model.FilePath = f;
                    NewStatusImageUpload.Add(model);
                    AudioAlbumTitleTextBoxText = "";
                    VideoAlbumTitleTextBoxText = "";
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        NewStatusMusicUpload.Clear();
                    }
                    if (NewStatusVideoUpload.Count > 0)
                    {
                        NewStatusVideoUpload.Clear();
                    }
                    if (NewStatusHashTag.Count > 0)
                    {
                        ClearNewStatusHashTagsExceptPlaceHolder();
                    }
                    if (NewStatusImageUpload.Count > 1 || ImageAlbumTitleTextBoxText.Trim().Length > 0)
                    {
                        if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                        {
                            PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                        }
                    }
                    UploadingText = "Selected Image(s): " + NewStatusImageUpload.Count;
                    this.StatusImagePanelVisibility = Visibility.Visible;
                    AudioPanelVisibility = Visibility.Collapsed;
                    VideoPanelVisibility = Visibility.Collapsed;
                    PreviewCancelButtonVisibility = Visibility.Visible;
                    ActionRemoveLinkPreview();
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            });
        }

        private void onUploadFromComputerClick(object param)
        {
            IsPopupUploadPhotoOpen = false;
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            op.Multiselect = true;
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if ((bool)op.ShowDialog())
            {
                foreach (string filename in op.FileNames)
                {
                    try
                    {
                        MediaFile mf = new MediaFile(filename);
                        if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit500MBinBytes)
                        { 
                            anyMorethan500MB = true; 
                            continue; 
                        }
                        int formatChecked = ImageUtility.GetFileImageTypeFromHeader(filename);
                        if (formatChecked != 1 && formatChecked != 2)
                        { 
                            anyCorrupted = true; continue; 
                        }
                        ImageUploaderModel model = new ImageUploaderModel();
                        model.FileSize = mf.size;
                        model.FilePath = filename;
                        NewStatusImageUpload.Add(model);
                        UploadingText = "Selected Image(s): " + NewStatusImageUpload.Count;
                    }
                    catch (Exception) { anyCorrupted = true; }
                }
                if (NewStatusImageUpload.Count > 0)
                {
                    AudioAlbumTitleTextBoxText = "";
                    VideoAlbumTitleTextBoxText = "";
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        NewStatusMusicUpload.Clear();
                    }
                    if (NewStatusVideoUpload.Count > 0)
                    {
                        NewStatusVideoUpload.Clear();
                    }
                    if (NewStatusHashTag.Count > 0)
                    {
                        ClearNewStatusHashTagsExceptPlaceHolder();
                    }
                    if (NewStatusImageUpload.Count > 1 || ImageAlbumTitleTextBoxText.Trim().Length > 0)
                    {
                        if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                        {
                            PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                        }
                    }
                    AudioPanelVisibility = Visibility.Collapsed;
                    VideoPanelVisibility = Visibility.Collapsed;
                    ActionRemoveLinkPreview();
                    StatusImagePanelVisibility = Visibility.Visible;
                    PreviewCancelButtonVisibility = Visibility.Visible;
                }
            }
            if (anyCorrupted)
            {
                UIHelperMethods.ShowWarning("One or more files are corrupted !");
            }
            if (anyMorethan500MB)
            {
                UIHelperMethods.ShowFailed("One or more files are greater than Maximum file Limit (500MB) !");
            }
        }

        private void onChooseFromAlbumClick(object param)
        {
            IsPopupUploadPhotoOpen = false;
            SetStatusImagePanelVisibility();
            MainSwitcher.PopupController.PhotoSelectionView.Show();
            MainSwitcher.PopupController.PhotoSelectionView.SetPhotoType(MiddlePanelConstants.TypeMyAlbum, this);
        }

        private void onNewStatusEmoticonClick(object param)
        {
            if (param is Button)
            {
                Button newStatusEmoticonButton = (Button)param;
                if (LocationMainBorderPanelVisibility == Visibility.Visible)
                {
                    LocationMainBorderPanelVisibility = Visibility.Collapsed;
                }
                if (TagListHolderVisibility == Visibility.Visible)
                {
                    TagListHolderVisibility = Visibility.Collapsed;
                }
                if (FeelingDoingButtonContainerVisibility == Visibility.Visible)
                {
                    FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
                }
                else
                {
                    if (SelectedDoingModel != null)
                    {
                        FeelingDoingButtonContainerVisibility = Visibility.Visible;
                    }
                }
                EmoPopup = true;
                if (ucNewStatusFeelingPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNewStatusFeelingPopup);
                }
                if (ucNewStatusFeelingPopup == null)
                {
                    ucNewStatusFeelingPopup = new UCNewStatusFeelingPopup(UCGuiRingID.Instance.MotherPanel);
                }
                ucNewStatusFeelingPopup.Show();
                ucNewStatusFeelingPopup.ShowFeelingPopup(newStatusEmoticonButton, this);
                ucNewStatusFeelingPopup.OnRemovedUserControl += () =>
                {
                    EmoPopup = false;
                };
                RingIDViewModel.Instance.LoadDoingModels();
            }
        }

        private void onNewsStatusTagFriendClick(object param)
        {
            if (param is Button)
            {
                Button NewStatusTagFriendButton = (Button)param;

                if (LocationMainBorderPanelVisibility == Visibility.Visible)
                {
                    LocationMainBorderPanelVisibility = Visibility.Collapsed;
                }
                if (FeelingDoingButtonContainerVisibility == Visibility.Visible)
                {
                    FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
                }
                if (TaggedFriends != null && TaggedFriends.Count > 0 && TagListHolderVisibility == Visibility.Collapsed)
                {
                    TagListHolderVisibility = Visibility.Visible;
                }
                else
                {
                    TagListHolderVisibility = Visibility.Collapsed;
                }
                TagPopup = true;
                if (ucNewStatusTagPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNewStatusTagPopup);
                }
                ucNewStatusTagPopup = new UCNewStatusTagPopup(UCGuiRingID.Instance.MotherPanel);
                ucNewStatusTagPopup.Show();
                ucNewStatusTagPopup.ShowTagPopup(NewStatusTagFriendButton, this);
                ucNewStatusTagPopup.OnRemovedUserControl += () =>
                {
                    ucNewStatusTagPopup = null;
                    TagPopup = false;
                };
            }
        }

        private void onRemoveFriendTagClick(object param)
        {
            if (param is Button)
            {
                Button button = (Button)param;
                UserShortInfoModel model = (UserShortInfoModel)button.DataContext;
                model.IsVisibleInTagList = false;
                TaggedFriends.Remove(model);
                OnPropertyChanged("TaggedFriends");
                if (TaggedUtids != null && TaggedUtids.Count > 0)
                {
                    TaggedUtids.Remove(model.UserTableID);
                }
                if (TaggedFriends.Count == 0 && TagListHolderVisibility == Visibility.Visible)
                {
                    TagListHolderVisibility = Visibility.Collapsed;
                }
            }
        }

        private void onNewStatusLocationButtonClick(object param)
        {
            if (param is TextBox)
            {
                locationTextBox = (TextBox)param;
                if (LocationMainBorderPanelVisibility == Visibility.Visible)
                {
                    LocationMainBorderPanelVisibility = Visibility.Collapsed;
                    locationTextBox.Text = String.Empty;
                }
                else
                {
                    LocationMainBorderPanelVisibility = Visibility.Visible;
                    if (SelectedLocationModel == null)
                    {
                        locationTextBox.Text = String.Empty;
                    }
                    else
                    {
                        locationTextBox.Text = SelectedLocationModel.LocationName;
                        IsLocationSet = true;
                    }
                    locationTextBox.Focus();
                    locationTextBox.SelectionStart = locationTextBox.Text.Length;
                }

                if (FeelingDoingButtonContainerVisibility == Visibility.Visible)
                {
                    FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
                }
                if (TagListHolderVisibility == Visibility.Visible)
                {
                    TagListHolderVisibility = Visibility.Collapsed;
                }
            }
        }

        private void onLocationTextBoxLostFocus(object param)
        {
            if (SelectedLocationModel != null)
            {
                IsLocationSet = true;
                LocationTextBoxText = SelectedLocationModel.LocationName;
            }
            if (LocationPopUp != null)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
            }
            if (timer != null && timer.IsEnabled)
            {
                timer.Stop();
            }
        }

        private void onLocationTextBoxTextChanged(object param)
        {
            try
            {
                locationTextBox = (TextBox)param;
                if (IsLocationSet && (!string.Equals(locationTextBox.Text, LocationTextBoxText)))
                {
                    IsLocationSet = false;
                }
                if (locationTextBox.Text.Trim().Length > 0)
                {
                    if (timer == null)
                    {
                        askSuggestions();
                        timer = new DispatcherTimer(DispatcherPriority.Background);
                        timer.Interval = TimeSpan.FromSeconds(1); //100ms
                        timer.Tick += lookUp;
                        timer.Start();
                    }
                    else if (!timer.IsEnabled)
                    {
                        timer.Start();
                    }
                }
                else if (locationTextBox.Text.Trim().Length == 0)
                {
                    if (LocationPopUp != null)
                    {
                        LocationPopUp.SelectedLocationModel = null;
                        LocationPopUp.popupLocation.IsOpen = false;
                    }
                    timer.Stop();
                }
            }
            catch (Exception ex) 
            { 
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); 
            }
        }

        private void onLocationTextBoxPreviewKeyDown(object param)
        {
            if (LocationPopUp != null && LocationPopUp.popupLocation.IsOpen == true)
            {
                LocationPopUp.popupLocation.StaysOpen = false;
                LocationPopUp.popupLocation.IsOpen = false;
            }
        }

        private void onTextLocationCrossButtonClick(object param)
        {
            SelectedLocationModel = null;
            locationTextBox.Text = "";
            IsLocationSet = false;
            locationTextBox.Focus();
            locationTextBox.SelectionStart = locationTextBox.Text.Length;
        }

        private void onPrivacyButtonClick(object param)
        {
            if (param is Button)
            {
                Button privacyButton = (Button)param;
                PrivacyPopup = true;
                if (ucNewStatusPrivacyPopup != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(ucNewStatusPrivacyPopup);
                }
                ucNewStatusPrivacyPopup = new UCNewStatusPrivacyPopup(UCGuiRingID.Instance.MotherPanel);
                ucNewStatusPrivacyPopup.Show();
                ucNewStatusPrivacyPopup.ShowPopUp(privacyButton, this);
                ucNewStatusPrivacyPopup.OnRemovedUserControl += () =>
                {
                    ucNewStatusPrivacyPopup = null;
                    PrivacyPopup = false;
                };
            }
        }

        private void onPreviewCancelButtonClick(object param)
        {
            if (MainSwitcher.PopupController.EditStatusCommentView != null
               && MainSwitcher.PopupController.EditStatusCommentView.IsVisible) MainSwitcher.PopupController.EditStatusCommentView.Hide();
            else
            {
                PreviewCancelButtonVisibility = Visibility.Collapsed;
                StatusImagePanelVisibility = Visibility.Collapsed;
                AudioPanelVisibility = Visibility.Collapsed;
                VideoPanelVisibility = Visibility.Collapsed;
                IsPrivacyButtonEnabled = true;
                AudioAlbumTitleTextBoxText = "";
                VideoAlbumTitleTextBoxText = "";
                ImageAlbumTitleTextBoxText = "";
                if (NewStatusImageUpload.Count > 0)
                {
                    NewStatusImageUpload.Clear();
                }
                if (NewStatusMusicUpload.Count > 0)
                {
                    NewStatusMusicUpload.Clear();
                }
                if (NewStatusVideoUpload.Count > 0)
                {
                    NewStatusVideoUpload.Clear();
                    if (UCVideoRecorderPreviewPanel.Instance != null)
                    {
                        UCVideoRecorderPreviewPanel.Instance.DeleteFilesInDirectory();
                        UCVideoRecorderPreviewPanel.Instance.CloseAll();
                        UCVideoRecorderPreviewPanel.Instance.Hide();
                    }
                }
                if (NewStatusHashTag.Count > 0)
                {
                    ClearNewStatusHashTagsExceptPlaceHolder();
                }
                HelperMethods.ClearBitMapImagesDictionary();
                ActionRemoveLinkPreview();
                if (ucMediaCloudUploadPopup != null)
                {
                    ucMediaCloudUploadPopup.LoadStatesInUI(0);
                }
            }
        }

        private void onRtbNewStatusAreaTextChanged(object param)
        {
            if (param is RichTextFeedEdit)
            {
                RichTextFeedEdit richTextFeedEditNewStatus = (RichTextFeedEdit)param;
                string statusText = richTextFeedEditNewStatus.Text;
                if (statusText.Length > 0)
                {

                    WriteSomethingTextBlockVisibility = Visibility.Collapsed;
                    if (statusText.EndsWith("@"))
                    {
                        richTextFeedEditNewStatus.AlphaStarts = statusText.Length;
                        if (UCAlphaTagPopUp.Instance == null)
                        {
                            UCAlphaTagPopUp.Instance = new UCAlphaTagPopUp();
                        }
                        if (UCAlphaTagPopUp.Instance.Parent != null && UCAlphaTagPopUp.Instance.Parent is Grid)
                        {
                            Grid g = (Grid)UCAlphaTagPopUp.Instance.Parent;
                            g.Children.Remove(UCAlphaTagPopUp.Instance);
                        }
                        Grid richGrid = (Grid)richTextFeedEditNewStatus.Parent;
                        richGrid.Children.Add(UCAlphaTagPopUp.Instance);
                        UCAlphaTagPopUp.Instance.InitializePopUpLocation(richTextFeedEditNewStatus);
                    }
                    else if (richTextFeedEditNewStatus.AlphaStarts >= 0 && richTextFeedEditNewStatus.Text.Length > richTextFeedEditNewStatus.AlphaStarts)
                    {
                        string searchString = richTextFeedEditNewStatus.Text.Substring(richTextFeedEditNewStatus.AlphaStarts);
                        UCAlphaTagPopUp.Instance.ViewSearchFriends(searchString);
                    }
                }
                else
                {
                    WriteSomethingTextBlockVisibility = Visibility.Visible;
                    richTextFeedEditNewStatus.AlphaStarts = -1;
                    richTextFeedEditNewStatus._AddedFriendsUtid.Clear();
                }
            }
        }

        private void onRtbNewStatusAreaPreviewKeyDown(object param)
        {
            if (param is KeyEventArgs)
            {
                KeyEventArgs pressedKeyEvent = (KeyEventArgs)param;
                RichTextFeedEdit richTextFeedEdit = (RichTextFeedEdit)pressedKeyEvent.KeyboardDevice.FocusedElement;
                if (pressedKeyEvent.Key == Key.Enter && (UCAlphaTagPopUp.Instance == null || (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen == false)))
                {
                    richTextFeedEdit.AppendText("\n");
                    pressedKeyEvent.Handled = true;
                }

                if (UCAlphaTagPopUp.Instance != null && UCAlphaTagPopUp.Instance.popupTag.IsOpen && UCAlphaTagPopUp.Instance.TagList.Items.Count > 0)
                {
                    if (pressedKeyEvent.Key == Key.Down)
                    {
                        pressedKeyEvent.Handled = true;
                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex < UCAlphaTagPopUp.Instance.TagList.Items.Count - 1)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex++;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = 0;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Up)
                    {
                        pressedKeyEvent.Handled = true;

                        if (UCAlphaTagPopUp.Instance.TagList.SelectedIndex == 0)
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex = UCAlphaTagPopUp.Instance.TagList.Items.Count - 1;
                        }
                        else
                        {
                            UCAlphaTagPopUp.Instance.TagList.SelectedIndex--;
                        }
                    }
                    if (pressedKeyEvent.Key == Key.Enter || pressedKeyEvent.Key == Key.Tab)
                    {
                        pressedKeyEvent.Handled = true;
                        UCAlphaTagPopUp.Instance.SelectFriendTag((UserBasicInfoModel)UCAlphaTagPopUp.Instance.TagList.SelectedItem);
                    }
                }

            }
        }

        private void onRtbNewStatusAreaPreviewMouseLeftButtonDown(object param)
        {
            if (LocationMainBorderPanelVisibility == Visibility.Visible)
            {
                LocationMainBorderPanelVisibility = Visibility.Collapsed;
            }
            if (TagListHolderVisibility == Visibility.Visible)
            {
                TagListHolderVisibility = Visibility.Collapsed;
            }
        }

        private void onNewStatusMusicClick(object param)
        {
            IsAudioAlbumTitleTextBoxEnabled = true;
            AddtoAudioAlbumButtonVisibility = Visibility.Visible;
            SelectedAudioAlbumCrossButtonVisibility = Visibility.Collapsed;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select a Music File";
            openFileDialog.Filter = "All supported Music files|*.mp3|MP3 files(*.mp3)|*.mp3";
            openFileDialog.Multiselect = true;
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length <= 5 && (openFileDialog.FileNames.Length + NewStatusMusicUpload.Count) <= 5) // When 5 audio select with cntrl--> openFileDialog.FileNames.Length <= 5
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mediaFile = new MediaFile(filename);
                            if (new FileInfo(filename).Length > DefaultSettings.MaxFileLimit500MBinBytes)
                            {
                                anyMorethan500MB = true;
                                continue;
                            }
                            if (mediaFile.Audio == null || mediaFile.Audio.Count == 0 || mediaFile.Video.Count > 0 || !mediaFile.format.Equals("MPEG Audio"))
                            {
                                anyCorrupted = true;
                                continue;
                            }
                            MusicUploaderModel model = new MusicUploaderModel();
                            model.FileSize = mediaFile.size;
                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                                if (file.Tag != null && file.Tag.Pictures.Any())
                                {
                                    if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null)
                                    {
                                        model.IsImageFound = true;
                                    }
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                    {
                                        model.AudioArtist = file.Tag.Performers[0];
                                    }
                                    else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                    {
                                        model.AudioArtist = file.Tag.AlbumArtists[0];
                                    }
                                    model.AudioTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception ex) 
                            {
                                log.Error("onNewStatusMusicClick() => Error Message : " + ex.Message + "Stack Trace : " + ex.StackTrace);
                            }
                            if (string.IsNullOrEmpty(model.AudioTitle))
                            {
                                model.AudioTitle = Path.GetFileNameWithoutExtension(filename);
                            }
                            model.AudioDuration = (long)(mediaFile.duration / 1000);
                            if (model.AudioDuration == 0)
                                model.AudioDuration = HelperMethods.MediaPlayerDuration(filename);
                            model.FilePath = filename;
                            NewStatusMusicUpload.Add(model);
                            UploadingText = "";
                        }
                        catch (Exception)
                        {
                            anyCorrupted = true;
                        }
                    }
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        VideoAlbumTitleTextBoxText = "";
                        ImageAlbumTitleTextBoxText = "";
                        if (NewStatusImageUpload.Count > 0)
                        {
                            NewStatusImageUpload.Clear();
                        }
                        if (NewStatusVideoUpload.Count > 0)
                        {
                            NewStatusVideoUpload.Clear();
                        }
                        if (NewStatusHashTag.Count > 0)
                        {
                            ClearNewStatusHashTagsExceptPlaceHolder();
                        }
                        if (NewStatusMusicUpload.Count > 1 || AudioAlbumTitleTextBoxText.Trim().Length > 0)
                        {
                            if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                            {
                                PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                            }
                        }
                        StatusImagePanelVisibility = Visibility.Collapsed;
                        VideoPanelVisibility = Visibility.Collapsed;
                        ActionRemoveLinkPreview();
                        AudioPanelVisibility = Visibility.Visible;
                        PreviewCancelButtonVisibility = Visibility.Visible;
                    }
                }
                else
                {
                    UIHelperMethods.ShowFailed("Can't upload more than 5 music at a time !");
                }
            }
            if (anyCorrupted)
            {
                UIHelperMethods.ShowWarning("One or more files are corrupted or invalid format!");
            }
            if (anyMorethan500MB)
            {
                UIHelperMethods.ShowFailed("One or more files are greater than Maximum file Limit (250MB) !", "Max media size");
            }
        }
        private void showMediaAlbumPopup(UIElement txtBox, int type)
        {
            if (UCMediaContentAlbumPopUp.Instance == null)
            {
                UCMediaContentAlbumPopUp.Instance = new UCMediaContentAlbumPopUp(UCGuiRingID.Instance.MotherPanel);
            }
            UCMediaContentAlbumPopUp.Instance.Show();
            UCMediaContentAlbumPopUp.Instance.ShowMediaPopup(txtBox, type, this);
            UCMediaContentAlbumPopUp.Instance.OnRemovedUserControl += () =>
            {
                UCMediaContentAlbumPopUp.Instance = null;
            };
        }
        private void onAddtoAudioAlbumButtonClick(object param)
        {
            if (param is TextBox)
            {
                TextBox audioAlbumTitleTextBox = (TextBox)param;

                if (DefaultSettings.MY_AUDIO_ALBUMS_COUNT == 0)
                {
                    UIHelperMethods.ShowFailed("You have No existing Audio Albums!", "No Albums!");
                }
                else if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                {
                    showMediaAlbumPopup(audioAlbumTitleTextBox, SettingsConstants.MEDIA_TYPE_AUDIO);
                }
                else if (DefaultSettings.IsInternetAvailable)
                {
                    showMediaAlbumPopup(audioAlbumTitleTextBox, SettingsConstants.MEDIA_TYPE_AUDIO);
                    SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_AUDIO, Guid.Empty);
                }
            }
        }

        private void onBorderMouseEnter(object param)
        {
            if (param is object[])
            {
                var wrapPanelsCommandParameters = (object[])param;
                WrapPanel audioAddHashTagWrapPanel = (WrapPanel)wrapPanelsCommandParameters[0];
                WrapPanel videoAddHashTagWrapPanel = (WrapPanel)wrapPanelsCommandParameters[1];
                if (AudioPanelVisibility == Visibility.Visible)
                {
                    audioAddHashTagWrapPanel.Cursor = Cursors.Arrow;
                }
                else if (VideoPanelVisibility == Visibility.Visible)
                {
                    videoAddHashTagWrapPanel.Cursor = Cursors.Arrow;
                }
            }
        }

        private void onBorderMouseLeave(object param)
        {
            if (param is object[])
            {
                var wrapPanelsCommandParameters = (object[])param;
                WrapPanel audioAddHashTagWrapPanel = (WrapPanel)wrapPanelsCommandParameters[0];
                WrapPanel videoAddHashTagWrapPanel = (WrapPanel)wrapPanelsCommandParameters[1];
                if (AudioPanelVisibility == Visibility.Visible)
                {
                    audioAddHashTagWrapPanel.Cursor = Cursors.IBeam;
                }
                else if (VideoPanelVisibility == Visibility.Visible)
                {
                    videoAddHashTagWrapPanel.Cursor = Cursors.IBeam;
                }
            }
        }

        private void onRemoveHashtagButtonClick(object param)
        {
            if (param is Control)
            {
                Control control = (Control)param;
                HashTagModel model = (HashTagModel)control.DataContext;
                NewStatusHashTag.Remove(model);
            }
        }

        private void onHashTagWrapPanelMouseLeftButtonUp(object param)
        {
            if (param is WrapPanel)
            {
                HashTagBoxEnabled = true;
                TextBox wrapPanelChildTextBox = HelperMethods.FindVisualChild<TextBox>((WrapPanel)param);

                if (wrapPanelChildTextBox != null)
                {
                    wrapPanelChildTextBox.Focusable = true;
                    wrapPanelChildTextBox.Focus();
                }
            }
        }

        private void onAudioAddHashTagTextBoxTextChanged(object param)
        {
            if (param is TextBox)
            {
                TextBox AudioAddHashTagTextBox = (TextBox)param;
                if (AudioAddHashTagTextBox.Text.Trim().Length > 0)
                {
                    if (NewStatusHashTag.Count > 5)
                    {
                        AudioAddHashTagTextBox.Text = string.Empty;
                    }
                    else if (AudioAddHashTagTextBox.Parent != null && AudioAddHashTagTextBox.Parent is Grid)
                    {
                        Grid AudioAddHashTagTextBoxParentGrid = (Grid)AudioAddHashTagTextBox.Parent;
                        HashTagPopUp.Show(this, AudioAddHashTagTextBox, AudioAddHashTagTextBoxParentGrid);
                    }
                }
                else
                {
                    HashTagPopUp.CloseHashTagPopUp();
                }
            }
        }

        private void onAudioAddHashTagTextBoxPreviewKeyDown(object param)
        {
            if (param is KeyEventArgs)
            {
                KeyEventArgs pressedKeyEvent = (KeyEventArgs)param;
                TextBox AudioAddHashTagTextBox = (TextBox)pressedKeyEvent.KeyboardDevice.FocusedElement;
                if (HashTagPopUp != null && HashTagPopUp.popupHashTag.IsOpen && HashTagPopUp.TagList.Items.Count > 0)
                {
                    if (pressedKeyEvent.Key == Key.Down)
                    {
                        pressedKeyEvent.Handled = true;
                        if (HashTagPopUp.TagList.SelectedIndex < HashTagPopUp.TagList.Items.Count - 1)
                        {
                            HashTagPopUp.TagList.SelectedIndex++;
                        }
                    }
                    else if (pressedKeyEvent.Key == Key.Up)
                    {
                        pressedKeyEvent.Handled = true;

                        if (HashTagPopUp.TagList.SelectedIndex > 0)
                        {
                            HashTagPopUp.TagList.SelectedIndex--;
                        }
                    }
                    else if (pressedKeyEvent.Key == Key.Enter || pressedKeyEvent.Key == Key.Tab)
                    {
                        pressedKeyEvent.Handled = true;

                        if (NewStatusHashTag.Count - 2 >= 0)
                        {
                            NewStatusHashTag.Insert(NewStatusHashTag.Count - 1, (HashTagModel)HashTagPopUp.TagList.SelectedItem);
                        }
                        else
                        {
                            NewStatusHashTag.Insert(0, (HashTagModel)HashTagPopUp.TagList.SelectedItem);
                        }
                        AudioAddHashTagTextBox.Text = string.Empty;
                        AudioAddHashTagTextBox.Focus();
                        HashTagPopUp.CloseHashTagPopUp();
                    }
                }
                if (pressedKeyEvent.Key == Key.Space)
                {
                    pressedKeyEvent.Handled = true;
                    string newTag = AudioAddHashTagTextBox.Text.Trim();
                    if (newTag.Length > 0 && (NewStatusHashTag.Count == 0 || !NewStatusHashTag.Any(P => P.HashTagSearchKey.Equals(newTag, StringComparison.InvariantCultureIgnoreCase))))
                    {
                        HashTagModel model = null;
                        if (HashTagPopUp.SearchHashTag.Count > 0)
                        {
                            model = HashTagPopUp.SearchHashTag.Where(P => P.HashTagSearchKey.Equals(newTag, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                            if (model != null)
                            {
                                model.HashTagSearchKey = newTag;
                            }
                        }
                        if (model == null && MediaDictionaries.Instance.HashTagSuggestions.Count > 0)
                        {
                            HashTagDTO dto = MediaDictionaries.Instance.HashTagSuggestions.Where(P => P.HashTagSearchKey.Equals(newTag, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                            if (dto != null)
                            {
                                model = new HashTagModel();
                                model.LoadData(dto);
                                model.HashTagSearchKey = newTag;
                            }
                        }
                        if (model == null)
                        {
                            HashTagDTO dto = new HashTagDTO { HashTagSearchKey = newTag, HashTagID = 0 };
                            if (!MediaDictionaries.Instance.HashTagSuggestions.Any(p => p.HashTagSearchKey.Equals(dto.HashTagSearchKey, StringComparison.InvariantCultureIgnoreCase)))
                            {
                                MediaDictionaries.Instance.HashTagSuggestions.Add(dto);
                            }
                            model = new HashTagModel 
                            { 
                                HashTagSearchKey = newTag, 
                                HashTagId = 0 
                            };
                        }
                        if (NewStatusHashTag.Count - 2 >= 0)
                        {
                            NewStatusHashTag.Insert(NewStatusHashTag.Count - 1, model);
                        }
                        else
                        {
                            NewStatusHashTag.Insert(0, model);
                        }
                        AudioAddHashTagTextBox.Text = string.Empty;
                        AudioAddHashTagTextBox.Focus();
                        HashTagPopUp.CloseHashTagPopUp();
                    }
                }
                if (pressedKeyEvent.Key == Key.Back && AudioAddHashTagTextBox.Text.Length == 0)
                {
                    if (NewStatusHashTag.Count - 2 >= 0)
                    {
                        NewStatusHashTag.RemoveAt(NewStatusHashTag.Count - 2);
                    }
                }
            }
        }

        private void onAddtoVideoAlbumButtonClick(object param)
        {
            if (param is TextBox)
            {
                TextBox videoAlbumTitleTextBox = (TextBox)param;
                if (DefaultSettings.MY_VIDEO_ALBUMS_COUNT == 0)
                {
                    UIHelperMethods.ShowFailed("You have No existing video albums!", "No albums!");
                }
                else if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                {
                    showMediaAlbumPopup(videoAlbumTitleTextBox, SettingsConstants.MEDIA_TYPE_VIDEO);
                }
                else if (DefaultSettings.IsInternetAvailable)
                {
                    showMediaAlbumPopup(videoAlbumTitleTextBox, SettingsConstants.MEDIA_TYPE_VIDEO);
                    SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_VIDEO, Guid.Empty);
                }
            }
        }

        private void onRecordVideoClick(object param)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (UCVideoRecorderPreviewPanel.Instance != null)
                {
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(UCVideoRecorderPreviewPanel.Instance);
                }
                UCVideoRecorderPreviewPanel.Instance = new UCVideoRecorderPreviewPanel(UCGuiRingID.Instance.MotherPanel);
                IsPopupUploadVideoOpen = false;
                UCVideoRecorderPreviewPanel.Instance.Show();
                UCVideoRecorderPreviewPanel.Instance.ShowRecorder();
                UCVideoRecorderPreviewPanel.Instance.OnRemovedUserControl += () =>
                {
                    UCVideoRecorderPreviewPanel.Instance = null;
                };
                UCVideoRecorderPreviewPanel.Instance.OnVideoAddedToUploadList += (fileName, duration) =>
                {
                    FeedVideoUploaderModel model = new FeedVideoUploaderModel 
                    { 
                        IsRecorded = true, 
                        FilePath = fileName, 
                        VideoDuration = duration, 
                        VideoTitle = Path.GetFileNameWithoutExtension(fileName) 
                    };
                    new SetArtImageFromVideo(model);
                    NewStatusVideoUpload.Add(model);
                    AudioAlbumTitleTextBoxText = "";
                    ImageAlbumTitleTextBoxText = "";
                    if (NewStatusImageUpload.Count > 0)
                    {
                        NewStatusImageUpload.Clear();
                    }
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        NewStatusMusicUpload.Clear();
                    }
                    if (NewStatusHashTag.Count > 0)
                    {
                        ClearNewStatusHashTagsExceptPlaceHolder();
                    }
                    if (NewStatusVideoUpload.Count > 1 || VideoAlbumTitleTextBoxText.Trim().Length > 0)
                    {
                        if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                        {
                            PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                        }
                    }
                    AudioPanelVisibility = Visibility.Collapsed;
                    StatusImagePanelVisibility = Visibility.Collapsed;
                    ActionRemoveLinkPreview();
                    VideoPanelVisibility = Visibility.Visible;
                    PreviewCancelButtonVisibility = Visibility.Visible;
                };
            });
        }

        private void onUploadVideoFromComputerClick(object param)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "All supported Video files|*.mp4|MP4 files(*.mp4)|*.mp4";
            bool anyCorrupted = false;
            if (openFileDialog.ShowDialog() == true)
            {
                if (openFileDialog.FileNames.Length <= 5 && (openFileDialog.FileNames.Length + NewStatusVideoUpload.Count) <= 5) // When 5 video select with cntrl--> openFileDialog.FileNames.Length <= 5
                {
                    foreach (string filename in openFileDialog.FileNames)
                    {
                        try
                        {
                            MediaFile mf = new MediaFile(filename);

                            if (mf.Video == null || mf.Video.Count == 0 || !mf.Video.ElementAt(0).Value.InternetMediaType.Contains("H264"))
                            {
                                anyCorrupted = true;
                                continue;
                            }

                            FeedVideoUploaderModel model = new FeedVideoUploaderModel();
                            model.FileSize = mf.size;
                            model.FilePath = filename;
                            NewStatusVideoUpload.Add(model);
                            TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                            if (file.Tag != null && file.Tag.Pictures.Any())
                            {
                                if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                {
                                    model.VideoArtist = file.Tag.Performers[0];
                                }
                                else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                {
                                    model.VideoArtist = file.Tag.AlbumArtists[0];
                                }
                                model.VideoTitle = file.Tag.Title;
                            }
                            UploadingText = "";
                            model.VideoTitle = Path.GetFileNameWithoutExtension(filename);
                            model.VideoDuration = (long)(mf.duration / 1000);
                            if (model.VideoDuration == 0)
                            {
                                model.VideoDuration = HelperMethods.MediaPlayerDuration(filename);
                            }
                            new SetArtImageFromVideo(model);//120*90

                        }
                        catch (Exception)
                        {
                            anyCorrupted = true;
                        }
                    }
                    if (NewStatusVideoUpload.Count > 0)
                    {
                        AudioAlbumTitleTextBoxText = "";
                        ImageAlbumTitleTextBoxText = "";
                        if (NewStatusImageUpload.Count > 0)
                        {
                            NewStatusImageUpload.Clear();
                        }
                        if (NewStatusMusicUpload.Count > 0)
                        {
                            NewStatusMusicUpload.Clear();
                        }
                        if (NewStatusHashTag.Count > 0)
                        {
                            ClearNewStatusHashTagsExceptPlaceHolder();
                        }
                        if (NewStatusVideoUpload.Count > 1 || VideoAlbumTitleTextBoxText.Trim().Length > 0)
                        {
                            if (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE)
                            {
                                PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                            }
                        }
                        StatusImagePanelVisibility = Visibility.Collapsed;
                        AudioPanelVisibility = Visibility.Collapsed;
                        ActionRemoveLinkPreview();
                        VideoPanelVisibility = Visibility.Visible;
                        PreviewCancelButtonVisibility = Visibility.Visible;
                    }
                }
                else
                {
                    UIHelperMethods.ShowFailed("Can't upload more than 5 videos at a time !");
                }
            }
            if (anyCorrupted)
            {
                UIHelperMethods.ShowWarning("One or more files are corrupted or Invalid format!");
            }
        }

        private void onLinkImageThumbnailCheckBoxChecked(object param)
        {
            PreviewImgUrl = "";
        }

        private void onLinkPreviewImagePreviousButtonClick(object param)
        {
            SelectedImageIndex--;
            if (SelectedImageIndex > -1)
            {
                PreviewImgUrl = AllImagesURLS.ElementAt(SelectedImageIndex);
            }
            if (SelectedImageIndex - 1 < 0)
            {
                IsLinkPreviewImagePreviousButtonEnabled = false;
            }
            IsLinkPreviewImageNextButtonEnabled = true;
        }

        private void onLinkPreviewImageNextButtonClick(object param)
        {
            SelectedImageIndex++;
            PreviewImgUrl = AllImagesURLS.ElementAt(SelectedImageIndex);
            if (SelectedImageIndex + 2 > AllImagesURLS.Count)
            {
                IsLinkPreviewImageNextButtonEnabled = false;
            }
            IsLinkPreviewImagePreviousButtonEnabled = true;
        }

        private void onCancelLinkPreviewButtonClick(object param)
        {
            ActionRemoveLinkPreview();
        }

        private void onRtbNewStatusAreaDragOverEvent(object param)
        {
            if (param is DragEventArgs)
            {
                DragEventArgs rtbNewStatusAreaDragOverEvent = (DragEventArgs)param;
                checkDragFiles(rtbNewStatusAreaDragOverEvent);
                if (FilesDragged.Count == 0)
                {
                    rtbNewStatusAreaDragOverEvent.Effects = DragDropEffects.None;
                }
                else if (!rtbNewStatusAreaDragOverEvent.Handled)
                {
                    IsDragShadeOn = true;
                    rtbNewStatusAreaDragOverEvent.Handled = true;
                }
            }
        }

        private void onRtbNewStatusAreaDropEvent(object param)
        {
            if (param is DragEventArgs)
            {
                DragEventArgs rtbNewStatusAreaDropEvent = (DragEventArgs)param;
                IsDragShadeOn = false;
                CheckDroppedFiles(rtbNewStatusAreaDropEvent);
            }
        }

        private void onRtbNewStatusAreaPreviewDragLeaveEvent(object param)
        {
            if (param is DragEventArgs)
            {
                DragEventArgs rtbNewStatusAreaPreviewDragLeaveEvent = (DragEventArgs)param;
                if (IsDragShadeOn)
                {
                    IsDragShadeOn = false;
                }
                rtbNewStatusAreaPreviewDragLeaveEvent.Handled = true;
            }
        }

        public void onRtbNewStatusAreaPreviewDragLeaveEvent(DragEventArgs e)
        {
            if (IsDragShadeOn)
            {
                IsDragShadeOn = false;
            }
            e.Handled = true;
        }

        private void onFeelingDoingCancelButtonClick(object param)
        {
            SelectedDoingModel = null;
            SelectedDoingId = 0;
            FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
        }

        private void onAddtoImageAlbumButtonClick(object param)
        {
            if (param is TextBox)
            {
                TextBox imageAlbumTitleTextBox = (TextBox)param;
                if (DefaultSettings.MY_IMAGE_ALBUMS_COUNT == 0)
                {
                    UIHelperMethods.ShowWarning("You have No existing Image Albums!", "No Albums!");
                }
                else if (RingIDViewModel.Instance.MyImageAlubms.Count == DefaultSettings.MY_IMAGE_ALBUMS_COUNT)
                {
                    if (UCImageAlbumsPopUp.Instance == null)
                    {
                        UCImageAlbumsPopUp.Instance = new UCImageAlbumsPopUp(UCGuiRingID.Instance.MotherPanel);
                    }
                    UCImageAlbumsPopUp.Instance.Show();
                    UCImageAlbumsPopUp.Instance.ShowImagePopup(imageAlbumTitleTextBox, this);
                    UCImageAlbumsPopUp.Instance.OnRemovedUserControl += () =>
                    {
                        UCImageAlbumsPopUp.Instance = null;
                    };
                }
                else if (DefaultSettings.IsInternetAvailable)
                {
                    SendDataToServer.ListOfImageAlbumsOfUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_IMAGE, Guid.Empty);
                    if (UCImageAlbumsPopUp.Instance == null)
                    {
                        UCImageAlbumsPopUp.Instance = new UCImageAlbumsPopUp(UCGuiRingID.Instance.MotherPanel);
                    }
                    UCImageAlbumsPopUp.Instance.Show();
                    UCImageAlbumsPopUp.Instance.ShowImagePopup(imageAlbumTitleTextBox, this);
                    UCImageAlbumsPopUp.Instance.OnRemovedUserControl += () =>
                    {
                        UCImageAlbumsPopUp.Instance = null;
                    };
                }
            }
        }

        private void onSelectedImageAlbumCrossButtonClick(object param)
        {
            ImageAlbumTitleTextBoxText = "";
            AddToImageAlbumButtonVisibility = Visibility.Visible;
            SelectedImageAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsImageAlbumTitleTextBoxEnabled = true;
            IsPrivacyButtonEnabled = true;
            IsImageAlbumSelectedFromExisting = false;
        }

        private void onSelectedAudioAlbumCrossButtonClick(object param)
        {
            AudioAlbumTitleTextBoxText = "";
            AddtoAudioAlbumButtonVisibility = Visibility.Visible;
            SelectedAudioAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsAudioAlbumTitleTextBoxEnabled = true;
            IsPrivacyButtonEnabled = true;
            IsAudioAlbumSelectedFromExisting = false;
        }

        private void onSelectedVideoAlbumCrossButtonClick(object param)
        {
            VideoAlbumTitleTextBoxText = "";
            AddtoVideoAlbumButtonVisibility = Visibility.Visible;
            SelectedVideoAlbumCrossButtonVisibility = Visibility.Collapsed;
            IsVideoAlbumTitleTextBoxEnabled = true;
            IsPrivacyButtonEnabled = true;
            IsVideoAlbumSelectedFromExisting = false;
        }

        private void onAudioAlbumTitleTextBoxTextChanged(object param)
        {
            if (param is TextBox)
            {
                setPrivacyForAlbumTitleChange((TextBox)param);
            }
        }

        private void onImageAlbumTitleTextBoxTextChanged(object param)
        {
            if (param is TextBox)
            {
                setPrivacyForAlbumTitleChange((TextBox)param);
            }
        }

        private void onVideoAlbumTitleTextBoxTextChanged(object param)
        {
            if (param is TextBox)
            {
                setPrivacyForAlbumTitleChange((TextBox)param);
            }
        }

        #endregion

        #region Private Methods

        private void lookUp(object sender, EventArgs e)
        {
            Dispatcher.Invoke(askSuggestions);
        }

        private void askSuggestions()
        {
            if (!locationTextBox.Text.Equals(LastMapSearchText))
            {
                LastMapSearchText = locationTextBox.Text;
                string address = LastMapSearchText.Trim();
                try
                {
                    if (IsLocationSet)
                    {
                        return;
                    }
                    if (LocationPopUp == null)
                    {
                        locationPopupView();
                    }
                    if (NewsFeedDictionaries.Instance.MAP_SEARCH_HISTORY.ContainsKey(address))
                    {
                        LocationPopUp.popupLocation.StaysOpen = true;
                        LocationPopUp.Show();
                        LocationPopUp.ShowLocationPopup(locationTextBox, this);
                        LocationPopUp.LoadLocationsFromHistory(address);
                    }
                    else
                    {
                        if (LocationPopUp != null)
                        {
                            if (LocationPopUp.LoadNewLocationFromMap(address))
                            {
                                LocationPopUp.popupLocation.StaysOpen = true;
                                LocationPopUp.Show();
                                LocationPopUp.ShowLocationPopup(locationTextBox, this);
                            }
                            else
                            {
                                LocationPopUp.popupLocation.StaysOpen = false;
                                LocationPopUp.popupLocation.IsOpen = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message + "\n" + ex.StackTrace);
                }
            }
            locationTextBox.Focus();
            locationTextBox.SelectionStart = locationTextBox.Text.Length;
        }

        private bool isSelectedMediaFilesLessThan500MB(int mediaType)
        {
            long count = 0;
            if (mediaType == 1)
            {
                foreach (var item in NewStatusMusicUpload)
                {
                    count += new FileInfo(item.FilePath).Length;
                }
                return (count < DefaultSettings.MaxFileLimit500MBinBytes);
            }
            else if (mediaType == 2)
            {
                foreach (var item in NewStatusVideoUpload)
                {
                    if(item.IsRecorded)
                    {
                        continue;
                    }
                    count += new FileInfo(item.FilePath).Length;
                }
                return (count < DefaultSettings.MaxFileLimit500MBinBytes);
            }
            return false;
        }

        private void checkDragFiles(DragEventArgs e)
        {
            try
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
                {
                    FilesDragged.Clear();
                    string[] fileNames = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                    if (fileNames != null && fileNames.Length > 0)
                    {
                        foreach (string filename in fileNames)
                        {
                            try
                            {
                                if (new FileInfo(filename).Length < DefaultSettings.MaxFileLimit250MBinBytes) FilesDragged.Add(filename);
                            }
                            catch (Exception ex) 
                            {
                                log.Error("File Check => Error Message : " + ex.Message + "Stack Trace : " + ex.StackTrace);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: CheckDragFiles() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void setPrivacyForAlbumTitleChange(TextBox albumTextBox)
        {
            if (albumTextBox.Text.Trim().Length > 0 && (PrivacyValue == AppConstants.PRIVACY_FRIENDSTOHIDE || PrivacyValue == AppConstants.PRIVACY_FRIENDSTOSHOW))
            {
                PrivacyValue = AppConstants.PRIVACY_PUBLIC;
            }
        }

        #endregion

        #region Public Methods

        public void SetSelectedModelValuesForValidityPopup(bool isFromEditPopup = false, int validityValue = AppConstants.DEFAULT_VALIDITY)
        {
            ValidityValue = isFromEditPopup ? validityValue : SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY;

            String commonPartOfValidationMessage = "Post Validity";
            if (ValidityValue == AppConstants.DEFAULT_VALIDITY)
            {
                ValidityToolTipText = commonPartOfValidationMessage + " Unlimited";
            }
            else if (ValidityValue == AppConstants.VALIDITY_ONE_DAY)
            {
                ValidityToolTipText = commonPartOfValidationMessage + " " + ValidityValue + " Day";
            }
            else
            {
                ValidityToolTipText = commonPartOfValidationMessage + " " + ValidityValue + " Days";
            }
        }

        public void ActionRemoveLinkPreview()
        {
            PreviewDomain = "";
            PreviewDesc = "";
            PreviewUrl = "";
            InputUrl = "";
            PreviewImgUrl = "";
            PreviewTitle = "";
            PreviewLnkType = 0;
            UpdateLinkImage = false;
            AllImagesURLS.Clear();
            SelectedImageIndex = 0;
            PrevNextVisibility = Visibility.Collapsed;
        }

        public void ClearNewStatusHashTagsExceptPlaceHolder()
        {
            NewStatusHashTag.Clear();
            NewStatusHashTag.Add(PlaceHolderHashTagModel);
        }

        public void SetStatusImagePanelVisibility()
        {
            try
            {
                if (NewStatusImageUpload.Count > 0)
                {

                    AudioAlbumTitleTextBoxText = "";
                    VideoAlbumTitleTextBoxText = "";
                    if (NewStatusMusicUpload.Count > 0)
                    {
                        NewStatusMusicUpload.Clear();
                    }
                    if (NewStatusVideoUpload.Count > 0)
                    {
                        NewStatusVideoUpload.Clear();
                    }
                    if (NewStatusHashTag.Count > 0)
                    {
                        ClearNewStatusHashTagsExceptPlaceHolder();
                    }
                    StatusImagePanelVisibility = Visibility.Visible;
                    AudioPanelVisibility = Visibility.Collapsed;
                    VideoPanelVisibility = Visibility.Collapsed;
                    PreviewCancelButtonVisibility = Visibility.Visible;
                    ActionRemoveLinkPreview();
                }
                else
                {
                    StatusImagePanelVisibility = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void SetPostLoaderandBtn(bool flag)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (!flag)
                {
                    PostLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
                }
                else
                {
                    GC.SuppressFinalize(PostLoader);
                    PostLoader = null;
                }
                IsPostButtonEnabled = flag;
            }));
        }

        public void EnablePostbutton(bool flag)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                try
                {
                    if (flag != prevState)
                    {
                        prevState = flag;
                        if (!flag)
                        {
                            PostLoader = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
                        }
                        else
                        {
                            GC.SuppressFinalize(PostLoader);
                            PostLoader = null;
                        }
                        IsRtbNewStatusAreaEnabled = flag;
                        IsPostButtonEnabled = flag;
                        IsPreviewCancelButtonEnabled = flag;
                        if (NewStatusImageUpload.Count > 0 || NewStatusVideoUpload.Count > 0 || NewStatusMusicUpload.Count > 0)
                        {
                            PreviewCancelButtonVisibility = Visibility.Visible;
                        }
                        else
                        {
                            PreviewCancelButtonVisibility = Visibility.Collapsed;
                        }
                        IsAudioAlbumTitleTextBoxEnabled = flag;
                        IsVideoAlbumTitleTextBoxEnabled = flag;
                        IsImageAlbumTitleTextBoxEnabled = flag;
                        HashTagBoxEnabled = flag;
                        IsAddtoAudioAlbumButtonEnabled = flag;
                        IsAddtoVideoAlbumButtonEnabled = flag;
                        IsAddtoImageAlbumButtonEnabled = flag;
                        IsUploadPhotoButtonEnabled = flag;
                        IsNewStatusEmoticonEnabled = flag;
                        IsNewStatusTagFriendEnabled = flag;
                        IsNewStatusMusicEnabled = flag;
                        IsNewStatusVideoButtonEnabled = flag;
                        IsNewStatusLocationButtonEnabled = flag;
                        IsValidityButtonEnabled = flag;
                        IsAudioAddHashTagWrapPanelEnabled = flag;
                        IsVideoAddHashTagGridPanelEnabled = flag;
                        IsPrivacyButtonEnabled = flag;
                        IsSelectedImageAlbumCrossButtonEnabled = flag;
                        IsSelectedAudioAlbumCrossButtonEnabled = flag;
                        IsSelectedVideoAlbumCrossButtonEnabled = flag;
                        if (NewStatusImageUpload.Count > 0)
                        {
                            foreach (var item in NewStatusImageUpload)
                            {
                                item.CaptionEnabled = flag;
                                item.RemoveEnabled = flag;
                                if (flag)
                                { 
                                    item.IsUploadedInImageServer = false; 
                                    UploadingText = "Selected Image(s): " + NewStatusImageUpload.Count; 
                                }
                            }
                        }
                            
                        if (NewStatusMusicUpload.Count > 0)
                        {
                            foreach (var item in NewStatusMusicUpload)
                            {
                                item.TitleEnabled = flag;
                                if (flag)
                                {
                                    item.IsUploadedInAudioServer = false;
                                }
                            }
                        }

                        if (NewStatusVideoUpload.Count > 0)
                        {
                            foreach (var item in NewStatusVideoUpload)
                            {
                                item.TitleEnabled = flag;
                                if (flag)
                                {
                                    item.IsUploadedInVideoServer = false;
                                }
                            }
                        }
                            
                        if (NewStatusHashTag.Count > 0)
                        {
                            foreach (var item in NewStatusHashTag)
                            {
                                item.RemoveEnabled = flag;
                            }
                        }
                            
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }));
        }

        public void ActionRemoveLocationFeelingTagStatusImagesMedia()
        {
            SelectedLocationModel = null;
            if (TaggedUtids != null)
            {
                TaggedUtids.Clear();
            }
            if (TaggedFriends != null)
            {
                foreach (var item in TaggedFriends)
                {
                    item.IsVisibleInTagList = false;
                }
                TaggedFriends.Clear();
            }
            LocationTextBoxText = "";
            SelectedDoingModel = null;
            SelectedDoingId = 0;
            richTextFeedEditFromViewModel.Document.Blocks.Clear();
            StatusText = "";
            StatusTextWithoutTags = "";
            StatusTagsJArray = null;
            AudioAlbumTitleTextBoxText = "";
            VideoAlbumTitleTextBoxText = "";
            ImageAlbumTitleTextBoxText = "";
            if (NewStatusImageUpload.Count > 0)
            {
                NewStatusImageUpload.Clear();
            }
            if (NewStatusMusicUpload.Count > 0)
            {
                NewStatusMusicUpload.Clear();
            }
            if (NewStatusVideoUpload.Count > 0)
            {
                NewStatusVideoUpload.Clear();
            }
            if (NewStatusHashTag.Count > 0)
            {
                ClearNewStatusHashTagsExceptPlaceHolder();
            }
            TagListHolderVisibility = Visibility.Collapsed;
            StatusImagePanelVisibility = Visibility.Collapsed;
            VideoPanelVisibility = Visibility.Collapsed;
            AudioPanelVisibility = Visibility.Collapsed;
            PreviewCancelButtonVisibility = Visibility.Collapsed;
            LocationMainBorderPanelVisibility = Visibility.Collapsed;
            FeelingDoingButtonContainerVisibility = Visibility.Collapsed;
        }

        #endregion

        #region Private Methods

        private void SetFeedWallType(NewStatusView newStatusView)
        {
            if (newStatusView.Tag != null)
            {
                if (newStatusView.Tag is string)
                {
                    FeedWallType = Convert.ToInt32(newStatusView.Tag);
                    FriendUtIdOrCircleId = 0;
                }
                else
                {
                    dynamic obj = (ExpandoObject)newStatusView.Tag;
                    FeedWallType = (int)obj.type;
                    FriendUtIdOrCircleId = (long)obj.id;
                }
            }
        }

        private void CheckDroppedFiles(DragEventArgs e)
        {
            if (FilesDragged.Count > 0)
            {
                int currentFiletype = CurrentNewStatusFileType();
                foreach (string filename in FilesDragged)
                {
                    try
                    {
                        int currentAddedFiletype = CurrentAddedFileType(filename);
                        if (currentAddedFiletype == 0)
                        {
                            continue;
                        }
                        if (currentFiletype == 0)
                        {
                            currentFiletype = currentAddedFiletype;
                        }
                        else if (currentFiletype != currentAddedFiletype)
                        {
                            continue;
                        }
                        if (currentFiletype == 1)
                        {
                            ImageUploaderModel model = new ImageUploaderModel();
                            model.FilePath = filename;
                            NewStatusImageUpload.Add(model);
                            UploadingText = "Selected Image(s): " + NewStatusImageUpload.Count;
                            AudioAlbumTitleTextBoxText = "";
                            VideoAlbumTitleTextBoxText = "";
                            if (NewStatusMusicUpload.Count > 0)
                            {
                                NewStatusMusicUpload.Clear();
                            }
                            if (NewStatusVideoUpload.Count > 0)
                            {
                                NewStatusVideoUpload.Clear();
                            }
                            if (NewStatusHashTag.Count > 0)
                            {
                                ClearNewStatusHashTagsExceptPlaceHolder();
                            }
                            AudioPanelVisibility = Visibility.Collapsed;
                            VideoPanelVisibility = Visibility.Collapsed;
                            ActionRemoveLinkPreview();
                            StatusImagePanelVisibility = Visibility.Visible;
                            PreviewCancelButtonVisibility = Visibility.Visible;
                        }
                        else if (currentFiletype == 3 && NewStatusVideoUpload.Count < 5)
                        {
                            FeedVideoUploaderModel model = new FeedVideoUploaderModel();
                            model.FilePath = filename;
                            NewStatusVideoUpload.Add(model);
                            UploadingText = "";
                            model.VideoTitle = Path.GetFileNameWithoutExtension(filename);
                            model.VideoDuration = (long)(new MediaFile(filename).duration / 1000);
                            if (model.VideoDuration == 0)
                            {
                                model.VideoDuration = HelperMethods.MediaPlayerDuration(filename);
                            }
                            new SetArtImageFromVideo(model);//120*90
                            AudioAlbumTitleTextBoxText = "";
                            ImageAlbumTitleTextBoxText = "";
                            if (NewStatusImageUpload.Count > 0)
                            {
                                NewStatusImageUpload.Clear();
                            }
                            if (NewStatusMusicUpload.Count > 0)
                            {
                                NewStatusMusicUpload.Clear();
                            }
                            if (NewStatusHashTag.Count > 0)
                            {
                                ClearNewStatusHashTagsExceptPlaceHolder();
                            }
                            StatusImagePanelVisibility = Visibility.Collapsed;
                            AudioPanelVisibility = Visibility.Collapsed;
                            ActionRemoveLinkPreview();
                            VideoPanelVisibility = Visibility.Visible;
                            PreviewCancelButtonVisibility = Visibility.Visible;
                        }
                        else if (currentFiletype == 2 && NewStatusMusicUpload.Count < 5)
                        {
                            MusicUploaderModel model = new MusicUploaderModel();
                            try
                            {
                                TagLib.File file = TagLib.File.Create(filename); //FilePath is the audio file location
                                if (file.Tag != null && file.Tag.Pictures.Any())
                                {
                                    if (file.Tag.Pictures[0].Data != null && file.Tag.Pictures[0].Data.Data != null)
                                    {
                                        model.IsImageFound = true;
                                    }
                                    if (file.Tag.Performers != null && file.Tag.Performers.Length > 0 && !string.IsNullOrEmpty(file.Tag.Performers[0]))
                                    {
                                        model.AudioArtist = file.Tag.Performers[0];
                                    }
                                    else if (file.Tag.AlbumArtists != null && file.Tag.AlbumArtists.Length > 0 && !string.IsNullOrEmpty(file.Tag.AlbumArtists[0]))
                                    {
                                        model.AudioArtist = file.Tag.AlbumArtists[0];
                                    }
                                    model.AudioTitle = file.Tag.Title;
                                }
                            }
                            catch (Exception ex) 
                            {
                                log.Error("Error Message : " + ex.Message + "Stack Trace : " + ex.StackTrace);
                            }
                            if (string.IsNullOrEmpty(model.AudioTitle))
                            {
                                model.AudioTitle = Path.GetFileNameWithoutExtension(filename);
                            }
                            model.AudioDuration = (long)(new MediaFile(filename).duration / 1000);
                            if (model.AudioDuration == 0)
                            {
                                model.AudioDuration = HelperMethods.MediaPlayerDuration(filename);
                            }
                            model.FilePath = filename;
                            NewStatusMusicUpload.Add(model);
                            UploadingText = "";
                            VideoAlbumTitleTextBoxText = "";
                            ImageAlbumTitleTextBoxText = "";
                            if (NewStatusImageUpload.Count > 0)
                            {
                                NewStatusImageUpload.Clear();
                            }
                            if (NewStatusVideoUpload.Count > 0)
                            {
                                NewStatusVideoUpload.Clear();
                            }
                            if (NewStatusHashTag.Count > 0)
                            {
                                ClearNewStatusHashTagsExceptPlaceHolder();
                            }
                            StatusImagePanelVisibility = Visibility.Collapsed;
                            VideoPanelVisibility = Visibility.Collapsed;
                            ActionRemoveLinkPreview();
                            AudioPanelVisibility = Visibility.Visible;
                            PreviewCancelButtonVisibility = Visibility.Visible;
                        }
                    }
                    catch (Exception) { }
                }
                FilesDragged.Clear();
            }
        }

        private int CurrentNewStatusFileType()
        {
            if (NewStatusImageUpload.Count > 0)
            {
                return 1;
            }
            else if (NewStatusMusicUpload.Count > 0)
            {
                return 2;
            }
            else if (NewStatusVideoUpload.Count > 0)
            {
                return 3;
            }
            return 0;
        }

        private int CurrentAddedFileType(string filename)
        {
            try
            {
                int formatChecked = ImageUtility.GetFileImageTypeFromHeader(filename);
                if (formatChecked == 1 || formatChecked == 2)
                {
                    return 1;
                }
                MediaFile mediaFile = new MediaFile(filename);
                if (mediaFile.Audio != null && mediaFile.Audio.Count > 0 && (mediaFile.Video == null || mediaFile.Video.Count == 0) && mediaFile.format.Equals("MPEG Audio"))
                {
                    return 2;
                }
                else if (mediaFile.Video != null && mediaFile.Video.Count > 0 && mediaFile.Video.ElementAt(0).Value.InternetMediaType.Contains("H264"))
                {
                    return 3;
                }
            }
            catch (Exception ex) 
            {
                log.Error(" Error message : " + ex.Message + "Stack Trace : " + ex.StackTrace);
            }
            return 0;
        }

        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using View.BindingModels;
using View.Constants;
//using View.UI.ImageViews;
using View.UI.PopUp;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;
using System.Linq;
using System.Collections.ObjectModel;
using View.UI;

namespace View.ViewModel
{
    public class CommentViewModel : INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(NewsFeedViewModel).Name);

        private UCEditHistoryPopupStyle ucEditHistoryPopupStyle = null;

        public static CommentViewModel Instance = new CommentViewModel();

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region "Properties"
        private UCLikeList LikeListView { get; set; }
        #endregion
        public void ShowLikeAnimation(CommentModel commentModel)
        {
            if (commentModel.PopupLikeLoader == null)
            {
                commentModel.PopupLikeLoader = ImageLocation.LOADER_LIKE_ANIMATION;
                commentModel.PopupLikeLoaderVisibility = Visibility.Visible;

                new System.Threading.Thread(delegate()
                {
                    Thread.Sleep(2000);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        commentModel.PopupLikeLoader = null;
                        commentModel.PopupLikeLoaderVisibility = Visibility.Collapsed;
                    });
                }).Start();

            }
            else
            {
                commentModel.PopupLikeLoader = null;
                commentModel.PopupLikeLoaderVisibility = Visibility.Collapsed;
            }
        }
        //private ICommand _NameCommand;
        //public ICommand NameCommand
        //{
        //    get
        //    {
        //        if (_NameCommand == null)
        //        {
        //            _NameCommand = new RelayCommand(param => OnNameCommand(param));
        //        }
        //        return _NameCommand;
        //    }
        //}
        private ICommand _likeCommand;
        public ICommand LikeCommand
        {
            get
            {
                if (_likeCommand == null)
                {
                    _likeCommand = new RelayCommand(param => OnLikeCommand(param));
                }
                return _likeCommand;
            }
        }

        private ICommand _noOfLikeCommand;
        public ICommand NoOfLikeCommand
        {
            get
            {
                if (_noOfLikeCommand == null)
                {
                    _noOfLikeCommand = new RelayCommand(param => OnNumberOfLikeCommand(param));
                }
                return _noOfLikeCommand;
            }
        }

        private ICommand _ImageOrMediaClickCommand;
        public ICommand ImageOrMediaClickCommand
        {
            get
            {
                if (_ImageOrMediaClickCommand == null)
                {
                    //_ImageOrMediaClickCommand = new RelayCommand(param => ImageUtility.OnCommentImageLeftClick(param));
                    _ImageOrMediaClickCommand = new RelayCommand(param => OnImageOrMediaClickCommand(param));
                }
                return _ImageOrMediaClickCommand;
            }
        }
        private void OnImageOrMediaClickCommand(object parameter)
        {
            try
            {
                if (parameter is CommentModel)
                {
                    CommentModel commentModel = (CommentModel)parameter;
                    if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_IMAGE)
                    {
                        ObservableCollection<ImageModel> tempList = new ObservableCollection<ImageModel>();
                        ImageModel imageModel = new ImageModel();
                        imageModel.ImageUrl = commentModel.MediaOrImageUrl;
                        tempList.Add(imageModel);
                        ImageUtility.ShowImageViewer(tempList, 0, 1, StatusConstants.NAVIGATE_FROM_FEED, Guid.Empty, null, false);
                    }
                    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_MUSIC)
                    {
                        SingleMediaModel singleMediaModel = new SingleMediaModel();
                        singleMediaModel.StreamUrl = commentModel.MediaOrImageUrl;
                        singleMediaModel.ThumbUrl = commentModel.ThumbUrl;
                        singleMediaModel.Duration = commentModel.Duration;
                        singleMediaModel.MediaOwner = commentModel.UserShortInfoModel;
                        singleMediaModel.MediaType = 1;
                        //singleMediaModel.MediaOwner.UserTableID = commentModel.PostOwnerUtId;
                        MediaUtility.PlaySingleMediaNoContentID(singleMediaModel, false);
                    }
                    else if (commentModel.UrlType == SettingsConstants.UPLOADTYPE_VIDEO)
                    {
                        SingleMediaModel singleMediaModel = new SingleMediaModel();
                        singleMediaModel.StreamUrl = commentModel.MediaOrImageUrl;
                        singleMediaModel.ThumbUrl = commentModel.ThumbUrl;
                        singleMediaModel.Duration = commentModel.Duration;
                        singleMediaModel.MediaOwner = commentModel.UserShortInfoModel;
                        singleMediaModel.MediaType = 2;
                        //singleMediaModel.MediaOwner.UserTableID = commentModel.PostOwnerUtId;
                        MediaUtility.PlaySingleMediaNoContentID(singleMediaModel, false);
                    }
                }
                else if (parameter is CommonUploaderModel)
                {
                    CommonUploaderModel commonUploaderModel = (CommonUploaderModel)parameter;
                    if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_MUSIC)
                    {
                        //if (string.IsNullOrEmpty(commonUploaderModel.FilePath))
                        //    PlayerHelperMethods.RunPlayListFromComments(false, commonUploaderModel.UploadedUrl, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 1);
                        //else
                        //    PlayerHelperMethods.RunPlayListFromComments(true, commonUploaderModel.FilePath, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 1);
                    }
                    else if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_VIDEO)
                    {
                        //if (string.IsNullOrEmpty(commonUploaderModel.FilePath))
                        //    PlayerHelperMethods.RunPlayListFromComments(false, commonUploaderModel.UploadedUrl, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 2);
                        //else
                        //    PlayerHelperMethods.RunPlayListFromComments(true, commonUploaderModel.FilePath, commonUploaderModel.UploadedThumbUrl, commonUploaderModel.FileTitle, commonUploaderModel.FileArtist, commonUploaderModel.FileDuration, 2);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        private void OnLikeCommand(object parameter)
        {
            if (parameter is Grid)
            {
                try
                {
                    Grid tb = (Grid)parameter;
                    CommentModel commentModel = (CommentModel)tb.DataContext;
                    FeedModel feedModel = null;
                    JObject pakToSend = new JObject();
                    bool ImageIdToContentId = false;
                    bool ILiked = false;
                    if (commentModel.ILikeComment == 0)
                    {
                        ShowLikeAnimation(commentModel);
                        commentModel.ILikeComment = 1;
                        commentModel.TotalLikeComment = commentModel.TotalLikeComment + 1;
                        ILiked = true;
                    }
                    else
                    {
                        commentModel.ILikeComment = 0;
                        commentModel.TotalLikeComment = commentModel.TotalLikeComment - 1;
                        ILiked = false;
                    }
                    pakToSend[JsonKeys.Action] = AppConstants.ACTION_MERGED_LIKE_UNLIKE_COMMENT;
                    pakToSend[JsonKeys.Liked] = ILiked ? 1 : 0;
                    pakToSend[JsonKeys.CommenterID] = commentModel.UserShortInfoModel.UserTableID;
                    pakToSend[JsonKeys.CommentId] = commentModel.CommentId;

                    if (commentModel.ImageId != Guid.Empty)  //LIKE A COMMENT ON IMAGE
                    {
                        ImageModel imageModel = null;
                        ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(commentModel.ImageId, out imageModel);
                        bool fromFeed = (ViewConstants.NavigateFrom == StatusConstants.NAVIGATE_FROM_FEED) ? true : false;
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_IMAGE;
                        pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                        pakToSend[JsonKeys.ContentId] = imageModel.ImageId;
                        ImageIdToContentId = true;

                        if (fromFeed)
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            if (imageModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(imageModel.NewsFeedId, out feedModel))
                            {
                                pakToSend[JsonKeys.NewsfeedId] = imageModel.NewsFeedId;
                                pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                                pakToSend[JsonKeys.UserTableID] = feedModel.WallOrContentOwner.UserTableID;
                            }
                        }
                        else
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                            pakToSend[JsonKeys.AlbumId] = imageModel.AlbumId;
                            pakToSend[JsonKeys.UserTableID] = imageModel.UserTableID;
                        }
                    }

                    else if (commentModel.ContentId != Guid.Empty)  //LIKE A COMMENT ON MULTIMEDIA
                    {
                        SingleMediaModel singleMediaModel = null;
                        MediaDataContainer.Instance.ContentModels.TryGetValue(commentModel.ContentId, out singleMediaModel);
                        bool fromFeed = singleMediaModel.PlayedFromFeed ? true : false;
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_MULTIMEDIA;
                        pakToSend[JsonKeys.MediaType] = singleMediaModel.MediaType;
                        pakToSend[JsonKeys.ContentId] = singleMediaModel.ContentId;

                        if (fromFeed)
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_FEED;
                            if (singleMediaModel.NewsFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(singleMediaModel.NewsFeedId, out feedModel))
                            {
                                pakToSend[JsonKeys.NewsfeedId] = singleMediaModel.NewsFeedId;
                                pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                                pakToSend[JsonKeys.UserTableID] = feedModel.WallOrContentOwner.UserTableID;
                            }
                        }
                        else
                        {
                            pakToSend[JsonKeys.RootContentType] = SettingsConstants.ROOT_CONTENT_TYPE_ALBUM;
                            pakToSend[JsonKeys.AlbumId] = singleMediaModel.AlbumId;
                            pakToSend[JsonKeys.UserTableID] = singleMediaModel.MediaOwner.UserTableID;
                        }
                    }
                    else //LIKE A COMMENT ON A STATUS
                    {
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_STATUS;

                        if (commentModel.NewsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(commentModel.NewsfeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.NewsfeedId] = commentModel.NewsfeedId;
                            pakToSend[JsonKeys.Privacy] = feedModel.Privacy;
                            pakToSend[JsonKeys.UserTableID] = feedModel.PostOwner.UserTableID;
                        }
                    }

                    new ThreadLikeUnlikeAny().StartThread(pakToSend, ImageIdToContentId);
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        private void ShowLikePopup(Guid contentID, Guid newsfeedId, long numberOflike, Guid imageId, Guid commentId)
        {
            ViewConstants.CommentID = commentId;
            if (LikeListView != null)
            {
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(LikeListView);
                else UCGuiRingID.Instance.MotherPanel.Children.Remove(LikeListView);
            }
            if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                LikeListView = new UCLikeList(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
            else LikeListView = new UCLikeList(UCGuiRingID.Instance.MotherPanel);
            
            LikeListView.ContentId = contentID;
            LikeListView.NewsfeedId = newsfeedId;
            LikeListView.NumberOfLike = numberOflike;
            LikeListView.ImageId = imageId;
            LikeListView.CommentId = commentId;
            LikeListView.Show();
            LikeListView.SendLikeListRequest();
            LikeListView.OnRemovedUserControl += () =>
            {
                LikeListView = null;
                //if (MediaPlayerInMain != null) MediaPlayerInMain.GrabKeyBoardFocus();
            };
        }
        private void OnNumberOfLikeCommand(object parameter)
        {
            if (parameter is CommentModel)
            {
                try
                {
                    CommentModel commentModel = (CommentModel)parameter;
                    if (commentModel.ContentId != Guid.Empty)
                    {
                        ShowLikePopup(commentModel.ContentId, Guid.Empty, commentModel.TotalLikeComment, Guid.Empty, commentModel.CommentId);
                        //MainSwitcher.PopupController.LikeListViewWrapper.Show(commentModel.TotalLikeComment, Guid.Empty, Guid.Empty, commentModel.ContentId, commentModel.CommentId);
                    }
                    else if (commentModel.ImageId != Guid.Empty)
                    {
                        ShowLikePopup(Guid.Empty, Guid.Empty, commentModel.TotalLikeComment, commentModel.ImageId, commentModel.CommentId);
                        //MainSwitcher.PopupController.LikeListViewWrapper.Show(commentModel.ImageId, commentModel.CommentId, commentModel.TotalLikeComment);
                    }
                    else
                        ShowLikePopup(Guid.Empty, commentModel.NewsfeedId, commentModel.TotalLikeComment, Guid.Empty, commentModel.CommentId);
                    //MainSwitcher.PopupController.LikeListViewWrapper.Show(commentModel.TotalLikeComment, commentModel.NewsfeedId, commentModel.CommentId, AppConstants.TYPE_LIST_LIKES_OF_COMMENT);
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
        //private void OnNameCommand(object parameter)
        //{
        //    if (parameter is BaseUserProfileModel)
        //    {
        //        BaseUserProfileModel model = (BaseUserProfileModel)parameter;
        //        RingIDViewModel.Instance.OnUserProfileClicked(model);
        //    }
        //}
        private ICommand _EditHistoryMouseEnter;
        public ICommand EditHistoryMouseEnter
        {
            get
            {
                if (_EditHistoryMouseEnter == null)
                {
                    _EditHistoryMouseEnter = new RelayCommand(param => OnEditHistoryMouseEnter(param));
                }
                return _EditHistoryMouseEnter;
            }
        }

        private ICommand _EditHistoryMouseLeave;
        public ICommand EditHistoryMouseLeave
        {
            get
            {
                if (_EditHistoryMouseLeave == null)
                {
                    _EditHistoryMouseLeave = new RelayCommand(param => OnEditHistoryMouseLeave(param));
                }
                return _EditHistoryMouseLeave;
            }
        }
        private void OnEditHistoryMouseEnter(object parameter)
        {
            if (parameter is Grid)
            {
                Grid mainPanel = (Grid)parameter;
                if (ucEditHistoryPopupStyle == null)
                {
                    ucEditHistoryPopupStyle = new UCEditHistoryPopupStyle();
                    mainPanel.Children.Add(ucEditHistoryPopupStyle);
                }
                ucEditHistoryPopupStyle.Show(mainPanel);
            }
        }

        private void OnEditHistoryMouseLeave(object parameter)
        {
            if (parameter is Grid)
            {
                Grid mainPanel = (Grid)parameter;
                mainPanel.Children.Remove(ucEditHistoryPopupStyle);
                ucEditHistoryPopupStyle.PopupEdited.IsOpen = false;
                ucEditHistoryPopupStyle = null;
            }
        }
        private ICommand _EditHistoryCommand;
        public ICommand EditHistoryCommand
        {
            get
            {
                if (_EditHistoryCommand == null)
                {
                    _EditHistoryCommand = new RelayCommand(param => OnEditHistoryCommand(param));
                }
                return _EditHistoryCommand;
            }
        }

        private ICommand _seeMoreCommand;
        public ICommand SeeMoreCommand
        {
            get
            {
                if (_seeMoreCommand == null)
                {
                    _seeMoreCommand = new RelayCommand(param => OnStatusSeeMoreCommand(param));
                }
                return _seeMoreCommand;
            }
        }
        private void OnEditHistoryCommand(object parameter)
        {
            if (parameter is CommentModel)
            {
                CommentModel _CommentModel = (CommentModel)parameter;
                MainSwitcher.PopupController.ucEditedHistoryView.Show();
                MainSwitcher.PopupController.ucEditedHistoryView.ShowEditHistoryView(_CommentModel);
            }
        }

        private void OnStatusSeeMoreCommand(object parameter)
        {
            try
            {
                if (parameter is CommentModel)
                {
                    CommentModel ModelToSeeMore = (CommentModel)parameter;
                    ThreadGetSingleFullComment threadFullComment = new ThreadGetSingleFullComment(ModelToSeeMore.NewsfeedId, ModelToSeeMore.CommentId, Guid.Empty, Guid.Empty);
                    threadFullComment.callBackEvent += (jObj) =>
                    {
                        if (jObj != null) ModelToSeeMore.LoadData(jObj);
                    };
                    threadFullComment.StartThread();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

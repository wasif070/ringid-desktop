﻿using log4net;
using Models.Constants;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using View.BindingModels;
using View.UI;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;

namespace View.ViewModel
{
    public class OtherViewsVM : INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(OtherViewsVM).Name);

        public static OtherViewsVM Instance = new OtherViewsVM();

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region "ICommand"
        private ICommand _FollowUnfollowBtnClicked;
        public ICommand FollowUnfollowBtnClicked
        {
            get
            {
                if (_FollowUnfollowBtnClicked == null)
                {
                    _FollowUnfollowBtnClicked = new RelayCommand(param => OnFollowUnfollowBtnClicked(param));
                }
                return _FollowUnfollowBtnClicked;
            }
        }

        private ICommand _BackBtnClicked;
        public ICommand BackBtnClicked
        {
            get
            {
                if (_BackBtnClicked == null)
                {
                    _BackBtnClicked = new RelayCommand(param => OnBackBtnClicked(param));
                }
                return _BackBtnClicked;
            }
        }
        #endregion

        private void OnFollowUnfollowBtnClicked(object parameter)
        {
            if (parameter is NewsPortalModel)
            {
                NewsPortalModel newsPortalModel = (NewsPortalModel)parameter;
                if (newsPortalModel.IsSubscribed)
                {
                    bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, "New Portal"), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                    if (isTrue)
                        new ThradSubscribeOrUnsubscribe(SettingsConstants.PROFILE_TYPE_NEWSPORTAL, newsPortalModel.UserTableID, 1, null, null, newsPortalModel.PortalId).StartThread();
                }
                else
                {
                    if (UCFollowingUnfollowingView.Instance == null)
                    {
                        UCFollowingUnfollowingView.Instance = new UCFollowingUnfollowingView(UCGuiRingID.Instance.MotherPanel);
                        UCFollowingUnfollowingView.Instance.Show();
                        UCFollowingUnfollowingView.Instance.ShowFollowUnFollow(newsPortalModel, false);
                        UCFollowingUnfollowingView.Instance.OnRemovedUserControl += () =>
                        {
                            UCFollowingUnfollowingView.Instance = null;
                        };
                    }
                }

            }
        }
        private void OnBackBtnClicked(object parameter)
        {
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }
        private ICommand _SpaceCommand;
        public ICommand SpaceCommand
        {
            get
            {
                if (_SpaceCommand == null) _SpaceCommand = new RelayCommand(param => OnSpaceCommandCommand());
                return _SpaceCommand;
            }
        }
        public void OnSpaceCommandCommand()
        {
        }
    }
}

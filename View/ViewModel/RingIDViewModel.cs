﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Feed;
using View.UI.FriendList;
using View.UI.PopUp;
using View.UI.Profile.MusicPageProfile;
using View.UI.Profile.NewsPortalProfile;
using View.UI.Profile.PageProfile;
using View.UI.Settings;
using View.Utility;
using View.Utility.Call;
using View.Utility.Call.Settings;
using View.Utility.Chat;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.FriendProfile;

namespace View.ViewModel
{
    public class RingIDViewModel : BaseViewModel
    {
        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(RingIDViewModel).Name);
        private UserBasicInfoModel _MyBasicInfoModel = new UserBasicInfoModel();
        private ChangableTexts _changableTexts;
        private bool _IsExpanded;
        private ObservableCollection<UserBasicInfoModel> _SpecialFriendList;
        private ObservableCollection<UserBasicInfoModel> _RecentlyAddedFriendList;
        private ObservableCollection<UserBasicInfoModel> _FavouriteFriendList = new ObservableCollection<UserBasicInfoModel>();
        private ObservableCollection<UserBasicInfoModel> _TopFriendList = new ObservableCollection<UserBasicInfoModel>();
        private ObservableCollection<UserBasicInfoModel> _NonFavouriteFriendList;
        private ObservableCollection<UserBasicInfoModel> _TempFriendList;
        private ObservableCollection<UserBasicInfoModel> _TempFriendSearchList;
        private ObservableCollection<UserBasicInfoModel> _SuggestionFriendList;
        private ObservableCollection<UserBasicInfoModel> _BlockedContactList;
        private ObservableCollection<CelebrityModel> _CelebritiesPopular;
        private ObservableCollection<SingleMediaModel> _BreakingMediaCloudFeeds;
        private ObservableCollection<FeedModel> _BreakingNewsPortalFeeds;
        private ObservableCollection<FeedModel> _BreakingNewsPortalProfileFeeds;
        private ObservableCollection<CelebrityModel> _CelebritiesFollowing;
        private ObservableCollection<CelebrityModel> _CelebritiesDiscovered;
        private ObservableCollection<MusicPageModel> _MusicPagesFollowing;
        private ObservableCollection<MusicPageModel> _MusicPagesDiscovered;
        private ObservableCollection<ImageModel> _ProfileImageList;
        private ObservableCollection<ImageModel> _CoverImageList;
        private ObservableCollection<ImageModel> _FeedImageList;
        private ObservableCollection<RecentModel> _CallLogModelsList;
        private ObservableCollection<CountryCodeModel> _CountryModelList;
        private ObservableCollection<ChatBgImageModel> _ChatBgImageList;
        private ObservableCollection<SingleWorkModel> _MyWorkModelsList;
        private ObservableCollection<SingleEducationModel> _MyEducationModelsList;
        private ObservableCollection<SingleSkillModel> _MySkillModelsList;
        private ObservableCollection<NotificationModel> _NotificationList;
        private ObservableCollection<UploadingModel> _UploadingModelList = new ObservableCollection<UploadingModel>();
        private ConcurrentDictionary<int, ObservableDictionary<int, string>> _SpamReasonList = new ConcurrentDictionary<int, ObservableDictionary<int, string>>();

        //inuse
        //private CustomObservableCollection _NewsFeeds;
        //private CustomObservableCollection _SavedFeeds, _SavedFeedsNewsPortal, _SavedFeedsPage, _SavedFeedsMediaPage, _SavedFeedsCelebrity, _SavedFeedsCircle;
        //private CustomObservableCollection _AllNewsPortalFeeds, _AllPagesFeeds, _AllMediaPageFeeds, _AllCelebrityFeeds, _AllCircleFeeds;
        //private CustomObservableCollection _NewsPortalProfileFeeds, _PageProfileFeeds, _MediaPageProfileFeeds, _CelebrityProfileFeeds, _CircleProfileFeeds, _FriendProfileFeeds;
        //private CustomObservableCollection _MyNewsFeeds;


        private CustomFeedCollection _AllMediaCustomFeeds, _AllNewsPortalCustomFeeds, _SavedNewsPortalCustomFeeds, _SavedMediaCustomFeeds, _SavedAllCustomFeeds, _ProfileMyCustomFeeds, _ProfileFriendCustomFeeds, _ProfileNewsPortalCustomFeeds, _ProfileMediaCustomFeeds, _ProfileCelebrityCustomFeeds;
        //
        //private CustomObservableCollection _MediaFeeds;
        //private CustomObservableCollection _MediaFeedsTrending;
        private ObservableCollection<DoingModel> _DoingListForNewStatus;
        private ObservableCollection<MediaContentModel> _MyAudioAlbums;
        private ObservableCollection<MediaContentModel> _MyVideoAlbums;
        private ObservableCollection<AlbumModel> _myImageAlbums;
        private ObservableCollection<SingleMediaModel> _MyDownloads, _MyRecentPlayList;
        private ObservableCollection<SingleMediaModel> _FeedMediasNewlyAdded;
        //CHAT RELATED
        private ObservableDictionary<long, GroupInfoModel> _GroupList = new ObservableDictionary<long, GroupInfoModel>();
        private RoomRequestModel _RoomListFetchQuery = new RoomRequestModel();
        private ObservableDictionary<string, RoomModel> _RoomList = new ObservableDictionary<string, RoomModel>();
        private RoomRequestModel _RoomListSearchQuery = new RoomRequestModel();
        private ObservableDictionary<string, RoomModel> _RoomSearchList = new ObservableDictionary<string, RoomModel>();
        private ObservableCollection<RoomCategoryModel> _RoomCategoryList = new ObservableCollection<RoomCategoryModel>();
        private ConcurrentDictionary<string, ObservableCollection<RecentModel>> _RecentModelList = new ConcurrentDictionary<string, ObservableCollection<RecentModel>>();
        private ObservableDictionary<string, ChatBgHistoryModel> _ChatBackgroundList = new ObservableDictionary<string, ChatBgHistoryModel>();
        private ObservableCollection<RecentModel> _ChatLogModelsList = new ObservableCollection<RecentModel>();
        private ObservableCollection<UserBasicInfoModel> _NoChatCallLogModel = new ObservableCollection<UserBasicInfoModel>();
        //private ObservableCollection<StickerCategoryModel> _StickerCategoryList;
        private ObservableCollection<MarketStickerCategoryModel> _MarketStickerCategoryList;
        private ObservableCollection<MarketStickerCategoryModel> _RecentStickerCategoryList;

        private ObservableCollection<FeedModel> _ShareListFeedModel = new ObservableCollection<FeedModel>();
        private ConcurrentDictionary<long, BlockedNonFriendModel> _BlockedNonFriendList = new ConcurrentDictionary<long, BlockedNonFriendModel>();
        private ObservableCollection<StreamAndChannelModel> _StreamAndChannelFeatureList = new ObservableCollection<StreamAndChannelModel>();

        public delegate void MainUserControlChangedHandler(int type);

        //   private ObservableDictionary<long, UserBasicInfoModel> _MoreFriendList = new ObservableDictionary<long, UserBasicInfoModel>();
        private ICommand _ExpandCommand;
        private ICommand _CollapseCommand;
        private ICommand _ShowFriendListOrDialpadCommand;
        private ICommand _MyProfileClickedCommand;

        private ICommand _GroupListCommand;
        private ICommand _RoomListCommand;

        private ICommand _AvatarCommand;
        private ICommand _FriendProfileCommand;
        private ICommand _FriendInformationCommand;
        private ICommand _FriendCallChatCommand;
        private ICommand _FriendChatSettingsCommand;
        private ICommand _VoiceCallCommand;
        private ICommand _VideoCallCommand;


        private ICommand _GroupCreateCommand;
        private ICommand _GroupChatSettingsCommand;
        private ICommand _GroupCallChatCommand;
        private ICommand _RoomCallChatCommand;
        private ICommand _RoomChatSettingsCommand;
        private ICommand _RoomMemberListCommand;
        private ICommand _CoverImageRightClickedActions;
        private ICommand _ProfileImageRightClickedActions;
        private ICommand _MediaPlayCommand;
        private ICommand _HidePage;
        private ICommand _EditPage;

        private ICommand _AddFriendCommand;
        private ICommand _AcceptCommand;
        private ICommand _RejectCommand;

        private ICommand _MyChannelCommand;

        private int _CallNotificationCounter;
        //private int _ChatNotificationCounter;
        private int _AllNotificationCounter;
        private int _AddFriendsNotificationCounter;
        private int _AddFriendsNotificationCounterSize;
        private int _FeedUnreadNotificationCounter;
        private int _NewStickerNotificationCounter;
        private int _SelectedMainMiddlePanel = MiddlePanelConstants.TypeAllFeeds;
        #endregion "Private Fields"

        #region "Public Fields"
        public WNRingIDSettingsWindow _WNRingIDSettings = null;
        //   public UCTopMenuBar _UCMainMenu = null;
        // public bool _IsNewStickerClicked = false;
        #endregion "Public Fields"

        #region Property
        public static RingIDViewModel Instance { get; set; }
        private VMRingIDMainWindow winDataModel;
        public VMRingIDMainWindow WinDataModel
        {
            get { return winDataModel; }
            set { winDataModel = value; OnPropertyChanged("DataModel"); }
        }

        private VMCall vmCall;
        public VMCall VMCall
        {
            get { return vmCall; }
            set { vmCall = value; OnPropertyChanged("VMCall"); }
        }

        public ChangableTexts ChangableTexts
        {
            get
            {
                if (_changableTexts == null) _changableTexts = new ChangableTexts();
                return _changableTexts;
            }
        }

        public WNRingIDSettingsWindow GetWindowRingIDSettings
        {
            get
            {
                if (_WNRingIDSettings == null) _WNRingIDSettings = new WNRingIDSettingsWindow();
                return _WNRingIDSettings;
            }
        }

        public UCMyAlbums _UCMyAlbums = null;
        public UCMyAlbums MyAlbums
        {
            get
            {
                if (_UCMyAlbums == null) _UCMyAlbums = new UCMyAlbums();
                return _UCMyAlbums;
            }
        }
        public bool IsExpanded
        {
            get { return this._IsExpanded; }
            set
            {
                this._IsExpanded = value;
                this.OnPropertyChanged("IsExpanded");
            }
        }
        /// <summary>
        public CustomFeedCollection AllMediaCustomFeeds
        {
            get { return _AllMediaCustomFeeds; }
            set
            {
                _AllMediaCustomFeeds = value;
                this.OnPropertyChanged("AllMediaCustomFeeds");
            }
        }
        public CustomFeedCollection AllNewsPortalCustomFeeds
        {
            get { return _AllNewsPortalCustomFeeds; }
            set
            {
                _AllNewsPortalCustomFeeds = value;
                this.OnPropertyChanged("AllNewsPortalCustomFeeds");
            }
        }

        public CustomFeedCollection SavedAllCustomFeeds
        {
            get { return _SavedAllCustomFeeds; }
            set
            {
                _SavedAllCustomFeeds = value;
                this.OnPropertyChanged("SavedAllCustomFeeds");
            }
        }

        public CustomFeedCollection SavedNewsPortalCustomFeeds
        {
            get { return _SavedNewsPortalCustomFeeds; }
            set
            {
                _SavedNewsPortalCustomFeeds = value;
                this.OnPropertyChanged("SavedNewsPortalCustomFeeds");
            }
        }

        public CustomFeedCollection SavedMediaCustomFeeds
        {
            get { return _SavedMediaCustomFeeds; }
            set
            {
                _SavedMediaCustomFeeds = value;
                this.OnPropertyChanged("SavedMediaCustomFeeds");
            }
        }
        //

        public CustomFeedCollection ProfileMyCustomFeeds
        {
            get { return _ProfileMyCustomFeeds; }
            set
            {
                _ProfileMyCustomFeeds = value;
                this.OnPropertyChanged("ProfileMyCustomFeeds");
            }
        }

        public CustomFeedCollection ProfileFriendCustomFeeds
        {
            get { return _ProfileFriendCustomFeeds; }
            set
            {
                _ProfileFriendCustomFeeds = value;
                this.OnPropertyChanged("ProfileFriendCustomFeeds");
            }
        }

        public CustomFeedCollection ProfileNewsPortalCustomFeeds
        {
            get { return _ProfileNewsPortalCustomFeeds; }
            set
            {
                _ProfileNewsPortalCustomFeeds = value;
                this.OnPropertyChanged("ProfileNewsPortalCustomFeeds");
            }
        }

        public CustomFeedCollection ProfileMediaCustomFeeds
        {
            get { return _ProfileMediaCustomFeeds; }
            set
            {
                _ProfileMediaCustomFeeds = value;
                this.OnPropertyChanged("ProfileMediaCustomFeeds");
            }
        }

        ///////
        //public CustomObservableCollection NewsFeeds
        //{
        //    get { return _NewsFeeds; }
        //    set
        //    {
        //        _NewsFeeds = value;
        //        this.OnPropertyChanged("NewsFeeds");
        //    }
        //}
        //public CustomObservableCollection SavedFeeds
        //{
        //    get { return _SavedFeeds; }
        //    set
        //    {
        //        _SavedFeeds = value;
        //        this.OnPropertyChanged("SavedFeeds");
        //    }
        //}
        //public CustomObservableCollection SavedFeedsNewsPortal
        //{
        //    get { return _SavedFeedsNewsPortal; }
        //    set
        //    {
        //        _SavedFeedsNewsPortal = value;
        //        this.OnPropertyChanged("SavedFeedsNewsPortal");
        //    }
        //}
        //public CustomObservableCollection SavedFeedsPage
        //{
        //    get { return _SavedFeedsPage; }
        //    set
        //    {
        //        _SavedFeedsPage = value;
        //        this.OnPropertyChanged("SavedFeedsPage");
        //    }
        //}
        //public CustomObservableCollection SavedFeedsMediaPage
        //{
        //    get { return _SavedFeedsMediaPage; }
        //    set
        //    {
        //        _SavedFeedsMediaPage = value;
        //        this.OnPropertyChanged("SavedFeedsMediaPage");
        //    }
        //}
        //public CustomObservableCollection SavedFeedsCelebrity
        //{
        //    get { return _SavedFeedsCelebrity; }
        //    set
        //    {
        //        _SavedFeedsCelebrity = value;
        //        this.OnPropertyChanged("SavedFeedsCelebrity");
        //    }
        //}
        //public CustomObservableCollection SavedFeedsCircle
        //{
        //    get { return _SavedFeedsCircle; }
        //    set
        //    {
        //        _SavedFeedsCircle = value;
        //        this.OnPropertyChanged("SavedFeedsCircle");
        //    }
        //}
        //public CustomObservableCollection NewsPortalProfileFeeds
        //{
        //    get { return _NewsPortalProfileFeeds; }
        //    set
        //    {
        //        _NewsPortalProfileFeeds = value;
        //        this.OnPropertyChanged("NewsPortalProfileFeeds");
        //    }
        //}
        //public CustomObservableCollection PageProfileFeeds
        //{
        //    get { return _PageProfileFeeds; }
        //    set
        //    {
        //        _PageProfileFeeds = value;
        //        this.OnPropertyChanged("PageProfileFeeds");
        //    }
        //}
        //public CustomObservableCollection MediaPageProfileFeeds
        //{
        //    get { return _MediaPageProfileFeeds; }
        //    set
        //    {
        //        _MediaPageProfileFeeds = value;
        //        this.OnPropertyChanged("MediaPageProfileFeeds");
        //    }
        //}
        //public CustomObservableCollection CelebrityProfileFeeds
        //{
        //    get { return _CelebrityProfileFeeds; }
        //    set
        //    {
        //        _CelebrityProfileFeeds = value;
        //        this.OnPropertyChanged("CelebrityProfileFeeds");
        //    }
        //}
        //public CustomObservableCollection CircleProfileFeeds
        //{
        //    get { return _CircleProfileFeeds; }
        //    set
        //    {
        //        _CircleProfileFeeds = value;
        //        this.OnPropertyChanged("CircleProfileFeeds");
        //    }
        //}

        //public CustomObservableCollection FriendProfileFeeds
        //{
        //    get { return _FriendProfileFeeds; }
        //    set
        //    {
        //        _FriendProfileFeeds = value;
        //        this.OnPropertyChanged("FriendProfileFeeds");
        //    }
        //}
        //public CustomObservableCollection MyNewsFeeds
        //{
        //    get { return _MyNewsFeeds; }
        //    set
        //    {
        //        _MyNewsFeeds = value;
        //        this.OnPropertyChanged("MyNewsFeeds");
        //    }
        //}
        //public CustomObservableCollection AllNewsPortalFeeds
        //{
        //    get { return _AllNewsPortalFeeds; }
        //    set
        //    {
        //        _AllNewsPortalFeeds = value;
        //        this.OnPropertyChanged("AllNewsPortalFeeds");
        //    }
        //}
        //public CustomObservableCollection AllPagesFeeds
        //{
        //    get { return _AllPagesFeeds; }
        //    set
        //    {
        //        _AllPagesFeeds = value;
        //        this.OnPropertyChanged("AllPagesFeeds");
        //    }
        //}
        //public CustomObservableCollection AllCircleFeeds
        //{
        //    get { return _AllCircleFeeds; }
        //    set
        //    {
        //        _AllCircleFeeds = value;
        //        this.OnPropertyChanged("AllCircleFeeds");
        //    }
        //}
        //public CustomObservableCollection AllCelebrityFeeds
        //{
        //    get { return _AllCelebrityFeeds; }
        //    set
        //    {
        //        _AllCelebrityFeeds = value;
        //        this.OnPropertyChanged("AllCelebrityFeeds");
        //    }
        //}
        //public CustomObservableCollection AllMediaPageFeeds
        //{
        //    get { return _AllMediaPageFeeds; }
        //    set
        //    {
        //        _AllMediaPageFeeds = value;
        //        this.OnPropertyChanged("AllMediaPageFeeds");
        //    }
        //}

        public ObservableCollection<SingleMediaModel> BreakingMediaCloudFeeds
        {
            get { return _BreakingMediaCloudFeeds; }
            set
            {
                _BreakingMediaCloudFeeds = value;
                this.OnPropertyChanged("BreakingMediaCloudFeeds");
            }
        }
        public ObservableCollection<FeedModel> BreakingNewsPortalFeeds
        {
            get { return _BreakingNewsPortalFeeds; }
            set
            {
                _BreakingNewsPortalFeeds = value;
                this.OnPropertyChanged("BreakingNewsPortalFeeds");
            }
        }
        public ObservableCollection<FeedModel> BreakingNewsPortalProfileFeeds
        {
            get { return _BreakingNewsPortalProfileFeeds; }
            set
            {
                _BreakingNewsPortalProfileFeeds = value;
                this.OnPropertyChanged("BreakingNewsPortalProfileFeeds");
            }
        }
        //public CustomObservableCollection MediaFeeds
        //{
        //    get { return _MediaFeeds; }
        //    set
        //    {
        //        _MediaFeeds = value;
        //        this.OnPropertyChanged("MediaFeeds");
        //    }
        //}

        //public CustomObservableCollection MediaFeedsTrending
        //{
        //    get { return _MediaFeedsTrending; }
        //    set
        //    {
        //        _MediaFeedsTrending = value;
        //        this.OnPropertyChanged("MediaFeedsTrending");
        //    }
        //}

        public ObservableCollection<DoingModel> DoingListForNewStatus
        {
            get { return _DoingListForNewStatus; }
            set
            {
                _DoingListForNewStatus = value;
                this.OnPropertyChanged("DoingListForNewStatus");
            }
        }

        public ObservableCollection<MediaContentModel> MyAudioAlbums
        {
            get { return _MyAudioAlbums; }
            set
            {
                _MyAudioAlbums = value;
                this.OnPropertyChanged("MyAudioAlbums");
            }
        }

        public ObservableCollection<MediaContentModel> MyVideoAlbums
        {
            get { return _MyVideoAlbums; }
            set { _MyVideoAlbums = value; this.OnPropertyChanged("MyVideoAlbums"); }
        }

        public ObservableCollection<AlbumModel> MyImageAlubms
        {
            get
            {
                return _myImageAlbums;
            }
            set
            {
                _myImageAlbums = value;
                this.OnPropertyChanged("MyImageAlubms");
            }
        }


        public ObservableCollection<SingleMediaModel> MyDownloads
        {
            get { return _MyDownloads; }
            set
            {
                _MyDownloads = value; this.OnPropertyChanged("MyDownloads");
            }
        }

        public ObservableCollection<SingleMediaModel> MyRecentPlayList
        {
            get { return _MyRecentPlayList; }
            set { _MyRecentPlayList = value; this.OnPropertyChanged("MyRecentPlayList"); }
        }

        public ObservableCollection<FeedModel> ShareListFeedModel
        {
            get { return _ShareListFeedModel; }
            set { _ShareListFeedModel = value; this.OnPropertyChanged("ShareListFeedModel"); }
        }

        public ObservableCollection<SingleMediaModel> FeedMediasNewlyAdded
        {
            get { return _FeedMediasNewlyAdded; }
            set { _FeedMediasNewlyAdded = value; this.OnPropertyChanged("FeedMediasNewlyAdded"); }
        }

        public ObservableCollection<CelebrityModel> CelebritiesPopular
        {
            get { return _CelebritiesPopular; }
            set { _CelebritiesPopular = value; this.OnPropertyChanged("CelebritiesPopular"); }
        }

        public ObservableCollection<CelebrityModel> CelebritiesFollowing
        {
            get { return _CelebritiesFollowing; }
            set { _CelebritiesFollowing = value; this.OnPropertyChanged("CelebritiesFollowing"); }
        }

        public ObservableCollection<CelebrityModel> CelebritiesDiscovered
        {
            get { return _CelebritiesDiscovered; }
            set { _CelebritiesDiscovered = value; this.OnPropertyChanged("CelebritiesDiscovered"); }
        }

        public ObservableCollection<MusicPageModel> MusicPagesFollowing
        {
            get { return _MusicPagesFollowing; }
            set { _MusicPagesFollowing = value; this.OnPropertyChanged("MusicPagesFollowing"); }
        }

        public ObservableCollection<MusicPageModel> MusicPagesDiscovered
        {
            get { return _MusicPagesDiscovered; }
            set { _MusicPagesDiscovered = value; this.OnPropertyChanged("MusicPagesDiscovered"); }
        }

        public ObservableCollection<RecentModel> CallLogModelsList
        {
            get { return _CallLogModelsList; }
            set { _CallLogModelsList = value; this.OnPropertyChanged("CallLogModelsList"); }
        }

        public ObservableCollection<RecentModel> ChatLogModelsList
        {
            get { return _ChatLogModelsList; }
            set { _ChatLogModelsList = value; this.OnPropertyChanged("ChatLogModelsList"); }
        }

        public ObservableCollection<UserBasicInfoModel> NoChatCallLogModel
        {
            get { return _NoChatCallLogModel; }
            set { _NoChatCallLogModel = value; this.OnPropertyChanged("NoChatCallLogModel"); }
        }

        public ObservableCollection<CountryCodeModel> CountryModelList
        {
            get { return _CountryModelList; }
            set { _CountryModelList = value; this.OnPropertyChanged("CountryModelList"); }
        }

        public ObservableCollection<UploadingModel> UploadingModelList
        {
            get { return _UploadingModelList; }
            set { _UploadingModelList = value; this.OnPropertyChanged("UploadingModelList"); }
        }

        public ConcurrentDictionary<int, ObservableDictionary<int, string>> SpamReasonList
        {
            get { return _SpamReasonList; }
            set { _SpamReasonList = value; }
        }

        public UserBasicInfoModel MyBasicInfoModel
        {
            get { return _MyBasicInfoModel; }
            set { _MyBasicInfoModel = value; this.OnPropertyChanged("MyBasicInfoModel"); }
        }

        public ObservableCollection<UserBasicInfoModel> RecentlyAddedFriendList
        {
            get { return _RecentlyAddedFriendList; }
            set
            {
                _RecentlyAddedFriendList = value;
                _RecentlyAddedFriendList.CollectionChanged += FriendList_CollectionChanged;
                this.OnPropertyChanged("RecentlyAddedFriendList");
            }
        }

        public ObservableCollection<UserBasicInfoModel> FavouriteFriendList
        {
            get { return _FavouriteFriendList; }
            set
            {
                _FavouriteFriendList = value;
                _FavouriteFriendList.CollectionChanged += FriendList_CollectionChanged;
                this.OnPropertyChanged("FavouriteFriendList");
            }
        }

        public ObservableCollection<UserBasicInfoModel> SpecialFriendList
        {
            get { return _SpecialFriendList; }
            set
            {
                _SpecialFriendList = value;
                this.OnPropertyChanged("SpecialFriendList");
            }
        }

        void FriendList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove || e.Action == NotifyCollectionChangedAction.Reset)
            {
                if (UCFriendList.Instance != null && UCFriendList.Instance.IsVisible)
                {
                    UCFriendList.Instance.ChangeOpenStatus();
                    UCFriendList.Instance.GetFriendCount();
                }
            }
        }

        public ObservableCollection<UserBasicInfoModel> BlockedContactList
        {
            get { return _BlockedContactList; }
            set
            {
                _BlockedContactList = value;
                _BlockedContactList.CollectionChanged += _BlockedContactList_CollectionChanged;
                this.OnPropertyChanged("BlockedContactList");
            }
        }

        void _BlockedContactList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add || e.Action == NotifyCollectionChangedAction.Remove || e.Action == NotifyCollectionChangedAction.Reset)
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (UCMiddlePanelSwitcher.View_UCAddFriendMainPanel != null && UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance != null)
                    {
                        if (BlockedContactList.Count > 0) UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance.NoBlockedUserVisibility = false;
                        else UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance.NoBlockedUserVisibility = true;
                    }
                });
            }
        }

        private void _SuggestionFriendList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add) IsSuggesFetchingCompleted = true;
        }

        private ObservableCollection<UserBasicInfoModel> _TempMyFriendList;

        public ObservableCollection<UserBasicInfoModel> TempMyFriendList
        {
            get { return _TempMyFriendList; }
            set { _TempMyFriendList = value; this.OnPropertyChanged("TempMyFriendList"); }
        }

        public ObservableCollection<UserBasicInfoModel> TempFriendList
        {
            get { return _TempFriendList; }
            set { _TempFriendList = value; this.OnPropertyChanged("TempFriendList"); }
        }

        public ObservableCollection<UserBasicInfoModel> TempFriendSearchList
        {
            get { return _TempFriendSearchList; }
            set { _TempFriendSearchList = value; this.OnPropertyChanged("TempFriendSearchList"); }
        }

        public ObservableCollection<UserBasicInfoModel> SuggestionFriendList
        {
            get { return _SuggestionFriendList; }
            set
            {
                _SuggestionFriendList = value;
                _SuggestionFriendList.CollectionChanged += _SuggestionFriendList_CollectionChanged;
                this.OnPropertyChanged("SuggestionFriendList");
            }
        }

        public ObservableCollection<MarketStickerCategoryModel> MarketStickerCategoryList
        {
            get { return _MarketStickerCategoryList; }
            set { _MarketStickerCategoryList = value; this.OnPropertyChanged("MarketStickerCategoryList"); }
        }

        public ObservableCollection<MarketStickerCategoryModel> RecentStickerCategoryList
        {
            get { return _RecentStickerCategoryList; }
            set { _RecentStickerCategoryList = value; this.OnPropertyChanged("RecentStickerCategoryList"); }
        }

        public ObservableCollection<ImageModel> ProfileImageList
        {
            get { return _ProfileImageList; }
            set { _ProfileImageList = value; this.OnPropertyChanged("ProfileImageList"); }
        }

        public ObservableCollection<ImageModel> CoverImageList
        {
            get { return _CoverImageList; }
            set { _CoverImageList = value; this.OnPropertyChanged("CoverImageList"); }
        }

        public ObservableCollection<ImageModel> FeedImageList
        {
            get { return _FeedImageList; }
            set { _FeedImageList = value; this.OnPropertyChanged("FeedImageList"); }
        }

        public ObservableCollection<SingleWorkModel> MyWorkModelsList
        {
            get { return _MyWorkModelsList; }
            set { _MyWorkModelsList = value; this.OnPropertyChanged("MyWorkModelsList"); }
        }

        public ObservableCollection<SingleEducationModel> MyEducationModelsList
        {
            get { return _MyEducationModelsList; }
            set { _MyEducationModelsList = value; this.OnPropertyChanged("MyEducationModelsList"); }
        }

        public ObservableCollection<SingleSkillModel> MySkillModelsList
        {
            get { return _MySkillModelsList; }
            set { _MySkillModelsList = value; this.OnPropertyChanged("MySkillModelsList"); }
        }

        public ObservableCollection<NotificationModel> NotificationList
        {
            get { return _NotificationList; }
            set { _NotificationList = value; this.OnPropertyChanged("NotificationList"); }
        }

        public ObservableCollection<UserBasicInfoModel> TopFriendList
        {
            get { return _TopFriendList; }
            set
            {
                _TopFriendList = value;
                _TopFriendList.CollectionChanged += FriendList_CollectionChanged;
                this.OnPropertyChanged("TopFriendList");
            }
        }

        public ObservableCollection<UserBasicInfoModel> NonFavouriteFriendList
        {
            get { return _NonFavouriteFriendList; }
            set
            {
                _NonFavouriteFriendList = value;
                _NonFavouriteFriendList.CollectionChanged += FriendList_CollectionChanged;
                this.OnPropertyChanged("NonFavouriteFriendList");
            }
        }

        public ObservableCollection<ChatBgImageModel> ChatBgImageList
        {
            get { return _ChatBgImageList; }
            set { _ChatBgImageList = value; this.OnPropertyChanged("ChatBgImageList"); }
        }

        public ObservableCollection<StreamAndChannelModel> StreamAndChannelFeatureList
        {
            get { return _StreamAndChannelFeatureList; }
            set { _StreamAndChannelFeatureList = value; this.OnPropertyChanged("StreamAndChannelFeatureList"); }
        }

        public int CallNotificationCounter
        {
            get { return _CallNotificationCounter; }
            set
            {
                if (_CallNotificationCounter == value) return;
                _CallNotificationCounter = value; OnPropertyChanged("CallNotificationCounter");
            }
        }

        public int AllNotificationCounter
        {
            get { return _AllNotificationCounter; }
            set
            {
                //if (_AllNotificationCounter == value) return;
                _AllNotificationCounter = value; OnPropertyChanged("AllNotificationCounter");
            }
        }

        public int AddFriendsNotificationCounter
        {
            get { return _AddFriendsNotificationCounter; }
            set
            {
                if (_AddFriendsNotificationCounter == value || value < 0) return;
                _AddFriendsNotificationCounter = value; OnPropertyChanged("AddFriendsNotificationCounter");
            }
        }

        public int AddFriendsNotificationCounterSize
        {
            get { return _AddFriendsNotificationCounterSize; }
            set
            {
                if (_AddFriendsNotificationCounterSize == value) return;
                _AddFriendsNotificationCounterSize = value; OnPropertyChanged("AddFriendsNotificationCounterSize");
            }
        }

        public int FeedUnreadNotificationCounter
        {
            get { return _FeedUnreadNotificationCounter; }
            set
            {
                if (_FeedUnreadNotificationCounter == value) return;
                _FeedUnreadNotificationCounter = value; OnPropertyChanged("FeedUnreadNotificationCounter");
            }
        }

        public int NewStickerNotificationCounter
        {
            get { return _NewStickerNotificationCounter; }
            set
            {
                if (_NewStickerNotificationCounter == value) return;
                _NewStickerNotificationCounter = value; OnPropertyChanged("NewStickerNotificationCounter");
            }
        }

        private int _TotalNewStickers;
        public int TotalNewStickers
        {
            get { return _TotalNewStickers; }
            set
            {
                if (_TotalNewStickers == value) return;
                _TotalNewStickers = value; OnPropertyChanged("TotalNewStickers");
            }
        }

        public int SelectedMainMiddlePanel
        {
            get { return _SelectedMainMiddlePanel; }
            set
            {
                if (_SelectedMainMiddlePanel == value)
                {
                    OnMainUserControlChanged(value);
                    return;
                }

                _SelectedMainMiddlePanel = value;
                OnPropertyChanged("SelectedMainMiddlePanel");
                OnMainUserControlChanged(value);
            }
        }

        private bool _FeedButtionSelection = false;
        public bool FeedButtionSelection
        {
            get { return _FeedButtionSelection; }
            set
            {
                if (_FeedButtionSelection == value) return;
                _FeedButtionSelection = value; OnPropertyChanged("FeedButtionSelection");
            }
        }

        private bool _AvatarButtonSelection = false;
        public bool AvatarButtonSelection
        {
            get { return _AvatarButtonSelection; }
            set
            {
                if (_AvatarButtonSelection == value) return;
                _AvatarButtonSelection = value; OnPropertyChanged("AvatarButtonSelection");
            }
        }

        private bool _DialPadButtionSelection = false;
        public bool DialPadButtionSelection
        {
            get { return _DialPadButtionSelection; }
            set
            {
                if (_DialPadButtionSelection == value) return;
                _DialPadButtionSelection = value; OnPropertyChanged("DialPadButtionSelection");
            }
        }

        private bool _MusicVideoSelection = false;
        public bool MusicVideoSelection
        {
            get { return _MusicVideoSelection; }
            set
            {
                if (_MusicVideoSelection == value) return;
                _MusicVideoSelection = value; OnPropertyChanged("MusicVideoSelection");
            }
        }

        private bool _MediaCloudSelection = false;
        public bool MediaCloudSelection
        {
            get { return _MediaCloudSelection; }
            set
            {
                if (_MediaCloudSelection == value) return;
                _MediaCloudSelection = value; OnPropertyChanged("MediaCloudSelection");
            }
        }

        private bool _MediaFeedsSelection = false;
        public bool MediaFeedsSelection
        {
            get { return _MediaFeedsSelection; }
            set
            {
                if (_MediaFeedsSelection == value) return;
                _MediaFeedsSelection = value; OnPropertyChanged("MediaFeedsSelection");
            }
        }

        private bool _NewsPortalSelection = false;
        public bool NewsPortalSelection
        {
            get { return _NewsPortalSelection; }
            set
            {
                if (_NewsPortalSelection == value) return;
                _NewsPortalSelection = value; OnPropertyChanged("NewsPortalSelection");
            }
        }

        private bool _SavedFeedSelection = false;
        public bool SavedFeedSelection
        {
            get { return _SavedFeedSelection; }
            set
            {
                if (_SavedFeedSelection == value) return;
                _SavedFeedSelection = value; OnPropertyChanged("SavedFeedSelection");
            }
        }
        private bool _FollowingSelection = false;
        public bool FollowingSelection
        {
            get { return _FollowingSelection; }
            set
            {
                if (_FollowingSelection == value) return;
                _FollowingSelection = value; OnPropertyChanged("FollowingSelection");
            }
        }

        private bool _WalletSelection = false;
        public bool WalletSelection
        {
            get { return _WalletSelection; }
            set { _WalletSelection = value; OnPropertyChanged("WalletSelection"); }
        }


        private bool _PagesSelection = false;
        public bool PagesSelection
        {
            get { return _PagesSelection; }
            set
            {
                if (_PagesSelection == value) return;
                _PagesSelection = value; OnPropertyChanged("PagesSelection");
            }
        }

        private bool _CelebritesSelection = false;
        public bool CelebritesSelection
        {
            get { return _CelebritesSelection; }
            set
            {
                if (_CelebritesSelection == value) return;
                _CelebritesSelection = value; OnPropertyChanged("CelebritesSelection");
            }
        }

        private bool _CircleButtonSelection = false;
        public bool CircleButtonSelection
        {
            get { return _CircleButtonSelection; }
            set
            {
                if (_CircleButtonSelection == value) return;
                _CircleButtonSelection = value; OnPropertyChanged("CircleButtonSelection");
            }
        }

        private bool _GroupButtonSelection = false;
        public bool GroupButtonSelection
        {
            get { return _GroupButtonSelection; }
            set
            {
                if (_GroupButtonSelection == value) return;
                _GroupButtonSelection = value; OnPropertyChanged("GroupButtonSelection");
            }
        }

        private bool _RoomButtonSelection = false;
        public bool RoomButtonSelection
        {
            get { return _RoomButtonSelection; }
            set
            {
                if (_RoomButtonSelection == value) return;
                _RoomButtonSelection = value; OnPropertyChanged("RoomButtonSelection");
            }
        }

        private bool _StreamAndChannelButtonSelection = false;
        public bool StreamAndChannelButtonSelection
        {
            get { return _StreamAndChannelButtonSelection; }
            set
            {
                if (_StreamAndChannelButtonSelection == value) return;
                _StreamAndChannelButtonSelection = value; OnPropertyChanged("StreamAndChannelButtonSelection");
            }
        }

        private bool _MyChannelButtonSelection = false;
        public bool MyChannelButtonSelection
        {
            get { return _MyChannelButtonSelection; }
            set
            {
                if (_MyChannelButtonSelection == value) return;
                _MyChannelButtonSelection = value; OnPropertyChanged("MyChannelButtonSelection");
            }
        }

        private bool _StickerMarketButtonSelection = false;
        public bool StickerMarketButtonSelection
        {
            get { return _StickerMarketButtonSelection; }
            set
            {
                if (_StickerMarketButtonSelection == value) return;
                _StickerMarketButtonSelection = value; OnPropertyChanged("StickerMarketButtonSelection");
            }
        }


        private bool _MyFeedNoImageFound = false;
        public bool MyFeedNoImageFound
        {
            get { return _MyFeedNoImageFound; }
            set
            {
                if (_MyFeedNoImageFound == value) return;
                _MyFeedNoImageFound = value; OnPropertyChanged("MyFeedNoImageFound");
            }
        }

        private bool _IsNeedToShowInPeopleYouMayKnow = false;
        public bool IsNeedToShowInPeopleYouMayKnow
        {
            get { return _IsNeedToShowInPeopleYouMayKnow; }
            set
            {
                if (value == _IsNeedToShowInPeopleYouMayKnow) return;
                _IsNeedToShowInPeopleYouMayKnow = value; OnPropertyChanged("IsNeedToShowInPeopleYouMayKnow");
            }
        }

        #region My Profile Photos

        private int _ProfileImageCount;
        public int ProfileImageCount
        {
            get { return _ProfileImageCount; }
            set
            {
                if (value == _ProfileImageCount) return;
                _ProfileImageCount = value; this.OnPropertyChanged("ProfileImageCount");
            }
        }

        private int _CoverImageCount;
        public int CoverImageCount
        {
            get { return _CoverImageCount; }
            set
            {
                if (value == _CoverImageCount) return;
                _CoverImageCount = value; this.OnPropertyChanged("CoverImageCount");
            }
        }

        private int _FeedImageCount;
        public int FeedImageCount
        {
            get { return _FeedImageCount; }
            set
            {
                if (value == _FeedImageCount) return;
                _FeedImageCount = value; this.OnPropertyChanged("FeedImageCount");
            }
        }

        private Visibility _IsVisibleProfilePhotosTopLoader = Visibility.Visible;
        public Visibility IsVisibleProfilePhotosTopLoader
        {
            get { return _IsVisibleProfilePhotosTopLoader; }
            set
            {
                if (value == _IsVisibleProfilePhotosTopLoader) return;
                _IsVisibleProfilePhotosTopLoader = value; this.OnPropertyChanged("IsVisibleProfilePhotosTopLoader");
            }
        }

        private Visibility _IsVisibleCoverPhotosTopLoader = Visibility.Visible;
        public Visibility IsVisibleCoverPhotosTopLoader
        {
            get { return _IsVisibleCoverPhotosTopLoader; }
            set
            {
                if (value == _IsVisibleCoverPhotosTopLoader) return;
                _IsVisibleCoverPhotosTopLoader = value; this.OnPropertyChanged("IsVisibleCoverPhotosTopLoader");
            }
        }

        private Visibility _IsVisibleFeedPhotosTopLoader = Visibility.Visible;
        public Visibility IsVisibleFeedPhotosTopLoader
        {
            get { return _IsVisibleFeedPhotosTopLoader; }
            set
            {
                if (value == _IsVisibleFeedPhotosTopLoader) return;
                _IsVisibleFeedPhotosTopLoader = value; this.OnPropertyChanged("IsVisibleFeedPhotosTopLoader");
            }
        }

        private Guid _audioAlbumNpUUId = Guid.Empty;
        public Guid AudioAlbumNpUUId
        {
            get { return _audioAlbumNpUUId; }
            set
            {
                if (_audioAlbumNpUUId == value) return;
                _audioAlbumNpUUId = value; OnPropertyChanged("AudioAlbumNpUUId");
            }
        }

        private Guid _videoAlbumNpUUId = Guid.Empty;
        public Guid VideoAlbumNpUUId
        {
            get { return _videoAlbumNpUUId; }
            set
            {
                if (_videoAlbumNpUUId == value) return;
                _videoAlbumNpUUId = value; OnPropertyChanged("VideoAlbumNpUUId");
            }
        }

        private Guid _imageAlbumNpUUId = Guid.Empty;
        public Guid ImageAlbumNpUUId
        {
            get
            {
                return _imageAlbumNpUUId;
            }
            set
            {
                if (value == _imageAlbumNpUUId) return;
                _imageAlbumNpUUId = value; this.OnPropertyChanged("ImageAlbumNpUUId");
            }
        }

        #endregion

        private int _MaxFeedColumn = 2;//1=feed 1, 2=feed 2, 3=feed 3
        public int MaxFeedColumn
        {
            get { return _MaxFeedColumn; }
            set
            {
                if (_MaxFeedColumn == value) return;
                _MaxFeedColumn = value; this.OnPropertyChanged("MaxFeedColumn");
            }
        }

        private int _NumberOfFeedColumn;
        public int NumberOfFeedColumn
        {
            get { return _NumberOfFeedColumn; }
            set
            {
                if (_NumberOfFeedColumn == value) return;
                _NumberOfFeedColumn = value; this.OnPropertyChanged("NumberOfFeedColumn");
            }
        }

        private bool _IsFetchingSticker = false;
        public bool IsFetchingSticker
        {
            get { return _IsFetchingSticker; }
            set
            {
                if (_IsFetchingSticker == value) return;
                _IsFetchingSticker = value; this.OnPropertyChanged("IsFetchingSticker");
            }
        }

        private bool _IsSuggesFetchingCompleted = false;
        public bool IsSuggesFetchingCompleted
        {
            get { return _IsSuggesFetchingCompleted; }
            set
            {
                if (_IsSuggesFetchingCompleted == value) return;
                _IsSuggesFetchingCompleted = value; this.OnPropertyChanged("IsSuggesFetchingCompleted");
            }
        }

        private bool _IsFetchingStickerCompleted = false;
        public bool IsFetchingStickerCompleted
        {
            get { return _IsFetchingStickerCompleted; }
            set
            {
                if (_IsFetchingStickerCompleted == value) return;
                _IsFetchingStickerCompleted = value; this.OnPropertyChanged("IsFetchingStickerCompleted");
            }
        }

        private bool _IsInternetAvailable = true;
        public bool IsInternetAvailable
        {
            get { return _IsInternetAvailable; }
            set
            {
                if (_IsInternetAvailable == value) return;
                _IsInternetAvailable = value; this.OnPropertyChanged("IsInternetAvailable");
            }
        }

        public ConcurrentDictionary<string, ObservableCollection<RecentModel>> RecentModelList
        {
            get { return _RecentModelList; }
            set { _RecentModelList = value; }
        }

        public ObservableDictionary<long, GroupInfoModel> GroupList
        {
            get { return _GroupList; }
            set { _GroupList = value; }
        }

        public RoomRequestModel RoomListFetchQuery
        {
            get { return _RoomListFetchQuery; }
            set { _RoomListFetchQuery = value; }
        }

        public ObservableDictionary<string, RoomModel> RoomList
        {
            get { return _RoomList; }
            set { _RoomList = value; }
        }

        public RoomRequestModel RoomListSearchQuery
        {
            get { return _RoomListSearchQuery; }
            set { _RoomListSearchQuery = value; }
        }

        public ObservableDictionary<string, RoomModel> RoomSearchList
        {
            get { return _RoomSearchList; }
            set { _RoomSearchList = value; }
        }

        public ObservableCollection<RoomCategoryModel> RoomCategoryList
        {
            get { return _RoomCategoryList; }
            set { _RoomCategoryList = value; }
        }

        public ConcurrentDictionary<long, BlockedNonFriendModel> BlockedNonFriendList
        {
            get { return _BlockedNonFriendList; }
            set { _BlockedNonFriendList = value; }
        }

        public ObservableDictionary<string, ChatBgHistoryModel> ChatBackgroundList
        {
            get { return _ChatBackgroundList; }
            set { _ChatBackgroundList = value; }
        }

        public ICommand ExpandCommand
        {
            get
            {
                if (_ExpandCommand == null) _ExpandCommand = new RelayCommand(param => { if (!RingIDViewModel.Instance.IsExpanded)  UCGuiRingID.Instance.DoExpandCollapseWindow(param); });
                return _ExpandCommand;
            }
        }

        public ICommand CollapseCommand
        {
            get
            {
                if (_CollapseCommand == null) _CollapseCommand = new RelayCommand(param => { if (RingIDViewModel.Instance.IsExpanded)  UCGuiRingID.Instance.DoExpandCollapseWindow(param); });
                return _CollapseCommand;
            }
        }

        public ICommand ShowFriendListOrDialpadCommand
        {
            get
            {
                if (_ShowFriendListOrDialpadCommand == null) _ShowFriendListOrDialpadCommand = new RelayCommand(param => ShowFriendListOrDialpad(param));
                return _ShowFriendListOrDialpadCommand;
            }
        }

        public ICommand MyProfileClickedCommand
        {
            get
            {
                if (_MyProfileClickedCommand == null) _MyProfileClickedCommand = new RelayCommand(param => OnMyProfileClicked(param));
                return _MyProfileClickedCommand;
            }
        }


        public ICommand GroupListCommand
        {
            get
            {
                if (_GroupListCommand == null) _GroupListCommand = new RelayCommand(param => OnGroupListClicked(param));
                return _GroupListCommand;
            }
        }

        public ICommand RoomListCommand
        {
            get
            {
                if (_RoomListCommand == null) _RoomListCommand = new RelayCommand(param => OnRoomListClicked(param));
                return _RoomListCommand;
            }
        }

        public ICommand MyChannelCommand
        {
            get
            {
                if (_MyChannelCommand == null) _MyChannelCommand = new RelayCommand(param => OnMyChannelCommand(param));
                return _MyChannelCommand;
            }
        }

        public ICommand AvatarCommand
        {
            get
            {
                if (_AvatarCommand == null) _AvatarCommand = new RelayCommand(param => OnAvatarClicked(param));
                return _AvatarCommand;
            }
        }

        public ICommand FriendProfileCommand
        {
            get
            {
                if (_FriendProfileCommand == null) _FriendProfileCommand = new RelayCommand(param => OnFriendProfileButtonClicked(PassUserTableIdByWrapper(param)));
                return _FriendProfileCommand;
            }
        }

        public ICommand FriendInformationCommand
        {
            get
            {
                if (_FriendInformationCommand == null) _FriendInformationCommand = new RelayCommand(param => OnFriendProfileInfoButtonClicked(PassUserTableIdByWrapper(param)));
                return _FriendInformationCommand;
            }
        }

        public ICommand FriendCallChatCommand
        {
            get
            {
                if (_FriendCallChatCommand == null) _FriendCallChatCommand = new RelayCommand(param => OnFriendCallChatButtonClicked(PassUserTableIdByWrapper(param)));
                return _FriendCallChatCommand;
            }
        }

        public ICommand FriendChatSettingsCommand
        {
            get
            {
                if (_FriendChatSettingsCommand == null) _FriendChatSettingsCommand = new RelayCommand(param => OnFriendChatSettingsButtonClicked(PassUserTableIdByWrapper(param)));
                return _FriendChatSettingsCommand;
            }
        }

        public ICommand VideoCallCommand
        {
            get
            {
                if (_VideoCallCommand == null) _VideoCallCommand = new RelayCommand(param => OnVideoCallButtonClicked(PassUserTableIdByWrapper(param)));
                return _VideoCallCommand;
            }
        }

        public ICommand VoiceCallCommand
        {
            get
            {
                if (_VoiceCallCommand == null) _VoiceCallCommand = new RelayCommand(param => OnVoiceCallButtonClicked(PassUserTableIdByWrapper(param)));
                return _VoiceCallCommand;
            }
        }

        public ICommand GroupChatSettingsCommand
        {
            get
            {
                if (_GroupChatSettingsCommand == null) _GroupChatSettingsCommand = new RelayCommand(param => OnGroupChatSettingsButtonClicked(param));
                return _GroupChatSettingsCommand;
            }
        }

        public ICommand GroupCallChatCommand
        {
            get
            {
                if (_GroupCallChatCommand == null) _GroupCallChatCommand = new RelayCommand(param => OnGroupCallChatButtonClicked(param));
                return _GroupCallChatCommand;
            }
        }

        public ICommand GroupCreateCommand
        {
            get
            {
                if (_GroupCreateCommand == null) _GroupCreateCommand = new RelayCommand(param => OnGroupCreateButtonClicked(param));
                return _GroupCreateCommand;
            }
        }

        public ICommand RoomCallChatCommand
        {
            get
            {
                if (_RoomCallChatCommand == null) _RoomCallChatCommand = new RelayCommand(param => OnRoomCallChatButtonClicked(param));
                return _RoomCallChatCommand;
            }
        }

        public ICommand RoomChatSettingsCommand
        {
            get
            {
                if (_RoomChatSettingsCommand == null) _RoomChatSettingsCommand = new RelayCommand(param => OnRoomChatSettingsButtonClicked(param));
                return _RoomChatSettingsCommand;
            }
        }

        public ICommand RoomMemberListCommand
        {
            get
            {
                if (_RoomMemberListCommand == null) _RoomMemberListCommand = new RelayCommand(param => OnRoomMemberListButtonClicked(param));
                return _RoomMemberListCommand;
            }
        }

        public ICommand ProfileImageRightClickedActions
        {
            get
            {
                if (_ProfileImageRightClickedActions == null) _ProfileImageRightClickedActions = new RelayCommand(param => ImageUtility.OnProfileImageClickedActions(param));
                return _ProfileImageRightClickedActions;
            }
        }

        public ICommand CoverImageRightClickedActions
        {
            get
            {
                if (_CoverImageRightClickedActions == null) _CoverImageRightClickedActions = new RelayCommand(param => ImageUtility.OnCoverImageClicked(param));
                return _CoverImageRightClickedActions;
            }
        }

        public ICommand MediaPlayCommand
        {
            get
            {
                if (_MediaPlayCommand == null) _MediaPlayCommand = new RelayCommand(param => OnMediaPlayerClickCommand(param));
                return _MediaPlayCommand;
            }
        }

        public ICommand CommandHidePage
        {
            get
            {
                if (_HidePage == null) _HidePage = new RelayCommand(param => OnHidePageClickCommand(param));
                return _HidePage;
            }
        }

        public ICommand CommandEditPage
        {
            get
            {
                if (_EditPage == null) _EditPage = new RelayCommand(param => OnEditPageClickCommand(param));
                return _EditPage;
            }
        }

        public ICommand AddFriendCommand
        {
            get
            {
                if (_AddFriendCommand == null) _AddFriendCommand = new RelayCommand(param => OnAddFriendClicked(param));
                return _AddFriendCommand;
            }
        }
        public ICommand AcceptCommand
        {
            get
            {
                if (_AcceptCommand == null) _AcceptCommand = new RelayCommand(param => OnAcceptButtonClicked(param));
                return _AcceptCommand;
            }
        }
        public ICommand RejectCommand
        {
            get
            {
                if (_RejectCommand == null) _RejectCommand = new RelayCommand(param => OnRejectButtonClicked(param));
                return _RejectCommand;
            }
        }
        #endregion Property

        #region Utility Method
        public void ChangeMyStatusIcon(int mood)
        {
            try
            {
                MyBasicInfoModel.ShortInfoModel.Mood = mood;
                MyBasicInfoModel.ShortInfoModel.OnPropertyChanged("Mood");
            }
            catch (Exception e) { log.Error("Exception in AuthHandler ==>" + e.StackTrace + "==>" + e.Message); }
        }

        public void ReloadMyProfile()
        {
            try
            {
                if (MyBasicInfoModel != null)
                {
                    MyBasicInfoModel.LoadData(DefaultSettings.userProfile);
                    MyBasicInfoModel.OnPropertyChanged("CurrentInstance");
                    MyBasicInfoModel.ShortInfoModel.OnPropertyChanged("CurrentInstance");
                    if (_WNRingIDSettings != null && _WNRingIDSettings._UCGeneralSettings != null)
                        _WNRingIDSettings._UCGeneralSettings.SetSelectedModelValuesForNotificationValidityPopup();
                }
            }
            catch (Exception ex) { log.Error("Error: ReloadMyProfile() => " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void ShowFriendListOrDialpad(object param)
        {
            try
            {
                if (param != null && (bool)param) OnDialPadClicked(null);
                else
                {
                    UCFriendTabControlPanel.Instance.LeftSideTabControl.SelectedIndex = 0;
                    UCGuiRingID.Instance.SearchTermTextBox.Focus();
                }
            }
            catch (Exception ex) { log.Error("Error: OnFriendListClicked() ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        public MediaContentModel GetMyAlbumModelFromAlbumID(Guid albumId, int mediaType = 0)
        {
            MediaContentModel model = null;
            if (mediaType == SettingsConstants.MEDIA_TYPE_AUDIO) model = MyAudioAlbums.Where(P => P.AlbumId == albumId).FirstOrDefault();
            else if (mediaType == SettingsConstants.MEDIA_TYPE_VIDEO) model = MyVideoAlbums.Where(P => P.AlbumId == albumId).FirstOrDefault();
            else
            {
                model = MyAudioAlbums.Where(P => P.AlbumId == albumId).FirstOrDefault();
                if (model == null) model = MyVideoAlbums.Where(P => P.AlbumId == albumId).FirstOrDefault();
            }
            return model;
        }

        public void OnAddFriendClicked(object parameter)
        {
            try
            {
                UserBasicInfoModel model = (UserBasicInfoModel)parameter;
                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(model.ShortInfoModel.UserTableID);
                int accessType = -1;
                if (!HelperMethods.HasFriendChatPermission(model, blockModel, out accessType))
                {
                    if (HelperMethods.ShowBlockWarning(model, accessType)) return;
                }
                HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, model, blockModel, (status) =>
                {
                    if (status == false) return 0;
                    new ThrdAddFriend().StartProcess(model);
                    return 1;
                }, true);
            }
            catch (Exception ex) { log.Error("Error: OnAddFriendButtonClicked() ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void OnAcceptButtonClicked(object parameter)
        {
            try
            {
                UserBasicInfoModel model = (UserBasicInfoModel)parameter;
                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(model.ShortInfoModel.UserTableID);

                int accessType = -1;
                if (!HelperMethods.HasFriendChatPermission(model, blockModel, out accessType))
                {
                    if (HelperMethods.ShowBlockWarning(model, accessType)) return;
                }
                HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, model, blockModel, (status) =>
                {
                    if (status == false) return 0;
                    if (MainSwitcher.ThreadManager().thradAcceptFriendRequest == null) MainSwitcher.ThreadManager().thradAcceptFriendRequest = new ThradAcceptFriendRequest();
                    MainSwitcher.ThreadManager().thradAcceptFriendRequest.StartThread(model);
                    return 1;
                }, true);
            }
            catch (Exception ex) { log.Error("Error: OnAddFriendButtonClicked() ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void OnRejectButtonClicked(object parameter)
        {
            if (parameter is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)parameter;
                //MessageBoxResult result;
                //if (model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED) result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.REMOVE_FROM_FRIEND, model.ShortInfoModel.FullName));
                //else
                //{
                //    string msg = model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING ? NotificationMessages.CANCEL_OUTGOING_REQUEST : NotificationMessages.CANCEL_INCOMING_REQUEST;
                //    result = CustomMessageBox.ShowQuestion(msg);
                //}
                //if (result == MessageBoxResult.Yes) (new DeleteFriend()).StartProcess(model);
                string dialog = string.Empty;
                if (model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                    dialog = string.Format(NotificationMessages.REMOVE_FROM_FRIEND, model.ShortInfoModel.FullName);
                else
                    dialog = model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING ? NotificationMessages.CANCEL_OUTGOING_REQUEST : NotificationMessages.CANCEL_INCOMING_REQUEST;
                bool isTrue = UIHelperMethods.ShowQuestion(dialog, string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
                if (isTrue)
                {
                    (new DeleteFriend()).StartProcess(model);
                }
            }
        }
<<<<<<< HEAD
=======

>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        public void OnMediaPlayerClickCommand(object parameter)
        {
        }

        public void OnMyProfileClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyProfile);
            UIHelperMethods.FocusMainWindow();
        }

        public void OnAllFeedsClicked(object parameter)
        {
            bool IsSameUI = RingIDViewModel.Instance.SelectedMainMiddlePanel == MiddlePanelConstants.TypeAllFeeds;
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeAllFeeds);
            NewsFeedViewModel.Instance.OnHomeRequestandRemoveBottomModels(IsSameUI);
            //if (RingIDViewModel.Instance.FeedUnreadNotificationCounter > 0) NewsFeedViewModel.Instance.TopLoadUnreadAllFeeds();
        }

        public void OnHidePageClickCommand(object parameter)
        {
            long utIdorPid = 0;
            BaseUserProfileModel userProfileModel = null;
            bool isProfileHidden = true;
            if (parameter is UCNewsPortalProfile)
            {
                UCNewsPortalProfile profile = (UCNewsPortalProfile)parameter;
                userProfileModel = profile.NewsPortalModel;
                utIdorPid = profile.NewsPortalModel.PortalId;
                isProfileHidden = profile.NewsPortalModel.IsProfileHidden;
                //if (profile.NewsPortalModel.IsProfileHidden) MainSwitcher.ThreadManager().HideUser.StartThread(profile.NewsPortalModel.PortalId, profile.NewsPortalModel, false);
                //else
                //{
                //    MessageBoxResult result = CustomMessageBox.ShowWarning(msg, "", true);
                //    if (result == MessageBoxResult.OK) MainSwitcher.ThreadManager().HideUser.StartThread(profile.NewsPortalModel.PortalId, profile.NewsPortalModel);
                //}
            }
            else if (parameter is UCPageProfile)
            {
                UCPageProfile profile = (UCPageProfile)parameter;
                userProfileModel = profile.PageModel;
                utIdorPid = profile.PageModel.PageId;
                isProfileHidden = profile.PageModel.IsProfileHidden;
                //if (profile.PageModel.IsProfileHidden) MainSwitcher.ThreadManager().HideUser.StartThread(profile.PageModel.PageId, profile.PageModel, false);
                //else
                //{
                //    MessageBoxResult result = CustomMessageBox.ShowWarning(msg, "", true);
                //    if (result == MessageBoxResult.OK) MainSwitcher.ThreadManager().HideUser.StartThread(profile.PageModel.PageId, profile.PageModel);
                //}
            }
            else if (parameter is UCMediaPageProfile)
            {
                UCMediaPageProfile profile = (UCMediaPageProfile)parameter;
                userProfileModel = profile.MediaPageModel;
                utIdorPid = profile.MediaPageModel.MusicPageId;
                isProfileHidden = profile.MediaPageModel.IsProfileHidden;
                //if (profile.MediaPageModel.IsProfileHidden) MainSwitcher.ThreadManager().HideUser.StartThread(profile.MediaPageModel.MusicPageId, profile.MediaPageModel, false);
                //else
                //{
                //    MessageBoxResult result = CustomMessageBox.ShowWarning(msg, "", true);
                //    if (result == MessageBoxResult.OK) MainSwitcher.ThreadManager().HideUser.StartThread(profile.MediaPageModel.MusicPageId, profile.MediaPageModel);
                //}
            }

            if (userProfileModel != null && utIdorPid > 0)
            {
                if (isProfileHidden)
                    MainSwitcher.ThreadManager().HideUser.StartThread(utIdorPid, userProfileModel, false);
                else
                {
                    bool isTrue = UIHelperMethods.ShowOKCancel("You want to hide this profile?", "You won't get updates from this profile!", "Hide confirmation");
                    if (isTrue)
                    {
                        MainSwitcher.ThreadManager().HideUser.StartThread(utIdorPid, userProfileModel);
                    }
                    //MessageBoxResult result = CustomMessageBox.ShowWarning(msg, "", true);
                    //if (result == MessageBoxResult.OK) MainSwitcher.ThreadManager().HideUser.StartThread(utIdorPid, userProfileModel);
                }
            }
        }

        public void OnEditPageClickCommand(object parameter)
        {
            if (parameter is UCNewsPortalProfile)
            {
                UCNewsPortalProfile profile = (UCNewsPortalProfile)parameter;
                if (UCFollowingUnfollowingView.Instance == null)
                {
                    UCFollowingUnfollowingView.Instance = new UCFollowingUnfollowingView(UCGuiRingID.Instance.MotherPanel);
                    UCFollowingUnfollowingView.Instance.Show();
                    UCFollowingUnfollowingView.Instance.ShowFollowUnFollow(profile.NewsPortalModel, true);
                    UCFollowingUnfollowingView.Instance.OnRemovedUserControl += () =>
                    {
                        UCFollowingUnfollowingView.Instance = null;
                    };
                }
            }
            else if (parameter is UCPageProfile)
            {
                UCPageProfile profile = (UCPageProfile)parameter;
            }
            else if (parameter is UCMediaPageProfile)
            {
                UCMediaPageProfile profile = (UCMediaPageProfile)parameter;
            }
        }

        public void OnDialPadClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeDialer);
        }
        public void OnCircleListClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCircleMain);
        }
        public void OnGroupListClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeGroupList);
        }

        public void OnAvatarClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeAddFriend);
        }
        private long PassUserTableIdByWrapper(object param)
        {

            long userTableId = 0;
            if (param is UserBasicInfoModel)
            {
                UserBasicInfoModel model = (UserBasicInfoModel)param;
                userTableId = model.ShortInfoModel.UserTableID;
                if (!FriendDictionaries.Instance.IfExistInBasicInfoDictionary(userTableId)) FriendDictionaries.Instance.AddIntoBasicInfoDictionary(userTableId, model.basicInfoDTO);
            }
            else if (param is NotificationModel)
            {
                NotificationModel model = (NotificationModel)param;
                userTableId = model.UserShortInfoModel.UserTableID;
            }
            else if (param is RecentModel)
            {
                RecentModel model = (RecentModel)param;
                if (model.Message != null) userTableId = model.Message != null && model.Message.FriendTableID > 0 ? model.Message.FriendTableID : 0;
                else if (model.CallLog != null) userTableId = model.CallLog.FriendTableID;
            }
            else if (param is long) userTableId = (long)param;
            return userTableId;
        }
        public void OnFriendProfileButtonClicked(object parameter)
        {
            dynamic state = new ExpandoObject();
            state.UtID = parameter;
            state.type = "profile";
            state.TabIndx = 0;
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeFriendProfile, state);
            UIHelperMethods.FocusMainWindow();
        }
        public void OnFriendProfileButtonClickedByUTID(object parameter)
        {
            dynamic state = new ExpandoObject();
            state.UtID = parameter;
            state.type = "profile";
            state.TabIndx = 0;
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeFriendProfile, state);
            UIHelperMethods.FocusMainWindow();
        }
        public void OnFriendProfileInfoButtonClicked(object parameter)
        {
            dynamic state = new ExpandoObject();
            state.UtID = parameter;
            state.type = "info";
            state.TabIndx = 5;
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeFriendProfile, state);
            UIHelperMethods.FocusMainWindow();
        }
        public void OnFriendCallChatButtonClicked(object parameter)
        {
            if (UCNameToolTipPopupView.Instance != null && UCNameToolTipPopupView.Instance.PopupToolTip.IsOpen) UCNameToolTipPopupView.Instance.HidePopUp(true);
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeFriendChatCall, parameter);
        }
        public void OnFriendChatSettingsButtonClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeFriendChatSettings, parameter);
            UIHelperMethods.FocusMainWindow();
        }

        public void OnUserProfileClicked(BaseUserProfileModel profileModel)
        {
            if (UCNameToolTipPopupView.Instance != null && UCNameToolTipPopupView.Instance.PopupToolTip.IsOpen)
                UCNameToolTipPopupView.Instance.HidePopUp(true);
            if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                UCSingleFeedDetailsView.Instance.Hide();
            switch (profileModel.ProfileType)
            {
                case SettingsConstants.PROFILE_TYPE_GENERAL:
                    if (profileModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID) MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyProfile);
                    else
                    {
                        dynamic state = new ExpandoObject();
                        state.UtID = profileModel.UserTableID;
                        state.type = "profile";
                        state.TabIndx = 0;
                        MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeFriendProfile, state);
                    }
                    UIHelperMethods.FocusMainWindow();
                    break;
                //case SettingsConstants.PROFILE_TYPE_CELEBRITY:
                //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCelebrityProfile, profileModel);
                //    break;
                //case SettingsConstants.PROFILE_TYPE_PAGES:
                //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypePageProfile, profileModel);
                //    break;
                case SettingsConstants.PROFILE_TYPE_MUSICPAGE:
                    MusicPageModel musicmodel = new MusicPageModel { FullName = profileModel.FullName, UserTableID = profileModel.UserTableID, ProfileImage = profileModel.ProfileImage, ProfileType = SettingsConstants.PROFILE_TYPE_MUSICPAGE };
<<<<<<< HEAD
=======
                    UserProfilesContainer.Instance.MediaPageModels[profileModel.UserTableID] = musicmodel;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                    //if (!UserProfilesContainer.Instance.MediaPageModels.TryGetValue(profileModel.UserTableID, out musicmodel))
                    //{
                    //    musicmodel = new MusicPageModel { FullName = profileModel.FullName, UserTableID = profileModel.UserTableID, ProfileImage = profileModel.ProfileImage, ProfileType = SettingsConstants.PROFILE_TYPE_MUSICPAGE };
                    //    UserProfilesContainer.Instance.MediaPageModels[profileModel.UserTableID] = musicmodel;
                    //}
                    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMediaCloudProfile, musicmodel);
                    break;
                case SettingsConstants.PROFILE_TYPE_NEWSPORTAL:
                    NewsPortalModel newsportalmodel = new NewsPortalModel { FullName = profileModel.FullName, UserTableID = profileModel.UserTableID, ProfileImage = profileModel.ProfileImage, ProfileType = SettingsConstants.PROFILE_TYPE_NEWSPORTAL };
<<<<<<< HEAD
=======
                    UserProfilesContainer.Instance.NewsPortalModels[profileModel.UserTableID] = newsportalmodel;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                    //if (!UserProfilesContainer.Instance.NewsPortalModels.TryGetValue(profileModel.UserTableID, out newsportalmodel))
                    //{
                    //    newsportalmodel = new NewsPortalModel { FullName = profileModel.FullName, UserTableID = profileModel.UserTableID, ProfileImage = profileModel.ProfileImage, ProfileType = SettingsConstants.PROFILE_TYPE_NEWSPORTAL };
                    //    UserProfilesContainer.Instance.NewsPortalModels[profileModel.UserTableID] = newsportalmodel;
                    //}
                    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeNewsPortalProfile, newsportalmodel);
                    break;
                //case SettingsConstants.PROFILE_TYPE_CIRCLE:
                //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCirclePanel, profileModel);
                //    break;
                default:
                    break;
            }
            if (UCNameToolTipPopupView.Instance != null && UCNameToolTipPopupView.Instance.PopupToolTip.IsOpen)
                UCNameToolTipPopupView.Instance.HidePopUp(true);
        }

        //public void OnNewsPortalButtonClicked(object parameter)
        //{
        //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeNewsPortalProfile, parameter);
        //}

        //public void OnPageProfileButtonClicked(object parameter)
        //{
        //    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypePageProfile, parameter);
        //}

        public void OnCelebrityProfleButtonClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCelebrityProfile, parameter);
        }

        public void OnMediaPageProfileButtonClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMediaCloudProfile, parameter);
        }

        public void OnGroupChatSettingsButtonClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeGroupChatSettings, parameter);
            UIHelperMethods.FocusMainWindow();
        }

        public void OnGroupCallChatButtonClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeGroupChatCall, parameter);
        }

        public void OnGroupCreateButtonClicked(object parameter)
        {
            ChatHelpers.CreateGroup(null, NotificationMessages.GROUP_NAME_ENTER_NEW_GROUP_MESSAGE);
        }

        public void OnRoomListClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeRoomList);
        }

        public void OnStreamAndChannelCommand(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeStreamAndChannel, parameter);
        }

        public void OnMyChannelCommand(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeMyChannel, parameter);
        }

        public void OnRoomCallChatButtonClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeRoomChat, parameter);
        }

        public void OnRoomChatSettingsButtonClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeRoomChatSettings, parameter);
            UIHelperMethods.FocusMainWindow();
        }

        public void OnRoomMemberListButtonClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeRoomMemberList, parameter);
        }

        public void OnVoiceCallButtonClicked(object parameter)
        {
            //if ((long)parameter > 0 && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
            //{
            //    if (MainSwitcher.CallController.CallData == null)
            //    {
            //        dynamic state = new ExpandoObject();
            //        state.UtId = parameter;
            //        MainSwitcher.CallController.CallNow(state.UtId, callsdkwrapper.CallMediaType.Voice);// VoiceConstants.CALL_TYPE_VOICE
            //        UIHelperMethods.FocusMainWindow();
            //    }
            //    else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.ALREADY_IN_CALL);
            //}
            //else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.CAN_NOT_CALL_RIGHT_NOW);
            if (CallStates.CurrentCallState == CallStates.UA_IDLE)
            {
                dynamic state = new ExpandoObject();
                state.UtId = parameter;
                CallerDTO callData = new CallerDTO();
                callData.calT = (int)callsdkwrapper.CallMediaType.Voice;
                callData.UserTableID = state.UtId;
                callData.CallID = SendToServer.GetRanDomPacketID(true);
<<<<<<< HEAD
                callData.IsIncomming = false;
=======
                CallStates.IsIncomming = false;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                CallHelperMethods.ShowCallUiInMainView(callData, callsdkwrapper.CallMediaType.Voice);
            }
            else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.ALREADY_IN_CALL);
        }

        public void OnVideoCallButtonClicked(object parameter)
        {
            //if (CallHelperMethods.IsVideoDeviceExists())
            //{
            //    if ((long)parameter > 0 && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
            //    {
            //        if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallData == null)
            //        {
            //            dynamic state = new ExpandoObject();
            //            state.UtId = parameter;
            //            if (state.UtId != DefaultSettings.LOGIN_TABLE_ID)
            //            {
            //                MainSwitcher.CallController.CallNow(state.UtId, callsdkwrapper.CallMediaType.Video);//VoiceConstants.CALL_TYPE_VIDEO
            //                UIHelperMethods.FocusMainWindow();
            //            }
            //        }
            //        else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.ALREADY_IN_CALL);
            //    }
            //    else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.CAN_NOT_CALL_RIGHT_NOW);
            //}
            //else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.NO_WEBCAM);

            if (CallStates.CurrentCallState == CallStates.UA_IDLE)
            {
                if (CallHelperMethods.IsVideoDeviceExists())
                {
                    if ((long)parameter > 0 && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                    {
                        dynamic state = new ExpandoObject();
                        state.UtId = parameter;
                        if (state.UtId != DefaultSettings.LOGIN_TABLE_ID)
                        {
                            //MainSwitcher.CallController.CallNow(state.UtId, callsdkwrapper.CallMediaType.Video);//VoiceConstants.CALL_TYPE_VIDEO
                            //UIHelperMethods.FocusMainWindow();
                            CallerDTO callData = new CallerDTO();
                            callData.calT = (int)callsdkwrapper.CallMediaType.Video;
                            callData.UserTableID = state.UtId;
                            callData.CallID = SendToServer.GetRanDomPacketID(true);
<<<<<<< HEAD
                            callData.IsIncomming = true;
=======
                            CallStates.IsIncomming = true;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                            CallHelperMethods.ShowCallSeparateWindow(callData, callsdkwrapper.CallMediaType.Video);
                        }
                    }
                    else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.CAN_NOT_CALL_RIGHT_NOW);
                }
                else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.NO_WEBCAM);
            }
            else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.ALREADY_IN_CALL);
        }

        public void SetCallDataModel(CallerDTO dto)
        {
            UserBasicInfoModel model = GetUserBasicInfoModelFromServerNotNullable(dto.UserTableID, 0, dto.FullName);
            if (VMCall == null)
            {
                VMCall = new VMCall();
                VMCall.FriendProfileSourceMini = ImageUtility.GetDetaultProfileImage(model.ShortInfoModel);
                VMCall.FriendProfileSource = ImageUtility.GetDetaultProfileImage(model.ShortInfoModel, 150, 150);
                VMCall.MyProfileImge = ImageUtility.GetDetaultProfileImage(MyBasicInfoModel.ShortInfoModel, 150, 150);
                VMCall.UserBasicInfoModel = model;
                if (dto.calT == (int)callsdkwrapper.CallMediaType.Video) VMCall.IsVideoCall = true;
            }
        }

        public void LoadMailMobileCountryCodeList()
        {
            CountryModelList = new ObservableCollection<CountryCodeModel>();
            for (int i = 2; i < DefaultSettings.COUNTRY_MOBILE_CODE.Length / 2; i++)
            {
                string name = DefaultSettings.COUNTRY_MOBILE_CODE[i, 0];
                string code = DefaultSettings.COUNTRY_MOBILE_CODE[i, 1];
                CountryModelList.Add(new CountryCodeModel(name, code));
            }
        }
        public void LoadDoingModels()
        {
            if (DoingListForNewStatus.Count == 0)
            {
                List<DoingDTO> list = NewsFeedDictionaries.Instance.DOING_DICTIONARY.Values.ToList();
                list = list.OrderBy(p => p.SortId).ToList();
                foreach (var item in list)
                {
                    DoingModel model = new DoingModel();
                    model.LoadData(item);
                    DoingListForNewStatus.Add(model);
                }
            }
        }

        public ObservableCollection<RecentModel> GetRecentModelListByID(long id)
        {
            return GetRecentModelListByID(id.ToString());
        }

        public ObservableCollection<RecentModel> GetRecentModelListByID(string id)
        {
            ObservableCollection<RecentModel> recentModelList = null;
            if (!RecentModelList.TryGetValue(id, out recentModelList) || recentModelList == null)
            {
                recentModelList = new ObservableCollection<RecentModel>();
                RecentModelList[id] = recentModelList;
            }
            return recentModelList;
        }

        public BlockedNonFriendModel GetBlockedNonFriendModelByID(long id)
        {
            BlockedNonFriendModel model = BlockedNonFriendList.TryGetValue(id);
            if (model == null)
            {
                model = new BlockedNonFriendModel { UserTableID = id };
                BlockedNonFriendList[id] = model;
            }
            return model;
        }

        public RoomModel GetRoomModelByRoomID(string roomID, string roomName = "Untitled")
        {
            RoomModel model = RingIDViewModel.Instance.RoomList.TryGetValue(roomID);
            if (model == null)
            {
                RoomDTO roomDTO = new RoomDTO();
                roomDTO.RoomID = roomID;
                roomDTO.RoomName = roomName.ToString();
                roomDTO.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
                roomDTO.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
                roomDTO.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
                model = new RoomModel(roomDTO);
                model.IsPartial = true;
                RingIDViewModel.Instance.RoomList[roomID] = model;
            }
            return model;
        }

        public ObservableDictionary<int, string> GetSpamReasonListByType(int type)
        {
            ObservableDictionary<int, string> list = RingIDViewModel.Instance.SpamReasonList.TryGetValue(type);
            if (list == null)
            {
                list = new ObservableDictionary<int, string>();
                RingIDViewModel.Instance.SpamReasonList[type] = list;
            }
            return list;
        }

        public UserBasicInfoModel GetUserBasicInfoModelFromServerNotNullable(long userTableID, long ringID = 0, string fullname = "", string profileImage = "")
        {
            UserBasicInfoModel model = null;
            if (MyBasicInfoModel.ShortInfoModel.UserTableID == userTableID)
            {
                model = MyBasicInfoModel;
            }
            if (model == null)
            {
                model = GetFriendBasicInfoModelNullable(userTableID);
                if (model == null)
                {
                    model = new UserBasicInfoModel();
                    model.ShortInfoModel.UserTableID = userTableID;
                    model.ShortInfoModel.FullName = fullname;
                    model.ShortInfoModel.FriendShipStatus = 0;

                    UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userTableID);
                    if (dto == null)
                    {
                        dto = new UserBasicInfoDTO { UserTableID = userTableID, RingID = ringID, FullName = fullname, ProfileImage = profileImage };
                        FriendDictionaries.Instance.AddOrUPdateInBasicInfoDictionary(userTableID, dto, true);
                        //   FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[utId] = dto;
                        model = new UserBasicInfoModel();
                        model.ShortInfoModel.UserTableID = dto.UserTableID;
                        model.ShortInfoModel.UserIdentity = dto.RingID;
                        model.ShortInfoModel.FullName = dto.FullName;
                        model.ShortInfoModel.ProfileImage = dto.ProfileImage;
                        model.ShortInfoModel.ImSoundEnabled = dto.ImSoundEnabled;
                        model.ShortInfoModel.ImNotificationEnabled = dto.ImNotificationEnabled;
                        model.ShortInfoModel.ImPopupEnabled = dto.ImPopupEnabled;
                        new ThradUnknownProfileInfoRequest().StartThread(userTableID, true);
                    }
                    else
                    {
                        model = new UserBasicInfoModel(dto);
                    }
                    model.ShortInfoModel.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_UNKNOWN;
                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                }
            }
            return model;
        }

        public UserShortInfoModel GetUserShortInfoModelFromServerNotNullable(long userTableID, long ringID = 0, string fullname = "", string profileImage = "")
        {
            return GetUserBasicInfoModelFromServerNotNullable(userTableID, ringID, fullname, profileImage).ShortInfoModel;
        }

        public UserBasicInfoModel GetUserBasicInfoModelNotNullable(long userTableID, long ringID = 0, string fullname = "", string profileImage = "")
        {

            UserBasicInfoModel model = null;
            if (MyBasicInfoModel.ShortInfoModel.UserTableID == userTableID) model = MyBasicInfoModel;

            if (model == null)
            {
                model = GetFriendBasicInfoModelNullable(userTableID);
                if (model == null)
                {
                    UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userTableID);
                    if (dto == null)
                    {
                        dto = new UserBasicInfoDTO { UserTableID = userTableID, RingID = ringID, FullName = fullname, ProfileImage = profileImage };
                        FriendDictionaries.Instance.AddOrUPdateInBasicInfoDictionary(userTableID, dto, true);
                        model = new UserBasicInfoModel();
                        model.ShortInfoModel.UserTableID = dto.UserTableID;
                        model.ShortInfoModel.UserIdentity = dto.RingID;
                        model.ShortInfoModel.FullName = dto.FullName;
                        model.ShortInfoModel.ProfileImage = dto.ProfileImage;
                        model.ShortInfoModel.ImSoundEnabled = dto.ImSoundEnabled;
                        model.ShortInfoModel.ImNotificationEnabled = dto.ImNotificationEnabled;
                        model.ShortInfoModel.ImPopupEnabled = dto.ImPopupEnabled;
                    }
                    else model = new UserBasicInfoModel(dto);
                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                }
            }
            return model;
        }

        public UserShortInfoModel GetUserShortInfoModelNotNullable(long userTableID, long ringID = 0, string fullname = "", string profileImage = "")
        {
            return GetUserBasicInfoModelNotNullable(userTableID, ringID = 0, fullname, profileImage).ShortInfoModel;
        }

        public UserBasicInfoModel GetFriendBasicInfoModelNullable(long userTableID)
        {
            return FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userTableID);
        }

        public UserShortInfoModel SearchShortInfoModelCollectionDictnryByUtId(long userTableID)
        {
            if (userTableID == DefaultSettings.LOGIN_TABLE_ID) return RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;
            UserBasicInfoModel model = GetFriendBasicInfoModelNullable(userTableID);
            if (model == null)
            {
                UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userTableID);
                if (dto != null && !string.IsNullOrEmpty(dto.FullName))
                {
                    dto.UserTableID = userTableID;
                    model = new UserBasicInfoModel(dto);
                    model.ShortInfoModel.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_UNKNOWN;
                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                }
            }
            return (model != null && model.ShortInfoModel != null && model.ShortInfoModel.UserTableID > 0 && !string.IsNullOrEmpty(model.ShortInfoModel.FullName)) ? model.ShortInfoModel : null;
        }

        public GroupInfoModel GetGroupInfoModelByGroupID(long groupID)
        {
            GroupInfoModel model = RingIDViewModel.Instance.GroupList.TryGetValue(groupID);
            if (model == null)
            {
                List<GroupInfoDTO> groupInfoDTOs = new List<GroupInfoDTO>();
                GroupInfoDTO groupInfoDTO = new GroupInfoDTO();
                groupInfoDTO.GroupID = groupID;
                groupInfoDTO.GroupName = groupID.ToString();
                groupInfoDTO.IsPartial = true;
                groupInfoDTOs.Add(groupInfoDTO);

                model = new GroupInfoModel(groupInfoDTO);
                Application.Current.Dispatcher.BeginInvoke(delegate
                {
                    RingIDViewModel.Instance.GroupList[groupID] = model;
                });

                new InsertIntoGroupOrMemberTable(groupInfoDTOs);
            }
            return model;
        }

        public void Reset()
        {
            //
            _AllMediaCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_ALL_MEDIA);
            _AllNewsPortalCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_ALL_NEWSPORTAL);

            _SavedAllCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_SAVED_ALL);
            _SavedMediaCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_SAVED_MEDIAPAGE);
            _SavedNewsPortalCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_SAVED_NEWSPORTAL);

            _ProfileMyCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_PROFILE_MY, true);
            _ProfileFriendCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_PROFILE_FRIEND, true);
            _ProfileNewsPortalCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_PROFILE_NEWSPORTAL);
            _ProfileMediaCustomFeeds = new CustomFeedCollection(DefaultSettings.FEED_TYPE_PROFILE_MEDIA);
            //
            //SavedFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_SAVED_ALL);
            //SavedFeedsNewsPortal = new CustomObservableCollection(DefaultSettings.FEED_TYPE_NEWSPORTAL_SAVED);
            //SavedFeedsPage = new CustomObservableCollection(DefaultSettings.FEED_TYPE_PAGE_SAVED);
            //SavedFeedsMediaPage = new CustomObservableCollection(DefaultSettings.FEED_TYPE_MUSIC_SAVED);
            //SavedFeedsCelebrity = new CustomObservableCollection(DefaultSettings.FEED_TYPE_CELEBRITY_SAVED);
            //SavedFeedsCircle = new CustomObservableCollection(DefaultSettings.FEED_TYPE_SAVED_CIRCLE);

            //AllNewsPortalFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_NEWSPORTAL);
            //AllPagesFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_PAGE);
            //AllMediaPageFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_MUSIC);
            //AllCelebrityFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_CELEBRITY);
            //AllCircleFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_CIRCLE_ALL);

            //NewsPortalProfileFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_NEWSPORTAL_PROFILE);
            //PageProfileFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_PAGE_PROFILE);
            //MediaPageProfileFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_MUSIC_PROFILE);
            //CelebrityProfileFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_CELEBRITY_PROFILE);
            //CircleProfileFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_CIRCLE, true);
            //FriendProfileFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_FRIEND, true);

            //MyNewsFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_MY, true);
            //NewsFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_ALL, true);

            //MediaFeeds = new CustomObservableCollection(DefaultSettings.FEED_TYPE_MEDIA);

            //MediaFeedsTrending = new CustomObservableCollection(DefaultSettings.FEED_TYPE_MEDIA_TRENDING);

            //MyBasicInfoModel = new UserBasicInfoModel();
            RecentlyAddedFriendList = new ObservableCollection<UserBasicInfoModel>();
            SpecialFriendList = new ObservableCollection<UserBasicInfoModel>();
            FavouriteFriendList = new ObservableCollection<UserBasicInfoModel>();
            TopFriendList = new ObservableCollection<UserBasicInfoModel>();
            NonFavouriteFriendList = new ObservableCollection<UserBasicInfoModel>();
            TempMyFriendList = new ObservableCollection<UserBasicInfoModel>();
            TempFriendList = new ObservableCollection<UserBasicInfoModel>();
            TempFriendSearchList = new ObservableCollection<UserBasicInfoModel>();
            SuggestionFriendList = new ObservableCollection<UserBasicInfoModel>();
            CelebritiesPopular = new ObservableCollection<CelebrityModel>();
            CelebritiesDiscovered = new ObservableCollection<CelebrityModel>();
            CelebritiesFollowing = new ObservableCollection<CelebrityModel>();

            BreakingMediaCloudFeeds = new ObservableCollection<SingleMediaModel>();
            MusicPagesDiscovered = new ObservableCollection<MusicPageModel>();
            MusicPagesFollowing = new ObservableCollection<MusicPageModel>();
            BreakingNewsPortalFeeds = new ObservableCollection<FeedModel>();
            BreakingNewsPortalProfileFeeds = new ObservableCollection<FeedModel>();

            ProfileImageList = new ObservableCollection<ImageModel>();
            ProfileImageList.Add(new ImageModel() { IsLoadMore = true });

            CoverImageList = new ObservableCollection<ImageModel>();
            CoverImageList.Add(new ImageModel() { IsLoadMore = true });

            FeedImageList = new ObservableCollection<ImageModel>();
            FeedImageList.Add(new ImageModel() { IsLoadMore = true });

            NotificationList = new ObservableCollection<NotificationModel>();
            CallNotificationCounter = AppConstants.CALL_NOTIFICATION_COUNT;
            //ChatNotificationCounter = AppConstants.CHAT_NOTIFICATION_COUNT;
            AllNotificationCounter = AppConstants.ALL_NOTIFICATION_COUNT;
            //   AddFriendsNotificationCounter = AppConstants.ADD_FRIEND_NOTIFICATION_COUNT;
            FeedUnreadNotificationCounter = 0;
            //IncomingRequestFriendList = new ObservableCollection<UserBasicInfoModel>();
            //OutgoingRequestFriendList = new ObservableCollection<UserBasicInfoModel>();
            CallLogModelsList = new ObservableCollection<RecentModel>();

            MyEducationModelsList = new ObservableCollection<SingleEducationModel>();
            MyWorkModelsList = new ObservableCollection<SingleWorkModel>();
            MySkillModelsList = new ObservableCollection<SingleSkillModel>();
            ChatBgImageList = new ObservableCollection<ChatBgImageModel>();
            //  TotalFriendList = new ObservableCollection<UserBasicInfoModel>();
            BlockedContactList = new ObservableCollection<UserBasicInfoModel>();
            DoingListForNewStatus = new ObservableCollection<DoingModel>();
            MyAudioAlbums = new ObservableCollection<MediaContentModel>();
            MyVideoAlbums = new ObservableCollection<MediaContentModel>();
            MyImageAlubms = new ObservableCollection<AlbumModel>();
            MyImageAlubms.Add(new AlbumModel() { IsLoadMore = true });
            MyDownloads = new ObservableCollection<SingleMediaModel>();
            MyRecentPlayList = new ObservableCollection<SingleMediaModel>();
            FeedMediasNewlyAdded = new ObservableCollection<SingleMediaModel>();
            //StickerCategoryList = new ObservableCollection<StickerCategoryModel>();
            MarketStickerCategoryList = new ObservableCollection<MarketStickerCategoryModel>();
            RecentStickerCategoryList = new ObservableCollection<MarketStickerCategoryModel>();

            ChatLogModelsList.Clear();
            foreach (KeyValuePair<string, ObservableCollection<RecentModel>> pair in RecentModelList.ToArray())
            {
                pair.Value.Clear();
            }
            RecentModelList.Clear();
            foreach (GroupInfoModel model in GroupList)
            {
                model.MemberInfoDictionary.Clear();
            }
            GroupList.Clear();
            RoomList.Clear();
            BlockedNonFriendList.Clear();
            ChatBackgroundList.Clear();
        }

        public event MainUserControlChangedHandler MainUserControlChanged;
        public void OnMainUserControlChanged(int type)
        {
            MainUserControlChangedHandler handler = MainUserControlChanged;
            if (handler != null)
            {
                handler(type);
            }
        }

        #endregion Utility Method

        #region Media Share Related
        private bool mediaShareViaWallet = false;
        public bool MediaShareViaWallet { get { return mediaShareViaWallet; } set { mediaShareViaWallet = value; this.OnPropertyChanged("MediaShareViaWallet"); } }

        private string contentIDHash = string.Empty;
        public string ContentIDHash { get { return contentIDHash; } set { contentIDHash = value; this.OnPropertyChanged("ContentIDHash"); } }

        private int shareContentType = WalletConstants.MEDIA_DEFAULT;
        public int ShareContentType { get { return shareContentType; } set { shareContentType = value; this.OnPropertyChanged("ShareContentType"); } }

        private int sociaMediaType = WalletConstants.SOCIAL_MEDIA_DEFAULT;
        public int SocialMediaType { get { return sociaMediaType; } set { sociaMediaType = value; this.OnPropertyChanged("SocialMediaType"); } }

        public void SetMediaShareInfo(string encryptedUrl, int mediaType, int socialMediaType)
        {
            ContentIDHash = encryptedUrl;
            ShareContentType = mediaType;
            SocialMediaType = socialMediaType;
        }

        public void ResetMediaShareInfo()
        {
            MediaShareViaWallet = false;
            ContentIDHash = string.Empty;
            ShareContentType = Models.Constants.WalletConstants.MEDIA_DEFAULT;
            SocialMediaType = Models.Constants.WalletConstants.SOCIAL_MEDIA_DEFAULT;
        }
        #endregion

        #region "Observable Collections"
        private ObservableCollection<UserBasicInfoModel> _LikeList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> LikeList
        {
            get { return _LikeList; }
            set { _LikeList = value; this.OnPropertyChanged("LikeList"); }
        }

        private ObservableCollection<CommentModel> mediaComments = new ObservableCollection<CommentModel>();
        public ObservableCollection<CommentModel> MediaComments
        {
            get { return mediaComments; }
            set { mediaComments = value; OnPropertyChanged("MediaComments"); }
        }

        private ObservableCollection<SingleMediaModel> mediaList = new ObservableCollection<SingleMediaModel>();
        public ObservableCollection<SingleMediaModel> MediaList
        {
            get { return mediaList; }
            set { mediaList = value; OnPropertyChanged("MediaList"); }
        }
        #endregion"Observable Collections"
    }
}

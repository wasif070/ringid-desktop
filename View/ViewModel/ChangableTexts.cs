﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models.Constants;
using View.Constants;
using View.Utility;

namespace View.ViewModel
{
    public class ChangableTexts : BaseViewModel
    {
        #region  "Fieldes"
        private string _textSignIn = "Sign In";
        private string _textSignUp = "Sign Up";
        private string _textCancel = "Cancel";
        private string _textVerify = "Verify";
        private string _textSend = "Send";
        private string _textBack = "Back";
        private string _textNewUser = " New User";
        private string _textExistingUser = "Existing User";
        private string _textDone = "Done";

        private string _textTyperingIDNumber = "Type ringID Number";
        private string _textTypePhoneNumber = "Type Phone Number";
        private string _textTypeEmailAddress = "abc@example.com";
        private string _textEmailAddress = "Email Address";
        private string _textEnterPassword = "Enter Password";
        private string _textCode = "Code";
        private string _textForgot = "Forgot?";
        private string _textForgotPassword = "Forgot Password?";
        private string _textSavePassword = "Save password";
        private string _textWelcome = "Welcome";
        private string _textWelcomeTo = "Welcome to";
        private string _textAppName = RingIDSettings.APP_NAME;
        private string _textMakeFreeCallsChat = "Make Free Calls & Chat";
        private string _textEnjoyCoolStickers = "Enjoy Cool Stickers";
        private string _textSecretChat = "Secret Chat";
        private string _textBrowseNewsfeed = "Browse Newsfeed";
        private string _textConnectwithFriendsFamily = "Connect with Friends & Family";
        private string _textLetsConnect = "Let's Connect...";

        private string _textSignInUsingPhoneNumber = "Sign In using phone number";
        private string _textSignInUsingEmailAddress = "Sign In using email address";
        private string _textSignInWithFacebook = "Sign In with facebook";
        private string _textSignInWithTwitter = "Sign In with twitter";
        private string _textSignInUsingRingIDNumber = "Sign In using ringID number";
        private string _textSignUpWithFacebook = "Sign Up with facebook";
        private string _textSignUpUsingEmailAddress = "Sign Up using email address";
        private string _textSignUpWithTwitter = "Sign Up with twitter";
        private string _textSignUpUsingPhoneNumber = "Sign Up using phone number";

        private string _textSetYourInformations = "Set Your Informations";
        private string _textYourInformations = "Your Informations";
        private string _textEnterName = "Enter Name";
        private string _textRetypePassword = "Retype Password";
        private string _textCapslockOn = "Caps lock On!";
        private string _textCongratulations = "Congratulations!";
        private string _textYourRingIDNumber = "Your ringID number";
        private string _textYourPassword = "Your Password";
        private string _textPhoneNumber = ControlTexts.TEXT_PHONE_NUMBER;
        private string _textEmail = ControlTexts.TEXT_EMAIL;
        private string _textFacebook = ControlTexts.TEXT_FACEBOOK;
        private string _textTwitter = ControlTexts.TEXT_TWITTER;
        private string _textPasswordRecovery = "Password Recovery";
        private string _textRecoverPassword = "Recover Password";
        private string _textRecoverby = "Recover by";
        private string _textPleasesetyournewPasswordandSignInToYourAccount = "Please set your new password and Sign in to your account!";
        private string _textPleaseVerifyYourAccountInformationToRecoverYourAccount = "Please verify your account information to recover your account!";
        private string _textResetPassword = "Reset Password";
        #endregion "Fields"

        #region "Constructor"
        public ChangableTexts() { }
        #endregion "Constructor"

        #region "Property"

        public string TextSignIn
        {
            get
            {
                return this._textSignIn;
            }
            set
            {
                this._textSignIn = value;
                this.OnPropertyChanged("TextSignIn");
            }
        }

        public string TextSignUp
        {
            get
            {
                return this._textSignUp;
            }
            set
            {
                this._textSignUp = value;
                this.OnPropertyChanged("TextSignUp");
            }
        }

        public string TextCancel
        {
            get
            {
                return this._textCancel;
            }
            set
            {
                this._textCancel = value;
                this.OnPropertyChanged("TextCancel");
            }
        }

        public string TextVerify
        {
            get
            {
                return this._textVerify;
            }
            set
            {
                this._textVerify = value;
                this.OnPropertyChanged("TextVerify");
            }
        }

        public string TextSend
        {
            get
            {
                return this._textSend;
            }
            set
            {
                this._textSend = value;
                this.OnPropertyChanged("TextSend");
            }
        }

        public string TextBack
        {
            get
            {
                return this._textBack;
            }
            set
            {
                this._textBack = value;
                this.OnPropertyChanged("TextBack");
            }
        }

        public string TextDone
        {
            get
            {
                return this._textDone;
            }
            set
            {
                this._textDone = value;
                this.OnPropertyChanged("TextDone");
            }
        }

        public string TextTyperingIDNumber
        {
            get
            {
                return this._textTyperingIDNumber;
            }
            set
            {
                this._textTyperingIDNumber = value;
                this.OnPropertyChanged("TextTyperingIDNumber");
            }
        }

        public string TextTypeEmailAddress
        {
            get
            {
                return this._textTypeEmailAddress;
            }
            set
            {
                this._textTypeEmailAddress = value;
                this.OnPropertyChanged("TextTypeEmailAddress");
            }
        }

        public string TextTypePhoneNumber
        {
            get
            {
                return this._textTypePhoneNumber;
            }
            set
            {
                this._textTypePhoneNumber = value;
                this.OnPropertyChanged("_textTypePhoneNumber");
            }
        }

        public string TextEmailAddress
        {
            get
            {
                return this._textEmailAddress;
            }
            set
            {
                this._textEmailAddress = value;
                this.OnPropertyChanged("TextEmailAddress");
            }
        }

        public string TextEnterPassword
        {
            get
            {
                return this._textEnterPassword;
            }
            set
            {
                this._textEnterPassword = value;
                this.OnPropertyChanged("TextEnterPassword");
            }
        }

        public string TextCode
        {
            get
            {
                return this._textCode;
            }
            set
            {
                this._textCode = value;
                this.OnPropertyChanged("TextCode");
            }
        }

        public string TextForgot
        {
            get
            {
                return this._textForgot;
            }
            set
            {
                this._textForgot = value;
                this.OnPropertyChanged("TextForgot");
            }
        }

        public string TextForgotPassword
        {
            get
            {
                return this._textForgotPassword;
            }
            set
            {
                this._textForgotPassword = value;
                this.OnPropertyChanged("TextForgotPassword");
            }
        }

        public string TextSavePassword
        {
            get
            {
                return this._textSavePassword;
            }
            set
            {
                this._textSavePassword = value;
                this.OnPropertyChanged("TextSavePassword");
            }
        }

        public string TextWelcome
        {
            get
            {
                return this._textWelcome;
            }
            set
            {
                this._textWelcome = value;
                this.OnPropertyChanged("TextWelcome");
            }
        }

        public string TextWelcomeTo
        {
            get
            {
                return this._textWelcomeTo;
            }
            set
            {
                this._textWelcomeTo = value;
                this.OnPropertyChanged("TextForgotWelcomeTo");
            }
        }

        public string TextAppName
        {
            get
            {
                return this._textAppName;
            }
            set
            {
                this._textAppName = value;
                this.OnPropertyChanged("TextAppName");
            }
        }

        public string TextMakeFreeCallsChat
        {
            get
            {
                return this._textMakeFreeCallsChat;
            }
            set
            {
                this._textMakeFreeCallsChat = value;
                this.OnPropertyChanged("TextMakeFreeCallsChat");
            }
        }

        public string TextEnjoyCoolStickers
        {
            get
            {
                return this._textEnjoyCoolStickers;
            }
            set
            {
                this._textEnjoyCoolStickers = value;
                this.OnPropertyChanged("TextEnjoyCoolStickers");
            }
        }

        public string TextSecretChat
        {
            get
            {
                return this._textSecretChat;
            }
            set
            {
                this._textSecretChat = value;
                this.OnPropertyChanged("TextSecretChat");
            }
        }

        public string TextBrowseNewsfeed
        {
            get
            {
                return this._textBrowseNewsfeed;
            }
            set
            {
                this._textBrowseNewsfeed = value;
                this.OnPropertyChanged("TextBrowseNewsfeed");
            }
        }

        public string TextConnectwithFriendsFamily
        {
            get
            {
                return this._textConnectwithFriendsFamily;
            }
            set
            {
                this._textConnectwithFriendsFamily = value;
                this.OnPropertyChanged("TextConnectwithFriendsFamily");
            }
        }

        public string TextLetsConnect
        {
            get
            {
                return this._textLetsConnect;
            }
            set
            {
                this._textLetsConnect = value;
                this.OnPropertyChanged("TextLetsConnect");
            }
        }

        public string TextNewUser
        {
            get
            {
                return this._textNewUser;
            }
            set
            {
                this._textNewUser = value;
                this.OnPropertyChanged("TextNewUser");
            }
        }

        public string TextExistingUser
        {
            get
            {
                return this._textExistingUser;
            }
            set
            {
                this._textExistingUser = value;
                this.OnPropertyChanged("TextExistingUser");
            }
        }

        public string TextSignInUsingEmailAddress
        {
            get
            {
                return this._textSignInUsingEmailAddress;
            }
            set
            {
                this._textSignInUsingEmailAddress = value;
                this.OnPropertyChanged("TextSignInUsingEmailAddress");
            }
        }

        public string TextSignInUsingPhoneNumber
        {
            get
            {
                return this._textSignInUsingPhoneNumber;
            }
            set
            {
                this._textSignInUsingPhoneNumber = value;
                this.OnPropertyChanged("TextSignInUsingPhoneNumber");
            }
        }

        public string TextSignInUsingRingIDNumber
        {
            get
            {
                return this._textSignInUsingRingIDNumber;
            }
            set
            {
                this._textSignInUsingRingIDNumber = value;
                this.OnPropertyChanged("TextSignInUsingRingIDNumber");
            }
        }

        public string TextSignInWithFacebook
        {
            get
            {
                return this._textSignInWithFacebook;
            }
            set
            {
                this._textSignInWithFacebook = value;
                this.OnPropertyChanged("TextSignInWithFacebook");
            }
        }

        public string TextSignInWithTwitter
        {
            get
            {
                return this._textSignInWithTwitter;
            }
            set
            {
                this._textSignInWithTwitter = value;
                this.OnPropertyChanged("TextSignInWithTwitter");
            }
        }

        public string TextSignUpUsingEmailAddress
        {
            get
            {
                return this._textSignUpUsingEmailAddress;
            }
            set
            {
                this._textSignUpUsingEmailAddress = value;
                this.OnPropertyChanged("TextSignUpUsingEmailAddress");
            }
        }

        public string TextSignUpUsingPhoneNumber
        {
            get
            {
                return this._textSignUpUsingPhoneNumber;
            }
            set
            {
                this._textSignUpUsingPhoneNumber = value;
                this.OnPropertyChanged("TextSignUpUsingPhoneNumber");
            }
        }

        public string TextSignUpWithFacebook
        {
            get
            {
                return this._textSignUpWithFacebook;
            }
            set
            {
                this._textSignUpWithFacebook = value;
                this.OnPropertyChanged("TextSignUpWithFacebook");
            }
        }

        public string TextSignUpWithTwitter
        {
            get
            {
                return this._textSignUpWithTwitter;
            }
            set
            {
                this._textSignUpWithTwitter = value;
                this.OnPropertyChanged("TextSignUpWithTwitter");
            }
        }

        public string TextSetYourInformations
        {
            get
            {
                return this._textSetYourInformations;
            }
            set
            {
                this._textSetYourInformations = value;
                this.OnPropertyChanged("TextSetYourInformations");
            }
        }

        public string TextYourInformations
        {
            get
            {
                return this._textYourInformations;
            }
            set
            {
                this._textYourInformations = value;
                this.OnPropertyChanged("TextYourInformations");
            }
        }

        public string TextEnterName
        {
            get
            {
                return this._textEnterName;
            }
            set
            {
                this._textEnterName = value;
                this.OnPropertyChanged("TextEnterName");
            }
        }

        public string TextRetypePassword
        {
            get
            {
                return this._textRetypePassword;
            }
            set
            {
                this._textRetypePassword = value;
                this.OnPropertyChanged("TextRetypePassword");
            }
        }

        public string TextCapslockOn
        {
            get
            {
                return this._textCapslockOn;
            }
            set
            {
                this._textCapslockOn = value;
                this.OnPropertyChanged("TextCapslockOn");
            }
        }

        public string TextCongratulations
        {
            get
            {
                return this._textCongratulations;
            }
            set
            {
                this._textCongratulations = value;
                this.OnPropertyChanged("TextCongratulations");
            }
        }

        public string TextYourRingIDNumber
        {
            get
            {
                return this._textYourRingIDNumber;
            }
            set
            {
                this._textYourRingIDNumber = value;
                this.OnPropertyChanged("TextYourRingIDNumber");
            }
        }

        public string TextYourPassword
        {
            get
            {
                return this._textYourPassword;
            }
            set
            {
                this._textYourPassword = value;
                this.OnPropertyChanged("TextYourPassword");
            }
        }

        public string TextPhoneNumber
        {
            get
            {
                return this._textPhoneNumber;
            }
            set
            {
                this._textPhoneNumber = value;
                this.OnPropertyChanged("TextPhoneNumber");
            }
        }

        public string TextEmail
        {
            get
            {
                return this._textEmail;
            }
            set
            {
                this._textEmail = value;
                this.OnPropertyChanged("TextEmail");
            }
        }

        public string TextFacebook
        {
            get
            {
                return this._textFacebook;
            }
            set
            {
                this._textFacebook = value;
                this.OnPropertyChanged("TextFacebook");
            }
        }

        public string TextTwitter
        {
            get
            {
                return this._textTwitter;
            }
            set
            {
                this._textTwitter = value;
                this.OnPropertyChanged("TextTwitter");
            }
        }

        public string TextPasswordRecovery
        {
            get
            {
                return this._textPasswordRecovery;
            }
            set
            {
                this._textPasswordRecovery = value;
                this.OnPropertyChanged("TextPasswordRecovery");
            }
        }

        public string TextRecoverPassword
        {
            get
            {
                return this._textRecoverPassword;
            }
            set
            {
                this._textRecoverPassword = value;
                this.OnPropertyChanged("TextRecoverPassword");
            }
        }

        public string TextRecoverby
        {
            get
            {
                return this._textRecoverby;
            }
            set
            {
                this._textRecoverby = value;
                this.OnPropertyChanged("TextRecoverby");
            }
        }

        public string TextPleasesetyournewPasswordandSignInToYourAccount
        {
            get
            {
                return this._textPleasesetyournewPasswordandSignInToYourAccount;
            }
            set
            {
                this._textPleasesetyournewPasswordandSignInToYourAccount = value;
                this.OnPropertyChanged("TextPleasesetyournewPasswordandSignInToYourAccount");
            }
        }

        public string TextPleaseVerifyYourAccountInformationToRecoverYourAccount
        {
            get
            {
                return this._textPleaseVerifyYourAccountInformationToRecoverYourAccount;
            }
            set
            {
                this._textPleaseVerifyYourAccountInformationToRecoverYourAccount = value;
                this.OnPropertyChanged("TextPleaseVerifyYourAccountInformationToRecoverYourAccount");
            }
        }

        public string TextResetPassword
        {
            get
            {
                return this._textResetPassword;
            }
            set
            {
                this._textResetPassword = value;
                this.OnPropertyChanged("TextResetPassword");
            }
        }

        #endregion "Property"
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Models.Constants;
using View.Constants;
using View.Utility;

namespace View.ViewModel
{
    public class VMRingIDMainWindow : BaseViewModel
    {
        #region  "Fieldes"
        #endregion "Fields"

        #region "Constructor"
        public VMRingIDMainWindow() { }
        #endregion "Constructor"

        #region "Data Change Property"

        public bool IsSignedInBackground { get; set; }
        public string[] PassedArguments { get; set; }

        private string title = RingIDSettings.APP_NAME;
        public string AppTilte
        {
            get { return title; }
            set { title = value; OnPropertyChanged("AppTilte"); }
        }

        private string errorText;
        public string ErrorText
        {
            get { return this.errorText; }
            set { this.errorText = value; this.OnPropertyChanged("ErrorText"); }
        }

        private int errorMessageType;
        public int ErrorMessageType
        {
            get { return errorMessageType; }
            set
            {
                if (value == errorMessageType) return;
                errorMessageType = value; OnPropertyChanged("ErrorMessageType");
            }
        }

        private ResizeMode _resizeMode = System.Windows.ResizeMode.CanMinimize;
        public ResizeMode MainResizeMode
        {
            get { return _resizeMode; }
            set { _resizeMode = value; this.OnPropertyChanged("MainResizeMode"); }
        }

        private bool showMenubar = false;
        public bool ShowMenubar
        {
            get { return showMenubar; }
            set { showMenubar = value; OnPropertyChanged("ShowMenubar"); }
        }

        private bool isSignIn = false;
        public bool IsSignIn
        {
            get { return isSignIn; }
            set
            {
                isSignIn = value;
                OnPropertyChanged("IsSignIn");
                if (DefaultSettings.IsInternetAvailable == false || value == false) TaskBarOverLayCounter = -1;
                else if (TaskBarOverLayCounter == -1 && DefaultSettings.IsInternetAvailable && value) TaskBarOverLayCounter = 0;
            }
        }

        private bool isActiveWindow = true;
        public bool IsActiveWindow
        {
            get { return isActiveWindow; }
            set
            {
                isActiveWindow = value;
                OnPropertyChanged("IsActiveWindow");
            }
        }

        private string ringID = string.Empty;
        public string RingID
        {
            get { return this.ringID; }
            set { this.ringID = value; this.OnPropertyChanged("RingID"); }
        }

        private string fullName;
        public string FullName
        {
            get { return this.fullName; }
            set { this.fullName = value; this.OnPropertyChanged("FullName"); }
        }

        private string profileImageUrl = null;
        public string ProfileImageUrl
        {
            get { return this.profileImageUrl; }
            set { this.profileImageUrl = value; this.OnPropertyChanged("ProfileImageUrl"); }
        }

        private string password;
        public string Password
        {
            get { return this.password; }
            set { this.password = value; this.OnPropertyChanged("Password"); }
        }

        private string synchronizeingText = "Please Wait";
        public string SynchronizeingText
        {
            get { return synchronizeingText; }
            set { synchronizeingText = value; OnPropertyChanged("SynchronizeingText"); }
        }

        private string countryCode;
        public string CountryCode
        {
            get { return this.countryCode; }
            set
            {
                this.countryCode = value;
                this.OnPropertyChanged("CountryCode");
                this.OnPropertyChanged("PhoneNumberWithCode");
            }
        }

        private string phoneNumber;
        public string PhoneNumber
        {
            get { return this.phoneNumber; }
            set
            {
                this.phoneNumber = value;
                this.OnPropertyChanged("PhoneNumber");
                this.OnPropertyChanged("PhoneNumberWithCode");
            }
        }

        private string phoneNumberWithCode;
        public string PhoneNumberWithCode
        {
            get { return this.phoneNumberWithCode; }
            set
            {
                this.phoneNumberWithCode = CountryCode + PhoneNumber;
                this.OnPropertyChanged("PhoneNumberWithCode");
            }
        }

        private int taskBarOverLayCounter = -1;
        public int TaskBarOverLayCounter
        {
            get { return taskBarOverLayCounter; }
            set
            {
                if (DefaultSettings.IsInternetAvailable && IsSignIn) taskBarOverLayCounter = value;
                else taskBarOverLayCounter = -1;
                this.OnPropertyChanged("TaskBarOverLayCounter");
            }
        }

        private bool showRingIDLogo = true;
        public bool ShowRingIDLogo
        {
            get { return showRingIDLogo; }
            set { showRingIDLogo = value; OnPropertyChanged("ShowRingIDLogo"); }
        }

        private bool showWelcomeBG = true;
        public bool ShowWelcomeBG
        {
            get { return showWelcomeBG; }
            set { showWelcomeBG = value; OnPropertyChanged("ShowWelcomeBG"); }
        }

        private int loginType;
        public int LoginType
        {
            get { return this.loginType; }
            set { this.loginType = value; this.OnPropertyChanged("LoginType"); }
        }

        private int currentUIType;
        public int CurrentUIType
        {
            get { return this.currentUIType; }
            set { this.currentUIType = value; this.OnPropertyChanged("CurrentUIType"); }
        }

        private bool isComponentEnabled = true;
        public bool IsComponentEnabled
        {
            get { return isComponentEnabled; }
            set
            {
                if (value == isComponentEnabled) return;
                isComponentEnabled = value; OnPropertyChanged("IsComponentEnabled");
            }
        }

        private bool pleaseWaitLoader = false;
        public bool PleaseWaitLoader
        {
            get { return this.pleaseWaitLoader; }
            set
            {
                this.pleaseWaitLoader = value;
                if (this.pleaseWaitLoader == true) ErrorText = "Please wait...";
                this.OnPropertyChanged("PleaseWaitLoader");
            }
        }

        private string emailID = string.Empty;
        public string EmailID
        {
            get { return this.emailID; }
            set { this.emailID = value; this.OnPropertyChanged("EmailID"); }
        }

        private bool isLoadingCotactList = true;
        public bool IsLoadingCotactList
        {
            get { return isLoadingCotactList; }
            set { isLoadingCotactList = value; OnPropertyChanged("IsLoadingCotactList"); }
        }

        public long UserTableID { get; set; }
        #endregion //"Property"

        #region "Uitily Methods"

        public void ShowErrorMessage(string Msg)
        {
            PleaseWaitLoader = false;
            ErrorMessageType = 0;
            ErrorText = Msg;
            IsComponentEnabled = true;
        }
        #endregion "Utility Methods"
    }
}
﻿using log4net;
using Models.Constants;
using Models.DAO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Feed;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Auth;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;

namespace View.ViewModel
{
    public class NewsFeedViewModel : INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(NewsFeedViewModel).Name);

        private UCEditHistoryPopupStyle ucEditHistoryPopupStyle = null;
        int imageCount;
        public bool _BannerLoaded = false;
        private Thread _BannerThread = null;
        private System.Timers.Timer timer;
        public bool _IsTimerStart = false;
        private int target = 0;
        private int CurrentLeftMargin = 0;
        private bool _IsLast = false;
        private VirtualizingStackPanel _ImageItemContainer = null;
        // public bool IsFirstTime = false;
        DispatcherTimer _ActivityNameToolTip = null;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static NewsFeedViewModel Instance = new NewsFeedViewModel();
        #region "AllFEEDS"
        public int AllFeedsBlankFeeds = 0;
        public bool AllFeedsScrollEnabled = true;
        public BackgroundWorker AllFeedsBackgroundWorker;
        public ConcurrentQueue<FeedHolderModel> AllFeedsQueue = new ConcurrentQueue<FeedHolderModel>();
        public bool AllFeedsRequestOn = false;
        public bool StopRemoving = false;
        public int AllFeedSpecialFeedCount = 0;
        private ObservableCollection<FeedHolderModel> _AllFeedsViewCollection;
        public ObservableCollection<FeedHolderModel> AllFeedsViewCollection
        {
            get { return _AllFeedsViewCollection; }
            set
            {
                _AllFeedsViewCollection = value;
                this.OnPropertyChanged("AllFeedsViewCollection");
            }
        }
        public FeedHolderModel AllFeedsLoadMoreModel, DummyModel1, DummyModel2, tempModel;
        public NewsFeedViewModel()
        {
            _AllFeedsViewCollection = new ObservableCollection<FeedHolderModel>();
            _AllFeedsViewCollection.Add(new FeedHolderModel(0));

            DummyModel1 = new FeedHolderModel(12);
            DummyModel2 = new FeedHolderModel(12);
            _AllFeedsViewCollection.Add(DummyModel1);
            _AllFeedsViewCollection.Add(DummyModel2);

            AllFeedsLoadMoreModel = new FeedHolderModel(1);
            _AllFeedsViewCollection.Add(AllFeedsLoadMoreModel);
        }
        public void ActionReloadFeeds()
        {
            try
            {
                // if (!AllFeedsRequestOn)
                // {
                if (!DefaultSettings.IsInternetAvailable)
                {
                    AllFeedsRequestFeeds(FeedDataContainer.Instance.AllBottomIds.PvtMinGuid, 2, 0);
                }
                else
                {
                    FeedDataContainer.Instance.UnreadNewsFeedIds.Clear();
                    FeedDataContainer.Instance.AllTopIds.Reset();
                    FeedDataContainer.Instance.AllBottomIds.Reset();
                    FeedDataContainer.Instance.AllRequestedPivotIds.Clear();
                    try
                    {
                        if (AllFeedsViewCollection.Count > 2)
                        {
                            lock (AllFeedsViewCollection) //might cause hang issue but helps duplicate issue
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    for (int i = AllFeedsViewCollection.Count - 2; i > 0; i--)
                                    {
                                        FeedHolderModel fm = AllFeedsViewCollection[i];
                                        if (fm.FeedId != Guid.Empty)
                                        {
                                            AllFeedsViewCollection.Remove(fm);
                                            CurrentFeedIds.Remove(fm.FeedId);
                                        }
                                    }
                                }, DispatcherPriority.Send);
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                    }
                    if (AllFeedsBackgroundWorker != null && AllFeedsBackgroundWorker.IsBusy) AllFeedsBackgroundWorker.CancelAsync();
                    FeedHolderModel holder = null;
                    while (!AllFeedsQueue.IsEmpty)
                        AllFeedsQueue.TryDequeue(out holder);
                    DefaultSettings.ALL_STARTPKT = Auth.utility.SendToServer.GetRanDomPacketID();
                    AllFeedsRequestFeeds(Guid.Empty, 2, 0, DefaultSettings.ALL_STARTPKT);
                }
                //}
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void ClearAllStorageFeeds()
        {
            try
            {
                if (AllFeedsViewCollection.Count > 2)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        lock (AllFeedsViewCollection)//might cause hang issue but helps duplicate issue
<<<<<<< HEAD
                            for (int i = AllFeedsViewCollection.Count - 2; i > 0; i--)
=======
                            for (int i = AllFeedsViewCollection.Count - 3; i > 0; i--)
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                            {
                                FeedHolderModel holder = AllFeedsViewCollection[i];
                                if (holder.FeedType == -1)
                                {
                                    holder.FeedType = 2;
                                    if (holder.Feed != null) { holder.Feed.FeedType = 2; }
                                    if (holder.FeedId != Guid.Empty)
                                    {
                                        AllFeedsViewCollection.Remove(holder);
                                        CurrentFeedIds.Remove(holder.FeedId);
                                        //FeedModel tmp = null;
                                        //FeedDataContainer.Instance.FeedModels.TryRemove(holder.FeedId, out tmp);
                                    }
                                }
                            }
                        DefaultSettings.STORAGE_FEEDS_REMOVED = true;
                    }, DispatcherPriority.Send);
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void ClearTopModels()
        {
            Task unloadTopData = new Task(delegate
            {
                try
                {
                    double scrollPosition = UCAllFeeds.Instance.scroll.VerticalOffset;
                    double scrollableHeight = UCAllFeeds.Instance.scroll.ScrollableHeight;
                    AllFeedsScrollEnabled = false;
                    int idxEnd = (int)(AllFeedsViewCollection.Count / 2);
                    for (int i = idxEnd; i > 0; i--)
                    {
                        FeedHolderModel fm = AllFeedsViewCollection[i];
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            try
                            {
                                AllFeedsViewCollection.Remove(fm);
                                if (i == 1)
                                {
                                    scrollPosition = (scrollPosition / scrollableHeight) * UCAllFeeds.Instance.scroll.ScrollableHeight;
                                    UCAllFeeds.Instance.scroll.ScrollToVerticalOffset(scrollPosition);
                                    AllFeedsScrollEnabled = true;
                                }
                            }
                            catch (Exception ex) { log.Error("Error: InvokeRemoveAt<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
                        }, DispatcherPriority.Send);
                        CurrentFeedIds.Remove(fm.FeedId);
                        if (fm.Feed != null) FeedDataContainer.Instance.AllTopIds.InsertIntoSortedList(fm.Feed.Time, fm.FeedId);
                    }
                }
                catch (Exception ex)
                {
                    AllFeedsScrollEnabled = true;
                    log.Error("Error: ClearTopModels" + ex.Message + "\n" + ex.StackTrace);
                }
            });
            unloadTopData.Start();
        }
        public void ClearBottomModels() //not use now for memory
        {
            Task unloadBottomData = new Task(delegate
            {
                try
                {
                    double scrollPosition = UCAllFeeds.Instance.scroll.VerticalOffset;
                    double scrollableHeight = UCAllFeeds.Instance.scroll.ScrollableHeight;
                    AllFeedsScrollEnabled = false;
                    int idxStart = (int)(AllFeedsViewCollection.Count / 2);
                    for (int i = AllFeedsViewCollection.Count - 4; i >= idxStart; i--)
                    {
                        FeedHolderModel fm = AllFeedsViewCollection[i];
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            try
                            {
                                AllFeedsViewCollection.Remove(fm);
                                if (i == idxStart)
                                {
                                    scrollPosition = (scrollPosition / scrollableHeight) * UCAllFeeds.Instance.scroll.ScrollableHeight;
                                    UCAllFeeds.Instance.scroll.ScrollToVerticalOffset(scrollPosition);
                                    AllFeedsScrollEnabled = true;
                                }
                            }
                            catch (Exception ex) { log.Error("Error: InvokeRemoveAt<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
                        }, DispatcherPriority.Send);
                        CurrentFeedIds.Remove(fm.FeedId);
                        if (fm.Feed != null) FeedDataContainer.Instance.AllBottomIds.InsertIntoSortedList(fm.Feed.Time, fm.FeedId);
                    }
                }
                catch (Exception ex)
                {
                    AllFeedsScrollEnabled = true;
                    log.Error("Error: ClearBottomModels" + ex.Message + "\n" + ex.StackTrace);
                }
            });
            unloadBottomData.Start();
        }
        public void DropAllKeeping1To20(bool fromHomeUnloader = false)
        {
            Task unloadBottomData = new Task(delegate
            {
                try
                {
                    AllFeedsScrollEnabled = false;
                    if (fromHomeUnloader)
                    {
                        Thread.Sleep(300);
                        RingIDViewModel.Instance.FeedButtionSelection = false;
                    }
                    if (AllFeedsBackgroundWorker != null && AllFeedsBackgroundWorker.IsBusy) AllFeedsBackgroundWorker.CancelAsync();
                    if (AllFeedsViewCollection[1].SequenceForAllFeeds > 1)
                    {
                        int feedCount = AllFeedsViewCollection.Count;
                        for (int i = AllFeedsViewCollection.Count - 4; i > 0; i--)
                        {
                            if (i <= AllFeedsViewCollection.Count - 4 && i > 0)
                            {
                                FeedHolderModel fm = AllFeedsViewCollection[i];
                                AllFeedsViewCollection.InvokeRemove(fm);
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    try { AllFeedsViewCollection.Remove(fm); }
                                    catch (Exception ex) { log.Error("Error: DropAllKeeping1To20<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
                                    //if (i == 1) AllFeedsScrollEnabled = true;
                                }, DispatcherPriority.Send);
                                CurrentFeedIds.Remove(fm.FeedId);
                                if (fm.Feed != null) FeedDataContainer.Instance.AllBottomIds.InsertIntoSortedList(fm.Feed.Time, fm.FeedId);
                            }
                        }
                        foreach (var item in FeedDataContainer.Instance.AllTopIds)
                        {
                            FeedDataContainer.Instance.AllBottomIds.InsertIntoSortedList(item.Key, item.Value);
                        }
                        FeedDataContainer.Instance.AllTopIds.Clear();
                        int maxCount = (FeedDataContainer.Instance.AllBottomIds.Count > 10) ? 10 : FeedDataContainer.Instance.AllBottomIds.Count;
                        Thread.Sleep(feedCount);
                        FeedModel feedModel;
                        for (int i = 0; i < maxCount; i++)
                        {
                            feedModel = null;
                            Guid toInsertFeedId = FeedDataContainer.Instance.AllBottomIds.LastFeedIdRemoved();
                            if (toInsertFeedId != Guid.Empty && !CurrentFeedIds.Contains(toInsertFeedId)
                                && FeedDataContainer.Instance.FeedModels.TryGetValue(toInsertFeedId, out feedModel))
                            {
                                FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                                holder.FeedId = toInsertFeedId;
                                holder.Feed = feedModel;
                                holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                holder.SelectViewTemplate();
                                NewsFeedViewModel.Instance.CalculateandInsertAllFeeds(holder, true);
                            }
                        }
                        Thread.Sleep(feedCount);
<<<<<<< HEAD
=======
                        AfterRemovalSequenceBelowFeeds(1, 1);
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                        AllFeedsRequestFeeds(FeedDataContainer.Instance.AllTopIds.PvtMaxGuid, 1, 0);
                        //ActionReloadFeeds();
                        AllFeedsScrollEnabled = true;
                    }
                    else
                    {
                        StopRemoving = false;
                        int idxStarts = ((AllFeedsViewCollection.Count - 4) > 10) ? 10 : AllFeedsViewCollection.Count - 4;
                        for (int i = AllFeedsViewCollection.Count - 4; i > 0; i--)
                        {
                            if (i > idxStarts)
                            {
                                if (!StopRemoving && i < AllFeedsViewCollection.Count - 1)
                                {
                                    FeedHolderModel fm = AllFeedsViewCollection[i];
                                    AllFeedsViewCollection.InvokeRemove(fm);
                                    CurrentFeedIds.Remove(fm.FeedId);
                                    if (fm.Feed != null) FeedDataContainer.Instance.AllBottomIds.InsertIntoSortedList(fm.Feed.Time, fm.FeedId);
                                }
                            }
                            else
                            {
                                AllFeedsViewCollection[i].ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                            }
                        }
                        foreach (var item in FeedDataContainer.Instance.AllTopIds)
                        {
                            Guid toInsertFeedId = item.Value;
                            FeedModel feedModel = null;
                            if (!CurrentFeedIds.Contains(toInsertFeedId)
                                && FeedDataContainer.Instance.FeedModels.TryGetValue(toInsertFeedId, out feedModel))
                            {
                                FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                                holder.FeedId = toInsertFeedId;
                                holder.Feed = feedModel;
                                holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                holder.SelectViewTemplate();
                                NewsFeedViewModel.Instance.CalculateandInsertAllFeeds(holder, true);
                            }
                        }
                        Thread.Sleep(100);
                        AllFeedsScrollEnabled = true;
                    }
                }
                catch (Exception ex)
                {
                    AllFeedsScrollEnabled = true;
                    log.Error("Error: DropAllKeeping1To20" + ex.Message + "\n" + ex.StackTrace);
                }
            });
            unloadBottomData.Start();
        }
        #region "freshcodestarts"
        public List<Guid> CurrentFeedIds = new List<Guid>();
        public void InsertModelandSeqViewCollection(int index, FeedHolderModel item) //only feedtype normalfeed
        {
            FeedHolderModel prevModel = AllFeedsViewCollection[index - 1];
            FeedHolderModel nextModel = AllFeedsViewCollection[index];
            if (prevModel != null && prevModel.FeedId != Guid.Empty)
            {
                int toAssignSeq = prevModel.SequenceForAllFeeds + 1;
                item.SequenceForAllFeeds = toAssignSeq;
                for (int i = index; i < AllFeedsViewCollection.Count - 3; i++)
                {
                    toAssignSeq++;
                    AllFeedsViewCollection[i].SequenceForAllFeeds = toAssignSeq;
                }
            }
            else if (nextModel != null && nextModel.FeedId != Guid.Empty)
            {
                int toAssignSeq = nextModel.SequenceForAllFeeds - 1;
                if (toAssignSeq < 1)
                {
                    toAssignSeq = 1;
                    item.SequenceForAllFeeds = toAssignSeq;
                    for (int i = index; i < AllFeedsViewCollection.Count - 3; i++)
                    {
                        toAssignSeq++;
                        AllFeedsViewCollection[i].SequenceForAllFeeds = toAssignSeq;
                    }
                }
                else
                {
                    item.SequenceForAllFeeds = toAssignSeq;
                }
            }
            else
            {
                item.SequenceForAllFeeds = 1;
            }

            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                try
                {
                    //if (AllFeedsScrollEnabled)
                    if (index < AllFeedsViewCollection.Count - 1)
                        lock (AllFeedsViewCollection)
                        {
                            AllFeedsViewCollection.Insert(index, item);
                            if (!CurrentFeedIds.Contains(item.FeedId)) CurrentFeedIds.Add(item.FeedId);
                        }
                }
                catch (Exception ex) { log.Error("Error: InViewCollection<Insert> ." + ex.Message + "\n" + ex.StackTrace); }
            }, DispatcherPriority.Send);
        }
        public void DropModelandSeqViewCollection(FeedHolderModel item)//CurrentFeedIds.Remove search
        {
            //Application.Current.Dispatcher.Invoke((Action)delegate
            //{
            try
            {
                //lock ?SJM
                int idxOfItem = AllFeedsViewCollection.IndexOf(item);
                if (idxOfItem >= 0)
                {
                    AllFeedsViewCollection.RemoveAt(idxOfItem);
                    CurrentFeedIds.Remove(item.FeedId);
                    AfterRemovalSequenceBelowFeeds(idxOfItem, item.SequenceForAllFeeds);
                }
            }
            catch (Exception ex) { log.Error("Error: InViewCollection<Remove> ." + ex.Message + "\n" + ex.StackTrace); }
            //}, DispatcherPriority.Send);
        }
        public void DropNfIdandSeqViewCollection(Guid nfId)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                try
                {
                    //lock ?SJM
                    bool found = false;
                    int seq = 0;
                    int idx = 1;
                    for (; idx < AllFeedsViewCollection.Count - 3; idx++)
                    {
                        tempModel = AllFeedsViewCollection[idx];
                        if (tempModel.FeedId == nfId)
                        {
                            found = true;
                            seq = tempModel.SequenceForAllFeeds;
                            break;
                        }
                    }
                    if (found)
                    {
                        AllFeedsViewCollection.RemoveAt(idx);
                        CurrentFeedIds.Remove(nfId);
                        AfterRemovalSequenceBelowFeeds(idx, seq);
                    }
                }
                catch (Exception ex) { log.Error("Error: InViewCollection<Remove> ." + ex.Message + "\n" + ex.StackTrace); }
            }, DispatcherPriority.Send);
        }
        public void AllFeedsRequestFeeds(Guid pvtUUID, short scrollType, int startLimit, string packetId = null)
        {
            if (!AllFeedsRequestOn)
            {
                ThreadAnyFeedsRequest thread = new ThreadAnyFeedsRequest();
                thread.callBackEvent += (response) =>
                {
                    AllFeedsRequestOn = false;
                    if (scrollType != 1)
                    {
                        DummyModel1.FeedType = 12;
                        DummyModel2.FeedType = 12;
                        switch (response)
                        {
                            case SettingsConstants.RESPONSE_SUCCESS:
                                AllFeedsLoadMoreModel.FeedType = 1;

                                if (FeedDataContainer.Instance.AllBottomIds.Count < 5)
                                    ActionAllFeedsBottomLoad();
                                break;
                            case SettingsConstants.RESPONSE_NOTSUCCESS:
                                AllFeedsLoadMoreModel.FeedType = 4;
                                AllFeedsScrollEnabled = false;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCAllFeeds.Instance.scroll.ScrollToBottom();
                                    AllFeedsScrollEnabled = true;
                                }, DispatcherPriority.Send);
                                Thread.Sleep(100);
                                break;
                            case SettingsConstants.NO_RESPONSE:
                                AllFeedsLoadMoreModel.FeedType = 5;
                                AllFeedsScrollEnabled = false;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCAllFeeds.Instance.scroll.ScrollToBottom();
                                    AllFeedsScrollEnabled = true;
                                }, DispatcherPriority.Send);
                                Thread.Sleep(100);
                                break;
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                        switch (response)
                        {
                            case SettingsConstants.RESPONSE_SUCCESS:
                                //ActionAllFeedsTopLoad();
                                break;
                            default:
                                //if (AllFeedsLoadMoreModel.FeedType == 3)
                                //    AllFeedsLoadMoreModel.FeedType = 5;
                                break;
                        }
                    }
                };
                AllFeedsRequestOn = true;
                AllFeedsLoadMoreModel.FeedType = 3;
                thread.StartThread(pvtUUID, scrollType, startLimit, 0, AppConstants.TYPE_NEWS_FEED, packetId, 0, 0);
            }
        }
        public void ActionAllFeedsBottomLoad()
        {
            Guid toInsertFeedId = FeedDataContainer.Instance.AllBottomIds.LastFeedIdRemoved();
            if (toInsertFeedId != Guid.Empty && !CurrentFeedIds.Contains(toInsertFeedId))
            {
                FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                holder.FeedId = toInsertFeedId;
                EnqueueAllFeeds(holder);
            }
            else
            {
                Thread.Sleep(10);
                DummyModel1.FeedType = 11;
                DummyModel2.FeedType = 11;
            }
        }
        public void CalculateandInsertAllFeeds(FeedHolderModel model, bool alwaysInsert = false)
        {
            try
            {
                int max = AllFeedsViewCollection.Count - 3;
                int pivot, min = AllFeedSpecialFeedCount + 1;//1;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.Feed != null && AllFeedsViewCollection[pivot].Feed != null && model.Feed.Time < AllFeedsViewCollection[pivot].Feed.Time)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                if (!alwaysInsert
                    && UCAllFeeds.Instance != null
                    && (!UCAllFeeds.Instance.IsVisible || (min == 1 && UCAllFeeds.Instance.newStatusView != null && UCAllFeeds.Instance.scroll.VerticalOffset > (UCAllFeeds.Instance.newStatusView.ActualHeight + 50))))
                {
                    RingIDViewModel.Instance.FeedUnreadNotificationCounter++;
                    FeedDataContainer.Instance.AllTopIds.InsertIntoSortedList(model.Feed.Time, model.FeedId); //FeedDataContainer.Instance.UnreadNewsFeedIds.Add(model.FeedId);
                }
                else
                {
                    InsertModelandSeqViewCollection(min, model);
                    //SequenceAllFeeds(min, model);
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void EnqueueAllFeeds(FeedHolderModel modelToQueue)
        {
            if (modelToQueue != null)
                AllFeedsQueue.Enqueue(modelToQueue);
            if (AllFeedsBackgroundWorker == null)
            {
                AllFeedsBackgroundWorker = new BackgroundWorker();
                AllFeedsBackgroundWorker.WorkerSupportsCancellation = true;
                AllFeedsBackgroundWorker.DoWork += AllFeedsBackgroundWorker_DoWork;
                if (!AllFeedsBackgroundWorker.IsBusy)
                    AllFeedsBackgroundWorker.RunWorkerAsync();
            }
            else if (!AllFeedsBackgroundWorker.IsBusy)
            {
                AllFeedsBackgroundWorker.RunWorkerAsync();
            }
        }
        private void AllFeedsBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while (!AllFeedsQueue.IsEmpty)
                {
                    FeedHolderModel holder = null;
                    if (AllFeedsQueue.TryDequeue(out holder))
                    {
                        FeedModel fm = null;
                        if (FeedDataContainer.Instance.FeedModels.TryGetValue(holder.FeedId, out fm))
                        {
                            holder.Feed = fm;
                            AllFeedsScrollEnabled = false;
                            CalculateandInsertAllFeeds(holder, true);
                            AllFeedsScrollEnabled = true;
                            holder.ShortModelType = SettingsConstants.FEED_PARTIAL_VIEW;
                            holder.SelectViewTemplate();
                        }
                        else AllFeedsViewCollection.InvokeRemove(holder);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }
        public void ActionAllFeedsTopLoad()
        {
            Guid toInsertFeedId = FeedDataContainer.Instance.AllTopIds.FirstFeedIdRemoved();
            if (toInsertFeedId != Guid.Empty && !CurrentFeedIds.Contains(toInsertFeedId))
            {
                FeedHolderModel holder = new FeedHolderModel(2);
                holder.FeedId = toInsertFeedId;
                EnqueueAllFeeds(holder);
            }
            else
            {
                //Thread.Sleep(10);
            }
        }
        public void OnHomeRequestandRemoveBottomModels(bool IsSameUI = true)
        {
            bool unreadFeeds = (RingIDViewModel.Instance.FeedUnreadNotificationCounter > 0);
            RingIDViewModel.Instance.FeedUnreadNotificationCounter = 0;
            if (FeedDataContainer.Instance.AllTopIds.PvtMaxGuid == Guid.Empty)
                FeedDataContainer.Instance.AllTopIds.PvtMaxGuid = HelperMethods.GetMaxMinGuidInAllFeedCollection();
            if (AllFeedsViewCollection[1].SequenceForAllFeeds == 1)
                AllFeedsRequestFeeds(FeedDataContainer.Instance.AllTopIds.PvtMaxGuid, 1, 0);
            if (IsSameUI || unreadFeeds)
                DropAllKeeping1To20();
        }
        public void SequenceAllFeeds(FeedHolderModel holder)
        {
            try
            {

                int seqNumber = 1;
                for (int i = 1; i < AllFeedsViewCollection.Count - 3; i++)
                {
                    AllFeedsViewCollection[i].SequenceForAllFeeds = seqNumber;
                    seqNumber++;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }
        public void AfterRemovalSequenceBelowFeeds(int removedIdx, int removedSeq)
        {
            try
            {
                int seqNumber = removedSeq;
                for (int i = removedIdx; i < AllFeedsViewCollection.Count - 3; i++)
                {
                    AllFeedsViewCollection[i].SequenceForAllFeeds = seqNumber;
                    seqNumber++;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }
        public void SequenceTopFeeds()
        {
            try
            {
                int seqNumber = 0;
                for (int i = 1; i < AllFeedsViewCollection.Count - 1; i++)
                {
                    seqNumber++;
                    AllFeedsViewCollection[i].SequenceForAllFeeds = seqNumber;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }
        # endregion
        /// <summary>
        /// //////
        /// </summary>
        public static object LockObj = new object();
        public void InsertFirstTimeAllFeedModel(FeedHolderModel model)
        {
            try
            {
                //Application.Current.Dispatcher.Invoke((Action)delegate
                //{
                lock (LockObj)
                {
                    if (model.Feed != null && model.FeedId != Guid.Empty)
                    {
                        int idx = 1;
                        bool duplicate = false;
                        if (AllFeedsViewCollection.Count > 2)
                        {
                            for (; idx < AllFeedsViewCollection.Count - 1; idx++)
                            {
                                if (AllFeedsViewCollection[idx].Feed != null)
                                {
                                    if (model.FeedId == AllFeedsViewCollection[idx].FeedId)
                                    {
                                        duplicate = true;
                                        break;
                                    }
                                    if (model.Feed.Time > AllFeedsViewCollection[idx].Feed.Time)
                                        break;
                                }
                            }
                        }
                        if (!duplicate)
                        {
                            if (UCAllFeeds.Instance != null && (!UCAllFeeds.Instance.IsVisible || (idx == 1 && UCAllFeeds.Instance.newStatusView != null && UCAllFeeds.Instance.scroll.VerticalOffset > (UCAllFeeds.Instance.newStatusView.ActualHeight + 50))))
                            {
                                RingIDViewModel.Instance.FeedUnreadNotificationCounter++;
                                FeedDataContainer.Instance.UnreadNewsFeedIds.Add(model.FeedId);
                            }
                            else
                            {
                                AllFeedsViewCollection.InvokeInsertNoDuplicate(idx, model);
                                //SequenceAllFeeds();
                            }
                        }
                    }
                    //}, DispatcherPriority.Send);
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void InsertAllFeedsModelNoSeq(FeedHolderModel model, bool alwaysInsert = false)
        {
            try
            {
                int max = AllFeedsViewCollection.Count - 1;
                int pivot, min = AllFeedSpecialFeedCount + 1;//1;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.Feed != null && AllFeedsViewCollection[pivot].Feed != null && model.Feed.Time < AllFeedsViewCollection[pivot].Feed.Time)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                if (!alwaysInsert
                    && UCAllFeeds.Instance != null
                    && (!UCAllFeeds.Instance.IsVisible || (min == 1 && UCAllFeeds.Instance.newStatusView != null && UCAllFeeds.Instance.scroll.VerticalOffset > (UCAllFeeds.Instance.newStatusView.ActualHeight + 50))))
                {
                    RingIDViewModel.Instance.FeedUnreadNotificationCounter++;
                    FeedDataContainer.Instance.UnreadNewsFeedIds.Add(model.FeedId);
                }
                else
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        lock (AllFeedsViewCollection)
                        {
                            if (model.FeedId != Guid.Empty && min >= 1 && min <= AllFeedsViewCollection.Count - 1 && !AllFeedsViewCollection.Any(P => P.FeedId == model.FeedId)) //model.FeedType == 2
                            {
                                AllFeedsViewCollection.Insert(min, model);
                                //UpdateAllFeedSequenceNumbers();//AssignSeqNumberAllFeeds(model, min, 1);
                            }
                        }
                    }, DispatcherPriority.Send);
                    //UpdateAllFeedSequenceNumbers();
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }



        //strategy 1

        //public void ActionAllFeedsBottomLoad()
        //{
        //    if (AllFeedsBlankFeeds < 5 || (FeedDataContainer.Instance.AllBottomIds.Count > 0 && AllFeedsQueue.IsEmpty))
        //    {
        //        Guid toInsertFeedId = FeedDataContainer.Instance.AllBottomIds.LastFeedIdRemoved();
        //        if (toInsertFeedId != Guid.Empty)
        //        {
        //            FeedHolderModel holder = new FeedHolderModel(2);
        //            holder.FeedId = toInsertFeedId;
        //            AllFeedsBlankFeeds++;
        //            AllFeedsScrollEnabled = false;
        //            int idx = UCMiddlePanelSwitcher.View_UCAllFeeds.ViewCollection.Count - 1;
        //            AllFeedsViewCollection.InvokeInsert(idx, holder);
        //            AllFeedsScrollEnabled = true;
        //            EnqueueAllFeeds(holder);
        //        }
        //        else
        //            AllFeedsRequestFeeds(FeedDataContainer.Instance.AllBottomIds.PvtMinGuid, 2, 0);
        //    }
        //    else if (AllFeedsBackgroundWorker != null && !AllFeedsBackgroundWorker.IsBusy)
        //    {
        //        AllFeedsBackgroundWorker.RunWorkerAsync();
        //    }
        //}


        //strategy 2
        //public void ActionAllFeedsBottomLoad()
        //{
        //    if (AllFeedsBlankFeeds < 5 || (FeedDataContainer.Instance.AllBottomIds.Count > 0 && AllFeedsQueue.IsEmpty))
        //    {
        //        FeedHolderModel holder = new FeedHolderModel(2);
        //        AllFeedsBlankFeeds++;
        //        AllFeedsScrollEnabled = false;
        //        int idx = UCMiddlePanelSwitcher.View_UCAllFeeds.ViewCollection.Count - 1;
        //        AllFeedsViewCollection.InvokeInsert(idx, holder);
        //        AllFeedsScrollEnabled = true;
        //        EnqueueAllFeeds(holder);
        //    }
        //    else if (AllFeedsBackgroundWorker != null && !AllFeedsBackgroundWorker.IsBusy)
        //    {
        //        AllFeedsBackgroundWorker.RunWorkerAsync();
        //    }
        //}
        //private void AllFeedsBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    try
        //    {
        //        while (!AllFeedsQueue.IsEmpty)
        //        {
        //            FeedHolderModel holder = null;
        //            if (AllFeedsQueue.TryDequeue(out holder))
        //            {
        //                if (holder.BottomLoad)
        //                {
        //                    while (FeedDataContainer.Instance.AllBottomIds.Count == 0 && AllFeedsLoadMoreModel.FeedType != 4 && AllFeedsLoadMoreModel.FeedType != 5)
        //                    {
        //                        AllFeedsRequestFeeds(FeedDataContainer.Instance.AllBottomIds.PvtMinGuid, 2, 0);
        //                        Thread.Sleep(100);
        //                    }
        //                    Guid toInsertFeedId = FeedDataContainer.Instance.AllBottomIds.LastFeedIdRemoved();
        //                    if (toInsertFeedId != Guid.Empty)
        //                    {
        //                        holder.Feed = FeedDataContainer.Instance.FeedModels[toInsertFeedId];
        //                        holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                        AllFeedsBlankFeeds--;
        //                        holder.FeedId = holder.Feed.NewsfeedId;
        //                        holder.ShortModel = false;
        //                        if (holder.Feed.ParentFeed != null && holder.Feed.ParentFeed.WhoShareList != null && holder.Feed.ParentFeed.WhoShareList.Count > 1)
        //                        {
        //                            holder.FeedPanelType = 20;
        //                            holder.FirstSharer = holder.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
        //                            if (holder.Feed.ParentFeed.WhoShareList.Count == 2)
        //                                holder.SecondSharer = holder.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
        //                            else
        //                                holder.ExtraSharerCount = (holder.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " Others";
        //                            Application.Current.Dispatcher.Invoke((Action)delegate
        //                            {
        //                                FeedHolderModel prevExtModel = AllFeedsViewCollection.Where(P => (P.Feed != null && P.Feed.ParentFeed != null && P.Feed.ParentFeed.NewsfeedId == holder.Feed.ParentFeed.NewsfeedId)).FirstOrDefault();
        //                                if (prevExtModel != null)
        //                                {
        //                                    AllFeedsViewCollection.Remove(prevExtModel);
        //                                }
        //                            }, DispatcherPriority.Send);
        //                        }
        //                        else
        //                        {
        //                            holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                        }
        //                        if (holder.SequenceForAllFeeds > 0)
        //                        {
        //                            holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        AllFeedsViewCollection.InvokeRemove(holder);
        //                    }
        //                    UpdateAllFeedSequenceNumbers();
        //                }
        //                else AllFeedsViewCollection.InvokeRemove(holder);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.StackTrace);
        //    }
        //}
        //public void EnqueueAllFeeds(FeedHolderModel modelToQueue)
        //{
        //    if (modelToQueue != null)
        //        AllFeedsQueue.Enqueue(modelToQueue);
        //    if (AllFeedsBackgroundWorker == null)
        //    {
        //        AllFeedsBackgroundWorker = new BackgroundWorker();
        //        AllFeedsBackgroundWorker.WorkerSupportsCancellation = true;
        //        AllFeedsBackgroundWorker.DoWork += AllFeedsBackgroundWorker_DoWork;
        //        if (!AllFeedsBackgroundWorker.IsBusy)
        //            AllFeedsBackgroundWorker.RunWorkerAsync();
        //    }
        //    else if (!AllFeedsBackgroundWorker.IsBusy)
        //    {
        //        AllFeedsBackgroundWorker.RunWorkerAsync();
        //    }
        //}

        //strategy 3
        //public void ActionAllFeedsBottomLoad()
        //{
        //    if (AllFeedsBlankFeeds < 5 || (FeedDataContainer.Instance.AllBottomIds.Count > 0 && AllFeedsQueue.IsEmpty))
        //    {
        //        FeedHolderModel holder = new FeedHolderModel(2);


        //        AllFeedsScrollEnabled = false;
        //        //lock (AllFeedsQueue)
        //        //{
        //        int idx = UCMiddlePanelSwitcher.View_UCAllFeeds.ViewCollection.Count - 1;
        //        AllFeedsViewCollection.InvokeInsert(idx, holder);
        //        //for (int i = 0; i < FeedDataContainer.Instance.AllBottomIds.Count; i++)
        //        //{
        //        //    log.Error("WAITING IDS-> " + i + "=>" + FeedDataContainer.Instance.AllBottomIds.ElementAt(i).Key);
        //        //}
        //        Thread.Sleep(10);
        //        AllFeedsQueue.Enqueue(holder);
        //        //}
        //        AllFeedsBlankFeeds++;
        //        AllFeedsScrollEnabled = true;

        //        //lock (AllFeedsQueue)
        //        //if (holder != null) AllFeedsQueue.Enqueue(holder);
        //        StartBgWorker();
        //    }
        //    else if (AllFeedsBackgroundWorker != null && !AllFeedsBackgroundWorker.IsBusy)
        //    {
        //        AllFeedsBackgroundWorker.RunWorkerAsync();
        //    }
        //}
        //private void AllFeedsBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    try
        //    {
        //        while (!AllFeedsQueue.IsEmpty)
        //        {
        //            FeedHolderModel holder = null;
        //            if (AllFeedsQueue.TryDequeue(out holder))
        //            {
        //                if (holder.BottomLoad)
        //                {
        //                    while (FeedDataContainer.Instance.AllBottomIds.Count == 0 && AllFeedsLoadMoreModel.FeedType != 4 && AllFeedsLoadMoreModel.FeedType != 5)
        //                    {
        //                        AllFeedsRequestFeeds(FeedDataContainer.Instance.AllBottomIds.PvtMinGuid, 2, 0);
        //                        Thread.Sleep(100);
        //                    }
        //                    Guid toInsertFeedId = FeedDataContainer.Instance.AllBottomIds.LastFeedIdRemoved();
        //                    FeedModel fm = null;
        //                    if (toInsertFeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(toInsertFeedId, out fm))
        //                    {
        //                        holder.Feed = fm;
        //                        holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                        holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
        //                        AllFeedsBlankFeeds--;
        //                        holder.FeedId = holder.Feed.NewsfeedId;
        //                        //for (int i = 1; i < AllFeedsViewCollection.Count - 1; i++)
        //                        //{
        //                        //    FeedHolderModel fmh = AllFeedsViewCollection.ElementAt(i);
        //                        //    if (fmh.Feed != null)
        //                        //        log.Error("SEQ-> " + i + "=>" + fmh.Feed.Time);
        //                        //}
        //                        holder.SequenceForAllFeeds = BottomSeqCount++;
        //                        holder.ShortModel = false;
        //                        if (holder.Feed.ParentFeed != null && holder.Feed.ParentFeed.WhoShareList != null && holder.Feed.ParentFeed.WhoShareList.Count > 1)
        //                        {
        //                            holder.FeedPanelType = 20;
        //                            holder.FirstSharer = holder.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
        //                            if (holder.Feed.ParentFeed.WhoShareList.Count == 2)
        //                                holder.SecondSharer = holder.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
        //                            else
        //                                holder.ExtraSharerCount = (holder.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " Others";
        //                            Application.Current.Dispatcher.Invoke((Action)delegate
        //                            {
        //                                FeedHolderModel prevExtModel = AllFeedsViewCollection.Where(P => (P.Feed != null && P.Feed.ParentFeed != null && P.Feed.ParentFeed.NewsfeedId == holder.Feed.ParentFeed.NewsfeedId)).FirstOrDefault();
        //                                if (prevExtModel != null)
        //                                {
        //                                    AllFeedsViewCollection.Remove(prevExtModel);
        //                                }
        //                            }, DispatcherPriority.Send);
        //                        }
        //                        else
        //                        {
        //                            holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                        }
        //                        if (holder.SequenceForAllFeeds > 0)
        //                        {
        //                            holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        AllFeedsViewCollection.InvokeRemove(holder);
        //                    }
        //                    UpdateAllFeedSequenceNumbers();
        //                }
        //                else AllFeedsViewCollection.InvokeRemove(holder);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.StackTrace);
        //    }
        //}
        //public void StartBgWorker()
        //{
        //    if (AllFeedsBackgroundWorker == null)
        //    {
        //        AllFeedsBackgroundWorker = new BackgroundWorker();
        //        AllFeedsBackgroundWorker.WorkerSupportsCancellation = true;
        //        AllFeedsBackgroundWorker.DoWork += AllFeedsBackgroundWorker_DoWork;
        //        if (!AllFeedsBackgroundWorker.IsBusy)
        //            AllFeedsBackgroundWorker.RunWorkerAsync();
        //    }
        //    else if (!AllFeedsBackgroundWorker.IsBusy)
        //    {
        //        AllFeedsBackgroundWorker.RunWorkerAsync();
        //    }
        //}        
        //int TopSeqCount = 0, BottomSeqCount = 0;
        //public void TopLoadUnreadAllFeeds()
        //{
        //    try
        //    {
        //        RingIDViewModel.Instance.FeedUnreadNotificationCounter = 0;
        //        for (int i = 0; i < FeedDataContainer.Instance.UnreadNewsFeedIds.Count; i++)
        //        {
        //            Guid nfId = FeedDataContainer.Instance.UnreadNewsFeedIds[i];
        //            FeedModel modeltoInsert = null;
        //            if (FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out modeltoInsert) && !AllFeedsViewCollection.Any(P => P.FeedId == modeltoInsert.NewsfeedId))
        //            {
        //                FeedHolderModel holder = new FeedHolderModel(2);
        //                holder.FeedId = nfId;
        //                holder.Feed = modeltoInsert;
        //                if (holder.Feed.ParentFeed != null && holder.Feed.ParentFeed.WhoShareList != null && holder.Feed.ParentFeed.WhoShareList.Count > 1)
        //                {
        //                    holder.FeedPanelType = 20;
        //                    holder.FirstSharer = holder.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
        //                    if (holder.Feed.ParentFeed.WhoShareList.Count == 2)
        //                        holder.SecondSharer = holder.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
        //                    else
        //                        holder.ExtraSharerCount = (holder.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " others";
        //                }
        //                else
        //                {
        //                    holder.FeedPanelType = holder.Feed.FeedPanelType;
        //                }
        //                //holder.SequenceForAllFeeds = BottomSeqCount++;
        //                holder.ShortModel = false;
        //                CalculateandInsertAllFeeds(holder, true);
        //            }
        //        }
        //        FeedDataContainer.Instance.UnreadNewsFeedIds.Clear();
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.StackTrace);
        //    }
        //}
        //public void ActionAllFeedsTopLoad()
        //{
        //    Guid toInsertFeedId = FeedDataContainer.Instance.AllTopIds.FirstFeedIdRemoved();
        //    if (toInsertFeedId != Guid.Empty)
        //    {
        //        FeedHolderModel holder = new FeedHolderModel(2);
        //        holder.BottomLoad = false;
        //        holder.Feed = FeedDataContainer.Instance.FeedModels[toInsertFeedId];
        //        if (holder.Feed.ParentFeed != null && holder.Feed.ParentFeed.WhoShareList != null && holder.Feed.ParentFeed.WhoShareList.Count > 1)
        //        {
        //            holder.FeedPanelType = 20;
        //            holder.FirstSharer = holder.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
        //            if (holder.Feed.ParentFeed.WhoShareList.Count == 2)
        //                holder.SecondSharer = holder.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
        //            else
        //                holder.ExtraSharerCount = (holder.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " others";
        //            Application.Current.Dispatcher.Invoke((Action)delegate
        //            {
        //                FeedHolderModel prevExtModel = AllFeedsViewCollection.Where(P => (P.Feed != null && P.Feed.ParentFeed != null && P.Feed.ParentFeed.NewsfeedId == holder.Feed.ParentFeed.NewsfeedId)).FirstOrDefault();
        //                if (prevExtModel != null)
        //                {
        //                    AllFeedsViewCollection.Remove(prevExtModel);
        //                }
        //            }, DispatcherPriority.Send);
        //        }
        //        else
        //        {
        //            holder.FeedPanelType = holder.Feed.FeedPanelType;
        //        }
        //        holder.FeedId = holder.Feed.NewsfeedId;
        //        AllFeedsViewCollection.InvokeInsert(1, holder);
        //        if (TopSeqCount == 0)
        //        {
        //            UpdateAllFeedSequenceNumbers();
        //        }
        //        else holder.SequenceForAllFeeds = TopSeqCount--;
        //        //InsertAllFeedsModel(holder, true);
        //        holder.ShortModel = false;
        //    }
        //    else
        //    {
        //        if (FeedDataContainer.Instance.AllTopIds.PvtMaxGuid == Guid.Empty)
        //            FeedDataContainer.Instance.AllTopIds.PvtMaxGuid = HelperMethods.GetMaxMinGuidInFeedCollection(AllFeedsViewCollection, 1); //ViewCollection.Max(y => y.FeedId); 
        //        AllFeedsRequestFeeds(FeedDataContainer.Instance.AllTopIds.PvtMaxGuid, 1, 0);
        //    }
        //}
        #endregion
        private UCLikeList LikeListView { get; set; }

        private UCMediaShareMoreOptions ucMediaShareMoreOptions { get; set; }

        public UCShareSingleFeedView ShareSingleFeedView
        {
            get { return MainSwitcher.PopupController.ShareSingleFeedView; }
        }

        public UCEditedHistoryView ucEditedHistoryView
        {
            get { return MainSwitcher.PopupController.ucEditedHistoryView; }
        }
        #region "ICommand"
        private ICommand _stsEditHistoryMouseEnter;
        public ICommand stsEditHistoryMouseEnter
        {
            get
            {
                if (_stsEditHistoryMouseEnter == null)
                {
                    _stsEditHistoryMouseEnter = new RelayCommand(param => OnStsEditHistoryMouseEnter(param));
                }
                return _stsEditHistoryMouseEnter;
            }
        }

        private ICommand _stsEditHistoryMouseLeave;
        public ICommand stsEditHistoryMouseLeave
        {
            get
            {
                if (_stsEditHistoryMouseLeave == null)
                {
                    _stsEditHistoryMouseLeave = new RelayCommand(param => OnStsEditHistoryMouseLeave(param));
                }
                return _stsEditHistoryMouseLeave;
            }
        }

        private ICommand _NameCommand;
        public ICommand NameCommand
        {
            get
            {
                if (_NameCommand == null)
                {
                    _NameCommand = new RelayCommand(param => OnNameCommand(param));
                }
                return _NameCommand;
            }
        }

        private ICommand _TagFirstCommand;
        public ICommand TagFirstCommand
        {
            get
            {
                if (_TagFirstCommand == null)
                {
                    _TagFirstCommand = new RelayCommand(param => OnTagFirstCommand(param));
                }
                return _TagFirstCommand;
            }
        }

        private ICommand _TagSecondCommand;
        public ICommand TagSecondCommand
        {
            get
            {
                if (_TagSecondCommand == null)
                {
                    _TagSecondCommand = new RelayCommand(param => OnTagSecondCommand(param));
                }
                return _TagSecondCommand;
            }
        }

        private ICommand _StsEditHistoryCommand;
        public ICommand StsEditHistoryCommand
        {
            get
            {
                if (_StsEditHistoryCommand == null)
                {
                    _StsEditHistoryCommand = new RelayCommand(param => OnStsEditHistoryCommand(param));
                }
                return _StsEditHistoryCommand;
            }
        }

        private ICommand _seeMoreCommand;
        public ICommand SeeMoreCommand
        {
            get
            {
                if (_seeMoreCommand == null)
                {
                    _seeMoreCommand = new RelayCommand(param => OnStatusSeeMoreCommand(param));
                }
                return _seeMoreCommand;
            }
        }

        private ICommand _LocationCommand;
        public ICommand LocationCommand
        {
            get
            {
                if (_LocationCommand == null)
                {
                    _LocationCommand = new RelayCommand(param => OnLocationClicked(param));
                }
                return _LocationCommand;
            }
        }

        private ICommand _PortalImageClickCommand;
        public ICommand PortalImageClickCommand
        {
            get
            {
                if (_PortalImageClickCommand == null)
                {
                    _PortalImageClickCommand = new RelayCommand(param => OnImageClick(param));
                }
                return _PortalImageClickCommand;
            }
        }
        private ICommand _rightSingleMediaCommand;
        public ICommand RightSingleMediaCommand
        {
            get
            {
                if (_rightSingleMediaCommand == null)
                {
                    _rightSingleMediaCommand = new RelayCommand(param => OnRightSingleMediaClick(param));
                }
                return _rightSingleMediaCommand;
            }
        }

        private ICommand _playSingleMediaCommand;
        public ICommand PlaySingleMediaCommand
        {
            get
            {
                if (_playSingleMediaCommand == null)
                {
                    _playSingleMediaCommand = new RelayCommand(param => OnPlaySingleMediaCommand(param));
                }
                return _playSingleMediaCommand;
            }
        }

        private ICommand _previewLinkCommand;
        public ICommand PreviewLinkCommand
        {
            get
            {
                if (_previewLinkCommand == null)
                {
                    _previewLinkCommand = new RelayCommand(param => OnPreviewLinkCommand(param));
                }
                return _previewLinkCommand;
            }
        }

        private ICommand _moreMediaBtnClick;
        public ICommand MoreMediaBtnClick
        {
            get
            {
                if (_moreMediaBtnClick == null)
                {
                    _moreMediaBtnClick = new RelayCommand(param => OnMoreMediaBtnClick(param));
                }
                return _moreMediaBtnClick;
            }
        }

        private ICommand _shareShowMoreButtonClick;
        public ICommand ShareShowMoreButtonClick
        {
            get
            {
                if (_shareShowMoreButtonClick == null)
                {
                    _shareShowMoreButtonClick = new RelayCommand(param => OnShareShowMoreButtonClick(param));
                }
                return _shareShowMoreButtonClick;
            }
        }

        private ICommand _newsPreviewPanelCommand;
        public ICommand NewsPreviewPanelCommand
        {
            get
            {
                if (_newsPreviewPanelCommand == null)
                {
                    _newsPreviewPanelCommand = new RelayCommand(param => OnNewsPreviewPanelCommand(param));
                }
                return _newsPreviewPanelCommand;
            }
        }

        private ICommand _likeCommand;
        public ICommand LikeCommand
        {
            get
            {
                if (_likeCommand == null)
                {
                    _likeCommand = new RelayCommand(param => OnLikeCommand(param));
                }
                return _likeCommand;
            }
        }

        private ICommand _noOfLikeCommand;
        public ICommand NoOfLikeCommand
        {
            get
            {
                if (_noOfLikeCommand == null)
                {
                    _noOfLikeCommand = new RelayCommand(param => OnNumberOfLikeCommand(param));
                }
                return _noOfLikeCommand;
            }
        }

        private ICommand _NewsFullStoryCommand;
        public ICommand NewsFullStoryCommand
        {
            get
            {
                if (_NewsFullStoryCommand == null)
                {
                    _NewsFullStoryCommand = new RelayCommand(param => OnNewsFullStoryClickedCommand(param));
                }
                return _NewsFullStoryCommand;
            }
        }

        private ICommand shareMediaCommand;
        public ICommand ShareMediaCommand
        {
            get
            {
                if (shareMediaCommand == null) shareMediaCommand = new RelayCommand(param => OnShareMediaCommand(param));
                return shareMediaCommand;
            }
        }
        //private ICommand _commentCommand;
        //public ICommand CommentCommand
        //{
        //    get
        //    {
        //        if (_commentCommand == null)
        //        {
        //            _commentCommand = new RelayCommand(param => OnCommentCommand(param));
        //        }
        //        return _commentCommand;
        //    }
        //}

        private ICommand _ShareTextBlockCommand;
        public ICommand ShareTextBlockCommand
        {
            get
            {
                if (_ShareTextBlockCommand == null)
                {
                    _ShareTextBlockCommand = new RelayCommand(param => OnShareTextBlockCommand(param));
                }
                return _ShareTextBlockCommand;
            }
        }

        private ICommand _ShowShareListCommand;
        public ICommand ShowShareListCommand
        {
            get
            {
                if (_ShowShareListCommand == null)
                {
                    _ShowShareListCommand = new RelayCommand(param => OnShowShareListCommand(param));
                }
                return _ShowShareListCommand;
            }
        }

        private ICommand _ShareDetailsAndNewsCommand;
        public ICommand ShareDetailsAndNewsCommand
        {
            get
            {
                if (_ShareDetailsAndNewsCommand == null)
                {
                    _ShareDetailsAndNewsCommand = new RelayCommand(param => OnShareDetailsAndNewsPreviewCommand(param));
                }
                return _ShareDetailsAndNewsCommand;
            }
        }

        private ICommand _ActivityNameToolTipOpenCommand;
        public ICommand ActivityNameToolTipOpenCommand
        {
            get
            {
                if (_ActivityNameToolTipOpenCommand == null)
                {
                    _ActivityNameToolTipOpenCommand = new RelayCommand(param => OnActivityNameToolTipOpenCommand(param));
                }
                return _ActivityNameToolTipOpenCommand;
            }
        }

        private ICommand _nameToolTipHiddenCommand;
        public ICommand NameToolTipHiddenCommand
        {
            get
            {
                if (_nameToolTipHiddenCommand == null) _nameToolTipHiddenCommand = new RelayCommand(param => OnNameToolTipHiddenCommand(param));
                return _nameToolTipHiddenCommand;
            }
        }
        #endregion

        public void ShowLikeAnimation(FeedModel _FeedModel)
        {
            if (_FeedModel.PopupLikeLoader == null)
            {
                _FeedModel.PopupLikeLoader = ImageLocation.LOADER_LIKE_ANIMATION;
                _FeedModel.PopupLikeLoaderVisibility = Visibility.Visible;
                new System.Threading.Thread(delegate()
                {
                    Thread.Sleep(2000);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        _FeedModel.PopupLikeLoader = null;
                        _FeedModel.PopupLikeLoaderVisibility = Visibility.Collapsed;
                    });
                }).Start();

            }
            else
            {
                _FeedModel.PopupLikeLoader = null;
                _FeedModel.PopupLikeLoaderVisibility = Visibility.Collapsed;
            }
        }

        private ICommand _VirtualizingStackPanelLoaded;
        public ICommand VirtualizingStackPanelLoaded
        {
            get
            {
                if (_VirtualizingStackPanelLoaded == null) _VirtualizingStackPanelLoaded = new RelayCommand(param => OnVirtualizingStackPanelLoaded(param));
                return _VirtualizingStackPanelLoaded;
            }
        }

        private ICommand _newsPortalSliderLoaded;
        public ICommand NewsPortalSliderLoaded
        {
            get
            {
                if (_newsPortalSliderLoaded == null) _newsPortalSliderLoaded = new RelayCommand(param => OnNewsPortalSliderLoaded(param));
                return _newsPortalSliderLoaded;
            }
        }

        private ICommand _newsPortalVisibleChaged;
        public ICommand NewsPortalVisibleChanged
        {
            get
            {
                if (_newsPortalVisibleChaged == null) _newsPortalVisibleChaged = new RelayCommand(param => OnNewsPortalVisibleChanged(param));
                return _newsPortalVisibleChaged;
            }
        }

        private ICommand downloadBtnCommand;
        public ICommand DownloadBtnCommand
        {
            get
            {
                if (downloadBtnCommand == null) downloadBtnCommand = new RelayCommand(param => OnDownloadBtnCommand(param));
                return downloadBtnCommand;
            }
        }

        private ICommand _AlbumContentCommand;
        public ICommand AlbumContentCommand
        {
            get
            {
                if (_AlbumContentCommand == null) _AlbumContentCommand = new RelayCommand(param => OnAlbumContentCommand(param));
                return _AlbumContentCommand;
            }
        }
        #region "Private Methods"
        private void OnNameCommand(object parameter)
        {
            if (parameter is BaseUserProfileModel)//&& !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)//07.02.17 order by leader
            {
                try
                {
                    BaseUserProfileModel PostOwner = (BaseUserProfileModel)parameter;
                    RingIDViewModel.Instance.OnUserProfileClicked(PostOwner);
                }
                catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
            }
            else if (parameter is FeedModel && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
            {
                FeedModel _feedModel = (FeedModel)parameter;
                if (UCFeedTagListView.Instance == null)
                    UCFeedTagListView.Instance = new UCFeedTagListView(UCGuiRingID.Instance.MotherPanel);
                UCFeedTagListView.Instance.Show();
                ObservableCollection<BaseUserProfileModel> sharers = new ObservableCollection<BaseUserProfileModel>();
                foreach (var item in _feedModel.ParentFeed.WhoShareList)
                {
                    sharers.Add(item.Feed.PostOwner);
                }
                UCFeedTagListView.Instance.ShowSharersView(sharers);
                //if (_feedModel.ParentFeed.LikeCommentShare.NumberOfShares > _feedModel.ParentFeed.WhoShareList.Count)
                SendDataToServer.SendShareListRequest(_feedModel.ParentFeed.NewsfeedId);
                UCFeedTagListView.Instance.OnRemovedUserControl += () =>
                {
                    UCFeedTagListView.Instance = null;
                };
            }
        }

        private void OnTagFirstCommand(object parameter)
        {
            if (parameter is BaseUserProfileModel)//&& !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)//07.02.17 order by leader
            {
                BaseUserProfileModel FirstTagProfile = (BaseUserProfileModel)parameter;
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCSingleFeedDetailsView.Instance.Hide();
                if (FirstTagProfile != null) RingIDViewModel.Instance.OnUserProfileClicked(FirstTagProfile);
            }
        }

        private void OnTagSecondCommand(object parameter)
        {
            if (parameter is FeedModel)//&& !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)//07.02.17 order by leader
            {
                FeedModel _feedModel = (FeedModel)parameter;
                if (_feedModel.SecondTagProfile != null)
                {
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance.Hide();
                    RingIDViewModel.Instance.OnUserProfileClicked(_feedModel.SecondTagProfile);
                }
                else if (!string.IsNullOrEmpty(_feedModel.ExtraTagCount) && _feedModel.TaggedFriendsList != null && _feedModel.TaggedFriendsList.Count > 0 && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (UCFeedTagListView.Instance != null)
                    {
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(UCFeedTagListView.Instance);
                        else UCGuiRingID.Instance.MotherPanel.Children.Remove(UCFeedTagListView.Instance);
                    }
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCFeedTagListView.Instance = new UCFeedTagListView(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                    else UCFeedTagListView.Instance = new UCFeedTagListView(UCGuiRingID.Instance.MotherPanel);
                    UCFeedTagListView.Instance.Show();
                    UCFeedTagListView.Instance.ShowTagView(_feedModel.TaggedFriendsList);
                    UCFeedTagListView.Instance.OnRemovedUserControl += () =>
                    {
                        UCFeedTagListView.Instance = null;
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        {
                            UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        }
                    };
                }
            }
        }

        private void OnStsEditHistoryCommand(object parameter)
        {
            if (parameter is FeedModel)
            {
                FeedModel _FeedModel = (FeedModel)parameter;
                ucEditedHistoryView.Show();
                ucEditedHistoryView.ShowEditHistoryView(_FeedModel);
            }
        }

        private void OnStatusSeeMoreCommand(object parameter)
        {
            try
            {
                if (parameter is FeedModel && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                {
                    FeedModel ModelToSeeMore = (FeedModel)parameter;
                    MainSwitcher.ThreadManager().DetailsOfaFeed.CallBackEvent += (dto) =>
                    {
                        if (dto != null) ModelToSeeMore.LoadData(dto);
                    };
                    MainSwitcher.ThreadManager().DetailsOfaFeed.StartThread(ModelToSeeMore.NewsfeedId);
                }
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void OnLocationClicked(object parameter)
        {
            if (parameter is LocationModel)
            {
                LocationModel locationModel = (LocationModel)parameter;
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCFeedLocationView.Instance = new UCFeedLocationView(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                else UCFeedLocationView.Instance = new UCFeedLocationView(UCGuiRingID.Instance.MotherPanel);
                UCFeedLocationView.Instance.Show();
                UCFeedLocationView.Instance.ShowLocationView((float)locationModel.Latitude, (float)locationModel.Longitude, locationModel.LocationName);
                UCFeedLocationView.Instance.OnRemovedUserControl += () =>
                {
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                };
                //UCFeedLocationWindow.Instance.ShowWindow((float)locationModel.Latitude, (float)locationModel.Longitude, locationModel.LocationName);
            }
        }

        private void OnImageClick(object param)
        {
            try
            {
                ImageModel obj = (ImageModel)param;
                if (!obj.NoImageFound)
                {
                    ObservableCollection<ImageModel> lst = new ObservableCollection<ImageModel>();
                    lst.Add(obj);
                    ImageUtility.ShowImageViewFromPortalPages(lst, obj.ImageId, true);
                }
                //if (param is ObservableCollection<ImageModel>)
                //{
                //    ObservableCollection<ImageModel> imgModels = (ObservableCollection<ImageModel>)param;
                //    Guid imageid = imgModels.ElementAt(0).ImageId;
                //    ImageUtility.ShowImageViewFromPortalPages(imgModels, imageid, true);
                //}
            }
            catch (Exception ex) { log.Error("Error: OnImageClick() ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void OnRightSingleMediaClick(object parameter)
        {
            Grid grid = (Grid)parameter;
            if (grid.DataContext is SingleMediaModel)
            {
                SingleMediaModel item = (SingleMediaModel)grid.DataContext;
                if (grid.ContextMenu != ContextMenuMediaOptions.Instance.cntxMenu)
                {
                    grid.ContextMenu = ContextMenuMediaOptions.Instance.cntxMenu;
                    ContextMenuMediaOptions.Instance.SetupMenuItem();
                }
                ContextMenuMediaOptions.Instance.ShowHandler(grid, item, item.MediaOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID ? 1 : -1);
                ContextMenuMediaOptions.Instance.cntxMenu.IsOpen = true;
            }
        }

        private void OnPlaySingleMediaCommand(object parameter)
        {
            if (parameter is SingleMediaModel)
            {
                SingleMediaModel item = (SingleMediaModel)parameter;
                if (item.NewsFeedId != Guid.Empty)
                {
                    FeedModel model = null;
                    if (FeedDataContainer.Instance.FeedModels.TryGetValue(item.NewsFeedId, out model) && model.MediaContent != null && model.MediaContent.MediaList != null)
                    {
                        foreach (var singleMedia in model.MediaContent.MediaList)
                        {
                            singleMedia.PlayedFromFeed = true;
                        }
                        int idx = model.MediaContent.MediaList.IndexOf(item);
                        MediaUtility.RunPlayList(model.NewsModel != null, model.NewsfeedId, model.MediaContent.MediaList, model.PostOwner, idx);
                    }
                }
                else
                {
                    MediaUtility.PlaySingleMediaNoContentID(item, false);
                }
                //int idx = FeedModel.MediaContent.MediaList.IndexOf(item);
                //BaseUserProfileModel ownerOfMedia = null;
                //if (item.UserTableID == FeedModel.PostOwner.UserTableID) ownerOfMedia = FeedModel.PostOwner;
                //else ownerOfMedia = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(item.UserTableID, 0, item.FullName, item.ProfileImage);

                //if (idx >= 0 && idx < FeedModel.MediaContent.MediaList.Count)
                //{
                //    MediaUtility.RunPlayList(FeedModel.PortalMediaModel != null, FeedModel.NewsfeedId, FeedModel.MediaContent.MediaList, ownerOfMedia, idx);

                //}

            }
            //else if (b.DataContext is UCSingleFeed && ((int)Tag == DefaultSettings.FEED_TYPE_DETAILS))
            //{
            //    UCSingleFeed singleFeed = (UCSingleFeed)b.DataContext;
            //    if (singleFeed.FeedModel != null && FeedModel.MediaContent != null && FeedModel.MediaContent.MediaList.Count == 1)
            //        MediaUtility.RunPlayList(true, FeedModel.NewsfeedId, FeedModel.MediaContent.MediaList, FeedModel.PostOwner, 0);
            //}
        }

        private void OnPreviewLinkCommand(object parameter)
        {
            if (parameter is string)
            {
                string url = (string)parameter;
                try
                {
                    string youtubeVideoID;
                    if (HelperMethods.ParseYoutubeURL(url, out youtubeVideoID))
                    {
                        using (System.Windows.Forms.WebBrowser wb = new System.Windows.Forms.WebBrowser())
                        {
                            if (wb.Version.Major == 8)
                            {
                                string defaultBrowser = HelperMethods.GetStandardBrowserPath();
<<<<<<< HEAD
                                if ( !string.IsNullOrEmpty( defaultBrowser) && !defaultBrowser.Substring(defaultBrowser.LastIndexOf("\\") + 1).ToLower().Contains("iexplore"))
=======
                                if (!string.IsNullOrEmpty(defaultBrowser) && !defaultBrowser.Substring(defaultBrowser.LastIndexOf("\\") + 1).ToLower().Contains("iexplore"))
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                                {
                                    HelperMethods.GoToSite(string.Format(SocialMediaConstants.YOUTUBE_NORMAL_URL, youtubeVideoID));
                                    return;
                                }
                                else
                                {
                                    UIHelperMethods.ShowFailed(NotificationMessages.MESSAGE_YOUTUBEVIEWER_BROWSER_DEPRECATED);
                                    return;
                                }
                            }
                        }
<<<<<<< HEAD
                        
                        if (HelperMethods.YoutubeFileExists(youtubeVideoID))
                        {
                            UIHelperMethods.OpenYoutubeViewer(youtubeVideoID);
=======

                        if (HelperMethods.YoutubeFileExists(youtubeVideoID))
                        {
                            //UIHelperMethods.OpenYoutubeViewer(youtubeVideoID);
                            ThreadStart start = delegate { UIHelperMethods.OpenYoutubeViewer(youtubeVideoID); };
                            new Thread(start).Start();
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                        }
                        else
                        {
                            UIHelperMethods.ShowFailed(NotificationMessages.MESSAGE_YOUTUBEVIEWER_FILE_PLAYING_FAILED);
                        }
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(url);
                    }
                }
                catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
            }
        }

        private void OnMoreMediaBtnClick(object parameter)
        {
            if (parameter is FeedModel)
            {
                try
                {
                    FeedModel feedModel = (FeedModel)parameter;
                    if (feedModel.ExtraMediaCount.StartsWith("+"))
                    {
                        if (feedModel.MediaContent.TotalMediaCount == feedModel.MediaContent.MediaList.Count)
                        {
                            for (int i = feedModel.MediaContent.FeedMediaList.Count; i < feedModel.MediaContent.MediaList.Count; i++)
                            {
                                feedModel.MediaContent.FeedMediaList.Add(feedModel.MediaContent.MediaList.ElementAt(i));
                            }
                            feedModel.ExtraMediaCount = "Show Less";
                        }
                        else
                        {
                            ThreadFetchExtraMedia thrd = new ThreadFetchExtraMedia();
                            thrd.CallBack += (response, mediaArray) =>
                            {
                                if (response == SettingsConstants.RESPONSE_SUCCESS)
                                {
                                    feedModel.ExtraMediaCount = "Show Less";
                                    foreach (JObject singleMediaObj in mediaArray)
                                    {
                                        Guid contentId = (Guid)singleMediaObj[JsonKeys.ContentId];
                                        SingleMediaModel singleMediaModel = null;
                                        if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel)) { singleMediaModel = new SingleMediaModel(); MediaDataContainer.Instance.ContentModels[contentId] = singleMediaModel; }
                                        singleMediaModel.LoadData(singleMediaObj, singleMediaModel.MediaType);
                                        if (singleMediaModel.MediaOwner == null) singleMediaModel.MediaOwner = feedModel.PostOwner;
                                        singleMediaModel.NewsFeedId = feedModel.NewsfeedId;
                                        singleMediaModel.MediaType = feedModel.MediaContent.MediaType;
                                        feedModel.MediaContent.MediaList.InvokeAddNoDuplicate(singleMediaModel);
                                        feedModel.MediaContent.FeedMediaList.InvokeAddNoDuplicate(singleMediaModel);
                                    }
                                }
                            };
                            thrd.StartThread(feedModel.NewsfeedId, 1, 0);
                        }
                    }
                    else
                    {
                        for (int i = feedModel.MediaContent.FeedMediaList.Count - 1; i >= 2; i--)
                            feedModel.MediaContent.FeedMediaList.RemoveAt(i);
                        feedModel.ExtraMediaCount = "+" + (feedModel.MediaContent.MediaList.Count - 2) + " More";
                    }
                }
                catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
            }
        }

        private void OnShareShowMoreButtonClick(object parameter)
        {
            if (parameter is FeedModel)
            {
                try
                {
                    FeedModel feedModdel = (FeedModel)parameter;
                    JObject pakToSend = new JObject();
                    pakToSend[JsonKeys.NewsfeedId] = feedModdel.NewsfeedId;
                    pakToSend[JsonKeys.StartLimit] = feedModdel.WhoShareList.Count;
                    (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_WHO_SHARES_LIST, AppConstants.REQUEST_TYPE_REQUEST);
                    feedModdel.ShowMoreSharesState = 1;
                    feedModdel.IsShareListVisible = true;
                }
                catch (Exception ex) { log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message); }
            }
        }

        private void OnNewsPreviewPanelCommand(object parameter)
        {
            if (parameter is FeedModel && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
            {
                FeedModel feedModel = (FeedModel)parameter;
                if (UCSingleFeedDetailsView.Instance != null)
                    UCGuiRingID.Instance.MotherPanel.Children.Remove(UCSingleFeedDetailsView.Instance);
                UCSingleFeedDetailsView.Instance = new UCSingleFeedDetailsView(UCGuiRingID.Instance.MotherPanel);
                UCSingleFeedDetailsView.Instance.Show();
                UCSingleFeedDetailsView.Instance.ShowDetailsView(feedModel);
                //ucSingleFeedDetailsView.GIFCtrlPanel.Visibility = Visibility.Visible;
                //ucSingleFeedDetailsView.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));

                MainSwitcher.ThreadManager().DetailsOfaFeed.CallBackEvent += (dto) =>
                {
                    if (dto != null) feedModel.LoadData(dto);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (UCSingleFeedDetailsView.Instance != null) UCSingleFeedDetailsView.Instance.LoadFeedModel(feedModel);
                        //ucSingleFeedDetailsView.GIFCtrlPanel.Visibility = Visibility.Collapsed;
                        //if (ucSingleFeedDetailsView.GIFCtrlLoader.IsRunning())
                        //    ucSingleFeedDetailsView.GIFCtrlLoader.StopAnimate();
                    });
                };
                UCSingleFeedDetailsView.Instance.OnRemovedUserControl += () =>
                {
                    UCSingleFeedDetailsView.Instance = null;
                };
                MainSwitcher.ThreadManager().DetailsOfaFeed.StartThread(feedModel.NewsfeedId);
            }
        }

        private void OnLikeCommand(object parameter)
        {
            if (parameter is Grid)
            {
                try
                {
                    Grid tb = (Grid)parameter;
                    FeedModel _FeedModel = ((FeedHolderModel)tb.DataContext).Feed;
                    if (_FeedModel.LikeCommentShare != null)
                    {
                        JObject pakToSend = new JObject();
                        bool ImgToContentID = false;
                        if (_FeedModel.LikeCommentShare.ILike == 0)
                        {
                            ShowLikeAnimation(_FeedModel);
                            _FeedModel.LikeCommentShare.ILike = 1;
                            _FeedModel.LikeCommentShare.NumberOfLikes = _FeedModel.LikeCommentShare.NumberOfLikes + 1;
                        }
                        else
                        {
                            _FeedModel.LikeCommentShare.ILike = 0;
                            if (_FeedModel.ActivityType == AppConstants.NEWSFEED_LIKED)
                            {
                                _FeedModel.Activist = null;
                                _FeedModel.ActivityType = 0;
                            }
                            if (_FeedModel.LikeCommentShare.NumberOfLikes > 0) _FeedModel.LikeCommentShare.NumberOfLikes = _FeedModel.LikeCommentShare.NumberOfLikes - 1;
                        }

                        Guid albumId = Guid.Empty;
                        if (_FeedModel.ImageList != null)
                        {
                            albumId = _FeedModel.ImageList[0].AlbumId;
                        }
                        else if (_FeedModel.MediaContent != null) albumId = _FeedModel.MediaContent.AlbumId;
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_LIKE_STATUS;
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_STATUS;
                        pakToSend[JsonKeys.Liked] = (_FeedModel.LikeCommentShare.ILike == 1) ? 1 : 0;
                        pakToSend[JsonKeys.NewsfeedId] = _FeedModel.NewsfeedId;
                        pakToSend[JsonKeys.Type] = _FeedModel.BookPostType;
                        pakToSend[JsonKeys.WallOwnerType] = _FeedModel.WallOrContentOwner.ProfileType;

                        if (_FeedModel.BookPostType == 2 || _FeedModel.BookPostType == 5 || _FeedModel.BookPostType == 8)
                        {
                            if (_FeedModel.SingleMediaFeedModel != null)
                            {
                                pakToSend[JsonKeys.ContentId] = _FeedModel.SingleMediaFeedModel.ContentId;
                                pakToSend[JsonKeys.MediaType] = _FeedModel.SingleMediaFeedModel.MediaType;
                            }
                            else if (_FeedModel.SingleImageFeedModel != null)
                            {
                                pakToSend[JsonKeys.ContentId] = _FeedModel.SingleImageFeedModel.ImageId;
                                pakToSend[JsonKeys.MediaType] = SettingsConstants.MEDIA_TYPE_IMAGE;
                                ImgToContentID = true;
                            }
                        }
                        new ThreadLikeUnlikeAny().StartThread(pakToSend, ImgToContentID);
                    }
                }
                catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
            }
        }

        private void OnNumberOfLikeCommand(object parameter)
        {
            if (parameter is FeedModel && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
            {
                FeedModel _FeedModel = (FeedModel)parameter;
                if (_FeedModel.LikeCommentShare != null)
                {
                    ViewConstants.NewsFeedID = _FeedModel.NewsfeedId;
                    //LikeListViewWrapper.Show(_FeedModel.NumberOfLikes, _FeedModel.NewsfeedId, AppConstants.TYPE_LIKES_FOR_STATUS);
                    if (LikeListView != null)
                    {
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                            UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(LikeListView);
                        else UCGuiRingID.Instance.MotherPanel.Children.Remove(LikeListView);
                    }
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) LikeListView = new UCLikeList(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                    else LikeListView = new UCLikeList(UCGuiRingID.Instance.MotherPanel);
                    //LikeListView.ContentId = contentID;
                    LikeListView.NewsfeedId = _FeedModel.NewsfeedId;
                    LikeListView.NumberOfLike = _FeedModel.LikeCommentShare.NumberOfLikes;
                    LikeListView.Show();
                    LikeListView.SendLikeListRequest();
                    LikeListView.OnRemovedUserControl += () =>
                    {
                        LikeListView = null;
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                    };
                }
                //if (MediaPlayerInMain != null) MediaPlayerInMain.GrabKeyBoardFocus();
            }
        }

        //private void OnCommentCommand(object parameter)
        //{
        //    if (parameter is Border)
        //   {
        //       try
        //       {
        //           Border cmntPanel = (Border)parameter;
        //           FeedModel _FeedModel = (FeedModel)cmntPanel.DataContext;

        //           if (_FeedModel.IsShareListVisible == true)
        //           {
        //               _FeedModel.IsShareListVisible = false;
        //           }
        //           if (cmntPanel.Visibility == Visibility.Collapsed)
        //           {
        //               //HelperMethods.ShowSingleFeedCommentPanel(_FeedModel, cmntPanel);
        //           }
        //           else
        //               cmntPanel.Visibility = Visibility.Collapsed;

        //       }
        //       catch (Exception ex)
        //       {
        //           log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
        //       }
        //    }
        //}

        private void OnShareTextBlockCommand(object parameter)
        {
            if (parameter is FeedModel && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
            {
                try
                {
                    FeedModel _FeedModel = (FeedModel)parameter;
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    {
                        UCSingleFeedDetailsView.Instance.Hide();
                    }
                    if (_FeedModel.ParentFeed != null)
                    {
                        if (_FeedModel.ParentFeed.PostOwner != null && _FeedModel.ParentFeed.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID
                            || _FeedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                        {
                            UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, ReasonCodeConstants.REASON_MSG_CANT_SHARE_OWN_FEED);
                        }
                        else if (_FeedModel.ParentFeed.LikeCommentShare != null && _FeedModel.ParentFeed.LikeCommentShare.IShare == 1)
                        {
                            UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.ALREADY_SHARED_MESSAGE);
                        }
                        else
                        {
                            ShareSingleFeedView.Show();
                            ShareSingleFeedView.ShowShareView(_FeedModel.ParentFeed);
                        }
                    }
                    else
                    {
                        if (_FeedModel.PostOwner != null && _FeedModel.PostOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID && _FeedModel.SingleMediaFeedModel == null
                            && _FeedModel.LinkModel != null && string.IsNullOrEmpty(_FeedModel.LinkModel.PreviewUrl))
                        {
                            UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, ReasonCodeConstants.REASON_MSG_CANT_SHARE_OWN_FEED);
                        }
                        else if (_FeedModel.LikeCommentShare != null && _FeedModel.LikeCommentShare.IShare == 1 && _FeedModel.SingleMediaFeedModel == null)
                        {
                            UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.ALREADY_SHARED_MESSAGE);
                        }
                        else
                        {
                            ShareSingleFeedView.Show();
                            ShareSingleFeedView.ShowShareView(_FeedModel);
                        }
                    }
                }
                catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
            }
        }

        private void OnShowShareListCommand(object parameter)
        {
            if (parameter is Border)
            {
                try
                {
                    Border cmntPanel = (Border)parameter;
                    FeedModel _FeedModel = ((FeedHolderModel)cmntPanel.DataContext).Feed;
                    //if (_FeedModel.SingleMediaFeedModel != null)
                    //{
                    //    LikeListViewWrapper.ShowSingleMediaShareList(_FeedModel.SingleMediaFeedModel.ShareCount, _FeedModel.NewsfeedId, _FeedModel.SingleMediaFeedModel.ContentId);
                    //}
                    if (_FeedModel.LikeCommentShare != null && _FeedModel.LikeCommentShare.NumberOfShares > 0)
                    {
                        if (_FeedModel.IsShareListVisible == false)
                        {
                            if (_FeedModel.WhoShareList != null && _FeedModel.WhoShareList.Count == _FeedModel.LikeCommentShare.NumberOfShares)
                            {
                                _FeedModel.IsShareListVisible = true;
                                _FeedModel.ShowMoreSharesState = 0;
                            }
                            else
                            {
                                JObject pakToSend = new JObject();
                                pakToSend[JsonKeys.NewsfeedId] = _FeedModel.NewsfeedId;
                                pakToSend[JsonKeys.StartLimit] = 0;
                                (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_WHO_SHARES_LIST, AppConstants.REQUEST_TYPE_REQUEST);
                            }
                        }
                        else _FeedModel.IsShareListVisible = false;
                    }
                    if (cmntPanel.Visibility == Visibility.Visible) cmntPanel.Visibility = Visibility.Collapsed;
                }
                catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
            }
        }

        private void OnStsEditHistoryMouseEnter(object parameter)
        {
            Grid mainPanel = (Grid)parameter;
            if (ucEditHistoryPopupStyle == null)
            {
                ucEditHistoryPopupStyle = new UCEditHistoryPopupStyle();
                mainPanel.Children.Add(ucEditHistoryPopupStyle);
            }
            ucEditHistoryPopupStyle.Show(mainPanel);
        }

        private void OnStsEditHistoryMouseLeave(object parameter)
        {
            ucEditHistoryPopupStyle.PopupEdited.IsOpen = false;
            ucEditHistoryPopupStyle = null;
        }

        private void OnShareDetailsAndNewsPreviewCommand(object parameter)
        {
            try
            {
                if (parameter is FeedModel && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                {
                    FeedModel feedModel = (FeedModel)parameter;
                    if (UCSingleFeedDetailsView.Instance != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(UCSingleFeedDetailsView.Instance);
                    UCSingleFeedDetailsView.Instance = new UCSingleFeedDetailsView(UCGuiRingID.Instance.MotherPanel);
                    UCSingleFeedDetailsView.Instance.Show();
                    UCSingleFeedDetailsView.Instance.ShowDetailsView(feedModel);
                    //ucSingleFeedDetailsView.GIFCtrlPanel.Visibility = Visibility.Visible;
                    // ucSingleFeedDetailsView.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));

                    MainSwitcher.ThreadManager().DetailsOfaFeed.CallBackEvent += (dto) =>
                    {
                        if (dto != null) feedModel.LoadData(dto);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (UCSingleFeedDetailsView.Instance != null)
                            {
                                UCSingleFeedDetailsView.Instance.LoadFeedModel(feedModel);
                                UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Collapsed;
                                if (UCSingleFeedDetailsView.Instance.GIFCtrlLoader.IsRunning())
                                    UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StopAnimate();
                            }
                        });
                    };
                    MainSwitcher.ThreadManager().DetailsOfaFeed.StartThread(feedModel.NewsfeedId);
                    UCSingleFeedDetailsView.Instance.OnRemovedUserControl += () =>
                    {
                        UCSingleFeedDetailsView.Instance = null;
                    };
                }
            }
            catch (Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void ShowOptionsPopup(SingleMediaModel singleMediaModel, UIElement btn, int type)
        {
            if (ucMediaShareMoreOptions != null)
            {
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(ucMediaShareMoreOptions);
                else UCGuiRingID.Instance.MotherPanel.Children.Remove(ucMediaShareMoreOptions);
            }
            if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                ucMediaShareMoreOptions = new UCMediaShareMoreOptions(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
            else ucMediaShareMoreOptions = new UCMediaShareMoreOptions(UCGuiRingID.Instance.MotherPanel);

            ucMediaShareMoreOptions.Show();
            ucMediaShareMoreOptions.ShowOptionsPopup(singleMediaModel, btn, singleMediaModel.MediaType, type);
            ucMediaShareMoreOptions.OnRemovedUserControl += () =>
            {
                singleMediaModel.ShareOptionsPopup = false;
                ucMediaShareMoreOptions = null;
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible && UCDownloadOrAddToAlbumPopUp.Instance == null)
                    UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                //if (ImageViewInMain != null)
                //{
                //    ImageViewInMain.GrabKeyboardFocus();
                //}
            };
        }

        private void OnShareMediaCommand(object parameter)
        {
            if (parameter is Button)
            {
                Button btn = (Button)parameter;
                if (btn.DataContext is SingleMediaModel)
                {
                    SingleMediaModel singleMediaModel = (SingleMediaModel)btn.DataContext;
                    int type = 0;
                    singleMediaModel.ShareOptionsPopup = true;
                    if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.IsVisible
                        && UCMeidaCloudDownloadPopup.Instance != null
                        && UCMeidaCloudDownloadPopup.Instance.IsVisible)
                        type = 2;
                    else if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.IsVisible
                        && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.tabContainerBorder.IsVisible
                        && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel.TabType == 4)
                        type = 3;
                    else if (singleMediaModel.MediaOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID) type = 1;
                    else type = -1;

                    ShowOptionsPopup(singleMediaModel, btn, type);
                }
            }
        }

        private void OnNewsFullStoryClickedCommand(object parameter)
        {
            if (parameter is string)
            {
                string url = (string)parameter;
                try
                {
                    HelperMethods.GoToSite(url);
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        public void OnNewsPortalSliderLoaded(object parameter)
        {
            if (parameter is FeedModel)
            {
                FeedModel model = (FeedModel)parameter;
                imageCount = model.ImageList.Count;
                //FeedModel feedModel = (FeedModel)parameter;
                //if (feedModel.ImageList != null)
                //    imageCount = feedModel.ImageList.Count;
                if (imageCount > 1 && imageCount == model.TotalImage) LoadBanner();
                //else if (imageCount > 1 && IsFirstTime) LoadBanner();
            }
        }

        private void OnNewsPortalVisibleChanged(object parameter)
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Enabled = false;
                timer.Elapsed -= timer_Elapsed;
                timer = null;
                _ImageItemContainer = null;
                //IsFirstTime = false;
            }
        }

        private void OnVirtualizingStackPanelLoaded(object parameter)
        {
            if (parameter is VirtualizingStackPanel) _ImageItemContainer = parameter as VirtualizingStackPanel;
        }

        CustomFileDownloader fileDownloader = null;
        public void MakeDownloaderStart(SingleMediaModel singleMediaModel)
        {
            UploadingModel _uploadingModel = new UploadingModel();
            _uploadingModel.IsDownload = true;
            _uploadingModel.ContentId = singleMediaModel.ContentId;
            _uploadingModel.UploadingContent = singleMediaModel.MediaType == 1 ? "1 audio is downloading..." : "1 video is downloading...";
            RingIDViewModel.Instance.UploadingModelList.Add(_uploadingModel);

            if (System.IO.File.Exists(HelperMethods.GetDownloadedFilePathFromStreamURL(singleMediaModel.StreamUrl, singleMediaModel.ContentId)))
            {
                //this.PlayingMedia.DeleteDownloadMedia(PlayingMedia.ContentId);
                singleMediaModel.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                singleMediaModel.DownloadProgress = 0;
                MediaDAO.Instance.DeleteFromDownloadedMediasTable(singleMediaModel.ContentId);
                //TODO delete downloaded files from PC
            }

            if (!MediaDataContainer.Instance.CurrentDownloads.TryGetValue(singleMediaModel.ContentId, out fileDownloader))
            {
                fileDownloader = new CustomFileDownloader(singleMediaModel);
                MediaDataContainer.Instance.CurrentDownloads[singleMediaModel.ContentId] = fileDownloader;
            }
            if (fileDownloader.Files == null || (this.fileDownloader.Files != null && this.fileDownloader.Files.Count == 0))
                this.fileDownloader.Files.Add(new FileDownloader.FileInfo(ServerAndPortSettings.GetVODServerResourceURL + singleMediaModel.StreamUrl, singleMediaModel.ContentId.ToString()));
            this.fileDownloader.Start();
            //}
        }

        private void OnDownloadBtnCommand(object parameter)
        {
            try
            {
                if (parameter is Button)
                {
                    Button btn = (Button)parameter;
                    SingleMediaModel singleMediaModel = (SingleMediaModel)btn.DataContext;
                    if (singleMediaModel.DownloadState == StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE)
                    {
                        //MakeDownloaderStart(singleMediaModel);
                        if (UCDownloadPopup.Instance != null)
                        {
                            if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                                UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(UCDownloadPopup.Instance);
                            else UCGuiRingID.Instance.MotherPanel.Children.Remove(UCDownloadPopup.Instance);
                        }
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                            UCDownloadPopup.Instance = new UCDownloadPopup(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                        else UCDownloadPopup.Instance = new UCDownloadPopup(UCGuiRingID.Instance.MotherPanel);
                        UCDownloadPopup.Instance.Show();
                        UCDownloadPopup.Instance.ShowPopup(singleMediaModel, btn, () =>
                        {
                            return 0;
                        });
                        UCDownloadPopup.Instance.OnRemovedUserControl += () =>
                        {
                            UCDownloadPopup.Instance = null;
                            if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible && UCDownloadOrAddToAlbumPopUp.Instance == null)
                                UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        };
                    }
                    else if (singleMediaModel.DownloadState == StatusConstants.MEDIA_DOWNLOAD_PAUSE_STATE)
                    {
                        CustomFileDownloader fileDownloader = null;
                        if (!MediaDataContainer.Instance.CurrentDownloads.TryGetValue(singleMediaModel.ContentId, out fileDownloader))
                        {
                            fileDownloader = new CustomFileDownloader(singleMediaModel);
                            MediaDataContainer.Instance.CurrentDownloads[singleMediaModel.ContentId] = fileDownloader;
                        }
                        if (!RingIDViewModel.Instance.UploadingModelList.Any(p => p.ContentId == singleMediaModel.ContentId))
                        {
                            UploadingModel _uploadingModel = new UploadingModel();
                            _uploadingModel.IsDownload = true;
                            _uploadingModel.ContentId = singleMediaModel.ContentId;
                            _uploadingModel.UploadingContent = singleMediaModel.MediaType == 1 ? "1 audio is downloading..." : "1 video is downloading...";
                            RingIDViewModel.Instance.UploadingModelList.Add(_uploadingModel);
                        }
                        if (!fileDownloader.CanResume)
                        {
                            FileDownloader.FileInfo file = fileDownloader.Files[0];
                            if (System.IO.File.Exists(fileDownloader.LocalDirectory + "\\" + file.Name))
                            {
                                long length = new System.IO.FileInfo(fileDownloader.LocalDirectory + "\\" + file.Name).Length;
                                fileDownloader.Start(length);
                            }
                            else
                            {
                                UIHelperMethods.ShowWarning("Paused Media file not found");
                                //singleMediaModel.DeleteDownloadMedia(singleMediaModel.ContentId);
                                singleMediaModel.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
                                singleMediaModel.DownloadProgress = 0;
                                MediaDAO.Instance.DeleteFromDownloadedMediasTable(singleMediaModel.ContentId);
                                //TODO delete downloaded files from PC
                            }
                        }
                        else fileDownloader.Resume();
                    }
                    else if (singleMediaModel.DownloadState == StatusConstants.MEDIA_DOWNLOADING_STATE)
                    {
                        //Open popup for Pause/Cancel options
                        MainSwitcher.PopupController.FeedDownloadPauseCancelPopup.Show(singleMediaModel, btn);
                    }
                }
            }
            catch (System.Exception ex) { log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        private void OnAlbumContentCommand(object parameter)
        {
            if (parameter is FeedModel && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
            {
                FeedModel model = (FeedModel)parameter;
                if (model.MediaContent != null)
                {
                    if (UCAlbumContentView.Instance != null) UCGuiRingID.Instance.MotherPanel.Children.Remove(UCAlbumContentView.Instance);
                    UCAlbumContentView.Instance = new UCAlbumContentView(UCGuiRingID.Instance.MotherPanel);
                    UCAlbumContentView.Instance.Show();
                    MediaContentModel contentModel = new MediaContentModel();
                    contentModel.AlbumId = model.MediaContent.AlbumId;
                    contentModel.AlbumImageUrl = model.MediaContent.AlbumImageUrl;
                    contentModel.AlbumName = model.MediaContent.AlbumName;
                    contentModel.MediaPrivacy = model.MediaContent.MediaPrivacy;
                    contentModel.MediaType = model.MediaContent.MediaType;
                    contentModel.PanelVisibility = model.MediaContent.PanelVisibility;
                    contentModel.UserTableID = model.MediaContent.UserTableID;
                    UCAlbumContentView.Instance.ShowMediaAlbumContent(contentModel);

                    UCAlbumContentView.Instance.OnRemovedUserControl += () =>
                    {
                        UCAlbumContentView.Instance = null;
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        //if (MediaPlayerInMain != null) MediaPlayerInMain.GrabKeyBoardFocus();
                    };
                }
            }
        }

        private void OnActivityNameToolTipOpenCommand(object parameter)
        {
            if (parameter is TextBlock)
            {
                try
                {
                    TextBlock tb = (TextBlock)parameter;

                    if (_ActivityNameToolTip == null)
                    {
                        _ActivityNameToolTip = new DispatcherTimer();
                        _ActivityNameToolTip.Interval = TimeSpan.FromSeconds(1);
                        _ActivityNameToolTip.Tick += (s, es) =>
                        {
                            if (UCNameToolTipPopupView.Instance != null)
                                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCNameToolTipPopupView.Instance);
                            UCNameToolTipPopupView.Instance = new UCNameToolTipPopupView(UCGuiRingID.Instance.MotherPanel);
                            UCNameToolTipPopupView.Instance.Show();
                            if (tb.IsMouseOver)
                            {
                                FeedHolderModel holder = (FeedHolderModel)tb.DataContext;
                                if (holder.FirstSharer != null)
                                    UCNameToolTipPopupView.Instance.ShowPopUp(tb, holder.FirstSharer);
                                else
                                {
                                    FeedModel feedModel = (holder).Feed;
                                    if (feedModel.Activist != null) UCNameToolTipPopupView.Instance.ShowPopUp(tb, feedModel.Activist);
                                }

                            }

                            UCNameToolTipPopupView.Instance.OnRemovedUserControl += () =>
                            {
                                UCNameToolTipPopupView.Instance = null;
                                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                                {
                                    UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                                }
                            };

                            _ActivityNameToolTip.Stop();
                            _ActivityNameToolTip.Interval = TimeSpan.FromSeconds(0.2);
                        };

                    }
                    _ActivityNameToolTip.Stop();
                    _ActivityNameToolTip.Start();
                }
                finally { }
            }
        }

        private void OnActivitySecondNameToolTipOpenCommand(object parameter)
        {
            if (parameter is TextBlock)
            {
                try
                {
                    TextBlock tb = (TextBlock)parameter;

                    if (_ActivityNameToolTip == null)
                    {
                        _ActivityNameToolTip = new DispatcherTimer();
                        _ActivityNameToolTip.Interval = TimeSpan.FromSeconds(1);
                        _ActivityNameToolTip.Tick += (s, es) =>
                        {
                            if (UCNameToolTipPopupView.Instance != null)
                                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCNameToolTipPopupView.Instance);
                            UCNameToolTipPopupView.Instance = new UCNameToolTipPopupView(UCGuiRingID.Instance.MotherPanel);
                            UCNameToolTipPopupView.Instance.Show();
                            if (tb.IsMouseOver)
                            {
                                FeedHolderModel holder = (FeedHolderModel)tb.DataContext;
                                if (holder.FirstSharer != null)
                                    UCNameToolTipPopupView.Instance.ShowPopUp(tb, holder.FirstSharer);
                                else
                                {
                                    FeedModel feedModel = (holder).Feed;
                                    if (feedModel.Activist != null) UCNameToolTipPopupView.Instance.ShowPopUp(tb, feedModel.Activist);
                                }
                            }

                            UCNameToolTipPopupView.Instance.OnRemovedUserControl += () =>
                            {
                                UCNameToolTipPopupView.Instance = null;
                                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible) UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                            };

                            _ActivityNameToolTip.Stop();
                            _ActivityNameToolTip.Interval = TimeSpan.FromSeconds(0.2);
                        };
                    }
                    _ActivityNameToolTip.Stop();
                    _ActivityNameToolTip.Start();
                }
                finally { }
            }
        }

        private void OnNameToolTipHiddenCommand(object parameter)
        {
            if (UCNameToolTipPopupView.Instance != null
                && UCNameToolTipPopupView.Instance.PopupToolTip.IsOpen
                && !UCNameToolTipPopupView.Instance.contentContainer.IsMouseOver)
                UCNameToolTipPopupView.Instance.HidePopUp(false);
        }
        #endregion

        #region "NewsPortal Slider"
        private void StartSliding()
        {
            timer = new System.Timers.Timer();
            timer.Interval = 3000;
            _IsTimerStart = true;
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        private void RunBanner()
        {
            if (imageCount > 1)
            {
                StartSliding();
            }
        }
        private void LoadBanner()
        {
            if (_BannerLoaded == false && !(_BannerThread != null && _BannerThread.IsAlive))
            {
                _BannerThread = new System.Threading.Thread(RunBanner);
                _BannerThread.Start();
            }
        }

        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    if (_IsLast == true)
                    {
                        AnimationOnArrowClicked(target, 0.0);
                    }
                    else
                    {
                        target = CurrentLeftMargin - (int)600;
                        AnimationOnArrowClicked(target, 0.35);
                    }

                    CurrentLeftMargin = target;
                    //SliderListBox_Loaded(null, null);

                    if ((CurrentLeftMargin <= -(((float)imageCount) * 600)) && CurrentLeftMargin < 0)
                    {
                        target = 0;
                        if (timer != null) timer.Interval = 5;
                        _IsLast = true;
                    }
                    else
                    {
                        _IsLast = false;
                        if (timer != null) timer.Interval = 3000;
                    }
                }
                catch (Exception ex)
                {
                    log4net.LogManager.GetLogger(typeof(UCSingleFeed).Name).Error("Error: OnNext() => " + ex.Message + "\n" + ex.StackTrace);
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }
        private void AnimationOnArrowClicked(int target, double delay)
        {
            if (_ImageItemContainer != null)
            {
                ThicknessAnimation MarginAnimation = new ThicknessAnimation();
                MarginAnimation.From = new Thickness(CurrentLeftMargin, 0, 0, 0);
                MarginAnimation.To = new Thickness(target, 0, 0, 0);
                MarginAnimation.Duration = new Duration(TimeSpan.FromSeconds(delay));
                Storyboard storyboard = new Storyboard();
                storyboard.Children.Add(MarginAnimation);
                Storyboard.SetTarget(MarginAnimation, _ImageItemContainer);
                Storyboard.SetTargetProperty(MarginAnimation, new PropertyPath(VirtualizingStackPanel.MarginProperty));
                storyboard.Begin(_ImageItemContainer);
            }
        }
        #endregion
    }
}

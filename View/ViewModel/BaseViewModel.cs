﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Threading;

namespace View.ViewModel
{
    /// <summary>
    /// This is the base view model class. Every view model class should inherit this class.
    /// It implements INotifyPropertyChanged class, which helps to notify UI in case of property changes.
    /// </summary>
    public class BaseViewModel : DispatcherObject, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void SetProperty<T>(ref T member, T value, string propertyName)
        {
            if (object.Equals(member, value))
            {
                return;
            }
            member = value;
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
            //try
            //{
            //if (PropertyChanged != null)
            //{
            //    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            //}
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Base ViewModel, Message :  " + ex.Message + " Stack Trace : " + ex.StackTrace);
            //}
        }
    }
}

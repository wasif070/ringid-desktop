﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using Auth.Parser;
using log4net;
using Microsoft.Win32;
using Models.Constants;
using View.Constants;
using View.Utility.IPC;
namespace View
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>

    public partial class App : Application
    {
        private static Mutex singleton = new Mutex(true, RingIDSettings.APP_NAME);
        private static readonly ILog log = LogManager.GetLogger(typeof(App));

        public static string[] fArgs = null;

        [DllImportAttribute("user32.dll")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImportAttribute("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImportAttribute("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        public static void ShowToFront(string windowName)
        {
            IntPtr firstInstance = FindWindow(null, windowName);
            ShowWindow(firstInstance, 1);
            SetForegroundWindow(firstInstance);
        }
        private void SetWebBrowserVersionandUpdateRegistries()
        {
            try
            {
                int RegVal = 8888, BrowserVer;
                using (System.Windows.Forms.WebBrowser Wb = new System.Windows.Forms.WebBrowser()) BrowserVer = Wb.Version.Major;
                if (BrowserVer >= 11) RegVal = 11001;
                else if (BrowserVer == 10) RegVal = 10001;
                else if (BrowserVer == 9) RegVal = 9999;
                else if (BrowserVer == 8) RegVal = 8888;
                else RegVal = 7000;
                RegistryKey Key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", true);
                if (Key == null) Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree);
                Key.SetValue("wbbrowserReg" + ".exe", RegVal, RegistryValueKind.DWord);
                Key.Close();
                RegistryKey StyleKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Styles", true);
                if (StyleKey == null) StyleKey = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Styles", RegistryKeyPermissionCheck.ReadWriteSubTree);
                StyleKey.SetValue("MaxScriptStatements", unchecked((int)0xffffffffu), RegistryValueKind.DWord);
                StyleKey.Close();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            fArgs = e.Args;
            try
            {
                //RingIDSettings.APP_FOLDER = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), RingIDSettings.APP_NAME);
                SetWebBrowserVersionandUpdateRegistries();
                bool isRestart = e.Args.Length > 0 && e.Args[0].Equals(StartUpConstatns.ARGUMENT_RESTART);
                if (isRestart)
                {
                    //Remote.Start();
                    StartApp(e.Args);
                }
                else
                {
                    bool isAlreadyRunning = false;
                    //bool isAlreadyRunning = !singleton.WaitOne(TimeSpan.Zero, true);
                    if (isAlreadyRunning)
                    {
                        if (e.Args.Length == 0 || String.IsNullOrWhiteSpace(e.Args[0]) || e.Args[0].Equals(StartUpConstatns.ARGUMENT_STARTUP))
                        {
                            ShowToFront(RingIDSettings.APP_NAME);
                            System.Diagnostics.Process.GetCurrentProcess().Kill();
                        }
                        else
                        {
                            Remote.Send(e.Args[0]);
                            var proc = Process.GetCurrentProcess();
                            proc.Kill();
                        }
                    }
                    else
                    {
                        if (e.Args.Length > 0 && e.Args[0].Equals(StartUpConstatns.ARGUMENT_STARTUP))
                        {
                            string path = System.IO.Path.Combine(RingIDSettings.APP_FOLDER, "AutoStartInfo.bat");
                            if (File.Exists(path))
                            {
                                using (StreamReader sr = new StreamReader(path))
                                {
                                    while (sr.Peek() >= 0)
                                    {
                                        string str = sr.ReadLine();
                                        int num;
                                        if (int.TryParse(str, out num)) DefaultSettings.VALUE_LOGIN_AUTO_START = num;
                                        else DefaultSettings.VALUE_LOGIN_AUTO_START = 1; //default value if can't parse
                                    }
                                }
                            }
                            if (DefaultSettings.VALUE_LOGIN_AUTO_START != 1) System.Diagnostics.Process.GetCurrentProcess().Kill();
                        }
                        //Remote.Start();
                        StartApp(e.Args);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            log.Info("Exiting forcefully from App.cs");
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private void StartApp(string[] args)
        {
            View.UI.WNRingIDMain.WinRingIDMain = new View.UI.WNRingIDMain();
            View.UI.WNRingIDMain.WinRingIDMain.ShowWindow(args);
        }

        public App()
        {
            this.DispatcherUnhandledException += App_DispatcherUnhandledException;
        }

        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(e.Exception.Message + e.Exception.StackTrace);
            AuthAsyncCommunication.Instance.SendLog(bytes);
        }
    }
}


﻿using View.UI.Profile.FriendProfile;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Models.Entity;
using View.UI.Group;
using View.UI.Circle;
using View.UI.Chat;
using View.UI.RingMarket;
using View.UI.Room;

namespace View.Dictonary
{
    class UIDictionaries
    {

        private static UIDictionaries _Instance;

        public static UIDictionaries Instance
        {
            get { return _Instance = _Instance ?? new UIDictionaries(); }
            set { UIDictionaries._Instance = value; }
        }


        #region Variable
        //private ConcurrentDictionary<long, UCFriendProfile> _FRIEND_PROFILE_DICTIONARY = new ConcurrentDictionary<long, UCFriendProfile>();
        private ConcurrentDictionary<long, UCFriendChatCallPanel> _FRIEND_CHAT_CALL_DICTIONARY = new ConcurrentDictionary<long, UCFriendChatCallPanel>();
        private ConcurrentDictionary<long, UCFriendChatSettings> _FRIEND_CHAT_SETTINGS_DICTIONARY = new ConcurrentDictionary<long, UCFriendChatSettings>();
        private ConcurrentDictionary<long, UCGroupChatCallPanel> _GROUP_CHAT_CALL_DICTIONARY = new ConcurrentDictionary<long, UCGroupChatCallPanel>();
        private ConcurrentDictionary<long, UCGroupChatSettings> _GROUP_CHAT_SETTINGS_DICTIONARY = new ConcurrentDictionary<long, UCGroupChatSettings>();
        private ConcurrentDictionary<string, UCRoomChatCallPanel> _ROOM_CHAT_CALL_DICTIONARY = new ConcurrentDictionary<string, UCRoomChatCallPanel>();
        private ConcurrentDictionary<string, UCRoomChatSettings> _ROOM_CHAT_SETTINGS_DICTIONARY = new ConcurrentDictionary<string, UCRoomChatSettings>();
        private ConcurrentDictionary<long, UCCirclePanel> _CIRCLE_PANEL_DICTIONARY = new ConcurrentDictionary<long, UCCirclePanel>();
        private ConcurrentDictionary<long, KeyValuePair<string, BitmapImage>> _CHAT_PROFILE_IMAGE_DICTIONARY = new ConcurrentDictionary<long, KeyValuePair<string, BitmapImage>>();
        private ConcurrentDictionary<int, UCRingChatStickerCategoryPanel> _RING_MARKET_STICKER_DICTIONARY = new ConcurrentDictionary<int, UCRingChatStickerCategoryPanel>();
        #endregion Variable

        #region Property
        //public ConcurrentDictionary<long, UCFriendProfile> FRIEND_PROFILE_DICTIONARY { get { return _FRIEND_PROFILE_DICTIONARY; } }
        public ConcurrentDictionary<long, UCFriendChatCallPanel> FRIEND_CHAT_CALL_DICTIONARY { get { return _FRIEND_CHAT_CALL_DICTIONARY; } }
        public ConcurrentDictionary<long, UCFriendChatSettings> FRIEND_CHAT_SETTINGS_DICTIONARY { get { return _FRIEND_CHAT_SETTINGS_DICTIONARY; } }
        public ConcurrentDictionary<long, UCGroupChatCallPanel> GROUP_CHAT_CALL_DICTIONARY { get { return _GROUP_CHAT_CALL_DICTIONARY; } }
        public ConcurrentDictionary<long, UCGroupChatSettings> GROUP_CHAT_SETTINGS_DICTIONARY { get { return _GROUP_CHAT_SETTINGS_DICTIONARY; } }
        public ConcurrentDictionary<string, UCRoomChatCallPanel> ROOM_CHAT_CALL_DICTIONARY { get { return _ROOM_CHAT_CALL_DICTIONARY; } }
        public ConcurrentDictionary<string, UCRoomChatSettings> ROOM_CHAT_SETTINGS_DICTIONARY { get { return _ROOM_CHAT_SETTINGS_DICTIONARY; } }
        public ConcurrentDictionary<long, UCCirclePanel> CIRCLE_PANEL_DICTIONARY { get { return _CIRCLE_PANEL_DICTIONARY; } }
        public ConcurrentDictionary<long, KeyValuePair<string, BitmapImage>> CHAT_PROFILE_IMAGE_DICTIONARY { get { return _CHAT_PROFILE_IMAGE_DICTIONARY; } }
        public ConcurrentDictionary<int, UCRingChatStickerCategoryPanel> RING_MARKET_STICKER_DICTIONARY { get { return _RING_MARKET_STICKER_DICTIONARY; } }
        #endregion Property

        #region Utility Method

        public void ClearUIDictionaries()
        {
            //FRIEND_PROFILE_DICTIONARY.Clear();
            FRIEND_CHAT_CALL_DICTIONARY.Clear();
            FRIEND_CHAT_SETTINGS_DICTIONARY.Clear();
            GROUP_CHAT_CALL_DICTIONARY.Clear();
            GROUP_CHAT_SETTINGS_DICTIONARY.Clear();
            ROOM_CHAT_CALL_DICTIONARY.Clear();
            ROOM_CHAT_SETTINGS_DICTIONARY.Clear();
            CIRCLE_PANEL_DICTIONARY.Clear();
            RING_MARKET_STICKER_DICTIONARY.Clear();

            ICollection<long> keys = CHAT_PROFILE_IMAGE_DICTIONARY.Keys;
            foreach (long utID in keys)
            {
                KeyValuePair<string, BitmapImage> pair = new KeyValuePair<string,BitmapImage>();
                if (CHAT_PROFILE_IMAGE_DICTIONARY.TryRemove(utID, out pair))
                {
                    BitmapImage image = pair.Value;
                    image = null;
                }
            }
        }

        #endregion Utility Method
    }
}

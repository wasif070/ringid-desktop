﻿using View.Utility.Images;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using View.Utility.Chat;

namespace Models.Stores
{
    public class ImageDictionaries
    {
        private static ImageDictionaries _Instance = new ImageDictionaries();
        public static ImageDictionaries Instance
        {
            get { return _Instance; }
            set { _Instance = value; }
        }

        public void ClearImageDictionaries()
        {
            _ALLREADY_TRIED_TO_DOWNLOAD_IMAGE.Clear();
            _IMAGES_IN_DOWNLOADING_STATE.Clear();
            _RING_IMAGE_QUEUE = new RingImageQueue();
            TMP_PREVIEW_IMAGES.Clear();
        }
        private HashSet<String> _ALLREADY_TRIED_TO_DOWNLOAD_IMAGE = new HashSet<string>();
        public HashSet<String> ALLREADY_TRIED_TO_DOWNLOAD_IMAGE
        {
            get { return _ALLREADY_TRIED_TO_DOWNLOAD_IMAGE; }
        }

        private HashSet<String> _IMAGES_IN_DOWNLOADING_STATE = new HashSet<string>();
        public HashSet<String> IMAGES_IN_DOWNLOADING_STATE
        {
            get { return _IMAGES_IN_DOWNLOADING_STATE; }
        }

        private RingImageQueue _RING_IMAGE_QUEUE = new RingImageQueue();
        public RingImageQueue RING_IMAGE_QUEUE
        {
            get { return _RING_IMAGE_QUEUE; }
        }

        private Dictionary<String, BitmapImage> _TMP_PREVIEW_IMAGES = new Dictionary<string, BitmapImage>();
        public Dictionary<String, BitmapImage> TMP_PREVIEW_IMAGES
        {
            get { return _TMP_PREVIEW_IMAGES; }
        }

<<<<<<< HEAD
=======
        private Dictionary<Guid, BitmapImage> _TMP_BOOK_IMAGES = new Dictionary<Guid, BitmapImage>();
        public Dictionary<Guid, BitmapImage> TMP_BOOK_IMAGES
        {
            get { return _TMP_BOOK_IMAGES; }
        }

>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        private ChatDownloadProcessorQueue _CHAT_DOWNLOAD_PROCESSOR_QUEUE = new ChatDownloadProcessorQueue();
        public ChatDownloadProcessorQueue CHAT_DOWNLOAD_PROCESSOR_QUEUE
        {
            get { return _CHAT_DOWNLOAD_PROCESSOR_QUEUE; }
        }

        private ChatFileTransferProcessorQueue _FILE_DOWNLOAD_PROCESSOR_QUEUE = new ChatFileTransferProcessorQueue();
        public ChatFileTransferProcessorQueue FILE_DOWNLOAD_PROCESSOR_QUEUE
        {
            get { return _FILE_DOWNLOAD_PROCESSOR_QUEUE; }
        }

        private ChatFileTransferProcessorQueue _FILE_UPLOAD_PROCESSOR_QUEUE = new ChatFileTransferProcessorQueue();
        public ChatFileTransferProcessorQueue FILE_UPLOAD_PROCESSOR_QUEUE
        {
            get { return _FILE_UPLOAD_PROCESSOR_QUEUE; }
        }
    }
}

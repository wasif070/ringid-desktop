﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace View.Utility
{
    public class PressAndReleaseButton : Button
    {
        public static readonly DependencyProperty PressCommandProperty = DependencyProperty.Register("PressCommand", typeof(ICommand), typeof(PressAndReleaseButton), new PropertyMetadata(null));

        public ICommand PressCommand
        {
            get { return (ICommand)GetValue(PressCommandProperty); }
            set { SetValue(PressCommandProperty, value); }
        }

        public static readonly DependencyProperty ReleaseCommandProperty = DependencyProperty.Register("ReleaseCommand", typeof(ICommand), typeof(PressAndReleaseButton), new PropertyMetadata(null));

        public ICommand ReleaseCommand
        {
            get { return (ICommand)GetValue(ReleaseCommandProperty); }
            set { SetValue(ReleaseCommandProperty, value); }
        }

        public PressAndReleaseButton()
        {
            PreviewMouseDown += (o, a) => { if (PressCommand.CanExecute(null)) PressCommand.Execute(null); };
            PreviewMouseUp += (o, a) => { if (ReleaseCommand.CanExecute(null)) ReleaseCommand.Execute(null); };
        }
    }

}

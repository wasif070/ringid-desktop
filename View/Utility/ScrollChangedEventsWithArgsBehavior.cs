﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace View.Utility
{
    class ScrollChangedEventsWithArgsBehavior : Behavior<ScrollViewer>
    {
        public ICommand ScrollChangedCommand
        {
            get { return (ICommand)GetValue(ScrollChangedCommandProperty); }
            set { SetValue(ScrollChangedCommandProperty, value); }
        }
        public static readonly DependencyProperty ScrollChangedCommandProperty = DependencyProperty.Register("ScrollChangedCommand", typeof(ICommand), typeof(ScrollChangedEventsWithArgsBehavior), new UIPropertyMetadata(null));

        protected override void OnAttached()
        {
            AssociatedObject.ScrollChanged += new ScrollChangedEventHandler(AssociatedObjectScrollChanged);
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.ScrollChanged -= new ScrollChangedEventHandler(AssociatedObjectScrollChanged);
            base.OnDetaching();
        }

        private void AssociatedObjectScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (ScrollChangedCommand != null)
                ScrollChangedCommand.Execute(e);
        }
    }
}

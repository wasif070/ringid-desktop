﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using View.Constants;
using View.Utility.GIF;

namespace View.Utility.WPFMessageBox
{
    /// <summary>
    /// Interaction logic for MessegeBoxWithTimer.xaml
    /// </summary>
    public partial class MessegeBoxWithLoader : Window,INotifyPropertyChanged
    {
        private BitmapImage _LoadingIcon = null;

        public MessegeBoxWithLoader()
        {
            InitializeComponent();
            this.DataContext = this;
            _LoadingIcon = ImageUtility.GetBitmapImage(ImageLocation.MORE_LOADER);
            imgControl.Source = _LoadingIcon;
            ImageBehavior.SetAnimatedSource(imgControl, _LoadingIcon);
        }

        public void ShowMessage()
        {
            Owner = Application.Current.MainWindow;
            double t = Top;
            base.Show();
        }

        public void CloseMessage()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                base.Close();
                imgControl.Source = null;
                ImageBehavior.SetAnimatedSource(imgControl, null);
                if (_LoadingIcon != null)
                {
                    GC.SuppressFinalize(_LoadingIcon);
                }
                _LoadingIcon = null;
            });
        }

        public void ShowCompleMessage(string message)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                imgControl.Source = null;
                imgControl.Visibility = Visibility.Collapsed;
                ImageBehavior.SetAnimatedSource(imgControl, null);
                Message = message;
                if (_LoadingIcon != null)
                {
                    GC.SuppressFinalize(_LoadingIcon);
                }
                _LoadingIcon = null;
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler temp = PropertyChanged;
            if (temp != null)
            {
                temp(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private string _Message = string.Empty;
        public string Message 
        {
            get { return _Message; }
            set 
            {
                _Message = value;
                OnPropertyChanged("Message");
            }
        }

        private void DoubleAnimationCompleted(object sender, EventArgs e)
        {
            //lblMessage.Visibility = Visibility.Visible;
        }
    }
}

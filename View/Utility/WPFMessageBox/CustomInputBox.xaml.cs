﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Animation;
using View.Converter;

namespace View.Utility.WPFMessageBox
{
    /// <summary>
    /// Interaction logic for MessageBox.xaml
    /// </summary>
    public partial class CustomInputBox : INotifyPropertyChanged
    {
        private bool _animationRan;

        private CustomInputBox(Window owner, string message, string hints, MessageBoxButton button, MessageBoxImage icon,
                          MessageBoxResult defaultResult, MessageBoxOptions options, string[] customButtonText)
        {
            _animationRan = false;

            InitializeComponent();

            Owner = owner ?? Application.Current.MainWindow;

            CreateButtons(button, defaultResult, customButtonText);

            CreateImage(icon);
            if (!String.IsNullOrWhiteSpace(message))
            {
                MessageText.Visibility = Visibility.Visible;
                MessageText.Text = message;
            }
            lblHints.Text = hints.Trim();

            ApplyOptions(options);
        }

        public MessageBoxResult MessageBoxResult { get; set; }
        public String InputText { get; set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Create Buttons


        private void CreateButtons(MessageBoxButton button, MessageBoxResult defaultResult, string[] customButtonText)
        {
            switch (button)
            {
                case MessageBoxButton.OKCancel:
                    ButtonsPanel.Children.Add(CreateYesButton(defaultResult, customButtonText != null && customButtonText.Length >= 1 ? customButtonText[0] : "Ok"));
                    ButtonsPanel.Children.Add(CreateCancelButton(defaultResult, customButtonText != null && customButtonText.Length >= 2 ? customButtonText[1] : "Cancel"));
                    break;
                case MessageBoxButton.YesNoCancel:
                    ButtonsPanel.Children.Add(CreateYesButton(defaultResult, customButtonText != null && customButtonText.Length >= 1 ? customButtonText[0] : "Yes"));
                    ButtonsPanel.Children.Add(CreateNoButton(defaultResult, customButtonText != null && customButtonText.Length >= 2 ? customButtonText[1] : "No"));
                    ButtonsPanel.Children.Add(CreateCancelButton(defaultResult, customButtonText != null && customButtonText.Length >= 3 ? customButtonText[2] : "Cancel"));
                    break;
                default:
                    throw new ArgumentOutOfRangeException("button");
            }
        }

        private Button CreateCancelButton(MessageBoxResult defaultResult, string buttonText)
        {
            var cancelButton = new Button
            {
                Name = "cancelButton",
                Content = buttonText,
                IsDefault = defaultResult == MessageBoxResult.Cancel,
                IsCancel = true,
                Tag = MessageBoxResult.Cancel,
            };
            cancelButton.Style = this.Resources["btnStyle"] as Style;
            cancelButton.Click += ButtonClick;
            
            return cancelButton;
        }

        private Button CreateYesButton(MessageBoxResult defaultResult, string buttonText)
        {
            var yesButton = new Button
            {
                Name = "yesButton",
                Content = buttonText,
                IsDefault = defaultResult == MessageBoxResult.Yes,
                Tag = MessageBoxResult.Yes,
            };
            yesButton.Style = this.Resources["btnStyle"] as Style;
            yesButton.Click += ButtonClick;
            Binding sourceBinding = new Binding
            {
                Path = new PropertyPath("Text"),
                ElementName = "InputTermTextBox",
                Converter = new IsNotNullOrWhiteSpaceConverter(),
                Mode = BindingMode.OneWay
            };
            yesButton.Style = this.Resources["btnStyle"] as Style;
            yesButton.SetBinding(System.Windows.Controls.Button.IsEnabledProperty, sourceBinding);

            return yesButton;
        }

        private Button CreateNoButton(MessageBoxResult defaultResult, string buttonText)
        {
            var noButton = new Button
            {
                Name = "noButton",
                Content = buttonText,
                IsDefault = defaultResult == MessageBoxResult.No,
                Tag = MessageBoxResult.No,
            };
            noButton.Style = this.Resources["btnStyle"] as Style;
            noButton.Click += ButtonClick;


            return noButton;
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            InputText = InputTermTextBox.Text;
            MessageBoxResult = (MessageBoxResult)(sender as Button).Tag;
            Close();
        }

        #endregion

        private void ApplyOptions(MessageBoxOptions options)
        {
            if ((options & MessageBoxOptions.RightAlign) == MessageBoxOptions.RightAlign)
            {
                MessageText.TextAlignment = TextAlignment.Right;
            }
            if ((options & MessageBoxOptions.RtlReading) == MessageBoxOptions.RtlReading)
            {
                FlowDirection = FlowDirection.RightToLeft;
            }
        }

        private void CreateImage(MessageBoxImage icon)
        {
            switch (icon)
            {
                case MessageBoxImage.None:
                    ImagePlaceholder.Visibility = Visibility.Collapsed;
                    break;
                case MessageBoxImage.Information:
                    ImagePlaceholder.Source = SystemIcons.Information.ToImageSource();
                    break;
                case MessageBoxImage.Question:
                    ImagePlaceholder.Source = SystemIcons.Question.ToImageSource();
                    break;
                case MessageBoxImage.Warning:
                    ImagePlaceholder.Source = SystemIcons.Warning.ToImageSource();
                    break;
                case MessageBoxImage.Error:
                    ImagePlaceholder.Source = SystemIcons.Error.ToImageSource();
                    break;
                default:
                    throw new ArgumentOutOfRangeException("icon");
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler temp = PropertyChanged;
            if (temp != null)
            {
                temp(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // DragMove();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SizeToContent = SizeToContent.Height;

            var animation = TryFindResource("LoadAnimation") as Storyboard;

            animation.Begin(this);
        }

        private void MessageBoxWindow_Closing(object sender, CancelEventArgs e)
        {
            if (!_animationRan)
            {
                e.Cancel = true;

                var animation = TryFindResource("UnloadAnimation") as Storyboard;

                animation.Completed += AnimationCompleted;

                animation.Begin(this);
            }
        }

        private void AnimationCompleted(object sender, EventArgs e)
        {
            _animationRan = true;

            Close();
        }

        #region Show Information

        public static CustomInputBox ShowInputBox(
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult,
            MessageBoxOptions options,
            string[] customButtonText = null)
        {
            return Show(null, "", "Enter Text", button, icon, defaultResult, options, customButtonText);
        }

        public static CustomInputBox ShowInputBox(
            string hints,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult,
            MessageBoxOptions options,
            string[] customButtonText = null)
        {
            return Show(null, "", hints, button, icon, defaultResult, options, customButtonText);
        }

        public static CustomInputBox ShowInputBox(
            string message,
            string hints,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult,
            MessageBoxOptions options,
            string[] customButtonText = null)
        {
            return Show(null, message, hints, button, icon, defaultResult, options, customButtonText);
        }

        public static CustomInputBox ShowInputBox(
            Window owner,
            string message, 
            string hints, 
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult,
            MessageBoxOptions options,
            string[] customButtonText = null)
        {
            return Show(owner, message, hints, button, icon, defaultResult, options, customButtonText);
        }

        private static CustomInputBox Show(
            Window owner, 
            string message, 
            string hints = "Enter Text",
            MessageBoxButton button = MessageBoxButton.OK,
            MessageBoxImage icon = MessageBoxImage.None,
            MessageBoxResult defaultResult = MessageBoxResult.None,
            MessageBoxOptions options = MessageBoxOptions.None,
           string[] customButtonText = null)
        {
            var result = Application.Current.Dispatcher.Invoke(new Func<CustomInputBox>(() =>
            {
                var messageBox = new CustomInputBox(owner, message, hints, button, icon, defaultResult, options, customButtonText);

                messageBox.ShowDialog();

                return messageBox;
            }));

            return (CustomInputBox)result;
        }

        #endregion
    }
}
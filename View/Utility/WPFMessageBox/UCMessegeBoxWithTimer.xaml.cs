﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using View.UI.PopUp;

namespace View.Utility.WPFMessageBox
{
    /// <summary>
    /// Interaction logic for UCMessegeBoxWithTimer.xaml
    /// </summary>
    public partial class UCMessegeBoxWithTimer : PopUpBaseControl, INotifyPropertyChanged
    {
         #region "Constructors"
        public UCMessegeBoxWithTimer(Grid motherGrid)
        {
            InitializeComponent();
            InitPopUpBaseControl(null, null, 0);//background Color Transparent Black
            SetParent(motherGrid);
            this.DataContext = this;
        }

        #endregion "Constructor"

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler temp = PropertyChanged;
            if (temp != null)
            {
                temp(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private string _Message = string.Empty;
        public string Message
        {
            get { return _Message; }
            set
            {
                _Message = value;
                OnPropertyChanged("Message");
            }
        }
    }
}

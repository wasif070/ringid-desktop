﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace View.Utility
{
    public class DragOverEventWithArgsBehavior : Behavior<UIElement>
    {
        public ICommand PreviewDragOverEventCommand
        {
            get { return (ICommand)GetValue(PreviewDragOverEventCommandProperty); }
            set { SetValue(PreviewDragOverEventCommandProperty, value); }
        }

        public static readonly DependencyProperty PreviewDragOverEventCommandProperty = DependencyProperty.Register("PreviewDragOverEventCommand", typeof(ICommand), typeof(DragOverEventWithArgsBehavior), new UIPropertyMetadata(null));

        protected override void OnAttached()
        {
            AssociatedObject.PreviewDragOver += new DragEventHandler(AssociatedObjectDragEvents);
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewDragOver -= new DragEventHandler(AssociatedObjectDragEvents);
            base.OnDetaching();
        }

        private void AssociatedObjectDragEvents(object sender, DragEventArgs e)
        {
            if (PreviewDragOverEventCommand != null)
            {
                PreviewDragOverEventCommand.Execute(e);
            }
        }
    }

    public class DropEventWithArgsBehavior : Behavior<UIElement>
    {
        public ICommand PreviewDropEventCommand
        {
            get { return (ICommand)GetValue(PreviewDropEventCommandProperty); }
            set { SetValue(PreviewDropEventCommandProperty, value); }
        }

        public static readonly DependencyProperty PreviewDropEventCommandProperty = DependencyProperty.Register("PreviewDropEventCommand", typeof(ICommand), typeof(DropEventWithArgsBehavior), new UIPropertyMetadata(null));

        protected override void OnAttached()
        {
            AssociatedObject.PreviewDrop += new DragEventHandler(AssociatedObjectDragEvents);
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewDrop -= new DragEventHandler(AssociatedObjectDragEvents);
            base.OnDetaching();
        }

        private void AssociatedObjectDragEvents(object sender, DragEventArgs e)
        {
            if (PreviewDropEventCommand != null)
            {
                PreviewDropEventCommand.Execute(e);
            }
        }
    }

    public class DragLeaveEventWithArgsBehavior : Behavior<UIElement>
    {
        public ICommand DragLeaveEventCommand
        {
            get
            {
                return (ICommand)GetValue(DragLeaveEventCommandCommandProperty);
            }
            set
            {
                SetValue(DragLeaveEventCommandCommandProperty, value);
            }
        }

        public static readonly DependencyProperty DragLeaveEventCommandCommandProperty = DependencyProperty.Register("DragLeaveEventCommand", typeof(ICommand), typeof(DragLeaveEventWithArgsBehavior), new UIPropertyMetadata(null));

        protected override void OnAttached()
        {
            AssociatedObject.DragLeave += new DragEventHandler(AssociatedObjectDragEvents);
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.DragLeave -= new DragEventHandler(AssociatedObjectDragEvents);
            base.OnDetaching();
        }

        private void AssociatedObjectDragEvents(object sender, DragEventArgs e)
        {
            if (DragLeaveEventCommand != null)
            {
                DragLeaveEventCommand.Execute(e);
            }
        }
    }

    //public class PreviewDragLeaveEventWithArgsBehavior : Behavior<UIElement>
    //{
    //    public ICommand DragLeaveEventCommand
    //    {
    //        get { return (ICommand)GetValue(DragLeaveEventCommandProperty); }
    //        set { SetValue(DragLeaveEventCommandProperty, value); }
    //    }

    //    public static readonly DependencyProperty DragLeaveEventCommandProperty = DependencyProperty.Register("DragLeaveEventCommand", typeof(ICommand), typeof(PreviewDragLeaveEventWithArgsBehavior), new UIPropertyMetadata(null));

    //    protected override void OnAttached()
    //    {
    //        AssociatedObject.DragLeave += new DragEventHandler(AssociatedObjectDragEvents);
    //        base.OnAttached();
    //    }

    //    protected override void OnDetaching()
    //    {
    //        AssociatedObject.DragLeave -= new DragEventHandler(AssociatedObjectDragEvents);
    //        base.OnDetaching();
    //    }

    //    private void AssociatedObjectDragEvents(object sender, DragEventArgs e)
    //    {
    //        if (DragLeaveEventCommand != null)
    //        {
    //            DragLeaveEventCommand.Execute(e);
    //        }
    //    }
    //}



}

﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.UI.FriendList;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;

namespace View.Utility.Suggestion
{
    class AddFriendFromPeopleYouMayKnow
    {
        UserBasicInfoModel userbasicInfo;
        private static Thread addFriendThread = null;
        public void StartProcess(UserBasicInfoModel userbasicInfo1)
        {
            if (addFriendThread == null || !addFriendThread.IsAlive)
            {
                userbasicInfo = userbasicInfo1;
                addFriendThread = new Thread(run);
                addFriendThread.Name = "AddFriendFromPeopleYouMayKnow";
                addFriendThread.Start();
            }
        }
        public void run()
        {
            try
            {
                bool isSuccess = false;
                string msg = "";
                long contactAddedTime = 0;
                UserBasicInfoDTO _dto = null;
                AddFriendRequest addasFriend = new AddFriendRequest(userbasicInfo.ShortInfoModel.UserTableID);
                addasFriend.Run(out isSuccess, out _dto, out msg, out contactAddedTime);
                if (isSuccess)
                {
                    _dto.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_PENDING;
                    FriendListLoadUtility.AddInfoInModel(userbasicInfo, _dto);
                    FriendDictionaries.Instance.AddOrUPdateInBasicInfoDictionaryWithLock(_dto);

                    List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
                    _dto.UpdateTime = contactAddedTime;
                    userBasicInfoList.Add(_dto);
                    new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();
                    //FriendListLoadUtility.InsertSingleFriendToUILists(_dto);

                    //AddRemoveInCollections.RemoveFromSuggestionFriendList(userbasicInfo);
                    FriendListLoadUtility.RemoveAModelFromUI(_dto, userbasicInfo, true);
                    userbasicInfo.FriendListType = AddRemoveInCollections.Dictonary_OutgoingRequestFriendList;
                    FriendListLoadUtility.AddAModelIntoUI(_dto, userbasicInfo, userbasicInfo.FriendListType);
                }
                else if (!string.IsNullOrWhiteSpace(msg))
                {
                    Application.Current.Dispatcher.Invoke(delegate { UIHelperMethods.ShowFailed(NotificationMessages.MSG_ADDFRIEND_FAIL + msg); });
                }
                Application.Current.Dispatcher.Invoke(delegate
                {
                    userbasicInfo.VisibilityModel.ShowActionButton = true;
                    userbasicInfo.VisibilityModel.ShowLoading = Visibility.Collapsed;
                });
                if (UCFriendList.Instance != null)
                {
                    UCFriendList.Instance.ShowNextFriendAfterAddOrRemove();
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (addFriendThread != null) addFriendThread = null;
            }
        }
    }

}
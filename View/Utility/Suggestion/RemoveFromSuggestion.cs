﻿using Auth.Service;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using View.BindingModels;
using View.UI.AddFriend;
using View.UI.FriendList;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Auth;

namespace View.Utility.Suggestion
{
    public class RemoveFromSuggestion
    {
        private readonly ILog log = LogManager.GetLogger(typeof(RemoveFromSuggestion).Name);
        private long userTableId;
        private UserBasicInfoModel userbasicInfoModel;
        private bool isRemoveFromFriendList = false;
        public RemoveFromSuggestion(UserBasicInfoModel model, bool isRemoveFromFriendList)
        {
            this.userbasicInfoModel = model;
            this.userTableId = model.ShortInfoModel.UserTableID;
            this.isRemoveFromFriendList = isRemoveFromFriendList;
            Thread th = new Thread(new ThreadStart(Run));
            th.Start();
        }
        public void Run()
        {
            try
            {
                UserBasicInfoDTO _dto = null;
                if (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.TryGetValue(userTableId, out _dto))
                {
                    bool isSuccess = false;
                    string msg = "";
                    JObject pakToSend = new JObject();
                    pakToSend[JsonKeys.UserTableID] = userTableId;
                    (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_REMOVE_SUGGESTION, AppConstants.REQUEST_TYPE_REQUEST)).Run(out isSuccess, out msg);
                    if (isSuccess)
                    {
                        FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(userTableId);
                        FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userTableId);
                        AddRemoveInCollections.RemoveFromSuggestionFriendList(userbasicInfoModel);
                        userbasicInfoModel.ShortInfoModel.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_UNKNOWN;
                        userbasicInfoModel.FriendListType = AddRemoveInCollections.Dictonary_UnknownFriendList;
                    }
                    else if (msg != null)
                    {
                        Application.Current.Dispatcher.Invoke(delegate
                        {
                            if (!String.IsNullOrWhiteSpace(msg)) UIHelperMethods.ShowFailed(NotificationMessages.MSG_REMOVE_FROM_SUGGESTION + msg);
                        });
                    }
                    ShowLoadingOff();
                    if (isRemoveFromFriendList)
                    {
                        if (UCFriendList.Instance != null) UCFriendList.Instance.ShowNextFriendAfterAddOrRemove();
                    }
                    else
                    {
                        if (UCSuggestions.Instance != null) UCSuggestions.Instance.ShowMorePanelHideShow();
                    }
                }
                else
                {
                    ShowLoadingOff();
                    log.Info("Failed to remove from Suggestion...");
                }
            }
            catch (Exception ex)
            {
                log.Error("RemoveSuggestion ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }
        private void ShowLoadingOff()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                userbasicInfoModel.VisibilityModel.ShowActionButton = true;
                userbasicInfoModel.VisibilityModel.ShowLoading = Visibility.Collapsed;
            });
        }
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using View.Constants;
using System.Linq;

namespace View.Utility.Doing
{
    public class DoingImageLoadUtility
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(DoingImageLoadUtility).Name);

        private static readonly object _syncRoot = new object();
        public static DoingImageLoadUtility Instance = null;
        private ConcurrentQueue<DoingDTO> ImageUploaderModelsQueue = new ConcurrentQueue<DoingDTO>();
        private BackgroundWorker worker;
        public DoingImageLoadUtility()
        {
            if (worker == null)
            {
                worker = new BackgroundWorker();
                worker.WorkerSupportsCancellation = true;
                worker.WorkerReportsProgress = true;
                worker.DoWork += DoingImageLoad_DowWork;
                worker.ProgressChanged += DoingImageLoad_ProgressChanged;
                worker.RunWorkerCompleted += DoingImageLoad_RunWorkerCompleted;
            }
        }


        public void LoadSingleImageUploaderModel(DoingDTO model)
        {
            try
            {
                lock (_syncRoot)
                {
                    if (DoingImageLoadUtility.Instance == null)
                    {
                        DoingImageLoadUtility.Instance = new DoingImageLoadUtility();
                        DoingImageLoadUtility.Instance.ImageUploaderModelsQueue.Enqueue(model);
                        DoingImageLoadUtility.Instance.Start();
                    }
                    else
                    {
                        DoingImageLoadUtility.Instance.ImageUploaderModelsQueue.Enqueue(model);
                        if (DoingImageLoadUtility.Instance.worker.IsBusy == false)
                        {
                            DoingImageLoadUtility.Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadSingleImageUploaderModel ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void DoingImageLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void DoingImageLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void Start()
        {
            worker.RunWorkerAsync();
        }

        private void DoingImageLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                DoingDTO modelToExecute = null;
                while (ImageUploaderModelsQueue.TryDequeue(out modelToExecute))
                {
                    string fileMini = RingIDSettings.TEMP_DOING_MINI_FOLDER + "/" + modelToExecute.ImgUrl;
                    string fileFull = RingIDSettings.TEMP_DOING_FULL_FOLDER + "/" + modelToExecute.ImgUrl;
                    if (!File.Exists(fileMini))// || !HelperMethods.IsValidImageFile(fileMini, 0))
                    {
                        /* Bitmap BmpImg = ImageUtility.DownloadDoingImage(ServerAndPortSettings.GetFeelingMiniURL + modelToExecute.ImgUrl, fileMini);
                         if (BmpImg != null)
                             BmpImg.Dispose();*/
                        ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetFeelingMiniURL + modelToExecute.ImgUrl, fileMini);
                    }
                    if (!File.Exists(fileFull))// || !HelperMethods.IsValidImageFile(fileFull, 1))
                    {
                        /* Bitmap BmpImg = ImageUtility.DownloadDoingImage(ServerAndPortSettings.GetFeelingFullURL + modelToExecute.ImgUrl, fileFull);
                         if (BmpImg != null)
                             BmpImg.Dispose();*/
                        ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetFeelingFullURL + modelToExecute.ImgUrl, fileFull);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("PreviewImageLoad_DowWork ==> " + ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }
    }
}

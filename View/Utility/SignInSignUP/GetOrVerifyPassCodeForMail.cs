﻿using System;
using Auth.Service.SigninSignup;
using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.Utility.Auth;
using View.Utility.InternetOptions;

namespace View.Utility.SignInSignUP
{
    public class GetOrVerifyPassCodeForMail
    {
        #region "Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(GetOrVerifyPassCodeForMail).Name);
        private bool running = false;
        private SocketPorts socketPorts;
        string loginID, code;
        long userID;
        public int ActionToDo;
        public int SendCodeSignup = 1, VerifyCodeSignup = 2, SendCodeRecovery = 3, VerifyCodeRecovery = 4;
        JObject pakToSend;
        public event DelegateBoolStringString OnEmailCodeFeedBack;
        #endregion "Fields"

        #region "Private methods"
        private void Run()
        {
            bool isscusse = false;
            string msg = string.Empty;
            try
            {
                running = true;
                bool isIPPortSet = false;
                DefaultSettings.IsInternetAvailable = InternetCheckUtility.IsInternetAvailable();
                if (socketPorts == null) socketPorts = new SocketPorts();
                pakToSend = new JObject();
                pakToSend[JsonKeys.DeviceId] = HelperMethods.GetGUID();
                if (ActionToDo == SendCodeSignup)
                {
                    pakToSend[JsonKeys.Email] = loginID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_SEND_EMAIL_PASSCODE;
                    CommunicationPortsDTO communicationPortsDTO = socketPorts.SetSocketPortsByUnusedRingId(DefaultSettings.VALUE_NEW_USER_NAME);
                    if (userID > 0 || !string.IsNullOrEmpty(communicationPortsDTO.RingID))
                    {
                        if (string.IsNullOrEmpty(HttpRequest.GetRingIDFromEmail(loginID))) isIPPortSet = true;
                        else msg = NotificationMessages.EMAIL_ALREADY_IN_USE;
                    }
                    else msg = NotificationMessages.SERVER_ERROR;
                    pakToSend[JsonKeys.UserIdentity] = DefaultSettings.VALUE_NEW_USER_NAME;
                }
                else if (ActionToDo == VerifyCodeSignup)
                {
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_SEND_EMAIL_PASSCODE;
                    pakToSend[JsonKeys.Email] = loginID;
                    pakToSend[JsonKeys.EmailVerificationCode] = code;
                    CommunicationPortsDTO communicationPortsDTO = socketPorts.SetSocketPortsByUnusedRingId(DefaultSettings.VALUE_NEW_USER_NAME);
                    if (userID > 0 && communicationPortsDTO != null) isIPPortSet = true;
                    else msg = NotificationMessages.SERVER_ERROR;
                    pakToSend[JsonKeys.UserIdentity] = DefaultSettings.VALUE_NEW_USER_NAME;
                }
                else if (ActionToDo == SendCodeRecovery)
                {
                    pakToSend[JsonKeys.RecoverBy] = loginID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_SEND_RECOVERED_PASSCODE;
                    CommunicationPortsDTO communicationPortsDTO = socketPorts.SetSocketPortsByEmail(loginID);
                    if (communicationPortsDTO != null) isIPPortSet = true;
                    else msg = NotificationMessages.FAILDED_TRY_AGAIN;
                }
                else if (ActionToDo == VerifyCodeRecovery)
                {
                    isIPPortSet = true;
                    pakToSend[JsonKeys.RecoverBy] = loginID;
                    pakToSend[JsonKeys.VerificationCode] = code;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_VERIFY_RECOVERED_PASSCODE;
                    pakToSend[JsonKeys.UserIdentity] = userID;
                }
                string packetId = SendToServer.GetRanDomPacketIDBeforeLogin(DefaultSettings.VALUE_NEW_USER_NAME.ToString());
                pakToSend[JsonKeys.PacketId] = packetId;
                if (isIPPortSet)
                {
                    JObject feedbackfields = SendAuthReqeust.SendRequestNoSessionID(pakToSend, AppConstants.REQUEST_TYPE_AUTH, packetId);
                    if (feedbackfields != null)
                    {
                        try
                        {
                            if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                            {
                                isscusse = true;
                                if ((ActionToDo == SendCodeSignup || ActionToDo == SendCodeRecovery) && feedbackfields[JsonKeys.Message] != null) msg = (string)feedbackfields[JsonKeys.Message];
                            }
                            else
                            {
                                if (feedbackfields[JsonKeys.Message] != null) msg = (string)feedbackfields[JsonKeys.Message];
                                else msg = "Failed! Try Again.";
                            }
                        }
                        finally { RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields); }
                    }
                    else
                    {
                        if (MainSwitcher.ThreadManager().PingNow()) msg = NotificationMessages.CAN_NOT_PROCESS;
                        else msg = NotificationMessages.INTERNET_UNAVAILABLE;
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error("Error in UCSignUpwithEmail: type ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                msg = "Failed! Try Again.";
            }
            finally
            {
                running = false;
                if (OnEmailCodeFeedBack != null) OnEmailCodeFeedBack(isscusse, code, msg);
            }
        }

        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(int actionToDo, string loginID, long userID)
        {
            if (!running)
            {
                this.ActionToDo = actionToDo;
                this.userID = userID;
                this.loginID = loginID;
                this.code = string.Empty;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
                thrd.Name = this.GetType().Name;
            }
        }
        public void StartThread(int actionToDo, string loginID, long userID, string code)
        {
            if (!running)
            {
                this.ActionToDo = actionToDo;
                this.loginID = loginID;
                this.userID = userID;
                this.code = code;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
                thrd.Name = this.GetType().Name;
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }
        #endregion "Public methods"
    }
}

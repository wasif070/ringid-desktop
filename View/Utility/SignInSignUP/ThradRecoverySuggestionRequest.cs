﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.SignInSignUP
{
    public class ThradRecoverySuggestionRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradRecoverySuggestionRequest).Name);
        private bool running = false;
        private string IDorMailorNumber;
        private List<string> sgns;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_RECOVERY_SUGGESTION;
                pakToSend[JsonKeys.DeviceId] = HelperMethods.GetGUID();
                pakToSend[JsonKeys.RecoverBy] = IDorMailorNumber;
                pakToSend[JsonKeys.Version] = AppConfig.VERSION_PC;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.SendRequestNoSessionID(pakToSend, AppConstants.REQUEST_TYPE_AUTH, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        if (feedbackfields[JsonKeys.Suggestions] != null)
                        {
                            sgns = new List<string>();
                            JArray SuggestionsList = (JArray)feedbackfields[JsonKeys.Suggestions];
                            for (int j = 0; j < SuggestionsList.Count; j++)
                            {
                                string a = (string)SuggestionsList.ElementAt(j);
                                sgns.Add(a);
                            }
                        }
                    }
                    //if (feedbackfields[JsonKeys.Message] != null)
                    //{
                    //    msg = (string)feedbackfields[JsonKeys.Message];
                    //}
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow())
                    {

                    }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(string IDorMailorNumber)
        {
            if (!running)
            {
                this.IDorMailorNumber = IDorMailorNumber;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

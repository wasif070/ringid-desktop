﻿using System;
using Auth.Service.SigninSignup;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.Utility.Services;

namespace View.Utility.SignInSignUP
{
    public class SigninRequest
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(SigninRequest).Name);
        private SignedInInfoDTO signedInInfoDTO;
        private bool isSigninBackground = false;
        AuthRequestNoSessionID authRequest;
        bool stopReqeust = false;
        #endregion//"Fields"

        #region"Ctors"
        public SigninRequest() { }
        #endregion//"Ctors"

        #region"Public Methods"

        public SignedInInfoDTO FetchPortsAndSignIn(int loginType, string ringId, string password, string email = null, string mobileCode = null, string phoneNumber = null, string socialMediaId = null, string inputToken = null)
        {
            if (loginType == SettingsConstants.RINGID_LOGIN) return signinRequestByRingId(ringId, password);
            else if (loginType == SettingsConstants.EMAIL_LOGIN) return signinRequestByEmail(email, password);
            else if (loginType == SettingsConstants.MOBILE_LOGIN)
            {
                if (mobileCode != null && phoneNumber != null) return signinRequestByMobile(phoneNumber, mobileCode, password);
                else return signinRequestByRingId(ringId, password);
            }
            else if (loginType == SettingsConstants.FACEBOOK_LOGIN || loginType == SettingsConstants.TWITTER_LOGIN) return signinRequestBySocialMediaID(loginType, socialMediaId, inputToken, ringId);
            return null;
        }

        public SignedInInfoDTO signinRequestByRingId(string ringid, string password)
        {
            signedInInfoDTO = new SignedInInfoDTO();
            CommunicationPortsDTO communicationPortsDTO = new SocketPorts().SetSocketPortsByRingId(ringid);
            startLogin(communicationPortsDTO, SettingsConstants.RINGID_LOGIN, ringid, password, null);
            return signedInInfoDTO;
        }

        public SignedInInfoDTO signinRequestByMobile(string mobile, string mbldc, string password)
        {
            signedInInfoDTO = new SignedInInfoDTO();
            signedInInfoDTO = new SignedInInfoDTO();
            CommunicationPortsDTO communicationPortsDTO = new SocketPorts().SetSocketPortsByMobile(mobile, mbldc);
            startLogin(communicationPortsDTO, SettingsConstants.MOBILE_LOGIN, mobile, password, mbldc);
            return signedInInfoDTO;
        }

        public SignedInInfoDTO signinRequestByEmail(string email, string password)
        {
            signedInInfoDTO = new SignedInInfoDTO();
            CommunicationPortsDTO communicationPortsDTO = new SocketPorts().SetSocketPortsByEmail(email);
            startLogin(communicationPortsDTO, SettingsConstants.EMAIL_LOGIN, email, password, null);
            return signedInInfoDTO;
        }

        public SignedInInfoDTO signinRequestBySocialMediaID(int loginType, string socialmediaid, string inputToken, string ringid)
        {
            signedInInfoDTO = new SignedInInfoDTO();
            CommunicationPortsDTO communicationPortsDTO = new SocketPorts().SetSocketPortsBySocialMedia(loginType, socialmediaid);
            startLogin(communicationPortsDTO, loginType, ringid, socialmediaid, inputToken);
            return signedInInfoDTO;
        }

        public SignedInInfoDTO SignInRequestBackground()
        {
            signedInInfoDTO = new SignedInInfoDTO();
            this.isSigninBackground = true;
            if (DefaultSettings.DEBUG) log.Info("SignInRequest in Background ==> START");
            CommunicationPortsDTO communicationPortsDTO = null;
            int loginType = DefaultSettings.VALUE_LOGIN_USER_TYPE;
            if (loginType == SettingsConstants.EMAIL_LOGIN) communicationPortsDTO = new SocketPorts().SetSocketPortsByEmail(DefaultSettings.VALUE_LOGIN_USER_NAME);
            else if (loginType == SettingsConstants.MOBILE_LOGIN) communicationPortsDTO = new SocketPorts().SetSocketPortsByMobile(DefaultSettings.VALUE_LOGIN_USER_NAME, DefaultSettings.VALUE_MOBILE_DIALING_CODE);
            else
            {
                loginType = SettingsConstants.RINGID_LOGIN;
                communicationPortsDTO = new SocketPorts().SetSocketPortsByRingId(DefaultSettings.VALUE_LOGIN_USER_NAME);
            }
            if (communicationPortsDTO != null && communicationPortsDTO.IsSuccess) startLogin(communicationPortsDTO, loginType, DefaultSettings.VALUE_LOGIN_USER_NAME, DefaultSettings.VALUE_LOGIN_USER_PASSWORD, DefaultSettings.VALUE_MOBILE_DIALING_CODE);
            else
            {
                if (DefaultSettings.userProfile != null) DefaultSettings.userProfile.Mood = StatusConstants.MOOD_OFFLINE;
                log.Error("Failed! => SignInRequest in Background ==> No Available Internet Connection!");
            }
            return signedInInfoDTO;
        }

        private void startLogin(CommunicationPortsDTO communicationPortsDTO, int loginType1, string loginIDOrMbl, string passwordMblSocialID, string mbldDcOrToken = null)
        {
            if (!stopReqeust)
            {
                if (communicationPortsDTO != null && communicationPortsDTO.IsSuccess)
                {
                    DefaultSettings.IsInternetAvailable = true;
                    if (DefaultSettings.IsInternetAvailable) signedInInfoDTO = StartLoginProcess(loginType1, loginIDOrMbl, passwordMblSocialID, mbldDcOrToken);
                    else
                    {
                        signedInInfoDTO.isSuccess = false;
                        signedInInfoDTO.errorMsg = "Failed! " + NotificationMessages.NETWORK_PROBLEM;
                    }
                }
                else
                {
                    signedInInfoDTO.isSuccess = false;
                    signedInInfoDTO.errorMsg = NotificationMessages.SERVER_ERROR;
                }
            }
            else
            {
                signedInInfoDTO.isSuccess = false;
                signedInInfoDTO.errorMsg = NotificationMessages.TEXT_CANCELED;
            }
        }

        public SignedInInfoDTO StartLoginProcess(int loginType1, string loginIDOrMbl, string passwordMblSocialID, string mbldDcOrToken = null)
        {
            SignedInInfoDTO signIdto = new SignedInInfoDTO();
            try
            {
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SIGN_IN;
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.DeviceId] = HelperMethods.GetGUID();
                pakToSend[JsonKeys.LoginType] = loginType1;
                pakToSend[JsonKeys.AppType] = SettingsConstants.APP_TYPE_RINGID_FULL;
                if (loginType1 == SettingsConstants.RINGID_LOGIN)
                {
                    pakToSend[JsonKeys.UserIdentity] = loginIDOrMbl;
                    pakToSend[JsonKeys.Password] = passwordMblSocialID;
                }
                else if (loginType1 == SettingsConstants.EMAIL_LOGIN)
                {
                    pakToSend[JsonKeys.Email] = loginIDOrMbl;
                    pakToSend[JsonKeys.Password] = passwordMblSocialID;
                }
                else if (loginType1 == SettingsConstants.MOBILE_LOGIN)
                {
                    if (mbldDcOrToken.Equals("+880") && loginIDOrMbl[0] == '0') loginIDOrMbl = loginIDOrMbl.Substring(1);
                    pakToSend[JsonKeys.MobilePhone] = loginIDOrMbl;
                    pakToSend["mblDc"] = mbldDcOrToken;
                    pakToSend[JsonKeys.Password] = passwordMblSocialID;
                }
                else if (loginType1 == SettingsConstants.FACEBOOK_LOGIN || loginType1 == SettingsConstants.TWITTER_LOGIN)
                {
                    pakToSend[JsonKeys.UserIdentity] = loginIDOrMbl;
                    pakToSend[JsonKeys.SocialMediaId] = passwordMblSocialID;
                    pakToSend[JsonKeys.InputToken] = mbldDcOrToken;
                }
                pakToSend[JsonKeys.Version] = AppConfig.VERSION_PC;
                pakToSend[JsonKeys.Device] = AppConstants.PLATFORM_DESKTOP;
                pakToSend[JsonKeys.DeviceCategory] = AppConstants.CATEGORY_PC;
                if (authRequest == null) authRequest = new AuthRequestNoSessionID();
                if (!stopReqeust)
                {
                    JObject jObject = authRequest.SendRequestNoSessionID(pakToSend, AppConstants.REQUEST_TYPE_AUTH, packetId, 500);
                    if (jObject != null)
                    {
                        if (jObject[JsonKeys.RequestCancelled] != null)
                        {
                            signIdto.isSuccess = false;
                            signIdto.ReqeustCancelled = true;
                        }
                        else
                        {
                            signIdto.gotResponseFromServer = true;
                            try
                            {
                                if (jObject[JsonKeys.Success] != null && (bool)jObject[JsonKeys.Success])
                                {
                                    if (jObject[JsonKeys.Success] != null && (bool)jObject[JsonKeys.Success])
                                    {
                                        signIdto.isSuccess = true;
                                        HelperMethodsModel.BindSignInDetails(jObject);
                                        SetDefaultSettings(loginType1, loginIDOrMbl, mbldDcOrToken);
                                        var pswProperty = jObject.Property(JsonKeys.Password);
                                        if (pswProperty != null)
                                        {
                                            jObject.Property(JsonKeys.Password).Remove();
                                            DefaultSettings.VALUE_LOGIN_USER_INFO = jObject;
                                            new DatabaseActivityDAO().SaveLoginInfointoDB(jObject.ToString());
                                        }
                                        DefaultSettings.IsInternetAvailable = true;
                                        if (View.ViewModel.RingIDViewModel.Instance.WinDataModel != null) View.ViewModel.RingIDViewModel.Instance.WinDataModel.IsSignIn = true;
                                        MainSwitcher.ThreadManager().StartKeepAliveThread();
                                        HelperMethods.InitChatSDK();
                                    }
                                }
                                else
                                {
                                    if (View.ViewModel.RingIDViewModel.Instance.WinDataModel != null) View.ViewModel.RingIDViewModel.Instance.WinDataModel.IsSignIn = false;
                                    signIdto.isSuccess = false;
                                    if (jObject != null)
                                    {
                                        if (jObject[JsonKeys.Message] != null) signIdto.errorMsg = (string)jObject[JsonKeys.Message];
                                        if (jObject[JsonKeys.ReasonCode] != null)
                                        {
                                            signIdto.reasonCode = (int)jObject[JsonKeys.ReasonCode];
                                            if (string.IsNullOrEmpty(signIdto.errorMsg)) signIdto.errorMsg = ReasonCodeConstants.GetReason((int)jObject[JsonKeys.ReasonCode]);
                                        }
                                        else if (jObject[JsonKeys.NoResponseFromServer] != null) signIdto.gotResponseFromServer = false;
                                        DefaultSettings.LOGIN_SESSIONID = null;
                                        if (DefaultSettings.userProfile != null) DefaultSettings.userProfile = null;
                                        if (jObject[JsonKeys.VersionMessage] != null) signIdto.versionMsg = (string)jObject[JsonKeys.VersionMessage];
                                        if (jObject[JsonKeys.DownloadMandatory] != null && (bool)jObject[JsonKeys.DownloadMandatory]) signIdto.isDownloadMandatory = true;
                                    }
                                    else
                                    {
                                        if (log.IsErrorEnabled) log.Error(AppConstants.TYPE_SIGN_IN + " response not in correct format==>" + jObject.ToString());
                                        signIdto.isSuccess = false;
                                        signIdto.errorMsg = " Failed! Internal server error. Please Try again";
                                    }
                                }
                            }
                            finally { RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out jObject); }
                        }
                    }
                    else
                    {
                        if (isSigninBackground) if (!MainSwitcher.ThreadManager().PingNow()) signedInInfoDTO.errorMsg = " Failed! " + NotificationMessages.NETWORK_PROBLEM;
                        signIdto.isSuccess = false;
                    }
                }
                else
                {
                    signIdto.isSuccess = false;
                    signIdto.ReqeustCancelled = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception in SignInRequest ==>" + ex.Message + "\n" + ex.StackTrace + ex.Message);
                signIdto.isSuccess = false;
                signIdto.errorMsg = " Failed! Try again";
            }
            return signIdto;
        }

        public void Stop()
        {
            if (authRequest != null) authRequest.Stop();
            stopReqeust = true;
        }
        #endregion//"Public Methods"

        #region"Private methods"

        private void SetDefaultSettings(int loginType1, string loginIDOrMblorEmail, string mbldDcOrToken = null)
        {
            DefaultSettings.VALUE_LOGIN_USER_TYPE = loginType1;
            if (loginType1 == SettingsConstants.EMAIL_LOGIN) DefaultSettings.VALUE_LOGIN_USER_NAME = loginIDOrMblorEmail;
            else if (loginType1 == SettingsConstants.MOBILE_LOGIN)
            {
                DefaultSettings.VALUE_LOGIN_USER_NAME = loginIDOrMblorEmail;
                DefaultSettings.VALUE_MOBILE_DIALING_CODE = mbldDcOrToken;
            }
            else
            {
                DefaultSettings.VALUE_LOGIN_USER_TYPE = SettingsConstants.RINGID_LOGIN;
                DefaultSettings.VALUE_LOGIN_USER_NAME = DefaultSettings.userProfile.RingID.ToString();
            }
        }
        #endregion//"Private methods"
    }
}

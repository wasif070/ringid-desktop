﻿using System;
using System.Collections.Generic;
using System.Linq;
using Auth.Service.SigninSignup;
using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.SignInSignUP
{
    public class RecoverySuggestionRequest
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(RecoverySuggestionRequest).Name);
        private string IDorMailorNumber;
        private List<string> sgns = null;
        private SocketPorts socketPorts;
        public RecoverySuggestionRequest(string IDorMailorNumber)
        {
            this.IDorMailorNumber = IDorMailorNumber;
        }
        public List<string> Run(out bool isSuccess, out string msg)
        {
            isSuccess = false;
            msg = "";
            try
            {
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_RECOVERY_SUGGESTION;
                pakToSend[JsonKeys.DeviceId] = HelperMethods.GetGUID();
                pakToSend[JsonKeys.RecoverBy] = IDorMailorNumber;
                pakToSend[JsonKeys.Version] = AppConfig.VERSION_PC;
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.SendRequestNoSessionID(pakToSend, AppConstants.REQUEST_TYPE_AUTH, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        isSuccess = true;
                        if (feedbackfields[JsonKeys.Suggestions] != null)
                        {
                            sgns = new List<string>();
                            JArray SuggestionsList = (JArray)feedbackfields[JsonKeys.Suggestions];
                            for (int j = 0; j < SuggestionsList.Count; j++)
                            {
                                string a = (string)SuggestionsList.ElementAt(j);
                                sgns.Add(a);
                            }
                        }
                    }
                    if (feedbackfields[JsonKeys.Message] != null) msg = (string)feedbackfields[JsonKeys.Message];
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    msg = "Failed! Try Again.";
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception e) { log.Error("Exception in RecoverySuggestionRequest ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message); }
            return sgns;
        }

        public List<string> RecoverySuggesionList(int loginType, string ringId, string email = null, string mobileCode = null, string phoneNumber = null)
        {
            socketPorts = new SocketPorts();
            CommunicationPortsDTO communicationPortsDTO = null;
            string messge = null;
            bool sucess = false;
            if (loginType == SettingsConstants.RINGID_LOGIN) communicationPortsDTO = socketPorts.SetSocketPortsByRingId(ringId);
            else if (loginType == SettingsConstants.EMAIL_LOGIN) communicationPortsDTO = socketPorts.SetSocketPortsByEmail(email);
            else if (loginType == SettingsConstants.MOBILE_LOGIN) communicationPortsDTO = socketPorts.SetSocketPortsByMobile(phoneNumber, mobileCode);
            if (communicationPortsDTO != null) return Run(out sucess, out messge);
            return null;
        }
    }
}

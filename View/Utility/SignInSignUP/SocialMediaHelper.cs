﻿using Auth.Service.SigninSignup;
using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.Constants;
using View.Utility.Auth;

namespace View.Utility.SignInSignUP
{
    /// <summary>
    /// For sign up 
    /// * If social id already used by another ringID then need to send login request with this id go to ui after login
    /// * Show ALREADY_HAVE_A_RINGID notification
    /// * If no ringID with this Social id then goto name password panel
    /// 
    /// For Recovery
    /// * Get ringID for this soical id 
    /// * If both same then processed to recovery
    /// * If not same show use a message "FB_NOT_SAME" 
    /// * If ringID null then go to name password Panel for new signup
    /// 
    /// For Sign IN
    /// Send signin reqeust with social media
    /// if faild show error 
    /// or Go to ui After login
    /// </summary>
    public class SocialMediaHelper
    {
        #region "Fields"
        private int loginType;
        private int CurrentUIType;
        string socialMediaID;
        string socialMediaToken;
        public string MessageToShow = null;
        public string RingID = null;
        public SigninRequest signInreguest;

        #endregion //Fields

        #region "Ctors"
        public SocialMediaHelper(int loginType, int currentUIType, string socialMediaID, string socialMediaToken)
        {
            this.loginType = loginType;
            this.CurrentUIType = currentUIType;
            this.socialMediaID = socialMediaID;
            this.socialMediaToken = socialMediaToken;
        }

        public int ProcessMedia()
        {
            int typeToSwitch = 0;
            if (CurrentUIType == SettingsConstants.TYPE_FROM_PROFILE)
            {
                bool success = false;
                string msg = string.Empty;
                JObject pakToSend = new JObject();
                string pakId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_SOCIAL_MEDIA_ID;
                pakToSend[JsonKeys.SocialMediaId] = socialMediaID;
                pakToSend[JsonKeys.SocialMediaType] = loginType;
                pakToSend[JsonKeys.InputToken] = socialMediaToken;
                new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_ADD_SOCIAL_MEDIA_ID, AppConstants.REQUEST_TYPE_REQUEST).Run(out success, out msg);
                MessageToShow = success ? string.Empty : msg;
            }
            else if (!string.IsNullOrEmpty(socialMediaID))
            {
                if (ServerAndPortSettings.GetComportURL != null)
                {
                    if (CurrentUIType == SettingsConstants.TYPE_FROM_SIGNUP)
                    {
                        CommunicationPortsDTO getComPorts = new SocketPorts().SetSocketPortsBySocialMedia(loginType, socialMediaID);
                        if (getComPorts == null || !getComPorts.IsSuccess)
                        {
                            CommunicationPortsDTO newPorts = new SocketPorts().SetSocketPortsByUnusedRingId(DefaultSettings.VALUE_NEW_USER_NAME);
                            if (newPorts != null && newPorts.IsSuccess) typeToSwitch = WelcomePanelConstants.TypeSignupNamePassword;
                            else MessageToShow = NotificationMessages.SERVER_ERROR + ": " + CurrentUIType;
                        }
                        else
                        {
                            RingID = getComPorts.RingID;
                            typeToSwitch = sendSignInReqeust(loginType, RingID, socialMediaID, socialMediaToken);
                        }
                    }
                    else if (CurrentUIType == SettingsConstants.TYPE_FROM_SIGNIN)
                    {
                        CommunicationPortsDTO getComPorts = new SocketPorts().SetSocketPortsBySocialMedia(loginType, socialMediaID);
                        if (getComPorts != null && getComPorts.IsSuccess && !string.IsNullOrEmpty(getComPorts.RingID))
                        {
                            RingID = getComPorts.RingID;
                            typeToSwitch = sendSignInReqeust(loginType, RingID, socialMediaID, socialMediaToken);
                        }
                        else if (getComPorts != null && !getComPorts.IsSuccess && loginType == SettingsConstants.TWITTER_LOGIN)
                        {
                            MessageToShow = NotificationMessages.INVALID_TWITTER;
                            typeToSwitch = WelcomePanelConstants.TypeSigninFaild;
                        }
                        else if (getComPorts != null && !getComPorts.IsSuccess && loginType == SettingsConstants.FACEBOOK_LOGIN)
                        {
                            MessageToShow = NotificationMessages.INVALID_FACEBOOK;
                            typeToSwitch = WelcomePanelConstants.TypeSigninFaild;
                        }
                        else
                        {
                            MessageToShow = NotificationMessages.SERVER_ERROR + ": " + CurrentUIType;
                            typeToSwitch = WelcomePanelConstants.TypeSigninFaild;
                        }
                    }
                    else if (CurrentUIType == SettingsConstants.TYPE_FROM_RECOVERY)
                    {
                        RingID = HttpRequest.GetRingIDFromSocialMediaID(socialMediaID, loginType);
                        if (string.IsNullOrEmpty(RingID))
                        {
                            //if ((DefaultSettings.VALUE_NEW_USER_NAME > 0 && new SocketPorts().SetSocketPortsByUnusedRingId(DefaultSettings.VALUE_NEW_USER_NAME.ToString()))
                            //    || new SocketPorts().SetSocketPortsWithNewRingId()) typeToSwitch = WelcomePanelConstants.TypeSignupNamePassword;
                            CommunicationPortsDTO newPorts = new SocketPorts().SetSocketPortsByUnusedRingId(DefaultSettings.VALUE_NEW_USER_NAME);
                            if (newPorts != null && newPorts.IsSuccess) typeToSwitch = WelcomePanelConstants.TypeSignupNamePassword;
                            else MessageToShow = NotificationMessages.SERVER_ERROR + ": " + CurrentUIType;
                        }
                    }
                }
            }
            else MessageToShow = NotificationMessages.SERVER_ERROR + ": " + CurrentUIType;
            return typeToSwitch;
        }
        #endregion //Ctors

        #region" Utility Mehotds"
        private int sendSignInReqeust(int loginType1, string ringID1, string soicalMediaId1, string socialMediaToken1)
        {
            int typeToSwitch1 = 0;
            signInreguest = new SigninRequest();
            SignedInInfoDTO signedInInfoDTO = signInreguest.StartLoginProcess(loginType1, ringID1, soicalMediaId1, socialMediaToken1);
            if (signedInInfoDTO.isDownloadMandatory) typeToSwitch1 = 100;
            else if (signedInInfoDTO.isSuccess) typeToSwitch1 = WelcomePanelConstants.TypeSigninSuccess;
            else
            {
                typeToSwitch1 = WelcomePanelConstants.TypeSigninFaild;
                if (!string.IsNullOrEmpty(signedInInfoDTO.versionMsg)) MessageToShow = NotificationMessages.SERVER_ERROR + ": " + CurrentUIType;
                else if (!string.IsNullOrEmpty(signedInInfoDTO.errorMsg)) MessageToShow = signedInInfoDTO.errorMsg;
                else if (!DefaultSettings.IsInternetAvailable) MessageToShow = NotificationMessages.PLEASE_ENTER_DETAILS_CORRECTLY;
                else MessageToShow = NotificationMessages.PLEASE_ENTER_DETAILS_CORRECTLY;
            }
            return typeToSwitch1;
        }
        public void Stop()
        {
            if (signInreguest != null) signInreguest.Stop();
        }
        #endregion ""
    }
}

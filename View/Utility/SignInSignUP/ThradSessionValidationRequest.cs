﻿using System;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.SignInSignUP
{
    public class ThradSessionValidationRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradSessionValidationRequest).Name);
        private bool running = false;
        private ThradSessionInvalidLoginRequest thradSessionInvalidLoginRequest = null;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SESSION_VALIDATION;
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.AppType] = SettingsConstants.APP_TYPE_RINGID_FULL;
                pakToSend[JsonKeys.DeviceId] = HelperMethods.GetGUID();
                pakToSend[JsonKeys.UserTableID] = DefaultSettings.LOGIN_TABLE_ID;
                pakToSend[JsonKeys.DeviceCategory] = AppConstants.CATEGORY_PC;
                pakToSend[JsonKeys.Version] = AppConfig.VERSION_PC;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_AUTH, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.ReasonCode] != null && (int)feedbackfields[JsonKeys.ReasonCode] == ReasonCodeConstants.REASON_CODE_LOGGED_IN_FROM_ANOTHER_DEVICE)
                        UIHelperMethods.MultipleSessionWarning();
                    else
                    {
                        if (thradSessionInvalidLoginRequest == null) thradSessionInvalidLoginRequest = new ThradSessionInvalidLoginRequest();
                        thradSessionInvalidLoginRequest.StartThread();
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
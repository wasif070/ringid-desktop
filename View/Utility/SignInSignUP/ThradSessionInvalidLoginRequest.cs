﻿using System;
using Models.Constants;
using Models.Entity;
using View.Utility.InternetOptions;
using View.ViewModel;

namespace View.Utility.SignInSignUP
{
    public class ThradSessionInvalidLoginRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradSessionInvalidLoginRequest).Name);
        private bool running = false;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                if (!String.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_NAME) && !string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_PASSWORD))
                {
                    SignedInInfoDTO signedInInfoDTO = new SigninRequest().SignInRequestBackground();
                    if (signedInInfoDTO != null)
                    {
                        if (signedInInfoDTO.isSuccess)
                        {
                            DefaultSettings.userProfile.Mood = StatusConstants.MOOD_ALIVE;
                            RingIDViewModel.Instance.ChangeMyStatusIcon(StatusConstants.MOOD_ALIVE);
                            RingIDViewModel.Instance.ReloadMyProfile();
                            MainSwitcher.ThreadManager().StopNetworkThread();
                        }
                        else if (signedInInfoDTO.isDownloadMandatory)
                        {
                            HelperMethods.DownloadMandatoryUpdater(signedInInfoDTO.versionMsg);
                        }
                        else if (signedInInfoDTO.reasonCode == ReasonCodeConstants.REASON_CODE_PASSWORD_DID_NOT_MATCHED)
                        {
                            UIHelperMethods.ShowErrorMessageBoxFromThread(ReasonCodeConstants.GetReason(ReasonCodeConstants.REASON_CODE_PASSWORD_DID_NOT_MATCHED), "Signin Failed!");
                            MainSwitcher.AuthSignalHandler().AppRestart();
                        }
                        else if (signedInInfoDTO.reasonCode == ReasonCodeConstants.REASON_CODE_INVALID_RINGID)
                        {
                            UIHelperMethods.ShowErrorMessageBoxFromThread(ReasonCodeConstants.GetReason(ReasonCodeConstants.REASON_CODE_INVALID_RINGID), "Signin Failed!");
                            MainSwitcher.AuthSignalHandler().AppRestart();
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(signedInInfoDTO.errorMsg)) UIHelperMethods.ShowErrorMessageBoxFromThread(signedInInfoDTO.errorMsg, "Signin Failed!");
                            MainSwitcher.AuthSignalHandler().AppRestart();
                        }
                    }
                }
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

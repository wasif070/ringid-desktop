﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Auth.Service.SigninSignup;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.Constants;
using View.Utility.Auth;
using View.Utility.InternetOptions;

namespace View.Utility.SignInSignUP
{
    //public class ThreadSendOrVerfyCodeForEmail
    //{
    //    #region "Private Fields"
    //    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadSendOrVerfyCodeForEmail).Name);
    //    private bool running = false;
    //    private SocketPorts socketPorts;
    //    string loginID;
    //    int actionToDo;
    //    string code;
    //    long userID;
    //    int sendCodeSignup = 1;
    //    int verifyCodeSignup = 2;
    //    int sendCodeRecovery = 3;
    //    int verifyCodeRecovery = 4;
    //    JObject pakToSend;
    //    #endregion "Private Fields"

    //    #region "Private methods"
    //    private void Run()
    //    {
    //        try
    //        {
    //            running = true;
    //            bool isIPPortSet = false;
    //            // MainSwitcher.MainController().ViewModelSignIn.ShowPleaseWaitLoader(SettingsConstants.EMAIL_LOGIN);
    //            setComponentVisibility(false);
    //            DefaultSettings.IsInternetAvailable = InternetCheckUtility.IsInternetAvailable();
    //            if (socketPorts == null)
    //            {
    //                socketPorts = new SocketPorts();
    //            }
    //            pakToSend = new JObject();
    //            pakToSend[JsonKeys.DeviceId] = HelperMethods.GetGUID();

    //            if (actionToDo == sendCodeSignup)
    //            {
    //                pakToSend[JsonKeys.Email] = loginID;
    //                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SEND_EMAIL_PASSCODE;
    //                if (userID > 0 && socketPorts.SetSocketPortsByUnusedRingId(DefaultSettings.VALUE_NEW_USER_NAME.ToString())
    //                    || socketPorts.SetSocketPortsWithNewRingId())
    //                {
    //                    if (string.IsNullOrEmpty(HttpRequest.GetRingIDFromEmail(loginID)))
    //                    {
    //                        isIPPortSet = true;
    //                    }
    //                    else
    //                    {
    //                        showErrorMessage("This E-mail address is associated with another ringID");
    //                    }
    //                }
    //                else
    //                {
    //                    showErrorMessage("Communication server not responding");
    //                }
    //                pakToSend[JsonKeys.UserIdentity] = DefaultSettings.VALUE_NEW_USER_NAME;
    //            }
    //            else if (actionToDo == verifyCodeSignup)
    //            {
    //                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SEND_EMAIL_PASSCODE;
    //                pakToSend[JsonKeys.Email] = loginID;
    //                pakToSend[JsonKeys.EmailVerificationCode] = code;
    //                if (userID > 0 && socketPorts.SetSocketPortsByUnusedRingId(DefaultSettings.VALUE_NEW_USER_NAME.ToString())
    //                    || socketPorts.SetSocketPortsWithNewRingId())
    //                {
    //                    isIPPortSet = true;
    //                }
    //                else
    //                {
    //                    showErrorMessage("Communication server not responding");
    //                }
    //                pakToSend[JsonKeys.UserIdentity] = DefaultSettings.VALUE_NEW_USER_NAME;
    //            }
    //            else if (actionToDo == sendCodeRecovery)
    //            {
    //                pakToSend[JsonKeys.RecoverBy] = loginID;
    //                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SEND_RECOVERED_PASSCODE;
    //                if (socketPorts.SetSocketPortsByEmail(loginID))                        isIPPortSet = true;
    //                }
    //                else
    //                {
    //                    showErrorMessage("Failed to Send Code! Please try again later!");
    //                }
    //            }
    //            else if (actionToDo == verifyCodeRecovery)
    //            {
    //                isIPPortSet = true;
    //                pakToSend[JsonKeys.RecoverBy] = loginID;
    //                pakToSend[JsonKeys.VerificationCode] = code;
    //                pakToSend[JsonKeys.Action] = AppConstants.TYPE_VERIFY_RECOVERED_PASSCODE;
    //                pakToSend[JsonKeys.UserIdentity] = userID;
    //            }

    //            string packetId = SendToServer.GetRanDomPacketIDBeforeLogin(DefaultSettings.VALUE_NEW_USER_NAME.ToString());
    //            pakToSend[JsonKeys.PacketId] = packetId;

    //            if (isIPPortSet)
    //            {
    //                JObject feedbackfields = SendAuthReqeust.SendRequestNoSessionID(pakToSend, AppConstants.REQUEST_TYPE_AUTH, packetId);
    //                if (feedbackfields != null)
    //                {
    //                    try
    //                    {
    //                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
    //                        {
    //                            if (actionToDo == sendCodeSignup || actionToDo == sendCodeRecovery)
    //                            {
    //                                if (feedbackfields[JsonKeys.Message] != null)
    //                                {
    //                                    showErrorMessage((string)feedbackfields[JsonKeys.Message], SettingsConstants.EMAIL_LOGIN);
    //                                }
    //                            }
    //                            else if (actionToDo == verifyCodeSignup)
    //                            {
    //                                //Application.Current.Dispatcher.Invoke(() =>
    //                                //  {
    //                                //      MainSwitcher.MainController().MainUIController().View_WelcomeMainSwitcher.LoadUI(WelcomePanelConstants.TypeSignupNamePassword);
    //                                //      MainSwitcher.MainController().MainUIController().View_WelcomeMainSwitcher.View_SignUpNamePassword.LoadEmailData(loginID, code);
    //                                //  });
    //                                showErrorMessage(string.Empty);
    //                            }
    //                            else if (actionToDo == verifyCodeRecovery)
    //                            {
    //                                // Application.Current.Dispatcher.Invoke(() =>
    //                                //{
    //                                //    MainSwitcher.MainController().MainUIController().View_WelcomeMainSwitcher.LoadUI(WelcomePanelConstants.TypePasswordReset);
    //                                //    MainSwitcher.MainController().MainUIController().View_WelcomeMainSwitcher.View_UCRecoverResetPassword.LoadEmailData(userID, loginID);
    //                                //});
    //                                showErrorMessage(string.Empty);
    //                            }
    //                        }
    //                        else
    //                        {
    //                            if (feedbackfields[JsonKeys.Message] != null)
    //                            {
    //                                showErrorMessage((string)feedbackfields[JsonKeys.Message]);
    //                            }
    //                            else
    //                            {
    //                                showErrorMessage("Failed! Try Again.");
    //                            }
    //                        }
    //                    }
    //                    finally
    //                    {
    //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
    //                    }
    //                }
    //                else
    //                {
    //                    if (MainSwitcher.ThreadManager().PingNow())
    //                    {
    //                        showErrorMessage(NotificationMessages.CAN_NOT_PROCESS);
    //                    }
    //                    else
    //                    {
    //                        showErrorMessage(NotificationMessages.INTERNET_UNAVAILABLE);
    //                    }
    //                }
    //            }

    //            //if ((DefaultSettings.VALUE_NEW_USER_NAME > 0 && socketPorts.SetSocketPortsByUnusedRingId(DefaultSettings.VALUE_NEW_USER_NAME.ToString()))
    //            //                    || socketPorts.SetSocketPortsWithNewRingId())
    //            //{
    //            //    if (string.IsNullOrEmpty(HttpRequest.GetRingIDFromEmail(loginID)))
    //            //    {
    //            //        JObject feedbackfields = SendAuthReqeust.SendRequestNoSessionID(pakToSend, AppConstants.REQUEST_TYPE_AUTH, packetId);
    //            //        if (feedbackfields != null)
    //            //        {
    //            //            if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
    //            //            {
    //            //                if (feedbackfields[JsonKeys.Message] != null)
    //            //                {
    //            //                    showErrorMessage((string)feedbackfields[JsonKeys.Message], SettingsConstants.EMAIL_LOGIN);
    //            //                }
    //            //            }

    //            //            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
    //            //        }
    //            //        else
    //            //        {
    //            //            if (MainSwitcher.ThreadManager().PingNow())
    //            //            {
    //            //                showErrorMessage(NotificationMessages.CAN_NOT_PROCESS);
    //            //            }
    //            //            else
    //            //            {
    //            //                showErrorMessage(NotificationMessages.INTERNET_UNAVAILABLE);
    //            //            }
    //            //        }
    //            //    }
    //            //    else
    //            //    {
    //            //        showErrorMessage("This E-mail address is associated with another ringID");
    //            //    }
    //            //}
    //            //else
    //            //{
    //            //    showErrorMessage("Communication server not responding");
    //            //}

    //            //}
    //            //else
    //            //{
    //            //    showErrorMessage(validationMsg);
    //            //}
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("Error in UCSignUpwithEmail: type ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
    //            showErrorMessage("Failed! Try Again.");
    //        }
    //        finally
    //        {
    //            running = false;
    //            setComponentVisibility(true);
    //        }
    //    }

    //    private void showErrorMessage(string msg)
    //    {
    //        //   MainSwitcher.MainController().ViewModelSignIn.ShowErrorMessage(msg);
    //    }

    //    private void showErrorMessage(string msg, int type)
    //    {
    //        // MainSwitcher.MainController().ViewModelSignIn.ShowErrorMessage(msg, type);
    //    }

    //    private void setComponentVisibility(bool value)
    //    {
    //        //    MainSwitcher.MainController().ViewModelSignIn.IsComponentEnabled = value;
    //    }
    //    #endregion "Private methods"

    //    #region "Public Methods"
    //    public void StartThread(int actionToDo, string loginID, long userID)
    //    {
    //        if (!running)
    //        {
    //            this.actionToDo = actionToDo;
    //            this.userID = userID;
    //            this.loginID = loginID;
    //            this.code = string.Empty;
    //            System.Threading.Thread thrd = new System.Threading.Thread(Run);
    //            thrd.Start();
    //            thrd.Name = this.GetType().Name;
    //        }
    //    }
    //    public void StartThread(int actionToDo, string loginID, long userID, string code)
    //    {
    //        if (!running)
    //        {
    //            this.actionToDo = actionToDo;
    //            this.loginID = loginID;
    //            this.userID = userID;
    //            this.code = code;
    //            System.Threading.Thread thrd = new System.Threading.Thread(Run);
    //            thrd.Start();
    //            thrd.Name = this.GetType().Name;
    //        }
    //    }

    //    public void StopThread()
    //    {
    //        running = false;
    //    }

    //    public bool IsRunning()
    //    {
    //        return running;
    //    }

    //    #endregion "Public methods"
    //}
}

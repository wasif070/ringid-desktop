﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.SignInSignUP
{
    public class ThradAddSocialMediaId
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradAddSocialMediaId).Name);
        private bool running = false;
        private long smid;
        private int smt;
        private string it;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"

        public ThradAddSocialMediaId(long smid, int smt, string it)
        {
            this.smid = smid;
            this.smt = smt;
            this.it = it;
        }

        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"

        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_SOCIAL_MEDIA_ID;
                pakToSend[JsonKeys.SocialMediaId] = smid;
                pakToSend[JsonKeys.SocialMediaType] = smt;
                pakToSend[JsonKeys.InputToken] = it;
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if ((bool)feedbackfields[JsonKeys.Success])
                    {
                        string mg = (feedbackfields[JsonKeys.Message] != null) ? (string)feedbackfields[JsonKeys.Message] : "Media ID added Successfully!";
                    }
                    else
                    {
                        string mg = (feedbackfields[JsonKeys.Message] != null) ? (string)feedbackfields[JsonKeys.Message] : "Failed to Add Social Media";
                       UIHelperMethods.ShowErrorMessageBoxFromThread((string)feedbackfields[JsonKeys.Message], "Failed!");
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    string msg = (DefaultSettings.IsInternetAvailable) ? "" : ReasonCodeConstants.REASON_MSG_INTERNET_UNAVAILABLE;
                   UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to Add Social Media!" + msg, "Failed!");
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }

        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

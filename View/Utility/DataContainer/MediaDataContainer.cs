﻿using log4net;
using Models.Constants;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using View.BindingModels;
using View.ViewModel;
using System;
using System.Collections.ObjectModel;

namespace View.Utility.DataContainer
{
    public class MediaDataContainer
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MediaDataContainer).Name);

        public static MediaDataContainer Instance = new MediaDataContainer();

        public ConcurrentDictionary<Guid, SingleMediaModel> ContentModels = new ConcurrentDictionary<Guid, SingleMediaModel>(); //contentid, singmemediamodel

        public ConcurrentDictionary<Guid, CustomFileDownloader> CurrentDownloads = new ConcurrentDictionary<Guid, CustomFileDownloader>(); //contentid, CustomFileDownloader

        public void LoadRecentOrDownloadedMedias(DataTable table, bool isRecentMedia)
        {
            try
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    try
                    {
                        DataRow row = table.Rows[i];
                        Guid contentId = Guid.Parse((row[DBConstants.MEDIA_CONTENTID]).ToString());
                        SingleMediaModel contentModel = null;
                        if (!ContentModels.TryGetValue(contentId, out contentModel))
                        {
                            contentModel = new SingleMediaModel();
                            contentModel.ContentId = contentId;
                            ContentModels[contentId] = contentModel;

                            long UserTableID = (long)row[DBConstants.USER_TABLE_ID];
                            contentModel.MediaOwner = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(UserTableID);
                            //object aaa;
                            if (row[DBConstants.MEDIA_ALBUM_ID] != null)
                                //aaa = row[DBConstants.MEDIA_ALBUM_ID];
                                contentModel.AlbumId = Guid.Parse((row[DBConstants.MEDIA_ALBUM_ID]).ToString());
                            contentModel.AlbumName = (string)row[DBConstants.MEDIA_ALBUM_NAME];
                            contentModel.MediaType = (int)row[DBConstants.MEDIA_MEDIA_TYPE];
                            contentModel.Title = (string)row[DBConstants.MEDIA_TITLE];
                            contentModel.Artist = (string)row[DBConstants.MEDIA_ARTIST];
                            contentModel.StreamUrl = (string)row[DBConstants.MEDIA_STREAM_URL];
                            contentModel.ThumbUrl = (string)row[DBConstants.MEDIA_THUMBURL];
                            contentModel.Duration = (long)row[DBConstants.MEDIA_DURAION];
                            contentModel.ThumbImageWidth = (int)row[DBConstants.MEDIA_THUMB_IMAGE_WIDTH];
                            contentModel.ThumbImageHeight = (int)row[DBConstants.MEDIA_THUMB_IMAGE_HEIGHT];
                            contentModel.AccessCount = (short)row[DBConstants.MEDIA_ACCESS_COUNT];
                            //contentModel.ILike = (short)row[DBConstants.MEDIA_ILIKE];
                            //contentModel.IComment = (short)row[DBConstants.MEDIA_ICOMMENT];
                            //contentModel.LikeCount = (long)row[DBConstants.MEDIA_LIKE_COUNT];
                            //contentModel.CommentCount = (long)row[DBConstants.MEDIA_COMMENT_COUNT];
                        }
                        if (isRecentMedia)
                        {
                            contentModel.LastPlayedTime = (long)row[DBConstants.MEDIA_PLAYING_TIME];
                            if (!RingIDViewModel.Instance.MyRecentPlayList.Any(P => P.ContentId == contentId))
                                RingIDViewModel.Instance.MyRecentPlayList.InvokeAdd(contentModel);
                        }
                        else
                        {
                            contentModel.DownloadState = (short)row[DBConstants.MEDIA_DOWNLOAD_STATE];
                            if (contentModel.DownloadState == StatusConstants.MEDIA_DOWNLOADING_STATE)
                            {
                                contentModel.DownloadState = StatusConstants.MEDIA_DOWNLOAD_PAUSE_STATE;
                            }
                            contentModel.DownloadTime = (long)row[DBConstants.MEDIA_DOWNLOAD_TIME];
                            contentModel.DownloadProgress = (int)row[DBConstants.MEDIA_DOWNLOAD_PROGRESS];
                            if (!RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == contentId))
                                RingIDViewModel.Instance.MyDownloads.InvokeAdd(contentModel);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.StackTrace + ex.Message);
                    }
                }
                if (isRecentMedia)
                    //RingIDViewModel.Instance.MyRecentPlayList.OrderBy(i => i.LastPlayedTime);
                    RingIDViewModel.Instance.MyRecentPlayList = new ObservableCollection<SingleMediaModel>(RingIDViewModel.Instance.MyRecentPlayList.OrderByDescending(a => a.LastPlayedTime));
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }

    }
}

﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System;
using View.BindingModels;
using log4net;
using Newtonsoft.Json.Linq;

namespace View.Utility.DataContainer
{
    public class FeedDataContainer
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FeedDataContainer).Name);

        public static FeedDataContainer Instance = new FeedDataContainer();

        public ConcurrentDictionary<Guid, FeedModel> FeedModels = new ConcurrentDictionary<Guid, FeedModel>(); //nfid,feedmodel

        public ConcurrentDictionary<Guid, JObject> StorageFeedJObjects = new ConcurrentDictionary<Guid, JObject>(); //nfid,JObject

        public ConcurrentDictionary<Guid, List<Guid>> SharedNewsfeedIds = new ConcurrentDictionary<Guid, List<Guid>>(); //intnfid,{list externals}

        public List<Guid> AllRequestedPivotIds = new List<Guid>();

        public CustomSortedList AllBottomIds = new CustomSortedList();
        public CustomSortedList AllTopIds = new CustomSortedList();

        public CustomSortedList AllMediaBottomIds = new CustomSortedList();
        public CustomSortedList AllMediaTopIds = new CustomSortedList();
        public List<Guid> AllMediaCurrentIds = new List<Guid>();

        public CustomSortedList AllNewsPortalBottomIds = new CustomSortedList();
        public CustomSortedList AllNewsPortalTopIds = new CustomSortedList();
        public List<Guid> AllNewsPortalCurrentIds = new List<Guid>();

        public CustomSortedList SavedAllBottomIds = new CustomSortedList(true);
        public CustomSortedList SavedAllTopIds = new CustomSortedList(true);
        public List<Guid> SavedAllCurrentIds = new List<Guid>();

        public CustomSortedList SavedMediaBottomIds = new CustomSortedList(true);
        public CustomSortedList SavedMediaTopIds = new CustomSortedList(true);
        public List<Guid> SavedMediaCurrentIds = new List<Guid>();

        public CustomSortedList SavedNewsPortalBottomIds = new CustomSortedList(true);
        public CustomSortedList SavedNewsPortalTopIds = new CustomSortedList(true);
        public List<Guid> SavedNewsPortalCurrentIds = new List<Guid>();

        public CustomSortedList ProfileMyBottomIds = new CustomSortedList();
        public CustomSortedList ProfileMyTopIds = new CustomSortedList();
        public List<Guid> ProfileMyCurrentIds = new List<Guid>();

        public CustomSortedList ProfileFriendBottomIds = new CustomSortedList();
        public CustomSortedList ProfileFriendTopIds = new CustomSortedList();
        public List<Guid> ProfileFriendCurrentIds = new List<Guid>();

        public CustomSortedList ProfileMediaBottomIds = new CustomSortedList();
        public CustomSortedList ProfileMediaTopIds = new CustomSortedList();
        public List<Guid> ProfileMediaCurrentIds = new List<Guid>();

        public CustomSortedList ProfileNewsPortalBottomIds = new CustomSortedList();
        public CustomSortedList ProfileNewsPortalTopIds = new CustomSortedList();
        public List<Guid> ProfileNewsPortalCurrentIds = new List<Guid>();
        /// <summary>
        /// ///////////
        /// </summary>
        public CustomSortedList AllFeedsSortedIds = new CustomSortedList(); //actualtime, nfid for all feeds tab (except special)

        public CustomSortedList SpecialFeedSortedIds = new CustomSortedList(); //actualtime, nfid for all feeds tab (except special)

        public CustomSortedList SavedFeedsAllSortedIds = new CustomSortedList(true); //time, nfid for all feeds tab (except special)

        public CustomSortedList SavedFeedsMediaPageSortedIds = new CustomSortedList(true); //time, nfid for all feeds tab (except special)

        public CustomSortedList SavedFeedsPageSortedIds = new CustomSortedList(true); //time, nfid for all feeds tab (except special)

        public CustomSortedList SavedFeedsNewsPortalSortedIds = new CustomSortedList(true); //time, nfid for all feeds tab (except special)

        public CustomSortedList SavedFeedsCelebritySortedIds = new CustomSortedList(true); //time, nfid for all feeds tab (except special)

        public CustomSortedList SavedFeedsCircleSortedIds = new CustomSortedList(true); //time, nfid for all feeds tab (except special)

        public CustomSortedList AllNewsPortalFeedsSortedIds = new CustomSortedList();

        public CustomSortedList AllPagesFeedsSortedIds = new CustomSortedList();

        public CustomSortedList AllMediaPageFeedsSortedIds = new CustomSortedList();

        public CustomSortedList AllCircleFeedsSortedIds = new CustomSortedList();

        public CustomSortedList AllCelebrityFeedsSortedIds = new CustomSortedList();

        public CustomSortedList MyFeedsSortedIds = new CustomSortedList();

        public CustomSortedList CircleProfileFeedsSortedIds = new CustomSortedList();

        public CustomSortedList FriendProfileFeedsSortedIds = new CustomSortedList();

        public CustomSortedList NewsPortalProfileFeedsSortedIds = new CustomSortedList();

        public CustomSortedList PageProfileFeedsSortedIds = new CustomSortedList();

        public CustomSortedList MediaPageProfileFeedsSortedIds = new CustomSortedList();

        public CustomSortedList CelebrityProfileFeedsSortedIds = new CustomSortedList();

        /// <summary>
        /// //////////////////
        /// </summary>

        public List<Guid> UnreadNewsFeedIds = new List<Guid>();

        ///
        //public CustomSortedList AllFeedsWaitingIds = new CustomSortedList(); //actualtime, nfid for all feeds tab (except special)

        public void InsertIntoSharedFeedIds(Guid intNfid, Guid extNfid)
        {
            List<Guid> extIds = null;
            if (!SharedNewsfeedIds.TryGetValue(intNfid, out extIds))
            {
                extIds = new List<Guid>();
                extIds.Add(extNfid);
                SharedNewsfeedIds[intNfid] = extIds;
            }
            else
            {
                if (!extIds.Contains(extNfid)) extIds.Add(extNfid);
            }
        }
    }
}

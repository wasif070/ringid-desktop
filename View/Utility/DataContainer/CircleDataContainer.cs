﻿using System.Collections.Generic;
using View.BindingModels;

namespace View.Utility.DataContainer
{
    public class CircleDataContainer
    {
        private static CircleDataContainer _Instance = null;
        public static CircleDataContainer Instance
        {
            get
            {
                _Instance = _Instance ?? new CircleDataContainer(); return _Instance;
            }
        }

        private ObservableDictionary<long, CircleModel> _CIRCLE_MODELS_DICTIONARY = new ObservableDictionary<long, CircleModel>();
        public ObservableDictionary<long, CircleModel> CIRCLE_MODELS_DICTIONARY
        {
            get { return _CIRCLE_MODELS_DICTIONARY; }
        }

        public HashSet<long> CircleIDs = new HashSet<long>();

    }
}

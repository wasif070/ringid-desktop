﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.BindingModels;

namespace View.Utility.DataContainer
{
    public class ImageDataContainer
    {
        public static ImageDataContainer Instance = new ImageDataContainer();

        private Dictionary<string, Dictionary<Guid, ImageModel>> _TEMP_MY_PROFILE_IMAGES = new Dictionary<string, Dictionary<Guid, ImageModel>>();
        public Dictionary<string, Dictionary<Guid, ImageModel>> TEMP_MY_PROFILE_IMAGES
        {
            get { return _TEMP_MY_PROFILE_IMAGES; }
        }

        private Dictionary<string, Dictionary<Guid, ImageModel>> _TEMP_MY_COVER_IMAGES = new Dictionary<string, Dictionary<Guid, ImageModel>>();
        public Dictionary<string, Dictionary<Guid, ImageModel>> TEMP_MY_COVER_IMAGES
        {
            get { return _TEMP_MY_COVER_IMAGES; }
        }
        private Dictionary<string, Dictionary<Guid, ImageModel>> _TEMP_MY_FEED_IMAGES = new Dictionary<string, Dictionary<Guid, ImageModel>>();
        public Dictionary<string, Dictionary<Guid, ImageModel>> TEMP_MY_FEED_IMAGES
        {
            get { return _TEMP_MY_FEED_IMAGES; }
        }

        private Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> _TEMP_FRIEND_PROFILE_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>>();
        public Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> TEMP_FRIEND_PROFILE_IMAGES
        {
            get { return _TEMP_FRIEND_PROFILE_IMAGES; }
        }

        private Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> _TEMP_FRIEND_COVER_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>>();
        public Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> TEMP_FRIEND_COVER_IMAGES
        {
            get { return _TEMP_FRIEND_COVER_IMAGES; }
        }
        private Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> _TEMP_FRIEND_FEED_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>>();
        public Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> TEMP_FRIEND_FEED_IMAGES
        {
            get { return _TEMP_FRIEND_FEED_IMAGES; }
        }

        private Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> _TEMP_CELEBRITY_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>>();
        public Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> TEMP_CELEBRITY_IMAGES
        {
            get { return _TEMP_CELEBRITY_IMAGES; }
        }

        private Dictionary<long, Dictionary<Guid, Dictionary<string, Dictionary<Guid, ImageModel>>>> _TEMP_IMAGES = new Dictionary<long, Dictionary<Guid, Dictionary<string, Dictionary<Guid, ImageModel>>>>();
        public Dictionary<long, Dictionary<Guid, Dictionary<string, Dictionary<Guid, ImageModel>>>> TEMP_IMAGES
        {
            get { return _TEMP_IMAGES; }
        }

        public ConcurrentDictionary<Guid, ImageModel> ImageModelDictionary = new ConcurrentDictionary<Guid, ImageModel>();

        public void AddOrReplaceImageModels(Guid imageId, ImageModel model)
        {
            ImageModelDictionary[imageId] = model;
        }

        public ImageModel GetFromImageModelDictionary(Guid imageId)
        {
            ImageModel model = null;
            ImageModelDictionary.TryGetValue(imageId, out model);
            return model;
        }
        public ImageModel GetFromImageModelDictionaryNotNullable(Guid imageId)
        {
            ImageModel model = null;
            ImageModelDictionary.TryGetValue(imageId, out model);
            if (model == null)
            {
                model = new ImageModel();
                AddOrReplaceImageModels(imageId, model);
            }
            return model;
        }
    }
}

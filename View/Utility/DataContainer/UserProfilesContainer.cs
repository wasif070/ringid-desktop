﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using View.BindingModels;
using View.UI;

namespace View.Utility.DataContainer
{
    public class UserProfilesContainer
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(UserProfilesContainer).Name);

        private static UserProfilesContainer _Instance = new UserProfilesContainer();
        public static UserProfilesContainer Instance
        {
            get { return _Instance; }
            set { _Instance = value; }
        }


        private ConcurrentDictionary<long, NewsPortalModel> _NewsPortalModels = new ConcurrentDictionary<long, NewsPortalModel>();
        public ConcurrentDictionary<long, NewsPortalModel> NewsPortalModels
        {
            get { return _NewsPortalModels; }
        }
        private ConcurrentDictionary<long, PageInfoModel> _PageModels = new ConcurrentDictionary<long, PageInfoModel>();
        public ConcurrentDictionary<long, PageInfoModel> PageModels
        {
            get { return _PageModels; }
        }

        private ConcurrentDictionary<long, MusicPageModel> _MediaPageModels = new ConcurrentDictionary<long, MusicPageModel>();
        public ConcurrentDictionary<long, MusicPageModel> MediaPageModels
        {
            get { return _MediaPageModels; }
        }

        private ConcurrentDictionary<long, CelebrityModel> _CelebrityModels = new ConcurrentDictionary<long, CelebrityModel>();
        public ConcurrentDictionary<long, CelebrityModel> CelebrityModels
        {
            get { return _CelebrityModels; }
        }
        private ConcurrentDictionary<string, List<long>> _SearchedPageIdsByParam = new ConcurrentDictionary<string, List<long>>();
        public ConcurrentDictionary<string, List<long>> SearchedPageIdsByParam
        {
            get { return _SearchedPageIdsByParam; }
        }
        private ConcurrentDictionary<string, List<long>> _SearchedCelebrityIdsByParam = new ConcurrentDictionary<string, List<long>>();
        public ConcurrentDictionary<string, List<long>> SearchedCelebrityIdsByParam
        {
            get { return _SearchedCelebrityIdsByParam; }
        }
        private ConcurrentDictionary<string, List<long>> _SearchedPortalIdsByParam = new ConcurrentDictionary<string, List<long>>();
        public ConcurrentDictionary<string, List<long>> SearchedPortalIdsByParam
        {
            get { return _SearchedPortalIdsByParam; }
        }
        private ConcurrentDictionary<string, List<long>> _SearchedMediaPageIdsByParam = new ConcurrentDictionary<string, List<long>>();
        public ConcurrentDictionary<string, List<long>> SearchedMediaPageIdsByParam
        {
            get { return _SearchedMediaPageIdsByParam; }
        }

        public NewsPortalModel GetandUpdatePortalModel(long utId, long uId, string fn, int sCount, string prIm) //send ids=0, scount=-1. fn=null, prm=null is jobject dont have these information
        {
            NewsPortalModel model = null;
            if (!NewsPortalModels.TryGetValue(utId, out model))
            {
                model = new NewsPortalModel { UserTableID = utId };
                NewsPortalModels[utId] = model;
            }
            if (uId > 0) model.UserIdentity = uId;
            if (utId > 0) model.UserTableID = utId;
            if (fn != null) model.FullName = fn;
            if (sCount >= 0) model.SubscriberCount = sCount;
            if (prIm != null) model.ProfileImage = prIm;
            return model;
        }

        public PageInfoModel GetandUpdatePageModel(long pageId, long uId, long utId, string fn, int sCount, string prIm) //send ids=0, scount=-1. fn=null, prm=null is jobject dont have these information
        {
            PageInfoModel model = null;
            if (!PageModels.TryGetValue(pageId, out model))
            {
                model = new PageInfoModel { PageId = pageId };
                PageModels[pageId] = model;
            }
            if (uId > 0) model.UserIdentity = uId;
            if (utId > 0) model.UserTableID = utId;
            if (fn != null) model.FullName = fn;
            if (sCount >= 0) model.SubscriberCount = sCount;
            if (prIm != null) model.ProfileImage = prIm;
            return model;
        }

        public MusicPageModel GetandUpdateMediaPageModel(long pageId, long uId, long utId, string fn, int sCount, string prIm) //send ids=0, scount=-1. fn=null, prm=null is jobject dont have these information
        {
            MusicPageModel model = null;
            if (!MediaPageModels.TryGetValue(pageId, out model))
            {
                model = new MusicPageModel { MusicPageId = pageId };
                MediaPageModels[pageId] = model;
            }
            if (uId > 0) model.UserIdentity = uId;
            if (utId > 0) model.UserTableID = utId;
            if (fn != null) model.FullName = fn;
            if (sCount >= 0) model.SubscriberCount = sCount;
            if (prIm != null) model.ProfileImage = prIm;
            return model;
        }

        public CelebrityModel GetandUpdateCelebrityModel(long utId, long uId, string fn, int sCount, string prIm) //send ids=0, scount=-1. fn=null, prm=null is jobject dont have these information
        {
            CelebrityModel model = null;
            if (!CelebrityModels.TryGetValue(utId, out model))
            {
                model = new CelebrityModel { UserTableID = utId };
                CelebrityModels[utId] = model;
            }
            if (uId > 0) model.UserIdentity = uId;
            if (utId > 0) model.UserTableID = utId;
            if (fn != null) model.FullName = fn;
            if (sCount >= 0) model.SubscriberCount = sCount;
            if (prIm != null) model.ProfileImage = prIm;
            return model;
        }
        //public PageInfoModel SearchPageInfoModelByUtId(long utid) //search all models
        //{
        //    PageInfoModel model = null;
        //    if (!PageInfoModels.TryGetValue(utid, out model))
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel != null)
        //        {
        //            if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null)
        //            {
        //                if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCDiscoverPanel != null)
        //                {
        //                    model = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Where(P => P.UserTableID == utid).FirstOrDefault();
        //                }
        //                if (model == null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCFollowingListPanel != null)
        //                {
        //                    model = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Where(P => P.UserTableID == utid).FirstOrDefault();
        //                }
        //            }
        //            if (model == null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesSearchPanel != null)
        //            {
        //                model = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesSearchPanel.SearchedPages.Where(P => P.UserTableID == utid).FirstOrDefault();
        //            }
        //        }
        //    }
        //    return model;
        //}

        //public CelebrityModel SearchCelebrityModelByUtId(long utid) //search all models
        //{
        //    CelebrityModel model = null;
        //    if (!CelebrityModels.TryGetValue(utid, out model))
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel != null)
        //        {
        //            //if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null)
        //            //{
        //            //    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCDiscoverPanel != null)
        //            //    {
        //            //        model = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Where(P => P.PortalId == utid).FirstOrDefault();
        //            //    }
        //            //    if (model == null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCFollowingListPanel != null)
        //            //    {
        //            //        model = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Where(P => P.PortalId == utid).FirstOrDefault();
        //            //    }
        //            //}
        //            if (model == null && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSearchPanel != null)
        //            {
        //                model = UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSearchPanel.SearchedCelebrities.Where(P => P.UserIdentity == utid).FirstOrDefault();
        //            }
        //        }
        //    }
        //    return model;
        //}
    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.ImageViewer;
using View.UI.MediaPlayer;
using View.UI.PopUp;
using View.UI.Profile.FriendProfile;
using View.Utility.DataContainer;
using View.ViewModel;

namespace View.Utility
{
    public class ImageUtility
    {
        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(ImageUtility).Name);
        #endregion "Private Fields"

        #region Properties
        public static int IMG_FULL { get { return 1; } }
        public static int IMG_THUMB { get { return 2; } }
        public static int IMG_CROP { get { return 3; } }
        public static int IMG_300 { get { return 4; } }
        public static int IMG_600 { get { return 5; } }
        public static int IMG_SMALL_LARGE { get { return 6; } }
        public static int IMG_GROUP_LARGE { get { return 7; } }
        public static int IMG_THUMB_LARGE { get { return 8; } }
        public static int ALBUM_IMAGE_SIZE { get { return 150; } }
        public static int PANEL_HEIGHT = 500;
        public static int SEPERATOR = 3;
        private static System.Windows.Controls.Grid motherGrid;

        private static UCSingleFeedDetailsView ucSingleFeedDetailsView
        {
            get
            {
                var obj = ((System.Windows.Controls.Border)(motherGrid.Parent)).Parent;
                if (obj != null && obj is UCSingleFeedDetailsView) return (UCSingleFeedDetailsView)obj;
                return null;
            }
        }
        #endregion

        #region "Public Methods"

        public static BitmapFrame GetBitmapFrame(string location)
        {
            try
            {
                Uri iconUri = GetUri(location);
                return BitmapFrame.Create(iconUri);
            }
            catch (Exception ex)
            {
                log.Error("Error: GetBitmapFrame()." + ex.Message);
            }
            return null;
        }

        public static BitmapImage GetBitmapImage(string location)
        {
            try
            {
                Uri iconUri = GetUri(location);
                return new BitmapImage(iconUri);
            }
            catch (Exception ex)
            {
                log.Error("Error: GetBitmapImage()." + ex.Message);
            }
            return null;
        }

        public static Bitmap GetBitmap(string location)
        {
            try
            {
                Uri iconUri = GetUri(location); // new Uri("pack://application:,,,/" + RingIDSettings.APP_NAME + ";component" + location, UriKind.RelativeOrAbsolute);
                return new Bitmap(Application.GetResourceStream(iconUri).Stream);
            }
            catch (Exception ex)
            {
                log.Error("Error: GetBitmap()." + ex.Message);
            }
            return null;
        }

        public static Icon GetIcon(string location)
        {
            try
            {
                Uri iconUri = GetUri(location);// new Uri("pack://application:,,,/" + RingIDSettings.APP_NAME + ";component" + location, UriKind.RelativeOrAbsolute);
                return new Icon(Application.GetResourceStream(iconUri).Stream);
            }
            catch (Exception ex)
            {
                log.Error("Error: GetIcon()." + ex.Message);
            }
            return null;
        }

        public static Cursor GetCursor(String location, byte hotspotx, byte hotspoty)
        {
            try
            {
                Uri iconUri = GetUri(location);// new Uri("pack://application:,,,/" + RingIDSettings.APP_NAME + ";component" + location, UriKind.RelativeOrAbsolute);
                System.IO.Stream iconstream = GetCursorFromICO(iconUri, hotspotx, hotspoty);
                return new Cursor(iconstream);
            }
            catch (Exception ex)
            {
                log.Error("Error: GetCursor()." + ex.Message);
            }
            return null;
        }

        public static System.IO.Stream GetCursorFromICO(Uri uri, byte hotspotx, byte hotspoty)
        {
            MemoryStream ms = null;

            try
            {
                StreamResourceInfo sri = Application.GetResourceStream(uri);

                System.IO.Stream s = sri.Stream;

                byte[] buffer = new byte[s.Length];

                s.Read(buffer, 0, (int)s.Length);

                ms = new MemoryStream();

                buffer[2] = 2; // change to CUR file type
                buffer[10] = hotspotx;
                buffer[12] = hotspoty;

                ms.Write(buffer, 0, (int)s.Length);

                ms.Position = 0;
            }
            catch (Exception ex)
            {
                log.Error("Error: GetCursorFromICO()." + ex.Message);
            }

            return ms;
        }

        public static Uri GetUri(string location)
        {
            return new Uri("pack://application:,,,/" + RingIDSettings.APP_NAME + ";component" + location, UriKind.RelativeOrAbsolute);
        }

        public static BitmapImage GetBitmapImageOfDynamicResource(String location, bool NavigateFromProfileCoverPicture = false)//to avoid caching while editing Profile/Cover pic
        {
            if (!NavigateFromProfileCoverPicture)
            {
                BitmapImage image = null;
                if (File.Exists(location))
                {
                    try
                    {
                        image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad; // <-- This is the important one
                        image.UriSource = new Uri(location);
                        image.EndInit();
                        GC.SuppressFinalize(image);
                        return image;
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error: GetBitmapImageOfDynamicResource()." + ex.Message);
                        image = null;
                    }
                }
                return null;
            }
            else
            {
                if (File.Exists(location))
                {
                    using (var stream = new FileStream(location, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        try
                        {
                            var bitmapImage = new BitmapImage();
                            bitmapImage.BeginInit();
                            //bitmapImage.CreateOptions = BitmapCreateOptions.None;
                            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                            bitmapImage.StreamSource = stream;
                            bitmapImage.EndInit();
                            //bitmapImage.Freeze();
                            return bitmapImage;
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.StackTrace);
                        }
                    }
                }
                return null;
            }
        }
        public static double GetScaledImageHeight(List<ImageModel> _Images, long AddedTime, int PANEL_WIDTH)
        {
            double ImageViewHeight = 100;
            try
            {
                if (_Images.Count == 1)
                {
                    _Images[0].FeedImageInfoModel.Y = 0;
                    _Images[0].FeedImageInfoModel.X = 0;

                    _Images[0].FeedImageInfoModel.ReWidth = (_Images[0].FeedImageInfoModel.Width >= PANEL_WIDTH) ? PANEL_WIDTH : _Images[0].FeedImageInfoModel.Width;

                    // _Images[0].FeedImageInfoModel.ReHeight = (_Images[0].FeedImageInfoModel.Height >= PANEL_HEIGHT) ? ((_Images[0].FeedImageInfoModel.Height * _Images[0].FeedImageInfoModel.ReWidth) / _Images[0].FeedImageInfoModel.Width) : _Images[0].FeedImageInfoModel.Height;

                    _Images[0].FeedImageInfoModel.ReHeight = (int)(((double)_Images[0].FeedImageInfoModel.ReWidth / (double)_Images[0].FeedImageInfoModel.Width) * (double)_Images[0].FeedImageInfoModel.Height);

                    //_Images[0].FeedImageInfoModel.ReHeight = (_Images[0].FeedImageInfoModel.Height >= PANEL_HEIGHT) ? PANEL_HEIGHT : _Images[0].FeedImageInfoModel.Height;

                    if (_Images[0].FeedImageInfoModel.ReHeight > PANEL_WIDTH)
                    {
                        _Images[0].FeedImageInfoModel.ReHeight = PANEL_HEIGHT;
                        _Images[0].FeedImageInfoModel.ReWidth = (int)(((double)_Images[0].FeedImageInfoModel.ReHeight / (double)_Images[0].FeedImageInfoModel.Height) * (double)_Images[0].FeedImageInfoModel.Width);
                    }

                    ImageViewHeight = _Images[0].FeedImageInfoModel.ReHeight;
                }
                else if (_Images.Count == 2)
                {
                    _Images[0].FeedImageInfoModel.Y = 0;
                    _Images[0].FeedImageInfoModel.X = 0;

                    _Images[1].FeedImageInfoModel.Y = 0;
                    _Images[1].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                    _Images[1].FeedImageInfoModel.ReWidth = _Images[0].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);

                    _Images[1].FeedImageInfoModel.ReHeight = _Images[0].FeedImageInfoModel.ReHeight = (PANEL_WIDTH / 2);

                    ImageViewHeight = 300;
                }
                else if (_Images.Count == 3)
                {
                    int random = (int)AddedTime % 4;

                    if (random == 1) // top single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.X = 0;

                        _Images[0].FeedImageInfoModel.ReWidth = PANEL_WIDTH + SEPERATOR;
                        _Images[0].FeedImageInfoModel.ReHeight = PANEL_HEIGHT / 2;

                        _Images[1].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[1].FeedImageInfoModel.X = 0;

                        _Images[2].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[2].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.ReWidth = _Images[1].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[2].FeedImageInfoModel.ReHeight = _Images[1].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;

                    }
                    else if (random == 3) // bot single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.X = 0;

                        _Images[1].FeedImageInfoModel.Y = 0;
                        _Images[1].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[1].FeedImageInfoModel.ReWidth = _Images[0].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[1].FeedImageInfoModel.ReHeight = _Images[0].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        _Images[2].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[2].FeedImageInfoModel.X = 0;

                        _Images[2].FeedImageInfoModel.ReWidth = PANEL_WIDTH + SEPERATOR;
                        _Images[2].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;
                    }
                    else if (random == 0) // left single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.X = 0;

                        _Images[0].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[0].FeedImageInfoModel.ReHeight = PANEL_HEIGHT + SEPERATOR;

                        _Images[1].FeedImageInfoModel.Y = 0;
                        _Images[1].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[2].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.ReWidth = _Images[1].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[2].FeedImageInfoModel.ReHeight = _Images[1].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;
                    }
                    else if (random == 2) // right single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.X = 0;

                        _Images[1].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[1].FeedImageInfoModel.X = 0;

                        _Images[1].FeedImageInfoModel.ReWidth = _Images[0].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[1].FeedImageInfoModel.ReHeight = _Images[0].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        _Images[2].FeedImageInfoModel.Y = 0;
                        _Images[2].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[2].FeedImageInfoModel.ReHeight = PANEL_HEIGHT + SEPERATOR;

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return ImageViewHeight;
        }
        //public static Bitmap GetOrientedBitmapFromFilePath(string filePath)
        public static Bitmap GetBitmapOfDynamicResource(String location)
        {
            Bitmap mBitmap = null;
            try
            {
                if (File.Exists(location))
                {
                    using (System.IO.Stream bitmapStream = System.IO.File.Open(location, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        Image img = Image.FromStream(bitmapStream);
                        mBitmap = new Bitmap(img);
                        img.Dispose();
                        return mBitmap;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: GetBitmapOfDynamicResource()." + ex.Message + ex.StackTrace + location);
                if (mBitmap != null) mBitmap.Dispose();
            }
            return mBitmap;
        }

        public static BitmapImage GetDetaultProfileImage(MessageModel model, int width = 65, int height = 65, int fontSize = 25)
        {
            return GetDetaultProfileImage(model.SenderTableID, model.FullName, width, height, fontSize);
        }
        public static BitmapImage GetDetaultProfileImage(BaseUserProfileModel model, int width = 65, int height = 65, int fontSize = 25)
        {
            return GetDetaultProfileImage(model.UserTableID, model.FullName, width, height, fontSize);
        }
        public static BitmapImage GetDetaultProfileImage(long utId, string shortName, int width = 65, int height = 65, int fontSize = 25)
        {
            try
            {
                shortName = HelperMethods.GetShortName(shortName);
                if (String.IsNullOrWhiteSpace(shortName)) return ImageObjects.UNKNOWN_IMAGE_LARGE;
                else
                {
                    System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml("#ebeced");
                    if (utId > 0) color = RingIDSettings.COLORS[(int)(utId % 8)];
                    return getDetaultProfileImageWithName(shortName, color, width, height, fontSize);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: GetDetaultProfileImage()." + ex.Message);
            }
            return ImageObjects.UNKNOWN_IMAGE_LARGE;
        }

        public static BitmapImage GetDetaultProfileImage(string shortName, int width = 95, int height = 95, int fontSize = 35)
        {
            return GetDetaultProfileImage(0, shortName, width, height, fontSize);
        }

        public static void GetResizeImageParameters(double iw, double ih, double MAX_WIDTH, double MAX_HEIGHT, out double w, out double h)
        {
            w = 0; h = 0;
            if (iw > MAX_WIDTH || ih > MAX_HEIGHT)
            {
                double nW = 0;
                double nH = 0;

                if (((double)iw / (double)ih) > ((double)MAX_WIDTH / (double)MAX_HEIGHT))
                {
                    nW = MAX_WIDTH;
                    nH = (int)((ih * nW) / iw);
                }
                else
                {
                    nH = MAX_HEIGHT;
                    nW = (int)((iw * nH) / ih);
                }

                if (nW < MAX_WIDTH || nH < MAX_HEIGHT)
                {
                    if (nW < MAX_WIDTH)
                    {
                        nW = MAX_WIDTH;
                        nH = (int)(((double)MAX_WIDTH / (double)iw) * (double)ih);
                    }
                    if (nH < MAX_HEIGHT)
                    {
                        nH = MAX_HEIGHT;
                        nW = (int)(((double)MAX_HEIGHT / (double)ih) * (double)iw);
                    }
                }
                h = nH;
                w = nW;
            }
            else
            {
                w = MAX_WIDTH;
                h = MAX_HEIGHT;
            }
        }

        public static BitmapImage GetResizedImage(Bitmap bitmap, int MAX_WIDTH, int MAX_HEIGHT)
        {
            BitmapImage bitmapImage = null;

            try
            {
                if (bitmap != null)
                {
                    int oW = bitmap.Width;
                    int oH = bitmap.Height;

                    if (oW > MAX_WIDTH || oH > MAX_HEIGHT)
                    {
                        int nW = 0;
                        int nH = 0;

                        if (((double)oW / (double)oH) > ((double)MAX_WIDTH / (double)MAX_HEIGHT))
                        {
                            nW = MAX_WIDTH;
                            nH = ((oH * nW) / oW);
                        }
                        else
                        {
                            nH = MAX_HEIGHT;
                            nW = ((oW * nH) / oH);
                        }

                        bitmap = ImageUtility.ResizeBitmap(bitmap, nW, nH);
                        bitmapImage = ImageUtility.ConvertBitmapToBitmapImage(bitmap);
                    }
                    else
                    {
                        bitmapImage = ImageUtility.ConvertBitmapToBitmapImage(bitmap);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: GetResizedImage()." + ex.Message);
            }
            return bitmapImage;
        }

        public static BitmapImage GetResizedImageToFit(Bitmap bitmap, int WIDTH, int HEIGHT)
        {
            BitmapImage bitmapImage = null;

            try
            {
                if (bitmap != null)
                {
                    int oW = bitmap.Width;
                    int oH = bitmap.Height;

                    if (oW > WIDTH || oH > HEIGHT)
                    {
                        int nW = 0;
                        int nH = 0;

                        if (((double)oW / (double)oH) > ((double)WIDTH / (double)HEIGHT))
                        {
                            nH = HEIGHT;
                            nW = ((oW * nH) / oH);
                        }
                        else
                        {
                            nW = WIDTH;
                            nH = ((oH * nW) / oW);
                        }

                        bitmap = ImageUtility.ResizeBitmap(bitmap, nW, nH);
                        bitmapImage = ImageUtility.ConvertBitmapToBitmapImage(bitmap);
                    }
                    else bitmapImage = ImageUtility.ConvertBitmapToBitmapImage(bitmap);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: GetResizedImage()." + ex.Message);
            }
            return bitmapImage;
        }

        public static BitmapImage GetResizedOrCroppedCoverImage(Bitmap btmap, int cropX, int cropY)
        {
            BitmapImage bitmapImage = new BitmapImage();
            try
            {
                if (btmap.Width < RingIDSettings.COVER_PIC_DISPLAY_WIDTH || btmap.Height < RingIDSettings.COVER_PIC_DISPLAY_HEIGHT)
                {
                    int oWidth = btmap.Width;
                    int oHeight = btmap.Height;
                    int nWidth = 0;
                    int nHeight = 0;

                    int mode = 1;
                    if (btmap.Width < RingIDSettings.COVER_PIC_DISPLAY_WIDTH && btmap.Height < RingIDSettings.COVER_PIC_DISPLAY_HEIGHT)
                    {
                        if (((float)btmap.Width / (float)btmap.Height) < ((float)RingIDSettings.COVER_PIC_DISPLAY_WIDTH / (float)RingIDSettings.COVER_PIC_DISPLAY_HEIGHT))
                            mode = 1;
                        else mode = 2;
                    }
                    else if (btmap.Width < RingIDSettings.COVER_PIC_DISPLAY_WIDTH) mode = 1;
                    else if (btmap.Height < RingIDSettings.COVER_PIC_DISPLAY_HEIGHT) mode = 2;

                    if (mode == 1)
                    {
                        nWidth = RingIDSettings.COVER_PIC_DISPLAY_WIDTH;
                        nHeight = (btmap.Height * nWidth) / btmap.Width;
                    }
                    else
                    {
                        nHeight = RingIDSettings.COVER_PIC_DISPLAY_HEIGHT;
                        nWidth = (btmap.Width * nHeight) / btmap.Height;
                    }

                    btmap = ImageUtility.ResizeBitmap(btmap, nWidth, nHeight);
                    cropX = (int)(cropX * ((float)nWidth / (float)oWidth));
                    cropY = (int)(cropY * ((float)nHeight / (float)oHeight));
                }

                int overflowX = (btmap.Width - cropX);
                if (overflowX < RingIDSettings.COVER_PIC_DISPLAY_WIDTH) cropX = cropX - (RingIDSettings.COVER_PIC_DISPLAY_WIDTH - overflowX);
                int overflowY = (btmap.Height - cropY);
                if (overflowY < RingIDSettings.COVER_PIC_DISPLAY_HEIGHT) cropY = cropY - (RingIDSettings.COVER_PIC_DISPLAY_HEIGHT - overflowY);
                if (btmap.Width > RingIDSettings.COVER_PIC_DISPLAY_WIDTH || btmap.Height > RingIDSettings.COVER_PIC_DISPLAY_HEIGHT)
                    btmap = ImageUtility.CropBitmap(btmap, cropX, cropY, RingIDSettings.COVER_PIC_DISPLAY_WIDTH, RingIDSettings.COVER_PIC_DISPLAY_HEIGHT);
                bitmapImage = ImageUtility.ConvertBitmapToBitmapImage(btmap);
            }
            catch (Exception ex)
            {
                log.Error("Error: GetResizedOrCroppedCoverImage()." + ex.Message);
                bitmapImage = null;
            }
            return bitmapImage;
        }

        public static Bitmap CropBitmap(Bitmap imgToCrop, int cropX, int cropY, int width, int height)
        {
            Bitmap temp = null;
            System.Drawing.Imaging.PixelFormat fromat = imgToCrop.PixelFormat;
            try
            {
                temp = imgToCrop.Clone(
                    new System.Drawing.Rectangle(
                        cropX, cropY,
                        width,
                        height),
                        fromat);
                imgToCrop.Dispose();
                imgToCrop = temp;
            }
            catch (Exception ex)
            {
                Console.WriteLine("cropX --> " + cropX + "cropY --> " + cropY + "width -->" + width + "height --> " + height);
                log.Error("Error: CropBitmap()." + ex.Message);
                //b = imgToCrop;
            }
            return imgToCrop;
        }

        public static Bitmap ResizeBitmap(Bitmap imgToResize, int width, int height)
        {
            Bitmap b = new Bitmap(width, height);
            try
            {
                using (Graphics g = Graphics.FromImage((Image)b))
                {
                    g.CompositingMode = CompositingMode.SourceCopy;
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(imgToResize, 0, 0, width, height);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ResizeBitmap()." + ex.Message);
                b = imgToResize;
            }
            return b;
        }

        public static BitmapImage ConvertBitmapToBitmapImage(Bitmap bitmap)
        {
            BitmapImage bitmapImage = null;
            try
            {
                using (MemoryStream memory = new MemoryStream())
                {
                    bitmapImage = new BitmapImage();
                    bitmap.Save(memory, ImageFormat.Jpeg);
                    memory.Position = 0;
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();
                    //bitmapImage.Freeze();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ConvertBitmapToBitmapImage()." + ex.Message);
            }

            return bitmapImage;

        }

        public static BitmapImage ConvertBitmapToBitmapImageAsPNG(Bitmap bitmap)
        {
            BitmapImage bitmapImage = new BitmapImage();
            try
            {
                using (MemoryStream memory = new MemoryStream())
                {
                    bitmap.Save(memory, ImageFormat.Png);
                    memory.Position = 0;
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ConvertBitmapToBitmapImage()." + ex.Message);
            }
            return bitmapImage;
        }

        public static Bitmap ConvertBitmapImageToBitmap(BitmapImage bitmapImage)
        {
            try
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    BitmapEncoder enc = new BmpBitmapEncoder();
                    enc.Frames.Add(BitmapFrame.Create(bitmapImage, null, null, null));
                    enc.Save(outStream);
                    Bitmap bitmap = new Bitmap(outStream);
                    bitmap.MakeTransparent();
                    return new Bitmap(bitmap);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ConvertBitmapImageToBitmap()." + ex.Message);
            }
            return null;
        }

        /// <summary>
        /// 
        // Check that the remote file was found. The ContentType
        // check is performed since a request for a non-existent
        // image file might be redirected to a 404-page, which would
        // yield the StatusCode "OK", even though the image was not
        // found.
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="fileName"></param>
        public static bool DownloadRemoteImageFile(string uri, string fileName)
        {
            bool flag = false;
            HttpWebResponse response = null;
            try
            {
                FileInfo info = new FileInfo(fileName);
                if (File.Exists(fileName)) return true;
                if (!info.Directory.Exists)
                {
                    string dirPath = Path.GetDirectoryName(fileName);
                    Directory.CreateDirectory(dirPath);
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                response = (HttpWebResponse)request.GetResponse();

                if ((response.StatusCode == HttpStatusCode.OK ||
                    response.StatusCode == HttpStatusCode.Moved ||
                    response.StatusCode == HttpStatusCode.Redirect) &&
                    response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                {
                    using (System.IO.Stream responseStream = response.GetResponseStream())
                    {
                        ImageFormat format = ImageFormat.Jpeg;
                        if (info.Extension.ToUpper().Equals(".PNG")) format = ImageFormat.Png;
                        using (Bitmap bitMapImage = new Bitmap(responseStream))
                        {
                            if (!File.Exists(fileName))
                            {
                                //File.WriteAllText(fileName, "empty");
                                bitMapImage.Save(fileName, format);
                            }
                        }
                        flag = true;
                    }
                }
            }
            catch (Exception ex)
            {
#if IMAGE_LOG
                log.Error(ex.StackTrace + ex.Message + "\nFILETOSAVE=" + fileName + " FROM=" + uri);
#endif
            }
            if (response != null) response.Close();
            return flag;
        }

        public static bool DownloadImage(string image_url, string imageName, string baseUrl = null)
        {
            if (baseUrl == null && ServerAndPortSettings.GetImageServerResourceURL == null) return false;
            String image_url_with_base = (!String.IsNullOrWhiteSpace(baseUrl) ? baseUrl : ServerAndPortSettings.GetImageServerResourceURL) + image_url;
            return DownloadRemoteImageFile(image_url_with_base, imageName);
        }

        public static bool SaveImageLocallyFromURL(string image_url, string filePath)
        {
            return DownloadRemoteImageFile(image_url, filePath);
        }

        public static byte[] ReduceQualityAndSize(Bitmap originalImage, double width, int quality, int imageType)
        {
            try
            {
                double resizedWidth;
                double ratio;
                double resizedHeight;
                if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                {
                    if (originalImage.Width >= width && originalImage.Height >= width)
                    {
                        resizedWidth = width;
                        ratio = originalImage.Width / width;
                        resizedHeight = originalImage.Height / ratio;

                        if (resizedHeight < width)
                        {
                            resizedHeight = width;
                            ratio = originalImage.Height / width;
                            resizedWidth = originalImage.Width / ratio;
                        }
                    }
                    else
                    {
                        if (originalImage.Width == originalImage.Height)
                        {
                            resizedWidth = width;
                            resizedHeight = width;
                        }
                        else
                        {
                            if (originalImage.Width >= width)
                            {
                                resizedHeight = width;
                                ratio = originalImage.Height / width;
                                resizedWidth = originalImage.Width / ratio;
                            }
                            else if (originalImage.Height >= width)
                            {
                                resizedWidth = width;
                                ratio = originalImage.Width / width;
                                resizedHeight = originalImage.Height / ratio;
                            }
                            else
                            {
                                resizedWidth = width;
                                ratio = originalImage.Width / width;
                                resizedHeight = originalImage.Height / ratio;

                                if (resizedHeight < width)
                                {
                                    resizedHeight = width;
                                    ratio = originalImage.Height / width;
                                    resizedWidth = originalImage.Width / ratio;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (originalImage.Width >= originalImage.Height)
                    {
                        if (width == -1 || (width > originalImage.Width))
                        {
                            resizedWidth = originalImage.Width;
                            resizedHeight = originalImage.Height;
                        }
                        else
                        {
                            resizedWidth = width;
                            ratio = originalImage.Width / width;
                            resizedHeight = originalImage.Height / ratio;
                        }
                    }
                    else
                    {
                        if (width == -1 || (width > originalImage.Height))
                        {
                            resizedWidth = originalImage.Width;
                            resizedHeight = originalImage.Height;
                        }
                        else
                        {
                            resizedHeight = width;
                            ratio = originalImage.Height / width;
                            resizedWidth = originalImage.Width / ratio;

                            if (resizedWidth < width)//Width Priority
                            {
                                resizedWidth = width;
                                ratio = originalImage.Width / width;
                                resizedHeight = originalImage.Height / ratio;
                            }
                        }
                    }
                }
                using (Bitmap newImage = new Bitmap((int)resizedWidth, (int)resizedHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                {
                    using (Graphics graphics = Graphics.FromImage(newImage))
                    {
                        graphics.CompositingQuality = CompositingQuality.HighQuality;
                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        graphics.SmoothingMode = SmoothingMode.HighQuality;
                        graphics.DrawImage(originalImage, 0, 0, (int)resizedWidth, (int)resizedHeight);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            ImageCodecInfo imageCodecInfo = GetEncoderInfo(ImageFormat.Jpeg);
                            System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;
                            EncoderParameters encoderParameters = new EncoderParameters(1);
                            EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
                            encoderParameters.Param[0] = encoderParameter;
                            newImage.Save(ms, imageCodecInfo, encoderParameters);
                            return ms.ToArray();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if IMAGE_LOG
                log.Error("Error: ReduceQualityAndSize()." + ex.Message);
#endif
            }
            return null;
        }

        public static byte[] ReduceBitmapQuality(Bitmap originalImage, int quality)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ImageCodecInfo imageCodecInfo = GetEncoderInfo(ImageFormat.Jpeg);
                    System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;
                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
                    encoderParameters.Param[0] = encoderParameter;
                    originalImage.Save(ms, imageCodecInfo, encoderParameters);
                    return ms.ToArray();
                }
            }
            catch (Exception ex) { log.Error("Error: ReduceQuality()." + ex.Message); }
            return null;
        }

        public static byte[] ReduceImageQuality(Image originalImage, int quality)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ImageCodecInfo imageCodecInfo = GetEncoderInfo(ImageFormat.Jpeg);
                    System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;
                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    EncoderParameter encoderParameter = new EncoderParameter(encoder, quality);
                    encoderParameters.Param[0] = encoderParameter;
                    originalImage.Save(ms, imageCodecInfo, encoderParameters);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
#if IMAGE_LOG
                log.Error("Error: ImageReduceQuality()." + ex.Message);
#endif
            }
            return null;
        }

        public static RotateFlipType GetOrientationToFlipType(int orientationValue)
        {
            RotateFlipType rotateFlipType = RotateFlipType.RotateNoneFlipNone;

            switch (orientationValue)
            {
                case 1:
                    rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    break;
                case 2:
                    rotateFlipType = RotateFlipType.RotateNoneFlipX;
                    break;
                case 3:
                    rotateFlipType = RotateFlipType.Rotate180FlipNone;
                    break;
                case 4:
                    rotateFlipType = RotateFlipType.Rotate180FlipX;
                    break;
                case 5:
                    rotateFlipType = RotateFlipType.Rotate90FlipX;
                    break;
                case 6:
                    rotateFlipType = RotateFlipType.Rotate90FlipNone;
                    break;
                case 7:
                    rotateFlipType = RotateFlipType.Rotate270FlipX;
                    break;
                case 8:
                    rotateFlipType = RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                    rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    break;
            }
            return rotateFlipType;
        }

        public static Image GetOrientedImageFromFilePath(string filePath)
        {
            Image img = null;
            try
            {
                if (File.Exists(@filePath))
                {
                    img = System.Drawing.Image.FromFile(@filePath);
                    foreach (var prop in img.PropertyItems)
                    {
                        if (prop.Id == 0x0112) //value of EXIF
                        {
                            int orientationValue = img.GetPropertyItem(prop.Id).Value[0];
                            RotateFlipType rotateFlipType = GetOrientationToFlipType(orientationValue);
                            if (rotateFlipType != RotateFlipType.RotateNoneFlipNone)
                                img.RotateFlip(rotateFlipType);
                            return img;
                        }
                    }
                }
            }
            catch (Exception e)
            {
#if IMAGE_LOG
                log.Error("Error: FeedImageResizeandQuality().FilePath = " + filePath + ", " + e.Message + "\n" + e.StackTrace);
#endif
            }
            return img;
        }

        public static byte[] ImageResizeandQuality(Image image, int quality, out int w, out int h)
        {
            byte[] byteArray = null;
            w = 0;
            h = 0;
            try
            {
                if (image != null)
                {
                    int targetW = 0;
                    int targetH = 0;
                    double ratio = (double)image.Width / (double)image.Height;

                    if (ratio >= 1)
                    {
                        targetW = HORIZONTAL_DIMENSION[0];
                        targetH = HORIZONTAL_DIMENSION[1];
                    }
                    else if (ratio < 1)
                    {
                        targetW = VERTICAL_DIMENSION[0];
                        targetH = VERTICAL_DIMENSION[1];
                    }

                    if (image.Width > targetW || image.Height > targetH)
                    {
                        int newW = 0;
                        int newH = 0;
                        int oldW = image.Width;
                        int oldH = image.Height;

                        if (oldW > targetW && oldH > targetH)
                        {
                            double diffW = (double)((oldW - targetW) * 100) / oldW;
                            double diffH = (double)((oldH - targetH) * 100) / oldH;
                            if (diffW > diffH)
                            {
                                newW = targetW;
                                newH = (oldH * newW) / oldW;
                            }
                            else
                            {
                                newH = targetH;
                                newW = (oldW * newH) / oldH;
                            }
                        }
                        else if (oldW > targetW)
                        {
                            newW = targetW;
                            newH = (oldH * newW) / oldW;
                        }
                        else if (oldH > targetH)
                        {
                            newH = targetH;
                            newW = (oldW * newH) / oldH;
                        }
                        //
                        Image newImage = new Bitmap(newW, newH);
                        using (var g = Graphics.FromImage(newImage))
                        {
                            g.CompositingMode = CompositingMode.SourceCopy;
                            g.CompositingQuality = CompositingQuality.HighQuality;
                            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            g.SmoothingMode = SmoothingMode.HighQuality;
                            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            g.DrawImage(image, 0, 0, newW, newH);
                        }
                        w = newW;
                        h = newH;
                        byteArray = ReduceImageQuality(newImage, quality);
                    }
                    else
                    {                    //
                        w = image.Width;
                        h = image.Height;
                        byteArray = ReduceImageQuality(image, quality);
                    }
                }
            }
            catch (Exception ex)
            {
#if IMAGE_LOG
                log.Error("Error: ImageResizeandQuality()." + ex.Message);
#endif
            }
            return byteArray;
        }

        public static void ImageHeightWidth(Image image, out int w, out int h)
        {
            w = 0;
            h = 0;
            try
            {
                if (image != null)
                {
                    int targetW = 0;
                    int targetH = 0;
                    double ratio = (double)image.Width / (double)image.Height;

                    if (ratio >= 1)
                    {
                        targetW = HORIZONTAL_DIMENSION[0];
                        targetH = HORIZONTAL_DIMENSION[1];
                    }
                    else if (ratio < 1)
                    {
                        targetW = VERTICAL_DIMENSION[0];
                        targetH = VERTICAL_DIMENSION[1];
                    }

                    if (image.Width > targetW || image.Height > targetH)
                    {
                        int newW = 0;
                        int newH = 0;
                        int oldW = image.Width;
                        int oldH = image.Height;

                        if (oldW > targetW && oldH > targetH)
                        {
                            double diffW = (double)((oldW - targetW) * 100) / oldW;
                            double diffH = (double)((oldH - targetH) * 100) / oldH;
                            if (diffW > diffH)
                            {
                                newW = targetW;
                                newH = (oldH * newW) / oldW;
                            }
                            else
                            {
                                newH = targetH;
                                newW = (oldW * newH) / oldH;
                            }
                        }
                        else if (oldW > targetW)
                        {
                            newW = targetW;
                            newH = (oldH * newW) / oldW;
                        }
                        else if (oldH > targetH)
                        {
                            newH = targetH;
                            newW = (oldW * newH) / oldH;
                        }
                        //
                        w = newW;
                        h = newH;
                    }
                    else
                    {                    //
                        w = image.Width;
                        h = image.Height;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ImageHeightWidth()." + ex.Message);
            }
        }

        public static BitmapImage ConvertByteArrayToBitmapImage(byte[] byteArray)
        {
            using (var ms = new System.IO.MemoryStream(byteArray))
            {
                var image = new BitmapImage();
                image.BeginInit();
                //image.CreateOptions = BitmapCreateOptions.None;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.StreamSource = ms;
                image.EndInit();
                image.Freeze();
                return image;
            }
        }

        public static byte[] ConvertBitmapToByteArray(Bitmap bitmap)
        {
            try
            {
                using (var ms = new System.IO.MemoryStream())
                {
                    bitmap.Save(ms, ImageFormat.Jpeg);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ConvertBitmapToByteArray()." + ex.Message);
            }
            return null;
        }

        public static Bitmap ConvertByteArrayToBitmap(byte[] byteArray)
        {
            try
            {
                using (var ms = new MemoryStream(byteArray)) return new Bitmap(ms);
            }
            catch (Exception ex)
            {
                log.Error("Error: ConvertByteArrayToBitmap()." + ex.Message);
            }
            return null;
        }

        public static void GetImageDimension(string fileName, out int w, out int h)
        {
            w = 0;
            h = 0;
            try
            {
                using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (Image tif = Image.FromStream(stream: file, useEmbeddedColorManagement: false, validateImageData: false))
                    {
                        w = (int)tif.PhysicalDimension.Width;
                        h = (int)tif.PhysicalDimension.Height;
                    }
                }
            }
            catch (Exception ex) { log.Error("Error: GetImageDimension()." + ex.Message); }
        }

        public static int GetFileImageTypeFromHeader(string file, byte[] allBytes = null)
        {
            try
            {
                byte[] headerBytes;
                if (allBytes == null)
                {
                    using (FileStream fileStream = new FileStream(file, FileMode.Open))
                    {
                        const int mostBytesNeeded = 11;//For JPEG
                        if (fileStream.Length < mostBytesNeeded)
                            return 0;
                        headerBytes = new byte[mostBytesNeeded];
                        fileStream.Read(headerBytes, 0, mostBytesNeeded);
                    }
                }
                else
                {
                    const int mostBytesNeeded = 11;//For JPEG
                    if (allBytes.Length < mostBytesNeeded)
                        return 0;
                    headerBytes = new byte[mostBytesNeeded];
                    Buffer.BlockCopy(allBytes, 0, headerBytes, 0, mostBytesNeeded);
                }

                if (HasJpegHeader(file))
                {
                    return 1;
                }

                //PNG 
                if (headerBytes[0] == 0x89 && //89 50 4E 47 0D 0A 1A 0A
                    headerBytes[1] == 0x50 &&
                    headerBytes[2] == 0x4E &&
                    headerBytes[3] == 0x47 &&
                    headerBytes[4] == 0x0D &&
                    headerBytes[5] == 0x0A &&
                    headerBytes[6] == 0x1A &&
                    headerBytes[7] == 0x0A)
                {
                    return 2;
                }
            }
            catch (Exception ex) { log.Error("Error: GetFileImageTypeFromHeader()." + ex.Message); }
            return 0;
        }

        static bool HasJpegHeader(string filename)
        {
            using (BinaryReader br = new BinaryReader(File.Open(filename, FileMode.Open)))
            {
                UInt16 soi = br.ReadUInt16();  // Start of Image (SOI) marker (FFD8)
                UInt16 marker = br.ReadUInt16(); // JFIF marker (FFE0) or EXIF marker(FF01)

                return soi == 0xd8ff && (marker & 0xe0ff) == 0xe0ff;
            }
        }

        public static byte[] GetByteArrayFromFilePath(string filePathImage)
        {
            byte[] oFileBytes = null;
            using (System.IO.FileStream fs = System.IO.File.Open(filePathImage, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite))
            {
                int numBytesToRead = System.Convert.ToInt32(fs.Length);
                oFileBytes = new byte[(numBytesToRead)];
                fs.Read(oFileBytes, 0, numBytesToRead);
            }
            return oFileBytes;
        }

        public static void AddProfileCoverImageToUIList(ObservableCollection<ImageModel> imageModelList, int imageType, long friendId = 0)
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                try
                {
                    if (imageModelList.Count > 0)
                    {
                        if (friendId > 0)
                        {

                        }
                        else
                        {
                            if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                            {
                                ImageModel imgModel = imageModelList.ElementAt(0);
                                AlbumModel model = RingIDViewModel.Instance.MyImageAlubms.Where(P => P.AlbumName == DefaultSettings.PROFILE_IMAGE_ALBUM_NAME).FirstOrDefault();
                                if (model != null)
                                {
                                    lock (model.ImageList)
                                    {
                                        if (!model.ImageList.Any(P => P.ImageId == imgModel.ImageId))
                                        {
                                            model.TotalImagesInAlbum++;
                                            model.ImageList.Insert(0, imgModel);
                                        }
                                    }
                                }
                            }
                            else if (imageType == SettingsConstants.TYPE_COVER_IMAGE)
                            {
                                ImageModel imgModel = imageModelList.ElementAt(0);
                                AlbumModel model = RingIDViewModel.Instance.MyImageAlubms.Where(P => P.AlbumName == DefaultSettings.COVER_IMAGE_ALBUM_NAME).FirstOrDefault();
                                if (model != null)
                                {
                                    lock (model.ImageList)
                                    {
                                        if (!model.ImageList.Any(P => P.ImageId == imgModel.ImageId))
                                        {
                                            model.TotalImagesInAlbum++;
                                            model.ImageList.Insert(0, imgModel);
                                        }
                                    }
                                }
                            }
                            else if (imageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)//to do
                            {
                                lock (RingIDViewModel.Instance.FeedImageList)
                                {
                                    foreach (ImageModel model in imageModelList)
                                    {
                                        if (!RingIDViewModel.Instance.FeedImageList.Any(P => P.ImageId == model.ImageId)) RingIDViewModel.Instance.FeedImageList.Insert(0, model);
                                        RingIDViewModel.Instance.FeedImageCount++;
                                    }
                                }
                                RingIDViewModel.Instance.OnPropertyChanged("FeedImageList");
                            }
                        }
                    }
                }
                catch (Exception ex) { log.Error("Error: AddProfileCoverImageToUIList() ==> " + ex.Message + "\n" + ex.StackTrace); }
            });
        }

        public static void DeleteImageFromLocalDirectory(string imageName)
        {
            try
            {
                if (File.Exists(imageName)) File.Delete(imageName);
            }
            catch (Exception ex) { log.Error("Error: DeleteImageFromLocalDirectory() => " + ex.Message + "\n" + ex.StackTrace); }
        }

        public static ImageSource GetImageSoruceFromBasicInfo(UserBasicInfoModel userinfo, int image_type, int width, int height)
        {
            String url = ServerAndPortSettings.GetImageServerResourceURL + userinfo.ShortInfoModel.ProfileImage;
            Bitmap bitmapImage = null;
            if (!String.IsNullOrEmpty(userinfo.ShortInfoModel.ProfileImage) && url.Contains("/"))
            {
                string convertedUrl = HelperMethods.ConvertUrlToName(userinfo.ShortInfoModel.ProfileImage, image_type);
                string dir = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                if (!File.Exists(dir))
                {
                    try
                    {
                        if (DefaultSettings.IsInternetAvailable)
                        {
                            Uri uriAddress = new Uri(url, UriKind.RelativeOrAbsolute);
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriAddress);
                            request.Headers.Add("x-app-version", AppConfig.VERSION_PC);
                            request.Proxy = null;
                            HttpWebResponse response = null;
                            String contentType = String.Empty;
                            try
                            {
                                response = (HttpWebResponse)request.GetResponse();
                                if ((response.StatusCode == HttpStatusCode.OK ||
                                    response.StatusCode == HttpStatusCode.Moved ||
                                    response.StatusCode == HttpStatusCode.Redirect) &&
                                    response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                                {
                                    System.Net.WebRequest request2 = System.Net.WebRequest.Create(uriAddress);
                                    System.Net.WebResponse response2 = request.GetResponse();
                                    System.IO.Stream responseStream = response.GetResponseStream();
                                    bitmapImage = new Bitmap(responseStream);
                                    bitmapImage.Save(dir, System.Drawing.Imaging.ImageFormat.Jpeg);
                                }
                            }
                            catch (Exception ex)
                            {
                                if (DefaultSettings.DEBUG) log.Error("Exception in getting response  => " + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message + "==>" + convertedUrl);
                            }
                            finally
                            {
                                if (response != null)
                                {
                                    response.Close();
                                    response = null;
                                }
                                request.Abort();
                                request = null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (DefaultSettings.DEBUG) log.Error("DownloadfullImage => " + ex.Message + "\n" + ex.StackTrace);
                    }
                }
                else
                {
                    using (System.IO.Stream BitmapStream = File.Open(dir, System.IO.FileMode.Open,FileAccess.ReadWrite))
                    {
                        Image img = Image.FromStream(BitmapStream);
                        bitmapImage = new Bitmap(img);
                    }
                }
                return ImageUtility.ConvertBitmapToBitmapImage(bitmapImage);
            }
            return null;
        }
        #endregion "Public methods"

        #region Private methods

        [DllImport("gdi32")]
        static extern int DeleteObject(IntPtr o);

        private static ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
        }

        private static BitmapImage getDetaultProfileImageWithName(string shortName, System.Drawing.Color colorPicker, int width, int height, int fontSize)
        {
            try
            {
                using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(width, height))
                {
                    for (int x = 0; x < bitmap.Height; ++x)
                        for (int y = 0; y < bitmap.Width; ++y)
                            bitmap.SetPixel(x, y, colorPicker);

                    Font font = new Font("Segoe UI", fontSize, System.Drawing.FontStyle.Regular, GraphicsUnit.Pixel);
                    System.Drawing.Point atpoint = new System.Drawing.Point(bitmap.Width / 2, bitmap.Height / 2);
                    SolidBrush brush = new SolidBrush(System.Drawing.Color.White);
                    StringFormat sf = new StringFormat();
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.DrawString(shortName, font, brush, atpoint, sf);
                        graphics.Dispose();
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            BitmapImage image = new BitmapImage();
                            image.BeginInit();
                            image.CacheOption = BitmapCacheOption.OnLoad;
                            ms.Seek(0, SeekOrigin.Begin);
                            image.StreamSource = ms;
                            image.EndInit();
                            return image;
                        }
                    }
                }
            }
            catch (Exception ex) { log.Error("Error: GetDetaultProfileImage()." + ex.Message); }
            return ImageObjects.UNKNOWN_IMAGE_LARGE;
        }

        private static int[] VERTICAL_DIMENSION = { 720, 1480 };

        private static int[] HORIZONTAL_DIMENSION = { 1480, 720 };

        #endregion Private methods

        #region "Full Image View Methods"

        public static void OnFeedImageLeftClick(bool notfound, Guid ClickedImgId, Guid ClickedNfId)
        {
            try
            {
                if (!notfound)
                {
                    FeedModel fm = null;
                    if (ClickedNfId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(ClickedNfId, out fm) && fm.ImageList != null && fm.ImageList.Count > 0)
                    {

                        ObservableCollection<ImageModel> imgModels = fm.ImageList;
                        int selectedIndex = 0;
                        foreach (ImageModel imgModel in imgModels)
                        {
                            if (imgModel.ImageId == ClickedImgId)
                            {
                                break;
                            }
                            selectedIndex++;
                        }
                        if (motherGrid == null && UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        {
                            motherGrid = UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid;
                        }
                        ShowImageViewer(imgModels, selectedIndex, fm.TotalImage, StatusConstants.NAVIGATE_FROM_FEED, Guid.Empty, motherGrid);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnFeedImageLeftClick()." + ex.Message + ex.StackTrace);
            }
        }
        //public static void OnFeedImageLeftClick(object parameter, bool notfound, Guid ClickedImageid, System.Windows.Controls.Grid motherGrid = null)
        //{
        //    try
        //    {
        //        if (!notfound)
        //        {
        //            if (parameter is ObservableCollection<ImageModel>)
        //            {
        //                ObservableCollection<ImageModel> imgModels = (ObservableCollection<ImageModel>)parameter;
        //                Guid newsfeedid = imgModels.ElementAt(0).NewsFeedId;
        //                int selectedIndex = 0;
        //                foreach (ImageModel feedImaginfo in imgModels)
        //                {
        //                    if (feedImaginfo.ImageId == ClickedImageid)
        //                    {
        //                        break;
        //                    }
        //                    selectedIndex++;
        //                }
        //                if (motherGrid == null && UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
        //                {
        //                    motherGrid = UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid;
        //                }
        //                FeedModel fm = null;
        //                if (FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedid, out fm))
        //                    ShowImageViewer(imgModels, selectedIndex, fm.TotalImage, StatusConstants.NAVIGATE_FROM_FEED, Guid.Empty, motherGrid);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: OnFeedImageLeftClick()." + ex.Message + ex.StackTrace);
        //    }
        //}

        public static void ShowImageViewer(ObservableCollection<ImageModel> ImageObservableList,
            int selectedIndex,
            int TotalImages,
            int NavigatedFrom,
            Guid CmntID,
            System.Windows.Controls.Grid ParentGrid = null,
            bool needToShowLikeComments = true)
        {
            ImageModel selectedModel = ImageObservableList.ElementAt(selectedIndex);
            UCImageViewInMain imageViewInMain = new UCImageViewInMain();
            imageViewInMain.NavigateFrom = NavigatedFrom;
            ViewConstants.NavigateFrom = NavigatedFrom;
            // imageViewInMain.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, "#FF000000", 1.0);//background Color Black
            if (ParentGrid != null)
            {
                motherGrid = ParentGrid;
                imageViewInMain.SetParentGrid(motherGrid);
            }
            else if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
            {
                motherGrid = UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid;
                imageViewInMain.SetParentGrid(motherGrid);
            }
            else imageViewInMain.SetParentGrid(UCGuiRingID.Instance.MotherPanel);

            imageViewInMain.DataModel.NeedToShowLikeComment = needToShowLikeComments;
            imageViewInMain.ChangeSelectedModel(selectedIndex, TotalImages, selectedModel);
            imageViewInMain.Show();
            imageViewInMain.OnRemovedUserControl += () =>
            {
                if (motherGrid != null && !(motherGrid == UCGuiRingID.Instance.MotherPanel))
                {
                    var obj = ((System.Windows.Controls.Border)(motherGrid.Parent)).Parent;
                    //Hide();
                    if (obj != null && obj is UCImageViewInMain)
                    {
                        UCImageViewInMain imageViewInMain1 = (UCImageViewInMain)obj;
                        imageViewInMain1.GrabKeyboardFocus();
                    }
                    else if (obj != null && obj is UCMediaPlayerInMain)
                    {
                        UCMediaPlayerInMain imageViewInMain1 = (UCMediaPlayerInMain)obj;
                        imageViewInMain1.GrabKeyBoardFocus();
                    }
                    else if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                }
                else if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();

                ViewConstants.ImageID = Guid.Empty;
                ViewConstants.AlbumID = Guid.Empty;
                ViewConstants.NavigateFrom = 0;
                imageViewInMain = null;
                motherGrid = null;
            };
            imageViewInMain.ImageModels = ImageObservableList;
        }
        public static void ShowImageViewerFromPortalSlider(ObservableCollection<ImageModel> ImageObservableList,
            int selectedIndex,
            int TotalImages,
            int NavigatedFrom,
            Guid CmntID,
            System.Windows.Controls.Grid ParentGrid = null,
            bool needToShowLikeComments = true)
        {
            ImageModel selectedModel = ImageObservableList.ElementAt(selectedIndex);
            UCImageViewInMain imageViewInMain = new UCImageViewInMain();
            imageViewInMain.NavigateFrom = NavigatedFrom;
            ViewConstants.NavigateFrom = NavigatedFrom;
            // imageViewInMain.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, "#FF000000", 1.0);//background Color Black
            imageViewInMain.SetParentGrid(ParentGrid);
            motherGrid = ParentGrid;
            imageViewInMain.DataModel.NeedToShowLikeComment = needToShowLikeComments;
            imageViewInMain.ChangeSelectedModel(selectedIndex, TotalImages, selectedModel);
            imageViewInMain.Show();
            imageViewInMain.OnRemovedUserControl += () =>
            {
                ViewConstants.ImageID = Guid.Empty;
                ViewConstants.AlbumID = Guid.Empty;
                ViewConstants.NavigateFrom = 0;
                imageViewInMain = null;
                if (ucSingleFeedDetailsView != null)
                {
                    ucSingleFeedDetailsView.GrabKeyboardFocus();
                }
            };
            imageViewInMain.ImageModels = ImageObservableList;
        }
        public static void ShowImageViewFromCollections(long clickedUserid, ObservableCollection<ImageModel> profileImageList2, Guid clickedImageID, int selectedIndex, int totalImageCount)
        {
            try
            {
                ShowImageViewer(profileImageList2, selectedIndex, totalImageCount, StatusConstants.NAVIGATE_FROM_PROFILE, Guid.Empty);
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowImageViewFromCollections()." + ex.Message);
            }
        }

        public static void ShowImageViewFromPortalPages(ObservableCollection<ImageModel> portalPagesImageList, Guid clickedImageID, bool isfromPortal, System.Windows.Controls.Grid motherGrid = null)
        {
            try
            {
                if (motherGrid == null && UCSingleFeedDetailsView.Instance != null)
                {
                    motherGrid = UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid;
                }
                int selectedIndex = 0;
                if (portalPagesImageList.Count > 0)
                {
                    foreach (ImageModel feedImaginfo in portalPagesImageList)
                    {
                        if (feedImaginfo.ImageId == clickedImageID) break;
                        selectedIndex++;
                    }
                }
                ShowImageViewerFromPortalSlider(portalPagesImageList, selectedIndex, portalPagesImageList.Count, StatusConstants.NAVIGATE_FROM_NEWSPORTAL, Guid.Empty, motherGrid, false);
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowImageViewFromPortalPages()." + ex.Message);
            }
        }

        public static void OnCoverImageClicked(object parameter)
        {
            try
            {
                if (parameter is UserBasicInfoModel)
                {
                    UserBasicInfoModel model = (UserBasicInfoModel)parameter;
                    int totalImage = 0;
                    if (model.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID || (model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && model.CoverImagePrivacy != 3))
                    {
                        if (model.CoverImage != null && model.CoverImage.Length > 3)
                        {
                            ObservableCollection<ImageModel> ImageObservableList = null;
                            UCFriendProfile ucFriendProfile = null;
                            long userid = model.ShortInfoModel.UserIdentity;
                            if (model.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                            {
                                userid = DefaultSettings.LOGIN_RING_ID;
                                ImageObservableList = RingIDViewModel.Instance.CoverImageList;
                                totalImage = RingIDViewModel.Instance.CoverImageCount;
                            }
                            else
                            {
                                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == model.ShortInfoModel.UserTableID)
                                {
                                    ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                                    ImageObservableList = ucFriendProfile.FriendCoverImageList;
                                    if (ucFriendProfile._UCFriendPhotos != null) totalImage = ucFriendProfile._UCFriendPhotos.FriendCoverImageCount;
                                }
                            }
                            if (ImageObservableList != null && ImageObservableList.Count > 1)
                            {
                                int selectedIndex = 0;
                                foreach (ImageModel ImgInfo in ImageObservableList)
                                {
                                    if (ImgInfo.ImageId == model.CoverImageId)
                                        break;
                                    selectedIndex++;
                                }
                                ImageUtility.ShowImageViewFromCollections(userid, ImageObservableList, model.CoverImageId, selectedIndex, totalImage);
                            }
                            else
                            {
                                ImageModel singImageMOdel = ImageDataContainer.Instance.GetFromImageModelDictionaryNotNullable(model.CoverImageId);
                                singImageMOdel.ImageId = model.CoverImageId;
                                singImageMOdel.ImageType = SettingsConstants.TYPE_COVER_IMAGE;
                                singImageMOdel.ImageUrl = model.CoverImage;
                                singImageMOdel.UserShortInfoModel = model.ShortInfoModel;

                                ObservableCollection<ImageModel> ImageObservableList2 = new ObservableCollection<ImageModel>();
                                ImageObservableList2.Add(singImageMOdel);
                                ImageObservableList2.Add(new ImageModel() { IsLoadMore = true });//Extra See More Model Add
                                ImageUtility.ShowImageViewer(ImageObservableList2, 0, 1, StatusConstants.NAVIGATE_FROM_PROFILE, Guid.Empty);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCoverImageClicked()." + ex.Message);
            }
        }

        public static void OnProfileImageClickedActions(object parameter)
        {
            try
            {
                if (parameter is UserBasicInfoModel)
                {
                    UCFriendProfile ucFriendProfile = null;
                    UserBasicInfoModel model = (UserBasicInfoModel)parameter;
                    int totalImage = 0;
                    if (model.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID || (model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && model.ProfileImagePrivacy != 3)) //only me
                    {
                        if (model.ShortInfoModel.ProfileImage != null && model.ShortInfoModel.ProfileImage.Length > 3)
                        {
                            long userid = model.ShortInfoModel.UserIdentity;
                            ObservableCollection<ImageModel> ImageObservableList = null;
                            if (model.ShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                            {
                                userid = DefaultSettings.LOGIN_RING_ID;
                                ImageObservableList = RingIDViewModel.Instance.ProfileImageList;
                                totalImage = RingIDViewModel.Instance.ProfileImageCount;
                            }
                            else
                            {
                                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == model.ShortInfoModel.UserTableID)
                                {
                                    ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                                    ImageObservableList = ucFriendProfile.FriendProfileImageList;
                                    if (ucFriendProfile._UCFriendPhotos != null) totalImage = ucFriendProfile._UCFriendPhotos.FriendProfileImageCount;
                                }
                            }
                            if (ImageObservableList != null && ImageObservableList.Count > 1)
                            {
                                int selectedIndex = 0;
                                foreach (ImageModel ImgInfo in ImageObservableList)
                                {
                                    if (ImgInfo.ImageId == model.ProfileImageId) break;
                                    selectedIndex++;
                                }
                                ImageUtility.ShowImageViewFromCollections(userid, ImageObservableList, model.ProfileImageId, selectedIndex, totalImage);
                            }
                            else
                            {
                                ImageModel singImageMOdel = ImageDataContainer.Instance.GetFromImageModelDictionaryNotNullable(model.ProfileImageId);
                                singImageMOdel.ImageId = model.ProfileImageId;
                                singImageMOdel.ImageType = SettingsConstants.TYPE_PROFILE_IMAGE;
                                singImageMOdel.ImageUrl = model.ShortInfoModel.ProfileImage;
                                singImageMOdel.UserShortInfoModel = model.ShortInfoModel;
                                ObservableCollection<ImageModel> ImageObservableList2 = new ObservableCollection<ImageModel>();
                                ImageObservableList2.Add(singImageMOdel);
                                ImageObservableList2.Add(new ImageModel() { IsLoadMore = true });//Extra See More Model Add

                                ShowImageViewer(ImageObservableList2, 0, 1, StatusConstants.NAVIGATE_FROM_PROFILE, Guid.Empty);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnProfileImageClickedActions()." + ex.Message + ex.StackTrace);
            }
        }

        public static void ShowFullImageFromComments(string imageUrl, System.Windows.Controls.Grid motherGrid = null)
        {
            ImageModel singleImageModel = new ImageModel();
            singleImageModel.ImageUrl = imageUrl;
            singleImageModel.ImageType = SettingsConstants.TYPE_IMAGE_IN_COMMENT;
            ObservableCollection<ImageModel> ImageObservableList2 = new ObservableCollection<ImageModel>();
            ImageObservableList2.Add(singleImageModel);

            if (motherGrid == null && UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
            {
                motherGrid = UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid;
            }
            ShowImageViewer(ImageObservableList2, 0, 1, StatusConstants.NAVIGATE_FROM_COMMENT, Guid.Empty, motherGrid, false);
        }

        #endregion "Full Image View Methods"
    }
}

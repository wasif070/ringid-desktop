﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Auth.utility;
using callsdkwrapper;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using View.BindingModels;
using View.Constants;
using View.UI.Call;
using View.Utility.Call.SDKWrapper;
using View.Utility.Call.Settings;
using View.Utility.Call.Video;
using View.Utility.Call.ViewModels;
using View.Utility.Naudio;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility.Call
{
    public class CallController
    {
        #region "Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(CallController).Name);
        #endregion " Fields"

        #region "Properties"

        public CallSDKCallBackHandler CallSDKCallBack { get; set; }

        public BaseCallService BaseCallService { get; set; }

        public CallerDTO CallData { get; set; }

        public VMCallInfo ViewModelCallInfo { get; set; }

        public AudioPlayerRecorder PCMDataPlayerAndRecorder { get; set; }

        public AforgeWebCamCapture WebCamController { get; set; }

        public BWCallManger CallManger { get; set; }

        public UCCallUiInMainWindow CallUiInMainWindow { get; set; }

        //public WNCallUiInFullScreen CallUiInFullScreen { get; set; }

        public WNCallUiInSmallScreen CallUiInSmallScreen { get; set; }

        public bool IsCallEnded { get; set; }

        #endregion "Properties"
        #region "Public Methods"

        public void InitCallSDK()
        {
            if (BaseCallService == null)
                BaseCallService = new BaseCallService(DefaultSettings.LOGIN_TABLE_ID);
            if (CallSDKCallBack == null)
                CallSDKCallBack = new CallSDKCallBackHandler(this);
            BaseCallService.registerEventHandler(CallSDKCallBack);
            log.Info("end InitCallSDK==>" + DefaultSettings.LOGIN_TABLE_ID);
        }

        public void InitViewModel()
        {
            if (ViewModelCallInfo == null)
            {
                ViewModelCallInfo = new VMCallInfo();
                UserBasicInfoModel model = new UserBasicInfoModel();
                model.ShortInfoModel.FullName = "Faiz 0375";
                model.ShortInfoModel.UserTableID = 301;
                model.ShortInfoModel.ProfileImage = "cloud/uploaded-139/2110010375/5558601456261367909.jpg";
                ViewModelCallInfo.FriendProfileSourceMini = ImageUtility.GetDetaultProfileImage(model.ShortInfoModel);
                ViewModelCallInfo.FriendProfileSource = ImageUtility.GetDetaultProfileImage(model.ShortInfoModel, 150, 150);
                ViewModelCallInfo.MyProfileImge = ImageUtility.GetDetaultProfileImage(RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, 150, 150);
                ViewModelCallInfo.UserBasicInfoModel = model;
                ViewModelCallInfo.CallState = CallStates.UA_ONCALL;
            }
        }

        public void ResetProperties()
        {
            WebCamController = null;
        }

        public void CallNow(long userTableId, CallMediaType callType)
        {
            try
            {
                if (userTableId != DefaultSettings.LOGIN_TABLE_ID)
                {
                    if (CallData == null)
                    {
                        CallData = new CallerDTO();
                        CallData.calT = (int)callType;
                        CallData.UserTableID = userTableId;
                        CallData.CallID = SendToServer.GetRanDomPacketID(true);
                        CallData.IsIncomming = false;
                        CallHelperMethods.StartCallManger(callType);
                    }
                    else
                        UIHelperMethods.ShowWarning(NotificationMessages.ALREADY_IN_CALL, "Call in progress");
                }
            }
            catch (Exception ex)
            {
                log.Error("Can not make a call " + userTableId + "==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void StartAudioPlayerAndRecorder(bool callEndIfAudioDeviceProblem)
        {
            if (PCMDataPlayerAndRecorder == null)
                PCMDataPlayerAndRecorder = new AudioPlayerRecorder();
            PCMDataPlayerAndRecorder.StartPlayerRecorder();
        }

        public void StopPlayerAndRecorder()
        {
            if (PCMDataPlayerAndRecorder != null)
            {
                try
                {
                    PCMDataPlayerAndRecorder.UnRegisterEventTrigger();
                }
                catch (Exception ex)
                {
                    log.Error("StopPlayerAndRecorder===>" + ex.StackTrace);
                }
                finally
                {
                    PCMDataPlayerAndRecorder.StopPlayerRecorder();
                    PCMDataPlayerAndRecorder = null;
                }
            }
        }

        public void PlayAudioByteData(byte[] bytestoPlay)
        {
            if (PCMDataPlayerAndRecorder != null)
                PCMDataPlayerAndRecorder.AddVoiceSampleByteToPlay(bytestoPlay);
        }

        public void StartACall()
        {
            try
            {
                if (CallData != null)
                {
                    string connectedString = " Call:Established";
                    if (!CallData.IsIncomming)
                        connectedString = "Outgoing" + connectedString;
                    else
                        connectedString = "Incoming" + connectedString;
                    if (ViewModelCallInfo != null)
                        ViewModelCallInfo.Duration = connectedString;

                    if (CallData.calT == XamlStaticValues.CALL_TYPE_VIDEO)
                    {
                        ViewModelCallInfo.NumberOfVideo = 1;
                        if (!CallData.IsIncomming)
                        {
                            CallHelperMethods.StartWebCam(0);
                            if (ViewModelCallInfo != null && MainSwitcher.CallController.ViewModelCallInfo.IsAcceptedWithVideo)
                                ViewModelCallInfo.NumberOfVideo = 2;
                        }
                        else if (CallData.IsIncomming)
                        {
                            if (ViewModelCallInfo != null && ViewModelCallInfo.IsAcceptedWithVideo)
                            {
                                CallHelperMethods.StartWebCam(0);
                                ViewModelCallInfo.NumberOfVideo = 2;
                            }
                        }
                    }
                    else if (ViewModelCallInfo != null)
                        ViewModelCallInfo.NumberOfVideo = 0;
                }
            }
            catch (Exception ex)
            {
                log.Error("Startduration==>" + ex.Message + "\n" + ex.StackTrace);
            }
            finally { }
        }

        public void SendPCMData(byte[] rowData)
        {
            if (ViewModelCallInfo != null)
            {
                if (!ViewModelCallInfo.IsMuted)
                {
                    short[] sdata = new short[(int)(rowData.Length / 2)];
                    Buffer.BlockCopy(rowData, 0, sdata, 0, rowData.Length);
                    BaseCallService.SendAudioData(CallData.CallSessionID, sdata, (uint)sdata.Length);
                }
                else
                    SendEmptyPCMData();
                CallConstants.LAST_RTP_SENT_TIME = ModelUtility.CurrentTimeMillis();
            }
        }

        public void SendEmptyPCMData()
        {
            byte[] tempArray = new byte[1600];
            short[] sdata = new short[800];
            Buffer.BlockCopy(tempArray, 0, sdata, 0, tempArray.Length);
            BaseCallService.SendAudioData(CallData.CallSessionID, sdata, (uint)sdata.Length);
        }

        public void ProcessMyRGBData(byte[] rgbbyte)
        {
            if (callsdkwrapper.CallProperty.isAbleToSendVideo() && CallData != null)
                BaseCallService.SendVideoData(CallData.CallSessionID, rgbbyte, (uint)rgbbyte.Length, 0, 0);
            ShowMyRGBDataInView(rgbbyte);
        }


        public void HoldButtonAction()
        {
            if (CallData != null)
                BaseCallService.Hold(CallData.UserTableID);
        }

        public void CancelButtonAction(string busyMessage = null)
        {
            if (ViewModelCallInfo != null)
            {
                ViewModelCallInfo.IsBusyMsgPopup = false;
                ViewModelCallInfo.IsNeedToSendEndSignal = true;
            }
            if (CallData != null)
            {
                CallData.BusyMessageToSend = busyMessage;
                CallData.CallEndMessage = NotificationMessages.TEXT_CANCELED;
                IsCallEnded = true;
                CallData.IsNeedToPlayEndCallSound = false;
            }
        }

        #endregion "Public methods"

        #region "UI Modification"

        public void ShowCallUiInMainView()
        {
            if (CallUiInMainWindow == null)
            {
                CallUiInMainWindow = new UCCallUiInMainWindow();
            }
            ViewModelCallInfo.ViewType = ViewConstants.POPUP;
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCallMainUI, CallData);
        }

        public void HideCallUIFromMainWindow(long userTableId = 0)
        {
            if (userTableId > 0)
                View.ViewModel.RingIDViewModel.Instance.OnFriendCallChatButtonClicked(userTableId);
            else
            {
                RingIDViewModel.Instance.OnAllFeedsClicked(new object());
            }
        }

        public void ShowFullScreen()
        {
            ViewModelCallInfo.ViewType = ViewConstants.FULL_SCREEN;
            ShowSaparateWindow(WindowState.Maximized);
        }

        public void CloseFullScreen()
        {
            CloseSmallScreen();
        }

        public void ShowSmallScreen()
        {
            try
            {
                ViewModelCallInfo.ViewType = ViewConstants.SMALL_SCREEN;
                ShowSaparateWindow(WindowState.Normal);
            }
            finally { }
        }

        public void ShowSaparateWindow(WindowState state)
        {
            try
            {
                if (CallUiInSmallScreen == null)
                {
                    CallUiInSmallScreen = new WNCallUiInSmallScreen();
                }
                CallUiInSmallScreen.WindowState = state;
                CallUiInSmallScreen.Show();
            }
            finally { }
        }

        public void CloseSmallScreen()
        {
            if (CallUiInSmallScreen != null)
            {
                CallUiInSmallScreen.Close();
            }
        }

        public void PlayFriendRGBData(byte[] array, int dataLength, int height, int width, int iOrienttation)
        {
            try
            {
                if (CallProperty.isAbleToReceiveVideo())
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() =>
                    {
                        if (ViewModelCallInfo != null)
                        {
                            if (WebCamController != null && WebCamController.IsRunning && ViewModelCallInfo.NumberOfVideo < 2)
                            {
                                ViewModelCallInfo.NumberOfVideo = 2;
                            }
                            else if (ViewModelCallInfo.NumberOfVideo == 0)
                            {
                                ViewModelCallInfo.NumberOfVideo = 1;
                            }
                            var bitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                            BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
                            Marshal.Copy(array, 0, bmpData.Scan0, width * height * 3);
                            bitmap.UnlockBits(bmpData);
                            if (iOrienttation > 0)
                            {
                                bitmap = CallHelperMethods.RotateImageByAngle(bitmap, CallHelperMethods.GetAngale(iOrienttation));
                            }
                            IntPtr hBitmap = bitmap.GetHbitmap();
                            BitmapSource bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

                            if (!ViewModelCallInfo.IsNeedToToggleVideo)
                            {
                                ViewModelCallInfo.FullVideoImageSource = bitmapSource;
                            }
                            else
                            {
                                ViewModelCallInfo.MiniVideoImageSource = bitmapSource;
                            }
                            DeleteObject(hBitmap);
                        }
                    }));
                }
            }
            catch (Exception)
            {
            }

        }

        #endregion"UI Modification"

        #region "Private methods"
        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        private void ShowMyRGBDataInView(byte[] array)
        {
            try
            {
                int PixelSize = 3;
                if (array == null) return;
                int stride = CallConstants.VideoSettings.FRAME_WIDTH * PixelSize;
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                int scan0 = (int)handle.AddrOfPinnedObject();
                scan0 += (CallConstants.VideoSettings.FRAME_HEIGHT - 1) * stride;
                Bitmap b = new Bitmap(CallConstants.VideoSettings.FRAME_WIDTH, CallConstants.VideoSettings.FRAME_HEIGHT, -stride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, (IntPtr)scan0);
                handle.Free();
                IntPtr hBitmap = b.GetHbitmap();
                try
                {
                    System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                   {
                       if (ViewModelCallInfo != null)
                       {
                           BitmapSource retval = Imaging.CreateBitmapSourceFromHBitmap(
                              hBitmap,
                              IntPtr.Zero,
                              Int32Rect.Empty,
                              BitmapSizeOptions.FromEmptyOptions());
                           if (ViewModelCallInfo.NumberOfVideo == 0)
                               ViewModelCallInfo.NumberOfVideo = 1;
                           if (ViewModelCallInfo.NumberOfVideo == 1 || ViewModelCallInfo.IsNeedToToggleVideo)
                               ViewModelCallInfo.FullVideoImageSource = retval;
                           else
                               ViewModelCallInfo.MiniVideoImageSource = retval;
                       }
                   }, System.Windows.Threading.DispatcherPriority.Render);
                }
                finally
                {
                    DeleteObject(hBitmap);
                    b.Dispose();
                }
            }
            finally { }
        }
        #endregion "Private methods"

    }
}

﻿using System;
using System.Threading;

namespace View.Utility.Call.Video
{
    public class ThrdStartVideoCallRequest
    {
        #region "Private Fields"
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ThrdStartVideoCallRequest).Name);
        private bool requestSendingStarted = false;
        #endregion

        #region Constructor
        public ThrdStartVideoCallRequest()
        {
        }
        #endregion "Cons"

        #region "Properties"

        private Models.Entity.CallerDTO CallData
        {
            get
            {
                return MainSwitcher.CallController.CallData;
            }
        }

        #endregion "Properties"

        #region "Public methods"
        public void StopSending()
        {
            //   ConfigFile.SendingVideoToServer = false;
        }
        public void StartReuest()
        {
            if (!requestSendingStarted)
            {
                requestSendingStarted = true;
                Thread trd = new Thread(RunDelegate);
                trd.Start();
            }
            //else
            //{
            //    logger.Info("***********Can not start ConfigFile.SendingVideoToServer is true*************");
            //}
        }
        #endregion "Public methods"

        #region "Private methods"
        private void RunDelegate()
        {
            try
            {
                //byte[] startVideoCallREquest = VoicePacketGenerator.makeSignalingPacket(VoiceConstants.VIDEO_CALL.START, CallData.CallID, DefaultSettings.LOGIN_USER_ID, CallData.FriendID);
                //if (!ConfigFile.SendVideoToFriend && CallData.VideoCommunicationPort > 0)
                //    RingIDSDKWrapper.SendAudioSingalingDataGram(startVideoCallREquest, CallData.CallIDLong, CallData.CallServerIP, CallData.VoiceBindingPort);
                //int seding = 0;
                //while (true)
                //{
                //    if (CallData == null || (ConfigFile.SendVideoToFriend && CallData.VideoCommunicationPort > 0))
                //    {
                //        break;
                //    }
                //    else if (CallData != null)
                //    {

                //        seding++;
                //        if (seding == 10)
                //        {
                //            if (CallData.VideoCommunicationPort > 0)
                //            {
                //                int ret = RingIDSDKWrapper.SendAudioSingalingDataGram(startVideoCallREquest, CallData.CallIDLong, CallData.CallServerIP, CallData.VoiceBindingPort);
                //                if (ret < 0)
                //                {
                //                    break;
                //                }
                //            }
                //            else
                //            {
                //                CallSignals.BindingPortRequest(0);
                //            }
                //            seding = 0;

                //        }
                //        Thread.Sleep(200);
                //    }
                //}
            }
            catch (Exception)
            {
            }
            finally
            {
                requestSendingStarted = false;
            }

        }
        #endregion "Private methods"
    }
}

﻿using System;
using System.Threading;

using callsdkwrapper;
using Models.Constants;
using Models.Entity;
using View.Utility.Call.ViewModels;

namespace View.Utility.Call.Video
{
    public class ThrdVideoCallController
    {
        //#region "Private Fields"
        //private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ThrdVideoCallController).Name);
        //private bool isRunning = false;
        //private bool isRunningEndCall = false;
        //#endregion "Private Fields"

        //#region "Public Fields"
        //#endregion "Public Fields"

        //#region "Constructors"
        //public ThrdVideoCallController()
        //{

        //}
        //#endregion "Constructors"


        //#region "Properties"

        //private static CallerDTO CallData
        //{
        //    get
        //    {
        //        return MainSwitcher.CallController.CallData;
        //    }
        //}

        //public VMCall ViewModelCall
        //{
        //    get
        //    {
        //        return MainSwitcher.CallController.ViewModelCall;
        //    }
        //}

        //#endregion "Properties"

        //#region Event handlers
        //#endregion

        //#region "Public Methods"

        //public void StartVideoCallAndKeepAlive()
        //{
        //    if (!isRunning)
        //    {
        //        isRunning = true;
        //        Thread startVideo = new Thread(RunDelegate);
        //        startVideo.Start();
        //    }
        //}

        //public void EndVideoCallMySide()
        //{
        //    if (!isRunningEndCall)
        //    {

        //        Thread stopVideo = new Thread(RunEndVideoDelegate);
        //        stopVideo.Start();
        //    }
        //}
        //#endregion "Public methods"

        //#region Private methods

        //public static void CreateVideoSessionCall()
        //{

        //    if (CallData != null)
        //    {
        //        try
        //        {
        //            if (!CallData.VideoSessionCreated)
        //            {
        //                BaseApiStatus baseApi = MainSwitcher.CallController.BaseCallService.VideoStart(CallData.FriendID);
        //                CallData.VideoSessionCreated = false;
        //                //SessionStatus sessionStatus = RingIDSDKWrapper.ipv_CreateSession(CallData.CallIDLong, ConfigFile.MediaType.IPV_MEDIA_VIDEO, CallData.CallServerIP, CallData.VideoCommunicationPort);
        //                //if (sessionStatus == SessionStatus.SESSION_CREATE_SUCCESSFULLY)
        //                //{
        //                //    RingIDSDKWrapper.ipv_SetRelayServerInformation(CallData.CallIDLong, ConfigFile.MediaType.IPV_MEDIA_VIDEO, CallData.CallServerIP, CallData.VideoCommunicationPort);
        //                //    RingIDSDKWrapper.StartVideoCall(CallData.CallIDLong, ConfigFile.VideoSettings.FRAME_HEIGHT, ConfigFile.VideoSettings.FRAME_WIDTH);
        //                //    CallData.VideoSessionCreated = true;
        //                //}
        //                //else
        //                //{
        //                //    CallData.VideoSessionCreated = false;
        //                //}
        //            }
        //            //if (CallData.VideoSessionCreated)
        //            //{
        //            //    byte[] keepAliveByte = VoicePacketGenerator.makeSignalingPacket(VoiceConstants.VIDEO_CALL.KEEPALIVE, CallData.CallID, ConfigFile.USER_ID, CallData.FriendID);
        //            //    RingIDSDKWrapper.SendVideoSingalingDataGram(keepAliveByte, CallData.CallIDLong, CallData.CallServerIP, CallData.VideoCommunicationPort);
        //            //}

        //        }
        //        catch (Exception ex)
        //        {
        //            logger.Error(ex.Message + "\n" + ex.StackTrace);
        //        }
        //        finally
        //        {
        //        }
        //    }
        //}

        //public void RunEndVideoDelegate()
        //{
        //    isRunningEndCall = true;
        //    try
        //    {
        //        if (CallData != null)
        //        {
        //            CallHelperMethods.VideoSenderSideEnd(CallData.FriendID);
        //            //byte[] stopVideoCallREquest = VoicePacketGenerator.MakeVideoEndSignal(VoiceConstants.VIDEO_CALL.END, CallData.CallID, DefaultSettings.LOGIN_USER_ID, CallData.FriendID, VoiceConstants.END_VIDEO_CALL_ONE_SIDE);
        //            //RingIDSDKWrapper.SendVideoSingalingDataGram(stopVideoCallREquest, CallData.CallIDLong, CallData.CallServerIP, CallData.VideoCommunicationPort);
        //            //string pakId = CallSignals.getPack(CallData.CallID, VoiceConstants.VIDEO_CALL.END_CONFIRMATION);
        //            //for (int i = 1; i <= ConfigFile.CALL_WAITING_TIME; i++)
        //            //{
        //            //    if (VoiceConstants.CALL_PACKET_FOR_CONFIRMATION.Contains(pakId))
        //            //    {
        //            //        VoiceConstants.CALL_PACKET_FOR_CONFIRMATION.Remove(pakId);
        //            //        break;
        //            //    }
        //            //    if (CallData == null || DefaultSettings.LOGIN_USER_ID == 0)
        //            //    {
        //            //        break;
        //            //    }
        //            //    Thread.Sleep(VoiceConstants.PROCESSING_INTERVAL);
        //            //    if (i % 6 == 0)
        //            //    {
        //            //        RingIDSDKWrapper.SendVideoSingalingDataGram(stopVideoCallREquest, CallData.CallIDLong, CallData.CallServerIP, CallData.VideoCommunicationPort);
        //            //    }
        //            //}
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.Message + "\n" + ex.StackTrace);
        //    }
        //    finally
        //    {
        //        isRunningEndCall = false;
        //    }
        //}

        //private void RunDelegate()
        //{
        //    if (CallData != null)
        //    {
        //        if (CallData.VideoCommunicationPort == 0)
        //        {
        //            CallSignals.BindingPortRequest(ConfigFile.CALL_WAITING_TIME);
        //        }
        //        CreateVideoSessionCall();
        //        isRunning = false;
        //    }
        //}

        //#endregion Private methods
    }
}

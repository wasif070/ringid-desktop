﻿using System;
using System.Threading;

namespace View.Utility.Call.Video
{
    class ThradStartMyVideo
    {
        #region "Private Fields"
        private static bool running = false;
        private long userTableID = 0;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ThradStartMyVideo).Name);
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                if (MainSwitcher.CallController.WebCamController == null)
                {
                    MainSwitcher.CallController.WebCamController = new AforgeWebCamCapture();
                }
                if (CallHelperMethods.IsVideoDeviceExists())
                {
                    System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            MainSwitcher.CallController.WebCamController.StartWebCam();
                        });

                    if (userTableID > 0 && !callsdkwrapper.CallProperty.isAbleToSendVideo())
                    {
                        callsdkwrapper.BaseApiStatus baseApi = CallHelperMethods.VideoStart(userTableID);
                    }
                    MainSwitcher.CallController.ViewModelCallInfo.IsVideoStopped = false;
                }
                else
                {
                    MainSwitcher.CallController.ViewModelCallInfo.IsVideoStopped = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace);
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(long userTableID)
        {
            if (!running)
            {
                this.userTableID = userTableID;
                Thread thrd = new Thread(Run);
                thrd.Start();
            }
        }
        #endregion "Public methods"
    }
}

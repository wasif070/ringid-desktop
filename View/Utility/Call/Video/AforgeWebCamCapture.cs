﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using AForge.Video;
using AForge.Video.DirectShow;
using View.Utility.Call.Settings;

namespace View.Utility.Call.Video
{
    public class AforgeWebCamCapture
    {
        #region "Private Fields"
        private FilterInfoCollection videosources = null;
        VideoCaptureDevice videoSource;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public AforgeWebCamCapture()
        {
        }
        #endregion "Constructors"

        #region "Properties"

        #endregion "Properties"

        #region "Event Trigger"
        //Bitmap bitmap = null;
        private void videoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
            byte[] rgbbyte = BmpToArray(bitmap);
            try
            {
                CallHelperMethods.ProcessMyRGBData(rgbbyte);
            }
            finally
            {
                //    GC.Collect();
            }
        }

        #endregion "Event Trigger"

        #region "Private methods"
        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        public byte[] BmpToArray(Bitmap value)
        {
            BitmapData data = value.LockBits(new Rectangle(0, 0, value.Width, value.Height), ImageLockMode.ReadOnly, value.PixelFormat);

            try
            {
                IntPtr ptr = data.Scan0;
                int bytes = Math.Abs(data.Stride) * value.Height;
                byte[] rgbValues = new byte[bytes];
                Marshal.Copy(ptr, rgbValues, 0, bytes);

                return rgbValues;
            }
            finally
            {
                value.UnlockBits(data);
            }
        }
        #endregion "Private methods"

        #region "Public Methods"


        public static bool IsVideoDeviceExists()
        {
            FilterInfoCollection sourcesses = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (sourcesses.Count > 0)
            {
                return true;
            }
            return false;
        }

        public bool StartWebCam()
        {
            if (videoSource == null || !videoSource.IsRunning)
            {
                videosources = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                if (videosources.Count > 0)
                {
                    videoSource = new VideoCaptureDevice(videosources[0].MonikerString);
                    // videoSource.VideoCapabilities[0].FrameSize = new System.Drawing.Size(640, 480);
                    try
                    {
                        //Check if the video device provides a list of supported resolutions
                        if (videoSource.VideoCapabilities.Length > 0)
                        {
                            string highestSolution = "0;0";
                            //Search for the highest resolution
                            for (int i = 0; i < videoSource.VideoCapabilities.Length; i++)
                            {
                                if (videoSource.VideoCapabilities[i].FrameSize.Width == CallConstants.VideoSettings.FRAME_WIDTH && videoSource.VideoCapabilities[i].FrameSize.Height == CallConstants.VideoSettings.FRAME_HEIGHT)
                                {
                                    highestSolution = videoSource.VideoCapabilities[i].FrameSize.Width.ToString() + ";" + i.ToString();
                                    break;
                                }
                            }
                            //Set the highest resolution as active
                            videoSource.VideoResolution = videoSource.VideoCapabilities[Convert.ToInt32(highestSolution.Split(';')[1])];
                        }
                    }
                    finally { }
                    videoSource.NewFrame += new NewFrameEventHandler(videoSource_NewFrame);
                    //videoSource.DisplayPropertyPage(IntPtr.Zero);
                    videoSource.Start();
                    return true;
                }
            }
            return false;
        }

        public void StopWebCam()
        {
            try
            {
                if (videoSource != null && videoSource.IsRunning)
                {
                    videoSource.NewFrame -= new NewFrameEventHandler(videoSource_NewFrame);
                    if (videoSource != null)
                    {
                        videoSource.Stop();
                    }
                    videoSource = null;
                }

            }
            catch (Exception)
            {
            }

        }

        public void RestartWebCam()
        {
            try
            {
                StopWebCam();
                StartWebCam();
            }
            catch (Exception)
            {
            }
        }

        public bool IsRunning
        {
            get
            {
                if (videoSource != null && videoSource.IsRunning)
                { return videoSource.IsRunning; }
                return false;
            }
        }

        #endregion "Public methods"
    }
}

﻿using System;
namespace View.Utility.Call.Video
{
    class ThrdStopMyVideo
    {
        #region "Private Fields"
        private static bool running = false;
        bool stopByFriend = false;
        long userTableID = 0;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                //if (CallUIManager != null)
                //{
                //    if (!stopByFriend)
                //    {
                //        CallHelperMethods.VideoSenderSideEnd(MainSwitcher.CallController.CallData.UserTableID);
                //        //  MainSwitcher.CallController.VideoCallController.EndVideoCallMySide();
                //    }
                //    MainSwitcher.CallController.WebCamController.StopWebCam();
                //    if (CallUIManager != null)
                //    {
                //        CallUIManager.ChageUIWhileStopMyVideo();
                //    }

                //    //  ConfigFile.SendVideoToFriend = false;
                //}

                if (MainSwitcher.CallController.WebCamController == null)
                {
                    MainSwitcher.CallController.WebCamController = new AforgeWebCamCapture();
                }

                if (MainSwitcher.CallController.WebCamController != null && MainSwitcher.CallController.WebCamController.IsRunning)
                {
                    MainSwitcher.CallController.WebCamController.StopWebCam();
                    if (!stopByFriend && userTableID > 0)
                    {
                        CallHelperMethods.VideoSenderSideEnd(userTableID);
                    }
                }

            }
            catch (Exception)
            {


            }
            finally
            {
                running = false;

            }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(bool byFriend, long tableID)
        {
            if (!running)
            {
                this.stopByFriend = byFriend;
                this.userTableID = tableID;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        #endregion "Public methods"
    }
}

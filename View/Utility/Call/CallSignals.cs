﻿using Models.Entity;

namespace View.Utility.Call
{
    class CallSignals
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(CallSignals).Name);
        #endregion "Private Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Private methods"

        //private static ResendPacketDTO makeResendingPacket(byte[] byteArray, string server, int port)
        //{
        //    ResendPacketDTO resendPacketDTO = new ResendPacketDTO();
        //    resendPacketDTO.SERVER_IP = server;
        //    resendPacketDTO.PORT = port;
        //    resendPacketDTO.ByteToSend = byteArray;
        //    resendPacketDTO.NumberOfResend = 1;
        //    return resendPacketDTO;
        //}

        //private static void resendThisPacket(int packetTypeToConfirm, byte[] byteToreSend, long friendIdentity, string callID, string voiceServerIP, int port)
        //{
        //    long callIDL = CallConstants.GetLong(callID);
        //    ResendPacketDTO resendPacketDTO = makeResendingPacket(byteToreSend, voiceServerIP, port);
        //    string pakId = getPack(callID, packetTypeToConfirm);
        //    new ThrdCallPacketResender(resendPacketDTO, pakId, friendIdentity, callIDL).StartThread();
        //}

        //private static void answerButtonAction(long friendIdentity, string callID, string voiceServerIP, int voiceBindingPort, int calltype = 1)
        //{
        //    callsdkwrapper.CallMediaType media = callsdkwrapper.CallMediaType.Voice;
        //    if (calltype == VoiceConstants.CALL_TYPE_VIDEO)
        //    {
        //        media = callsdkwrapper.CallMediaType.Video;
        //    }
        //    CallHelperMethods.Answer(friendIdentity, media);
        //    //byte[] byteToreSend = VoicePacketGenerator.makeSignalingPacket(VoiceConstants.CALL_STATE.ANSWER, callID, DefaultSettings.LOGIN_USER_ID, friendIdentity, calltype);
        //    ////SendDataGram(VoiceConstants.CALL_STATE.CONNECTED, byteToreSend, friendIdentity, callID, voiceServerIP, voiceBindingPort, true);
        //    //SendRegisteredDataGram(VoiceConstants.CALL_STATE.CONNECTED, byteToreSend, friendIdentity, callID, true);
        //}

        #endregion "Private methods"

        #region "Properties"

        private static CallerDTO CallData
        {
            get
            {
                return MainSwitcher.CallController.CallData;
            }
        }

        #endregion "Properties"

        #region "Public Methods"

        public static string getPack(string pakid, int type)
        {
            return pakid + "_" + type;
        }

        //public static int SendDataGram(int packetTypeToConfirm, byte[] byteToSend, long friendIdentity, string callID, string voiceServerIP, int port, bool resend)
        //{
        //    long callIDL = CallConstants.GetLong(callID);
        //    int sent = RingIDSDKWrapper.SendAudioSingalingDataGram(byteToSend, callIDL, voiceServerIP, port);
        //    if (resend)
        //        resendThisPacket(packetTypeToConfirm, byteToSend, friendIdentity, callID, voiceServerIP, port);
        //    return sent;
        //}

        //public static int SendDataGramNoResend(byte[] byteToSend, string CallID, string voiceServerIP, int port)
        //{
        //    long callIDL = CallConstants.GetLong(CallID);
        //    return RingIDSDKWrapper.SendAudioSingalingDataGram(byteToSend, callIDL, voiceServerIP, port);
        //}

        //public static int SendRegisteredDataGram(int packetTypeToConfirm, byte[] byteToSend, long friendIdentity, string callID, bool resend)
        //{
        //    long callIDL = CallConstants.GetLong(callID);
        //    //   int sent = UICommunicator.sendAudioSingalingDataGram(byteToSend, friendIdentity);
        //    int sent = RingIDSDKWrapper.SendAudioSingalingDataGram(byteToSend, callIDL, CallData.CallServerIP, CallData.VoiceBindingPort);
        //    if (resend)
        //    {
        //        ResendPacketDTO resendPacketDTO = new ResendPacketDTO();
        //        resendPacketDTO.ByteToSend = byteToSend;
        //        resendPacketDTO.NumberOfResend = 1;
        //        string pakId = getPack(callID, packetTypeToConfirm);
        //        new ThrdRegisteredCallPacketResender(resendPacketDTO, pakId, friendIdentity, callIDL).StartThread();
        //    }
        //    return sent;
        //}

        //public static void SendRegButtonAction(long friendIdentity, string callID, string voiceServerIP, int voiceRegPort, int calltype)
        //{
        //    byte[] byteToreSend = VoicePacketGenerator.makeRegisterPacket(VoiceConstants.VOICE_REGISTER, DefaultSettings.LOGIN_USER_ID, friendIdentity, callID, calltype);
        //    SendDataGram(VoiceConstants.VOICE_REGISTER_CONFIRMATION, byteToreSend, friendIdentity, callID, voiceServerIP, voiceRegPort, true);
        //}

        //public static void SendCallIn(long friendIdentity, string callID, String voiceServerIP, int port)
        //{
        //    long callIDL = CallConstants.GetLong(callID);
        //    (new ThrdSendINCall(friendIdentity, callID, callIDL, voiceServerIP, port)).StartThread();
        //}

        //public static void CallButtonAction(int packetType, long friendIdentity, string callID, string voiceServerIP, int voiceBindingPort)
        //{
        //    byte[] byteToreSend = VoicePacketGenerator.makeSignalingPacket(packetType, callID, DefaultSettings.LOGIN_USER_ID, friendIdentity);
        //    SendRegisteredDataGram(VoiceConstants.CALL_STATE.RINGING, byteToreSend, friendIdentity, callID, true);
        //}

        //public static void SendHoldSignal(bool hold, long friendIdentity, string callID, string voiceServerIP, int voiceBindingPort)
        //{
        //    int packetType = VoiceConstants.CALL_STATE.VOICE_CALL_HOLD;
        //    int packetTypeToConfirm = VoiceConstants.CALL_STATE.VOICE_CALL_HOLD_CONFIRMATION;
        //    if (!hold)
        //    {
        //        packetType = VoiceConstants.CALL_STATE.VOICE_CALL_UNHOLD;
        //        packetTypeToConfirm = VoiceConstants.CALL_STATE.VOICE_UNHOLD_CONFIRMATION;
        //    }
        //    byte[] byteToreSend = VoicePacketGenerator.makeSignalingPacket(packetType, callID, DefaultSettings.LOGIN_USER_ID, friendIdentity);
        //    SendRegisteredDataGram(packetTypeToConfirm, byteToreSend, friendIdentity, callID, true);
        //}

        //public static void SendKeepAlive(long freindID, string callId, string ip, int port)
        //{
        //    (new SendKeepAliveForCall(callId, freindID, ip, port)).StartThread();
        //}

        //public static void SendSignalingPacket(int type, VoiceMessageDTO voiceMessageDTO, String ip, int port)
        //{
        //    byte[] confirmationByte = VoicePacketGenerator.makeSignalingPacket(type, voiceMessageDTO.PacketID, ConfigFile.USER_ID, CallData.FriendID);
        //    CallSignals.SendDataGramNoResend(confirmationByte, CallData.CallID, CallData.CallServerIP, CallData.VoiceBindingPort);
        //}

        //public static void AnswerButtonAction(int answerType = 1)
        //{
        //    answerButtonAction(CallData.FriendID, CallData.CallID, CallData.CallServerIP, CallData.VoiceBindingPort, answerType);
        //}

        //public static void SendConnect(long userid)
        //{
        //    byte[] confirmationByte = VoicePacketGenerator.makeSignalingPacket(VoiceConstants.CALL_STATE.CONNECTED, CallData.CallID, ConfigFile.USER_ID, userid);
        //    CallSignals.SendDataGramNoResend(confirmationByte, CallData.CallID, CallData.CallServerIP, CallData.VoiceBindingPort);
        //}

        public static void BindingPortRequest(int resedingTime)
        {
            //byte[] bindingPortRequest = VoicePacketGenerator.makeSignalingPacket(VoiceConstants.VIDEO_CALL.BINDING_PORT, CallData.CallID, DefaultSettings.LOGIN_USER_ID, CallData.FriendID);
            //RingIDSDKWrapper.SendAudioSingalingDataGram(bindingPortRequest, CallData.CallIDLong, CallData.CallServerIP, CallData.CallRegPort);
            //Thread.Sleep(500);
            //for (int reg = 1; reg <= resedingTime; reg++)
            //{
            //    if (CallData == null || DefaultSettings.LOGIN_USER_ID == 0)
            //    {
            //        break;
            //    }
            //    if (CallData.VideoCommunicationPort > 0)
            //    {
            //        break;
            //    }
            //    else
            //    {
            //        if (reg % 6 == 0)
            //        {
            //            RingIDSDKWrapper.SendAudioSingalingDataGram(bindingPortRequest, CallData.CallIDLong, CallData.CallServerIP, CallData.CallRegPort);
            //        }
            //        Thread.Sleep(VoiceConstants.PROCESSING_INTERVAL);
            //    }
            //}
        }

        #endregion "Public methods"
    }
}

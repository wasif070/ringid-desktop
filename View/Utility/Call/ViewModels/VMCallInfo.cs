﻿using System;
using System.Windows.Input;
using System.Windows.Media;
using Models.Constants;
using View.BindingModels;
using View.UI;
using View.Utility.audio;
using View.Utility.Call.Settings;
using View.ViewModel;

namespace View.Utility.Call.ViewModels
{
    public class VMCallInfo : BaseViewModel
    {
        #region "Private Fields"
        #endregion "Private Fields"

        #region "Constructors"
        public VMCallInfo()
        {

        }
        #endregion "Constructors"

        #region "Properties"

        private int playerViewType = ViewConstants.POPUP;
        public int ViewType
        {
            get { return playerViewType; }
            set
            {
                playerViewType = value;
                OnPropertyChanged("ViewType");
            }
        }

        private int uiType = 1;
        public int UiType
        {
            get { return uiType; }
            set { uiType = value; OnPropertyChanged("UiType"); }
        }

        private ImageSource netWorkImage;
        public ImageSource NetWorkImage
        {
            get { return netWorkImage; }
            set { netWorkImage = value; OnPropertyChanged("NetWorkImage"); }
        }

        private ImageSource myProfileImge = null;
        public ImageSource MyProfileImge
        {
            get { return myProfileImge; }
            set { myProfileImge = value; OnPropertyChanged("MyProfileImge"); }
        }

        private ImageSource loaderImageSource;
        public ImageSource LoaderImageSource
        {
            get { return loaderImageSource; }
            set { loaderImageSource = value; OnPropertyChanged("LoaderImageSource"); }
        }

        private ImageSource friendProfileSource = null;
        public ImageSource FriendProfileSource
        {
            get { return friendProfileSource; }
            set { friendProfileSource = value; OnPropertyChanged("FriendProfileSource"); }
        }

        private ImageSource friendProfileSourceMini = null;
        public ImageSource FriendProfileSourceMini
        {
            get { return friendProfileSourceMini; }
            set { friendProfileSourceMini = value; OnPropertyChanged("FriendProfileSourceMini"); }
        }

        private ImageSource miniVideoImageSource = null;
        public ImageSource MiniVideoImageSource
        {
            get { return miniVideoImageSource; }
            set { miniVideoImageSource = value; OnPropertyChanged("MiniVideoImageSource"); }
        }

        private ImageSource fullVideoImageSource = null;
        public ImageSource FullVideoImageSource
        {
            get { return fullVideoImageSource; }
            set { fullVideoImageSource = value; OnPropertyChanged("FullVideoImageSource"); }
        }

        private UserBasicInfoModel userBasicInfoModel = null;
        public UserBasicInfoModel UserBasicInfoModel
        {
            get { return userBasicInfoModel; }
            set { userBasicInfoModel = value; OnPropertyChanged("UserBasicInfoModel"); }
        }

        private string errorText;
        public string ErrorText
        {
            get { return errorText; }
            set { errorText = value; OnPropertyChanged("ErrorText"); }
        }

        private string duration = "00:00:00";
        public string Duration
        {
            get { return duration; }
            set { duration = value; OnPropertyChanged("Duration"); }
        }

        private string netWorkStrength = "Good";
        public string NetWorkStrength
        {
            get { return netWorkStrength; }
            set { netWorkStrength = value; OnPropertyChanged("NetWorkStrength"); }
        }

        private string volumnPercentag;
        public string VolumnPercentag
        {
            get { return volumnPercentag; }
            set { volumnPercentag = value; OnPropertyChanged("VolumnPercentag"); }
        }

        private int volumnInView;
        public int VolumnInView
        {
            get { return volumnInView; }
            set { volumnInView = value; OnPropertyChanged("VolumnInView"); }
        }

        private int callState;
        public int CallState
        {
            get { return callState; }
            set
            {
                if (value == callState) return;
                callState = value;
                ChangeCallButtonsVisibility();
                OnPropertyChanged("CallState");
            }
        }

        private bool isVideoStopped = true;
        public bool IsVideoStopped
        {
            get { return isVideoStopped; }
            set { if (isVideoStopped == value) return; isVideoStopped = value; OnPropertyChanged("IsVideoStopped"); }
        }

        private bool isMuted = false;
        public bool IsMuted
        {
            get { return isMuted; }
            set
            {
                if (value == isMuted) return;
                isMuted = value;
                OnPropertyChanged("IsMuted");
            }
        }

        private bool isVideoCall = false;
        public bool IsVideoCall
        {
            get { return isVideoCall; }
            set
            {
                if (value == isVideoCall) return;
                isVideoCall = value;
                OnPropertyChanged("IsVideoCall");
            }
        }

        private bool isOpenVolumPopup = false;
        public bool IsOpenVolumPopup
        {
            get { return isOpenVolumPopup; }
            set
            {
                if (value == isOpenVolumPopup) return;
                isOpenVolumPopup = value;
                OnPropertyChanged("IsOpenVolumPopup");
            }
        }

        private bool isBusyMsgPopup = false;
        public bool IsBusyMsgPopup
        {
            get { return isBusyMsgPopup; }
            set
            {
                if (value == isBusyMsgPopup) return;
                isBusyMsgPopup = value;
                OnPropertyChanged("IsBusyMsgPopup");
            }
        }

        private bool isOpenContextMenu = false;
        public bool IsOpenContextMenu
        {
            get { return isOpenContextMenu; }
            set
            {
                if (value == isOpenContextMenu) return;
                isOpenContextMenu = value;
                OnPropertyChanged("IsOpenContextMenu");
            }
        }

        private bool isAcceptedWithVideo = false;
        public bool IsAcceptedWithVideo
        {
            get { return isAcceptedWithVideo; }
            set
            {
                isAcceptedWithVideo = value;
            }
        }

        private bool isNeedToSendEndSignal = false;
        public bool IsNeedToSendEndSignal
        {
            get { return isNeedToSendEndSignal; }
            set
            {
                isNeedToSendEndSignal = value;
            }
        }

        private bool isNeedToToggleVideo = false;
        public bool IsNeedToToggleVideo
        {
            get { return isNeedToToggleVideo; }
            set
            {
                isNeedToToggleVideo = value;
            }
        }

        private int numberOfVideo = 0;
        public int NumberOfVideo
        {
            get { return numberOfVideo; }
            set
            {
                numberOfVideo = value; OnPropertyChanged("NumberOfVideo");
            }
        }
        #endregion "Properties"

        #region "Commands"

        private ICommand doubleClickOnScreen;

        public ICommand DoubleClickOnScreen
        {
            get
            {
                if (doubleClickOnScreen == null)
                {
                    doubleClickOnScreen = new RelayCommand(param => OnDoubleClickOnScreen(param));
                } return doubleClickOnScreen;
            }
            set { doubleClickOnScreen = value; }
        }

        private ICommand showInMainView;

        public ICommand ShowInMainView
        {
            get
            {
                if (showInMainView == null)
                {
                    showInMainView = new RelayCommand(param => OnShowInMainView(param));
                } return showInMainView;
            }
        }

        private ICommand hideFromMain;

        public ICommand HideFromMain
        {
            get
            {
                if (hideFromMain == null)
                {
                    hideFromMain = new RelayCommand(param => OnHideFromMainView(param));
                } return hideFromMain;
            }
        }

        private ICommand closeFullScreen;

        public ICommand CloseFullScreen
        {
            get
            {
                if (closeFullScreen == null)
                {
                    closeFullScreen = new RelayCommand(param => OnExitFullScreen(param));
                } return closeFullScreen;
            }
        }

        private ICommand endCall;

        public ICommand EndCall
        {
            get
            {
                if (endCall == null)
                {
                    endCall = new RelayCommand(param => OnEndCall(param));
                }
                return endCall;
            }

        }

        private ICommand goToFullScreen;

        public ICommand GoToFullScreen
        {
            get
            {
                if (goToFullScreen == null)
                {
                    goToFullScreen = new RelayCommand(param => OnGoToFullScreen(param));
                }
                return goToFullScreen;
            }
        }

        private ICommand goToSmallScreen;

        public ICommand GoToSmallScreen
        {
            get
            {
                if (goToSmallScreen == null)
                {
                    goToSmallScreen = new RelayCommand(param => OnGoToSmallScreen(param));
                }
                return goToSmallScreen;
            }
        }

        private ICommand profileButtonCommand;

        public ICommand ProfileButtonCommand
        {
            get { if (profileButtonCommand == null)profileButtonCommand = new RelayCommand(param => OnProfileView(param)); return profileButtonCommand; }
            set { profileButtonCommand = value; }
        }

        private ICommand chatButtonCommand;

        public ICommand ChatButtonCommand
        {
            get
            {
                if (chatButtonCommand == null)
                    chatButtonCommand = new RelayCommand(pararm => OnChatButton(pararm));
                return chatButtonCommand;
            }
            set { chatButtonCommand = value; }
        }

        private ICommand muteButtonCommand;

        public ICommand MuteButtonCommand
        {
            get
            {
                if (muteButtonCommand == null)
                    muteButtonCommand = new RelayCommand(param => OnMuteButtonClick(param));
                return muteButtonCommand;
            }
            set { muteButtonCommand = value; }
        }

        private ICommand unMuteButtonCommand;

        public ICommand UnMuteButtonCommand
        {
            get
            {
                if (unMuteButtonCommand == null)
                    unMuteButtonCommand = new RelayCommand(param => OnUnMuteButtonClick(param));
                return unMuteButtonCommand;
            }
            set { unMuteButtonCommand = value; }
        }

        private ICommand videoButtonCommand;

        public ICommand VideoButtonCommand
        {
            get { if (videoButtonCommand == null) videoButtonCommand = new RelayCommand(param => OnStartVideoCommand(param)); return videoButtonCommand; }
            set { videoButtonCommand = value; }
        }

        private ICommand stopVideoButtonCommand;

        public ICommand StopVideoButtonCommand
        {
            get
            {
                if (stopVideoButtonCommand == null)
                    stopVideoButtonCommand = new RelayCommand(param => OnStopVideoButtonMainClick(param));
                return stopVideoButtonCommand;
            }
            set { stopVideoButtonCommand = value; }
        }

        private ICommand volumeButtonCommand;

        public ICommand VolumeButtonCommand
        {
            get
            {
                if (volumeButtonCommand == null) volumeButtonCommand = new RelayCommand(param => OnVolumeButtonClick(param));
                return volumeButtonCommand;
            }
            set { volumeButtonCommand = value; }
        }

        private ICommand callCancalOrRejectCommand;

        public ICommand CallCancalOrRejectCommand
        {
            get
            {
                if (callCancalOrRejectCommand == null) callCancalOrRejectCommand = new RelayCommand(param => OnEndCall(param));
                return callCancalOrRejectCommand;
            }
            set { callCancalOrRejectCommand = value; }
        }

        private ICommand incomingCallAcceptCommand;

        public ICommand IncomingCallAcceptCommand
        {
            get
            {
                if (incomingCallAcceptCommand == null) incomingCallAcceptCommand = new RelayCommand(param => OnIncomingCallAcceptClick(param));
                return incomingCallAcceptCommand;
            }
            set { incomingCallAcceptCommand = value; }
        }

        private ICommand videoCallAcceptCommand;

        public ICommand VideoCallAcceptCommand
        {
            get
            {
                if (videoCallAcceptCommand == null) videoCallAcceptCommand = new RelayCommand(param => OnVideoCallAcceptClick(param));
                return videoCallAcceptCommand;
            }
            set { videoCallAcceptCommand = value; }
        }

        private ICommand busyMessageCommand;

        public ICommand BusyMessageCommand
        {
            get
            {
                if (busyMessageCommand == null) busyMessageCommand = new RelayCommand(param => OnBusyMessageClick(param));
                return busyMessageCommand;
            }
            set { busyMessageCommand = value; }
        }

        private ICommand contextMenuPopup;

        public ICommand ContextMenuPopup
        {
            get
            {
                if (contextMenuPopup == null) contextMenuPopup = new RelayCommand(param => OnContextMenuPopup(param));
                return contextMenuPopup;
            }
            set { contextMenuPopup = value; }
        }

        private ICommand doubleClickMiniWindow;

        public ICommand DoubleClickMiniWindowCommand
        {
            get
            {
                if (doubleClickMiniWindow == null)
                {
                    doubleClickMiniWindow = new RelayCommand(param => OnDoubleClickMiniWindow(param));
                }
                return doubleClickMiniWindow;
            }
        }
        #endregion "Commands"

        #region "Utility methods"
        private void ChangeCallButtonsVisibility()
        {
            if (CallState > 0)
            {
                if (CallState == CallStates.UA_CALL_ACCEPTED)
                {
                    UiType = ViewConstants.CONNECTED;
                }
                else if (CallState == CallStates.UA_ONCALL || CallState == CallStates.HOLD_CALL)
                {
                    UiType = ViewConstants.CONNECTED;
                }
                else if (CallState == CallStates.UA_INCOMING_CALL)
                {
                    UiType = ViewConstants.INCOMMING;
                }
                else if (CallState == CallStates.UA_OUTGOING_CALL)
                {
                    UiType = ViewConstants.OUTGOING;
                }
            }
        }

        private void OnDoubleClickOnScreen(object param)
        {
            Console.WriteLine("OnDoubleClickOnScreen");
            if (UiType == ViewConstants.CONNECTED)
            {
                if (ViewType == ViewConstants.FULL_SCREEN)
                {
                    OnGoToSmallScreen(null);
                }
                else
                {
                    OnGoToFullScreen(null);
                }
            }
        }

        private void OnShowInMainView(object param)
        {
            OnExitSmallScreen(null);
            MainSwitcher.CallController.ShowCallUiInMainView();//.add
        }

        public void OnHideFromMainView(object param)
        {
            if (ViewType == ViewConstants.POPUP)
                MainSwitcher.CallController.HideCallUIFromMainWindow(UserBasicInfoModel.ShortInfoModel.UserTableID);
        }

        private void OnGoToFullScreen(object param)
        {

            OnHideFromMainView(null);
            //     OnExitSmallScreen(null);
            MainSwitcher.CallController.ShowFullScreen();
        }

        private void OnExitFullScreen(object param)
        {
            MainSwitcher.CallController.CloseFullScreen();
        }

        public void OnGoToSmallScreen(object param)
        {
            OnHideFromMainView(null);
            //  OnExitFullScreen(null);
            MainSwitcher.CallController.ShowSmallScreen();
        }

        private void OnExitSmallScreen(object param)
        {
            MainSwitcher.CallController.CloseSmallScreen();
        }

        public void OnEndCall(object param)
        {
            CallHelperMethods.CancelButtonAction();
            MainSwitcher.CallController.IsCallEnded = true;
            IsNeedToSendEndSignal = true;
        }

        public void OnProfileView(object sender)
        {
            if (UserBasicInfoModel != null && UserBasicInfoModel.ShortInfoModel.UserTableID > 0)
                RingIDViewModel.Instance.OnFriendProfileButtonClicked(UserBasicInfoModel.ShortInfoModel.UserTableID);
        }

        public void OnChatButton(object sender)
        {
            if (UserBasicInfoModel != null && UserBasicInfoModel.ShortInfoModel.UserTableID > 0)
                RingIDViewModel.Instance.OnFriendCallChatButtonClicked(UserBasicInfoModel.ShortInfoModel.UserTableID);
        }

        private void OnMuteButtonClick(object sender)
        {
            IsMuted = true;
        }

        private void OnUnMuteButtonClick(object sender)
        {
            IsMuted = false;
        }

        private void OnStartVideoCommand(object sender)
        {
            if (CallHelperMethods.IsVideoDeviceExists())
            {
                IsVideoStopped = false;
                IsVideoCall = true;
                CallHelperMethods.StartWebCam(UserBasicInfoModel.ShortInfoModel.UserTableID);
                IncrementNumberOfVideo();
            }
            else
            {
                UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.NO_WEBCAM);
            }
        }

        private void OnStopVideoButtonMainClick(object sender)
        {
            IsVideoStopped = true;
            CallHelperMethods.StopWebCam(UserBasicInfoModel.ShortInfoModel.UserTableID);
            MiniVideoImageSource = null;
            DecrementNumberOfVideo();
        }

        private void OnVolumeButtonClick(object sender)
        {
            IsOpenVolumPopup = true;
            float currentVolumn = MasterVolumControl.GetCurrentVolumnLabel();
        }

        //public void OnCallCancalOrRejectClick(object sender)
        //{
        //    OnEndCall(null);

        //}

        private void OnIncomingCallAcceptClick(object sender)
        {
            CallState = CallStates.UA_CALL_ACCEPTED;
            IsAcceptedWithVideo = false;
            Duration = "Please wait...";
            CallHelperMethods.Answer(UserBasicInfoModel.ShortInfoModel.UserTableID, callsdkwrapper.CallMediaType.Voice);
        }

        private void OnVideoCallAcceptClick(object sender)
        {
            CallState = CallStates.UA_CALL_ACCEPTED;
            IsAcceptedWithVideo = true;
            Duration = "Please wait...";
            if (CallHelperMethods.IsVideoDeviceExists())
            {
                CallHelperMethods.Answer(UserBasicInfoModel.ShortInfoModel.UserTableID, callsdkwrapper.CallMediaType.Video);
            }
            else
            {
                CallHelperMethods.Answer(UserBasicInfoModel.ShortInfoModel.UserTableID, callsdkwrapper.CallMediaType.Voice);
            }

        }

        private void OnBusyMessageClick(object sender)
        {
            IsBusyMsgPopup = true;
        }

        private void OnCloseMiniWindowClick(object sender)
        {
        }

        private void OnCloseMaximizeWindow(object sender)
        {
        }

        private void OnGotoMaximizeWindow(object sender)
        {
        }

        private void OnDoubleClickMiniWindow(object sender)
        {
            if (IsNeedToToggleVideo)
            {
                IsNeedToToggleVideo = false;
            }
            else
            {
                IsNeedToToggleVideo = true;
            }
        }

        private void OnContextMenuPopup(object sender)
        {
            IsOpenContextMenu = true;
        }

        #endregion "Utility methods"

        #region "Public Methods"

        public void EndOrCancelCall()
        {
            MainSwitcher.CallController.IsCallEnded = true;
            OnHideFromMainView(null);
            OnExitFullScreen(null);
            OnExitSmallScreen(null);
            ViewType = ViewConstants.POPUP;
        }

        public void IncrementNumberOfVideo()
        {
            if (NumberOfVideo < 2)
            {
                NumberOfVideo = NumberOfVideo + 1;// NumberOfVideo++;
            }
        }

        public void DecrementNumberOfVideo()
        {
            if (NumberOfVideo > 0 && NumberOfVideo <= 2)
            {
                NumberOfVideo = NumberOfVideo - 1;
            }
            if (NumberOfVideo == 0)
            {
                FullVideoImageSource = null;
                MiniVideoImageSource = null;
                CallHelperMethods.StopWebCam(0, false);
            }
        }
        #endregion "Public methods"
    }
}

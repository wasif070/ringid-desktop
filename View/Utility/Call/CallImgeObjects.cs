﻿using System.Windows.Media.Imaging;
using View.Constants;

namespace View.Utility.Call
{
    public class CallImgeObjects
    {
        #region "Private Fields"
        private static BitmapImage _INCOMING_CALL_ANIMATION = null;
        private static BitmapImage _NETWORK_STRENGTH0 = null;
        private static BitmapImage _NETWORK_STRENGTH1 = null;
        private static BitmapImage _NETWORK_STRENGTH2 = null;
        private static BitmapImage _NETWORK_STRENGTH3 = null;
        private static BitmapImage _NETWORK_STRENGTH4 = null;
        private static BitmapImage _OUTGOING_ANIMATION = null;
        #endregion"Private Fields"

        #region "Property change"

        public static BitmapImage INCOMING_CALL_ANIMATION
        {
            get
            {
                if (_INCOMING_CALL_ANIMATION == null)
                {
                    _INCOMING_CALL_ANIMATION = ImageUtility.GetBitmapImage(ImageLocation.INCOMING_CALL_ANIMATION);
                }
                return _INCOMING_CALL_ANIMATION;
            }
            set
            {
                _INCOMING_CALL_ANIMATION = value;
            }
        }

        public static BitmapImage NETWORK_STRENGTH0
        {
            get
            {
                if (_NETWORK_STRENGTH0 == null)
                {
                    _NETWORK_STRENGTH0 = ImageUtility.GetBitmapImage(ImageLocation.CALL_NET0);
                }
                return _NETWORK_STRENGTH0;
            }
        }
        public static BitmapImage NETWORK_STRENGTH1
        {
            get
            {
                if (_NETWORK_STRENGTH1 == null)
                {
                    _NETWORK_STRENGTH1 = ImageUtility.GetBitmapImage(ImageLocation.CALL_NET1);
                }
                return _NETWORK_STRENGTH1;
            }
        }
        public static BitmapImage NETWORK_STRENGTH2
        {
            get
            {
                if (_NETWORK_STRENGTH2 == null)
                {
                    _NETWORK_STRENGTH2 = ImageUtility.GetBitmapImage(ImageLocation.CALL_NET2);
                }
                return _NETWORK_STRENGTH2;
            }
        }
        public static BitmapImage NETWORK_STRENGTH3
        {
            get
            {
                if (_NETWORK_STRENGTH3 == null) _NETWORK_STRENGTH3 = ImageUtility.GetBitmapImage(ImageLocation.CALL_NET3);
                return _NETWORK_STRENGTH3;
            }
        }
        public static BitmapImage NETWORK_STRENGTH4
        {
            get
            {
                if (_NETWORK_STRENGTH4 == null)
                {
                    _NETWORK_STRENGTH4 = ImageUtility.GetBitmapImage(ImageLocation.CALL_NET4);
                }
                return _NETWORK_STRENGTH4;
            }
        }
        public static BitmapImage OUTGOING_ANIMATION
        {
            get
            {
                if (_OUTGOING_ANIMATION == null)
                {
                    _OUTGOING_ANIMATION = ImageUtility.GetBitmapImage(ImageLocation.OUTGOING_ANIMATION);
                }
                return _OUTGOING_ANIMATION;
            }
            set
            {
                _OUTGOING_ANIMATION = value;
            }
        }
        #endregion
    }
}

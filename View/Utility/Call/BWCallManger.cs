﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using Auth.utility;
using callsdkwrapper;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using NAudio.Wave;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.audio;
using View.Utility.Call.Settings;
using View.Utility.Call.ViewModels;
using View.Utility.Stream;
using View.ViewModel;

namespace View.Utility.Call
{
    public class BWCallManger : BackgroundWorker
    {
        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(BWCallManger).Name);
        private CallMediaType callType;
        long currentTime = 0;
        private long MaxNoRTP = 5000;
        private WaveOut waveOut;
        private WaveFileReader reader;
        private int increment = 0;
        private bool callConnected = false;
        private long callDurationInSec = 0;
        private int previousNetworkStrength = 0;
        UserBasicInfoModel friendInfoModel = null;
        int accessType = -1;
        BlockedNonFriendModel blockModel = null;
        #endregion "Private Fields"

        #region "Constructors"
        public BWCallManger()
        {
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            DoWork += new DoWorkEventHandler(bw_DoWork);
            ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
        }
        #endregion "Constructors"

        #region "Properties"

        private CallerDTO CallData
        {
            get { return MainSwitcher.CallController.CallData; }
        }

        private VMCallInfo ViewModelCall
        {
            get { return MainSwitcher.CallController.ViewModelCallInfo; }
        }
        #endregion "Properties"

        #region "Event Trigger"

        /// <summary>
        /// // because BWCallManager is single instance so need to reset the values first
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {

            increment = 0;
            callConnected = false;
            callDurationInSec = 0;
            string msg = null;
            MainSwitcher.CallController.IsCallEnded = false;

            BackgroundWorker worker = sender as BackgroundWorker;
            MainSwitcher.CallController.ViewModelCallInfo = new VMCallInfo();
            friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(CallData.UserTableID, 0, CallData.FullName);
            if (friendInfoModel != null)
            {
                CallData.FullName = friendInfoModel.ShortInfoModel.FullName;
                CallData.ProfileImage = friendInfoModel.ShortInfoModel.ProfileImage;
                ViewModelCall.UserBasicInfoModel = friendInfoModel;
                CallData.UserTableID = friendInfoModel.ShortInfoModel.UserTableID;
            }
            else
            {
                UserBasicInfoModel userinfo = new UserBasicInfoModel();
                ViewModelCall.UserBasicInfoModel = userinfo;
            }

            if (!CallData.IsIncomming)
            {
                if (friendInfoModel != null)
                {
                    blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(CallData.UserTableID);
                    if (HelperMethods.HasFriendCallPermission(friendInfoModel, blockModel))
                    {
                        if (!HelperMethods.HasFriendCallPermission(friendInfoModel, blockModel, out accessType))
                        {
                            if (HelperMethods.ShowBlockWarning(friendInfoModel, accessType))
                                msg = NotificationMessages.ACCESS_DENIED;// cancel clicked
                            else
                            {
                                HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, friendInfoModel, blockModel, (status) =>
                                {
                                    if (status == false) return 0;
                                    if (CallData != null)
                                    {
                                        CallData.FullName = friendInfoModel.ShortInfoModel.FullName;
                                        CallData.ProfileImage = friendInfoModel.ShortInfoModel.ProfileImage;
                                    }
                                    return 1;
                                });
                            }
                        }
                    }
                    else msg = NotificationMessages.ACCESS_DENIED;

                }
                ViewModelCall.UiType = ViewConstants.OUTGOING;
                ViewModelCall.CallState = CallStates.UA_OUTGOING_CALL;
                ViewModelCall.Duration = NotificationMessages.TEXT_AUTHENTICATING;
            }
            else
            {
                ViewModelCall.UiType = ViewConstants.INCOMMING;
                ViewModelCall.CallState = CallStates.UA_INCOMING_CALL;
                ViewModelCall.Duration = NotificationMessages.TEXT_INCOMING_CALL;
                friendInfoModel.ShortInfoModel.Presence = CallData.Presence;
            }

            StreamHelpers.OnRingCallStartInterrupt(CallData.IsIncomming);

            if (msg == null)
            {
                worker.ReportProgress(5);
                if (!CallData.IsIncomming)
                {
                    CallData.ErrorMessage = null;
                    CallerDTO callerDto = sendAuthRegisterRequest();
                    if (callerDto.IsSuccess)
                    {
                        MainSwitcher.CallController.CallData = callerDto;
                        CallHelperMethods.ChangeDurationText(NotificationMessages.TEXT_CONNECTING);
                        bool libLoaded = CallProperty.isCallSdkLoaded();
                        if (!libLoaded) CallHelperMethods.InitCallSDK();
                        CallHelperMethods.Register(CallUserType.Caller, CallData.UserTableID, CallData.CallServerIP, CallData.CallRegPort, CallData.FullName, CallData.Device, CallData.Presence, CallData.Mood, false, true, CallData.ApplicationType, CallData.DeviceToken, CallData.CallID, CallData.Time, false, 0, callType, CallData.P2PCall, 0, false);
                    }
                    else msg = callerDto.ErrorMessage;
                }

                if (callType == CallMediaType.Video) ViewModelCall.IsVideoCall = true;

                if (CallData.ProfileImage != null) ViewModelCall.UserBasicInfoModel.ShortInfoModel.ProfileImage = CallData.ProfileImage;
                if (CallData.FullName != null) ViewModelCall.UserBasicInfoModel.ShortInfoModel.FullName = CallData.FullName;
                if (CallData.UserTableID > 0) ViewModelCall.UserBasicInfoModel.ShortInfoModel.UserTableID = CallData.UserTableID;

                if (msg == null)
                {
                    playRingtoneOrProgress();
                    worker.ReportProgress(10);
                    Stopwatch stopWatch = null;
                    while (!MainSwitcher.CallController.IsCallEnded)
                    {
                        try
                        {
                            if (callConnected)
                            {
                                stopWatch = new Stopwatch();
                                stopWatch.Start();
                                OnTimedEvent();
                                if (increment % 5 == 0)
                                {
                                    ///<summary>
                                    ///Check if Mainswitcher is Inactive and open mini call window iff window is inactive
                                    ///</summary>
                                    worker.ReportProgress(20);

                                    ///<summary>
                                    ///Change Network Strength
                                    ///</summary>
                                    ///
                                    worker.ReportProgress(25);
                                    increment = 0;
                                }
                                ///<summary>
                                ///Send Empty (all data in array fillup with 0) PCM data to user 
                                ///if Audio device problem (CallConstants.LAST_RTP_SENT_TIME)
                                ///</summary>
                                currentTime = ModelUtility.CurrentTimeMillis();
                                long diff = currentTime - CallConstants.LAST_RTP_SENT_TIME;
                                if (diff > MaxNoRTP)
                                {
                                    MainSwitcher.CallController.SendEmptyPCMData();
                                    MainSwitcher.CallController.SendEmptyPCMData();
                                }

                                ///<summary>
                                ///Calcualte Exact 1 Second Delay for duration
                                ///</summary>

                                stopWatch.Stop();
                                var sleepTime = 1000;
                                if (stopWatch.ElapsedMilliseconds <= sleepTime) sleepTime = sleepTime - (int)stopWatch.ElapsedMilliseconds;
                                else sleepTime = 0;
                                System.Threading.Thread.Sleep(sleepTime);
                                increment++;
                            }
                            else
                            {
                                if (ViewModelCall.CallState == CallStates.UA_ONCALL || ViewModelCall.CallState == CallStates.HOLD_CALL)
                                {
                                    callConnected = true;
                                    if (CallData != null) CallData.CallSessionID = CallProperty.getSessionId();
                                    worker.ReportProgress(30);
                                }
                                else if (ViewModelCall.CallState == CallStates.UA_CALL_ACCEPTED) stopRingtoneOrProgress();
                                System.Threading.Thread.Sleep(500);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error ThreadAudioController==>" + ex.Message + "\n" + ex.StackTrace);
                        }
                        finally { }
                    }
                    stopRingtoneOrProgress();
                    worker.ReportProgress(100);
                }
                else
                {
                    e.Result = msg;
                    CallData.CallEndMessage = (string)e.Result;
                    CallData.ErrorMessage = e.Result.ToString();
                    worker.ReportProgress(100);
                }
            }
            else
            {
                e.Result = msg;
                CallData.CallEndMessage = (string)e.Result;
                CallData.ErrorMessage = e.Result.ToString();
                worker.ReportProgress(100);
            }
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                if (e.ProgressPercentage == 0)
                {
                    UIHelperMethods.ShowInformation("Failed to call! Invalid user.", "Call failed");
                    Cancel();
                }
                else if (e.ProgressPercentage == 30)
                {
                    stopRingtoneOrProgress();
                    CallHelperMethods.ProcessConnected();
                }
                else if (e.ProgressPercentage == 5 && ViewModelCall != null)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        if (ViewModelCall.UiType == ViewConstants.OUTGOING)
                        {
                            MainSwitcher.CallController.ShowCallUiInMainView();
                            ViewModelCall.LoaderImageSource = CallImgeObjects.OUTGOING_ANIMATION;
                        }
                        else
                        {
                            MainSwitcher.CallController.ShowSmallScreen();
                            ViewModelCall.LoaderImageSource = CallImgeObjects.INCOMING_CALL_ANIMATION;
                        }
                        ViewModelCall.FriendProfileSourceMini = ImageUtility.GetDetaultProfileImage(ViewModelCall.UserBasicInfoModel.ShortInfoModel);
                        ViewModelCall.FriendProfileSource = ImageUtility.GetDetaultProfileImage(ViewModelCall.UserBasicInfoModel.ShortInfoModel, 150, 150);
                        ViewModelCall.MyProfileImge = ImageUtility.GetDetaultProfileImage(RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, 150, 150);

                        ProfileImagesOfCall ff = new ProfileImagesOfCall();
                        ff.FreindProfileImages(150, 150, ViewModelCall.UserBasicInfoModel);
                        ff.MyProfileImgage(150, 150, RingIDViewModel.Instance.MyBasicInfoModel);
                    });
                    if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        HelperMethods.ActionIncreaseCallFavorite(CallData.UserTableID);
                }
                else if (e.ProgressPercentage == 10)
                {
                    //Application.Current.Dispatcher.Invoke(delegate()
                    //{
                    //    ProfileImagesOfCall ff = new ProfileImagesOfCall();
                    //    ff.FreindProfileImages(150, 150, ViewModelCall.UserBasicInfoModel);
                    //    ff.MyProfileImgage(150, 150, RingIDViewModel.Instance.MyBasicInfoModel);
                    //});
                }
                else if (e.ProgressPercentage == 20)
                {
                    if (!RingIDViewModel.Instance.WinDataModel.IsActiveWindow && ViewModelCall.ViewType == ViewConstants.POPUP)
                        RingIDViewModel.Instance.OnFriendCallChatButtonClicked(CallData.UserTableID);
                }
                else if (e.ProgressPercentage == 25)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        CallHelperMethods.ChangeNetorkStrength((NetworkStrength)CallConstants.LastNetWorkStrengthType);
                        previousNetworkStrength = CallConstants.LastNetWorkStrengthType;
                    });
                }
                else if (e.ProgressPercentage == 100)
                {
                    CallerDTO callerDto = (CallerDTO)CallData.Clone();
                    new ResetCallAll().ResetALL(callerDto, callDurationInSec);
                    MainSwitcher.CallController.ResetProperties();
                    MainSwitcher.CallController.CallData = null;
                    MainSwitcher.CallController.ViewModelCallInfo = null;
                    MainSwitcher.CallController.IsCallEnded = false;
                    StreamHelpers.OnRingCallStartInterruptOver(callerDto.IsIncomming);
                }
            }
            finally { }
        }

        #endregion "Event Trigger"

        #region "Private methods"

        private void OnTimedEvent()
        {
            if (CallData.Muted && callDurationInSec % 2 == 0)
            {
                CallHelperMethods.ChangeDurationText(NotificationMessages.TEXT_CALL_MUTED);
            }
            else
            {
                TimeSpan t = TimeSpan.FromSeconds(callDurationInSec);
                string answer = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                t.Hours,
                                t.Minutes,
                                t.Seconds,
                                t.Milliseconds);
                CallHelperMethods.ChangeDurationText(answer);
            }

            callDurationInSec++;
        }

        ///<summary>
        ///Send to server:{"pckId":"663608254478511","actn":174,"sId":"372497901838542712110010034","futId":301,"callID":"663608254478511","calT":1}
        ///Received from server:{"futId":301,"swIp":"38.127.68.57","swPr":1250,"rc":0,"sucs":true,"psnc":2,"mood":1,"dvc":1,"callID":"663608254478511",
        ///"tm":37250835167940030,"dt":"","fn":"Faiz 0375","idc":false,"apt":1,"calT":1,"p2p":2,"rpt":1}
        ///
        /// When sending push
        /// {  "futId": 37407,  "swIp": "104.193.36.137",  "swPr": 1250,  "mg": "Friend is away.\nPush notification has been sent.",  "rc": 0,  "sucs": true,  "psnc": 3,  "mood": 1,  "dvc": 2,  "callID": "6163611615001735",  "tm": 416282361776688,  "fn": "Faiz 044433",  "idc": false,  "apt": 1,  "calT": 1,  "p2p": 2,  "rpt": 1}
        /// ///</summary>
        private CallerDTO sendAuthRegisterRequest()
        {
            //        threadCallAuthRequests = new ThreadCallAuthRequests(CallData.CallID, CallData.UserTableID);
            CallerDTO callerDto2 = null;
            makeAndSendAuthRequest(out callerDto2);
            callerDto2.IsIncomming = CallData.IsIncomming;
            callerDto2.CallID = CallData.CallID;
            callerDto2.ProfileImage = CallData.ProfileImage;
            callerDto2.calT = CallData.calT;

            //  ViewModelCall.FullName = CallData.FullName;
            if (!callerDto2.IsSuccess)
            {
                if (callerDto2.Presence == StatusConstants.PRESENCE_OFFLINE)
                {
                    callerDto2.ErrorMessage = NotificationMessages.TEXT_FRIEND_OFFLINE;
                }
                else if (callerDto2.Presence == StatusConstants.PRESENCE_AWAY)
                {
                    callerDto2.ErrorMessage = NotificationMessages.TEXT_FRIEND_AWAY;
                }
                else if (callerDto2.Presence == StatusConstants.PRESENCE_ONLINE && callerDto2.Mood == StatusConstants.MOOD_DONT_DISTURB)
                {
                    callerDto2.ErrorMessage = NotificationMessages.TEXT_FRIEND_OFFLINE;
                }
                else if (callerDto2.ErrorMessage != null)
                {
                    // callerDto2.ErrorMessage = callerDto2.ErrorMessage;
                }
                else
                {
                    callerDto2.ErrorMessage = NotificationMessages.TEXT_USER_UNREACHABLE;
                }
            }
            return callerDto2;
        }

        private void makeAndSendAuthRequest(out CallerDTO caller)
        {
            JObject feedbackfields = null;
            caller = new CallerDTO();
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    pakToSend[JsonKeys.PacketId] = CallData.CallID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_SEND_REGISTER;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.FutId] = CallData.UserTableID;
                    pakToSend[JsonKeys.CallId] = CallData.CallID;
                    pakToSend[JsonKeys.CallType] = (int)callType;

                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_CALL, data);
                    Thread.Sleep(25);

                    for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    {
                        if (CallData == null || MainSwitcher.CallController.IsCallEnded)
                        {
                            caller.ErrorMessage = NotificationMessages.TEXT_CANCELED;
                            break;
                        }
                        Thread.Sleep(DefaultSettings.WAITING_TIME);
                        if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(CallData.CallID))
                        {
                            if (i % DefaultSettings.SEND_INTERVAL == 0)
                                SendToServer.SendNormalPacket((int)callType, data);
                        }
                        else
                        {
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(CallData.CallID, out feedbackfields);
                            break;
                        }
                    }
                    if (CallConstants.IN_CALL)
                    {
                        caller.ErrorMessage = NotificationMessages.TEXT_USER_IN_CALL;
                    }
                    else
                    {
                        if (feedbackfields != null)
                        {
                            CallHelperMethods.ParseCall_174_374(feedbackfields, caller);
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(CallData.CallID, out feedbackfields);
                        }
                        else
                        {
                            if (!MainSwitcher.ThreadManager().PingNow())
                            {
                                caller.ErrorMessage = NotificationMessages.INTERNET_UNAVAILABLE;
                            }
                            else
                            {
                                caller.ErrorMessage = NotificationMessages.CAN_NOT_PROCESS;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error("CallAuthRequests ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("CallAuthRequests Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
        }

        private void playRingtoneOrProgress()
        {
            try
            {
                if (waveOut == null)
                {
                    if (CallData.IsIncomming)
                    {
                        reader = new WaveFileReader(AudioFilesAndSettings.CLIP_RINGING);
                    }
                    else
                    {
                        reader = new WaveFileReader(AudioFilesAndSettings.CLIP_CALL_PROGRESS);
                    }
                    LoopStream loop = new LoopStream(reader);
                    waveOut = new WaveOut();
                    waveOut.Stop();
                    waveOut.Init(loop);
                    waveOut.Play();
                }
            }
            catch (Exception ex)
            {

                log.Error("PlayRingtoneOrProgress==>" + ex.Message + "\n" + ex.StackTrace);

            }

        }

        private void stopRingtoneOrProgress()
        {
            if (waveOut != null)
            {
                waveOut.Stop();
                waveOut.Dispose();
                waveOut = null;
            }
            if (reader != null)
            {
                reader.Flush();
            }
        }

        #endregion "Private methods"

        #region "Public Methods"

        public bool IsBWBusy()
        {
            return IsBusy;
        }

        public void Start(CallMediaType calT2)
        {
            if (IsBusy != true && CallData != null)
            {
                this.callType = calT2;
                RunWorkerAsync();
            }
            else
            {
                log.Error("call dAta null");
            }
        }

        public void Cancel()
        {
            if (WorkerSupportsCancellation == true)
            {
                CancelAsync();
            }
        }
        #endregion "Public methods"
    }
}

﻿using System;
using System.Windows.Threading;
using log4net;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.Profile.FriendProfile;
using View.Utility.Call.Settings;
using View.Utility.Call.ViewModels;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.Recent;
using View.ViewModel;

namespace View.Utility.Call
{
    public class ResetCallAll
    {
        #region "Private Fields"
        private readonly ILog logger = LogManager.GetLogger(typeof(ResetCallAll).Name);
        long durationInSecond = 0;

        CallerDTO callerDto;

        #endregion "Private Fields"

        #region"Properties"

        public VMCallInfo ViewModelCall
        {
            get
            {
                return MainSwitcher.CallController.ViewModelCallInfo;
            }
        }
        #endregion "Properties"

        #region "Utility methods"

        private void resetUI()
        {
            ///STA becauce of UI
            System.Windows.Application.Current.Dispatcher.Invoke(delegate()
            {
                ViewModelCall.EndOrCancelCall();
                if (callerDto.ErrorMessage != null && callerDto.ErrorMessage.Length > 0)
                {
                    UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, callerDto.ErrorMessage);
                }
            }, DispatcherPriority.Send);
        }

        public void ResetALL(CallerDTO dto, long durationInSeconds)
        {
            this.callerDto = dto;
            this.durationInSecond = durationInSeconds;
            ViewModelCall.CallState = CallStates.UA_IDLE;
            try
            {
                int viewType = ViewModelCall.ViewType;
                CallHelperMethods.StopPlayerAndRecorder();
                CallHelperMethods.StopWebCam(0);
                resetUI();
                if (ViewModelCall.IsNeedToSendEndSignal)
                {
                    CallHelperMethods.SendCancelSignal(callerDto, durationInSecond);
                }
                AddCallLogData(viewType);
                if (dto.IsNeedToPlayEndCallSound)
                {
                    CallHelperMethods.PlayClipOffSound();
                }
                CallDictionaries.Instance.CALLERS_INFO_DICTIONARY.TryRemove(callerDto.CallID);
            }
            catch (Exception)
            {
                logger.Error("Exception While Resting");
            }
            finally
            {
                CallConstants.IN_CALL = false;
            }
        }

        private void AddCallLogData(int viewType)
        {
            try
            {
                int calltype = 0;
                int callCategory = callerDto.calT;
                long duration = durationInSecond * 1000;

                if (duration < 1 && callerDto.IsIncomming)
                {
                    calltype = XamlStaticValues.CALL_TYPE_MISS_CALL;
                }
                else if (duration < 1 && !callerDto.IsIncomming)
                {
                    calltype = XamlStaticValues.CALL_TYPE_OUTGOING;
                }
                else if (duration > 0 && !callerDto.IsIncomming)
                {
                    calltype = XamlStaticValues.CALL_TYPE_OUTGOING;
                }
                else
                {
                    calltype = XamlStaticValues.CALL_TYPE_INCOMING;
                }

                CallLogDTO callLogDTO = new CallLogDTO();
                callLogDTO.CallDuration = duration;
                callLogDTO.CallingTime = ChatService.GetServerTime();
                callLogDTO.FriendTableID = callerDto.UserTableID;
                callLogDTO.CallCategory = callCategory;
                callLogDTO.CallType = calltype;
                callLogDTO.CallID = callerDto.CallID;
                callLogDTO.Message = callerDto.CallEndMessage;
                callLogDTO.CallLogID = callLogDTO.FriendTableID + "-" + callLogDTO.CallCategory + "-" + callLogDTO.CallType;

                UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(callLogDTO.FriendTableID);
                UserBasicInfoModel friendInfoModel = ucFriendChatCall != null ? ucFriendChatCall.FriendBasicInfoModel : RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(callLogDTO.FriendTableID);

                bool isNotifiable = !ChatHelpers.IsDontDisturbMood() && callerDto.IsIncomming;
                bool isChatViewInactive = UIHelperMethods.IsViewActivate(ucFriendChatCall) == false;
                bool isChatViewHiddenInactive = isChatViewInactive || HelperMethods.IsViewVisible(ucFriendChatCall) == false;
                bool isViewAtTop = HelperMethods.IsViewAtTop(ucFriendChatCall);
                bool isUnread = friendInfoModel.ShortInfoModel.ImNotificationEnabled && isNotifiable && (isChatViewHiddenInactive || isViewAtTop);

                callLogDTO.IsUnread = isUnread;
                RecentChatCallActivityDAO.InsertCallLogDTO(callLogDTO);

                RecentDTO recentDTO = new RecentDTO();
                recentDTO.ContactID = callLogDTO.FriendTableID.ToString();
                recentDTO.FriendTableID = callLogDTO.FriendTableID;
                recentDTO.Time = callLogDTO.CallingTime;
                recentDTO.IsMoveToButtom = !isViewAtTop;
                recentDTO.CallLog = callLogDTO;

                if (isUnread)
                {
                    ChatHelpers.AddUnreadCall(recentDTO.ContactID, recentDTO.CallLog.CallID, recentDTO.CallLog.CallingTime);
                }

                if (ucFriendChatCall != null && ucFriendChatCall.IsVisible)
                {
                    RecentLoadUtility.LoadRecentData(recentDTO);
                }

                CallLogLoadUtility.LoadRecentData(recentDTO);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("AddCallLogData exception " + e.Message + "\n" + e.StackTrace);
            }
        }

        #endregion "Utility methods"
    }
}

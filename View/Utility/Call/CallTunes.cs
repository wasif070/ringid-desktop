﻿using System;
using log4net;
using NAudio.Wave;
using View.Utility.audio;

namespace View.Utility.Call
{
    public class CallTunes
    {
        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(CallTunes).Name);
        private WaveOut waveOut;
        private WaveFileReader reader;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Private methods"
        #endregion "Private methods"

        #region "Public Methods"

        public void PlayProgressTune()
        {
            try
            {
                reader = new WaveFileReader(AudioFilesAndSettings.CLIP_CALL_PROGRESS);
                LoopStream loop = new LoopStream(reader);
                waveOut = new WaveOut();
                waveOut.Stop();
                waveOut.Init(loop);
                waveOut.Play();
            }
            catch (Exception)
            {
            }
        }

        public void PlayRingTune()
        {
            try
            {
                reader = new WaveFileReader(AudioFilesAndSettings.CLIP_RINGING);
                LoopStream loop = new LoopStream(reader);
                waveOut = new WaveOut();
                waveOut.Stop();
                waveOut.Init(loop);
                waveOut.Play();
            }
            catch (Exception ex)
            {
                log.Error("PlayRingtoneOrProgress==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void PlayCallEndTune()
        {
            AudioFilesAndSettings.Play(AudioFilesAndSettings.CLIP_OFF_TUNE);
        }

        public void StopTune()
        {
            if (waveOut != null)
            {
                waveOut.Stop();
                waveOut.Dispose();
                waveOut = null;
            }
            if (reader != null)
                reader.Flush();
        }
        #endregion "Public methods"
    }
}

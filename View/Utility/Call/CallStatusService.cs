﻿using System;
using System.Diagnostics;
using Models.Constants;
using Models.Utility;
using View.Utility.Call.Settings;
using View.ViewModel;

namespace View.Utility.Call
{
    public class CallStatusService
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(CallStatusService).Name);
        private bool running = false;
        long callDurationInSec = 0;
        public event DelegateBoolString OnNeedToCloseUI;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                Stopwatch stopWatch = null;
                int increment = 0;
                long currentTime = 0;
                long MaxNoRTP = 5000;
                callDurationInSec = 0;
                //changeProfileImages();
                while (CallStates.CurrentCallState != CallStates.UA_IDLE)
                {
                    try
                    {
                        if (CallStates.CurrentCallState == CallStates.UA_ONCALL || CallStates.CurrentCallState == CallStates.HOLD_CALL)
                        {
                            stopWatch = new Stopwatch();
                            stopWatch.Start();
                            OnTimedEvent();
                            if (increment % 5 == 0)
                            {
                                OpenIndividiulwindow();
                                changeNetworkstrengthImage();
                                increment = 0;
                            }
                            ///<summary>
                            ///Send Empty (all data in array fillup with 0) PCM data to user 
                            ///if Audio device problem (CallConstants.LAST_RTP_SENT_TIME)
                            ///</summary>
                            currentTime = ModelUtility.CurrentTimeMillis();
                            long diff = currentTime - CallConstants.LAST_RTP_SENT_TIME;
                            if (diff > MaxNoRTP)
                            {
                                //MainSwitcher.CallController.SendEmptyPCMData();
                                //MainSwitcher.CallController.SendEmptyPCMData();
                            }

                            ///<summary>
                            ///Calcualte Exact 1 Second Delay for duration
                            ///</summary>
                            stopWatch.Stop();
                            var sleepTime = 1000;
                            if (stopWatch.ElapsedMilliseconds <= sleepTime) sleepTime = sleepTime - (int)stopWatch.ElapsedMilliseconds;
                            else sleepTime = 0;
                            System.Threading.Thread.Sleep(sleepTime);
                            increment++;
                        }
                        else
                        {
                            Console.WriteLine("increment==>" + increment);
                            if (increment >= 60)
                                CallStates.CurrentCallState = CallStates.UA_IDLE;
                            else
                                increment++;
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error ThreadAudioController==>" + ex.Message + "\n" + ex.StackTrace);
                    }
                    finally { }
                }
                closeCallUI();
                RingIDViewModel.Instance.VMCall = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                running = false;
            }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            CallStates.CurrentCallState = CallStates.UA_IDLE;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"

        #region "Utiity methods"

        private void OnTimedEvent()
        {
            if (CallStates.IsMuted && callDurationInSec % 2 == 0)
            {
                changeDuration(NotificationMessages.TEXT_CALL_MUTED);
            }
            else
            {
                TimeSpan t = TimeSpan.FromSeconds(callDurationInSec);
                string answer = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                t.Hours,
                                t.Minutes,
                                t.Seconds,
                                t.Milliseconds);
                changeDuration(answer);
            }
            callDurationInSec++;
        }

        ///<summary>
        ///Change Network Strength
        ///</summary>
        ///
        private void changeNetworkstrengthImage() { }

        private void changeDuration() { }

        ///<summary>
        ///Check if Mainswitcher is Inactive and open mini call window iff window is inactive
        ///</summary>
        private void OpenIndividiulwindow() { }

        private void closeCallUI()
        {
            System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
            {
                if (OnNeedToCloseUI != null)
                    OnNeedToCloseUI(true, "Exit");
            });
        }

        private void changeDuration(string duration)
        {
            RingIDViewModel.Instance.VMCall.Duration = duration;
        }
        #endregion//Utility methods
    }
}

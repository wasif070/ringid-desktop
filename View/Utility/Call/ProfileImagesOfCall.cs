﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Media;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.Utility.Call.ViewModels;

namespace View.Utility.Call
{
    class ProfileImagesOfCall
    {

        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(ProfileImagesOfCall).Name);
        private Bitmap bitmapImage = null;
        #endregion "Private Fields"

        #region "Properties"

        public VMCallInfo ViewModelCall
        {
            get
            {
                return MainSwitcher.CallController.ViewModelCallInfo;
            }
        }
        #endregion "Properties"

        public void MyProfileImgage(int width, int height, UserBasicInfoModel userinfo)
        {

            if (ViewModelCall != null)
            {
                //  ViewModelCall.MyProfileImge = ImageUtility.GetDetaultProfileImage(userinfo.ShortInfoModel, width, height, 35);
                String url = ServerAndPortSettings.GetImageServerResourceURL + userinfo.ShortInfoModel.ProfileImage;
                if (!String.IsNullOrEmpty(userinfo.ShortInfoModel.ProfileImage) && url.Contains("/"))
                {
                    string convertedUrl = HelperMethods.ConvertUrlToName(userinfo.ShortInfoModel.ProfileImage, ImageUtility.IMG_CROP);
                    string dir = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    if (!File.Exists(dir))
                    {

                        try
                        {

                            //DefaultSettings.IsInternetAvailable = PingInServer.CheckPingServer();
                            if (DefaultSettings.IsInternetAvailable)
                            {
                                Uri uriAddress = new Uri(url, UriKind.RelativeOrAbsolute);
                                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriAddress);
                                request.Headers.Add("x-app-version", AppConfig.VERSION_PC);
                                request.Proxy = null;
                                HttpWebResponse response = null;
                                String contentType = String.Empty;
                                try
                                {
                                    response = (HttpWebResponse)request.GetResponse();
                                    if ((response.StatusCode == HttpStatusCode.OK ||
                                        response.StatusCode == HttpStatusCode.Moved ||
                                        response.StatusCode == HttpStatusCode.Redirect) &&
                                        response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                                    {
                                        System.Net.WebRequest request2 = System.Net.WebRequest.Create(uriAddress);
                                        System.Net.WebResponse response2 = request.GetResponse();
                                        System.IO.Stream responseStream = response.GetResponseStream();
                                        bitmapImage = new Bitmap(responseStream);
                                        bitmapImage.Save(dir, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    if (DefaultSettings.DEBUG)
                                        log.Error("Exception in getting response  => " + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message + "==>" + convertedUrl);
                                }
                                finally
                                {
                                    if (response != null)
                                    {
                                        response.Close();
                                        response = null;
                                    }
                                    request.Abort();
                                    request = null;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (DefaultSettings.DEBUG)
                                log.Error("DownloadfullImage => " + ex.Message + "\n" + ex.StackTrace);
                        }
                    }
                    else
                    {
                        using (System.IO.Stream BitmapStream = File.Open(dir, System.IO.FileMode.Open, FileAccess.ReadWrite))
                        {
                            Image img = Image.FromStream(BitmapStream);
                            bitmapImage = new Bitmap(img);
                            //...do whatever
                        }
                    }

                    if (ViewModelCall != null && bitmapImage != null)
                    {
                        ViewModelCall.MyProfileImge = ImageUtility.ConvertBitmapToBitmapImage(bitmapImage);
                        //VoiceVideoCallController.GetVoiceVideoCallController().GetCallUIHelpers.View_UCCallMainPanel.MyProfileImge = ImageUtility.ConvertBitmapToBitmapImage(bitmapImage);

                    }
                    if (bitmapImage != null)
                        bitmapImage.Dispose();
                }
            }
        }

        public void FreindProfileImages(int width, int height, UserBasicInfoModel userinfo)
        {

            if (ViewModelCall != null)
            {
                //    ViewModelCall.FriendProfileSource = ImageUtility.GetDetaultProfileImage(userinfo.ShortInfoModel, width, height, 35);
                String url = ServerAndPortSettings.GetImageServerResourceURL + userinfo.ShortInfoModel.ProfileImage;
                if (!String.IsNullOrEmpty(userinfo.ShortInfoModel.ProfileImage) && url.Contains("/"))
                {
                    string convertedUrl = HelperMethods.ConvertUrlToName(userinfo.ShortInfoModel.ProfileImage, ImageUtility.IMG_THUMB);
                    string dir = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                    if (!File.Exists(dir))
                    {
                        try
                        {
                            if (DefaultSettings.IsInternetAvailable)
                            {
                                Uri uriAddress = new Uri(url, UriKind.RelativeOrAbsolute);
                                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriAddress);
                                request.Headers.Add("x-app-version", AppConfig.VERSION_PC);
                                request.Proxy = null;
                                HttpWebResponse response = null;
                                String contentType = String.Empty;
                                try
                                {
                                    response = (HttpWebResponse)request.GetResponse();
                                    if ((response.StatusCode == HttpStatusCode.OK ||
                                        response.StatusCode == HttpStatusCode.Moved ||
                                        response.StatusCode == HttpStatusCode.Redirect) &&
                                        response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                                    {
                                        System.Net.WebRequest request2 = System.Net.WebRequest.Create(uriAddress);
                                        System.Net.WebResponse response2 = request.GetResponse();
                                        System.IO.Stream responseStream = response.GetResponseStream();
                                        bitmapImage = new Bitmap(responseStream);
                                        bitmapImage.Save(dir, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    if (DefaultSettings.DEBUG)
                                        log.Error("Exception in getting response  => " + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message + "==>" + convertedUrl);
                                }
                                finally
                                {
                                    if (response != null)
                                    {
                                        response.Close();
                                        response = null;
                                    }
                                    request.Abort();
                                    request = null;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            if (DefaultSettings.DEBUG)
                                log.Error("DownloadfullImage => " + ex.Message + "\n" + ex.StackTrace);
                        }
                    }
                    else
                    {
                        using (System.IO.Stream BitmapStream = File.Open(dir, System.IO.FileMode.Open, FileAccess.ReadWrite))
                        {
                            Image img = Image.FromStream(BitmapStream);
                            bitmapImage = new Bitmap(img);
                        }
                    }

                    if (MainSwitcher.CallController.CallData != null && bitmapImage != null)
                    {
                        ViewModelCall.FriendProfileSourceMini = ImageUtility.ConvertBitmapToBitmapImage(bitmapImage);
                        ViewModelCall.FriendProfileSource = ImageUtility.ConvertBitmapToBitmapImage(bitmapImage);
                    }
                    if (bitmapImage != null)
                    {
                        bitmapImage.Dispose();
                    }

                }
            }
        }

    }
}

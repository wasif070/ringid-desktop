﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using callsdkwrapper;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility.Chat;
using Newtonsoft.Json.Linq;
using View.UI;
using View.Utility.audio;
using View.Utility.Call.Settings;
using View.Utility.Call.Video;
using View.Utility.Chat;
using View.Utility.Stream.Utils;
using View.ViewModel;
namespace View.Utility.Call
{
    public class CallHelperMethods
    {
        #region "Private Fields"
        private static readonly ILog logger = LogManager.GetLogger(typeof(CallHelperMethods).Name);

        private static View.UI.Call.UCCallUIInMain callUIInMain;
        private static View.UI.Call.WNCallInSeparateView callInSeparateView;
        private static CallTunes callTunes;
        private static PCMDataPlayerRecorder pcmDataPlayerRecorder;
        private static WebcamRGBData webcamRGBData;
        #endregion "Private Fields"

        #region "Private Methods"

        private static void startDuration()
        {
            MainSwitcher.CallController.StartACall();
        }

        private static void addErrorLog(string msg)
        {
            if (logger.IsErrorEnabled)
            {
                logger.Error(msg);
            }
        }

        #endregion "Private Methods"

        #region "Utility Methods"

        public static void InitCallSDK()
        {
            try
            {
                if (MainSwitcher.CallController == null)
                {
                    MainSwitcher.CallController = new CallController();
                }
                MainSwitcher.CallController.InitCallSDK();
                System.Threading.Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                logger.Error("**********************Failded To connect CallSDK ***********************" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void StartAudioPlayerAndRecorder(bool callEndIfAudioDeviceProblem)
        {
            MainSwitcher.CallController.StartAudioPlayerAndRecorder(callEndIfAudioDeviceProblem);
        }

        public static void StopPlayerAndRecorder()
        {
            MainSwitcher.CallController.StopPlayerAndRecorder();
        }

        public static void PlayClipOffSound()
        {
            AudioFilesAndSettings.Play(AudioFilesAndSettings.CLIP_OFF_TUNE);
        }

        public static void ProcessConnected()
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                StartAudioPlayerAndRecorder(true);
            });
            startDuration();
        }

        public static void SendCancelSignal(CallerDTO callerDto, long durationInSeconds)
        {
            if (durationInSeconds > 0)
            {
                CallHelperMethods.Bye(callerDto.UserTableID);
            }
            else if (callerDto.BusyMessageToSend != null && callerDto.BusyMessageToSend.Length > 0)
            {
                string chatPacketID = View.Utility.Chat.Service.ChatService.GeneratePacketID().PacketID;
                CallHelperMethods.BusyWithMessage(callerDto.UserTableID, callerDto.BusyMessageToSend, chatPacketID);
                ChatHelpers.AddCallMessage(callerDto.UserTableID, DefaultSettings.LOGIN_TABLE_ID, callerDto.BusyMessageToSend, chatPacketID);
            }
            else
            {
                if (callerDto.IsIncomming)
                {
                    CallHelperMethods.Busy(callerDto.UserTableID);
                }
                else
                {
                    CallHelperMethods.Cancel(callerDto.UserTableID);
                }
            }
        }

        public static bool RemoveAChildFromParent(System.Windows.Controls.Image element)
        {
            try
            {
                Panel parentPanel = (Panel)element.Parent;
                if (parentPanel != null && parentPanel.Children.Count > 0)
                {
                    parentPanel.Children.Remove(element);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static bool IsVideoDeviceExists()
        {
            return AforgeWebCamCapture.IsVideoDeviceExists();
        }

        public static void ParseCall_174_374(JObject _JobjFromResponse, CallerDTO caller)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null)
                {
                    caller.IsSuccess = (bool)_JobjFromResponse[JsonKeys.Success];
                }
                if (_JobjFromResponse[JsonKeys.AppType] != null)
                {
                    caller.ApplicationType = (int)_JobjFromResponse[JsonKeys.AppType];
                }
                if (_JobjFromResponse[JsonKeys.CallId] != null)
                {
                    caller.CallID = _JobjFromResponse[JsonKeys.CallId].ToString();
                }
                if (_JobjFromResponse[JsonKeys.CallType] != null)
                {
                    caller.calT = int.Parse(_JobjFromResponse[JsonKeys.CallType].ToString());
                }
                if (_JobjFromResponse[JsonKeys.Device] != null)
                {
                    caller.Device = (int)_JobjFromResponse[JsonKeys.Device];
                }
                if (_JobjFromResponse[JsonKeys.DeviceToken] != null)
                {
                    caller.DeviceToken = _JobjFromResponse[JsonKeys.DeviceToken].ToString();
                }
                if (_JobjFromResponse[JsonKeys.FutId] != null)
                {
                    caller.UserTableID = long.Parse(_JobjFromResponse[JsonKeys.FutId].ToString());
                }
                if (_JobjFromResponse[JsonKeys.FullName] != null)
                {
                    caller.FullName = _JobjFromResponse[JsonKeys.FullName].ToString();
                }
                if (_JobjFromResponse[JsonKeys.Mood] != null)
                {
                    caller.Mood = (int)_JobjFromResponse[JsonKeys.Mood];
                }
                if (_JobjFromResponse[JsonKeys.Message] != null)
                {
                    caller.ErrorMessage = _JobjFromResponse[JsonKeys.Message].ToString();
                }
                if (_JobjFromResponse[JsonKeys.P2PCall] != null)
                {
                    caller.P2PCall = (int)_JobjFromResponse[JsonKeys.P2PCall];
                }
                if (_JobjFromResponse[JsonKeys.RemotePushType] != null)
                {
                    caller.RemotePushType = (int)_JobjFromResponse[JsonKeys.RemotePushType];
                }
                if (_JobjFromResponse[JsonKeys.Presence] != null)
                {
                    caller.Presence = (int)_JobjFromResponse[JsonKeys.Presence];
                }
                if (_JobjFromResponse[JsonKeys.ReasonCode] != null)
                {
                    caller.ReasonCode = (int)_JobjFromResponse[JsonKeys.ReasonCode];
                }
                if (_JobjFromResponse[JsonKeys.SwitchIp] != null)
                {
                    caller.CallServerIP = _JobjFromResponse[JsonKeys.SwitchIp].ToString();
                }
                if (_JobjFromResponse[JsonKeys.SwitchPort] != null)
                {
                    caller.CallRegPort = (int)_JobjFromResponse[JsonKeys.SwitchPort];
                }
                if (_JobjFromResponse[JsonKeys.Time] != null)
                {
                    caller.Time = long.Parse(_JobjFromResponse[JsonKeys.Time].ToString());
                }
            }
            catch (Exception e)
            {
                logger.Error("ParseCall_174_374 ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public static void CancelButtonAction(string busyMessage = null)
        {
            MainSwitcher.CallController.CancelButtonAction(busyMessage);
        }

        public static void InitOnCall()
        {
            RingIDViewModel.Instance.VMCall.CallState = CallStates.UA_ONCALL;
            CallStates.CurrentCallState = CallStates.UA_ONCALL;
            stopCallTune();
            RunAudioPlayerRecorder();
            if (RingIDViewModel.Instance.VMCall.IsNeedToShowMyVideo)
                StartWebcam();
        }

        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);
        #endregion "Utility Methods"

        #region "Modify UI"

        public static float GetAngale(int orientation)
        {
            float angle = 0;
            switch (orientation)
            {
                case 1:
                    angle = -90;
                    break;
                case 2:
                    angle = 180;
                    break;
                case 3:
                    angle = 90;
                    break;
            }
            return angle;
        }

        public static Bitmap RotateImageByAngle(System.Drawing.Image oldBitmap, float angle)
        {
            var newBitmap = new Bitmap(oldBitmap.Width, oldBitmap.Height);
            newBitmap.SetResolution(oldBitmap.HorizontalResolution, oldBitmap.VerticalResolution);
            var graphics = Graphics.FromImage(newBitmap);
            graphics.TranslateTransform((float)oldBitmap.Width / 2, (float)oldBitmap.Height / 2);
            graphics.RotateTransform(angle);
            graphics.TranslateTransform(-(float)oldBitmap.Width / 2, -(float)oldBitmap.Height / 2);
            graphics.DrawImage(oldBitmap, new System.Drawing.Point(0, 0));
            return newBitmap;
        }

        public static void ChangeNetorkStrength(NetworkStrength strength)
        {
            if (MainSwitcher.CallController.ViewModelCallInfo != null)
                switch (strength)
                {
                    case NetworkStrength.High:
                        MainSwitcher.CallController.ViewModelCallInfo.NetWorkImage = CallImgeObjects.NETWORK_STRENGTH4;
                        MainSwitcher.CallController.ViewModelCallInfo.NetWorkStrength = "Excellent";
                        break;
                    case NetworkStrength.Low:
                        MainSwitcher.CallController.ViewModelCallInfo.NetWorkImage = CallImgeObjects.NETWORK_STRENGTH3;
                        MainSwitcher.CallController.ViewModelCallInfo.NetWorkStrength = "Average";
                        break;
                    case NetworkStrength.NotReachable:
                        MainSwitcher.CallController.ViewModelCallInfo.NetWorkImage = CallImgeObjects.NETWORK_STRENGTH0;
                        MainSwitcher.CallController.ViewModelCallInfo.NetWorkStrength = "Reconnecting...";
                        //  CallUIManager.ChangeNetworkStrengthAndColor("Reconnecting", CallImgeObjects.NETWORK_STRENGTH0);
                        break;
                    default:
                        MainSwitcher.CallController.ViewModelCallInfo.NetWorkImage = CallImgeObjects.NETWORK_STRENGTH1;
                        MainSwitcher.CallController.ViewModelCallInfo.NetWorkStrength = "Poor";
                        //CallUIManager.ChangeNetworkStrengthAndColor("Poor", CallImgeObjects.NETWORK_STRENGTH1);
                        break;
                }
        }

        public static void StartCallManger(CallMediaType callType) //voice=1 ,video=2
        {
            if (MainSwitcher.CallController.CallManger == null) MainSwitcher.CallController.CallManger = new BWCallManger();
            MainSwitcher.CallController.CallManger.Start(callType);
        }

        public static void ChangeDurationText(string text)
        {
            if (MainSwitcher.CallController.ViewModelCallInfo != null) MainSwitcher.CallController.ViewModelCallInfo.Duration = text;
        }

        public static void ShowCallUiInMainView(CallerDTO dto, CallMediaType callType)
        {

            if (dto != null && RingIDViewModel.Instance.WinDataModel.IsSignIn)
            {
                if (CallStates.CurrentCallState == CallStates.UA_IDLE)
                {
                    RingIDViewModel.Instance.SetCallDataModel(dto);
                    RingIDViewModel.Instance.VMCall.CallState = CallStates.UA_OUTGOING_CALL;
                    CallStates.CurrentCallState = CallStates.UA_OUTGOING_CALL;
                    CallStatusService callStatusService = new CallStatusService();
                    callStatusService.OnNeedToCloseUI += (needToSwitchSeparateWindow, msg) =>
                    {
                        closeCallUI();
                    };
                    callStatusService.StartThread();
                    if (callTunes == null) callTunes = new CallTunes();
                    callTunes.PlayProgressTune();
                }
                if (callUIInMain == null)
                {
                    callUIInMain = new View.UI.Call.UCCallUIInMain();
                    callUIInMain.OnRemovedFromUI += (isNeedToShowSeparateWindow) =>
                    {
                        try
                        {
                            if (CallStates.CurrentCallState == CallStates.UA_IDLE)
                            {
                                CallStates.CurrentCallState = CallStates.UA_IDLE;
                                if (callTunes != null) callTunes.StopTune();
                                if (callInSeparateView != null)
                                {
                                    callInSeparateView.Close();
                                }
                                callUIInMain = null;
                                if (RingIDViewModel.Instance.VMCall != null)
                                    RingIDViewModel.Instance.VMCall.IsInFullScreen = false;
                                if (MiddlePanelSwitcher.pageSwitcher.Content == null)
                                {
                                    if (RingIDViewModel.Instance.VMCall != null && RingIDViewModel.Instance.VMCall.UserBasicInfoModel.ShortInfoModel.UserTableID > 0)
                                        View.ViewModel.RingIDViewModel.Instance.OnFriendCallChatButtonClicked(RingIDViewModel.Instance.VMCall.UserBasicInfoModel.ShortInfoModel.UserTableID);
                                    else
                                        RingIDViewModel.Instance.OnAllFeedsClicked(new object());
                                }
                            }
                            else if (isNeedToShowSeparateWindow)
                            {
                                ShowCallSeparateWindow(dto, callType);
                                callUIInMain = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.StackTrace);
                        }
                    };
                }
                callUIInMain.AddInMiddlePanel();
            }
            //  else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.ALREADY_IN_CALL);
            else UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.CAN_NOT_CALL_RIGHT_NOW);
        }

        public static void ShowCallSeparateWindow(CallerDTO dto, CallMediaType callType)
        {
            if (CallStates.CurrentCallState == CallStates.UA_IDLE)
            {
                if (callType == CallMediaType.Video) RingIDViewModel.Instance.VMCall.IsNeedToShowMyVideo = true;
                RingIDViewModel.Instance.SetCallDataModel(dto);
                RingIDViewModel.Instance.VMCall.CallState = CallStates.UA_INCOMING_CALL;
                CallStates.CurrentCallState = CallStates.UA_INCOMING_CALL;
                CallStatusService callStatusService = new CallStatusService();
                callStatusService.OnNeedToCloseUI += (needToSwitchSeparateWindow, msg) =>
                {
                    closeCallUI();
                };
                callStatusService.StartThread();
                if (callTunes == null) callTunes = new CallTunes();
                callTunes.PlayRingTune();
            }
            if (callInSeparateView == null) callInSeparateView = new UI.Call.WNCallInSeparateView();
            callInSeparateView.Show();
            callInSeparateView.OnClosedWindow += (isCallEnded) =>
            {
                if (CallStates.CurrentCallState == CallStates.UA_IDLE)
                {
                    if (callUIInMain != null)
                        callUIInMain.OnEndCallCommand();
                    if (callTunes != null) callTunes.StopTune();
                }
                else
                {
                    ShowCallUiInMainView(dto, callType);
                    UIHelperMethods.FocusMainWindow();
                }
                callInSeparateView = null;
            };
        }

        private static void closeCallUI()
        {
            try
            {
                if (callTunes != null)
                    callTunes.StopTune();
                if (callUIInMain != null)
                {
                    callUIInMain.OnEndCallCommand();
                    callUIInMain = null;
                }
                if (callInSeparateView != null)
                {
                    callInSeparateView.CloseThis();
                    callInSeparateView = null;
                }
                StopAudioPlayerRecorder();
            }
            finally
            {
                CallStates.CurrentCallState = CallStates.UA_IDLE;
            }
        }

        public static void StartWebCam(long tableID)
        {
            try
            {
                if (MainSwitcher.CallController.ViewModelCallInfo != null && MainSwitcher.CallController.ViewModelCallInfo.CallState == CallStates.UA_ONCALL)
                    new ThradStartMyVideo().StartThread(tableID);
                else if (MainSwitcher.CallController.ViewModelCallInfo != null && MainSwitcher.CallController.ViewModelCallInfo.CallState == CallStates.HOLD_CALL)
                {
                    System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        UIHelperMethods.ShowWarning(NotificationMessages.NO_VIDEO_ON_HOLD, "Hold");
                        //CustomMessageBox.ShowWarning(NotificationMessages.NO_VIDEO_ON_HOLD);
                    });
                }
            }
            catch (Exception ex) { logger.Error("StartWebCam==>" + ex.StackTrace); }
        }

        public static void StopWebCam(long userTalbeId, bool byFriend = false)
        {
            if (MainSwitcher.CallController.ViewModelCallInfo != null)
            {
                new ThrdStopMyVideo().StartThread(byFriend, userTalbeId);
                MainSwitcher.CallController.ViewModelCallInfo.IsVideoStopped = true;
            }
        }

        private static void showMyRGBDataInView(byte[] array)
        {
            try
            {
                int PixelSize = 3;
                if (array == null) return;
                int stride = CallConstants.VideoSettings.FRAME_WIDTH * PixelSize;
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                int scan0 = (int)handle.AddrOfPinnedObject();
                scan0 += (CallConstants.VideoSettings.FRAME_HEIGHT - 1) * stride;
                Bitmap b = new Bitmap(CallConstants.VideoSettings.FRAME_WIDTH, CallConstants.VideoSettings.FRAME_HEIGHT, -stride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, (IntPtr)scan0);
                handle.Free();
                IntPtr hBitmap = b.GetHbitmap();
                try
                {
                    System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                   {
                       BitmapSource retval = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                       RingIDViewModel.Instance.VMCall.MiniVideoImageSource = retval;
                   }, System.Windows.Threading.DispatcherPriority.Render);
                }
                finally
                {
                    DeleteObject(hBitmap);
                    b.Dispose();
                }
            }
            finally { }
        }

        #endregion "Modify UI"

        #region "Audio/Vedio"
        private static void stopCallTune()
        {
            if (callTunes != null)
                callTunes.StopTune();
        }

        public static void RunAudioPlayerRecorder()
        {

            if (pcmDataPlayerRecorder == null) pcmDataPlayerRecorder = new PCMDataPlayerRecorder();
            pcmDataPlayerRecorder.StartPlayerRecorder();
            pcmDataPlayerRecorder.OnPCMdataAvailable += (dataBytes) =>
            {
                pcmDataPlayerRecorder.AddVoiceSampleByteToPlay(dataBytes);
            };
        }

        public static void StopAudioPlayerRecorder()
        {
            if (pcmDataPlayerRecorder != null) pcmDataPlayerRecorder.StopPlayerRecorder();
        }

        public static void StartWebcam()
        {
            if (webcamRGBData == null)
            {
                webcamRGBData = new WebcamRGBData();
                webcamRGBData.OnRGBDataFound += (rgbDataBytes) =>
                {
                    showMyRGBDataInView(rgbDataBytes);
                };
            }
            webcamRGBData.StartWebCam();
        }

        public static void StopWebcam()
        {
            if (webcamRGBData != null)
                webcamRGBData.StopWebCam();
        }

        #endregion"Audio/Video"

        #region "CallSDK Helpers"

        public static void unRegisterEventHandler()
        {
            MainSwitcher.CallController.BaseCallService.unRegisterEventHandler();
        }

        public static BaseApiStatus Register(CallUserType callUserType, long friendId, string voiceChatServerAddress, int voiceChatServerPort, string fullName, int deviceType, int onlineStatus, int mood, bool idc, bool success, int appType, string friendDeviceToken, string callId, long callTime, bool isTransferredFromAuth, long transferredFriendIdFromAuth, CallMediaType callType, int isP2PEnable, int isVoipPush, bool isGsmCallActive)
        {
            return MainSwitcher.CallController.BaseCallService.Register(callUserType, friendId, voiceChatServerAddress, voiceChatServerPort, fullName, deviceType, onlineStatus, mood, idc, success, appType, friendDeviceToken, callId, callTime, isTransferredFromAuth, transferredFriendIdFromAuth, callType, isP2PEnable, isVoipPush, isGsmCallActive);
        }

        public static BaseApiStatus Answer(long friendId, CallMediaType callType)
        {
            return MainSwitcher.CallController.BaseCallService.Answer(friendId, callType);
        }

        public static BaseApiStatus Busy(long friendId)
        {
            return MainSwitcher.CallController.BaseCallService.Busy(friendId);
        }

        public static BaseApiStatus BusyWithMessage(long friendId, string message, string packetID)
        {
            return MainSwitcher.CallController.BaseCallService.BusyWithMessage(friendId, message, packetID);
        }

        public static BaseApiStatus Cancel(long friendId)
        {
            return MainSwitcher.CallController.BaseCallService.Cancel(friendId);
        }

        public static BaseApiStatus Bye(long friendId)
        {
            return MainSwitcher.CallController.BaseCallService.Bye(friendId);
        }

        public static BaseApiStatus Hold(long friendId)
        {
            return MainSwitcher.CallController.BaseCallService.Hold(friendId);
        }

        public BaseApiStatus Unhold(long friendId)
        {
            return MainSwitcher.CallController.BaseCallService.Unhold(friendId);
        }

        public static BaseApiStatus VideoStart(long friendId)
        {
            return MainSwitcher.CallController.BaseCallService.VideoStart(friendId);
        }

        public static BaseApiStatus VideoEnd(long friendId)
        {
            return MainSwitcher.CallController.BaseCallService.VideoEnd(friendId);
        }

        public static BaseApiStatus VideoSenderSideEnd(long friendId)
        {
            return MainSwitcher.CallController.BaseCallService.VideoSenderSideEnd(friendId);
        }

        public static void VoiceChatReset()
        {
            MainSwitcher.CallController.BaseCallService.VoiceChatReset();
        }

        public static void ReinitializeVoiceChat(long userId)
        {
            MainSwitcher.CallController.BaseCallService.ReinitializeVoiceChat(userId);
        }

        public static void SetVideoResolution(int jvideoHeight, int jvideoWidth)
        {
            MainSwitcher.CallController.BaseCallService.setVideoResolution(jvideoHeight, jvideoWidth);
        }

        public static string Version()
        {
            return MainSwitcher.CallController.BaseCallService.Version();
        }

        public static int CheckDeviceCapability(long randomID, int iHeightHigh, int iWidthHigh, int iHeightLow, int iWidthLow)
        {
            return MainSwitcher.CallController.BaseCallService.CheckDeviceCapability(randomID, iHeightHigh, iWidthHigh, iHeightLow, iWidthLow);
        }

        public static int SetDeviceCapabilityResults(int iNotification, int iHeightHigh, int iWidthHigh, int iHeightLow, int iWidthLow)
        {
            return MainSwitcher.CallController.BaseCallService.SetDeviceCapabilityResults(iNotification, iHeightHigh, iWidthHigh, iHeightLow, iWidthLow);
        }

        public static void SendPCMData(byte[] rowData)
        {
            MainSwitcher.CallController.SendPCMData(rowData);
        }

        public static void ProcessMyRGBData(byte[] rgbbyte)
        {
            MainSwitcher.CallController.ProcessMyRGBData(rgbbyte);
        }

        /// <summary>
        /// add for new methods for encode and decode audio data 31-08-2016
        /// int StartAudioEncodeDecodeSession();

        ///array<short> ^EncodeAudioFrame(array<short> ^pcmData);

        ///array<short> ^DecodeAudioFrame(array<short> ^encodedAudioData);

        ///int StopAudioEncodeDecodeSession();
        /// </summary>

        public static void StartAudioEncodeDecodeSession()
        {
            MainSwitcher.CallController.BaseCallService.StartAudioEncodeDecodeSession();
        }

        public static byte[] EncodeAudioFrame(byte[] pcmData)
        {
            return MainSwitcher.CallController.BaseCallService.EncodeAudioFrame(pcmData);
        }

        public static byte[] DecodeAudioFrame(byte[] encodedAudioData)
        {
            return MainSwitcher.CallController.BaseCallService.DecodeAudioFrame(encodedAudioData);
        }

        public static void StopAudioEncodeDecodeSession()
        {
            MainSwitcher.CallController.BaseCallService.StopAudioEncodeDecodeSession();
        }

        #endregion //"CallSDK Helpers"

        #region "Live Stream"

        public static void SetStreamResolution(int Width, int Height)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::SET_STREAM_RESOLUTION".PadRight(66, ' ') + "==>   Width = " + Width + ", Height = " + Height);
#endif
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null) MainSwitcher.CallController.BaseCallService.setVideoResolution(Height, Width);
                else logger.Error("Error at SetStreamResolution() ==> CallSDK not initialized.");
            }
            catch (Exception ex) { logger.Error("Error at SetStreamResolution() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void PublisherRegister(String streamId, String publisherServerIp, int publisherServerPort, RegType regType)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::PUBLISHER_REGISTER_SENDING".PadRight(66, ' ') + "==>   streamId = " + streamId + ", serverIp = " + publisherServerIp + ", registerPort = " + publisherServerPort + ", authIP = " + ServerAndPortSettings.AUTH_SERVER_IP + ", authPort = " + ServerAndPortSettings.COMMUNICATION_PORT + ", sessionID = " + DefaultSettings.LOGIN_SESSIONID + ", regType = " + regType);
#endif
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                    {
                        BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.PublisherRegister(publisherServerIp, publisherServerPort, ServerAndPortSettings.AUTH_SERVER_IP, ServerAndPortSettings.COMMUNICATION_PORT, DefaultSettings.LOGIN_SESSIONID, streamId, (int)regType);
                        if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK)
                        {
                            logger.Error("Error at PublisherRegister() ==> ErrorCode = " + apiStatus.ErrorCode);

                            if (MainSwitcher.CallController.CallSDKCallBack != null)
                            {
                                MainSwitcher.CallController.CallSDKCallBack.onPublisherRegisterFailure(DefaultSettings.LOGIN_TABLE_ID);
                            }
                        }
                    }
                    else
                    {
                        logger.Error("Error at PublisherRegister() ==> CallSDK not initialized.");

                        if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallSDKCallBack != null)
                        {
                            MainSwitcher.CallController.CallSDKCallBack.onPublisherRegisterFailure(DefaultSettings.LOGIN_TABLE_ID);
                        }
                    }
                }
                else
                {
                    logger.Error("PublisherRegister Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);

                    if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallSDKCallBack != null)
                    {
                        MainSwitcher.CallController.CallSDKCallBack.onPublisherRegisterFailure(DefaultSettings.LOGIN_TABLE_ID);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error at PublisherRegister() ==>" + ex.Message + "\n" + ex.StackTrace);

                if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallSDKCallBack != null)
                {
                    MainSwitcher.CallController.CallSDKCallBack.onPublisherRegisterFailure(DefaultSettings.LOGIN_TABLE_ID);
                }
            }
        }

        public static void ViewerRegisterForLive(long publisherId, String PublisherServerIp, int PublisherServerPort, String ViewerServerIp, int ViewerServerPort, String fullName, String profileImage, string streamID)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::VIEWER_REGISTER_SENDING_FOR_LIVE".PadRight(66, ' ') + "==>   publisherId = " + publisherId + ", PublisherServerIp = " + PublisherServerIp + ", PublisherServerPort = " + PublisherServerPort + ", ViewerServerIp = " + ViewerServerIp + ", ViewerServerPort = " + ViewerServerPort + ", fullName = " + fullName + ", profileImage = " + profileImage + ", streamID = " + streamID);
#endif
            StreamChannel.ViewerStart();

            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                    {
                        BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.ViewerRegisterForLiveStreaming(publisherId, PublisherServerIp, PublisherServerPort, ViewerServerIp, ViewerServerPort, fullName, profileImage, streamID);
                        if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK)
                        {
                            logger.Error("Error at ViewerRegisterForLive() ==> ErrorCode = " + apiStatus.ErrorCode);

                            if (MainSwitcher.CallController.CallSDKCallBack != null)
                            {
                                MainSwitcher.CallController.CallSDKCallBack.onViewerRegisterFailure(publisherId);
                            }
                        }
                    }
                    else
                    {
                        logger.Error("Error at ViewerRegisterForLive() ==> CallSDK not initialized.");

                        if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallSDKCallBack != null)
                        {
                            MainSwitcher.CallController.CallSDKCallBack.onViewerRegisterFailure(publisherId);
                        }
                    }
                }
                else
                {
                    logger.Error("ViewerRegisterForLive Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);

                    if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallSDKCallBack != null)
                    {
                        MainSwitcher.CallController.CallSDKCallBack.onViewerRegisterFailure(publisherId);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error at ViewerRegisterForLive() ==>" + ex.Message + "\n" + ex.StackTrace);

                if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallSDKCallBack != null)
                {
                    MainSwitcher.CallController.CallSDKCallBack.onViewerRegisterFailure(publisherId);
                }
            }
        }

        public static void ViewerRegisterForChannel(long publisherId, String serverIp, int registerPort, String fullName, String profileImage, string streamID, RegType regType)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::VIEWER_REGISTER_SENDING_FOR_CHANNEL".PadRight(66, ' ') + "==>   publisherId = " + publisherId + ", serverIp = " + serverIp + ", registerPort = " + registerPort + ", fullName = " + fullName + ", profileImage = " + profileImage + ", streamID = " + streamID + ", regType = " + regType);
#endif
            StreamChannel.ViewerStart();

            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                    {
                        BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.ViewerRegister(publisherId, serverIp, registerPort, fullName, profileImage, streamID, (int)regType);
                        if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK)
                        {
                            logger.Error("Error at ViewerRegisterForChannel() ==> ErrorCode = " + apiStatus.ErrorCode);

                            if (MainSwitcher.CallController.CallSDKCallBack != null)
                            {
                                MainSwitcher.CallController.CallSDKCallBack.onViewerRegisterFailure(publisherId);
                            }
                        }
                    }
                    else
                    {
                        logger.Error("Error at ViewerRegisterForChannel() ==> CallSDK not initialized.");

                        if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallSDKCallBack != null)
                        {
                            MainSwitcher.CallController.CallSDKCallBack.onViewerRegisterFailure(publisherId);
                        }
                    }
                }
                else
                {
                    logger.Error("ViewerRegisterForChannel Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);

                    if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallSDKCallBack != null)
                    {
                        MainSwitcher.CallController.CallSDKCallBack.onViewerRegisterFailure(publisherId);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error at ViewerRegisterForChannel() ==>" + ex.Message + "\n" + ex.StackTrace);

                if (MainSwitcher.CallController != null && MainSwitcher.CallController.CallSDKCallBack != null)
                {
                    MainSwitcher.CallController.CallSDKCallBack.onViewerRegisterFailure(publisherId);
                }
            }
        }

        public static void PublisherUnregister(String streamId, long totalViewers, long totalCoins, long totalFollowers, ChatEventDelegate onComplete = null)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::PUBLISHER_UNREGISTER_SENDING".PadRight(66, ' ') + "==>  streamId = " + streamId + " streamId = " + totalViewers + ", totalCoins = " + totalCoins + ", totalFollowers = " + totalFollowers);
#endif
            StreamChannel.PublisherStop();

            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                    {
                        BaseApiStatus data = MainSwitcher.CallController.BaseCallService.PublisherUnregister((int)totalViewers, (int)totalCoins, (int)totalFollowers);
                        if (data.ErrorCode == VocieChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = streamId });
                        }
                        else
                        {
                            logger.Error("PublisherUnregister Failed ==> VocieChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                        }
                    }
                    else
                    {
                        logger.Debug("PublisherUnregister Failed => BaseCallService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                    }
                }
                else
                {
                    logger.Error("PublisherUnregister Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error at PublisherUnregister() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
            }
        }

        public static void ViewerUnregister(long publisherId, ChatEventDelegate onComplete = null)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::VIEWER_UNREGISTER_SENDING".PadRight(66, ' ') + "==>  publisherId = " + publisherId);
#endif
            StreamChannel.ViewerStop();

            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                    {
                        BaseApiStatus data = MainSwitcher.CallController.BaseCallService.ViewerUnregister(publisherId);
                        if (data.ErrorCode == VocieChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID });
                        }
                        else
                        {
                            logger.Error("ViewerUnregister Failed ==> VocieChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                        }
                    }
                    else
                    {
                        logger.Debug("ViewerUnregister Failed => BaseCallService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                    }
                }
                else
                {
                    logger.Error("ViewerUnregister Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error at ViewerUnregister() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
            }
        }

        public static void StreamPlayPause(long sessionId, PlayPauseType playPauseType)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::STREAM_PLAY_PAUSE".PadRight(66, ' ') + "==>   sessionId = " + sessionId + ", playPauseType = " + playPauseType);
#endif
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.StreamPlayPause((int)playPauseType, sessionId);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK)
                    {
                        logger.Error("Error at StreamPlayPause() ==> ErrorCode = " + apiStatus.ErrorCode);
                    }
                }
                else { logger.Error("Error at StreamPlayPause() ==> CallSDK not initialized."); }
            }
            catch (Exception ex) { logger.Error("Error at StreamPlayPause() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void GetViewerList(long publisherId, int totalViewerCount)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::GET_VIEWER_LIST".PadRight(66, ' ') + "==>  publisherId = " + publisherId + ", totalViewerCount = " + totalViewerCount);
#endif
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.GetViewerList(publisherId, totalViewerCount);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK) { logger.Error("Error at GetViewerList() ==> ErrorCode = " + apiStatus.ErrorCode); }
                }
                else logger.Error("Error at GetViewerList() ==> CallSDK not initialized.");
            }
            catch (Exception ex) { logger.Error("Error at GetViewerList() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void SendGifts(long publisherId, long giftId, string giftName, int giftAmount, String giftUrl, String viewerName, String viewerUrl)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::SEND_GIFTS".PadRight(66, ' ') + "==>  publisherId = " + publisherId + ", giftId = " + giftId + ", giftName = " + giftName + ", giftAmount = " + giftAmount + ", giftUrl = " + giftUrl + ", viewerName = " + viewerName + ", viewerUrl = " + viewerUrl);
#endif
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.SendGifts(publisherId, giftId, giftAmount, giftUrl, viewerName, viewerUrl, giftName);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK) logger.Error("Error at SendGifts() ==> ErrorCode = " + apiStatus.ErrorCode);
                }
                else logger.Error("Error at SendGifts() ==> CallSDK not initialized.");

            }
            catch (Exception ex) { logger.Error("Error at SendGifts() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void SendLike(long publisherId, string viewerName, string viewerUrl, int totalLike)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::SEND_LIKE".PadRight(66, ' ') + "==>  publisherId = " + publisherId + ", viewerName = " + viewerName + ", viewerUrl = " + viewerUrl + ", totalLike = " + totalLike);
#endif
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.SendLike(publisherId, viewerName, viewerUrl, totalLike);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK) { logger.Error("Error at SendLike() ==> ErrorCode = " + apiStatus.ErrorCode); }
                }
                else logger.Error("Error at SendLike() ==> CallSDK not initialized.");
            }
            catch (Exception ex) { logger.Error("Error at SendLike() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void FollowPublisher(long publisherId, string viewerName, string viewerUrl, int totalFollower)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::FOLLOW_PUBLISHER".PadRight(66, ' ') + "==>  publisherId = " + publisherId + ", viewerName = " + viewerName + ", viewerUrl = " + viewerUrl + ", totalFollower = " + totalFollower);
#endif
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.FollowPublisher(publisherId, viewerName, viewerUrl, totalFollower);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK) { logger.Error("Error at FollowPublisher() ==> ErrorCode = " + apiStatus.ErrorCode); }
                }
                else logger.Error("Error at FollowPublisher() ==> CallSDK not initialized.");
            }
            catch (Exception ex) { logger.Error("Error at FollowPublisher() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void ShareLiveMedia(long publisherId, string fullName, string profileImageURL)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::SHARE_LIVE_MEDIA".PadRight(66, ' ') + "==>  publisherId = " + publisherId + ", fullName = " + fullName + ", profileImageURL = " + profileImageURL);
#endif
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.ShareLiveMedia(publisherId, fullName, profileImageURL);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK) { logger.Error("Error at FollowPublisher() ==> ErrorCode = " + apiStatus.ErrorCode); }
                }
                else logger.Error("Error at ShareLiveMedia() ==> CallSDK not initialized.");
            }
            catch (Exception ex) { logger.Error("Error at ShareLiveMedia() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void SendStreamAudioData(byte[] pcmData)
        {
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    short[] sdata = new short[(int)(pcmData.Length / 2)];
                    Buffer.BlockCopy(pcmData, 0, sdata, 0, pcmData.Length);
                    int status = MainSwitcher.CallController.BaseCallService.SendAudioData(CallProperty.getSessionIdForLiveStreaming(), sdata, (uint)sdata.Length);
                    if (status == 0) logger.Error("Error at SendStreamAudioData() ==>  Status = " + false);
                }
                else logger.Error("Error at SendStreamAudioData() ==>  CallSDK not initialized.");
            }
            catch (Exception ex) { logger.Error("Error at SendStreamAudioData() ==> " + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void SendStreamVideoData(byte[] rgbData)
        {
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    int status = MainSwitcher.CallController.BaseCallService.SendVideoData(CallProperty.getSessionIdForLiveStreaming(), rgbData, (uint)rgbData.Length, 0, 0);
                    if (status == 0) logger.Error("Error at SendStreamVideoData() ==>  Status = " + false);
                }
                else logger.Error("Error at SendStreamVideoData() ==>  CallSDK not initialized.");

                StreamChannel.PUBLISHER_SENT_COUNTER++;
            }
            catch (Exception ex) { logger.Error("Error at SendStreamVideoData() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static bool StartLiveCall(long viewerId)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::START_LIVE_CALL".PadRight(66, ' ') + "==>  viewerId = " + viewerId);
#endif
            bool status = true;
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.StartLiveCall(viewerId);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK)
                    {
                        logger.Error("Error at StartLiveCall() ==> ErrorCode = " + apiStatus.ErrorCode);
                        status = false;
                    }
                }
                else
                {
                    logger.Error("Error at StartLiveCall() ==>  CallSDK not initialized.");
                    status = false;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error at StartLiveCall() ==>" + ex.Message + "\n" + ex.StackTrace);
                status = false;
            }
            return status;
        }

        public static bool AnswerLiveCall(long publisherId, CallMediaType mediaType)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::ANSWER_LIVE_CALL".PadRight(66, ' ') + "==>  publisherId = " + publisherId + ", mediaType = " + mediaType);
#endif
            bool status = true;
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.AnswerLiveCall(publisherId, mediaType);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK)
                    {
                        logger.Error("Error at AnswerLiveCall() ==> ErrorCode = " + apiStatus.ErrorCode);
                        status = false;
                    }
                }
                else
                {
                    logger.Error("Error at AnswerLiveCall() ==>  CallSDK not initialized.");
                    status = false;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error at AnswerLiveCall() ==>" + ex.Message + "\n" + ex.StackTrace);
                status = false;
            }
            return status;
        }

        public static void BusyLiveCall(long publisherId)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::BUSY_LIVE_CALL".PadRight(66, ' ') + "==>  publisherId = " + publisherId);
#endif
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.BusyLiveCall(publisherId);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK) { logger.Error("Error at BusyLiveCall() ==> ErrorCode = " + apiStatus.ErrorCode); }
                }
                else logger.Error("Error at BusyLiveCall() ==>  CallSDK not initialized.");
            }
            catch (Exception ex) { logger.Error("Error at BusyLiveCall() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void ByeLiveCall(long receiverId)
        {
#if CHAT_LOG
            logger.Debug("SERVICE::STREAM::CALL::BYE_LIVE_CALL".PadRight(66, ' ') + "==>  receiverId = " + receiverId);
#endif
            try
            {
                if (MainSwitcher.CallController != null && MainSwitcher.CallController.BaseCallService != null)
                {
                    BaseApiStatus apiStatus = MainSwitcher.CallController.BaseCallService.ByeLiveCall(receiverId);
                    if (apiStatus.ErrorCode != VocieChatError.NO_ERROR_IN_SDK) { logger.Error("Error at SendLike() ==> ErrorCode = " + apiStatus.ErrorCode); }
                }
                else logger.Error("Error at SendStreamVideoData() ==>  CallSDK not initialized.");
            }
            catch (Exception ex) { logger.Error("Error at SendStreamVideoData() ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void InsertDelegate(VocieChatError errorCode, ChatEventDelegate onComplete, ChatEventArgs args)
        {
            if (onComplete != null)
            {
                if (errorCode == VocieChatError.NO_ERROR_IN_SDK)
                {
                    ChatDictionaries.Instance.CHAT_EVENT_DELEGATE[args.PacketID] = onComplete;
                }
                else
                {
                    new Thread(() =>
                    {
                        onComplete.Invoke(args);
                    }).Start();
                }
            }
        }

        public static void InvokeDelegate(ChatEventDelegate onComplete, ChatEventArgs args)
        {
            new Thread(() =>
            {
                try
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke(args);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error at InsertDelegate() ==>" + ex.Message + "\n" + ex.StackTrace);
                }
            }).Start();
        }

        #endregion "Live Stream"
    }
}

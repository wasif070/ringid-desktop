﻿using System;
using callsdkwrapper;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using View.Utility.Call.Settings;
using View.Utility.Call.Video;
using View.Utility.Chat;
using System.Collections.Generic;
using log4net;
using View.Utility.Stream;
using View.Utility.Chat.Service;
using System.Diagnostics;
using View.BindingModels;
using View.UI.Stream;
using View.Utility.Stream.Utils;
using View.UI.Channel;
using View.Utility.Channel;
using System.Windows;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Wallet;
using System.Linq;
using Models.Utility.Chat;
using System.Windows.Threading;
using View.UI.StreamAndChannel;

namespace View.Utility.Call.SDKWrapper
{
    public class CallSDKCallBackHandler : ICallListener
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CallSDKCallBackHandler).Name);

        #region "Properties"
        CallController callcontroller;
        #endregion "Properties"

        #region Ctors

        public CallSDKCallBackHandler(CallController controller)
        {
            this.callcontroller = controller;
        }
        #endregion

        #region "Member funtions of ICallListener"

        ///<summary>
        ///Need to end the video signal if found while network strength is low
        ///</summary>
        ///

        public override void notifyClientMethodWithAudioData(long sessionID, short[] data, int dataLenth)
        {
            //#if DEBUG
            //            Console.WriteLine("notifyClientMethodWithAudioData" + data[0]);
            //#endif
            int bytlength = dataLenth * 2;
            byte[] pcmData = new byte[bytlength];
            Buffer.BlockCopy(data, 0, pcmData, 0, pcmData.Length);
            MainSwitcher.CallController.PlayAudioByteData(pcmData);
        }

        public override void notifyClientMethodWithVideoData(long sessionID, byte[] data, int dataLenth, int iVideoHeight, int iVideoWidth, int iOrientation)
        {
            callcontroller.PlayFriendRGBData(data, dataLenth, iVideoHeight, iVideoWidth, iOrientation);
        }

        public override void notifyClientMethodWithAudioAlarm(long sessionID, short[] data, int dataLenth)
        {

        }

        public override void notifyClientMethodWithNetworkStrengthNotification(long sessionID, int networkStrength)
        {
            //#if DEBUG
            //            Console.WriteLine("************notifyClientMethodWithNetworkStrengthNotification==>" + networkStrength);
            //#endif
            CallConstants.LastNetWorkStrengthType = networkStrength;
        }

        public override void notifyClientMethodWithVideoNotification(long sessionID, VideoNotificationCode eventType)
        {
            //#if DEBUG
            //            Console.WriteLine("CallConstants.VideoSettings.FRAME_WIDTH==>" + CallConstants.VideoSettings.FRAME_WIDTH + "************notifyClientMethodWithVideoNotification ==>" + eventType);
            //#endif
            switch (eventType)
            {
                case VideoNotificationCode.VideoInitializeCameraWithNearly_352_288://210
                    if (CallConstants.VideoSettings.FRAME_WIDTH != CallConstants.VideoSettings.FRAME_WIDTH_MID)
                    {
                        CallConstants.SetMinimumVideoQuality();
                        setResulationForVideo();
                        if (callcontroller.WebCamController != null && callcontroller.WebCamController.IsRunning) callcontroller.WebCamController.RestartWebCam();
                    }
                    break;
                case VideoNotificationCode.VideoInitializeCameraWithNearly_640_480://211
                    if (CallConstants.VideoSettings.FRAME_WIDTH != CallConstants.VideoSettings.FRAME_WIDTH_MAX)
                    {
                        CallConstants.SetMaxVideoQuality();
                        setResulationForVideo();
                        if (callcontroller.WebCamController != null && callcontroller.WebCamController.IsRunning)
                            callcontroller.WebCamController.RestartWebCam();
                    }
                    break;
                case VideoNotificationCode.VideoNotificationCodeDefaultCode://100
                    break;
                case VideoNotificationCode.VideoResolution_352_288_fps_15_supported://208
                case VideoNotificationCode.VideoResolution_352_288_fps_25_supported://207
                    CallConstants.SetMinimumVideoQuality();
                    setResulationForVideo();
                    break;
                case VideoNotificationCode.VideoResolution_640_480_fps_25_supported://205
                    CallConstants.SetMaxVideoQuality();
                    setResulationForVideo();
                    break;
                default:
                    break;
            }
        }

        public override void onEventHandlerAttached()
        {
        }

        public override void onEventHandlerDetached()
        {
        }

        public override void onNetworkUsage(long sentSignalBytes, long receivedSignalBytes, long sentAudioBytes, long receivedAudioBytes, long sentVideoBytes, long receivedVideoBytes)
        {
        }

        public override void onSDKEvent(EventType eventType)
        {
#if DEBUG
            Console.WriteLine("************EventType ==>" + eventType);
#endif
            switch (eventType)
            {
                case EventType.AnotherIncomingCall:
                    break;
                case EventType.AnswerSendingFailure:
                    break;
                case EventType.AuthRegistrationFailure:
                    break;
                case EventType.BusyMessage:
                    if (callcontroller.CallData != null)
                    {
                        string busymessage = CallProperty.getBusyMessgae();
                        if (!string.IsNullOrEmpty(busymessage))
                        {
                            callcontroller.CallData.ErrorMessage = busymessage;
                            callcontroller.CallData.CallEndMessage = NotificationMessages.TEXT_USER_BUSY;
                        }
                        MainSwitcher.CallController.IsCallEnded = true;
                        ChatHelpers.AddCallMessage(CallProperty.getFriendIdentity(), CallProperty.getFriendIdentity(), busymessage, CallProperty.getBusyMessgaePacketId());
                    }
                    break;
                case EventType.BusyMessageSendingFailure:
                    break;
                case EventType.BusySendingFailure:
                    break;
                case EventType.ByeSendingFailure:
                    break;
                case EventType.CallBandwidth:
                    break;
                case EventType.CallConnected:
                    if (callcontroller.ViewModelCallInfo != null && callcontroller.ViewModelCallInfo.CallState != CallStates.UA_ONCALL)
                    {
                        callcontroller.ViewModelCallInfo.CallState = CallStates.UA_ONCALL;
                    }
                    break;
                case EventType.CallDisconnectedBusy:
                    break;
                case EventType.CallDisconnectedCancelled:
                    break;
                case EventType.CallDropped:
                    if (callcontroller.CallData != null)
                    {
                        callcontroller.CallData.CallEndMessage = NotificationMessages.TEXT_CALL_DROPPED;
                        callcontroller.CallData.ErrorMessage = NotificationMessages.TEXT_CALL_DROPPED;
                        MainSwitcher.CallController.IsCallEnded = true;
                    }
                    break;
                case EventType.CallEnded:
                    MainSwitcher.CallController.IsCallEnded = true;
                    break;
                case EventType.CallSdkReset:
                    break;
                case EventType.CallingSendingFailure:
                    if (callcontroller.CallData != null)
                    {
                        callcontroller.CallData.CallEndMessage = NotificationMessages.TEXT_CALL_FAILED_REASON;
                        callcontroller.CallData.ErrorMessage = NotificationMessages.TEXT_CALL_FAILED_REASON;
                        MainSwitcher.CallController.IsCallEnded = true;
                    }
                    break;
                case EventType.CancleSendingFailure:
                    break;
                case EventType.DonotDisturbFriend:
                    break;
                case EventType.FriendBusy:
                    if (callcontroller.CallData != null)
                    {
                        callcontroller.CallData.ErrorMessage = NotificationMessages.TEXT_USER_BUSY;
                        callcontroller.CallData.CallEndMessage = NotificationMessages.TEXT_USER_BUSY;
                        MainSwitcher.CallController.IsCallEnded = true;
                    }
                    break;
                case EventType.FriendInCall:
                    CallConstants.IN_CALL = true;
                    if (callcontroller.CallData != null)
                    {
                        callcontroller.CallData.ErrorMessage = NotificationMessages.TEXT_USER_IN_CALL;
                        callcontroller.CallData.CallEndMessage = NotificationMessages.TEXT_USER_IN_CALL;
                        MainSwitcher.CallController.IsCallEnded = true;
                    }
                    break;
                case EventType.FriendRinging:
                    if (callcontroller.ViewModelCallInfo != null && (callcontroller.ViewModelCallInfo.CallState != CallStates.UA_ONCALL || callcontroller.ViewModelCallInfo.CallState == CallStates.UA_OUTGOING_CALL))
                    {
                        ChatHelpers.ForcePauseMediaPlayer();
                        callcontroller.ViewModelCallInfo.CallState = CallStates.UA_RINGING;
                        CallHelperMethods.ChangeDurationText(NotificationMessages.TEXT_RINGING);
                    }
                    break;
                case EventType.HoldCall:
                    // callcontroller.CallHoldActions();
                    break;
                case EventType.HoldCallConfirmation:
                    //callcontroller.HoldConfirmationActions();
                    break;
                case EventType.InCallSendingFailure:
                    break;
                case EventType.IncomingCall:
                    if (callcontroller.CallData == null)
                    {
                        callcontroller.CallData = CallDictionaries.Instance.CALLERS_INFO_DICTIONARY.TryGetValue(CallProperty.getCurrentCallId());
                        if (callcontroller.CallData != null)
                        {
                            callcontroller.CallData.IsIncomming = true;
                            callsdkwrapper.CallMediaType mideaType = CallMediaType.Voice;
                            if (CallProperty.getCurrentCallMediaType() == XamlStaticValues.CALL_TYPE_VIDEO)
                            {
                                mideaType = CallMediaType.Video;
                            }
                            CallHelperMethods.StartCallManger(mideaType);
                            //CallHelperMethods.ShowCallSeparateWindow(callcontroller.CallData, mideaType);
                        }
                    }
                    break;
                case EventType.MissedCall:
                    if (callcontroller.CallData != null)
                    {
                        MainSwitcher.CallController.IsCallEnded = true;
                        callcontroller.CallData.IsNeedToPlayEndCallSound = false;
                    }
                    break;
                case EventType.NoAnswer:
                    if (callcontroller.ViewModelCallInfo != null && callcontroller.ViewModelCallInfo.CallState != CallStates.UA_ONCALL)
                    {
                        callcontroller.CallData.CallEndMessage = NotificationMessages.TEXT_NO_ANSWER;
                        callcontroller.CallData.ErrorMessage = NotificationMessages.TEXT_NO_ANSWER;
                        MainSwitcher.CallController.IsCallEnded = true;
                    }
                    break;
                case EventType.NoAnswerSendingFailure:
                    break;
                case EventType.OfflineFrined:
                    break;
                case EventType.RegisterFailure:
                    if (callcontroller.CallData != null)
                    {
                        callcontroller.CallData.VoiceRegisterd = (int)EventType.RegisterFailure;
                    }
                    break;
                case EventType.RegisterSuccess:
                    if (callcontroller.CallData != null)
                    {
                        callcontroller.CallData.VoiceRegisterd = (int)EventType.RegisterSuccess;
                    }
                    break;
                case EventType.SdkError:
                    break;
                case EventType.StopServiceThread:
                    break;
                case EventType.UnholdCall:
                    break;
                case EventType.UnholdCallConfirmation:
                    break;
                case EventType.VideoConnected:
                    if (callcontroller.ViewModelCallInfo != null && callcontroller.ViewModelCallInfo.CallState != CallStates.UA_ONCALL)
                    {
                        callcontroller.ViewModelCallInfo.CallState = CallStates.UA_ONCALL;
                        callcontroller.ViewModelCallInfo.IsAcceptedWithVideo = true;
                    }
                    break;
                case EventType.VideoEndFailure:
                    break;
                case EventType.VideoEnded:
                    if (callcontroller.ViewModelCallInfo != null)
                    {
                        callcontroller.ViewModelCallInfo.NumberOfVideo = 0;
                    }
                    // MainSwitcher.CallController.ResetAllWhileStopVideoCall();
                    break;
                case EventType.VideoEndedByOtherEnd:
                    if (callcontroller.ViewModelCallInfo != null)
                    {
                        callcontroller.ViewModelCallInfo.DecrementNumberOfVideo();
                    }
                    break;
                case EventType.VideoEndedBySenderConfirmation:
                    break;
                case EventType.VideoRegisterFailure:
                    break;
                case EventType.VideoRegisterSuccessful:
                    break;
                case EventType.VideoStartFailure:
                    break;
                case EventType.VideoStarted:
                    if (callsdkwrapper.CallProperty.isAbleToReceiveVideo())
                    {
                        //MainSwitcher.CallController.ResetALLWhileStartFriendVideo();
                    }
                    break;
                case EventType.VideoStartedByOtherEnd:
                    if (callcontroller.ViewModelCallInfo != null)
                    {
                        callcontroller.ViewModelCallInfo.IncrementNumberOfVideo();
                    }
                    //if (callsdkwrapper.CallProperty.isAbleToReceiveVideo())
                    //{
                    //    MainSwitcher.CallController.ResetALLWhileStartFriendVideo();
                    //}
                    break;
                case EventType.StreamingPublisherRegisterSuccessful:
                    onPublisherRegisterSuccessful(DefaultSettings.LOGIN_TABLE_ID);
                    break;
                case EventType.PublisherRegisterFailure:
                    onPublisherRegisterFailure(DefaultSettings.LOGIN_TABLE_ID);
                    break;
                case EventType.ViewerRegisterFailure:
                    onViewerRegisterFailure(CallProperty.getFriendIdentity());
                    break;
                case EventType.PublisherUnregisterSuccessful:
                    onPublisherUnregisterConfirmation(DefaultSettings.LOGIN_TABLE_ID, true);
                    break;
                case EventType.PublisherUnregisterFailure:
                    onPublisherUnregisterConfirmation(DefaultSettings.LOGIN_TABLE_ID, false);
                    break;
                case EventType.LiveStreamingPoorConnection:
                    onLiveStreamingPoorConnection(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveStreamingNoStreamData:
                    onLiveStreamingNoStreamData(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveStreamingNoDataToSend:
                    onLiveStreamingNoDataToSend(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveIncommingCall:
                    onLiveIncomingCall(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveCallRinging:
                    onLiveCallRinging(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveCallAnsweredWithAudio:
                    onLiveCallAnswerAudio(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveCallBusy:
                    onLiveCallBusy(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveCallBye:
                    onLiveCallBye(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveCallConnected:
                    onLiveCallConnected(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveCallDisconnected:
                    onLiveCallDisconnected(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveCallAnsweredWithVideo:
                    onLiveCallAnswerVideo(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveStreamInterruptedOver:
                    onLiveStreamInterruptedOver(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveStreamInterrupted:
                    onLiveStreamInterrupted(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveStreamCallingFailure:
                    onLiveStreamCallingFailure(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveStreamByeSendingFailure:
#if CHAT_LOG
                    log.Debug("HANDLER::STREAM::CALL::LIVE_STREAM_BYE_SENDING_FAILED".PadRight(61, ' ') + "==>   EventType = " + EventType.LiveStreamByeSendingFailure);
#endif
                    break;
                case EventType.LiveStreamAnswerSendingFailure:
                    onLiveStreamAnswerSendingFailure(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveStreamBusySendingFailure:
#if CHAT_LOG
                    log.Debug("HANDLER::STREAM::CALL::LIVE_STREAM_BUSY_SENDING_FAILED".PadRight(61, ' ') + "==>   EventType = " + EventType.LiveStreamBusySendingFailure);
#endif
                    break;
                case EventType.LiveStreamNoAnswer:
                    onLiveStreamNoAnswer(CallProperty.getFriendIdentity());
                    break;
                case EventType.LiveStreamRingingTimeout:
                    onLiveStreamRingingTimeout(CallProperty.getFriendIdentity());
                    break;
                default:
                    break;
            }
        }

        #endregion //"Member funtions of ICallListener"

        #region "Utility Methods"

        private void setResulationForVideo()
        {
            CallHelperMethods.SetVideoResolution(CallConstants.VideoSettings.FRAME_HEIGHT, CallConstants.VideoSettings.FRAME_WIDTH);
        }
        #endregion "Utility Methods"

        #region Live Stream

        public void onPublisherRegisterSuccessful(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::PUBLISHER_REGISTER_SUCCESS".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                if (StreamViewModel.Instance.StreamingChannel != null)
                {
                    StreamViewModel.Instance.StreamingChannel.IsRegistered = true;
                }

                StreamChannel.PublisherStart();
            }
            catch (Exception ex)
            {
                log.Error("Error at onViewerRegisterSuccessful() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void onPublisherRegisterFailure(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::PUBLISHER_REGISTER_FAILED".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_FINISHED;

                UCStreamLiveViewer streamViewer = StreamHelpers.GetCurruentStreamLiveViewer();
                if (streamViewer != null)
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        UIHelperMethods.ShowFailed("Connection to server failed! Please try again.", "Live failed");
                        // CustomMessageBox.ShowError("Connection to server failed! Please try again.", "Live Failed");
                        UCStreamAndChannelViewer.Instance.Dispose();
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublisherRegisterFailure() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onViewerRegisterSuccessful(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::VIEWER_REGISTER_SUCCESS".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                if (StreamViewModel.Instance.StreamingChannel != null)
                {
                    StreamViewModel.Instance.StreamingChannel.IsRegistered = true;
                }

                if (StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.UserTableID == publisherId)
                {
                    CallHelperMethods.GetViewerList(publisherId, 0);
                }
                else if (StreamViewModel.Instance.ChannelInfoModel != null && StreamViewModel.Instance.ChannelInfoModel.PublisherID == publisherId)
                {
                    Guid channelID = StreamViewModel.Instance.ChannelInfoModel.ChannelID;
                    new ThrdGetChannelMediaDetails(channelID, (mediaDTO) =>
                    {
                        if (mediaDTO != null && StreamViewModel.Instance.ChannelMediaInfoModel != null && StreamViewModel.Instance.ChannelMediaInfoModel.ChannelID.Equals(channelID))
                        {
                            StreamViewModel.Instance.ChannelMediaInfoModel.LoadData(mediaDTO);
                        }
                        return 0;
                    }).Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onViewerRegisterSuccessful() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void onViewerRegisterFailure(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::VIEWER_REGISTER_FAILED".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_FINISHED;

                UCStreamLiveViewer streamViewer = StreamHelpers.GetCurruentStreamLiveViewer();
                if (streamViewer != null)
                {
                    streamViewer.OnPublisherNotAvailable();
                    return;
                }

                UCChannelViewer channelViewer = ChannelHelpers.GetCurruentChannelViewer();
                if (channelViewer != null)
                {
                    channelViewer.OnPublisherNotAvailable();
                    return;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onViewerRegisterFailure() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onPublisherUnregisterConfirmation(long publisherId, bool status)
        {
            try
            {
                Guid streamId = StreamViewModel.Instance.StreamInfoModel != null ? StreamViewModel.Instance.StreamInfoModel.StreamID : Guid.NewGuid();
                string packetId = streamId.ToString();
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::PUBLISHER_UNREGISTER_CONFIRMATION".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", streamId = " + streamId + ", status = " + status);
#endif
                ChatEventArgs args = new ChatEventArgs { PacketID = packetId, FriendTableID = publisherId, Status = status };
                ChatEventDelegate onComplete = null;
                if (ChatDictionaries.Instance.CHAT_EVENT_DELEGATE.TryRemove(args.PacketID, out onComplete))
                {
                    onComplete.Invoke(args);
                    onComplete = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublisherUnregisterConfirmation() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onViewerUnregisterConfirmation(long publisherId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::VIEWER_UNREGISTER_CONFIRMATION".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", packetId = " + packetId + ", status = " + status);
#endif
                ChatEventArgs args = new ChatEventArgs { PacketID = packetId, FriendTableID = publisherId, Status = status };
                ChatEventDelegate onComplete = null;
                if (ChatDictionaries.Instance.CHAT_EVENT_DELEGATE.TryRemove(args.PacketID, out onComplete))
                {
                    onComplete.Invoke(args);
                    onComplete = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onViewerUnregisterConfirmation() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublisherUnregisterUpdate(long publisherId, int totalViewer, int totalCoin, int totalFollower, long totalTime)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::PUBLISHER_UNREGISTER_UPDATE".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_FINISHED;

                if (StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.UserTableID == publisherId)
                {
                    StreamViewModel.Instance.StreamInfoModel.ViewCount = totalViewer;
                    StreamViewModel.Instance.StreamInfoModel.EndTime = StreamViewModel.Instance.StreamInfoModel.StartTime + totalTime;
                }

                if (StreamViewModel.Instance.StreamUserInfoModel != null && StreamViewModel.Instance.StreamUserInfoModel.UserTableID == publisherId)
                {
                    StreamViewModel.Instance.StreamUserInfoModel.CoinCount = totalCoin;
                    StreamViewModel.Instance.StreamUserInfoModel.FollowerCount = totalFollower;
                }

                UCStreamLiveViewer streamViewer = StreamHelpers.GetCurruentStreamLiveViewer();
                if (streamViewer != null)
                {
                    streamViewer.OnPublisherNotAvailable();
                    return;
                }

                UCChannelViewer channelViewer = ChannelHelpers.GetCurruentChannelViewer();
                if (channelViewer != null)
                {
                    channelViewer.OnPublisherNotAvailable();
                    return;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublisherUnregister() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onViewerUnregisterUpdate(long viewerId, long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::VIEWER_UNREGISTER_UPDATE".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", viewerId = " + viewerId);
#endif
                if (StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.UserTableID == publisherId)
                {
                    if (StreamViewModel.Instance.StreamInfoModel.ViewCount > 0)
                    {
                        StreamViewModel.Instance.StreamInfoModel.ViewCount--;
                    }
                    else
                    {
                        StreamViewModel.Instance.StreamInfoModel.ViewCount = 0;
                    }
                }
                StreamViewerLoadUtility.RemoveViewer(publisherId, viewerId);

            }
            catch (Exception ex)
            {
                log.Error("Error at onViewerUnregister() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublisherNotAvailable(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::PUBLISHER_NOT_AVAILABLE".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_FINISHED;

                UCStreamLiveViewer streamViewer = StreamHelpers.GetCurruentStreamLiveViewer();
                if (streamViewer != null)
                {
                    streamViewer.OnPublisherNotAvailable();
                    return;
                }

                UCChannelViewer channelViewer = ChannelHelpers.GetCurruentChannelViewer();
                if (channelViewer != null)
                {
                    channelViewer.OnPublisherNotAvailable();
                    return;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublisherNotAvailable() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onViewerCountUpdate(long publisherId, int viewerCount, long newViewerId, string newViewerName, string newViewerProfileImageURL)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::VIEWER_COUNT_UPDATE".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", viewerCount = " + viewerCount + ", newViewerId = " + newViewerId + ", newViewerName = " + newViewerName + ", newViewerProfileImageURL = " + newViewerProfileImageURL);
#endif
                StreamUserDTO streamUserDTO = new StreamUserDTO();
                streamUserDTO.PublisherID = publisherId;
                streamUserDTO.UserTableID = newViewerId;
                streamUserDTO.UserName = newViewerName;
                streamUserDTO.ProfileImage = newViewerProfileImageURL;
                streamUserDTO.AddedTime = ChatService.GetServerTime();
                StreamViewerLoadUtility.LoadViewerData(streamUserDTO);

                if (StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.UserTableID == publisherId)
                {
                    StreamViewModel.Instance.StreamInfoModel.ViewCount = viewerCount;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onViewerCountUpdate() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onViewerListUpdate(long publisherId, List<BaseViewerDTO> viewerList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::VIEWER_LIST_UPDATE".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", viewerList = " + viewerList.ToString("\n"));
#endif
                List<StreamUserDTO> list = new List<StreamUserDTO>();
                if (viewerList != null && viewerList.Count > 0)
                {
                    foreach (BaseViewerDTO vDTO in viewerList)
                    {
                        StreamUserDTO streamUserDTO = new StreamUserDTO();
                        streamUserDTO.PublisherID = publisherId;
                        streamUserDTO.UserTableID = vDTO.ViewerID;
                        streamUserDTO.UserName = vDTO.ViewerFullName;
                        streamUserDTO.ProfileImage = vDTO.ProfileImageURL;
                        streamUserDTO.AddedTime = vDTO.AddedTime;
                        list.Add(streamUserDTO);
                    }
                }
                StreamViewerLoadUtility.LoadViewerData(list);

            }
            catch (Exception ex)
            {
                log.Error("Error at onViewerListUpdate() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublisherGift(long publisherId, long viewerId, long giftId, int giftAmount, string giftUrl, string viewerName, string viewerProfileImageUrl, string giftName)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::PUBLISHER_GIFT".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", viewerId = " + viewerId + ", giftId = " + giftId + ", giftName = " + giftName + ", giftAmount = " + giftAmount + ", giftUrl = " + giftUrl + ", viewerName = " + viewerName + ", viewerProfileImageUrl = " + viewerProfileImageUrl);
#endif
                UCStreamLiveViewer viewer = StreamHelpers.GetCurruentStreamLiveViewer();
                if (viewer != null)
                {
                    if (StreamViewModel.Instance.StreamUserInfoModel != null && StreamViewModel.Instance.StreamUserInfoModel.UserTableID == publisherId)
                    {
                        StreamViewModel.Instance.StreamUserInfoModel.CoinCount += giftAmount;
                        if (publisherId == DefaultSettings.LOGIN_TABLE_ID)
                        {
                            View.Utility.Wallet.WalletViewModel.Instance.MyCoinStat.Quantity += giftAmount;
                            WalletGiftProductModel giftModel = new WalletGiftProductModel();
                            giftModel.ProductID = (int)giftId;
                            giftModel.ProductName = giftName;
                            giftModel.ProductPriceCoinQuantity = giftAmount;
                            giftModel.ProductUrl = giftUrl;
                            giftModel.GiftSenderTableId = viewerId;
                            giftModel.GiftSenderName = viewerName;
                            giftModel.GiftSenderProfileImage = viewerProfileImageUrl;
                            if (WalletViewModel.Instance.GiftProductsList.Where(P => P.ProductID == giftModel.ProductID).FirstOrDefault() == null)
                            {
                                WalletViewModel.Instance.GiftProductsList.InvokeAdd(giftModel);
                            }
                            if (!(StreamGiftSendReceive._Instance != null && StreamGiftSendReceive._Instance._IsReady == false))
                            {
                                StreamGiftSendReceive.Instance.AddGift(giftModel);
                                viewer.GiftProductModel = giftModel;
                                viewer.InitGiftGif(giftModel);
                            }

                            MessageDTO messageDTO = new MessageDTO();
                            messageDTO.PublisherID = publisherId;
                            messageDTO.SenderTableID = viewerId;
                            PacketTimeID packet = ChatService.GeneratePacketID();
                            messageDTO.PacketID = packet.PacketID;
                            //messageDTO.MessageType = StreamConstants.GIFT_MESSAGE;
                            messageDTO.StreamMsgDTO = new StreamMessageDTO { MessageType = StreamConstants.GIFT_MESSAGE };
                            messageDTO.Message = giftId.ToString();
                            messageDTO.MessageDate = packet.Time;
                            messageDTO.FullName = viewerName;
                            messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                            StreamMsgLoadUtility.LoadMessageData(messageDTO);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublisherGift() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onReceivingLike(long publisherId, long viewerId, string viewerName, string viewerUrl, int totalLike)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::RECEIVING_LIKE".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", viewerId = " + viewerId + ", viewerName = " + viewerName + ", viewerUrl = " + viewerUrl + ", totalLike = " + totalLike);
#endif
                if (StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.UserTableID == publisherId)
                {
                    if (totalLike > StreamViewModel.Instance.StreamInfoModel.LikeCount)
                    {
                        StreamViewModel.Instance.StreamInfoModel.LikeCount = totalLike;
                    }
                    if (publisherId == DefaultSettings.LOGIN_TABLE_ID || viewerId != DefaultSettings.LOGIN_TABLE_ID)
                    {
                        MessageDTO messageDTO = new MessageDTO();
                        messageDTO.PublisherID = publisherId;
                        messageDTO.SenderTableID = viewerId;
                        PacketTimeID packet = ChatService.GeneratePacketID();
                        messageDTO.PacketID = packet.PacketID;
                        messageDTO.StreamMsgDTO = new StreamMessageDTO { MessageType = StreamConstants.LIKE_MESSAGE };
                        messageDTO.Message = viewerUrl;
                        messageDTO.MessageDate = packet.Time;
                        messageDTO.FullName = viewerName;
                        messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                        StreamMsgLoadUtility.LoadMessageData(messageDTO);

                        UCStreamLiveViewer viewer = StreamHelpers.GetCurruentStreamLiveViewer();
                        if (viewer != null)
                        {
                            Application.Current.Dispatcher.BeginInvoke(() => { viewer.bubbleAnimation.AddBubble(); });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onReceivingLike() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onReceivingFollower(long publisherId, long viewerId, string viewerName, string viewerUrl, int totalFollower)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::RECEIVING_FOLLOWER".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", viewerId = " + viewerId + ", viewerName = " + viewerName + ", viewerUrl = " + viewerUrl + ", totalFollower = " + totalFollower);
#endif
                if (publisherId == DefaultSettings.LOGIN_TABLE_ID)
                {
                    MessageDTO messageDTO = new MessageDTO();
                    messageDTO.PublisherID = publisherId;
                    messageDTO.SenderTableID = viewerId;
                    PacketTimeID packet = ChatService.GeneratePacketID();
                    messageDTO.PacketID = packet.PacketID;
                    //messageDTO.MessageType = StreamConstants.FOLLOW_MESSAGE;
                    messageDTO.StreamMsgDTO = new StreamMessageDTO { MessageType = StreamConstants.FOLLOW_MESSAGE };
                    messageDTO.Message = viewerUrl;
                    messageDTO.MessageDate = packet.Time;
                    messageDTO.FullName = viewerName;
                    messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                    StreamMsgLoadUtility.LoadMessageData(messageDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onReceivingFollower() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onLiveStreamShare(long publisherId, long viewerId, string viewerName, string viewerUrl)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_STREAM_SHARE".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", viewerId = " + viewerId + ", viewerName = " + viewerName + ", viewerUrl = " + viewerUrl);
#endif
                if (publisherId == DefaultSettings.LOGIN_TABLE_ID)
                {
                    MessageDTO messageDTO = new MessageDTO();
                    messageDTO.PublisherID = publisherId;
                    messageDTO.SenderTableID = viewerId;
                    PacketTimeID packet = ChatService.GeneratePacketID();
                    messageDTO.PacketID = packet.PacketID;
                    //messageDTO.MessageType = StreamConstants.SHARE_MESSAGE;
                    messageDTO.StreamMsgDTO = new StreamMessageDTO { MessageType = StreamConstants.SHARE_MESSAGE };
                    messageDTO.Message = viewerUrl;
                    messageDTO.MessageDate = packet.Time;
                    messageDTO.FullName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                    messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                    StreamMsgLoadUtility.LoadMessageData(messageDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamShare() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onNowPlayingMedia(long publisherId, string mediaName)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::NOW_PLAYING_MEDIA".PadRight(61, ' ') + "==>   publisherId = " + publisherId + ", mediaName = " + mediaName);
#endif
                if (StreamViewModel.Instance.ChannelMediaInfoModel != null && StreamViewModel.Instance.ChannelInfoModel != null && StreamViewModel.Instance.ChannelInfoModel.PublisherID == publisherId)
                {
                    StreamViewModel.Instance.ChannelMediaInfoModel.Title = mediaName;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onNowPlayingMedia() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveStreamInterrupted(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_STREAM_INTERRUPTED".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                StreamViewModel.Instance.StreamingStatus = StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_FINISHED ? StreamConstants.STREAMING_INTERRUPTED : StreamViewModel.Instance.StreamingStatus;
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamInterrupted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveStreamInterruptedOver(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_STREAM_INTERRUPTED_OVER".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                StreamViewModel.Instance.StreamingStatus = StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_FINISHED ? StreamConstants.STREAMING_CONNECTED : StreamViewModel.Instance.StreamingStatus;
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamInterruptedOver() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveStreamingPoorConnection(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_STREAMING_POOR_CONNECTION".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                if (StreamViewModel.Instance.StreamingChannel != null
                    && StreamViewModel.Instance.StreamingChannel.ChannelType == StreamConstants.CHANNEL_IN
                    && StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_INTERRUPTED)
                {
                    StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_POOR_NERWORK;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamingPoorConnection() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveStreamingNoStreamData(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_STREAMING_NO_STREAM_DATA".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                if (StreamViewModel.Instance.StreamingChannel != null
                    && StreamViewModel.Instance.StreamingChannel.ChannelType == StreamConstants.CHANNEL_IN
                    && StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_INTERRUPTED)
                {
                    StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_NO_DATA;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamingNoStreamData() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveStreamingNoDataToSend(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_STREAMING_NO_DATA_TO_SEND".PadRight(61, ' ') + "==>   publisherId = " + publisherId);
#endif
                if (StreamViewModel.Instance.StreamingChannel != null
                    && StreamViewModel.Instance.StreamingChannel.ChannelType == StreamConstants.CHANNEL_IN
                    && StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_INTERRUPTED)
                {
                    StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_NO_DATA;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamingNoDataToSend() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveIncomingCall(long callerId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_INCOMING_CALL".PadRight(61, ' ') + "==>   callerId = " + callerId);
#endif
                if (StreamViewModel.Instance.StreamUserInfoModel != null && StreamViewModel.Instance.StreamUserInfoModel.UserTableID == callerId)
                {
                    if (!StreamViewModel.Instance.InitCallInfo(StreamConstants.STREAM_CALL_INCOMING, StreamViewModel.Instance.StreamUserInfoModel))
                    {
                        CallHelperMethods.BusyLiveCall(callerId);
                        StreamViewModel.Instance.DestroyCallInfo();
                    }
                }
                else
                {
                    CallHelperMethods.BusyLiveCall(callerId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveIncomingCall() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveCallRinging(long calleeId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_RINGING".PadRight(61, ' ') + "==>   calleeId = " + calleeId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveCallRinging() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveCallAnswerAudio(long calleeId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_ANSWER_WITH_AUDIO".PadRight(61, ' ') + "==>   calleeId = " + calleeId);
#endif
                StreamViewModel.Instance.CallStatus = StreamConstants.STREAM_CALL_CONNECTED;
                StreamViewModel.Instance.CallType = CallMediaType.Voice;
                StreamViewModel.Instance.StartCallTimer();
                StreamViewModel.Instance.InitCallChannel(StreamConstants.CHANNEL_IN, StreamViewModel.Instance.CallRenderModel);
                Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.SetCallChannelSource());
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveCallAnswer() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveCallAnswerVideo(long calleeId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_ANSWER_VIDEO".PadRight(61, ' ') + "==>   calleeId = " + calleeId);
#endif
                StreamViewModel.Instance.CallStatus = StreamConstants.STREAM_CALL_CONNECTED;
                StreamViewModel.Instance.CallType = CallMediaType.Video;
                StreamViewModel.Instance.StartCallTimer();
                StreamViewModel.Instance.InitCallChannel(StreamConstants.CHANNEL_IN, StreamViewModel.Instance.CallRenderModel);
                Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.SetCallChannelSource());
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveCallAnswer() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveCallBusy(long calleeId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_BUSY".PadRight(61, ' ') + "==>   calleeId = " + calleeId);
#endif
                Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.DestroyCallChannel(), DispatcherPriority.Send);
                StreamViewModel.Instance.DestroyCallInfo();
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveCallBusy() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveCallBye(long callerOrCalleeId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_BYE".PadRight(61, ' ') + "==>   callerOrCalleeId = " + callerOrCalleeId);
#endif
                //Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.StreamChannel.CallDisconnected());
                StreamViewModel.Instance.DestroyCallInfo();
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveCallBye() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveStreamRingingTimeout(long callerId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_RINGING_TIMEOUT".PadRight(61, ' ') + "==>   callerId = " + callerId);
#endif
                Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.DestroyCallChannel(), DispatcherPriority.Send);
                StreamViewModel.Instance.DestroyCallInfo();
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamRingingTimeout() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveStreamNoAnswer(long calleeId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_NO_ANSWER".PadRight(61, ' ') + "==>   calleeId = " + calleeId);
#endif
                Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.DestroyCallChannel(), DispatcherPriority.Send);
                StreamViewModel.Instance.DestroyCallInfo();
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamNoAnswer() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveStreamCallingFailure(long calleeId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALLING_FAILED".PadRight(61, ' ') + "==>   calleeId = " + calleeId);
#endif
                Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.DestroyCallChannel(), DispatcherPriority.Send);
                StreamViewModel.Instance.DestroyCallInfo();
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamCallingFailure() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveStreamAnswerSendingFailure(long callerId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_ANSWER_SENDING_FAILED".PadRight(61, ' ') + "==>   callerId = " + callerId);
#endif
                Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.DestroyCallChannel(), DispatcherPriority.Send);
                StreamViewModel.Instance.DestroyCallInfo();
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamAnswerSendingFailure() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveCallConnected(long callerId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_CONNECTED".PadRight(61, ' ') + "==>   callerId = " + callerId);
#endif

                StreamViewModel.Instance.CallStatus = StreamConstants.STREAM_CALL_CONNECTED;
                StreamViewModel.Instance.StartCallTimer();
                StreamViewModel.Instance.InitCallChannel(StreamConstants.CHANNEL_OUT, null);
                Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.SetCallChannelSource());
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveCallConnected() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void onLiveCallDisconnected(long callerOrCalleeId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CALL::LIVE_CALL_DISCONNECTED".PadRight(61, ' ') + "==>   callerOrCalleeId = " + callerOrCalleeId);
#endif
                Application.Current.Dispatcher.BeginInvoke(() => StreamViewModel.Instance.DestroyCallChannel(), DispatcherPriority.Send);
                StreamViewModel.Instance.DestroyCallInfo();
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveCallDisconnected() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void notifyClientMethodWithLiveStreamingAudioData(long sessionID, short[] data, int dataLenth)
        {
            try
            {
                if (StreamViewModel.Instance.StreamingChannel != null
                    && StreamViewModel.Instance.StreamingChannel.ChannelType == StreamConstants.CHANNEL_IN
                    && StreamViewModel.Instance.StreamingChannel.IsRegistered)
                {
                    byte[] pcmData = new byte[dataLenth * 2];
                    Buffer.BlockCopy(data, 0, pcmData, 0, pcmData.Length);
                    StreamViewModel.Instance.StreamingChannel.SetAudioStreamBuffer(pcmData);
                }

                if (StreamViewModel.Instance.CallChannel != null
                    && StreamViewModel.Instance.CallChannel.ChannelType == StreamConstants.CHANNEL_IN
                    && StreamViewModel.Instance.CallChannel.IsRegistered)
                {
                    byte[] pcmData = new byte[dataLenth * 2];
                    Buffer.BlockCopy(data, 0, pcmData, 0, pcmData.Length);
                    StreamViewModel.Instance.CallChannel.SetAudioStreamBuffer(pcmData);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at notifyClientMethodWithAudioData() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void notifyClientMethodWithLiveStreamingVideoData(long sessionID, byte[] data, int dataLenth, int iVideoHeight, int iVideoWidth, int iOrientation)
        {
            try
            {
                if (StreamViewModel.Instance.StreamingChannel != null
                    && StreamViewModel.Instance.StreamingChannel.ChannelType == StreamConstants.CHANNEL_IN
                    && StreamViewModel.Instance.StreamingChannel.IsRegistered)
                {
                    StreamChannel.VIEWER_RECIEVED_COUNTER++;
                    if (StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_INTERRUPTED) StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_CONNECTED;
                    StreamViewModel.Instance.StreamingChannel.SetVideoStreamBuffer(data, iVideoWidth, iVideoHeight, iOrientation);
                }

                if (StreamViewModel.Instance.CallChannel != null
                    && StreamViewModel.Instance.CallChannel.ChannelType == StreamConstants.CHANNEL_IN
                    && StreamViewModel.Instance.CallChannel.IsRegistered)
                {
                    StreamViewModel.Instance.CallChannel.SetVideoStreamBuffer(data, iVideoWidth, iVideoHeight, iOrientation);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at notifyClientMethodWithVideoData() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Live Stream

    }
}

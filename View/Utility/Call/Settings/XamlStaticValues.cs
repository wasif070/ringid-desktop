﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Call.Settings
{
    public class XamlStaticValues
    {
        public static readonly int CALL_TYPE_VOICE = 1;
        public static readonly int CALL_TYPE_VIDEO = 2;
        public const  int CALL_TYPE_MISS_CALL = 0;
        public const int CALL_TYPE_INCOMING = 1;
        public const int CALL_TYPE_OUTGOING = 2;
    }
}

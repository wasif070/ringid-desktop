﻿
namespace View.Utility.Call.Settings
{
    public class CallConstants
    {
        public static int LastNetWorkStrengthType = 202;
        public static long LAST_RTP_SENT_TIME;
        public static bool IN_CALL = false;

        public class VideoSettings
        {
            public const int FRAME_WIDTH_MID = 320;
            public const int FRAME_HEIGHT_MID = 240;

            public const int FRAME_WIDTH_MAX = 640;
            public const int FRAME_HEIGHT_MAX = 480;

            public static int FRAME_WIDTH = FRAME_WIDTH_MAX;
            public static int FRAME_HEIGHT = FRAME_HEIGHT_MAX;
        }

        public static void SetMaxVideoQuality()
        {
            VideoSettings.FRAME_WIDTH = VideoSettings.FRAME_WIDTH_MAX;
            VideoSettings.FRAME_HEIGHT = VideoSettings.FRAME_HEIGHT_MAX;
        }

        public static void SetMinimumVideoQuality()
        {
            VideoSettings.FRAME_WIDTH = VideoSettings.FRAME_WIDTH_MID;
            VideoSettings.FRAME_HEIGHT = VideoSettings.FRAME_HEIGHT_MID;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models.Entity;

namespace View.Utility
{
    public class ViewConstants
    {
        public static int POPUP = 0;
        public static int FULL_SCREEN = 1;
        public static int SMALL_SCREEN = 2;

        public static int MEDIA_PLAYING = 0;
        public static int MEDIA_PAUSED = 1;
        //public static int MEDIA_BUFFERING = 3;

        public static string NAVIGATE_SONG_FORMAT = "{0} fo {1} ";

        /*Call*/
        public static int OUTGOING = 1;
        public static int CONNECTED = 2;
        public static int INCOMMING = 3;
        /**/


        public static Guid ContentID = Guid.Empty;
        public static Guid AlbumID = Guid.Empty;
        public static Guid ImageID = Guid.Empty;
        public static Guid CommentID = Guid.Empty;
        public static Guid NewsFeedID = Guid.Empty;
        public static int NavigateFrom = 0;
        //PopPup BG Colors
        public static readonly string POP_UP_BG_BORDER1 = "#DD000000";
        public static readonly string POP_UP_BG_BORDER1_LIGHT = "#AA000000";
        public static readonly string POP_UP_BG_BORDER2 = "#FFFFFFFF";
        public static readonly string POP_UP_BORDER_BRUSH = "#d0dbe1";
        public static readonly string POP_UP_TRANSPARENT_BG_COLOR = "#0000ffff";

        //DigitsVarifications
        public static DigitsData DigitsDatas { get; set; }

        public const int MinRingIdLength = 8;

        public const int MinUserNameLength = 1;
        public const int MaxUserNameLength = 100;

        public const int MinPasswordLength = 6;
        public const int MaxPasswordLength = 31;

        public const int MinPhoneNumberLength = 7;

        public const int MinVCodeLength = 4;
    }
}

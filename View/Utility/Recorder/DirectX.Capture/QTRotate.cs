﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace View.Utility.Recorder.DirectX.Capture
{
    public class QTRotate
    {

        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(QTRotate).Name);

        public double GetRotation(string fileName)
        {
            return Process(fileName);
        }

        public double SetRotation(string fileName, double angle)
        {
            return Process(fileName, angle);
        }

        private Atom ReadAtom(FileStream datastream)
        {
            byte[] temp = new byte[8];
            datastream.Read(temp, 0, temp.Length);
            object[] d = QTRotate.Unpack(">L4s", temp);

            Atom atom = new Atom();
            atom.AtomSize = (uint)d[0];
            atom.AtomType = (string)d[1];
            return atom;
        }

        private List<Index> GetIndex(FileStream datastream)
        {
            List<Index> index = new List<Index>();

            while (datastream.Position != datastream.Length)
            {
                Atom atom = null;

                try
                {
                    atom = ReadAtom(datastream);
                }
                catch
                {
                    break;
                }

                index.Add(new Index(atom.AtomType, datastream.Position - 8, atom.AtomSize));

                if (atom.AtomSize < 8)
                {
                    break;
                }
                else
                {
                    datastream.Seek(atom.AtomSize - 8, SeekOrigin.Current);
                }
            }

            List<string> top_level_atoms = index.Select(P => P.Type).Distinct().ToList();
            foreach (string key in _AtomType)
            {
                if (!top_level_atoms.Contains(key))
                {
                    throw new Exception(key + " atom not found, is this a valid MOV/MP4 file?");
                }
            }
            return index;
        }

        private IEnumerable<string> FindAtoms(long size, FileStream datastream)
        {
            long stop = datastream.Position + size;

            while (datastream.Position < stop)
            {
                Atom atom = null;
                try
                {
                    atom = ReadAtom(datastream);
                }
                catch
                {
                    throw new Exception("Error reading next atom!");
                }

                if (atom.AtomType.Equals("trak"))
                {
                    foreach (string atomType in FindAtoms(atom.AtomSize - 8, datastream))
                        yield return atomType;
                }
                else if (atom.AtomType.Equals("mvhd") || atom.AtomType.Equals("tkhd"))
                {
                    yield return atom.AtomType;
                }
                else
                {
                    datastream.Seek(atom.AtomSize - 8, SeekOrigin.Current);
                }
            }
        }

        public double Process(string fileName, double? angle = null)
        {
            HashSet<double> degrees = new HashSet<double>();
            FileStream datastream = null;

            try
            {
                datastream = File.Open(fileName, FileMode.Open, FileAccess.ReadWrite);
                List<Index> index = GetIndex(datastream);
                long moov_size = -1;

                foreach (Index idx in index)
                {
                    if (idx.Type.Equals("moov"))
                    {
                        moov_size = idx.Length;
                        datastream.Seek(idx.Offset + 8, SeekOrigin.Begin);
                    }
                }

                if (moov_size == -1)
                {
                    throw new Exception("Couldn't find moov!");
                }

                foreach (string atom_type in FindAtoms(moov_size - 8, datastream))
                {
                    byte[] vf = new byte[4];
                    datastream.Read(vf, 0, vf.Length);

                    byte version = (byte)QTRotate.Unpack(">Bxxx", vf)[0];
                    uint flags = (uint)QTRotate.Unpack(">L", vf)[0] & 0x00FFFFFF;

                    if (version == 1)
                    {
                        if (atom_type.Equals("mvhd"))
                        {
                            byte[] temp = new byte[28];
                            datastream.Read(temp, 0, temp.Length);
                        }
                        else if (atom_type.Equals("tkhd"))
                        {
                            byte[] temp = new byte[32];
                            datastream.Read(temp, 0, temp.Length);
                        }
                    }
                    else if (version == 0)
                    {
                        if (atom_type.Equals("mvhd"))
                        {
                            byte[] temp = new byte[16];
                            datastream.Read(temp, 0, temp.Length);
                        }
                        else if (atom_type.Equals("tkhd"))
                        {
                            byte[] temp = new byte[20];
                            datastream.Read(temp, 0, temp.Length);
                        }
                    }
                    else
                    {
                        throw new Exception("Unknown " + atom_type + " version: " + version + "!");
                    }

                    byte[] tp = new byte[16];
                    datastream.Read(tp, 0, tp.Length);

                    byte[] tempMatrix = new byte[36];
                    datastream.Read(tempMatrix, 0, tempMatrix.Length);
                    object[] matrix = QTRotate.Unpack(">9l", tempMatrix);

                    if (atom_type.Equals("tkhd") && (angle != null))
                    {
                        double radians = (double)angle / (180 / Math.PI);
                        int cos_deg = (int)((1 << 16) * Math.Cos(radians));
                        int sin_deg = (int)((1 << 16) * Math.Sin(radians));
                        byte[] value = QTRotate.Pack(new object[9] { cos_deg, sin_deg, 0, -sin_deg, cos_deg, 0, 0, 0, (1 << 30) });
                        datastream.Seek(-36, SeekOrigin.Current);
                        datastream.Write(value, 0, value.Length);
                    }
                    else
                    {
                        for (int x = 0; x < 9; x++)
                        {
                            if ((x + 1) % 3 > 0)
                            {
                                matrix[x] = (int)(matrix[x]) / (1 << 16);
                            }
                            else
                            {
                                matrix[x] = (int)(matrix[x]) / (1 << 30);
                            }
                        }

                        if (atom_type.Equals("mvhd") || atom_type.Equals("tkhd"))
                        {
                            double deg = -(Math.Asin(double.Parse(matrix[3] + "")) * (180.0 / Math.PI)) % 360;

                            if (deg <= 0)
                            {
                                deg = (Math.Acos(double.Parse(matrix[0] + "")) * (180.0 / Math.PI));
                            }

                            if (deg > 0)
                            {
                                degrees.Add(deg);
                            }
                        }
                    }

                    if (atom_type.Equals("mvhd"))
                    {
                        byte[] temp = new byte[28];
                        datastream.Read(temp, 0, temp.Length);
                    }
                    else if (atom_type.Equals("tkhd"))
                    {
                        byte[] temp = new byte[8];
                        datastream.Read(temp, 0, temp.Length);
                    }
                }

                datastream.Close();
                datastream.Dispose();
                GC.SuppressFinalize(datastream);
                datastream = null;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace);
                if (datastream != null)
                {
                    datastream.Close();
                    datastream.Dispose();
                    GC.SuppressFinalize(datastream);
                    datastream = null;
                }
            }

            if (degrees.Count == 0)
            {
                return 0;
            }
            else if (degrees.Count == 1)
            {
                return degrees.LastOrDefault();
            }
            else
            {
                return -1;
            }
        }

        static List<string> _AtomType = new List<string>();

        static QTRotate()
        {
            _AtomType.Add("ftyp");
            _AtomType.Add("moov");
            _AtomType.Add("mdat");
        }

        public static object[] Unpack(string fmt, byte[] bytes)
        {
            if (fmt.Length < 1) throw new ArgumentException("Format string cannot be empty.");

            bool endianFlip = false;
            if (fmt.Substring(0, 1) == "<")
            {
                if (BitConverter.IsLittleEndian == false) endianFlip = true;
                fmt = fmt.Substring(1);
            }
            else if (fmt.Substring(0, 1) == ">")
            {
                if (BitConverter.IsLittleEndian == true) endianFlip = true;
                fmt = fmt.Substring(1);
            }

            int totalByteLength = 0;
            string stringLength = String.Empty;

            foreach (char c in fmt.ToCharArray())
            {
                switch (c)
                {
                    case 'q':
                    case 'Q':
                        int length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        totalByteLength += length * 8;
                        stringLength = String.Empty;
                        break;
                    case 'l':
                    case 'L':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        totalByteLength += length * 4;
                        stringLength = String.Empty;
                        break;
                    case 'h':
                    case 'H':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        totalByteLength += length * 2;
                        stringLength = String.Empty;
                        break;
                    case 'b':
                    case 'B':
                    case 'x':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        totalByteLength += length * 1;
                        stringLength = String.Empty;
                        break;
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        stringLength += c;
                        break;
                    case 's':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        totalByteLength += length * 1;
                        stringLength = String.Empty;
                        break;
                    default:
                        throw new ArgumentException("Invalid character found in format string.");
                }
            }

            if (bytes.Length != totalByteLength)
                throw new ArgumentException("The number of bytes provided does not match the total length of the format string.");

            int byteArrayPosition = 0;
            stringLength = String.Empty;
            List<object> outputList = new List<object>();
            byte[] buf;

            foreach (char c in fmt.ToCharArray())
            {
                switch (c)
                {
                    case 'q':
                        int length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        int startPoint = byteArrayPosition;
                        byteArrayPosition += length * 8;

                        while ((length--) > 0)
                        {
                            long longResult = 0;
                            try
                            {
                                byte[] b = new byte[8];
                                b[0] = bytes[startPoint++];
                                b[1] = bytes[startPoint++];
                                b[2] = bytes[startPoint++];
                                b[3] = bytes[startPoint++];
                                b[4] = bytes[startPoint++];
                                b[5] = bytes[startPoint++];
                                b[6] = bytes[startPoint++];
                                b[7] = bytes[startPoint++];
                                if (BitConverter.IsLittleEndian) Array.Reverse(b);
                                longResult = BitConverter.ToInt64(b, 0);
                            }
                            catch (Exception)
                            {
                            }
                            outputList.Add((object)longResult);
                        }
                        break;
                    case 'Q':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        startPoint = byteArrayPosition;
                        byteArrayPosition += length * 8;

                        while ((length--) > 0)
                        {
                            ulong ulongResult = 0;
                            try
                            {
                                byte[] b = new byte[8];
                                b[0] = bytes[startPoint++];
                                b[1] = bytes[startPoint++];
                                b[2] = bytes[startPoint++];
                                b[3] = bytes[startPoint++];
                                b[4] = bytes[startPoint++];
                                b[5] = bytes[startPoint++];
                                b[6] = bytes[startPoint++];
                                b[7] = bytes[startPoint++];
                                if (BitConverter.IsLittleEndian) Array.Reverse(b);
                                ulongResult = BitConverter.ToUInt64(b, 0);
                            }
                            catch (Exception)
                            {
                            }
                            outputList.Add((object)ulongResult);
                        }
                        break;
                    case 'l':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        startPoint = byteArrayPosition;
                        byteArrayPosition += length * 4;

                        while ((length--) > 0)
                        {
                            int intResult = 0;
                            intResult += (bytes[startPoint++] & 0xFF) << 24;
                            intResult += (bytes[startPoint++] & 0xFF) << 16;
                            intResult += (bytes[startPoint++] & 0xFF) << 8;
                            intResult += (bytes[startPoint++] & 0xFF);
                            intResult = Convert.ToInt32(intResult);
                            outputList.Add((object)intResult);
                        }
                        break;
                    case 'L':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        startPoint = byteArrayPosition;
                        byteArrayPosition += length * 4;

                        while ((length--) > 0)
                        {
                            uint uintResult = 0;
                            uintResult += (uint)(bytes[startPoint++] & 0xFF) << 24;
                            uintResult += (uint)(bytes[startPoint++] & 0xFF) << 16;
                            uintResult += (uint)(bytes[startPoint++] & 0xFF) << 8;
                            uintResult += (uint)(bytes[startPoint++] & 0xFF);
                            uintResult = Convert.ToUInt32(uintResult);
                            outputList.Add((object)uintResult);
                        }
                        break;
                    case 'h':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        startPoint = byteArrayPosition;
                        byteArrayPosition += length * 2;

                        while ((length--) > 0)
                        {
                            int shortResult = 0;
                            shortResult += (bytes[startPoint++] & 0xFF) << 8;
                            shortResult += (bytes[startPoint++] & 0xFF);
                            outputList.Add((object)Convert.ToInt16(shortResult));
                        }
                        break;
                    case 'H':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        startPoint = byteArrayPosition;
                        byteArrayPosition += length * 2;

                        while ((length--) > 0)
                        {
                            int ushortResult = 0;
                            ushortResult += (bytes[startPoint++] & 0xFF) << 8;
                            ushortResult += (bytes[startPoint++] & 0xFF);
                            outputList.Add((object)Convert.ToUInt16(ushortResult));
                        }
                        break;
                    case 'b':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        startPoint = byteArrayPosition;
                        byteArrayPosition += length * 1;

                        while ((length--) > 0)
                        {
                            buf = new byte[1];
                            Array.Copy(bytes, startPoint, buf, 0, 1);
                            outputList.Add((object)(sbyte)buf[0]);
                        }
                        break;
                    case 'B':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        startPoint = byteArrayPosition;
                        byteArrayPosition += length * 1;

                        while ((length--) > 0)
                        {
                            buf = new byte[1];
                            Array.Copy(bytes, startPoint, buf, 0, 1);
                            outputList.Add((object)(byte)buf[0]);
                        }
                        break;
                    case 'x':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        byteArrayPosition += length * 1;
                        break;
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        stringLength += c;
                        break;
                    case 's':
                        length = !String.IsNullOrEmpty(stringLength) ? int.Parse(stringLength) : 1;
                        startPoint = byteArrayPosition;
                        byteArrayPosition += length * 1;

                        string stringResult = Encoding.ASCII.GetString(bytes, startPoint, length);
                        outputList.Add((object)stringResult);
                        stringLength = String.Empty;
                        break;
                    default:
                        throw new ArgumentException("You should not be here.");
                }
            }
            return outputList.ToArray();
        }

        public static byte[] Pack(object[] items, bool LittleEndian = true)
        {

            List<byte> outputBytes = new List<byte>();
            bool endianFlip = (LittleEndian != BitConverter.IsLittleEndian);

            string outString = (LittleEndian == false ? ">" : "<");

            foreach (object o in items)
            {
                byte[] theseBytes = TypeAgnosticGetBytes(o);
                if (endianFlip == true) theseBytes = (byte[])theseBytes.Reverse();
                outString += GetFormatSpecifierFor(o);
                outputBytes.AddRange(theseBytes);
            }

            return outputBytes.ToArray();

        }


        private static byte[] TypeAgnosticGetBytes(object o)
        {
            if (o is string) return Encoding.ASCII.GetBytes((string)o);
            if (o is int) return BitConverter.GetBytes((int)o);
            if (o is uint) return BitConverter.GetBytes((uint)o);
            if (o is long) return BitConverter.GetBytes((long)o);
            if (o is ulong) return BitConverter.GetBytes((ulong)o);
            if (o is short) return BitConverter.GetBytes((short)o);
            if (o is ushort) return BitConverter.GetBytes((ushort)o);
            if (o is byte || o is sbyte) return new byte[] { (byte)o };
            throw new ArgumentException("Unsupported object type found");
        }

        private static string GetFormatSpecifierFor(object o)
        {
            if (o is string) return "s";
            if (o is int) return "l";
            if (o is uint) return "L";
            if (o is long) return "q";
            if (o is ulong) return "Q";
            if (o is short) return "h";
            if (o is ushort) return "H";
            if (o is byte) return "B";
            if (o is sbyte) return "b";
            throw new ArgumentException("Unsupported object type found");
        }

        private class Index
        {
            public Index(string type, long offset, long lenght)
            {
                this.Type = type;
                this.Offset = offset;
                this.Length = lenght;
            }

            public string Type { get; set; }
            public long Offset { get; set; }
            public long Length { get; set; }
        }

        private class Atom
        {
            public uint AtomSize { get; set; }
            public string AtomType { get; set; }
        }


    }
}

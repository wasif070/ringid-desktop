﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using View;
using View.Utility.Recorder.DirectShowNET;

namespace View.Utility.Recorder.DirectX.Capture
{
    class BitmapCapture : ISampleGrabberCB
    {

        protected enum GraphState
        {
            Null,			// No filter graph at all
            Created,		// Filter graph created with device filters added
            Rendered,		// Filter complete built, ready to run (possibly previewing)
        }

        public Control PreviewWindow
        {
            get { return (previewWindow); }
            set
            {
                derenderGraph();
                previewWindow = value;
                wantPreviewRendered = ((previewWindow != null) && (videoDevice != null));
                renderGraph();
                startPreviewIfNeeded();
            }
        }

        public VideoCapabilities VideoCaps
        {
            get
            {
                if (videoCaps == null)
                {
                    if (videoStreamConfig != null)
                    {
                        try
                        {
                            videoCaps = new VideoCapabilities(videoStreamConfig);
                        }
                        catch (Exception ex) { Debug.WriteLine("VideoCaps: unable to create videoCaps." + ex.ToString()); }
                    }
                }
                return (videoCaps);
            }
        }

        public Filter VideoDevice { get { return (videoDevice); } }

        public Filter VideoCompressor
        {
            get { return (videoCompressor); }
            set
            {
                destroyGraph();
                videoCompressor = value;
                renderGraph();
                startPreviewIfNeeded();
            }
        }

        public Source VideoSource
        {
            get { return (VideoSources.CurrentSource); }
            set { VideoSources.CurrentSource = value; }
        }

        public SourceCollection VideoSources
        {
            get
            {
                if (videoSources == null)
                {
                    try
                    {
                        if (videoDevice != null)
                            videoSources = new SourceCollection(captureGraphBuilder, videoDeviceFilter, true);
                        else
                            videoSources = new SourceCollection();
                    }
                    catch (Exception ex) { Debug.WriteLine("VideoSources: unable to create VideoSources." + ex.ToString()); }
                }
                return (videoSources);
            }
        }

        public double FrameRate
        {
            get
            {
                long avgTimePerFrame = (long)getStreamConfigSetting(videoStreamConfig, "AvgTimePerFrame");
                return ((double)10000000 / avgTimePerFrame);
            }
            set
            {
                long avgTimePerFrame = (long)(10000000 / value);
                setStreamConfigSetting(videoStreamConfig, "AvgTimePerFrame", avgTimePerFrame);
            }
        }

        public Size FrameSize
        {
            get
            {
                BitmapInfoHeader bmiHeader;
                bmiHeader = (BitmapInfoHeader)getStreamConfigSetting(videoStreamConfig, "BmiHeader");
                Size size = new Size(bmiHeader.Width, bmiHeader.Height);
                return (size);
            }
            set
            {
                BitmapInfoHeader bmiHeader;
                bmiHeader = (BitmapInfoHeader)getStreamConfigSetting(videoStreamConfig, "BmiHeader");
                bmiHeader.Width = value.Width;
                bmiHeader.Height = value.Height;
                setStreamConfigSetting(videoStreamConfig, "BmiHeader", bmiHeader);
            }
        }

        private GraphState graphState = GraphState.Null;		// State of the internal filter graph
        private bool isPreviewRendered = false;			// When graphState==Rendered, have we rendered the preview stream?
        private bool wantPreviewRendered = false;		// Do we need the preview stream rendered (VideoDevice and PreviewWindow != null)

        private int rotCookie = 0;						// Cookie into the Running Object Table
        private Filter videoDevice = null;					// Property Backer: Video capture device filter
        private Filter videoCompressor = null;				// Property Backer: Video compression filter
        private string filename = "";						// Property Backer: Name of file to capture to
        private Control previewWindow = null;				// Property Backer: Owner control for preview
        private VideoCapabilities videoCaps = null;					// Property Backer: capabilities of video device
        private SourceCollection videoSources = null;				// Property Backer: list of physical video sources

        private IGraphBuilder graphBuilder;						// DShow Filter: Graph builder 
        private IMediaControl mediaControl;						// DShow Filter: Start/Stop the filter graph -> copy of graphBuilder
        private IVideoWindow videoWindow;						// DShow Filter: Control preview window -> copy of graphBuilder
        private ICaptureGraphBuilder2 captureGraphBuilder = null;	// DShow Filter: building graphs for capturing video
        private IAMStreamConfig videoStreamConfig = null;			// DShow Filter: configure frame rate, size
        private IBaseFilter videoDeviceFilter = null;			// DShow Filter: selected video device
        private IBaseFilter videoCompressorFilter = null;		// DShow Filter: selected video compressor

        private IBaseFilter baseGrabFlt;
        public ISampleGrabber sampGrabber = null;
        private VideoInfoHeader videoInfoHeader;
        private byte[] savedArray;
        private int bufferedSize;

        public delegate void TakePictureHandler(Bitmap bitmap);
        public event TakePictureHandler OnTakePicture;

        public delegate void StreamHandler(Bitmap bitmap);
        public event StreamHandler OnStream;

        public BitmapCapture(Filter videoDevice)
        {
            if (videoDevice == null)
                throw new ArgumentException("The videoDevice parameter must be set to a valid Filter.\n");
            this.videoDevice = videoDevice;
            createGraph();
        }

        ~BitmapCapture()
        {
            Dispose();
        }

        public void TakePicture()
        {
            if (savedArray == null)
            {
                int size = videoInfoHeader.BmiHeader.ImageSize;
                if ((size < 1000) || (size > 16000000))
                    return;
                savedArray = new byte[size];
                sampGrabber.SetCallback(this, 1);
            }
        }

        public void PausePreview()
        {
            sampGrabber.SetOneShot(true);
        }

        public void Dispose()
        {
            wantPreviewRendered = false;
            OnTakePicture = null;

            try { destroyGraph(); }
            catch { }

            if (videoSources != null)
                videoSources.Dispose(); videoSources = null;

        }

        private void createGraph()
        {
            Guid cat;
            Guid med;
            int hr;
            Type comType = null;
            object comObj = null;

            if (videoDevice == null)
                throw new ArgumentException("The video have not been set. Please set one or both to valid capture devices.\n");

            if ((int)graphState < (int)GraphState.Created)
            {
                GC.Collect();

                graphBuilder = (IGraphBuilder)Activator.CreateInstance(Type.GetTypeFromCLSID(Clsid.FilterGraph, true));

                Guid clsid = Clsid.CaptureGraphBuilder2;
                Guid riid = typeof(ICaptureGraphBuilder2).GUID;
                captureGraphBuilder = (ICaptureGraphBuilder2)DsBugWO.CreateDsInstance(ref clsid, ref riid);

                hr = captureGraphBuilder.SetFiltergraph(graphBuilder);
                if (hr < 0) Marshal.ThrowExceptionForHR(hr);

                comType = Type.GetTypeFromCLSID(Clsid.SampleGrabber);
                if (comType == null)
                    throw new NotImplementedException(@"DirectShow SampleGrabber not installed/registered!");
                comObj = Activator.CreateInstance(comType);
                sampGrabber = (ISampleGrabber)comObj; comObj = null;

                baseGrabFlt = (IBaseFilter)sampGrabber;

#if DEBUG
                DsROT.AddGraphToRot(graphBuilder, out rotCookie);
#endif

                AMMediaType media = new AMMediaType();
                if (VideoDevice != null)
                {
                    videoDeviceFilter = (IBaseFilter)Marshal.BindToMoniker(VideoDevice.MonikerString);
                    hr = graphBuilder.AddFilter(videoDeviceFilter, "Video Capture Device");
                    if (hr < 0) Marshal.ThrowExceptionForHR(hr);

                    media.majorType = MediaType.Video;
                    media.subType = MediaSubType.RGB24;
                    media.formatType = FormatType.VideoInfo;
                    hr = sampGrabber.SetMediaType(media);
                    if (hr < 0)
                        Marshal.ThrowExceptionForHR(hr);

                    hr = graphBuilder.AddFilter(baseGrabFlt, "Ds.NET Grabber");
                    if (hr < 0) Marshal.ThrowExceptionForHR(hr);
                }

                if (VideoCompressor != null)
                {
                    videoCompressorFilter = (IBaseFilter)Marshal.BindToMoniker(VideoCompressor.MonikerString);
                    hr = graphBuilder.AddFilter(videoCompressorFilter, "Video Compressor");
                    if (hr < 0) Marshal.ThrowExceptionForHR(hr);
                }

                object o;
                cat = PinCategory.Capture;
                med = MediaType.Interleaved;
                Guid iid = typeof(IAMStreamConfig).GUID;
                hr = captureGraphBuilder.FindInterface(
                    ref cat, ref med, videoDeviceFilter, ref iid, out o);

                if (hr != 0)
                {
                    med = MediaType.Video;
                    hr = captureGraphBuilder.FindInterface(
                        ref cat, ref med, videoDeviceFilter, ref iid, out o);

                    if (hr != 0)
                        o = null;
                }
                videoStreamConfig = o as IAMStreamConfig;

                mediaControl = (IMediaControl)graphBuilder;

                if (videoSources != null) videoSources.Dispose(); videoSources = null;

                videoCaps = null;

                videoInfoHeader = (VideoInfoHeader)Marshal.PtrToStructure(media.formatPtr, typeof(VideoInfoHeader));
                Marshal.FreeCoTaskMem(media.formatPtr); media.formatPtr = IntPtr.Zero;

                hr = sampGrabber.SetBufferSamples(false);
                if (hr == 0)
                    hr = sampGrabber.SetOneShot(false);
                if (hr == 0)
                    hr = sampGrabber.SetCallback(null, 0);
                if (hr == 0)
                    hr = sampGrabber.SetCallback(null, 0);
                if (hr < 0)
                    Marshal.ThrowExceptionForHR(hr);

                graphState = GraphState.Created;
            }
        }

        private void renderGraph()
        {
            Guid cat;
            Guid med;
            int hr;
            bool didSomething = false;
            const int WS_CHILD = 0x40000000;
            const int WS_CLIPCHILDREN = 0x02000000;
            const int WS_CLIPSIBLINGS = 0x04000000;

            if (filename == null)
                throw new ArgumentException("The Filename property has not been set to a file.\n");

            if (mediaControl != null)
                mediaControl.Stop();

            createGraph();

            if (!wantPreviewRendered && isPreviewRendered)
                derenderGraph();

            if (wantPreviewRendered && !isPreviewRendered)
            {
                cat = PinCategory.Preview;
                med = MediaType.Video;
                hr = captureGraphBuilder.RenderStream(ref cat, ref med, videoDeviceFilter, baseGrabFlt, null);
                if (hr < 0) Marshal.ThrowExceptionForHR(hr);

                videoWindow = (IVideoWindow)graphBuilder;

                hr = videoWindow.put_Owner(previewWindow.Handle);
                if (hr < 0) Marshal.ThrowExceptionForHR(hr);

                hr = videoWindow.put_WindowStyle(WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
                if (hr < 0) Marshal.ThrowExceptionForHR(hr);

                previewWindow.Resize += new EventHandler(onPreviewWindowResize);
                onPreviewWindowResize(this, null);

                hr = videoWindow.put_Visible(DsHlp.OATRUE);
                if (hr < 0) Marshal.ThrowExceptionForHR(hr);

                isPreviewRendered = true;
                didSomething = true;

                AMMediaType media = new AMMediaType();
                hr = sampGrabber.GetConnectedMediaType(media);
                if (hr < 0)
                    Marshal.ThrowExceptionForHR(hr);
                if ((media.formatType != FormatType.VideoInfo) || (media.formatPtr == IntPtr.Zero))
                    throw new NotSupportedException("Unknown Grabber Media Format");

                videoInfoHeader = (VideoInfoHeader)Marshal.PtrToStructure(media.formatPtr, typeof(VideoInfoHeader));
                Marshal.FreeCoTaskMem(media.formatPtr); media.formatPtr = IntPtr.Zero;
            }

            if (didSomething)
                graphState = GraphState.Rendered;
        }

        private void startPreviewIfNeeded()
        {
            if (wantPreviewRendered && isPreviewRendered)
            {
                mediaControl.Run();
            }
        }

        private void derenderGraph()
        {
            if (mediaControl != null)
                mediaControl.Stop();

            if (videoWindow != null)
            {
                videoWindow.put_Visible(DsHlp.OAFALSE);
                videoWindow.put_Owner(IntPtr.Zero);
                videoWindow = null;
            }

            if (PreviewWindow != null)
                previewWindow.Resize -= new EventHandler(onPreviewWindowResize);

            if ((int)graphState >= (int)GraphState.Rendered)
            {
                graphState = GraphState.Created;
                isPreviewRendered = false;

                if (videoDeviceFilter != null)
                    removeDownstream(videoDeviceFilter, (videoCompressor == null));
            }
        }

        private void removeDownstream(IBaseFilter filter, bool removeFirstFilter)
        {
            IEnumPins pinEnum;
            int hr = filter.EnumPins(out pinEnum);
            pinEnum.Reset();
            if ((hr == 0) && (pinEnum != null))
            {
                IPin[] pins = new IPin[1];
                int f;
                do
                {
                    hr = pinEnum.Next(1, pins, out f);
                    if ((hr == 0) && (pins[0] != null))
                    {
                        IPin pinTo = null;
                        pins[0].ConnectedTo(out pinTo);
                        if (pinTo != null)
                        {
                            PinInfo info = new PinInfo();
                            hr = pinTo.QueryPinInfo(out info);
                            if ((hr == 0) && (info.dir == (PinDirection.Input)))
                            {
                                removeDownstream(info.filter, true);

                                graphBuilder.Disconnect(pinTo);
                                graphBuilder.Disconnect(pins[0]);

                                if (info.filter != videoCompressorFilter)
                                    graphBuilder.RemoveFilter(info.filter);
                            }
                            Marshal.ReleaseComObject(info.filter);
                            Marshal.ReleaseComObject(pinTo);
                        }
                        Marshal.ReleaseComObject(pins[0]);
                    }
                }
                while (hr == 0);

                Marshal.ReleaseComObject(pinEnum); pinEnum = null;
            }
        }

        private void destroyGraph()
        {
            try { derenderGraph(); }
            catch { }

            graphState = GraphState.Null;
            isPreviewRendered = false;

            if (rotCookie != 0)
            {
                DsROT.RemoveGraphFromRot(ref rotCookie);
                rotCookie = 0;
            }

            if (videoCompressorFilter != null)
                graphBuilder.RemoveFilter(videoCompressorFilter);

            if (videoDeviceFilter != null)
                graphBuilder.RemoveFilter(videoDeviceFilter);


            if (videoSources != null)
                videoSources.Dispose(); videoSources = null;

            if (graphBuilder != null)
                Marshal.ReleaseComObject(graphBuilder); graphBuilder = null;
            if (captureGraphBuilder != null)
                Marshal.ReleaseComObject(captureGraphBuilder); captureGraphBuilder = null;

            if (videoCompressorFilter != null)
                Marshal.ReleaseComObject(videoCompressorFilter); videoCompressorFilter = null;


            mediaControl = null;
            videoWindow = null;

            GC.Collect();
        }

        private void onPreviewWindowResize(object sender, EventArgs e)
        {
            if (videoWindow != null)
            {
                Rectangle rc = previewWindow.ClientRectangle;
                videoWindow.SetWindowPosition(0, 0, rc.Right, rc.Bottom);
            }
        }

        private object getStreamConfigSetting(IAMStreamConfig streamConfig, string fieldName)
        {
            if (streamConfig == null)
                throw new NotSupportedException();
            derenderGraph();

            object returnValue = null;
            IntPtr pmt = IntPtr.Zero;
            AMMediaType mediaType = new AMMediaType();

            try
            {
                int hr = streamConfig.GetFormat(out pmt);
                if (hr != 0)
                    Marshal.ThrowExceptionForHR(hr);
                Marshal.PtrToStructure(pmt, mediaType);

                object formatStruct;
                if (mediaType.formatType == FormatType.WaveEx)
                    formatStruct = new WaveFormatEx();
                else if (mediaType.formatType == FormatType.VideoInfo)
                    formatStruct = new VideoInfoHeader();
                else if (mediaType.formatType == FormatType.VideoInfo2)
                    formatStruct = new VideoInfoHeader2();
                else
                    throw new NotSupportedException("This device does not support a recognized format block.");

                Marshal.PtrToStructure(mediaType.formatPtr, formatStruct);

                Type structType = formatStruct.GetType();
                FieldInfo fieldInfo = structType.GetField(fieldName);
                if (fieldInfo == null)
                    throw new NotSupportedException("Unable to find the member '" + fieldName + "' in the format block.");

                returnValue = fieldInfo.GetValue(formatStruct);
            }
            finally
            {
                DsUtils.FreeAMMediaType(mediaType);
                Marshal.FreeCoTaskMem(pmt);
            }
            renderGraph();
            startPreviewIfNeeded();

            return (returnValue);
        }

        private object setStreamConfigSetting(IAMStreamConfig streamConfig, string fieldName, object newValue)
        {
            if (streamConfig == null)
                throw new NotSupportedException();
            derenderGraph();

            object returnValue = null;
            IntPtr pmt = IntPtr.Zero;
            AMMediaType mediaType = new AMMediaType();

            try
            {
                int hr = streamConfig.GetFormat(out pmt);
                if (hr != 0)
                    Marshal.ThrowExceptionForHR(hr);
                Marshal.PtrToStructure(pmt, mediaType);

                object formatStruct;
                if (mediaType.formatType == FormatType.WaveEx)
                    formatStruct = new WaveFormatEx();
                else if (mediaType.formatType == FormatType.VideoInfo)
                    formatStruct = new VideoInfoHeader();
                else if (mediaType.formatType == FormatType.VideoInfo2)
                    formatStruct = new VideoInfoHeader2();
                else
                    throw new NotSupportedException("This device does not support a recognized format block.");

                Marshal.PtrToStructure(mediaType.formatPtr, formatStruct);

                Type structType = formatStruct.GetType();
                FieldInfo fieldInfo = structType.GetField(fieldName);
                if (fieldInfo == null)
                    throw new NotSupportedException("Unable to find the member '" + fieldName + "' in the format block.");

                fieldInfo.SetValue(formatStruct, newValue);

                Marshal.StructureToPtr(formatStruct, mediaType.formatPtr, false);

                hr = streamConfig.SetFormat(mediaType);
                if (hr != 0)
                    Marshal.ThrowExceptionForHR(hr);
            }
            finally
            {
                DsUtils.FreeAMMediaType(mediaType);
                Marshal.FreeCoTaskMem(pmt);
            }
            renderGraph();
            startPreviewIfNeeded();

            return (returnValue);
        }

        int ISampleGrabberCB.SampleCB(double SampleTime, IMediaSample pSample)
        {
            return 0;
        }

        int ISampleGrabberCB.BufferCB(double SampleTime, IntPtr pBuffer, int BufferLen)
        {
            bufferedSize = BufferLen;
            int w = videoInfoHeader.BmiHeader.Width;
            int h = videoInfoHeader.BmiHeader.Height;

            int stride = w * 3;

            Marshal.Copy(pBuffer, savedArray, 0, BufferLen);

            GCHandle handle = GCHandle.Alloc(savedArray, GCHandleType.Pinned);
            int scan0 = (int)handle.AddrOfPinnedObject();
            scan0 += (h - 1) * stride;
            Bitmap b = new Bitmap(w, h, -stride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, (IntPtr)scan0);
            handle.Free();

            if (OnStream != null)
            {
                OnStream(b);
            }

            if (OnTakePicture != null)
            {
                OnTakePicture(b);
            }
            return 0;
        }

        public void CheckWebcam()
        {
            int errorCode = this.mediaControl.Run();
            if (errorCode != 0)
            {
                Marshal.ThrowExceptionForHR(errorCode);
            }
            //this.mediaControl.Stop();
        }

    }
}

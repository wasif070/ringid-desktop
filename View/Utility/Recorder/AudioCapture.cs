﻿using log4net;
using MediaToolkit;
using MediaToolkit.Model;
using Models.Constants;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using View.Constants;

namespace View.Utility.Recorder
{
    public class AudioCapture : IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AudioCapture).Name);
        public const string AUDIO_FORMAT = ".wav";
        private short Channels = 1;
        private int SampleRate = 8000;
        private short BitsPerSample = 16;

        private int? _MaxCountDown { get; set; }
        public string _FileName = String.Empty;

        private CustomizeTimer _RecorderTimer = new CustomizeTimer(100);
        public delegate void RecordingCountDownChangeHandler(int value);
        public event RecordingCountDownChangeHandler OnRecordingCountDownChange;
        public delegate void RecordingCompletedHandler();
        public event RecordingCompletedHandler OnRecordingCompleted;

        private WaveIn waveIn = null;
        //private NAudio.CoreAudioApi.Ext.WasapiCapture waveIn = null;
        private WaveFileWriter waveWriter = null;

        public AudioCapture(int? maxCountDown = null)
        {
            _MaxCountDown = maxCountDown;

            _RecorderTimer.IntervalInSecond = 1;
            _RecorderTimer.Tick += (o, e, s) =>
            {
                if (OnRecordingCountDownChange != null)
                {
                    OnRecordingCountDownChange(o);
                }

                if (_MaxCountDown != null && o >= _MaxCountDown)
                {
                    _RecorderTimer.Stop();
                }
            };
            _RecorderTimer.Compete += (o) =>
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    Stop();
                }, DispatcherPriority.Send);

                if (OnRecordingCompleted != null)
                {
                    OnRecordingCompleted();
                }
            };
        }

        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (waveWriter == null || _RecorderTimer.IsAlive == false || _RecorderTimer.IsPaused) return;
            waveWriter.WriteData(e.Buffer, 0, e.BytesRecorded);
            waveWriter.Flush();
        }

        public bool Record(string fileName)
        {
            _FileName = fileName;
            int deviceNumber = GetDeviceNumber();

            if (deviceNumber >= 0)
            {
                waveIn = new WaveIn();
                //waveIn = new NAudio.Wave.Ext.WasapiLoopbackCapture();
                waveIn.DeviceNumber = deviceNumber;
                waveIn.WaveFormat = new WaveFormat(SampleRate, BitsPerSample, Channels);
                waveIn.DataAvailable += waveIn_DataAvailable;
                waveWriter = new WaveFileWriter(_FileName, waveIn.WaveFormat);

                _RecorderTimer.Start();
                waveIn.StartRecording();
                return true;
            }

            return false;
        }

        private int GetDeviceNumber()
        {
            MMDeviceCollection deviceList = new MMDeviceEnumerator().EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active);
            int selectedDeviceNumber = SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER;

            int deviceNumber = -1;
            if (deviceList.Count == 1)
            {
                deviceNumber = 0;
            }
            else if (selectedDeviceNumber >= 0 && deviceList.Count > selectedDeviceNumber)
            {
                deviceNumber = selectedDeviceNumber;
            }
            else if (deviceList.Count > 1)
            {
                for (int idx = 0; idx < deviceList.Count; idx++)
                {
                    if (deviceList[idx].FriendlyName.ToLower().Contains("high definition"))
                    {
                        deviceNumber = idx;
                        break;
                    }
                }
            }

            return deviceNumber;
        }

        public void RequestForStop()
        {
            _RecorderTimer.Stop();
        }

        private void Stop()
        {
            if (waveIn != null)
            {
                waveIn.StopRecording();
                waveIn.Dispose();
                waveIn = null;
            }

            if (waveWriter != null)
            {
                waveWriter.Dispose();
                waveWriter = null;
            }
        }

        public void Pause()
        {
            waveIn.StopRecording();
            _RecorderTimer.Pause();
        }

        public void Resume()
        {
            waveIn.StartRecording();
            _RecorderTimer.Resume();
        }

        public void Dispose()
        {
            _RecorderTimer.Stop();
            Stop();
        }

    }
}

﻿using log4net;
using Models.Constants;
using Models.Utility;
using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Threading;
using View.Utility.Recorder.DirectX.Capture;

namespace View.Utility.Recorder
{
    class RecorderWebcamPreview : Panel
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RecorderWebcamPreview).Name);
        public static string VIDEO_FORMAT = ".avi";

        private VideoCapture capture = null;
        private Filters filters = new Filters();
        //private int _TotalFile = 0;
        private string _FileName = String.Empty;

        private int CountDown { get; set; }
        private int? _MaxCountDown { get; set; }

        public const int ERROR_NONE = 0;
        public const int ERROR_WEBCAM_NOT_FOUND = 1;
        public const int ERROR_WEBCAM_ALREADY_USING = 2;

        private CustomizeTimer _RecorderTimer = new CustomizeTimer(100);
        public delegate void RecordingCountDownChangeHandler(int value);
        public event RecordingCountDownChangeHandler OnRecordingCountDownChange;
        public delegate void RecordingCompletedHandler();
        public event RecordingCompletedHandler OnRecordingCompleted;

        public RecorderWebcamPreview()
        {
            _RecorderTimer.IntervalInSecond = 1;
            _RecorderTimer.Tick += (o, e, s) =>
            {
                if (OnRecordingCountDownChange != null)
                {
                    OnRecordingCountDownChange(o);
                }

                if (_MaxCountDown != null && o >= _MaxCountDown)
                {
                    _RecorderTimer.Stop();
                }
            };
            _RecorderTimer.Compete += (o) =>
            {
                StopRecording(o);
                if (OnRecordingCompleted != null)
                {
                    OnRecordingCompleted();
                }
            };
        }

        public int InitWebcamDevices(int? maxCountDown = null)
        {
            _MaxCountDown = maxCountDown;
            try
            {
                if (filters.VideoInputDevices == null || filters.VideoInputDevices.Count <= 0)
                {
                    return RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }

                Filter videoDevice = GetVideoDevice();
                Filter audioDevice = GetAudioDevice();

                capture = new VideoCapture(videoDevice, audioDevice);
                capture.PreviewWindow = this;
                capture.CheckWebcam();

                //capture.VideoCompressor = filters.VideoCompressors[0];
                //capture.AudioCompressor = filters.AudioCompressors[0];
                //capture.FrameRate = 29.997;
                //capture.FrameSize = new System.Drawing.Size(640, 480);
                //capture.AudioSamplingRate = 44100;
                //capture.AudioSampleSize = 16;
                //Filter videoDevice = capture.VideoDevice;
                //Filter audioDevice = capture.AudioDevice;
                //Filter videoCompressor = capture.VideoCompressor;
                //Filter audioCompressor = capture.AudioCompressor;
                //Source videoSource = capture.VideoSource;
                //Source audioSource = capture.AudioSource;
                //int frameRate = (int)(capture.FrameRate * 1000);
                //System.Drawing.Size frameSize = capture.FrameSize;
                //short audioChannels = capture.AudioChannels;
                //int samplingRate = capture.AudioSamplingRate;
                //short sampleSize = capture.AudioSampleSize;
            }
            catch (Exception)
            {
                return RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING;
            }
            return RecorderWebcamPreview.ERROR_NONE;
        }

        public int StartRecording(string fileName)
        {
            try
            {
                DeleteDumpFiles();
                _FileName = fileName;
                //_TotalFile = 0;
                CountDown = 0;

                if (capture == null)
                {
                    return RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }

                if (filters.VideoInputDevices == null || filters.VideoInputDevices.Count <= 0)
                {
                    return RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }

                if (!capture.Cued)
                {
                    capture.Filename = _FileName;
                }

                if (OnRecordingCountDownChange != null)
                {
                    OnRecordingCountDownChange(CountDown);
                }
                if (_RecorderTimer.IsAlive)
                {
                    _RecorderTimer.Stop();
                }

                capture.Start();
                _RecorderTimer.Start();
            }
            catch (Exception)
            {
                return RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING;
            }
            return RecorderWebcamPreview.ERROR_NONE;
        }

        public void RequestForStop()
        {
            _RecorderTimer.Stop();
        }

        private int StopRecording(int count)
        {
            try
            {
                if (capture == null)
                {
                    return RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }

                if (filters.VideoInputDevices == null || filters.VideoInputDevices.Count <= 0)
                {
                    return RecorderWebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }

                capture.Stop(count);
            }
            catch (Exception)
            {
                return RecorderWebcamPreview.ERROR_WEBCAM_ALREADY_USING;
            }
            return RecorderWebcamPreview.ERROR_NONE;
        }

        public void CloseRecorder()
        {
            try
            {
                if (_RecorderTimer.IsAlive)
                {
                    _RecorderTimer.Stop();
                }

                if (capture != null)
                {
                    capture.Dispose();
                    capture = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at CloseRecorder() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public new void Dispose()
        {
            try
            {
                CloseRecorder();
                base.Dispose();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                log.Error("Error at Dispose() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void DeleteDumpFiles()
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(_FileName) && File.Exists(_FileName))
                {
                    File.Delete(_FileName);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at DeleteDumpFiles() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }


        private Filter GetVideoDevice()
        {
            Filter device = null;
            if (filters.VideoInputDevices.Count > 0)
            {
                device = filters.VideoInputDevices[0];
                if (!String.IsNullOrWhiteSpace(SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID))
                {
                    foreach (Filter filter in filters.VideoInputDevices)
                    {
                        if (filter.MonikerString.Equals(SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID))
                        {
                            device = filter;
                        }
                    }
                }
            }
            return device;
        }

        private Filter GetAudioDevice()
        {
            Filter device = null;
            if (filters.AudioInputDevices.Count > 0)
            {
                int idx = 0;
                string deviceID = String.Empty;
                device = filters.AudioInputDevices[0];

                var enumerator = new MMDeviceEnumerator();
                var endPoints = enumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active);
                foreach (var endPoint in endPoints)
                {
                    if (SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER >= 0 && idx == SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER)
                    {
                        Regex rex = new Regex(DefaultSettings.GUID_EXPRESSION);
                        deviceID = rex.Match(endPoint.ID).Groups[1].ToString().ToLower();
                        break;
                    }
                    idx++;
                }

                if (!String.IsNullOrWhiteSpace(deviceID))
                {
                    foreach (Filter filter in filters.AudioInputDevices)
                    {
                        Regex rex = new Regex(@"wave:\s*" + DefaultSettings.GUID_EXPRESSION);
                        string guid = rex.Match(filter.MonikerString).Groups[1].ToString().ToLower();
                        if (deviceID.Equals(guid))
                        {
                            device = filter;
                            break;
                        }
                    }
                }
            }
            return device;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View.Utility.Recorder
{
    public static class RecorderConstants
    {

        public const int MAX_VOICE_RECORD_TIME = 30;//Seconds
        public const int MAX_VIDEO_RECORD_TIME = 10;//Seconds

        public const int TYPE_FRIEND_VOICE_RECORD = 1;
        public const int TYPE_GROUP_VOICE_RECORD = 2;
        public const int TYPE_ROOM_VOICE_RECORD = 3;
        public const int TYPE_FRIEND_VIDEO_RECORD = 4;
        public const int TYPE_GROUP_VIDEO_RECORD = 5;
        public const int TYPE_ROOM_VIDEO_RECORD = 6;

        public const int STATE_READY = 1;
        public const int STATE_RUNNING = 2;
        public const int STATE_PAUSED = 3;
        public const int STATE_COMPLETED = 4;
        public const int STATE_PLAY = 5;
        public const int STATE_SENT = 6;

        public const int BUTTON_START = 1;
        public const int BUTTON_STOP = 2;
        public const int BUTTON_PAUSE = 3;
        public const int BUTTON_RESUME = 4;
        public const int BUTTON_RESTART = 5;
        public const int BUTTON_SEND = 6;
        public const int BUTTON_PLAY = 7;

    }
}

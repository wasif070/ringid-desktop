﻿using System.Runtime.InteropServices;
using System.Text;

namespace View.Utility
{

    public class PcGeoLocation
    {
        private const int GEOCLASS_NATION = 16;

        //SYSGEOTYPE
        private const int GEO_NATION = 1;
        private const int GEO_LATITUDE = 2;
        private const int GEO_LONGITUDE = 3;
        private const int GEO_ISO2 = 4;
        private const int GEO_ISO3 = 5;
        private const int GEO_RFC1766 = 6;
        private const int GEO_LCID = 7;
        private const int GEO_FRIENDLYNAME = 8;
        private const int GEO_OFFICIALNAME = 9;
        private const int GEO_TIMEZONES = 10;
        private const int GEO_OFFICIALLANGUAGES = 11;


        [DllImport("kernel32.dll")]
        static extern int GetUserGeoID(int geoId);
        [DllImport("kernel32.dll")]
        static extern int GetGeoInfo(int geoid, int GeoType, StringBuilder lpGeoData, int cchData, int langid);
        [DllImport("kernel32.dll")]
        static extern int GetUserDefaultLCID();

        public string GetReg()
        {
            int geoId = GetUserGeoID(GEOCLASS_NATION);
            return GetGeoFriendlyName(geoId);
        }

        private string GetGeoFriendlyName(int geoId)
        {
            int lcid = GetUserDefaultLCID();
            StringBuilder bldr = new StringBuilder(50);
            GetGeoInfo(geoId, GEO_FRIENDLYNAME, bldr, bldr.Capacity, lcid);
            return bldr.ToString();
        }
    }
}

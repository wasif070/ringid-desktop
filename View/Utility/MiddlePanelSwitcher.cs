﻿using View.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using View.Constants;
using View.UI.Feed;

namespace View.Utility
{
    class MiddlePanelSwitcher
    {
        public static UCMiddlePanelSwitcher pageSwitcher;

        public static void Switch(int type)
        {
            pageSwitcher.Navigate(type);
        }

        public static void Switch(int type, object state)
        {
            pageSwitcher.Navigate(type, state);
        }

        public static void AddUserControl(UserControl control)
        {
            if (control != null)
                pageSwitcher.Content = control;
        }
    }
}

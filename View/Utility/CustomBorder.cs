﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace View.Utility
{
    public class CustomBorder : Border
    {

        public static readonly DependencyProperty CustomTag_1Property = DependencyProperty.Register("CustomTag_1", typeof(object), typeof(CustomBorder), new PropertyMetadata(null));

        public object CustomTag_1
        {
            get { return (object)GetValue(CustomTag_1Property); }
            set { SetValue(CustomTag_1Property, value); }
        }

        public static readonly DependencyProperty CustomTag_2Property = DependencyProperty.Register("CustomTag_2", typeof(object), typeof(CustomBorder), new PropertyMetadata(null));

        public object CustomTag_2
        {
            get { return (object)GetValue(CustomTag_2Property); }
            set { SetValue(CustomTag_2Property, value); }
        }

        public static readonly DependencyProperty CustomTag_3Property = DependencyProperty.Register("CustomTag_3", typeof(object), typeof(CustomBorder), new PropertyMetadata(null));

        public object CustomTag_3
        {
            get { return (object)GetValue(CustomTag_3Property); }
            set { SetValue(CustomTag_3Property, value); }
        }

        public static readonly DependencyProperty CustomTag_4Property = DependencyProperty.Register("CustomTag_4", typeof(object), typeof(CustomBorder), new PropertyMetadata(null));

        public object CustomTag_4
        {
            get { return (object)GetValue(CustomTag_4Property); }
            set { SetValue(CustomTag_4Property, value); }
        }

        public static readonly DependencyProperty CustomTag_5Property = DependencyProperty.Register("CustomTag_5", typeof(object), typeof(CustomBorder), new PropertyMetadata(null));

        public object CustomTag_5
        {
            get { return (object)GetValue(CustomTag_5Property); }
            set { SetValue(CustomTag_5Property, value); }
        }
        
        public event PropertyChangedCallback IsOpenedChanged;
        public static readonly DependencyProperty IsOpenedProperty = DependencyProperty.Register("IsOpened", typeof(bool), typeof(CustomBorder), new PropertyMetadata(false, (s, e) => 
        {
            CustomBorder cb = (CustomBorder)s;
            if (cb.IsOpenedChanged != null)
            {
                cb.IsOpenedChanged(s, e);
            }
        }));

        public bool IsOpened
        {
            get { return (bool)GetValue(IsOpenedProperty); }
            set { SetValue(IsOpenedProperty, value); }
        }

    }

}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using View.BindingModels;
using View.ViewModel;

namespace View.Utility
{
    public class ActivityRichTextView : RichTextBox
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ActivityRichTextView).Name);
        private Paragraph paragraph;

        public ActivityRichTextView()
        {
            this.IsReadOnly = true;
            this.Margin = new Thickness(-5, 0, -5, 0);
            this.BorderThickness = new Thickness(0, 0, 0, 0);
            this.Background = Brushes.Transparent;
            this.UndoLimit = 0;
            this.IsUndoEnabled = false;
        }

        public ActivityModel Data
        {
            get
            {
                return (ActivityModel)GetValue(DataProperty);
            }
            set
            {
                SetValue(DataProperty, value);
            }
        }

        public static readonly DependencyProperty DataProperty = DependencyProperty.Register("Data", typeof(ActivityModel), typeof(ActivityRichTextView), new PropertyMetadata(null, DataPropertyChanged));

        private static void DataPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            try
            {
                var rtb = (ActivityRichTextView)dependencyObject;
                if (dependencyPropertyChangedEventArgs.NewValue != null && dependencyPropertyChangedEventArgs.NewValue is ActivityModel)
                {
                    ActivityModel model = (ActivityModel)dependencyPropertyChangedEventArgs.NewValue;
                    rtb.paragraph = new Paragraph();
                    rtb.paragraph.TextAlignment = TextAlignment.Center;
                    rtb.paragraph.LineHeight = 13;

                    if (model.ActivityByMemberModel == null)
                    {
                        model.ActivityByMemberModel = new ActivityMemberModel();
                        model.ActivityByMemberModel.UserTableID = model.ActivityBy;
                        GetMemberInfo(model.GroupID, model.ActivityByMemberModel, model.ActivityBy, true);
                    }
                    rtb.paragraph.Inlines.Add(GetHyperlink(model.ActivityByMemberModel));

                    if (model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_ADDED_INTO_GROUP)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" added ", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_REMOVED_FROM_GROUP || model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_REMOVE_FROM_ADMIN)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" removed ", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_MADE_ADMIN || model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_MADE_OWNER)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" made ", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_LEFT_CONVERSATION)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" left this conversation", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_CREATE)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" created this group", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_NAME_CHANGE)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" named the group ", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_PROFILE_IMAGE_CHANGE)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" changed the group photo", FontWeights.SemiBold));
                    }

                    if (model.MemberList != null && model.MemberList.Count > 0)
                    {
                        int i = 0;
                        foreach (ActivityMemberModel memberModel in model.MemberList)
                        {
                            if (String.IsNullOrWhiteSpace(memberModel.FullName))
                            {
                                GetMemberInfo(model.GroupID, memberModel, model.ActivityBy);
                            }

                            rtb.paragraph.Inlines.Add(GetHyperlink(memberModel));
                            if (i < model.MemberList.Count - 1)
                            {
                                rtb.paragraph.Inlines.Add(GetRun(", ", FontWeights.SemiBold));
                            }
                            i++;
                        }
                    }

                    if (model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_REMOVED_FROM_GROUP)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" from the group", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_MADE_ADMIN)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" Admin", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_REMOVE_FROM_ADMIN)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" from Admin", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_MEMBER_MADE_OWNER)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(" Owner", FontWeights.SemiBold));
                    }
                    else if (model.MessageType == ActivityConstants.MSG_GROUP_NAME_CHANGE)
                    {
                        rtb.paragraph.Inlines.Add(GetRun(model.GroupName, FontWeights.SemiBold));
                    }

                    rtb.Document.PagePadding = new Thickness(0);
                    rtb.Document.Blocks.Clear();
                    rtb.Document.Blocks.Add(rtb.paragraph);
                    double width = GetTotalWidth(rtb);
                    rtb.Width = width > rtb.MaxWidth ? rtb.MaxWidth : width;
                }
                else
                {
                    foreach (Inline ic in rtb.paragraph.Inlines)
                    {
                        if (ic is Hyperlink)
                        {
                            Hyperlink hyperlink = ((Hyperlink)ic);
                            hyperlink.MouseLeftButtonDown -= HyperLink_Click;
                            hyperlink.MouseEnter -= HyperLink_MouseEnter;
                            hyperlink.MouseLeave -= HyperLink_MouseLeave;
                        }
                    }
                    rtb.paragraph.Inlines.Clear();
                    rtb.paragraph = null;
                    rtb.Document.Blocks.Clear();
                    rtb.Width = 0;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: DataPropertyChanged()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static Hyperlink GetHyperlink(ActivityMemberModel match)
        {
            Hyperlink hyperlink = new Hyperlink
            {
                NavigateUri = new Uri("http://" + match.UserTableID.ToString() + ".tmp"),
                Tag = match.UserTableID,
                Cursor = Cursors.Hand,
                Foreground = Brushes.White,
                FontWeight = FontWeights.SemiBold,
                TextDecorations = null,
                FontSize = 13
            };
            hyperlink.MouseLeftButtonDown += HyperLink_Click;
            hyperlink.MouseEnter += HyperLink_MouseEnter;
            hyperlink.MouseLeave += HyperLink_MouseLeave;
            hyperlink.Inlines.Add(match.FullName);
            return hyperlink;
        }

        private static void HyperLink_Click(object o, MouseButtonEventArgs e)
        {
            Hyperlink hyperlink = ((Hyperlink)o);
            long uTid = (long)hyperlink.Tag;
            hyperlink.TextDecorations = null;

            if (uTid != DefaultSettings.LOGIN_TABLE_ID)
            {
                RingIDViewModel.Instance.OnFriendProfileButtonClicked(uTid);
            }
            else
            {
                RingIDViewModel.Instance.OnMyProfileClicked(null);
            }
        }

        private static void HyperLink_MouseEnter(object o, MouseEventArgs e)
        {
            Hyperlink hyperlink = ((Hyperlink)o);
            hyperlink.TextDecorations = TextDecorations.Underline;
        }

        private static void HyperLink_MouseLeave(object o, MouseEventArgs e)
        {
            Hyperlink hyperlink = ((Hyperlink)o);
            hyperlink.TextDecorations = null;
        }

        public static Run GetRun(string text, FontWeight weight)
        {
            Run run = new Run
            {
                Text = text,
                Foreground = Brushes.White,
                FontWeight = weight,
                FontSize = 13
            };
            return run;
        }

        public static void GetMemberInfo(long groupID, ActivityMemberModel member, long by, bool subject = false)
        {
            try
            {
                if (member.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
                {
                    UserBasicInfoDTO temp = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(member.UserTableID);
                    if (temp != null)
                    {
                        member.FullName = temp.FullName;
                        member.RingID = temp.RingID;
                    }
                    else
                    {
                        GroupInfoModel groupModel = RingIDViewModel.Instance.GroupList.TryGetValue(groupID);
                        GroupMemberInfoModel memberModel = groupModel != null ? groupModel.MemberInfoDictionary.TryGetValue(member.UserTableID) : null;
                        if (memberModel != null)
                        {
                            member.FullName = memberModel.FullName;
                            member.RingID = memberModel.RingID;
                        }
                    }
                    member.FullName = String.IsNullOrWhiteSpace(member.FullName) ? member.RingID.ToString() : member.FullName.Trim();
                }
                else
                {
                    member.FullName = subject == true ? "You" : member.UserTableID == by ? "yourself" : "you";
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: GetMemberInfo()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static double GetTotalWidth(ActivityRichTextView rtb)
        {
            List<double> widthList = new List<double>();
            try
            {
                double width = 0.0;

                string value = String.Empty;
                BlockCollection bc = rtb.Document.Blocks;
                int count = bc.Count;

                foreach (Paragraph b in bc)
                {
                    foreach (Inline ic in b.Inlines)
                    {
                        if (ic is Run)
                        {
                            Run run = ((Run)ic);
                            value = new TextRange(run.ContentStart, run.ContentEnd).Text;
                            width += GetTextWith(value, run);
                        }
                        else if (ic is Hyperlink)
                        {
                            Hyperlink hyperlink = ((Hyperlink)ic);
                            value = new TextRange(hyperlink.ContentStart, hyperlink.ContentEnd).Text;
                            width += GetTextWith(value, hyperlink);
                        }
                        else if (ic is LineBreak)
                        {
                            value += "\r\n";
                            widthList.Add(width);
                            width = 0.0;
                        }
                    }

                    widthList.Add(width);
                    width = 0.0;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: GetTotalWidth()." + ex.Message + "\n" + ex.StackTrace);
            }
            return widthList.Max();

        }

        private static double GetTextWith(string text, Inline rtb)
        {
            FormattedText formattedText = new FormattedText(text,
                                                CultureInfo.CurrentUICulture,
                                                FlowDirection.LeftToRight,
                                                new Typeface(rtb.FontFamily, rtb.FontStyle, rtb.FontWeight, rtb.FontStretch),
                                                rtb.FontSize + 1.5,
                                                Brushes.Black);
            return formattedText.WidthIncludingTrailingWhitespace;
        }

    }

}

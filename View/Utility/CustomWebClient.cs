﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace View.Utility
{
    public class CustomWebClient : WebClient
    {
        public CustomWebClient(Uri address)
        {
            var objWebRequest = base.GetWebRequest(address);
            ((HttpWebRequest)objWebRequest).AllowWriteStreamBuffering = false;
            objWebRequest.Timeout = 600000;
        }
        //protected override WebRequest GetWebRequest(Uri address)
        //{
        //    var objWebRequest = base.GetWebRequest(address);
        //    ((HttpWebRequest)objWebRequest).AllowWriteStreamBuffering = false;
        //    objWebRequest.Timeout = 600000;
        //    return objWebRequest;
        //}
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using View.BindingModels;
using View.Constants;
using View.ViewModel;

namespace View.Utility
{
    public class RichTextFeedView : RichTextBox
    {
        //for normal,shared,all kinds of Feed View
        private static readonly ILog log = LogManager.GetLogger(typeof(RichTextFeedView).Name);

        public RichTextFeedView()
        {
            this.IsReadOnly = true;
            this.Margin = new Thickness(0, 0, 0, 0);
            this.Padding = new Thickness(0, 0, 0, 0);
            this.BorderThickness = new Thickness(0, 0, 0, 0);
            this.Background = Brushes.Transparent;
            this.FontSize = 12.5;
            this.UndoLimit = 0;
        }

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(RichTextFeedView), new PropertyMetadata(default(string)));

        public void TextHandle(string feedStatus, ObservableCollection<TaggedUserModel> feedStatusTags = null)
        {
            try
            {
                if (string.IsNullOrEmpty(feedStatus) && (feedStatusTags == null || feedStatusTags.Count == 0)) return;

                if (feedStatusTags != null && feedStatusTags.Count > 0)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.Append(feedStatus);
                    List<TaggedUserModel> tmpList = feedStatusTags.OrderByDescending(i => i.Index).ToList();
                    foreach (TaggedUserModel tagModel in tmpList)
                    {
                        builder.Insert(tagModel.Index, "{" + tagModel.UserTableID + "}");
                    }
                    feedStatus = builder.ToString();
                }

                var text = feedStatus;
                int textPosition = 0;
                var paragraph = new Paragraph();
                var matches = Regex.Matches(text, DefaultSettings.EMOTION_REGULAR_EXPRESSION + DefaultSettings.TAG_EXPRESSION + "|" + DefaultSettings.HYPER_LINK_EXPRESSION);

                foreach (Match match in matches)
                {
                    string value = match.Value;
                    int urlOccurrenceIndex = text.IndexOf(value, textPosition, StringComparison.Ordinal);
                    if (urlOccurrenceIndex >= 0)
                    {
                        paragraph.Inlines.Add(text.Substring(textPosition, urlOccurrenceIndex - textPosition));
                        textPosition += urlOccurrenceIndex - textPosition;
                    }
                    if (Regex.IsMatch(value, DefaultSettings.HYPER_LINK_EXPRESSION))
                    {
                        Hyperlink hyperlink = GetHyperlink(match);
                        paragraph.Inlines.Add(hyperlink);
                    }
                    else if (DefaultDictionaries.Instance.EMOTICON_DICTIONARY.ContainsKey(value))
                    {
                        Image image = GetImage(value);
                        paragraph.Inlines.Add(new InlineUIContainer { Child = image, BaselineAlignment = BaselineAlignment.Center });
                    }
                    else if (Regex.IsMatch(value, DefaultSettings.TAG_EXPRESSION) && feedStatusTags != null && feedStatusTags.Count > 0)
                    {
                        //long utId = Convert.ToInt64(value.Substring(1, value.Length - 2));
                        StringBuilder inputTxt = new StringBuilder(value);
                        StringBuilder firstRnTxt = new StringBuilder();
                        StringBuilder TagRnTxt = new StringBuilder();
                        StringBuilder LastRnTxt = new StringBuilder();
                        int idx = 0;
                        for (; idx < value.Length; idx++)
                        {
                            char ch = value[idx];
                            if (ch != '{') firstRnTxt.Append(ch);
                            else break;
                        }
                        idx++;
                        for (; idx < value.Length; idx++)
                        {
                            char ch = value[idx];
                            if (ch != '}') TagRnTxt.Append(ch);
                            else break;
                        }
                        idx++;
                        for (; idx < value.Length; idx++) LastRnTxt.Append(value[idx]);
                        string st = firstRnTxt.ToString();
                        paragraph.Inlines.Add(new Run { Text = st });
                        st = TagRnTxt.ToString();
                        long utId = Convert.ToInt64(st);
                        //
                        TaggedUserModel tgModel = feedStatusTags.Where(P => P.UserTableID == utId).FirstOrDefault();
                        if (tgModel != null)
                        {
                            Hyperlink hyperlink = new Hyperlink();
                            hyperlink.Tag = tgModel.UserTableID;
                            hyperlink.Cursor = Cursors.Hand;
                            hyperlink.MouseLeftButtonDown += Tag_MouseLeftButtonDown;
                            hyperlink.MouseEnter += link_hover;
                            hyperlink.MouseLeave += link_leave;
                            hyperlink.TextDecorations = null;
                            hyperlink.Inlines.Clear();
                            hyperlink.Inlines.Add(new Run { Text = tgModel.FullName, FontWeight = FontWeights.Bold });
                            paragraph.Inlines.Add(hyperlink);
                        }
                    }
                    textPosition += value.Length;
                }

                if (matches.Count == 0)
                {
                    Run run = new Run { Text = text };
                    paragraph.Inlines.Add(run);
                }
                else if (textPosition <= text.Length - 1)
                {
                    Run run = new Run { Text = text };
                    paragraph.Inlines.Add(text.Substring(textPosition, text.Length - textPosition));
                }
                this.Document.PagePadding = new Thickness(0);
                this.Document.Blocks.Clear();
                this.Document.Blocks.Add(paragraph);
            }
            catch (Exception ex)
            {
                log.Error("Error: TextHandle()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        void Tag_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Hyperlink hyperlink = (Hyperlink)sender;
            //long uId = (long)hyperlink.Tag;
            //if (uId == DefaultSettings.LOGIN_USER_ID)
            //    RingIDViewModel.Instance.OnMyProfileClicked(uId);
            //else
            //    RingIDViewModel.Instance.OnFriendProfileButtonClicked(uId);
            if (MainSwitcher.PopupController.ucEditedHistoryView != null && MainSwitcher.PopupController.ucEditedHistoryView.IsVisible)
            {
                MainSwitcher.PopupController.ucEditedHistoryView.Hide();
                MainSwitcher.PopupController.ucEditedHistoryView.HideEditHistoryView();
            }
            long utId = (long)hyperlink.Tag;
            if (utId == DefaultSettings.LOGIN_TABLE_ID) RingIDViewModel.Instance.OnMyProfileClicked(utId);
            else RingIDViewModel.Instance.OnFriendProfileButtonClickedByUTID(utId);
        }

        public static Hyperlink GetHyperlink(Match match)
        {
            string u = match.Value.StartsWith("www.") ? ("http://" + match.Value) : match.Value;
            Hyperlink hyperlink = new Hyperlink
            {
                NavigateUri = new Uri(u),
                TargetName = "_blank",
                Cursor = Cursors.Hand,
                TextDecorations = null,
                Foreground = Brushes.SkyBlue
            };
            // hyperlink.RequestNavigate += new RequestNavigateEventHandler(link_RequestNavigate);
            hyperlink.MouseLeftButtonDown += new MouseButtonEventHandler(link_click);
            hyperlink.MouseEnter += new MouseEventHandler(link_hover);
            hyperlink.MouseLeave += new MouseEventHandler(link_leave);
            hyperlink.Inlines.Add(match.Value);
            return hyperlink;
        }

        private static void link_leave(object sender, MouseEventArgs e)
        {
            Hyperlink link = (Hyperlink)sender;
            link.TextDecorations = null;
        }
        private static void link_hover(object sender, MouseEventArgs e)
        {
            Hyperlink link = (Hyperlink)sender;
            link.TextDecorations = TextDecorations.Underline;
        }

        private static void link_click(object sender, RoutedEventArgs e)
        {
            try
            {
                Hyperlink link = (Hyperlink)sender;
                System.Diagnostics.Process.Start(link.NavigateUri.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error: link_click()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static Image GetImage(string value)
        {
            Image image = new Image();
            image.ToolTip = value;

            System.Windows.Media.Imaging.BitmapImage emoticonImage = ImageObjects.EMOTICON_ICON[value];
            if (emoticonImage == null)
            {
                EmoticonDTO entry = DefaultDictionaries.Instance.EMOTICON_DICTIONARY[value];
                if (entry != null)
                {
                    emoticonImage = ImageUtility.GetBitmapImageOfDynamicResource(RingIDSettings.EMOTICON_FOLDER + Path.DirectorySeparatorChar + "dmid" + Path.DirectorySeparatorChar + entry.Url);
                    ImageObjects.EMOTICON_ICON[entry.Symbol] = emoticonImage;
                }
            }
            image.Source = emoticonImage;
            image.Height = 25;
            image.Width = 25;
            image.Margin = new Thickness(0, 0, 0, 0);//new Thickness(0, 0, 0, -3);
            return image;
        }
    }
}

﻿using log4net;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using View.Constants;

namespace View.Utility
{
    public class RichTextCommentEdit : RichTextBox
    {
        //for Image,Media,Feed,Sharedfeed all Comments Edit & new Comment,Caption new status image upload
        private static readonly ILog log = LogManager.GetLogger(typeof(RichTextCommentEdit).Name);
        private DispatcherTimer _Timer;
        public delegate void RichTextChangedHandler(string text);
        public event RichTextChangedHandler RichTextChanged;
        public event DelegateBool OnGotFocusOnThis;
        public bool HasEmoticon = false;
        public int _CurrLength = 0;
        private bool IsMoveToEnd = false;

        public RichTextCommentEdit()
        {
            this.Margin = new Thickness(0, 0, 0, 0);
            this.BorderThickness = new Thickness(0, 0, 0, 0);
            this.Padding = new Thickness(0, 0, 0, 0);
            this.FontSize = 12.5;
            this.PreviewTextInput += RichTextCommentEdit_PreviewTextInput;
            DataObject.AddPastingHandler(this, new DataObjectPastingEventHandler(OnPaste));
            this.GotFocus += RichTextCommentEdit_GotFocus;
            this.LostFocus += RichTextCommentEdit_LostFocus;
        }
        void RichTextCommentEdit_LostFocus(object sender, RoutedEventArgs e)
        {
            if (OnGotFocusOnThis != null) OnGotFocusOnThis(false);
            // View.UI.Feed.UCAllFeeds.Instance.scrollEnabled = true;
        }
        void RichTextCommentEdit_GotFocus(object sender, RoutedEventArgs e)
        {
            if (OnGotFocusOnThis != null) OnGotFocusOnThis(true);
            //View.UI.Feed.UCAllFeeds.Instance.scrollEnabled = false;
        }

        private void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            try
            {
                String lPastingText = e.DataObject.GetData(DataFormats.UnicodeText) as String;
                if (!string.IsNullOrEmpty(lPastingText))
                {
                    int tempCurrentLength = _CurrLength > 0 ? _CurrLength - 2 : 0;
                    if (Selection.IsEmpty && tempCurrentLength >= MaxLength)
                    {
                        e.CancelCommand();
                        return;
                    }

                    if (!Selection.IsEmpty)
                    {
                        TextRange selectedRange = new TextRange(Selection.Start, Selection.End);
                        tempCurrentLength = tempCurrentLength - selectedRange.Text.Length;
                        if (lPastingText.Length + tempCurrentLength > MaxLength && MaxLength > tempCurrentLength)
                        {
                            lPastingText = lPastingText.Substring(0, MaxLength - tempCurrentLength);
                        }
                        selectedRange.Text = lPastingText;
                    }
                    else if (lPastingText.Length + tempCurrentLength > MaxLength && MaxLength > tempCurrentLength)
                    {
                        String pText = lPastingText.Substring(0, MaxLength - tempCurrentLength);
                        CaretPosition.InsertTextInRun(pText);
                    }
                    else
                    {
                        AppendText(lPastingText);
                    }

                    e.CancelCommand();
                }
                else
                {
                    e.CancelCommand();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPaste()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void RichTextCommentEdit_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            int tempCurrentLength = _CurrLength - 2;
            if (tempCurrentLength >= MaxLength)
            {
                e.Handled = true;
            }
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            foreach (TextChange tc in e.Changes)
            {
                _CurrLength += tc.AddedLength;
                _CurrLength -= tc.RemovedLength;
            }

            if (_Timer == null)
            {
                _Timer = new DispatcherTimer(DispatcherPriority.Background);
                _Timer.Interval = TimeSpan.FromSeconds(0.1);
                _Timer.Tick += LookUp;
            }
            _Timer.Stop();
            _Timer.Start();
            if (RichTextChanged != null)
            {
                RichTextChanged(Text);
            }

            base.OnTextChanged(e);
        }

        private void LookUp(object sender, EventArgs e)
        {
            Dispatcher.Invoke(UpdateEmoticons);
            _Timer.Stop();
        }

        private void UpdateEmoticons()
        {
            FlowDocument doc = Document;
            for (int blockIndex = 0; blockIndex < doc.Blocks.Count; blockIndex++)
            {
                Block b = doc.Blocks.ElementAt(blockIndex);
                Paragraph p = b as Paragraph;
                if (p != null)
                {
                    ProcessInlines(p.Inlines);
                }
            }

            if (IsMoveToEnd)
            {
                CaretPosition = CaretPosition.DocumentEnd;
                ScrollToEnd();
                Focus();
            }
            IsMoveToEnd = false;
        }

        public new void AppendText(string textData)
        {
            try
            {
                string careForwordText = (new TextRange(CaretPosition, CaretPosition.DocumentEnd)).Text.TrimEnd();
                CaretPosition.InsertTextInRun(textData);

                if (careForwordText.Length <= 0)
                {
                    IsMoveToEnd = true;
                }
                else
                {
                    Focus();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AppendText()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private static int FindFirstEmoticon(string text, int startIndex, out string emoticonText, out BitmapImage emoticonImage)
        {
            emoticonText = string.Empty;
            emoticonImage = null;
            int minIndex = -1;
            try
            {
                if (ImageObjects.EMOTICON_ICON != null && ImageObjects.EMOTICON_ICON.Count > 0)
                    foreach (KeyValuePair<String, BitmapImage> e in ImageObjects.EMOTICON_ICON.ToList())
                    {
                        int index = text.IndexOf(e.Key, startIndex);
                        if (index >= 0)
                        {
                            if (minIndex < 0 || index < minIndex)
                            {
                                minIndex = index;
                                emoticonText = e.Key;
                                emoticonImage = e.Value;
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                log.Error("Error: FindFirstEmoticon()." + ex.Message + "\n" + ex.StackTrace);
            }
            return minIndex;
        }

        private void ProcessInlines(InlineCollection inlines)
        {
            try
            {
                for (int inlineIndex = 0; inlineIndex < inlines.Count; inlineIndex++)
                {
                    Inline i = inlines.ElementAt(inlineIndex);
                    if (i is Run)
                    {
                        Run r = i as Run;
                        string text = r.Text;

                        BitmapImage emoticonImage = null;
                        string emoticonText = string.Empty;
                        int index = FindFirstEmoticon(text, 0, out emoticonText, out emoticonImage);

                        if (!String.IsNullOrWhiteSpace(emoticonText) && emoticonImage == null)
                        {
                            EmoticonDTO entry = DefaultDictionaries.Instance.EMOTICON_DICTIONARY[emoticonText];
                            if (entry != null)
                            {
                                emoticonImage = ImageUtility.GetBitmapImageOfDynamicResource(RingIDSettings.EMOTICON_FOLDER + Path.DirectorySeparatorChar + "dmid" + Path.DirectorySeparatorChar + entry.Url);
                                ImageObjects.EMOTICON_ICON[entry.Symbol] = emoticonImage;
                            }
                        }

                        if (index >= 0 && emoticonImage != null)
                        {
                            TextPointer start = i.ContentStart;
                            bool reposition = false;
                            while (!start.GetTextInRun(LogicalDirection.Forward).StartsWith(emoticonText))
                                start = start.GetNextInsertionPosition(LogicalDirection.Forward);
                            TextPointer end = start;
                            for (int j = 0; j < emoticonText.Length; j++)
                                end = end.GetNextInsertionPosition(LogicalDirection.Forward);
                            TextRange tr = new TextRange(start, end);

                            reposition = CaretPosition.CompareTo(tr.End) == 0;
                            tr.Text = string.Empty;

                            Image image = new Image();
                            image.ToolTip = emoticonText;
                            image.Width = 25;
                            image.Height = 25;
                            image.Source = emoticonImage;

                            InlineUIContainer iui = new InlineUIContainer(image, start);
                            iui.BaselineAlignment = BaselineAlignment.Center;

                            if (reposition)
                            {
                                CaretPosition = start.GetNextInsertionPosition(LogicalDirection.Forward);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ProcessInlines()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public string Text
        {
            get
            {
                HasEmoticon = false;
                string value = String.Empty;
                BlockCollection bc = this.Document.Blocks;
                int count = bc.Count;

                foreach (Paragraph b in bc)
                {
                    foreach (Inline ic in b.Inlines)
                    {
                        if (ic is Run)
                        {
                            Run run = ((Run)ic);
                            value += new TextRange(run.ContentStart, run.ContentEnd).Text;
                        }
                        else if (ic is InlineUIContainer)
                        {
                            UIElement emo = ((InlineUIContainer)ic).Child;
                            if (emo != null)
                            {
                                Image image = ((Image)emo);
                                value += image.ToolTip;
                                HasEmoticon = true;
                            }
                        }
                        else if (ic is Hyperlink)
                        {
                            Hyperlink hyperlink = ((Hyperlink)ic);
                            value += new TextRange(hyperlink.ContentStart, hyperlink.ContentEnd).Text;
                        }
                        else if (ic is LineBreak)
                        {
                            value += "\r\n";
                        }
                    }

                    if (--count > 0)
                    {
                        value += "\r\n";
                    }
                }

                return value;
            }
        }

        public static readonly DependencyProperty MaxLengthProperty = DependencyProperty.Register("MaxLength", typeof(int), typeof(RichTextCommentEdit), new PropertyMetadata(8000, (o, e) => { }));

        public int MaxLength
        {
            get { return (int)GetValue(MaxLengthProperty); }
            set { SetValue(MaxLengthProperty, value); }
        }
    }
}

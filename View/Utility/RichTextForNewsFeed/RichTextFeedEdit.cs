﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using View.BindingModels;
using View.Constants;
using View.UI.Feed;
using View.ViewModel.NewStatus;

namespace View.Utility
{
    //for normal,shared,all kinds of Feed Edit,New Status
    public class RichTextFeedEdit : RichTextBox
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RichTextFeedEdit).Name);

        private DispatcherTimer _Timer;
        public delegate void RichTextChangedHandler(string text);
        public event RichTextChangedHandler RichTextChanged;
        public bool HasEmoticon = false;
        public int _CurrLength = 0;
        private bool IsMoveToEnd = false;
        public bool isHypLinkPasted = false;
        public int AlphaStarts = -1; //AlphaStarts>=0 @ found & its friendCount, AlphaStarts=-1 @not found yet or no suggestions
        private BackgroundWorker bgworker;

        private bool addingFrdTag = false;
        private bool removingFrdTag = false;
        private Inline inline;
        private Hyperlink hl;
        private Run run;
        private TextPointer point;
        public List<long> _AddedFriendsUtid;

        public string StringWithoutTags;
        public JArray TagsJArray;


        public RichTextFeedEdit()
        {
            this.Margin = new Thickness(0, 0, 0, 0);
            this.BorderThickness = new Thickness(0, 0, 0, 0);
            this.Padding = new Thickness(0, 0, 0, 0);
            this.FontSize = 12.5;
            this.PreviewTextInput += rtb_PreviewTextInput;
            this._AddedFriendsUtid = new List<long>();
            DataObject.AddPastingHandler(this, new DataObjectPastingEventHandler(OnPaste));
        }

        private void rtb_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            int tempCurrentLength = _CurrLength - 2;
            if (tempCurrentLength >= MaxLength)
            {
                e.Handled = true;
            }
        }


        private void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            try
            {
                String lPastingText = e.DataObject.GetData(DataFormats.UnicodeText) as String;

                if (!String.IsNullOrEmpty(lPastingText))
                {
                    int tempCurrentLength = _CurrLength > 0 ? _CurrLength - 2 : 0;
                    if (Selection.IsEmpty && tempCurrentLength >= MaxLength)
                    {
                        e.CancelCommand();
                        return;
                    }
                    if (Regex.IsMatch(lPastingText, DefaultSettings.HYPER_LINK_EXPRESSION))
                    {
                        //this.AppendText(" ");
                        isHypLinkPasted = true;
                    }
                    if (!Selection.IsEmpty)
                    {
                        TextRange selectedRange = new TextRange(Selection.Start, Selection.End);
                        tempCurrentLength = tempCurrentLength - selectedRange.Text.Length;
                        if (lPastingText.Length + tempCurrentLength > MaxLength && MaxLength > tempCurrentLength)
                        {
                            lPastingText = lPastingText.Substring(0, MaxLength - tempCurrentLength);
                        }
                        selectedRange.Text = lPastingText;
                    }
                    else if (lPastingText.Length + tempCurrentLength > MaxLength && MaxLength > tempCurrentLength)
                    {
                        String pText = lPastingText.Substring(0, MaxLength - tempCurrentLength);
                        CaretPosition.InsertTextInRun(pText);
                    }
                    else
                    {
                        CaretPosition.InsertTextInRun(lPastingText);
                    }
                    e.CancelCommand();

                }
                else if (this.Tag is NewStatusViewModel && e.DataObject.GetData(DataFormats.Bitmap) is BitmapSource)
                {

                    BitmapSource bitmapSource = e.DataObject.GetData(DataFormats.Bitmap) as BitmapSource;

                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    MemoryStream memoryStream = new MemoryStream();
                    BitmapImage bitmapImage = new BitmapImage();

                    encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                    encoder.Save(memoryStream);

                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = new MemoryStream(memoryStream.ToArray());
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();

                    memoryStream.Close();


                    ImageUploaderModel model = new ImageUploaderModel();
                    model.CopiedBitmapImage = bitmapImage;
                    model.FilePath = FeedUtils.CurrentTimeMillis().ToString();
                    model.IsFromUpload = true;

                    NewStatusViewModel newStatusViewModel = (NewStatusViewModel)this.Tag;

                    newStatusViewModel.NewStatusImageUpload.Add(model);
                    newStatusViewModel.UploadingText = "Selected Image(s): " + newStatusViewModel.NewStatusImageUpload.Count;

                    //model.FilePath = filename;
                    //NewStatusImageUpload.Add(model);

                    newStatusViewModel.StatusImagePanelVisibility = Visibility.Visible;
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;

                    bitmapImage.Freeze();
                    bitmapImage = null;
                    e.CancelCommand();
                }
                else
                {
                    e.CancelCommand();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnPaste()." + ex.Message + "\n" + ex.StackTrace);
            }
        }


        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            foreach (TextChange tc in e.Changes)
            {
                _CurrLength += tc.AddedLength;
                _CurrLength -= tc.RemovedLength;

                if (tc.RemovedLength > 0 && !addingFrdTag && !removingFrdTag)
                {
                    inline = null;
                    inline = CaretPosition.Parent as Inline;
                    if (inline != null)
                    {
                        hl = null;
                        hl = inline.ElementStart.Parent as Hyperlink;
                        if (hl != null && hl.NavigateUri == null)
                        {
                            removingFrdTag = true;
                            Selection.Select(hl.ElementStart, hl.ElementEnd);
                            Selection.Text = "";
                            _AddedFriendsUtid.Remove((long)hl.Tag);
                            removingFrdTag = false;
                        }
                        else
                        {
                            run = null;
                            run = CaretPosition.Parent as Run;
                            if (run != null && !string.IsNullOrEmpty(run.Text) && !run.Text.Equals(" "))
                            {
                                point = null;
                                point = CaretPosition.GetNextInsertionPosition(LogicalDirection.Backward);
                                if (point != null)
                                {
                                    inline = null;
                                    inline = point.Parent as Inline;
                                    if (inline != null)
                                    {
                                        hl = null;
                                        hl = inline.ElementStart.Parent as Hyperlink;
                                        if (hl != null && hl.NavigateUri == null)
                                        {
                                            removingFrdTag = true;
                                            Selection.Select(hl.ElementStart, hl.ElementEnd);
                                            Selection.Text = "";
                                            CaretPosition.DeleteTextInRun(1);
                                            _AddedFriendsUtid.Remove((long)hl.Tag);
                                            removingFrdTag = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Paragraph paragraph = CaretPosition.Parent as Paragraph;
                        if (paragraph != null)
                        {
                            TextPointer point = CaretPosition.GetNextInsertionPosition(LogicalDirection.Backward);
                            if (point != null)
                            {
                                inline = point.Parent as Inline;
                                if (inline != null)
                                {
                                    Hyperlink hl = inline.ElementStart.Parent as Hyperlink;
                                    if (hl != null && hl.NavigateUri == null)
                                    {
                                        removingFrdTag = true;
                                        Selection.Select(hl.ElementStart, hl.ElementEnd);
                                        Selection.Text = "";
                                        CaretPosition.DeleteTextInRun(1);
                                        _AddedFriendsUtid.Remove((long)hl.Tag);
                                        removingFrdTag = false;
                                    }
                                }
                            }
                        }

                    }

                }
            }

            if (_Timer == null)
            {
                _Timer = new DispatcherTimer(DispatcherPriority.Background);
                _Timer.Interval = TimeSpan.FromSeconds(0.1);
                _Timer.Tick += LookUp;
            }
            _Timer.Stop();
            _Timer.Start();

            if (Text.Length == 0)
            {
                if (this.Tag is NewStatusViewModel)
                {
                    NewStatusViewModel newStatusViewModel = (NewStatusViewModel)this.Tag;
                    newStatusViewModel.ActionRemoveLinkPreview();
                }
            }
            else if (Text.Length > 0 && (isHypLinkPasted || string.IsNullOrWhiteSpace(Text.ElementAt(Text.Length - 1).ToString())) && this.Tag is NewStatusViewModel)
            {
                isHypLinkPasted = false;
                NewStatusViewModel newStatusViewModel = (NewStatusViewModel)this.Tag;
                if (string.IsNullOrEmpty(newStatusViewModel.PreviewUrl))
                {
                    Regex linkParser = new Regex(DefaultSettings.HYPER_LINK_EXPRESSION);
                    MatchCollection mc = linkParser.Matches(this.Text);
                    if (mc.Count > 0)
                    {
                        string parsedUrl = mc[mc.Count - 1].Value;
                        newStatusViewModel.InputUrl = parsedUrl;
                        if (!parsedUrl.StartsWith("htt")) parsedUrl = "http://" + parsedUrl;
                        if (bgworker == null)
                        {
                            bgworker = new BackgroundWorker();
                            bgworker.WorkerReportsProgress = true;
                            bgworker.WorkerSupportsCancellation = true;
                            bgworker.DoWork += (s, eb) =>
                            {
                                string urltoWork = (string)eb.Argument;
                                newStatusViewModel.SetPostLoaderandBtn(false);
                                LinkInformationDTO obj = HelperMethods.FetchDynamicObject(urltoWork);
                                newStatusViewModel.SetPostLoaderandBtn(true);
                                if (obj != null)
                                    eb.Result = obj;
                                else
                                    eb.Result = null;
                            };
                            bgworker.RunWorkerCompleted += (s, eb) =>
                            {
                                if (!eb.Cancelled && eb.Error == null && eb.Result is LinkInformationDTO)
                                {
                                    LinkInformationDTO obj = (LinkInformationDTO)eb.Result;
                                    if (!string.IsNullOrEmpty(obj.Domain))
                                    {
                                        newStatusViewModel.PreviewDomain = obj.Domain;
                                    }
                                    if (!string.IsNullOrEmpty(obj.Description))
                                    {
                                        newStatusViewModel.PreviewDesc = obj.Description;
                                    }
                                    if (!string.IsNullOrEmpty(obj.Title))
                                    {
                                        newStatusViewModel.PreviewTitle = obj.Title;
                                    }
                                    else if (!string.IsNullOrEmpty(obj.Url))
                                    {
                                        newStatusViewModel.PreviewTitle = obj.Url;
                                    }
                                    if (!string.IsNullOrEmpty(obj.Url))
                                    {
                                        newStatusViewModel.PreviewUrl = obj.Url;
                                    }
                                    if (obj.ImageUrls != null && obj.ImageUrls.Count > 0)
                                    {
                                        newStatusViewModel.AllImagesURLS = obj.ImageUrls;
                                        newStatusViewModel.PreviewImgUrl = newStatusViewModel.AllImagesURLS.ElementAt(0);
                                        if (obj.ImageUrls.Count > 1) newStatusViewModel.PrevNextVisibility = Visibility.Visible;
                                    }
                                    if (obj.MediaType > 0) newStatusViewModel.PreviewLnkType = obj.MediaType;
                                }
                            };
                        }
                        if (bgworker.IsBusy != true)
                        {
                            bgworker.RunWorkerAsync(parsedUrl);
                        }
                    }
                }
            }

            if (RichTextChanged != null)
            {
                RichTextChanged(Text);
            }

            base.OnTextChanged(e);
        }



        private void LookUp(object sender, EventArgs e)
        {
            Dispatcher.Invoke(UpdateEmoticons);
            _Timer.Stop();
        }

        private void UpdateEmoticons()
        {
            FlowDocument doc = Document;
            for (int blockIndex = 0; blockIndex < doc.Blocks.Count; blockIndex++)
            {
                Block b = doc.Blocks.ElementAt(blockIndex);
                Paragraph p = b as Paragraph;
                if (p != null)
                {
                    ProcessInlines(p.Inlines);
                }
            }

            if (IsMoveToEnd)
            {
                CaretPosition = CaretPosition.DocumentEnd;
                ScrollToEnd();
                Focus();
            }
            IsMoveToEnd = false;
        }

        public void SetStatusandTags()
        {
            StringBuilder str = new StringBuilder();
            int currentIndex = 0;
            TagsJArray = null;
            StringWithoutTags = "";

            BlockCollection bc = this.Document.Blocks;
            int count = bc.Count;

            foreach (Paragraph b in bc)
            {
                foreach (Inline ic in b.Inlines)
                {
                    if (ic is Run)
                    {
                        Run run = ((Run)ic);
                        string text = new TextRange(run.ContentStart, run.ContentEnd).Text;
                        str.Append(text);
                        currentIndex += text.Length;
                    }
                    else if (ic is Hyperlink && ic.Tag == null)
                    {
                        Hyperlink hyperlink = ((Hyperlink)ic);
                        string text = new TextRange(hyperlink.ContentStart, hyperlink.ContentEnd).Text;
                        str.Append(text);
                        currentIndex += text.Length;
                    }
                    else if (ic is InlineUIContainer)
                    {
                        UIElement emo = ((InlineUIContainer)ic).Child;
                        if (emo != null)
                        {
                            Image image = ((Image)emo);
                            string emoText = (String)image.ToolTip;
                            str.Append(emoText);
                            currentIndex += emoText.Length;
                        }
                    }
                    else if (ic is LineBreak)
                    {
                        string text = "\r\n";
                        str.Append(text);
                        currentIndex += text.Length;
                    }
                    else if (ic is Hyperlink && ic.Tag is long)
                    {
                        Hyperlink hyperlink = ((Hyperlink)ic);
                        if (TagsJArray == null) TagsJArray = new JArray();
                        JObject obj = new JObject();
                        obj[JsonKeys.Position] = currentIndex;
                        obj[JsonKeys.UserTableID] = (long)hyperlink.Tag;
                        obj[JsonKeys.FullName] = new TextRange(hyperlink.ContentStart, hyperlink.ContentEnd).Text;
                        TagsJArray.Add(obj);
                    }

                }
            }
            StringWithoutTags = str.ToString();
        }

        //public void SetStatusandTags()
        //{
        //    StringBuilder str = new StringBuilder();
        //    int len = 0, currentIndex = 0;
        //    StatusTagsJArray = null;
        //    StatusWithoutTags = "";
        //    Run r;
        //    Inline prevInline = null;
        //    TextPointer pointer = CaretPosition.DocumentStart;
        //    while (pointer != null && pointer != CaretPosition.DocumentEnd)
        //    {
        //        Inline inline = pointer.Parent as Inline;
        //        if (inline != null && inline != prevInline)
        //        {
        //            Hyperlink hl = inline.ElementStart.Parent as Hyperlink;
        //            if (hl != null && hl.NavigateUri == null && hl.Tag != null && hl.Tag is long)
        //            {
        //                if (StatusTagsJArray == null) StatusTagsJArray = new JArray();
        //                JObject obj = new JObject();
        //                obj[JsonKeys.Position] = currentIndex;
        //                obj[JsonKeys.UserTableID] = (long)hl.Tag;
        //                Run rn = (Run)hl.Inlines.ElementAt(0);
        //                obj[JsonKeys.FullName] = rn.Text;
        //                StatusTagsJArray.Add(obj);
        //            }
        //            else if (inline is Run)
        //            {
        //                r = (Run)inline;
        //                len = r.Text.Length;
        //                str.Append(r.Text);
        //                currentIndex += len;
        //            }
        //            prevInline = inline;
        //        }
        //        pointer = pointer.GetNextInsertionPosition(LogicalDirection.Forward);
        //    }
        //    StatusWithoutTags = str.ToString();
        //}
        public void ReplaceText(string replaceTxt, string textData, long utId)
        {
            addingFrdTag = true;
            try
            {
                string careForwordText = (new TextRange(CaretPosition, CaretPosition.DocumentEnd)).Text.TrimEnd();

                TextPointer start = CaretPosition;
                int length = replaceTxt.Length;
                while (length > 0)
                {
                    start = start.GetNextInsertionPosition(LogicalDirection.Backward);
                    length--;
                }

                Hyperlink hyperlink = new Hyperlink(start, CaretPosition);
                hyperlink.Tag = utId;
                hyperlink.TextDecorations = null;
                hyperlink.Inlines.Clear();
                hyperlink.Inlines.Add(new Run { Text = textData, FontWeight = FontWeights.Bold });
                _AddedFriendsUtid.Add(utId);

                CaretPosition.InsertTextInRun(" ");

                if (careForwordText.Length <= 0)
                {
                    IsMoveToEnd = true;
                }
                else
                {
                    Focus();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ReplcaeText()." + ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
            addingFrdTag = false;
        }


        public new void AppendText(string textData)
        {
            try
            {
                string careForwordText = (new TextRange(CaretPosition, CaretPosition.DocumentEnd)).Text.TrimEnd();
                CaretPosition.InsertTextInRun(textData);

                if (careForwordText.Length <= 0)
                {
                    IsMoveToEnd = true;
                }
                else
                {
                    Focus();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AppendText()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void TagsHandle(ObservableCollection<TaggedUserModel> StatusTags)
        {
            List<TaggedUserModel> tempList = StatusTags.OrderByDescending(P => P.Index).ToList();
            foreach (TaggedUserModel tagModel in tempList)
            {
                TextPointer ss = this.Document.ContentStart.GetPositionAtOffset(tagModel.Index + 2);
                Hyperlink hyperlink = new Hyperlink(ss, ss);
                hyperlink.Tag = tagModel.UserTableID;
                hyperlink.TextDecorations = null;
                hyperlink.Inlines.Clear();
                hyperlink.Inlines.Add(new Run { Text = tagModel.FullName, FontWeight = FontWeights.Bold });
            }
        }

        private static int FindFirstEmoticon(string text, int startIndex, out string emoticonText, out BitmapImage emoticonImage)
        {
            emoticonText = string.Empty;
            emoticonImage = null;
            int minIndex = -1;
            try
            {
                foreach (KeyValuePair<String, BitmapImage> e in ImageObjects.EMOTICON_ICON.ToList())
                {
                    int index = text.IndexOf(e.Key, startIndex);
                    if (index >= 0)
                    {
                        if (minIndex < 0 || index < minIndex)
                        {
                            minIndex = index;
                            emoticonText = e.Key;
                            emoticonImage = e.Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: FindFirstEmoticon()." + ex.Message + "\n" + ex.StackTrace);
            }
            return minIndex;
        }

        private void ProcessInlines(InlineCollection inlines)
        {
            try
            {
                for (int inlineIndex = 0; inlineIndex < inlines.Count; inlineIndex++)
                {
                    Inline i = inlines.ElementAt(inlineIndex);
                    if (i is Run)
                    {
                        Run r = i as Run;
                        string text = r.Text;

                        BitmapImage emoticonImage = null;
                        string emoticonText = string.Empty;
                        int index = FindFirstEmoticon(text, 0, out emoticonText, out emoticonImage);

                        if (!String.IsNullOrWhiteSpace(emoticonText) && emoticonImage == null)
                        {
                            EmoticonDTO entry = DefaultDictionaries.Instance.EMOTICON_DICTIONARY[emoticonText];
                            if (entry != null)
                            {
                                emoticonImage = ImageUtility.GetBitmapImageOfDynamicResource(RingIDSettings.EMOTICON_FOLDER + Path.DirectorySeparatorChar + "dmid" + Path.DirectorySeparatorChar + entry.Url);
                                ImageObjects.EMOTICON_ICON[entry.Symbol] = emoticonImage;
                            }
                        }

                        if (index >= 0 && emoticonImage != null)
                        {

                            TextPointer start = i.ContentStart;
                            bool reposition = false;
                            while (!start.GetTextInRun(LogicalDirection.Forward).StartsWith(emoticonText))
                                start = start.GetNextInsertionPosition(LogicalDirection.Forward);
                            TextPointer end = start;
                            for (int j = 0; j < emoticonText.Length; j++)
                                end = end.GetNextInsertionPosition(LogicalDirection.Forward);
                            TextRange tr = new TextRange(start, end);

                            reposition = CaretPosition.CompareTo(tr.End) == 0;
                            tr.Text = string.Empty;

                            Image image = new Image();
                            image.ToolTip = emoticonText;
                            image.Width = 25;
                            image.Height = 25;
                            image.Source = emoticonImage;

                            InlineUIContainer iui = new InlineUIContainer(image, start);
                            iui.BaselineAlignment = BaselineAlignment.Center;

                            if (reposition)
                            {
                                CaretPosition = start.GetNextInsertionPosition(LogicalDirection.Forward);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ProcessInlines()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public string Text
        {
            get
            {
                HasEmoticon = false;
                string value = String.Empty;
                BlockCollection bc = this.Document.Blocks;
                int count = bc.Count;

                foreach (Paragraph b in bc)
                {
                    foreach (Inline ic in b.Inlines)
                    {
                        if (ic is Run)
                        {
                            Run run = ((Run)ic);
                            value += new TextRange(run.ContentStart, run.ContentEnd).Text;
                        }
                        else if (ic is InlineUIContainer)
                        {
                            UIElement emo = ((InlineUIContainer)ic).Child;
                            if (emo != null)
                            {
                                Image image = ((Image)emo);
                                value += image.ToolTip;
                                HasEmoticon = true;
                            }
                        }
                        else if (ic is Hyperlink)
                        {
                            Hyperlink hyperlink = ((Hyperlink)ic);
                            value += new TextRange(hyperlink.ContentStart, hyperlink.ContentEnd).Text;
                        }
                        else if (ic is LineBreak)
                        {
                            value += "\r\n";
                        }
                    }

                    if (--count > 0)
                    {
                        value += "\r\n";
                    }
                }

                return value;
            }
        }

        public static readonly DependencyProperty MaxLengthProperty = DependencyProperty.Register("MaxLength", typeof(int), typeof(RichTextFeedEdit), new PropertyMetadata(8000, (o, e) => { }));

        public int MaxLength
        {
            get { return (int)GetValue(MaxLengthProperty); }
            set
            {
                SetValue(MaxLengthProperty, value);
            }
        }

    }



}

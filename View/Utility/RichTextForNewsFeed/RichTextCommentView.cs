﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using View.Constants;

namespace View.Utility
{
    public class RichTextCommentView : RichTextBox
    {
        //for Image,Media,Feed all Comments View, Caption View BasicimageView
        private static readonly ILog log = LogManager.GetLogger(typeof(RichTextCommentView).Name);

        public RichTextCommentView()
        {
            this.IsReadOnly = true;
            this.Margin = new Thickness(-5, 0, 0, 0);
            this.BorderThickness = new Thickness(0, 0, 0, 0);
            this.Background = Brushes.Transparent;
            this.FontSize = 12.5;
            this.UndoLimit = 0;
        }

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(RichTextCommentView), new PropertyMetadata(default(string), TextPropertyChanged));

        private static void TextPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            try
            {
                var rtb = (RichTextCommentView)dependencyObject;
                var text = (string)dependencyPropertyChangedEventArgs.NewValue;
                if (text == null) return;

                int textPosition = 0;
                var paragraph = new Paragraph();
                var matches = Regex.Matches(text, DefaultSettings.EMOTION_REGULAR_EXPRESSION + DefaultSettings.HYPER_LINK_EXPRESSION);

                foreach (Match match in matches)
                {
                    string value = match.Value;
                    int urlOccurrenceIndex = text.IndexOf(value, textPosition, StringComparison.Ordinal);
                    if (urlOccurrenceIndex >= 0)
                    {
                        paragraph.Inlines.Add(text.Substring(textPosition, urlOccurrenceIndex - textPosition));
                        textPosition += urlOccurrenceIndex - textPosition;
                    }

                    if (Regex.IsMatch(value, DefaultSettings.HYPER_LINK_EXPRESSION))
                    {
                        Hyperlink hyperlink = GetHyperlink(match);
                        paragraph.Inlines.Add(hyperlink);
                    }
                    else if (DefaultDictionaries.Instance.EMOTICON_DICTIONARY.ContainsKey(value))
                    {
                        Image image = GetImage(value);
                        paragraph.Inlines.Add(new InlineUIContainer { Child = image, BaselineAlignment = BaselineAlignment.Center });
                    }
                    textPosition += value.Length;
                }

                if (matches.Count == 0)
                {
                    Run run = new Run { Text = text };
                    paragraph.Inlines.Add(run);
                }
                else if (textPosition <= text.Length - 1)
                {
                    Run run = new Run { Text = text };
                    paragraph.Inlines.Add(text.Substring(textPosition, text.Length - textPosition));
                }
                rtb.Document.PagePadding = new Thickness(0);
                rtb.Document.Blocks.Clear();
                rtb.Document.Blocks.Add(paragraph);
            }
            catch (Exception ex)
            {
                log.Error("Error: TextPropertyChanged()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static Hyperlink GetHyperlink(Match match)
        {
            string u = match.Value.StartsWith("www.") ? ("http://" + match.Value) : match.Value;
            Hyperlink hyperlink = new Hyperlink
            {
                NavigateUri = new Uri(u),
                TargetName = "_blank",
                Cursor = Cursors.Hand,
                TextDecorations = null,
                Foreground = Brushes.SkyBlue
            };
            // hyperlink.RequestNavigate += new RequestNavigateEventHandler(link_RequestNavigate);
            hyperlink.MouseLeftButtonDown += new MouseButtonEventHandler(link_click);
            hyperlink.MouseEnter += new MouseEventHandler(link_hover);
            hyperlink.MouseLeave += new MouseEventHandler(link_leave);
            hyperlink.Inlines.Add(match.Value);
            return hyperlink;
        }

        private static void link_leave(object sender, MouseEventArgs e)
        {
            Hyperlink link = (Hyperlink)sender;
            link.TextDecorations = null;
        }

        private static void link_hover(object sender, MouseEventArgs e)
        {
            Hyperlink link = (Hyperlink)sender;
            link.TextDecorations = TextDecorations.Underline;
        }

        private static void link_click(object sender, RoutedEventArgs e)
        {
            try
            {
                Hyperlink link = (Hyperlink)sender;
                System.Diagnostics.Process.Start(link.NavigateUri.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error: link_click()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static Image GetImage(string value)
        {
            Image image = new Image();
            image.ToolTip = value;

            System.Windows.Media.Imaging.BitmapImage emoticonImage = ImageObjects.EMOTICON_ICON[value];
            if (emoticonImage == null)
            {
                if (emoticonImage == null)
                {
                    EmoticonDTO entry = DefaultDictionaries.Instance.EMOTICON_DICTIONARY[value];
                    if (entry != null)
                    {
                        emoticonImage = ImageUtility.GetBitmapImageOfDynamicResource(RingIDSettings.EMOTICON_FOLDER + Path.DirectorySeparatorChar + "dmid" + Path.DirectorySeparatorChar + entry.Url);
                        ImageObjects.EMOTICON_ICON[entry.Symbol] = emoticonImage;
                    }
                }
            }
            image.Source = emoticonImage;

            image.Height = 25;
            image.Width = 25;
            image.Margin = new Thickness(0, 0, 0, 0);//new Thickness(0, 0, 0, -3);
            return image;
        }

    }

}

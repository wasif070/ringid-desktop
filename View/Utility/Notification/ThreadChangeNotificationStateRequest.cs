﻿using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;

namespace View.Utility.Notification
{

    public class ThreadChangeNotificationStateRequest
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadChangeNotificationStateRequest).Name);
        private bool running = false;

        public ThreadChangeNotificationStateRequest()
        {

        }

        private void Run()
        {
            try
            {
                running = true;

                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    JObject pakToSend = new JObject();
                    String pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_RESET_NOTIFICATION_COUNTER;
                    pakToSend[JsonKeys.UpdateTime] = AppConstants.NOTIFICATION_MAX_UT;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            NotificationHistoryDAO.Instance.LoadMaxTimeFromNotificationHistory();
                            NotificationHistoryDAO.Instance.LoadMinTimeFromNotificationHistory();
                        }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out pakToSend);
                    }
                    else
                    {
                        if (!MainSwitcher.ThreadManager().PingNow()) { }
                    }

                    //string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    //SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //Thread.Sleep(25);
                    //for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    //{
                    //    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                    //    {
                    //        break;
                    //    }
                    //    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    //    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                    //    {
                    //        if (i % DefaultSettings.SEND_INTERVAL == 0)
                    //            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //    }
                    //    else
                    //    {
                    //        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out pakToSend);
                    //        NotificationHistoryDAO.Instance.LoadMaxTimeFromNotificationHistory();
                    //        NotificationHistoryDAO.Instance.LoadMinTimeFromNotificationHistory();
                    //        break;
                    //    }
                    //    PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
                    //}

                }
                else
                {
                    log.Error("ChangeNotificationResetCounter Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in ChangeNotificationResetCounter ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
            finally
            {
                running = false;
            }
        }

        #region "Public Methods"

        public void StartThread()
        {
            if (!running)
            {
                running = true;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public bool IsRunning()
        {
            return running;
        }
        #endregion "Public methods"
    }
}

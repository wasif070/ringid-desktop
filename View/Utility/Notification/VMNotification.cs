﻿using Auth.Service.Notification;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using View.BindingModels;
using View.UI.Notification;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Feed;

namespace View.Utility.Notification
{
    public class VMNotification : INotifyPropertyChanged
    {
        private static VMNotification _Instance;

        public static VMNotification Instance
        {
            get
            {
                _Instance = _Instance ?? new VMNotification();
                return _Instance;
            }
        }

        private long _NotificationModelCount = 0;

        public long NotificationModelCount
        {
            get { return _NotificationModelCount; }
            set { _NotificationModelCount = value; this.OnPropertyChanged("NotificationModelCount"); }
        }

        private int _NotificationLoaded = 0;

        public int NotificationLoaded
        {
            get { return _NotificationLoaded; }
            set { _NotificationLoaded = value; this.OnPropertyChanged("NotificationLoaded"); }
        }

        private bool _AllRead = false;

        public bool AllRead
        {
            get { return _AllRead; }
            set { _AllRead = value; this.OnPropertyChanged("AllRead"); }
        }

        private string _SeeMoreText = NotificationMessages.NOTIFICATION_SHOW_MORE;

        public string SeeMoreText
        {
            get { return _SeeMoreText; }
            set { _SeeMoreText = value; this.OnPropertyChanged("SeeMoreText"); }
        }

        private bool _IsLoading = false;

        public bool IsLoading
        {
            get { return _IsLoading; }
            set { _IsLoading = value; this.OnPropertyChanged("IsLoading"); }
        }

        private ICommand _NotificationReadUnreadClickCommand;

        public ICommand NotificationReadUnreadClickCommand
        {
            get { _NotificationReadUnreadClickCommand = _NotificationReadUnreadClickCommand ?? new RelayCommand(param => OnNotificationReadUnreadClicked(param)); return _NotificationReadUnreadClickCommand; }
        }

        private ICommand _NotificationDeleteClickCommand;

        public ICommand NotificationDeleteClickCommand
        {
            get
            {
                _NotificationDeleteClickCommand = _NotificationDeleteClickCommand ?? new RelayCommand(param => OnNotificationDeleteClicked(param));
                return _NotificationDeleteClickCommand;
            }
        }

        private ICommand _NotificationClickCommand;

        public ICommand NotificationClickCommand
        {
            get
            {
                _NotificationClickCommand = _NotificationClickCommand ?? new RelayCommand(param => OnNotificationClicked(param));
                return _NotificationClickCommand;
            }
        }

        private ICommand _SeeMoreButtonClickCommand;

        public ICommand SeeMoreButtonClickCommand
        {
            get
            {
                _SeeMoreButtonClickCommand = _SeeMoreButtonClickCommand ?? new RelayCommand(param => OnSeeMoreClicked());
                return _SeeMoreButtonClickCommand;
            }
        }

        private ICommand _NotificationSelectAllCommand;

        public ICommand NotificationSelectAllCommand
        {
            get
            {
                _NotificationSelectAllCommand = _NotificationSelectAllCommand ?? new RelayCommand(param => OnSelectAllButtonClick());
                return _NotificationSelectAllCommand;
            }
        }

        private ICommand _NotificationDeselectAllCommand;

        public ICommand NotificationDeselectAllCommand
        {
            get
            {
                _NotificationDeselectAllCommand = _NotificationDeselectAllCommand ?? new RelayCommand(param => OnDeselectAllButtonClick());
                return _NotificationDeselectAllCommand;
            }
        }

        private ICommand _NotificationCancelButtonCommand;

        public ICommand NotificationCancelButtonCommand
        {
            get
            {
                _NotificationCancelButtonCommand = _NotificationCancelButtonCommand ?? new RelayCommand(param => OnCancelButtonClick());
                return _NotificationCancelButtonCommand;
            }
        }

        private ICommand _NotificationDeleteAllCommand;

        public ICommand NotificationDeleteAllCommand
        {
            get
            {
                _NotificationDeleteAllCommand = _NotificationDeleteAllCommand ?? new RelayCommand(param => OnDeleteAllButtonClick());
                return _NotificationDeleteAllCommand;
            }
        }

        private ICommand _NotificationMarkAllReadCommand;

        public ICommand NotificationMarkAllReadCommand
        {
            get
            {
                _NotificationMarkAllReadCommand = _NotificationMarkAllReadCommand ?? new RelayCommand(param => OnMarkAllReadClick());
                return _NotificationMarkAllReadCommand;
            }
        }

        private ICommand _NotificationEditButtonCommand;

        public ICommand NotificationEditButtonCommand
        {
            get
            {
                _NotificationEditButtonCommand = _NotificationEditButtonCommand ?? new RelayCommand(param => OnEditButtonClick());
                return _NotificationEditButtonCommand;
            }
            //set { _NotificationEditButtonCommand = value; }
        }

        public void LoadData()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    List<NotificationDTO> sortedList = (from l in RingDictionaries.Instance.NOTIFICATION_LISTS
                                                        orderby l.UpdateTime descending
                                                        select l).ToList();

                    lock (RingIDViewModel.Instance.NotificationList)
                    {
                        foreach (NotificationDTO dto in sortedList)
                        {
                            if (!RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == dto.ID))
                            {
                                NotificationModel model = NotificationModel.LoadDataFromDTO(dto); //new NotificationModel(dto);
                                if (!model.NotificationID.Equals(Guid.Empty))
                                {
                                    RingIDViewModel.Instance.NotificationList.Add(model);
                                }
                            }
                        }
                    }

                    AppConstants.NOTIFICATION_MIN_UT = RingIDViewModel.Instance.NotificationList.Count > 0 ? RingIDViewModel.Instance.NotificationList.Min(x => x.UpdateTime) : AppConstants.NOTIFICATION_MIN_UT;

                    NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
                    NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;
                    IsAllRead();
                }
                catch (Exception) { }
            });

        }

        public void IsAllRead()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    AllRead = true;
                    //foreach (var item in Notification.Items)
                    foreach (NotificationModel item in RingIDViewModel.Instance.NotificationList)
                    {
                        if (item.IsRead == false)//&& item.ShowPanelSeeMore != true
                        {
                            AllRead = false;
                            break;
                        }
                    }
                }
                catch (Exception) { }
            });
        }

        public void OnSeeMoreClicked()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    //NotificationUtility.Instance.ShowMoreAction(string.Empty);
                    IsLoading = true;

                    if (AppConstants.NOTIFICATION_ALL_FETCHED_FROM_DB == false)
                    {
                        NotificationHistoryDAO.Instance.LoadNotificationFromDB(RingIDViewModel.Instance.NotificationList.Min(x => x.UpdateTime));
                        LoadData();
                        IsLoading = false;
                        NotificationUtility.Instance.ShowMoreAction();
                        if (AppConstants.NOTIFICATION_MIN_UT == NotificationHistoryDAO.Instance.GetMinTimeFromNotificationHistory())
                        {
                            AppConstants.NOTIFICATION_ALL_FETCHED_FROM_DB = true;
                            IsLoading = true;
                            NotificationRequest(AppConstants.NOTIFICATION_MIN_UT, (short)2);
                        }
                    }
                    else
                    {
                        NotificationRequest(AppConstants.NOTIFICATION_MIN_UT, (short)2);
                    }
                    IsAllRead();
                }
                catch (Exception) { }
            });
        }

        public void OnNotificationReadUnreadClicked(object parameter)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (parameter is NotificationModel)
                    {
                        NotificationModel model = (NotificationModel)parameter;
                        NotificationUtility.Instance.ReadUnreadNotification(model);
                        IsAllRead();
                    }
                }
                catch (Exception) { }
            });
        }

        public void OnNotificationDeleteClicked(object parameter)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (parameter is NotificationModel)
                    {
                        NotificationModel model = (NotificationModel)parameter;
                        //MessageBoxResult result = CustomMessageBox.ShowQuestion(NotificationMessages.ASK_DELETE_NOTIFICATION, NotificationMessages.HEADER_DELETE_NOTIFICATION);
                        //if (result == MessageBoxResult.Yes)
                        bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this notification"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
                        if (isTrue)
                            NotificationUtility.Instance.DeleteNotificationFromCommand(model);
                    }
                }
                catch (Exception) { }
            });
        }

        public void OnNotificationClicked(object parameter)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (parameter is NotificationModel)
                    {
                        NotificationModel model = (NotificationModel)parameter;
                        NotificationUtility.Instance.NotificationClickOperation(model);
                    }
                    IsAllRead();
                }
                catch (Exception) { }
            });

        }

        public void OnSelectAllButtonClick()
        {
            AllSelected = true;
            NotificationUtility.Instance.NotificationSelectDeselect();
        }

        public void OnDeselectAllButtonClick()
        {
            AllSelected = false;
            NotificationUtility.Instance.NotificationSelectDeselect(false);
        }

        public void OnCancelButtonClick()
        {
            EditMode = false;
            NotificationUtility.Instance.NotificationSelectDeselect(false);
        }

        public void OnDeleteAllButtonClick()
        {

            try
            {
                bool anyItemSelected = RingIDViewModel.Instance.NotificationList.Any(P => P.IsChecked == true);
                if (anyItemSelected)
                {
                    List<Guid> notificationIds = new List<Guid>();

                    foreach (NotificationModel model in RingIDViewModel.Instance.NotificationList)
                    {
                        if (model.IsChecked)
                        {
                            notificationIds.AddRange(model.DeleteNotificationIDs);
                            if (!notificationIds.Contains(model.NotificationID))
                            { notificationIds.Add(model.NotificationID); }
                        }
                    }
                    string text = notificationIds.Count > 1 ? "these notifications" : "this notification";
                    bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, text), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
                    if (isTrue)
                        Application.Current.Dispatcher.Invoke(() => { NotificationUtility.Instance.DeleteNotifications(notificationIds); });
                }
                else
                {
                    UIHelperMethods.ShowWarning("Select an item to delete", "Delete failed");
                    //  CustomMessageBox.ShowWarning("Select an item to delete");
                }
                EditMode = false;
            }
            catch (Exception) { }

        }

        public void OnMarkAllReadClick()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    NotificationUtility.Instance.MarkAllNotificationsAsRead();
                    AllRead = true;
                }
                catch (Exception) { }
            });
        }

        public void OnEditButtonClick()
        {
            EditMode = true;
        }

        private bool editMode = false;
        public bool EditMode
        {
            get { return editMode; }
            set { editMode = value; this.OnPropertyChanged("EditMode"); }
        }

        private bool allSelected = false;
        public bool AllSelected
        {
            get { return allSelected; }
            set { allSelected = value; this.OnPropertyChanged("AllSelected"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #region "Utility Methods"

        public void NotificationRequest(long max_ut, short scl)
        {
            new ThradNotificationRequest().StartThread(max_ut, scl);
        }

        public void GetLiveDTOForNotification(NotificationDTO nDTO)
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                NotificationDTO existingDTO = NotificationDTO.FindExistingDTO(nDTO);
                if (existingDTO != null)//
                {
                    RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(existingDTO);
                    RingDictionaries.Instance.NOTIFICATION_LISTS.Add(nDTO);
                    RingIDViewModel.Instance.AllNotificationCounter = RingIDViewModel.Instance.AllNotificationCounter > 0 ? RingIDViewModel.Instance.AllNotificationCounter - 1 : 0;
                    RingIDViewModel.Instance.AllNotificationCounter++;
                    new DeleteFromNotificationHistoryTable(existingDTO.ID);
                }
                else//new
                {
                    RingDictionaries.Instance.NOTIFICATION_LISTS.Add(nDTO);
                    RingIDViewModel.Instance.AllNotificationCounter++;
                }
                if (RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == nDTO.ID))
                {
                    NotificationModel model = RingIDViewModel.Instance.NotificationList.Where(x => x.NotificationID == nDTO.ID).First();
                    RingIDViewModel.Instance.NotificationList.Remove(model);
                }
                nDTO.PreviousIds = new List<Guid>();
                NotificationModel insertModel = NotificationModel.LoadDataFromDTO(nDTO);

                if (!insertModel.NotificationID.Equals(Guid.Empty) && !RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == insertModel.NotificationID))
                {
                    RingIDViewModel.Instance.NotificationList.Add(insertModel);
                }

                AppConstants.ALL_NOTIFICATION_COUNT = RingIDViewModel.Instance.AllNotificationCounter;
                new InsertIntoNotificationCountTable();
                NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
                IsLoading = false;
                IsAllRead();
                NotificationUtility.Instance.ShowMoreAction();
                List<NotificationDTO> list = new List<NotificationDTO>();
                list.Add(nDTO);
                new InsertIntoNotificationHistoryTable(list);
                NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;


                if (SettingsConstants.VALUE_RINGID_ALERT_SOUND)
                {
                    View.Utility.audio.AudioFilesAndSettings.Play(View.Utility.audio.AudioFilesAndSettings.NOTIFICATION_RECEIVED);
                }
            });
        }

        #endregion "Utility Methods"
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Notification
{
    public class ThradNotificationRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradNotificationRequest).Name);
        private bool running = false;
        private long max_ut = 0;
        private short scl = 0;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                String packetId = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_NOTIFICATIONS;
                if (max_ut > 0)
                {
                    pakToSend[JsonKeys.Scroll] = scl;
                    pakToSend[JsonKeys.UpdateTime] = max_ut;
                }

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                VMNotification.Instance.IsLoading = false;
                if (feedbackfields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    MainSwitcher.AuthSignalHandler().notificationSignalHandler.AddSeemoreInNotificationList();
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread(long max_ut, short scl)
        {
           if (!running)
            {
                this.max_ut = max_ut;
                this.scl = scl;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

﻿
using System;
using System.Windows;
using System.Windows.Media;
namespace View.Utility
{

    public static class ImageButtonDependency
    {
        public static ImageSource GetDefaultImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(DefaultImageProperty);
        }
        public static void SetDefaultImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(DefaultImageProperty, value);
        }

        public static readonly DependencyProperty DefaultImageProperty =
            DependencyProperty.RegisterAttached(
            "DefaultImage",
            typeof(ImageSource),
            typeof(ImageButtonDependency),
            new UIPropertyMetadata(null));

        public static ImageSource GetHoverImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(HoverImageProperty);
        }
        public static void SetHoverImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(HoverImageProperty, value);
        }

        public static readonly DependencyProperty HoverImageProperty =
            DependencyProperty.RegisterAttached(
            "HoverImage",
            typeof(ImageSource),
            typeof(ImageButtonDependency),
            new UIPropertyMetadata(null));

        public static ImageSource GetDisabledImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(DisabledImageProperty);
        }
        public static void SetDisabledImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(DisabledImageProperty, value);
        }

        public static readonly DependencyProperty DisabledImageProperty =
            DependencyProperty.RegisterAttached(
            "DisabledImage",
            typeof(ImageSource),
            typeof(ImageButtonDependency),
            new UIPropertyMetadata(null));

        public static ImageSource GetPressedImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(PressedImageProperty);
        }
        public static void SetPressedImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(PressedImageProperty, value);
        }

        public static readonly DependencyProperty PressedImageProperty =
            DependencyProperty.RegisterAttached(
            "PressedImage",
            typeof(ImageSource),
            typeof(ImageButtonDependency),
            new UIPropertyMetadata(null));

        public static String GetBtnText(DependencyObject obj)
        {
            return (String)obj.GetValue(BtnTextProperty);
        }

        public static void SetBtnText(DependencyObject obj, String value)
        {
            obj.SetValue(BtnTextProperty, value);
        }

        public static readonly DependencyProperty BtnTextProperty =
         DependencyProperty.RegisterAttached("BtnText", typeof(String), typeof(ImageButtonDependency));
    }
}

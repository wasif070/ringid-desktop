﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio.CoreAudioApi;
using View.Utility.Call;

namespace View.Utility.Naudio
{
    class NotificationClientImplementation : NAudio.CoreAudioApi.Interfaces.IMMNotificationClient
    {

        public void OnDefaultDeviceChanged(DataFlow dataFlow, Role deviceRole, string defaultDeviceId)
        {
            //Console.WriteLine("OnDefaultDeviceChanged");
        }

        public void OnDeviceAdded(string deviceId)
        {
            //Console.WriteLine("OnDeviceAdded");
        }

        public void OnDeviceRemoved(string deviceId)
        {
            //Console.WriteLine("OnDeviceRemoved");
        }

        public void OnDeviceStateChanged(string deviceId, DeviceState newState)
        {
            //Console.WriteLine("OnDeviceStateChanged\n Device Id -->{0} : Device State {1}", deviceId, newState);
            if (newState == DeviceState.NotPresent)
            {
                //if (VoiceVideoCallController.GetVoiceVideoCallController().PCMDataPlayerAndRecorder != null)
                //    VoiceVideoCallController.GetVoiceVideoCallController().PCMDataPlayerAndRecorder.RestartPlayerRecorder();
            }
            else if (newState == DeviceState.Unplugged)
            {

            }
            else if (newState == DeviceState.Active)
            {
                if (MainSwitcher.CallController.PCMDataPlayerAndRecorder != null)
                    MainSwitcher.CallController.PCMDataPlayerAndRecorder.RestartPlayerRecorder();
            }
            //Do some Work
        }

        public NotificationClientImplementation()
        {
            //_realEnumerator.RegisterEndpointNotificationCallback();
            if (System.Environment.OSVersion.Version.Major < 6)
            {
                throw new NotSupportedException("This functionality is only supported on Windows Vista or newer.");
            }
        }

        public void OnPropertyValueChanged(string deviceId, PropertyKey propertyKey)
        {
            Console.WriteLine("OnPropertyValueChanged");
        }

    }
}
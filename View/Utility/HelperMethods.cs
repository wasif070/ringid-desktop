﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Linq;
using Auth.Service.RingMarket;
using Auth.utility;
using HtmlAgilityPack;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Chat;
using View.UI.Group;
using View.UI.PopUp.MediaSendViaChat;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility.Auth;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.Utility.FriendList;
using View.Utility.FriendProfile;
using View.Utility.Myprofile;
using View.Utility.Notification;
using View.Utility.RingMarket;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using System.Globalization;
using View.UI.Feed;
using View.Utility.Channel;
using View.Utility.Stream;
using View.Utility.Feed;
using View.UI.StreamAndChannel;
using Microsoft.Win32;

namespace View.Utility
{
    public static class HelperMethods
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HelperMethods).Name);
        public static readonly object _PROCESS_SYNC_LOCK = new object();

        #region "Private Methods"

        public static void changeUI(UserBasicInfoDTO user)
        {
            FriendListLoadUtility.FavoriteTopNonFavoriteListChange(user);
            //  new RepositionFriendList(user);
        }

        private static void SendCircleListDoingListRequest()
        {
            JObject pak1 = new JObject();
            AuthRequestNoResult(pak1, AppConstants.TYPE_VIEW_DOING_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SaveDeviceUniqueIDintoDB()
        {
            new DatabaseActivityDAO().SaveDeviceUniqueIDintoDB();
        }

        #endregion "Private Methods"

        #region "Public Methods"

        public static string GetGUID()
        {
            try
            {
                if (string.IsNullOrEmpty(DefaultSettings.DEVICE_UNIQUE_ID))
                {
                    DefaultSettings.DEVICE_UNIQUE_ID = Guid.NewGuid().ToString();//iid.ToString(); //Guid.NewGuid().ToString(); //
                    SaveDeviceUniqueIDintoDB();
                }
            }
            catch (Exception e)
            {
                log.Error("Can not generate GUID, returning Mac-Address==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

                string macAddresses = string.Empty;
                foreach (System.Net.NetworkInformation.NetworkInterface nic in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == System.Net.NetworkInformation.OperationalStatus.Up)
                    {
                        macAddresses += nic.GetPhysicalAddress().ToString();
                        break;
                    }
                }
                DefaultSettings.DEVICE_UNIQUE_ID = macAddresses;
                SaveDeviceUniqueIDintoDB();
            }
            return DefaultSettings.DEVICE_UNIQUE_ID;
        }

        private static DateTime utc1582 = new DateTime(1582, 10, 15, 0, 0, 0, DateTimeKind.Utc);
        private static long START_EPOCH = (long)(utc1582 - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        public static long GetUnixTimestamp(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                return -1;
            }

            string[] components = name.Split('-');
            if (components.Length != 5)
            {
                return -1;
            }

            long mostSigBits = Int64.Parse(components[0], NumberStyles.HexNumber);
            mostSigBits <<= 16;
            mostSigBits |= Int64.Parse(components[1], NumberStyles.HexNumber);
            mostSigBits <<= 16;
            mostSigBits |= Int64.Parse(components[2], NumberStyles.HexNumber);

            long timestamp = (mostSigBits & 0x0FFFL) << 48
              | ((mostSigBits >> 16) & 0x0FFFFL) << 32
              | (long)((ulong)mostSigBits >> 32);

            return timestamp;
        }
        public static long GetTime(string name)
        {
            return (GetUnixTimestamp(name) / 10000) + START_EPOCH;
        }
        public static bool IsLessThanMin(Guid minId, Guid currentId)
        {
            if (minId == Guid.Empty) return false;
            if (currentId == Guid.Empty) return true;
            if (currentId == minId) return false;
            long minIdTm = GetTime(minId.ToString());
            long currentIdTm = GetTime(currentId.ToString());
            return (currentIdTm < minIdTm);
        }
        public static Guid GetMaxMinGuidInMediaCollection(ObservableCollection<SingleMediaModel> lst, int startIdx, bool findMax = true)
        {
            if (findMax)
            {
                Guid maxId = lst[startIdx].ContentId;
                long maxIdTm = GetTime(maxId.ToString());
                for (int i = startIdx + 1; i < lst.Count; i++)
                {
                    Guid currentId = lst[i].ContentId;
                    if (currentId != Guid.Empty)
                    {
                        long currentIdTm = GetTime(currentId.ToString());
                        if (currentIdTm > maxIdTm) { maxId = currentId; maxIdTm = currentIdTm; }
                    }
                }
                return maxId;
            }
            else
            {
                Guid minId = lst[startIdx].ContentId;
                long minIdTm = GetTime(minId.ToString());
                for (int i = startIdx + 1; i < lst.Count; i++)
                {
                    Guid currentId = lst[i].ContentId;
                    if (currentId != Guid.Empty)
                    {
                        long currentIdTm = GetTime(currentId.ToString());
                        if (currentIdTm < minIdTm) { minId = currentId; minIdTm = currentIdTm; }
                    }
                }
                return minId;
            }
        }
        public static Guid GetMaxMinGuidInFeedCollection(ObservableCollection<FeedHolderModel> coll, int startIdx, bool findMax = true)
        {
            if (findMax)
            {
                Guid maxId = coll[startIdx].FeedId;
                long maxIdTm = GetTime(maxId.ToString());
                for (int i = startIdx + 1; i < coll.Count; i++)
                {
                    Guid currentId = coll[i].FeedId;
                    if (currentId != Guid.Empty)
                    {
                        long currentIdTm = GetTime(currentId.ToString());
                        if (currentIdTm > maxIdTm) { maxId = currentId; maxIdTm = currentIdTm; }
                    }
                }
                return maxId;
            }
            else
            {
                Guid minId = coll[startIdx].FeedId;
                long minIdTm = GetTime(minId.ToString());
                for (int i = startIdx + 1; i < coll.Count; i++)
                {
                    Guid currentId = coll[i].FeedId;
                    if (currentId != Guid.Empty)
                    {
                        long currentIdTm = GetTime(currentId.ToString());
                        if (currentIdTm < minIdTm) { minId = currentId; minIdTm = currentIdTm; }
                    }
                }
                return minId;
            }
        }
        public static Guid GetMaxMinGuidInAllFeedCollection(bool findMax = true)
        {
            List<Guid> list = NewsFeedViewModel.Instance.CurrentFeedIds;
            if (list.Count == 0) return Guid.Empty;
            int startIdx = 0;
            if (findMax)
            {
                Guid maxId = list[startIdx];
                long maxIdTm = GetTime(maxId.ToString());
                for (int i = startIdx + 1; i < list.Count; i++)
                {
                    Guid currentId = list[i];
                    if (currentId != Guid.Empty)
                    {
                        long currentIdTm = GetTime(currentId.ToString());
                        if (currentIdTm > maxIdTm) { maxId = currentId; maxIdTm = currentIdTm; }
                    }
                }
                return maxId;
            }
            else
            {
                Guid minId = list[startIdx];
                long minIdTm = GetTime(minId.ToString());
                for (int i = startIdx + 1; i < list.Count; i++)
                {
                    Guid currentId = list[i];
                    if (currentId != Guid.Empty)
                    {
                        long currentIdTm = GetTime(currentId.ToString());
                        if (currentIdTm < minIdTm) { minId = currentId; minIdTm = currentIdTm; }
                    }
                }
                return minId;
            }
        }
        public static void InitChatSDK()
        {
            try
            {
                List<string> directoryList = new List<string>();
                directoryList.Add(RingIDSettings.APP_FOLDER);//Settings
                directoryList.Add(RingIDSettings.TEMP_CHAT_FILES_TRANSFER_FOLDER);//temp
                directoryList.Add(RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER);//image
                directoryList.Add(RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER);//audio
                directoryList.Add(RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER);//video
                directoryList.Add(RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER);//secret image
                directoryList.Add(RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER);//secret audio
                directoryList.Add(RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER);//secret video

                ChatService.Initialize(
                    DefaultSettings.LOGIN_TABLE_ID,
                    DefaultSettings.userProfile != null ? DefaultSettings.userProfile.FullName : String.Empty,
                    DefaultSettings.RING_OFFLINE_IM_IP,//"192.168.1.125"
                    DefaultSettings.RING_OFFLINE_IM_PORT,//1200
                    ServerAndPortSettings.AUTH_SERVER_IP,
                    ServerAndPortSettings.COMMUNICATION_PORT,
                    AppConstants.PLATFORM_DESKTOP,
                    DefaultSettings.LOGIN_SESSIONID,
                    int.Parse(AppConfig.VERSION_PC),
                    directoryList,
                    ServerAndPortSettings.ChatSharedFileUploadingUrl,
                    ServerAndPortSettings.ChatSharedFileDownloadingUrl,
                    DefaultSettings.VALUE_LOGIN_USER_PASSWORD);
                log.Error("************* END InitChatSDK ***************");
            }
            catch (Exception ex)
            {
                log.Error("Error: StartVoiceAndChatSockets()." + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public static bool IsNetworkConnectionAvailable()
        {
            try
            {
                // only recognizes changes related to Internet adapters
                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    // however, this will include all adapters
                    NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
                    foreach (NetworkInterface face in interfaces)
                    {
                        // filter so we see only Internet adapters
                        if (face.OperationalStatus == OperationalStatus.Up)
                        {
                            if ((face.NetworkInterfaceType != NetworkInterfaceType.Tunnel) &&
                                (face.NetworkInterfaceType != NetworkInterfaceType.Loopback))
                            {
                                IPv4InterfaceStatistics statistics = face.GetIPv4Statistics();
                                // all testing seems to prove that once an interface comes online
                                // it has already accrued statistics for both received and sent...
                                if ((statistics.BytesReceived > 0) && (statistics.BytesSent > 0)) return true;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return false;
        }
        public static bool IsRingIDOfficialAccount(long utID)
        {
            return (utID == DefaultSettings.RINGID_OFFICIAL_UTID ||
                    (RingIDViewModel.Instance.SpecialFriendList != null
                    && RingIDViewModel.Instance.SpecialFriendList.Any(P => P.ShortInfoModel.UserTableID.Equals(utID))));
        }
        public static DateTime GetNISTTime()
        {
            try
            {
                var client = new System.Net.Sockets.TcpClient("time.nist.gov", 13);
                var streamReader = new StreamReader(client.GetStream());
                var response = streamReader.ReadToEnd();
                streamReader.Close();
                client.Close();
                var utcDateTimeString = response.Substring(7, 17);
                var localDateTime = DateTime.ParseExact(utcDateTimeString, "yy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeUniversal);
                return localDateTime;
            }
            catch (Exception)
            {
            }
            return DateTime.UtcNow;
        }

        public static void GetLatLongByCurrentTimeZone(out float lat, out float lng)
        {
            lat = 0; lng = 0;
            try
            {
                if (DefaultSettings.IsInternetAvailable)
                {
                    TimeZoneInfo.ClearCachedData();
                    TimeZoneInfo zone = TimeZoneInfo.Local;
                    string location = zone.ToString();
                    location = Regex.Replace(location, @" ?\(.*?\)", string.Empty).Trim();
                    string latLong = "https://maps.googleapis.com/maps/api/geocode/xml?address=" + location + "&sensor=false";
                    var request = WebRequest.Create(new Uri(latLong));
                    var response = request.GetResponse();
                    var xdoc = XDocument.Load(response.GetResponseStream());
                    var result = xdoc.Element("GeocodeResponse").Element("result");
                    if (result == null) return;
                    var locationElement = result.Element("geometry").Element("location");
                    if (locationElement == null) return;

                    var Lat = locationElement.Element("lat").Value;
                    if (Lat != null) lat = Convert.ToSingle(Lat);
                    var Long = locationElement.Element("lng").Value;
                    if (Long != null) lng = Convert.ToSingle(Long);

                }
            }
            catch (ArgumentException ex)
            {
                log.Error(ex.Message + ex.StackTrace);
            }
        }

        public static void CreateDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    return;
                }

                DirectoryInfo di = Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                log.Error("Error: Creating  " + path + "  directory." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void WriteAutoStartValuetoFile()
        {
            try
            {
                string path = System.IO.Path.Combine(RingIDSettings.APP_FOLDER, "AutoStartInfo.bat");
                if (File.Exists(path))
                {
                    using (StreamWriter sw = new StreamWriter(path, false))
                    {
                        sw.WriteLine(DefaultSettings.VALUE_LOGIN_AUTO_START + ""); //default autostartvalue
                        sw.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Writing in 'AutoStartInfo.bat' Value - " + DefaultSettings.VALUE_LOGIN_AUTO_START + ". Error - " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static long MediaPlayerDuration(string filename)
        {
            long d = 0;
            MediaPlayer player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };
            try
            {
                player.Open(new Uri(filename));
                //player.Pause();
                System.Threading.Thread.Sleep(300);
                if (player.NaturalDuration.HasTimeSpan) d = (long)player.NaturalDuration.TimeSpan.TotalSeconds;
                player.Close();
            }
            catch (Exception)
            {
                if (player != null) player.Close();
            }
            return d;
        }

        public static void SetVideoArtImage(CommonUploaderModel model)
        {
            MediaPlayer player = null;
            try
            {
                player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };
                player.MediaOpened += new EventHandler(delegate(object s, EventArgs e)
                {
                    uint[] framePixels = new uint[player.NaturalVideoWidth * player.NaturalVideoHeight];
                    uint[] previousFramePixels = new uint[framePixels.Length];
                    player.Pause();
                    if (player.NaturalDuration.HasTimeSpan && player.NaturalDuration.TimeSpan.TotalSeconds > 5) player.Position = TimeSpan.FromSeconds(5);
                    else player.Position = TimeSpan.FromSeconds(1);
                    // Render the current frame into a bitmap
                    var drawingVisual = new DrawingVisual();
                    var renderTargetBitmap = new RenderTargetBitmap(player.NaturalVideoWidth, player.NaturalVideoHeight, 96, 96, PixelFormats.Default);
                    using (var drawingContext = drawingVisual.RenderOpen())
                    {
                        drawingContext.DrawVideo(player, new Rect(0, 0, player.NaturalVideoWidth, player.NaturalVideoHeight));
                    }
                    renderTargetBitmap.Render(drawingVisual);

                    // Copy the pixels to the specified location
                    renderTargetBitmap.CopyPixels(previousFramePixels, player.NaturalVideoWidth * 4, 0);

                    // Return the bitmap
                    var bitmapImage = new BitmapImage();
                    GC.SuppressFinalize(bitmapImage);
                    var bitmapEncoder = new PngBitmapEncoder();
                    bitmapEncoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));

                    using (var stream = new MemoryStream())
                    {
                        bitmapEncoder.Save(stream);
                        stream.Seek(0, SeekOrigin.Begin);
                        bitmapImage.BeginInit();
                        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                        bitmapImage.StreamSource = stream;
                        bitmapImage.EndInit();
                        bitmapImage.Freeze();
                    }

                    model.ThumbnailBmp = bitmapImage;
                    bitmapImage = null;
                    GC.SuppressFinalize(model.ThumbnailBmp);
                    player.Close();
                });
                player.Open(new Uri(model.FilePath));
                System.Threading.Thread.Sleep(500);
            }
            catch (Exception)
            {
                model.ThumbnailBmp = null;
                if (player != null)
                {
                    player.Close();
                    player = null;
                }
            }
        }

        public static void ClearBitMapImagesDictionary()
        {
            try
            {
                foreach (string key in ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.Keys)
                {
                    GC.SuppressFinalize(ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[key]);
                    ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[key] = null;
                    ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.Remove(key);
                }
                //ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.Clear();
                //GC.Collect();
            }
            catch (Exception ex)
            {
                log.Error("Error: ClearBitMapImagesDictionary() ." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static int GetAvailablePortForMemoryServer(int startPort = 2260)
        {
            try
            {
                IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
                IPEndPoint[] tcpEndPoints = properties.GetActiveTcpListeners();
                IPEndPoint[] udpEndPoints = properties.GetActiveUdpListeners();

                List<int> usedTCPPorts = tcpEndPoints.Select(p => p.Port).ToList<int>();
                List<int> usedUDPPorts = udpEndPoints.Select(p => p.Port).ToList<int>();
                int unusedPort = 0;

                unusedPort = Enumerable.Range(startPort, 199).Where(port => !usedTCPPorts.Contains(port) && !usedUDPPorts.Contains(port)).FirstOrDefault();
                return unusedPort;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                return 0;
            }
        }

        public static string GetCountryFromIP()
        {
            string country = null;
            WebResponse objResponse = null;
            WebRequest objRequest = null;
            StreamReader responseStream = null;
            try
            {
                string externalIP = "";
                externalIP = new WebClient().DownloadString("http://icanhazip.com");
                externalIP = externalIP.Trim();
                //
                objRequest = WebRequest.Create("http://ip-api.com/xml/" + externalIP);
                objResponse = objRequest.GetResponse();
                if (objResponse != null)
                {
                    responseStream = new StreamReader(objResponse.GetResponseStream());
                    string responseRead = responseStream.ReadToEnd();
                    if (!string.IsNullOrEmpty(responseRead))
                        using (TextReader sr = new StringReader(responseRead))
                        {
                            using (System.Data.DataSet dataBase = new System.Data.DataSet())
                            {
                                dataBase.ReadXml(sr);
                                country = dataBase.Tables[0].Rows[0][1].ToString();
                            }
                        }
                }
            }
            catch (Exception)
            {
            }
            if (responseStream != null) { responseStream.Close(); responseStream = null; }
            if (objResponse != null) { objResponse.Close(); objResponse = null; }
            objRequest = null;
            return country;
        }

        public static CountryCodeModel GetCountryCodeModelFromCountry()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.DefaultCountry))
                for (int i = 2; i < DefaultSettings.COUNTRY_MOBILE_CODE.Length / 2; i++)
                {
                    if (string.Equals(DefaultSettings.DefaultCountry, DefaultSettings.COUNTRY_MOBILE_CODE[i, 0], StringComparison.OrdinalIgnoreCase))
                    {
                        return new CountryCodeModel(DefaultSettings.COUNTRY_MOBILE_CODE[i, 0], DefaultSettings.COUNTRY_MOBILE_CODE[i, 1]);
                    }
                }
            return new CountryCodeModel("Canada", "+1");
        }

        public static byte[] ReadByteArrayFromFile(string filePath)
        {
            byte[] buffer = null;
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
            }
            return buffer;
        }

        public static string GetURLData(string URL, out int contentType) //1=html,2=image,3.4...others
        {
            contentType = 0;
            HttpWebRequest request = null;
            WebResponse response = null;
            System.IO.Stream stream = null;
            string returnString = string.Empty;
            try
            {
                request = (HttpWebRequest)HttpWebRequest.Create(URL);
                request.UnsafeAuthenticatedConnectionSharing = true;
                request.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
                request.Timeout = 20000;
                response = request.GetResponse();
                if (response != null)
                {
                    string cnt = response.ContentType.ToLower(System.Globalization.CultureInfo.InvariantCulture);
                    if (cnt.StartsWith("text/html"))
                    {
                        contentType = 1;
                        stream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(stream);
                        returnString = reader.ReadToEnd();
                        reader.Close();
                    }
                    else if (cnt.StartsWith("image/")) { contentType = 2; returnString = "image"; }
                    else { contentType = 3; returnString = "others"; }
                }
            }
            catch (WebException ex)
            {
                log.Error("Error: GetURLData() ." + ex.Message + "\n" + ex.StackTrace);
                if (ex.Response != null)
                    using (WebResponse res = ex.Response)
                    {
                        HttpWebResponse httpResponse = (HttpWebResponse)res;
                        using (System.IO.Stream data = res.GetResponseStream())
                        using (var rdr = new StreamReader(data))
                        {
                            returnString = rdr.ReadToEnd();
                        }
                    }
            }
            request = null;
            if (stream != null)
            {
                stream.Close();
                stream = null;
            }
            if (response != null)
            {
                response.Close();
                response = null;
            }
            return returnString;
        }

        public static bool IsValidImageFile(string filePath, int sz)
        {
            try
            {
                MediaInfoDotNet.MediaFile mf = new MediaInfoDotNet.MediaFile(filePath);
                if (sz == 1) return (mf.Image != null && mf.Image.Count > 0 && mf.Image.ElementAt(0).Value.height == 45 && mf.Image.ElementAt(0).Value.width == 45);
                else return (mf.Image != null && mf.Image.Count > 0 && mf.Image.ElementAt(0).Value.height == 15 && mf.Image.ElementAt(0).Value.width == 15);
            }
            catch (Exception) { }
            return false;
        }

        public static LinkInformationDTO FetchDynamicObject(string url)
        {
            LinkInformationDTO obj = new LinkInformationDTO();
            try
            {
                //var webGet = new HtmlWeb();
                //var document = webGet.Load(url);
                int contentType = 0;
                string Data = GetURLData(url, out contentType);
                if (!string.IsNullOrEmpty(Data))
                {
                    if (contentType == 2)//if (Data != null && url.EndsWith(".jpg") || url.EndsWith(".gif") || url.EndsWith(".png"))
                    {
                        obj.ImageUrls = new List<string>();
                        obj.ImageUrls.Add(url);
                    }
                    else if (contentType == 1)
                    {
                        HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                        document.LoadHtml(Data);
                        var metaTags = document.DocumentNode.SelectNodes("//meta"); //metatags==content for property,name,itemprop(images only)--images not ending with gif
                        if (metaTags != null)
                            foreach (var metaNode in metaTags)
                            {
                                if (metaNode.HasAttributes)
                                {
                                    if (metaNode.Attributes["property"] != null && metaNode.Attributes["content"] != null && !string.IsNullOrEmpty(metaNode.Attributes["content"].Value))
                                    {
                                        switch (metaNode.Attributes["property"].Value)
                                        {
                                            case "og:site_name":
                                                try
                                                {
                                                    obj.Domain = HtmlAgilityPack.HtmlEntity.DeEntitize(metaNode.Attributes["content"].Value);
                                                }
                                                catch (Exception)
                                                {
                                                    obj.Domain = metaNode.Attributes["content"].Value;
                                                }
                                                break;
                                            case "og:title":
                                                try
                                                {
                                                    obj.Title = HtmlAgilityPack.HtmlEntity.DeEntitize(metaNode.Attributes["content"].Value);
                                                }
                                                catch (Exception)
                                                {
                                                    obj.Title = metaNode.Attributes["content"].Value;
                                                }
                                                break;
                                            case "og:description":
                                                try
                                                {
                                                    obj.Description = HtmlAgilityPack.HtmlEntity.DeEntitize(metaNode.Attributes["content"].Value);
                                                }
                                                catch (Exception)
                                                {
                                                    obj.Description = metaNode.Attributes["content"].Value;
                                                }
                                                break;
                                            case "og:url":
                                                obj.Url = metaNode.Attributes["content"].Value;
                                                break;
                                            case "og:image":
                                                if (!metaNode.Attributes["content"].Value.EndsWith(".gif"))
                                                {
                                                    if (obj.ImageUrls == null) obj.ImageUrls = new List<string>();
                                                    obj.ImageUrls.Add(metaNode.Attributes["content"].Value);
                                                }
                                                break;
                                            case "og:type":
                                                string con = metaNode.Attributes["content"].Value;
                                                if (con.Contains("video")) obj.MediaType = 2;
                                                else if (con.Contains("sound") || con.Contains("audio") || con.Contains("music")) obj.MediaType = 1;
                                                break;
                                            case "og:music":
                                                obj.MediaType = 1;
                                                break;
                                            case "og:video":
                                                obj.MediaType = 2;
                                                break;
                                        }
                                    }
                                    if (metaNode.Attributes["name"] != null && metaNode.Attributes["content"] != null && !string.IsNullOrEmpty(metaNode.Attributes["content"].Value))
                                    {
                                        switch (metaNode.Attributes["name"].Value)
                                        {
                                            case "description":
                                                try
                                                {
                                                    if (string.IsNullOrEmpty(obj.Description))
                                                        obj.Description = HtmlAgilityPack.HtmlEntity.DeEntitize(metaNode.Attributes["content"].Value);
                                                }
                                                catch (Exception)
                                                {
                                                    obj.Description = metaNode.Attributes["content"].Value;
                                                }
                                                break;
                                            case "image":
                                                if (!metaNode.Attributes["content"].Value.EndsWith(".gif"))
                                                {
                                                    if (obj.ImageUrls == null) obj.ImageUrls = new List<string>();
                                                    obj.ImageUrls.Add(metaNode.Attributes["content"].Value);
                                                }
                                                break;
                                            case "title":
                                                try
                                                {
                                                    if (string.IsNullOrEmpty(obj.Title))
                                                        obj.Title = HtmlAgilityPack.HtmlEntity.DeEntitize(metaNode.Attributes["content"].Value);
                                                }
                                                catch (Exception)
                                                {
                                                    obj.Title = metaNode.Attributes["content"].Value;
                                                }
                                                break;
                                            case "site_name":
                                                try
                                                {
                                                    if (string.IsNullOrEmpty(obj.Domain))
                                                        obj.Domain = HtmlAgilityPack.HtmlEntity.DeEntitize(metaNode.Attributes["content"].Value);
                                                }
                                                catch (Exception)
                                                {
                                                    obj.Domain = metaNode.Attributes["content"].Value;
                                                }
                                                break;
                                        }
                                    }
                                    if (metaNode.Attributes["itemprop"] != null && metaNode.Attributes["itemprop"].Value.Equals("image") && metaNode.Attributes["content"] != null && !string.IsNullOrEmpty(metaNode.Attributes["content"].Value) && !metaNode.Attributes["content"].Value.EndsWith(".gif"))
                                    {
                                        if (obj.ImageUrls == null) obj.ImageUrls = new List<string>();
                                        obj.ImageUrls.Add(metaNode.Attributes["content"].Value);
                                    }
                                }
                            }
                        //img src,link rel=image_src --images not ending with gif 
                        if (obj.ImageUrls == null)
                        {
                            obj.ImageUrls = new List<string>();
                            try
                            {
                                var urls_img_src = document.DocumentNode.Descendants("img")
                                                    .Select(e => e.GetAttributeValue("src", null))
                                                    .Where(s => !string.IsNullOrEmpty(s) && !s.EndsWith(".gif"));
                                if (urls_img_src != null)
                                    foreach (var item in urls_img_src)
                                    {
                                        obj.ImageUrls.Add(item);
                                    }
                            }
                            catch (Exception)
                            {
                            }
                            try
                            {
                                var urls_link_rel = document.DocumentNode.SelectNodes("//link[@rel='image_src']");
                                if (urls_link_rel != null)
                                    foreach (var item in urls_link_rel)
                                    {
                                        if (!string.IsNullOrEmpty(item.Attributes["href"].Value) && !item.Attributes["href"].Value.EndsWith(".gif"))
                                        {
                                            obj.ImageUrls.Add(item.Attributes["href"].Value);
                                        }
                                    }
                            }
                            catch (Exception)
                            {
                            }
                        }
                        //Title,url,domain
                        if (string.IsNullOrEmpty(obj.Title))
                        {
                            try
                            {
                                HtmlNode titleTag = document.DocumentNode.Descendants("title").SingleOrDefault();
                                if (titleTag != null && !string.IsNullOrEmpty(titleTag.InnerHtml))
                                    obj.Title = HtmlAgilityPack.HtmlEntity.DeEntitize(titleTag.InnerHtml);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(obj.Title))
                {
                    obj.Title = obj.Title.Trim();
                }
                //if (string.IsNullOrEmpty(obj.Url))
                obj.Url = url;
                if (string.IsNullOrEmpty(obj.Domain))
                {
                    var uri = new Uri(url);
                    var host = uri.Host;
                    obj.Domain = host;
                }
                //relative images append
                if (obj.ImageUrls != null)
                {
                    int i = 0, len = obj.ImageUrls.Count;
                    for (i = 0; i < len; i++)
                    {
                        //string str = obj.ImageUrls[i];
                        if (!obj.ImageUrls[i].StartsWith("http"))
                        {
                            if (obj.ImageUrls[i].StartsWith("//"))
                            {
                                obj.ImageUrls[i] = "http:" + obj.ImageUrls[i];
                            }
                            else
                            {
                                Uri uri = new Uri(obj.Url);
                                string absoluteUri = uri.AbsoluteUri;
                                if (!absoluteUri.EndsWith("/") && !obj.ImageUrls[i].StartsWith("/")) absoluteUri = absoluteUri + "/";
                                obj.ImageUrls[i] = absoluteUri + obj.ImageUrls[i];
                            }
                        }
                        int cntTp = 0;
                        HelperMethods.GetURLData(obj.ImageUrls[i], out cntTp);
                        if (cntTp != 2)
                        {
                            obj.ImageUrls = null;
                            break;
                        }
                    }
                }
                if (obj != null && !string.IsNullOrEmpty(obj.Description))
                {
                    obj.Description = WebUtility.HtmlDecode(obj.Description);
                }
                return obj;
            }
            catch (Exception ex)
            {
                log.Error("Error: FetchDynamicObject() ." + ex.Message + "\n" + ex.StackTrace);
                if (obj != null && !string.IsNullOrEmpty(obj.Description))
                {
                    obj.Description = WebUtility.HtmlDecode(obj.Description);
                }
                return obj;
            }
        }

        public static bool IsTagDTOsSameasJArray(JArray StatusTagsJArray, ObservableCollection<TaggedUserModel> collection)
        {
            if ((StatusTagsJArray == null && collection == null) || (StatusTagsJArray == null && collection.Count == 0)) return true;
            if ((StatusTagsJArray != null && collection == null) || (StatusTagsJArray == null && collection != null)) return false;
            if (StatusTagsJArray.Count != collection.Count) return false;
            foreach (JObject singleObj in StatusTagsJArray)
            {
                if (singleObj[JsonKeys.Position] == null || singleObj[JsonKeys.UserTableID] == null) return false;
                int idx = (int)singleObj[JsonKeys.Position];
                long utid = (long)singleObj[JsonKeys.UserTableID];
                TaggedUserModel model = collection.Where(P => P.Index == idx).FirstOrDefault();
                if (model == null) return false;
                if (model.UserTableID != utid) return false;
            }
            return true;
        }

        public static JArray GetStatusTagJArrayFromModels(ObservableCollection<TaggedUserModel> list)
        {
            if (list == null || list.Count == 0) return null;
            JArray jarr = new JArray();
            foreach (var item in list)
            {
                JObject obj = new JObject();
                obj[JsonKeys.Position] = item.Index;
                obj[JsonKeys.FullName] = item.FullName;
                obj[JsonKeys.UserTableID] = item.UserTableID;
                jarr.Add(obj);
            }
            return jarr;
        }

        public static void InvokeInsert<TValue>(this ObservableCollection<TValue> collection, int index, TValue item)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    try
                    {
                        lock (collection)
                        {
                            collection.Insert(index, item);
                        }
                    }
                    catch (Exception ex) { log.Error("Error: InvokeInsert<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: InvokeInsert<TValue> ." + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public static void InvokeInsertNoDuplicate<TValue>(this ObservableCollection<TValue> collection, int index, TValue item)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    try
                    {
                        lock (collection)
                        {
                            if (!collection.Contains(item))
                                collection.Insert(index, item);
                        }
                    }
                    catch (Exception ex) { log.Error("Error: InvokeInsertNoDuplicate<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: InvokeInsertNoDuplicate<TValue> ." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void InvokeAdd<TValue>(this ObservableCollection<TValue> collection, TValue item)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                try
                {
                    lock (collection)
                    {
                        collection.Add(item);
                    }
                }
                catch (Exception ex) { log.Error("Error: InvokeAdd<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
            }, DispatcherPriority.Send);
        }

        public static void InvokeAddNoDuplicate<TValue>(this ObservableCollection<TValue> collection, TValue item)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                try
                {
                    lock (collection)
                    {
                        if (!collection.Contains(item))
                            collection.Add(item);
                    }
                }
                catch (Exception ex) { log.Error("Error: InvokeAddNoDuplicate<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
            }, DispatcherPriority.Send);
        }
        public static void InvokeRemoveAt<TValue>(this ObservableCollection<TValue> collection, int index)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                try { collection.RemoveAt(index); }
                catch (Exception ex) { log.Error("Error: InvokeRemoveAt<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
            }, DispatcherPriority.Send);
        }

        public static void InvokeRemove<TValue>(this ObservableCollection<TValue> collection, TValue item)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                try { collection.Remove(item); }
                catch (Exception ex) { log.Error("Error: InvokeRemove<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
            }, DispatcherPriority.Send);
        }

        public static void InvokeMove<TValue>(this ObservableCollection<TValue> collection, int oldIndex, int newIndex)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                try { collection.Move(oldIndex, newIndex); }
                catch (Exception ex) { log.Error("Error: InvokeMove<TValue> ." + ex.Message + "\n" + ex.StackTrace); }
            }, DispatcherPriority.Send);
        }

        public static string GetShortName(string fullName)
        {
            string shortName = "";
            if (!String.IsNullOrWhiteSpace(fullName))
            {
                try
                {
                    string[] split = fullName.Trim().Split(' ');
                    if (split.Length > 0)
                    {
                        shortName += (split[0][0] + "").ToUpper();

                        if (split.Length > 1)
                        {
                            shortName += (split[split.Length - 1][0] + "").ToUpper();
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: GetShortName()." + ex.Message + "\n" + ex.StackTrace);
                }
            }
            return shortName;
        }


        public static string GetStatusIconLocation(int presence, int device, int mood, long userTableId)
        {
            string status_image = ImageLocation.OFFLINE;
            if (presence == (StatusConstants.PRESENCE_ONLINE))
            {
                if (device == 1)
                {
                    status_image = ImageLocation.ONLINE_DESKTOP;
                }
                else if (device == 2)
                {
                    status_image = ImageLocation.ONLINE_ANDROID;
                }
                else if (device == 3)
                {
                    status_image = ImageLocation.ONLINE_IOS;
                }
                else if (device == 4)
                {
                    status_image = ImageLocation.ONLINE_WINDOWS;
                }
                else if (device == 5)
                {
                    status_image = ImageLocation.ONLINE_WEB;
                }
                else
                {
                    if (userTableId == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        status_image = ImageLocation.ONLINE_DESKTOP;
                    }
                    else
                    {
                        status_image = ImageLocation.OFFLINE;
                    }
                }
            }
            else if (presence == StatusConstants.PRESENCE_AWAY)
            {
                status_image = ImageLocation.AWAY;
            }

            if (mood == StatusConstants.MOOD_DONT_DISTURB)
            {
                status_image = ImageLocation.STATUS_DONOT_DISTURB;
            }
            return status_image;
        }

        public static string GetStatusText(int presence, int mood, long lastOnlineTime)
        {
            string text = "Offline";
            if (presence == (StatusConstants.PRESENCE_ONLINE))
            {
                text = "Online";
            }
            else if ((presence == StatusConstants.PRESENCE_OFFLINE || presence == StatusConstants.PRESENCE_AWAY) && lastOnlineTime > 0)
            {
                text = "Seen " + FeedUtils.GetShowableDate(lastOnlineTime, ChatService.GetServerTime());
            }
            else if (presence == StatusConstants.PRESENCE_AWAY)
            {
                text = "Away";
            }
            if (mood == StatusConstants.MOOD_DONT_DISTURB)
            {
                text = "Don't Disturb";
            }
            return text;
        }

        public static string ConvertUrlToPurl(string imageUrl, int imgType)
        {
            String fullImageName = String.Empty;

            if (imageUrl != null && imageUrl.Trim().Length > 0 && imageUrl.Contains("/"))
            {
                int index = imageUrl.LastIndexOf('/');
                StringBuilder bulider = new StringBuilder(imageUrl);

                string temp = String.Empty;
                if (imgType == ImageUtility.IMG_THUMB)
                {
                    temp = "pthumb";
                }
                else if (imgType == ImageUtility.IMG_CROP)
                {
                    temp = "pcrp";
                }
                else if (imgType == ImageUtility.IMG_300)
                {
                    temp = "p300";
                }
                else if (imgType == ImageUtility.IMG_600)
                {
                    temp = "p600";
                }
                else if (imgType == ImageUtility.IMG_FULL)
                {
                    temp = "p";
                }

                bulider.Insert(imageUrl.LastIndexOf('/') + 1, temp);
                fullImageName = bulider.ToString();
            }

            return fullImageName;
        }

        public static string ConvertUrlToName(string imageUrl, int imgType)
        {
            String fullImageName = String.Empty;

            if (imageUrl != null && imageUrl.Trim().Length > 0 && imageUrl.Contains("/"))
            {
                int index = imageUrl.LastIndexOf('/');
                StringBuilder bulider = new StringBuilder(imageUrl);

                string temp = String.Empty;
                if (imgType == ImageUtility.IMG_THUMB)
                {
                    temp = "pthumb";
                }
                else if (imgType == ImageUtility.IMG_CROP)
                {
                    temp = "pcrp";
                }
                else if (imgType == ImageUtility.IMG_300)
                {
                    temp = "p300";
                }
                else if (imgType == ImageUtility.IMG_600)
                {
                    temp = "p600";
                }
                else if (imgType == ImageUtility.IMG_FULL)
                {
                    temp = "p";
                }

                bulider.Insert(imageUrl.LastIndexOf('/') + 1, temp);
                fullImageName = bulider.ToString().Replace('/', '_');
            }

            return fullImageName;

        }
        public static string GetUrlWithSize(string imageUrl, int imgType)
        {
            String fullImageName = String.Empty;

            if (imageUrl != null && imageUrl.Trim().Length > 0 && imageUrl.Contains("/"))
            {
                int index = imageUrl.LastIndexOf('/');
                StringBuilder bulider = new StringBuilder(imageUrl);

                string temp = String.Empty;
                if (imgType == ImageUtility.IMG_THUMB)
                {
                    temp = "pthumb";
                }
                else if (imgType == ImageUtility.IMG_CROP)
                {
                    temp = "pcrp";
                }
                else if (imgType == ImageUtility.IMG_300)
                {
                    temp = "p300";
                }
                else if (imgType == ImageUtility.IMG_600)
                {
                    temp = "p600";
                }
                else if (imgType == ImageUtility.IMG_FULL)
                {
                    temp = "p";
                }

                bulider.Insert(imageUrl.LastIndexOf('/') + 1, temp);
                fullImageName = bulider.ToString();
            }

            return fullImageName;

        }
        public static void FetchStickerList()
        {
            try
            {
                List<MarketStickerCollectionDTO> collectionDTOs = RingMarkerStickerDAO.GetRingMarketStickerCollection();
                if (collectionDTOs != null && collectionDTOs.Count > 0)
                {
                    collectionDTOs.ForEach(i =>
                    {
                        StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY[i.sClId] = i;
                    });
                }

                List<MarketStickerCategoryDTO> categoryDTOs = RingMarkerStickerDAO.GetAllRingMarketSticker();
                if (categoryDTOs != null && categoryDTOs.Count > 0)
                {
                    categoryDTOs.ForEach(i =>
                    {
                        StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[i.sCtId] = i;
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: FetchStickerList()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void FetchRoomList()
        {
            try
            {
                List<string> categoryList = RoomDAO.GetAllRoomCategory();
                if (categoryList != null && categoryList.Count > 0)
                {
                    categoryList.ForEach(i =>
                    {
                        RoomCategoryModel model = new RoomCategoryModel();
                        model.CategoryName = i;
                        RingIDViewModel.Instance.RoomCategoryList.InvokeAdd(model);
                    });
                }

                List<RoomDTO> roomDTOs = RoomDAO.GetAllRoom();
                if (roomDTOs != null && roomDTOs.Count > 0)
                {
                    roomDTOs.ForEach(i =>
                    {
                        RoomModel roomModel = new RoomModel(i);
                        roomModel.FromDB = true;
                        RingIDViewModel.Instance.RoomList[i.RoomID] = roomModel;
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: FetchRoomList()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void FetchChatBackgroundList()
        {
            try
            {
                List<ChatBgImageDTO> chatBgDTOs = ChatBgHistoryDAO.GetChatBgHistoryDTOs();
                if (chatBgDTOs != null && chatBgDTOs.Count > 0)
                {
                    chatBgDTOs.ForEach(i =>
                    {
                        RingIDViewModel.Instance.ChatBackgroundList[i.name] = new ChatBgHistoryModel(i);
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: FetchChatBackgroundList()." + ex.Message + "\n" + ex.StackTrace);
            }

        }

        public static void FetchBlockedNonFriendList()
        {
            try
            {
                ConcurrentDictionary<long, bool[]> list = BlockedNonFriendDAO.GetBlockedNonFriends();
                if (list != null && list.Count > 0)
                {
                    foreach (KeyValuePair<long, bool[]> pair in list)
                    {
                        RingIDViewModel.Instance.BlockedNonFriendList[pair.Key] = new BlockedNonFriendModel
                        {
                            UserTableID = pair.Key,
                            IsBlockedByMe = pair.Value[0],
                            IsBlockedByFriend = pair.Value[1]
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: FetchBlockedNonFriendList()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void BuildGroupList()
        {
            try
            {
                if (ChatDictionaries.Instance.TEMP_GROUP_LIST.Count > 0)
                {
                    ChatDictionaries.Instance.TEMP_GROUP_LIST.ForEach(i =>
                    {
                        GroupInfoModel groupModel = RingIDViewModel.Instance.GroupList.TryGetValue(i.GroupID);
                        if (groupModel == null)
                        {
                            groupModel = new GroupInfoModel();
                            Application.Current.Dispatcher.BeginInvoke(delegate
                            {
                                RingIDViewModel.Instance.GroupList[i.GroupID] = groupModel;
                            });
                        }
                        groupModel.LoadData(i);
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: FetchGroupList()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void BuildGroupMemberList()
        {
            try
            {
                if (ChatDictionaries.Instance.TEMP_GROUP_LIST.Count > 0)
                {
                    ChatDictionaries.Instance.TEMP_GROUP_LIST.ForEach(i =>
                    {
                        GroupInfoModel groupModel = RingIDViewModel.Instance.GroupList.TryGetValue(i.GroupID);
                        if (groupModel != null)
                        {
                            if (i.MemberList != null && i.MemberList.Count > 0)
                            {
                                i.MemberList.ForEach(j =>
                                {
                                    groupModel.AddMemberData(j);
                                });
                            }
                        }
                    });
                    ChatDictionaries.Instance.TEMP_GROUP_LIST.Clear();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: BuildGroupMemberList()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void BuildUnreadChatCallList()
        {
            try
            {
                List<RecentDTO> unreadChatList = RecentChatCallActivityDAO.GetUnreadChatList();
                var chatByGroups = unreadChatList.GroupBy(item => item.ContactID);
                foreach (var group in chatByGroups)
                {
                    string contactID = group.Key;

                    ObservableDictionary<string, long> list = ChatViewModel.Instance.GetUnreadChatByID(contactID);
                    foreach (RecentDTO recentDTO in group)
                    {
                        list[recentDTO.UniqueKey] = recentDTO.Time;
                    }

                    ChatViewModel.Instance.UnreadChatContactList[contactID] = list.Count;
                }

                List<RecentDTO> unreadCallList = RecentChatCallActivityDAO.GetUnreadCallList();
                var callByGroups = unreadCallList.GroupBy(item => item.ContactID);
                foreach (var group in callByGroups)
                {
                    string contactID = group.Key;

                    ObservableDictionary<string, long> list = ChatViewModel.Instance.GetUnreadCallByID(contactID);
                    foreach (RecentDTO recentDTO in group)
                    {
                        list[recentDTO.UniqueKey] = recentDTO.Time;
                    }

                    ChatViewModel.Instance.UnreadCallContactList[contactID] = list.Count;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: BuildUnreadChatCallList()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void UpdateRingMarketStickerLastUsedDate(MarkertStickerImagesModel imageModel)
        {
            try
            {
                long lastUsedDate = ChatService.GetServerTime();
                RingMarkerStickerDAO.UpdateStickerCategoryLastUsedDate(imageModel.StickerCategoryID, lastUsedDate);

                MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(imageModel.StickerCategoryID);
                if (ctgDTO != null)
                {
                    ctgDTO.LastUsedDate = lastUsedDate;
                    RingMarketStickerLoadUtility.AddRecentSticker(imageModel);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UpdateRingMarketStickerLastUsedDate()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ActionIncreaseChatsFavorite(long userTableId)
        {
            try
            {
                UserBasicInfoDTO user = getFromUserBasicInfoDictionary(userTableId);
                //FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userTableId, out user);
                if (user != null && user.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                {
                    user.NumberOfChats += 1;
                    if (user.FavoriteRank < SettingsConstants.FORCE_FAVOURITE_VALUE)
                    {
                        user.FavoriteRank = FriendListLoadUtility.GetFavoriteValue(user);
                        //FriendListLoadUtility.FavoriteTopNonFavoriteListChange(user);
                        changeUI(user);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ActionIncreaseChatsFavorite()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void GoToSite(string url, bool useIE = false)
        {
            url = Uri.UnescapeDataString(url);
            if (useIE) { Process.Start("IExplore.exe", url); return; }
            Process myProcess = new Process();
            try
            {
                // true is the default, but it is important not to set it to false
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = url;
                myProcess.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: GoToSite()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static string GetDisplayRingIDfromUserID(long UserID)
        {
            if (UserID.ToString().Length > 8)
            {
                string userIdentity = UserID.ToString().Substring(2);

                return userIdentity.Insert(4, " ");
            }
            else
            {
                return "";
            }
        }

        public static void ActionIncreaseCallFavorite(long userTableId)
        {
            UserBasicInfoDTO user = getFromUserBasicInfoDictionary(userTableId);
            //     FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userTableId, out user);
            if (user != null)
            {
                user.NumberOfCalls += 1;
                if (user.FavoriteRank < SettingsConstants.FORCE_FAVOURITE_VALUE)
                {
                    user.FavoriteRank = FriendListLoadUtility.GetFavoriteValue(user);
                    changeUI(user);
                }
            }
        }

        public static bool IsValidFilePath(string path)
        {
            if (String.IsNullOrWhiteSpace(path))
                return false;

            Regex driveCheck = new Regex(@"^[a-zA-Z]:\\$");
            if (!driveCheck.IsMatch(path.Substring(0, 3)))
                return false;

            string strTheseAreInvalidFileNameChars = new string(Path.GetInvalidPathChars());
            strTheseAreInvalidFileNameChars += @":/?*" + "\"";
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(strTheseAreInvalidFileNameChars) + "]");
            if (containsABadCharacter.IsMatch(path.Substring(3, path.Length - 3)))
                return false;

            if (File.Exists(path))
                return true;

            return false;
        }

        public static void OpenChatForwardBySentToRingID(string path)
        {
            try
            {
                log.Debug("Invoke => OpenChatForwardBySentToRingID(string path)");

                if (UCGuiRingID.Instance != null && UCGuiRingID.Instance.IsVisible && DefaultSettings.FRIEND_LIST_LOADED)
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                        ucMediaShareToFriendPopup.Show();
                        ucMediaShareToFriendPopup.SetSingleChatFile(new ChatFileDTO { File = path });
                        App.ShowToFront(RingIDSettings.APP_NAME);
                    });
                }
                else
                {
                    if (App.fArgs == null || App.fArgs.Length == 0) App.fArgs = new string[1];
                    App.fArgs[0] = path;
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        App.ShowToFront(RingIDSettings.APP_NAME);
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OpenChatForwardBySentToRingID(string path)." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void OpenChatForwardBySentToRingID()
        {
            try
            {
                log.Debug("Invoke => OpenChatForwardBySentToRingID()");
                bool isValidFilePath = App.fArgs != null && App.fArgs.Length > 0 && HelperMethods.IsValidFilePath(App.fArgs[0]);
                if (isValidFilePath) OpenChatForwardBySentToRingID(App.fArgs[0]);
            }
            catch (Exception ex)
            {
                log.Error("Error: OpenChatForwardBySentToRingID()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void SendDefaultRequestToServer(bool isNotFirstTime = false)
        {
            try
            {
                StreamHelpers.LoadFollowingStreamList(StreamConstants.STREAM_TOP_SCROLL);
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_TOP_SCROLL);
                    ChannelHelpers.LoadFeaturedChannelList(true);
                    StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);
                    ChannelHelpers.LoadMostViewChannelList(true);
                });

                MainSwitcher.ThreadManager().ContactUtIdsRequest().StarThread();
                if (isNotFirstTime == false)
                {
                    if (MainSwitcher.ThreadManager().InstanceOfSuggestionTimer == null)
                    {
                        MainSwitcher.ThreadManager().InstanceOfSuggestionTimer = new SuggestionTimer();
                        MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.IntervalInSecond = 4 * 60 * 60;
                        MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.Tick += ClearPreviousAndFetchNewSuggestions;
                        MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.Start();
                    }
                }
                if (UCMiddlePanelSwitcher.View_UCAllFeeds != null)
                {
                    DefaultSettings.ALL_STARTPKT = SendToServer.GetRanDomPacketID();
                }
                SendCircleListRequest();
                SendCircleListDoingListRequest();
                new ThradNotificationRequest().StartThread(AppConstants.NOTIFICATION_MAX_UT, (short)1);
                new ThradMySettingsRequest().StartThread();
                new ThrdGetStreamCategoryList(String.Empty).Start();
                new ThrdGetChannelCategoryList().Start();
                if (DefaultSettings.userProfile != null) ringStickerService(DefaultSettings.userProfile.IsMobileNumberVerified == 1 ? DefaultSettings.userProfile.MobileDialingCode : null);
                ChatService.RequestOffline(SettingsConstants.VALUE_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT);
                MainSwitcher.AuthSignalHandler().feedSignalHandler.EnqueueDoingDTOforImageDownload();
            }
            catch (Exception ex)
            {
                log.Error("Error: SendDefaultRequestToServer()==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }
        private static void ringStickerService(string countryCode)
        {
            new RingMarketStickerService(countryCode, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler());
        }
        public static void SendStartingFeedRequest()
        {
            DefaultSettings.ALL_STARTPKT = SendToServer.GetRanDomPacketID();
            ThreadAnyFeedsRequest thread = new ThreadAnyFeedsRequest();
            thread.callBackEvent += (response) =>
            {
                switch (response)
                {
                    case SettingsConstants.RESPONSE_SUCCESS:
                        NewsFeedViewModel.Instance.AllFeedsLoadMoreModel.FeedType = 1;
                        break;
                    case SettingsConstants.RESPONSE_NOTSUCCESS:
                        NewsFeedViewModel.Instance.AllFeedsLoadMoreModel.FeedType = 4;
                        break;
                    case SettingsConstants.NO_RESPONSE:
                        NewsFeedViewModel.Instance.AllFeedsLoadMoreModel.FeedType = 5;
                        break;
                }
            };
            NewsFeedViewModel.Instance.AllFeedsLoadMoreModel.FeedType = 3;
            thread.StartThread(Guid.Empty, 2, 0, 0, AppConstants.TYPE_NEWS_FEED, DefaultSettings.ALL_STARTPKT, 0, 0);
        }

        public static void SendOtherRequestToserver()
        {
            try
            {
                StreamHelpers.LoadFollowingStreamList(StreamConstants.STREAM_TOP_SCROLL);
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    StreamHelpers.LoadFeaturedStreamList(StreamConstants.STREAM_TOP_SCROLL);
                    ChannelHelpers.LoadFeaturedChannelList(true);
                    StreamHelpers.LoadRecentStreamList(StreamConstants.STREAM_TOP_SCROLL);
                    ChannelHelpers.LoadMostViewChannelList(true);
                });

                SendStartingFeedRequest();
                if (MainSwitcher.ThreadManager().InstanceOfSuggestionTimer == null)
                {
                    MainSwitcher.ThreadManager().InstanceOfSuggestionTimer = new SuggestionTimer();
                    MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.IntervalInSecond = 4 * 60 * 60;
                    MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.Tick += ClearPreviousAndFetchNewSuggestions;
                    MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.Start();
                }
                SendCircleListRequest();
                SendCircleListDoingListRequest();
                new ThradNotificationRequest().StartThread(AppConstants.NOTIFICATION_MAX_UT, (short)1);
                new ThradMySettingsRequest().StartThread();
                new ThrdGetStreamCategoryList(String.Empty).Start();
                new ThrdGetChannelCategoryList().Start();
                if (DefaultSettings.userProfile != null) ringStickerService(DefaultSettings.userProfile.IsMobileNumberVerified == 1 ? DefaultSettings.userProfile.MobileDialingCode : null);
                ChatService.RequestOffline(SettingsConstants.VALUE_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT);
                MainSwitcher.AuthSignalHandler().feedSignalHandler.EnqueueDoingDTOforImageDownload();
                Wallet.WalletViewModel.Instance.ProcessWalletDailyDwellingOnStartup();
            }
            catch (Exception ex)
            {
                log.Error("Error: SendOtherRequestToserver()==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void SendCircleListRequest()
        {
            JObject pakToSend = new JObject();
            string pakId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = pakId;
            pakToSend[JsonKeys.UpdateTime] = 0;// SettingsConstants.VALUE_RINGID_CIRCLE_UT;
            //pakToSend[JsonKeys.GroupMemberUpdateTime] = -1;
            AuthRequestNoResult(pakToSend, AppConstants.TYPE_CIRCLE_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ClearPreviousAndFetchNewSuggestions(int timeOut)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Clear();
                FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Clear();
                RingIDViewModel.Instance.SuggestionFriendList.Clear();
                JObject pakToSend = new JObject();
                AuthRequestNoResult(pakToSend, AppConstants.TYPE_SUGGESTION_IDS, AppConstants.REQUEST_TYPE_REQUEST);
            });
        }

        public static void InsertIntoCommentListByAscTime(ObservableCollection<CommentModel> commentCollection, JObject commentObj, Guid nfId, Guid imgId, Guid contentId, long postOwnerUtId)
        {
            Guid cmntId = (Guid)commentObj[JsonKeys.CommentId];
            long tm = 0;// (commentObj[JsonKeys.Time] != null) ? (long)commentObj[JsonKeys.Time] : 0;
            if (commentObj[JsonKeys.Time] != null)
            {
                tm = (long)commentObj[JsonKeys.Time];
            }
            else if (commentObj[JsonKeys.UpdateTime] != null)
            {
                tm = (long)commentObj[JsonKeys.UpdateTime];
            }
            CommentModel commentModel = commentCollection.Where(P => P.CommentId == cmntId).FirstOrDefault();
            if (commentModel == null)
            {
                commentModel = new CommentModel();
                commentModel.LoadCommenter(commentObj);
                commentModel.CommentId = cmntId;
                commentModel.Time = tm;
                lock (commentCollection)
                {
                    if (commentCollection.Count == 0)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            commentCollection.Insert(0, commentModel);
                        }, DispatcherPriority.Send);
                    }
                    else
                    {
                        bool inserted = false;
                        for (int i = 0; i < commentCollection.Count; i++)
                        {
                            if (tm < commentCollection[i].Time)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    //UCAllFeeds.Instance.scrollEnabled = false;
                                    commentCollection.Insert(i, commentModel);
                                    //UCAllFeeds.Instance.scrollEnabled = true;
                                }, DispatcherPriority.Send);
                                inserted = true;
                                break;
                            }
                        }
                        if (!inserted)
                        {
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                //UCAllFeeds.Instance.scrollEnabled = false;
                                commentCollection.Add(commentModel);
                                // UCAllFeeds.Instance.scrollEnabled = true;
                            }, DispatcherPriority.Send);
                        }
                    }
                }
            }
            if (nfId != Guid.Empty) commentModel.NewsfeedId = nfId;
            commentModel.PostOwnerUtId = postOwnerUtId;
            if (imgId != Guid.Empty) commentModel.ImageId = imgId;
            else if (contentId != Guid.Empty) commentModel.ContentId = contentId;
            commentModel.LoadData(commentObj);
            //log.Error("Comment==> " + commentModel.Comment + " Name==> " + commentModel.UserShortInfoModel.FullName + commentModel.UserShortInfoModel.ProfileImage);
        }

        public static void InsertIntoCommentListByDscTime(ObservableCollection<CommentModel> commentCollection, JObject commentObj, Guid nfId, Guid imgId, Guid contentId, long postOwnerUtId)
        {
            Guid cmntId = (Guid)commentObj[JsonKeys.CommentId];
            long tm = (long)commentObj[JsonKeys.Time];
            CommentModel commentModel = commentCollection.Where(P => P.CommentId == cmntId).FirstOrDefault();
            if (commentModel == null)
            {
                commentModel = new CommentModel();
                commentModel.CommentId = cmntId;
                commentModel.Time = tm;
                if (commentCollection.Count == 0) commentCollection.InvokeInsert(0, commentModel);
                else
                {
                    bool inserted = false;
                    for (int i = 0; i < commentCollection.Count; i++)
                    {
                        if (tm > commentCollection[i].Time)
                        {
                            commentCollection.InvokeInsert(i, commentModel);
                            inserted = true;
                            break;
                        }
                    }
                    if (!inserted) commentCollection.InvokeAdd(commentModel);
                }
            }
            if (nfId != Guid.Empty) commentModel.NewsfeedId = nfId;
            commentModel.PostOwnerUtId = postOwnerUtId;
            if (imgId != Guid.Empty) commentModel.ImageId = imgId;
            else if (contentId != Guid.Empty) commentModel.ContentId = contentId;
            commentModel.LoadData(commentObj);
        }

        public static bool IsViewVisible(UserControl userControl)
        {
            return !(userControl == null || userControl.IsVisible == false || userControl.Parent == null);
        }

        public static bool IsViewActivate(Window window)
        {
            bool value = false;
            Application.Current.Dispatcher.Invoke(() =>
            {
                value = window.IsActive && window.WindowState != WindowState.Minimized;
            }, DispatcherPriority.Send);
            return value;
        }

        public static bool IsViewAtTop(UCFriendChatCallPanel ucFriendChatCall)
        {
            return ucFriendChatCall != null && (ucFriendChatCall.ChatScrollViewer.srvRecentList.ExtentHeight - ucFriendChatCall.ChatScrollViewer.srvRecentList.VerticalOffset) > (ucFriendChatCall.ChatScrollViewer.srvRecentList.ViewportHeight + 200);
        }

        public static bool IsViewAtTop(UCGroupChatCallPanel ucGroupChatCall)
        {
            return ucGroupChatCall != null && (ucGroupChatCall.ChatScrollViewer.srvRecentList.ExtentHeight - ucGroupChatCall.ChatScrollViewer.srvRecentList.VerticalOffset) > (ucGroupChatCall.ChatScrollViewer.srvRecentList.ViewportHeight + 200);
        }

        public static bool IsViewAtTop(UCRoomChatCallPanel ucRoomChatCall)
        {
            return ucRoomChatCall != null && (ucRoomChatCall.ChatScrollViewer.srvRecentList.ExtentHeight - ucRoomChatCall.ChatScrollViewer.srvRecentList.VerticalOffset) > (ucRoomChatCall.ChatScrollViewer.srvRecentList.ViewportHeight + 200);
        }

        public static T FindVisualParent<T>(DependencyObject child) where T : DependencyObject
        {
            T parent = default(T);
            Visual v = (Visual)VisualTreeHelper.GetParent(child);
            if (v != null)
            {
                parent = v as T;
                if (parent == null) parent = FindVisualParent<T>(v);
            }

            return parent;
        }

        public static T FindVisualChild<T>(DependencyObject parent) where T : DependencyObject
        {
            T child = default(T);

            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = FindVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }

        public static List<object> ChangeViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl)
        {
            List<object> contentList = new List<object>();
            Rect scrollRectangle = new Rect(new Point(0, 0), scrollViewer.RenderSize);
            Rect itemsControlRectangle = itemsControl.TransformToAncestor(scrollViewer).TransformBounds(new Rect(new Point(0, 0), itemsControl.RenderSize));
            if (scrollRectangle.IntersectsWith(itemsControlRectangle))
            {
                for (int i = 0; i < itemsControl.Items.Count; i++)
                {
                    var presenter = itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                    if (presenter == null)
                        break;

                    CustomBorder child = HelperMethods.FindVisualChild<CustomBorder>(presenter);
                    if (child == null)
                        break;

                    if (!child.IsVisible || child.IsOpened)
                        continue;

                    Rect childRectangle = child.TransformToAncestor(scrollViewer).TransformBounds(new Rect(new Point(0, 0), child.RenderSize));
                    if (scrollRectangle.IntersectsWith(childRectangle))
                    {
                        child.IsOpened = true;
                        if (presenter is ContentPresenter)
                        {
                            contentList.Add(((ContentPresenter)presenter).Content);
                        }
                        else if (presenter is ListBoxItem)
                        {
                            contentList.Add(((ListBoxItem)presenter).Content);
                        }
                    }
                }
            }
            return contentList;
        }

        public static string GetDownloadedFilePathFromStreamURL(string path, Guid mediaContentId)
        {
            string[] arr = path.Split("/"[0]);
            if (arr.Length > 1) return RingIDSettings.TEMP_DOWNLOADED_MEDIA_FOLDER + "\\" + arr[arr.Length - 2] + "_" + mediaContentId + "_" + arr[arr.Length - 1];
            else return RingIDSettings.TEMP_DOWNLOADED_MEDIA_FOLDER + "\\" + mediaContentId + "_" + arr[arr.Length - 1];
        }

        public static void Runupdater(bool needToWaringMessage)
        {
            try
            {
                string updaterModulePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + Path.DirectorySeparatorChar + "updater.exe";
                log.Info("updaterModulePath==>" + updaterModulePath);
                if (System.IO.File.Exists(updaterModulePath))
                {
                    Process process = Process.Start(updaterModulePath, "/checknow");
                    process.Close();
                    if (UCGuiRingID.Instance == null)
                    {
                        UIHelperMethods.HideTrayICon();
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }
                    else System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
                else
                {
                    if (needToWaringMessage) UIHelperMethods.ShowInformation(NotificationMessages.NO_UPDATER, "Download new version");
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception updaterModulePath==>" + ex.Message + "\n" + ex.StackTrace);
                if (needToWaringMessage) UIHelperMethods.ShowInformation(NotificationMessages.NO_UPDATER, "Download new version");
            }
        }

        //public static void FocusMainWindow()
        //{
        //    try
        //    {
        //        if (SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW)
        //        {
        //            MainSwitcher.MainController().MainUIController().MainWindow.FocusWindow();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error ==>" + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        public static void ConvertUtIdToSwitchFriendProfile(string type, long utId, Func<string, long, bool, bool> onComplete)
        {
            UserBasicInfoModel basicInfoModel = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(utId);
            if (basicInfoModel == null)
            {
                ThradUnknownProfileInfoRequest request = new ThradUnknownProfileInfoRequest();
                request.OnSuccess += (userDTO) =>
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        onComplete(type, userDTO.UserTableID, true);
                    });
                };
                request.StartThread(utId, false);
            }
            else
            {
                onComplete(type, basicInfoModel.ShortInfoModel.UserTableID, false);
            }
        }

        public static GroupInfoModel GetModelToSwtichGroupProfile(long groupID)
        {
            GroupInfoModel groupInfoModel = RingIDViewModel.Instance.GetGroupInfoModelByGroupID(groupID);
            if (groupInfoModel.IsPartial)
            {
                ChatService.GetGroupInformationWithMembers(groupID, (args) =>
                {
                    new Thread(() =>
                    {
                        if (args.Status)
                        {
                            for (int idx = 0; idx < 15000 && !groupInfoModel.MemberInfoDictionary.ContainsKey(DefaultSettings.LOGIN_TABLE_ID); idx += 500)
                            {
                                Thread.Sleep(500);
                            }
                            ChatService.StartGroupChat(groupID, groupInfoModel);
                        }
                        else
                        {
                            ChatService.StartGroupChat(groupID, groupInfoModel);
                        }
                    }).Start();
                });
            }
            else
            {
                ChatService.StartGroupChat(groupID, groupInfoModel);
            }
            return groupInfoModel;
        }

        public static RoomModel GetModelToSwtichRoomProfile(string roomID)
        {

            RoomModel roomModel = RingIDViewModel.Instance.RoomList.TryGetValue(roomID);
            if (roomModel == null)
            {
                roomModel = new RoomModel();
                roomModel.RoomID = roomID;
                roomModel.RoomName = roomID;
                roomModel.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
                roomModel.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
                roomModel.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
                roomModel.IsPartial = true;
                RingIDViewModel.Instance.RoomList[roomID] = roomModel;
            }

            if (roomModel.FromDB == false)
            {
                List<RoomDTO> roomList = new List<RoomDTO>();
                RoomDTO roomDTO = new RoomDTO();
                roomDTO.RoomID = roomModel.RoomID;
                roomDTO.RoomName = roomModel.RoomName;
                roomDTO.RoomProfileImage = roomModel.RoomProfileImage;
                roomDTO.NumberOfMembers = roomModel.NumberOfMembers;
                roomDTO.IsPartial = roomModel.IsPartial;
                roomDTO.ChatBgUrl = roomModel.ChatBgUrl;
                roomDTO.ChatMinPacketID = roomModel.ChatMinPacketID;
                roomDTO.ChatMaxPacketID = roomModel.ChatMaxPacketID;
                roomDTO.ImSoundEnabled = roomModel.ImSoundEnabled;
                roomDTO.ImNotificationEnabled = roomModel.ImNotificationEnabled;
                roomDTO.ImPopupEnabled = roomModel.ImPopupEnabled;
                roomList.Add(roomDTO);
                new InsertIntoRoomListTable(roomList).Start();
                roomModel.FromDB = true;
            }

            if (roomModel.IsPartial)
            {
                ChatService.GetRoomInformation(roomID, (args) =>
                {
                    ChatService.StartPublicRoomChat(roomID, roomModel);
                });
            }
            else
            {
                ChatService.StartPublicRoomChat(roomID, roomModel);
            }

            return roomModel;
        }

        public static void ClearUIOfPreviousFriend(UCFriendProfile ucFriendProfilePanel)
        {
            #region FEED
            if (ucFriendProfilePanel != null && ucFriendProfilePanel._UCFriendNewsFeeds != null)
            {
                //ucFriendProfilePanel._UCFriendNewsFeeds.FriendNewsFeeds.RemoveAllModels();
            }
            #endregion

            #region PHOTOS
            if (ucFriendProfilePanel._UCFriendPhotos != null)
            {
                ucFriendProfilePanel._UCFriendPhotos = null;

                lock (ucFriendProfilePanel.FriendProfileImageList)
                {
                    while (ucFriendProfilePanel.FriendProfileImageList.Count > 1)
                    {
                        ImageModel model = ucFriendProfilePanel.FriendProfileImageList.FirstOrDefault();
                        if (!model.IsLoadMore)
                        {
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel.FriendProfileImageList.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }
                }

                lock (ucFriendProfilePanel.FriendCoverImageList)
                {
                    while (ucFriendProfilePanel.FriendCoverImageList.Count > 1)
                    {
                        ImageModel model = ucFriendProfilePanel.FriendCoverImageList.FirstOrDefault();
                        if (!model.IsLoadMore)
                        {
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel.FriendCoverImageList.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }
                }

                lock (ucFriendProfilePanel.FriendFeedImageList)
                {
                    while (ucFriendProfilePanel.FriendFeedImageList.Count > 1)
                    {
                        ImageModel model = ucFriendProfilePanel.FriendFeedImageList.FirstOrDefault();
                        if (!model.IsLoadMore)
                        {
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel.FriendFeedImageList.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }
                }
            }
            #endregion

            #region CONTACT_LIST
            if (ucFriendProfilePanel._UCFriendContactList != null)
            {
                ucFriendProfilePanel._UCFriendContactList = null;
                FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY.Remove(ucFriendProfilePanel._FriendTableID);
                FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.Remove(ucFriendProfilePanel._FriendTableID);

                lock (ucFriendProfilePanel.FriendList)
                {
                    while (ucFriendProfilePanel.FriendList.Count > 1)
                    {
                        UserBasicInfoModel model = ucFriendProfilePanel.FriendList.FirstOrDefault();
                        if (model.ShortInfoModel.UserTableID != 0)
                        {
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel.FriendList.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }
                }

                lock (ucFriendProfilePanel.MutualList)
                {
                    while (ucFriendProfilePanel.MutualList.Count > 1)
                    {
                        UserBasicInfoModel model = ucFriendProfilePanel.MutualList.FirstOrDefault();
                        if (model.ShortInfoModel.UserTableID != 0)
                        {
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel.MutualList.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }
                }

                lock (ucFriendProfilePanel.TempFriendList)
                {
                    ucFriendProfilePanel.TempFriendList.Clear();
                }
            }
            #endregion

            #region ABOUT
            if (ucFriendProfilePanel._UCFriendAbout != null)
            {
                RingDictionaries.Instance.WORK_DICTIONARY.Remove(ucFriendProfilePanel._FriendTableID);
                RingDictionaries.Instance.EDUCATION_DICTIONARY.Remove(ucFriendProfilePanel._FriendTableID);
                RingDictionaries.Instance.SKILL_DICTIONARY.Remove(ucFriendProfilePanel._FriendTableID);
                if (ucFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer != null)
                {
                    lock (ucFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer.FriendWorkModelsList)
                    {
                        while (ucFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer.FriendWorkModelsList.Count > 0)
                        {
                            SingleWorkModel model = ucFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer.FriendWorkModelsList.FirstOrDefault();
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer.FriendWorkModelsList.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }
                    ucFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer = null;
                }

                if (ucFriendProfilePanel._UCFriendAbout.ucFriendEducation != null)
                {
                    lock (ucFriendProfilePanel._UCFriendAbout.ucFriendEducation.FriendEducationModelsList)
                    {
                        while (ucFriendProfilePanel._UCFriendAbout.ucFriendEducation.FriendEducationModelsList.Count > 0)
                        {
                            SingleEducationModel model = ucFriendProfilePanel._UCFriendAbout.ucFriendEducation.FriendEducationModelsList.FirstOrDefault();
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel._UCFriendAbout.ucFriendEducation.FriendEducationModelsList.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }

                    ucFriendProfilePanel._UCFriendAbout.ucFriendEducation = null;
                }

                if (ucFriendProfilePanel._UCFriendAbout.ucFriendSkill != null)
                {
                    lock (ucFriendProfilePanel._UCFriendAbout.ucFriendSkill.FriendSkillModelsList)
                    {
                        while (ucFriendProfilePanel._UCFriendAbout.ucFriendSkill.FriendSkillModelsList.Count > 0)
                        {
                            SingleSkillModel model = ucFriendProfilePanel._UCFriendAbout.ucFriendSkill.FriendSkillModelsList.FirstOrDefault();
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel._UCFriendAbout.ucFriendSkill.FriendSkillModelsList.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }
                    ucFriendProfilePanel._UCFriendAbout.ucFriendSkill = null;
                }

                ucFriendProfilePanel._UCFriendAbout = null;
            }
            #endregion

            #region ALBUM
            if (ucFriendProfilePanel._UCFriendProfileMediaAlbum != null)
            {
                if (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum != null)
                {
                    lock (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums)
                    {
                        while (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums.Count > 0)
                        {
                            MediaContentModel model = ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums.FirstOrDefault();
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }
                    ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum = null;
                }

                if (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum != null)
                {
                    lock (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums)
                    {
                        while (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums.Count > 0)
                        {
                            MediaContentModel model = ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums.FirstOrDefault();
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }
                    ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum = null;
                }


                //if (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio != null)
                //{

                //    lock (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio.FriendSingleMediaAudios)
                //    {
                //        while (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio.FriendSingleMediaAudios.Count > 0)
                //        {
                //            SingleMediaModel model = ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio.FriendSingleMediaAudios.FirstOrDefault();
                //            Application.Current.Dispatcher.Invoke((Action)delegate
                //            {
                //                ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio.FriendSingleMediaAudios.Remove(model);
                //                GC.SuppressFinalize(model);
                //            }, DispatcherPriority.Send);
                //        }
                //    }
                //    ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio = null;
                //}


                //if (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo != null)
                //{

                //    lock (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo.FriendSingleMediaVedios)
                //    {
                //        while (ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo.FriendSingleMediaVedios.Count > 0)
                //        {
                //            SingleMediaModel model = ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo.FriendSingleMediaVedios.FirstOrDefault();
                //            Application.Current.Dispatcher.Invoke((Action)delegate
                //            {
                //                ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo.FriendSingleMediaVedios.Remove(model);
                //                GC.SuppressFinalize(model);
                //            }, DispatcherPriority.Send);
                //        }
                //    }
                //    ucFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo = null;
                //}

                if (ucFriendProfilePanel._UCFriendProfileMediaAlbum != null)
                {
                    ucFriendProfilePanel._UCFriendProfileMediaAlbum = null;
                }
            }
            #endregion

            ucFriendProfilePanel.UCFPContentSwitcherInstance.Content = null;
            ucFriendProfilePanel.SelectedBorder = 0;
            ucFriendProfilePanel.TempFriendPreviewImageCount = 0;
            ucFriendProfilePanel.IsMutualFreindRequestSend = false;

        }

        public static void InitFriendChatCallPanel(UCFriendChatCallPanel ucFriendChatCallPanel, bool force = false)
        {
            long userTableID = ucFriendChatCallPanel._FriendTableID;
            UserBasicInfoModel friendInfoModel = ucFriendChatCallPanel.FriendBasicInfoModel;
            BlockedNonFriendModel blockModel = ucFriendChatCallPanel.BlockedNonFriendModel;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (force)
                {
                    if (!ChatService.StartFriendChat(userTableID, friendInfoModel, blockModel, (args) =>
                    {
                        if (!args.Status)
                        {
                            if (friendInfoModel.ShortInfoModel.FriendShipStatus == 0)
                            {
                                ThradUnknownProfileInfoRequest request = new ThradUnknownProfileInfoRequest();
                                request.OnSuccess += (userDTO) => { new ThreadFriendPresenceInfo(userTableID); };
                                request.StartThread(userTableID, true);
                            }
                            else
                            {
                                new ThreadFriendPresenceInfo(args.FriendTableID);
                            }
                        }
                    }))
                    {
                        if (HelperMethods.HasAnonymousAndFriendChatPermission(friendInfoModel, blockModel))
                        {
                            if (friendInfoModel.ShortInfoModel.FriendShipStatus == 0)
                            {
                                ThradUnknownProfileInfoRequest request = new ThradUnknownProfileInfoRequest();
                                request.OnSuccess += (userDTO) => { new ThreadFriendPresenceInfo(userTableID); };
                                request.StartThread(userTableID, true);
                            }
                            else
                            {
                                new ThreadFriendPresenceInfo(userTableID);
                            }
                        }
                    }
                }
                else
                {
                    ChatService.StartFriendChat(userTableID, friendInfoModel, blockModel);
                }

                ucFriendChatCallPanel.loadInitHistory();
                ucFriendChatCallPanel.CurrentDateEvent = new DateEventDTO(DateTime.Now.Day, DateTime.Now.Month, null);

                if (force)
                {
                    HelperMethods.ActionIncreaseChatsFavorite(userTableID);
                }
            }, DispatcherPriority.ApplicationIdle);
        }

        public static void InitGroupChatCallPanel(UCGroupChatCallPanel ucGroupChatCallPanel)
        {
            long groupID = ucGroupChatCallPanel._GroupID;
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                ucGroupChatCallPanel.loadInitHistory();
                ucGroupChatCallPanel.CurrentDateEvent = new DateEventDTO(DateTime.Now.Day, DateTime.Now.Month, null);
            }, DispatcherPriority.ApplicationIdle);
        }

        public static void InitRoomChatCallPanel(UCRoomChatCallPanel ucRoomChatCallPanel)
        {
            string roomID = ucRoomChatCallPanel._RoomID;
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                ucRoomChatCallPanel.loadInitHistory();
                ucRoomChatCallPanel.CurrentDateEvent = new DateEventDTO(DateTime.Now.Day, DateTime.Now.Month, null);
            }, DispatcherPriority.ApplicationIdle);
        }

        public static int CalculateCircleSizeOfAddFriendNotifactionCounter(int count)
        {
            int size = 0;
            if (0 <= count && count <= 9)
            {
                size = 20;//15;
            }
            else if (10 <= count && count <= 99)
            {
                size = 25;//18;
            }
            // else if (100 <= count && count <= 999)
            else
            {
                size = 29;//22;
            }
            return size;
        }

        public static void RemoveAUserControlFromAnotherPanel(UserControl panel)
        {
            try
            {
                if (panel != null)
                {
                    Panel parentPanel = (Panel)panel.Parent;
                    if (parentPanel != null && parentPanel.Children.Count > 0) parentPanel.Children.Remove(panel);
                }
            }
            catch (Exception ex) { log.Error(ex.Message + ex.StackTrace); }
        }

        public static void DownloadMandatoryUpdater(string message = null)
        {
            Application.Current.Dispatcher.Invoke(delegate
            {
                if (string.IsNullOrEmpty(message)) message = "Need to update now. Update to the latest version of ringID!";
                //MessageBoxResult result = CustomMessageBox.ShowWarning(message, "Mandatory update!", true);
                //if (result == MessageBoxResult.OK)
                //{
                WNConfirmationView cv = new WNConfirmationView("Download mandatory", message, CustomConfirmationDialogButtonOptions.OKCancel, "You have to update your ringID to enjoy the new features");
                var result = cv.ShowCustomDialog();
                if (result == ConfirmationDialogResult.OK)
                {
                    UIHelperMethods.HideTrayICon();
                    Runupdater(true);
                }
                else
                {
                    UIHelperMethods.HideTrayICon();
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            });
        }

        public static void OnMainUserControlChanged(int type)
        {
            try
            {
                if (StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_FINISHED)
                {
                    UCStreamAndChannelViewer.Instance.ShowWindow();
                }
                else
                {
                    UCStreamAndChannelViewer.Instance.Dispose();
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMainUserControlChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoFeaturedStreamAndChannelList(StreamDTO newStreamDTO, ChannelDTO newChannelDTO, bool insertAtTop = false)
        {
            try
            {
                ObservableCollection<StreamAndChannelModel> viewerModelList = RingIDViewModel.Instance.StreamAndChannelFeatureList;
                lock (HelperMethods._PROCESS_SYNC_LOCK)
                {
                    if (newStreamDTO != null)
                    {
                        StreamAndChannelModel newModel = viewerModelList.Where(P => P.ViewType == SettingsConstants.TYPE_STREAM && P.Stream.UserTableID == newStreamDTO.UserTableID).FirstOrDefault();
                        if (newModel != null)
                        {
                            newModel.LoadData(newStreamDTO);
                            if (insertAtTop)
                            {
                                int index = viewerModelList.IndexOf(newModel);
                                if (index > 0)
                                {
                                    viewerModelList.InvokeMove(index, 0);
                                }
                            }
                        }
                        else
                        {
                            if (insertAtTop)
                            {
                                viewerModelList.InvokeInsert(0, new StreamAndChannelModel(newStreamDTO));
                            }
                            else
                            {
                                viewerModelList.InvokeAdd(new StreamAndChannelModel(newStreamDTO));
                            }
                        }
                    }
                    else if (newChannelDTO != null)
                    {
                        StreamAndChannelModel newModel = viewerModelList.Where(P => P.ViewType == SettingsConstants.TYPE_CHANNEL && P.Channel.ChannelID == newChannelDTO.ChannelID).FirstOrDefault();
                        if (newModel != null)
                        {
                            newModel.LoadData(newChannelDTO);
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(new StreamAndChannelModel(newChannelDTO));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoFeaturedStreamAndChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void RemoveFromFeaturedStreamAndChannelList(int type = SettingsConstants.TYPE_STREAM_AND_CHANNEL)
        {
            try
            {
                ObservableCollection<StreamAndChannelModel> viewerModelList = RingIDViewModel.Instance.StreamAndChannelFeatureList;
                lock (HelperMethods._PROCESS_SYNC_LOCK)
                {
                    if (type == SettingsConstants.TYPE_STREAM_AND_CHANNEL)
                    {
                        viewerModelList.Clear();
                    }
                    else if (type == SettingsConstants.TYPE_STREAM)
                    {
                        for (int idx = viewerModelList.Count - 1; idx >= 0; idx--)
                        {
                            if (viewerModelList[idx].ViewType == SettingsConstants.TYPE_STREAM) viewerModelList.RemoveAt(idx);
                        }
                    }
                    else if (type == SettingsConstants.TYPE_CHANNEL)
                    {
                        for (int idx = viewerModelList.Count - 1; idx >= 0; idx--)
                        {
                            if (viewerModelList[idx].ViewType == SettingsConstants.TYPE_CHANNEL) viewerModelList.RemoveAt(idx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("RemoveIntoFeaturedStreamAndChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ChangeStreamAndChannelViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl, int paddingTop)
        {
            try
            {
                //double offset = scrollViewer.VerticalOffset - paddingTop;
                //double height = itemsControl.ActualHeight - scrollViewer.ActualHeight > 0 ? scrollViewer.ActualHeight : itemsControl.ActualHeight;
                //double x = (scrollViewer.ActualWidth - 615) / 2;
                //double y = offset + height;
                double offset = scrollViewer.VerticalOffset;
                double x = 25;
                double height = itemsControl.ActualHeight;
                double y = itemsControl.ActualHeight;

                if ((itemsControl.ActualHeight + paddingTop) - scrollViewer.ActualHeight > 0)
                {
                    height = scrollViewer.ActualHeight;
                    y = height + offset - paddingTop;
                }

                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y));
                if (hitTest != null)
                {
                    int maxIndex = itemsControl.Items.Count - 1;
                    int startIndex = maxIndex;

                    ContentPresenter presenter = ChatHelpers.GetContentPresenterFromEvent(hitTest.VisualHit, itemsControl) as ContentPresenter;
                    if (presenter != null && !(presenter.Content is StreamAndChannelModel))
                    {
                        presenter = HelperMethods.FindVisualParent<ContentPresenter>(presenter);
                    }

                    if (presenter != null)
                    {
                        startIndex = itemsControl.ItemContainerGenerator.IndexFromContainer(presenter);
                        if (startIndex < 0) return;
                    }

                    int overLimit = 2;
                    int index = (startIndex + 2 > maxIndex ? maxIndex : startIndex + 2);

                    int i = maxIndex - index > 3 ? index + 3 : maxIndex;
                    for (; i > index; i--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        StreamAndChannelModel model = (StreamAndChannelModel)presenter.Content;
                        model.IsViewOpened = false;
                    }

                    for (int overflow = 0; index >= 0; index--)
                    {
                        if (height <= 0 && (++overflow) >= overLimit) break;

                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(index);
                        if (temp == null)
                            break;

                        presenter = temp as ContentPresenter;
                        StreamAndChannelModel model = (StreamAndChannelModel)presenter.Content;
                        model.IsViewOpened = true;

                        if (index <= startIndex)
                        {
                            height -= presenter.ActualHeight;
                        }
                    }

                    int count = index > 3 ? index - 3 : 0;
                    for (int j = index; j >= count; j--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(j);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        StreamAndChannelModel model = (StreamAndChannelModel)presenter.Content;
                        model.IsViewOpened = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeStreamAndChannelViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static System.IO.Stream DownloadImage(Uri uri, string savePath)
        {
            var request = WebRequest.Create(uri);
            var response = request.GetResponse();
            using (var stream = response.GetResponseStream())
            {
                Byte[] buffer = new Byte[response.ContentLength];
                int offset = 0, actuallyRead = 0;
                do
                {
                    actuallyRead = stream.Read(buffer, offset, buffer.Length - offset);
                    offset += actuallyRead;
                }
                while (actuallyRead > 0);
                File.WriteAllBytes(savePath, buffer);
                return new MemoryStream(buffer);
            }
        }

        public static bool ParseYoutubeURL(string url, out string youtubeVideoID)
        {
            Regex youtubeRegex = new Regex(SocialMediaConstants.YOUTUBE_URL_PATTERN, RegexOptions.IgnoreCase | RegexOptions.Multiline);
            var youtubeMatch = youtubeRegex.Match(url);
            if (youtubeMatch.Success)
            {
                youtubeVideoID = youtubeMatch.Groups[1].Value;
                return true;
            }
            else
            {
                youtubeVideoID = null;
                return false;
            }
        }

        public static bool YoutubeFileExists(string youtubeVideoID)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(string.Format("https://www.youtube.com/v/{0}", youtubeVideoID)) as HttpWebRequest;
                request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                response.Close();

                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception)
            {
                return false;
                //throw;
            }
        }

        public static string GetStandardBrowserPath()
        {
            string name = string.Empty;
            RegistryKey regKey = null;

            try
            {
                regKey = Registry.ClassesRoot.OpenSubKey("HTTP\\shell\\open\\command", false);

                name = regKey.GetValue(null).ToString().ToLower().Replace("" + (char)34, "");

                if (!name.EndsWith("exe"))
                    name = name.Substring(0, name.LastIndexOf(".exe") + 4);

            }
            catch (Exception ex)
            {
                name = string.Format("ERROR: An exception of type: {0} occurred in method: {1}", ex.GetType(), ex.TargetSite);
            }
            finally
            {
                //check and see if the key is still open, if so
                //then close it
                if (regKey != null)
                    regKey.Close();
            }
            //return the value
            return name;
        }

        #endregion "Public Methods"

        #region "Utility Methods"

        private static UserBasicInfoDTO getFromUserBasicInfoDictionary(long userTableID)
        {
            return FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userTableID);
        }

        public static void SendNewsFeedsRequest(long time, short scl, int limit)
        {
            //if (MainSwitcher.ThreadManager().thradNewsFeedsRequest == null)
            //{
            //    MainSwitcher.ThreadManager().thradNewsFeedsRequest = new ThradNewsFeedsRequest();
            //}
            //MainSwitcher.ThreadManager().thradNewsFeedsRequest.StartThread(time, scl, limit);
            //new ThradNewsFeedsRequest().StartThread(time, scl, limit);
        }

        private static FeedModel GetFeedModel(FeedDTO feedDto)
        {
            return null;//return (FeedModel)MainSwitcher.AuthSignalHandler().feedSignalHandler.GetFeedModel(feedDto);
        }

        private static void AuthRequestNoResult(JObject pakToSend2, int action1, int requestType, string message = null)
        {
            (new AuthRequestNoResult()).StartThread(pakToSend2, action1, requestType, message);
        }

        #endregion "Utility Methods"

        #region "Block/Unblock Methods"

        public static MessageBoxResult ShowAnonymousWarning(string actionName = CustomStrings.TEXT_OK)
        {
            MessageBoxResult result = CustomMessageBox.ShowWarning(
                NotificationMessages.NON_FRIEND_ANONYMOUS_CHAT_ALLOW_WARNING,
                NotificationMessages.NON_FRIEND_ANONYMOUS_CHAT_ALLOW_WARNING_TITLE,
                MessageBoxButton.YesNo,
                MessageBoxResult.Yes,
                new String[] { "Cancel", actionName }
                );
            return result;
        }

        //public static MessageBoxResult ShowBlockWarning(UserBasicInfoModel friendBasicInfoModel, int accessType, string actionName = CustomStrings.TEXT_OK)
        //{
        //    string title = String.Empty;
        //    string msg = String.Empty;

        //    if (accessType == StatusConstants.CHAT_ACCESS)
        //    {
        //        title = NotificationMessages.UNBLOCK_WARNING_CHAT_TITLE;
        //        msg = String.Format(NotificationMessages.UNBLOCK_WARNING_CHAT, friendBasicInfoModel.ShortInfoModel.FullName);
        //    }
        //    else if (accessType == StatusConstants.CALL_ACCESS)
        //    {
        //        title = NotificationMessages.UNBLOCK_WARNING_CALL_TITLE;
        //        msg = String.Format(NotificationMessages.UNBLOCK_WARNING_CALL, friendBasicInfoModel.ShortInfoModel.FullName);
        //    }
        //    else
        //    {
        //        title = String.Format(NotificationMessages.UNBLOCK_WARNING_TITLE, friendBasicInfoModel.ShortInfoModel.FullName);
        //        msg = String.Format(NotificationMessages.UNBLOCK_WARNING, friendBasicInfoModel.ShortInfoModel.FullName);
        //    }

        //    MessageBoxResult result = CustomMessageBox.ShowWarning(
        //        msg,
        //        title,
        //        MessageBoxButton.YesNo,
        //        MessageBoxResult.Yes,
        //        new String[] { "Cancel", actionName }
        //        );
        //    return result;
        //}

        public static bool ShowBlockWarning(UserBasicInfoModel friendBasicInfoModel, int accessType)
        {
            string title = String.Empty;
            string msg = String.Empty;

            if (accessType == StatusConstants.CHAT_ACCESS)
            {
                title = NotificationMessages.UNBLOCK_WARNING_CHAT_TITLE;
                msg = String.Format(NotificationMessages.UNBLOCK_WARNING_CHAT, friendBasicInfoModel.ShortInfoModel.FullName);
            }
            else if (accessType == StatusConstants.CALL_ACCESS)
            {
                title = NotificationMessages.UNBLOCK_WARNING_CALL_TITLE;
                msg = String.Format(NotificationMessages.UNBLOCK_WARNING_CALL, friendBasicInfoModel.ShortInfoModel.FullName);
            }
            else
            {
                title = String.Format(NotificationMessages.UNBLOCK_WARNING_TITLE, friendBasicInfoModel.ShortInfoModel.FullName);
                msg = String.Format(NotificationMessages.UNBLOCK_WARNING, friendBasicInfoModel.ShortInfoModel.FullName);
            }

            //MessageBoxResult result = CustomMessageBox.ShowWarning(
            //    msg,
            //    title,
            //    MessageBoxButton.YesNo,
            //    MessageBoxResult.Yes,
            //    new String[] { "Cancel", actionName }
            //    );
            //return result == MessageBoxResult.Yes;
            WNConfirmationView cv = new WNConfirmationView("Access change confirmation", title, CustomConfirmationDialogButtonOptions.OKCancel, msg);
            var result = cv.ShowCustomDialog();
            return result == ConfirmationDialogResult.OK;
        }

        public static bool HasAnonymousChatPermission(UserBasicInfoModel model)
        {
            bool value = true;

            try
            {
                if (model != null && model.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                    value = RingIDViewModel.Instance.MyBasicInfoModel.AnonymousChatAccess == StatusConstants.CHAT_CALL_EVERYONE;
            }
            catch (Exception ex)
            {
                log.Error("Error: HasAnonymousChatPermission()." + ex.Message + "\n" + ex.StackTrace);
            }

            return value;
        }

        public static bool HasFriendChatPermission(UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel, out int accessType)
        {
            bool value = true;
            accessType = -1;

            try
            {
                if (friendInfoModel != null && blockModel != null)
                {
                    if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                    {
                        if (friendInfoModel.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED && friendInfoModel.CallAccess == StatusConstants.TYPE_ACCESS_BLOCKED && friendInfoModel.FeedAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
                        {
                            accessType = StatusConstants.FULL_ACCESS;
                            value = false;
                        }
                        else if (friendInfoModel.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
                        {
                            accessType = StatusConstants.CHAT_ACCESS;
                            value = false;
                        }
                    }
                    else if (friendInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                    {
                        if (blockModel.IsBlockedByMe)
                        {
                            accessType = StatusConstants.FULL_ACCESS;
                            value = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: HasFriendChatPermission()." + ex.Message + "\n" + ex.StackTrace);
            }

            return value;
        }

        public static bool HasFriendCallPermission(UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel, out int accessType)
        {
            bool value = true;
            accessType = -1;

            try
            {
                if (friendInfoModel != null && blockModel != null)
                {
                    if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                    {
                        if (friendInfoModel.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED && friendInfoModel.CallAccess == StatusConstants.TYPE_ACCESS_BLOCKED && friendInfoModel.FeedAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
                        {
                            accessType = StatusConstants.FULL_ACCESS;
                            value = false;
                        }
                        else if (friendInfoModel.CallAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
                        {
                            accessType = StatusConstants.CALL_ACCESS;
                            value = false;
                        }
                    }
                    else if (friendInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                    {
                        if (blockModel.IsBlockedByMe)
                        {
                            accessType = StatusConstants.FULL_ACCESS;
                            value = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: HasFriendCallPermission()." + ex.Message + "\n" + ex.StackTrace);
            }

            return value;
        }

        public static bool HasFriendCallPermission(UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel)
        {
            bool value = true;
            try
            {
                if (friendInfoModel != null && blockModel != null)
                    if (friendInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        if (blockModel.IsBlockedByFriend) value = false;
            }
            catch (Exception ex)
            {
                log.Error("Error: HasFriendCallPermission()." + ex.Message + "\n" + ex.StackTrace);
            }
            return value;
        }

        public static bool HasAnonymousAndFriendChatPermission(UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel)
        {
            bool value = true;
            try
            {
                if (friendInfoModel != null && blockModel != null)
                {
                    if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        value = friendInfoModel.ChatAccess == StatusConstants.TYPE_ACCESS_UNBLOCKED;
                    else if (friendInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        value = !(blockModel.IsBlockedByMe || RingIDViewModel.Instance.MyBasicInfoModel.AnonymousChatAccess == StatusConstants.CHAT_CALL_FRIENDS_ONLY);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: HasAnonymousAndFriendChatPermission()." + ex.Message + "\n" + ex.StackTrace);
            }
            return value;
        }

        public static bool IsChatBlockedByFriend(long friendId, UserBasicInfoModel friendInfoModel = null, BlockedNonFriendModel blockModel = null)
        {
            bool value = false;

            try
            {
                friendInfoModel = friendInfoModel == null ? RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(friendId) : friendInfoModel;
                blockModel = blockModel == null ? RingIDViewModel.Instance.GetBlockedNonFriendModelByID(friendId) : blockModel;

                if (friendInfoModel != null && blockModel != null)
                {
                    if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED) value = blockModel.IsFriendRegDenied;
                    else if (friendInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        value = blockModel.IsBlockedByFriend;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: IsChatBlockedByFriend()." + ex.Message + "\n" + ex.StackTrace);
            }

            return value;
        }

        public static bool IsChatBlockedByAnySide(long friendId, UserBasicInfoModel friendInfoModel = null, BlockedNonFriendModel blockModel = null)
        {
            bool value = false;

            try
            {
                friendInfoModel = friendInfoModel == null ? RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(friendId) : friendInfoModel;
                blockModel = blockModel == null ? RingIDViewModel.Instance.GetBlockedNonFriendModelByID(friendId) : blockModel;

                if (friendInfoModel != null && blockModel != null)
                {
                    if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        value = blockModel.IsFriendRegDenied || friendInfoModel.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED;
                    else if (friendInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                        value = blockModel.IsBlockedByFriend || blockModel.IsBlockedByMe || RingIDViewModel.Instance.MyBasicInfoModel.AnonymousChatAccess == StatusConstants.CHAT_CALL_FRIENDS_ONLY;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: IsChatBlockedByFriend()." + ex.Message + "\n" + ex.StackTrace);
            }

            return value;
        }

        public static void ChangeAnonymousSettingsWrapper(Func<bool, int> onComplete, int settingName)
        {
            if (settingName > -1) ChangeAnonymousSettings(onComplete, settingName);
            else onComplete(true);
        }

        private static void ChangeAnonymousSettings(Func<bool, int> onComplete, int settingName)
        {
            try
            {
                ThradChangeAccessSettings requester = new ThradChangeAccessSettings(settingName, 1);
                requester.OnComplete += (status, erroMessage) =>
                {
                    if (!status)
                        UIHelperMethods.ShowFailed(erroMessage, "Change settings");
                    //CustomMessageBox.ShowError(erroMessage, "Failed!");
                    onComplete(status);
                };
                requester.StartThread();
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeAnonymousValue() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ChangeBlockUnblockSettingsWrapper(int accessType, UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel, Func<bool, int> onComplete = null, bool showAlert = false)
        {
            if (accessType > -1) ChangeBlockUnblockSettings(accessType, friendInfoModel, blockModel, onComplete, showAlert);
            else onComplete(true);
        }

        private static void ChangeBlockUnblockSettings(int accessType, UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel, Func<bool, int> onComplete, bool showAlert)
        {
            try
            {
                friendInfoModel.IsAccessChanging = true;
                if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                {
                    #region FRIEND BLOCK

                    int prevChatAccess = friendInfoModel.ChatAccess;
                    int prevCallAccess = friendInfoModel.CallAccess;
                    int prevFeedAccess = friendInfoModel.FeedAccess;

                    if (accessType == StatusConstants.FULL_ACCESS)
                    {
                        #region FULL BLOCK

                        int currAccess = (prevChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED && prevCallAccess == StatusConstants.TYPE_ACCESS_BLOCKED && prevFeedAccess == StatusConstants.TYPE_ACCESS_BLOCKED) ? StatusConstants.TYPE_ACCESS_UNBLOCKED : StatusConstants.TYPE_ACCESS_BLOCKED;

                        if (showAlert && currAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
                        {
                            //MessageBoxResult result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.FRIEND_BLOCK_WARNING, friendInfoModel.ShortInfoModel.FullName));
                            //if (result != MessageBoxResult.Yes)
                            //{
                            //    friendInfoModel.IsAccessChanging = false;
                            //    //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                            //    if (onComplete != null) onComplete(false);
                            //    return;
                            //}
                            WNConfirmationView cv = new WNConfirmationView("Block confirmation", String.Format(NotificationMessages.FRIEND_BLOCK_WARNING, friendInfoModel.ShortInfoModel.FullName), CustomConfirmationDialogButtonOptions.YesNo);
                            var result = cv.ShowCustomDialog();
                            if (result == ConfirmationDialogResult.Yes)
                            {
                                friendInfoModel.IsAccessChanging = false;
                                if (onComplete != null) onComplete(false);
                                return;
                            }
                        }

                        friendInfoModel.ChatAccess = currAccess;
                        friendInfoModel.CallAccess = currAccess;
                        friendInfoModel.FeedAccess = currAccess;

                        ThreadBlockUnblockFriend requester = new ThreadBlockUnblockFriend(friendInfoModel.ShortInfoModel.UserTableID, friendInfoModel.ChatAccess);
                        requester.OnComplete += (status, errorMsg) =>
                        {
                            if (status)
                            {
                                BaseFriendInformation registerInfo = ChatService.GetFriendRegisterInfo(friendInfoModel.ShortInfoModel.UserTableID);
                                if (friendInfoModel.ChatAccess == StatusConstants.TYPE_ACCESS_UNBLOCKED || (registerInfo != null && registerInfo.Presence == (int)OnlineStatus.ONLINE))
                                {
                                    ChatService.BlockUnblockFriend(currAccess == StatusConstants.TYPE_ACCESS_BLOCKED, friendInfoModel.ShortInfoModel.UserTableID, (args) =>
                                    {
                                        if (UCMiddlePanelSwitcher.View_UCAddFriendMainPanel != null && UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance != null)
                                        {
                                            UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance.AddRemoveEffectIntoBlockedContactUI(friendInfoModel);
                                        }

                                        friendInfoModel.IsAccessChanging = false;
                                        //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                                        if (onComplete != null) onComplete(true);

                                    });
                                }
                                else
                                {
                                    if (UCMiddlePanelSwitcher.View_UCAddFriendMainPanel != null && UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance != null)
                                    {
                                        UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance.AddRemoveEffectIntoBlockedContactUI(friendInfoModel);
                                    }

                                    friendInfoModel.IsAccessChanging = false;
                                    //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                                    if (onComplete != null) onComplete(true);
                                }
                            }
                            else
                            {
                                friendInfoModel.ChatAccess = prevChatAccess;
                                friendInfoModel.CallAccess = prevCallAccess;
                                friendInfoModel.FeedAccess = prevFeedAccess;

                                friendInfoModel.IsAccessChanging = false;
                                //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                                if (onComplete != null) onComplete(false);

                                if (showAlert)
                                    UIHelperMethods.ShowFailed("Failed to change block status!");
                                //   CustomMessageBox.ShowError("Failed to change block status!");
                            }
                        };
                        requester.Start();

                        #endregion FULL BLOCK
                    }
                    else
                    {
                        #region ACCESS CHANGE

                        string errorType = String.Empty;
                        SettingsDTO settingsDTO = new SettingsDTO();
                        settingsDTO.SettingsName = accessType;

                        if (accessType == StatusConstants.CHAT_ACCESS)
                        {
                            int currAccess = prevChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED ? StatusConstants.TYPE_ACCESS_UNBLOCKED : StatusConstants.TYPE_ACCESS_BLOCKED;

                            if (showAlert && currAccess == StatusConstants.TYPE_ACCESS_BLOCKED)
                            {
                                //MessageBoxResult result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.FRIEND_ACCESS_PREVENT_WARNING, friendInfoModel.ShortInfoModel.FullName, "Chat"));
                                //if (result != MessageBoxResult.Yes)
                                //{
                                //    friendInfoModel.IsAccessChanging = false;
                                //    //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                                //    if (onComplete != null) onComplete(false);
                                //    return;
                                //}

                                WNConfirmationView cv = new WNConfirmationView("Access change confirmation", String.Format(NotificationMessages.FRIEND_BLOCK_WARNING, friendInfoModel.ShortInfoModel.FullName), CustomConfirmationDialogButtonOptions.OKCancel, String.Format(NotificationMessages.BLOCKING_WILL_PREVENT_ACCESS, "Chat"));
                                var result = cv.ShowCustomDialog();
                                if (result == ConfirmationDialogResult.OK)
                                {
                                    friendInfoModel.IsAccessChanging = false;
                                    if (onComplete != null) onComplete(false);
                                    return;
                                }
                            }

                            friendInfoModel.ChatAccess = currAccess;
                            settingsDTO.SettingsValue = currAccess;
                            errorType = "chat access";
                        }
                        else if (accessType == StatusConstants.CALL_ACCESS)
                        {
                            int currAccess = prevCallAccess == StatusConstants.TYPE_ACCESS_BLOCKED ? StatusConstants.TYPE_ACCESS_UNBLOCKED : StatusConstants.TYPE_ACCESS_BLOCKED;
                            friendInfoModel.CallAccess = currAccess;
                            settingsDTO.SettingsValue = currAccess;
                            errorType = "call access";
                        }
                        else
                        {
                            int currAccess = prevFeedAccess == StatusConstants.TYPE_ACCESS_BLOCKED ? StatusConstants.TYPE_ACCESS_UNBLOCKED : StatusConstants.TYPE_ACCESS_BLOCKED;
                            friendInfoModel.FeedAccess = currAccess;
                            settingsDTO.SettingsValue = currAccess;
                            errorType = "feed access";
                        }

                        ThreadChangeFriendAccess requester = new ThreadChangeFriendAccess(friendInfoModel.ShortInfoModel.UserTableID, settingsDTO);
                        requester.OnComplete += (settings, status, errorMsg) =>
                        {
                            if (status)
                            {
                                BaseFriendInformation registerInfo = settings.SettingsName == StatusConstants.CHAT_ACCESS ? ChatService.GetFriendRegisterInfo(friendInfoModel.ShortInfoModel.UserTableID) : null;
                                if (settings.SettingsName == StatusConstants.CHAT_ACCESS && (friendInfoModel.ChatAccess == StatusConstants.TYPE_ACCESS_UNBLOCKED || (registerInfo != null && registerInfo.Presence == (int)OnlineStatus.ONLINE)))
                                {
                                    ChatService.BlockUnblockFriend(settings.SettingsValue == StatusConstants.TYPE_ACCESS_BLOCKED, friendInfoModel.ShortInfoModel.UserTableID, (args) =>
                                    {
                                        if (UCMiddlePanelSwitcher.View_UCAddFriendMainPanel != null && UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance != null)
                                        {
                                            UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance.AddRemoveEffectIntoBlockedContactUI(friendInfoModel);
                                        }

                                        friendInfoModel.IsAccessChanging = false;
                                        //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                                        if (onComplete != null) onComplete(true);
                                    });
                                }
                                else
                                {
                                    if (UCMiddlePanelSwitcher.View_UCAddFriendMainPanel != null && UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance != null)
                                    {
                                        UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance.AddRemoveEffectIntoBlockedContactUI(friendInfoModel);
                                    }

                                    friendInfoModel.IsAccessChanging = false;
                                    //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                                    if (onComplete != null) onComplete(true);
                                }
                            }
                            else
                            {
                                friendInfoModel.ChatAccess = prevChatAccess;
                                friendInfoModel.CallAccess = prevCallAccess;
                                friendInfoModel.FeedAccess = prevFeedAccess;

                                friendInfoModel.IsAccessChanging = false;
                                //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                                if (onComplete != null) onComplete(false);

                                if (showAlert)
                                    UIHelperMethods.ShowFailed("Failed to change " + errorType + "!", "Change block/unblock");
                                //CustomMessageBox.ShowError("Failed to change " + errorType + "!");
                            }
                        };
                        requester.Start();

                        #endregion ACCESS CHANGE
                    }

                    #endregion FRIEND BLOCK
                }
                else if (accessType == StatusConstants.FULL_ACCESS)
                {
                    #region NON FRIEND BLOCK

                    bool prevBlockByMe = blockModel.IsBlockedByMe;

                    if (showAlert && !prevBlockByMe)
                    {
                        //MessageBoxResult result;
                        //if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
                        //{
                        //    result = CustomMessageBox.ShowQuestion(String.Format(NotificationMessages.NON_FRIEND_OUTGOING_BLOCK_WARNING, friendInfoModel.ShortInfoModel.FullName));
                        //}
                        //else
                        //{
                        //    result = CustomMessageBox.ShowQuestion(String.Format(NotificationMessages.NON_FRIEND_BLOCK_WARNING, friendInfoModel.ShortInfoModel.FullName));
                        //}

                        //if (result != MessageBoxResult.Yes)
                        //{
                        //    friendInfoModel.IsAccessChanging = false;
                        //    //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                        //    if (onComplete != null) onComplete(false);
                        //    return;
                        //}
                        string dialog = String.Format(NotificationMessages.NON_FRIEND_BLOCK_WARNING, friendInfoModel.ShortInfoModel.FullName);
                        string willCanceloutgoingRequest = string.Empty;
                        if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
                            willCanceloutgoingRequest = NotificationMessages.BLOCKING_CANCELLED_OUTGOING_REQUEST;
                        WNConfirmationView cv = new WNConfirmationView("Block confirmation", dialog, CustomConfirmationDialogButtonOptions.OKCancel, willCanceloutgoingRequest);
                        var result = cv.ShowCustomDialog();
                        if (result != ConfirmationDialogResult.OK)
                        {
                            friendInfoModel.IsAccessChanging = false;
                            if (onComplete != null) onComplete(false);
                            return;
                        }
                    }

                    blockModel.IsBlockedByMe = !prevBlockByMe;

                    //ChatService.StartFriendChat(friendInfoModel.ShortInfoModel.UserTableID, friendInfoModel, blockModel, (e) =>
                    //{
                    //    if (e.Status)
                    //    {
                    ChatService.BlockUnblockFriend(blockModel.IsBlockedByMe, friendInfoModel.ShortInfoModel.UserTableID, (args) =>
                    {
                        if (args.Status)
                        {
                            BlockedNonFriendDAO.SaveBlockedNonFriend(friendInfoModel.ShortInfoModel.UserTableID, blockModel.IsBlockedByMe, blockModel.IsBlockedByFriend);

                            if (UCMiddlePanelSwitcher.View_UCAddFriendMainPanel != null && UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance != null)
                            {
                                UCMiddlePanelSwitcher.View_UCAddFriendMainPanel._BlockedContactsInstance.AddRemoveEffectIntoBlockedContactUI(friendInfoModel);
                            }

                            if (blockModel.IsBlockedByMe && friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
                            {
                                (new DeleteFriend()).StartProcess(friendInfoModel); ;
                            }

                            friendInfoModel.IsAccessChanging = false;
                            //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                            if (onComplete != null) onComplete(true);
                        }
                        else
                        {
                            blockModel.IsBlockedByMe = prevBlockByMe;

                            friendInfoModel.IsAccessChanging = false;
                            //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                            if (onComplete != null) onComplete(false);

                            if (showAlert)
                                UIHelperMethods.ShowFailed("Failed to change block status!", "Change block");
                            //CustomMessageBox.ShowError("Failed to change block status!");
                        }
                    });
                    //}
                    //else
                    //{
                    //    blockModel.IsBlockedByMe = prevBlockByMe;

                    //    friendInfoModel.IsAccessChanging = false;
                    //    //if (accessChangeCommand != null) ((RelayCommand)accessChangeCommand).RaiseCanExecuteChanged();
                    //    if (onComplete != null) onComplete(false);

                    //    if (showAlert) CustomMessageBox.ShowError("Failed to change block status!");
                    //}
                    //}, false, false);

                    #endregion NON FRIEND BLOCK
                }
                else
                {
                    friendInfoModel.IsAccessChanging = true;
                    if (onComplete != null) onComplete(false);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeBlockUnblockValue() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion "Block/Unblock Methods"
    }
}

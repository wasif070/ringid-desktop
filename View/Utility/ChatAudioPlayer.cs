﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using View.BindingModels;
using View.Utility.Chat;

namespace View.Utility
{
    public class ChatAudioPlayer
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatMediaElement).Name);
        private static object _Lock = new object();
        public static ChatAudioPlayer _Instance = null;
        private MessageModel _Model;
        private string _PacketID;
        private MediaPlayer _Player;
        private CustomizeTimer _PlayerProgressTimer;

        public ChatAudioPlayer()
        {
            _Player = new MediaPlayer();
            _PlayerProgressTimer = new CustomizeTimer();
        }

        public void Play(MessageModel model, string filePath)
        {
            try
            {
                lock (_Lock)
                {
                    this._Model = model;
                    this._PacketID = model.PacketID;
                    this._Player.Open(new Uri(filePath));
                    this._Player.Volume = 1;
                    this._Player.Position = TimeSpan.FromSeconds((double)(this._Model.MultiMediaState == StatusConstants.MEDIA_PAUSE_STATE ? this._Model.OffsetPosition : 0));
                    this._Model.MultiMediaState = StatusConstants.MEDIA_PLAY_STATE;
                    this._Player.MediaOpened += ChatAudioPlayer_MediaOpened;
                    this._PlayerProgressTimer.IntervalInSecond = 1;
                    this._PlayerProgressTimer.Tick += PlayerProgressTimer_Tick;
                    this._Player.Play();
                    ChatHelpers.SendViewPlayedSatatus(this._Model);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Play() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Complete()
        {
            try
            {
                this._PacketID = null;
                this._PlayerProgressTimer.Tick -= PlayerProgressTimer_Tick;
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    this._Player.MediaOpened -= ChatAudioPlayer_MediaOpened;
                    this._Player.Stop();
                    this._Player.Close();
                    GC.SuppressFinalize(this._Player);
                }, System.Windows.Threading.DispatcherPriority.Send);
                this._PlayerProgressTimer.Stop();
                this._PlayerProgressTimer = null;
                if (this._Model != null)
                {
                    this._Model.MultiMediaState = StatusConstants.MEDIA_INIT_STATE;
                }
                this._Model = null;
                _Instance = null;
            }
            catch (Exception ex)
            {
                log.Error("Error in Complete() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Stop()
        {
            try
            {
                lock (_Lock)
                {
                    if (this._Model != null && this._Model.FromFriend && this._Model.IsSecretChat)
                    {
                        this.Complete();
                        return;
                    }

                    this._PacketID = null;
                    this._PlayerProgressTimer.Tick -= PlayerProgressTimer_Tick;
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        this._Player.MediaOpened -= ChatAudioPlayer_MediaOpened;
                        this._Player.Stop();
                        this._Player.Close();
                        GC.SuppressFinalize(this._Player);
                        this._Player = null;
                    }, System.Windows.Threading.DispatcherPriority.Send);
                    this._PlayerProgressTimer.Stop();
                    this._PlayerProgressTimer = null;
                    if (this._Model != null)
                    {
                        this._Model.MultiMediaState = StatusConstants.MEDIA_PAUSE_STATE;
                    }
                    this._Model = null;
                    _Instance = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Stop() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void ChatAudioPlayer_MediaOpened(object sender, EventArgs e)
        {
            this._Player.MediaOpened -= ChatAudioPlayer_MediaOpened;
            this._PlayerProgressTimer.Start();
        }

        private void PlayerProgressTimer_Tick(int counter, bool initTick = false, object state = null)
        {
            try
            {
                if (initTick) return;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    lock (_Lock)
                    {
                        if (this._Player != null)
                        {
                            bool hasTimeSpan = this._Player.NaturalDuration.HasTimeSpan;
                            if (hasTimeSpan)
                            {
                                TimeSpan tempTimeSpan = this._Player.Position;
                                double diff = Math.Ceiling(1000 - (tempTimeSpan.TotalMilliseconds % 1000));
                                TimeSpan OffsetTimeSpan = diff < 100 ? tempTimeSpan.Add(TimeSpan.FromMilliseconds(diff)) : tempTimeSpan;

                                if (this._Model != null)
                                {
                                    this._Model.OffsetPosition = (int)OffsetTimeSpan.TotalSeconds;
                                    if (this._Model.OffsetPosition >= this._Model.Duration)
                                    {
                                        this.Complete();
                                    }
                                }
                            }
                        }
                    }
                }, System.Windows.Threading.DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: PlayerProgressTimer_Tick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static ChatAudioPlayer Instance
        {
            get
            {
                lock (_Lock)
                {
                    if (_Instance == null) { _Instance = new ChatAudioPlayer(); }
                }
                return _Instance;
            }
        }

        public static int MultiMediaState
        {
            get { return _Instance != null && _Instance._Model != null ? _Instance._Model.MultiMediaState : StatusConstants.MEDIA_INIT_STATE; }
        }

        public static string PacketID
        {
            get { return _Instance != null && _Instance._PacketID != null ? _Instance._PacketID : String.Empty; }
        }

    }
}

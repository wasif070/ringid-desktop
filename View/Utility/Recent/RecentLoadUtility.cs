﻿using Models.Constants;
using Models.Entity;
using Models.Utility;
using Models.Stores;
using View.Utility;
using View.BindingModels;
using View.UI.FriendList;
using View.ViewModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using View.Dictonary;
using View.UI.Profile.FriendProfile;
using System.Collections.ObjectModel;
using System.Windows;
using View.UI.Chat;
using View.UI.Group;
using log4net;
using Auth.utility;
using System.Windows.Threading;
using View.Utility.Chat;
using imsdkwrapper;
using View.UI.Room;
using View.UI.Call;

namespace View.Utility.Recent
{
    class RecentLoadUtility : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RecentLoadUtility).Name);

        private static readonly object _LoadSyncRoot = new object();
        private static RecentLoadUtility _Instance = null;
        private ConcurrentQueue<RecentDTO> _RecentQueue = new ConcurrentQueue<RecentDTO>();
        private ConcurrentQueue<RecentDTO> _ProcessQueue = new ConcurrentQueue<RecentDTO>();

        public RecentLoadUtility()
        {
            _Instance = this;
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += FriendListLoad_DowWork;
            ProgressChanged += FriendListLoad_ProgressChanged;
            RunWorkerCompleted += FriendListLoad_RunWorkerCompleted;
        }

        public static void LoadRecentData(List<RecentDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                foreach (RecentDTO dto in list)
                {
                    RecentLoadUtility.LoadData(dto);
                }
            }
        }

        public static void LoadRecentData(RecentDTO list)
        {
            RecentLoadUtility.LoadData(list);
        }

        private static void LoadData(RecentDTO recentDTO)
        {
            try
            {
                lock (_LoadSyncRoot)
                {
                    if (RecentLoadUtility._Instance == null)
                    {

                        RecentLoadUtility._Instance = new RecentLoadUtility();
                        RecentLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        RecentLoadUtility._Instance._ProcessQueue.Enqueue(recentDTO);
                        RecentLoadUtility._Instance.Start();
                    }
                    else
                    {

                        RecentLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        RecentLoadUtility._Instance._ProcessQueue.Enqueue(recentDTO);
                        if (RecentLoadUtility._Instance.IsBusy == false)
                        {
                            RecentLoadUtility._Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadShowMoreData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Start()
        {
            RunWorkerAsync();
        }

        private void FriendListLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                RecentDTO recentDTO = null;
                while (_RecentQueue.TryDequeue(out recentDTO))
                {
                    //System.Threading.Thread.Sleep(1);
                    Process(recentDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("GroupListLoad_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            e.Result = true;
        }

        private void FriendListLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void FriendListLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_RecentQueue.Count > 0)
            {
                if (RecentLoadUtility._Instance.IsBusy == false)
                {
                    RecentLoadUtility._Instance.Start();
                }
            }
        }

        private void Process(RecentDTO newDTO)
        {
            try
            {
                if (newDTO.Message != null)
                {
                    newDTO.UniqueKey = newDTO.Message.PacketID;
                    newDTO.Time = newDTO.Message.MessageDate;
                    if (newDTO.FriendTableID > 0)
                    {
                        newDTO.ContactType = ChatConstants.TYPE_FRIEND;
                        newDTO.Type = ChatConstants.SUBTYPE_FRIEND_CHAT;
                    }
                    else if (newDTO.GroupID > 0)
                    {
                        newDTO.ContactType = ChatConstants.TYPE_GROUP;
                        newDTO.Type = ChatConstants.SUBTYPE_GROUP_CHAT;
                    }
                    else if (!String.IsNullOrWhiteSpace(newDTO.RoomID))
                    {
                        newDTO.ContactType = ChatConstants.TYPE_ROOM;
                        newDTO.Type = ChatConstants.SUBTYPE_ROOM_CHAT;
                    }
                }
                else if (newDTO.CallLog != null)
                {
                    newDTO.UniqueKey = newDTO.CallLog.CallID;
                    newDTO.Time = newDTO.CallLog.CallingTime;
                    newDTO.ContactType = ChatConstants.TYPE_FRIEND;
                    newDTO.Type = ChatConstants.SUBTYPE_CALL_LOG;
                }
                else if (newDTO.Activity != null)
                {
                    newDTO.UniqueKey = newDTO.Activity.PacketID;
                    newDTO.Time = newDTO.Activity.UpdateTime;
                    newDTO.ContactType = ChatConstants.TYPE_GROUP;
                    newDTO.Type = ChatConstants.SUBTYPE_GROUP_ACTIVITY;
                }

                if (newDTO.ContactType == ChatConstants.TYPE_FRIEND)
                {
                    UCFriendChatCallPanel friendCallChatPanel = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(newDTO.FriendTableID);
                    if (friendCallChatPanel != null)
                    {
                        ObservableCollection<RecentModel> recentModelList = RingIDViewModel.Instance.GetRecentModelListByID(newDTO.FriendTableID);
                        UserShortInfoModel friendInfoModel = newDTO.Message != null && newDTO.Message.SenderTableID == DefaultSettings.LOGIN_TABLE_ID ? RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel : friendCallChatPanel.FriendBasicInfoModel.ShortInfoModel;
                        AddRecentModel(newDTO, friendInfoModel, friendCallChatPanel.IsDeletePanelVisible);
                        if (newDTO.IsMoveToButtom) friendCallChatPanel.ChatScrollViewer.MoveToBottom();
                    }
                }
                else if (newDTO.ContactType == ChatConstants.TYPE_GROUP)
                {
                    UCGroupChatCallPanel groupCallChatPanel = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(newDTO.GroupID);
                    if (groupCallChatPanel != null)
                    {
                        AddRecentModel(newDTO, null, groupCallChatPanel.IsDeletePanelVisible);
                        if (newDTO.IsMoveToButtom) groupCallChatPanel.ChatScrollViewer.MoveToBottom();
                    }
                }

                else if (newDTO.ContactType == ChatConstants.TYPE_ROOM)
                {
                    UCRoomChatCallPanel roomCallChatPanel = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(newDTO.RoomID);
                    if (roomCallChatPanel != null && roomCallChatPanel.IsVisible)
                    {
                        AddRecentModel(newDTO);
                        if (newDTO.IsMoveToButtom) roomCallChatPanel.ChatScrollViewer.MoveToBottom();
                    }
                }

                RecentDTO dequeue = null;
                _ProcessQueue.TryDequeue(out dequeue);
            }
            catch (Exception ex)
            {
                RecentDTO dequeue = null;
                _ProcessQueue.TryDequeue(out dequeue);

                log.Error("StartProcess() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void CloseRunningResources(MessageModel messageModel)
        {
            try
            {
                switch ((MessageType)messageModel.MessageType)
                {
                    case MessageType.AUDIO_FILE:
                        if (ChatAudioPlayer.PacketID.Equals(messageModel.PacketID))
                        {
                            ChatAudioPlayer.Instance.Stop();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("CloseRunningResources() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static bool IsProcessed(List<RecentDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                foreach (RecentDTO obj in list)
                {
                    if (RecentLoadUtility._Instance._ProcessQueue.Contains(obj))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static void RemoveRangeByMessageType(object id, List<string> uniqueKeys, bool fromHistoryOrLog = false)
        {
            string contactID = id.ToString();
            if (uniqueKeys != null && uniqueKeys.Count > 0)
            {
                foreach (string uniqueKey in uniqueKeys)
                {
                    RemoveByMessageType(contactID, uniqueKey, true);
                }
            }

            if (fromHistoryOrLog == false)
            {
                ChatHelpers.RemoveUnreadChat(contactID, uniqueKeys);

                if (UCChatLogPanel.Instance != null)
                {
                    UCChatLogPanel.Instance.LoadChatLogByContactID(contactID);
                }
            }

            ChatHelpers.UpdateStatusByRemove(id, uniqueKeys);
        }

        public static void RemoveByMessageType(object id, string uniqueKey, bool fromRemoveRangeByMessageType = false)
        {
            try
            {
                string contactID = id.ToString();
                ObservableCollection<RecentModel> recentModelList = RingIDViewModel.Instance.GetRecentModelListByID(contactID);
                RecentModel model = !String.IsNullOrWhiteSpace(uniqueKey) ? recentModelList.Where(P => P.Message != null && P.UniqueKey == uniqueKey).FirstOrDefault() : null;
                if (model != null)
                {
                    CloseRunningResources(model.Message);
                    model.Message.IsUnread = false;
                    model.Message.MessageType = (int)MessageType.DELETE_MESSAGE;
                    model.Message.ViewModel.LoadViewType(model.Message);
                    model.Message.Status = ChatConstants.STATUS_DELETED;
                }

                if (fromRemoveRangeByMessageType == false)
                {
                    ChatHelpers.RemoveUnreadChat(contactID, uniqueKey);

                    if (UCChatLogPanel.Instance != null)
                    {
                        UCChatLogPanel.Instance.LoadChatLogByContactID(contactID);
                    }

                    ChatHelpers.UpdateStatusByRemove(id, uniqueKey);
                }
            }
            catch (Exception ex)
            {
                log.Error("Remove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void RemoveRange(object id, List<string> uniqueKeys, bool fromHistoryOrLog = false)
        {
            string contactID = id.ToString();
            if (uniqueKeys != null && uniqueKeys.Count > 0)
            {
                foreach (string uniqueKey in uniqueKeys)
                {
                    Remove(contactID, uniqueKey, true);
                }
            }

            if (fromHistoryOrLog == false)
            {
                ChatHelpers.RemoveUnreadChat(contactID, uniqueKeys);
                if (UCChatLogPanel.Instance != null)
                {
                    UCChatLogPanel.Instance.LoadChatLogByContactID(contactID);
                }

                ChatHelpers.RemoveUnreadCall(contactID, uniqueKeys);
                if (UCCallLog.Instance != null)
                {
                    CallLogLoadUtility.RemoveRange(contactID, uniqueKeys);
                }
            }

            ChatHelpers.UpdateStatusByRemove(id, uniqueKeys);
        }

        public static void Remove(object id, string uniqueKey, bool fromRemoveRange = false)
        {
            try
            {
                string contactID = id.ToString();
                ObservableCollection<RecentModel> recentModelList = RingIDViewModel.Instance.GetRecentModelListByID(contactID);
                RecentModel model = !String.IsNullOrWhiteSpace(uniqueKey) ? recentModelList.Where(P => P.UniqueKey == uniqueKey).FirstOrDefault() : null;
                if (model != null)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        if (model.Message != null)
                        {
                            CloseRunningResources(model.Message);
                            model.Message.IsUnread = false;
                            model.Message.MessageType = (int)MessageType.DELETE_MESSAGE;
                            model.Message.Status = ChatConstants.STATUS_DELETED;
                        }

                        lock (recentModelList)
                        {
                            int index = recentModelList.IndexOf(model);
                            recentModelList.RemoveAt(index);
                            SetNextIdentity(recentModelList, model, index, true);
                            RecentModel prevModel = index > 0 ? recentModelList.ElementAt(index - 1) : null;

                            if (prevModel != null && prevModel.Type == ChatConstants.SUBTYPE_DATE_TITLE)
                            {
                                if (index < recentModelList.Count)
                                {
                                    RecentModel nextModel = recentModelList.ElementAt(index);
                                    if (!prevModel.DateTime.IsSameDateOf(nextModel.DateTime))
                                    {
                                        recentModelList.RemoveAt(index - 1);
                                    }
                                }
                                else
                                {
                                    recentModelList.RemoveAt(index - 1);
                                }
                            }
                        }
                    }, DispatcherPriority.Send);

                    GC.SuppressFinalize(model);
                }

                if (fromRemoveRange == false)
                {
                    if (model != null)
                    {
                        if (model.Message != null)
                        {
                            ChatHelpers.RemoveUnreadChat(contactID, uniqueKey);
                            if (UCChatLogPanel.Instance != null)
                            {
                                UCChatLogPanel.Instance.LoadChatLogByContactID(contactID);
                            }
                            ChatHelpers.UpdateStatusByRemove(id, uniqueKey);
                        }
                        else if (model.CallLog != null)
                        {
                            ChatHelpers.RemoveUnreadCall(contactID, uniqueKey);
                            if (UCCallLog.Instance != null)
                            {
                                CallLogLoadUtility.Remove(contactID, uniqueKey);
                            }
                        }
                    }
                    else
                    {
                        ChatHelpers.RemoveUnreadChat(contactID, uniqueKey);
                        ChatHelpers.RemoveUnreadCall(contactID, uniqueKey);
                        ChatHelpers.UpdateStatusByRemove(id, uniqueKey);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Remove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private static void AddRecentModel(RecentDTO newDTO, UserShortInfoModel friendInfoModel = null, bool isCheckedVisible = false)
        {
            try
            {
                ObservableCollection<RecentModel> recentModelList = RingIDViewModel.Instance.GetRecentModelListByID(newDTO.ContactID);
                RecentModel newModel = recentModelList.Where(P => P.UniqueKey == newDTO.UniqueKey).FirstOrDefault();
                if (newModel == null)
                {
                    newModel = new RecentModel(newDTO, true);
                    newModel.IsCheckedVisible = isCheckedVisible;

                    if (newModel.Type == ChatConstants.SUBTYPE_FRIEND_CHAT || newModel.Type == ChatConstants.SUBTYPE_GROUP_CHAT)
                    {
                        long friendOrGroupID = newModel.Type == ChatConstants.SUBTYPE_FRIEND_CHAT ? newModel.FriendTableID : newModel.GroupID;
                        newModel.Message.FriendInfoModel = friendInfoModel != null ? friendInfoModel : RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(newModel.Message.SenderTableID);
                        newModel.Message.LastStatus = ChatViewModel.Instance.GetLastStatusModel(friendOrGroupID);
                        ChatHelpers.UpdateStatusByInsertOrChange(friendOrGroupID, newModel.Message.PacketID, newModel.Message.MessageType, newModel.Message.MessageDate, newModel.Message.SenderTableID, newModel.Message.Status, newModel.Message.LastStatus);
                    }

                    if (recentModelList.Count > 0)
                    {
                        RecentModel firstModel = recentModelList.First();
                        RecentModel lastModel = recentModelList.Last();

                        if (newModel.Time < firstModel.Time)
                        {
                            int startIndex = 1;
                            recentModelList.InvokeInsert(0, newModel);
                            if (!newModel.DateTime.IsSameDateOf(firstModel.DateTime))
                            {
                                startIndex = 2;
                                recentModelList.InvokeInsert(0, new RecentModel(new RecentDTO { ContactID = newDTO.ContactID, FriendTableID = newDTO.FriendTableID, GroupID = newDTO.GroupID, RoomID = newDTO.RoomID, Time = ModelUtility.GetStartTimeMillisLocal(newModel.DateTime) }));
                            }
                            SetNextIdentity(recentModelList, newModel, startIndex, false);
                        }
                        else if (newModel.Time > lastModel.Time)
                        {
                            int startIndex = 2;
                            if (!newModel.DateTime.IsSameDateOf(lastModel.DateTime))
                            {
                                startIndex = 3;
                                recentModelList.InvokeAdd(new RecentModel(new RecentDTO { ContactID = newDTO.ContactID, FriendTableID = newDTO.FriendTableID, GroupID = newDTO.GroupID, RoomID = newDTO.RoomID, Time = ModelUtility.GetStartTimeMillisLocal(newModel.DateTime) }));
                            }
                            recentModelList.InvokeAdd(newModel);
                            SetPreviousIdentity(recentModelList, newModel, recentModelList.Count - startIndex);
                        }
                        else if (newModel.DateTime.IsSameDateOf(lastModel.DateTime))
                        {
                            int index = recentModelList.Count - 1;
                            for (; index >= 0; index--)
                            {
                                RecentModel temp = recentModelList.ElementAt(index);
                                if (newModel.Time > temp.Time)
                                {
                                    index++;
                                    break;
                                }
                            }

                            if (index < 0)
                            {
                                index = 1;
                            }

                            recentModelList.InvokeInsert(index, newModel);
                            SetPreviousIdentity(recentModelList, newModel, index - 1);
                            SetNextIdentity(recentModelList, newModel, index + 1, false);
                        }
                        else if (newModel.DateTime.IsSameDateOf(firstModel.DateTime))
                        {
                            int index = 0;
                            for (; index < recentModelList.Count; index++)
                            {
                                RecentModel temp = recentModelList.ElementAt(index);
                                if (newModel.Time < temp.Time)
                                    break;
                            }
                            recentModelList.InvokeInsert(index, newModel);
                            SetPreviousIdentity(recentModelList, newModel, index - 1);
                            SetNextIdentity(recentModelList, newModel, index + 1, false);
                        }
                        else
                        {
                            int max = recentModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                long pivotTime = recentModelList.ElementAt(pivot).Time;
                                if (newModel.Time > pivotTime)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            recentModelList.InvokeInsert(min, newModel);
                            if (min == 0 || !newModel.DateTime.IsSameDateOf(recentModelList.ElementAt(min - 1).DateTime))
                            {
                                recentModelList.InvokeInsert(min, new RecentModel(new RecentDTO { ContactID = newDTO.ContactID, FriendTableID = newDTO.FriendTableID, GroupID = newDTO.GroupID, RoomID = newDTO.RoomID, Time = ModelUtility.GetStartTimeMillisLocal(newModel.DateTime) }));
                                min = min + 1;
                            }
                            SetPreviousIdentity(recentModelList, newModel, min - 1);
                            SetNextIdentity(recentModelList, newModel, min + 1, false);
                        }
                    }
                    else
                    {
                        recentModelList.InvokeAdd(new RecentModel(new RecentDTO { ContactID = newDTO.ContactID, FriendTableID = newDTO.FriendTableID, GroupID = newDTO.GroupID, RoomID = newDTO.RoomID, Time = ModelUtility.GetStartTimeMillisLocal(newModel.DateTime) }));
                        recentModelList.InvokeAdd(newModel);
                    }
                }
                else
                {
                    newModel.IsCheckedVisible = isCheckedVisible;

                    long prevTime = newModel.Time;
                    long currTime = newDTO.Time;
                    if (prevTime == currTime)
                    {
                        newModel.LoadData(newDTO);
                        return;
                    }

                    //REVALIDATE IN PREVIOUS LOCATION MODEL
                    int indexOf = recentModelList.IndexOf(newModel);
                    SetNextIdentity(recentModelList, newModel, indexOf + 1, true, true);
                    RecentModel prevModel = indexOf > 0 ? recentModelList.ElementAt(indexOf - 1) : null;
                    if (prevModel != null && prevModel.Type == ChatConstants.SUBTYPE_DATE_TITLE)
                    {
                        if (indexOf + 1 < recentModelList.Count)
                        {
                            RecentModel nextModel = recentModelList.ElementAt(indexOf + 1);
                            if (!prevModel.DateTime.IsSameDateOf(nextModel.DateTime))
                            {
                                recentModelList.InvokeRemoveAt(indexOf - 1);
                            }
                        }
                        else
                        {
                            recentModelList.InvokeRemoveAt(indexOf - 1);
                        }
                    }

                    indexOf = recentModelList.IndexOf(newModel);
                    int max = currTime > prevTime ? recentModelList.Count : indexOf;
                    int min = currTime > prevTime ? indexOf : 0;
                    int pivot;
                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        long pivotTime = recentModelList.ElementAt(pivot).Time;
                        if (currTime > pivotTime)
                        {
                            min = pivot + 1;
                        }
                        else
                        {
                            max = pivot;
                        }
                    }

                    newModel.LoadData(newDTO);
                    if ((newModel.Type == ChatConstants.SUBTYPE_FRIEND_CHAT || newModel.Type == ChatConstants.SUBTYPE_GROUP_CHAT) && newModel.Message.FriendInfoModel == null)
                    {
                        newModel.Message.FriendInfoModel = friendInfoModel != null ? friendInfoModel : RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(newModel.Message.SenderTableID);
                    }

                    //REVALIDATE IN NEW LOCATION MODEL
                    min = min > indexOf ? min - 1 : min;
                    recentModelList.InvokeMove(indexOf, min);
                    if (min == 0 || !newModel.DateTime.IsSameDateOf(recentModelList.ElementAt(min - 1).DateTime))
                    {
                        recentModelList.InvokeInsert(min, new RecentModel(new RecentDTO { ContactID = newDTO.ContactID, FriendTableID = newDTO.FriendTableID, GroupID = newDTO.GroupID, RoomID = newDTO.RoomID, Time = ModelUtility.GetStartTimeMillisLocal(newModel.DateTime) }));
                        min = min + 1;
                    }
                    SetPreviousIdentity(recentModelList, newModel, min - 1, true);
                    SetNextIdentity(recentModelList, newModel, min + 1, false, true);
                }

            }
            catch (Exception ex)
            {
                log.Error("AddRecentModel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private static void SetPreviousIdentity(ObservableCollection<RecentModel> recentModelList, RecentModel newModel, int index, bool forceTrigger = false)
        {
            try
            {
                if (newModel.Type == ChatConstants.SUBTYPE_FRIEND_CHAT || newModel.Type == ChatConstants.SUBTYPE_GROUP_CHAT || newModel.Type == ChatConstants.SUBTYPE_ROOM_CHAT)
                {
                    if (forceTrigger)
                    {
                        newModel.Message.ViewModel.PrevIdentity = 0;
                        newModel.Message.ViewModel.IsSamePrevAndCurrID = false;
                    }

                    for (int i = index; i >= 0; i--)
                    {
                        RecentModel temp = recentModelList[i];
                        if (temp.Type == ChatConstants.SUBTYPE_FRIEND_CHAT || temp.Type == ChatConstants.SUBTYPE_GROUP_CHAT || temp.Type == ChatConstants.SUBTYPE_ROOM_CHAT)
                        {
                            newModel.Message.ViewModel.PrevIdentity = temp.Message.SenderTableID;
                            newModel.Message.ViewModel.IsSamePrevAndCurrID = newModel.Message.SenderTableID == newModel.Message.ViewModel.PrevIdentity;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SetPreviousIdentity() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private static void SetNextIdentity(ObservableCollection<RecentModel> recentModelList, RecentModel newModel, int index, bool forRemove, bool forceTrigger = false)
        {
            try
            {
                if (newModel.Type == ChatConstants.SUBTYPE_FRIEND_CHAT || newModel.Type == ChatConstants.SUBTYPE_GROUP_CHAT || newModel.Type == ChatConstants.SUBTYPE_ROOM_CHAT)
                {
                    for (int i = index; i < recentModelList.Count; i++)
                    {
                        RecentModel temp = recentModelList[i];
                        if (temp.Type == ChatConstants.SUBTYPE_FRIEND_CHAT || temp.Type == ChatConstants.SUBTYPE_GROUP_CHAT || temp.Type == ChatConstants.SUBTYPE_ROOM_CHAT)
                        {
                            if (forceTrigger)
                            {
                                temp.Message.ViewModel.PrevIdentity = 0;
                                temp.Message.ViewModel.IsSamePrevAndCurrID = false;
                            }

                            temp.Message.ViewModel.PrevIdentity = forRemove ? newModel.Message.ViewModel.PrevIdentity : newModel.Message.SenderTableID;
                            temp.Message.ViewModel.IsSamePrevAndCurrID = temp.Message.SenderTableID == temp.Message.ViewModel.PrevIdentity;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SetNextIdentity() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

    }
}

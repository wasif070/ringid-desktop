﻿using Models.Constants;
using Models.Entity;
using View.BindingModels;
using View.ViewModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using View.UI.Chat;
using log4net;
using View.UI.PopUp;
using Models.DAO;
using View.Utility.Chat;

namespace View.Utility.Recent
{
    class ChatLogLoadUtility : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatLogLoadUtility).Name);

        private static readonly object _LoadSyncRoot = new object();
        private static ChatLogLoadUtility _Instance = null;
        private ConcurrentQueue<RecentDTO> _RecentQueue = new ConcurrentQueue<RecentDTO>();

        public ChatLogLoadUtility()
        {
            _Instance = this;
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += FriendListLoad_DowWork;
            ProgressChanged += FriendListLoad_ProgressChanged;
            RunWorkerCompleted += FriendListLoad_RunWorkerCompleted;
        }

        public static void LoadRecentData(List<RecentDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                foreach (RecentDTO dto in list)
                {
                    ChatLogLoadUtility.LoadData(dto);
                }
            }
        }

        public static void LoadRecentData(RecentDTO recentDTO)
        {
            ChatLogLoadUtility.LoadData(recentDTO);
        }

        private static void LoadData(RecentDTO recentDTO)
        {
            try
            {
                lock (_LoadSyncRoot)
                {
                    if (ChatLogLoadUtility._Instance == null)
                    {
                        ChatLogLoadUtility._Instance = new ChatLogLoadUtility();
                        ChatLogLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        ChatLogLoadUtility._Instance.Start();
                    }
                    else
                    {
                        ChatLogLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        if (ChatLogLoadUtility._Instance.IsBusy == false)
                        {
                            ChatLogLoadUtility._Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadShowMoreData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Start()
        {
            RunWorkerAsync();
        }

        private void FriendListLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                RecentDTO recentDTO = null;
                while (_RecentQueue.TryDequeue(out recentDTO))
                {
                    //System.Threading.Thread.Sleep(50);
                    if (UCChatLogPanel.Instance != null)
                    {
                        AddRecentModel(recentDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GroupListLoad_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            e.Result = true;
        }

        private void FriendListLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void FriendListLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_RecentQueue.Count > 0)
            {
                if (ChatLogLoadUtility._Instance.IsBusy == false)
                {
                    ChatLogLoadUtility._Instance.Start();
                }
            }
        }

        public static void Remove(long contactID)
        {
            ChatLogLoadUtility.Remove(contactID.ToString());
        }

        public static void Remove(string contactID)
        {
            try
            {
                RecentModel model = RingIDViewModel.Instance.ChatLogModelsList.Where(P => P.ContactID.Equals(contactID)).FirstOrDefault();
                if (model != null)
                {
                    RingIDViewModel.Instance.ChatLogModelsList.InvokeRemove(model);
                }
            }
            catch (Exception ex)
            {
                log.Error("Remove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void Remove(RecentModel model)
        {
            try
            {
                if (model != null)
                {
                    RingIDViewModel.Instance.ChatLogModelsList.InvokeRemove(model);
                }
            }
            catch (Exception ex)
            {
                log.Error("Remove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void RemoveRange(List<string> contactIDs)
        {
            try
            {
                if (contactIDs != null && contactIDs.Count > 0)
                {
                    foreach (string contactID in contactIDs)
                    {
                        Remove(contactID);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("RemoveRange() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void RemoveRange(List<RecentModel> modelList)
        {
            try
            {
                if (modelList != null && modelList.Count > 0)
                {
                    foreach (RecentModel model in modelList)
                    {
                        Remove(model);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("RemoveRange() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private static void AddRecentModel(RecentDTO newDTO)
        {
            try
            {
                RecentModel prevModel = RingIDViewModel.Instance.ChatLogModelsList.Where(P => P.ContactID.Equals(newDTO.ContactID)).FirstOrDefault();
                RecentModel firstModel = RingIDViewModel.Instance.ChatLogModelsList.FirstOrDefault();

                if (prevModel == null)
                {
                    RecentModel newModel = new RecentModel(newDTO);
                    newModel.Message.Message = newModel.Message.Message != null ? newModel.Message.Message.Trim() : String.Empty;
                    newModel.UnreadList = ChatViewModel.Instance.GetUnreadChatByID(newDTO.ContactID);

                    if (newDTO.FriendTableID > 0)
                    {
                        newModel.FriendInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(newDTO.FriendTableID);
                    }
                    else if (newDTO.GroupID > 0)
                    {
                        newModel.GroupInfoModel = RingIDViewModel.Instance.GetGroupInfoModelByGroupID(newDTO.GroupID);
                    }
                    else if (!String.IsNullOrWhiteSpace(newDTO.RoomID))
                    {
                        newModel.RoomModel = RingIDViewModel.Instance.GetRoomModelByRoomID(newDTO.RoomID);
                    }

                    if (firstModel == null || newDTO.Message.MessageDate >= firstModel.Time)
                    {
                        RingIDViewModel.Instance.ChatLogModelsList.InvokeInsert(0, newModel);
                    }
                    else
                    {
                        int max = RingIDViewModel.Instance.ChatLogModelsList.Count;
                        int min = 0;
                        int pivot;

                        while (max > min)
                        {
                            pivot = (min + max) / 2;
                            if (newDTO.Message.MessageDate < RingIDViewModel.Instance.ChatLogModelsList.ElementAt(pivot).Time)
                            {
                                min = pivot + 1;
                            }
                            else
                            {
                                max = pivot;
                            }
                        }

                        RingIDViewModel.Instance.ChatLogModelsList.InvokeInsert(min, newModel);
                    }

                    if (UCChatLogPanel.Instance != null && UCChatLogPanel.Instance.IsSelectAllMode == false)
                    {
                        newModel.IsChecked = true;
                    }
                }
                else
                {
                    if (newDTO.FriendTableID > 0)
                    {
                        if (prevModel.FriendInfoModel == null)
                        {
                            prevModel.FriendInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(newDTO.FriendTableID);
                        }
                    }
                    else if (newDTO.GroupID > 0)
                    {
                        if (prevModel.GroupInfoModel == null)
                        {
                            prevModel.GroupInfoModel = RingIDViewModel.Instance.GroupList.TryGetValue(newDTO.GroupID);
                        }
                    }
                    else if (!String.IsNullOrWhiteSpace(newDTO.RoomID))
                    {
                        if (prevModel.RoomModel == null)
                        {
                            prevModel.RoomModel = RingIDViewModel.Instance.GetRoomModelByRoomID(newDTO.RoomID);
                        }
                    }

                    if (newDTO.Message.MessageDate >= firstModel.Time && newDTO.ContactID.Equals(firstModel.ContactID))
                    {
                        prevModel.LoadData(newDTO);
                        prevModel.Message.Message = prevModel.Message.Message != null ? prevModel.Message.Message.Trim() : String.Empty;
                    }
                    else if (newDTO.Message.MessageDate >= firstModel.Time)
                    {
                        if (newDTO.Message.MessageDate > firstModel.Time)
                        {
                            int indexOf = RingIDViewModel.Instance.ChatLogModelsList.IndexOf(prevModel);
                            RingIDViewModel.Instance.ChatLogModelsList.InvokeMove(indexOf, 0);
                        }
                        prevModel.LoadData(newDTO);
                        prevModel.Message.Message = prevModel.Message.Message != null ? prevModel.Message.Message.Trim() : String.Empty;
                    }
                    else if (newDTO.Message.MessageDate >= prevModel.Time || newDTO.NeedForcelyLoad == true)
                    {
                        if (newDTO.Message.MessageDate > prevModel.Time || newDTO.NeedForcelyLoad == true)
                        {
                            int indexOf = RingIDViewModel.Instance.ChatLogModelsList.IndexOf(prevModel);
                            int max = RingIDViewModel.Instance.ChatLogModelsList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.Message.MessageDate < RingIDViewModel.Instance.ChatLogModelsList.ElementAt(pivot).Time)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                RingIDViewModel.Instance.ChatLogModelsList.InvokeMove(indexOf, min);
                            }
                        }
                        prevModel.LoadData(newDTO);
                        prevModel.Message.Message = prevModel.Message.Message != null ? prevModel.Message.Message.Trim() : String.Empty;
                    }

                    if (prevModel.Message.FromFriend)
                    {
                        //TRIGGER MESSAGE TEXT COLOR & FULL NAME FONT IN CHAT LOG PANEL
                        prevModel.Message.OnPropertyChanged("CurrentInstance");
                    }
                }

                newDTO.NeedForcelyLoad = false;
            }
            catch (Exception ex)
            {
                log.Error("AddRecentModel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

    }
}

﻿using Models.Entity;
using View.BindingModels;
using View.ViewModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using log4net;
using View.Utility.Chat;
using View.UI.Call;

namespace View.Utility.Recent
{
    class CallLogLoadUtility : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CallLogLoadUtility).Name);

        private static readonly object _LoadSyncRoot = new object();
        private static readonly object _AddRemoveLock = new object();
        private static CallLogLoadUtility _Instance = null;
        private ConcurrentQueue<RecentDTO> _RecentQueue = new ConcurrentQueue<RecentDTO>();

        public CallLogLoadUtility()
        {
            _Instance = this;
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += FriendListLoad_DowWork;
            ProgressChanged += FriendListLoad_ProgressChanged;
            RunWorkerCompleted += FriendListLoad_RunWorkerCompleted;
        }

        public static void LoadRecentData(List<RecentDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                foreach (RecentDTO dto in list)
                {
                    CallLogLoadUtility.LoadData(dto);
                }
            }
        }

        public static void LoadRecentData(RecentDTO recentDTO)
        {
            CallLogLoadUtility.LoadData(recentDTO);
        }

        private static void LoadData(RecentDTO recentDTO)
        {
            try
            {
                lock (_LoadSyncRoot)
                {
                    if (CallLogLoadUtility._Instance == null)
                    {
                        CallLogLoadUtility._Instance = new CallLogLoadUtility();
                        CallLogLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        CallLogLoadUtility._Instance.Start();
                    }
                    else
                    {
                        CallLogLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        if (CallLogLoadUtility._Instance.IsBusy == false)
                        {
                            CallLogLoadUtility._Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadShowMoreData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Start()
        {
            RunWorkerAsync();
        }

        private void FriendListLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                RecentDTO recentDTO = null;
                while (_RecentQueue.TryDequeue(out recentDTO))
                {
                    //System.Threading.Thread.Sleep(50);
                    if (UCCallLog.Instance != null)
                    {
                        AddRecentModel(recentDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GroupListLoad_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            e.Result = true;
        }

        private void FriendListLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void FriendListLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_RecentQueue.Count > 0)
            {
                if (CallLogLoadUtility._Instance.IsBusy == false)
                {
                    CallLogLoadUtility._Instance.Start();
                }
            }
        }

        public static void Remove(string contactID, string uniqueKey)
        {
            try
            {
                List<string> uniqueKeys = new List<string>();
                uniqueKeys.Add(uniqueKey);
                RemoveRange(contactID, uniqueKeys);
            }
            catch (Exception ex)
            {
                log.Error("Remove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static bool RemoveRange(string contactID, List<string> uniqueKeys)
        {
            bool status = false;
            try
            {
                if (uniqueKeys == null || uniqueKeys.Count == 0) 
                { 
                    return status; 
                }

                lock (_AddRemoveLock)
                {
                    List<RecentModel> modelList = RingIDViewModel.Instance.CallLogModelsList.Where(P => P.ContactID.Equals(contactID)).ToList();
                    List<string> tempList = uniqueKeys.ToList();

                    for (int mIdx = 0; mIdx < modelList.Count; mIdx++)
                    {
                        RecentModel currModel = modelList[mIdx];
                        foreach (string uniqueKey in tempList.ToList())
                        {
                            if (currModel.CallLog.CallIDs.TryRemove(uniqueKey))
                            {
                                tempList.Remove(uniqueKey);
                            }
                            else
                            {
                                continue;
                            }

                            if (currModel.CallLog.CallIDs.Count > 0)
                            {
                                continue;
                            }

                            int index = RingIDViewModel.Instance.CallLogModelsList.IndexOf(currModel);
                            if (index == 0)
                            {
                                RingIDViewModel.Instance.CallLogModelsList.InvokeRemoveAt(index);
                                status = true;
                                mIdx--;
                            }
                            else if (index > 0)
                            {
                                RingIDViewModel.Instance.CallLogModelsList.InvokeRemoveAt(index);
                                status = true;
                                if (index < RingIDViewModel.Instance.CallLogModelsList.Count)
                                {
                                    RecentModel prevModel = RingIDViewModel.Instance.CallLogModelsList.ElementAt(index - 1);
                                    RecentModel nextModel = RingIDViewModel.Instance.CallLogModelsList.ElementAt(index);
                                    if (prevModel.CallLog.CallLogID.Equals(nextModel.CallLog.CallLogID))
                                    {
                                        MergeCallLogIDs(prevModel, nextModel);
                                        RingIDViewModel.Instance.CallLogModelsList.InvokeRemoveAt(index);
                                        mIdx--;
                                    }
                                }
                                mIdx--;
                            }
                            modelList = RingIDViewModel.Instance.CallLogModelsList.Where(P => P.ContactID.Equals(contactID)).ToList();
                            if (UCCallLog.Instance != null)
                            {
                                UCCallLog.Instance._DB_OFFSET = RingIDViewModel.Instance.CallLogModelsList.Count;
                            }
                            break;
                        }

                        if (tempList.Count == 0) return status;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Remove() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return status;
        }

        private static void AddRecentModel(RecentDTO newDTO)
        {
            try
            {
                lock (_AddRemoveLock)
                {
                    bool isProcessed = false;
                    if (RingIDViewModel.Instance.CallLogModelsList.Count > 0)
                    {
                        RecentModel firstModel = RingIDViewModel.Instance.CallLogModelsList.FirstOrDefault();
                        bool isFirstSame = newDTO.CallLog.CallLogID.Equals(firstModel.CallLog.CallLogID);

                        if (RingIDViewModel.Instance.CallLogModelsList.Count == 1)
                        {
                            if (isFirstSame)
                            {
                                MergeCallLogIDs(firstModel, newDTO);
                                isProcessed = true;
                            }
                            else if (newDTO.CallLog.CallingTime >= firstModel.CallLog.CallingTime)
                            {
                                RingIDViewModel.Instance.CallLogModelsList.InvokeInsert(0, CreateCallLog(newDTO));
                                isProcessed = true;
                            }
                            else
                            {
                                RingIDViewModel.Instance.CallLogModelsList.InvokeAdd(CreateCallLog(newDTO));
                                isProcessed = true;
                            }
                        }
                        else
                        {
                            if (!isFirstSame && newDTO.CallLog.CallingTime >= firstModel.CallLog.CallingTime)
                            {
                                RingIDViewModel.Instance.CallLogModelsList.InvokeInsert(0, CreateCallLog(newDTO));
                                isProcessed = true;
                            }
                            else if (isFirstSame && newDTO.CallLog.CallingTime >= RingIDViewModel.Instance.CallLogModelsList.ElementAt(1).CallLog.CallingTime)
                            {
                                MergeCallLogIDs(firstModel, newDTO);
                                isProcessed = true;
                            }
                            else
                            {
                                RecentModel lastModel = RingIDViewModel.Instance.CallLogModelsList.LastOrDefault();
                                bool isLastSame = newDTO.CallLog.CallLogID.Equals(lastModel.CallLog.CallLogID);

                                if (!isLastSame && newDTO.CallLog.CallingTime < lastModel.CallLog.CallingTime)
                                {
                                    RingIDViewModel.Instance.CallLogModelsList.InvokeAdd(CreateCallLog(newDTO));
                                    isProcessed = true;
                                }
                                else if (isLastSame && newDTO.CallLog.CallingTime < RingIDViewModel.Instance.CallLogModelsList.ElementAt(RingIDViewModel.Instance.CallLogModelsList.Count - 2).CallLog.CallingTime)
                                {
                                    MergeCallLogIDs(lastModel, newDTO);
                                    isProcessed = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        RingIDViewModel.Instance.CallLogModelsList.InvokeAdd(CreateCallLog(newDTO));
                        isProcessed = true;
                    }

                    if (isProcessed == false)
                    {
                        RecentModel prevModel = null;
                        int max = RingIDViewModel.Instance.CallLogModelsList.Count;
                        int min = 0;
                        int pivot;

                        while (max > min)
                        {
                            pivot = (min + max) / 2;
                            RecentModel pivotModel = RingIDViewModel.Instance.CallLogModelsList.ElementAt(pivot);
                            if (newDTO.CallLog.CallingTime < pivotModel.CallLog.CallingTime)
                            {
                                min = pivot + 1;
                            }
                            else
                            {
                                max = pivot;
                            }
                        }

                        if (min < RingIDViewModel.Instance.CallLogModelsList.Count)
                        {
                            RecentModel tempModel = RingIDViewModel.Instance.CallLogModelsList.ElementAt(min);
                            if (newDTO.CallLog.CallLogID.Equals(tempModel.CallLog.CallLogID))
                            {
                                prevModel = tempModel;
                            }
                        }

                        if (prevModel == null && min > 0)
                        {
                            RecentModel tempModel = RingIDViewModel.Instance.CallLogModelsList.ElementAt(min - 1);
                            if (newDTO.CallLog.CallLogID.Equals(tempModel.CallLog.CallLogID))
                            {
                                prevModel = tempModel;
                            }
                        }

                        if (prevModel != null)
                        {
                            MergeCallLogIDs(prevModel, newDTO);
                        }
                        else
                        {
                            RingIDViewModel.Instance.CallLogModelsList.InvokeInsert(min, CreateCallLog(newDTO));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddRecentModel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private static void MergeCallLogIDs(RecentModel recentModel, RecentDTO recentDTO)
        {
            if (recentDTO.CallLog.CallIDs != null && recentDTO.CallLog.CallIDs.Count > 0)
            {
                foreach (KeyValuePair<string, long> pair in recentDTO.CallLog.CallIDs.ToArray())
                {
                    recentModel.CallLog.CallIDs[pair.Key] = pair.Value;
                    if (pair.Value > recentModel.CallLog.CallingTime)
                    {
                        recentModel.CallLog.CallingTime = pair.Value;
                        recentModel.Time = pair.Value;
                    }
                    if (pair.Value < recentModel.CallLog.LastCallingTime)
                    {
                        recentModel.CallLog.LastCallingTime = pair.Value;
                    }
                }
            }
            else if (recentDTO.CallLog.CallID != null)
            {
                recentModel.CallLog.CallIDs[recentDTO.CallLog.CallID] = recentDTO.CallLog.CallingTime;
                if (recentDTO.CallLog.CallingTime > recentModel.CallLog.CallingTime)
                {
                    recentModel.CallLog.CallingTime = recentDTO.CallLog.CallingTime;
                    recentModel.Time = recentDTO.CallLog.CallingTime;
                }
                if (recentDTO.CallLog.CallingTime <= recentModel.CallLog.LastCallingTime)
                {
                    recentModel.CallLog.LastCallingTime = recentDTO.CallLog.CallingTime;
                }
            }
        }

        private static void MergeCallLogIDs(RecentModel prevModel, RecentModel nextModel)
        {
            if (nextModel.CallLog.CallIDs != null && nextModel.CallLog.CallIDs.Count > 0)
            {
                foreach (KeyValuePair<string, long> pair in nextModel.CallLog.CallIDs.ToArray())
                {
                    prevModel.CallLog.CallIDs[pair.Key] = pair.Value;
                    if (pair.Value > prevModel.CallLog.CallingTime)
                    {
                        prevModel.CallLog.CallingTime = pair.Value;
                        prevModel.Time = pair.Value;
                    }
                    if (pair.Value < prevModel.CallLog.LastCallingTime)
                    {
                        prevModel.CallLog.LastCallingTime = pair.Value;
                    }
                }
            }
        }

        private static RecentModel CreateCallLog(RecentDTO newDTO)
        {
            RecentModel newModel = new RecentModel(newDTO);
            newModel.UnreadList = ChatViewModel.Instance.GetUnreadCallByID(newDTO.ContactID);
            newModel.FriendInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(newDTO.FriendTableID);
            return newModel;
        }

    }
}

﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdDeleteUploadedChannelMedia
    {
        private List<Guid> _MediaList;
        private Func<bool, int> _OnComplete;

        public ThrdDeleteUploadedChannelMedia(List<Guid> mediaList, Func<bool, int> onComplete)
        {
            this._MediaList = mediaList;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            if (_MediaList != null && _MediaList.Count > 0)
            {
                JArray itemList = new JArray();
                foreach(Guid mediaId in _MediaList)
                {
                    itemList.Add(mediaId);
                }
                pakToSend[JsonKeys.ChannelMediaIds] = itemList;
            }
            else
            {
                if (_OnComplete != null)
                {
                    this._OnComplete(false);
                }
            }
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_DELETE_UPLOADED_CHANNEL_MEDIA;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                if (_OnComplete != null)
                {
                    this._OnComplete(true);
                }
            }
            else
            {
                if (_OnComplete != null)
                {
                    this._OnComplete(false);
                }
            }
        }
    }
}

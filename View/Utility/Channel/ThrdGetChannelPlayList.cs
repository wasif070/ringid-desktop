<<<<<<< HEAD
﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdGetChannelPlayList
    {
        private Guid _ChannelId;
        private int _StartLimit;
        private int _Limit;
        private Func<bool, int> _Oncomplete;

        public ThrdGetChannelPlayList(Guid channelId, int startLimit, int limit, Func<bool, int> onComplete)
        {
            this._ChannelId = channelId;
            this._StartLimit = startLimit;
            this._Limit = limit;
            this._Oncomplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.StartLimit] = _StartLimit;
            pakToSend[JsonKeys.Limit] = _Limit;

            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_CHANNEL_PLAYLIST;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._Oncomplete != null)
            {
                this._Oncomplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }
        }

    }
}
=======
﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdGetChannelPlayList
    {
        private Guid _ChannelId;
        private int _StartLimit;
        private Guid _Pivot;
        private int _Limit;
        private Func<bool, int> _Oncomplete;

        public ThrdGetChannelPlayList(Guid channelId, int startLimit, Guid pivot, int limit, Func<bool, int> onComplete)
        {
            this._ChannelId = channelId;
            this._StartLimit = startLimit;
            this._Pivot = pivot;
            this._Limit = limit;
            this._Oncomplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.StartLimit] = _StartLimit;
            if (_StartLimit > 0)
            {
                pakToSend[JsonKeys.PivotUUID] = _Pivot;
            }
            pakToSend[JsonKeys.Limit] = _Limit;

            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_CHANNEL_PLAYLIST;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._Oncomplete != null)
            {
                this._Oncomplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }
        }

    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

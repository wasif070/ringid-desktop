﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;
using View.UI;
using View.Utility.Auth;
using View.Utility.Chat.Service;
using View.Utility.WPFMessageBox;

namespace View.Utility.Channel
{
    public class ThrdCreateChannel
    {
        private string _ChannelTitle;
        private string _ChannelDescription;
        private List<ChannelCategoryDTO> _ChannelCategoryList;
        private string _CountryName;
        private string _ProfileImageUrl;
        private int _ProfileImageHeight;
        private int _ProfileImageWidth;
        private string _CoverImageUrl;
        private int _CoverImageHeight;
        private int _CoverImageWidth;
        private int _CoverImagePosX;
        private int _CoverImagePosY;
        private int _ChannelType = 1;
        private int _UserType = 1;
        private Func<bool, ChannelDTO, int> _Oncomplete;

        public ThrdCreateChannel(string title, string description, List<ChannelCategoryDTO> ctgList, string countryName, string pImgUrl, int pImgH, int pImgW,
                                string cImgUrl, int cImgH, int cImgW, int cImgPosX, int cImgPosY, int channelType, Func<bool, ChannelDTO, int> oncomplete = null)
        {
            this._ChannelTitle = title;
            this._ChannelDescription = description;
            this._ChannelCategoryList = ctgList;
            this._CountryName = countryName;
            this._ProfileImageUrl = pImgUrl;
            this._ProfileImageHeight = pImgH;
            this._ProfileImageWidth = pImgW;
            this._CoverImageUrl = cImgUrl;
            this._CoverImageHeight = cImgH;
            this._CoverImageWidth = cImgW;
            this._CoverImagePosX = cImgPosX;
            this._CoverImagePosY = cImgPosY;
            this._ChannelType = channelType;
            this._Oncomplete = oncomplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.Title] = _ChannelTitle;
            pakToSend[JsonKeys.Description] = _ChannelDescription;
            JArray catArray = new JArray();
            if (_ChannelCategoryList != null)
            {
                foreach (ChannelCategoryDTO channelDTO in _ChannelCategoryList)
                {
                    JObject obj = new JObject();
                    obj[JsonKeys.StreamCategoryId] = channelDTO.CategoryID;
                    obj[JsonKeys.Category] = channelDTO.CategoryName;
                    catArray.Add(obj);
                }
            }
            pakToSend[JsonKeys.ChannelCatetoryList] = catArray;
            pakToSend[JsonKeys.Country] = _CountryName;
            pakToSend[JsonKeys.ProfileImage] = _ProfileImageUrl;
            pakToSend[JsonKeys.ProfileImageHeight] = _ProfileImageHeight;
            pakToSend[JsonKeys.ProfileImageWidth] = _ProfileImageWidth;
            pakToSend[JsonKeys.CoverImage] = _CoverImageUrl;
            pakToSend[JsonKeys.CoverImageHeight] = _CoverImageHeight;
            pakToSend[JsonKeys.CoverImageWidth] = _CoverImageWidth;
            pakToSend[JsonKeys.CropImageX] = _CoverImagePosX;
            pakToSend[JsonKeys.CropImageY] = _CoverImagePosY;
            pakToSend[JsonKeys.ChannelType] = _ChannelType;
            pakToSend[JsonKeys.ChannelUserType] = _UserType;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_CREATE_CHANNEL;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, "Channel Created Succesfully");
            }
            else
            {

            }
            if (this._Oncomplete != null)
            {
                bool status = _JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success];
                ChannelDTO chnlDTO = null;
                if (status && _JobjFromResponse[JsonKeys.ChannelDTO] != null)
                {
                    JObject jChannelDTO = (JObject)_JobjFromResponse[JsonKeys.ChannelDTO];
                    chnlDTO = ChannelSignalHandler.MakeChannelDTO(jChannelDTO);
                    chnlDTO.CreationTime = ChatService.GetServerTime();
                }
                this._Oncomplete(status, chnlDTO);
            }
        }
    }
}

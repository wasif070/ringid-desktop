﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdAddChannelProfileImage
    {
        private Guid _ChannelId;
        private string _ProfileImageUrl;
        private int _ProfileImageHeight;
        private int _ProfileImageWidth;
        private Func<bool, int> _Oncomplete;

        public ThrdAddChannelProfileImage(Guid channelId, string pImgUrl, int pImgH, int pImgW, Func<bool, int> oncomplete = null)
        {
            this._ChannelId = channelId;
            this._ProfileImageUrl = pImgUrl;
            this._ProfileImageHeight = pImgH;
            this._ProfileImageWidth = pImgW;
            this._Oncomplete = oncomplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.ProfileImage] = _ProfileImageUrl;
            pakToSend[JsonKeys.ProfileImageHeight] = _ProfileImageHeight;
            pakToSend[JsonKeys.ProfileImageWidth] = _ProfileImageWidth;

            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_CHANNEL_PROFILE_IMAGE;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(true);
                }
            }
            else
            {
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(false);
                }
            }
        }
    }
}

﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdActivateDeactivateChannel
    {
        private Guid _ChannelId;
        private int _SettingsValue;
        private Func<bool, int> _Oncomplete;

        public ThrdActivateDeactivateChannel(Guid channelId, int settingsValue, Func<bool, int> onComplete = null)
        {
            this._ChannelId = channelId;
            this._SettingsValue = settingsValue;
            this._Oncomplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.SettingsValue] = _SettingsValue; // 1 for activation, 2 for deactivation
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACTIVATE_DEACTIVATE_CHANNEL;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                Guid channelId = _JobjFromResponse[JsonKeys.ChannelID] != null ? (Guid)_JobjFromResponse[JsonKeys.ChannelID] : Guid.Empty;
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(true);
                }
            }
            else
            {
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(false);
                }
            }
        }

    }
}

<<<<<<< HEAD
﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdGetFollowningChannelList
    {
        private int _Start;
        private int _Limit;
        private Func<bool, int> _OnComplete;

        public ThrdGetFollowningChannelList(int start, int limit, Func<bool, int> onComplete = null)
        {
            this._Start = start;
            this._Limit = limit;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.StartLimit] = _Start;
            pakToSend[JsonKeys.Limit] = _Limit;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_FOLLOWING_CHANNEL_LIST;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._OnComplete != null) 
            {
                this._OnComplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }
        }

    }
}
=======
﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdGetFollowningChannelList
    {
        private int _Start;
        private Guid _Pivot;
        private int _Limit;
        private Func<bool, int> _OnComplete;

        public ThrdGetFollowningChannelList(int start, Guid pivot, int limit, Func<bool, int> onComplete = null)
        {
            this._Start = start;
            this._Pivot = pivot;
            this._Limit = limit;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.StartLimit] = _Start;
            if (_Start > 0)
            {
                pakToSend[JsonKeys.PivotUUID] = _Pivot;
            }
            pakToSend[JsonKeys.Limit] = _Limit;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_FOLLOWING_CHANNEL_LIST;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._OnComplete != null) 
            {
                this._OnComplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }
        }

    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

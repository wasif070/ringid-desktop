﻿using Auth.utility;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdSearchChannels
    {
        private int _Start = 0;
        private int _Limit = 0;
        private string _SearchParam = string.Empty;
        private string _Country = string.Empty;
        private List<int> _CatIDs = null;
        private Func<bool, int> _OnComplete;

        public ThrdSearchChannels(int start, int limit, string searchParam, string country, List<int> catIDs, Func<bool, int> onComplete = null)
        {
            this._Start = start;
            this._Limit = limit;
            this._SearchParam = searchParam;
            this._Country = country;
            this._CatIDs = catIDs;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.StartLimit] = _Start;
            pakToSend[JsonKeys.Limit] = _Limit;
            if (!String.IsNullOrWhiteSpace(_SearchParam))
            {
                pakToSend[JsonKeys.Title] = _SearchParam;
            }
            if (!String.IsNullOrWhiteSpace(_Country))
            {
                pakToSend[JsonKeys.Country] = _Country;
            }
            if (_CatIDs != null && _CatIDs.Count > 0)
            {
                JArray itemList = new JArray();
                foreach (int id in _CatIDs)
                {
                    itemList.Add(id);
                }
                pakToSend[JsonKeys.ChannelCatetoryIDs] = itemList;
            }
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._OnComplete != null)
            {
                this._OnComplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }
        }
    }
}

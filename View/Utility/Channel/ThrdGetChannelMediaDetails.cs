<<<<<<< HEAD
﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.Utility.Auth;
using View.Utility.Chat.Service;

namespace View.Utility.Channel
{
    public class ThrdGetChannelMediaDetails
    {
        private log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdGetChannelMediaDetails).Name);
        private Guid _ChannelId;
        private Func<ChannelMediaDTO, int> _OnComplete = null;

        public ThrdGetChannelMediaDetails(Guid channelId, Func<ChannelMediaDTO, int> onComplete = null)
        {
            this._ChannelId = channelId;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.ChannelID] = _ChannelId;
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_CHANNEL_MEDIA_ITEM;
                pakToSend[JsonKeys.Time] = ChatService.GetServerTime();

                JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
#if CHAT_LOG
                log.Debug("CHANNEL MEDIA DETAILS => " + _JobjFromResponse);
#endif
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    ChannelMediaDTO mediaDTO = null;
                    if (_JobjFromResponse[JsonKeys.ChannelMediaDTO] != null)
                    {
                        mediaDTO = ChannelSignalHandler.MakeChannelMediaDTO((JObject)_JobjFromResponse[JsonKeys.ChannelMediaDTO]);
                    }

                    if (_OnComplete != null)
                    {
                        _OnComplete(mediaDTO);
                    }
                }
                else
                {
                    if (_OnComplete != null)
                    {
                        _OnComplete(null);
                    }
                }
            }
            catch (Exception ex)
            {
                if (_OnComplete != null)
                {
                    _OnComplete(null);
                }
                log.Error(ex.Message + ex.StackTrace);
            }
        }
    }
}
=======
﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.Utility.Auth;
using View.Utility.Chat.Service;

namespace View.Utility.Channel
{
    public class ThrdGetChannelMediaDetails
    {
        private log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdGetChannelMediaDetails).Name);
        private Guid _ChannelId;
        private Func<ChannelMediaDTO, int> _OnComplete = null;

        public ThrdGetChannelMediaDetails(Guid channelId, Func<ChannelMediaDTO, int> onComplete = null)
        {
            this._ChannelId = channelId;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.ChannelID] = _ChannelId;
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_CHANNEL_MEDIA_ITEM;

                JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
#if CHAT_LOG
                log.Debug("CHANNEL MEDIA DETAILS => " + _JobjFromResponse);
#endif
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    ChannelMediaDTO mediaDTO = null;
                    if (_JobjFromResponse[JsonKeys.ChannelMediaDTO] != null)
                    {
                        mediaDTO = ChannelSignalHandler.MakeChannelMediaDTO((JObject)_JobjFromResponse[JsonKeys.ChannelMediaDTO]);
                    }

                    if (_OnComplete != null)
                    {
                        _OnComplete(mediaDTO);
                    }
                }
                else
                {
                    if (_OnComplete != null)
                    {
                        _OnComplete(null);
                    }
                }
            }
            catch (Exception ex)
            {
                if (_OnComplete != null)
                {
                    _OnComplete(null);
                }
                log.Error(ex.Message + ex.StackTrace);
            }
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

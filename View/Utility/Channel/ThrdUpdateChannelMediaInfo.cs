﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdUpdateChannelMediaInfo
    {
        private Guid _ChannelId;
        private string _Title;
        private Guid _MediaId;
        private string _Description;
        private string _Artist;

        public ThrdUpdateChannelMediaInfo(Guid channelId, string title, Guid mediaId, string description, string artist)
        {
            this._ChannelId = channelId;
            this._Title = title;
            this._MediaId = mediaId;
            this._Description = description;
            this._Artist = artist;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.Title] = _Title;
            pakToSend[JsonKeys.ChannelMediaID] = _MediaId;
            pakToSend[JsonKeys.Description] = _Description;
            pakToSend[JsonKeys.Artist] = _Artist;

            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_UPDATE_CHANNEL_MEDIA_INFO;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {

            }
            else
            {

            }

        }
    }
}

﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;

namespace View.Utility.Channel
{
    public class ThrdGetMostViewedChannelList
    {
        private int _Start;
        private int _Limit;
        private List<int> _CategoryList;
        private string _SearchParam;
        private Func<bool, int> _OnComplete;

        public ThrdGetMostViewedChannelList(int start, int limit, string searchParam, List<int> catList, Func<bool, int> onComplete)
        {
            this._Start = start;
            this._Limit = limit;
            this._SearchParam = searchParam;
            this._CategoryList = catList;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            if (!String.IsNullOrWhiteSpace(_SearchParam))
            {
                pakToSend[JsonKeys.Title] = _SearchParam;
            }
            if (_CategoryList != null && _CategoryList.Count > 0)
            {
                JArray itemList = new JArray();
                foreach (int catId in _CategoryList)
                {
                    itemList.Add(catId);
                }
                pakToSend[JsonKeys.ChannelCatetoryIDs] = itemList;
            }
            pakToSend[JsonKeys.StartLimit] = _Start;
            pakToSend[JsonKeys.Limit] = _Limit;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_MOST_VIEWED_CHANNEL_LIST;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                if (_OnComplete != null)
                {
                    this._OnComplete(true);
                }
            }
            else
            {
                if (_OnComplete != null)
                {
                    this._OnComplete(false);
                }
            }
        }
    }
}

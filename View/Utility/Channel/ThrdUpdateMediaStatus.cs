﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdUpdateMediaStatus
    {
        private Guid _ChannelId;
        private List<Guid> _ChannelMediaIds;
        private int _MediaStatus;
        //private List<ChannelMediaDTO> _ChannelMediaList;
        private long? _StartTime;
        private int _UserType = 1;
        private Func<bool, int> _Oncomplete;


        public ThrdUpdateMediaStatus(Guid channelId, List<Guid> channelMediaIds, int mediaStatus, long? startTime, Func<bool, int> oncomplete = null)
        {
            this._ChannelId = channelId;
            this._ChannelMediaIds = channelMediaIds;
            this._MediaStatus = mediaStatus;
            //this._ChannelMediaList = channelMediaList;
            this._StartTime = startTime;
            this._Oncomplete = oncomplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            JArray jarray = new JArray();
            if (_ChannelMediaIds != null)
            {
                foreach (Guid channlId in _ChannelMediaIds)
                {
                    jarray.Add(channlId);
                }
            }
            pakToSend[JsonKeys.ChannelMediaIds] = jarray;
            pakToSend[JsonKeys.ChannelMediaStatus] = _MediaStatus;
            if (_StartTime != null)
            {
                pakToSend[JsonKeys.Time] = _StartTime;
            }
            pakToSend[JsonKeys.ChannelUserType] = _UserType;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_UPDATE_CHANNEL_MEDIA_STATUS;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (this._Oncomplete != null)
            {
                this._Oncomplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }
        }

    }
}

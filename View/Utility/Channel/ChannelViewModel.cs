﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.UI;
using Models.Utility;
using View.Utility;
using View.Utility.Call;
using System.Windows.Controls;
using View.UI.PopUp.Stream;
using View.Utility.WPFMessageBox;
using View.Utility.Stream.Utils;
using View.Constants;
using View.Utility.StreamAndChannel;
using View.Utility.Chat.Service;
using View.UI.StreamAndChannel;
using System.Dynamic;
using View.Utility.Stream;
<<<<<<< HEAD
=======
using View.ViewModel;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

namespace View.Utility.Channel
{
    public class ChannelViewModel : INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(ChannelViewModel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private ChannelLoadStatusModel _LoadStatusModel = new ChannelLoadStatusModel();
        private ObservableCollection<ChannelModel> _MyChannelList = new ObservableCollection<ChannelModel>();
        private ObservableCollection<ChannelModel> _ChannelFollowingList = new ObservableCollection<ChannelModel>();
        private ObservableCollection<ChannelCategoryModel> _ChannelCategoryList = new ObservableCollection<ChannelCategoryModel>();
        private ObservableCollection<ChannelModel> _ChannelFeatureList = new ObservableCollection<ChannelModel>();
        private ObservableCollection<ChannelModel> _ChannelMostViewList = new ObservableCollection<ChannelModel>();
        private ObservableCollection<ChannelModel> _ChannelSearchList = new ObservableCollection<ChannelModel>();
        private ObservableCollection<ChannelModel> _ChannelByCountryList = new ObservableCollection<ChannelModel>();
        private ObservableCollection<ChannelModel> _ChannelByCategoryList = new ObservableCollection<ChannelModel>();
        private ConcurrentDictionary<Guid, ObservableCollection<ChannelMediaModel>> _ChannelPlayList = new ConcurrentDictionary<Guid, ObservableCollection<ChannelMediaModel>>();
        private ConcurrentDictionary<Guid, ObservableCollection<ChannelMediaModel>> _ChannelNowPlayingList = new ConcurrentDictionary<Guid, ObservableCollection<ChannelMediaModel>>();
<<<<<<< HEAD
=======
        private ConcurrentDictionary<int, ObservableCollection<ChannelMediaModel>> _ChannelUploadedMediaList = new ConcurrentDictionary<int, ObservableCollection<ChannelMediaModel>>();
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

        public static ChannelViewModel Instance
        {
            get;
            set;
        }

        #region Common Part

        private ICommand _CreateChannelCommand;
        private ICommand _MyChannelListCommand;
        private ICommand _MyChannelMainCommand;
        private ICommand _ChannelDetailsCommand;
        private ICommand _ChannelViewCommand;
        private ICommand _ChannelDiscoveryCommand;
        private ICommand _BackCommad;
<<<<<<< HEAD
=======
        private ICommand _MediaPlayCommand;
        
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

        private ICommand _SubscribeUnsubscribeCommand;
        private ICommand _DiscoverByCategoryCommand;
        private ICommand _DiscoverByCountryCommand;

        #region Property

        public ChannelLoadStatusModel LoadStatusModel
        {
            get { return _LoadStatusModel; }
            set
            {
                _LoadStatusModel = value;
                this.OnPropertyChanged("LoadStatusModel");
            }
        }

        public ObservableCollection<ChannelModel> MyChannelList
        {
            get { return _MyChannelList; }
            set
            {
                _MyChannelList = value;
                this.OnPropertyChanged("MyChannelList");
            }
        }

        public ObservableCollection<ChannelModel> ChannelFollowingList
        {
            get { return _ChannelFollowingList; }
            set
            {
                _ChannelFollowingList = value;
                this.OnPropertyChanged("ChannelFollowingList");
            }
        }

        public ObservableCollection<ChannelCategoryModel> ChannelCategoryList
        {
            get { return _ChannelCategoryList; }
            set
            {
                _ChannelCategoryList = value;
                this.OnPropertyChanged("ChannelCategoryList");
            }
        }

        public ObservableCollection<ChannelModel> ChannelFeatureList
        {
            get { return _ChannelFeatureList; }
            set
            {
                _ChannelFeatureList = value;
                this.OnPropertyChanged("ChannelFeatureList");
            }
        }

        public ObservableCollection<ChannelModel> ChannelMostViewList
        {
            get { return _ChannelMostViewList; }
            set
            {
                _ChannelMostViewList = value;
                this.OnPropertyChanged("ChannelMostViewList");
            }
<<<<<<< HEAD
        }      
=======
        }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

        public ObservableCollection<ChannelModel> ChannelSearchList
        {
            get { return _ChannelSearchList; }
            set
            {
                _ChannelSearchList = value;
                this.OnPropertyChanged("ChannelSearchList");
            }
        }

        public ObservableCollection<ChannelModel> ChannelByCountryList
        {
            get { return _ChannelByCountryList; }
            set
            {
                _ChannelByCountryList = value;
                this.OnPropertyChanged("ChannelByCountryList");
            }
        }

        public ObservableCollection<ChannelModel> ChannelByCategoryList
        {
            get { return _ChannelByCategoryList; }
            set
            {
                _ChannelByCategoryList = value;
                this.OnPropertyChanged("ChannelByCategoryList");
            }
        }

        public ConcurrentDictionary<Guid, ObservableCollection<ChannelMediaModel>> ChannelPlayList
        {
            get { return _ChannelPlayList; }
            set { _ChannelPlayList = value; }
        }

        public ConcurrentDictionary<Guid, ObservableCollection<ChannelMediaModel>> ChannelNowPlayingList
        {
            get { return _ChannelNowPlayingList; }
            set { _ChannelNowPlayingList = value; }
        }

<<<<<<< HEAD
=======
        public ConcurrentDictionary<int, ObservableCollection<ChannelMediaModel>> ChannelUploadedMediaList
        {
            get { return _ChannelUploadedMediaList; }
            set { _ChannelUploadedMediaList = value; }
        }

>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        #endregion Property

        #region Utility Method

<<<<<<< HEAD
=======
        public ObservableCollection<ChannelMediaModel> GetChannelUploadedMediaList(int mediaType)
        {
            ObservableCollection<ChannelMediaModel> mediaList = this.ChannelUploadedMediaList.TryGetValue(mediaType); ;
            if (mediaList == null)
            {
                mediaList = new ObservableCollection<ChannelMediaModel>();
                this.ChannelUploadedMediaList[mediaType] = mediaList;
            }
            return mediaList;
        }

>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        public ObservableCollection<ChannelMediaModel> GetChannelPlayList(Guid channelId)
        {
            ObservableCollection<ChannelMediaModel> channelList = this.ChannelPlayList.TryGetValue(channelId);
            if (channelList == null)
            {
                channelList = new ObservableCollection<ChannelMediaModel>();
                this.ChannelPlayList[channelId] = channelList;
            }
            return channelList;
        }

        public void RemoveChannelPlayList(Guid channelId)
        {
            this.ChannelPlayList.TryRemove(channelId);
        }

        public ObservableCollection<ChannelMediaModel> GetChannelNowPlayingList(Guid channelId)
        {
            ObservableCollection<ChannelMediaModel> channelList = this.ChannelNowPlayingList.TryGetValue(channelId);
            if (channelList == null)
            {
                channelList = new ObservableCollection<ChannelMediaModel>();
                this.ChannelNowPlayingList[channelId] = channelList;
            }
            return channelList;
        }

        public void RemoveChannelNowPlayingList(Guid channelId)
        {
            this.ChannelNowPlayingList.TryRemove(channelId);
        }

        private void OnCreateChannelCommand(object param)
        {
            MyChannelSwitcher.Switch(MyChannelConstants.TypeMyChannelCreatePanel);
            UCMiddlePanelSwitcher.View_UCMyChannel._BackChannelList.Add(ChannelConstants._BACK_TO_CHANNEL_LIST);
        }

        private void OnMyChannelListCommand(object param)
        {
            MyChannelSwitcher.Switch(MyChannelConstants.TypeMyChannelAndFollowingPanel);
        }

        private void OnMyChannelMainCommand(object param)
        {
            MyChannelSwitcher.Switch(MyChannelConstants.TypeMyChannelMainPanel, param);
            UCMiddlePanelSwitcher.View_UCMyChannel._BackChannelList.Add(ChannelConstants._BACK_TO_CHANNEL_LIST);
        }

        private void OnChannelDetailsCommand(object param)
        {
            if (((dynamic)param).IsFromMyChannel)
            {
                MyChannelSwitcher.Switch(MyChannelConstants.TypeMyChannelDetailsPanel, param);
            }
            else
            {
                StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeChannelDetailsPanel, param);
            }
        }

        public void OnChannelViewCommand(object param)
        {
            UCStreamAndChannelViewer.Instance.Show((ChannelModel)param);
        }

        public void OnChannelDiscoveryCommand(object parameter)
        {
            StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeChannelDiscoveryPanel, parameter);
        }

<<<<<<< HEAD
=======
        private void OnMediaPlayCommand(object param)
        {
            try
            {
                ChannelMediaModel item = (ChannelMediaModel)param;
                List<ChannelMediaModel> tempList = ChannelViewModel.Instance.GetChannelUploadedMediaList(item.MediaType).ToList();
                int index = tempList.IndexOf(item);

                if (index >= 0)
                {
                    ObservableCollection<SingleMediaModel> MediaList = new ObservableCollection<SingleMediaModel>();
                    foreach (ChannelMediaModel mediaModel in tempList)
                    {
                        SingleMediaModel singleMediaModel = new SingleMediaModel();
                        singleMediaModel.StreamUrl = mediaModel.MediaUrl;
                        singleMediaModel.ContentId = mediaModel.MediaID;
                        singleMediaModel.MediaOwner = new BaseUserProfileModel();
                        singleMediaModel.MediaOwner.UserTableID = mediaModel.OwnerID;
                        singleMediaModel.Title = mediaModel.Title;
                        singleMediaModel.Duration = mediaModel.Duration / 1000;
                        singleMediaModel.Artist = mediaModel.Artist;
                        singleMediaModel.MediaType = 2;
                        singleMediaModel.ThumbUrl = mediaModel.ThumbImageUrl;
                        singleMediaModel.IsFromLocalDirectory = false;
                        singleMediaModel.ChannelId = mediaModel.ChannelID;
                        MediaList.Add(singleMediaModel);
                    }
                    MediaUtility.RunPlayList(true, Guid.Empty, MediaList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, index);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnMediaPlayCommand() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        private void OnBackCommad(object param)
        {
            if (UCMiddlePanelSwitcher.View_UCMyChannel._BackChannelList.Count > 0)
            {
                int type = UCMiddlePanelSwitcher.View_UCMyChannel._BackChannelList.LastOrDefault();
                switch (type)
                {
                    case ChannelConstants._BACK_TO_CHANNEL_LIST:
                        MyChannelSwitcher.Switch(MyChannelConstants.TypeMyChannelAndFollowingPanel);
                        break;
                    case ChannelConstants._BACK_TO_CREATE_CHANNEL:
                        MyChannelSwitcher.Switch(MyChannelConstants.TypeMyChannelCreatePanel);
                        break;
                    //case StreamConstants.STREAM_BACK_TO_SEARCH:
                    //    UCMiddlePanelSwitcher.View_UCStreamWrapper.ShowStreamSearchView();
                    //    break;
                    //case StreamConstants.STREAM_BACK_TO_COUNTRY:
                    //    UCMiddlePanelSwitcher.View_UCStreamWrapper.ShowMoreStreamView();
                    //    break;
                    //default: UCMiddlePanelSwitcher.View_UCStreamWrapper.ShowStreamHomeView();
                    //    break;
                }

                UCMiddlePanelSwitcher.View_UCMyChannel._BackChannelList.RemoveAt(UCMiddlePanelSwitcher.View_UCMyChannel._BackChannelList.Count - 1);
            }
        }

        private void OnSubscribeUnsubscribeCommand(object param)
        {
            ChannelModel model = (ChannelModel)param;
            model.IsProcessing = true;
            int type = model.SubscriptionTime > 0 ? ChannelConstants.UNSUBSCRIBE : ChannelConstants.SUBSCRIBE;

            new ThrdSubscribeUnsubscribeChannel(model.ChannelID, type, (status, channelId) =>
            {
                if (status)
                {
                    if (type == ChannelConstants.SUBSCRIBE)
                    {
                        model.SubscriptionTime = ChatService.GetServerTime();
                        model.SubscriberCount++;
                        ChannelHelpers.AddIntoFollowingChannelList(ChannelHelpers.GetChannelDTOFromChannnelModel(model));
                    }
                    else
                    {
                        model.SubscriptionTime = 0;
                        model.SubscriberCount = model.SubscriberCount > 0 ? model.SubscriberCount - 1 : 0;
                        ChannelFollowingList.InvokeRemove(model);
                    }
                }
                model.IsProcessing = false;
                return 0;
            }).Start();
        }

        public void OnDiscoverByCategoryCommand(object param)
        {
            try
            {
                ChannelCategoryModel categoryModel = (ChannelCategoryModel)param;
                ParamModel paramModel = new ParamModel();
                paramModel.Title = categoryModel.CategoryName + " - Channels";
                paramModel.ActionType = AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST;
                paramModel.Param = categoryModel;
                paramModel.PrevParam = null;
                paramModel.PrevViewType = StreamAndChannelConstants.TypeChannelDiscoveryPanel;
                StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel, paramModel);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnDiscoverByCategoryCommand() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnDiscoverByCountryCommand(object param)
        {
            try
            {
                CountryCodeModel countryModel = (CountryCodeModel)param;
                ParamModel paramModel = new ParamModel();
                paramModel.Title = countryModel.CountryName + " - Channels";
                paramModel.ActionType = AppConstants.TYPE_ACTION_SEARCH_CHANNEL_LIST;
                paramModel.Param = countryModel;
                paramModel.PrevParam = null;
                paramModel.PrevViewType = StreamAndChannelConstants.TypeChannelDiscoveryPanel;
                StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel, paramModel);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnDiscoverByCountryCommand() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Command

        public ICommand CreateChannelCommand
        {
            get
            {
                if (_CreateChannelCommand == null)
                {
                    _CreateChannelCommand = new RelayCommand(param => OnCreateChannelCommand(param));
                }
                return _CreateChannelCommand;
            }
        }

        public ICommand MyChannelListCommand
        {
            get
            {
                if (_MyChannelListCommand == null)
                {
                    _MyChannelListCommand = new RelayCommand(param => OnMyChannelListCommand(param));
                }
                return _MyChannelListCommand;
            }
        }

        public ICommand ChannelDetailsCommand
        {
            get
            {
                if (_ChannelDetailsCommand == null)
                {
                    _ChannelDetailsCommand = new RelayCommand(param => OnChannelDetailsCommand(param));
                }
                return _ChannelDetailsCommand;
            }
        }

        public ICommand MyChannelMainCommand
        {
            get
            {
                if (_MyChannelMainCommand == null)
                {
                    _MyChannelMainCommand = new RelayCommand(param => OnMyChannelMainCommand(param));
                }
                return _MyChannelMainCommand;
            }
        }

        public ICommand ChannelViewCommand
        {
            get
            {
                if (_ChannelViewCommand == null)
                {
                    _ChannelViewCommand = new RelayCommand(param => OnChannelViewCommand(param), param => { return !StreamViewModel.Instance.RingCallConnected; });
                }
                return _ChannelViewCommand;
            }
        }

        public ICommand ChannelDiscoveryCommand
        {
            get
            {
                if (_ChannelDiscoveryCommand == null)
                {
                    _ChannelDiscoveryCommand = new RelayCommand((param) => OnChannelDiscoveryCommand(param));
                }
                return _ChannelDiscoveryCommand;
            }
        }

        public ICommand ChannelBackeCommand
        {
            get
            {
                if (_BackCommad == null)
                {
                    _BackCommad = new RelayCommand(param => OnBackCommad(param));
                }
                return _BackCommad;
            }
        }

<<<<<<< HEAD
=======
        public ICommand MediaPlayCommand
        {
            get
            {
                if (_MediaPlayCommand == null)
                {
                    _MediaPlayCommand = new RelayCommand(param => OnMediaPlayCommand(param));
                }
                return _MediaPlayCommand;
            }
        }

>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        public ICommand SubscribeUnsubscribeCommand
        {
            get
            {
                if (_SubscribeUnsubscribeCommand == null)
                {
                    _SubscribeUnsubscribeCommand = new RelayCommand(param => OnSubscribeUnsubscribeCommand(param));
                }
                return _SubscribeUnsubscribeCommand;
            }
        }

        public ICommand DiscoverByCategoryCommand
        {
            get
            {
                if (_DiscoverByCategoryCommand == null)
                {
                    _DiscoverByCategoryCommand = new RelayCommand(param => OnDiscoverByCategoryCommand(param));
                }
                return _DiscoverByCategoryCommand;
            }
        }

        public ICommand DiscoverByCountryCommand
        {
            get
            {
                if (_DiscoverByCountryCommand == null)
                {
                    _DiscoverByCountryCommand = new RelayCommand(param => OnDiscoverByCountryCommand(param));
                }
                return _DiscoverByCountryCommand;
            }
        }

        #endregion Command

        #endregion Common Part

    }
}

﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.Utility.Auth;

namespace View.Utility.Channel
{
    public class ThrdGetChannelDetails
    {
        private log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdGetChannelDetails).Name);
        private Guid _ChannelId;
        private Func<ChannelDTO, int> _OnComplete = null;

        public ThrdGetChannelDetails(Guid channelId, Func<ChannelDTO, int> onComplete = null)
        {
            this._ChannelId = channelId;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    JObject pakToSend = new JObject();
                    pakToSend[JsonKeys.ChannelID] = _ChannelId;
                    string packetId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_CHANNEL_DETAILS;

                    JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);

#if CHAT_LOG
                    log.Debug("CHANNEL DETAILS => " + _JobjFromResponse);
#endif

                    if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                    {
                        ChannelDTO channelDTO = null;
                        if (_JobjFromResponse[JsonKeys.ChannelDTO] != null)
                        {
                            channelDTO = ChannelSignalHandler.MakeChannelDTO((JObject)_JobjFromResponse[JsonKeys.ChannelDTO]);
                        }

                        if (_OnComplete != null)
                        {
                            _OnComplete(channelDTO);
                        }
                    }
                    else
                    {
                        if (_OnComplete != null)
                        {
                            _OnComplete(null);
                        }
                    }
                }
                else
                {
                    log.Error("Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);

                    if (_OnComplete != null)
                    {
                        _OnComplete(null);
                    }
                }
            }
            catch (Exception ex)
            {
                if (_OnComplete != null)
                {
                    _OnComplete(null);
                }
                log.Error(ex.Message + ex.StackTrace);
            }
        }
    }
}

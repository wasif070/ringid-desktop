﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using stdole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdUpdateChannelInfo
    {
        private Guid _ChannelId;
        private string _ChannelTitle;
        private string _ChannelDescription;
        private string _Country;
        private List<ChannelCategoryDTO> _ChannelCategoryList;
        private Func<bool, int> _Oncomplete;

        public ThrdUpdateChannelInfo(Guid id, string title, string country, string description, List<ChannelCategoryDTO> ctgList, Func<bool, int> oncomplete = null)
        {
            this._ChannelId = id;
            this._ChannelTitle = title;
            this._Country = country;
            this._ChannelDescription = description;
            this._ChannelCategoryList = ctgList;
            this._Oncomplete = oncomplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.Title] = _ChannelTitle;
            pakToSend[JsonKeys.Country] = _Country;
            pakToSend[JsonKeys.Description] = _ChannelDescription;
            JArray catArray = new JArray();
            if (_ChannelCategoryList != null)
            {
                foreach (ChannelCategoryDTO channelDTO in _ChannelCategoryList)
                {
                    JObject obj = new JObject();
                    obj[JsonKeys.StreamCategoryId] = channelDTO.CategoryID;
                    obj[JsonKeys.Category] = channelDTO.CategoryName;
                    catArray.Add(obj);
                }
            }

            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.ChannelCatetoryList] = catArray;
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_UPDATE_CHANNEL_INFO;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(true);
                }
            }
            else
            {
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(false);
                }
            }
        }

    }
}

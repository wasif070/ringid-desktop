﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdSubscribeUnsubscribeChannel
    {
        private Guid _ChannelId;
        private int _SubUnSubValue;
        private Func<bool, Guid, int> _Oncomplete;

        public ThrdSubscribeUnsubscribeChannel(Guid channelId, int subUnSubValue, Func<bool, Guid, int> oncomplete = null)
        {
            this._ChannelId = channelId;
            this._SubUnSubValue = subUnSubValue;
            this._Oncomplete = oncomplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.ChannelSubscrbUnSubscrbValue] = _SubUnSubValue;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_SUBSCRIBE_UNSUBSCRIBE_CHANNEL;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                Guid channelId = _JobjFromResponse[JsonKeys.ChannelID] != null ? (Guid)_JobjFromResponse[JsonKeys.ChannelID] : Guid.Empty;
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(true, channelId);
                }
            }
            else
            {
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(false, Guid.Empty);
                }
            }
        }

    }
}

<<<<<<< HEAD:View/Utility/Channel/ThrdAddMediaToChannel.cs
﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.Utility.WPFMessageBox;

namespace View.Utility.Channel
{
    public class ThrdAddMediaToChannel
    {
        private Guid _ChannelId;
        private int _MediaType;
        private List<ChannelMediaDTO> _ChannelMediaList;
        private Func<bool, string, int> _OnComplete;

        public ThrdAddMediaToChannel(Guid channelId, int mediaType, List<ChannelMediaDTO> channelMediaList, Func<bool, string, int> onComplete)
        {
            this._ChannelId = channelId;
            this._MediaType = mediaType;
            this._ChannelMediaList = channelMediaList;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            //pakToSend[JsonKeys.MediaType] = _MediaType;

            JArray jarray = new JArray();
            if (_ChannelMediaList != null)
            {
                foreach (ChannelMediaDTO mediaDTO in _ChannelMediaList)
                {
                    JObject mediaObj = new JObject();
                    mediaObj[JsonKeys.ChannelID] = mediaDTO.ChannelID;
                    mediaObj[JsonKeys.ChannelOwnerID] = mediaDTO.OwnerID;
                    mediaObj[JsonKeys.Title] = mediaDTO.Title;
                    mediaObj[JsonKeys.ChannelMediaArtist] = mediaDTO.Artist;
                    //mediaObj[JsonKeys.ChannelMediaID] = mediaDTO.MediaID;
                    mediaObj[JsonKeys.ChannelMediaType] = mediaDTO.MediaType;
                    mediaObj[JsonKeys.ChannelType] = mediaDTO.ChannelType;
                    mediaObj[JsonKeys.ChannelMediaUrl] = mediaDTO.MediaUrl;
                    mediaObj[JsonKeys.ChannelThumbImageUrl] = mediaDTO.ThumbImageUrl;
                    mediaObj[JsonKeys.ChannelThumbImageWidth] = mediaDTO.ThumbImageWidth;
                    mediaObj[JsonKeys.ChannelThumbImageHeight] = mediaDTO.ThumbImageHeight;
                    mediaObj[JsonKeys.MediaDuration] = mediaDTO.Duration;
                    mediaObj[JsonKeys.Status] = mediaDTO.MediaStatus;
                    mediaObj[JsonKeys.Description] = mediaDTO.Description;
                    mediaObj[JsonKeys.ChannelMediaStartTime] = 0;
                    jarray.Add(mediaObj);
                }
            }
            pakToSend[JsonKeys.ChannelMediaList] = jarray;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_MEDIA_TO_CHANNEL;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                if (this._OnComplete != null)
                {
                    this._OnComplete(true, null);
                }
            }
            else
            {
                string message = _JobjFromResponse[JsonKeys.MSG] != null ? (string)_JobjFromResponse[JsonKeys.MSG] : string.Empty;
                if (this._OnComplete != null)
                {
                    this._OnComplete(false, message);
                }
            }
        }

    }
}
=======
﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.Utility.WPFMessageBox;

namespace View.Utility.Channel
{
    public class ThrdAddUploadedChannelMedia
    {
        private List<ChannelMediaDTO> _ChannelMediaList;
        private Func<bool, string, int> _OnComplete;

        public ThrdAddUploadedChannelMedia( List<ChannelMediaDTO> channelMediaList, Func<bool, string, int> onComplete)
        {
            this._ChannelMediaList = channelMediaList;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();

            JArray jarray = new JArray();
            if (_ChannelMediaList != null)
            {
                foreach (ChannelMediaDTO mediaDTO in _ChannelMediaList)
                {
                    JObject mediaObj = new JObject();
                    mediaObj[JsonKeys.Title] = mediaDTO.Title;
                    mediaObj[JsonKeys.ChannelMediaArtist] = mediaDTO.Artist;
                    mediaObj[JsonKeys.ChannelMediaType] = mediaDTO.MediaType;
                    mediaObj[JsonKeys.ChannelMediaUrl] = mediaDTO.MediaUrl;
                    mediaObj[JsonKeys.ChannelThumbImageUrl] = mediaDTO.ThumbImageUrl;
                    mediaObj[JsonKeys.ChannelThumbImageWidth] = mediaDTO.ThumbImageWidth;
                    mediaObj[JsonKeys.ChannelThumbImageHeight] = mediaDTO.ThumbImageHeight;
                    mediaObj[JsonKeys.MediaDuration] = mediaDTO.Duration;
                    mediaObj[JsonKeys.Description] = mediaDTO.Description;
                    jarray.Add(mediaObj);
                }
            }
            pakToSend[JsonKeys.ChannelMediaList] = jarray;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_UPLOADED_CHANNEL_MEDIA;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                if (this._OnComplete != null)
                {
                    this._OnComplete(true, null);
                }
            }
            else
            {
                string message = _JobjFromResponse[JsonKeys.MSG] != null ? (string)_JobjFromResponse[JsonKeys.MSG] : string.Empty;
                if (this._OnComplete != null)
                {
                    this._OnComplete(false, message);
                }
            }
        }

    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596:View/Utility/Channel/ThrdAddUploadedChannelMedia.cs

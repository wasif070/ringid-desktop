﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.UI.Channel;

namespace View.Utility.Channel
{
    public class MyChannelSwitcher
    {
        public static UCMyChannelWrapper pageSwitcher;

        public static void Switch(int type)
        {
            pageSwitcher.Navigate(type);
        }

        public static void Switch(int type, object state)
        {
            pageSwitcher.Navigate(type, state);
        }
    }
}

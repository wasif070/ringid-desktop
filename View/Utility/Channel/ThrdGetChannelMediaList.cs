<<<<<<< HEAD
﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdGetChannelMediaList
    {
        private Guid _ChannelId;
        private int _MediaType;
        private int _Start;
        private int _Limit;
        private int _MediaStatus;
        private Func<bool, int> _Oncomplete;


        public ThrdGetChannelMediaList(Guid channelId, int mediaType, int start, int limit, int mediaStatus, Func<bool, int> oncomplete = null)
        {
            this._ChannelId = channelId;
            this._MediaType = mediaType;
            this._Start = start;
            this._Limit = limit;
            this._MediaStatus = mediaStatus;
            this._Oncomplete = oncomplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.MediaType] = _MediaType;
            pakToSend[JsonKeys.StartLimit] = _Start;
            pakToSend[JsonKeys.Limit] = _Limit;
            pakToSend[JsonKeys.ChannelMediaStatus] = _MediaStatus;

            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_CHANNEL_MEDIA_LIST;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._Oncomplete != null)
            {
                this._Oncomplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }

        }
    }
}
=======
﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdGetChannelMediaList
    {
        private Guid _ChannelId;
        private int _MediaType;
        private int _Start;
        private Guid _Pivot;
        private int _Limit;
        private int _MediaStatus;
        private Func<bool, int> _Oncomplete;


        public ThrdGetChannelMediaList(Guid channelId, int mediaType, int start, Guid pivot, int limit, int mediaStatus, Func<bool, int> oncomplete = null)
        {
            this._ChannelId = channelId;
            this._MediaType = mediaType;
            this._Start = start;
            this._Pivot = pivot;
            this._Limit = limit;
            this._MediaStatus = mediaStatus;
            this._Oncomplete = oncomplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.MediaType] = _MediaType;
            pakToSend[JsonKeys.StartLimit] = _Start;
            if (_Start > 0)
            {
                pakToSend[JsonKeys.PivotUUID] = _Pivot;
            }
            pakToSend[JsonKeys.Limit] = _Limit;
            pakToSend[JsonKeys.ChannelMediaStatus] = _MediaStatus;

            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_CHANNEL_MEDIA_LIST;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._Oncomplete != null)
            {
                this._Oncomplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }

        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

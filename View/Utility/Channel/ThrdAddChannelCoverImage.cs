﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Channel
{
    public class ThrdAddChannelCoverImage
    {
        private Guid _ChannelId;
        private string _CoverImageUrl;
        private int _CoverImageHeight;
        private int _CoverImageWidth;
        private int _CoverImagePosX;
        private int _CoverImagePosY;
        private Func<bool, int> _Oncomplete;

        public ThrdAddChannelCoverImage(Guid channelId, string cImgUrl, int cImgH, int cImgW, int cImgPosX, int cImgPosY, Func<bool, int> onComplete = null)
        {
            this._ChannelId = channelId;
            this._CoverImageUrl = cImgUrl;
            this._CoverImageHeight = cImgH;
            this._CoverImageWidth = cImgW;
            this._CoverImagePosX = cImgPosX;
            this._CoverImagePosY = cImgPosY;
            this._Oncomplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ChannelID] = _ChannelId;
            pakToSend[JsonKeys.CoverImage] = _CoverImageUrl;
            pakToSend[JsonKeys.CoverImageHeight] = _CoverImageHeight;
            pakToSend[JsonKeys.CoverImageWidth] = _CoverImageWidth;
            pakToSend[JsonKeys.CropImageX] = _CoverImagePosX;
            pakToSend[JsonKeys.CropImageY] = _CoverImagePosY;

            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_CHANNEL_COVER_IMAGE;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(true);
                }
            }
            else
            {
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(false);
                }
            }

        }
    }
}

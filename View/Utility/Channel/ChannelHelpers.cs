<<<<<<< HEAD
﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using View.BindingModels;
using View.UI;
using View.UI.Channel;
using View.UI.StreamAndChannel;
using View.Utility.Chat;

namespace View.Utility.Channel
{
    public static class ChannelHelpers
    {
        private static ILog log = LogManager.GetLogger(typeof(ChannelHelpers).Name);

        private static readonly object _PROCESS_SYNC_LOCK = new object();

        public static UCChannelViewer GetCurruentChannelViewer()
        {
            if (UCStreamAndChannelViewer.ChannelViewer != null
                && UCStreamAndChannelViewer.ChannelViewer.IS_INITIALIZED)
            {
                return UCStreamAndChannelViewer.ChannelViewer;
            }
            return null;
        }

        public static long GetPublisherID(Guid channelId, bool isHD = false)
        {
            try
            {
                byte[] bytes = channelId.ToByteArray();
                byte[] b = new byte[8];
                b[0] = isHD ? (byte)1 : bytes[8];
                b[1] = bytes[9];
                b[2] = bytes[10];
                b[3] = bytes[11];
                b[4] = bytes[12];
                b[5] = bytes[13];
                b[6] = bytes[14];
                b[7] = bytes[15];

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(b);
                }

                long result = BitConverter.ToInt64(b, 0);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static void AddChannelCategory(List<ChannelCategoryDTO> catList)
        {
            try
            {
                foreach (ChannelCategoryDTO catDTO in catList)
                {
                    ChannelCategoryModel model = ChannelViewModel.Instance.ChannelCategoryList.FirstOrDefault(P => P.CategoryID == catDTO.CategoryID);
                    if (model == null)
                    {
                        model = new ChannelCategoryModel { CategoryID = catDTO.CategoryID, CategoryName = catDTO.CategoryName };

                        int max = ChannelViewModel.Instance.ChannelCategoryList.Count;
                        int min = 0;
                        int pivot;

                        while (max > min)
                        {
                            pivot = (min + max) / 2;
                            if (model.CategoryID > ChannelViewModel.Instance.ChannelCategoryList.ElementAt(pivot).CategoryID)
                            {
                                min = pivot + 1;
                            }
                            else
                            {
                                max = pivot;
                            }
                        }
                        ChannelViewModel.Instance.ChannelCategoryList.InvokeInsert(min, model);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AddChannelCategory() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoFeaturedChannelList(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelViewModel.Instance.ChannelFeatureList;
                lock (_PROCESS_SYNC_LOCK)
                {
                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();
                    if (newModel != null)
                    {
                        newModel.LoadData(newDTO);
                    }
                    else
                    {
                        viewerModelList.InvokeAdd(new ChannelModel(newDTO));
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoFeaturedChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoMostViewedChannelList(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelViewModel.Instance.ChannelMostViewList;
                lock (_PROCESS_SYNC_LOCK)
                {

                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();

                    ChannelModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.ViewCount > firstModel.ViewCount)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.ViewCount < lastModel.ViewCount)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotCount = viewerModelList.ElementAt(pivot).ViewCount;
                                    if (newModel.ViewCount > pivotCount)
                                    {
                                        max = pivot;
                                    }
                                    else
                                    {
                                        min = pivot + 1;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.ViewCount > firstModel.ViewCount)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.ViewCount > newModel.ViewCount)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.ViewCount > viewerModelList.ElementAt(pivot).ViewCount)
                                {
                                    max = pivot;
                                }
                                else
                                {
                                    min = pivot + 1;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoMostViewedChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoMyChannelList(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelViewModel.Instance.MyChannelList;
                lock (_PROCESS_SYNC_LOCK)
                {

                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();

                    ChannelModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.CreationTime > firstModel.CreationTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.CreationTime < lastModel.CreationTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotCount = viewerModelList.ElementAt(pivot).CreationTime;
                                    if (newModel.CreationTime > pivotCount)
                                    {
                                        max = pivot;
                                    }
                                    else
                                    {
                                        min = pivot + 1;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.CreationTime > firstModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.CreationTime > newModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.CreationTime > viewerModelList.ElementAt(pivot).CreationTime)
                                {
                                    max = pivot;
                                }
                                else
                                {
                                    min = pivot + 1;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoMyChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoFollowingChannelList(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelViewModel.Instance.ChannelFollowingList;
                lock (_PROCESS_SYNC_LOCK)
                {
                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();
                    ChannelModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.CreationTime > firstModel.CreationTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.CreationTime < lastModel.CreationTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotCount = viewerModelList.ElementAt(pivot).CreationTime;
                                    if (newModel.CreationTime > pivotCount)
                                    {
                                        max = pivot;
                                    }
                                    else
                                    {
                                        min = pivot + 1;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.CreationTime > firstModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.CreationTime > newModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.CreationTime > viewerModelList.ElementAt(pivot).CreationTime)
                                {
                                    max = pivot;
                                }
                                else
                                {
                                    min = pivot + 1;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoChannelFollowingList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoSearchChannelList(ChannelDTO newDTO, ObservableCollection<ChannelModel> viewerModelList)
        {
            try
            {
                lock (_PROCESS_SYNC_LOCK)
                {
                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();
                    if (newModel != null)
                    {
                        newModel.LoadData(newDTO);
                    }
                    else
                    {
                        viewerModelList.InvokeAdd(new ChannelModel(newDTO));
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoSearchChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoChannelPlayList(ChannelMediaDTO newMediaDTO, ObservableCollection<ChannelMediaModel> viewerModelList)
=======
﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using View.BindingModels;
using View.UI;
using View.UI.Channel;
using View.UI.StreamAndChannel;
using View.Utility.Chat;

namespace View.Utility.Channel
{
    public static class ChannelHelpers
    {
        private static ILog log = LogManager.GetLogger(typeof(ChannelHelpers).Name);

        private static readonly object _PROCESS_SYNC_LOCK = new object();

        public static UCChannelViewer GetCurruentChannelViewer()
        {
            if (UCStreamAndChannelViewer.ChannelViewer != null
                && UCStreamAndChannelViewer.ChannelViewer.IS_INITIALIZED)
            {
                return UCStreamAndChannelViewer.ChannelViewer;
            }
            return null;
        }

        public static long GetPublisherID(Guid channelId, bool isHD = false)
        {
            try
            {
                byte[] bytes = channelId.ToByteArray();
                byte[] b = new byte[8];
                b[0] = isHD ? (byte)1 : bytes[8];
                b[1] = bytes[9];
                b[2] = bytes[10];
                b[3] = bytes[11];
                b[4] = bytes[12];
                b[5] = bytes[13];
                b[6] = bytes[14];
                b[7] = bytes[15];

                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(b);
                }

                long result = BitConverter.ToInt64(b, 0);
                return result;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static void AddChannelCategory(List<ChannelCategoryDTO> catList)
        {
            try
            {
                foreach (ChannelCategoryDTO catDTO in catList)
                {
                    ChannelCategoryModel model = ChannelViewModel.Instance.ChannelCategoryList.FirstOrDefault(P => P.CategoryID == catDTO.CategoryID);
                    if (model == null)
                    {
                        model = new ChannelCategoryModel { CategoryID = catDTO.CategoryID, CategoryName = catDTO.CategoryName };

                        int max = ChannelViewModel.Instance.ChannelCategoryList.Count;
                        int min = 0;
                        int pivot;

                        while (max > min)
                        {
                            pivot = (min + max) / 2;
                            if (model.CategoryID > ChannelViewModel.Instance.ChannelCategoryList.ElementAt(pivot).CategoryID)
                            {
                                min = pivot + 1;
                            }
                            else
                            {
                                max = pivot;
                            }
                        }
                        ChannelViewModel.Instance.ChannelCategoryList.InvokeInsert(min, model);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AddChannelCategory() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoFeaturedChannelList(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelViewModel.Instance.ChannelFeatureList;
                lock (_PROCESS_SYNC_LOCK)
                {
                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();
                    if (newModel != null)
                    {
                        newModel.LoadData(newDTO);
                    }
                    else
                    {
                        viewerModelList.InvokeAdd(new ChannelModel(newDTO));
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoFeaturedChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoMostViewedChannelList(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelViewModel.Instance.ChannelMostViewList;
                lock (_PROCESS_SYNC_LOCK)
                {

                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();

                    ChannelModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.ViewCount > firstModel.ViewCount)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.ViewCount < lastModel.ViewCount)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotCount = viewerModelList.ElementAt(pivot).ViewCount;
                                    if (newModel.ViewCount > pivotCount)
                                    {
                                        max = pivot;
                                    }
                                    else
                                    {
                                        min = pivot + 1;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.ViewCount > firstModel.ViewCount)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.ViewCount > newModel.ViewCount)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.ViewCount > viewerModelList.ElementAt(pivot).ViewCount)
                                {
                                    max = pivot;
                                }
                                else
                                {
                                    min = pivot + 1;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoMostViewedChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoMyChannelList(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelViewModel.Instance.MyChannelList;
                lock (_PROCESS_SYNC_LOCK)
                {

                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();

                    ChannelModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.CreationTime > firstModel.CreationTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.CreationTime < lastModel.CreationTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotCount = viewerModelList.ElementAt(pivot).CreationTime;
                                    if (newModel.CreationTime > pivotCount)
                                    {
                                        max = pivot;
                                    }
                                    else
                                    {
                                        min = pivot + 1;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.CreationTime > firstModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.CreationTime > newModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.CreationTime > viewerModelList.ElementAt(pivot).CreationTime)
                                {
                                    max = pivot;
                                }
                                else
                                {
                                    min = pivot + 1;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoMyChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoFollowingChannelList(ChannelDTO newDTO)
        {
            try
            {
                ObservableCollection<ChannelModel> viewerModelList = ChannelViewModel.Instance.ChannelFollowingList;
                lock (_PROCESS_SYNC_LOCK)
                {
                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();
                    ChannelModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.CreationTime > firstModel.CreationTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.CreationTime < lastModel.CreationTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotCount = viewerModelList.ElementAt(pivot).CreationTime;
                                    if (newModel.CreationTime > pivotCount)
                                    {
                                        max = pivot;
                                    }
                                    else
                                    {
                                        min = pivot + 1;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.CreationTime > firstModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.CreationTime > newModel.CreationTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.CreationTime > viewerModelList.ElementAt(pivot).CreationTime)
                                {
                                    max = pivot;
                                }
                                else
                                {
                                    min = pivot + 1;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoChannelFollowingList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoSearchChannelList(ChannelDTO newDTO, ObservableCollection<ChannelModel> viewerModelList)
        {
            try
            {
                lock (_PROCESS_SYNC_LOCK)
                {
                    ChannelModel newModel = viewerModelList.Where(P => P.ChannelID == newDTO.ChannelID).FirstOrDefault();
                    if (newModel != null)
                    {
                        newModel.LoadData(newDTO);
                    }
                    else
                    {
                        viewerModelList.InvokeAdd(new ChannelModel(newDTO));
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoSearchChannelList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoUploadedChannelMediaList(ChannelMediaDTO newMediaDTO, ObservableCollection<ChannelMediaModel> viewerModelList, bool atTop = false)
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        {
            try
            {
                lock (_PROCESS_SYNC_LOCK)
                {
                    ChannelMediaModel newModel = viewerModelList.Where(P => P.MediaID == newMediaDTO.MediaID).FirstOrDefault();
<<<<<<< HEAD
                    ChannelMediaModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelMediaModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelMediaModel(newMediaDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.StartTime > firstModel.StartTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.StartTime < lastModel.StartTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotTime = viewerModelList.ElementAt(pivot).StartTime;
                                    if (newModel.StartTime < pivotTime)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newMediaDTO.StartTime > firstModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newMediaDTO);
                        }
                        else if (newMediaDTO.StartTime > newModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newMediaDTO.StartTime < viewerModelList.ElementAt(pivot).StartTime)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newMediaDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoStreamFollwingList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoChannelNowPlayingList(ChannelMediaDTO mediaDTO)
        {
            try
            {
                lock (_PROCESS_SYNC_LOCK)
                {
                    ObservableCollection<ChannelMediaModel> viewerModelList = ChannelViewModel.Instance.GetChannelNowPlayingList(mediaDTO.ChannelID);
                    if (viewerModelList.Count > 0)
                    {
                        viewerModelList[0].LoadData(mediaDTO);
                    }
                    else
                    {
                        ChannelMediaModel model = new ChannelMediaModel(mediaDTO);
                        model.IsViewOpened = true;
                        viewerModelList.InvokeAdd(model);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoChannelMediaPlayingList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddMediaIntoMediaList(List<ChannelMediaDTO> mediaDTOList, int channelMediaStatus)
        {
            if (channelMediaStatus == ChannelConstants.MEDIA_STATUS_UPLOADED)
            {
                if (UCMyChannelMainPanel.MyChannelUploadsPanel != null)
                {
                    foreach (ChannelMediaDTO mediaDTO in mediaDTOList)
                    {
                        ChannelMediaModel oldMediaModel = UCMyChannelMainPanel.MyChannelUploadsPanel.SingleChannelInfoModel.MediaList.Where(P => P.MediaID == mediaDTO.MediaID).FirstOrDefault();
                        if (oldMediaModel == null)
                        {
                            ChannelMediaModel newMediaModel = new ChannelMediaModel(mediaDTO);
                            UCMyChannelMainPanel.MyChannelUploadsPanel.SingleChannelInfoModel.MediaList.InvokeAdd(newMediaModel);
                        }
                        else
                        {
                            oldMediaModel.LoadData(mediaDTO);
                        }
                    }
                }
            }
            else if (channelMediaStatus == ChannelConstants.MEDIA_STATUS_PENDING)
            {
                if (UCMyChannelMainPanel.MyChannelPendingPanel != null)
                {
                    foreach (ChannelMediaDTO mediaDTO in mediaDTOList)
                    {
                        ChannelMediaModel oldMediaModel = UCMyChannelMainPanel.MyChannelPendingPanel.PendingMediaList.Where(P => P.MediaID == mediaDTO.MediaID).FirstOrDefault();
                        if (oldMediaModel == null)
                        {
                            ChannelMediaModel newMediaModel = new ChannelMediaModel(mediaDTO);
                            UCMyChannelMainPanel.MyChannelPendingPanel.PendingMediaList.InvokeAdd(newMediaModel);
                        }
                        else
                        {
                            oldMediaModel.LoadData(mediaDTO);
                        }
                    }
                }
            }
        }

        public static void ChangeChannelViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl, int paddingTop)
        {
            try
            {
                //double offset = scrollViewer.VerticalOffset - paddingTop;
                //double height = itemsControl.ActualHeight - scrollViewer.ActualHeight > 0 ? scrollViewer.ActualHeight : itemsControl.ActualHeight;
                //double x = (scrollViewer.ActualWidth - 615) / 2;
                //double y = offset + height;
                double offset = scrollViewer.VerticalOffset;
                double x = 25;
                double height = itemsControl.ActualHeight;
                double y = itemsControl.ActualHeight;

                if ((itemsControl.ActualHeight + paddingTop) - scrollViewer.ActualHeight > 0)
                {
                    height = scrollViewer.ActualHeight;
                    y = height + offset - paddingTop;
                    y = y > itemsControl.ActualHeight ? itemsControl.ActualHeight : (y < 0 ? 0 : y);
                }

                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y > itemsControl.ActualHeight ? itemsControl.ActualHeight : y));
                if (hitTest != null)
                {
                    int maxIndex = itemsControl.Items.Count - 1;
                    int startIndex = maxIndex;

                    ContentPresenter presenter = ChatHelpers.GetContentPresenterFromEvent(hitTest.VisualHit, itemsControl) as ContentPresenter;
                    if (presenter != null && !(presenter.Content is ChannelModel))
                    {
                        presenter = HelperMethods.FindVisualParent<ContentPresenter>(presenter);
                    }

                    if (presenter != null)
                    {
                        startIndex = itemsControl.ItemContainerGenerator.IndexFromContainer(presenter);
                        if (startIndex < 0) return;
                    }

                    int overLimit = 2;
                    int index = (startIndex + 2 > maxIndex ? maxIndex : startIndex + 2);

                    int i = maxIndex - index > 3 ? index + 3 : maxIndex;
                    for (; i > index; i--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        ChannelModel model = (ChannelModel)presenter.Content;
                        model.IsViewOpened = false;
                    }

                    for (int overflow = 0; index >= 0; index--)
                    {
                        if (height <= 0 && (++overflow) >= overLimit) break;

                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(index);
                        if (temp == null)
                            break;

                        presenter = temp as ContentPresenter;
                        ChannelModel model = (ChannelModel)presenter.Content;
                        model.IsViewOpened = true;

                        if (index <= startIndex)
                        {
                            height -= presenter.ActualHeight;
                        }
                    }

                    int count = index > 3 ? index - 3 : 0;
                    for (int j = index; j >= count; j--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(j);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        ChannelModel model = (ChannelModel)presenter.Content;
                        model.IsViewOpened = false;
                    }
=======
                    if (newModel != null)
                    {
                        newModel.LoadData(newMediaDTO);
                    }
                    else
                    {
                        if (atTop)
                        {
                            viewerModelList.InvokeInsert(0, new ChannelMediaModel(newMediaDTO));
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(new ChannelMediaModel(newMediaDTO));
                        }
                    }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                }
            }
            catch (Exception ex)
            {
<<<<<<< HEAD
                log.Error("Error: ChangeChannelViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ChangeChannelMediaViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl, int paddingTop)
        {
            try
            {
                //double offset = scrollViewer.VerticalOffset - paddingTop;
                //double height = itemsControl.ActualHeight - scrollViewer.ActualHeight > 0 ? scrollViewer.ActualHeight : itemsControl.ActualHeight;
                //double x = (scrollViewer.ActualWidth - 615) / 2;
                //double y = offset + height;
=======
                log.Error("AddIntoUploadedChannelMediaList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoChannelPlayList(ChannelMediaDTO newMediaDTO, ObservableCollection<ChannelMediaModel> viewerModelList)
        {
            try
            {
                lock (_PROCESS_SYNC_LOCK)
                {
                    ChannelMediaModel newModel = viewerModelList.Where(P => P.MediaID == newMediaDTO.MediaID).FirstOrDefault();
                    ChannelMediaModel firstModel = viewerModelList.FirstOrDefault();
                    ChannelMediaModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new ChannelMediaModel(newMediaDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.StartTime > firstModel.StartTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.StartTime < lastModel.StartTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotTime = viewerModelList.ElementAt(pivot).StartTime;
                                    if (newModel.StartTime < pivotTime)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newMediaDTO.StartTime > firstModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newMediaDTO);
                        }
                        else if (newMediaDTO.StartTime > newModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newMediaDTO.StartTime < viewerModelList.ElementAt(pivot).StartTime)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newMediaDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoStreamFollwingList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoChannelNowPlayingList(ChannelMediaDTO mediaDTO)
        {
            try
            {
                lock (_PROCESS_SYNC_LOCK)
                {
                    ObservableCollection<ChannelMediaModel> viewerModelList = ChannelViewModel.Instance.GetChannelNowPlayingList(mediaDTO.ChannelID);
                    if (viewerModelList.Count > 0)
                    {
                        viewerModelList[0].LoadData(mediaDTO);
                    }
                    else
                    {
                        ChannelMediaModel model = new ChannelMediaModel(mediaDTO);
                        model.IsViewOpened = true;
                        viewerModelList.InvokeAdd(model);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoChannelMediaPlayingList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddMediaIntoMediaList(List<ChannelMediaDTO> mediaDTOList, int channelMediaStatus)
        {
            if (channelMediaStatus == ChannelConstants.MEDIA_STATUS_PENDING)
            {
                if (UCMyChannelMainPanel.MyChannelPendingPanel != null)
                {
                    foreach (ChannelMediaDTO mediaDTO in mediaDTOList)
                    {
                        ChannelMediaModel oldMediaModel = UCMyChannelMainPanel.MyChannelPendingPanel.PendingMediaList.Where(P => P.MediaID == mediaDTO.MediaID).FirstOrDefault();
                        if (oldMediaModel == null)
                        {
                            ChannelMediaModel newMediaModel = new ChannelMediaModel(mediaDTO);
                            UCMyChannelMainPanel.MyChannelPendingPanel.PendingMediaList.InvokeAdd(newMediaModel);
                        }
                        else
                        {
                            oldMediaModel.LoadData(mediaDTO);
                        }
                    }
                }
            }
        }

        public static void ChangeChannelViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl, int paddingTop)
        {
            try
            {
                //double offset = scrollViewer.VerticalOffset - paddingTop;
                //double height = itemsControl.ActualHeight - scrollViewer.ActualHeight > 0 ? scrollViewer.ActualHeight : itemsControl.ActualHeight;
                //double x = (scrollViewer.ActualWidth - 615) / 2;
                //double y = offset + height;
                double offset = scrollViewer.VerticalOffset;
                double x = 25;
                double height = itemsControl.ActualHeight;
                double y = itemsControl.ActualHeight;

                if ((itemsControl.ActualHeight + paddingTop) - scrollViewer.ActualHeight > 0)
                {
                    height = scrollViewer.ActualHeight;
                    y = height + offset - paddingTop;
                    y = y > itemsControl.ActualHeight ? itemsControl.ActualHeight : (y < 0 ? 0 : y);
                }

                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y > itemsControl.ActualHeight ? itemsControl.ActualHeight : y));
                if (hitTest != null)
                {
                    int maxIndex = itemsControl.Items.Count - 1;
                    int startIndex = maxIndex;

                    ContentPresenter presenter = ChatHelpers.GetContentPresenterFromEvent(hitTest.VisualHit, itemsControl) as ContentPresenter;
                    if (presenter != null && !(presenter.Content is ChannelModel))
                    {
                        presenter = HelperMethods.FindVisualParent<ContentPresenter>(presenter);
                    }

                    if (presenter != null)
                    {
                        startIndex = itemsControl.ItemContainerGenerator.IndexFromContainer(presenter);
                        if (startIndex < 0) return;
                    }

                    int overLimit = 2;
                    int index = (startIndex + 2 > maxIndex ? maxIndex : startIndex + 2);

                    int i = maxIndex - index > 3 ? index + 3 : maxIndex;
                    for (; i > index; i--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        ChannelModel model = (ChannelModel)presenter.Content;
                        model.IsViewOpened = false;
                    }

                    for (int overflow = 0; index >= 0; index--)
                    {
                        if (height <= 0 && (++overflow) >= overLimit) break;

                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(index);
                        if (temp == null)
                            break;

                        presenter = temp as ContentPresenter;
                        ChannelModel model = (ChannelModel)presenter.Content;
                        model.IsViewOpened = true;

                        if (index <= startIndex)
                        {
                            height -= presenter.ActualHeight;
                        }
                    }

                    int count = index > 3 ? index - 3 : 0;
                    for (int j = index; j >= count; j--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(j);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        ChannelModel model = (ChannelModel)presenter.Content;
                        model.IsViewOpened = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeChannelViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ChangeChannelMediaViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl, int paddingTop)
        {
            try
            {
                //double offset = scrollViewer.VerticalOffset - paddingTop;
                //double height = itemsControl.ActualHeight - scrollViewer.ActualHeight > 0 ? scrollViewer.ActualHeight : itemsControl.ActualHeight;
                //double x = (scrollViewer.ActualWidth - 615) / 2;
                //double y = offset + height;
                double offset = scrollViewer.VerticalOffset;
                double x = 25;
                double height = itemsControl.ActualHeight;
                double y = itemsControl.ActualHeight;

                if ((itemsControl.ActualHeight + paddingTop) - scrollViewer.ActualHeight > 0)
                {
                    height = scrollViewer.ActualHeight;
                    y = height + offset - paddingTop;
                    y = y > itemsControl.ActualHeight ? itemsControl.ActualHeight : (y < 0 ? 0 : y);
                }

                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y > itemsControl.ActualHeight ? itemsControl.ActualHeight : y));
                if (hitTest != null)
                {
                    int maxIndex = itemsControl.Items.Count - 1;
                    int startIndex = maxIndex;

                    ContentPresenter presenter = ChatHelpers.GetContentPresenterFromEvent(hitTest.VisualHit, itemsControl) as ContentPresenter;
                    if (presenter != null && !(presenter.Content is ChannelMediaModel))
                    {
                        presenter = HelperMethods.FindVisualParent<ContentPresenter>(presenter);
                    }

                    if (presenter != null)
                    {
                        startIndex = itemsControl.ItemContainerGenerator.IndexFromContainer(presenter);
                        if (startIndex < 0) return;
                    }

                    int overLimit = 2;
                    int index = (startIndex + 2 > maxIndex ? maxIndex : startIndex + 2);

                    int i = maxIndex - index > 3 ? index + 3 : maxIndex;
                    for (; i > index; i--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        ChannelMediaModel model = (ChannelMediaModel)presenter.Content;
                        model.IsViewOpened = false;
                    }

                    for (int overflow = 0; index >= 0; index--)
                    {
                        if (height <= 0 && (++overflow) >= overLimit) break;

                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(index);
                        if (temp == null)
                            break;

                        presenter = temp as ContentPresenter;
                        ChannelMediaModel model = (ChannelMediaModel)presenter.Content;
                        model.IsViewOpened = true;

                        if (index <= startIndex)
                        {
                            height -= presenter.ActualHeight;
                        }
                    }

                    int count = index > 3 ? index - 3 : 0;
                    for (int j = index; j >= count; j--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(j);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        ChannelMediaModel model = (ChannelMediaModel)presenter.Content;
                        model.IsViewOpened = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeChannelMediaViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ChangeChannelMediaViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl, int paddingTop, int numberOfColumn)
        {
            try
            {
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                double offset = scrollViewer.VerticalOffset;
                double x = 25;
                double height = itemsControl.ActualHeight;
                double y = itemsControl.ActualHeight;

                if ((itemsControl.ActualHeight + paddingTop) - scrollViewer.ActualHeight > 0)
                {
                    height = scrollViewer.ActualHeight;
                    y = height + offset - paddingTop;
                    y = y > itemsControl.ActualHeight ? itemsControl.ActualHeight : (y < 0 ? 0 : y);
                }

<<<<<<< HEAD
                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y > itemsControl.ActualHeight ? itemsControl.ActualHeight : y));
=======
                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y));
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                if (hitTest != null)
                {
                    int maxIndex = itemsControl.Items.Count - 1;
                    int startIndex = maxIndex;

                    ContentPresenter presenter = ChatHelpers.GetContentPresenterFromEvent(hitTest.VisualHit, itemsControl) as ContentPresenter;
                    if (presenter != null && !(presenter.Content is ChannelMediaModel))
                    {
                        presenter = HelperMethods.FindVisualParent<ContentPresenter>(presenter);
                    }

                    if (presenter != null)
                    {
                        startIndex = itemsControl.ItemContainerGenerator.IndexFromContainer(presenter);
                        if (startIndex < 0) return;
<<<<<<< HEAD
                    }

                    int overLimit = 2;
                    int index = (startIndex + 2 > maxIndex ? maxIndex : startIndex + 2);

                    int i = maxIndex - index > 3 ? index + 3 : maxIndex;
=======

                        int mod = maxIndex % numberOfColumn;
                        int diff = maxIndex - startIndex;
                        startIndex = diff > 0 ? startIndex + (diff > mod ? mod : diff) : startIndex;
                    }

                    int overLimit = 1;
                    int index = (startIndex + numberOfColumn > maxIndex ? maxIndex : startIndex + numberOfColumn);

                    int i = maxIndex - index > numberOfColumn * 2 ? index + numberOfColumn * 2 : maxIndex;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                    for (; i > index; i--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        ChannelMediaModel model = (ChannelMediaModel)presenter.Content;
                        model.IsViewOpened = false;
<<<<<<< HEAD
=======
                        //Debug.WriteLine("HIDE => " + model.Title);
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                    }

                    for (int overflow = 0; index >= 0; index--)
                    {
<<<<<<< HEAD
                        if (height <= 0 && (++overflow) >= overLimit) break;
=======
                        if (height <= 0 && index % numberOfColumn == 0 && (++overflow) >= overLimit) break;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(index);
                        if (temp == null)
                            break;

                        presenter = temp as ContentPresenter;
                        ChannelMediaModel model = (ChannelMediaModel)presenter.Content;
                        model.IsViewOpened = true;
<<<<<<< HEAD

                        if (index <= startIndex)
=======
                        //Debug.WriteLine("SHOW => " + model.Title);

                        if (index % numberOfColumn == 0 && index <= startIndex)
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                        {
                            height -= presenter.ActualHeight;
                        }
                    }

<<<<<<< HEAD
                    int count = index > 3 ? index - 3 : 0;
=======
                    int count = index > numberOfColumn * 2 ? index - numberOfColumn * 2 : 0;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                    for (int j = index; j >= count; j--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(j);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        ChannelMediaModel model = (ChannelMediaModel)presenter.Content;
                        model.IsViewOpened = false;
<<<<<<< HEAD
=======
                        //Debug.WriteLine("HIDE => " + model.Title);
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                    }
                }
            }
            catch (Exception ex)
            {
<<<<<<< HEAD
                log.Error("Error: ChangeChannelMediaViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static ChannelDTO GetChannelDTOFromChannnelModel(ChannelModel model)
        {
            ChannelDTO chnlDTO = new ChannelDTO();
            if (model != null)
            {
                chnlDTO.ChannelID = model.ChannelID;
                chnlDTO.OwnerID = model.OwnerID;
                chnlDTO.Title = model.Title;
                chnlDTO.Description = model.Description;
                chnlDTO.ProfileImage = model.ProfileImage;
                chnlDTO.ProfileImageWidth = model.ProfileImageWidth;
                chnlDTO.ProfileImageHeight = model.ProfileImageHeight;
                chnlDTO.ProfileImageID = model.ProfileImageID;
                chnlDTO.CoverImage = model.CoverImage;
                chnlDTO.CoverImageWidth = model.CoverImageWidth;
                chnlDTO.CoverImageHeight = model.CoverImageHeight;
                chnlDTO.CoverImageID = model.CoverImageID;
                chnlDTO.CoverImageX = model.CoverImageX;
                chnlDTO.CoverImageY = model.CoverImageY;
                chnlDTO.ChannelStatus = model.ChannelStatus;
                chnlDTO.ChannelType = model.ChannelType;
                chnlDTO.SubscriberCount = model.SubscriberCount;
                chnlDTO.SubscriptionTime = model.SubscriptionTime;
                chnlDTO.ViewCount = model.ViewCount;
                chnlDTO.CreationTime = model.CreationTime;
                chnlDTO.Country = model.Country;
                chnlDTO.ChannelIP = model.ChannelIP;
                chnlDTO.ChannelPort = model.ChannelPort;
                chnlDTO.StreamIP = model.StreamIP;
                chnlDTO.StreamPort = model.StreamPort;


                if (model.CategoryList != null)
                {
                    foreach (ChannelCategoryModel ctgModdel in model.CategoryList)
                    {
                        chnlDTO.CategoryList.Add(new ChannelCategoryDTO { CategoryID = ctgModdel.CategoryID, CategoryName = ctgModdel.CategoryName });
                    }
                }

                if (model.MediaList != null)
                {
                    foreach (ChannelMediaModel mediaModel in model.MediaList)
                    {
                        chnlDTO.MediaList.Add(GetChannelMediaDtoFromChannelMediaModel(mediaModel));
                    }
                }

            }

            return chnlDTO;
        }

        public static ChannelMediaDTO GetChannelMediaDtoFromChannelModel(ChannelModel model)
        {
            ChannelMediaDTO medialDTO = new ChannelMediaDTO();
            if (model != null)
            {
                medialDTO.ChannelID = model.ChannelID;
                medialDTO.OwnerID = model.OwnerID;
                medialDTO.Description = model.Description;
                medialDTO.MediaStatus = model.ChannelStatus;
                medialDTO.ChannelType = model.ChannelType;
            }

            return medialDTO;
        }

        public static ChannelMediaDTO GetChannelMediaDtoFromSingleMediaModel(SingleMediaModel mediaModel)
        {
            ChannelMediaDTO channelMediaDTO = new ChannelMediaDTO();
            if (mediaModel != null)
            {
                channelMediaDTO.Title = mediaModel.Title;
                channelMediaDTO.Artist = mediaModel.Artist;
                channelMediaDTO.MediaType = mediaModel.MediaType;
                channelMediaDTO.MediaUrl = mediaModel.StreamUrl;
                channelMediaDTO.MediaID = mediaModel.ContentId;
                channelMediaDTO.ThumbImageUrl = mediaModel.ThumbUrl;
                channelMediaDTO.ThumbImageWidth = mediaModel.ThumbImageWidth;
                channelMediaDTO.ThumbImageHeight = mediaModel.ThumbImageHeight;
                channelMediaDTO.Duration = 1000 * mediaModel.Duration;// in ms
            }
            return channelMediaDTO;
        }

        public static ChannelMediaDTO GetChannelMediaDtoFromChannelMediaModel(ChannelMediaModel model)
        {
            ChannelMediaDTO medialDTO = new ChannelMediaDTO();
            if (model != null)
            {
                medialDTO.ChannelID = model.ChannelID;
                medialDTO.OwnerID = model.OwnerID;
                medialDTO.Title = model.Title;
                medialDTO.Artist = model.Artist;
                medialDTO.Description = model.Description;
                medialDTO.MediaID = model.MediaID;
                medialDTO.MediaType = model.MediaType;
                medialDTO.MediaUrl = model.MediaUrl;
                medialDTO.ThumbImageUrl = model.ThumbImageUrl;
                medialDTO.ThumbImageWidth = model.ThumbImageWidth;
                medialDTO.ThumbImageHeight = model.ThumbImageHeight;
                medialDTO.Duration = model.Duration;
                medialDTO.MediaStatus = model.MediaStatus;
                medialDTO.ChannelType = model.ChannelType;
                medialDTO.StartTime = model.StartTime;
            }

            return medialDTO;
        }

        public static void LoadFeaturedChannelList(bool initLoad = false, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading = true;
                    string searchParam = String.Empty;
                    int catId = 0;
                    List<int> catIds = null;

                    if (UCMiddlePanelSwitcher.View_UCStreamAndChannel != null && UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel != null)
                    {
                        searchParam = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.txtSearch.Text;
                        catId = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbChannelCategory.SelectedValue != null ? (int)UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbChannelCategory.SelectedValue : 0;
                    }

                    if (catId > 0)
                    {
                        catIds = new List<int>();
                        catIds.Add(catId);
                    }

                    new ThrdGetFeaturedChannelList(initLoad ? 0 : ChannelViewModel.Instance.ChannelFeatureList.Count, limit, searchParam, catIds, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading = false;
                log.Error("Error: LoadFeaturedChannelList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadMostViewChannelList(bool initLoad = false, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading = true;
                    string searchParam = String.Empty;
                    int catId = 0;
                    List<int> catIds = null;

                    if (UCMiddlePanelSwitcher.View_UCStreamAndChannel != null && UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel != null)
                    {
                        searchParam = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.txtSearch.Text;
                        catId = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbChannelCategory.SelectedValue != null ? (int)UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbChannelCategory.SelectedValue : 0;
                    }

                    if (catId > 0)
                    {
                        catIds = new List<int>();
                        catIds.Add(catId);
                    }

                    new ThrdGetMostViewedChannelList(initLoad ? 0 : ChannelViewModel.Instance.ChannelMostViewList.Count, limit, searchParam, catIds, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading = false;
                log.Error("Error: LoadMostViewChannelList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadSearchChannelList(bool initLoad, string searchParam, string country = null, int catId = 0, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading = true;
                    List<int> catIds = null;

                    if (catId > 0)
                    {
                        catIds = new List<int>();
                        catIds.Add(catId);
                    }

                    new ThrdSearchChannels(initLoad ? 0 : ChannelViewModel.Instance.ChannelSearchList.Count, limit, searchParam, country, catIds, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading = false;
                log.Error("Error: LoadSearchChannelList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadMyChannelList(int channelStatus, bool initLoad = false, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsMyChannelLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsMyChannelLoading = true;

                    new ThrdGetOwnChannelList(channelStatus, initLoad ? 0 : ChannelViewModel.Instance.MyChannelList.Count, limit, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsMyChannelLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsMyChannelLoading = false;
                log.Error("Error: LoadMyChannelList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadChannelFollowingList(bool initLoad = false, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsFollowingLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsFollowingLoading = true;

                    new ThrdGetFollowningChannelList(initLoad ? 0 : ChannelViewModel.Instance.ChannelFollowingList.Count, limit, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsFollowingLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsFollowingLoading = false;
                log.Error("Error: LoadChannelFollowingfList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadChannelPlayList(Guid channelId, bool initLoad = false, int limit = 10, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsPlaylistLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsPlaylistLoading = true;

                    new ThrdGetChannelPlayList(channelId, initLoad ? 0 : ChannelViewModel.Instance.GetChannelPlayList(channelId).Count, limit, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsPlaylistLoading = false;
=======
                log.Error("Error: ChangeStreamViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static ChannelDTO GetChannelDTOFromChannnelModel(ChannelModel model)
        {
            ChannelDTO chnlDTO = new ChannelDTO();
            if (model != null)
            {
                chnlDTO.ChannelID = model.ChannelID;
                chnlDTO.OwnerID = model.OwnerID;
                chnlDTO.Title = model.Title;
                chnlDTO.Description = model.Description;
                chnlDTO.ProfileImage = model.ProfileImage;
                chnlDTO.ProfileImageWidth = model.ProfileImageWidth;
                chnlDTO.ProfileImageHeight = model.ProfileImageHeight;
                chnlDTO.ProfileImageID = model.ProfileImageID;
                chnlDTO.CoverImage = model.CoverImage;
                chnlDTO.CoverImageWidth = model.CoverImageWidth;
                chnlDTO.CoverImageHeight = model.CoverImageHeight;
                chnlDTO.CoverImageID = model.CoverImageID;
                chnlDTO.CoverImageX = model.CoverImageX;
                chnlDTO.CoverImageY = model.CoverImageY;
                chnlDTO.ChannelStatus = model.ChannelStatus;
                chnlDTO.ChannelType = model.ChannelType;
                chnlDTO.SubscriberCount = model.SubscriberCount;
                chnlDTO.SubscriptionTime = model.SubscriptionTime;
                chnlDTO.ViewCount = model.ViewCount;
                chnlDTO.CreationTime = model.CreationTime;
                chnlDTO.Country = model.Country;
                chnlDTO.ChannelIP = model.ChannelIP;
                chnlDTO.ChannelPort = model.ChannelPort;
                chnlDTO.StreamIP = model.StreamIP;
                chnlDTO.StreamPort = model.StreamPort;


                if (model.CategoryList != null)
                {
                    foreach (ChannelCategoryModel ctgModdel in model.CategoryList)
                    {
                        chnlDTO.CategoryList.Add(new ChannelCategoryDTO { CategoryID = ctgModdel.CategoryID, CategoryName = ctgModdel.CategoryName });
                    }
                }

                if (model.MediaList != null)
                {
                    foreach (ChannelMediaModel mediaModel in model.MediaList)
                    {
                        chnlDTO.MediaList.Add(GetChannelMediaDtoFromChannelMediaModel(mediaModel));
                    }
                }

            }

            return chnlDTO;
        }

        public static ChannelMediaDTO GetChannelMediaDtoFromChannelModel(ChannelModel model)
        {
            ChannelMediaDTO medialDTO = new ChannelMediaDTO();
            if (model != null)
            {
                medialDTO.ChannelID = model.ChannelID;
                medialDTO.OwnerID = model.OwnerID;
                medialDTO.Description = model.Description;
                medialDTO.MediaStatus = model.ChannelStatus;
                medialDTO.ChannelType = model.ChannelType;
            }

            return medialDTO;
        }

        public static ChannelMediaDTO GetChannelMediaDtoFromSingleMediaModel(SingleMediaModel mediaModel)
        {
            ChannelMediaDTO channelMediaDTO = new ChannelMediaDTO();
            if (mediaModel != null)
            {
                channelMediaDTO.Title = mediaModel.Title;
                channelMediaDTO.Artist = mediaModel.Artist;
                channelMediaDTO.MediaType = mediaModel.MediaType;
                channelMediaDTO.MediaUrl = mediaModel.StreamUrl;
                channelMediaDTO.MediaID = mediaModel.ContentId;
                channelMediaDTO.ThumbImageUrl = mediaModel.ThumbUrl;
                channelMediaDTO.ThumbImageWidth = mediaModel.ThumbImageWidth;
                channelMediaDTO.ThumbImageHeight = mediaModel.ThumbImageHeight;
                channelMediaDTO.Duration = 1000 * mediaModel.Duration;// in ms
            }
            return channelMediaDTO;
        }

        public static ChannelMediaDTO GetChannelMediaDtoFromChannelMediaModel(ChannelMediaModel model)
        {
            ChannelMediaDTO medialDTO = new ChannelMediaDTO();
            if (model != null)
            {
                medialDTO.ChannelID = model.ChannelID;
                medialDTO.OwnerID = model.OwnerID;
                medialDTO.Title = model.Title;
                medialDTO.Artist = model.Artist;
                medialDTO.Description = model.Description;
                medialDTO.MediaID = model.MediaID;
                medialDTO.MediaType = model.MediaType;
                medialDTO.MediaUrl = model.MediaUrl;
                medialDTO.ThumbImageUrl = model.ThumbImageUrl;
                medialDTO.ThumbImageWidth = model.ThumbImageWidth;
                medialDTO.ThumbImageHeight = model.ThumbImageHeight;
                medialDTO.Duration = model.Duration;
                medialDTO.MediaStatus = model.MediaStatus;
                medialDTO.ChannelType = model.ChannelType;
                medialDTO.StartTime = model.StartTime;
            }

            return medialDTO;
        }

        public static void LoadFeaturedChannelList(bool initLoad = false, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading = true;
                    string searchParam = String.Empty;
                    int catId = 0;
                    List<int> catIds = null;

                    if (UCMiddlePanelSwitcher.View_UCStreamAndChannel != null && UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel != null)
                    {
                        searchParam = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.txtSearch.Text;
                        catId = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbChannelCategory.SelectedValue != null ? (int)UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbChannelCategory.SelectedValue : 0;
                    }

                    if (catId > 0)
                    {
                        catIds = new List<int>();
                        catIds.Add(catId);
                    }

                    int startLimit = initLoad ? 0 : ChannelViewModel.Instance.ChannelFeatureList.Count;
                    Guid pivot = startLimit > 0 ? ChannelViewModel.Instance.ChannelFeatureList.ElementAt(ChannelViewModel.Instance.ChannelFeatureList.Count - 1).ChannelID : Guid.NewGuid();

                    new ThrdGetFeaturedChannelList(startLimit, pivot, limit, searchParam, catIds, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsFeaturedLoading = false;
                log.Error("Error: LoadFeaturedChannelList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadMostViewChannelList(bool initLoad = false, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading = true;
                    string searchParam = String.Empty;
                    int catId = 0;
                    List<int> catIds = null;

                    if (UCMiddlePanelSwitcher.View_UCStreamAndChannel != null && UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel != null)
                    {
                        searchParam = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.txtSearch.Text;
                        catId = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbChannelCategory.SelectedValue != null ? (int)UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbChannelCategory.SelectedValue : 0;
                    }

                    if (catId > 0)
                    {
                        catIds = new List<int>();
                        catIds.Add(catId);
                    }

                    new ThrdGetMostViewedChannelList(initLoad ? 0 : ChannelViewModel.Instance.ChannelMostViewList.Count, limit, searchParam, catIds, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsMostViewLoading = false;
                log.Error("Error: LoadMostViewChannelList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadSearchChannelList(bool initLoad, string searchParam, string country = null, int catId = 0, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading = true;
                    List<int> catIds = null;

                    if (catId > 0)
                    {
                        catIds = new List<int>();
                        catIds.Add(catId);
                    }

                    new ThrdSearchChannels(initLoad ? 0 : ChannelViewModel.Instance.ChannelSearchList.Count, limit, searchParam, country, catIds, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsSearchLoading = false;
                log.Error("Error: LoadSearchChannelList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadMyChannelList(int channelStatus, bool initLoad = false, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsMyChannelLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsMyChannelLoading = true;

                    int startLimit = initLoad ? 0 : ChannelViewModel.Instance.MyChannelList.Count;
                    Guid pivot = startLimit > 0 ? ChannelViewModel.Instance.MyChannelList.ElementAt(ChannelViewModel.Instance.MyChannelList.Count - 1).ChannelID : Guid.NewGuid();

                    new ThrdGetOwnChannelList(channelStatus, startLimit, pivot, limit, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsMyChannelLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsMyChannelLoading = false;
                log.Error("Error: LoadMyChannelList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadChannelFollowingList(bool initLoad = false, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsFollowingLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsFollowingLoading = true;

                    int startLimit = initLoad ? 0 : ChannelViewModel.Instance.ChannelFollowingList.Count;
                    Guid pivot = startLimit > 0 ? ChannelViewModel.Instance.ChannelFollowingList.ElementAt(ChannelViewModel.Instance.ChannelFollowingList.Count - 1).ChannelID : Guid.NewGuid();

                    new ThrdGetFollowningChannelList(startLimit, pivot, limit, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsFollowingLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsFollowingLoading = false;
                log.Error("Error: LoadChannelFollowingfList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadChannelPlayList(Guid channelId, bool initLoad = false, int limit = 10, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsPlaylistLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsPlaylistLoading = true;

                    ObservableCollection<ChannelMediaModel> tempList = ChannelViewModel.Instance.GetChannelPlayList(channelId);
                    int startLimit = initLoad ? 0 : tempList.Count;
                    Guid pivot = startLimit > 0 ? tempList.ElementAt(tempList.Count - 1).ChannelID : Guid.NewGuid();

                    new ThrdGetChannelPlayList(channelId, startLimit, pivot, limit, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsPlaylistLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                ChannelViewModel.Instance.LoadStatusModel.IsPlaylistLoading = false;
                log.Error("Error: LoadChannelPlayList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadUploadedChannelMediaList(int mediaType, bool initLoad = false, int limit = 10, Func<bool, int> onComplete = null)
        {
            try
            {
                if (ChannelViewModel.Instance.LoadStatusModel.IsUploadedMediaLoading == false)
                {
                    ChannelViewModel.Instance.LoadStatusModel.IsUploadedMediaLoading = true;

                    ObservableCollection<ChannelMediaModel> tempList = ChannelViewModel.Instance.GetChannelUploadedMediaList(mediaType);
                    int startLimit = initLoad ? 0 : tempList.Count;
                    Guid pivot = startLimit > 0 ? tempList.ElementAt(tempList.Count - 1).ChannelID : Guid.NewGuid();

                    new ThrdGetUploadedChannelMediaList(mediaType, startLimit, pivot, limit, (status) =>
                    {
                        ChannelViewModel.Instance.LoadStatusModel.IsUploadedMediaLoading = false;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
<<<<<<< HEAD
                ChannelViewModel.Instance.LoadStatusModel.IsPlaylistLoading = false;
                log.Error("Error: LoadChannelPlayList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        
    }
}
=======
                ChannelViewModel.Instance.LoadStatusModel.IsUploadedMediaLoading = false;
                log.Error("Error: LoadUploadedChannelMediaList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

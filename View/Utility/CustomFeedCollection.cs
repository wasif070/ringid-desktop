﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using View.BindingModels;

namespace View.Utility.Feed
{
    public class CustomFeedCollection : ObservableCollection<FeedHolderModel>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CustomFeedCollection).Name);
        private object LockObj = new object();
        public int MinIndex = 0;
        //public bool isUpdateTime = true;
        public int Type;
        public FeedHolderModel LoadMoreModel;
        public FeedHolderModel DummyModel;
        public long UserProfileUtId = 0;//for only profiles
        //public long pId = 0;//for only page type of profiles

        public CustomFeedCollection(int type, bool isNewStatus = false)
        {
            this.Type = type;
            if (isNewStatus)
            {
                this.MinIndex = 1;
                this.Add(new FeedHolderModel(0)); //NewStatus
            }
            else
            {
                this.MinIndex = 0;
            }
            DummyModel = new FeedHolderModel(12);
            this.Add(DummyModel);
            LoadMoreModel = new FeedHolderModel(1);
            this.Add(LoadMoreModel); //Load more
        }

        public void CalculateandInsertOtherFeeds(FeedHolderModel model, List<Guid> CurrentIds)
        {
            try
            {
                int max = this.Count - 2;
                int pivot, min = MinIndex;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.Feed != null && this[pivot].Feed != null && model.Feed.Time < this[pivot].Feed.Time)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    try
                    {
                        lock (this)
                        {
                            this.Insert(min, model);
                            if (!CurrentIds.Contains(model.FeedId))
                                CurrentIds.Add(model.FeedId);
                        }
                    }
                    catch (Exception ex) { log.Error("Error: InViewCollection<Insert> ." + ex.Message + "\n" + ex.StackTrace); }
                }, DispatcherPriority.Send);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void CalculateandInsertSavedFeeds(FeedHolderModel model, List<Guid> CurrentIds)
        {
            try
            {
                int max = this.Count - 2;
                int pivot, min = MinIndex;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.Feed != null && this[pivot].Feed != null && model.Feed.SavedTime < this[pivot].Feed.SavedTime)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    try
                    {
                        lock (this)
                        {
                            this.Insert(min, model);
                            if (!CurrentIds.Contains(model.FeedId))
                                CurrentIds.Add(model.FeedId);
                        }
                    }
                    catch (Exception ex) { log.Error("Error: InViewCollection<Insert> ." + ex.Message + "\n" + ex.StackTrace); }
                }, DispatcherPriority.Send);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void DropModel(FeedHolderModel item, List<Guid> CurrentIds)//CurrentFeedIds.Remove search
        {
            //Application.Current.Dispatcher.Invoke((Action)delegate
            //{
            try
            {
                //lock ?SJM
                int idxOfItem = this.IndexOf(item);
                if (idxOfItem >= 0)
                {
                    this.RemoveAt(idxOfItem);
                    if (CurrentIds != null && CurrentIds.Count > 0)
                        CurrentIds.Remove(item.FeedId);
                }
            }
            catch (Exception ex) { log.Error("Error: InViewCollection<Remove> ." + ex.Message + "\n" + ex.StackTrace); }
            //}, DispatcherPriority.Send);
        }
        public void DropNfId(Guid nfId, List<Guid> CurrentIds)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                try
                {
                    //lock ?SJM
                    bool found = false;
                    int idx = 1;
                    for (; idx < this.Count - 2; idx++)
                    {
                        FeedHolderModel tempModel = this[idx];
                        if (tempModel.FeedId == nfId)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (found)
                    {
                        this.RemoveAt(idx);
                        if (CurrentIds != null && CurrentIds.Count > 0)
                            CurrentIds.Remove(nfId);
                    }
                }
                catch (Exception ex) { log.Error("Error: InViewCollection<Remove> ." + ex.Message + "\n" + ex.StackTrace); }
            }, DispatcherPriority.Send);
        }

        public void InsertModel(FeedHolderModel model)
        {
            try
            {
                int min = MinIndex;
                int pivot;
                int max = this.Count - 1;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.Feed != null && this[pivot].Feed != null && model.Feed.ActualTime < this[pivot].Feed.ActualTime)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    if (model.FeedId != Guid.Empty && min >= MinIndex && min <= this.Count - 1 && !this.Any(P => P.FeedId == model.FeedId))
                    {
                        this.Insert(min, model);
                    }
                }, DispatcherPriority.Send);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void InsertSavedFeedModel(FeedHolderModel model)
        {
            try
            {
                int min = MinIndex;
                int pivot;
                int max = this.Count - 1;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.Feed != null && this[pivot].Feed != null && model.Feed.SavedTime < this[pivot].Feed.SavedTime)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    if (model.FeedId != Guid.Empty && min >= MinIndex && min <= this.Count - 1 && !this.Any(P => P.FeedId == model.FeedId))
                    {
                        this.Insert(min, model);
                    }
                }, DispatcherPriority.Send);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void RemoveAllModels()
        {
            try
            {
                if (this.Count > 2)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        for (int i = this.Count - 2; i > MinIndex; i--)
                        {
                            FeedHolderModel fm = this[i];
                            if (fm.FeedId != Guid.Empty)
                                this.Remove(fm);
                        }
                    }, DispatcherPriority.Send);
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void RemoveModel(Guid nfId, bool iDelete = false)
        {
            try
            {
                List<FeedHolderModel> tempList = this.ToList();
                if (this.Count > 0)
                {
                    foreach (FeedHolderModel data in tempList)
                    {
                        if (data.FeedId == nfId && data.FeedType == 2)
                        {
                            if (data.Feed != null && data.Feed.ParentFeed != null)
                            {
                                if (data.Feed.ParentFeed.WhoShareList != null && data.Feed.ParentFeed.WhoShareList.Count > 0)
                                {
                                    foreach (FeedHolderModel singleShare in data.Feed.ParentFeed.WhoShareList)
                                    {
                                        if (singleShare.FeedId == nfId)
                                        {
                                            Application.Current.Dispatcher.Invoke((Action)delegate
                                            {
                                                data.Feed.ParentFeed.WhoShareList.Remove(singleShare);
                                            }, DispatcherPriority.Send);
                                            if (iDelete)
                                            {
                                                if (data.Feed.ParentFeed.LikeCommentShare != null)
                                                    data.Feed.ParentFeed.LikeCommentShare.IShare = 0;
                                            }
                                            if (data.Feed.ParentFeed.LikeCommentShare != null)
                                                data.Feed.ParentFeed.LikeCommentShare.NumberOfShares -= 1;
                                            data.Feed.ParentFeed.OnPropertyChanged("WhoShareList");
                                            data.Feed.ParentFeed.OnPropertyChanged("CurrentInstance");
                                            break;
                                        }
                                    }
                                }
                            }
                            int idxData = this.IndexOf(data);
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                this.Remove(data);
                            }, DispatcherPriority.Send);
                            break;
                        }
                    }
                    foreach (FeedHolderModel data in tempList)
                    {
                        if (data.Feed != null && data.Feed.ParentFeed != null && data.Feed.ParentFeed.NewsfeedId == nfId)
                        {
                            data.Feed.ParentFeed = null;
                            data.Feed.OnPropertyChanged("CurrentInstance");
                            break;
                        }
                    }
                    foreach (FeedHolderModel data in tempList)
                    {
                        if (data.Feed != null && data.Feed.WhoShareList != null && data.Feed.WhoShareList.Count > 0)
                        {
                            foreach (FeedHolderModel singleShare in data.Feed.WhoShareList)
                            {
                                if (singleShare.FeedId == nfId)
                                {
                                    Application.Current.Dispatcher.Invoke((Action)delegate
                                    {
                                        data.Feed.WhoShareList.Remove(singleShare);
                                    }, DispatcherPriority.Send);
                                    if (iDelete)
                                    {
                                        if (data.Feed.LikeCommentShare != null)
                                            data.Feed.LikeCommentShare.IShare = 0;
                                    }
                                    if (data.Feed.LikeCommentShare != null)
                                        data.Feed.LikeCommentShare.NumberOfShares -= 1;
                                    data.Feed.OnPropertyChanged("WhoShareList");
                                    data.Feed.OnPropertyChanged("CurrentInstance");
                                    break;
                                }
                            }
                        }
                    }
                    foreach (FeedHolderModel data in tempList)
                    {
                        if (data.Feed != null && data.Feed.ParentFeed != null && data.Feed.ParentFeed.WhoShareList != null && data.Feed.ParentFeed.WhoShareList.Count > 0)
                        {
                            foreach (FeedHolderModel singleShare in data.Feed.ParentFeed.WhoShareList)
                            {
                                if (singleShare.FeedId == nfId)
                                {
                                    Application.Current.Dispatcher.Invoke((Action)delegate
                                    {
                                        data.Feed.ParentFeed.WhoShareList.Remove(singleShare);
                                    }, DispatcherPriority.Send);

                                    if (iDelete)
                                    {
                                        if (data.Feed.ParentFeed.LikeCommentShare != null)
                                            data.Feed.ParentFeed.LikeCommentShare.IShare = 0;
                                    }
                                    if (data.Feed.ParentFeed.LikeCommentShare != null)
                                        data.Feed.ParentFeed.LikeCommentShare.NumberOfShares -= 1;
                                    data.Feed.ParentFeed.OnPropertyChanged("WhoShareList");
                                    data.Feed.ParentFeed.OnPropertyChanged("CurrentInstance");
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        /* public void RemoveTopModels()
         {
             try
             {
                 if (this.Count > 10)
                 {
                     int keepCount = 0, startIdx = 0;
                     for (startIdx = this.Count - 2; startIdx > 0; startIdx--)
                     {
                         if (keepCount >= 10) break;
                         else if (this[startIdx].FeedType == 2) keepCount++;
                     }
                     Application.Current.Dispatcher.Invoke((Action)delegate
                     {
                         if (startIdx < this.Count)
                             for (int i = startIdx; i > 0; i--)
                             {
                                 if (this[i].FeedType == 2)
                                     this.RemoveAt(i);
                                 else break;
                             }
                     }, DispatcherPriority.Send);
                 }
             }
             catch (Exception e)
             {
                 log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
             }
         }

         public void RemoveBottomModels()
         {
             try
             {
                 if (this.Count > 10)
                 {
                     int keepCount = 0, startIdx = 0;
                     for (startIdx = 1; startIdx < this.Count - 1; startIdx++)
                     {
                         if (keepCount >= 10) break;
                         else if (this[startIdx].FeedType == 2) keepCount++;
                     }
                     Application.Current.Dispatcher.Invoke((Action)delegate
                     {
                         for (int i = this.Count - 1; i >= startIdx; i--)
                         {
                             if (this[i].FeedType == 2)
                                 this.RemoveAt(i);
                             else break;
                         }
                     }, DispatcherPriority.Send);
                 }
             }
             catch (Exception e)
             {
                 log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
             }
         }

         public FeedModel GetFirstIndexModel()
         {
             FeedModel model = null;
             try
             {
                 //model = this.Where(P => P.FeedType == 2).FirstOrDefault();
                 for (int i = 1; i < this.Count - 1; i++)
                 {
                     model = this[i];
                     if (model.FeedType == 2) return model;
                 }
             }
             catch (Exception e)
             {
                 log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
             }
             return null;
         }

         public FeedModel GetFirstModel()
         {
             FeedModel model = null;
             try
             {
                 //model = this.Where(P => P.FeedType == 2 && P.FeedCategory != SettingsConstants.SPECIAL_FEED).FirstOrDefault();
                 if (this.Count > 1)
                     for (int i = MinIndex; i < this.Count - 1; i++)
                     {
                         model = this[i];
                         if (model.NewsfeedId != Guid.Empty && model.FeedCategory != SettingsConstants.SPECIAL_FEED) return model; //model.FeedType == 2
                     }
             }
             catch (Exception e)
             {
                 log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
             }
             return null;
         }

         public FeedModel GetFirstSpecialFeedModel()
         {
             FeedModel model = null;
             try
             {
                 //model = this.Where(P => P.FeedType == 2 && P.FeedCategory != SettingsConstants.SPECIAL_FEED).FirstOrDefault();
                 if (this.Count > 1)
                 {
                     model = this[MinIndex];
                     if (model.NewsfeedId != Guid.Empty && model.FeedCategory == SettingsConstants.SPECIAL_FEED) return model;
                 }
             }
             catch (Exception e)
             {
                 log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
             }
             return null;
         }

         public FeedModel GetLastModel()
         {
             FeedModel model = null;
             try
             {
                 //model = this.Where(P => P.FeedType == 2 && P.FeedCategory != SettingsConstants.SPECIAL_FEED).LastOrDefault();
                 if (this.Count > 1)
                     for (int i = this.Count - 2; i >= MinIndex; i--)
                     {
                         model = this[i];
                         if (model != null && model.NewsfeedId != Guid.Empty && model.FeedCategory != SettingsConstants.SPECIAL_FEED) return model; //.FeedType == 2
                     }
             }
             catch (Exception e)
             {
                 log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
             }
             return null;
         }

         public FeedModel GetModel(Guid nfId)
         {
             FeedModel model = null;
             try
             {
                 if (this.Count > 2)
                 {
                     List<FeedModel> tempList = this.ToList();
                     foreach (FeedModel data in tempList)
                     {
                         if (data.NewsfeedId == nfId)
                         {
                             model = data;
                             return model;
                         }
                         else if (data.ParentFeed != null && data.ParentFeed.NewsfeedId == nfId)
                         {
                             model = data.ParentFeed;
                             return model;
                         }
                         else if (data.WhoShareList != null && data.WhoShareList.Count > 0)
                         {
                             List<FeedModel> tempWhoShareList = data.WhoShareList.ToList();
                             foreach (FeedModel singleShare in tempWhoShareList)
                             {
                                 if (singleShare.NewsfeedId == nfId)
                                 {
                                     model = singleShare;
                                     return model;
                                 }
                             }
                         }
                         else if (data.ParentFeed != null && data.ParentFeed.WhoShareList != null && data.ParentFeed.WhoShareList.Count > 0)
                         {
                             List<FeedModel> tempParentWhoShareList = data.ParentFeed.WhoShareList.ToList();
                             foreach (FeedModel singleShare in tempParentWhoShareList)
                             {
                                 if (singleShare.NewsfeedId == nfId)
                                 {
                                     model = singleShare;
                                     return model;
                                 }
                             }
                         }
                     }
                 }
             }
             catch (Exception e)
             {
                 log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
             }
             return null;
         }

         public void RemoveAllModels()
         {
             try
             {
                 if (this.Count > 2)
                 {
                     Application.Current.Dispatcher.Invoke((Action)delegate
                     {
                         for (int i = this.Count - 2; i >= MinIndex; i--)
                         {
                             FeedModel fm = this[i];
                             if (fm.NewsfeedId != Guid.Empty) //FeedType == 2
                                 //lock (this)
                                 this.Remove(fm);
                         }
                     }, DispatcherPriority.Send);
                 }
             }
             catch (Exception e)
             {
                 log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
             }
         } */

    }
}

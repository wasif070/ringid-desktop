﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;
using View.Utility.Auth;

namespace View.Utility.Stream
{
    public class ThrdStartLiveStream
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThrdStartLiveStream).Name);

        private static float _Longitude;
        private static float _Latitude;

        private long _PublisherId;
        private Guid _StreamId;
        private string _Title;
        private List<StreamCategoryDTO> _CatList;
        private bool _ChatON = false;
        private bool _GiftON = false;
        private int _ProfileType;
        private Func<bool, string, int, int> _OnComplete = null;

        public ThrdStartLiveStream(long publisherId, Guid streamId, string title, bool chatON, bool giftON, List<StreamCategoryDTO> tagDTOs, Func<bool, string, int, int> onComplete = null, int profileType = 0)
        {
            this._PublisherId = publisherId;
            this._StreamId = streamId;
            this._Title = title;
            this._CatList = tagDTOs;
            this._ChatON = chatON;
            this._GiftON = giftON;
            this._ProfileType = profileType;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (_Latitude == 0 || _Longitude == 0)
                    {
                        HelperMethods.GetLatLongByCurrentTimeZone(out _Latitude, out _Longitude);
                    }

                    JArray catArray = new JArray();
                    if (_CatList != null && _CatList.Count > 0)
                    {
                        foreach (StreamCategoryDTO strmDTO in _CatList)
                        {
                            JObject obj = new JObject();
                            obj[JsonKeys.StreamCategoryId] = strmDTO.CategoryID;
                            obj[JsonKeys.Category] = strmDTO.CategoryName;
                            catArray.Add(obj);
                        }
                    }

                    JObject pakToSend = new JObject();
                    pakToSend[JsonKeys.StreamId] = _StreamId;
                    pakToSend[JsonKeys.Title] = _Title ?? String.Empty;
                    pakToSend[JsonKeys.StreamLongitude] = _Longitude;
                    pakToSend[JsonKeys.Latitude] = _Latitude;
                    pakToSend[JsonKeys.StreamCategoryList] = catArray;
                    pakToSend[JsonKeys.StreamChatON] = _ChatON;
                    pakToSend[JsonKeys.StreamGiftON] = _GiftON;
                    pakToSend[JsonKeys.ProfileType] = _ProfileType;
                    string packetId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_START_LIVE_STREAM;

                    JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);

#if CHAT_LOG
                    log.Debug("STREAM START LIVE => " + _JobjFromResponse);
#endif

                    if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                    {
                        if (this._OnComplete != null)
                        {
                            this._OnComplete(true, String.Empty, 0);
                        }
                    }
                    else
                    {
                        if (this._OnComplete != null)
                        {
                            this._OnComplete(false, "Failed! Couldn't connect with server.", (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.ReasonCode] != null ? (int)_JobjFromResponse[JsonKeys.ReasonCode] : 0));
                        }
                    }
                }
                else
                {
                    log.Error("Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);

                    if (_OnComplete != null)
                    {
                        _OnComplete(false, "Failed! Couldn't connect with server.", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                if (_OnComplete != null)
                {
                    _OnComplete(false, "Failed! Couldn't connect with server.", 0);
                }
                log.Error(ex.Message + ex.StackTrace);
            }
        }
    }
}
﻿using log4net;
using Models.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.UI.Stream;

namespace View.Utility.Stream
{
    class StreamMsgLoadUtility : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(StreamViewerLoadUtility).Name);

        private static readonly object _LoadSyncRoot = new object();
        private static StreamMsgLoadUtility _Instance = null;
        private ConcurrentQueue<MessageDTO> _RecentQueue = new ConcurrentQueue<MessageDTO>();

        public StreamMsgLoadUtility()
        {
            _Instance = this;
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += FriendListLoad_DowWork;
            ProgressChanged += FriendListLoad_ProgressChanged;
            RunWorkerCompleted += FriendListLoad_RunWorkerCompleted;
        }

        public static void LoadMessageData(List<MessageDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                foreach (MessageDTO dto in list)
                {
                    StreamMsgLoadUtility.LoadData(dto);
                }
            }
        }

        public static void LoadMessageData(MessageDTO list)
        {
            StreamMsgLoadUtility.LoadData(list);
        }

        private static void LoadData(MessageDTO recentDTO)
        {
            try
            {
                lock (_LoadSyncRoot)
                {
                    if (StreamMsgLoadUtility._Instance == null)
                    {

                        StreamMsgLoadUtility._Instance = new StreamMsgLoadUtility();
                        StreamMsgLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        StreamMsgLoadUtility._Instance.Start();
                    }
                    else
                    {

                        StreamMsgLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        if (StreamMsgLoadUtility._Instance.IsBusy == false)
                        {
                            StreamMsgLoadUtility._Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadShowMoreData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Start()
        {
            RunWorkerAsync();
        }

        private void FriendListLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                MessageDTO mDTO = null;
                while (_RecentQueue.TryDequeue(out mDTO))
                {
                    //System.Threading.Thread.Sleep(1);
                    AddMessageModel(mDTO);

                    UCStreamLiveViewer viewer = StreamHelpers.GetCurruentStreamLiveViewer();
                    if (viewer != null)
                    {
                        viewer.MoveMessageListToBottom(mDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FriendListLoad_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            e.Result = true;
        }

        private void FriendListLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void FriendListLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_RecentQueue.Count > 0)
            {
                if (StreamMsgLoadUtility._Instance.IsBusy == false)
                {
                    StreamMsgLoadUtility._Instance.Start();
                }
            }
        }

        private static void AddMessageModel(MessageDTO newDTO)
        {
            try
            {
                ObservableCollection<MessageModel> messageModelList = StreamViewModel.Instance.GetMessageListByPublisherID(newDTO.PublisherID);
                MessageModel newModel = messageModelList.Where(P => P.PacketID == newDTO.PacketID).FirstOrDefault();
                if (newModel == null)
                {
                    newModel = new MessageModel(newDTO);
                    newModel.StreamMsgModel = new StreamMessageModel { MessageType = newDTO.StreamMsgDTO.MessageType };
                    if (messageModelList.Count > 0)
                    {
                        MessageModel firstModel = messageModelList.FirstOrDefault();
                        MessageModel lastModel = messageModelList.LastOrDefault();

                        if (newModel.MessageDate > firstModel.MessageDate)
                        {
                            messageModelList.InvokeAdd(newModel);
                        }
                        else if (newModel.MessageDate < lastModel.MessageDate)
                        {
                            messageModelList.InvokeInsert(0, newModel);
                        }
                        else
                        {
                            int max = messageModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                long pivotTime = messageModelList.ElementAt(pivot).MessageDate;
                                if (newModel.MessageDate > pivotTime)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            messageModelList.InvokeInsert(min, newModel);
                        }
                    }
                    else
                    {
                        messageModelList.InvokeAdd(newModel);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddMessageModel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.Utility.Auth;

namespace View.Utility.Stream
{
    public class ThrdStreamDetails
    {
        private log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdStreamDetails).Name);
        private Guid _StreamId;
        private Func<StreamDTO, StreamUserDTO, int> _OnComplete = null;

        public ThrdStreamDetails(Guid streamId, Func<StreamDTO, StreamUserDTO, int> onComplete)
        {
            this._StreamId = streamId;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    JObject pakToSend = new JObject();
                    pakToSend[JsonKeys.StreamId] = this._StreamId;
                    string packetId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_LIVE_STREAMING_DETAILS;

                    JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);

#if CHAT_LOG
                    log.Debug("STREAM DETAILS => " + _JobjFromResponse);
#endif


                    if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null)
                    {
                        if ((bool)_JobjFromResponse[JsonKeys.Success])
                        {
                            StreamDTO streamingDTO = null;
                            StreamUserDTO streamUserDTO = null;
                            if (_JobjFromResponse[JsonKeys.StreamDetails] != null)
                            {
                                streamingDTO = StreamSignalHandler.MakeStreamDTO((JObject)_JobjFromResponse[JsonKeys.StreamDetails]);
                                if (streamingDTO.EndTime > 0) StreamHelpers.RemoveStreamByID(this._StreamId);
                            }
                            if (_JobjFromResponse[JsonKeys.StreamUser] != null)
                            {
                                streamUserDTO = StreamSignalHandler.MakeStreamUserDTO((JObject)_JobjFromResponse[JsonKeys.StreamUser]);
                            }

                            if (_OnComplete != null)
                            {
                                _OnComplete(streamingDTO, streamUserDTO);
                            }
                        }
                        else
                        {
                            StreamHelpers.RemoveStreamByID(this._StreamId);

                            if (_OnComplete != null)
                            {
                                _OnComplete(null, null);
                            }
                        }
                    }
                    else
                    {
                        if (_OnComplete != null)
                        {
                            _OnComplete(null, null);
                        }
                    }
                }
                else
                {
                    log.Error("Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    
                    if (_OnComplete != null)
                    {
                        _OnComplete(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                if (_OnComplete != null)
                {
                    _OnComplete(null, null);
                }
                log.Error(ex.Message + ex.StackTrace);
            }
        }
    }
}

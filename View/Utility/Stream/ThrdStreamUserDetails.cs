﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.Utility.Auth;

namespace View.Utility.Stream
{
    public class ThrdStreamUserDetails
    {
        private long _UserTableId;
        private Func<StreamUserDTO, int> _OnComplete = null;

        public ThrdStreamUserDetails(long utId, Func<StreamUserDTO, int> onComplete = null)
        {
            this._UserTableId = utId;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.UserTableID] = this._UserTableId;
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACTION_LIVE_STREAMING_USER_DETAILS;

                JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamUser] != null)
                {
                    StreamUserDTO streamUserDTO = StreamSignalHandler.MakeStreamUserDTO((JObject)_JobjFromResponse[JsonKeys.StreamUser]);

                    if (_OnComplete != null)
                    {
                        _OnComplete(streamUserDTO);
                    }
                }
                else
                {
                    if (_OnComplete != null)
                    {
                        _OnComplete(null);
                    }
                }
            }
            catch (Exception ex)
            {
                if (_OnComplete != null)
                {
                    _OnComplete(null);
                }
            }
        }
    }
}

﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;
using View.Utility.Auth;

namespace View.Utility.Stream
{
    public class ThrdEndLiveStream
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThrdStartLiveStream).Name);
        private Guid _StreamID;

        public ThrdEndLiveStream(Guid streamID)
        {
            this._StreamID = streamID;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {

                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.StreamId] = this._StreamID;
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_END_LIVE_STREAM;

                JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
        }
    }
}
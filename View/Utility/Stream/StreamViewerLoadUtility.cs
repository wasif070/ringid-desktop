﻿using callsdkwrapper;
using log4net;
using Models.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.UI.Stream;

namespace View.Utility.Stream
{
    class StreamViewerLoadUtility : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(StreamViewerLoadUtility).Name);

        private static readonly object _LoadSyncRoot = new object();
        private static readonly object _ProcessSyncRoot = new object();
        private static StreamViewerLoadUtility _Instance = null;
        private ConcurrentQueue<StreamUserDTO> _RecentQueue = new ConcurrentQueue<StreamUserDTO>();

        public StreamViewerLoadUtility()
        {
            _Instance = this;
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += FriendListLoad_DowWork;
            ProgressChanged += FriendListLoad_ProgressChanged;
            RunWorkerCompleted += FriendListLoad_RunWorkerCompleted;
        }

        public static void LoadViewerData(List<StreamUserDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                foreach (StreamUserDTO dto in list)
                {
                    StreamViewerLoadUtility.LoadData(dto);
                }
            }
        }

        public static void LoadViewerData(StreamUserDTO list)
        {
            StreamViewerLoadUtility.LoadData(list);
        }

        private static void LoadData(StreamUserDTO recentDTO)
        {
            try
            {
                lock (_LoadSyncRoot)
                {
                    if (StreamViewerLoadUtility._Instance == null)
                    {

                        StreamViewerLoadUtility._Instance = new StreamViewerLoadUtility();
                        StreamViewerLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        StreamViewerLoadUtility._Instance.Start();
                    }
                    else
                    {

                        StreamViewerLoadUtility._Instance._RecentQueue.Enqueue(recentDTO);
                        if (StreamViewerLoadUtility._Instance.IsBusy == false)
                        {
                            StreamViewerLoadUtility._Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadShowMoreData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Start()
        {
            RunWorkerAsync();
        }

        private void FriendListLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                StreamUserDTO vDTO = null;
                while (_RecentQueue.TryDequeue(out vDTO))
                {
                    //System.Threading.Thread.Sleep(1);
                    AddViewerModel(vDTO);

                    UCStreamLiveViewer viewer = StreamHelpers.GetCurruentStreamLiveViewer();
                    if (viewer != null)
                    {
                        viewer.MoveViewerListToLeftEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("FriendListLoad_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            e.Result = true;
        }

        private void FriendListLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void FriendListLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_RecentQueue.Count > 0)
            {
                if (StreamViewerLoadUtility._Instance.IsBusy == false)
                {
                    StreamViewerLoadUtility._Instance.Start();
                }
            }
        }

        public static void RemoveViewer(long publisherID, long viewerID)
        {
            try
            {
                ObservableCollection<StreamUserModel> viewerModelList = StreamViewModel.Instance.GetViewerListByPublisherID(publisherID);
                lock (_ProcessSyncRoot)
                {
                    StreamUserModel model = viewerModelList.Where(P => P.UserTableID == viewerID).FirstOrDefault();
                    if (model != null)
                    {
                        int index = viewerModelList.IndexOf(model);
                        viewerModelList.InvokeRemoveAt(index);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("RemoveViewer() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private static void AddViewerModel(StreamUserDTO newDTO)
        {
            try
            {
                ObservableCollection<StreamUserModel> viewerModelList = StreamViewModel.Instance.GetViewerListByPublisherID(newDTO.PublisherID);
                lock (_ProcessSyncRoot)
                {
                    StreamUserModel newModel = viewerModelList.Where(P => P.UserTableID == newDTO.UserTableID).FirstOrDefault();

                    StreamUserModel firstModel = viewerModelList.FirstOrDefault();
                    StreamUserModel lastModel = viewerModelList.FirstOrDefault();

                    if (newModel == null)
                    {
                        newModel = new StreamUserModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.AddedTime > firstModel.AddedTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.AddedTime < lastModel.AddedTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotTime = viewerModelList.ElementAt(pivot).AddedTime;
                                    if (newModel.AddedTime < pivotTime)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.AddedTime > firstModel.AddedTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.AddedTime > newModel.AddedTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.AddedTime < viewerModelList.ElementAt(pivot).AddedTime)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddViewerModel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

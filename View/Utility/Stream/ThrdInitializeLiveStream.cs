﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;
using View.Utility.Auth;

namespace View.Utility.Stream
{
    public class ThrdInitializeLiveStream
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThrdInitializeLiveStream).Name);
        private Guid _StreamId;
        private Func<StreamServerDTO, int> _OnComplete = null;

        public ThrdInitializeLiveStream(Guid streamId, Func<StreamServerDTO, int> onComplete = null)
        {
            this._StreamId = streamId;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Process));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Process()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    JObject pakToSend = new JObject();
                    pakToSend[JsonKeys.StreamId] = _StreamId;
                    string packetId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_INITIALIZE_LIVE_STREAM;

                    JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);

#if CHAT_LOG
                    log.Debug("STREAM INITIALIZE LIVE => " + _JobjFromResponse);
#endif

                    if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamServerDTO] != null)
                    {
                        StreamServerDTO serverDTO = StreamSignalHandler.MakeStreamServerDTO((JObject)_JobjFromResponse[JsonKeys.StreamServerDTO]);
                        if (this._OnComplete != null)
                        {
                            this._OnComplete(serverDTO);
                        }
                    }
                    else
                    {
                        if (this._OnComplete != null)
                        {
                            this._OnComplete(null);
                        }
                    }
                }
                else
                {
                    log.Error("Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);

                    if (_OnComplete != null)
                    {
                        _OnComplete(null);
                    }
                }
            }
            catch (Exception ex)
            {
                if (_OnComplete != null)
                {
                    _OnComplete(null);
                }
                log.Error(ex.Message + ex.StackTrace);
            }
        }
    }
}
﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Stream
{
    public class ThrdUpdateStreamingLikeCount
    {
        private log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdUpdateStreamingLikeCount).Name);
        private Guid _StreamId;
        private Func<bool, string, long, int> _OnComplete;

        public ThrdUpdateStreamingLikeCount(Guid streamId, Func<bool, string, long, int> onComplete)
        {
            this._StreamId = streamId;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.StreamId] = this._StreamId;
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_UPDATE_STREAMING_LIKE_COUNT;

                JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    long likeCount = _JobjFromResponse[JsonKeys.LikeCount] != null ? (long)_JobjFromResponse[JsonKeys.LikeCount] : 0;
                    string message = _JobjFromResponse[JsonKeys.MSG] != null ? (string)_JobjFromResponse[JsonKeys.MSG] : string.Empty;
                    if (_OnComplete != null)
                    {
                        _OnComplete(true, message, likeCount);
                    }
                }
                else
                {
                    if (_OnComplete != null)
                    {
                        _OnComplete(false, null, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                if (_OnComplete != null)
                {
                    _OnComplete(false, null, 0);
                }
                log.Error("ThrdUpdateStreamingLikeCount => " + ex.Message + ex.StackTrace);
            }
        }
    }
}

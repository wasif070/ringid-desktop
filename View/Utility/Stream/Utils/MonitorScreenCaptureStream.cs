﻿namespace AForge.Video.Ext
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Threading;
    using System.Windows;
    using System.Windows.Interop;
    using View.Constants;
    using View.Utility.Stream;
    using View.Utility.Stream.Utils;
    using View.ViewModel;

    public class MonitorScreenCaptureStream : IVideoSource
    {
        private Rectangle region;

        private int frameInterval = 50;
        private int framesReceived;

        private Thread thread = null;
        private ManualResetEvent stopEvent = null;
        private IntPtr WindowHandler = IntPtr.Zero;
        private System.Drawing.Brush DisplayBrush;
        private VideoDeviceInfo _DeviceInfo;

        public event NewFrameEventHandler NewFrame;
        public event VideoSourceErrorEventHandler VideoSourceError;
        public event PlayingFinishedEventHandler PlayingFinished;

        public virtual string Source
        {
            get { return "Screen Capture"; }
        }

        public Rectangle Region
        {
            get { return region; }
            set { region = value; }
        }

        public int FrameInterval
        {
            get { return frameInterval; }
            set { frameInterval = Math.Max(0, value); }
        }

        public int FramesReceived
        {
            get
            {
                int frames = framesReceived;
                framesReceived = 0;
                return frames;
            }
        }

        public long BytesReceived
        {
            get { return 0; }
        }

        public VideoDeviceInfo DeviceInfo
        {
            get { return _DeviceInfo; }
            set { _DeviceInfo = value; }
        }

        public bool IsRunning
        {
            get
            {
                if (thread != null)
                {
                    // check thread status
                    if (thread.Join(0) == false)
                        return true;

                    // the thread is not running, free resources
                    Free();
                }
                return false;
            }
        }

        public MonitorScreenCaptureStream(VideoDeviceInfo deviceInfo)
        {
            this.DeviceInfo = deviceInfo;
            Rectangle screenArea = Rectangle.Empty;
            foreach (System.Windows.Forms.Screen screen in System.Windows.Forms.Screen.AllScreens)
            {
                screenArea = Rectangle.Union(screenArea, screen.Bounds);
            }
            this.region = screenArea;
        }

        public MonitorScreenCaptureStream(VideoDeviceInfo deviceInfo, int frameInterval)
        {
            this.DeviceInfo = deviceInfo;
            Rectangle screenArea = Rectangle.Empty;
            foreach (System.Windows.Forms.Screen screen in System.Windows.Forms.Screen.AllScreens)
            {
                screenArea = Rectangle.Union(screenArea, screen.Bounds);
            }
            this.region = screenArea;
            this.FrameInterval = frameInterval;
        }

        public void Start()
        {
            if (!IsRunning)
            {
                framesReceived = 0;

                // create events
                stopEvent = new ManualResetEvent(false);
                DisplayBrush = new SolidBrush(ColorTranslator.FromHtml("#252526"));
                WindowHandler = new WindowInteropHelper(Application.Current.MainWindow).EnsureHandle();
                // create and start new thread
                thread = new Thread(new ThreadStart(WorkerThread));
                thread.Name = Source; // mainly for debugging
                thread.Start();
            }
        }

        public void SignalToStop()
        {
            // stop thread
            if (thread != null)
            {
                // signal to stop
                stopEvent.Set();
            }
        }

        public void WaitForStop()
        {
            if (thread != null)
            {
                // wait for thread stop
                thread.Join();
                Free();
            }
        }

        public void Stop()
        {
            if (this.IsRunning)
            {
                stopEvent.Set();
                thread.Abort();
                WaitForStop();
            }
        }

        private void Free()
        {
            thread = null;

            // release events
            stopEvent.Dispose();
            stopEvent = null;
        }

        // Worker thread
        private void WorkerThread()
        {
            IntPtr desktopHandler = NativeLibrary.GetDesktopWindow();
            int width = region.Width;
            int height = region.Height;

            // download start time and duration
            DateTime start;
            TimeSpan span;

            while (!stopEvent.WaitOne(0))
            {
                Bitmap bitmap = null;
                // set dowbload start time
                start = DateTime.Now;

                try
                {
                    IntPtr hdcSrc = NativeLibrary.GetWindowDC(desktopHandler);
                    IntPtr hdcDest = NativeLibrary.CreateCompatibleDC(hdcSrc);
                    // create a bitmap we can copy it to,
                    // using GetDeviceCaps to get the width/height
                    IntPtr hBitmap = NativeLibrary.CreateCompatibleBitmap(hdcSrc, width, height);
                    // select the bitmap object
                    IntPtr hOld = NativeLibrary.SelectObject(hdcDest, hBitmap);
                    // bitblt over
                    NativeLibrary.BitBlt(hdcDest, 0, 0, width, height, hdcSrc, 0, 0, NativeLibrary.SRCCOPY);
                    // restore selection
                    NativeLibrary.SelectObject(hdcDest, hOld);
                    // clean up
                    NativeLibrary.DeleteDC(hdcDest);
                    NativeLibrary.ReleaseDC(desktopHandler, hdcSrc);
                    // get a .NET image object for it
                    bitmap = Image.FromHbitmap(hBitmap);
                    // free up the Bitmap object
                    NativeLibrary.DeleteObject(hBitmap);

                    WINDOWPLACEMENT pos = NativeLibrary.GetPlacement(WindowHandler);
                    bool isVisible = NativeLibrary.IsWindowVisible(WindowHandler) && (pos.showCmd == ShowWindowCommands.Maximized || pos.showCmd == ShowWindowCommands.Normal);

                    if (isVisible)
                    {
                        List<RECT> rectList = new List<RECT>();
                        RECT originRect = NativeLibrary.GetWindowRect(WindowHandler);
                        RECT marginRect = new RECT();
                        marginRect.Right = originRect.Right - 8 - RingIDSettings.RIGHT_MENUBAR_WIDTH - (RingIDViewModel.Instance.IsExpanded ? RingIDSettings.RIGHT_PANEL_WIDTH : 0);
                        marginRect.Left = originRect.Left + 8 + RingIDSettings.LEFT_FRIEND_LIST_WIDTH;
                        //marginRect.Top = originRect.Top + 30 + RingIDSettings.MAIN_MENU_HEIGHT + 45;
                        //marginRect.Bottom = originRect.Bottom - 8 - 155;
                        marginRect.Top = originRect.Top + 30 + 25;
                        marginRect.Bottom = originRect.Bottom - 8;


                        IntPtr prevWinddowHandler = NativeLibrary.GetWindow(WindowHandler, GetWindow_Cmd.GW_HWNDPREV);
                        while (prevWinddowHandler != IntPtr.Zero)
                        {
                            pos = NativeLibrary.GetPlacement(prevWinddowHandler);
                            isVisible = NativeLibrary.IsWindowVisible(prevWinddowHandler) && (pos.showCmd == ShowWindowCommands.Maximized || pos.showCmd == ShowWindowCommands.Normal);
                            if (isVisible)
                            {
                                RECT rect = NativeLibrary.GetWindowRect(prevWinddowHandler);
                                rectList.Add(rect);
                            }

                            prevWinddowHandler = NativeLibrary.GetWindow(prevWinddowHandler, GetWindow_Cmd.GW_HWNDPREV);
                        }
                        NativeLibrary.DeleteObject(prevWinddowHandler);
                        List<RECT> subRectList = NativeLibrary.SubtractRect(rectList, marginRect);

                        using (Graphics graphics = Graphics.FromImage(bitmap))
                        {
                            foreach (RECT r in subRectList)
                            {
                                graphics.FillRectangle(DisplayBrush, r.Left, r.Top, r.Right - r.Left, r.Bottom - r.Top);
                            }
                        }
                    }

                    // increment frames counter
                    framesReceived++;

                    // provide new image to clients
                    if (NewFrame != null)
                    {
                        // notify client
                        NewFrame(this, new NewFrameEventArgs(bitmap));
                    }

                    // wait for a while ?
                    if (frameInterval > 0)
                    {
                        // get download duration
                        span = DateTime.Now.Subtract(start);

                        // miliseconds to sleep
                        int msec = frameInterval - (int)span.TotalMilliseconds;

                        if ((msec > 0) && (stopEvent.WaitOne(msec)))
                            break;
                    }
                }
                catch (ThreadAbortException)
                {
                    break;
                }
                catch (Exception exception)
                {
                    // provide information to clients
                    if (VideoSourceError != null)
                    {
                        VideoSourceError(this, new VideoSourceErrorEventArgs(exception.Message));
                    }
                    // wait for a while before the next try
                    Thread.Sleep(250);
                }
                finally
                {
                    if (bitmap != null)
                    {
                        bitmap.Dispose();
                        bitmap = null;
                    }
                }

                // need to stop ?
                if (stopEvent.WaitOne(0))
                    break;
            }

            // release resources
            NativeLibrary.DeleteObject(desktopHandler);
            NativeLibrary.DeleteObject(WindowHandler);
            DisplayBrush.Dispose();
            DisplayBrush = null;

            if (PlayingFinished != null)
            {
                PlayingFinished(this, ReasonToFinishPlaying.StoppedByUser);
            }
        }
    }
}

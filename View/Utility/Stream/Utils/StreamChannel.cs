﻿using callsdkwrapper;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using View.BindingModels;

namespace View.Utility.Stream.Utils
{
    public class StreamChannel : IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(StreamChannel).Name);

        private int _ChannelType = 0;
        private bool _IsRegistered = false;
        private bool _IsVoiceHD = false;

        private StreamAudioChannel AudioChannel = null;
        private StreamVideoChannel VideoChannel = null;
        private INotificationStreamChannel NotifyStreamChannel = null;

        private bool _disposed = false;

        public StreamChannel(int channelType, bool isVoiceHD)
        {
            this.ChannelType = channelType;
            this.IsVoiceHD = isVoiceHD;
            this.AudioChannel = new StreamAudioChannel(this);
            this.VideoChannel = new StreamVideoChannel(this);
        }

        public void InitStream(INotificationStreamChannel notifyStreamChannel)
        {
            if (this._disposed)
            {
                log.Error("Object is already disposed => InitStream()");
            }

            this.IsRegistered = false;
            this.NotifyStreamChannel = notifyStreamChannel;
        }

        public void CloseStream()
        {
            this.NotifyStreamChannel = null;
            if (this.AudioChannel != null)
            {
                this.AudioChannel.CloseDevice();
            }
            if (this.VideoChannel != null)
            {
                this.VideoChannel.CloseDevice();
            }
        }

        public bool SetStreamSource(List<AudioDeviceInfo> audioDevice, VideoDeviceInfo videoDevice)
        {
            if (this._disposed)
            {
                log.Error("Object is already disposed => SetStreamSource()");
                return false;
            }

            bool status = false;
            this.AudioChannel.OpenDevice(audioDevice);
            this.VideoChannel.OpenDevice(videoDevice, out status);
            return status;
        }

        public void ResetStreamSource()
        {
            if (this.AudioChannel != null)
            {
                this.AudioChannel.CloseDevice();
            }
            if (this.VideoChannel != null)
            {
                this.VideoChannel.CloseDevice();
            }
        }

        public void SetMuted(bool isMuted)
        {
            if (this._disposed)
            {
                log.Error("Object is already disposed => SetMuted()");
                return;
            }

            if (this.AudioChannel != null)
            {
                this.AudioChannel.IsMuted = isMuted;
            }
        }

        public void ResetVideoDisplay()
        {
            if (this.NotifyStreamChannel != null)
            {
                this.NotifyStreamChannel.OnResetVideoDisplay();
            }
        }

        public void SetResolution(int width, int height)
        {
            if (this.NotifyStreamChannel != null)
            {
                this.NotifyStreamChannel.OnResolutionChange(width, height);
            }
        }

        public void SetAudioStreamBuffer(byte[] buffer)
        {
            if (this._disposed)
            {
                log.Error("Object is already disposed => SetAudioStreamBuffer()");
                return;
            }

            if (this.ChannelType == StreamConstants.CHANNEL_OUT)
            {
                if (this.IsRegistered && this.NotifyStreamChannel != null) this.NotifyStreamChannel.OnAudioStreamSend(buffer);
            }
            else
            {
                if (this.AudioChannel != null && this.AudioChannel.IsReady) this.AudioChannel.PlayAudioSample(buffer);
            }
        }

        public void SetVideoStreamBuffer(byte[] buffer, int width, int height, int orientation)
        {
            if (this._disposed)
            {
                log.Error("Object is already disposed => SetVideoStreamBuffer()");
                return;
            }

            if (this.NotifyStreamChannel != null)
            {
                if (this.ChannelType == StreamConstants.CHANNEL_OUT)
                {
                    if (this.IsRegistered) this.NotifyStreamChannel.OnVideoStreamSend(buffer);
<<<<<<< HEAD
                    this.NotifyStreamChannel.OnVideoStreamDisplay(buffer, width, height, -1, StreamConstants.CHANNEL_OUT);
=======
                    this.NotifyStreamChannel.OnVideoStreamDisplay(buffer, width, height, orientation, StreamConstants.CHANNEL_OUT);
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                }
                else
                {
                    this.NotifyStreamChannel.OnVideoStreamDisplay(buffer, width, height, orientation, StreamConstants.CHANNEL_IN);
                }
            }
        }

        public void Dispose()
        {
            this.IsRegistered = false;

            if (this._disposed == false)
            {
                this._disposed = true;
                
                if (this.VideoChannel != null)
                {
                    this.VideoChannel.CloseDevice();
                    this.VideoChannel = null;
                }

                if (this.AudioChannel != null)
                {
                    this.AudioChannel.CloseDevice();
                    this.AudioChannel = null;
                }

                if (this.NotifyStreamChannel != null)
                {
                    this.NotifyStreamChannel.Dispose();
                    this.NotifyStreamChannel = null;
                }

                this.ChannelType = 0;
                this.IsVoiceHD = false;
            }
        }

        public int ChannelType
        {
            get { return _ChannelType; }
            private set { _ChannelType = value; }
        }

        public bool IsRegistered
        {
            get { return _IsRegistered; }
            set { _IsRegistered = value; }
        }

        public bool IsVoiceHD
        {
            get { return _IsVoiceHD; }
            private set { _IsVoiceHD = value; }
        }

        public VideoDeviceInfo DeviceInfo
        {
            get { return VideoChannel != null ? VideoChannel.GetDeviceInfo() : null; }
        }

        #region STREAM FREQUENCY COUNT

        private static Timer PUBLISHER_TIMER = null;
        public static int PUBLISHER_RECORDED_COUNTER = 0;
        public static int PUBLISHER_SENT_COUNTER = 0;
<<<<<<< HEAD
        //private static Timer VIEWER_TIMER = null;
        //public static int VIEWER_RECIEVED_COUNTER = 0;
        //public static int VIEWER_DISPLAYED_COUNTER = 0;
=======
        private static Timer VIEWER_TIMER = null;
        public static int VIEWER_RECIEVED_COUNTER = 0;
        public static int VIEWER_DISPLAYED_COUNTER = 0;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

        public static void PublisherStart()
        {
            //PUBLISHER_TIMER = new Timer();
            //PUBLISHER_TIMER.Interval = 1000;
            //PUBLISHER_TIMER.Elapsed += PUBLISHER_TIMER_Elapsed;
            //PUBLISHER_TIMER.Start();
            //PUBLISHER_RECORDED_COUNTER = 0;
            //PUBLISHER_SENT_COUNTER = 0;
        }

        public static void PublisherStop()
        {
            //if (PUBLISHER_TIMER != null)
            //{
            //    PUBLISHER_TIMER.Stop();
            //    PUBLISHER_TIMER.Elapsed -= PUBLISHER_TIMER_Elapsed;
            //    PUBLISHER_TIMER = null;
            //}
        }

<<<<<<< HEAD
        //public static void ViewerStart()
        //{
        //    VIEWER_TIMER = new Timer();
        //    VIEWER_TIMER.Interval = 1000;
        //    VIEWER_TIMER.Elapsed += VIEWER_TIMER_Elapsed;
        //    VIEWER_TIMER.Start();
        //    VIEWER_RECIEVED_COUNTER = 0;
        //    VIEWER_DISPLAYED_COUNTER = 0;
        //}

        //public static void ViewerStop()
        //{
        //    if (VIEWER_TIMER != null)
        //    {
        //        VIEWER_TIMER.Stop();
        //        VIEWER_TIMER.Elapsed -= VIEWER_TIMER_Elapsed;
        //        VIEWER_TIMER = null;
        //    }
        //}
=======
        public static void ViewerStart()
        {
            //VIEWER_TIMER = new Timer();
            //VIEWER_TIMER.Interval = 1000;
            //VIEWER_TIMER.Elapsed += VIEWER_TIMER_Elapsed;
            //VIEWER_TIMER.Start();
            //VIEWER_RECIEVED_COUNTER = 0;
            //VIEWER_DISPLAYED_COUNTER = 0;
        }

        public static void ViewerStop()
        {
            //if (VIEWER_TIMER != null)
            //{
            //    VIEWER_TIMER.Stop();
            //    VIEWER_TIMER.Elapsed -= VIEWER_TIMER_Elapsed;
            //    VIEWER_TIMER = null;
            //}
        }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

        private static void PUBLISHER_TIMER_Elapsed(object sender, ElapsedEventArgs e)
        {
            Debug.WriteLine("RECORDED =>  " + PUBLISHER_RECORDED_COUNTER + ",  SENT  =>  " + PUBLISHER_SENT_COUNTER);
            PUBLISHER_RECORDED_COUNTER = 0;
            PUBLISHER_SENT_COUNTER = 0;
        }

<<<<<<< HEAD
        //private static void VIEWER_TIMER_Elapsed(object sender, ElapsedEventArgs e)
        //{
        //    Debug.WriteLine("RECIEVED =>  " + VIEWER_RECIEVED_COUNTER + ",  DISPLAYED    =>  " + VIEWER_DISPLAYED_COUNTER);
        //    VIEWER_RECIEVED_COUNTER = 0;
        //    VIEWER_DISPLAYED_COUNTER = 0;
        //}
=======
        private static void VIEWER_TIMER_Elapsed(object sender, ElapsedEventArgs e)
        {
            Debug.WriteLine("RECIEVED =>  " + VIEWER_RECIEVED_COUNTER + ",  DISPLAYED    =>  " + VIEWER_DISPLAYED_COUNTER);
            VIEWER_RECIEVED_COUNTER = 0;
            VIEWER_DISPLAYED_COUNTER = 0;
        }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

        #endregion STREAM FREQUENCY COUNT
    }

    public interface INotificationStreamChannel
    {
        void OnResetVideoDisplay();

        void OnResolutionChange(int width, int height);

        void OnAudioStreamSend(byte[] buffer);

        void OnVideoStreamSend(byte[] buffer);

        void OnVideoStreamDisplay(byte[] buffer, int width, int height, int orientation, int channelType);

        void Dispose();

    }
}

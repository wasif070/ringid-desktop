﻿using AForge.Video.DirectShow;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace View.Utility.Stream.Utils
{
    public class VideoDeviceInfo
    {
        public int DeviceType;
        public String Name;
        public String Key;
        public int Width;
        public int Height;

        public VideoDeviceInfo()
        {
            this.DeviceType = StreamConstants.MONITOR_SCREEN;
            this.Name = "Monitor Screen";
            this.Key = "-999";
            this.Width = 640;
            this.Height = (int)((SystemParameters.VirtualScreenHeight * 640) / SystemParameters.VirtualScreenWidth);
        }

        public VideoDeviceInfo(FilterInfo filterInfo)
        {
            this.DeviceType = StreamConstants.WEB_CAMERA;
            this.Name = filterInfo.Name;
            this.Key = filterInfo.MonikerString;
            this.Width = 640;
            this.Height = 480;
        }

        public VideoDeviceInfo(string sourceName, int SourceID)
        {
            this.DeviceType = StreamConstants.APP_SCREEN;
            this.Name = sourceName;
            this.Key = SourceID.ToString();
            this.Width = 640;
            this.Height = 480;
        }

        public override string ToString()
        {
            return "VideoDeviceInfo [DeviceType=" + this.DeviceType + ", Name=" + this.Name + ", Key=" + this.Key + "]";
        }

        public static List<VideoDeviceInfo> GetDeviceList()
        {
            List<VideoDeviceInfo> deviceList = new List<VideoDeviceInfo>();

            FilterInfoCollection collection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo fi in collection)
            {
                deviceList.Add(new VideoDeviceInfo(fi));
            }
            deviceList.Add(new VideoDeviceInfo());

            return deviceList;
        }
    }
}

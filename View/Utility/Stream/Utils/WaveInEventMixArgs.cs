﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NAudio.Wave.Ext
{
    public class WaveInEventMixArgs : EventArgs
    {
        private byte[] buffer;
        private int bytes;
        private byte[] bufferLoopback;
        private int bytesLoopback;

        public WaveInEventMixArgs(byte[] buffer, int bytes, byte[] bufferLoopback, int bytesLoopback)
        {
            this.buffer = buffer;
            this.bytes = bytes;
            this.bufferLoopback = bufferLoopback;
            this.bytesLoopback = bytesLoopback;
        }

        public byte[] Buffer
        {
            get { return buffer; }
        }

        public int BytesRecorded
        {
            get { return bytes; }
        }

        public byte[] BufferLoopback
        {
            get { return bufferLoopback; }
        }

        public int BytesLoopbackRecorded
        {
            get { return bytesLoopback; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NAudio.Wave.SampleProviders.Ext
{
    /// <summary>
    /// Helper class for when you need to convert back to an IWaveProvider from
    /// an ISampleProvider. Keeps it as IEEE float
    /// </summary>
    public class SampleToWaveProvider : NAudio.Wave.Ext.IWaveProvider
    {
        private NAudio.Wave.Ext.ISampleProvider source;

        /// <summary>
        /// Initializes a new instance of the WaveProviderFloatToWaveProvider class
        /// </summary>
        /// <param name="source">Source wave provider</param>
        public SampleToWaveProvider(NAudio.Wave.Ext.ISampleProvider source)
        {
            if (source.WaveFormat.Encoding != WaveFormatEncoding.IeeeFloat)
            {
                throw new ArgumentException("Must be already floating point");
            }
            this.source = source;
        }

        /// <summary>
        /// Reads from this provider
        /// </summary>
        public byte[] Read(byte[] inBuffer, int inCount)
        {
            if (inCount <= 0) return new byte[0];

            int samplesProvided = inCount / 4 /*32 Bit*/;
            float[] providedFloatBuffer = new float[inCount];
            for (int i = 0; i < providedFloatBuffer.Length; i += 4)
            {
                providedFloatBuffer[i / 4] = (BitConverter.ToSingle(inBuffer, i)) * 1; ;
            }

            byte[] data = new byte[(int)(inCount / this.source.Ratio)];
            int samplesNeeded = data.Length / 4 /*32 Bit*/;
            WaveBuffer nWb = new WaveBuffer(data);

            int samplesRead = source.Read(nWb.FloatBuffer, samplesNeeded, providedFloatBuffer, samplesProvided) * 4;
            if (samplesRead <= 0) return new byte[0];

            byte[] outBuffer = null;
            if (source.WaveFormat.Channels == 2)
            {
                byte[] monoData = new byte[samplesRead / 2];
                for (int i = 0, j = 0; i < samplesRead; i += 8)
                {
                    monoData[j++] = data[i + 0];
                    monoData[j++] = data[i + 1];
                    monoData[j++] = data[i + 2];
                    monoData[j++] = data[i + 3];
                }

                outBuffer = new byte[monoData.Length / 2];
                float volume = 1.0F;
                short sample16;
                float sample32;
                for (int i = 0; i < monoData.Length; i += 4)
                {
                    sample32 = (BitConverter.ToSingle(monoData, i)) * volume;
                    if (sample32 > 1.0f)
                        sample32 = 1.0f;
                    if (sample32 < -1.0f)
                        sample32 = -1.0f;
                    sample16 = (short)(sample32 * short.MaxValue);

                    outBuffer[i / 2] = (byte)(sample16 & 0xFF);
                    outBuffer[i / 2 + 1] = (byte)((sample16 >> 8) & 0xFF);
                }
            }
            else
            {
                outBuffer = new byte[data.Length / 2];
                float volume = 1.0F;
                short sample16;
                float sample32;
                for (int i = 0; i < data.Length; i += 4)
                {
                    sample32 = (BitConverter.ToSingle(data, i)) * volume;
                    if (sample32 > 1.0f)
                        sample32 = 1.0f;
                    if (sample32 < -1.0f)
                        sample32 = -1.0f;
                    sample16 = (short)(sample32 * short.MaxValue);

                    outBuffer[i / 2] = (byte)(sample16 & 0xFF);
                    outBuffer[i / 2 + 1] = (byte)((sample16 >> 8) & 0xFF);
                }
            }

            return outBuffer;
        }

        /// <summary>
        /// The waveformat of this WaveProvider (same as the source)
        /// </summary>
        public WaveFormat WaveFormat
        {
            get { return source.WaveFormat; }
        }
    }
}

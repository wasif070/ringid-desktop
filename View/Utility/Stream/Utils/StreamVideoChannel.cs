﻿using AForge.Video;
using AForge.Video.DirectShow;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Threading;
using View.Constants;
using View.Utility.SignInSignUP;

namespace View.Utility.Stream.Utils
{
    public class StreamVideoChannel
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(StreamVideoChannel).Name);

        bool IsReady = false;
        private StreamChannel Stream = null;
        private AForge.Video.Ext.WebcamCaptureStream WebcamCapture;
        private AForge.Video.Ext.MonitorScreenCaptureStream MonitorScreenCapture;
        private AForge.Video.Ext.AppScreenCaptureStream AppScreenCapture;
        private VideoDeviceInfo UsedDeviceInfo;

        public StreamVideoChannel(StreamChannel stream)
        {
            this.Stream = stream;
        }

        public VideoDeviceInfo GetDeviceInfo()
        {
            return this.UsedDeviceInfo;
        }

        public void OpenDevice(VideoDeviceInfo deviceInfo, out bool status)
        {
            status = true;
            CloseDevice();

            if (this.Stream.ChannelType == StreamConstants.CHANNEL_OUT)
            {
                if (deviceInfo != null)
                {
                    if (deviceInfo.DeviceType == StreamConstants.WEB_CAMERA)
                    {
                        log.Debug(">>>>>>>>>>>>>>>>>> OPEN VIDEO = WEB_CAMERA");
                        this.Stream.SetResolution(deviceInfo.Width, deviceInfo.Height);
                        this.WebcamCapture = new AForge.Video.Ext.WebcamCaptureStream(deviceInfo);
                        this.WebcamCapture.VideoResolution = WebcamCapture.VideoCapabilities.OrderByDescending(P => P.FrameSize.Width).FirstOrDefault();
                        this.WebcamCapture.NewFrame += OnNewWebcamFrame;
                        this.WebcamCapture.VideoSourceError += OnVideoSourceError;
                        this.WebcamCapture.Start();
                    }
                    else if (deviceInfo.DeviceType == StreamConstants.APP_SCREEN)
                    {
                        log.Debug(">>>>>>>>>>>>>>>>>> OPEN VIDEO = APP_SCREEN");
                        this.Stream.SetResolution(deviceInfo.Width, deviceInfo.Height);
                        this.AppScreenCapture = new AForge.Video.Ext.AppScreenCaptureStream(deviceInfo);
                        this.AppScreenCapture.NewFrame += OnNewApplicationFrame;
                        this.AppScreenCapture.VideoSourceError += OnVideoSourceError;
                        this.AppScreenCapture.Start();
                    }
                    else if (deviceInfo.DeviceType == StreamConstants.MONITOR_SCREEN)
                    {
                        log.Debug(">>>>>>>>>>>>>>>>>> OPEN VIDEO = MONITOR_SCREEN");
                        this.Stream.SetResolution(deviceInfo.Width, deviceInfo.Height);
                        this.MonitorScreenCapture = new AForge.Video.Ext.MonitorScreenCaptureStream(deviceInfo);
                        this.MonitorScreenCapture.NewFrame += OnNewMonitorFrame;
                        this.MonitorScreenCapture.VideoSourceError += OnVideoSourceError;
                        this.MonitorScreenCapture.Start();
                    }
                }
                else
                {
                    status = false;
                }
            }

            this.UsedDeviceInfo = deviceInfo;
            this.IsReady = true;
        }

        public void CloseDevice()
        {
            this.IsReady = false;
            this.Stream.ResetVideoDisplay();

            if (this.WebcamCapture != null)
            {
                log.Debug(">>>>>>>>>>>>>>>>>> DISTROY VIDEO = WEB_CAMERA");
                this.WebcamCapture.DeviceInfo = null;
                this.WebcamCapture.SignalToStop();
                //this.VideoCapture.Stop();
                this.WebcamCapture.VideoSourceError -= OnVideoSourceError;
                this.WebcamCapture.NewFrame -= OnNewWebcamFrame;
                this.WebcamCapture = null;
            }

            if (this.AppScreenCapture != null)
            {
                log.Debug(">>>>>>>>>>>>>>>>>> DISTROY VIDEO = APP_SCREEN");
                this.AppScreenCapture.DeviceInfo = null;
                this.AppScreenCapture.SignalToStop();
                //this.ScreenCapture.Stop();
                this.AppScreenCapture.VideoSourceError -= OnVideoSourceError;
                this.AppScreenCapture.NewFrame -= OnNewApplicationFrame;
                this.AppScreenCapture = null;
            }

            if (this.MonitorScreenCapture != null)
            {
                log.Debug(">>>>>>>>>>>>>>>>>> DISTROY VIDEO = MONITOR_SCREEN");
                this.MonitorScreenCapture.DeviceInfo = null;
                this.MonitorScreenCapture.SignalToStop();
                //this.ScreenCapture.Stop();
                this.MonitorScreenCapture.VideoSourceError -= OnVideoSourceError;
                this.MonitorScreenCapture.NewFrame -= OnNewMonitorFrame;
                this.MonitorScreenCapture = null;
            }

            this.UsedDeviceInfo = null;
            this.Stream.ResetVideoDisplay();
        }

        private void OnNewWebcamFrame(object sender, NewFrameEventArgs args)
        {
<<<<<<< HEAD
            if (args.Frame != null && this.IsReady)
            {
                this.OnNewFrame(((AForge.Video.Ext.WebcamCaptureStream)sender).DeviceInfo, (Bitmap)args.Frame.Clone());
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (((AForge.Video.Ext.WebcamCaptureStream)sender).DeviceInfo != null)
                    {
                        this.Stream.ResetVideoDisplay();
                    }
                }, DispatcherPriority.Send);
=======
            VideoDeviceInfo deviceInfo = ((AForge.Video.Ext.WebcamCaptureStream)sender).DeviceInfo;
            if (deviceInfo != null)
            {
                if (args.Frame != null && this.IsReady)
                {
                    this.OnNewFrame(deviceInfo, (Bitmap)args.Frame.Clone());
                }
                else
                {
                    this.Stream.ResetVideoDisplay();
                }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
            }
        }

        private void OnNewMonitorFrame(object sender, NewFrameEventArgs args)
        {
<<<<<<< HEAD
            if (args.Frame != null && this.IsReady)
            {
                this.OnNewFrame(((AForge.Video.Ext.MonitorScreenCaptureStream)sender).DeviceInfo, (Bitmap)args.Frame.Clone());
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    if (((AForge.Video.Ext.MonitorScreenCaptureStream)sender).DeviceInfo != null)
                    {
                        this.Stream.ResetVideoDisplay();
                    }
                }, DispatcherPriority.Send);
=======
            VideoDeviceInfo deviceInfo = ((AForge.Video.Ext.MonitorScreenCaptureStream)sender).DeviceInfo;
            if (deviceInfo != null)
            {
                if (args.Frame != null && this.IsReady)
                {
                    this.OnNewFrame(deviceInfo, (Bitmap)args.Frame.Clone());
                }
                else
                {
                    this.Stream.ResetVideoDisplay();
                }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
            }
        }

        private void OnNewApplicationFrame(object sender, NewFrameEventArgs args)
        {
<<<<<<< HEAD
            if (args.Frame != null && this.IsReady)
            {
                this.OnNewFrame(((AForge.Video.Ext.AppScreenCaptureStream)sender).DeviceInfo, (Bitmap)args.Frame.Clone());
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(() => 
                {
                    if (((AForge.Video.Ext.AppScreenCaptureStream)sender).DeviceInfo != null)
                    {
                        this.Stream.ResetVideoDisplay();
                    }
                }, DispatcherPriority.Send);
=======
            VideoDeviceInfo deviceInfo = ((AForge.Video.Ext.AppScreenCaptureStream)sender).DeviceInfo;
            if (deviceInfo != null)
            {
                if (args.Frame != null && this.IsReady)
                {
                    this.OnNewFrame(deviceInfo, (Bitmap)args.Frame.Clone());
                }
                else
                {
                    this.Stream.ResetVideoDisplay();
                }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
            }
        }

        private void OnNewFrame(VideoDeviceInfo deviceInfo, Bitmap bitmap)
        {
            StreamChannel.PUBLISHER_RECORDED_COUNTER++;
<<<<<<< HEAD
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                BitmapData bitmapData = null;
                byte[] rgbValues = null;

                if (deviceInfo == null)
                {
                    bitmap.Dispose();
                    this.Stream.ResetVideoDisplay();
                    return;
                }

                if (bitmap.Width != deviceInfo.Width || bitmap.Height != deviceInfo.Height)
                {
                    bitmap = new Bitmap(bitmap, new System.Drawing.Size(deviceInfo.Width, deviceInfo.Height));
                }

                int newWidth = bitmap.Width;
                int newHeight = bitmap.Height;

                try
                {
                    bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
                    bitmapData = bitmap.LockBits(new Rectangle(0, 0, newWidth, newHeight), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                    IntPtr ptr = bitmapData.Scan0;
                    int bytes = Math.Abs(bitmapData.Stride) * newHeight;
                    rgbValues = new byte[bytes];
                    Marshal.Copy(ptr, rgbValues, 0, bytes);
                    NativeLibrary.DeleteObject(ptr);
                }
                finally
                {
                    if (bitmapData != null) bitmap.UnlockBits(bitmapData);
                    bitmap.Dispose();
                }

                if (this.IsReady && deviceInfo != null && this.UsedDeviceInfo != null && deviceInfo.Key.Equals(this.UsedDeviceInfo.Key))
                {
                    this.Stream.SetVideoStreamBuffer(rgbValues, newWidth, newHeight, 0);
                }
                else
                {
                    this.Stream.ResetVideoDisplay();
                }

            }, DispatcherPriority.Send);
=======

            BitmapData bitmapData = null;
            byte[] rgbValues = null;

            if (bitmap.Width != deviceInfo.Width || bitmap.Height != deviceInfo.Height)
            {
                bitmap = new Bitmap(bitmap, new System.Drawing.Size(deviceInfo.Width, deviceInfo.Height));
            }

            int newWidth = bitmap.Width;
            int newHeight = bitmap.Height;

            try
            {
                bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
                bitmapData = bitmap.LockBits(new Rectangle(0, 0, newWidth, newHeight), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
                IntPtr ptr = bitmapData.Scan0;
                int bytes = Math.Abs(bitmapData.Stride) * newHeight;
                rgbValues = new byte[bytes];
                Marshal.Copy(ptr, rgbValues, 0, bytes);
                NativeLibrary.DeleteObject(ptr);
            }
            finally
            {
                if (bitmapData != null) bitmap.UnlockBits(bitmapData);
                bitmap.Dispose();
            }

            if (this.IsReady && this.UsedDeviceInfo != null && deviceInfo.Key.Equals(this.UsedDeviceInfo.Key))
            {
                this.Stream.SetVideoStreamBuffer(rgbValues, newWidth, newHeight, -1);
            }
            else
            {
                this.Stream.ResetVideoDisplay();
            }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        }

        private void OnVideoSourceError(object sender, VideoSourceErrorEventArgs eventArgs)
        {
            log.Error("Error => OnVideoSourceError()");
            this.CloseDevice();
        }

    }

}

﻿using System;
using System.Linq;
using NAudio.Dsp;
using System.Diagnostics;

namespace NAudio.Wave.SampleProviders.Ext
{

    public class WdlResamplingSampleProvider : NAudio.Wave.Ext.ISampleProvider
    {
        private readonly NAudio.Dsp.Ext.WdlResampler resampler;
        private readonly WaveFormat outFormat;
        private readonly float ratio = 1.0F;

        public WdlResamplingSampleProvider(WaveFormat format, int newSampleRate)
        {
            if (format != null)
            {
                outFormat = WaveFormat.CreateIeeeFloatWaveFormat(newSampleRate, format.Channels);

                resampler = new NAudio.Dsp.Ext.WdlResampler();
                resampler.SetMode(true, 2, false);
                resampler.SetFilterParms();
                resampler.SetFeedMode(false); // output driven
                resampler.SetRates(format.SampleRate, newSampleRate);
                ratio = (float)format.SampleRate / newSampleRate;
                if (ratio < 1)
                {
                    throw new ArgumentException("Only Support Down Resampling");
                }
            }
        }

        public int Read(float[] outBuffer, int outCount, float[] inBuffer, int inCount)
        {
            if (outFormat == null || inCount == 0 || outCount == 0) return 0;

            float[] tempBuffer = null;
            int tempBufferOffset = 0;
            int framesRequested = outCount / outFormat.Channels;
            int inNeeded = resampler.ResamplePrepare(framesRequested, outFormat.Channels, out tempBuffer, out tempBufferOffset);

            int length = tempBuffer.Length - tempBufferOffset < inNeeded * outFormat.Channels ? tempBuffer.Length - tempBufferOffset : inNeeded * outFormat.Channels;
            Array.Copy(inBuffer, 0, tempBuffer, tempBufferOffset, inNeeded * outFormat.Channels);
            int inAvailable = inNeeded * outFormat.Channels / outFormat.Channels;

            int outAvailable = resampler.ResampleOut(outBuffer, 0, inAvailable, framesRequested, outFormat.Channels);
            return outAvailable * outFormat.Channels;
        }

        public WaveFormat WaveFormat
        {
            get { return outFormat; }
        }


        public float Ratio
        {
            get { return ratio; }
        }
    }
}

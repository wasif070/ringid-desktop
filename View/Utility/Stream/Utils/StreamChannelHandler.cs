﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Utility.Call;

namespace View.Utility.Stream.Utils
{
    public class StreamChannelHandler : INotificationStreamChannel
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(StreamChannelHandler).Name);

        private StreamRenderModel _StreamRenderModel = null;
        private bool _disposed = false;

        public StreamChannelHandler(StreamRenderModel renderModel)
        {
            this._StreamRenderModel = renderModel;
        }

        #region Stream Interface

        public void OnResetVideoDisplay()
        {
<<<<<<< HEAD
            if (_StreamRenderModel != null)
            {
                this._StreamRenderModel.RenderSource = null;
            }
=======
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                if (_StreamRenderModel != null) this._StreamRenderModel.RenderSource = null;
            }, System.Windows.Threading.DispatcherPriority.Render);
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        }

        public void OnResolutionChange(int width, int height)
        {
            CallHelperMethods.SetStreamResolution(width, height);
        }

        public void OnAudioStreamSend(byte[] buffer)
        {
            CallHelperMethods.SendStreamAudioData(buffer);
        }

        public void OnVideoStreamSend(byte[] buffer)
        {
            CallHelperMethods.SendStreamVideoData(buffer);
        }

        public void OnVideoStreamDisplay(byte[] buffer, int width, int height, int orientation, int channelType)
        {
            try
            {
                if (this._disposed || _StreamRenderModel == null) return;

                var b = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                var bmpData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
<<<<<<< HEAD
                Marshal.Copy(buffer, 0, bmpData.Scan0, buffer.Length);
=======
                //Marshal.Copy(buffer, 0, bmpData.Scan0, buffer.Length);
                int paddingX = ((4 - (width % 4)) % 4);
                for (int i = 0; i < height;i++ )
                {
                    Marshal.Copy(buffer, i * (width * 3), bmpData.Scan0 + i * ((width * 3) + paddingX), width * 3);
                }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                b.UnlockBits(bmpData);

                switch (orientation)
                {
                    case -1:
                        b.RotateFlip(RotateFlipType.RotateNoneFlipY);
                        break;
                    case 1://-90
                        b.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        break;
                    case 2://180
                        b.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        break;
                    case 3://90
                        b.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        break;
                }

                IntPtr hBitmap = b.GetHbitmap();

<<<<<<< HEAD
=======
                BitmapSource bSource = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                bSource.Freeze();

>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                try
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
<<<<<<< HEAD
                        if (this._disposed || _StreamRenderModel == null) return;

                        this._StreamRenderModel.RenderSource = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
=======
                        StreamChannel.VIEWER_DISPLAYED_COUNTER++;
                        if (this._disposed == false && _StreamRenderModel != null) this._StreamRenderModel.RenderSource = bSource;
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                    }, System.Windows.Threading.DispatcherPriority.Render);
                }
                finally
                {
                    DeleteObject(hBitmap);
                    b.Dispose();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnVideoStreamDisplay() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void Dispose()
        {
            if (this._disposed == false)
            {
                this._disposed = true;
                this.OnResetVideoDisplay();
                this._StreamRenderModel = null;
            }
        }

        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        #endregion Stream Interface
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NAudio.Wave.Ext
{
    /// <summary>
    /// Like IWaveProvider, but makes it much simpler to put together a 32 bit floating
    /// point mixing engine
    /// </summary>
    public interface ISampleProvider
    {

        WaveFormat WaveFormat { get; }

        float Ratio { get; }

        int Read(float[] outBuffer, int outCount, float[] inBuffer, int inCount);
    }
}

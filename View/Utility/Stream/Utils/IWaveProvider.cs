﻿using System;

namespace NAudio.Wave.Ext
{
    /// <summary>
    /// Generic interface for all WaveProviders.
    /// </summary>
    public interface IWaveProvider
    {

        WaveFormat WaveFormat { get; }


        byte[] Read(byte[] inBuffer, int inCount);
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace View.Utility.Stream.Utils
{
    public class CircularStream : System.IO.Stream
    {
        private CircularBuffer<byte> buffer;

        public CircularStream(int bufferCapacity)
            : base()
        {
            buffer = new CircularBuffer<byte>(bufferCapacity);
        }

        public CircularStream(int bufferCapacity, bool allowOverflow)
            : base()
        {
            buffer = new CircularBuffer<byte>(bufferCapacity, allowOverflow);
        }

        public override long Position
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int Capacity
        {
            get { return buffer.Capacity; }
            set { buffer.Capacity = value; }
        }

        public override long Length
        {
            get { return buffer.Size; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public byte[] GetBuffer()
        {
            return buffer.GetBuffer();
        }

        public byte[] ToArray()
        {
            return buffer.ToArray();
        }

        public override void Flush()
        {
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.buffer.Put(buffer, offset, count);
        }

        public void Write(byte[] buffer)
        {
            this.buffer.Put(buffer);
        }

        public override void WriteByte(byte value)
        {
            this.buffer.Put(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return this.buffer.Get(buffer, offset, count);
        }

        public int Read(byte[] buffer)
        {
            return this.buffer.Get(buffer);
        }

        public override int ReadByte()
        {
            return this.buffer.Get();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return buffer != null ? buffer.ToString() : base.ToString();
        }
    }
}

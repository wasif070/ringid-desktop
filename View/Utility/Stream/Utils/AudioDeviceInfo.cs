﻿using Models.Constants;
using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Stream.Utils
{
    public class AudioDeviceInfo
    {
        public int DeviceType;
        public String Name;
        public String Key;
        public MMDevice Device;
        public int DeviceNumber;

        public AudioDeviceInfo(int deviceType, int deviceNumber, MMDevice device)
        {
            this.DeviceType = deviceType;
            this.DeviceNumber = deviceNumber;
            this.Device = device;
            this.Name = device.FriendlyName;
            this.Key = device.ID;
        }

        public override string ToString()
        {
            return "AudioDeviceInfo [DeviceType=" + this.DeviceType + ", DeviceNumber=" + this.DeviceNumber + ", Name=" + this.Name + ", Key=" + this.Key + "]";
        }

        public static List<AudioDeviceInfo> GetDeviceList()
        {
            List<AudioDeviceInfo> deviceList = new List<AudioDeviceInfo>();
            NAudio.CoreAudioApi.MMDeviceEnumerator deviceEnumerator = new NAudio.CoreAudioApi.MMDeviceEnumerator();

            int number = 0;
            MMDeviceCollection collection = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active);
            foreach (MMDevice fi in collection)
            {
                deviceList.Add(new AudioDeviceInfo(StreamConstants.MICROPHONE, number++, fi));
            }

            number = 0;
            collection = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active);
            foreach (MMDevice fi in collection)
            {
                deviceList.Add(new AudioDeviceInfo(StreamConstants.SPEAKER, number++, fi));
            }

            deviceEnumerator = null;
            collection = null;

            return deviceList;
        }
    }
}

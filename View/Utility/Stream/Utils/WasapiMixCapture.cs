﻿using System;
using NAudio.Wave;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace NAudio.CoreAudioApi.Ext
{

    public enum MixCaptureState
    {
        Stopped,
        Starting,
        Capturing,
        Stopping
    }

    public class WasapiMixCapture : IWaveIn
    {
        private const long ReftimesPerSec = 10000000;
        private const long ReftimesPerMillisec = 10000;

        private volatile MixCaptureState captureState;
        private Thread captureThread;
        private readonly SynchronizationContext syncContext;
        private EventWaitHandle frameEventWaitHandle;

        private byte[] recordBuffer;
        private byte[] recordBufferLoopback;
        private AudioClient audioClient;
        private AudioClient audioClientLoopback;

        private int bytesPerFrame;
        private int bytesPerFrameLoopback;
        private WaveFormat waveFormat;
        private WaveFormat waveFormatLoopback;

        private readonly bool isUsingEventSync;
        private readonly int audioBufferMillisecondsLength;
        private bool initialized;

        public event EventHandler<WaveInEventArgs> DataAvailable;
        public event EventHandler<NAudio.Wave.Ext.WaveInEventMixArgs> MixDataAvailable;
        public event EventHandler<StoppedEventArgs> RecordingStopped;

        public WasapiMixCapture() :
            this(GetDefaultCaptureDevice(), GetDefaultLoopbackCaptureDevice())
        {
        }

        public WasapiMixCapture(MMDevice captureDevice, MMDevice captureDeviceLoopback)
            : this(captureDevice, captureDeviceLoopback, false)
        {

        }

        public WasapiMixCapture(MMDevice captureDevice, MMDevice captureDeviceLoopback, bool useEventSync)
            : this(captureDevice, captureDeviceLoopback, useEventSync, 100)
        {
        }

        public WasapiMixCapture(MMDevice captureDevice, MMDevice captureDeviceLoopback, bool useEventSync, int audioBufferMillisecondsLength)
        {
            this.isUsingEventSync = useEventSync;
            this.audioBufferMillisecondsLength = audioBufferMillisecondsLength;

            this.ShareMode = AudioClientShareMode.Shared;
            this.syncContext = SynchronizationContext.Current;

            if (captureDevice != null)
            {
                this.audioClient = captureDevice.AudioClient;
                this.waveFormat = audioClient.MixFormat;
            }

            if (captureDeviceLoopback != null)
            {
                this.audioClientLoopback = captureDeviceLoopback.AudioClient;
                this.waveFormatLoopback = audioClientLoopback.MixFormat;
            }
        }

        public AudioClientShareMode ShareMode { get; set; }

        public MixCaptureState CaptureState { get { return captureState; } }

        public virtual WaveFormat WaveFormat
        {
            get
            {
                return waveFormat;
            }
            set { waveFormat = value; }
        }

        public virtual WaveFormat WaveFormatLoopback
        {
            get
            {
                return waveFormatLoopback;
            }
            set { waveFormatLoopback = value; }
        }

        public static MMDevice GetDefaultCaptureDevice()
        {
            try
            {
                var devices = new MMDeviceEnumerator();
                return devices.GetDefaultAudioEndpoint(DataFlow.Capture, Role.Console);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static MMDevice GetDefaultLoopbackCaptureDevice()
        {
            try
            {
                MMDeviceEnumerator devices = new MMDeviceEnumerator();
                return devices.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void InitializeCaptureDevice()
        {
            if (initialized)
                return;

            long requestedDuration = ReftimesPerMillisec * audioBufferMillisecondsLength;

            if (audioClient != null && !audioClient.IsFormatSupported(ShareMode, waveFormat))
            {
                throw new ArgumentException("Unsupported Wave Format");
            }

            if (audioClientLoopback != null && !audioClientLoopback.IsFormatSupported(ShareMode, waveFormatLoopback))
            {
                throw new ArgumentException("Unsupported Lookback Wave Format");
            }

            // If using EventSync, setup is specific with shareMode
            if (isUsingEventSync)
            {
                // Init Shared or Exclusive
                if (ShareMode == AudioClientShareMode.Shared)
                {
                    // With EventCallBack and Shared, both latencies must be set to 0
                    if (audioClient != null)
                    {
                        audioClient.Initialize(ShareMode, AudioClientStreamFlags.EventCallback | AudioClientStreamFlags.None, requestedDuration, 0, audioClient.MixFormat, Guid.Empty);
                    }
                    if (audioClientLoopback != null)
                    {
                        audioClientLoopback.Initialize(ShareMode, AudioClientStreamFlags.EventCallback | AudioClientStreamFlags.Loopback, requestedDuration, 0, audioClientLoopback.MixFormat, Guid.Empty);
                    }
                }
                else
                {
                    // With EventCallBack and Exclusive, both latencies must equals
                    if (audioClient != null)
                    {
                        audioClient.Initialize(ShareMode, AudioClientStreamFlags.EventCallback | AudioClientStreamFlags.None, requestedDuration, requestedDuration, audioClient.MixFormat, Guid.Empty);
                    }
                    if (audioClientLoopback != null)
                    {
                        audioClientLoopback.Initialize(ShareMode, AudioClientStreamFlags.EventCallback | AudioClientStreamFlags.Loopback, requestedDuration, requestedDuration, audioClientLoopback.MixFormat, Guid.Empty);
                    }
                }

                // Create the Wait Event Handle
                frameEventWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
                if (audioClient != null)
                {
                    audioClient.SetEventHandle(frameEventWaitHandle.SafeWaitHandle.DangerousGetHandle());
                }
                if (audioClientLoopback != null)
                {
                    audioClientLoopback.SetEventHandle(frameEventWaitHandle.SafeWaitHandle.DangerousGetHandle());
                }
            }
            else
            {
                // Normal setup for both sharedMode
                if (audioClient != null)
                {
                    audioClient.Initialize(ShareMode, AudioClientStreamFlags.None, requestedDuration, 0, audioClient.MixFormat, Guid.Empty);
                }
                if (audioClientLoopback != null)
                {
                    audioClientLoopback.Initialize(ShareMode, AudioClientStreamFlags.Loopback, requestedDuration, 0, audioClientLoopback.MixFormat, Guid.Empty);
                }
            }

            if (audioClient != null)
            {
                int bufferFrameCount = audioClient.BufferSize;
                bytesPerFrame = waveFormat.Channels * waveFormat.BitsPerSample / 8;
                recordBuffer = new byte[bufferFrameCount * bytesPerFrame];
            }

            if (audioClientLoopback != null)
            {
                int bufferFrameCountLoopback = audioClientLoopback.BufferSize;
                bytesPerFrameLoopback = waveFormatLoopback.Channels * waveFormatLoopback.BitsPerSample / 8;
                recordBufferLoopback = new byte[bufferFrameCountLoopback * bytesPerFrameLoopback];
            }
            //Debug.WriteLine(string.Format("record buffer size = {0}", this.recordBuffer.Length));

            initialized = true;
        }

        public void StartRecording()
        {
            if (audioClient == null && audioClientLoopback == null)
            {
                return;
            }

            if (captureState != MixCaptureState.Stopped)
            {
                throw new InvalidOperationException("Previous recording still in progress");
            }
            captureState = MixCaptureState.Starting;
            InitializeCaptureDevice();
            ThreadStart start = () => CaptureThread(audioClient, audioClientLoopback);
            captureThread = new Thread(start);
            captureThread.Start();
        }

        public void StopRecording()
        {
            if (captureState != MixCaptureState.Stopped)
                captureState = MixCaptureState.Stopping;
        }

        private void CaptureThread(AudioClient client, AudioClient clientLoopback)
        {
            Exception exception = null;
            try
            {
                DoRecording(client, clientLoopback);
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                if (client != null)
                {
                    client.Stop();
                }

                if (clientLoopback != null)
                {
                    clientLoopback.Stop();
                }
                // don't dispose - the AudioClient only gets disposed when WasapiCapture is disposed
            }
            captureThread = null;
            captureState = MixCaptureState.Stopped;
            RaiseRecordingStopped(exception);
        }

        private void DoRecording(AudioClient client, AudioClient clientLoopback)
        {
            int bufferFrameCount = 0;
            int sampleRate = 0;

            if (client != null)
            {
                bufferFrameCount = client.BufferSize;
                sampleRate = waveFormat.SampleRate;
            }
            else if (clientLoopback != null)
            {
                bufferFrameCount = clientLoopback.BufferSize;
                sampleRate = waveFormatLoopback.SampleRate;
            }

            Debug.WriteLine(String.Format("Client buffer frame count: {0}", bufferFrameCount));

            // Calculate the actual duration of the allocated buffer.
            long actualDuration = (long)((double)ReftimesPerSec * bufferFrameCount / sampleRate);
            int sleepMilliseconds = (int)(actualDuration / ReftimesPerMillisec / 2);
            int waitMilliseconds = (int)(3 * actualDuration / ReftimesPerMillisec);

            AudioCaptureClient capture = null;
            AudioCaptureClient captureLoopback = null;

            if (client != null)
            {
                capture = client.AudioCaptureClient;
                client.Start();
            }

            if (clientLoopback != null)
            {
                captureLoopback = clientLoopback.AudioCaptureClient;
                clientLoopback.Start();
            }

            captureState = MixCaptureState.Capturing;
            while (captureState == MixCaptureState.Capturing)
            {
                bool isRead = true;
                if (isUsingEventSync)
                {
                    isRead = frameEventWaitHandle.WaitOne(waitMilliseconds, false);
                }
                else
                {
                    Thread.Sleep(sleepMilliseconds);
                }
                if (captureState != MixCaptureState.Capturing)
                    break;

                // If still recording and notification is ok
                if (isRead)
                {
                    ReadNextPacket(capture, captureLoopback);
                }
            }
        }

        private void RaiseRecordingStopped(Exception e)
        {
            var handler = RecordingStopped;
            if (handler == null) return;
            if (syncContext == null)
            {
                handler(this, new StoppedEventArgs(e));
            }
            else
            {
                syncContext.Post(state => handler(this, new StoppedEventArgs(e)), null);
            }
        }

        private void ReadNextPacket(AudioCaptureClient capture, AudioCaptureClient captureLoopback)
        {
            int packetSize = capture != null ? capture.GetNextPacketSize() : 0;
            int packetSizeLoopback = captureLoopback != null ? captureLoopback.GetNextPacketSize() : 0;
            //Debug.WriteLine(packetSize + ", " + packetSizeLoopback);

            int recordBufferOffset = 0;
            int recordBufferOffsetLoopback = 0;

            while (packetSize != 0 || packetSizeLoopback != 0)
            {
                if (packetSize != 0)
                {
                    int framesAvailable;
                    AudioClientBufferFlags flags;
                    IntPtr buffer = capture.GetBuffer(out framesAvailable, out flags);
                    int bytesAvailable = Math.Min(framesAvailable * bytesPerFrame, recordBuffer.Length - recordBufferOffset);

                    //int spaceRemaining = Math.Max(0, recordBuffer.Length - recordBufferOffset);
                    //if (spaceRemaining < bytesAvailable && recordBufferOffset > 0)
                    //{
                    //    if (MixDataAvailable != null)
                    //    {
                    //        MixDataAvailable(this, new NAudio.Wave.Ext.WaveInEventMixArgs(recordBuffer, recordBufferOffset, recordLoopbackBuffer, recordLoopbackBufferOffset));
                    //    }
                    //    recordBufferOffset = 0;
                    //}

                    // if not silence...
                    if ((flags & AudioClientBufferFlags.Silent) != AudioClientBufferFlags.Silent)
                    {
                        Marshal.Copy(buffer, recordBuffer, recordBufferOffset, bytesAvailable);
                    }
                    else
                    {
                        Array.Clear(recordBuffer, recordBufferOffset, bytesAvailable);
                    }

                    recordBufferOffset += bytesAvailable;
                    capture.ReleaseBuffer(framesAvailable);
                }

                if (packetSizeLoopback != 0)
                {
                    int framesAvailableLoopback;
                    AudioClientBufferFlags flagsLoopback;
                    IntPtr bufferLoopback = captureLoopback.GetBuffer(out framesAvailableLoopback, out flagsLoopback);
                    int bytesAvailableLoopback = Math.Min(framesAvailableLoopback * bytesPerFrameLoopback, recordBufferLoopback.Length - recordBufferOffsetLoopback);

                    //int spaceRemaining = Math.Max(0, recordLoopbackBuffer.Length - recordLoopbackBufferOffset);
                    //if (spaceRemaining < bytesAvailableLoopback && recordLoopbackBufferOffset > 0)
                    //{
                    //    if (MixDataAvailable != null)
                    //    {
                    //        MixDataAvailable(this, new NAudio.Wave.Ext.WaveInEventMixArgs(recordBuffer, recordBufferOffset, recordLoopbackBuffer, recordLoopbackBufferOffset));
                    //    }
                    //    recordLoopbackBufferOffset = 0;
                    //}

                    // if not silence...
                    if ((flagsLoopback & AudioClientBufferFlags.Silent) != AudioClientBufferFlags.Silent)
                    {
                        Marshal.Copy(bufferLoopback, recordBufferLoopback, recordBufferOffsetLoopback, bytesAvailableLoopback);
                    }
                    else
                    {
                        Array.Clear(recordBufferLoopback, recordBufferOffsetLoopback, bytesAvailableLoopback);
                    }

                    recordBufferOffsetLoopback += bytesAvailableLoopback;
                    captureLoopback.ReleaseBuffer(framesAvailableLoopback);
                }

                packetSize = capture != null ? capture.GetNextPacketSize() : 0;
                packetSizeLoopback = captureLoopback != null ? captureLoopback.GetNextPacketSize() : 0;
                //Debug.WriteLine(packetSize + ", " + packetSizeLoopback);
            }

            if (MixDataAvailable != null)
            {
                MixDataAvailable(this, new NAudio.Wave.Ext.WaveInEventMixArgs(recordBuffer, recordBufferOffset, recordBufferLoopback, recordBufferOffsetLoopback));
            }

            //Debug.WriteLine(capture.GetNextPacketSize() + ", " + captureLoopback.GetNextPacketSize());

            /*int packetSize = capture.GetNextPacketSize();
            int recordBufferOffset = 0;
            //Debug.WriteLine(string.Format("packet size: {0} samples", packetSize / 4));

            while (packetSize != 0)
            {
                int framesAvailable;
                AudioClientBufferFlags flags;
                IntPtr buffer = capture.GetBuffer(out framesAvailable, out flags);

                int bytesAvailable = framesAvailable * bytesPerFrameLoopback;

                // apparently it is sometimes possible to read more frames than we were expecting?
                // fix suggested by Michael Feld:
                int spaceRemaining = Math.Max(0, recordLoopbackBuffer.Length - recordBufferOffset);
                if (spaceRemaining < bytesAvailable && recordBufferOffset > 0)
                {
                    if (DataAvailable != null)
                    {
                        DataAvailable(this, new WaveInEventArgs(recordLoopbackBuffer, recordBufferOffset));
                    }
                    recordBufferOffset = 0;
                }

                // if not silence...
                if ((flags & AudioClientBufferFlags.Silent) != AudioClientBufferFlags.Silent)
                {
                    Marshal.Copy(buffer, recordLoopbackBuffer, recordBufferOffset, bytesAvailable);
                }
                else
                {
                    Array.Clear(recordLoopbackBuffer, recordBufferOffset, bytesAvailable);
                }
                recordBufferOffset += bytesAvailable;
                capture.ReleaseBuffer(framesAvailable);
                packetSize = capture.GetNextPacketSize();
            }

            if (DataAvailable != null)
            {
                DataAvailable(this, new WaveInEventArgs(recordLoopbackBuffer, recordBufferOffset));
            }*/
        }

        public void Dispose()
        {
            StopRecording();
            if (captureThread != null)
            {
                captureThread.Join();
                captureThread = null;
            }
            if (audioClientLoopback != null)
            {
                audioClientLoopback.Dispose();
                audioClientLoopback = null;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NAudio.Wave.SampleProviders.Ext
{
    /// <summary>
    /// Helper class for when you need to convert back to an IWaveProvider from
    /// an ISampleProvider. Keeps it as IEEE float
    /// </summary>
    public class SampleToWaveMixProvider : NAudio.Wave.Ext.IWaveMixProvider
    {
        private NAudio.Wave.Ext.ISampleProvider source;
        private NAudio.Wave.Ext.ISampleProvider sourceLoopback;
        private int _Channel = 1;
        private int _ChannelLoopback = 1;

        /// <summary>
        /// Initializes a new instance of the WaveProviderFloatToWaveProvider class
        /// </summary>
        /// <param name="source">Source wave provider</param>
        public SampleToWaveMixProvider(NAudio.Wave.Ext.ISampleProvider source, NAudio.Wave.Ext.ISampleProvider sourceLoopback)
        {
            if (source.WaveFormat != null)
            {
                this._Channel = source.WaveFormat.Channels;
                if(source.WaveFormat.Encoding != WaveFormatEncoding.IeeeFloat) throw new ArgumentException("Must be already floating point");
            }
            if (sourceLoopback.WaveFormat != null)
            {
                this._ChannelLoopback = sourceLoopback.WaveFormat.Channels;
                if (sourceLoopback.WaveFormat.Encoding != WaveFormatEncoding.IeeeFloat) throw new ArgumentException("Must be already floating point");
            }

            this.source = source;
            this.sourceLoopback = sourceLoopback;
        }

        /// <summary>
        /// Reads from this provider
        /// </summary>
        public byte[] Read(byte[] inBuffer, int inCount, byte[] inBufferLoopback, int inCountLoopback)
        {
            byte[] outBuffer = null;

            int samplesProvided = inCount / 4; //32 Bit;
            float[] providedFloatBuffer = new float[inCount];
            for (int i = 0; i < providedFloatBuffer.Length; i += 4)
            {
                providedFloatBuffer[i / 4] = (BitConverter.ToSingle(inBuffer, i)) * 1;
            }

            byte[] data = new byte[(int)(inCount / this.source.Ratio)];
            int samplesNeeded = data.Length / 4; //32 Bit;
            WaveBuffer nWb = new WaveBuffer(data);

            int samplesProvidedLoopback = inCountLoopback / 4; //32 Bit;
            float[] providedFloatBufferLoopback = new float[inCountLoopback];
            for (int i = 0; i < providedFloatBufferLoopback.Length; i += 4)
            {
                providedFloatBufferLoopback[i / 4] = (BitConverter.ToSingle(inBufferLoopback, i)) * 1; ;
            }

            byte[] dataLoopback = new byte[(int)(inCountLoopback / this.sourceLoopback.Ratio)];
            int samplesNeededLoopback = dataLoopback.Length / 4; //32 Bit;
            WaveBuffer nWbLoopback = new WaveBuffer(dataLoopback);

            int samplesRead = source.Read(nWb.FloatBuffer, samplesNeeded, providedFloatBuffer, samplesProvided) * 4;
            int samplesReadLoopback = sourceLoopback.Read(nWbLoopback.FloatBuffer, samplesNeededLoopback, providedFloatBufferLoopback, samplesProvidedLoopback) * 4;

            int inc = 4 * this._Channel;
            byte[] monoData = new byte[samplesRead / this._Channel];
            for (int i = 0, j = 0; i < samplesRead; i += inc)
            {
                monoData[j++] = data[i + 0];
                monoData[j++] = data[i + 1];
                monoData[j++] = data[i + 2];
                monoData[j++] = data[i + 3];
            }

            int incLoopback = 4 * this._ChannelLoopback;
            byte[] monoDataLoopback = new byte[samplesReadLoopback / this._ChannelLoopback];
            for (int i = 0, j = 0; i < samplesReadLoopback; i += incLoopback)
            {
                monoDataLoopback[j++] = dataLoopback[i + 0];
                monoDataLoopback[j++] = dataLoopback[i + 1];
                monoDataLoopback[j++] = dataLoopback[i + 2];
                monoDataLoopback[j++] = dataLoopback[i + 3];
            }

            float volume = 1.0F;
            short sample16;
            float sample32;

            outBuffer = new byte[Math.Max(monoData.Length, monoDataLoopback.Length) / 2];
<<<<<<< HEAD
            if (monoDataLoopback.Length > monoData.Length)
            {
                Sum32BitAudio(monoDataLoopback, 0, monoData, monoData.Length);

                for (int idx = 0; idx < monoDataLoopback.Length; idx += 4)
                {
                    sample32 = BitConverter.ToSingle(monoDataLoopback, idx) * volume;
                    if (sample32 > 1.0f)
                        sample32 = 1.0f;
                    if (sample32 < -1.0f)
                        sample32 = -1.0f;
                    sample16 = (short)(sample32 * short.MaxValue);

                    outBuffer[idx / 2] = (byte)(sample16 & 0xFF);
                    outBuffer[idx / 2 + 1] = (byte)((sample16 >> 8) & 0xFF);
                }
            }
            else
            {
                Sum32BitAudio(monoData, 0, monoDataLoopback, monoDataLoopback.Length);

                for (int idx = 0; idx < monoData.Length; idx += 4)
                {
                    sample32 = BitConverter.ToSingle(monoData, idx) * volume;
                    if (sample32 > 1.0f)
                        sample32 = 1.0f;
                    if (sample32 < -1.0f)
                        sample32 = -1.0f;
                    sample16 = (short)(sample32 * short.MaxValue);

                    outBuffer[idx / 2] = (byte)(sample16 & 0xFF);
                    outBuffer[idx / 2 + 1] = (byte)((sample16 >> 8) & 0xFF);
                }
            }

            /*int maxLength = Math.Max(monoData.Length, monoDataLoopback.Length);
            int idx = 0;
            float volume = 1.0F;
            short sample16;
            float sample32;
            outBuffer = new byte[maxLength / 2];

            for (; idx < maxLength; idx += 4)
            {
                sample32 = ((idx < monoData.Length ? BitConverter.ToSingle(monoData, idx) : 0) + (idx < monoDataLoopback.Length ? BitConverter.ToSingle(monoDataLoopback, idx) : 0)) * volume;
                if (sample32 > 1.0f)
                    sample32 = 1.0f;
                if (sample32 < -1.0f)
                    sample32 = -1.0f;
                sample16 = (short)(sample32 * short.MaxValue);

                outBuffer[idx / 2] = (byte)(sample16 & 0xFF);
                outBuffer[idx / 2 + 1] = (byte)((sample16 >> 8) & 0xFF);
            }*/

=======
            if (outBuffer.Length > 0)
            {
                if (monoDataLoopback.Length > monoData.Length)
                {
                    if (monoData.Length > 0)
                    {
                        Sum32BitAudio(monoDataLoopback, 0, monoData, monoData.Length);
                    }

                    for (int idx = 0; idx < monoDataLoopback.Length; idx += 4)
                    {
                        sample32 = BitConverter.ToSingle(monoDataLoopback, idx) * volume;
                        if (sample32 > 1.0f)
                            sample32 = 1.0f;
                        if (sample32 < -1.0f)
                            sample32 = -1.0f;
                        sample16 = (short)(sample32 * short.MaxValue);

                        outBuffer[idx / 2] = (byte)(sample16 & 0xFF);
                        outBuffer[idx / 2 + 1] = (byte)((sample16 >> 8) & 0xFF);
                    }
                }
                else
                {
                    if (monoDataLoopback.Length > 0)
                    {
                        Sum32BitAudio(monoData, 0, monoDataLoopback, monoDataLoopback.Length);
                    }

                    for (int idx = 0; idx < monoData.Length; idx += 4)
                    {
                        sample32 = BitConverter.ToSingle(monoData, idx) * volume;
                        if (sample32 > 1.0f)
                            sample32 = 1.0f;
                        if (sample32 < -1.0f)
                            sample32 = -1.0f;
                        sample16 = (short)(sample32 * short.MaxValue);

                        outBuffer[idx / 2] = (byte)(sample16 & 0xFF);
                        outBuffer[idx / 2 + 1] = (byte)((sample16 >> 8) & 0xFF);
                    }
                }
            }

>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
            return outBuffer;
        }

        static unsafe void Sum32BitAudio(byte[] destBuffer, int offset, byte[] sourceBuffer, int bytesRead)
        {
            fixed (byte* pDestBuffer = &destBuffer[offset],
                      pSourceBuffer = &sourceBuffer[0])
            {
                float* pfDestBuffer = (float*)pDestBuffer;
                float* pfReadBuffer = (float*)pSourceBuffer;
                int samplesRead = bytesRead / 4;
                for (int n = 0; n < samplesRead; n++)
                {
                    pfDestBuffer[n] += pfReadBuffer[n];
                }
            }
        }

        /// <summary>
        /// The waveformat of this WaveProvider (same as the source)
        /// </summary>
        public WaveFormat WaveFormat
        {
            get { return source.WaveFormat; }
        }

        public WaveFormat WaveFormatLoopback
        {
            get { return sourceLoopback.WaveFormat; }
        }
    }
}

﻿using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.Utility.Stream.Utils;

namespace AForge.Video.Ext
{
    public class WebcamCaptureStream : VideoCaptureDevice
    {
        private VideoDeviceInfo _DeviceInfo;
        public VideoDeviceInfo DeviceInfo
        {
            get { return _DeviceInfo; }
            set { _DeviceInfo = value; }
        }

        public WebcamCaptureStream(VideoDeviceInfo deviceInfo)
            : base(deviceInfo.Key)
        {
            this.DeviceInfo = deviceInfo;
        }
    }
}

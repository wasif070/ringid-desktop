﻿using log4net;
using Models.Constants;
using NAudio.CoreAudioApi;
using NAudio.CoreAudioApi.Interfaces;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using View.Utility.Naudio;

namespace View.Utility.Stream.Utils
{
    public class StreamAudioChannel
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(StreamAudioChannel).Name);

        public bool IsReady = false;
        public bool IsMuted = false;
        private WaveFormat PCMFormat = new WaveFormat(8000, 16, 1);
        private WaveFormat AACFormat = new WaveFormat(44100, 16, 2);
        public StreamChannel Stream = null;
        private BufferedWaveProvider BwProvider;
        private WaveOut PlayerWave = null;
        private WaveIn MicRecorderWave = null;
        private NAudio.CoreAudioApi.Ext.WasapiCapture SpeakerRecorderWave = null;
        private NAudio.Wave.SampleProviders.Ext.WdlResamplingSampleProvider SpeakerResamplingProvider = null;
        private NAudio.Wave.SampleProviders.Ext.SampleToWaveProvider SpeakerWaveProvider = null;
        private NAudio.CoreAudioApi.Ext.WasapiMixCapture MicSpreakerRecorderWave = null;
        private NAudio.Wave.SampleProviders.Ext.WdlResamplingSampleProvider MicSpeakerResamplingProvider1 = null;
        private NAudio.Wave.SampleProviders.Ext.WdlResamplingSampleProvider MicSpeakerResamplingProvider2 = null;
        private NAudio.Wave.SampleProviders.Ext.SampleToWaveMixProvider MicSpeakerWaveProvider = null;
        private CircularStream CircularStream = null;
        private List<AudioDeviceInfo> UsedDeviceInfos = null;
        //WaveFileWriter WaveWriter = null;

        public StreamAudioChannel(StreamChannel stream)
        {
            this.Stream = stream;
        }

        public List<AudioDeviceInfo> GetDeviceInfos()
        {
            return this.UsedDeviceInfos;
        }

        public void OpenDevice(List<AudioDeviceInfo> audioDeviceList)
        {
            this.CloseDevice();

            //this.WaveWriter = new WaveFileWriter(@"E:\output_mix_8000_16_1.wav", new WaveFormat(PCMFormat.SampleRate, PCMFormat.BitsPerSample, PCMFormat.Channels));

            if (audioDeviceList != null && audioDeviceList.Count > 0)
            {
                if (this.Stream.ChannelType == StreamConstants.CHANNEL_OUT)
                {
                    if (audioDeviceList.Count > 1)
                    {
                        log.Debug(">>>>>>>>>>>>>>>>>> OPEN AUDIO = MICROPHONE_SPEAKER_MIX");
                        this.CircularStream = new CircularStream(PCMFormat.SampleRate, true);
                        this.MicSpreakerRecorderWave = new NAudio.CoreAudioApi.Ext.WasapiMixCapture(audioDeviceList[0].Device, audioDeviceList[1].Device);
                        this.MicSpeakerResamplingProvider1 = new NAudio.Wave.SampleProviders.Ext.WdlResamplingSampleProvider(this.MicSpreakerRecorderWave.WaveFormat, PCMFormat.SampleRate);
                        this.MicSpeakerResamplingProvider2 = new NAudio.Wave.SampleProviders.Ext.WdlResamplingSampleProvider(this.MicSpreakerRecorderWave.WaveFormatLoopback, PCMFormat.SampleRate);
                        this.MicSpeakerWaveProvider = new NAudio.Wave.SampleProviders.Ext.SampleToWaveMixProvider(this.MicSpeakerResamplingProvider1, this.MicSpeakerResamplingProvider2);
                        this.MicSpreakerRecorderWave.MixDataAvailable += OnDataAvailableInMicSpeaker;
                        this.MicSpreakerRecorderWave.StartRecording();
                    }
                    else if (audioDeviceList[0].DeviceType == StreamConstants.MICROPHONE)
                    {
                        if (audioDeviceList[0].DeviceNumber >= 0)
                        {
                            log.Debug(">>>>>>>>>>>>>>>>>> OPEN AUDIO = MICROPHONE");
                            this.MicRecorderWave = new WaveIn();
                            this.MicRecorderWave.DataAvailable += OnDataAvailableInMic;
                            this.MicRecorderWave.WaveFormat = PCMFormat;
                            this.MicRecorderWave.DeviceNumber = audioDeviceList[0].DeviceNumber;
                            this.MicRecorderWave.StartRecording();
                        }
                    }
                    else if (audioDeviceList[0].DeviceType == StreamConstants.SPEAKER)
                    {
                        log.Debug(">>>>>>>>>>>>>>>>>> OPEN AUDIO = SPEAKER");
                        this.CircularStream = new CircularStream(PCMFormat.SampleRate, true);
                        this.SpeakerRecorderWave = new NAudio.Wave.Ext.WasapiLoopbackCapture(audioDeviceList[0].Device);
                        this.SpeakerResamplingProvider = new NAudio.Wave.SampleProviders.Ext.WdlResamplingSampleProvider(this.SpeakerRecorderWave.WaveFormat, PCMFormat.SampleRate);
                        this.SpeakerWaveProvider = new NAudio.Wave.SampleProviders.Ext.SampleToWaveProvider(this.SpeakerResamplingProvider);
                        this.SpeakerRecorderWave.DataAvailable += OnDataAvailableInSpeaker;
                        this.SpeakerRecorderWave.StartRecording();
                    }
                }
                else
                {
                    if (audioDeviceList[0].DeviceType == StreamConstants.SPEAKER)
                    {
                        if (audioDeviceList[0].DeviceNumber >= 0)
                        {
                            log.Debug(">>>>>>>>>>>>>>>>>> OPEN AUDIO = PLAYER");
                            this.PlayerWave = new WaveOut();
                            this.BwProvider = new BufferedWaveProvider(this.Stream.IsVoiceHD ? AACFormat : PCMFormat);
                            this.PlayerWave.Init(this.BwProvider);
                            this.BwProvider.DiscardOnBufferOverflow = true;
                            this.BwProvider.ClearBuffer();
                            this.PlayerWave.Volume = this.IsMuted ? 0 : 1;
                            this.PlayerWave.DeviceNumber = audioDeviceList[0].DeviceNumber;
                            this.PlayerWave.Play();
                        }
                    }
                }
            }

            this.UsedDeviceInfos = audioDeviceList;
            this.IsReady = true;
        }

        public void CloseDevice()
        {
            this.IsReady = false;

            if (this.MicSpreakerRecorderWave != null)
            {
                log.Debug(">>>>>>>>>>>>>>>>>> DISTROY AUDIO = MICROPHONE_SPEAKER_MIX");
                this.MicSpreakerRecorderWave.MixDataAvailable -= OnDataAvailableInMicSpeaker;
                this.MicSpreakerRecorderWave.StopRecording();
                this.MicSpreakerRecorderWave.Dispose();
                this.MicSpreakerRecorderWave = null;
                this.MicSpeakerWaveProvider = null;
                this.MicSpeakerResamplingProvider1 = null;
                this.MicSpeakerResamplingProvider2 = null;
            }

            if (this.SpeakerRecorderWave != null)
            {
                log.Debug(">>>>>>>>>>>>>>>>>> DISTROY AUDIO = SPEAKER");
                this.SpeakerRecorderWave.DataAvailable -= OnDataAvailableInSpeaker;
                this.SpeakerRecorderWave.StopRecording();
                this.SpeakerRecorderWave.Dispose();
                this.SpeakerRecorderWave = null;
                this.SpeakerWaveProvider = null;
                this.SpeakerResamplingProvider = null;
            }

            if (this.MicRecorderWave != null)
            {
                log.Debug(">>>>>>>>>>>>>>>>>> DISTROY AUDIO = MICROPHONE");
                this.MicRecorderWave.DataAvailable -= OnDataAvailableInMic;
                this.MicRecorderWave.StopRecording();
                this.MicRecorderWave.Dispose();
                this.MicRecorderWave = null;
            }

            if (this.PlayerWave != null)
            {
                log.Debug(">>>>>>>>>>>>>>>>>> DISTROY AUDIO = PLAYER");
                this.PlayerWave.Stop();
                this.PlayerWave.Dispose();
                this.PlayerWave = null;
            }

            if (this.BwProvider != null)
            {
                this.BwProvider.ClearBuffer();
                this.BwProvider = null;
            }

            if (this.CircularStream != null)
            {
                this.CircularStream.Dispose();
                this.CircularStream = null;
            }

            //if (this.WaveWriter != null) 
            //{ 
                //this.WaveWriter.Dispose();
                //this.WaveWriter = null;
            //}

            this.UsedDeviceInfos = null;
        }

        private void OnDataAvailableInMicSpeaker(object sender, NAudio.Wave.Ext.WaveInEventMixArgs args)
        {
            if (this.IsMuted)
            {
                byte[] temp = new byte[1600];
                //this.WaveWriter.Write(temp, 0, temp.Length);
                this.Stream.SetAudioStreamBuffer(temp);
            }
            else
            {
                if (this.MicSpeakerWaveProvider != null)
                {
                    byte[] buffer = this.MicSpeakerWaveProvider.Read(args.Buffer, args.BytesRecorded, args.BufferLoopback, args.BytesLoopbackRecorded);
                    if (buffer.Length > 0)
                    {
                        this.CircularStream.Write(buffer);
                        if (this.CircularStream.Length >= 1600)
                        {
                            byte[] temp = new byte[1600];
                            this.CircularStream.Read(temp);
                            //this.WaveWriter.Write(temp, 0, temp.Length);
                            this.Stream.SetAudioStreamBuffer(temp);
                        }
                    }
                }
            }
        }

        private void OnDataAvailableInSpeaker(object sender, WaveInEventArgs args)
        {
            if (this.IsMuted)
            {
                byte[] temp = new byte[1600];
                //this.WaveWriter.Write(temp, 0, temp.Length);
                this.Stream.SetAudioStreamBuffer(temp);
            }
            else
            {
                if (this.SpeakerWaveProvider != null)
                {
                    byte[] buffer = this.SpeakerWaveProvider.Read(args.Buffer, args.BytesRecorded);
                    if (buffer.Length > 0)
                    {
                        this.CircularStream.Write(buffer);
                        if (this.CircularStream.Length >= 1600)
                        {
                            byte[] temp = new byte[1600];
                            this.CircularStream.Read(temp);
                            //this.WaveWriter.Write(temp, 0, temp.Length);
                            this.Stream.SetAudioStreamBuffer(temp);
                        }
                    }
                }
            }
        }

        private void OnDataAvailableInMic(object sender, WaveInEventArgs args)
        {
            if (this.IsMuted)
            {
                byte[] temp = new byte[args.Buffer.Length];
                //this.WaveWriter.Write(temp, 0, temp.Length);
                this.Stream.SetAudioStreamBuffer(temp);
            }
            else
            {
                //this.WaveWriter.Write(args.Buffer, 0, args.Buffer.Length);
                this.Stream.SetAudioStreamBuffer(args.Buffer);
            }
        }

        public void PlayAudioSample(byte[] byteToPlay)
        {
            if (this.BwProvider != null)
            {
                this.BwProvider.AddSamples(byteToPlay, 0, byteToPlay.Length);
            }
        }

    }

}

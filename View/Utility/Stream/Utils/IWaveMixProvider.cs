﻿using System;

namespace NAudio.Wave.Ext
{
    /// <summary>
    /// Generic interface for all WaveProviders.
    /// </summary>
    public interface IWaveMixProvider
    {

        WaveFormat WaveFormat { get; }

        WaveFormat WaveFormatLoopback { get; }

        byte[] Read(byte[] inBuffer, int inCount, byte[] inBufferLoopBack, int inCountLoopback);

    }
}

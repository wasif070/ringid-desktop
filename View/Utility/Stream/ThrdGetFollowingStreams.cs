﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Stream
{
    public class ThrdGetFollowingStreams
    {
        private int _Limit;
        private long _Time;
        private int _StreamScroll;
        private Func<bool, int> _OnComplete;

        public ThrdGetFollowingStreams(int scroll, int limit, long time, Func<bool, int> onComplete = null)
        {
            this._StreamScroll = scroll;
            this._Limit = limit;
            this._Time = time;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            if (_Time > 0)
            {
                pakToSend[JsonKeys.StreamScroll] = _StreamScroll;
            }
            pakToSend[JsonKeys.Limit] = _Limit;
            pakToSend[JsonKeys.Time] = _Time;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_FOLLOWING_STREAMS;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                if (_OnComplete != null)
                {
                    _OnComplete(true);
                }
            }
            else
            {
                if (_OnComplete != null)
                {
                    _OnComplete(false);
                }
            }
        }
    }
}

﻿using Auth.utility;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.Utility.Auth;

namespace View.Utility.Stream
{
    public class ThrdGetStreamCategoryList
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThrdStartLiveStream).Name);
        private string _SearchParameter = string.Empty;
        private Func<bool, int> _OnComplete;

        public ThrdGetStreamCategoryList(string searchParam, Func<bool, int> onComplete = null)
        {
            this._SearchParameter = searchParam;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            //pakToSend[JsonKeys.StreamSearchParm] = _SearchParameter;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_STREAM_CATEGORY_LIST;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._OnComplete != null)
            {
                this._OnComplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }
        }
    }
}

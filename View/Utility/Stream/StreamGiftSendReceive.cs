﻿using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;
using View.UI.Stream;

namespace View.Utility.Stream
{
    public class StreamGiftSendReceive : BackgroundWorker
    {
        private readonly ILog log = LogManager.GetLogger(typeof(StreamGiftSendReceive).Name);

        private ConcurrentQueue<WalletGiftProductModel> _GiftQueue = new ConcurrentQueue<WalletGiftProductModel>();
        public bool _IsReady = true;
        public static StreamGiftSendReceive _Instance;
        private WalletGiftProductModel _CurrentGiftModel;

        public StreamGiftSendReceive()
        {
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += StreamGiftSendReceive_Do_Work;
            RunWorkerCompleted += StreamGiftSendReceive_RunWorkerCompleted;
        }

        public static StreamGiftSendReceive Instance
        {
            get
            {
                _Instance = _Instance ?? new StreamGiftSendReceive();
                return _Instance;
            }
        }

        public void Initialize()
        {

        }

        public void AddGift(WalletGiftProductModel giftModel)
        {
            if (_IsReady)
            {
                _GiftQueue.Enqueue(giftModel);
                //this.StartThread();
            }
        }

        public void StartThread(WalletGiftProductModel newGiftModel)
        {
            if (!this.IsBusy)
            {
                this._CurrentGiftModel = newGiftModel;
                this.RunWorkerAsync();
            }
        }

        public void CancelThread()
        {
            if (_IsReady == false)
            {
                this.CancelAsync();
            }
        }

        private void StreamGiftSendReceive_Do_Work(object sender, DoWorkEventArgs e)
        {
            try
            {
                WalletGiftProductModel giftModel = _CurrentGiftModel != null ? _GiftQueue.Where(P => P.ProductID == _CurrentGiftModel.ProductID).FirstOrDefault() : null;
                if (_GiftQueue.TryDequeue(out giftModel) && giftModel != null)
                {
                    giftModel.GiftFilePath = _CurrentGiftModel.GiftFilePath;
                    if (this.CancellationPending || _IsReady == false)
                    {
                        e.Cancel = true;
                        //break;
                    }

                    UCStreamLiveViewer viewer = StreamHelpers.GetCurruentStreamLiveViewer();
                    if (viewer != null)
                    {
                        giftModel.IsNeedToDisplay = true;
                        viewer.ShowGiftImage(giftModel.GiftFilePath);
                        Thread.Sleep(giftModel.GiftDisplayingTime);
                        giftModel.IsNeedToDisplay = false;
                        viewer.HideGiftImage();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("StreamGiftSendReceive_Do_Work() ==> " + ex.Message + "\n" + ex.StackTrace);
                e.Cancel = true;
            }
        }

        private void StreamGiftSendReceive_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
    }
}

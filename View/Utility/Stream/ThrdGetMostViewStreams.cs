﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Stream
{
    public class ThrdGetMostViewStreams
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdGetMostViewStreams).Name);

        private int _StartTime;
        private int _Limit;
        private Func<bool, int> _OnComplete;

        public ThrdGetMostViewStreams(int startLimit, int limit, Func<bool, int> onComplete = null)
        {
            this._StartTime = startLimit;
            this._Limit = limit;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.StartLimit] = _StartTime;
            pakToSend[JsonKeys.Limit] = _Limit;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_MOST_VIEWED_STREAMS;
            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null)
            {
                if (this._OnComplete != null)
                {
                    this._OnComplete(true);
                }
            }
            else
            {
                if (this._OnComplete != null)
                {
                    this._OnComplete(false);
                }
            }
        }

    }
}

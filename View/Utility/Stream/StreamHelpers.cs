﻿using callsdkwrapper;
using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using View.BindingModels;
using View.UI;
using View.UI.Stream;
using View.UI.StreamAndChannel;
using View.Utility.Call;
using View.Utility.Chat;
using View.Utility.Notification;
using View.Utility.Stream.Utils;
using View.ViewModel;

namespace View.Utility.Stream
{
    public static class StreamHelpers
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(StreamHelpers).Name);

        public static readonly object _PROCESS_SYNC_LOCK = new object();

        public static void AddStreamCategoryList(List<StreamCategoryDTO> catList, ObservableCollection<StreamCategoryModel> modelList)
        {
            try
            {
                foreach (StreamCategoryDTO catDTO in catList)
                {
                    StreamCategoryModel model = modelList.FirstOrDefault(P => P.CategoryID == catDTO.CategoryID);
                    if (model == null)
                    {
                        model = new StreamCategoryModel { CategoryID = catDTO.CategoryID, CategoryName = catDTO.CategoryName, FollowingCount = catDTO.FollowingCount };

                        int max = modelList.Count;
                        int min = 0;
                        int pivot;

                        while (max > min)
                        {
                            pivot = (min + max) / 2;
                            if (model.CategoryID > modelList.ElementAt(pivot).CategoryID)
                            {
                                min = pivot + 1;
                            }
                            else
                            {
                                max = pivot;
                            }
                        }
                        modelList.InvokeInsert(min, model);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AddStreamCategory() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static UCStreamLiveViewer GetCurruentStreamLiveViewer()
        {
            if (UCStreamAndChannelViewer.StreamLiveWrapper != null
                && UCStreamAndChannelViewer.StreamLiveWrapper.IS_INITIALIZED
                && UCStreamAndChannelViewer.StreamLiveWrapper._StreamLiveViewer != null
                && UCStreamAndChannelViewer.StreamLiveWrapper._StreamLiveViewer._IS_INITIALIZED)
            {
                return UCStreamAndChannelViewer.StreamLiveWrapper._StreamLiveViewer;
            }
            return null;
        }

        public static UCStreamLiveWrapper GetCurruentStreamLiveWrapper()
        {
            if (UCStreamAndChannelViewer.StreamLiveWrapper != null
                && UCStreamAndChannelViewer.StreamLiveWrapper.IS_INITIALIZED)
            {
                return UCStreamAndChannelViewer.StreamLiveWrapper;
            }
            return null;
        }

        public static VideoDeviceInfo GetVideoDevice(int channelType, int sourceType, AppInfoModel appInfo)
        {
            VideoDeviceInfo videoDevice = null;
            List<VideoDeviceInfo> vDeviceList = VideoDeviceInfo.GetDeviceList();

            if (channelType == StreamConstants.CHANNEL_OUT)
            {
                if (sourceType == StreamConstants.MONITOR_SCREEN)
                {
                    videoDevice = vDeviceList.FirstOrDefault(P => P.DeviceType == StreamConstants.MONITOR_SCREEN);
                }
                else if (sourceType == StreamConstants.WEB_CAMERA)
                {
                    List<VideoDeviceInfo> tempList = vDeviceList.Where(P => P.DeviceType == StreamConstants.WEB_CAMERA).ToList();
                    videoDevice = tempList.FirstOrDefault();

                    if (tempList.Count > 0 && !String.IsNullOrWhiteSpace(SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID))
                    {
                        foreach (VideoDeviceInfo vdi in tempList)
                        {
                            if (vdi.Key.Equals(SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID))
                            {
                                videoDevice = vdi;
                                break;
                            }
                        }
                    }
                }
                else if (sourceType == StreamConstants.APP_SCREEN)
                {
                    if (appInfo != null)
                    {
                        videoDevice = new VideoDeviceInfo(appInfo.AppCaption, appInfo.AppHandler);
                        if (appInfo.AppHandler > 0 && NativeLibrary.IsWindow((IntPtr)appInfo.AppHandler))
                        {
                            WINDOWPLACEMENT pos = NativeLibrary.GetPlacement((IntPtr)appInfo.AppHandler);
                            if (pos.showCmd == ShowWindowCommands.Maximized)
                            {
                                NativeLibrary.ShowWindow((IntPtr)appInfo.AppHandler, GetWindow_Cmd.GW_HWNDPREV);
                            }
                            else
                            {
                                NativeLibrary.ShowWindow((IntPtr)appInfo.AppHandler, GetWindow_Cmd.GW_HWNDLAST);
                            }
                            RECT rect = NativeLibrary.GetWindowRect((IntPtr)appInfo.AppHandler);
                            int w = rect.Right - rect.Left;
                            int h = rect.Bottom - rect.Top;
                            if (rect.Right > 0 && rect.Bottom > 0 && w > 0 && h > 0)
                            {
                                double originalRatio = (double)w / h;
                                if (originalRatio > 1.33333)
                                {
                                    videoDevice.Width = 640;
                                    videoDevice.Height = (h * videoDevice.Width) / w;
                                }
                                else
                                {
                                    videoDevice.Height = 480;
                                    videoDevice.Width = (w * videoDevice.Height) / h;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                videoDevice = vDeviceList.FirstOrDefault(P => P.DeviceType == StreamConstants.MONITOR_SCREEN);
            }
            return videoDevice;
        }

        public static List<AudioDeviceInfo> GetAudioDevice(int channelType, int sourceType)
        {
            List<AudioDeviceInfo> deviceList = new List<AudioDeviceInfo>();
            List<AudioDeviceInfo> aDeviceList = AudioDeviceInfo.GetDeviceList();

            if (channelType == StreamConstants.CHANNEL_OUT)
            {
                if (sourceType == StreamConstants.MICROPHONE || sourceType == StreamConstants.MICROPHONE_SPEAKER_MIX)
                {
                    List<AudioDeviceInfo> tempList = aDeviceList.Where(P => P.DeviceType == StreamConstants.MICROPHONE).ToList();
                    AudioDeviceInfo device = tempList.FirstOrDefault();

                    if (SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER >= 0 && tempList.Count > SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER)
                    {
                        device = tempList.ElementAt(SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER);
                    }
                    else if (tempList.Count > 1)
                    {
                        for (int idx = 0; idx < tempList.Count; idx++)
                        {
                            if (tempList[idx].Name.ToLower().Contains("high definition"))
                            {
                                device = tempList.ElementAt(idx);
                                break;
                            }
                        }
                    }

                    if (device != null) deviceList.Add(device);
                }

                if (sourceType == StreamConstants.SPEAKER || sourceType == StreamConstants.MICROPHONE_SPEAKER_MIX)
                {
                    List<AudioDeviceInfo> tempList = aDeviceList.Where(P => P.DeviceType == StreamConstants.SPEAKER).ToList();
                    AudioDeviceInfo device = tempList.FirstOrDefault();

                    if (SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER >= 0 && tempList.Count > SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER)
                    {
                        device = tempList.ElementAt(SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER);
                    }
                    else if (tempList.Count > 1)
                    {
                        for (int idx = 0; idx < tempList.Count; idx++)
                        {
                            if (tempList[idx].Name.ToLower().Contains("high definition"))
                            {
                                device = tempList.ElementAt(idx);
                                break;
                            }
                        }
                    }

                    if (device != null) deviceList.Add(device);
                }
            }
            else
            {
                List<AudioDeviceInfo> tempList = aDeviceList.Where(P => P.DeviceType == StreamConstants.SPEAKER).ToList();
                AudioDeviceInfo device = tempList.FirstOrDefault();

                if (SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER >= 0 && tempList.Count > SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER)
                {
                    device = tempList.ElementAt(SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER);
                }
                else if (tempList.Count > 1)
                {
                    for (int idx = 0; idx < tempList.Count; idx++)
                    {
                        if (tempList[idx].Name.ToLower().Contains("high definition"))
                        {
                            device = tempList.ElementAt(idx);
                            break;
                        }
                    }
                }

                if (device != null) deviceList.Add(device);
            }

            return deviceList;
        }

        public static void OnRingCallStartInterrupt(bool isIncoming)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::STREAM::CALL::LIVE_STREAM_INTERRUPTED".PadRight(61, ' ') + "==>   isIncoming = " + isIncoming);
#endif
                StreamViewModel.Instance.RingCallConnected = true;

                if (StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_FINISHED)
                {
                    StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_INTERRUPTED;

                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        StreamViewModel.Instance.ResetStreamingChannelSource();
                        StreamViewModel.Instance.ResetCallChannelSource();
                    }, DispatcherPriority.Send);

                    if (StreamViewModel.Instance.StreamingChannel != null)
                    {
                        if (StreamViewModel.Instance.StreamInfoModel != null)
                        {
                            CallHelperMethods.StreamPlayPause(CallProperty.getSessionIdForLiveStreaming(), PlayPauseType.Pause);
                        }
                        else if (StreamViewModel.Instance.ChannelInfoModel != null)
                        {
                            Application.Current.Dispatcher.BeginInvoke(() => UCStreamAndChannelViewer.Instance.Dispose(), DispatcherPriority.ApplicationIdle);
                        }
                    }
                }
                else if (StreamViewModel.Instance.StreamInfoModel != null)
                {
                    Application.Current.Dispatcher.BeginInvoke(() => { StreamViewModel.Instance.ResetStreamingChannelSource(); }, DispatcherPriority.Send);
                }
                else if (StreamViewModel.Instance.ChannelInfoModel != null)
                {
                    Application.Current.Dispatcher.BeginInvoke(() => UCStreamAndChannelViewer.Instance.Dispose());
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnRingCallStartInterrupt() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void OnRingCallStartInterruptOver(bool isIncoming)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::STREAM::CALL::LIVE_STREAM_INTERRUPT_OVER".PadRight(61, ' ') + "==>   isIncoming = " + isIncoming);
#endif
                StreamViewModel.Instance.RingCallConnected = false;

                if (StreamViewModel.Instance.StreamingStatus != StreamConstants.STREAMING_FINISHED)
                {
                    StreamViewModel.Instance.StreamingStatus = StreamConstants.STREAMING_CONNECTED;

                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        StreamViewModel.Instance.SetStreamingChannelSource();
                        StreamViewModel.Instance.SetCallChannelSource();
                    }, DispatcherPriority.Send);

                    if (StreamViewModel.Instance.StreamingChannel != null)
                    {
                        if (StreamViewModel.Instance.StreamInfoModel != null)
                        {
                            CallHelperMethods.StreamPlayPause(CallProperty.getSessionIdForLiveStreaming(), PlayPauseType.Play);
                        }
                        else if (StreamViewModel.Instance.ChannelInfoModel != null)
                        {
                            //DO NOTHING
                        }
                    }
                }
                else if (StreamViewModel.Instance.StreamInfoModel != null)
                {
                    if (StreamViewModel.Instance.VideoSourceType > 0 || StreamViewModel.Instance.AudioSourceType > 0)
                    {
                        Application.Current.Dispatcher.BeginInvoke(() => { StreamViewModel.Instance.SetStreamingChannelSource(); }, DispatcherPriority.Send);
                    }
                }
                else if (StreamViewModel.Instance.ChannelInfoModel != null)
                {
                    //DO NOTHING
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnRingCallStartInterruptOver() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //public static bool IsStreamNotifyable()
        //{
        //    return
        //        (
        //        UCMiddlePanelSwitcher.View_UCStreamAndChannel != null &&
        //        UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel != null &&
        //        UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.IsVisible &&
        //        (
        //        UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.StreamAndChannelHomePanel != null &&
        //        UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.StreamAndChannelHomePanel.IsVisible
        //        )
        //        ||
        //        (UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.StreamHomePanel != null &&
        //        UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.StreamHomePanel.IsVisible
        //        )
        //        );
        //}

        public static void AddIntoStreamFeatureList(StreamDTO newDTO)
        {
            try
            {
                ObservableCollection<StreamModel> viewerModelList = StreamViewModel.Instance.StreamFeatureList;
                lock (_PROCESS_SYNC_LOCK)
                {

                    StreamModel newModel = viewerModelList.Where(P => P.UserTableID == newDTO.UserTableID).FirstOrDefault();

                    StreamModel firstModel = viewerModelList.FirstOrDefault();
                    StreamModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new StreamModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.StartTime > firstModel.StartTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.StartTime < lastModel.StartTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotTime = viewerModelList.ElementAt(pivot).StartTime;
                                    if (newModel.StartTime < pivotTime)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.StartTime > firstModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.StartTime > newModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.StartTime < viewerModelList.ElementAt(pivot).StartTime)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoStreamFollwingList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoStreamRecentList(StreamDTO newDTO)
        {
            try
            {
                ObservableCollection<StreamModel> viewerModelList = StreamViewModel.Instance.StreamRecentLiveList;
                lock (_PROCESS_SYNC_LOCK)
                {
                    StreamModel newModel = viewerModelList.Where(P => P.UserTableID == newDTO.UserTableID).FirstOrDefault();

                    StreamModel firstModel = viewerModelList.FirstOrDefault();
                    StreamModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new StreamModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.StartTime > firstModel.StartTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.StartTime < lastModel.StartTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotTime = viewerModelList.ElementAt(pivot).StartTime;
                                    if (newModel.StartTime < pivotTime)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.StartTime > firstModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.StartTime > newModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.StartTime < viewerModelList.ElementAt(pivot).StartTime)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoStreamRecentList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoStreamNearByList(StreamDTO newDTO)
        {
            try
            {
                ObservableCollection<StreamModel> viewerModelList = StreamViewModel.Instance.StreamNearByLiveList;
                lock (_PROCESS_SYNC_LOCK)
                {
                    StreamModel newModel = viewerModelList.Where(P => P.UserTableID == newDTO.UserTableID).FirstOrDefault();

                    StreamModel firstModel = viewerModelList.FirstOrDefault();
                    StreamModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new StreamModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (IsCheckStreamNearBy(newModel, firstModel) == 1)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (IsCheckStreamNearBy(newModel, lastModel) != 1)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    if (IsCheckStreamNearBy(newModel, viewerModelList.ElementAt(pivot)) == 1)
                                    {
                                        max = pivot;
                                    }
                                    else
                                    {
                                        min = pivot + 1;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        int value = IsCheckStreamNearBy(newDTO, newModel);
                        if (value == 1)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (value == -1)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (IsCheckStreamNearBy(newDTO, viewerModelList.ElementAt(pivot)) == 1)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoStreamNearByList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoStreamMostViewList(StreamDTO newDTO)
        {
            try
            {
                ObservableCollection<StreamModel> viewerModelList = StreamViewModel.Instance.StreamMostViewLiveList;
                lock (_PROCESS_SYNC_LOCK)
                {
                    StreamModel newModel = viewerModelList.Where(P => P.UserTableID == newDTO.UserTableID).FirstOrDefault();

                    StreamModel firstModel = viewerModelList.FirstOrDefault();
                    StreamModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new StreamModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.ViewCount > firstModel.ViewCount)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.ViewCount <= lastModel.ViewCount)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotTime = viewerModelList.ElementAt(pivot).ViewCount;
                                    if (newModel.ViewCount < pivotTime)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.ViewCount > firstModel.ViewCount)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.ViewCount != newModel.ViewCount)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.ViewCount < viewerModelList.ElementAt(pivot).ViewCount)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoStreamNearByList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoStreamSearchList(StreamDTO newDTO, ObservableCollection<StreamModel> viewerModelList)
        {
            try
            {
                lock (_PROCESS_SYNC_LOCK)
                {
                    StreamModel newModel = viewerModelList.Where(P => P.UserTableID == newDTO.UserTableID).FirstOrDefault();

                    StreamModel firstModel = viewerModelList.FirstOrDefault();
                    StreamModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        newModel = new StreamModel(newDTO);

                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.StartTime > firstModel.StartTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.StartTime < lastModel.StartTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotTime = viewerModelList.ElementAt(pivot).StartTime;
                                    if (newModel.StartTime < pivotTime)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (newDTO.StartTime > firstModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.StartTime > newModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.StartTime < viewerModelList.ElementAt(pivot).StartTime)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoStreamSearchList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddIntoStreamFollowingList(StreamDTO newDTO, bool isNotifyable = true)
        {
            try
            {
                ObservableCollection<StreamModel> viewerModelList = StreamViewModel.Instance.StreamFollowingList;
                lock (_PROCESS_SYNC_LOCK)
                {

                    StreamModel newModel = viewerModelList.Where(P => P.UserTableID == newDTO.UserTableID).FirstOrDefault();

                    StreamModel firstModel = viewerModelList.FirstOrDefault();
                    StreamModel lastModel = viewerModelList.LastOrDefault();

                    if (newModel == null)
                    {
                        if (isNotifyable)
                        {
                            Guid streamID = StreamViewModel.Instance.StreamUnreadNotificationCounter.TryGetValue(newDTO.UserTableID);
                            if (streamID != newDTO.StreamID)
                            {
                                StreamViewModel.Instance.StreamUnreadNotificationCounter[newDTO.UserTableID] = newDTO.StreamID;
                                NotificationDTO nDTO = new NotificationDTO();
                                nDTO.ID = newDTO.StreamID;
                                nDTO.FriendTableID = newDTO.UserTableID;
                                nDTO.FriendName = newDTO.UserName;
                                nDTO.ImageUrl = newDTO.ProfileImage;
                                nDTO.UpdateTime = newDTO.StartTime;
                                nDTO.MessageType = StatusConstants.MESSAGE_NOTIFICATION_LIVE;
                                VMNotification.Instance.GetLiveDTOForNotification(nDTO);
                            }
                        }

                        newModel = new StreamModel(newDTO);
                        newModel.IsViewOpened = true;
                        if (viewerModelList.Count > 0)
                        {
                            if (newModel.StartTime > firstModel.StartTime)
                            {
                                viewerModelList.InvokeInsert(0, newModel);
                            }
                            else if (newModel.StartTime < lastModel.StartTime)
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }
                            else
                            {
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    long pivotTime = viewerModelList.ElementAt(pivot).StartTime;
                                    if (newModel.StartTime < pivotTime)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                viewerModelList.InvokeInsert(min, newModel);
                            }
                        }
                        else
                        {
                            viewerModelList.InvokeAdd(newModel);
                        }
                    }
                    else
                    {
                        if (isNotifyable && newDTO.StartTime > newModel.StartTime)
                        {
                            Guid streamID = StreamViewModel.Instance.StreamUnreadNotificationCounter.TryGetValue(newDTO.UserTableID);
                            if (streamID != newDTO.StreamID)
                            {
                                StreamViewModel.Instance.StreamUnreadNotificationCounter[newDTO.UserTableID] = newDTO.StreamID;
                                NotificationDTO nDTO = new NotificationDTO();
                                nDTO.ID = newDTO.StreamID;
                                nDTO.FriendTableID = newDTO.UserTableID;
                                nDTO.FriendName = newDTO.UserName;
                                nDTO.ImageUrl = newDTO.ProfileImage;
                                nDTO.UpdateTime = newDTO.StartTime;
                                nDTO.MessageType = StatusConstants.MESSAGE_NOTIFICATION_LIVE;
                                VMNotification.Instance.GetLiveDTOForNotification(nDTO);
                            }
                        }

                        if (newDTO.StartTime > firstModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            viewerModelList.InvokeMove(indexOf, 0);
                            newModel.LoadData(newDTO);
                        }
                        else if (newDTO.StartTime > newModel.StartTime)
                        {
                            int indexOf = viewerModelList.IndexOf(newModel);
                            int max = viewerModelList.Count;
                            int min = 0;
                            int pivot;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                if (newDTO.StartTime < viewerModelList.ElementAt(pivot).StartTime)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            if (indexOf != min)
                            {
                                min = min > indexOf ? min - 1 : min;
                                viewerModelList.InvokeMove(indexOf, min);
                            }
                            newModel.LoadData(newDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoStreamFollowingList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private static int IsCheckStreamNearBy(StreamModel newModel, StreamModel model)
        {
            if (newModel.Distance < model.Distance)
            {
                return 1;
            }
            else if (newModel.Distance == model.Distance)
            {
                if (newModel.StartTime > model.StartTime)
                {
                    return 1;
                }
                else if (newModel.StartTime == model.StartTime)
                {
                    return 0;
                }
            }
            return -1;
        }

        private static int IsCheckStreamNearBy(StreamDTO newDTO, StreamModel model)
        {
            if (newDTO.Distance < model.Distance)
            {
                return 1;
            }
            else if (newDTO.Distance == model.Distance)
            {
                if (newDTO.StartTime > model.StartTime)
                {
                    return 1;
                }
                else if (newDTO.StartTime == model.StartTime)
                {
                    return 0;
                }
            }
            return -1;
        }

        public static void AddIntoStreamContributorList(StreamUserModel newModel)
        {
            try
            {
                if (newModel != null)
                {
                    ObservableCollection<StreamUserModel> viewerModelList = StreamViewModel.Instance.SreamContributorList;
                    lock (_PROCESS_SYNC_LOCK)
                    {
                        StreamUserModel existingModel = viewerModelList.Where(P => P.UserTableID == newModel.UserTableID).FirstOrDefault();
                        StreamUserModel firstModel = viewerModelList.FirstOrDefault();
                        StreamUserModel lastModel = viewerModelList.LastOrDefault();

                        if (existingModel == null)
                        {

                            if (viewerModelList.Count > 0)
                            {
                                if (newModel.Contribution > firstModel.Contribution)
                                {
                                    viewerModelList.InvokeInsert(0, newModel);
                                }
                                else if (newModel.Contribution < lastModel.Contribution && viewerModelList.Count < 6)
                                {
                                    viewerModelList.InvokeAdd(newModel);
                                }
                                else if (!(viewerModelList.Count == 6 && newModel.Contribution < lastModel.Contribution))
                                {
                                    int max = viewerModelList.Count;
                                    int min = 0;
                                    int pivot;

                                    while (max > min)
                                    {
                                        pivot = (min + max) / 2;
                                        long pivotTime = viewerModelList.ElementAt(pivot).Contribution;
                                        if (newModel.Contribution < pivotTime)
                                        {
                                            min = pivot + 1;
                                        }
                                        else
                                        {
                                            max = pivot;
                                        }
                                    }

                                    viewerModelList.InvokeInsert(min, newModel);
                                }
                            }
                            else
                            {
                                viewerModelList.InvokeAdd(newModel);
                            }

                            if (viewerModelList.Count > 6)
                            {
                                viewerModelList.InvokeRemoveAt(viewerModelList.Count - 1);
                            }
                            for (int i = 0; i < viewerModelList.Count; i++)
                            {
                                StreamUserModel userModel = viewerModelList.ElementAt(i);
                                userModel.ContributionType = i + 1;
                            }
                        }
                        else
                        {
                            if (newModel.Contribution > firstModel.Contribution)
                            {
                                int indexOf = viewerModelList.IndexOf(existingModel);
                                viewerModelList.InvokeMove(indexOf, 0);
                                existingModel.LoadData(newModel);
                            }
                            else if (newModel.Contribution > existingModel.Contribution)
                            {
                                int indexOf = viewerModelList.IndexOf(existingModel);
                                int max = viewerModelList.Count;
                                int min = 0;
                                int pivot;

                                while (max > min)
                                {
                                    pivot = (min + max) / 2;
                                    if (newModel.Contribution < viewerModelList.ElementAt(pivot).Contribution)
                                    {
                                        min = pivot + 1;
                                    }
                                    else
                                    {
                                        max = pivot;
                                    }
                                }

                                if (indexOf != min)
                                {
                                    min = min > indexOf ? min - 1 : min;
                                    viewerModelList.InvokeMove(indexOf, min);
                                }
                                existingModel.LoadData(newModel);
                            }

                            for (int i = 0; i < viewerModelList.Count; i++)
                            {
                                StreamUserModel userModel = viewerModelList.ElementAt(i);
                                userModel.ContributionType = i + 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddIntoStreamFollwingList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void RemoveStreamByID(Guid streamID)
        {
            try
            {
                lock (HelperMethods._PROCESS_SYNC_LOCK)
                {
                    int idx = -1, i = 0;
                    foreach (StreamAndChannelModel tempModel in RingIDViewModel.Instance.StreamAndChannelFeatureList)
                    {
                        if (tempModel.Stream != null && tempModel.Stream.StreamID == streamID)
                        {
                            idx = i;
                            break;
                        }
                        i++;
                    }

                    if (idx >= 0 && idx < RingIDViewModel.Instance.StreamAndChannelFeatureList.Count)
                    {
                        RingIDViewModel.Instance.StreamAndChannelFeatureList.InvokeRemoveAt(idx);
                    }
                }

                lock (_PROCESS_SYNC_LOCK)
                {
                    int idx = -1, i = 0;
                    foreach (StreamModel tempModel in StreamViewModel.Instance.StreamFeatureList)
                    {
                        if (tempModel.StreamID == streamID) 
                        {
                            idx = i;
                            break;
                        }
                        i++;
                    }

                    if (idx >= 0 && idx < StreamViewModel.Instance.StreamFeatureList.Count)
                    {
                        StreamViewModel.Instance.StreamFeatureList.InvokeRemoveAt(idx);
                    }
                }

                lock (_PROCESS_SYNC_LOCK)
                {
                    int idx = -1, i = 0;
                    foreach (StreamModel tempModel in StreamViewModel.Instance.StreamRecentLiveList)
                    {
                        if (tempModel.StreamID == streamID)
                        {
                            idx = i;
                            break;
                        }
                        i++;
                    }

                    if (idx >= 0 && idx < StreamViewModel.Instance.StreamRecentLiveList.Count)
                    {
                        StreamViewModel.Instance.StreamRecentLiveList.InvokeRemoveAt(idx);
                    }
                }

                lock (_PROCESS_SYNC_LOCK)
                {
                    int idx = -1, i = 0;
                    foreach (StreamModel tempModel in StreamViewModel.Instance.StreamFollowingList)
                    {
                        if (tempModel.StreamID == streamID)
                        {
                            idx = i;
                            break;
                        }
                        i++;
                    }

                    if (idx >= 0 && idx < StreamViewModel.Instance.StreamFollowingList.Count)
                    {
                        StreamViewModel.Instance.StreamFollowingList.InvokeRemoveAt(idx);
                    }
                }

                lock (_PROCESS_SYNC_LOCK)
                {
                    int idx = -1, i = 0;
                    foreach (StreamModel tempModel in StreamViewModel.Instance.StreamNearByLiveList)
                    {
                        if (tempModel.StreamID == streamID)
                        {
                            idx = i;
                            break;
                        }
                        i++;
                    }

                    if (idx >= 0 && idx < StreamViewModel.Instance.StreamNearByLiveList.Count)
                    {
                        StreamViewModel.Instance.StreamNearByLiveList.InvokeRemoveAt(idx);
                    }
                }

                lock (_PROCESS_SYNC_LOCK)
                {
                    int idx = -1, i = 0;
                    foreach (StreamModel tempModel in StreamViewModel.Instance.StreamMostViewLiveList)
                    {
                        if (tempModel.StreamID == streamID)
                        {
                            idx = i;
                            break;
                        }
                        i++;
                    }

                    if (idx >= 0 && idx < StreamViewModel.Instance.StreamMostViewLiveList.Count)
                    {
                        StreamViewModel.Instance.StreamMostViewLiveList.InvokeRemoveAt(idx);
                    }
                }

                lock (_PROCESS_SYNC_LOCK)
                {
                    int idx = -1, i = 0;
                    foreach (StreamModel tempModel in StreamViewModel.Instance.StreamSearchList)
                    {
                        if (tempModel.StreamID == streamID)
                        {
                            idx = i;
                            break;
                        }
                        i++;
                    }

                    if (idx >= 0 && idx < StreamViewModel.Instance.StreamSearchList.Count)
                    {
                        StreamViewModel.Instance.StreamSearchList.InvokeRemoveAt(idx);
                    }
                }

                lock (_PROCESS_SYNC_LOCK)
                {
                    int idx = -1, i = 0;
                    foreach (StreamModel tempModel in StreamViewModel.Instance.StreamByCountryList)
                    {
                        if (tempModel.StreamID == streamID)
                        {
                            idx = i;
                            break;
                        }
                        i++;
                    }

                    if (idx >= 0 && idx < StreamViewModel.Instance.StreamByCountryList.Count)
                    {
                        StreamViewModel.Instance.StreamByCountryList.InvokeRemoveAt(idx);
                    }
                }

                lock (_PROCESS_SYNC_LOCK)
                {
                    int idx = -1, i = 0;
                    foreach (StreamModel tempModel in StreamViewModel.Instance.StreamByCategoryList)
                    {
                        if (tempModel.StreamID == streamID)
                        {
                            idx = i;
                            break;
                        }
                        i++;
                    }

                    if (idx >= 0 && idx < StreamViewModel.Instance.StreamByCategoryList.Count)
                    {
                        StreamViewModel.Instance.StreamByCategoryList.InvokeRemoveAt(idx);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("RemoveStreamByID() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ChangeStreamViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl, int paddingTop)
        {
            try
            {
                //double offset = scrollViewer.VerticalOffset - paddingTop;
                //double height = itemsControl.ActualHeight - scrollViewer.ActualHeight > 0 ? scrollViewer.ActualHeight : itemsControl.ActualHeight;
                //double x = (scrollViewer.ActualWidth - 615) / 2;
                //double y = offset + height;
                double offset = scrollViewer.VerticalOffset;
                double x = 25;
                double height = itemsControl.ActualHeight;
                double y = itemsControl.ActualHeight;

                if ((itemsControl.ActualHeight + paddingTop) - scrollViewer.ActualHeight > 0)
                {
                    height = scrollViewer.ActualHeight;
                    y = height + offset - paddingTop;
                    y = y > itemsControl.ActualHeight ? itemsControl.ActualHeight : (y < 0 ? 0 : y);
                }

                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y));
                if (hitTest != null)
                {
                    int maxIndex = itemsControl.Items.Count - 1;
                    int startIndex = maxIndex;

                    ContentPresenter presenter = ChatHelpers.GetContentPresenterFromEvent(hitTest.VisualHit, itemsControl) as ContentPresenter;
                    if (presenter != null && !(presenter.Content is StreamModel))
                    {
                        presenter = HelperMethods.FindVisualParent<ContentPresenter>(presenter);
                    }

                    if (presenter != null)
                    {
                        startIndex = itemsControl.ItemContainerGenerator.IndexFromContainer(presenter);
                        if (startIndex < 0) return;
                    }

                    int overLimit = 2;
                    int index = (startIndex + 2 > maxIndex ? maxIndex : startIndex + 2);

                    int i = maxIndex - index > 3 ? index + 3 : maxIndex;
                    for (; i > index; i--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        StreamModel model = (StreamModel)presenter.Content;
                        model.IsViewOpened = false;
                    }

                    for (int overflow = 0; index >= 0; index--)
                    {
                        if (height <= 0 && (++overflow) >= overLimit) break;

                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(index);
                        if (temp == null)
                            break;

                        presenter = temp as ContentPresenter;
                        StreamModel model = (StreamModel)presenter.Content;
                        model.IsViewOpened = true;

                        if (index <= startIndex)
                        {
                            height -= presenter.ActualHeight;
                        }
                    }

                    int count = index > 3 ? index - 3 : 0;
                    for (int j = index; j >= count; j--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(j);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        StreamModel model = (StreamModel)presenter.Content;
                        model.IsViewOpened = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeStreamViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ChangeStreamViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl, int paddingTop, int numberOfColumn)
        {
            try
            {
                double offset = scrollViewer.VerticalOffset;
                double x = 25;
                double height = itemsControl.ActualHeight;
                double y = itemsControl.ActualHeight;

                if ((itemsControl.ActualHeight + paddingTop) - scrollViewer.ActualHeight > 0)
                {
                    height = scrollViewer.ActualHeight;
                    y = height + offset - paddingTop;
                    y = y > itemsControl.ActualHeight ? itemsControl.ActualHeight : (y < 0 ? 0 : y);
                }

                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y));
                if (hitTest != null)
                {
                    int maxIndex = itemsControl.Items.Count - 1;
                    int startIndex = maxIndex;

                    ContentPresenter presenter = ChatHelpers.GetContentPresenterFromEvent(hitTest.VisualHit, itemsControl) as ContentPresenter;
                    if (presenter != null && !(presenter.Content is StreamModel))
                    {
                        presenter = HelperMethods.FindVisualParent<ContentPresenter>(presenter);
                    }

                    if (presenter != null)
                    {
                        startIndex = itemsControl.ItemContainerGenerator.IndexFromContainer(presenter);
                        if (startIndex < 0) return;

                        int mod = maxIndex % numberOfColumn;
                        int diff = maxIndex - startIndex;
                        startIndex = diff > 0 ? startIndex + (diff > mod ? mod : diff) : startIndex;
                    }

                    int overLimit = 1;
                    int index = (startIndex + numberOfColumn > maxIndex ? maxIndex : startIndex + numberOfColumn);

                    int i = maxIndex - index > numberOfColumn * 2 ? index + numberOfColumn * 2 : maxIndex;
                    for (; i > index; i--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        StreamModel model = (StreamModel)presenter.Content;
                        model.IsViewOpened = false;
                        //Debug.WriteLine("HIDE => " + model.Title);
                    }

                    for (int overflow = 0; index >= 0; index--)
                    {
                        if (height <= 0 && index % numberOfColumn == 0 && (++overflow) >= overLimit) break;

                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(index);
                        if (temp == null)
                            break;

                        presenter = temp as ContentPresenter;
                        StreamModel model = (StreamModel)presenter.Content;
                        model.IsViewOpened = true;
                        //Debug.WriteLine("SHOW => " + model.Title);

                        if (index % numberOfColumn == 0 && index <= startIndex)
                        {
                            height -= presenter.ActualHeight;
                        }
                    }

                    int count = index > numberOfColumn * 2 ? index - numberOfColumn * 2 : 0;
                    for (int j = index; j >= count; j--)
                    {
                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(j);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        StreamModel model = (StreamModel)presenter.Content;
                        model.IsViewOpened = false;
                        //Debug.WriteLine("HIDE => " + model.Title);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeStreamViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadFeaturedStreamList(int scrollType, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (StreamViewModel.Instance.LoadStatusModel.IsFeaturedLoading == false)
                {
                    StreamViewModel.Instance.LoadStatusModel.IsFeaturedLoading = true;
                    long time = 0;
                    string searchParam = String.Empty;
                    int categoryId = 0;

                    if (UCMiddlePanelSwitcher.View_UCStreamAndChannel != null && UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel != null)
                    {
                        searchParam = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.txtSearch.Text;
                        categoryId = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbStreamCategory.SelectedValue != null ? (int)(UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbStreamCategory.SelectedValue) : 0;
                    }

                    if (StreamViewModel.Instance.StreamFeatureList.Count > 0)
                    {
                        if (scrollType == StreamConstants.STREAM_TOP_SCROLL)
                        {
                            time = StreamViewModel.Instance.StreamFeatureList.ElementAt(StreamViewModel.Instance.StreamFeatureList.Count - 1).StartTime;
                        }
                        else
                        {
                            time = StreamViewModel.Instance.StreamFeatureList.ElementAt(0).StartTime;
                        }
                    }

                    new ThrdFeaturedLiveStreams(scrollType, limit, time, searchParam, categoryId, (status) =>
                    {
                        StreamViewModel.Instance.LoadStatusModel.IsFeaturedLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                StreamViewModel.Instance.LoadStatusModel.IsFeaturedLoading = false;
                log.Error("Error: LoadFeaturedStreamList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadRecentStreamList(int scrollType, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (StreamViewModel.Instance.LoadStatusModel.IsRecentLoading == false)
                {
                    StreamViewModel.Instance.LoadStatusModel.IsRecentLoading = true;
                    long time = 0;
                    string searchParam = String.Empty;
                    int categoryId = 0;

                    if (UCMiddlePanelSwitcher.View_UCStreamAndChannel != null && UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel != null)
                    {
                        searchParam = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.txtSearch.Text;
                        categoryId = UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbStreamCategory.SelectedValue != null ? (int)(UCMiddlePanelSwitcher.View_UCStreamAndChannel.StreamAndChannelMainPanel.cmbStreamCategory.SelectedValue) : 0;
                    }

                    if (StreamViewModel.Instance.StreamRecentLiveList.Count > 0)
                    {
                        if (scrollType == StreamConstants.STREAM_TOP_SCROLL)
                        {
                            time = StreamViewModel.Instance.StreamRecentLiveList.ElementAt(StreamViewModel.Instance.StreamRecentLiveList.Count - 1).StartTime;
                        }
                        else
                        {
                            time = StreamViewModel.Instance.StreamRecentLiveList.ElementAt(0).StartTime;
                        }
                    }

                    new ThrdRecentStreams(scrollType, limit, time, searchParam, categoryId, (status) =>
                    {
                        StreamViewModel.Instance.LoadStatusModel.IsRecentLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                StreamViewModel.Instance.LoadStatusModel.IsRecentLoading = false;
                log.Error("Error: LoadRecentStreamList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadNearByStreamList(int scrollType, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (StreamViewModel.Instance.LoadStatusModel.IsNearByLoading == false)
                {
                    StreamViewModel.Instance.LoadStatusModel.IsNearByLoading = true;
                    long time = 0;

                    if (StreamViewModel.Instance.StreamNearByLiveList.Count > 0)
                    {
                        var temp = StreamViewModel.Instance.StreamNearByLiveList.OrderByDescending(P => P.StartTime).ToList();
                        if (scrollType == StreamConstants.STREAM_TOP_SCROLL)
                        {
                            time = temp.First().StartTime;
                        }
                        else
                        {
                            time = temp.Last().StartTime;
                        }
                    }

                    new ThrdGetNearestStreams(scrollType, limit, time, (status) =>
                    {
                        StreamViewModel.Instance.LoadStatusModel.IsNearByLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                StreamViewModel.Instance.LoadStatusModel.IsNearByLoading = false;
                log.Error("Error: LoadNearByStreamList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadMostViewStreamList(bool initLoad = false, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (StreamViewModel.Instance.LoadStatusModel.IsMostViewLoading == false)
                {
                    StreamViewModel.Instance.LoadStatusModel.IsMostViewLoading = true;

                    new ThrdGetMostViewStreams(initLoad ? 0 : StreamViewModel.Instance.StreamMostViewLiveList.Count, limit, (status) =>
                    {
                        StreamViewModel.Instance.LoadStatusModel.IsMostViewLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                StreamViewModel.Instance.LoadStatusModel.IsMostViewLoading = false;
                log.Error("Error: LoadMostViewStreamList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadSearchStreamList(int scrollType, string searchParam, string country, StreamCategoryModel catModel, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (StreamViewModel.Instance.LoadStatusModel.IsSearchLoading == false)
                {
                    StreamViewModel.Instance.LoadStatusModel.IsSearchLoading = true;
                    long time = 0;
                    List<StreamCategoryModel> catList = null;

                    if (StreamViewModel.Instance.StreamSearchList.Count > 0)
                    {
                        var temp = StreamViewModel.Instance.StreamSearchList.OrderByDescending(P => P.StartTime).ToList();
                        if (scrollType == StreamConstants.STREAM_TOP_SCROLL)
                        {
                            time = temp.First().StartTime;
                        }
                        else
                        {
                            time = temp.Last().StartTime;
                        }
                    }

                    if (catModel != null)
                    {
                        catList = new List<StreamCategoryModel>();
                        catList.Add(catModel);
                    }

                    new ThrdSearchLiveStreams(scrollType, limit, time, searchParam, country, false, catList, (status) =>
                    {
                        StreamViewModel.Instance.LoadStatusModel.IsSearchLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                StreamViewModel.Instance.LoadStatusModel.IsSearchLoading = false;
                log.Error("Error: LoadNearByStreamList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadFollowingStreamList(int scrollType, int limit = 6, Func<bool, int> onComplete = null)
        {
            try
            {
                if (StreamViewModel.Instance.LoadStatusModel.IsFollowingLoading == false)
                {
                    StreamViewModel.Instance.LoadStatusModel.IsFollowingLoading = true;
                    long time = 0;

                    if (StreamViewModel.Instance.StreamFollowingList.Count > 0)
                    {
                        var temp = StreamViewModel.Instance.StreamFollowingList.OrderByDescending(P => P.StartTime).ToList();
                        if (scrollType == StreamConstants.STREAM_TOP_SCROLL)
                        {
                            time = temp.First().StartTime;
                        }
                        else
                        {
                            time = temp.Last().StartTime;
                        }
                    }

                    new ThrdGetFollowingStreams(scrollType, limit, time, (status) =>
                    {
                        StreamViewModel.Instance.LoadStatusModel.IsFollowingLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                StreamViewModel.Instance.LoadStatusModel.IsFollowingLoading = false;
                log.Error("Error: LoadFollowingStreamList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void LoadCategoryWiseStreamCount(Func<bool, int> onComplete = null)
        {
            try
            {
                if (StreamViewModel.Instance.LoadStatusModel.IsCategoryWiseCountLoading == false)
                {
                    StreamViewModel.Instance.LoadStatusModel.IsCategoryWiseCountLoading = true;

                    new ThrdGetCategoryWiseStreamCount((status) =>
                    {
                        StreamViewModel.Instance.LoadStatusModel.IsCategoryWiseCountLoading = false;
                        if (onComplete != null)
                        {
                            onComplete(status);
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    if (onComplete != null)
                    {
                        onComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                StreamViewModel.Instance.LoadStatusModel.IsCategoryWiseCountLoading = false;
                log.Error("Error: LoadCategoryWiseFollowingCount() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

    }
}

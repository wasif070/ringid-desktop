﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Stream
{
    public class ThrdGetNearestStreams
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdGetNearestStreams).Name);

        private static float _Longitude;
        private static float _Latitude;
        private int _Limit;
        private long _Time;
        private int _StreamScroll;
        private Func<bool, int> _OnComplete;

        public ThrdGetNearestStreams(int scroll, int limit, long time, Func<bool, int> onComplete = null)
        {
            this._StreamScroll = scroll;
            this._Limit = limit;
            this._Time = time;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            if (_Latitude == 0 && _Longitude == 0)
            {
                HelperMethods.GetLatLongByCurrentTimeZone(out _Latitude, out _Longitude);
            }

            if (_Latitude > 0 || _Longitude > 0)
            {
                JObject pakToSend = new JObject();
                if (_Time > 0)
                {
                    pakToSend[JsonKeys.StreamScroll] = StreamConstants.STREAM_TOP_SCROLL;
                }
                pakToSend[JsonKeys.Limit] = _Limit;
                pakToSend[JsonKeys.Time] = _Time;
                pakToSend[JsonKeys.Latitude] = _Latitude;
                pakToSend[JsonKeys.StreamLongitude] = _Longitude;
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_NEAREST_STREAMS;
                JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null)
                {
                    if (this._OnComplete != null)
                    {
                        this._OnComplete(true);
                    }
                }
                else
                {
                    if (this._OnComplete != null)
                    {
                        this._OnComplete(false);
                    }
                }
            }
            else
            {
                Thread.Sleep(1000);
                if (this._OnComplete != null)
                {
                    this._OnComplete(false);
                }
            }
        }

    }
}

﻿using Auth.utility;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;

namespace View.Utility.Stream
{
    public class ThrdSearchLiveStreams
    {
        
        private int _ScrollType = 0;
        private long _Time = 0;
        private int _Limit = 0;
        private string _SearchParam = string.Empty;
        private string _Country = string.Empty;
        private bool _IsOnlyFriend = false;
        private List<StreamCategoryModel> _CatList = null;
        private Func<bool, int> _OnComplete;

        public ThrdSearchLiveStreams(int scrollType, int limit, long time, string searchParam, string country, bool isOnlyFriend, List<StreamCategoryModel> catList, Func<bool, int> onComplete = null)
        {
            this._ScrollType = scrollType;
            this._Time = time;
            this._Limit = limit;
            this._SearchParam = searchParam;
            this._Country = country;
            this._IsOnlyFriend = isOnlyFriend;
            this._CatList = catList;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();

            if (_Time > 0)
            {
                pakToSend[JsonKeys.StreamScroll] = _ScrollType;
            }
            pakToSend[JsonKeys.Time] = _Time;
            pakToSend[JsonKeys.Limit] = _Limit;
            if (!String.IsNullOrWhiteSpace(_SearchParam))
            {
                pakToSend[JsonKeys.Title] = _SearchParam;
            }
            if (!String.IsNullOrWhiteSpace(_Country))
            {
                pakToSend[JsonKeys.CelebrityCountry] = _Country;
            }
            if (_CatList != null && _CatList.Count > 0)
            {
                JArray itemList = new JArray();
                foreach (StreamCategoryModel catModel in _CatList)
                {
                    JObject item = new JObject();
                    item[JsonKeys.StreamCategoryId] = catModel.CategoryID;
                    item[JsonKeys.Category] = catModel.CategoryName;
                    itemList.Add(item);
                }
                pakToSend[JsonKeys.StreamCategoryList] = itemList;
            }
            if (_IsOnlyFriend)
            {
                pakToSend[JsonKeys.StreamIsFriendOnly] = _IsOnlyFriend;
            }
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._OnComplete != null)
            {
                this._OnComplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }
        }
    }
}

﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Stream
{
    public class ThrdFollowUnfollowUser
    {
        private long _UserTableID;
        private bool _IsFollow;
        private Func<bool, int> _OnComplete;

        public ThrdFollowUnfollowUser(long userTableID, bool isFollow, Func<bool, int> onComplete = null)
        {
            this._UserTableID = userTableID;
            this._IsFollow = isFollow;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(Run);
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = _UserTableID;
            pakToSend[JsonKeys.Follow] = _IsFollow;
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_FOLLOW_UNFOLLOW_USER;

            JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
            if (this._OnComplete != null) 
            {
                this._OnComplete(_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success]);
            }
        }

    }
}

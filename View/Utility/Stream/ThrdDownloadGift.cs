﻿using Auth.utility;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Shapes;
using View.BindingModels;

namespace View.Utility.Stream
{
    public class ThrdDownloadGift
    {
        private WalletGiftProductModel _GiftModel;
        private MessageModel _MessageModel;
        private string _RootPath = string.Empty;
        private string _Extension = string.Empty;
        Func<WalletGiftProductModel, int> _Oncomplete;

        public ThrdDownloadGift(WalletGiftProductModel giftModel, string rootPath, string extension, Func<WalletGiftProductModel, int> oncomplete = null)
        {
            this._GiftModel = giftModel;
            this._RootPath = rootPath;
            this._Extension = extension;
            this._Oncomplete = oncomplete;
        }

        public ThrdDownloadGift(MessageModel msgModel, WalletGiftProductModel giftModel, string rootPath, string extension, Func<WalletGiftProductModel, int> oncomplete = null)
        {
            this._MessageModel = msgModel;
            this._GiftModel = giftModel;
            this._RootPath = rootPath;
            this._Extension = extension;
            this._Oncomplete = oncomplete;
        }

        public void StartThread()
        {
            Thread t = new Thread(DownloadGift);
            t.Name = this.GetType().Name;
            t.Start();
        }

        public void DownloadGift()
        {
            try
            {
                string imageName = _GiftModel != null ? _GiftModel.ProductName : string.Empty;
                string filePath = this._RootPath + System.IO.Path.DirectorySeparatorChar + imageName + _Extension;
                string uri = ServerAndPortSettings.StreamGiftDownloadUrl + "/" + ServerAndPortSettings.D_MINI + "/" + imageName + _Extension;
                ImageFile file = new ImageFile(filePath);
                if (file.IsComplete || HttpRequest.DownloadFile(uri, filePath))
                {
                    if (_MessageModel != null)
                    {
                        _MessageModel.OnPropertyChanged("Message");
                    }
                    else if (_GiftModel != null)
                    {
                        _GiftModel.OnPropertyChanged("ProductName");
                    }
                    if (_Oncomplete != null)
                    {
                        _Oncomplete(_GiftModel);
                    }
                }
            }
            catch (Exception ex)
            {
                // log.Error("DownloadMarketImagList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

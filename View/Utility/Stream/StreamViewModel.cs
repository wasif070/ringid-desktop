﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.UI;
using Models.Utility;
using View.Utility;
using View.Utility.Call;
using System.Windows.Controls;
using View.UI.PopUp.Stream;
using View.Utility.WPFMessageBox;
using View.Utility.Stream.Utils;
using View.Utility.StreamAndChannel;
using View.Constants;
using View.UI.Stream;
using System.IO;
using View.UI.StreamAndChannel;
using System.Windows.Threading;
using System.Timers;
using callsdkwrapper;
using View.UI.Channel;
using View.Utility.Channel;
using Models.Entity;
using View.Utility.Chat;
using View.Utility.Chat.Service;
using View.ViewModel;

namespace View.Utility.Stream
{
    public class StreamViewModel : INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(StreamViewModel).Name);
        public event PropertyChangedEventHandler PropertyChanged;
        public static StreamViewModel Instance
        {
            get;
            set;
        }

        #region Common Part

        private ICommand _StreamViewCommand;
        private ICommand _StreamDiscoveryCommand;
        private ICommand _StreamNotificationCommand;
        private ICommand _StreamBackCommand;

        private ICommand _StreamFollowUnfollowCommand;
        private ICommand _ExpandCollapseCategoryCommand;
        private ICommand _DiscoverByCategoryCommand;
        private ICommand _DiscoverByCountryCommand;
        private ICommand _StreamNameOrProfileCommand;
        private ICommand _StreamShareOnFacebookCommand;
        private ICommand _StreamShareOnTwitterCommand;

        private StreamLoadStatusModel _LoadStatusModel = new StreamLoadStatusModel();
        private ObservableCollection<StreamCategoryModel> _StreamCategoryList = new ObservableCollection<StreamCategoryModel>();
        private ObservableCollection<StreamModel> _StreamFeatureList = new ObservableCollection<StreamModel>();
        private ObservableCollection<StreamModel> _StreamRecentLiveList = new ObservableCollection<StreamModel>();
        private ObservableCollection<StreamModel> _StreamNearByLiveList = new ObservableCollection<StreamModel>();
        private ObservableCollection<StreamModel> _StreamMostViewLiveList = new ObservableCollection<StreamModel>();
        private ObservableCollection<StreamModel> _StreamSearchList = new ObservableCollection<StreamModel>();
        private ObservableCollection<StreamModel> _StreamFollowingList = new ObservableCollection<StreamModel>();
        private ObservableCollection<StreamModel> _StreamByCountryList = new ObservableCollection<StreamModel>();
        private ObservableCollection<StreamModel> _StreamByCategoryList = new ObservableCollection<StreamModel>();
        private ObservableCollection<StreamCategoryModel> _StreamCountByCategoryList = new ObservableCollection<StreamCategoryModel>();

        private ObservableCollection<StreamUserModel> _SreamContributorList = new ObservableCollection<StreamUserModel>();
        private ConcurrentDictionary<long, ObservableCollection<MessageModel>> _MessageList = new ConcurrentDictionary<long, ObservableCollection<MessageModel>>();
        private ConcurrentDictionary<long, ObservableCollection<StreamUserModel>> _ViewerList = new ConcurrentDictionary<long, ObservableCollection<StreamUserModel>>();

        private ObservableDictionary<long, Guid> _StreamUnreadNotificationCounter = new ObservableDictionary<long, Guid>();

        #region Property

        public StreamLoadStatusModel LoadStatusModel
        {
            get { return _LoadStatusModel; }
            set
            {
                _LoadStatusModel = value;
                this.OnPropertyChanged("LoadStatusModel");
            }
        }

        public ConcurrentDictionary<long, ObservableCollection<MessageModel>> MessageList
        {
            get
            {
                return _MessageList;
            }
            set
            {
                _MessageList = value;
            }
        }

        public ConcurrentDictionary<long, ObservableCollection<StreamUserModel>> ViewerList
        {
            get
            {
                return _ViewerList;
            }
            set
            {
                _ViewerList = value;
            }
        }

        public ObservableCollection<StreamCategoryModel> StreamCategoryList
        {
            get
            {
                return _StreamCategoryList;
            }
            set
            {
                _StreamCategoryList = value;
            }
        }

        public ObservableCollection<StreamModel> StreamFeatureList
        {
            get
            {
                return _StreamFeatureList;
            }
            set
            {
                _StreamFeatureList = value;
                OnPropertyChanged("StreamFeatureList");
            }
        }

        public ObservableCollection<StreamModel> StreamRecentLiveList
        {
            get
            {
                return _StreamRecentLiveList;
            }
            set
            {
                _StreamRecentLiveList = value;
                OnPropertyChanged("StreamRecentLiveList");
            }
        }

        public ObservableCollection<StreamModel> StreamNearByLiveList
        {
            get
            {
                return _StreamNearByLiveList;
            }
            set
            {
                _StreamNearByLiveList = value;
                OnPropertyChanged("StreamNearByLiveList");
            }
        }

        public ObservableCollection<StreamModel> StreamMostViewLiveList
        {
            get
            {
                return _StreamMostViewLiveList;
            }
            set
            {
                _StreamMostViewLiveList = value;
                OnPropertyChanged("StreamMostViewLiveList");
            }
        }

        public ObservableCollection<StreamModel> StreamSearchList
        {
            get
            {
                return _StreamSearchList;
            }
            set
            {
                _StreamSearchList = value;
                OnPropertyChanged("StreamSearchList");
            }
        }

        public ObservableCollection<StreamModel> StreamFollowingList
        {
            get
            {
                return _StreamFollowingList;
            }
            set
            {
                _StreamFollowingList = value;
                OnPropertyChanged("StreamFollowingList");
            }
        }

        public ObservableCollection<StreamModel> StreamByCountryList
        {
            get
            {
                return _StreamByCountryList;
            }
            set
            {
                _StreamByCountryList = value;
                OnPropertyChanged("StreamByCountryList");
            }
        }

        public ObservableCollection<StreamModel> StreamByCategoryList
        {
            get
            {
                return _StreamByCategoryList;
            }
            set
            {
                _StreamByCategoryList = value;
                OnPropertyChanged("StreamByCategoryList");
            }
        }

        public ObservableCollection<StreamCategoryModel> StreamCountByCategoryList
        {
            get
            {
                return _StreamCountByCategoryList;
            }
            set
            {
                _StreamCountByCategoryList = value;
                OnPropertyChanged("StreamCountByCategoryList");
            }
        }

        public ObservableCollection<StreamUserModel> SreamContributorList
        {
            get
            {
                return _SreamContributorList;
            }
            set
            {
                _SreamContributorList = value;
                OnPropertyChanged("SreamContributorList");
            }
        }

        public ObservableDictionary<long, Guid> StreamUnreadNotificationCounter
        {
            get { return _StreamUnreadNotificationCounter; }
            set
            {
                if (_StreamUnreadNotificationCounter == value) return;
                _StreamUnreadNotificationCounter = value; OnPropertyChanged("StreamUnreadNotificationCounter");
            }
        }

        #endregion Property

        #region Utility Method

        public ObservableCollection<MessageModel> GetMessageListByPublisherID(long id)
        {
            ObservableCollection<MessageModel> mList = MessageList.TryGetValue(id);
            if (mList == null)
            {
                mList = new ObservableCollection<MessageModel>();
                MessageList[id] = mList;
            }
            return mList;
        }

        public ObservableCollection<StreamUserModel> GetViewerListByPublisherID(long id)
        {
            ObservableCollection<StreamUserModel> vList = ViewerList.TryGetValue(id);
            if (vList == null)
            {
                vList = new ObservableCollection<StreamUserModel>();
                ViewerList[id] = vList;
            }
            return vList;
        }

        public void OnStreamViewCommand(object parameter)
        {
            UCStreamAndChannelViewer.Instance.Show((StreamModel)parameter);
        }

        public void OnStreamDiscoveryCommand(object parameter)
        {
            StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamDiscoveryPanel, parameter);
        }

        public void OnStreamNotificationCommand(object parameter)
        {
            StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamFollowingPanel, parameter);
        }

        public void OnBackCommand(object param)
        {

        }

        public void OnStreamFollowUnfollowCommand(object param)
        {
            if (param is StreamUserModel)
            {
                StreamUserModel userModel = (StreamUserModel)param;
                bool status = userModel.IsFollowing;
                BaseUserProfileModel baseProfileModel = new BaseUserProfileModel();
                baseProfileModel.UserTableID = userModel.UserTableID;
                baseProfileModel.ProfileImage = userModel.ProfileImage;
                baseProfileModel.FullName = userModel.UserName;

                if (status == false)
                {
                    new ThrdFollowUnfollowUser(userModel.UserTableID, true, (success) =>
                    {
                        if (success)
                        {
                            if (StreamViewModel.Instance.StreamInfoModel != null && baseProfileModel.UserTableID == StreamViewModel.Instance.StreamInfoModel.UserTableID)
                            {
                                MessageDTO messageDTO = new MessageDTO();
                                messageDTO.PublisherID = baseProfileModel.UserTableID;
                                messageDTO.SenderTableID = DefaultSettings.LOGIN_TABLE_ID;
                                PacketTimeID packet = ChatService.GeneratePacketID();
                                messageDTO.PacketID = packet.PacketID;
                                //messageDTO.MessageType = StreamConstants.FOLLOW_MESSAGE;
                                messageDTO.StreamMsgDTO = new StreamMessageDTO { MessageType = StreamConstants.FOLLOW_MESSAGE };
                                messageDTO.Message = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage;
                                messageDTO.MessageDate = packet.Time;
                                messageDTO.FullName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                                messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                                StreamMsgLoadUtility.LoadMessageData(messageDTO);
                                CallHelperMethods.FollowPublisher(baseProfileModel.UserTableID, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage, (int)userModel.FollowerCount);
                            }
                            userModel.IsFollowing = !status;
                        }
                        return 0;
                    }).Start();
                }
                else
                {
                    bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.UNFOLLOW_CONFIRMATION, userModel.UserName), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Unfollow"));
                    if (isTrue)
                    {
                        new ThrdFollowUnfollowUser(userModel.UserTableID, false, (success) =>
                        {
                            if (success)
                            {
                                userModel.IsFollowing = false;
                            }
                            return 0;
                        }).Start();
                    }
                }
            }
        }

        public void OnExpandCollapseCategoryCommand(object param)
        {
            try
            {
                StreamCategoryModel categoryModel = (StreamCategoryModel)param;
                if (categoryModel.IsExpanded)
                {
                    categoryModel.IsExpanded = false;
                    categoryModel.StreamList.Clear();
                }
                else
                {
                    if (categoryModel.StreamList == null)
                    {
                        categoryModel.StreamList = new ObservableCollection<StreamModel>();
                    }
                    StreamHelpers.LoadSearchStreamList(StreamConstants.STREAM_TOP_SCROLL, null, null, categoryModel, categoryModel.FollowingCount);
                    //StreamHelpers.LoadFollowingStreamList(StreamConstants.STREAM_TOP_SCROLL, categoryModel.CategoryID, categoryModel.FollowingCount);
                    categoryModel.IsExpanded = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnExpandCollapseCategoryCommand() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnDiscoverByCategoryCommand(object param)
        {
            try
            {
                StreamCategoryModel categoryModel = (StreamCategoryModel)param;
                ParamModel paramModel = new ParamModel();
                paramModel.Title = categoryModel.CategoryName + " - Lives";
                paramModel.ActionType = AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS;
                paramModel.Param = categoryModel;
                paramModel.PrevParam = null;
                paramModel.PrevViewType = StreamAndChannelConstants.TypeStreamDiscoveryPanel;
                StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel, paramModel);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnDiscoverByCategoryCommand() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnDiscoverByCountryCommand(object param)
        {
            try
            {
                CountryCodeModel countryModel = (CountryCodeModel)param;
                ParamModel paramModel = new ParamModel();
                paramModel.Title = countryModel.CountryName + " - Lives";
                paramModel.ActionType = AppConstants.TYPE_ACTION_SEARCH_LIVE_STREAMS;
                paramModel.Param = countryModel;
                paramModel.PrevParam = null;
                paramModel.PrevViewType = StreamAndChannelConstants.TypeStreamDiscoveryPanel;
                StreamAndChannelSwitcher.Switch(StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel, paramModel);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnDiscoverByCountryCommand() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnStreamNameOrProfileCommand(object param)
        {
            if (param is StreamUserModel)
            {
                StreamUserModel userModel = (StreamUserModel)param;
                UCStreamLiveViewer viewer = StreamHelpers.GetCurruentStreamLiveViewer();
                if (viewer != null)
                {
                    UCStreamProfileInfoPopUP ucStreamProfileInfoPopUP = new UCStreamProfileInfoPopUP();
                    ucStreamProfileInfoPopUP.ShowProfileInfo(viewer.gridChatViewWrapper);
                    ucStreamProfileInfoPopUP.SelectedStreamModel = StreamViewModel.Instance.StreamInfoModel;
                    ucStreamProfileInfoPopUP.SelectedStreamUserModel = userModel;

                    new ThrdStreamUserDetails(userModel.UserTableID, (streamUserDTO) =>
                    {
                        if (streamUserDTO != null && userModel.UserTableID == streamUserDTO.UserTableID)
                        {
                            userModel.LoadData(streamUserDTO);
                        }
                        return 0;
                    }).Start();
                }
            }
        }

        public void OnStreamShareOnFacebookCommand()
        {
            try
            {
                if (StreamViewModel.Instance.StreamInfoModel != null)
                {
                    CallHelperMethods.ShareLiveMedia(StreamViewModel.Instance.StreamInfoModel.UserTableID, StreamViewModel.Instance.StreamInfoModel.UserName, StreamViewModel.Instance.StreamInfoModel.ProfileImage);
                }
                string fbShareLink = "https://www.facebook.com/sharer/sharer.php?u=" + MakeStreamShareUrl();
                System.Diagnostics.Process myProcess = new System.Diagnostics.Process();
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = fbShareLink;
                myProcess.ErrorDataReceived += ((snd, exc) =>
                {
                    log.Info(snd.ToString() + "  " + exc.ToString());
                });
                myProcess.Start();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void OnStreamShareOnTwitterCommand()
        {
            try
            {
                if (StreamViewModel.Instance.StreamInfoModel != null)
                {
                    CallHelperMethods.ShareLiveMedia(StreamViewModel.Instance.StreamInfoModel.UserTableID, StreamViewModel.Instance.StreamInfoModel.UserName, StreamViewModel.Instance.StreamInfoModel.ProfileImage);
                }
                string fbShareLink = "https://twitter.com/intent/tweet?text=" + MakeStreamShareUrl();
                System.Diagnostics.Process myProcess = new System.Diagnostics.Process();
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = fbShareLink;
                myProcess.ErrorDataReceived += ((snd, exc) =>
                {
                    log.Info(snd.ToString() + "  " + exc.ToString());
                });
                myProcess.Start();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + "==>" + ex.Message);
            }
        }

        private string MakeStreamShareUrl()
        {
            long ringId = DefaultSettings.LOGIN_RING_ID;
            string userName = DefaultSettings.userProfile != null ? DefaultSettings.userProfile.FullName : string.Empty;
            string profileImage = DefaultSettings.userProfile != null ? DefaultSettings.userProfile.ProfileImage : string.Empty;

            string ampercent = "%26";
            string postData = ServerAndPortSettings.StreamShareBaseUrl + "uid=" + ringId + ampercent + "uname=" + userName + ampercent + "img=" + profileImage;
            return postData;
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

        #region Command

        public ICommand StreamViewCommand
        {
            get
            {
                if (_StreamViewCommand == null)
                {
                    _StreamViewCommand = new RelayCommand(param => OnStreamViewCommand(param), param => { return !StreamViewModel.Instance.RingCallConnected; });
                }
                return _StreamViewCommand;
            }
        }

        public ICommand StreamDiscoveryCommand
        {
            get
            {
                if (_StreamDiscoveryCommand == null)
                {
                    _StreamDiscoveryCommand = new RelayCommand((param) => OnStreamDiscoveryCommand(param));
                }
                return _StreamDiscoveryCommand;
            }
        }

        public ICommand StreamNotificationCommand
        {
            get
            {
                if (_StreamNotificationCommand == null)
                {
                    _StreamNotificationCommand = new RelayCommand((param) => OnStreamNotificationCommand(param));
                }
                return _StreamNotificationCommand;
            }
        }

        public ICommand StreamFollowUnfollowCommand
        {
            get
            {
                if (_StreamFollowUnfollowCommand == null)
                {
                    _StreamFollowUnfollowCommand = new RelayCommand((param) => OnStreamFollowUnfollowCommand(param));
                }

                return _StreamFollowUnfollowCommand;
            }
        }

        public ICommand ExpandCollapseCategoryCommand
        {
            get
            {
                if (_ExpandCollapseCategoryCommand == null)
                {
                    _ExpandCollapseCategoryCommand = new RelayCommand(param => OnExpandCollapseCategoryCommand(param));
                }
                return _ExpandCollapseCategoryCommand;
            }
        }

        public ICommand DiscoverByCategoryCommand
        {
            get
            {
                if (_DiscoverByCategoryCommand == null)
                {
                    _DiscoverByCategoryCommand = new RelayCommand(param => OnDiscoverByCategoryCommand(param));
                }
                return _DiscoverByCategoryCommand;
            }
        }

        public ICommand DiscoverByCountryCommand
        {
            get
            {
                if (_DiscoverByCountryCommand == null)
                {
                    _DiscoverByCountryCommand = new RelayCommand(param => OnDiscoverByCountryCommand(param));
                }
                return _DiscoverByCountryCommand;
            }
        }

        public ICommand StreamBackCommand
        {
            get
            {
                if (_StreamBackCommand == null)
                {
                    _StreamBackCommand = new RelayCommand((param) => OnBackCommand(param));
                }
                return _StreamBackCommand;
            }
        }

        public ICommand StreamNameOrProfileCommand
        {
            get
            {
                if (_StreamNameOrProfileCommand == null)
                {
                    _StreamNameOrProfileCommand = new RelayCommand(param => OnStreamNameOrProfileCommand(param));
                }
                return _StreamNameOrProfileCommand;
            }
        }

        public ICommand StreamShareOnFacebookCommand
        {
            get
            {
                if (_StreamShareOnFacebookCommand == null)
                {
                    _StreamShareOnFacebookCommand = new RelayCommand(param => OnStreamShareOnFacebookCommand());
                }
                return _StreamShareOnFacebookCommand;
            }
        }

        public ICommand StreamShareOnTwitterCommand
        {
            get
            {
                if (_StreamShareOnTwitterCommand == null)
                {
                    _StreamShareOnTwitterCommand = new RelayCommand(param => OnStreamShareOnTwitterCommand());
                }
                return _StreamShareOnTwitterCommand;
            }
        }

        #endregion Command

        #endregion Common Part

        #region Streaming Part

        private StreamRenderModel _StreamRenderModel = new StreamRenderModel();
        private StreamModel _StreamInfoModel = null;
        private StreamUserModel _StreamUserInfoModel = null;
        private ChannelModel _ChannelInfoModel = null;
        private ChannelMediaModel _ChannelMediaInfoModel = null;
        private StreamChannel _StreamingChannel = null;
        private int _ChannelType = 0;
        private int _StreamingStatus = StreamConstants.STREAMING_FINISHED;
        private Timer _StreamingInturruptTimer = null;

        private int _VideoSourceType = 0;
        private int _AudioSourceType = 0;
        private AppInfoModel _AppInfoModel = null;

        #region Property

        public StreamRenderModel StreamRenderModel
        {
            get
            {
                return _StreamRenderModel;
            }
            set
            {
                _StreamRenderModel = value;
                OnPropertyChanged("StreamRenderModel");
            }
        }

        public StreamModel StreamInfoModel
        {
            get
            {
                return _StreamInfoModel;
            }
            set
            {
                _StreamInfoModel = value;
                OnPropertyChanged("StreamInfoModel");
            }
        }

        public StreamUserModel StreamUserInfoModel
        {
            get
            {
                return _StreamUserInfoModel;
            }
            set
            {
                _StreamUserInfoModel = value;
                OnPropertyChanged("StreamUserInfoModel");
            }
        }

        public ChannelModel ChannelInfoModel
        {
            get
            {
                return _ChannelInfoModel;
            }
            set
            {
                _ChannelInfoModel = value;
                OnPropertyChanged("ChannelInfoModel");
            }
        }

        public ChannelMediaModel ChannelMediaInfoModel
        {
            get
            {
                return _ChannelMediaInfoModel;
            }
            set
            {
                _ChannelMediaInfoModel = value;
                OnPropertyChanged("ChannelMediaInfoModel");
            }
        }

        public StreamChannel StreamingChannel
        {
            get
            {
                return _StreamingChannel;
            }
            set
            {
                _StreamingChannel = value;
            }
        }

        public int ChannelType
        {
            get { return _ChannelType; }
            set
            {
                if (_ChannelType == value)
                    return;

                _ChannelType = value;
                this.OnPropertyChanged("ChannelType");
            }
        }

        public int VideoSourceType
        {
            get { return _VideoSourceType; }
            set
            {
                if (_VideoSourceType == value)
                    return;

                _VideoSourceType = value;
                this.OnPropertyChanged("VideoSourceType");
            }
        }

        public int AudioSourceType
        {
            get { return _AudioSourceType; }
            set
            {
                if (_AudioSourceType == value)
                    return;

                _AudioSourceType = value;
                this.OnPropertyChanged("AudioSourceType");
            }
        }

        public AppInfoModel AppInfoModel
        {
            get { return _AppInfoModel; }
            set
            {
                if (_AppInfoModel == value)
                    return;

                _AppInfoModel = value;
                this.OnPropertyChanged("AppInfoModel");
            }
        }

        public int StreamingStatus
        {
            get { return _StreamingStatus; }
            set
            {
                if (_StreamingStatus == value)
                    return;

                _StreamingStatus = value;
                this.OnPropertyChanged("StreamingStatus");
            }
        }

        #endregion Property

        #region Utility Method

        public void InitStreamingInfo(StreamModel streamModel, StreamUserModel streamUserModel, int channelType)
        {
            this.StreamingStatus = StreamConstants.STREAMING_FINISHED;
            this.StreamInfoModel = streamModel;
            this.StreamUserInfoModel = streamUserModel;
            this.ChannelType = channelType;
            this.VideoSourceType = 0;
            this.AudioSourceType = 0;
            this.AppInfoModel = null;
            this.StreamRenderModel.RenderSource = null;
        }

        public void InitStreamingInfo(ChannelModel channelModel, ChannelMediaModel channelMediaModel)
        {
            this.StreamingStatus = StreamConstants.STREAMING_FINISHED;
            this.ChannelInfoModel = channelModel;
            this.ChannelMediaInfoModel = channelMediaModel;
            this.ChannelType = StreamConstants.CHANNEL_IN;
            this.VideoSourceType = 0;
            this.AudioSourceType = 0;
            this.AppInfoModel = null;
            this.StreamRenderModel.RenderSource = null;
        }

        public void DestroyStreamingInfo()
        {
            this.StreamingStatus = StreamConstants.STREAMING_FINISHED;
            this.StreamInfoModel = null;
            this.StreamUserInfoModel = null;
            this.ChannelInfoModel = null;
            this.ChannelMediaInfoModel = null;
            this.ChannelType = 0;
            this.VideoSourceType = 0;
            this.AudioSourceType = 0;
            this.AppInfoModel = null;
            this.StreamRenderModel.RenderSource = null;
            this.StopStreamingInturruptTimer();
        }

        public void InitStreamingChannel(bool isVoiceHD = false)
        {
            this.StreamingChannel = new StreamChannel(this.ChannelType, isVoiceHD);
            this.StreamingChannel.InitStream(new StreamChannelHandler(this.StreamRenderModel));
        }

        public void DestroyStreamingChannel()
        {
            if (this.StreamingChannel != null)
            {
                this.StreamingChannel.Dispose();
            }
            this.StreamingChannel = null;
        }

        public void InitStreamingSourceInfo(int audioSourceType, int videoSourceType, AppInfoModel appInfo)
        {
            this.VideoSourceType = videoSourceType;
            this.AudioSourceType = audioSourceType;
            this.AppInfoModel = appInfo;
        }

        public void DestroyStreamingSourceInfo()
        {
            this.VideoSourceType = 0;
            this.AudioSourceType = 0;
            this.AppInfoModel = null;
        }

        public bool SetStreamingChannelSource()
        {
            bool status = false;
            if (this.StreamingChannel != null)
            {
                List<AudioDeviceInfo> audioDevice = StreamHelpers.GetAudioDevice(this.StreamingChannel.ChannelType, this.AudioSourceType);
                VideoDeviceInfo videoDevice = StreamHelpers.GetVideoDevice(this.StreamingChannel.ChannelType, this.VideoSourceType, this.AppInfoModel);
                status = this.StreamingChannel.SetStreamSource(audioDevice, videoDevice);
            }
            return status;
        }

        public void ResetStreamingChannelSource()
        {
            if (this.StreamingChannel != null)
            {
                this.StreamingChannel.ResetStreamSource();
            }
        }

        public void StartStreamingInturruptTimer()
        {
            if (this._StreamingInturruptTimer == null)
            {
                this._StreamingInturruptTimer = new Timer();
                this._StreamingInturruptTimer.Interval = 30 * 1000;
                this._StreamingInturruptTimer.Elapsed += (o, e) =>
                {
                    this._StreamingInturruptTimer.Stop();
                    this._StreamingInturruptTimer = null;

                    UCStreamLiveViewer streamViewer = StreamHelpers.GetCurruentStreamLiveViewer();
                    if (streamViewer != null)
                    {
                        streamViewer.OnInternetNotAvailable();
                        return;
                    }

                    UCChannelViewer channelViewer = ChannelHelpers.GetCurruentChannelViewer();
                    if (channelViewer != null)
                    {
                        channelViewer.OnInternetNotAvailable();
                        return;
                    }

                    log.Debug("*************************  StreamInturruptTimer Completed *************************");
                };
            }

            if (!this._StreamingInturruptTimer.Enabled)
            {
                this._StreamingInturruptTimer.Start();
            }
        }

        public void StopStreamingInturruptTimer()
        {
            if (this._StreamingInturruptTimer != null)
            {
                this._StreamingInturruptTimer.Stop();
                this._StreamingInturruptTimer = null;
            }
        }

        #endregion Utility Method

        #endregion Streaming Part

        #region Calling Part

        private StreamRenderModel _CallRenderModel = new StreamRenderModel();
        private StreamUserModel _CallerInfoModel = null;
        private StreamChannel _CallChannel = null;
        private CallMediaType _CallType = CallMediaType.Video;
        private int _CallDuration = 0;
        private int _CallStatus = StreamConstants.STREAM_CALL_DISCONNECTED;
        private bool _RingCallConnected = false;
        private Timer _CallTimer = null;

        #region Property

        public StreamRenderModel CallRenderModel
        {
            get
            {
                return _CallRenderModel;
            }
            set
            {
                _CallRenderModel = value;
                OnPropertyChanged("CallRenderModel");
            }
        }

        public StreamUserModel CallerInfoModel
        {
            get { return _CallerInfoModel; }
            set
            {
                if (_CallerInfoModel == value)
                    return;

                _CallerInfoModel = value;
                this.OnPropertyChanged("CallerInfoModel");
            }
        }

        public int CallStatus
        {
            get { return _CallStatus; }
            set
            {
                if (_CallStatus == value)
                    return;

                _CallStatus = value;
                this.OnPropertyChanged("CallStatus");
            }
        }

        public bool RingCallConnected
        {
            get { return _RingCallConnected; }
            set
            {
                if (_RingCallConnected == value)
                    return;

                _RingCallConnected = value;
                this.OnPropertyChanged("RingCallConnected");
            }
        }

        public CallMediaType CallType
        {
            get { return _CallType; }
            set
            {
                if (_CallType == value)
                    return;

                _CallType = value;
                this.OnPropertyChanged("CallType");
            }
        }

        public int CallDuration
        {
            get { return _CallDuration; }
            set
            {
                if (_CallDuration == value)
                    return;

                _CallDuration = value;
                this.OnPropertyChanged("CallDuration");
            }
        }

        public StreamChannel CallChannel
        {
            get
            {
                return _CallChannel;
            }
            set
            {
                _CallChannel = value;
            }
        }

        #endregion Property

        #region Utility Method

        public bool InitCallInfo(int callStatus, StreamUserModel callerInfoModel)
        {
            UCStreamLiveViewer viewer = StreamHelpers.GetCurruentStreamLiveViewer();
            if (viewer != null && this.CallerInfoModel == null)
            {
                this.CallerInfoModel = callerInfoModel;
                this.CallStatus = callStatus;
                this.CallType = CallMediaType.Video;
                this.CallDuration = 0;
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    viewer.ShowCallPreview();
                    if (viewer.IsCallPreviewMode && viewer._StreamLivePreview != null && viewer._StreamLivePreview.IsVisible)
                    {
                        viewer.ShowCallPreview();
                    }
                });
                return true;
            }
            return false;
        }

        public void DestroyCallInfo()
        {
            this.StopCallTimer();
            this.CallStatus = StreamConstants.STREAM_CALL_DISCONNECTED;
            this.CallType = CallMediaType.Video;
            this.CallDuration = 0;
            this.CallerInfoModel = null;
        }

        public void InitCallChannel(int channelType, StreamRenderModel renderModel)
        {
            this.CallChannel = new StreamChannel(channelType, false);
            this.CallChannel.InitStream(new StreamChannelHandler(renderModel));
            this.CallChannel.IsRegistered = true;
        }

        public void DestroyCallChannel()
        {
            if (this.CallChannel != null)
            {
                this.CallChannel.Dispose();
            }
            this.CallChannel = null;
        }

        public bool SetCallChannelSource()
        {
            bool status = false;

            if (this.CallChannel != null)
            {
                List<AudioDeviceInfo> audioDevice = StreamHelpers.GetAudioDevice(this.CallChannel.ChannelType, StreamConstants.MICROPHONE);
                VideoDeviceInfo videoDevice = StreamHelpers.GetVideoDevice(this.CallChannel.ChannelType, StreamConstants.WEB_CAMERA, null);
                status = this.CallChannel.SetStreamSource(audioDevice, videoDevice);
            }

            return status;
        }

        public void ResetCallChannelSource()
        {
            if (this.CallChannel != null)
            {
                this.CallChannel.ResetStreamSource();
            }
        }

        public void StartCallTimer()
        {
            if (this._CallTimer == null)
            {
                this._CallTimer = new Timer();
                this._CallTimer.Interval = 1000;
                this._CallTimer.Elapsed += (o, e) =>
                {
                    this.CallDuration += 1;
                };
            }
            this._CallTimer.Stop();
            this._CallTimer.Start();
        }

        public void StopCallTimer()
        {
            if (this._CallTimer != null)
            {
                this._CallTimer.Stop();
                this._CallTimer = null;
            }
        }

        #endregion Utility Method

        #endregion Calling Part

    }
}

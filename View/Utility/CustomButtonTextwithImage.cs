﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;

namespace View.Utility
{
    class CustomButtonTextwithImage 
    {
        public static readonly DependencyProperty BtnNormalImageProperty =
            DependencyProperty.RegisterAttached("BtnNormalImage", typeof(ImageSource), typeof(CustomButtonTextwithImage));

        public static readonly DependencyProperty BtnHoverImageProperty =
           DependencyProperty.RegisterAttached("BtnHoverImage", typeof(ImageSource), typeof(CustomButtonTextwithImage));

        public static readonly DependencyProperty BtnPressedImageProperty =
          DependencyProperty.RegisterAttached("BtnPressedImage", typeof(ImageSource), typeof(CustomButtonTextwithImage));

        public static readonly DependencyProperty BtnTextProperty =
         DependencyProperty.RegisterAttached("BtnText", typeof(String), typeof(CustomButtonTextwithImage));

        public static readonly DependencyProperty BtnImageWidthProperty =
         DependencyProperty.RegisterAttached("BtnImageWidth", typeof(double), typeof(CustomButtonTextwithImage), new PropertyMetadata(25d));

        public static readonly DependencyProperty BtnImageHeightProperty =
         DependencyProperty.RegisterAttached("BtnImageHeight", typeof(double), typeof(CustomButtonTextwithImage), new PropertyMetadata(24d));

        public static readonly DependencyProperty BtnIsImageVisibleProperty =
         DependencyProperty.RegisterAttached("BtnIsImageVisible", typeof(Visibility), typeof(CustomButtonTextwithImage), new PropertyMetadata(Visibility.Visible));


        public static ImageSource GetBtnNormalImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(BtnNormalImageProperty);
        }

        public static void SetBtnNormalImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(BtnNormalImageProperty, value);
        }

        public static ImageSource GetBtnHoverImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(BtnHoverImageProperty);
        }

        public static void SetBtnHoverImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(BtnHoverImageProperty, value);
        }

        public static ImageSource GetBtnPressedImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(BtnPressedImageProperty);
        }

        public static void SetBtnPressedImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(BtnPressedImageProperty, value);
        }

        public static String GetBtnText(DependencyObject obj)
        {
            return (String)obj.GetValue(BtnTextProperty);
        }

        public static void SetBtnText(DependencyObject obj, String value)
        {
            obj.SetValue(BtnTextProperty, value);
        }

        public static double GetBtnImageWidth(DependencyObject obj)
        {
            return (double)obj.GetValue(BtnImageWidthProperty);
        }

        public static void SetBtnImageWidth(DependencyObject obj, double value)
        {
            obj.SetValue(BtnImageWidthProperty, value);
        }

        public static double GetBtnImageHeight(DependencyObject obj)
        {
            return (double)obj.GetValue(BtnImageHeightProperty);
        }

        public static void SetBtnImageHeight(DependencyObject obj, double value)
        {
            obj.SetValue(BtnImageHeightProperty, value);
        }

        public static Visibility GetBtnIsImageVisible(DependencyObject obj)
        {
            return (Visibility)obj.GetValue(BtnIsImageVisibleProperty);
        }

        public static void SetBtnIsImageVisible(DependencyObject obj, Visibility value)
        {
            obj.SetValue(BtnIsImageVisibleProperty, value);
        }

    }
}

<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Windows;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Notification;
using View.UI.PopUp;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.Notification;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.DataContainer;
using View.Utility.Stream;
using View.UI.StreamAndChannel;

namespace View.Utility
{
    public class NotificationUtility
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCAllNotification).Name);
        private static NotificationUtility _Instance;
        public static NotificationUtility Instance { get { _Instance = _Instance ?? new NotificationUtility(); return _Instance; } }

        #region Deprecated

        //public NotificationModel FindExistingModel(NotificationModel model)
        //{
        //    if (model == null) return null;
        //    NotificationModel item = null;
        //    switch (model.MessageType)
        //    {
        //        case StatusConstants.MESSAGE_LIKE_STATUS:
        //        case StatusConstants.MESSAGE_LIKE_COMMENT:
        //        case StatusConstants.MESSAGE_ADD_STATUS_COMMENT:
        //        case StatusConstants.MESSAGE_SHARE_STATUS:
        //            item = RingIDViewModel.Instance.NotificationList.Where(x => x.NewsfeedID == model.NewsfeedID && x.MessageType == model.MessageType).FirstOrDefault();

        //            break;
        //        case StatusConstants.MESSAGE_LIKE_IMAGE:
        //        case StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT:
        //        case StatusConstants.MESSAGE_IMAGE_COMMENT:
        //        case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA:
        //        case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT:
        //        case StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW:
        //        case StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT:
        //        case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA:
        //        case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT:
        //        case StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT:
        //        case StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW:
        //            item = RingIDViewModel.Instance.NotificationList.Where(x => x.ImageID == model.ImageID && x.MessageType == model.MessageType).FirstOrDefault();
        //            break;
        //        case StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER:
        //            item = RingIDViewModel.Instance.NotificationList.Where(x => x.ActivityID == model.ActivityID && x.MessageType == model.MessageType).FirstOrDefault();
        //            break;
        //        default:
        //            break;
        //    }
        //    return item;
        //}
        //public void InsertNotification(NotificationModel existingModel, NotificationModel newModel, bool indexTop)
        //{
        //    if (existingModel != null)
        //    {
        //        if (existingModel.UpdateTime < newModel.UpdateTime)
        //        {
        //            //if (existingModel.NotificationID != newModel.NotificationID)
        //            //{
        //            newModel.DeleteNotificationIDs.AddRange(existingModel.DeleteNotificationIDs);
        //            RingIDViewModel.Instance.NotificationList.Remove(existingModel);
        //            //  }
        //        }
        //        else
        //        {
        //            //if (existingModel.NotificationID != newModel.NotificationID)
        //            //{
        //            RingIDViewModel.Instance.NotificationList[RingIDViewModel.Instance.NotificationList.IndexOf(existingModel)].DeleteNotificationIDs.Add(newModel.NotificationID);
        //            newModel = null;
        //            //}
        //        }
        //    }
        //    if (newModel != null)
        //    {

        //        if (!RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == newModel.NotificationID))
        //        {
        //            if (indexTop)
        //            {
        //                RingIDViewModel.Instance.NotificationList.Insert(0, newModel);
        //            }
        //            else
        //            {
        //                RingIDViewModel.Instance.NotificationList.Add(newModel);
        //            }
        //        }

        //    }
        //}
        //public void InsertNotification(NotificationModel existingModel, NotificationModel newModel)
        //{
        //    if (existingModel != null)
        //    {
        //        if (existingModel.UpdateTime < newModel.UpdateTime)
        //        {
        //            newModel.DeleteNotificationIDs.AddRange(existingModel.DeleteNotificationIDs);
        //            RingIDViewModel.Instance.NotificationList.Remove(existingModel);
        //            RingIDViewModel.Instance.NotificationList.Add(newModel);
        //        }
        //        else
        //        {
        //            RingIDViewModel.Instance.NotificationList[RingIDViewModel.Instance.NotificationList.IndexOf(existingModel)].DeleteNotificationIDs.Add(newModel.NotificationID);
        //            newModel = null;
        //        }
        //    }
        //    else
        //    {
        //        if (!RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == newModel.NotificationID))
        //        {
        //            RingIDViewModel.Instance.NotificationList.Add(newModel);
        //        }
        //    }
        //}

        #endregion

        public void MarkAllNotificationsAsRead()
        {
            List<NotificationDTO> list = new List<NotificationDTO>();

            foreach (NotificationModel model in RingIDViewModel.Instance.NotificationList)//.Where(p => p.ShowPanelSeeMore == false)
            {
                model.IsRead = true;
                model.OnPropertyChanged("IsRead");

                NotificationDTO notificationdto = null;
                //RingDictionaries.Instance.NOTIFICATION_DICTIONARY.TryGetValue(model.NotificationID, out notificationdto);
                notificationdto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == model.NotificationID).First();
                if (notificationdto != null)
                {
                    notificationdto.IsRead = true;
                    list.Add(notificationdto);
                }
            }
            if (list.Count > 0)
            {
                new InsertIntoNotificationHistoryTable(list);
            }
        }

        public void ReadUnreadNotification(NotificationModel model)
        {
            if (model.IsRead)
            {
                //make unread
                model.IsRead = false;
            }
            else
            {
                //make read
                model.IsRead = true;
            }
            model.OnPropertyChanged("IsRead");
            model.OnPropertyChanged("CurrentInstance");

            StoreReadUnreadToDB(model);
        }

        private static void StoreReadUnreadToDB(NotificationModel model)
        {
            NotificationDTO dto = null;
            dto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == model.NotificationID).First();
            if (dto != null)
            {
                dto.IsRead = model.IsRead;
                List<NotificationDTO> list = new List<NotificationDTO>();
                list.Add(dto);
                new InsertIntoNotificationHistoryTable(list);
            }
        }

        public void DeleteNotificationFromCommand(NotificationModel model)
        {
            bool isSuccess = false;
            string msg = string.Empty;
            string nIds = string.Empty;
            nIds += model.NotificationID;

            foreach (Guid singleId in model.DeleteNotificationIDs)
            {
                nIds += string.IsNullOrEmpty(nIds) ? "" : ",";
                nIds += singleId;
            }

            JObject pakToSend = new JObject();
            JArray deleteIDS = new JArray();
            deleteIDS = CreateDeleteArrays(model.DeleteNotificationIDs);
            deleteIDS.Add(model.NotificationID);

            pakToSend[JsonKeys.DeleteNotificationIds] = deleteIDS;//CreateDeleteArrays(model.DeleteNotificationIDs);//nIds;
            (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_DELETE_MY_NOTIFICATIONS, AppConstants.REQUEST_TYPE_REQUEST)).Run(out isSuccess, out msg);
            if (isSuccess)
            {
                foreach (Guid nfId in model.DeleteNotificationIDs)
                {
                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
                    {
                        if (RingDictionaries.Instance.NOTIFICATION_LISTS.Any(x => x.ID == nfId))
                        {
                            NotificationDTO dto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == nfId).First();
                            RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(dto);
                        }
                    }
                    new DeleteFromNotificationHistoryTable(nfId);
                }
                lock (RingIDViewModel.Instance.NotificationList)
                {
                    NotificationModel deleteModel = RingIDViewModel.Instance.NotificationList.Where(P => P.NotificationID == model.NotificationID).FirstOrDefault();
                    if (deleteModel != null)
                    {
                        RingIDViewModel.Instance.NotificationList.Remove(deleteModel);

                        NotificationDTO dto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == deleteModel.NotificationID).FirstOrDefault();
                        if (dto != null)
                        {
                            RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(dto);
                        }

                        new DeleteFromNotificationHistoryTable(deleteModel.NotificationID);
                        VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;

                        if (VMNotification.Instance.NotificationModelCount == 1)
                        {
                            new ThradNotificationRequest().StartThread(AppConstants.NOTIFICATION_MIN_UT, (short)2);
                        }
                    }
                }
                VMNotification.Instance.NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(msg))
                {
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_CONFIRMATION_MESSAGE_TITLE, "Delete notification");
                    // CustomMessageBox.ShowError(NotificationMessages.NOTIFICATION_DELETE_FAIL);
                }
            }
        }

        public void ShowMoreAction(string SeeMoreText = NotificationMessages.NOTIFICATION_SHOW_MORE)
        {
            VMNotification.Instance.SeeMoreText = SeeMoreText;
        }

        public void DeleteNotifications(List<Guid> notificationIds)
        {
            bool isSuccess = false;
            string msg = string.Empty;
            string nIds = string.Empty;
            foreach (Guid singleId in notificationIds)
            {
                nIds += singleId.ToString() + ((notificationIds.IndexOf(singleId) != (notificationIds.Count - 1)) ? "," : "");
            }
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.DeleteNotificationIds] = CreateDeleteArrays(notificationIds);//nIds;
            (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_DELETE_MY_NOTIFICATIONS, AppConstants.REQUEST_TYPE_REQUEST)).Run(out isSuccess, out msg);
            if (isSuccess)
            {
                foreach (Guid id in notificationIds)
                {

                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
                    {
                        NotificationDTO dto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == id).FirstOrDefault();
                        if (dto != null)
                        {
                            RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(dto);
                        }
                    }
                    new DeleteFromNotificationHistoryTable(id);

                    lock (RingIDViewModel.Instance.NotificationList)
                    {
                        int extra = RingIDViewModel.Instance.NotificationList.Where(x => x.NotificationID == id).Count();
                        if (extra > 0)
                        {
                            if (extra > 1)
                            {
                                for (int i = 0; i < extra - 1; i++)
                                {
                                    RingIDViewModel.Instance.NotificationList.Remove(RingIDViewModel.Instance.NotificationList.Where(y => y.NotificationID == id).First());
                                }
                            }
                            RingIDViewModel.Instance.NotificationList.Remove(RingIDViewModel.Instance.NotificationList.Where(P => P.NotificationID == id).SingleOrDefault());
                        }

                    }
                }
                lock (RingIDViewModel.Instance.NotificationList)
                {
                    VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
                }

                if (VMNotification.Instance.NotificationModelCount == 1)
                {
                    new ThradNotificationRequest().StartThread(AppConstants.NOTIFICATION_MIN_UT, (short)2);
                }
                else
                {
                    VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
                }

                VMNotification.Instance.NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(msg))
                {
                    UIHelperMethods.ShowFailed("Failed! " + msg, "Delete notification");
                    //  CustomMessageBox.ShowError("Failed to delete notifications! " + msg);
                }
            }
        }

        public void NotificationSelectDeselect(bool chage = true)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                lock (RingIDViewModel.Instance.NotificationList)
                {
                    foreach (NotificationModel notificationModel in RingIDViewModel.Instance.NotificationList)//.Where(p => p.ShowPanelSeeMore == false)
                    {
                        notificationModel.IsChecked = chage;
                    }
                }
            });
        }

        private JArray CreateDeleteArrays(List<Guid> notificationIds)
        {
            JArray deleteList = new JArray();
            foreach (Guid id in notificationIds)
            {
                deleteList.Add(id);
            }
            return deleteList;
        }

        public void NotificationClickOperation(NotificationModel model)
        {
            dynamic state = new ExpandoObject();
            state.UserTableID = model.UserShortInfoModel.UserTableID;
            switch (model.MessageType)
            {
                case StatusConstants.MESSAGE_ADD_FRIEND:
                case StatusConstants.MESSAGE_ACCEPT_FRIEND:
                case StatusConstants.MESSAGE_ACCEPT_FRIEND_ACCESS:
                case StatusConstants.MESSAGE_UPGRADE_FRIEND_ACCESS:
                case StatusConstants.MESSAGE_DOWNGRADE_FRIEND_ACCESS:
                    RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(model.UserShortInfoModel.UserTableID, model.UserShortInfoModel.UserIdentity, model.UserShortInfoModel.FullName, model.UserShortInfoModel.ProfileImage);
                    break;
                ////
                case StatusConstants.MESSAGE_LIKE_IMAGE:
                case StatusConstants.MESSAGE_UPDATE_PROFILE_IMAGE:
                case StatusConstants.MESSAGE_UPDATE_COVER_IMAGE:
                case StatusConstants.MESSAGE_IMAGE_COMMENT:
                    //ImageModel singImageMOdel;
                    //if (!ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(model.ImageID, out singImageMOdel))
                    //{
                    //    singImageMOdel = new ImageModel();
                    //    ImageDataContainer.Instance.AddOrReplaceImageModels(model.ImageID, singImageMOdel);
                    //}
                    //singImageMOdel.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;//model.UserShortInfoModel;
                    //singImageMOdel.NewsFeedId = model.NewsfeedID;
                    //singImageMOdel.ImageId = model.ImageID;
                    //singImageMOdel.ImageType = 0;
                    //List<ImageModel> tempList = new List<ImageModel>();
                    //tempList.Add(singImageMOdel);
                    //if (basicImageViewWrapper().ucBasicImageView == null)
                    //{
                    //    basicImageViewWrapper().AddUCBasicImageViewToWrapperPanel();
                    //}
                    //basicImageViewWrapper().ucBasicImageView.TotalImages = 1;
                    //basicImageViewWrapper().ucBasicImageView.SelectedIndex1 = 0;
                    //basicImageViewWrapper().ucBasicImageView.NavigateFrom = "Notification";
                    //basicImageViewWrapper().ShowHandlerDialog(tempList, tempList[basicImageViewWrapper().ucBasicImageView.SelectedIndex1]);

                    //ImageModel singImageMOdel;
                    //if (!ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(model.ImageID, out singImageMOdel))
                    //{
                    //    singImageMOdel = new ImageModel();
                    //    ImageDataContainer.Instance.AddOrReplaceImageModels(model.ImageID, singImageMOdel);
                    //}
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                    {
                        ImageModel singImageMOdel = ImageDataContainer.Instance.GetFromImageModelDictionaryNotNullable(model.ImageID);
                        singImageMOdel.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;//model.UserShortInfoModel;
                        singImageMOdel.NewsFeedId = model.NewsfeedID;
                        singImageMOdel.ImageId = model.ImageID;
                        singImageMOdel.ImageType = 0;
                        ObservableCollection<ImageModel> ImageObservableList2 = new ObservableCollection<ImageModel>();
                        ImageObservableList2.Add(singImageMOdel);
                        ImageUtility.ShowImageViewer(ImageObservableList2, 0, 1, StatusConstants.NAVIGATE_FROM_NOTIFICATION, Guid.Empty);
                    }
                    break;
                case StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT:
                    //ImageModel singImageMOdel2;
                    //if (!ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(model.ImageID, out singImageMOdel2))
                    //{
                    //    singImageMOdel2 = new ImageModel();
                    //    ImageDataContainer.Instance.AddOrReplaceImageModels(model.ImageID, singImageMOdel2);
                    //}
                    //singImageMOdel2.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;//model.UserShortInfoModel;
                    //singImageMOdel2.NewsFeedId = model.NewsfeedID;
                    //singImageMOdel2.ImageId = model.ImageID;
                    //singImageMOdel2.ImageType = 0;
                    //List<ImageModel> tempList2 = new List<ImageModel>();
                    //tempList2.Add(singImageMOdel2);
                    //if (basicImageViewWrapper().ucBasicImageView == null)
                    //{
                    //    basicImageViewWrapper().AddUCBasicImageViewToWrapperPanel();
                    //}
                    //basicImageViewWrapper().ucBasicImageView.TotalImages = 1;
                    //basicImageViewWrapper().ucBasicImageView.SelectedIndex1 = 0;
                    //basicImageViewWrapper().ucBasicImageView.NavigateFrom = "Notification";
                    //basicImageViewWrapper().ShowHandlerDialog(tempList2, tempList2[basicImageViewWrapper().ucBasicImageView.SelectedIndex1], model.CommentID);
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                    {
                        ImageModel singImageMOdel2 = ImageDataContainer.Instance.GetFromImageModelDictionaryNotNullable(model.ImageID);
                        singImageMOdel2.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;//model.UserShortInfoModel;
                        singImageMOdel2.NewsFeedId = model.NewsfeedID;
                        singImageMOdel2.ImageId = model.ImageID;
                        singImageMOdel2.ImageType = 0;
                        ObservableCollection<ImageModel> ImageObservableList3 = new ObservableCollection<ImageModel>();
                        ImageObservableList3.Add(singImageMOdel2);
                        ImageUtility.ShowImageViewer(ImageObservableList3, 0, 1, StatusConstants.NAVIGATE_FROM_NOTIFICATION, Guid.Empty);
                    }
                    break;
                ////
                case StatusConstants.MESSAGE_ADD_STATUS_COMMENT:
                    onStatusRelatedNotificationClicked(model.NewsfeedID, Guid.Empty, true);
                    break;
                case StatusConstants.MESSAGE_LIKE_STATUS:
                case StatusConstants.MESSAGE_ADD_COMMENT_ON_COMMENT:
                case StatusConstants.MESSAGE_SHARE_STATUS:
                case StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG:
                case StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG:
                case StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG:
                    onStatusRelatedNotificationClicked(model.NewsfeedID, Guid.Empty);
                    break;
                case StatusConstants.MESSAGE_LIKE_COMMENT:
                    onStatusRelatedNotificationClicked(model.NewsfeedID, model.CommentID);
                    break;
                ////
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA:
                case StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT:
                case StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW:
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA:
                case StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT:
                case StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW:
                    //Application.Current.Dispatcher.Invoke(delegate
                    //{
                    //    // BasicMediaViewWrapper.OpenMediaPlayer();
                    //}, System.Windows.Threading.DispatcherPriority.Send);
                    //new System.Threading.Thread(delegate()
                    //{
                    //    if (DefaultSettings.IsInternetAvailable)
                    //        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, Guid.Empty);
                    //}).Start();
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                    {
                        SingleMediaModel singleMediamodel = null;
                        if (!MediaDataContainer.Instance.ContentModels.TryGetValue(model.ImageID, out singleMediamodel))
                        {
                            singleMediamodel = new SingleMediaModel { ContentId = model.ImageID, MediaOwner = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel };
                            MediaDataContainer.Instance.ContentModels[model.ImageID] = singleMediamodel;
                        }
                        ObservableCollection<SingleMediaModel> playList = new ObservableCollection<SingleMediaModel>();
                        playList.Add(singleMediamodel);
                        MediaUtility.RunPlayList(true, Guid.Empty, playList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, 0, true);

                        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    }
                    break;
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT:
                    //Application.Current.Dispatcher.Invoke(delegate
                    //{
                    //    // BasicMediaViewWrapper.OpenMediaPlayer();
                    //}, System.Windows.Threading.DispatcherPriority.Send);
                    //new System.Threading.Thread(delegate()
                    //{
                    //    if (DefaultSettings.IsInternetAvailable)
                    //        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    //}).Start();
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    break;
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT:
                    //Application.Current.Dispatcher.Invoke(delegate
                    //{
                    //    //BasicMediaViewWrapper.OpenMediaPlayer();
                    //}, System.Windows.Threading.DispatcherPriority.Send);
                    //new System.Threading.Thread(delegate()
                    //{
                    //    if (DefaultSettings.IsInternetAvailable)
                    //        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    //}).Start();
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    break;
                ////
                case StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER:
                    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCirclePanel, (long)model.ActivityID);
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_LIVE:
                    StreamViewModel.Instance.OnStreamViewCommand(new StreamModel
                    {
                        StreamID = model.NotificationID,
                        StartTime = model.UpdateTime,
                        UserTableID = model.UserShortInfoModel.UserTableID,
                        UserName = model.UserShortInfoModel.FullName,
                        ProfileImage = model.UserShortInfoModel.ProfileImage,
                    });
                    break;
                default:
                    break;
            }
            model.IsRead = true;
            model.OnPropertyChanged("IsRead");

            StoreReadUnreadToDB(model);
        }

        private void onStatusRelatedNotificationClicked(Guid nfId, Guid cmntId, bool commentRequest = false)
        {
            if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
            {
                try
                {
                    if (UCSingleFeedDetailsView.Instance != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(UCSingleFeedDetailsView.Instance);
                    UCSingleFeedDetailsView.Instance = new UCSingleFeedDetailsView(UCGuiRingID.Instance.MotherPanel);
                    UCSingleFeedDetailsView.Instance.Show();
                    UCSingleFeedDetailsView.Instance.ShowDetailsView();
                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Visible;
                    UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                    UCSingleFeedDetailsView.Instance.OnRemovedUserControl += () =>
                    {
                        UCSingleFeedDetailsView.Instance = null;
                    };
                    Thread myThread = new Thread(delegate()
                    {
                        bool isSuccess = false;
                        JObject feed = null;
                        string msg = string.Empty;
                        ThreadDetailsOfStatus dt = new ThreadDetailsOfStatus(nfId);
                        dt.Run(out isSuccess, out feed, out msg);
                        if (feed != null)
                        {
                            FeedModel fm = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out fm))
                            {
                                fm = new FeedModel();
                                FeedDataContainer.Instance.FeedModels[nfId] = fm;
                            }
                            fm.LoadData(feed);
                            if (cmntId != Guid.Empty) //liked my comment //>0
                            {
                                ThreadGetSingleFullComment threadFullComment = new ThreadGetSingleFullComment(nfId, cmntId, Guid.Empty, Guid.Empty);
                                threadFullComment.callBackEvent += (jObj) =>
                                {
                                    if (jObj != null)
                                    {
                                        CommentModel commentModel = new CommentModel();
                                        commentModel.CommentId = cmntId;
                                        commentModel.LoadData(jObj);
                                        commentModel.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;
                                        commentModel.NewsfeedId = nfId;
                                        fm.CommentList = new ObservableCollection<CommentModel>();
                                        fm.CommentList.Add(commentModel);
                                        fm.ShowNextCommentsVisible = 1;
                                    }
                                };
                                threadFullComment.Run();
                            }
                            else if (commentRequest == true) //<0              // 
                            {
                                fm.ShowNextCommentsVisible = 2;
                                PreviousOrNextFeedComments(1, 0, fm.NewsfeedId, Guid.Empty, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_STATUS);
                            }
                            else if (commentRequest == false) //
                            {
                                fm.ShowNextCommentsVisible = 0;
                            }
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (UCSingleFeedDetailsView.Instance != null)
                                {
                                    UCSingleFeedDetailsView.Instance.LoadFeedModel(fm);
                                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Collapsed;
                                    if (UCSingleFeedDetailsView.Instance.GIFCtrlLoader.IsRunning())
                                        UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StopAnimate();
                                }
                            });
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (UCSingleFeedDetailsView.Instance != null)
                                {
                                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Collapsed;
                                    if (UCSingleFeedDetailsView.Instance.GIFCtrlLoader.IsRunning())
                                        UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StopAnimate();
                                    UCSingleFeedDetailsView.Instance.notFound.Visibility = Visibility.Visible;
                                }
                            });
                        }
                    });
                    myThread.Start();
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        #region "Utility Methods"


        public void PreviousOrNextFeedComments(int scl, long tm, Guid nfid, Guid cntId, Guid imgId, int action, Guid pvtUUID, int ActivityType)
        {
            new PreviousOrNextFeedComments().StartThread(scl, tm, nfid, cntId, imgId, action, pvtUUID, ActivityType);
        }

        //public void PreviousOrNextFeedComments(int st, Guid nfid, Guid cntId, Guid imgId, int action, Guid pvtUUID)
        //{
        //    new PreviousOrNextFeedComments().StartThread(st, nfid, cntId, imgId, action, pvtUUID);
        //}

        //public void NotificationRequest(long max_ut, short scl)
        //{
        //    new ThradNotificationRequest().StartThread(max_ut, scl);
        //}

        //public void DetailsofaMediaItem(Guid contentId, long utId = 0, Guid cmntID)
        //{
        //    new ThradDetailsOfaMediaItem().StartThread(contentId, utId, cmntID);
        //}
        #endregion "Utility Methods"

        #region Deprecated 2
        //public void DeleteNotificationsFromDBAndServer()
        //{
        //    List<Guid> notificationIds = new List<Guid>();
        //    lock (RingIDViewModel.Instance.NotificationList)
        //    {
        //        foreach (NotificationModel nModel in RingIDViewModel.Instance.NotificationList)
        //        {
        //            if (nModel.DeleteNotificationIDs != null && nModel.DeleteNotificationIDs.Count > 0)
        //            {
        //                foreach (var item in nModel.DeleteNotificationIDs)
        //                {
        //                    if (item != nModel.NotificationID) //UCAllNotification.Instance.notificationIds.Add(item);
        //                        notificationIds.Add(item);
        //                }
        //            }
        //        }
        //    }
        //    if (notificationIds.Count > 0)
        //    {
        //        bool isSuccess = false;
        //        string msg = string.Empty;
        //        string nIds = string.Empty;
        //        //delete from db
        //        //delete from server
        //        foreach (var item in notificationIds)
        //        {
        //            nIds += string.IsNullOrEmpty(nIds) ? "" : ",";
        //            nIds += item;
        //        }
        //        JObject pakToSend = new JObject();
        //        pakToSend[JsonKeys.DeleteNotificationIds] = CreateDeleteArrays(notificationIds); //nIds;
        //        (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_DELETE_MY_NOTIFICATIONS, AppConstants.REQUEST_TYPE_REQUEST)).Run(out isSuccess, out msg);
        //        //if (isSuccess)
        //        //{
        //        //    //foreach (long nfId in nIds)
        //        //    //{
        //        //    //    //delete from db
        //        //    //    NotificationHistoryDAO.Instance.DeleteNotification(nfId);
        //        //    //}
        //        //    //UCAllNotification.Instance.notificationIds.Clear();
        //        //}
        //        lock (notificationIds)
        //        {
        //            foreach (Guid nfId in notificationIds)
        //            {
        //                //delete from db
        //                NotificationHistoryDAO.Instance.DeleteNotification(nfId);
        //            }
        //        }
        //        notificationIds.Clear();
        //    }
        //}
        #endregion
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Windows;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Notification;
using View.UI.PopUp;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.Notification;
using View.ViewModel;
using View.Utility.DataContainer;
using View.Utility.Stream;

namespace View.Utility
{
    public class NotificationUtility
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UCAllNotification).Name);
        private static NotificationUtility _Instance;
        public static NotificationUtility Instance { get { _Instance = _Instance ?? new NotificationUtility(); return _Instance; } }

        #region Deprecated

        //public NotificationModel FindExistingModel(NotificationModel model)
        //{
        //    if (model == null) return null;
        //    NotificationModel item = null;
        //    switch (model.MessageType)
        //    {
        //        case StatusConstants.MESSAGE_LIKE_STATUS:
        //        case StatusConstants.MESSAGE_LIKE_COMMENT:
        //        case StatusConstants.MESSAGE_ADD_STATUS_COMMENT:
        //        case StatusConstants.MESSAGE_SHARE_STATUS:
        //            item = RingIDViewModel.Instance.NotificationList.Where(x => x.NewsfeedID == model.NewsfeedID && x.MessageType == model.MessageType).FirstOrDefault();

        //            break;
        //        case StatusConstants.MESSAGE_LIKE_IMAGE:
        //        case StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT:
        //        case StatusConstants.MESSAGE_IMAGE_COMMENT:
        //        case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA:
        //        case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT:
        //        case StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW:
        //        case StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT:
        //        case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA:
        //        case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT:
        //        case StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT:
        //        case StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW:
        //            item = RingIDViewModel.Instance.NotificationList.Where(x => x.ImageID == model.ImageID && x.MessageType == model.MessageType).FirstOrDefault();
        //            break;
        //        case StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER:
        //            item = RingIDViewModel.Instance.NotificationList.Where(x => x.ActivityID == model.ActivityID && x.MessageType == model.MessageType).FirstOrDefault();
        //            break;
        //        default:
        //            break;
        //    }
        //    return item;
        //}
        //public void InsertNotification(NotificationModel existingModel, NotificationModel newModel, bool indexTop)
        //{
        //    if (existingModel != null)
        //    {
        //        if (existingModel.UpdateTime < newModel.UpdateTime)
        //        {
        //            //if (existingModel.NotificationID != newModel.NotificationID)
        //            //{
        //            newModel.DeleteNotificationIDs.AddRange(existingModel.DeleteNotificationIDs);
        //            RingIDViewModel.Instance.NotificationList.Remove(existingModel);
        //            //  }
        //        }
        //        else
        //        {
        //            //if (existingModel.NotificationID != newModel.NotificationID)
        //            //{
        //            RingIDViewModel.Instance.NotificationList[RingIDViewModel.Instance.NotificationList.IndexOf(existingModel)].DeleteNotificationIDs.Add(newModel.NotificationID);
        //            newModel = null;
        //            //}
        //        }
        //    }
        //    if (newModel != null)
        //    {

        //        if (!RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == newModel.NotificationID))
        //        {
        //            if (indexTop)
        //            {
        //                RingIDViewModel.Instance.NotificationList.Insert(0, newModel);
        //            }
        //            else
        //            {
        //                RingIDViewModel.Instance.NotificationList.Add(newModel);
        //            }
        //        }

        //    }
        //}
        //public void InsertNotification(NotificationModel existingModel, NotificationModel newModel)
        //{
        //    if (existingModel != null)
        //    {
        //        if (existingModel.UpdateTime < newModel.UpdateTime)
        //        {
        //            newModel.DeleteNotificationIDs.AddRange(existingModel.DeleteNotificationIDs);
        //            RingIDViewModel.Instance.NotificationList.Remove(existingModel);
        //            RingIDViewModel.Instance.NotificationList.Add(newModel);
        //        }
        //        else
        //        {
        //            RingIDViewModel.Instance.NotificationList[RingIDViewModel.Instance.NotificationList.IndexOf(existingModel)].DeleteNotificationIDs.Add(newModel.NotificationID);
        //            newModel = null;
        //        }
        //    }
        //    else
        //    {
        //        if (!RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == newModel.NotificationID))
        //        {
        //            RingIDViewModel.Instance.NotificationList.Add(newModel);
        //        }
        //    }
        //}

        #endregion

        public void MarkAllNotificationsAsRead()
        {
            List<NotificationDTO> list = new List<NotificationDTO>();

            foreach (NotificationModel model in RingIDViewModel.Instance.NotificationList)//.Where(p => p.ShowPanelSeeMore == false)
            {
                model.IsRead = true;
                model.OnPropertyChanged("IsRead");

                NotificationDTO notificationdto = null;
                //RingDictionaries.Instance.NOTIFICATION_DICTIONARY.TryGetValue(model.NotificationID, out notificationdto);
                notificationdto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == model.NotificationID).First();
                if (notificationdto != null)
                {
                    notificationdto.IsRead = true;
                    list.Add(notificationdto);
                }
            }
            if (list.Count > 0)
            {
                new InsertIntoNotificationHistoryTable(list);
            }
        }

        public void ReadUnreadNotification(NotificationModel model)
        {
            model.IsRead = model.IsRead == true ? false : true;
            model.OnPropertyChanged("IsRead");
            model.OnPropertyChanged("CurrentInstance");

            StoreReadUnreadToDB(model);
        }

        private static void StoreReadUnreadToDB(NotificationModel model)
        {
            NotificationDTO dto = null;
            dto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == model.NotificationID).First();
            if (dto != null)
            {
                dto.IsRead = model.IsRead;
                List<NotificationDTO> list = new List<NotificationDTO>();
                list.Add(dto);
                new InsertIntoNotificationHistoryTable(list);
            }
        }

        public void DeleteNotificationFromCommand(NotificationModel model)
        {
            bool isSuccess = false;
            string msg = string.Empty;
            
            JObject pakToSend = new JObject();
            JArray deleteIDS = new JArray();
            deleteIDS = CreateDeleteArrays(model.DeleteNotificationIDs);
            deleteIDS.Add(model.NotificationID);

            pakToSend[JsonKeys.DeleteNotificationIds] = deleteIDS;//CreateDeleteArrays(model.DeleteNotificationIDs);//nIds;
            (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_DELETE_MY_NOTIFICATIONS, AppConstants.REQUEST_TYPE_REQUEST)).Run(out isSuccess, out msg);
            if (isSuccess)
            {
                foreach (Guid nfId in model.DeleteNotificationIDs)
                {
                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
                    {
                        if (RingDictionaries.Instance.NOTIFICATION_LISTS.Any(x => x.ID == nfId))
                        {
                            NotificationDTO dto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == nfId).First();
                            RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(dto);
                        }
                    }
                    new DeleteFromNotificationHistoryTable(nfId);
                }
                lock (RingIDViewModel.Instance.NotificationList)
                {
                    NotificationModel deleteModel = RingIDViewModel.Instance.NotificationList.Where(P => P.NotificationID == model.NotificationID).FirstOrDefault();
                    if (deleteModel != null)
                    {
                        RingIDViewModel.Instance.NotificationList.Remove(deleteModel);

                        NotificationDTO dto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == deleteModel.NotificationID).FirstOrDefault();
                        if (dto != null)
                        {
                            RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(dto);
                        }

                        new DeleteFromNotificationHistoryTable(deleteModel.NotificationID);
                        VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;

                        if (VMNotification.Instance.NotificationModelCount == 1)
                        {
                            new ThradNotificationRequest().StartThread(AppConstants.NOTIFICATION_MIN_UT, (short)2);
                        }
                    }
                }
                VMNotification.Instance.NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(msg))
                {
                    UIHelperMethods.ShowFailed(NotificationMessages.FAILED_CONFIRMATION_MESSAGE_TITLE, "Delete notification");
                    // CustomMessageBox.ShowError(NotificationMessages.NOTIFICATION_DELETE_FAIL);
                }
            }
        }

        public void ShowMoreAction(string SeeMoreText = NotificationMessages.NOTIFICATION_SHOW_MORE)
        {
            VMNotification.Instance.SeeMoreText = SeeMoreText;
        }

        public void DeleteNotifications(List<Guid> notificationIds)
        {
            bool isSuccess = false;
            string msg = string.Empty;
            
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.DeleteNotificationIds] = CreateDeleteArrays(notificationIds);//nIds;
            (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_DELETE_MY_NOTIFICATIONS, AppConstants.REQUEST_TYPE_REQUEST)).Run(out isSuccess, out msg);
            if (isSuccess)
            {
                foreach (Guid id in notificationIds)
                {

                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
                    {
                        NotificationDTO dto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == id).FirstOrDefault();
                        if (dto != null)
                        {
                            RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(dto);
                        }
                    }
                    new DeleteFromNotificationHistoryTable(id);

                    lock (RingIDViewModel.Instance.NotificationList)
                    {
                        int extra = RingIDViewModel.Instance.NotificationList.Where(x => x.NotificationID == id).Count();
                        if (extra > 0)
                        {
                            if (extra > 1)
                            {
                                for (int i = 0; i < extra - 1; i++)
                                {
                                    RingIDViewModel.Instance.NotificationList.Remove(RingIDViewModel.Instance.NotificationList.Where(y => y.NotificationID == id).First());
                                }
                            }
                            RingIDViewModel.Instance.NotificationList.Remove(RingIDViewModel.Instance.NotificationList.Where(P => P.NotificationID == id).SingleOrDefault());
                        }

                    }
                }
                lock (RingIDViewModel.Instance.NotificationList)
                {
                    VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
                }

                if (VMNotification.Instance.NotificationModelCount == 1)
                {
                    new ThradNotificationRequest().StartThread(AppConstants.NOTIFICATION_MIN_UT, (short)2);
                }
                else
                {
                    VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
                }

                VMNotification.Instance.NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(msg))
                {
                    UIHelperMethods.ShowFailed("Failed! " + msg, "Delete notification");
                    //  CustomMessageBox.ShowError("Failed to delete notifications! " + msg);
                }
            }
        }

        public void NotificationSelectDeselect(bool chage = true)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                lock (RingIDViewModel.Instance.NotificationList)
                {
                    foreach (NotificationModel notificationModel in RingIDViewModel.Instance.NotificationList)//.Where(p => p.ShowPanelSeeMore == false)
                    {
                        notificationModel.IsChecked = chage;
                    }
                }
            });
        }

        private JArray CreateDeleteArrays(List<Guid> notificationIds)
        {
            JArray deleteList = new JArray();
            foreach (Guid id in notificationIds)
            {
                deleteList.Add(id);
            }
            return deleteList;
        }

        public void NotificationClickOperation(NotificationModel model)
        {
            dynamic state = new ExpandoObject();
            state.UserTableID = model.UserShortInfoModel.UserTableID;
            switch (model.MessageType)
            {
                case StatusConstants.MESSAGE_ADD_FRIEND:
                case StatusConstants.MESSAGE_ACCEPT_FRIEND:
                case StatusConstants.MESSAGE_ACCEPT_FRIEND_ACCESS:
                case StatusConstants.MESSAGE_UPGRADE_FRIEND_ACCESS:
                case StatusConstants.MESSAGE_DOWNGRADE_FRIEND_ACCESS:
                    RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(model.UserShortInfoModel.UserTableID, model.UserShortInfoModel.UserIdentity, model.UserShortInfoModel.FullName, model.UserShortInfoModel.ProfileImage);
                    break;
                ////
                case StatusConstants.MESSAGE_LIKE_IMAGE:
                case StatusConstants.MESSAGE_UPDATE_PROFILE_IMAGE:
                case StatusConstants.MESSAGE_UPDATE_COVER_IMAGE:
                case StatusConstants.MESSAGE_IMAGE_COMMENT:
                    //ImageModel singImageMOdel;
                    //if (!ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(model.ImageID, out singImageMOdel))
                    //{
                    //    singImageMOdel = new ImageModel();
                    //    ImageDataContainer.Instance.AddOrReplaceImageModels(model.ImageID, singImageMOdel);
                    //}
                    //singImageMOdel.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;//model.UserShortInfoModel;
                    //singImageMOdel.NewsFeedId = model.NewsfeedID;
                    //singImageMOdel.ImageId = model.ImageID;
                    //singImageMOdel.ImageType = 0;
                    //List<ImageModel> tempList = new List<ImageModel>();
                    //tempList.Add(singImageMOdel);
                    //if (basicImageViewWrapper().ucBasicImageView == null)
                    //{
                    //    basicImageViewWrapper().AddUCBasicImageViewToWrapperPanel();
                    //}
                    //basicImageViewWrapper().ucBasicImageView.TotalImages = 1;
                    //basicImageViewWrapper().ucBasicImageView.SelectedIndex1 = 0;
                    //basicImageViewWrapper().ucBasicImageView.NavigateFrom = "Notification";
                    //basicImageViewWrapper().ShowHandlerDialog(tempList, tempList[basicImageViewWrapper().ucBasicImageView.SelectedIndex1]);

                    //ImageModel singImageMOdel;
                    //if (!ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(model.ImageID, out singImageMOdel))
                    //{
                    //    singImageMOdel = new ImageModel();
                    //    ImageDataContainer.Instance.AddOrReplaceImageModels(model.ImageID, singImageMOdel);
                    //}
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                    {
                        ImageModel singImageMOdel = ImageDataContainer.Instance.GetFromImageModelDictionaryNotNullable(model.ImageID);
                        singImageMOdel.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;//model.UserShortInfoModel;
                        singImageMOdel.NewsFeedId = model.NewsfeedID;
                        singImageMOdel.ImageId = model.ImageID;
                        singImageMOdel.ImageType = 0;
                        ObservableCollection<ImageModel> ImageObservableList2 = new ObservableCollection<ImageModel>();
                        ImageObservableList2.Add(singImageMOdel);
                        ImageUtility.ShowImageViewer(ImageObservableList2, 0, 1, StatusConstants.NAVIGATE_FROM_NOTIFICATION, Guid.Empty);
                    }
                    break;
                case StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT:
                    //ImageModel singImageMOdel2;
                    //if (!ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(model.ImageID, out singImageMOdel2))
                    //{
                    //    singImageMOdel2 = new ImageModel();
                    //    ImageDataContainer.Instance.AddOrReplaceImageModels(model.ImageID, singImageMOdel2);
                    //}
                    //singImageMOdel2.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;//model.UserShortInfoModel;
                    //singImageMOdel2.NewsFeedId = model.NewsfeedID;
                    //singImageMOdel2.ImageId = model.ImageID;
                    //singImageMOdel2.ImageType = 0;
                    //List<ImageModel> tempList2 = new List<ImageModel>();
                    //tempList2.Add(singImageMOdel2);
                    //if (basicImageViewWrapper().ucBasicImageView == null)
                    //{
                    //    basicImageViewWrapper().AddUCBasicImageViewToWrapperPanel();
                    //}
                    //basicImageViewWrapper().ucBasicImageView.TotalImages = 1;
                    //basicImageViewWrapper().ucBasicImageView.SelectedIndex1 = 0;
                    //basicImageViewWrapper().ucBasicImageView.NavigateFrom = "Notification";
                    //basicImageViewWrapper().ShowHandlerDialog(tempList2, tempList2[basicImageViewWrapper().ucBasicImageView.SelectedIndex1], model.CommentID);
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                    {
                        ImageModel singImageMOdel2 = ImageDataContainer.Instance.GetFromImageModelDictionaryNotNullable(model.ImageID);
                        singImageMOdel2.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;//model.UserShortInfoModel;
                        singImageMOdel2.NewsFeedId = model.NewsfeedID;
                        singImageMOdel2.ImageId = model.ImageID;
                        singImageMOdel2.ImageType = 0;
                        ObservableCollection<ImageModel> ImageObservableList3 = new ObservableCollection<ImageModel>();
                        ImageObservableList3.Add(singImageMOdel2);
                        ImageUtility.ShowImageViewer(ImageObservableList3, 0, 1, StatusConstants.NAVIGATE_FROM_NOTIFICATION, Guid.Empty);
                    }
                    break;
                ////
                case StatusConstants.MESSAGE_ADD_STATUS_COMMENT:
                    onStatusRelatedNotificationClicked(model.NewsfeedID, Guid.Empty, true);
                    break;
                case StatusConstants.MESSAGE_LIKE_STATUS:
                case StatusConstants.MESSAGE_ADD_COMMENT_ON_COMMENT:
                case StatusConstants.MESSAGE_SHARE_STATUS:
                case StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG:
                case StatusConstants.MESSAGE_NOTIFICATION_STATUS_TAG:
                case StatusConstants.MESSAGE_NOTIFICATION_COMMENT_TAG:
                    onStatusRelatedNotificationClicked(model.NewsfeedID, Guid.Empty);
                    break;
                case StatusConstants.MESSAGE_LIKE_COMMENT:
                    onStatusRelatedNotificationClicked(model.NewsfeedID, model.CommentID);
                    break;
                ////
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA:
                case StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT:
                case StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW:
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA:
                case StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT:
                case StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW:
                    //Application.Current.Dispatcher.Invoke(delegate
                    //{
                    //    // BasicMediaViewWrapper.OpenMediaPlayer();
                    //}, System.Windows.Threading.DispatcherPriority.Send);
                    //new System.Threading.Thread(delegate()
                    //{
                    //    if (DefaultSettings.IsInternetAvailable)
                    //        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, Guid.Empty);
                    //}).Start();
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                    {
                        SingleMediaModel singleMediamodel = null;
                        if (!MediaDataContainer.Instance.ContentModels.TryGetValue(model.ImageID, out singleMediamodel))
                        {
                            singleMediamodel = new SingleMediaModel { ContentId = model.ImageID, MediaOwner = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel };
                            MediaDataContainer.Instance.ContentModels[model.ImageID] = singleMediamodel;
                        }
                        ObservableCollection<SingleMediaModel> playList = new ObservableCollection<SingleMediaModel>();
                        playList.Add(singleMediamodel);
                        MediaUtility.RunPlayList(true, Guid.Empty, playList, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, 0, true);

                        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    }
                    break;
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT:
                    //Application.Current.Dispatcher.Invoke(delegate
                    //{
                    //    // BasicMediaViewWrapper.OpenMediaPlayer();
                    //}, System.Windows.Threading.DispatcherPriority.Send);
                    //new System.Threading.Thread(delegate()
                    //{
                    //    if (DefaultSettings.IsInternetAvailable)
                    //        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    //}).Start();
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    break;
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT:
                    //Application.Current.Dispatcher.Invoke(delegate
                    //{
                    //    //BasicMediaViewWrapper.OpenMediaPlayer();
                    //}, System.Windows.Threading.DispatcherPriority.Send);
                    //new System.Threading.Thread(delegate()
                    //{
                    //    if (DefaultSettings.IsInternetAvailable)
                    //        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    //}).Start();
                    if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
                        new ThradDetailsOfaMediaItem().StartThread(model.ImageID, 0, model.CommentID);
                    break;
                ////
                case StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER:
                    MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCirclePanel, (long)model.ActivityID);
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_LIVE:
                    StreamViewModel.Instance.OnStreamViewCommand(new StreamModel
                    {
                        StreamID = model.NotificationID,
                        StartTime = model.UpdateTime,
                        UserTableID = model.UserShortInfoModel.UserTableID,
                        UserName = model.UserShortInfoModel.FullName,
                        ProfileImage = model.UserShortInfoModel.ProfileImage,
                    });
                    break;
                default:
                    break;
            }
            model.IsRead = true;
            model.OnPropertyChanged("IsRead");

            StoreReadUnreadToDB(model);
        }

        private void onStatusRelatedNotificationClicked(Guid nfId, Guid cmntId, bool commentRequest = false)
        {
            if (DefaultSettings.IsInternetAvailable && RingIDViewModel.Instance.WinDataModel.IsSignIn)
            {
                try
                {
                    if (UCSingleFeedDetailsView.Instance != null)
                        UCGuiRingID.Instance.MotherPanel.Children.Remove(UCSingleFeedDetailsView.Instance);
                    UCSingleFeedDetailsView.Instance = new UCSingleFeedDetailsView(UCGuiRingID.Instance.MotherPanel);
                    UCSingleFeedDetailsView.Instance.Show();
                    UCSingleFeedDetailsView.Instance.ShowDetailsView();
                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Visible;
                    UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StartAnimate(ImageUtility.GetBitmap(ImageLocation.LOADER_MEDIUM));
                    UCSingleFeedDetailsView.Instance.OnRemovedUserControl += () =>
                    {
                        UCSingleFeedDetailsView.Instance = null;
                    };
                    Thread myThread = new Thread(delegate()
                    {
                        bool isSuccess = false;
                        JObject feed = null;
                        string msg = string.Empty;
                        ThreadDetailsOfStatus dt = new ThreadDetailsOfStatus(nfId);
                        dt.Run(out isSuccess, out feed, out msg);
                        if (feed != null)
                        {
                            FeedModel fm = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out fm))
                            {
                                fm = new FeedModel();
                                FeedDataContainer.Instance.FeedModels[nfId] = fm;
                            }
                            fm.LoadData(feed);
                            if (cmntId != Guid.Empty) //liked my comment //>0
                            {
                                ThreadGetSingleFullComment threadFullComment = new ThreadGetSingleFullComment(nfId, cmntId, Guid.Empty, Guid.Empty);
                                threadFullComment.callBackEvent += (jObj) =>
                                {
                                    if (jObj != null)
                                    {
                                        CommentModel commentModel = new CommentModel();
                                        commentModel.CommentId = cmntId;
                                        commentModel.LoadData(jObj);
                                        commentModel.UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;
                                        commentModel.NewsfeedId = nfId;
                                        fm.CommentList = new ObservableCollection<CommentModel>();
                                        fm.CommentList.Add(commentModel);
                                        fm.ShowNextCommentsVisible = 1;
                                    }
                                };
                                threadFullComment.Run();
                            }
                            else if (commentRequest == true) //<0              // 
                            {
                                fm.ShowNextCommentsVisible = 2;
                                PreviousOrNextFeedComments(1, 0, fm.NewsfeedId, Guid.Empty, Guid.Empty, AppConstants.ACTION_MERGED_COMMENTS_LIST, Guid.Empty, SettingsConstants.ACTIVITY_ON_STATUS);
                            }
                            else if (commentRequest == false) //
                            {
                                fm.ShowNextCommentsVisible = 0;
                            }
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (UCSingleFeedDetailsView.Instance != null)
                                {
                                    UCSingleFeedDetailsView.Instance.LoadFeedModel(fm);
                                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Collapsed;
                                    if (UCSingleFeedDetailsView.Instance.GIFCtrlLoader.IsRunning())
                                        UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StopAnimate();
                                }
                            });
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (UCSingleFeedDetailsView.Instance != null)
                                {
                                    UCSingleFeedDetailsView.Instance.GIFCtrlPanel.Visibility = Visibility.Collapsed;
                                    if (UCSingleFeedDetailsView.Instance.GIFCtrlLoader.IsRunning())
                                        UCSingleFeedDetailsView.Instance.GIFCtrlLoader.StopAnimate();
                                    UCSingleFeedDetailsView.Instance.notFound.Visibility = Visibility.Visible;
                                }
                            });
                        }
                    });
                    myThread.Start();
                }
                catch (Exception ex)
                {
                    log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        #region "Utility Methods"


        public void PreviousOrNextFeedComments(int scl, long tm, Guid nfid, Guid cntId, Guid imgId, int action, Guid pvtUUID, int ActivityType)
        {
            new PreviousOrNextFeedComments().StartThread(scl, tm, nfid, cntId, imgId, action, pvtUUID, ActivityType);
        }

        //public void PreviousOrNextFeedComments(int st, Guid nfid, Guid cntId, Guid imgId, int action, Guid pvtUUID)
        //{
        //    new PreviousOrNextFeedComments().StartThread(st, nfid, cntId, imgId, action, pvtUUID);
        //}

        //public void NotificationRequest(long max_ut, short scl)
        //{
        //    new ThradNotificationRequest().StartThread(max_ut, scl);
        //}

        //public void DetailsofaMediaItem(Guid contentId, long utId = 0, Guid cmntID)
        //{
        //    new ThradDetailsOfaMediaItem().StartThread(contentId, utId, cmntID);
        //}
        #endregion "Utility Methods"

        #region Deprecated 2
        //public void DeleteNotificationsFromDBAndServer()
        //{
        //    List<Guid> notificationIds = new List<Guid>();
        //    lock (RingIDViewModel.Instance.NotificationList)
        //    {
        //        foreach (NotificationModel nModel in RingIDViewModel.Instance.NotificationList)
        //        {
        //            if (nModel.DeleteNotificationIDs != null && nModel.DeleteNotificationIDs.Count > 0)
        //            {
        //                foreach (var item in nModel.DeleteNotificationIDs)
        //                {
        //                    if (item != nModel.NotificationID) //UCAllNotification.Instance.notificationIds.Add(item);
        //                        notificationIds.Add(item);
        //                }
        //            }
        //        }
        //    }
        //    if (notificationIds.Count > 0)
        //    {
        //        bool isSuccess = false;
        //        string msg = string.Empty;
        //        string nIds = string.Empty;
        //        //delete from db
        //        //delete from server
        //        foreach (var item in notificationIds)
        //        {
        //            nIds += string.IsNullOrEmpty(nIds) ? "" : ",";
        //            nIds += item;
        //        }
        //        JObject pakToSend = new JObject();
        //        pakToSend[JsonKeys.DeleteNotificationIds] = CreateDeleteArrays(notificationIds); //nIds;
        //        (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_DELETE_MY_NOTIFICATIONS, AppConstants.REQUEST_TYPE_REQUEST)).Run(out isSuccess, out msg);
        //        //if (isSuccess)
        //        //{
        //        //    //foreach (long nfId in nIds)
        //        //    //{
        //        //    //    //delete from db
        //        //    //    NotificationHistoryDAO.Instance.DeleteNotification(nfId);
        //        //    //}
        //        //    //UCAllNotification.Instance.notificationIds.Clear();
        //        //}
        //        lock (notificationIds)
        //        {
        //            foreach (Guid nfId in notificationIds)
        //            {
        //                //delete from db
        //                NotificationHistoryDAO.Instance.DeleteNotification(nfId);
        //            }
        //        }
        //        notificationIds.Clear();
        //    }
        //}
        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

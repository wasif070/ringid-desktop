﻿using log4net;
using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace View.Utility
{
    public class BitmapImagesLoader : BackgroundWorker
    {
        private class BmpPathObject
        {
            public BitmapImage Image;
            public string FilePath;
        }
        private static readonly ILog log = LogManager.GetLogger(typeof(BitmapImagesLoader).Name);
        public static BitmapImagesLoader Instance = new BitmapImagesLoader();

        private ConcurrentQueue<BmpPathObject> Queue = new ConcurrentQueue<BmpPathObject>();

        public BitmapImagesLoader()
        {
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += BitmapsLoader_DowWork;
        }

        private void BitmapsLoader_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BmpPathObject tmp;
                while (Queue.TryDequeue(out tmp))
                {
                    tmp.Image.BeginInit();
                    tmp.Image.CacheOption = BitmapCacheOption.OnLoad;
                    tmp.Image.UriSource = new Uri(tmp.FilePath);
                    tmp.Image.EndInit();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
        public void EnqueueImages(BitmapImage image, string filePath)
        {
            try
            {
                Queue.Enqueue(new BmpPathObject { Image = image, FilePath = filePath });
                if (!IsBusy) RunWorkerAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
    }
}

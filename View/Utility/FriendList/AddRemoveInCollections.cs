﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using log4net;
using Models.Constants;
using Models.Entity;
using View.BindingModels;
using View.UI;
using View.UI.AddFriend;
using View.ViewModel;
namespace View.Utility.FriendList
{
    public class AddRemoveInCollections
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AddRemoveInCollections).Name);
        public const int Dictonary_FavouriteFriendList = 1;
        public const int Dictonary_TopFriendList = 2;
        public const int Dictonary_NonFavouriteFriendList = 3;
        public const int Dictonary_IncomingRequestFriendList = 4;
        public const int Dictonary_OutgoingRequestFriendList = 5;
        public const int Dictonary_SuggestionFriendList = 6;
        public const int Dictonary_UnknownFriendList = 7;
        public const int Dictonary_SpecialFriendList = 8;

        #region "RecentlyAddedFriendList"

        public static void AddIntoRecentlyAddedFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null && !RingIDViewModel.Instance.RecentlyAddedFriendList.Any(P => P.ShortInfoModel.UserTableID == userbasicinforModel.ShortInfoModel.UserTableID))
            {
                try
                {
                    int max = RingIDViewModel.Instance.RecentlyAddedFriendList.Count;
                    int min = 0, pivot;
                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (userbasicinforModel.ContactAddedTime < RingIDViewModel.Instance.RecentlyAddedFriendList.ElementAt(pivot).ContactAddedTime) min = pivot + 1;
                        else max = pivot;
                    }
                    RingIDViewModel.Instance.RecentlyAddedFriendList.InvokeInsert(min, userbasicinforModel);
                }
                catch (Exception) { RingIDViewModel.Instance.RecentlyAddedFriendList.InvokeAdd(userbasicinforModel); }
                if (RingIDViewModel.Instance.RecentlyAddedFriendList.Count > 10)
                {
                    UserBasicInfoModel lastItem = RingIDViewModel.Instance.RecentlyAddedFriendList.LastOrDefault();
                    RingIDViewModel.Instance.RecentlyAddedFriendList.InvokeRemove(lastItem);
                }
            }
            else log.Debug("RecentlyAddedFriendList model=null*************");
        }

        public static void RemoveFromRecentlyAddedFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (RingIDViewModel.Instance.RecentlyAddedFriendList.Where(P => P.ShortInfoModel.UserTableID == userbasicinforModel.ShortInfoModel.UserTableID).FirstOrDefault() != null)
                RingIDViewModel.Instance.RecentlyAddedFriendList.InvokeRemove(userbasicinforModel);
        }

        public static bool checkIfSingleRecentFriendExist(long utId)
        {
            return RingIDViewModel.Instance.RecentlyAddedFriendList.Any(P => P.ShortInfoModel.UserTableID == utId);
        }

        #endregion

        #region "SpecialFriendList"

        public static void AddIntoSpecialFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null)
            {
                try
                {
                    int max = RingIDViewModel.Instance.SpecialFriendList.Count;
                    int min = 0, pivot;
                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (string.Compare(userbasicinforModel.ShortInfoModel.FullName, RingIDViewModel.Instance.SpecialFriendList.ElementAt(pivot).ShortInfoModel.FullName) == 1)
                            min = pivot + 1;
                        else max = pivot;
                    }
                    RingIDViewModel.Instance.SpecialFriendList.InvokeInsert(min, userbasicinforModel);
                }
                catch (Exception) { RingIDViewModel.Instance.SpecialFriendList.InvokeAdd(userbasicinforModel); }
                AddIntoDictionaryMapper(userbasicinforModel, Dictonary_SpecialFriendList);
            }
        }

        public static bool checkIfSingleSpecialFriendExist(long utID)
        {
            return RingIDViewModel.Instance.SpecialFriendList.Any(P => P.ShortInfoModel.UserTableID == utID);
        }

        public static UserBasicInfoModel getSpecialModel(long utID)
        {
            return RingIDViewModel.Instance.SpecialFriendList.Where(P => P.ShortInfoModel.UserTableID == utID).FirstOrDefault();
        }

        public static UserBasicInfoModel getSpecialModelWitUtID(long utID)
        {
            return RingIDViewModel.Instance.SpecialFriendList.Where(P => P.ShortInfoModel.UserTableID == utID).FirstOrDefault();
        }

        #endregion

        #region "FavouriteFriendList"

        public static void AddIntoFavouriteFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null && !RingIDViewModel.Instance.FavouriteFriendList.Any(P => P.ShortInfoModel.UserTableID == userbasicinforModel.ShortInfoModel.UserTableID))
            {
                int index = 0;
                try
                {
                    int max = RingIDViewModel.Instance.FavouriteFriendList.Count;
                    int min = 0, pivot;
                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (userbasicinforModel.FavoriteRank < RingIDViewModel.Instance.FavouriteFriendList.ElementAt(pivot).FavoriteRank)
                            min = pivot + 1;
                        else max = pivot;
                    }
                    index = min;
                    RingIDViewModel.Instance.FavouriteFriendList.InvokeInsert(min, userbasicinforModel);
                }
                catch (Exception) { RingIDViewModel.Instance.FavouriteFriendList.InvokeAdd(userbasicinforModel); }
                userbasicinforModel.FriendListType = AddRemoveInCollections.Dictonary_FavouriteFriendList;
            }
        }

        public static void RemoveFromFavouriteFriendList(UserBasicInfoModel userbasicinforModel)
        {
            RingIDViewModel.Instance.FavouriteFriendList.InvokeRemove(userbasicinforModel);
            RemoveFromDictionaryMapper(userbasicinforModel);
        }

        public static UserBasicInfoModel getSingleFavouriteFriend(long utId)
        {
            return RingIDViewModel.Instance.FavouriteFriendList.Where(P => P.ShortInfoModel.UserTableID == utId).FirstOrDefault();
        }

        public static UserBasicInfoModel getSingleFavouriteFriendutID(long utID)
        {
            return RingIDViewModel.Instance.FavouriteFriendList.Where(P => P.ShortInfoModel.UserTableID == utID).FirstOrDefault();
        }

        public static bool checkIfSingleFavouriteFriendExist(long utId)
        {
            return RingIDViewModel.Instance.FavouriteFriendList.Any(P => P.ShortInfoModel.UserTableID == utId);
        }

        #endregion "FavouriteFriendList"

        #region "TopFriendList"

        public static void AddIntoTopFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null && !RingIDViewModel.Instance.TopFriendList.Any(P => P.ShortInfoModel.UserTableID == userbasicinforModel.ShortInfoModel.UserTableID))
            {
                int index = 0;
                try
                {
                    int max = RingIDViewModel.Instance.TopFriendList.Count;
                    int min = 0, pivot;
                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (userbasicinforModel.FavoriteRank < RingIDViewModel.Instance.TopFriendList.ElementAt(pivot).FavoriteRank)
                            min = pivot + 1;
                        else max = pivot;
                    }
                    index = min;
                    RingIDViewModel.Instance.TopFriendList.InvokeInsert(min, userbasicinforModel);
                }
                catch (Exception) { RingIDViewModel.Instance.TopFriendList.InvokeAdd(userbasicinforModel); }
                userbasicinforModel.FriendListType = Dictonary_TopFriendList;
                if (RingIDViewModel.Instance.TopFriendList.Count > 10)
                {
                    UserBasicInfoModel lastModel = RingIDViewModel.Instance.TopFriendList.LastOrDefault();
                    lastModel.NumberOfCalls = 0;
                    lastModel.NumberOfChats = 0;
                    RemoveFromTopFriendList(lastModel);
                    lastModel.FriendListType = AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
                    UserBasicInfoDTO dto = Models.Stores.FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(lastModel.ShortInfoModel.UserTableID);
                    FriendListLoadUtility.AddAModelIntoUI(dto, lastModel, lastModel.FriendListType);
                }
            }
            else log.Debug("TopFriendList model=null*************");
        }

        public static void RemoveFromTopFriendList(UserBasicInfoModel userbasicinforModel)
        {
            RingIDViewModel.Instance.TopFriendList.InvokeRemove(userbasicinforModel);
            RemoveFromDictionaryMapper(userbasicinforModel);
        }
        public static UserBasicInfoModel getSingleTopFriend(long utId)
        {
            return RingIDViewModel.Instance.TopFriendList.Where(P => P.ShortInfoModel.UserTableID == utId).FirstOrDefault();
        }
        public static UserBasicInfoModel getSingleTopFriendutID(long utID)
        {
            return RingIDViewModel.Instance.TopFriendList.Where(P => P.ShortInfoModel.UserTableID == utID).FirstOrDefault();
        }
        public static bool checkIfSingleTopFriendExist(long userID)
        {
            return RingIDViewModel.Instance.TopFriendList.Any(P => P.ShortInfoModel.UserTableID == userID);
        }
        #endregion "TopFriendList"

        #region "NonFavouriteFriendList"
        public static void AddIntoNonFavouriteFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null)
            {
                int index = 0;
                try
                {
                    int max = RingIDViewModel.Instance.NonFavouriteFriendList.Count;
                    int min = 0;
                    int pivot;

                    while (max > min)
                    {
                        pivot = (min + max) / 2;
                        if (string.Compare(userbasicinforModel.ShortInfoModel.FullName, RingIDViewModel.Instance.NonFavouriteFriendList.ElementAt(pivot).ShortInfoModel.FullName) == 1)
                            min = pivot + 1;
                        else max = pivot;
                    }
                    index = min;
                    RingIDViewModel.Instance.NonFavouriteFriendList.InvokeInsert(min, userbasicinforModel);
                }
                catch (Exception) { RingIDViewModel.Instance.NonFavouriteFriendList.InvokeAdd(userbasicinforModel); }
                userbasicinforModel.FriendListType = AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
            }
            else log.Debug("NonFavouriteFriendList model=null*************");
        }
        public static void AddIntoBlockContctList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null) RingIDViewModel.Instance.BlockedContactList.InvokeAdd(userbasicinforModel);
            else log.Debug("NonFavouriteFriendList model=null*************");
        }

        public static void RemoveFromNonFavouriteFriendList(UserBasicInfoModel userbasicinforModel)
        {
            RingIDViewModel.Instance.NonFavouriteFriendList.InvokeRemove(userbasicinforModel);
            RemoveFromDictionaryMapper(userbasicinforModel);
        }
        public static UserBasicInfoModel getSingleNonFavouriteFriend(long utId)
        {
            return RingIDViewModel.Instance.NonFavouriteFriendList.Where(P => P.ShortInfoModel.UserTableID == utId).FirstOrDefault();
        }
        public static UserBasicInfoModel getSingleNonFavouriteFriendutID(long utID)
        {
            return RingIDViewModel.Instance.NonFavouriteFriendList.Where(P => P.ShortInfoModel.UserTableID == utID).FirstOrDefault();
        }
        public static bool checkIfSingleNonFavouriteFriendExist(long userID)
        {
            return RingIDViewModel.Instance.NonFavouriteFriendList.Any(P => P.ShortInfoModel.UserTableID == userID);
        }
        public static bool checkIfSingleBlockedContact(long utId)
        {
            return RingIDViewModel.Instance.BlockedContactList.Any(P => P.ShortInfoModel.UserTableID == utId);
        }
        #endregion "NonFavouriteFriendList"

        #region "SuggestionFriendList"
        public static void AddIntoSuggestionFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null)
            {
                RingIDViewModel.Instance.SuggestionFriendList.InvokeAdd(userbasicinforModel);
                AddIntoDictionaryMapper(userbasicinforModel, Dictonary_SuggestionFriendList);
            }
            else log.Debug("SuggestionFriendList model=null*************");
        }

        public static void RemoveFromSuggestionFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (RingIDViewModel.Instance.SuggestionFriendList.Where(P => P.ShortInfoModel.UserTableID == userbasicinforModel.ShortInfoModel.UserTableID).FirstOrDefault() != null)
                RingIDViewModel.Instance.SuggestionFriendList.InvokeRemove(userbasicinforModel);
        }

        public static bool checkIfSingleSuggestionFriendExist(long utId)
        {
            return RingIDViewModel.Instance.SuggestionFriendList.Any(P => P.ShortInfoModel.UserTableID == utId);
        }
        #endregion "SuggestionFriendList"

        #region "UnknownFriendList"
        #endregion "UnknownFriendList"

        #region "TempFriendList"
        public static void AddIntoTempFriendList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null)
                if (userbasicinforModel.ShortInfoModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
                    RingIDViewModel.Instance.TempFriendList.InvokeAdd(userbasicinforModel);
                else log.Debug("TempFriendList model=null*************");
        }
        public static void AddIntoTempFriendSearchList(UserBasicInfoModel userbasicinforModel)
        {
            if (userbasicinforModel != null)
                if (userbasicinforModel.ShortInfoModel.UserTableID != DefaultSettings.LOGIN_TABLE_ID) RingIDViewModel.Instance.TempFriendSearchList.InvokeAdd(userbasicinforModel);
                else log.Debug("TempFriendSearchList model=null*************");
        }

        public static void InsertIntoTempFriendList(int index, UserBasicInfoModel userbasicinforModel)
        {
            RingIDViewModel.Instance.TempFriendList.InvokeInsert(index, userbasicinforModel);
        }

        public static void RemoveFromTempFriendList(UserBasicInfoModel userbasicinforModel)
        {
            RingIDViewModel.Instance.TempFriendList.InvokeRemove(userbasicinforModel);
        }

        public static UserBasicInfoModel getSingleTempFriend(long userTableId)
        {
            return RingIDViewModel.Instance.TempFriendList.Where(P => P.ShortInfoModel.UserTableID == userTableId).FirstOrDefault();
        }

        public static bool checkIfTempFriendList(long userTableId)
        {
            if (RingIDViewModel.Instance.TempFriendList.Count > 0) return RingIDViewModel.Instance.TempFriendList.Any(P => P.ShortInfoModel.UserTableID == userTableId);
            return false;
        }

        public static bool checkIfTempFriendSearchList(long userTableId)
        {
            if (RingIDViewModel.Instance.TempFriendSearchList.Count > 0) return RingIDViewModel.Instance.TempFriendSearchList.Any(P => P.ShortInfoModel.UserTableID == userTableId);
            return false;
        }

        public static void ClearTempFriendList()
        {
            Application.Current.Dispatcher.Invoke(() =>
           {
               RingIDViewModel.Instance.TempFriendList.Clear();
           }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public static void ClearTempFriendSearchList()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                RingIDViewModel.Instance.TempFriendSearchList.Clear();
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public static void AddIntoTempFriendListWithChecking(UserBasicInfoModel newModel, UserBasicInfoDTO userDto)
        {
            List<UserBasicInfoModel> tempList = RingIDViewModel.Instance.TempFriendList.Count > 0 ? RingIDViewModel.Instance.TempFriendList.ToList() : new List<UserBasicInfoModel>();
            UserBasicInfoModel userModel = null;
            if (tempList.Count > 0) userModel = tempList.Where(P => P.ShortInfoModel.UserTableID == userDto.UserTableID).FirstOrDefault();
            if (userModel == null) RingIDViewModel.Instance.TempFriendList.InvokeAdd(newModel);
            else
            {
                userModel.NumberOfMutualFriends = userDto.NumberOfMutualFriends;
                userModel.ShortInfoModel.FullName = userDto.FullName;
                userModel.ShortInfoModel.ProfileImage = userDto.ProfileImage;
            }
        }

        public static void AddIntoTempFriendList(UserBasicInfoModel model, long userTableId)
        {
            AddRemoveInCollections.AddIntoTempFriendList(model);
        }

        public static void AddIntoTempFriendSearchListWithChecking(UserBasicInfoModel model, long userTableId)
        {
            if (!AddRemoveInCollections.checkIfTempFriendSearchList(userTableId))
            {
                AddRemoveInCollections.AddIntoTempFriendSearchList(model);
            }
        }

        #endregion "AddFriendList"

        #region "AddRemoveinto Dictionary"
        public static void AddIntoDictionaryMapper(UserBasicInfoModel userbasicinforModel, int type)
        {
            //lock (FriendDictionaries.Instance.FRIEND_DICTIONARY_MAPPER)
            //{
            //    if (userbasicinforModel.ShortInfoModel.UserIdentity > 0)
            //        FriendDictionaries.Instance.FRIEND_DICTIONARY_MAPPER[userbasicinforModel.ShortInfoModel.UserIdentity] = type;
            //    else if (userbasicinforModel.ShortInfoModel.UserTableID > 0)
            //    {
            //        FriendDictionaries.Instance.FRIEND_DICTIONARY_MAPPER[userbasicinforModel.ShortInfoModel.UserTableID] = type;
            //    }
            //}

        }
        public static void RemoveFromDictionaryMapper(UserBasicInfoModel userbasicinforModel)
        {
            //lock (FriendDictionaries.Instance.FRIEND_DICTIONARY_MAPPER)
            //{
            //    if (userbasicinforModel != null && userbasicinforModel.ShortInfoModel != null)
            //    {
            //        if (userbasicinforModel.ShortInfoModel.UserIdentity > 0)
            //            FriendDictionaries.Instance.FRIEND_DICTIONARY_MAPPER.Remove(userbasicinforModel.ShortInfoModel.UserIdentity);
            //        else if (userbasicinforModel.ShortInfoModel.UserTableID > 0)
            //            FriendDictionaries.Instance.FRIEND_DICTIONARY_MAPPER.Remove(userbasicinforModel.ShortInfoModel.UserTableID);
            //    }
            //}
        }
        //public static int getDictionaryType(long useridOrutid)
        //{
        //    int type;
        //    lock (FriendDictionaries.Instance.FRIEND_DICTIONARY_MAPPER)
        //    {
        //        FriendDictionaries.Instance.FRIEND_DICTIONARY_MAPPER.TryGetValue(useridOrutid, out type);
        //        return type;
        //    }
        //}
        #endregion "addRemoveinto Dictionary"

        #region "Change UI In Accepted Friend List"
        //public static void UpdateUIMyprofile(int index, UserBasicInfoModel userbasicinforModel)
        //{
        //    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists != null)
        //    {
        //        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.InsertIntoFriendListInMyProfile(index, userbasicinforModel);
        //    }
        //}

        public static void DeleteUIFromProfile(long utId)
        {
            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists != null)
            {
                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.DeleteFromFriendListInMyProfile(utId);
            }
        }
        #endregion "Change UI In Accepted Friend List"

        #region "Change UI in  UnKnownFriend"
        public static void RemoveUIFromPending(UserBasicInfoDTO userDto, bool Isincoming, bool isFromServer)
        {
            if (!(UCAddFriendPending.Instance != null && UCAddFriendPending.Instance.IsVisible) && AppConstants.ADD_FRIEND_NOTIFICATION_COUNT > 0 && !isFromServer)
            {
                AppConstants.ADD_FRIEND_NOTIFICATION_COUNT = AppConstants.ADD_FRIEND_NOTIFICATION_COUNT - 1;
                FriendListLoadUtility.AddFriendsNotificationForIncomingRequest();
            }
        }
        public static void InsertASingleFriendToUIPending(UserBasicInfoModel userModel, bool IsIncoming)
        {
            if (UCAddFriendPending.Instance != null) UCAddFriendPending.Instance.InsertUIInPending(userModel, IsIncoming);
        }

        public static void InsertUIPending(UserBasicInfoModel userModel, bool IsIncoming)
        {
            if (UCAddFriendPending.Instance != null)
            {
                if ((IsIncoming && UCAddFriendPending.Instance.PendingListIncoming.Count < 10) || (!IsIncoming && UCAddFriendPending.Instance.PendingListOutgoing.Count < 10))
                    UCAddFriendPending.Instance.InsertUIInPending(userModel, IsIncoming);
            }
        }
        #endregion "Change UI in  UnKnownFriend"

        #region "Public methods"
        public static int GetTotalFrindsTopAndFav()
        {
            return RingIDViewModel.Instance.FavouriteFriendList.Count + RingIDViewModel.Instance.TopFriendList.Count;
        }
        public static int GetTotalFrindsIncollections()
        {
            return RingIDViewModel.Instance.NonFavouriteFriendList.Count + RingIDViewModel.Instance.FavouriteFriendList.Count + RingIDViewModel.Instance.TopFriendList.Count;
        }
        #endregion
    }
}

﻿using System.Collections.Generic;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.UI;
using View.ViewModel;

namespace View.Utility.FriendList
{
    class FriendListSearchResult
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(FriendListSearchResult).Name);

        public void StartProcessing(long searchCount, long matchCount, List<UserBasicInfoDTO> listuserBasicInfoDTO)
        {
            ThreadLeftSearchFriend.totalSearchCount += searchCount;
            ThreadLeftSearchFriend.friendCountInOneRequest += matchCount;
            // HideSearchingLoader();

            if (ThreadLeftSearchFriend.friendCountInOneRequest > 0 && listuserBasicInfoDTO != null)
            {
                lock (UCGuiRingID.Instance._syncRoot)
                {
                    foreach (var dto in listuserBasicInfoDTO)
                    {
                        UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(dto.UserTableID);
                        if (model == null)
                        {
                            model = new UserBasicInfoModel(dto);
                            FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                        }
                        else
                        {
                            model.LoadData(dto);
                        }

                        if (ThreadLeftSearchFriend.searchParm != null)
                        {
                            AddRemoveInCollections.AddIntoTempFriendSearchListWithChecking(model, dto.UserTableID);
                        }
                        #region Recent OldCode
                        ////if (!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(dto.UserTableID)
                        ////          || (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(dto.UserTableID) && FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[dto.UserTableID].FriendShipStatus == 0))
                        ////{
                        //UserBasicInfoDTO userbasicinfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(dto.UserTableID);
                        //if (userbasicinfo == null || userbasicinfo.FriendShipStatus == 0)
                        //{
                        //    UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(dto.UserTableID);
                        //    if (model == null)
                        //    {
                        //        model = new UserBasicInfoModel(dto);
                        //        FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                        //    }
                        //    else
                        //    {
                        //        model.LoadData(dto);
                        //    }

                        //    if (ThreadLeftSearchFriend.searchParm != null && FriendListLoadUtility.CheckIfMatch(ThreadLeftSearchFriend.searchParm, model, ThreadLeftSearchFriend.searchBy))
                        //    {
                        //        AddRemoveInCollections.AddIntoTempFriendSearchListWithChecking(model, dto.UserTableID);
                        //    }
                        //}
                        #endregion
                    }
                }
                #region OldCode
                //lock (RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY)
                //{
                //    foreach (Dictionary<long, UserBasicInfoDTO> userMap in RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY.Values)
                //    {
                //        foreach (long key in userMap.Keys)
                //        {
                //            if (!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(userMap[key].UserIdentity)
                //              || (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(userMap[key].UserIdentity) && FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userMap[key].UserIdentity].FriendShipStatus == 0))
                //            {
                //                //  model = null;
                //                UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userMap[key].UserIdentity);
                //                if (model == null)
                //                {
                //                    model = new UserBasicInfoModel(userMap[key]);
                //                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                //                }
                //                else
                //                {
                //                    userMap[key].BlockedBy = model.BlockedBy;
                //                    model.LoadShowMoreData(userMap[key]);
                //                }


                //                if (ThreadLeftSearchFriend.searchParm != null && FriendListLoadUtility.CheckIfMatch(ThreadLeftSearchFriend.searchParm, model, ThreadLeftSearchFriend.searchBy))
                //                {
                //                    AddRemoveInCollections.AddIntoTempFriendSearchListWithChecking(model, userMap[key].UserIdentity);
                //                }
                //            }
                //        }

                //    }
                //}
                #endregion
            }
            hideShowMoreInAddFriend(ThreadLeftSearchFriend.friendCountInOneRequest);
        }

        private void hideShowMoreInAddFriend(long searchCount)
        {
            ThreadLeftSearchFriend.SearchForShowMore = false;
            if (UCGuiRingID.Instance.ucFriendSearchListPanel != null && UCGuiRingID.Instance.ucFriendSearchListPanel.IsVisible)
            {
                if (searchCount == 0)
                {
                    UCGuiRingID.Instance.ucFriendSearchListPanel.HideShowMorePanel();
                    if (ThreadLeftSearchFriend.totalSearchCount == 0 || RingIDViewModel.Instance.TempFriendSearchList.Count == 0)
                    {
                        UCGuiRingID.Instance.ucFriendSearchListPanel.NoResultsFound = true;
                    }
                }
                else
                {
                    UCGuiRingID.Instance.ucFriendSearchListPanel.NoResultsFound = false;
                    UCGuiRingID.Instance.ucFriendSearchListPanel.ShowLoader(false); // showing showmore text disabling loading image
                }
            }
            if (UCMiddlePanelSwitcher.View_UCWalletMainPanel != null)
            {
                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                {
                    if (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child is View.UI.Wallet.UCCoinTransfer)
                    {
                        if (searchCount == 0)
                        {
                            (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child as View.UI.Wallet.UCCoinTransfer)._ReferralPopup.HideShowMorePanel();
                            if (ThreadLeftSearchFriend.totalSearchCount == 0 || RingIDViewModel.Instance.TempFriendSearchList.Count == 0)
                            {
                                (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child as View.UI.Wallet.UCCoinTransfer)._ReferralPopup.NoResultsFound = true;
                            }
                        }
                        else
                        {
                            (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child as View.UI.Wallet.UCCoinTransfer)._ReferralPopup.NoResultsFound = false;
                            (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._MyCoinsPanel.Child as View.UI.Wallet.UCCoinTransfer)._ReferralPopup.ShowLoader(false);
                        }
                    }
                    else if (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel != null && UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child is View.UI.Wallet.UCSendReferral)
                    {
                        if (searchCount == 0)
                        {
                            (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child as View.UI.Wallet.UCSendReferral)._ReferralPopup.HideShowMorePanel();
                            if (ThreadLeftSearchFriend.totalSearchCount == 0 || RingIDViewModel.Instance.TempFriendSearchList.Count == 0)
                            {
                                (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child as View.UI.Wallet.UCSendReferral)._ReferralPopup.NoResultsFound = true;
                            }
                        }
                        else
                        {
                            (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child as View.UI.Wallet.UCSendReferral)._ReferralPopup.NoResultsFound = false;
                            (UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child as View.UI.Wallet.UCSendReferral)._ReferralPopup.ShowLoader(false);
                        }
                    }
                    else if (Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content is UI.Wallet.UCSetReferrer)
                    {
                        if (searchCount == 0)
                        {
                            (Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content as View.UI.Wallet.UCSetReferrer)._ReferralPopup.HideShowMorePanel();
                            if (ThreadLeftSearchFriend.totalSearchCount == 0 || RingIDViewModel.Instance.TempFriendSearchList.Count == 0)
                            {
                                (Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content as View.UI.Wallet.UCSetReferrer)._ReferralPopup.NoResultsFound = true;
                            }
                        }
                        else
                        {
                            (Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content as View.UI.Wallet.UCSetReferrer)._ReferralPopup.NoResultsFound = false;
                            (Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content as View.UI.Wallet.UCSetReferrer)._ReferralPopup.ShowLoader(false);
                        }
                    }

                });
            }
        }
    }
}

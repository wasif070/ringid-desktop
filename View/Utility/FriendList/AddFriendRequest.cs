﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.Utility.FriendProfile;

namespace View.Utility.FriendList
{
    public class AddFriendRequest
    {
        private long _UserTableID;
        //private int _ContactType;
        private readonly ILog log = LogManager.GetLogger(typeof(AddFriendRequest).Name);
        public AddFriendRequest(long userTableID)
        {
            this._UserTableID = userTableID;
            //this._ContactType = contactType;
        }
        public void Run(out bool isSuccess, out UserBasicInfoDTO userBasicInfo, out string msg, out long updateTime)
        {
            msg = "";
            isSuccess = false;
            userBasicInfo = null;
            updateTime = 0;
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string packetId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetId;
                    pakToSend[JsonKeys.UserTableID] = _UserTableID.ToString();
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_FRIEND;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                    if (feedbackfields != null)
                    {
                        try
                        {
                            if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                            {

                                isSuccess = true;
                                if (feedbackfields[JsonKeys.UpdateTime] != null)
                                {
                                    updateTime = (long)feedbackfields[JsonKeys.UpdateTime];
                                }
                                //    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(_UserTableID, out userBasicInfo);
                                userBasicInfo = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(_UserTableID, feedbackfields);
                                // HelperMethodsModel.BindAddFriendDetails(feedbackfields, userBasicInfo);
                                //   HelperMethodsAuth.AuthHandlerInstance.CheckFriendPresence(userBasicInfo.UserIdentity);
                                MainSwitcher.AuthSignalHandler().profileSignalHandler.CheckFriendPresence(userBasicInfo.UserTableID);
                                FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(userBasicInfo.UserTableID);
                                FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userBasicInfo.UserTableID);
                            }
                            else
                            {
                                if (feedbackfields[JsonKeys.Message] != null)
                                {
                                    msg = (string)feedbackfields[JsonKeys.Message];
                                }
                                if (feedbackfields[JsonKeys.ReasonCode] != null)
                                {
                                    if (string.IsNullOrEmpty(msg))
                                    {
                                        msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
                                    }
                                    ThradUnknownProfileInfoRequest unknwnReq = new ThradUnknownProfileInfoRequest();
                                    unknwnReq.StartThread(_UserTableID, true);
                                }
                            }

                        }
                        finally { }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    }
                    else
                    {
                        if (!MainSwitcher.ThreadManager().PingNow()) { }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("AddFriendRequest ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                }
            }
            else
            {
                log.Error("AddFriendRequest Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                //msg = NotificationMessages.INTERNET_UNAVAILABLE;
            }
        }
    }
}

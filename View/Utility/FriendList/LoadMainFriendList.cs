﻿
namespace View.Utility.FriendList
{
    //class LoadMainFriendList : BackgroundWorker
    //{
    //    //private ConcurrentQueue<UserBasicInfoDTO> _FriendQueue = new ConcurrentQueue<UserBasicInfoDTO>();
    //    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(BackgroundWorker).Name);
    //    public bool isLoadDataFromServer = false;

    //    public LoadMainFriendList()
    //    {
    //        WorkerSupportsCancellation = true;
    //        WorkerReportsProgress = true;
    //        DoWork += LoadMainFriendList_DowWork;
    //        ProgressChanged += LoadMainFriendList_ProgressChanged;
    //        RunWorkerCompleted += LoadMainFriendList_RunWorkerCompleted;
    //    }

    //    public void Start()
    //    {
    //        if (IsBusy != true)
    //        {
    //            RunWorkerAsync();
    //        }
    //    }

    //    public void Cancel()
    //    {
    //        if (WorkerSupportsCancellation == true)
    //        {
    //            CancelAsync();
    //        }
    //    }

    //    private void LoadMainFriendList_DowWork(object sender, DoWorkEventArgs e)
    //    {
    //        try
    //        {
    //            UserBasicInfoDTO userDTO = null;
    //            while (FriendListController.Instance.FriendDataContainer.TempFriendQueue.TryDequeue(out userDTO))
    //            {
    //                //System.Threading.Thread.Sleep(10);
    //                if (FriendListController.Instance.FriendDataContainer.TempFriendQueue.Count > 0 && FriendListController.Instance.FriendDataContainer.TempFriendQueue.Count % 20 == 0)
    //                {
    //                    System.Threading.Thread.Sleep(10);
    //                }
    //                int isFavTop = 0;
    //                if (userDTO.IsFavouriteTemp)
    //                {
    //                    isFavTop = 1;
    //                }
    //                else if (userDTO.IsTopTemp)
    //                {
    //                    isFavTop = 2;
    //                }

    //                // ReportProgress(isFavTop, userDTO);
    //                InsertIntoList(isFavTop, userDTO);
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("GroupListLoad_DowWork ==> " + ex.Message + "\n" + ex.StackTrace);
    //        }
    //        e.Result = true;
    //    }

    //    private void InsertIntoList(int isFavTop, UserBasicInfoDTO userDTO)
    //    {
    //        try
    //        {
    //            bool isFavourite = (isFavTop == 1) ? true : false;
    //            bool isTop = (isFavTop == 2) ? true : false;

    //            if (isLoadDataFromServer)
    //            {
    //                FriendListLoadUtility.RemoveAModelFromUI(userDTO, null, true);
    //                //   FriendListLoadUtility.RemoveSingleFriendFromUILists(userDTO, true, true, true); // For Removing Duplicate Data
    //            }
    //            UserBasicInfoModel userModel = new UserBasicInfoModel(userDTO);
    //            if (userModel.ShortInfoModel.ContactType == SettingsConstants.SPECIAL_CONTACT)
    //            {
    //                if (!AddRemoveInCollections.checkIfSingleSpecialFriendExist(userModel.ShortInfoModel.UserIdentity))
    //                {
    //                    AddRemoveInCollections.AddIntoSpecialFriendList(userModel);
    //                }
    //            }
    //            else
    //            {
    //                if (userModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
    //                {
    //                    long unreadUserContactTime = Models.Utility.ModelUtility.CurrentTimeMillisLocal() - Models.Utility.ModelUtility.MilliSecondsInSevenDay;
    //                    long readUserContactTime = Models.Utility.ModelUtility.CurrentTimeMillisLocal() - Models.Utility.ModelUtility.MilliSecondsInOneDay;

    //                    if ((userModel.ContactAddedReadUnread == SettingsConstants.RECENT_FRND_UNREAD && userModel.ContactAddedTime > unreadUserContactTime)
    //                       || (userModel.ContactAddedReadUnread == SettingsConstants.RECENT_FRND_READ && userModel.ContactAddedTime > readUserContactTime))
    //                    {
    //                        //if (!AddRemoveInCollections.checkIfSingleRecentFriendExist(userModel.ShortInfoModel.UserIdentity))
    //                        //{
    //                        AddRemoveInCollections.AddIntoRecentlyAddedFriendList(userModel);
    //                        //   }
    //                    }
    //                    if (isFavourite)
    //                    {
    //                        if (!AddRemoveInCollections.checkIfSingleFavouriteFriendExist(userModel.ShortInfoModel.UserIdentity))
    //                        {
    //                            AddRemoveInCollections.AddIntoFavouriteFriendList(userModel);
    //                        }
    //                    }
    //                    else if (isTop)
    //                    {
    //                        //if (!AddRemoveInCollections.checkIfSingleTopFriendExist(userModel.ShortInfoModel.UserIdentity))
    //                        //{
    //                        AddRemoveInCollections.AddIntoTopFriendList(userModel);
    //                        //   }
    //                    }
    //                    else if (!isFavourite && !isTop)
    //                    {
    //                        if (!AddRemoveInCollections.checkIfSingleNonFavouriteFriendExist(userModel.ShortInfoModel.UserIdentity))
    //                        {
    //                            AddRemoveInCollections.AddIntoNonFavouriteFriendList(userModel);
    //                        }
    //                    }
    //                    if (userDTO.CallAccess == 0 && userDTO.ChatAccess == 0 && userDTO.FeedAccess == 0)
    //                    {
    //                        if (!AddRemoveInCollections.checkIfSingleBlockedContact(userModel.ShortInfoModel.UserIdentity))
    //                        {
    //                            AddRemoveInCollections.AddIntoBlockContctList(userModel);
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    if (userModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
    //                    {
    //                        //if (!AddRemoveInCollections.checkIfSingleIncomingRequestFriendExist(userModel.ShortInfoModel.UserIdentity))
    //                        //{
    //                        //    AddRemoveInCollections.AddIntoIncomingRequestFriendList(userModel);
    //                        //    AddRemoveInCollections.InsertUIPending(userModel, true);
    //                        //}
    //                    }
    //                    else if (userModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
    //                    {
    //                        //if (!AddRemoveInCollections.checkIfSingleOutgoingRequestFriendExist(userModel.ShortInfoModel.UserIdentity))
    //                        //{
    //                        //    AddRemoveInCollections.AddIntoOutgoingRequestFriendList(userModel);
    //                        //    AddRemoveInCollections.InsertUIPending(userModel, false);
    //                        //}
    //                    }
    //                    else if (userModel.ShortInfoModel.FriendShipStatus == 0)
    //                    {
    //                        FriendListLoadUtility.RemoveAModelFromUI(userDTO, userModel, true);
    //                        //   FriendListLoadUtility.RemoveSingleFriendFromUILists(userModel.basicInfoDTO, true);
    //                    }
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("Error GroupListLoad_ProgressChanged()." + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
    //        }
    //    }

    //    private void LoadMainFriendList_ProgressChanged(object sender, ProgressChangedEventArgs e)
    //    {

    //    }
    //    private void LoadMainFriendList_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    //    {
    //        if (DefaultSettings.DEBUG)
    //        {
    //            log.Info("*********Main FriendList Loading completed**********");
    //        }
    //        Models.Constants.DefaultSettings.FRIEND_LIST_LOADED_FROM_DB = true;
    //    }
    //}
}

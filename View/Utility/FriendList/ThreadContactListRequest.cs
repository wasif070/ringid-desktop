﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace View.Utility.FriendList
{
    public class ThreadContactListRequest
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadContactListRequest).Name);
        // private static ContactListRequest _Instance;
        private bool _IsRunning = false;
        private Dictionary<String, JObject> sendMap = new Dictionary<String, JObject>();

        //public static ThreadContactListRequest Instance
        //{
        //    get { return _Instance; }
        //}

        public ThreadContactListRequest()
        {
            //_Instance = this;
            //ThreadStart childref = new ThreadStart(run);
            //Thread childThread = new Thread(childref);
            //childThread.Start();

        }

        public void StartThread()
        {
            if (!_IsRunning)
            {
                ThreadStart childref = new ThreadStart(run);
                Thread childThread = new Thread(childref);
                childThread.Start();
                childThread.Name = this.GetType().Name;
            }
        }

        private JObject method(long[] utids)
        {
            JArray jArray = new JArray();
            lock (utids)
            {
                for (int i = 0; i < utids.Length; i++)
                {
                    byte[] bytes = BitConverter.GetBytes(utids[i]);
                    if (BitConverter.IsLittleEndian) bytes = bytes.Reverse().ToArray();
                    foreach (var item in bytes) jArray.Add((object)item);
                }
            }

            /*byte[] send_bytes = new byte[utids.Length * 8];
            int index = 0;
            foreach (long value in utids)
            {
                send_bytes[index++] = (byte)(value >> 56);
                send_bytes[index++] = (byte)(value >> 48);
                send_bytes[index++] = (byte)(value >> 40);
                send_bytes[index++] = (byte)(value >> 32);
                send_bytes[index++] = (byte)(value >> 24);
                send_bytes[index++] = (byte)(value >> 16);
                send_bytes[index++] = (byte)(value >> 8);
                send_bytes[index++] = (byte)(value);
            }*/

            JObject packet = new JObject();
            packet[JsonKeys.Action] = AppConstants.TYPE_CONTACT_LIST;
            String pakId = SendToServer.GetRanDomPacketID();
            packet[JsonKeys.PacketId] = pakId;
            packet[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            packet[JsonKeys.UserTableIDs] = jArray;

            if (SettingsConstants.VALUE_RINGID_USERINFO_UT <= 0 && SettingsConstants.VALUE_RINGID_CONTACT_UT <= 0)
            {
                packet[JsonKeys.UpdateOnly] = false;
            }
            else
            {
                packet[JsonKeys.UpdateOnly] = true;
            }
            return packet;
        }


        private Dictionary<String, JObject> buildPacket()
        {
            Dictionary<String, JObject> tempMap = new Dictionary<String, JObject>();
            List<long> utIds = new List<long>();
            lock (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY)
            {
                foreach (long id in FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Keys)
                {
                    utIds.Add(id);
                }
            }

            long[] utidArray = null;

            while (utIds.Count > 0)
            {
                if (utIds.Count > 50)
                {
                    utidArray = new long[50];
                    for (int i = 0; i < 50; i++)
                    {
                        utidArray[i] = utIds.ElementAt(i);
                    }
                    foreach (long id in utidArray)
                    {
                        utIds.Remove(id);
                    }
                    JObject packet = method(utidArray);
                    tempMap[packet[JsonKeys.PacketId].ToString()] = packet;
                }
                else
                {
                    utidArray = new long[utIds.Count];
                    for (int i = 0; i < utIds.Count; i++)
                    {
                        utidArray[i] = utIds.ElementAt(i);
                    }
                    foreach (long id in utidArray)
                    {
                        utIds.Remove(id);
                    }
                    JObject packet = method(utidArray);
                    tempMap[packet[JsonKeys.PacketId].ToString()] = packet;
                }
            }
            return tempMap;
        }

        private void run()
        {

            SetRunning(true);
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    sendPacket();

                    for (int i = 250; i <= 5 * 3000; i += 250)
                    {
                        Thread.Sleep(250);

                        if (isAllPacketConfirmationReceived() == false)
                        {
                            if (i % 3000 == 0)
                            {
                                sendPacket();
                            }
                        }
                        else
                        {
                            break;
                        }

                    }
                }
                catch (Exception e)
                {
                    log.Error("ContactListRequest run() ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            SetRunning(false);
        }

        private void sendPacket()
        {
            try
            {
                sendMap = buildPacket();
                foreach (string key in sendMap.Keys)
                {
                    JObject packet;
                    sendMap.TryGetValue(key, out packet);
                    string data = JsonConvert.SerializeObject(packet, Formatting.None);
                    SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    Thread.Sleep(500);
                }

            }
            catch (Exception e)
            {
                log.Error("ContactListRequest sendPacket() ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        private bool isAllPacketConfirmationReceived()
        {

            foreach (String packetId in sendMap.Keys)
            {
                // if (RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(packetId))
                // {
                JObject obj;
                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out obj);
                // }
            }
            if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IsRunning()
        {
            return _IsRunning;
        }

        public void SetRunning(bool isRunning)
        {
            this._IsRunning = isRunning;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.Utility.FriendProfile;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility.FriendList
{
    public class ThradAcceptFriendRequest
    {
        #region "Private Fields"
        private UserBasicInfoModel _model;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradAcceptFriendRequest).Name);
        private bool running = false;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThradAcceptFriendRequest()
        {

        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                _model.VisibilityModel.ShowActionButton = false;
                _model.VisibilityModel.ShowLoading = Visibility.Visible;

                UserBasicInfoDTO userBasicInfo = null;
                JObject pakToSend = new JObject();
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACCEPT_FRIEND;
                pakToSend[JsonKeys.UserTableID] = _model.ShortInfoModel.UserTableID;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    userBasicInfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(_model.ShortInfoModel.UserTableID);
                    //   FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(_model.ShortInfoModel.UserTableID, out userBasicInfo);
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        if (userBasicInfo != null)
                        {
                            if (feedbackfields[JsonKeys.UserIdentity] != null)
                            {
                                userBasicInfo.RingID = (long)feedbackfields[JsonKeys.UserIdentity];
                            }
                            if (feedbackfields[JsonKeys.FullName] != null)
                            {
                                userBasicInfo.FullName = (string)feedbackfields[JsonKeys.FullName];
                            }
                            if (feedbackfields[JsonKeys.Gender] != null)
                            {
                                userBasicInfo.Gender = (string)feedbackfields[JsonKeys.Gender];
                            }
                            if (feedbackfields[JsonKeys.FriendshipStatus] != null)
                            {
                                userBasicInfo.FriendShipStatus = (int)feedbackfields[JsonKeys.FriendshipStatus];
                            }
                            if (feedbackfields[JsonKeys.ProfileImage] != null)
                            {
                                userBasicInfo.ProfileImage = (string)feedbackfields[JsonKeys.ProfileImage];
                            }
                            if (feedbackfields[JsonKeys.ProfileImageId] != null)
                            {
                                userBasicInfo.ProfileImageId = (Guid)feedbackfields[JsonKeys.ProfileImageId];
                            }
                            if (feedbackfields[JsonKeys.ContactType] != null)
                            {
                                userBasicInfo.ContactType = (int)feedbackfields[JsonKeys.ContactType];
                            }
                            if (feedbackfields[JsonKeys.NumberOfMutualFriend] != null)
                            {
                                userBasicInfo.NumberOfMutualFriends = (int)feedbackfields[JsonKeys.NumberOfMutualFriend];
                            }
                            if (feedbackfields[JsonKeys.UpdateTime] != null)
                            {
                                userBasicInfo.UpdateTime = (long)feedbackfields[JsonKeys.UpdateTime];
                            }
                            if (feedbackfields[JsonKeys.CallAccess] != null)
                            {
                                userBasicInfo.CallAccess = (int)feedbackfields[JsonKeys.CallAccess];
                            }
                            if (feedbackfields[JsonKeys.ChatAccess] != null)
                            {
                                userBasicInfo.ChatAccess = (int)feedbackfields[JsonKeys.ChatAccess];
                            }
                            if (feedbackfields[JsonKeys.FeedAccess] != null)
                            {
                                userBasicInfo.FeedAccess = (int)feedbackfields[JsonKeys.FeedAccess];
                            }

                            userBasicInfo.ContactAddedTime = Models.Utility.ModelUtility.CurrentTimeMillis();
                            userBasicInfo.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;
                            userBasicInfo.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
                            userBasicInfo.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
                            userBasicInfo.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;

                            FriendListLoadUtility.AddInfoInModel(_model, userBasicInfo);
                            //FriendListLoadUtility.InsertSingleFriendToUILists(userBasicInfo);
                            if (_model.FriendListType < 1)
                            {
                                _model.FriendListType = AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
                                FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, _model, _model.FriendListType);
                            }
                            else
                            {
                                FriendListLoadUtility.MoveAFriendOneToAnotherList(userBasicInfo);
                            }

                            MainSwitcher.AuthSignalHandler().contactListHandler.ReloadFriendsMutualFriendsProfile();

                            List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
                            userBasicInfoList.Add(userBasicInfo);
                            new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();

                            BlockedNonFriendModel model = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(userBasicInfo.UserTableID);
                            model.IsBlockedByMe = false;
                            model.IsBlockedByFriend = false;
                            BlockedNonFriendDAO.SaveBlockedNonFriendFromThread(userBasicInfo.UserTableID, model.IsBlockedByMe, model.IsBlockedByFriend);

                            //MainSwitcher.AuthSignalHandler().contactListHandler.UI_AcceptFriendRequest(userBasicInfo);
                        }
                    }
                    else if (feedbackfields[JsonKeys.ReasonCode] != null)
                    {
                        Application.Current.Dispatcher.Invoke(delegate
                        {
                            try
                            {
                                string msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
                                if (msg.Equals(ReasonCodeConstants.REASON_MSG_ALREADY_FRIEND))
                                {
                                    if (userBasicInfo != null)
                                    {
                                        userBasicInfo.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_ACCEPTED;
                                        userBasicInfo.ContactAddedTime = Models.Utility.ModelUtility.CurrentTimeMillis();
                                        userBasicInfo.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;
                                        FriendListLoadUtility.AddInfoInModel(_model, userBasicInfo);
                                        //MessageBoxResult result = CustomMessageBox.ShowError(userBasicInfo.FullName + ReasonCodeConstants.REASON_MSG_ALREADY_FRIEND);
                                        //if (result.Equals(MessageBoxResult.OK))
                                        //{
                                        bool isTrue = UIHelperMethods.ShowQuestion(userBasicInfo.FullName + ReasonCodeConstants.REASON_MSG_ALREADY_FRIEND, "Accept confirmation");
                                        if (isTrue)
                                        {
                                            new ThradFriendDetailsInfoRequest().StartThread(userBasicInfo.UserTableID);
                                            FriendListLoadUtility.AddOrMoveAFriendOneToAnotherList(userBasicInfo, _model, AddRemoveInCollections.Dictonary_NonFavouriteFriendList);
                                        }

                                    }
                                }
                                else
                                {
                                    //MessageBoxResult result = CustomMessageBox.ShowError(msg);
                                    //if (result.Equals(MessageBoxResult.OK) && !msg.Equals(ReasonCodeConstants.REASON_MSG_USER_HAS_TOO_MANY_FRNDS))
                                    //{
                                    bool isTrue = UIHelperMethods.ShowOKCancel(msg, "Accept confirmation");
                                    if (isTrue && !msg.Equals(ReasonCodeConstants.REASON_MSG_USER_HAS_TOO_MANY_FRNDS))
                                    {
                                        if (userBasicInfo != null)
                                        {
                                            userBasicInfo.FriendShipStatus = 0;
                                            FriendListLoadUtility.AddInfoInModel(_model, userBasicInfo);
                                            if (_model.FriendListType > 0)
                                            {
                                                FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, _model, false);
                                            }
                                            new Models.DAO.DeleteFromContactListTable(userBasicInfo.UserTableID);
                                        }
                                    }
                                }

                            }
                            catch (Exception)
                            {
                            }
                            userBasicInfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(_model.ShortInfoModel.UserTableID);
                            //    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(_model.ShortInfoModel.UserTableID, out userBasicInfo);
                        }, System.Windows.Threading.DispatcherPriority.Send);
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
                _model.VisibilityModel.ShowActionButton = true;
                _model.VisibilityModel.ShowLoading = Visibility.Hidden;
                FriendListController.Instance.FriendDataContainer.TotalFriends();
            }
            catch (Exception)
            {
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(UserBasicInfoModel _model)
        {
            if (!running)
            {
                this._model = _model;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
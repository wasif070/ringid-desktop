﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.UI.AddFriend;
using View.ViewModel;

namespace View.Utility.FriendList
{
    class PendingListLoadUtility
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PendingListLoadUtility).Name);
        private static PendingListLoadUtility _Instance;
        public PendingListLoadUtility()
        {
            _Instance = this;
        }
        //public static PendingListLoadUtility Instance
        //{
        //    get
        //    {
        //        if (_Instance == null)
        //        {
        //            _Instance = new PendingListLoadUtility();
        //        }
        //        return _Instance;
        //    }
        //}

        public void LoadPendingList(int incomingIndex, int outgoingIndex, bool isNeedToLoadIncoming = false, bool isNeedToLoadOutgoing = false)
        {
            //try
            //{
            //    if (isNeedToLoadIncoming && RingIDViewModel.Instance.IncomingRequestFriendList.Count > 0)
            //    {
            //        lock (UCAddFriendPending.Instance.PendingListIncoming)
            //        {
            //            for (int idx = incomingIndex; idx < incomingIndex + 10; idx++)
            //            {
            //                if (UCAddFriendPending.Instance.PendingListIncoming.Count == RingIDViewModel.Instance.IncomingRequestFriendList.Count || idx > RingIDViewModel.Instance.IncomingRequestFriendList.Count)
            //                {
            //                    UCAddFriendPending.Instance.HideShowMorePanel(true);
            //                    break;
            //                }
            //                UserBasicInfoModel model = RingIDViewModel.Instance.IncomingRequestFriendList.ElementAt(idx);
            //                if (!checkIfAnyIncomingFriendAlreadyExists(model))
            //                {
            //                    UCAddFriendPending.Instance.PendingListIncoming.Add(model);
            //                }
            //            }
            //        }
            //        if (UCAddFriendPending.Instance.PendingListIncoming.Count == RingIDViewModel.Instance.IncomingRequestFriendList.Count)
            //        {
            //            UCAddFriendPending.Instance.HideShowMorePanel(true);
            //        }
            //        else if (UCAddFriendPending.Instance.PendingListIncoming.Count >= 10 && RingIDViewModel.Instance.IncomingRequestFriendList.Count > UCAddFriendPending.Instance.PendingListIncoming.Count)
            //        {
            //            UCAddFriendPending.Instance.ShowIncomingLoader(false);
            //        }
            //    }

            //    if (isNeedToLoadOutgoing && RingIDViewModel.Instance.OutgoingRequestFriendList.Count > 0)
            //    {
            //        lock (UCAddFriendPending.Instance.PendingListOutgoing)
            //        {
            //            for (int idx = outgoingIndex; idx < outgoingIndex + 10; idx++)
            //            {
            //                if (UCAddFriendPending.Instance.PendingListOutgoing.Count == RingIDViewModel.Instance.OutgoingRequestFriendList.Count || idx > RingIDViewModel.Instance.OutgoingRequestFriendList.Count)
            //                {
            //                    UCAddFriendPending.Instance.HideShowMorePanel(false);
            //                    break;
            //                }
            //                UserBasicInfoModel model = RingIDViewModel.Instance.OutgoingRequestFriendList.ElementAt(idx);
            //                if (!checkIfAnyOutgoingFriendAlreadyExists(model))
            //                {
            //                    UCAddFriendPending.Instance.PendingListOutgoing.Add(model);
            //                }
            //            }
            //        }

            //        if (UCAddFriendPending.Instance.PendingListOutgoing.Count == RingIDViewModel.Instance.OutgoingRequestFriendList.Count)
            //        {
            //            UCAddFriendPending.Instance.HideShowMorePanel(false);
            //        }
            //        else if (UCAddFriendPending.Instance.PendingListOutgoing.Count >= 10 && RingIDViewModel.Instance.OutgoingRequestFriendList.Count > UCAddFriendPending.Instance.PendingListOutgoing.Count)
            //        {
            //            UCAddFriendPending.Instance.ShowOutgoingLoader(false);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    log.Error(ex.Message + "\n" + ex.StackTrace + " " + ex.Message);
            //}
        }

        private bool checkIfAnyIncomingFriendAlreadyExists(UserBasicInfoModel model)
        {
            return UCAddFriendPending.Instance.PendingListIncoming.Any(P => P.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID);
        }

        private bool checkIfAnyOutgoingFriendAlreadyExists(UserBasicInfoModel model)
        {
            return UCAddFriendPending.Instance.PendingListOutgoing.Any(P => P.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID);
        }
    }
}
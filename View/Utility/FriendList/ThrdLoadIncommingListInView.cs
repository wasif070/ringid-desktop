﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Models.Entity;
using View.BindingModels;
using View.UI.AddFriend;

namespace View.Utility.FriendList
{
    class ThrdLoadIncommingListInView
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdLoadIncommingListInView).Name);
        private static bool running = false;
        private int numberOfFriendNeedToLoad = 10;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThrdLoadIncommingListInView() { }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                if (UCAddFriendPending.Instance != null)
                {
                    running = true;
                    if (UCAddFriendPending.Instance.PendingListIncoming.Count < FriendListController.Instance.FriendDataContainer.IncommingListCount())
                    {
                        FriendListController.Instance.FriendDataContainer.SortIncomingList();
                        int totalFrinds = FriendListController.Instance.FriendDataContainer.IncommingListCount();
                        int friendInview = UCAddFriendPending.Instance.PendingListIncoming.Count;
                        int loop = totalFrinds - friendInview;
                        if (loop > 0)
                        {
                            if (loop > numberOfFriendNeedToLoad)
                            {
                                loop = numberOfFriendNeedToLoad;
                            }
                            for (int i = friendInview; i < friendInview + loop; i++)
                            {
                                try
                                {
                                    UserBasicInfoDTO dto = FriendListController.Instance.FriendDataContainer.IncomingList[i];
                                    UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(dto.UserTableID);
                                    if (!UCAddFriendPending.Instance.PendingListIncoming.Contains(model))
                                    {
                                        //UCAddFriendPending.Instance.AddIntoIncomingRequestFriendList(model);
                                        lock (UCAddFriendPending.Instance.PendingListIncoming)
                                        {
                                            Application.Current.Dispatcher.Invoke(() =>
                                            {
                                                UCAddFriendPending.Instance.PendingListIncoming.Add(model);
                                            }, System.Windows.Threading.DispatcherPriority.Send);
                                        }
                                        System.Threading.Thread.Sleep(10);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    log.Error(ex.StackTrace);
                                }
                            }
                            if (UCAddFriendPending.Instance.PendingListIncoming.Count == totalFrinds)
                            {
                                UCAddFriendPending.Instance.HideShowMorePanel(true);
                            }
                            else
                            {
                                UCAddFriendPending.Instance.ShowMorePanel(true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
            finally
            {
                running = false;
            }
        }

        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(int numberOfFreindNeedToLoad)
        {
            if (!running)
            {
                this.numberOfFriendNeedToLoad = numberOfFreindNeedToLoad;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public static bool IsRunnig()
        {
            return running;
        }
        #endregion "Public methods"
    }
}

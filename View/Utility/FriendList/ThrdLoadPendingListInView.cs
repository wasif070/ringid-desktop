﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Models.Entity;
using View.BindingModels;
using View.UI.AddFriend;

namespace View.Utility.FriendList
{
    class ThrdLoadPendingListInView
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdLoadPendingListInView).Name);
        private bool running = false;
        int numberOfFriendNeedToLoad;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThrdLoadPendingListInView() { }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                if (UCAddFriendPending.Instance != null)
                {
                    if (UCAddFriendPending.Instance.PendingListOutgoing.Count < FriendListController.Instance.FriendDataContainer.PendingListcount())
                    {
                        FriendListController.Instance.FriendDataContainer.SortPendingList();
                        int totalFrinds = FriendListController.Instance.FriendDataContainer.PendingListcount();
                        int friendInview = UCAddFriendPending.Instance.PendingListOutgoing.Count;
                        //int loop = 10;
                        //if (FriendListController.Instance.FriendDataContainer.PendingListcount() < loop || friendInview + loop > FriendListController.Instance.FriendDataContainer.PendingListcount())
                        //{
                        //    loop = FriendListController.Instance.FriendDataContainer.PendingListcount();
                        //}
                        int loop = totalFrinds - friendInview;
                        if (loop > 0)
                        {
                            if (loop > numberOfFriendNeedToLoad)
                            {
                                loop = numberOfFriendNeedToLoad;
                            }
                            if (UCAddFriendPending.Instance != null)
                            {
                                for (int i = friendInview; i < friendInview + loop; i++)
                                {
                                    try
                                    {
                                        UserBasicInfoDTO dto = FriendListController.Instance.FriendDataContainer.PendingList[i];
                                        UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(dto.UserTableID);

                                        if (!UCAddFriendPending.Instance.PendingListOutgoing.Contains(model))
                                        {
                                            //UCAddFriendPending.Instance.AddIntoOutgoingRequestFriendList(model);
                                            lock (UCAddFriendPending.Instance.PendingListOutgoing)
                                            {
                                                Application.Current.Dispatcher.Invoke(() =>
                                                {
                                                    UCAddFriendPending.Instance.PendingListOutgoing.Add(model);
                                                }, System.Windows.Threading.DispatcherPriority.Send);
                                            }
                                            System.Threading.Thread.Sleep(10);
                                        }
                                        //if (UCAddFriendPending.Instance != null)
                                        //{
                                        //    UCAddFriendPending.Instance.PendingListOutgoing.Add(model);
                                        //}
                                        //System.Threading.Thread.Sleep(10);
                                        //if (!RingIDViewModel.Instance.OutgoingRequestFriendList.Contains(model))
                                        //{
                                        //    AddRemoveInCollections.AddIntoOutgoingRequestFriendList(model);

                                        //}
                                    }
                                    catch (Exception ex)
                                    {
                                        log.Error(ex.StackTrace);
                                    }
                                }
                                if (UCAddFriendPending.Instance.PendingListOutgoing.Count == totalFrinds)
                                {
                                    UCAddFriendPending.Instance.HideShowMorePanel(false);
                                }
                                else
                                {
                                    UCAddFriendPending.Instance.ShowMorePanel(false);
                                }
                                //else if (UCAddFriendPending.Instance.PendingListOutgoing.Count >= numberOfFriendNeedToLoad && totalFrinds > UCAddFriendPending.Instance.PendingListOutgoing.Count)
                                //{
                                //    UCAddFriendPending.Instance.ShowOutgoingLoader(false);
                                //}
                            }

                        }
                    }

                }
                //FriendListController.Instance.FriendDataContainer.TotalFriends();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
            finally
            {
                running = false;
            }
        }

        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(int numberOfFriendNeedToLoad)
        {
            if (!running)
            {
                this.numberOfFriendNeedToLoad = numberOfFriendNeedToLoad;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        #endregion "Public methods"
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using View.BindingModels;
using View.UI.AddFriend;
using View.ViewModel;

namespace View.Utility.FriendList
{
    class FriendListLoadUtility
    {
        #region "Private Methods"
        private static readonly ILog log = LogManager.GetLogger(typeof(FriendListLoadUtility).Name);
        private static readonly object _syncRoot = new object();
        #endregion "Private Methods"

        #region "Public Methods"

        public static void LoadDataFromServer(List<UserBasicInfoDTO> list)
        {
            lock (_syncRoot)
            {
                if (list != null && list.Count > 0)
                {
                    FriendListController.Instance.StartThrdHandleFriendFromServer();
                    RingIDViewModel.Instance.WinDataModel.IsLoadingCotactList = true;
                    foreach (UserBasicInfoDTO userDTO in list)
                    {
                        UserBasicInfoModel userbasicinforModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                        if (userbasicinforModel == null)
                        {
                            userbasicinforModel = FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModelsByDto(userDTO);
                            if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                            {
                                if (userDTO.ContactType == SettingsConstants.SPECIAL_CONTACT) AddRemoveInCollections.AddIntoSpecialFriendList(userbasicinforModel);
                                else
                                {
                                    if (userDTO.ContactAddedTime > (Models.Utility.ModelUtility.CurrentTimeMillis() - Models.Utility.ModelUtility.MilliSecondsInSevenDay))
                                        AddRemoveInCollections.AddIntoRecentlyAddedFriendList(userbasicinforModel);
                                    if (ThreadContactUtIdsRequest._TotalRecords > 15
                                        && RingIDViewModel.Instance.NonFavouriteFriendList.Count <= 15
                                        && !RingIDViewModel.Instance.NonFavouriteFriendList.Contains(userbasicinforModel))
                                    {
                                        if (!RingIDViewModel.Instance.NonFavouriteFriendList.Contains(userbasicinforModel))
                                        {
                                            userbasicinforModel.FriendListType = AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
                                            AddRemoveInCollections.AddIntoNonFavouriteFriendList(userbasicinforModel);
                                        }
                                    }
                                }
                            }
                        }
                        else userbasicinforModel.LoadData(userDTO);
                        if (!FriendListController.Instance.FriendDataContainer.TempFriendQueue.Any(P => P.UserTableID == userDTO.UserTableID))
                        {
                            FriendListController.Instance.FriendDataContainer.TempFriendQueue.Enqueue(userDTO);
                        }
                    }
                }
            }
        }

        public static void AddInfoInModel(UserBasicInfoModel model, UserBasicInfoDTO user)
        {
            model.LoadData(user);
            model.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
            model.OnPropertyChanged("CurrentInstance");
        }

        public static int CheckFriendType(UserBasicInfoDTO userDTO)
        {
            if (userDTO.ContactType == SettingsConstants.SPECIAL_CONTACT) return AddRemoveInCollections.Dictonary_SpecialFriendList;
            else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
            {
                if (userDTO.FavoriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE) return AddRemoveInCollections.Dictonary_FavouriteFriendList;
                else if (userDTO.FavoriteRank < SettingsConstants.FORCE_FAVOURITE_VALUE
                   && (userDTO.NumberOfCalls > 0 || userDTO.NumberOfChats > 0))
                    return AddRemoveInCollections.Dictonary_TopFriendList;
                else return AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
            }
            else
            {
                if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING) return AddRemoveInCollections.Dictonary_IncomingRequestFriendList;
                else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING) return AddRemoveInCollections.Dictonary_OutgoingRequestFriendList;
                else if (userDTO.FriendShipStatus == 0) return AddRemoveInCollections.Dictonary_UnknownFriendList;
            }
            return -1;
        }

        public static void RemoveAModelFromUI(UserBasicInfoDTO userDTO, UserBasicInfoModel userModel, bool deleteFromSuggestion)
        {
            try
            {
                if (userModel == null) userModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                if (userModel != null)
                {
                    int friendType = userModel.FriendListType;
                    if (friendType > 0)
                    {
                        switch (friendType)
                        {
                            case AddRemoveInCollections.Dictonary_TopFriendList:
                                if (RingIDViewModel.Instance.TopFriendList.Where(P => P.ShortInfoModel.UserTableID == userModel.ShortInfoModel.UserTableID).FirstOrDefault() != null)
                                    AddRemoveInCollections.RemoveFromTopFriendList(userModel);
                                break;
                            case AddRemoveInCollections.Dictonary_NonFavouriteFriendList:
                                FriendListController.Instance.FriendDataContainer.RemoveFromNonFavoriteList(userModel.ShortInfoModel.UserTableID);
                                List<UserBasicInfoModel> list = RingIDViewModel.Instance.NonFavouriteFriendList.Where(P => P.ShortInfoModel.UserTableID == userModel.ShortInfoModel.UserTableID).ToList();
                                if (RingIDViewModel.Instance.NonFavouriteFriendList.Where(P => P.ShortInfoModel.UserTableID == userModel.ShortInfoModel.UserTableID).FirstOrDefault() != null)
                                    AddRemoveInCollections.RemoveFromNonFavouriteFriendList(userModel);
                                break;
                            case AddRemoveInCollections.Dictonary_FavouriteFriendList:
                                if (RingIDViewModel.Instance.FavouriteFriendList.Where(P => P.ShortInfoModel.UserTableID == userModel.ShortInfoModel.UserTableID).FirstOrDefault() != null)
                                    AddRemoveInCollections.RemoveFromFavouriteFriendList(userModel);
                                break;
                            case AddRemoveInCollections.Dictonary_OutgoingRequestFriendList:
                                FriendListController.Instance.FriendDataContainer.RemoveFromPendingList(userModel.ShortInfoModel.UserTableID);
                                if (UCAddFriendPending.Instance != null && UCAddFriendPending.Instance.PendingListOutgoing.Where(P => P.ShortInfoModel.UserTableID == userModel.ShortInfoModel.UserTableID).FirstOrDefault() != null) UCAddFriendPending.Instance.RemoveFromOutgoingRequestFriendList(userModel);
                                break;
                            case AddRemoveInCollections.Dictonary_IncomingRequestFriendList:
                                FriendListController.Instance.FriendDataContainer.RemoveFromIncomingList(userModel.ShortInfoModel.UserTableID);
                                AppConstants.ADD_FRIEND_NOTIFICATION_COUNT--;
                                FriendListLoadUtility.AddFriendsNotificationForIncomingRequest();
                                if (UCAddFriendPending.Instance != null && UCAddFriendPending.Instance.PendingListIncoming.Where(P => P.ShortInfoModel.UserTableID == userModel.ShortInfoModel.UserTableID).FirstOrDefault() != null) UCAddFriendPending.Instance.RemoveFromIncomingRequestFriendList(userModel);
                                break;
                        }
                        if (userModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_UNKNOWN || friendType != AddRemoveInCollections.Dictonary_FavouriteFriendList)
                            AddRemoveInCollections.RemoveFromRecentlyAddedFriendList(userModel);
                    }
                    if (deleteFromSuggestion) AddRemoveInCollections.RemoveFromSuggestionFriendList(userModel);
                }
            }
            catch (Exception) { }
        }

        public static void AddAModelIntoUI(UserBasicInfoDTO userDtoToInsert, UserBasicInfoModel userModel, int friendTypeToAdd = -1)
        {
            try
            {
                if (userDtoToInsert != null)
                {
                    if (userModel == null) userModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDtoToInsert.UserTableID);
                    if (userModel == null)
                    {
                        userModel = new UserBasicInfoModel(userDtoToInsert);
                        FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(userModel);
                    }
                    if (friendTypeToAdd < 0) friendTypeToAdd = CheckFriendType(userDtoToInsert);
                    if (userDtoToInsert.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_UNKNOWN) userModel.FriendListType = AddRemoveInCollections.Dictonary_UnknownFriendList;
                    else
                    {
                        switch (friendTypeToAdd)
                        {
                            case AddRemoveInCollections.Dictonary_TopFriendList:
                                userModel.FriendListType = AddRemoveInCollections.Dictonary_TopFriendList;
                                AddRemoveInCollections.AddIntoTopFriendList(userModel);
                                break;
                            case AddRemoveInCollections.Dictonary_NonFavouriteFriendList:
                                if (!RingIDViewModel.Instance.NonFavouriteFriendList.Contains(userModel))
                                {
                                    userModel.FriendListType = AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
                                    if (!FriendListController.Instance.FriendDataContainer.NonFavoriteList.Any(P => P.UserTableID == userDtoToInsert.UserTableID))
                                    {
                                        FriendListController.Instance.FriendDataContainer.AddIntoNonFavoriteList(userDtoToInsert);
                                        FriendListController.Instance.FriendDataContainer.SortNonFavList();
                                    }
                                    var index = FriendListController.Instance.FriendDataContainer.NonFavoriteList.FindIndex(a => a.UserTableID == userDtoToInsert.UserTableID);
                                    if (index <= RingIDViewModel.Instance.NonFavouriteFriendList.Count) AddRemoveInCollections.AddIntoNonFavouriteFriendList(userModel);
                                }
                                break;
                            case AddRemoveInCollections.Dictonary_FavouriteFriendList:
                                AddRemoveInCollections.AddIntoFavouriteFriendList(userModel);
                                break;
                            case AddRemoveInCollections.Dictonary_OutgoingRequestFriendList:
                                if (userDtoToInsert.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
                                {
                                    userModel.FriendListType = AddRemoveInCollections.Dictonary_OutgoingRequestFriendList;
                                    if (!FriendListController.Instance.FriendDataContainer.PendingList.Contains(userDtoToInsert)) FriendListController.Instance.FriendDataContainer.AddIntoPendingList(userDtoToInsert);
                                    if (UCAddFriendPending.Instance != null && !UCAddFriendPending.Instance.PendingListOutgoing.Contains(userModel)) UCAddFriendPending.Instance.InvokeInsertIPendingListOutgoing(0, userModel);
                                }
                                break;
                            case AddRemoveInCollections.Dictonary_IncomingRequestFriendList:
                                if (userDtoToInsert.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
                                {
                                    userModel.FriendListType = AddRemoveInCollections.Dictonary_IncomingRequestFriendList;
                                    if (!FriendListController.Instance.FriendDataContainer.IncomingList.Contains(userDtoToInsert)) FriendListController.Instance.FriendDataContainer.AddIntoIncomingList(userDtoToInsert);
                                    if (UCAddFriendPending.Instance != null && !UCAddFriendPending.Instance.PendingListIncoming.Contains(userModel)) UCAddFriendPending.Instance.InvokeInsertIncomingRequestFriendList(0, userModel);
                                }
                                break;
                        }
                        if ((friendTypeToAdd == AddRemoveInCollections.Dictonary_NonFavouriteFriendList || friendTypeToAdd == AddRemoveInCollections.Dictonary_FavouriteFriendList)
                            && userModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED
                            && userDtoToInsert.ContactType != SettingsConstants.SPECIAL_CONTACT
                            && userDtoToInsert.ContactAddedTime > (Models.Utility.ModelUtility.CurrentTimeMillis() - Models.Utility.ModelUtility.MilliSecondsInSevenDay))
                            AddRemoveInCollections.AddIntoRecentlyAddedFriendList(userModel);
                    }
                }
            }
            catch (Exception ex) { log.Error(ex.StackTrace); }
        }

        public static void MoveAFriendOneToAnotherList(UserBasicInfoDTO userDTO)
        {
            UserBasicInfoModel userModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
            if (userModel != null)
            {
                RemoveAModelFromUI(userDTO, userModel, true);
                AddInfoInModel(userModel, userDTO);
                AddAModelIntoUI(userDTO, userModel);
            }
            else
            {
                userModel = new UserBasicInfoModel(userDTO);
                AddAModelIntoUI(userDTO, userModel);
            }
        }

        public static void AddOrMoveAFriendOneToAnotherList(UserBasicInfoDTO userDTO, UserBasicInfoModel userModel, int targetDictionary)
        {
            if (userModel == null) userModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
            if (userModel != null)
            {
                if (userModel.FriendListType < 1)
                {
                    userModel.FriendListType = targetDictionary;
                    FriendListLoadUtility.AddAModelIntoUI(userDTO, userModel, userModel.FriendListType);
                }
                else FriendListLoadUtility.MoveAFriendOneToAnotherList(userDTO);
            }
            else
            {
                userModel = new UserBasicInfoModel(userDTO);
                AddAModelIntoUI(userDTO, userModel);
            }
        }

        public static void FavoriteTopNonFavoriteListChange(UserBasicInfoDTO user)
        {
            ThrdRepositionAFriend thrdRepositionAFriend = new ThrdRepositionAFriend();
            thrdRepositionAFriend.StartThread(user);
        }

        public static int FriendType(long friendUserTableId)
        {
            UserBasicInfoModel mdl = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(friendUserTableId);
            if (mdl != null) return mdl.FriendListType;
            return 0;
        }

        public static bool CheckIfMatch(string previousString, UserBasicInfoModel model, int searchBy)
        {
            try
            {
                string searchText = previousString.ToLower();
                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    if (searchBy == StatusConstants.SEARCH_BY_RINGID)
                    {
                        if (model.ShortInfoModel.UserIdentity > 0)
                        {
                            string ringID = model.ShortInfoModel.UserIdentity.ToString().Substring(2).Trim();
                            if (ringID.Contains(searchText)) return true;
                        }
                    }
                    else if (searchBy == StatusConstants.SEARCH_BY_NAME)
                    {
                        if (!string.IsNullOrWhiteSpace(model.ShortInfoModel.FullName))
                        {
                            string fullName = model.ShortInfoModel.FullName.ToLower().Trim();
                            if (fullName.Contains(searchText)) return true;
                        }
                    }
                    else if (searchBy == StatusConstants.SEARCH_BY_ALL)
                    {
                        if (model.ShortInfoModel.UserIdentity > 0)
                        {
                            string ringID = model.ShortInfoModel.UserIdentity.ToString().Trim();
                            if (ringID.Contains(searchText)) return true;
                        }
                        if (!string.IsNullOrWhiteSpace(model.ShortInfoModel.FullName))
                        {
                            string fullName = model.ShortInfoModel.FullName.ToLower().Trim();
                            if (fullName.Contains(searchText)) return true;
                        }
                        if (!string.IsNullOrWhiteSpace(model.MobilePhoneDialingCode) && !string.IsNullOrWhiteSpace(model.MobilePhone))
                        {
                            string phone = model.MobilePhoneDialingCode.Trim() + " " + model.MobilePhone.Trim();
                            if (phone.Contains(searchText)) return true;
                        }
                        if (!string.IsNullOrWhiteSpace(model.Email))
                        {
                            string email = model.Email.Trim();
                            if (email.Contains(searchText)) return true;
                        }
                        if (!string.IsNullOrEmpty(model.HomeCity) && !string.IsNullOrEmpty(model.CurrentCity))
                        {
                            string location = model.HomeCity.ToLower().Trim() + model.CurrentCity.ToLower().Trim();
                            if (location.Contains(searchText)) return true;
                        }
                    }
                }
            }
            catch (Exception ex) { log.Error(ex.StackTrace + " " + ex.Message); }
            return false;
        }

        public static void AddFriendsNotificationForIncomingRequest()
        {
            try
            {
                if (AppConstants.ADD_FRIEND_NOTIFICATION_COUNT < 0) AppConstants.ADD_FRIEND_NOTIFICATION_COUNT = 0;
                else if (AppConstants.ADD_FRIEND_NOTIFICATION_COUNT > FriendListController.Instance.FriendDataContainer.IncommingListCount())
                    AppConstants.ADD_FRIEND_NOTIFICATION_COUNT = FriendListController.Instance.FriendDataContainer.IncommingListCount();
                if (RingIDViewModel.Instance.AddFriendsNotificationCounter != AppConstants.ADD_FRIEND_NOTIFICATION_COUNT)
                {
                    RingIDViewModel.Instance.AddFriendsNotificationCounter = AppConstants.ADD_FRIEND_NOTIFICATION_COUNT;
                    RingIDViewModel.Instance.OnPropertyChanged("AddFriendsNotificationCounter");
                    RingIDViewModel.Instance.AddFriendsNotificationCounterSize = HelperMethods.CalculateCircleSizeOfAddFriendNotifactionCounter(RingIDViewModel.Instance.AddFriendsNotificationCounter);
                    new InsertIntoNotificationCountTable();
                }
            }
            catch (Exception e) { log.Error("Exception in AuthHandler ==>" + e.StackTrace); }
        }

        public static void ClearAddFriendsNotificationCount()
        {
            try
            {
                if (AppConstants.ADD_FRIEND_NOTIFICATION_COUNT > 0)
                {
                    AppConstants.ADD_FRIEND_NOTIFICATION_COUNT = 0;
                    RingIDViewModel.Instance.AddFriendsNotificationCounter = AppConstants.ADD_FRIEND_NOTIFICATION_COUNT;
                    RingIDViewModel.Instance.OnPropertyChanged("AddFriendsNotificationCounter");
                    RingIDViewModel.Instance.AddFriendsNotificationCounterSize = HelperMethods.CalculateCircleSizeOfAddFriendNotifactionCounter(RingIDViewModel.Instance.AddFriendsNotificationCounter);
                    new InsertIntoNotificationCountTable();
                }
            }
            catch (Exception e) { log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message); }
        }

        public static List<Models.Entity.UserBasicInfoDTO> SortFriendListByFriendshipStatusAccepted(List<Models.Entity.UserBasicInfoDTO> list)
        {
            List<Models.Entity.UserBasicInfoDTO> sortedList = new List<Models.Entity.UserBasicInfoDTO>();
            foreach (Models.Entity.UserBasicInfoDTO userDTO in list)
            {
                if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED) sortedList.Insert(0, userDTO);
                else sortedList.Add(userDTO);
            }
            return sortedList;
        }

        public static double GetFavoriteValue(Models.Entity.UserBasicInfoDTO user)
        {
            double val;
            val = (user.MatchedBy * 0.1)
                    + (user.ContactType * 0.1)
                    + (user.NumberOfMutualFriends * 0.0001)
                    + (user.NumberOfChats * 0.25)
                    + (user.NumberOfCalls * 0.25);
            return val;
        }

        #endregion "Public Methods"
    }
}

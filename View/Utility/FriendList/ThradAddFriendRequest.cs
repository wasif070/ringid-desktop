﻿using System;
using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.Utility.FriendProfile;

namespace View.Utility.FriendList
{
    public class ThradAddFriendRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradAddFriendRequest).Name);
        private bool running = false;
        private long _UserTableID;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                UserBasicInfoDTO userBasicInfo = null;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.UserTableID] = _UserTableID.ToString();
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_FRIEND;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    try
                    {
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            userBasicInfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(_UserTableID);
                            //   FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(_UserTableID, out userBasicInfo);
                            HelperMethodsModel.BindAddFriendDetails(feedbackfields, userBasicInfo);
                            //   HelperMethodsAuth.AuthHandlerInstance.CheckFriendPresence(userBasicInfo.UserIdentity);
                            MainSwitcher.AuthSignalHandler().profileSignalHandler.CheckFriendPresence(userBasicInfo.UserTableID);
                            FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(userBasicInfo.UserTableID);
                            FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userBasicInfo.UserTableID);
                        }
                        else
                        {
                            if (feedbackfields[JsonKeys.ReasonCode] != null)
                            {
                                if (ReasonCodeConstants.REASON_CODE_ALREADY_FRIEND_REQUESTED == (int)feedbackfields[JsonKeys.ReasonCode])
                                {
                                    ThradUnknownProfileInfoRequest unknwnReq = new ThradUnknownProfileInfoRequest();
                                    unknwnReq.StartThread(_UserTableID, true);
                                }
                            }
                        }
                    }
                    finally { }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(long userTableID)
        {
            if (!running)
            {
                this._UserTableID = userTableID;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

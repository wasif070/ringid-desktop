﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using Auth.Service;
using Models.Stores;
using Models.DAO;
using Auth.utility;
using View.Utility.WPFMessageBox;
using View.UI.AddFriend;
using View.Utility.Auth;
using View.ViewModel;
using View.UI;

namespace View.Utility.FriendList
{
    class DeleteFriend
    {
        UserBasicInfoModel _UserbasicInfoModel;
        //private static Thread deleteFriend = null;
        public void StartProcess(UserBasicInfoModel userbasicInfoModel)
        {
            //if (deleteFriend == null || !deleteFriend.IsAlive)
            //{
            this._UserbasicInfoModel = userbasicInfoModel;
            Thread deleteFriend = new Thread(run);
            deleteFriend.Name = "DeleteFriend";
            deleteFriend.Start();
            //}
        }
        public void run()
        {
            try
            {
                UserBasicInfoDTO _dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(_UserbasicInfoModel.ShortInfoModel.UserTableID);
                List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
                if (_dto != null)
                {
                    bool isSuccess = false;
                    string msg = "";

                    _UserbasicInfoModel.VisibilityModel.ShowActionButton = false;
                    _UserbasicInfoModel.VisibilityModel.ShowLoading = Visibility.Visible;
                    _dto.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;
                    _dto.FriendListType = _UserbasicInfoModel.FriendListType;

                    JObject pakToSend = new JObject();
                    pakToSend[JsonKeys.UserTableID] = _UserbasicInfoModel.ShortInfoModel.UserTableID;
                    (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_DELETE_FRIEND, AppConstants.REQUEST_TYPE_UPDATE)).Run(out isSuccess, out msg);

                    if (isSuccess)
                    {
                        bool doBlock = _UserbasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && _dto.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED;
                        _dto.FriendShipStatus = 0;
                        _dto.ChatAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
                        _dto.CallAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
                        _dto.FeedAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
                        _dto.FriendListType = _UserbasicInfoModel.FriendListType;
                        /*Favorite Rank*/
                        if (_dto.FavoriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE)
                        {
                            _dto.FavoriteRank = 0;
                            ContactListDAO.UpdateContactRanking(_dto);
                        }

                        _UserbasicInfoModel.LoadData(_dto);
                        _UserbasicInfoModel.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
                        _UserbasicInfoModel.OnPropertyChanged("CurrentInstance");

                        userList.Add(_dto);
                        new InsertIntoUserBasicInfoTable(userList).Start();
                        MainSwitcher.AuthSignalHandler().profileSignalHandler.UI_ProcessDeleteFriend(_dto);

                        if (doBlock)
                        {
                            BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(_UserbasicInfoModel.ShortInfoModel.UserTableID);
                            blockModel.IsBlockedByMe = false;
                            HelperMethods.ChangeBlockUnblockSettingsWrapper(StatusConstants.FULL_ACCESS, _UserbasicInfoModel, blockModel);
                        }
                    }
                    else if (!string.IsNullOrWhiteSpace(msg))
                    {
                        Application.Current.Dispatcher.Invoke(delegate
                        {
                            // MessageBoxResult result = CustomMessageBox.ShowError(NotificationMessages.MSG_UNFRIEND_FAIL + msg);
                            //if (result == ConfirmationDialogResult.Yes)
                            //{
                            bool isTrue = UIHelperMethods.ShowQuestion(NotificationMessages.MSG_UNFRIEND_FAIL + msg, "This friend will no longer in your list.",  string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
                            if (isTrue)
                            {
                                bool doBlock = _UserbasicInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && _dto.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED;
                                _dto.FriendShipStatus = 0;
                                _dto.ChatAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
                                _dto.CallAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
                                _dto.FeedAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
                                _dto.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;

                                _UserbasicInfoModel.LoadData(_dto);
                                _UserbasicInfoModel.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
                                _UserbasicInfoModel.OnPropertyChanged("CurrentInstance");

                                FriendListLoadUtility.RemoveAModelFromUI(_dto, _UserbasicInfoModel, true);
                                new DeleteFromContactListTable(_UserbasicInfoModel.ShortInfoModel.UserTableID);

                                if (doBlock)
                                {
                                    BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(_UserbasicInfoModel.ShortInfoModel.UserTableID);
                                    blockModel.IsBlockedByMe = false;
                                    HelperMethods.ChangeBlockUnblockSettingsWrapper(StatusConstants.FULL_ACCESS, _UserbasicInfoModel, blockModel);
                                }
                            }
                        });
                    }
                    _UserbasicInfoModel.VisibilityModel.ShowActionButton = true;
                    _UserbasicInfoModel.VisibilityModel.ShowLoading = Visibility.Collapsed;
                    FriendListController.Instance.FriendDataContainer.TotalFriends();
                    if (UCAddFriendPending.Instance != null)
                    {
                        UCAddFriendPending.Instance.ShowIncommingAndPendingCounts();
                    }
                }
            }
            catch (Exception)
            {
            }
            /*finally
            {
                if (deleteFriend != null)
                {
                    deleteFriend = null;
                }
            }*/
        }
    }
}

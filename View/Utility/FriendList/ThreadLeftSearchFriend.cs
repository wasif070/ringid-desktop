﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;
using View.UI;
using View.ViewModel;

namespace View.Utility.FriendList
{
    public class ThreadLeftSearchFriend
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadLeftSearchFriend).Name);

        public static int searchingFrom;
        public static int searchBy = 0;
        public bool runningSearchFriendThread = false;
        public static string searchParm = null;
        public static bool SearchForShowMore = false;
        public static long totalSearchCount = 0;
        public static string prevString;
        public static bool processingList = false;
        public static int SearchFromLeftPanel = 2;
        public static long friendCountInOneRequest = 0;
        private Thread thread;

        private static int waiting = 0;
        private bool ContactListSearchComplete = false;
        private Int32 SLEEP_TIME = 10;//ms

        #region "Constructors"
        public ThreadLeftSearchFriend()
        {

        }
        public void StartThread(int searchFrom, int searchBy)
        {

            //if (!runningSearchFriendThread)
            //{
            //    if (thread == null)
            //    {
            //        thread = new Thread(Run);
            //    }
            //    runningSearchFriendThread = true;
            //    ThreadLeftSearchFriend.searchingFrom = searchFrom;
            //    ThreadLeftSearchFriend.searchBy = searchBy;
            //    thread.Abort();
            //    thread.Start();
            //}
            //else
            //{
            //    Console.WriteLine("Thread is allre**************************");
            //}
            //if (!runningSearchFriendThread)
            //{
            //   runningSearchFriendThread = true;
            //    Thread trd = new Thread(Run);
            //    trd.Name = "SearchFriend";
            //trd.IsBackground = true;
            //Prevent optimization from setting the field before calling Start
            // Thread.MemoryBarrier();
            //    trd.Start();
            //}


            var thread2 = thread;
            //Prevent optimization from not using the local variable
            Thread.MemoryBarrier();
            if
            (
                thread2 == null ||
                thread2.ThreadState == System.Threading.ThreadState.Stopped
            )
            {
                runningSearchFriendThread = true;
                var newThread = new Thread(Run);
                newThread.IsBackground = true;
                newThread.Name = "ThreadLeftSearchFriend";
                newThread.Start();
                //Prevent optimization from setting the field before calling Start
                Thread.MemoryBarrier();
                thread = newThread;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("******************myThreadis already Running.");
            }
        }

        public void StopThread()
        {
            runningSearchFriendThread = false;
        }

        #endregion "Constructors"

        #region Private methods
        private void ResetUIWhileStopThread()
        {
            if (UCGuiRingID.Instance.ucFriendSearchListPanel != null)
            {
                UCGuiRingID.Instance.ucFriendSearchListPanel.ShowSearchingLoader(false);
            }
        }

        private void StartSearchingLoader(bool needToSearchingLoader)
        {
            if (UCGuiRingID.Instance.ucFriendSearchListPanel != null && UCGuiRingID.Instance.ucFriendSearchListPanel.IsVisible)
            {
                UCGuiRingID.Instance.ucFriendSearchListPanel.ShowSearchingLoader(needToSearchingLoader);
            }
        }

        private void sendSearchRequest()
        {
            try
            {
                StartSearchingLoader(true);
                JObject packet = new JObject();
                String pakId = SendToServer.GetRanDomPacketID();
                packet[JsonKeys.PacketId] = pakId;
                RingDictionaries.Instance.SEARCHING_UI_NAME_DICTIONARY[StatusConstants.SEARCH_FRIENDLIST] = pakId;
                if (ThreadLeftSearchFriend.searchBy != StatusConstants.SEARCH_BY_RINGID)
                {
                    packet[JsonKeys.LogStartLimit] = ThreadLeftSearchFriend.totalSearchCount;//SearchCount();//RingIDViewModel.Instance.TempFriendList.Count
                }
                packet[JsonKeys.Action] = AppConstants.TYPE_CONTACT_SEARCH;
                packet[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                packet[JsonKeys.SearchParam] = searchParm;
                packet[JsonKeys.SearchCategory] = ThrdSearchFriend.searchBy;
                SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, packet.ToString());

                for (int pakSendingAttempt = 1; pakSendingAttempt <= DefaultSettings.TRYING_TIME; pakSendingAttempt++)
                {
                    if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) || !runningSearchFriendThread)
                    {
                        break;
                    }
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                    {
                        if (pakSendingAttempt % 9 == 0)
                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, packet.ToString());
                    }
                    else
                    {
                        JObject feedbackfields;
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                        break;
                    }
                    if (pakSendingAttempt == DefaultSettings.TRYING_TIME)
                    {
                        ResetUIWhileStopThread();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + " " + ex.Message);
            }
            finally
            {
                StartSearchingLoader(false);
                ResetUIWhileStopThread();
            }
        }
        private void addASleep()
        {
            System.Threading.Thread.Sleep(SLEEP_TIME);
        }

        private void AddIntoTempFriendSearchlistBySearchResult(int searchBy)
        {

            if (!ThreadLeftSearchFriend.processingList)
            {
                try
                {
                    ThreadLeftSearchFriend.processingList = true;

                    #region Commented Code
                    //if (searchingFrom == SearchFromLeftPanel)
                    //{

                    //    var numQuery1 =
                    //        from userModel in RingIDViewModel.Instance.FavouriteFriendList
                    //        where (userModel.ShortInfoModel.UserIdentity) == FriendListLoadUtility.GetUserIdentityBySearchParameter(prevString, userModel, searchBy)
                    //        select userModel;

                    //    foreach (UserBasicInfoModel model in numQuery1)
                    //    {
                    //        if ((prevString == null || !prevString.Equals(ThreadLeftSearchFriend.searchParm)) || RingIDViewModel.Instance.TempFriendSearchList.Count > 200)
                    //        {
                    //            break;
                    //        }
                    //        AddRemoveInCollections.AddIntoTempFriendSearchListWithChecking(model, model.ShortInfoModel.UserIdentity);
                    //        addASleep();
                    //    }
                    //    var numQuery2 =
                    //         from userModel in RingIDViewModel.Instance.TopFriendList
                    //         where (userModel.ShortInfoModel.UserIdentity) == FriendListLoadUtility.GetUserIdentityBySearchParameter(prevString, userModel, searchBy)
                    //         select userModel;
                    //    foreach (UserBasicInfoModel model in numQuery2)
                    //    {
                    //        if ((prevString == null || !prevString.Equals(ThreadLeftSearchFriend.searchParm)) || RingIDViewModel.Instance.TempFriendSearchList.Count > 200)
                    //        {
                    //            break;
                    //        }
                    //        AddRemoveInCollections.AddIntoTempFriendSearchListWithChecking(model, model.ShortInfoModel.UserIdentity);
                    //        addASleep();
                    //    }
                    //    var numQuery3 =
                    //        from userModel in RingIDViewModel.Instance.NonFavouriteFriendList
                    //        where (userModel.ShortInfoModel.UserIdentity) == FriendListLoadUtility.GetUserIdentityBySearchParameter(prevString, userModel, searchBy)
                    //        select userModel;
                    //    foreach (UserBasicInfoModel model in numQuery3)
                    //    {
                    //        if ((prevString == null || !prevString.Equals(ThreadLeftSearchFriend.searchParm)) || RingIDViewModel.Instance.TempFriendSearchList.Count > 200)
                    //        {
                    //            break;
                    //        }
                    //        AddRemoveInCollections.AddIntoTempFriendSearchListWithChecking(model, model.ShortInfoModel.UserIdentity);
                    //        addASleep();
                    //    }
                    //}
                    //var numQuery4 =
                    //       from userModel in RingIDViewModel.Instance.IncomingRequestFriendList
                    //       where (userModel.ShortInfoModel.UserIdentity) == FriendListLoadUtility.GetUserIdentityBySearchParameter(prevString, userModel, searchBy)
                    //       select userModel;
                    //foreach (UserBasicInfoModel model in numQuery4)
                    //{
                    //    if ((prevString == null || !prevString.Equals(ThreadLeftSearchFriend.searchParm)) || RingIDViewModel.Instance.TempFriendSearchList.Count > 200)
                    //    {
                    //        break;
                    //    }
                    //    AddRemoveInCollections.AddIntoTempFriendSearchListWithChecking(model, model.ShortInfoModel.UserIdentity);
                    //    addASleep();
                    //}
                    //var numQuery5 =
                    //     from userModel in RingIDViewModel.Instance.OutgoingRequestFriendList
                    //     where (userModel.ShortInfoModel.UserIdentity) == FriendListLoadUtility.GetUserIdentityBySearchParameter(prevString, userModel, searchBy)
                    //     select userModel;
                    //foreach (UserBasicInfoModel model in numQuery5)
                    //{
                    //    if ((prevString == null || !prevString.Equals(ThreadLeftSearchFriend.searchParm)) || RingIDViewModel.Instance.TempFriendSearchList.Count > 200)
                    //    {
                    //        break;
                    //    }
                    //    AddRemoveInCollections.AddIntoTempFriendSearchListWithChecking(model, model.ShortInfoModel.UserIdentity);
                    //    addASleep();
                    //}
                    //ContactListSearchComplete = true;
                    #endregion

                    List<UserBasicInfoModel> userList = FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values.Where(P => P.ShortInfoModel.FriendShipStatus != 0
                                                            && P.ShortInfoModel.ContactType != 3
                                                            && FriendListLoadUtility.CheckIfMatch(prevString, P, searchBy)).ToList().OrderBy(p => p.ShortInfoModel.FullName).ToList();

                    if (userList.Count > 0)
                    {
                        foreach (UserBasicInfoModel model in userList)
                        {
                            if ((prevString == null || !prevString.Equals(ThreadLeftSearchFriend.searchParm)) || RingIDViewModel.Instance.TempFriendSearchList.Count > 200)
                            {
                                break;
                            }
                            AddRemoveInCollections.AddIntoTempFriendSearchListWithChecking(model, model.ShortInfoModel.UserTableID);
                            addASleep();
                        }
                    }

                    ContactListSearchComplete = true;
                }
                catch (Exception ex)
                {
                    log.Error("Searchfreind==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);
                }
                finally
                {
                    ContactListSearchComplete = true;
                    ThreadLeftSearchFriend.processingList = false;
                }
            }
        }

        private void Run()
        {
            try
            {
                while (runningSearchFriendThread)
                {
                    if (ThreadLeftSearchFriend.searchParm != null && ThreadLeftSearchFriend.searchParm.Trim().Length > 0)
                    {
                        if (SearchForShowMore)
                        {
                            waiting = 0;
                            sendSearchRequest();
                        }
                        else if ((prevString == null || !prevString.Trim().Equals(ThreadLeftSearchFriend.searchParm.Trim())) && !SearchForShowMore)
                        {
                            waiting = 0;
                            //Thread.Sleep(500);
                            prevString = ThreadLeftSearchFriend.searchParm;
                            AddRemoveInCollections.ClearTempFriendSearchList();
                            ContactListSearchComplete = false;
                            AddIntoTempFriendSearchlistBySearchResult(ThreadLeftSearchFriend.searchBy);
                            if (!string.IsNullOrEmpty(ThreadLeftSearchFriend.searchParm) && ContactListSearchComplete && RingIDViewModel.Instance.TempFriendSearchList.Count <= 5)
                            {
                                /*if ( UCGuiRingID.Instance.ucFriendSearchListPanel != null && UCGuiRingID.Instance.ucFriendSearchListPanel.IsVisible)
                                    UCGuiRingID.Instance.ucFriendSearchListPanel.NoResultsFound = true;*/
                                sendSearchRequest();
                            }
                        }
                    }
                    else
                    {
                        prevString = null;
                        AddRemoveInCollections.ClearTempFriendSearchList();
                    }
                    if (waiting != 0 && waiting % 100 == 0)
                    {
                        break;
                    }
                    waiting++;
                    Thread.Sleep(400);
                }
            }
            catch (Exception ex)
            {
                log.Error("Searchfreind==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                ThreadLeftSearchFriend.searchParm = null;
                runningSearchFriendThread = false;
                SearchForShowMore = false;
                ResetUIWhileStopThread();
                prevString = null;
            }
        }
        #endregion Private methods
    }
}

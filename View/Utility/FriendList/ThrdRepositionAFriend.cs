﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models.Constants;
using Models.Entity;
using View.BindingModels;
using View.ViewModel;
using Models.DAO;

namespace View.Utility.FriendList
{
    class ThrdRepositionAFriend
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdRepositionAFriend).Name);
        private bool running = false;
        UserBasicInfoDTO userDtoToInsert;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThrdRepositionAFriend() { }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                ContactListDAO.UpdateContactRankingWithoutThread(userDtoToInsert);

                UserBasicInfoModel userModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDtoToInsert.UserTableID);
                int newFriendListType = FriendListLoadUtility.CheckFriendType(userDtoToInsert);

                if (userModel == null)
                {
                    userModel = new UserBasicInfoModel(userDtoToInsert);
                    userModel.FriendListType = newFriendListType;
                    FriendListController.Instance.FriendDataContainer.AddOrReplaceUserBasicInfoModels(userDtoToInsert.UserTableID, userModel);
                }
                else
                {
                    FriendListLoadUtility.RemoveAModelFromUI(userDtoToInsert, userModel, false);
                }
                userModel.FriendListType = newFriendListType;
                FriendListLoadUtility.AddAModelIntoUI(userDtoToInsert, userModel, userModel.FriendListType);
                //  FriendListLoadUtility.AddIntoACollectins(userModel);

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
            finally
            {
                //FriendListController.Instance.FriendDataContainer.TotalFriends();
                running = false;
            }
        }

        public void addInfoInModel(UserBasicInfoModel model, UserBasicInfoDTO user)
        {
            model.LoadData(user);
            model.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
            model.OnPropertyChanged("CurrentInstance");
        }

        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(UserBasicInfoDTO userDtoToInsert)
        {
            if (!running)
            {
                this.userDtoToInsert = userDtoToInsert;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        #endregion "Public methods"
    }
}

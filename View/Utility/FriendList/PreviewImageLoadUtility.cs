﻿using log4net;
using Models.Stores;
using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using View.BindingModels;

namespace View.Utility.FriendList
{
    public class PreviewImageLoadUtility
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PreviewImageLoadUtility).Name);

        private static readonly object _syncRoot = new object();
        public static PreviewImageLoadUtility Instance = null;
        private ConcurrentQueue<ImageUploaderModel> ImageUploaderModelsQueue = new ConcurrentQueue<ImageUploaderModel>();
        private BackgroundWorker worker;
        public PreviewImageLoadUtility()
        {
            if (worker == null)
            {
                worker = new BackgroundWorker();
                worker.WorkerSupportsCancellation = true;
                worker.WorkerReportsProgress = true;
                worker.DoWork += PreviewImageLoad_DowWork;
                worker.ProgressChanged += PreviewImageLoad_ProgressChanged;
                worker.RunWorkerCompleted += PreviewImageLoad_RunWorkerCompleted;
            }
        }

        public void LoadSingleImageUploaderModel(ImageUploaderModel model)
        {
            try
            {
                lock (_syncRoot)
                {
                    if (PreviewImageLoadUtility.Instance == null)
                    {
                        PreviewImageLoadUtility.Instance = new PreviewImageLoadUtility();
                        PreviewImageLoadUtility.Instance.ImageUploaderModelsQueue.Enqueue(model);
                        PreviewImageLoadUtility.Instance.Start();
                    }
                    else
                    {
                        PreviewImageLoadUtility.Instance.ImageUploaderModelsQueue.Enqueue(model);
                        if (PreviewImageLoadUtility.Instance.worker.IsBusy == false)
                        {
                            PreviewImageLoadUtility.Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadSingleImageUploaderModel ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Start()
        {
            worker.RunWorkerAsync();
        }

        private void PreviewImageLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ImageUploaderModel modelToExecute = null;
                while (ImageUploaderModelsQueue.TryDequeue(out modelToExecute))
                {
                    BitmapImage bitmapsource = null;
                    long n;

                    if (long.TryParse(modelToExecute.FilePath, out n))
                    {
                        bitmapsource = modelToExecute.CopiedBitmapImage;
                    }
                    else
                    {
                        Bitmap bitmapImage = (Bitmap)ImageUtility.GetOrientedImageFromFilePath(modelToExecute.FilePath);
                        //MemoryStream ms = new MemoryStream();
                        //bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //bitmapsource = new BitmapImage();
                        //bitmapsource.BeginInit();
                        //ms.Seek(0, SeekOrigin.Begin);
                        //bitmapsource.StreamSource = ms;
                        //// bitmapsource.DecodePixelHeight = 150;
                        //// bitmapsource.DecodePixelWidth = 150;
                        //bitmapsource.EndInit();
                        //ImageUtility.GetResizedImage(bitmapsource, 150, 150);


                        //bitmapsource = ImageUtility.ConvertBitmapToBitmapImage(bmp);
                        double mw = 0, mh = 0;
                        if(bitmapImage != null)
                        {
                            ImageUtility.GetResizeImageParameters(bitmapImage.Width, bitmapImage.Height, 150, 150, out mw, out mh);
                            MemoryStream ms = new MemoryStream();
                            bitmapImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            bitmapsource = new BitmapImage();
                            bitmapsource.BeginInit();
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapsource.StreamSource = ms;
                            bitmapsource.CacheOption = BitmapCacheOption.OnLoad;
                            bitmapsource.DecodePixelHeight = (int)mh + 20;
                            bitmapsource.DecodePixelWidth = (int)mw + 20;
                            bitmapsource.EndInit();
                            ms = null;
                            bitmapImage.Dispose();
                            if (bitmapImage != null) bitmapImage.Dispose();
                        }
                    }

                    if (bitmapsource != null)
                    {
                        if (ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.ContainsKey(modelToExecute.FilePath))
                        {
                            ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[modelToExecute.FilePath] = bitmapsource;
                        }
                        else
                        {
                            ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.Add(modelToExecute.FilePath, bitmapsource);
                        }

                        bitmapsource.Freeze();
                        bitmapsource = null;
                    }
                    modelToExecute.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception ex)
            {
                log.Error("PreviewImageLoad_DowWork ==> " + ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }


        private void PreviewImageLoad_ProgressChanged(object sender, ProgressChangedEventArgs e) { }

        private void PreviewImageLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }
    }
}

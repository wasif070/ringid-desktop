﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.UI.AddFriend;
using View.UI.Dialer;
using View.ViewModel;
using View.UI;


namespace View.Utility.FriendList
{
    class AddContactSearchResult
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(AddContactSearchResult).Name);

        public void StartProcessing(long searchCount, long matchCount, List<UserBasicInfoDTO> listuserBasicInfoDTO)
        {
            ThrdSearchFriend.totalSearchCount += searchCount;
            ThrdSearchFriend.friendCountInOneRequest += matchCount;
            // HideSearchingLoader();

            if (ThrdSearchFriend.friendCountInOneRequest > 0 && listuserBasicInfoDTO != null)
            {
                lock (UCGuiRingID.Instance._syncRoot)
                {
                    foreach (var userDTO in listuserBasicInfoDTO)
                    {
                        UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userDTO.UserTableID);
                        if (model == null)
                        {
                            model = new UserBasicInfoModel(userDTO);
                            FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                        }
                        else
                        {
                            userDTO.ChatBgUrl = model.ChatBgUrl;
                            model.LoadData(userDTO);
                        }

                        if (ThrdSearchFriend.searchParm != null)
                        {
                            AddRemoveInCollections.AddIntoTempFriendListWithChecking(model, userDTO);
                        }
                        #region Recent OldCode
                        ////if (!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(userDTO.UserTableID)
                        ////  || (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(userDTO.UserTableID) && FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userDTO.UserTableID].FriendShipStatus == 0))
                        ////{
                        //UserBasicInfoDTO userbasicinfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userDTO.UserTableID);
                        //if (userbasicinfo == null || userbasicinfo.FriendShipStatus == 0)
                        //{
                        //    //  model = null;
                        //    UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userDTO.UserTableID);
                        //    if (model == null)
                        //    {
                        //        model = new UserBasicInfoModel(userDTO);
                        //        FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                        //    }
                        //    else
                        //    {
                        //        userDTO.ChatBgUrl = model.ChatBgUrl;
                        //        model.LoadData(userDTO);
                        //    }

                        //    if (ThrdSearchFriend.searchParm != null && FriendListLoadUtility.CheckIfMatch(ThrdSearchFriend.searchParm, model, ThrdSearchFriend.searchBy))
                        //    {
                        //        AddRemoveInCollections.AddIntoTempFriendListWithChecking(model, userDTO);
                        //    }
                        //}
                        #endregion
                    }
                }
                #region OldCode
                //lock (RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY)
                //{
                //    foreach (Dictionary<long, UserBasicInfoDTO> userMap in RingDictionaries.Instance.SEARCH_FRIENDS_DICTIONARY.Values)
                //    {
                //        foreach (long key in userMap.Keys)
                //        {
                //            if (!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(userMap[key].UserIdentity)
                //              || (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.ContainsKey(userMap[key].UserIdentity) && FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userMap[key].UserIdentity].FriendShipStatus == 0))
                //            {
                //                //  model = null;
                //                UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userMap[key].UserIdentity);
                //                if (model == null)
                //                {
                //                    model = new UserBasicInfoModel(userMap[key]);
                //                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                //                }
                //                else
                //                {
                //                    userMap[key].BlockedBy = model.BlockedBy;
                //                    model.LoadShowMoreData(userMap[key]);
                //                }
                //                #region Commented Code
                //                //if (userMap[key].FriendShipStatus == 0)
                //                //{
                //                //    lock (RingIDViewModel.Instance.SuggestionFriendList)
                //                //    {
                //                //        model = RingIDViewModel.Instance.SuggestionFriendList.Where(P => P.ShortInfoModel.UserIdentity == userMap[key].UserIdentity).FirstOrDefault();
                //                //        if (model == null)
                //                //        {
                //                //            model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userMap[key].UserIdentity);
                //                //            if (model == null)
                //                //            {
                //                //                model = new UserBasicInfoModel(userMap[key]);
                //                //                model.ShortInfoModel.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_UNKNOWN;
                //                //                FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                //                //            }
                //                //            else
                //                //            {
                //                //                userMap[key].BlockedBy = model.BlockedBy;
                //                //                model.LoadShowMoreData(userMap[key]);
                //                //            }

                //                //            FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModelsByDto(userMap[key]);
                //                //        }
                //                //    }
                //                //}
                //                //else //Pending/Accepted Friend 
                //                //{
                //                //    model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userMap[key].UserIdentity);
                //                //    if (model == null)
                //                //    {
                //                //        FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModelsByDto(userMap[key]);
                //                //    }
                //                //    else
                //                //    {
                //                //        userMap[key].BlockedBy = model.BlockedBy;
                //                //        model.LoadShowMoreData(userMap[key]);
                //                //    }
                //                //}
                //                #endregion

                //                if (ThrdSearchFriend.searchParm != null && FriendListLoadUtility.CheckIfMatch(ThrdSearchFriend.searchParm, model, ThrdSearchFriend.searchBy))
                //                {
                //                    AddRemoveInCollections.AddIntoTempFriendListWithChecking(model, userMap[key].UserIdentity);
                //                }
                //            }
                //        }

                //    }
                //}
                #endregion
            }
            hideShowMoreInAddFriend(ThrdSearchFriend.friendCountInOneRequest);
        }

        private void hideShowMoreInAddFriend(long searchCount)
        {
            ThrdSearchFriend.SearchForShowMore = false;
            if (UCAddFriendSearchPanel.Instance != null && UCAddFriendSearchPanel.Instance.IsVisible)
            {
                if (searchCount == 0)
                {
                    UCAddFriendSearchPanel.Instance.HideShowMorePanel();
                    if (ThrdSearchFriend.totalSearchCount == 0 || RingIDViewModel.Instance.TempFriendList.Count == 0)
                    {
                        UCAddFriendSearchPanel.Instance.NoResultsFound = true;
                    }
                }
                else
                {
                    UCAddFriendSearchPanel.Instance.NoResultsFound = false;
                    UCAddFriendSearchPanel.Instance.ShowLoader(false); // showing showmore text disabling loading image
                }
            }
            else if (UCDialer.Instance != null && UCDialer.Instance.Visibility == Visibility.Visible)
            {
                if (searchCount == 0 || RingIDViewModel.Instance.TempFriendList.Count == 0)
                {
                    UCDialer.Instance.NoResultsFound = true;
                }
                else
                {
                    UCDialer.Instance.NoResultsFound = false;
                }
            }
            //else if ( UCGuiRingID.Instance.ucFriendSearchListPanel != null && UCGuiRingID.Instance.ucFriendSearchListPanel.Visibility == Visibility.Visible)
            //{
            //    if (searchCount == 0 || RingIDViewModel.Instance.TempFriendSearchList.Count == 0)
            //    {
            //        UCGuiRingID.Instance.ucFriendSearchListPanel.NoResultsFound = true;
            //    }
            //    else
            //    {
            //        UCGuiRingID.Instance.ucFriendSearchListPanel.NoResultsFound = false;
            //    }
            //}
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.UI.AddFriend;
using View.UI.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility.FriendList
{
    public class ThrdAddFriend
    {
        #region "Private Fields"
        private UserBasicInfoModel userbasicInfoModel;
        private bool fromPeopleYouMayKnow = false;
        private Func<bool, int> _Oncomplete;
        #endregion "Private Fields"


        public void StartProcess(UserBasicInfoModel userbasicInfo1)
        {
            userbasicInfoModel = userbasicInfo1;
            Thread addFriendThread = new Thread(run);
            addFriendThread.Name = "ThrdAddFriend";
            addFriendThread.Start();
        }
        public void StartProcess(UserBasicInfoModel userModel, bool fromPeopleYouMayKnow, Func<bool, int> oncomplete = null)
        {
            this.userbasicInfoModel = userModel;
            this.fromPeopleYouMayKnow = fromPeopleYouMayKnow;
            this._Oncomplete = oncomplete;
            Thread addFriendThread = new Thread(run);
            addFriendThread.Name = "ThrdAddFriend";
            addFriendThread.Start();

        }
        public void run()
        {
            try
            {
                bool isSuccess = false;
                string msg = "";
                long updateTime = 0;
                UserBasicInfoDTO _dto = null;
                userbasicInfoModel.VisibilityModel.ShowActionButton = false;
                userbasicInfoModel.VisibilityModel.ShowLoading = Visibility.Visible;
                AddFriendRequest addasFriend = new AddFriendRequest(userbasicInfoModel.ShortInfoModel.UserTableID);
                addasFriend.Run(out isSuccess, out _dto, out msg, out updateTime);
                if (isSuccess)
                {
                    _dto.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_PENDING;
                    _dto.UpdateTime = updateTime;
                    FriendListLoadUtility.AddInfoInModel(userbasicInfoModel, _dto);
                    //lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
                    //{
                    //    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[_dto.UserTableID] = _dto;
                    //}
                    FriendDictionaries.Instance.AddOrUPdateInBasicInfoDictionaryWithLock(_dto);

                    List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();

                    userBasicInfoList.Add(_dto);
                    //new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();
                    //FriendListLoadUtility.InsertSingleFriendToUILists(_dto);
                    userbasicInfoModel.FriendListType = AddRemoveInCollections.Dictonary_OutgoingRequestFriendList;
                    FriendListLoadUtility.AddAModelIntoUI(_dto, userbasicInfoModel, userbasicInfoModel.FriendListType);

                    if (this.fromPeopleYouMayKnow)
                    {
                        AddRemoveInCollections.RemoveFromSuggestionFriendList(userbasicInfoModel);
                        if (UCFriendList.Instance != null && RingIDViewModel.Instance.IsNeedToShowInPeopleYouMayKnow)
                        {
                            UCFriendList.Instance.ShowNextFriendAfterAddOrRemove();
                        }
                    }
                }
                else if (string.IsNullOrWhiteSpace(msg))
                {
                    Application.Current.Dispatcher.Invoke(delegate
                    {
                        UIHelperMethods.ShowFailed(NotificationMessages.MSG_ADDFRIEND_FAIL + msg, "Add friend");
                        // CustomMessageBox.ShowError(NotificationMessages.MSG_ADDFRIEND_FAIL + msg);
                    });
                }

                //Application.Current.Dispatcher.Invoke(delegate
                //{
                userbasicInfoModel.VisibilityModel.ShowActionButton = true;
                userbasicInfoModel.VisibilityModel.ShowLoading = Visibility.Collapsed;
                if (UCAddFriendPending.Instance != null)
                {
                    UCAddFriendPending.Instance.ShowIncommingAndPendingCounts();
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (this._Oncomplete != null)
                {
                    this._Oncomplete(true);
                }
            }
        }
    }
}

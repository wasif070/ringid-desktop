﻿using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using View.BindingModels;

namespace View.Utility.FriendList
{
    class AddToFavouriteFriend
    {
        UserBasicInfoModel userbasicInfo;
        public void StartProcess(UserBasicInfoModel userbasicInfo1)
        {
            userbasicInfo = userbasicInfo1;
            Thread Favourite = new Thread(run);
            Favourite.Name = "AddToFavouriteFriend";
            Favourite.Start();
        }
        public void run()
        {
            try
            {
                UserBasicInfoDTO friendDTO = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userbasicInfo.ShortInfoModel.UserTableID);
                List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
                //if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userbasicInfo.ShortInfoModel.UserTableID, out friendDTO))
                //{
                if (friendDTO != null)
                {
                    if (friendDTO.FavoriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE)
                    {
                        friendDTO.FavoriteRank -= SettingsConstants.FORCE_FAVOURITE_VALUE;
                    }
                    else
                    {
                        friendDTO.FavoriteRank += SettingsConstants.FORCE_FAVOURITE_VALUE;
                    }

                    HelperMethods.changeUI(friendDTO);
                    userbasicInfo.LoadData(friendDTO);
                    userbasicInfo.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception)
            {
            }
        }
    }
}

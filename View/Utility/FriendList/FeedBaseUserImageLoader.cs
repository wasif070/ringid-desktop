﻿using log4net;
using Models.Stores;
using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;

namespace View.Utility.FriendList
{
    public class FeedBaseUserImageLoader
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FeedBaseUserImageLoader).Name);

        private static readonly object _syncRoot = new object();
        public static FeedBaseUserImageLoader Instance = null;
        private ConcurrentQueue<SingleMediaModel> ImageUploaderModelsQueue = new ConcurrentQueue<SingleMediaModel>();
        private BackgroundWorker worker;
        public FeedBaseUserImageLoader()
        {
            if (worker == null)
            {
                worker = new BackgroundWorker();
                worker.WorkerSupportsCancellation = true;
                worker.WorkerReportsProgress = true;
                worker.DoWork += PreviewImageLoad_DowWork;
                worker.ProgressChanged += PreviewImageLoad_ProgressChanged;
                worker.RunWorkerCompleted += PreviewImageLoad_RunWorkerCompleted;
            }
        }
        public void LoadSingleImageUploaderModel(SingleMediaModel model)
        {
            try
            {
                lock (_syncRoot)
                {
                    if (Instance == null)
                    {
                        Instance = new FeedBaseUserImageLoader();
                        ImageUploaderModelsQueue.Enqueue(model);
                        Instance.Start();
                    }
                    else
                    {
                        Instance.ImageUploaderModelsQueue.Enqueue(model);
                        if (Instance.worker.IsBusy == false)
                        {
                            Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadSingleImageUploaderModel ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Start()
        {
            worker.RunWorkerAsync();
        }

        private void PreviewImageLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                SingleMediaModel modelToExecute = null;
                while (ImageUploaderModelsQueue.TryDequeue(out modelToExecute))
                {
                    BitmapImage bitmapsource = null;
                    string filePathImage = RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER + Path.DirectorySeparatorChar + modelToExecute.ThumbUrl.Replace("/", "_");
                    if (File.Exists(filePathImage))
                    {
                        Bitmap bmp = (Bitmap)ImageUtility.GetOrientedImageFromFilePath(filePathImage);
                        if (bmp != null)
                        {
                            MemoryStream ms = new MemoryStream();
                            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            bitmapsource = new BitmapImage();
                            bitmapsource.BeginInit();
                            ms.Seek(0, SeekOrigin.Begin);
                            bitmapsource.StreamSource = ms;
                            bitmapsource.CacheOption = BitmapCacheOption.OnLoad;
                            bitmapsource.EndInit();
                            ms = null;
                            if (bmp != null) bmp.Dispose();
                            if (ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.ContainsKey(filePathImage))
                            {
                                ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[filePathImage] = bitmapsource;
                            }
                            else
                            {
                                ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.Add(filePathImage, bitmapsource);
                            }
                            bitmapsource.Freeze();
                            bitmapsource = null;
                            modelToExecute.OnPropertyChanged("CurrentInstance");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("PreviewImageLoad_DowWork ==> " + ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }


        private void PreviewImageLoad_ProgressChanged(object sender, ProgressChangedEventArgs e) { }

        private void PreviewImageLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }
    }
}

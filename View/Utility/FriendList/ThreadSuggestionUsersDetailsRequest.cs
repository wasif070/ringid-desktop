﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.FriendList
{
    public class ThreadSuggestionUsersDetailsRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadSuggestionUsersDetailsRequest).Name);
        private bool running = false;
        private List<long> list;
        private Func<bool, int> _OnComplete;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JArray suggestionIdsList = new JArray();
                foreach (long i in list)
                {
                    suggestionIdsList.Add(i);
                }
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.ContactUtIdList] = suggestionIdsList;
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_USERS_DETAILS;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ThreadSuggestionUsersDetailsRequest => " + ex.Message + ex.StackTrace);
            }
            finally 
            { 
                running = false;
                if (this._OnComplete != null)
                {
                    this._OnComplete(true);
                }
            }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(List<long> list1, Func<bool,int> onComplete = null)
        {
            if (!running)
            {
                this.list = list1;
                this._OnComplete = onComplete;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Name = this.GetType().Name;
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

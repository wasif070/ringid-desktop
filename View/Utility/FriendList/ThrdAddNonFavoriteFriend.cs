﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models.Entity;
using View.BindingModels;
using View.ViewModel;

namespace View.Utility.FriendList
{
    class ThrdAddNonFavoriteFriend
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdAddNonFavoriteFriend).Name);
        private bool running = false;
        int numberOfFriendNeedToLoad;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThrdAddNonFavoriteFriend() { }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                if (RingIDViewModel.Instance.NonFavouriteFriendList.Count < FriendListController.Instance.FriendDataContainer.NonFavoriteList.Count)
                {
                    FriendListController.Instance.FriendDataContainer.SortNonFavList();
                    int totalFrinds = FriendListController.Instance.FriendDataContainer.NonFavoriteList.Count;
                    int friendInview = RingIDViewModel.Instance.NonFavouriteFriendList.Count;
                    //int loop = 10;
                    //if (FriendListController.Instance.FriendDataContainer.NonFavoriteList.Count < loop || friendInview + loop > FriendListController.Instance.FriendDataContainer.NonFavoriteList.Count)
                    //{
                    //    loop = FriendListController.Instance.FriendDataContainer.NonFavoriteList.Count;
                    //}
                    int loop = totalFrinds - friendInview;
                    if (loop > 0)
                    {
                        if (loop > numberOfFriendNeedToLoad)
                        {
                            loop = numberOfFriendNeedToLoad;
                        }
                        for (int i = friendInview; i < friendInview + loop; i++)
                        {
                            if (i < totalFrinds)
                            {
                                UserBasicInfoDTO dto = FriendListController.Instance.FriendDataContainer.NonFavoriteList[i];
                                UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(dto.UserTableID);
                                if (!RingIDViewModel.Instance.NonFavouriteFriendList.Contains(model))
                                {
                                    AddRemoveInCollections.AddIntoNonFavouriteFriendList(model);
                                    System.Threading.Thread.Sleep(10);
                                }
                            }
                        }
                        //  FriendListController.Instance.FriendDataContainer.TotalFriends();
                    }

                }

                // Console.WriteLine("FriendListController.Instance.FriendDataContainer.NonFavoriteList.Count==>" + FriendListController.Instance.FriendDataContainer.NonFavoriteList.Count);
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
            finally
            {
                running = false;
            }
        }

        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(int numberOfFriendNeedToLoad)
        {
            if (!running)
            {
                this.numberOfFriendNeedToLoad = numberOfFriendNeedToLoad;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        #endregion "Public methods"
    }
}

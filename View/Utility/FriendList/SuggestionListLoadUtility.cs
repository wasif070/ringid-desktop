﻿using log4net;
using Models.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using View.BindingModels;
using View.UI.AddFriend;
using View.ViewModel;
using Models.Constants;

namespace View.Utility.FriendList
{
    class SuggestionListLoadUtility : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SuggestionListLoadUtility).Name);

        private static readonly object _syncRoot = new object();
        private static SuggestionListLoadUtility _Instance = null;
        private ConcurrentQueue<UserBasicInfoDTO> _SuggestionQueue = new ConcurrentQueue<UserBasicInfoDTO>();

        public SuggestionListLoadUtility()
        {
            _Instance = this;
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += SuggestionListLoad_DowWork;
            ProgressChanged += SuggestionListLoad_ProgressChanged;
            RunWorkerCompleted += SuggestionListLoad_RunWorkerCompleted;
        }

        public static void LoadDataFromServer(List<UserBasicInfoDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                LoadData(list);
            }
        }

        private static void LoadData(List<UserBasicInfoDTO> list)
        {
            try
            {
                lock (_syncRoot)
                {
                    if (SuggestionListLoadUtility._Instance == null)
                    {
                        SuggestionListLoadUtility._Instance = new SuggestionListLoadUtility();
                        foreach (UserBasicInfoDTO userDTO in list)
                        {
                            SuggestionListLoadUtility._Instance._SuggestionQueue.Enqueue(userDTO);
                        }
                        SuggestionListLoadUtility._Instance.Start();
                    }
                    else
                    {
                        foreach (UserBasicInfoDTO userDTO in list)
                        {
                            SuggestionListLoadUtility._Instance._SuggestionQueue.Enqueue(userDTO);
                        }
                        if (SuggestionListLoadUtility._Instance.IsBusy == false)
                        {
                            SuggestionListLoadUtility._Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadShowMoreData ex ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Start()
        {
            RunWorkerAsync();
        }

        private void SuggestionListLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                UserBasicInfoDTO userDTO = null;
                while (_SuggestionQueue.TryDequeue(out userDTO))
                {
                    System.Threading.Thread.Sleep(10);
                    //ReportProgress(1, userDTO);
                    DoJob(userDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("SuggestionListLoad_DowWork ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            e.Result = true;
        }

        private UserBasicInfoModel ProcessModel(UserBasicInfoDTO userDTO)
        {
            UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
            //lock (RingIDViewModel.Instance.UnknownFriendList)
            //{
            //    model = RingIDViewModel.Instance.UnknownFriendList.Where(u => u.ShortInfoModel.UserIdentity == userDTO.UserIdentity).FirstOrDefault();
            //    if (model == null)
            //        model = new UserBasicInfoModel(userDTO);
            //}
            if (model == null)
            {
                model = new UserBasicInfoModel(userDTO);
                model.ShortInfoModel.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_UNKNOWN;
                FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
            }
            return model;
        }

        private void DoJob(UserBasicInfoDTO dto)
        {
            try
            {
                UserBasicInfoModel suggestionModel = ProcessModel(dto);
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    if (suggestionModel != null)
                    {
                        if (!AddRemoveInCollections.checkIfSingleSuggestionFriendExist(suggestionModel.ShortInfoModel.UserTableID))
                        {
                            if (RingIDViewModel.Instance.SuggestionFriendList.Count() < 5 && (RingIDViewModel.Instance.RecentlyAddedFriendList.Count() + RingIDViewModel.Instance.FavouriteFriendList.Count() + RingIDViewModel.Instance.NonFavouriteFriendList.Count()) < 30)
                            {
                                suggestionModel.VisibilityModel.IsVisibleInPeopleUmayKnow = Visibility.Visible;
                                suggestionModel.VisibilityModel.IsVisibleInSuggestion = Visibility.Collapsed;
                                RingIDViewModel.Instance.IsNeedToShowInPeopleYouMayKnow = true;
                            }
                            else
                            {
                                if (UCSuggestions.Instance != null)
                                {
                                    suggestionModel.VisibilityModel.IsVisibleInSuggestion = UCSuggestions.Instance.IsFriendVisibility(suggestionModel);
                                    UCSuggestions.Instance.ShowLoader(false);
                                    //UCSuggestions.Instance.showMoreText.Visibility = Visibility.Visible;
                                    UCSuggestions.Instance.showMoreButtonPanel.Visibility = Visibility.Visible;
                                    UCSuggestions.Instance.ShowMorePanelHideShow();
                                }
                            }
                            AddRemoveInCollections.AddIntoSuggestionFriendList(suggestionModel);
                        }
                    }
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("SuggestionListLoad_ProgressChanged ==> " + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }
        private void SuggestionListLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            /* try
             {
                 UserBasicInfoDTO dto = ((UserBasicInfoDTO)e.UserState);
                 UserBasicInfoModel suggestionModel = ProcessModel(dto);
                 Application.Current.Dispatcher.Invoke((Action)delegate
                     {
                         if (suggestionModel != null)
                         {
                             lock (RingIDViewModel.Instance.SuggestionFriendList)
                             {
                                 if (!AddRemoveInCollections.checkIfSingleSuggestionFriendExist(suggestionModel.ShortInfoModel.UserIdentity))
                                 {

                                     if (RingIDViewModel.Instance.SuggestionFriendList.Count() < 5)
                                     {
                                         suggestionModel.VisibilityModel.IsVisibleInPeopleUmayKnow = Visibility.Visible;
                                         suggestionModel.VisibilityModel.IsVisibleInSuggestion = Visibility.Collapsed;
                                     }
                                     else
                                     {
                                         if (UCSuggestions.Instance != null)
                                         {
                                             suggestionModel.VisibilityModel.IsVisibleInSuggestion = UCSuggestions.Instance.IsFriendVisibility(suggestionModel);
                                             UCSuggestions.Instance.ShowLoader(false);
                                             //UCSuggestions.Instance.showMoreText.Visibility = Visibility.Visible;
                                             UCSuggestions.Instance.showMoreButtonPanel.Visibility = Visibility.Visible;
                                             UCSuggestions.Instance.ShowMorePanelHideShow();
                                         }
                                     }
                                     AddRemoveInCollections.AddIntoSuggestionFriendList(suggestionModel);
                                 }
                             }
                         }
                     }, DispatcherPriority.Send);
             }
             catch (Exception ex)
             {
                 log.Error("SuggestionListLoad_ProgressChanged ==> " + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
             }*/
        }

        private void SuggestionListLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.UI.FriendList;
using View.ViewModel;
using View.UI;

namespace View.Utility.FriendList
{
    class FriendDataContainer
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(FriendDataContainer).Name);
        #endregion "Private Fields"

        #region "Public Fields"

        public ConcurrentQueue<UserBasicInfoDTO> TempFriendQueue = new ConcurrentQueue<UserBasicInfoDTO>();
        public List<UserBasicInfoDTO> NonFavoriteList = new List<UserBasicInfoDTO>();
        public List<UserBasicInfoDTO> IncomingList = new List<UserBasicInfoDTO>();
        public List<UserBasicInfoDTO> PendingList = new List<UserBasicInfoDTO>();

        #endregion "Public Fields"

        #region "Constructors"
        public FriendDataContainer()
        {

        }
        #endregion "Constructors"

        #region "Properties"
        private ConcurrentDictionary<long, UserBasicInfoModel> _userBasicInfoModels = new ConcurrentDictionary<long, UserBasicInfoModel>();
        public ConcurrentDictionary<long, UserBasicInfoModel> UserBasicInfoModels
        {
            get { return _userBasicInfoModels; }
        }
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        #endregion "Private methods"

        #region "Public Methods"

        #region "UserBasicInfoModels"

        public void AddIntoUserBasicInfoModels(UserBasicInfoModel userModel)
        {
            AddOrReplaceUserBasicInfoModels(userModel.ShortInfoModel.UserTableID, userModel);
        }

        public UserBasicInfoModel AddIntoUserBasicInfoModelsByDto(UserBasicInfoDTO userDTO)
        {
            return AddOrReplaceUserBasicInfoModelsByDto(userDTO);
        }

        public void AddOrReplaceUserBasicInfoModels(long utId, UserBasicInfoModel userModel)
        {
            UserBasicInfoModels[utId] = userModel;
        }

        public UserBasicInfoModel AddOrReplaceUserBasicInfoModelsByDto(UserBasicInfoDTO userDTO)
        {
            try
            {
                UserBasicInfoModel model = new UserBasicInfoModel(userDTO);
                UserBasicInfoModels[userDTO.UserTableID] = model;
                return model;
            }
            catch (Exception ex) { log.Error(userDTO + "==>" + ex.StackTrace); }
            return null;
        }

        public UserBasicInfoModel GetFromUserBasicInfoModels(long utId)
        {
            UserBasicInfoModel user = null;
            UserBasicInfoModels.TryGetValue(utId, out user);
            return user;
        }

        public void RemoveFromUserBasicInfoModels(long utId)
        {
            UserBasicInfoModel user;
            UserBasicInfoModels.TryRemove(utId, out user);
        }

        public int FreindListType(UserBasicInfoModel model)
        {
            return model.FriendListType;
        }

        #endregion "UserBasicInfoModels"

        #region "NonFavoriteList"

        public void AddIntoNonFavoriteList(UserBasicInfoDTO userBasicInfoDTO)
        {
            lock (NonFavoriteList)
            {
                if (!NonFavoriteList.Any(P => P.UserTableID == userBasicInfoDTO.UserTableID)) NonFavoriteList.Add(userBasicInfoDTO);
            }
        }

        public void RemoveFromNonFavoriteList(long utID)
        {
            lock (NonFavoriteList)
            {
                UserBasicInfoDTO user = NonFavoriteList.SingleOrDefault(D => D.UserTableID == utID);
                if (user != null) NonFavoriteList.Remove(user);
            }
        }

        public void SortNonFavList()
        {
            NonFavoriteList = NonFavoriteList.GroupBy(test => test.RingID).Select(grp => grp.First()).OrderBy(p => p.FullName).ToList();
        }

        #endregion "NonFavoriteList"

        #region "IncomingList"

        public void AddIntoIncomingList(UserBasicInfoDTO userBasicInfoDTO)
        {
            IncomingList.Add(userBasicInfoDTO);
        }

        public void RemoveFromIncomingList(long userTableID)
        {
            UserBasicInfoDTO user = IncomingList.SingleOrDefault(D => D.UserTableID == userTableID);
            if (user != null)
            {
                IncomingList.Remove(user);
            }
        }

        public void SortIncomingList()
        {
            ///   orderby l.ContactAddedTime descending, l.FullName ascending
            IncomingList = IncomingList.OrderByDescending(p => p.ContactAddedTime).ThenBy(p => p.FullName).ToList();
        }

        public int IncommingListCount()
        {
            return IncomingList.Count;
        }

        #endregion "IncomingList"

        #region "PendingList"

        public void AddIntoPendingList(UserBasicInfoDTO userBasicInfoDTO)
        {
            PendingList.Add(userBasicInfoDTO);
        }

        public void RemoveFromPendingList(long utID)
        {
            UserBasicInfoDTO user = PendingList.SingleOrDefault(D => D.UserTableID == utID);
            if (user != null)
            {
                PendingList.Remove(user);
            }
        }

        public void SortPendingList()
        {
            //   orderby l.ContactAddedTime descending, l.FullName ascending
            PendingList = PendingList.OrderByDescending(p => p.ContactAddedTime).ThenBy(p => p.FullName).ToList();
        }

        public int PendingListcount()
        {
            return PendingList.Count;
        }

        #endregion "PendingList"

        public int TotalFriends()
        {
            int total = RingIDViewModel.Instance.FavouriteFriendList.Count + RingIDViewModel.Instance.TopFriendList.Count + NonFavoriteList.Count;
            UCFriendList.Instance.TotalFriend = total;
            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists != null)
                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.TotalFriend = total;
            return total;
        }

        #endregion "Public methods"

        #region "Friends stores"

        #endregion""
    }
}

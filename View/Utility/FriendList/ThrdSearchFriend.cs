﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI.AddFriend;
using View.UI.Dialer;
using View.ViewModel;
namespace View.Utility.FriendList
{

    public class ThrdSearchFriend
    {
        #region "Private Fields"
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdSearchFriend).Name);
        private static int waiting = 0;
        int searchingFrom;
        private Int32 SLEEP_TIME = 10;//ms
        public static string prevString;
        private bool ContactListSearchComplete = false;
        #endregion "Private Fields"

        #region "Public Fields"
        public static int SearchFromAddFriend = 0;
        public static int SearchFromDailPad = 1;

        public static bool runningSearchFriendThread = false;
        public static string searchParm = null;
        public static int searchBy = 0;
        public static bool processingList = false;
        public static bool SearchForShowMore = false;
        public static long totalSearchCount = 0;
        public static long friendCountInOneRequest = 0;
        #endregion "Public Fields"

        #region "Constructors"
        public ThrdSearchFriend(int searchFrom, int searchBy)
        {
            this.searchingFrom = searchFrom;
            ThrdSearchFriend.searchBy = searchBy;
        }
        public void StartThread()
        {
            if (!ThrdSearchFriend.runningSearchFriendThread)
            {
                ThrdSearchFriend.runningSearchFriendThread = true;
                Thread trd = new Thread(Run);
                trd.Name = "SearchFriend";
                trd.Start();
            }
        }
        #endregion "Constructors"

        #region Private methods
        private void ResetUIWhileStopThread()
        {
            if (UCAddFriendSearchPanel.Instance != null)
            {
                UCAddFriendSearchPanel.Instance.ShowSearchingLoader(false);
                UCAddFriendSearchPanel.Instance.HideShowMorePanel();
            }
        }
        private void AddIntoTempFriendlistBySearchResult(int searchBy)
        {

            if (!ThrdSearchFriend.processingList)
            {
                try
                {
                    ThrdSearchFriend.processingList = true;
                    List<UserBasicInfoModel> userList = new List<UserBasicInfoModel>();
                    // For QA requirements accepted list should be upper than nonfriendlist, that's why below two lists are taken
                    List<UserBasicInfoModel> acceptedFriendList = new List<UserBasicInfoModel>(); 
                    List<UserBasicInfoModel> nonFriendList = new List<UserBasicInfoModel>();

                    foreach (UserBasicInfoModel model in FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values)
                    {
                        if ((prevString == null || !prevString.Equals(ThrdSearchFriend.searchParm)) || RingIDViewModel.Instance.TempFriendList.Count > 200)
                        {
                            break;
                        }
                        if (model.ShortInfoModel.ContactType != 3 && FriendListLoadUtility.CheckIfMatch(prevString, model, searchBy))
                        {
                            if (model.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                            {
                                acceptedFriendList.Add(model);
                            }
                            else
                            {
                                nonFriendList.Add(model);
                            }
                        }
                    }
                    if ((acceptedFriendList.Count + nonFriendList.Count) > 0)
                    {
                        userList.AddRange(acceptedFriendList.OrderBy(p => p.ShortInfoModel.FullName).ToList());
                        userList.AddRange(nonFriendList.OrderBy(p => p.ShortInfoModel.FullName).ToList()); 
                        //userList = userList.OrderBy(p => p.ShortInfoModel.FullName).ToList();

                        foreach (UserBasicInfoModel model in userList)
                        {
                            if ((prevString == null || !prevString.Equals(ThrdSearchFriend.searchParm)) || RingIDViewModel.Instance.TempFriendList.Count > 200)
                            {
                                break;
                            }
                            AddRemoveInCollections.AddIntoTempFriendList(model, model.ShortInfoModel.UserTableID);
                            addASleep();
                        }
                    }
                    ContactListSearchComplete = true;
                }
                catch (Exception ex)
                {
                    log.Error("Searchfreind==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);
                }
                finally
                {
                    ContactListSearchComplete = true;
                    ThrdSearchFriend.processingList = false;
                }
            }
        }
        private void StartSearchingLoader(bool needToSearchingLoader)
        {
            if (!SearchForShowMore)
            {
                if (UCAddFriendSearchPanel.Instance != null && UCAddFriendSearchPanel.Instance.IsVisible)
                {
                    UCAddFriendSearchPanel.Instance.ShowSearchingLoader(needToSearchingLoader);
                }
            }
            if (UCDialer.Instance != null && UCDialer.Instance.IsVisible)
            {
                UCDialer.Instance.ShowSearchingLoader(needToSearchingLoader);
            }
        }

        private void sendSearchRequest()
        {
            try
            {
                StartSearchingLoader(true);
                JObject packet = new JObject();
                String pakId = SendToServer.GetRanDomPacketID();
                packet[JsonKeys.PacketId] = pakId;
                RingDictionaries.Instance.SEARCHING_UI_NAME_DICTIONARY[StatusConstants.SEARCH_ADD_FRIEND_OR_DIALPAD] = pakId;
                if (ThrdSearchFriend.searchBy != StatusConstants.SEARCH_BY_RINGID)
                {
                    packet[JsonKeys.LogStartLimit] = ThrdSearchFriend.totalSearchCount;//SearchCount();//RingIDViewModel.Instance.TempFriendList.Count
                }
                packet[JsonKeys.Action] = AppConstants.TYPE_CONTACT_SEARCH;
                packet[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                packet[JsonKeys.SearchParam] = searchParm;
                packet[JsonKeys.SearchCategory] = ThrdSearchFriend.searchBy;
                SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, packet.ToString());
                for (int pakSendingAttempt = 1; pakSendingAttempt <= DefaultSettings.TRYING_TIME; pakSendingAttempt++)
                {
                    if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) || !ThrdSearchFriend.runningSearchFriendThread)
                    {
                        break;
                    }
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                    {
                        if (pakSendingAttempt % 9 == 0)
                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, packet.ToString());
                    }
                    else
                    {
                        JObject feedbackfields;
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                        break;
                    }
                    if (pakSendingAttempt == DefaultSettings.TRYING_TIME)
                    {
                        ResetUIWhileStopThread();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + " " + ex.Message);
            }
            finally
            {
                StartSearchingLoader(false);
                ResetUIWhileStopThread();
            }
        }
        private void addASleep()
        {
            System.Threading.Thread.Sleep(SLEEP_TIME);
        }

        private void Run()
        {
            try
            {
                while (ThrdSearchFriend.runningSearchFriendThread)
                {
                    if (ThrdSearchFriend.searchParm != null && ThrdSearchFriend.searchParm.Trim().Length > 0)
                    {
                        if (SearchForShowMore)
                        {
                            waiting = 0;
                            sendSearchRequest();
                        }
                        else if ((prevString == null || !prevString.Trim().Equals(ThrdSearchFriend.searchParm.Trim())) && !SearchForShowMore)
                        {
                            waiting = 0;
                            //Thread.Sleep(500);
                            prevString = ThrdSearchFriend.searchParm;
                            AddRemoveInCollections.ClearTempFriendList();
                            ContactListSearchComplete = false;
                            AddIntoTempFriendlistBySearchResult(ThrdSearchFriend.searchBy);
                            if (!string.IsNullOrEmpty(ThrdSearchFriend.searchParm) && ContactListSearchComplete && RingIDViewModel.Instance.TempFriendList.Count <= 5)
                            {
                                sendSearchRequest();
                            }
                        }
                    }
                    else
                    {
                        prevString = null;
                        AddRemoveInCollections.ClearTempFriendList();
                    }
                    if (waiting != 0 && waiting % 130 == 0)
                    {
                        break;
                    }
                    waiting++;
                    Thread.Sleep(400);
                }
            }
            catch (Exception ex)
            {
                log.Error("Searchfreind==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                ThrdSearchFriend.searchParm = null;
                runningSearchFriendThread = false;
                SearchForShowMore = false;
                ResetUIWhileStopThread();
                prevString = null;
            }
        }
        #endregion Private methods
    }
}

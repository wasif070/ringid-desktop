﻿
using Models.Entity;
namespace View.Utility.FriendList
{
    class FriendListController
    {
        #region "Private Fields"
        private static readonly FriendListController instance = new FriendListController();
        //    private ThrdLoadFriendModelIntoViewDB loadFriendModelIntoView;
        private ThrdRepositionAFriend thrdRepositionAFriend;
        //private ThrdLoadPendingListInView thrdLoadPendingListInView;
        // private ThrdAddNonFavoriteFriend thrdAddNonFavoriteFriend;
        public ThrdHandleFriendFromServer thrdHandleFriendFromServer;

        #endregion "Private Fields"

        #region "Public Fields"
        public static int TotalFriend = 0;
        //   public ThrdLoadIncommingListInView thrdLoadIncommingListInView;
        #endregion "Public Fields"

        #region "Constructors"
        public FriendListController()
        {
            FriendDataContainer = new FriendDataContainer();
        }
        #endregion "Constructors"

        #region "Properties"
        public FriendDataContainer FriendDataContainer { get; set; }
        #endregion "Properties"

        #region "Public Methods"
        public static FriendListController Instance
        {
            get { return instance; }
        }

        //public void StartThrdRepositionAFriend(UserBasicInfoDTO userb)
        //{
        //    if (thrdRepositionAFriend == null) thrdRepositionAFriend = new ThrdRepositionAFriend();
        //    thrdRepositionAFriend.StartThread(userb);
        //}

        //public void StartThrdLoadIncommingListInView()
        //{
        //    if (thrdLoadIncommingListInView == null) thrdLoadIncommingListInView = new ThrdLoadIncommingListInView();
        //    thrdLoadIncommingListInView.StartThread(10);
        //}

        //public void StartThrdLoadPendingListInView()
        //{
        //    if (thrdLoadPendingListInView == null) thrdLoadPendingListInView = new ThrdLoadPendingListInView();
        //    thrdLoadPendingListInView.StartThread(10);
        //}


        public void StartThrdHandleFriendFromServer()
        {
            if (thrdHandleFriendFromServer == null) thrdHandleFriendFromServer = new ThrdHandleFriendFromServer();
            thrdHandleFriendFromServer.StartThread();
        }

        #endregion "Public methods"
    }
}

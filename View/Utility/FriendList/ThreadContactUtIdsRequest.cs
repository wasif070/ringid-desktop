﻿using System;
using System.Threading;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace View.Utility.FriendList
{
    public class ThreadContactUtIdsRequest
    {
        // private static ThreadContactUtIdsRequest _Instance;
        public static int? _TotalRecords = null;
        public static int _TotalPackets = 0;
        private static bool _IsRunning = false;
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadContactUtIdsRequest).Name);

        public void StarThread()
        {
            if (!_IsRunning)
            {
                Thread thread = new Thread(new ThreadStart(Run));
                thread.Start();
                thread.Name = this.GetType().Name;
            }
        }

        public static bool IsRunning()
        {
            return _IsRunning;
        }

        public static void SetRunning(bool isRunning)
        {
            _IsRunning = isRunning;
        }

        public void Run()
        {
            if (!_IsRunning)
            {
                _TotalRecords = null;
                _TotalPackets = 0;
                SetRunning(true);
                try
                {
                    if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                    {
                        try
                        {
                            JObject pakToSend = new JObject();
                            pakToSend[JsonKeys.Action] = AppConstants.TYPE_CONTACT_UTIDS;
                            String pakId = SendToServer.GetRanDomPacketID();
                            pakToSend[JsonKeys.PacketId] = pakId;
                            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                            pakToSend[JsonKeys.UpdateTime] = SettingsConstants.VALUE_RINGID_USERINFO_UT;
                            pakToSend[JsonKeys.ContactUpdateTime] = SettingsConstants.VALUE_RINGID_CONTACT_UT;
                            string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                            Thread.Sleep(25);
                            for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                            {
                                if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)) break;
                                Thread.Sleep(DefaultSettings.WAITING_TIME);
                                if ((!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                                        || (_TotalRecords != null && FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count != _TotalRecords))
                                {
                                    if (i % DefaultSettings.SEND_INTERVAL == 0) SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                                }
                                else
                                {
                                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out pakToSend);
                                    return;
                                }
                            }
                            if (!MainSwitcher.ThreadManager().PingNow()) { }
                        }
                        catch (Exception e) { log.Error("ContactUtIdsRequest ex ==>" + e.StackTrace); }
                    }
                    else log.Error("ContactUtIdsRequest Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
                finally { SetRunning(false); }
            }
        }
    }
}

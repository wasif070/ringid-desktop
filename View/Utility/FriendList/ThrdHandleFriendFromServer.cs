﻿using System;
using System.Linq;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.ViewModel;

namespace View.Utility.FriendList
{
    class ThrdHandleFriendFromServer
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdHandleFriendFromServer).Name);
        private bool running = false;
        int watingTime = 0;
        int previousCount = 0;
        #endregion "Private Fields"

        #region "Constructors"
        public ThrdHandleFriendFromServer() { }
        #endregion "Constructors"

        #region "Private methods"

        private void Run()
        {
            try
            {
                if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.WinDataModel != null) RingIDViewModel.Instance.WinDataModel.IsLoadingCotactList = true;
                #region "Wait Untill all Friend Fetched From Server"
                while (true)
                {
                    if (FriendListController.Instance.FriendDataContainer.TempFriendQueue.Count > previousCount)
                    {
                        previousCount = FriendListController.Instance.FriendDataContainer.TempFriendQueue.Count;
                        watingTime = 0;
                    }
                    else watingTime++;
                    if (ThrdLoadIncommingListInView.IsRunnig()) watingTime = 0;
                    else if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0) break;
                    else
                    {
                        if (watingTime > (2 * 60)) break;
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                #endregion "Wait Untill all Friend Fetched From Server"

                #region "Make Model from Dto and add into Friends Model Dictionary"
                UserBasicInfoDTO userDTO = null;
                while (FriendListController.Instance.FriendDataContainer.TempFriendQueue.TryDequeue(out userDTO))
                {
                    try
                    {
                        if (userDTO.UserTableID > 0)
                        {
                            UserBasicInfoModel model = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                            if (model != null)
                            {
                                if (userDTO.ContactType != SettingsConstants.SPECIAL_CONTACT) FriendListLoadUtility.RemoveAModelFromUI(userDTO, model, false);
                                userDTO.ImSoundEnabled = model.ShortInfoModel.ImSoundEnabled;
                                userDTO.ImNotificationEnabled = model.ShortInfoModel.ImNotificationEnabled;
                                userDTO.ImPopupEnabled = model.ShortInfoModel.ImPopupEnabled;
                                userDTO.IncomingStatus = model.IncomingStatus;
                                userDTO.ContactAddedReadUnread = model.ContactAddedReadUnread;
                                userDTO.ChatBgUrl = model.ChatBgUrl;
                                FriendListLoadUtility.AddInfoInModel(model, userDTO);
                                if (userDTO.Delete == 1) model.FriendListType = AddRemoveInCollections.Dictonary_UnknownFriendList;
                                else
                                {
                                    if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && userDTO.ContactType != SettingsConstants.SPECIAL_CONTACT)
                                    {
                                        //if (userDTO.ContactAddedTime > (Models.Utility.ModelUtility.CurrentTimeMillisLocal() - Models.Utility.ModelUtility.MilliSecondsInSevenDay))
                                        //    AddRemoveInCollections.AddIntoRecentlyAddedFriendList(model);
                                        int friendType = model.FriendListType;
                                        if (friendType != AddRemoveInCollections.Dictonary_TopFriendList && friendType != AddRemoveInCollections.Dictonary_FavouriteFriendList && friendType != AddRemoveInCollections.Dictonary_NonFavouriteFriendList)
                                        {
                                            if (model.FavoriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE) friendType = AddRemoveInCollections.Dictonary_FavouriteFriendList;
                                            else if (model.FavoriteRank < SettingsConstants.FORCE_FAVOURITE_VALUE && (model.NumberOfCalls > 0 || model.NumberOfChats > 0)) friendType = AddRemoveInCollections.Dictonary_TopFriendList;
                                            else friendType = AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
                                            model.FriendListType = friendType;
                                        }
                                        switch (friendType)
                                        {
                                            case AddRemoveInCollections.Dictonary_TopFriendList:
                                                if (!RingIDViewModel.Instance.TopFriendList.Contains(model)) AddRemoveInCollections.AddIntoTopFriendList(model);
                                                break;
                                            case AddRemoveInCollections.Dictonary_FavouriteFriendList:
                                                if (!RingIDViewModel.Instance.FavouriteFriendList.Contains(model)) AddRemoveInCollections.AddIntoFavouriteFriendList(model);
                                                break;
                                            case AddRemoveInCollections.Dictonary_NonFavouriteFriendList:
                                                if (!FriendListController.Instance.FriendDataContainer.NonFavoriteList.Any(P => P.RingID == userDTO.RingID)) FriendListController.Instance.FriendDataContainer.AddIntoNonFavoriteList(userDTO);
                                                break;
                                        }
                                    }
                                    else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
                                    {
                                        FriendListController.Instance.FriendDataContainer.AddIntoIncomingList(userDTO);
                                        model.IncomingStatus = StatusConstants.INCOMING_REQUEST_UNREAD;
                                        model.FriendListType = AddRemoveInCollections.Dictonary_IncomingRequestFriendList;
                                        AppConstants.ADD_FRIEND_NOTIFICATION_COUNT++;
                                    }
                                    else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
                                    {
                                        FriendListController.Instance.FriendDataContainer.AddIntoPendingList(userDTO);
                                        model.FriendListType = AddRemoveInCollections.Dictonary_OutgoingRequestFriendList;
                                    }
                                    else if (userDTO.FriendShipStatus == 0) model.FriendListType = AddRemoveInCollections.Dictonary_UnknownFriendList;
                                }
                            }
                            //if (model == null)
                            //{
                            //    Console.WriteLine("UserBasicInfoModel==>" + model.ShortInfoModel.FullName);
                            //    model = new UserBasicInfoModel(userDTO);
                            //    if (userDTO.ContactType == SettingsConstants.SPECIAL_CONTACT) AddRemoveInCollections.AddIntoSpecialFriendList(model);
                            //    else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                            //    {
                            //        if (userDTO.ContactAddedTime > (Models.Utility.ModelUtility.CurrentTimeMillisLocal() - Models.Utility.ModelUtility.MilliSecondsInSevenDay))
                            //            AddRemoveInCollections.AddIntoRecentlyAddedFriendList(model);
                            //        FriendListController.Instance.FriendDataContainer.AddIntoNonFavoriteList(userDTO);
                            //    }
                            //    else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
                            //    {
                            //        FriendListController.Instance.FriendDataContainer.AddIntoIncomingList(userDTO);
                            //        model.FriendListType = AddRemoveInCollections.Dictonary_IncomingRequestFriendList;
                            //        AppConstants.ADD_FRIEND_NOTIFICATION_COUNT++;
                            //    }
                            //    else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
                            //    {
                            //        FriendListController.Instance.FriendDataContainer.AddIntoPendingList(userDTO);
                            //        model.FriendListType = AddRemoveInCollections.Dictonary_OutgoingRequestFriendList;
                            //    }
                            //    else if (userDTO.FriendShipStatus == 0) model.FriendListType = AddRemoveInCollections.Dictonary_UnknownFriendList;
                            //    FriendListController.Instance.FriendDataContainer.AddOrReplaceUserBasicInfoModels(userDTO.UserTableID, model);
                            //}
                            //else
                            //{
                            //    FriendListLoadUtility.RemoveAModelFromUI(userDTO, model, false);
                            //    userDTO.ImSoundEnabled = model.ShortInfoModel.ImSoundEnabled;
                            //    userDTO.ImNotificationEnabled = model.ShortInfoModel.ImNotificationEnabled;
                            //    userDTO.ImPopupEnabled = model.ShortInfoModel.ImPopupEnabled;
                            //    userDTO.IncomingStatus = model.IncomingStatus;
                            //    userDTO.ContactAddedReadUnread = model.ContactAddedReadUnread;
                            //    userDTO.ChatBgUrl = model.ChatBgUrl;
                            //    FriendListLoadUtility.AddInfoInModel(model, userDTO);

                            //    if (userDTO.Delete == 1) model.FriendListType = AddRemoveInCollections.Dictonary_UnknownFriendList;
                            //    else
                            //    {
                            //        if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && userDTO.ContactType != SettingsConstants.SPECIAL_CONTACT)
                            //        {
                            //            //if (userDTO.ContactType == SettingsConstants.SPECIAL_CONTACT) AddRemoveInCollections.AddIntoSpecialFriendList(model);
                            //            //else
                            //            //{
                            //            if (userDTO.ContactAddedTime > (Models.Utility.ModelUtility.CurrentTimeMillisLocal() - Models.Utility.ModelUtility.MilliSecondsInSevenDay))
                            //                AddRemoveInCollections.AddIntoRecentlyAddedFriendList(model);
                            //            int friendType = model.FriendListType;
                            //            if (friendType != AddRemoveInCollections.Dictonary_TopFriendList && friendType != AddRemoveInCollections.Dictonary_FavouriteFriendList && friendType != AddRemoveInCollections.Dictonary_NonFavouriteFriendList)
                            //            {
                            //                if (model.ShortInfoModel.ContactType != SettingsConstants.SPECIAL_CONTACT && model.FavoriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE)
                            //                    friendType = AddRemoveInCollections.Dictonary_FavouriteFriendList;
                            //                else if (model.ShortInfoModel.ContactType != SettingsConstants.SPECIAL_CONTACT && model.FavoriteRank < SettingsConstants.FORCE_FAVOURITE_VALUE && (model.NumberOfCalls > 0 || model.NumberOfChats > 0)) friendType = AddRemoveInCollections.Dictonary_TopFriendList;
                            //                else friendType = AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
                            //                model.FriendListType = friendType;
                            //            }

                            //            switch (friendType)
                            //            {
                            //                case AddRemoveInCollections.Dictonary_TopFriendList:
                            //                    if (!RingIDViewModel.Instance.TopFriendList.Contains(model)) AddRemoveInCollections.AddIntoTopFriendList(model);
                            //                    break;
                            //                case AddRemoveInCollections.Dictonary_FavouriteFriendList:
                            //                    if (!RingIDViewModel.Instance.FavouriteFriendList.Contains(model)) AddRemoveInCollections.AddIntoFavouriteFriendList(model);
                            //                    break;
                            //                case AddRemoveInCollections.Dictonary_NonFavouriteFriendList:
                            //                    if (!FriendListController.Instance.FriendDataContainer.NonFavoriteList.Any(P => P.RingID == userDTO.RingID)) FriendListController.Instance.FriendDataContainer.AddIntoNonFavoriteList(userDTO);
                            //                    break;
                            //            }
                            //            // }
                            //        }
                            //        else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
                            //        {
                            //            FriendListController.Instance.FriendDataContainer.AddIntoIncomingList(userDTO);
                            //            model.IncomingStatus = StatusConstants.INCOMING_REQUEST_UNREAD;
                            //            model.FriendListType = AddRemoveInCollections.Dictonary_IncomingRequestFriendList;
                            //            AppConstants.ADD_FRIEND_NOTIFICATION_COUNT++;
                            //        }
                            //        else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
                            //        {
                            //            FriendListController.Instance.FriendDataContainer.AddIntoPendingList(userDTO);
                            //            model.FriendListType = AddRemoveInCollections.Dictonary_OutgoingRequestFriendList;
                            //        }
                            //        else if (userDTO.FriendShipStatus == 0) model.FriendListType = AddRemoveInCollections.Dictonary_UnknownFriendList;
                            //    }
                            //}
                        }
                        else if (userDTO.Delete == 1) log.Info("\n\nFriend Deleted ****************************");
                    }
                    catch (Exception ex) { log.Error(ex.StackTrace); }
                    finally { }
                }

                #endregion //"Make Model from Dto and and into Friends Model Dictionary"

                #region "show First 100 freinds "
                FriendListController.Instance.FriendDataContainer.TotalFriends();
                FriendListController.Instance.FriendDataContainer.SortNonFavList();
                try
                {
                    for (var i = 0; i < FriendListController.Instance.FriendDataContainer.NonFavoriteList.Count; i++)
                    {
                        try
                        {
                            if (RingIDViewModel.Instance.NonFavouriteFriendList.Count >= 100) break;
                            System.Threading.Thread.Sleep(10);
                            UserBasicInfoDTO userDTO1 = FriendListController.Instance.FriendDataContainer.NonFavoriteList[i];
                            UserBasicInfoModel userbasicinforModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO1.UserTableID);
                            if (userbasicinforModel != null)
                            {
                                if (!RingIDViewModel.Instance.NonFavouriteFriendList.Contains(userbasicinforModel))
                                {
                                    userbasicinforModel.FriendListType = AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
                                    AddRemoveInCollections.AddIntoNonFavouriteFriendList(userbasicinforModel);
                                }
                            }
                            else log.Error("NO mOdel for UIID==>" + userDTO1.UserTableID);
                        }
                        catch (Exception ex) { log.Error("NO mOdel for UIID==>" + ex.StackTrace); }
                    }
                    FriendListLoadUtility.AddFriendsNotificationForIncomingRequest();
                }
                catch (Exception) { }
                #endregion ""
            }
            catch (Exception) { }
            finally
            {
                if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.WinDataModel != null) RingIDViewModel.Instance.WinDataModel.IsLoadingCotactList = false;
                running = false;
            }
        }

        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                running = true;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        public bool IsRunnig()
        {
            return running;
        }
        #endregion "Public methods"
    }
}

﻿using System;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.Utility.Services;

namespace View.Utility.Auth
{
    public class AuthRequestWithMessageNoSession
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AuthRequestWithMessageNoSession).Name);
        private JObject pakToSend;
        private int paketType;
        private int action;
        private int DefaultTryingTime;
        private AuthRequestNoSessionID authRequest;

        public AuthRequestWithMessageNoSession(JObject pakToSend2, int action1, int requestType, int tryingTime = 0)
        {
            this.pakToSend = pakToSend2;
            this.paketType = requestType;
            this.action = action1;
            if (tryingTime > 0)
            {
                DefaultTryingTime = DefaultSettings.TRYING_TIME;
                DefaultSettings.TRYING_TIME = tryingTime;
            }
        }

        public void Run(out bool isSuccess, out string msg)
        {
            isSuccess = false;
            msg = "";
            try
            {
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = action;
                authRequest = new AuthRequestNoSessionID();
                // JObject feedbackfields = SendAuthReqeust.SendRequestNoSessionID(pakToSend, paketType, packetId);
                JObject feedbackfields = authRequest.SendRequestNoSessionID(pakToSend, paketType, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.RequestCancelled] != null)
                    {
                        isSuccess = false;
                        msg = NotificationMessages.TEXT_CANCELED;
                    }
                    else
                    {
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success]) isSuccess = true;
                        if (feedbackfields[JsonKeys.Message] != null) msg = (string)feedbackfields[JsonKeys.Message];
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    }
                }
                else
                {
                    if (MainSwitcher.ThreadManager().PingNow())
                    {
                        isSuccess = false;
                        msg = NotificationMessages.CAN_NOT_PROCESS;
                        return;
                    }
                    else
                    {
                        isSuccess = false;
                        msg = NotificationMessages.INTERNET_UNAVAILABLE;
                        return;
                    }
                }
            }
            catch (Exception e) { log.Error("Error in AuthRequstWithMessage: type ==>" + this.paketType + e.StackTrace); }
            finally
            {
                if (DefaultTryingTime > 0) DefaultSettings.TRYING_TIME = DefaultTryingTime;
            }
        }

        public void Stop()
        {
            if (authRequest != null) authRequest.Stop();
        }
    }
}

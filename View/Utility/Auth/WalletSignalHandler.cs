﻿using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.Utility.Wallet;
using View.ViewModel;

namespace View.Utility.Auth
{
    public class WalletSignalHandler
    {
        private readonly ILog log = LogManager.GetLogger(typeof(WalletSignalHandler).Name);

        public void Process_TYPE_GET_WALLET_INFORMATION_1026(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
                    && _JobjFromResponse[WalletConstants.RSP_KEY_MY_WALLET_INFO] != null)
                {
                    // JObject
                    /*My Coin*/
                    JObject _jObjWalletInfo = (JObject)_JobjFromResponse[WalletConstants.RSP_KEY_MY_WALLET_INFO];
                    if (_jObjWalletInfo[WalletConstants.RSP_KEY_MY_COIN] != null)
                    {
                        JArray myCoin = (JArray)_jObjWalletInfo[WalletConstants.RSP_KEY_MY_COIN];
                        foreach (JObject jObject in myCoin)
                        {
                            WalletCoinModel model = WalletCoinModel.GetDataFromJson(jObject);
                            model.UserTableID = (long)_JobjFromResponse[WalletConstants.RSP_KEY_UTID];
                            //model.CoinId = 1;
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                            {
                                if (model.CoinId != WalletConstants.COIN_ID_DEFAULT)
                                {
                                    WalletViewModel.Instance.MyCoinStat = model;                                    
                                }
                            });

                        }
                    }
                    else
                    {
                        WalletViewModel.Instance.LoadDefaultMyCoins();
                    }
                    /*Bonus coin*/
                    //if (_jObjWalletInfo[JsonKeys.BonusCoin] != null)
                    //{
                    //    JArray bonusCoin = (JArray)_jObjWalletInfo[JsonKeys.BonusCoin];
                    //    foreach (JObject jObject in bonusCoin)
                    //    {
                    //        WalletCoinModel model = new WalletCoinModel();
                    //        if (jObject[JsonKeys.AvailableQuantity] != null)
                    //        {
                    //            model.AvailableQuantity = (int)jObject[JsonKeys.AvailableQuantity];
                    //        }
                    //        if (jObject[JsonKeys.CoinName] != null)
                    //        {
                    //            model.CoinName = (string)jObject[JsonKeys.CoinName];
                    //        }
                    //        if (jObject[JsonKeys.CoinId] != null)
                    //        {
                    //            model.CoinId = (int)jObject[JsonKeys.CoinId];
                    //        }
                    //        if (jObject[JsonKeys.TotalQuantity] != null)
                    //        {
                    //            model.TotalQuantity = (int)jObject[JsonKeys.TotalQuantity];
                    //        }
                    //        model.UserTableID = (long)_JobjFromResponse[JsonKeys.WUserTableId];

                    //        if (!WalletViewModel.Instance.BonusCoinList.Any(x => x.CoinId == model.CoinId))
                    //        {
                    //            WalletViewModel.Instance.BonusCoinList.InvokeAdd(model);
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    WalletViewModel.Instance.LoadDefaultBonusCoins();
                    //}
                    WalletViewModel.Instance.IsCoinStatsRequested = true;
                }
                else
                {
                    //if (WalletViewModel.Instance.MyCoinList.Count == 0)
                    //{
                    //WalletViewModel.Instance.LoadDefaultMyCoins();
                    //}
                    //if (WalletViewModel.Instance.BonusCoinList.Count == 0)
                    //{
                    //    WalletViewModel.Instance.LoadDefaultBonusCoins();
                    //}
                    WalletViewModel.Instance.CoinStatsError = true;
                }
                //WalletViewModel.Instance.IsCoinStatsLoading = false;
            }
            catch (Exception ex)
            {
                log.Error("WALLET INFO ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION_1027(JObject _JobjFromResponse)
        {
            try
            {
                //if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
                //    && _JobjFromResponse[JsonKeys.ExchangeRate] != null)
                //{
                //    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ExchangeRate];
                //    foreach (JObject jObject in jarray)
                //    {
                //        WalletExchangeRateModel model = WalletExchangeRateModel.LoadDataFromJson(jObject);
                //        if (!string.IsNullOrEmpty(model.CurrencyISOCode) && !WalletViewModel.Instance.ExchangeRateList.Any(x => x.CurrencyISOCode == model.CurrencyISOCode))
                //        {
                //            WalletViewModel.Instance.ExchangeRateList.InvokeAdd(model);
                //        }
                //    }
                //    if (WalletViewModel.Instance.ExchangeRateList.Count > 0)
                //    {
                //        WalletViewModel.Instance.IsExchangeRateRequested = true;
                //    }
                //    System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //    {
                //        if (View.UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel != null && View.UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel != null && View.UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel.IsVisible)
                //        {
                //            //   (View.UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel._CoinExchangeRatePanel.Child as View.UI.Wallet.UCCoinCurrencyExchangeStats)._ComboBoxCurrencyExchange.SelectedItem = WalletViewModel.Instance.ExchangeRateList.ElementAt(0);
                //        }
                //    });
                //}
                //else
                //{
                //    if (WalletViewModel.Instance.ExchangeRateList.Count == 0)
                //    {
                //        WalletViewModel.Instance.ExchangeRateError = true;
                //    }
                //}
                //WalletViewModel.Instance.IsExchangeRateLoading = false;
            }
            catch (Exception ex)
            {
                log.Error("ALL EXCHANGE RATE ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_TYPE_GET_TRANSACTION_HISTORY_1031(JObject _JobjFromResponse)
        {
            try
            {
                //if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.TransactionHistory] != null)
                //{
                //    int txnType = (int)_JobjFromResponse[JsonKeys.TransactionType];
                //    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.TransactionHistory];

                //    System.Collections.Generic.List<WalletCoinTransactionModel> list = new List<WalletCoinTransactionModel>();
                //    foreach (JObject txnObject in jArray)
                //    {
                //        WalletCoinTransactionModel model = new WalletCoinTransactionModel();
                //        if (txnObject[JsonKeys.TransactionID] != null)
                //        {
                //            model.TransactionID = (int)txnObject[JsonKeys.TransactionID];
                //        }
                //        if (txnObject[JsonKeys.TransactionDate] != null)
                //        {
                //            model.TransactionDate = (long)txnObject[JsonKeys.TransactionDate];
                //        }
                //        if (txnObject[JsonKeys.TransactionRemarks] != null)
                //        {
                //            model.TransactionRemarks = (string)txnObject[JsonKeys.TransactionRemarks];
                //        }
                //        if (txnObject[JsonKeys.TransactionTypeID] != null)
                //        {
                //            model.TransactionTypeID = (int)txnObject[JsonKeys.TransactionTypeID];
                //        }
                //        if (txnObject[JsonKeys.TransactionAmount] != null)
                //        {
                //            model.Amount = (int)txnObject[JsonKeys.TransactionAmount];
                //        }
                //        if (txnObject[JsonKeys.CoinId] != null)
                //        {
                //            model.CoinID = (int)txnObject[JsonKeys.CoinId];
                //        }
                //        if (txnObject[JsonKeys.OtherUserID] != null)
                //        {
                //            model.OtherUserID = (long)txnObject[JsonKeys.OtherUserID];
                //        }
                //        if (txnObject[JsonKeys.UserIdentity] != null)
                //        {
                //            model.RingID = (string)txnObject[JsonKeys.UserIdentity];
                //        }
                //        if (txnType == StatusConstants.WALLET_TRANSACTION_TYPE_SENT || txnType == StatusConstants.WALLET_TRANSACTION_TYPE_RECEIVED)
                //        {
                //            //make usershortinfomodel
                //            model.ShortInfoModel = new UserShortInfoModel();
                //            model.ShortInfoModel.LoadFromTransactionHistory(txnObject);
                //        }
                //        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //        {
                //            lock (WalletViewModel.Instance.SentTransactionHistory)
                //            {
                //                if (txnType == StatusConstants.WALLET_TRANSACTION_TYPE_SENT && !WalletViewModel.Instance.SentTransactionHistory.Any(x => x.TransactionID == model.TransactionID))
                //                {
                //                    WalletViewModel.Instance.SentTransactionHistory.Add(model);
                //                }
                //            }
                //        });

                //        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //        {
                //            lock (WalletViewModel.Instance.ReceivedTransactionHistory)
                //            {
                //                if (txnType == StatusConstants.WALLET_TRANSACTION_TYPE_RECEIVED && !WalletViewModel.Instance.ReceivedTransactionHistory.Any(x => x.TransactionID == model.TransactionID))
                //                {
                //                    WalletViewModel.Instance.ReceivedTransactionHistory.Add(model);
                //                }
                //            }
                //        });

                //        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //        {
                //            lock (WalletViewModel.Instance.BonusMigrationTransactionHistory)
                //            {
                //                if (txnType == StatusConstants.WALLET_TRANSACTION_TYPE_BONUS_MIGRATION && !WalletViewModel.Instance.BonusMigrationTransactionHistory.Any(x => x.TransactionID == model.TransactionID))
                //                {
                //                    WalletViewModel.Instance.BonusMigrationTransactionHistory.Add(model);
                //                }
                //            }
                //        });
                //    }

                //    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //    {
                //        switch (txnType)
                //        {
                //            case StatusConstants.WALLET_TRANSACTION_TYPE_SENT:
                //                WalletViewModel.Instance.IsSentLoading = false;
                //                break;
                //            case StatusConstants.WALLET_TRANSACTION_TYPE_RECEIVED:
                //                WalletViewModel.Instance.IsReceivedLoading = false;
                //                break;
                //            case StatusConstants.WALLET_TRANSACTION_TYPE_BONUS_MIGRATION:
                //                WalletViewModel.Instance.IsBonusLoading = false;
                //                break;
                //        }
                //    });
                //}
                //else
                //{
                //    int txnType = (int)_JobjFromResponse[JsonKeys.TransactionType];
                //    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //    {
                //        switch (txnType)
                //        {
                //            case StatusConstants.WALLET_TRANSACTION_TYPE_SENT:
                //                WalletViewModel.Instance.IsSentLoading = false;
                //                WalletViewModel.Instance.NoMoreSent = true;
                //                break;
                //            case StatusConstants.WALLET_TRANSACTION_TYPE_RECEIVED:
                //                WalletViewModel.Instance.IsReceivedLoading = false;
                //                WalletViewModel.Instance.NoMoreReceived = true;
                //                break;
                //            case StatusConstants.WALLET_TRANSACTION_TYPE_BONUS_MIGRATION:
                //                WalletViewModel.Instance.IsBonusLoading = false;
                //                WalletViewModel.Instance.NoMoreBonus = true;
                //                break;
                //        }
                //    });
                //}
            }
            catch (Exception ex)
            {
                log.Error("GET TRANSACTION HISTORY ==> " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public void PROCESS_TYPE_UPDATE_REFERRAL_REQUEST_1037(JObject jObject)
        {
            try
            {
                //if (jObject[JsonKeys.Success] != null && (bool)jObject[JsonKeys.Success])
                //{
                //    View.BindingModels.WalletReferralModel model = BindingModels.WalletReferralModel.LoadData(jObject);
                //    if (model != null)
                //    {
                //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //        {
                //            if (UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel != null && UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel != null)
                //            {
                //                if (WalletViewModel.Instance.IncomingRequestList.Count > 0)
                //                {
                //                    WalletViewModel.Instance.IncomingRequestList.Clear();
                //                }
                //                WalletViewModel.Instance.IncomingRequestList.InvokeAdd(model);
                //            }
                //        });
                //    }
                //}
            }
            catch (Exception) { }
        }

        public void PROCESS_TYPE_GET_WALLET_GIFT_1039(JObject jObject)
        {
            try
            {
                if (jObject[JsonKeys.Success] != null && (bool)jObject[JsonKeys.Success] && jObject[WalletConstants.RSP_KEY_PRODUCTS] != null)
                {
                    JArray giftArr = (JArray)jObject[WalletConstants.RSP_KEY_PRODUCTS];
                    foreach (JObject gift in giftArr)
                    {
                        WalletGiftProductModel newModel = WalletGiftProductModel.LoadDataFromJson(gift);
                        if (!WalletViewModel.Instance.GiftProductsList.Any(P => P.ProductID == newModel.ProductID))
                        {
                            int max = WalletViewModel.Instance.GiftProductsList.Count;
                            int min = 0;
                            int pivot = 0;

                            while (max > min)
                            {
                                pivot = (min + max) / 2;
                                double pivotVal = WalletViewModel.Instance.GiftProductsList.ElementAt(pivot).ProductPriceCoinQuantity;
                                if (newModel.ProductPriceCoinQuantity > pivotVal)
                                {
                                    min = pivot + 1;
                                }
                                else
                                {
                                    max = pivot;
                                }
                            }

                            WalletViewModel.Instance.GiftProductsList.InvokeInsert(min, newModel);
                        }
                    }
                    //System.Diagnostics.Debug.WriteLine("GIFT_1039 ==> " + WalletViewModel.Instance.GiftProductsList.Count);
                }
                else
                {
                    //no more
                    log.Info("Wallet gift request response = false");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at PROCESS_TYPE_GET_WALLET_GIFT_1039 => " + ex.Message + " " + ex.StackTrace);
            }
        }

        public void PROCESS_TYPE_GET_WALLET_COIN_BUNDLE_LIST_1038(JObject jObject)
        {
            if ((bool)jObject[JsonKeys.Success] && jObject[WalletConstants.RSP_KEY_COIN_BUNDLES] != null && ((JArray)jObject[WalletConstants.RSP_KEY_COIN_BUNDLES]).Count > 0)
            {
                JArray jArr = (JArray)jObject[WalletConstants.RSP_KEY_COIN_BUNDLES];
                foreach (JObject coinBundleItem in jArr)
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        WalletCoinBundleModel model = WalletCoinBundleModel.LoadDataFromJson(coinBundleItem);
                        if (model.BundleTypeID != 0 && !WalletViewModel.Instance.CoinBundles.Any(x => x.CoinBundleID == model.CoinBundleID))
                        {
                            WalletViewModel.Instance.CoinBundles.Add(model);
                        }
                    });
                }
            }
            else
            {
                //WalletViewModel.Instance.CoinBundleListReload = jObject["message"] != null && jObject["message"].ToString().ToLower().Contains("error") ? true : false;
                WalletViewModel.Instance.CoinBundleListReload = true;
            }
        }

        public void PROCESS_TYPE_PAYMENT_RECEIVED_1046(JObject jObject)
        {
            if (jObject[JsonKeys.Success] != null && (bool)jObject[JsonKeys.Success])
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                {
                    MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                    App.ShowToFront(View.Constants.RingIDSettings.APP_NAME);
                    UIHelperMethods.ShowMessageWithTimerFromNonThread(UI.UCGuiRingID.Instance.MotherPanel, "You have purchased coin succcessfully!");
                    //new ThreadGetWalletInformation().StartThread();
                    if (jObject[WalletConstants.RSP_KEY_MY_WALLET_INFO] != null)
                    {
                        JObject walletInfo = (JObject)jObject[WalletConstants.RSP_KEY_MY_WALLET_INFO];
                        if (walletInfo[WalletConstants.RSP_KEY_MY_COIN] != null)
                        {
                            JArray myCoins = (JArray)walletInfo[WalletConstants.RSP_KEY_MY_COIN];
                            foreach (JObject myCoin in myCoins)
                            {
                                WalletCoinModel model = WalletCoinModel.GetDataFromJson(myCoin);
                                model.UserTableID = DefaultSettings.userProfile.UserTableID;
                                WalletViewModel.Instance.MyCoinStat = model;
                            }
                        }
                    }
                    else
                    {
                        new ThreadGetWalletInformation().StartThread();
                    }
                });
            }
            else // fail
            {

            }
        }

        public void PROCESS_TYPE_GET_COIN_EARNING_RULE_LIST_1045(JObject jObject)
        {
            if (jObject[JsonKeys.Success] != null && (bool)jObject[JsonKeys.Success] && jObject[WalletConstants.RSP_KEY_RULES_LIST] != null && ((JArray)jObject[WalletConstants.RSP_KEY_RULES_LIST]).Count > 0)
            {
                JArray ruleArr = (JArray)jObject[WalletConstants.RSP_KEY_RULES_LIST];
                foreach (JObject rule in ruleArr)
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        WalletReferralRuleListModel model = WalletReferralRuleListModel.LoadDataFromJson(rule);
                        lock (WalletViewModel.Instance.CoinEarningRules)
                        {
                            if (model.RuleItemID != 0 && !WalletViewModel.Instance.CoinEarningRules.Any(x => x.RuleItemID == model.RuleItemID))
                            {
                                WalletViewModel.Instance.CoinEarningRules.Add(model);
                            }
                        }
                    });
                }
            }
            else//no data
            {
                System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                {
                    WalletViewModel.Instance.RuleListReloadRequire = true;
                });
            }
        }

        public void PROCESS_TYPE_REFERRAL_NETWORK_SUMMARY_1041(JObject _jObjFromResponse)
        {
            if (_jObjFromResponse[JsonKeys.Success] != null && (bool)_jObjFromResponse[JsonKeys.Success])
            {
                if (_jObjFromResponse[WalletConstants.RSP_KEY_MY_REFERRER_STAT] != null)
                {
                    // two state...either set or skipped
                    JObject myReferrer = (JObject)_jObjFromResponse[WalletConstants.RSP_KEY_MY_REFERRER_STAT];
                    if ((long)myReferrer[WalletConstants.RSP_KEY_REFERRER_ID] > 0) //set already
                    {
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            WalletViewModel.Instance.Referrer = new UserBasicInfoModel();
                            WalletViewModel.Instance.Referrer.ShortInfoModel.UserTableID = (long)myReferrer[WalletConstants.RSP_KEY_REFERRER_ID];
                            WalletViewModel.Instance.Referrer.ShortInfoModel.UserIdentity = (long)myReferrer[JsonKeys.UserIdentity];
                            WalletViewModel.Instance.Referrer.ShortInfoModel.FullName = (string)myReferrer[JsonKeys.FullName];
                            WalletViewModel.Instance.Referrer.ShortInfoModel.ProfileImage = (string)myReferrer[JsonKeys.ProfileImage];

                            WalletViewModel.Instance.IsReferrerSet = true;
                            WalletViewModel.Instance.IsReferrerSkipped = false;
                        });


                    }
                    else // skipped
                    {
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            WalletViewModel.Instance.IsReferrerSkipped = true;
                            WalletViewModel.Instance.IsReferrerSet = false;
                        });
                    }
                }
                else
                {
                    WalletViewModel.Instance.IsReferrerSet = false;
                }
                //WalletViewModel.Instance.IsReferrerSkipped = false;
                WalletViewModel.Instance.ReferralCount = _jObjFromResponse[WalletConstants.RSP_KEY_REFERRAL_COUNT]  != null? (int)_jObjFromResponse[WalletConstants.RSP_KEY_REFERRAL_COUNT] : 0;
                WalletViewModel.Instance.ReferrerResponseSuccess = true;
            }
            else//succ false
            {
                WalletViewModel.Instance.ReferrerResponseSuccess = false;
            }
        }

        public void PROCESS_TYPE_MY_REFERRAL_LIST_1042(JObject _jObjFromResponse)
        {
            if (_jObjFromResponse[JsonKeys.Success] != null && (bool)_jObjFromResponse[JsonKeys.Success] && _jObjFromResponse[WalletConstants.RSP_KEY_REFERRAL_LIST] != null)
            {
                JArray referralArray = (JArray)_jObjFromResponse[WalletConstants.RSP_KEY_REFERRAL_LIST];
                foreach (JObject referral in referralArray)
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        UserBasicInfoModel model = new UserBasicInfoModel();
                        model.ShortInfoModel.UserTableID = (long)referral[JsonKeys.UserTableID];
                        model.ShortInfoModel.UserIdentity = (long)referral[JsonKeys.UserIdentity];
                        model.ShortInfoModel.FullName = (string)referral[JsonKeys.FullName];
                        model.ShortInfoModel.ProfileImage = (string)referral[JsonKeys.ProfileImage];

                        lock (WalletViewModel.Instance.Referrals)
                        {
                            if (!WalletViewModel.Instance.Referrals.Any(x => x.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID))
                            {
                                WalletViewModel.Instance.Referrals.Add(model);
                            }
                        }
                    });

                }
                if (!WalletViewModel.Instance.InitialRequestReferralList) WalletViewModel.Instance.InitialRequestReferralList = true;
            }
            else // no data found
            {
                WalletViewModel.Instance.EndOfReferralList = true;
            }
        }

        public void PROCESS_TYPE_GET_DAILY_CHECKIN_HISTORY_1054(JObject _jObjFromResponse)
        {
            if (_jObjFromResponse[JsonKeys.Success] != null && (bool)_jObjFromResponse[JsonKeys.Success] && _jObjFromResponse[WalletCheckInHistoryModel.keyCheckInHistory] != null)
            {
                JArray checkInArray = (JArray)_jObjFromResponse[WalletCheckInHistoryModel.keyCheckInHistory];
                List<WalletCheckInHistoryModel> list = new List<WalletCheckInHistoryModel>();
                foreach (JObject checkInObj in checkInArray)
                {
                    WalletCheckInHistoryModel model = WalletCheckInHistoryModel.LoadDataFromJson(checkInObj);
                    //System.Diagnostics.Debug.WriteLine("CHECK IN HISTORY ==> " + model.ReportingDate + "==>" + model.CheckIn + " ==> " + model.ReportingDay + Environment.NewLine);
                    if (!list.Any(x => x.ReportingDate == model.ReportingDate))
                    {
                        list.Add(model);
                    }
                }
                System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                {
                    WalletViewModel.Instance.ProcessCheckInList(list.OrderBy(x => x.ReportingDate).ToList());
                });

            }
            else // succ false
            {

            }
        }

        public void PROCESS_TYPE_GIFT_RECEIVED_1052(JObject _jObjFromResponse)
        {
            if (_jObjFromResponse[JsonKeys.Success] != null && (bool)_jObjFromResponse[JsonKeys.Success])
            {

            }
        }
    }
}

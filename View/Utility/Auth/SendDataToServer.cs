<<<<<<< HEAD
﻿using System;
using System.Threading;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Auth
{
    public class SendDataToServer
    {
        #region Private Fields

        private static readonly ILog log = LogManager.GetLogger(typeof(SendDataToServer).Name);

        #endregion

        #region Public Static Methods

        public static void AuthRequestMethod(JObject pakToSend, int action, int requestType, out bool isSuccess, out string message)
        {
            int waitingTime = 0;
            isSuccess = false;
            message = "";
            if ((!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable))
            {
                try
                {
                    string packetId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = action;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, requestType, packetId);
                    if (feedbackfields != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        if (feedbackfields[JsonKeys.Message] != null)
                        {
                            message = (string)feedbackfields[JsonKeys.Message];
                        }
                        while (feedbackfields[JsonKeys.Action] != null && (int)feedbackfields[JsonKeys.Action] != action)
                        {
                            if (waitingTime == DefaultSettings.TRYING_TIME)
                            {
                                break;
                            }
                            Thread.Sleep(DefaultSettings.WAITING_TIME);
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(packetId, out feedbackfields);
                            waitingTime++;
                            if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                            {
                                if (feedbackfields[JsonKeys.Message] != null)
                                {
                                    message = (string)feedbackfields[JsonKeys.Message];
                                }
                                return;
                            }
                        }
                        isSuccess = true;
                        return;
                    }
                    else
                    {
                        isSuccess = false;
                        message = !MainSwitcher.ThreadManager().PingNow() ? NotificationMessages.CAN_NOT_PROCESS : NotificationMessages.INTERNET_UNAVAILABLE;
                    }
                }
                catch (Exception e)
                {
                    log.Error("Error in AuthRequstWithMessage==>" + e.Message + "\n" + e.StackTrace);
                }
            }
            else
            {
                log.Error("AuthRequstWithMessage Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
        }

        public static void SendMutualFreindRequest(long FrienduserTableID)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.FutId] = FrienduserTableID;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MUTUAL_FRIENDS, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SendFreindContactListRequest(long friendUtId, int StartLimit)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = friendUtId;
            pakToSend[JsonKeys.StartLimit] = StartLimit;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_FRIEND_CONTACT_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SendFreindContactListRequest(long friendUtId, long pvtid, int limit)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = friendUtId;
            pakToSend[JsonKeys.PivotId] = pvtid;
            pakToSend[JsonKeys.Limit] = limit;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141, AppConstants.REQUEST_TYPE_REQUEST);
        }
        public static void SendShareListRequest(Guid nfId)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.NewsfeedId] = nfId;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_SINGLE_FEED_SHARE_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }
        public static void SendAlbumRequest(long friendTableId, Guid friendAlbumId, Guid pivotUUID, int limit)
        {
            JObject pakToSend = new JObject();
            int action = AppConstants.TYPE_MY_ALBUM_IMAGES;
            if (friendTableId > 0)
            {                
                pakToSend[JsonKeys.FutId] = friendTableId;
            }
            if (friendAlbumId != Guid.Empty)
            {
                pakToSend[JsonKeys.AlbumId] = friendAlbumId;
            }
            if (pivotUUID != Guid.Empty)
            {
                pakToSend[JsonKeys.PivotUUID] = pivotUUID;
            }
            pakToSend[JsonKeys.Limit] = limit;
            (new AuthRequestNoResult()).StartThread(pakToSend, action, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SendAlbumRequest(long friendTableId, Guid friendAlbumId, int startLimit)
        {
            JObject pakToSend = new JObject();
            int action = AppConstants.TYPE_MY_ALBUM_IMAGES;
            if (friendTableId > 0)
            {               
                pakToSend[JsonKeys.FutId] = friendTableId;
            }
            pakToSend[JsonKeys.AlbumId] = friendAlbumId;

            pakToSend[JsonKeys.StartLimit] = startLimit;
            (new AuthRequestNoResult()).StartThread(pakToSend, action, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SendMoreFeedImagesRequest(Guid newsFeedID, int startIndex, int limit)
        {
            JObject pakToSend = new JObject();
            int action = AppConstants.TYPE_ACTION_GET_MORE_FEED_IMAGE;
            pakToSend[JsonKeys.NewsfeedId] = newsFeedID;
            pakToSend[JsonKeys.StartLimit] = startIndex;
            pakToSend[JsonKeys.Scroll] = 2;
            pakToSend[JsonKeys.Limit] = limit;
            (new AuthRequestNoResult()).StartThread(pakToSend, action, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ImageLikersOrImageCommentLikersList(Guid imageId, Guid commentId, long startLimit = 0) //Practise Named Parameter 
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ImageId] = imageId;
            pakToSend[JsonKeys.StartLimit] = startLimit;
            if (commentId != Guid.Empty)
            {
                pakToSend[JsonKeys.CommentId] = commentId;
            }
            (new AuthRequestNoResult()).StartThread(pakToSend, (commentId != Guid.Empty) ? AppConstants.TYPE_IMAGE_COMMENT_LIKES : AppConstants.TYPE_LIKES_FOR_IMAGE, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListMediaLikes(Guid contentId, Guid nfId, int st = 0)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ContentId] = contentId;
            if (nfId != Guid.Empty)
            {
                pakToSend[JsonKeys.NewsfeedId] = nfId;
            }
            pakToSend[JsonKeys.StartLimit] = st;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIA_LIKE_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListNewsPortalInfoCategories(long utId, int ProfileType)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = utId;
            pakToSend[JsonKeys.ProfileType] = ProfileType;
            pakToSend[JsonKeys.SubscribeType] = 0;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_NEWSPORTAL_CATEGORIES_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListMediaCommentLikes(Guid contentId, Guid commentId, Guid nfId, int startLimit = 0)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ContentId] = contentId;
            pakToSend[JsonKeys.CommentId] = commentId;
            pakToSend[JsonKeys.StartLimit] = startLimit;
            if (nfId != Guid.Empty)
            {
                pakToSend[JsonKeys.NewsfeedId] = nfId;
            }
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIACOMMENT_LIKE_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListMediaAlbumsOfaUser(long utId, int mediaType, Guid pivotUUId)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = utId;
            pakToSend[JsonKeys.MediaType] = mediaType;
            if (pivotUUId != Guid.Empty)
            {
                pakToSend[JsonKeys.PivotUUID] = pivotUUId;
            }
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIA_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListOfImageAlbumsOfUser(long utId, int mediaType, Guid pvtUUID)
        {
            JObject paktoSend = new JObject();
            if (utId != DefaultSettings.LOGIN_TABLE_ID)
            {
                paktoSend[JsonKeys.FutId] = utId;
            }
            if (pvtUUID != Guid.Empty)
            {
                paktoSend[JsonKeys.PivotUUID] = pvtUUID;
            }
            paktoSend[JsonKeys.Limit] = 10;
            (new AuthRequestNoResult()).StartThread(paktoSend, AppConstants.TYPE_IMAGE_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void MediaFullSearchByType(string searchSgtn, int searchtype, int scl, Guid pvtid)//later pvtid,scl has to be parameterized
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.SearchParam] = searchSgtn;
            pakToSend[JsonKeys.MediaSuggestionType] = searchtype;
            pakToSend[JsonKeys.Scroll] = scl;//1;//for now
            pakToSend[JsonKeys.PivotId] = pvtid;//0;//for now
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListHashTagContents(long hashTagId, int scl = 1, long pvtid = 0)//later pvtid,scl has to be parameterized
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.HashTagId] = hashTagId;
            pakToSend[JsonKeys.Scroll] = scl;//1;//for now
            pakToSend[JsonKeys.PivotId] = pvtid;//0;//for now
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void BreakingMediaCloudFeeds()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.Limit] = 10;
            pakToSend[JsonKeys.StartLimit] = 0;
            pakToSend[JsonKeys.MediaTrendingFeed] = SettingsConstants.MEDIA_FEED_TYPE_TRENDING;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIA_PAGE_TRENDING_FEED, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void WorkEducationSkillRequest(long? Utid)
        {
            JObject pakToSend = new JObject();
            if (Utid != null)
            {
                pakToSend[JsonKeys.UserTableID] = Utid;
            }
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_LIST_WORKS_EDUCATIONS_SKILLS, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SuggestionUsersDetailsRequest(JArray suggestionIdsList)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ContactUtIdList] = suggestionIdsList;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_USERS_DETAILS, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static JObject AddWorkInfo(WorkDTO work)
        {
            JObject workObject = new JObject();
            workObject[JsonKeys.CompanyName] = work.CompanyName;
            if (work.Position != null)
            {
                workObject[JsonKeys.Position] = work.Position;
            }
            if (work.City != null)
            {
                workObject[JsonKeys.City] = work.City;
            }
            if (work.Description != null)
            {
                workObject[JsonKeys.Description] = work.Description;
            }
            workObject[JsonKeys.FromTime] = work.FromTime;
            workObject[JsonKeys.ToTime] = work.ToTime;

            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.WorkObj] = workObject;
            return pakToSend;
        }

        public static JObject UpdateEducation(EducationDTO updatedEducation)
        {
            JObject educationObject = new JObject();
            educationObject[JsonKeys.SchoolId] = updatedEducation.Id;
            educationObject[JsonKeys.SchoolName] = updatedEducation.SchoolName;
            if (updatedEducation.Description != null) 
            { 
                educationObject[JsonKeys.Description] = updatedEducation.Description; 
            }
            educationObject[JsonKeys.FromTime] = updatedEducation.FromTime;
            educationObject[JsonKeys.ToTime] = updatedEducation.ToTime;
            educationObject[JsonKeys.Graduated] = updatedEducation.Graduated;
            educationObject[JsonKeys.IsSchool] = updatedEducation.IsSchool;
            
            educationObject[JsonKeys.AttendedFor] = updatedEducation.AttendedFor;
            if (updatedEducation.Degree != null) 
            { 
                educationObject[JsonKeys.Degree] = updatedEducation.Degree; 
            }
            if (updatedEducation.Concentration != null) 
            { 
                educationObject[JsonKeys.Concentration] = updatedEducation.Concentration; 
            }

            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.EducationObj] = educationObject;
            return pakToSend;
        }

        #endregion
    }
}
=======
﻿using System;
using System.Threading;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Auth
{
    public class SendDataToServer
    {
        #region Private Fields

        private static readonly ILog log = LogManager.GetLogger(typeof(SendDataToServer).Name);

        #endregion

        #region Public Static Methods

        public static void AuthRequestMethod(JObject pakToSend, int action, int requestType, out bool isSuccess, out string message)
        {
            int waitingTime = 0;
            isSuccess = false;
            message = "";
            if ((!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable))
            {
                try
                {
                    string packetId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = action;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, requestType, packetId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                        {
                        }
                        else
                        {
                            while (feedbackfields[JsonKeys.Action] != null && (int)feedbackfields[JsonKeys.Action] != action)
                            {
                                if (waitingTime == DefaultSettings.TRYING_TIME)
                                {
                                    break;
                                }
                                Thread.Sleep(DefaultSettings.WAITING_TIME);
                                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(packetId, out feedbackfields);
                                waitingTime++;
                                if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                                {
                                    if (feedbackfields[JsonKeys.Message] != null)
                                    {
                                        message = (string)feedbackfields[JsonKeys.Message];
                                    }
                                    return;
                                }
                            }
                            isSuccess = true;
                            return;
                        }
                    }
                    else
                    {
                        message = !MainSwitcher.ThreadManager().PingNow() ? NotificationMessages.CAN_NOT_PROCESS : NotificationMessages.INTERNET_UNAVAILABLE;
                    }
                    //if (feedbackfields != null && feedbackfields[JsonKeys.Success]  != null && (bool)feedbackfields[JsonKeys.Success])
                    //{
                    //    if (feedbackfields[JsonKeys.Message] != null)
                    //    {
                    //        message = (string)feedbackfields[JsonKeys.Message];
                    //    }
                    //    while (feedbackfields[JsonKeys.Action] != null && (int)feedbackfields[JsonKeys.Action] != action)
                    //    {
                    //        if (waitingTime == DefaultSettings.TRYING_TIME)
                    //        {
                    //            break;
                    //        }
                    //        Thread.Sleep(DefaultSettings.WAITING_TIME);
                    //        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(packetId, out feedbackfields);
                    //        waitingTime++;
                    //        if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                    //        {
                    //            if (feedbackfields[JsonKeys.Message] != null)
                    //            {
                    //                message = (string)feedbackfields[JsonKeys.Message];
                    //            }
                    //            return;
                    //        }
                    //    }
                    //    isSuccess = true;
                    //    return;
                    //}
                    //else
                    //{
                    //    isSuccess = false;
                    //    message = !MainSwitcher.ThreadManager().PingNow() ? NotificationMessages.CAN_NOT_PROCESS : NotificationMessages.INTERNET_UNAVAILABLE;
                    //}
                }
                catch (Exception e)
                {
                    log.Error("Error in AuthRequstWithMessage==>" + e.Message + "\n" + e.StackTrace);
                }
            }
            else
            {
                log.Error("AuthRequstWithMessage Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
        }

        public static void SendMutualFreindRequest(long FrienduserTableID)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.FutId] = FrienduserTableID;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MUTUAL_FRIENDS, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SendFreindContactListRequest(long friendUtId, int StartLimit)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = friendUtId;
            pakToSend[JsonKeys.StartLimit] = StartLimit;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_FRIEND_CONTACT_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SendFreindContactListRequest(long friendUtId, long pvtid, int limit)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = friendUtId;
            pakToSend[JsonKeys.PivotId] = pvtid;
            pakToSend[JsonKeys.Limit] = limit;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141, AppConstants.REQUEST_TYPE_REQUEST);
        }
        public static void SendShareListRequest(Guid nfId)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.NewsfeedId] = nfId;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_SINGLE_FEED_SHARE_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }
        public static void SendAlbumRequest(long friendTableId, Guid friendAlbumId, Guid pivotUUID, int limit)
        {
            JObject pakToSend = new JObject();
            int action = AppConstants.TYPE_MY_ALBUM_IMAGES;
            if (friendTableId > 0)
            {
                pakToSend[JsonKeys.FutId] = friendTableId;
            }
            if (friendAlbumId != Guid.Empty)
            {
                pakToSend[JsonKeys.AlbumId] = friendAlbumId;
            }
            if (pivotUUID != Guid.Empty)
            {
                pakToSend[JsonKeys.PivotUUID] = pivotUUID;
            }
            pakToSend[JsonKeys.Limit] = limit;
            (new AuthRequestNoResult()).StartThread(pakToSend, action, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SendAlbumRequest(long friendTableId, Guid friendAlbumId, int startLimit)
        {
            JObject pakToSend = new JObject();
            int action = AppConstants.TYPE_MY_ALBUM_IMAGES;
            if (friendTableId > 0)
            {
                pakToSend[JsonKeys.FutId] = friendTableId;
            }
            pakToSend[JsonKeys.AlbumId] = friendAlbumId;

            pakToSend[JsonKeys.StartLimit] = startLimit;
            (new AuthRequestNoResult()).StartThread(pakToSend, action, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SendMoreFeedImagesRequest(Guid newsFeedID, int startIndex, int limit)
        {
            JObject pakToSend = new JObject();
            int action = AppConstants.TYPE_ACTION_GET_MORE_FEED_IMAGE;
            pakToSend[JsonKeys.NewsfeedId] = newsFeedID;
            pakToSend[JsonKeys.StartLimit] = startIndex;
            pakToSend[JsonKeys.Scroll] = 2;
            pakToSend[JsonKeys.Limit] = limit;
            (new AuthRequestNoResult()).StartThread(pakToSend, action, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ImageLikersOrImageCommentLikersList(Guid imageId, Guid commentId, long startLimit = 0) //Practise Named Parameter 
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ImageId] = imageId;
            pakToSend[JsonKeys.StartLimit] = startLimit;
            if (commentId != Guid.Empty)
            {
                pakToSend[JsonKeys.CommentId] = commentId;
            }
            (new AuthRequestNoResult()).StartThread(pakToSend, (commentId != Guid.Empty) ? AppConstants.TYPE_IMAGE_COMMENT_LIKES : AppConstants.TYPE_LIKES_FOR_IMAGE, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListMediaLikes(Guid contentId, Guid nfId, int st = 0)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ContentId] = contentId;
            if (nfId != Guid.Empty)
            {
                pakToSend[JsonKeys.NewsfeedId] = nfId;
            }
            pakToSend[JsonKeys.StartLimit] = st;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIA_LIKE_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListNewsPortalInfoCategories(long utId, int ProfileType)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = utId;
            pakToSend[JsonKeys.ProfileType] = ProfileType;
            pakToSend[JsonKeys.SubscribeType] = 0;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_NEWSPORTAL_CATEGORIES_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListMediaCommentLikes(Guid contentId, Guid commentId, Guid nfId, int startLimit = 0)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ContentId] = contentId;
            pakToSend[JsonKeys.CommentId] = commentId;
            pakToSend[JsonKeys.StartLimit] = startLimit;
            if (nfId != Guid.Empty)
            {
                pakToSend[JsonKeys.NewsfeedId] = nfId;
            }
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIACOMMENT_LIKE_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListMediaAlbumsOfaUser(long utId, int mediaType, Guid pivotUUId)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.UserTableID] = utId;
            pakToSend[JsonKeys.MediaType] = mediaType;
            if (pivotUUId != Guid.Empty)
            {
                pakToSend[JsonKeys.PivotUUID] = pivotUUId;
            }
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIA_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListOfImageAlbumsOfUser(long utId, int mediaType, Guid pvtUUID)
        {
            JObject paktoSend = new JObject();
            if (utId != DefaultSettings.LOGIN_TABLE_ID)
            {
                paktoSend[JsonKeys.FutId] = utId;
            }
            if (pvtUUID != Guid.Empty)
            {
                paktoSend[JsonKeys.PivotUUID] = pvtUUID;
            }
            paktoSend[JsonKeys.Limit] = 10;
            (new AuthRequestNoResult()).StartThread(paktoSend, AppConstants.TYPE_IMAGE_ALBUM_LIST, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void MediaFullSearchByType(string searchSgtn, int searchtype, int scl, Guid pvtid)//later pvtid,scl has to be parameterized
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.SearchParam] = searchSgtn;
            pakToSend[JsonKeys.MediaSuggestionType] = searchtype;
            pakToSend[JsonKeys.Scroll] = scl;//1;//for now
            pakToSend[JsonKeys.PivotId] = pvtid;//0;//for now
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void ListHashTagContents(long hashTagId, int scl = 1, long pvtid = 0)//later pvtid,scl has to be parameterized
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.HashTagId] = hashTagId;
            pakToSend[JsonKeys.Scroll] = scl;//1;//for now
            pakToSend[JsonKeys.PivotId] = pvtid;//0;//for now
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_HASHTAG_MEDIA_CONTENTS, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void BreakingMediaCloudFeeds()
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.Limit] = 10;
            pakToSend[JsonKeys.StartLimit] = 0;
            pakToSend[JsonKeys.MediaTrendingFeed] = SettingsConstants.MEDIA_FEED_TYPE_TRENDING;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_MEDIA_PAGE_TRENDING_FEED, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void WorkEducationSkillRequest(long? Utid)
        {
            JObject pakToSend = new JObject();
            if (Utid != null)
            {
                pakToSend[JsonKeys.UserTableID] = Utid;
            }
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_LIST_WORKS_EDUCATIONS_SKILLS, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static void SuggestionUsersDetailsRequest(JArray suggestionIdsList)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ContactUtIdList] = suggestionIdsList;
            (new AuthRequestNoResult()).StartThread(pakToSend, AppConstants.TYPE_USERS_DETAILS, AppConstants.REQUEST_TYPE_REQUEST);
        }

        public static JObject AddWorkInfo(WorkDTO work)
        {
            JObject workObject = new JObject();
            workObject[JsonKeys.CompanyName] = work.CompanyName;
            if (work.Position != null)
            {
                workObject[JsonKeys.Position] = work.Position;
            }
            if (work.City != null)
            {
                workObject[JsonKeys.City] = work.City;
            }
            if (work.Description != null)
            {
                workObject[JsonKeys.Description] = work.Description;
            }
            workObject[JsonKeys.FromTime] = work.FromTime;
            workObject[JsonKeys.ToTime] = work.ToTime;

            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.WorkObj] = workObject;
            return pakToSend;
        }

        public static JObject UpdateEducation(EducationDTO updatedEducation)
        {
            JObject educationObject = new JObject();
            educationObject[JsonKeys.SchoolId] = updatedEducation.Id;
            educationObject[JsonKeys.SchoolName] = updatedEducation.SchoolName;
            if (updatedEducation.Description != null)
            {
                educationObject[JsonKeys.Description] = updatedEducation.Description;
            }
            educationObject[JsonKeys.FromTime] = updatedEducation.FromTime;
            educationObject[JsonKeys.ToTime] = updatedEducation.ToTime;
            educationObject[JsonKeys.Graduated] = updatedEducation.Graduated;
            educationObject[JsonKeys.IsSchool] = updatedEducation.IsSchool;

            educationObject[JsonKeys.AttendedFor] = updatedEducation.AttendedFor;
            if (updatedEducation.Degree != null)
            {
                educationObject[JsonKeys.Degree] = updatedEducation.Degree;
            }
            if (updatedEducation.Concentration != null)
            {
                educationObject[JsonKeys.Concentration] = updatedEducation.Concentration;
            }

            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.EducationObj] = educationObject;
            return pakToSend;
        }

        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using System;
using System.Collections.Generic;
using System.Threading;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace View.Utility.Auth
{
    public class SendAuthReqeust
    {
        #region "Fields"
        private readonly static log4net.ILog log = log4net.LogManager.GetLogger(typeof(SendAuthReqeust).Name);
        #endregion "Fields"

        #region "Send Data"

        private static JObject SendData(JObject packetToSend, int requestType, string packetID, int firstDelay = 25)
        {
            JObject feedBackFields = null;
            try
            {
                string data = JsonConvert.SerializeObject(packetToSend, Formatting.None);
                SendToServer.SendNormalPacket(requestType, data);
                Thread.Sleep(firstDelay);
                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                {
                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)) break;
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(packetID))
                    {
                        if (i % DefaultSettings.SEND_INTERVAL == 0) SendToServer.SendNormalPacket(requestType, data);
                    }
                    else
                    {
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(packetID, out feedBackFields);
                        break;
                    }
                }
            }
            catch (Exception e) { log.Error("ChangeMood ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message); }
            return feedBackFields;
        }

        private static JObject SendData(JObject packetToSend, int requestType, string packetID, bool needToSendPingIfFail)
        {
            JObject feedBackFields = null;
            try
            {
                string data = JsonConvert.SerializeObject(packetToSend, Formatting.None);
                SendToServer.SendNormalPacket(requestType, data);
                Thread.Sleep(25);
                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                {
                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)) break;
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(packetID))
                    {
                        if (i % DefaultSettings.SEND_INTERVAL == 0) SendToServer.SendNormalPacket(requestType, data);
                    }
                    else
                    {
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(packetID, out feedBackFields);
                        break;
                    }
                }
            }
            catch (Exception e) { log.Error("ChangeMood ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message); }
            if (feedBackFields == null && needToSendPingIfFail) MainSwitcher.ThreadManager().PingNow();
            return feedBackFields;
        }

        private static JObject SendDataNoSession(JObject packetToSend, int requestType, string packetID, int firstDelay = 25)
        {
            JObject feedBackFields = null;
            try
            {
                string data = JsonConvert.SerializeObject(packetToSend, Formatting.None);
                SendToServer.SendNormalPacket(requestType, data);
                Thread.Sleep(firstDelay);
                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                {
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(packetID))
                    {
                        if (i % DefaultSettings.SEND_INTERVAL == 0) SendToServer.SendNormalPacket(requestType, data);
                    }
                    else
                    {
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(packetID, out feedBackFields);
                        break;
                    }
                }
            }
            catch (Exception e) { log.Error("ChangeMood ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message); }
            return feedBackFields;
        }

        #endregion "Send Data"

        #region "Public Methods"

        public static JObject Send(JObject pakToSend, int requestType, string packetID, int firstDelay = 25)
        {
            JObject feedBackFields = null;
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                feedBackFields = SendData(pakToSend, requestType, packetID, firstDelay);
                if (feedBackFields == null) log.Error(" Send Failed ==> Response not found");
            }
            else log.Error(" Send Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            return feedBackFields;
        }

        public static JObject Send(JObject pakToSend, int requestType, string packetID, bool isNeedSendPing)
        {
            JObject feedBackFields = null;
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                feedBackFields = SendData(pakToSend, requestType, packetID, isNeedSendPing);
                if (feedBackFields == null) log.Error(" Send Failed ==> Response not found");
            }
            else log.Error(" Send Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            return feedBackFields;
        }

        public static JObject SendRequestNoSessionID(JObject pakToSend, int requestType, string packetID, int firstDelay = 25)
        {
            return SendDataNoSession(pakToSend, requestType, packetID, firstDelay);
        }

        public static JObject SendBrokenPacket(JObject packetToSend, int requestType, string packetID)
        {
            JObject feedBackFields = null;
            try
            {
                string data = JsonConvert.SerializeObject(packetToSend, Formatting.None);
                Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, (int)packetToSend[JsonKeys.Action], packetID);
                int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
                List<string> packetIds = new List<string>(packets.Keys);
                SendToServer.SendBrokenPacket(packetType, packets);
                Thread.Sleep(25);
                string makeResponsepak = packetID + "_" + (int)packetToSend[JsonKeys.Action];
                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                {
                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)) break;
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsepak))
                    {
                        if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
                        {
                            if (i % DefaultSettings.SEND_INTERVAL == 0) SendToServer.SendBrokenPacket(packetType, packets);
                        }
                    }
                    else
                    {
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsepak, out feedBackFields);
                        break;
                    }
                }
            }
            catch (Exception e) { log.Error("ChangeMood ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message); }
            return feedBackFields;
        }

        #endregion "Public Mehods"
    }
}

﻿using System;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Auth
{
    public class AuthRequstWithMessage
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AuthRequstWithMessage).Name);
        JObject pakToSend;
        int paketType;
        int action;
        public AuthRequstWithMessage(JObject pakToSend2, int action1, int requestType)
        {
            this.pakToSend = pakToSend2;
            this.paketType = requestType;
            this.action = action1;
        }
        public void Run(out bool isSuccess, out string msg)
        {
            isSuccess = false;
            msg = "";
            if (DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    string packetId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = action;


                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, paketType, packetId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            isSuccess = true;
                        }
                        if (feedbackfields[JsonKeys.Message] != null)
                        {
                            msg = (string)feedbackfields[JsonKeys.Message];
                        }
                        if (feedbackfields[JsonKeys.MSG] != null)
                        {
                            msg = (string)feedbackfields[JsonKeys.MSG];
                        }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    }
                    else
                    {
                        if (MainSwitcher.ThreadManager().PingNow())
                        {
                            isSuccess = false; msg = NotificationMessages.CAN_NOT_PROCESS;
                        }
                        else
                        {
                            isSuccess = false; msg = NotificationMessages.INTERNET_UNAVAILABLE;
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error("Error in AuthRequstWithMessage: type ==>" + this.paketType + "==>" + e.Message + "\n" + e.StackTrace);
                }
            }
            else
                msg = NotificationMessages.INTERNET_UNAVAILABLE;
        }
    }
}

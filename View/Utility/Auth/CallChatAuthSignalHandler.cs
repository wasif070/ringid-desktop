﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;

using callsdkwrapper;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Models.Utility.Chat;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.Utility.Call;
using View.Utility.Call.Settings;
using View.Utility.Chat.Service;
using View.Utility.FriendProfile;
using View.ViewModel;
using View.Utility.Stream;

namespace View.Utility.Auth
{
    public class CallChatAuthSignalHandler
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(CallChatAuthSignalHandler).Name);
        #endregion "Private Fields"

        #region "Signale Handler Methods"

        public void Process_UPDATE_SEND_REGISTER_374(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                try
                {
                    CallerDTO caller = new CallerDTO();
                    CallHelperMethods.ParseCall_174_374(_JobjFromResponse, caller);
                    ProcessUpdateSendRegister(caller);
                }
                catch (Exception e)
                {

                    log.Error("ProcessUpdateSendRegister ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

                }
            }
        }

        public void Process_START_GROUP_CHAT_134(JObject _JobjFromResponse, int action, long server_packet_id, string client_packet_id)
        {
            processStartGroupChat(_JobjFromResponse, action, server_packet_id);
        }

        public void Process_UPDATE_START_GROUP_CHAT_334(JObject _JobjFromResponse, int action, long server_packet_id, string client_packet_id)
        {
            processStartGroupChat(_JobjFromResponse, action, server_packet_id);
        }

        public void Process_UPDATE_ADD_GROUP_MEMBER_335(JObject _JobjFromResponse, int action, long server_packet_id, string client_packet_id)
        {
            processStartGroupChat(_JobjFromResponse, action, server_packet_id);
        }

        public void Process_UPDATE_START_FRIEND_CHAT_375(JObject _JobjFromResponse, int action, long server_packet_id, string client_packet_id)
        {
            processStartFriendChat(_JobjFromResponse, action, server_packet_id);
        }

        public void Process_START_FRIEND_CHAT_175(JObject _JobjFromResponse, int action, long server_packet_id, string client_packet_id)
        {
            processStartFriendChat(_JobjFromResponse, action, server_packet_id);
        }

        public void PROCESS_TYPE_START_LIVE_STREAM_REQUEST_2001(JObject _JobjFromResponse)
        {
            processStartLiveStream(_JobjFromResponse);
        }

        #endregion "Signal Handler methods"

        #region "Chat Utility Methods"

        private void processStartFriendChat(JObject _JobjFromResponse, int action, long server_packet_id)//175-375
        {
            try
            {
                if (_JobjFromResponse != null)
                {
                    ChatInitiatorDTO initiatorDTO = new ChatInitiatorDTO();
                    initiatorDTO.ActionType = (action == AppConstants.TYPE_START_FRIEND_CHAT && server_packet_id == 0) ? AppConstants.TYPE_START_FRIEND_CHAT : AppConstants.TYPE_UPDATE_START_CHAT;
                    initiatorDTO.FriendTableID = _JobjFromResponse[JsonKeys.FutId] != null ? ModelUtility.ConvertToLong(_JobjFromResponse[JsonKeys.FutId]) : 0;
                    initiatorDTO.Presence = _JobjFromResponse[JsonKeys.Presence] != null ? (int)_JobjFromResponse[JsonKeys.Presence] : 0;
                    initiatorDTO.Mood = _JobjFromResponse[JsonKeys.Mood] != null ? (int)_JobjFromResponse[JsonKeys.Mood] : 0;
                    initiatorDTO.LastOnlineTime = _JobjFromResponse[JsonKeys.LastOnlineTime] != null ? (long)_JobjFromResponse[JsonKeys.LastOnlineTime] : 0;
                    initiatorDTO.ReasonCode = _JobjFromResponse[JsonKeys.ReasonCode] != null ? (int)_JobjFromResponse[JsonKeys.ReasonCode] : 0;
                    initiatorDTO.ChatServerIP = (string)_JobjFromResponse[JsonKeys.ChatServerIp];
                    initiatorDTO.ChatRegisterPort = _JobjFromResponse[JsonKeys.ChatRegistrationPort] != null ? (int)_JobjFromResponse[JsonKeys.ChatRegistrationPort] : 0;
                    initiatorDTO.Device = _JobjFromResponse[JsonKeys.Device] != null ? (int)_JobjFromResponse[JsonKeys.Device] : 0;
                    initiatorDTO.DeviceToken = (string)_JobjFromResponse[JsonKeys.DeviceToken];
                    initiatorDTO.FullName = (string)_JobjFromResponse[JsonKeys.Name];
                    initiatorDTO.ApplicationType = _JobjFromResponse[JsonKeys.AppType] != null ? (int)_JobjFromResponse[JsonKeys.AppType] : 0;
                    initiatorDTO.RemotePushType = _JobjFromResponse[JsonKeys.RemotePushType] != null ? (int)_JobjFromResponse[JsonKeys.RemotePushType] : 1;
                    initiatorDTO.BlockByList = _JobjFromResponse[JsonKeys.BlockBy] != null ? ((JArray)_JobjFromResponse[JsonKeys.BlockBy]).ToObject<List<long>>() : null;
                    initiatorDTO.AnonymousBlockByList = _JobjFromResponse[JsonKeys.AnonymousBlockBy] != null ? ((JArray)_JobjFromResponse[JsonKeys.AnonymousBlockBy]).ToObject<List<long>>() : null;
                    initiatorDTO.Time = _JobjFromResponse[JsonKeys.Time] != null ? (long)_JobjFromResponse[JsonKeys.Time] : 0;
                    initiatorDTO.ServerPacketID = server_packet_id;

                    ProcessStartFriendChat(initiatorDTO);
                }
            }
            catch (Exception e)
            {

                log.Error("ProcessUpdateStartFriendChat ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }

        private void processStartGroupChat(JObject _JobjFromResponse, int action, long server_packet_id)//134-334-335
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    ChatInitiatorDTO initiatorDTO = new ChatInitiatorDTO();
                    initiatorDTO.ActionType = action == AppConstants.TYPE_START_GROUP_CHAT && server_packet_id == 0 ? AppConstants.TYPE_START_GROUP_CHAT : action == AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER ? AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER : AppConstants.TYPE_UPDATE_START_GROUP_CHAT;
                    initiatorDTO.ReasonCode = _JobjFromResponse[JsonKeys.ReasonCode] != null ? (int)_JobjFromResponse[JsonKeys.ReasonCode] : 0;
                    initiatorDTO.ChatServerIP = (string)_JobjFromResponse[JsonKeys.ChatServerIp];
                    initiatorDTO.ChatRegisterPort = _JobjFromResponse[JsonKeys.ChatRegistrationPort] != null ? (int)_JobjFromResponse[JsonKeys.ChatRegistrationPort] : 0;
                    initiatorDTO.GroupID = _JobjFromResponse[JsonKeys.TagId] != null ? (long)_JobjFromResponse[JsonKeys.TagId] : 0;
                    initiatorDTO.Time = _JobjFromResponse[JsonKeys.Time] != null ? (long)_JobjFromResponse[JsonKeys.Time] : 0;
                    initiatorDTO.ServerPacketID = server_packet_id;

                    ProcessStartGroupChat(initiatorDTO);
                }
            }
            catch (Exception e)
            {

                log.Error("ProcessUpdateStartGroupChat ex==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }

        private void processStartLiveStream(JObject _JobjFromResponse) //2001
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamDetails] != null)
            {
                StreamDTO streamingDTO = StreamSignalHandler.MakeStreamDTO((JObject)_JobjFromResponse[JsonKeys.StreamDetails]);
                ProcessStartLiveStream(streamingDTO);
            }
        }

        public void ProcessStartFriendChat(ChatInitiatorDTO initiatorDTO, Func<long, bool, int> onComplete = null)
        {
            try
            {
#if CHAT_LOG
                log.Debug("AUTH::FRIEND::RESPONSE".PadRight(58, ' ') + "==>   initiatorDTO = " + initiatorDTO.ToString());
#endif
                if (initiatorDTO.FriendTableID <= 0)
                {
                    if (onComplete != null)
                    {
                        onComplete(initiatorDTO.FriendTableID, false);
                    }
                    return;
                }

                UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(initiatorDTO.FriendTableID, 0, initiatorDTO.FullName);
                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(initiatorDTO.FriendTableID);

                if (initiatorDTO.ReasonCode == ReasonCodeConstants.REASON_CODE_PERMISSION_DENIED)
                {
                    log.Debug("AUTH::FRIEND::REGISTER_DENIED_FOR_PERMISSION".PadRight(58, ' ') + "==>   friendId = " + initiatorDTO.FriendTableID);

                    if (initiatorDTO.BlockByList != null && initiatorDTO.BlockByList.Count > 0)
                    {
                        if (initiatorDTO.BlockByList.Contains(DefaultSettings.LOGIN_TABLE_ID))
                        {
                            ThradFriendDetailsInfoRequest request = new ThradFriendDetailsInfoRequest();
                            request.StartThread(friendInfoModel.ShortInfoModel.UserTableID);
                        }
                        else
                        {
                            blockModel.IsFriendRegDenied = true;
                        }
                    }
                    else if (initiatorDTO.AnonymousBlockByList != null && initiatorDTO.AnonymousBlockByList.Count > 0)
                    {
                        if (initiatorDTO.AnonymousBlockByList.Contains(DefaultSettings.LOGIN_TABLE_ID))
                        {
                            RingIDViewModel.Instance.MyBasicInfoModel.AnonymousChatAccess = StatusConstants.CHAT_CALL_FRIENDS_ONLY;
                            DefaultSettings.userProfile.AnonymousChatAccess = StatusConstants.CHAT_CALL_FRIENDS_ONLY;
                        }
                        else
                        {
                            blockModel.IsFriendRegDenied = true;
                        }
                    }
                    else
                    {
                        blockModel.IsFriendRegDenied = true;
                    }

                    ChatService.DontSendPendingMessages(initiatorDTO.FriendTableID);
                    ChatService.UnRegisterFriendChat(initiatorDTO.FriendTableID, StatusConstants.PRESENCE_ONLINE, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.Mood);

                    if (onComplete != null)
                    {
                        onComplete(initiatorDTO.FriendTableID, false);
                    }
                    return;
                }

#if CHAT_LOG
                log.Debug("AUTH::FRIEND::LOCK_REG_EVENT".PadRight(58, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif
                lock (ChatEventHandler.CHAT_REG_EVENT_LOCKER)
                {
                    ChatEventHandler eventhandler = ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryGetValue(initiatorDTO.FriendTableID.ToString());
                    if (eventhandler == null)
                    {
                        eventhandler = new ChatEventHandler { FriendTableID = initiatorDTO.FriendTableID, Time = initiatorDTO.Time };
                        ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT[initiatorDTO.FriendTableID.ToString()] = eventhandler;
#if CHAT_LOG
                        log.Debug("AUTH::FRIEND::NEW_REGISTRATION".PadRight(58, ' ') + "==>   friendId = " + initiatorDTO.FriendTableID);
#endif
                        ChatService.RegisterFriendChat(initiatorDTO.FriendTableID, initiatorDTO.FullName, initiatorDTO.ProfileImage, initiatorDTO.ChatServerIP, initiatorDTO.ChatRegisterPort, initiatorDTO.Device, initiatorDTO.DeviceToken, initiatorDTO.ApplicationType, initiatorDTO.Presence, initiatorDTO.Mood, initiatorDTO.Time, initiatorDTO.RemotePushType);
                    }
                    else if (initiatorDTO.Time >= eventhandler.Time)
                    {
#if CHAT_LOG
                        if (eventhandler.Time > 0) log.Debug("AUTH::FRIEND::OVERRITE_REGISTRATION".PadRight(58, ' ') + "==>   friendId = " + initiatorDTO.FriendTableID);
#endif
                        eventhandler.Time = initiatorDTO.Time;
                        ChatService.RegisterFriendChat(initiatorDTO.FriendTableID, initiatorDTO.FullName, initiatorDTO.ProfileImage, initiatorDTO.ChatServerIP, initiatorDTO.ChatRegisterPort, initiatorDTO.Device, initiatorDTO.DeviceToken, initiatorDTO.ApplicationType, initiatorDTO.Presence, initiatorDTO.Mood, initiatorDTO.Time, initiatorDTO.RemotePushType);
                    }
                    else
                    {
#if CHAT_LOG
                        log.Debug("AUTH::FRIEND::INVALID_REGISTRATION".PadRight(58, ' ') + "==>   friendId = " + initiatorDTO.FriendTableID);
#endif
                        return;
                    }
                }
#if CHAT_LOG
                log.Debug("AUTH::FRIEND::UNLOCK_REG_EVENT".PadRight(58, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif

                blockModel.IsFriendRegDenied = false;
                friendInfoModel.ShortInfoModel.FullName = initiatorDTO.FullName;
                friendInfoModel.ShortInfoModel.Presence = initiatorDTO.Presence;
                friendInfoModel.ShortInfoModel.LastOnlineTime = initiatorDTO.LastOnlineTime;
                friendInfoModel.ShortInfoModel.Mood = initiatorDTO.Mood;
                friendInfoModel.ShortInfoModel.Device = initiatorDTO.Device;
                friendInfoModel.DeviceToken = initiatorDTO.DeviceToken;
                friendInfoModel.OnPropertyChanged("CurrentInstance");
                friendInfoModel.ShortInfoModel.OnPropertyChanged("CurrentInstance");

                //UserBasicInfoDTO friendInfoDTO = null;
                //if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(initiatorDTO.FriendTableID, out friendInfoDTO))
                UserBasicInfoDTO friendInfoDTO = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(initiatorDTO.FriendTableID);
                if (friendInfoDTO != null)
                {
                    friendInfoDTO.FullName = initiatorDTO.FullName;
                    friendInfoDTO.Presence = initiatorDTO.Presence;
                    friendInfoDTO.LastOnlineTime = initiatorDTO.LastOnlineTime;
                    friendInfoDTO.Mood = initiatorDTO.Mood;
                    friendInfoDTO.Device = initiatorDTO.Device;
                    friendInfoDTO.DeviceToken = initiatorDTO.DeviceToken;

                    if (friendInfoDTO.FriendShipStatus == 0)
                    {
                        List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
                        list.Add(friendInfoDTO);
                        new InsertIntoUserBasicInfoTable(list).Start();
                    }
                }

                ChatService.RequestFriendChatHistory(initiatorDTO.FriendTableID, friendInfoModel.ChatMaxPacketID, ChatConstants.HISTORY_DOWN, 100, null);

                if (onComplete != null)
                {
                    onComplete(initiatorDTO.FriendTableID, true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ProcessStartFriendChat() => " + ex.Message + "\n" + ex.StackTrace);
                if (onComplete != null)
                {
                    onComplete(initiatorDTO.FriendTableID, false);
                }
            }
        }

        public void ProcessStartGroupChat(ChatInitiatorDTO initiatorDTO, Func<long, bool, int> onComplete = null)
        {
            try
            {
#if CHAT_LOG
                log.Debug("AUTH::GROUP::RESPONSE".PadRight(58, ' ') + "==>   initiatorDTO = " + initiatorDTO.ToString());
#endif

#if CHAT_LOG
                log.Debug("AUTH::GROUP::LOCK_REG_EVENT".PadRight(58, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif
                lock (ChatEventHandler.CHAT_REG_EVENT_LOCKER)
                {
                    ChatEventHandler eventhandler = ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryGetValue(initiatorDTO.GroupID.ToString());
                    if (eventhandler == null)
                    {
                        eventhandler = new ChatEventHandler { GroupID = initiatorDTO.GroupID, Time = initiatorDTO.Time };
                        ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT[initiatorDTO.GroupID.ToString()] = eventhandler;
#if CHAT_LOG
                        log.Debug("AUTH::GROUP::NEW_REGISTRATION".PadRight(58, ' ') + "==>   groupId = " + initiatorDTO.GroupID);
#endif
                        ChatService.RegisterGroupChat(initiatorDTO.GroupID, initiatorDTO.ChatServerIP, initiatorDTO.ChatRegisterPort, initiatorDTO.Time);
                    }
                    else if (initiatorDTO.Time >= eventhandler.Time)
                    {
#if CHAT_LOG
                        if (eventhandler.Time > 0) log.Debug("AUTH::GROUP::OVERRITE_REGISTRATION".PadRight(58, ' ') + "==>   groupId = " + initiatorDTO.GroupID);
#endif
                        eventhandler.Time = initiatorDTO.Time;
                        ChatService.RegisterGroupChat(initiatorDTO.GroupID, initiatorDTO.ChatServerIP, initiatorDTO.ChatRegisterPort, initiatorDTO.Time);
                    }
                    else
                    {
#if CHAT_LOG
                        log.Debug("AUTH::GROUP::INVALID_REGISTRATION".PadRight(58, ' ') + "==>   groupId = " + initiatorDTO.GroupID);
#endif
                        return;
                    }
                }
#if CHAT_LOG
                log.Debug("AUTH::GROUP::UNLOCK_REG_EVENT".PadRight(58, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif

                GroupInfoModel groupInfoModel = RingIDViewModel.Instance.GetGroupInfoModelByGroupID(initiatorDTO.GroupID);
                if (initiatorDTO.ActionType == AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER)
                {
                    ChatService.GetGroupInformationWithMembers(initiatorDTO.GroupID, null);
                }
                else
                {
                    if (groupInfoModel.IsPartial || groupInfoModel.MemberInfoDictionary.Values.FirstOrDefault(P => P.UserTableID == DefaultSettings.LOGIN_TABLE_ID && P.MemberAccessType != ChatConstants.MEMBER_TYPE_NOT_MEMBER) == null)
                    {
                        ChatService.GetGroupInformationWithMembers(initiatorDTO.GroupID, null);
                    }
                    else
                    {
                        ChatService.RequestGroupChatHistory(groupInfoModel.GroupID, groupInfoModel.ChatMaxPacketID, ChatConstants.HISTORY_DOWN, 100, null);
                    }
                }

                if (onComplete != null)
                {
                    onComplete(initiatorDTO.GroupID, true);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ProcessStartGroupChat() => " + ex.Message + "\n" + ex.StackTrace);
                if (onComplete != null)
                {
                    onComplete(initiatorDTO.GroupID, false);
                }
            }
        }

        public void ProcessStartLiveStream(StreamDTO streamDTO)
        {
            try
            {
#if CHAT_LOG
                log.Debug("AUTH::STREAM::RESPONSE".PadRight(58, ' ') + "==>   publisherID = " + streamDTO.UserTableID + ", streamID = " + streamDTO.StreamID + ", userName = " + streamDTO.UserName + ", startTime = " + streamDTO.StartTime + ", endTime = " + streamDTO.EndTime + ", chatServerIP = " + streamDTO.ChatServerIP + ", chatServerPort = " + streamDTO.ChatServerPort);
#endif
                if (StreamViewModel.Instance.StreamInfoModel != null && StreamViewModel.Instance.StreamInfoModel.StreamID.Equals(streamDTO.StreamID))
                {
                    StreamViewModel.Instance.StreamInfoModel.LoadData(streamDTO);

                    StreamViewModel.Instance.StreamInfoModel.ChatOn = !String.IsNullOrWhiteSpace(StreamViewModel.Instance.StreamInfoModel.ChatServerIP) && StreamViewModel.Instance.StreamInfoModel.ChatServerPort > 0;
                    if (StreamViewModel.Instance.StreamInfoModel.ChatOn)
                    {
                        ChatService.RegisterStreamChat(
                            StreamViewModel.Instance.StreamInfoModel.UserTableID,
                            StreamViewModel.Instance.StreamInfoModel.StreamID.ToString(),
                            StreamViewModel.Instance.StreamInfoModel.ChatServerIP,
                            StreamViewModel.Instance.StreamInfoModel.ChatServerPort);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ProcessStartLiveStream() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion "Chat Utility Methods"

        #region "Call Utility Methods"

        /// <summary>
        /// From Server :
        /// {"futId":57223,"swIp":"38.127.68.57","swPr":1250,"rc":0,
        /// "sucs":true,"psnc":2,"dvc":1,"callID":"1863608675034619","tm":37671464493070749,
        /// "fn":"aaaaaa","idc":false,"calT":1,"p2p":2}
        /// </summary>
        /// <param name="callerDto"></param>
        public void ProcessUpdateSendRegister(CallerDTO callerDto) //374
        {
            try
            {
                bool libLoaded = CallProperty.isCallSdkLoaded();
                if (!libLoaded) CallHelperMethods.InitCallSDK();
                callsdkwrapper.CallMediaType mediaType = callsdkwrapper.CallMediaType.Voice;
                if (callerDto.calT == XamlStaticValues.CALL_TYPE_VIDEO) mediaType = callsdkwrapper.CallMediaType.Video;
                if (MainSwitcher.CallController.ViewModelCallInfo == null)
                {
                    callerDto.IsIncomming = true;
                    CallHelperMethods.Register(CallUserType.Callee, callerDto.UserTableID, callerDto.CallServerIP, callerDto.CallRegPort, callerDto.FullName, callerDto.Device, callerDto.Presence, callerDto.Mood, false, true, callerDto.ApplicationType, callerDto.DeviceToken, callerDto.CallID, callerDto.Time, false, 0, mediaType, callerDto.P2PCall, 0, false);
                    CallDictionaries.Instance.CALLERS_INFO_DICTIONARY.TryAdd(callerDto.CallID, callerDto);
                }
                else
                {
                    CallHelperMethods.Register(CallUserType.Callee, callerDto.UserTableID, callerDto.CallServerIP, callerDto.CallRegPort, callerDto.FullName, callerDto.Device, callerDto.Presence, callerDto.Mood, false, true, callerDto.ApplicationType, callerDto.DeviceToken, callerDto.CallID, callerDto.Time, false, 0, mediaType, callerDto.P2PCall, 0, true);
                    CallDictionaries.Instance.CALLERS_INFO_DICTIONARY.TryAdd(callerDto.CallID, callerDto);
                }
            }
            catch (Exception ex) { log.Error("Error: ProcessUpdateSendRegister() ==> StartProcess update register ==>" + ex.Message + "\n" + ex.StackTrace); }
        }

        #endregion "Call Utility Methods"
    }
}

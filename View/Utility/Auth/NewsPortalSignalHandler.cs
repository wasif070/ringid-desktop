﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.PopUp;
using View.UI.Profile.FriendProfile;
using View.ViewModel;
using View.UI.Profile.CelebrityProfile;
using View.Utility.DataContainer;

namespace View.Utility.Auth
{
    public class NewsPortalSignalHandler
    {
        //FeedSignalHandler feedSignalHandler = new FeedSignalHandler();
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(NewsPortalSignalHandler).Name);
        #endregion "Fields"

        public void Process_NEWSPORTAL_FEED_295(JObject _JobjFromResponse, string client_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    int scl = (int)_JobjFromResponse[JsonKeys.Scroll];
                    Guid npUUid = (Guid)_JobjFromResponse[JsonKeys.NpUUID];
                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                    {
                        int feed_state = client_packet_id.Equals(DefaultSettings.ALLNEWSPORTAL_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);

                            //FeedDataContainer.Instance.AllNewsPortalFeedsSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                            //if (feed_state == SettingsConstants.FEED_FIRSTTIME)
                            //{
                            //    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null
                            //        && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel != null
                            //        && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel.IsVisible)
                            //        RingIDViewModel.Instance.AllNewsPortalFeeds.InsertModel(model);
                            //}
                            if (scl != 1)
                            {
                                FeedDataContainer.Instance.AllNewsPortalBottomIds.PvtMinGuid = npUUid;
                                if (feed_state != SettingsConstants.FEED_FIRSTTIME && !FeedDataContainer.Instance.AllNewsPortalCurrentIds.Contains(nfId))
                                    FeedDataContainer.Instance.AllNewsPortalBottomIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            else
                            {
                                FeedDataContainer.Instance.AllNewsPortalTopIds.PvtMaxGuid = npUUid;
                                //if (feed_state != SettingsConstants.FEED_FIRSTTIME) FeedDataContainer.Instance.AllNewsPortalTopIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME || scl == 1)
                            {
                                if (!FeedDataContainer.Instance.AllNewsPortalCurrentIds.Contains(nfId))
                                {
                                    FeedHolderModel holder = new FeedHolderModel(2);
                                    holder.FeedId = nfId;
                                    holder.Feed = model;
                                    holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                    holder.FeedPanelType = holder.Feed.FeedPanelType;
                                    RingIDViewModel.Instance.AllNewsPortalCustomFeeds.CalculateandInsertOtherFeeds(holder, FeedDataContainer.Instance.AllNewsPortalCurrentIds);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_NEWSPORTAL_BREAKING_FEED_302(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.BreakingNewsFeeds] != null)
                {
                    long utId = (_JobjFromResponse[JsonKeys.UserTableID] != null) ? (long)_JobjFromResponse[JsonKeys.UserTableID] : 0;
                    ObservableCollection<FeedModel> breakingCollection = (utId == 0) ? RingIDViewModel.Instance.BreakingNewsPortalFeeds : ((UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null && UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsPortalModel.UserTableID == utId) ? RingIDViewModel.Instance.BreakingNewsPortalProfileFeeds : null);
                    if (breakingCollection != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.BreakingNewsFeeds];
                        foreach (JObject singleObj in jarray)
                        {
                            FeedModel model = new FeedModel();
                            model.NewsModel = new NewsModel();
                            model.NewsfeedId = model.NewsModel.NewsId = (singleObj[JsonKeys.NtUUID] != null) ? ((Guid)(singleObj[JsonKeys.NtUUID])) : Guid.Empty;

                            long npUtId = (long)singleObj[JsonKeys.UserTableID];
                            string npPrImg = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                            string npNm = (singleObj[JsonKeys.PageTitle] != null) ? (string)singleObj[JsonKeys.PageTitle] : string.Empty;
                            model.PostOwner = model.WallOrContentOwner = new BaseUserProfileModel { UserTableID = npUtId, FullName = npNm, ProfileImage = npPrImg, ProfileType = SettingsConstants.PROFILE_TYPE_NEWSPORTAL };

                            model.NewsModel.NewsShortDescription = (singleObj[JsonKeys.NewsShortDescription] != null) ? ((string)(singleObj[JsonKeys.NewsShortDescription])) : string.Empty;
                            model.NewsModel.NewsTitle = (singleObj[JsonKeys.NewsPortalTitle] != null) ? ((string)(singleObj[JsonKeys.NewsPortalTitle])) : string.Empty;

                            //model.ImageList = new ObservableCollection<ImageModel>();
                            //model.ImageList.Add(new ImageModel { ImageUrl = npPrImg });

                            if (!breakingCollection.Any(P => P.NewsfeedId == model.NewsfeedId))
                                breakingCollection.Add(model);
                        }
                        if (breakingCollection.Count > 0)
                        {
                            //UCMiddlePanelSwitcher.View_UCNewsPortalProfile.IsFirstTime = true;
                            if (utId == 0)//(UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel.IsVisible)
                                UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel.LoadSliderData();
                            else //if (UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null)
                                UCMiddlePanelSwitcher.View_UCNewsPortalProfile.LoadSliderData();
                        }
                    }
                }

                /*if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.BreakingNewsFeeds] != null
                    && (UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null || (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel != null)))
                {
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.BreakingNewsFeeds];
                    foreach (JObject singleObj in jarray)
                    {
                        FeedModel model = new FeedModel();
                        model.NewsModel = new NewsModel();
                        model.NewsfeedId = model.NewsModel.NewsId = (singleObj[JsonKeys.NtUUID] != null) ? ((Guid)(singleObj[JsonKeys.NtUUID])) : Guid.Empty;
                        //model.PostOwner = model.WallOrContentOwner = UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsPortalModel;
                        model.ImageList = new ObservableCollection<ImageModel>();
                        model.ImageList.Add(new ImageModel { ImageUrl = model.PostOwner.ProfileImage });
                        model.NewsModel.NewsShortDescription = (singleObj[JsonKeys.NewsShortDescription] != null) ? ((string)(singleObj[JsonKeys.NewsShortDescription])) : string.Empty;
                        model.NewsModel.NewsTitle = (singleObj[JsonKeys.NewsPortalTitle] != null) ? ((string)(singleObj[JsonKeys.NewsPortalTitle])) : string.Empty;
                        if (!RingIDViewModel.Instance.BreakingNewsPortalFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                            RingIDViewModel.Instance.BreakingNewsPortalFeeds.Add(model);
                    }
                    if (RingIDViewModel.Instance.BreakingNewsPortalFeeds.Count > 0)
                    {
                        //UCMiddlePanelSwitcher.View_UCNewsPortalProfile.IsFirstTime = true;
                        if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel.IsVisible)
                            UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel.LoadSliderData();
                        else if(UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null)
                            UCMiddlePanelSwitcher.View_UCNewsPortalProfile.LoadSliderData();
                    }
                }*/
            }
            catch (Exception e)
            {
                log.Error("ProcessBreakingNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

    }
}

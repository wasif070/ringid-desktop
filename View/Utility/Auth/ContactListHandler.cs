﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Auth.Parser;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.AddFriend;
using View.UI.PopUp;
using View.UI.Profile.FriendProfile;
using View.Utility.FriendList;
using View.Utility.FriendProfile;
using View.ViewModel;
using View.Utility.DataContainer;

namespace View.Utility.Auth
{
    public class ContactListHandler
    {
        #region "Private Fields"
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ContactListHandler).Name);
        int MutualFriendsSeqCount = 0;
        #endregion "Private Fields"

        #region "Private methods"
        private void processFriendContactList(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.TotalFriend] != null)
                {
                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableID];
                    int tf = (int)_JobjFromResponse[JsonKeys.TotalFriend];
                    if (_JobjFromResponse[JsonKeys.ContactList] != null)
                    {
                        JArray contactList = (JArray)_JobjFromResponse[JsonKeys.ContactList];
                        lock (FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY)
                        {
                            foreach (JObject singleObj in contactList)
                            {
                                long utId_singleObj = 0;
                                if (singleObj[JsonKeys.UserTableID] != null) utId_singleObj = (long)singleObj[JsonKeys.UserTableID];
                                UserBasicInfoDTO userBasicInfo = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(utId_singleObj);
                                if (singleObj[JsonKeys.UserIdentity] != null) userBasicInfo.RingID = long.Parse(singleObj[JsonKeys.UserIdentity].ToString());
                                if (singleObj[JsonKeys.FullName] != null) userBasicInfo.FullName = (string)singleObj[JsonKeys.FullName];
                                if (singleObj[JsonKeys.Gender] != null) userBasicInfo.Gender = (string)singleObj[JsonKeys.Gender];
                                if (singleObj[JsonKeys.FriendshipStatus] != null) userBasicInfo.FriendShipStatus = (int)singleObj[JsonKeys.FriendshipStatus];
                                if (singleObj[JsonKeys.ProfileImage] != null) userBasicInfo.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
                                if (singleObj[JsonKeys.ProfileImageId] != null) userBasicInfo.ProfileImageId = (Guid)singleObj[JsonKeys.ProfileImageId];
                                if (singleObj[JsonKeys.NumberOfMutualFriend] != null) userBasicInfo.NumberOfMutualFriends = (int)singleObj[JsonKeys.NumberOfMutualFriend];
                                if (singleObj[JsonKeys.ContactUpdate] != null) userBasicInfo.ContactUpdateTime = (long)singleObj[JsonKeys.ContactUpdate];
                                if (singleObj[JsonKeys.UpdateTime] != null) userBasicInfo.UpdateTime = (long)singleObj[JsonKeys.UpdateTime];
                                FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY[userBasicInfo.UserTableID] = userBasicInfo;
                                lock (FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY)
                                {
                                    if (FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY.ContainsKey(utid))
                                    {
                                        if (!FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[utid].FriendList.Any(f => f.Equals(userBasicInfo.UserTableID)))
                                            FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[utid].FriendList.Add(userBasicInfo.UserTableID);
                                    }
                                    else
                                    {
                                        FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY.Add(utid, new FriendInfoDTO());
                                        FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[utid].FriendList.Add(userBasicInfo.UserTableID);
                                    }
                                    FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[utid].TotalFriend = tf;
                                }
                            }
                        }
                    }
                    ShowFriendTotalContactList(utid, FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY.Values.ToList());
                    FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY.Clear();
                }
                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
                {
                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableID];
                    NoContactFoundInFriendsContactList(utid);
                }
            }
            catch (Exception e) { log.Error("ProcessFriendContactList ex ==>" + e.StackTrace); }
        }

        #endregion "Private methods"
        void processDeleteFriend_128_328(JObject _JobjFromResponse, int action, long server_packet_id)
        {
            try
            {
                if ((action == AppConstants.TYPE_DELETE_FRIEND && server_packet_id != 0) || action == AppConstants.TYPE_UPDATE_DELETE_FRIEND)
                {
                    if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                    {
                        long userTableID = _JobjFromResponse[JsonKeys.UserTableID] != null ? long.Parse(_JobjFromResponse[JsonKeys.UserTableID].ToString()) : 0;

                        UserBasicInfoDTO userBasicInfo = null;
                        lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
                        {
                            userBasicInfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userTableID);
                            if (userBasicInfo != null)
                            {
                                bool doBlock = userBasicInfo.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && userBasicInfo.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED;
                                userBasicInfo.FriendShipStatus = 0;
                                userBasicInfo.ChatAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
                                userBasicInfo.CallAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;
                                userBasicInfo.FeedAccess = StatusConstants.TYPE_ACCESS_UNBLOCKED;

                                /*Favorite Rank*/
                                if (userBasicInfo.FavoriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE)
                                {
                                    userBasicInfo.FavoriteRank = 0;
                                    ContactListDAO.UpdateContactRanking(userBasicInfo);
                                }
                                userBasicInfo.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;

                                List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
                                userList.Add(userBasicInfo);
                                new InsertIntoUserBasicInfoTable(userList).Start();

                                UI_ProcessDeleteFriendUpdate(userBasicInfo);

                                if (doBlock)
                                {
                                    UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(userBasicInfo.UserTableID, userBasicInfo.RingID, userBasicInfo.FullName, userBasicInfo.ProfileImage);
                                    BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(userBasicInfo.UserTableID);
                                    blockModel.IsBlockedByMe = false;
                                    HelperMethods.ChangeBlockUnblockSettingsWrapper(StatusConstants.FULL_ACCESS, friendInfoModel, blockModel);
                                }
                            }
                        }
                    }
                    else ReloadFriendsMutualFriendsProfile();
                }
            }
            catch (Exception e) { log.Error("Exception in ProcessDeleteFriendUpdate==>" + e.StackTrace); }
        }

        #region "Public Methods"

        public void Process_CONTACT_UTIDS_29(CommonPacketAttributes packet_attributes) //29
        {
            try
            {
                //if (ThreadContactUtIdsRequest.IsRunning()) ThreadContactUtIdsRequest._TotalRecords = packet_attributes.TotalRecords;                
                if (packet_attributes.Success && packet_attributes.UserIDs != null)
                {
                    byte[] userIDsBytes = packet_attributes.UserIDs;
                    for (int i = 0; i < userIDsBytes.Length; i += 11)
                    {
                        long userTableId = Parser.GetLong(userIDsBytes, i, 8);
                        int ct = (int)userIDsBytes[i + 8];
                        int mb = (int)userIDsBytes[i + 9];
                        int fns = (int)userIDsBytes[i + 10];

                        UserBasicInfoDTO user = new UserBasicInfoDTO();
                        user.UserTableID = userTableId;
                        user.MatchedBy = mb;
                        user.FriendShipStatus = fns;
                        FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY[user.UserTableID] = user;
                    }
                    ThreadContactUtIdsRequest._TotalPackets++;

                    //if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == packet_attributes.TotalRecords)
                    if (ThreadContactUtIdsRequest._TotalPackets == packet_attributes.TotalPackets)
                    {
                        ThreadContactUtIdsRequest._TotalRecords = FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count;
                        List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
                        foreach (Models.Entity.UserBasicInfoDTO item in FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Values) userList.Add(item);
                        FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Clear();
                        ThreadContactUtIdsRequest._TotalPackets = 0;
                        userList = FriendListLoadUtility.SortFriendListByFriendshipStatusAccepted(userList);
                        foreach (UserBasicInfoDTO user in userList) FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY[user.UserTableID] = user;
                        MainSwitcher.ThreadManager().ContactListRequest().StartThread();
                    }
                }
                else UI_ProcessByteContactList(null, packet_attributes.Success);
            }
            catch (Exception e) { log.Error("Exception in TYPE_CONTACT_UTIDS 29==>" + e.StackTrace); }
        }

        public void Process_CONTACT_LIST_23(CommonPacketAttributes packet_attributes) //23
        {
            try
            {
                List<UserBasicInfoDTO> basicInfos = null;
                if (packet_attributes.Success)
                {
                    if (packet_attributes.ContactList != null && packet_attributes.ContactList.Count > 0)
                    {
                        basicInfos = new List<UserBasicInfoDTO>();
                        foreach (CommonPacketAttributes contact in packet_attributes.ContactList)
                        {
                            UserBasicInfoDTO entity = null;
                            FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.TryGetValue(contact.UserID, out entity);
                            if (entity != null)
                            {
                                FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.TryRemove(contact.UserID, out entity);
                                entity.UserTableID = contact.UserID;
                                entity.RingID = contact.UserIdentity;
                                entity.FullName = contact.UserName;
                                entity.ContactType = contact.ContactType;
                                entity.ContactAddedTime = contact.ContactAddedTime;
                                entity.Delete = contact.Deleted;
                                entity.FriendShipStatus = (entity.Delete == StatusConstants.STATUS_DELETED) ? 0 : contact.FriendshipStatus;
                                entity.ProfileImage = contact.ProfileImage;
                                if (!string.IsNullOrEmpty(contact.ProfileImageId)) entity.ProfileImageId = Guid.Parse(contact.ProfileImageId);
                                entity.ContactUpdateTime = contact.ContactUpdateTime;
                                entity.UpdateTime = contact.UpdateTime;
                                entity.NumberOfMutualFriends = contact.MutualFriendCount;
                                entity.CallAccess = contact.CallAccess;
                                entity.ChatAccess = contact.ChatAccess;
                                entity.FeedAccess = contact.FeedAccess;
                                if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED && (entity.ContactAddedTime > (Models.Utility.ModelUtility.CurrentTimeMillis() - Models.Utility.ModelUtility.MilliSecondsInSevenDay)))
                                    entity.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;
                                UserBasicInfoDTO prevDTO = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(entity.UserTableID);
                                if (prevDTO != null)
                                {
                                    entity.ContactAddedReadUnread = prevDTO.ContactAddedReadUnread;
                                    entity.ChatBgUrl = prevDTO.ChatBgUrl;
                                    entity.ChatMaxPacketID = prevDTO.ChatMaxPacketID;
                                    entity.ChatMinPacketID = prevDTO.ChatMinPacketID;
                                    if (entity.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                                    {
                                        entity.FavoriteRank = prevDTO.FavoriteRank;
                                        if (prevDTO.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                                        {
                                            entity.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
                                            entity.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
                                            entity.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
                                            BlockedNonFriendModel model = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(entity.UserTableID);
                                            model.IsBlockedByMe = false;
                                            model.IsBlockedByFriend = false;
                                            BlockedNonFriendDAO.SaveBlockedNonFriendFromThread(entity.UserTableID, model.IsBlockedByMe, model.IsBlockedByFriend);
                                        }
                                        else
                                        {
                                            entity.ImSoundEnabled = prevDTO.ImSoundEnabled;
                                            entity.ImNotificationEnabled = prevDTO.ImNotificationEnabled;
                                            entity.ImPopupEnabled = prevDTO.ImPopupEnabled;
                                        }
                                    }
                                    else
                                    {
                                        entity.ImSoundEnabled = prevDTO.ImSoundEnabled;
                                        entity.ImNotificationEnabled = prevDTO.ImNotificationEnabled;
                                        entity.ImPopupEnabled = prevDTO.ImPopupEnabled;
                                        if (prevDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                                        {
                                            BlockedNonFriendModel model = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(entity.UserTableID);
                                            model.IsBlockedByMe = prevDTO.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED;
                                            model.IsFriendRegDenied = false;
                                            BlockedNonFriendDAO.SaveBlockedNonFriendFromThread(entity.UserTableID, model.IsBlockedByMe, model.IsBlockedByFriend);
                                        }
                                    }
                                }
                                if (entity.UserTableID > 0) FriendDictionaries.Instance.AddOrUPdateInBasicInfoDictionaryWithLock(entity);
                                lock (FriendDictionaries.Instance.TEMP_CONTACT_LIST)
                                    FriendDictionaries.Instance.TEMP_CONTACT_LIST.Add(entity);
                                basicInfos.Add(entity);
                            }
                        }
                    }
                }
                UI_ProcessByteContactList(basicInfos, packet_attributes.Success);
            }
            catch (Exception e) { log.Error("Exception in TYPE_CONTACT_LIST 23==>" + e.StackTrace); }
        }
        /// <summary>
        ///  {"sucs":true,"uId":"2110079881","fn":"aaaaaa","frnS":2,"prImId":0,"utId":57223,"ct":2,"nmf":0,"acpt":false,"ut":1473080561275,"rc":0,"apt":1}
        /// </summary>
        /// <param name="_JobjFromResponse"></param>
        public void Process_UPDATE_ADD_FRIEND_327(JObject _JobjFromResponse) //someone sent me friend request//327
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    long userTableID = _JobjFromResponse[JsonKeys.UserTableID] != null ? long.Parse(_JobjFromResponse[JsonKeys.UserTableID].ToString()) : 0;
                    int FriendshipStatus = _JobjFromResponse[JsonKeys.FriendshipStatus] != null ? Int32.Parse(_JobjFromResponse[JsonKeys.FriendshipStatus].ToString()) : 0;
                    UserBasicInfoDTO userBasicInfo = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(userTableID, _JobjFromResponse);
                    lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
                        FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(userBasicInfo.UserTableID);
                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY) FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userBasicInfo.UserTableID);
                    List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
                    userBasicInfoList.Add(userBasicInfo);
                    new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();
                    UI_ProcessAddFriendUpdate(userBasicInfo);
                }
            }
            catch (Exception e) { log.Error("Exception in ProcessAddFriendUpdate==>" + e.StackTrace); }
        }

        public void Process_UPDATE_ACCEPT_FRIEND_329(JObject _JobjFromResponse) //someone accepted my request//329
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    if (_JobjFromResponse[JsonKeys.UserTableID] != null)
                    {
                        UserBasicInfoDTO userBasicInfo = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(long.Parse(_JobjFromResponse[JsonKeys.UserTableID].ToString()));
                        if (_JobjFromResponse[JsonKeys.UserIdentity] != null) userBasicInfo.RingID = long.Parse(_JobjFromResponse[JsonKeys.UserIdentity].ToString());
                        if (_JobjFromResponse[JsonKeys.UserTableID] != null) userBasicInfo.UserTableID = (long)_JobjFromResponse[JsonKeys.UserTableID];
                        if (_JobjFromResponse[JsonKeys.FullName] != null) userBasicInfo.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
                        if (_JobjFromResponse[JsonKeys.Gender] != null) userBasicInfo.Gender = (string)_JobjFromResponse[JsonKeys.Gender];
                        if (_JobjFromResponse[JsonKeys.FriendshipStatus] != null) userBasicInfo.FriendShipStatus = (int)_JobjFromResponse[JsonKeys.FriendshipStatus];
                        if (_JobjFromResponse[JsonKeys.ProfileImage] != null) userBasicInfo.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
                        if (_JobjFromResponse[JsonKeys.ProfileImageId] != null) userBasicInfo.ProfileImageId = (Guid)_JobjFromResponse[JsonKeys.ProfileImageId];
                        if (_JobjFromResponse[JsonKeys.NumberOfMutualFriend] != null) userBasicInfo.NumberOfMutualFriends = (int)_JobjFromResponse[JsonKeys.NumberOfMutualFriend];
                        if (_JobjFromResponse[JsonKeys.MatchedBy] != null) userBasicInfo.MatchedBy = (int)_JobjFromResponse[JsonKeys.MatchedBy];
                        if (_JobjFromResponse[JsonKeys.CallAccess] != null) userBasicInfo.CallAccess = (int)_JobjFromResponse[JsonKeys.CallAccess];
                        if (_JobjFromResponse[JsonKeys.ChatAccess] != null) userBasicInfo.ChatAccess = (int)_JobjFromResponse[JsonKeys.ChatAccess];
                        if (_JobjFromResponse[JsonKeys.FeedAccess] != null) userBasicInfo.FeedAccess = (int)_JobjFromResponse[JsonKeys.FeedAccess];
                        userBasicInfo.ContactAddedTime = Models.Utility.ModelUtility.CurrentTimeMillis();
                        userBasicInfo.ContactAddedReadUnread = SettingsConstants.RECENT_FRND_UNREAD;
                        userBasicInfo.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
                        userBasicInfo.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
                        userBasicInfo.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;

                        List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
                        userBasicInfoList.Add(userBasicInfo);
                        new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();
                        BlockedNonFriendModel model = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(userBasicInfo.UserTableID);
                        model.IsBlockedByMe = false;
                        model.IsBlockedByFriend = false;
                        BlockedNonFriendDAO.SaveBlockedNonFriendFromThread(userBasicInfo.UserTableID, model.IsBlockedByMe, model.IsBlockedByFriend);
                        UI_ProcessAcceptFriendUpdate(userBasicInfo);
                    }
                }
            }
            catch (Exception e) { log.Error("Exception in ProcessAcceptFriendUpdate==>" + e.StackTrace); }
        }

        /// <summary>
        /// =328 from Auth JSON==> {"uId":"2110079881","utId":57223,"sucs":true,"rc":0}
        /// </summary>
        /// <param name="_JobjFromResponse"></param>
        /// <param name="action"></param>
        /// <param name="server_packet_id"></param>
        public void Process_DELETE_FRIEND_128(JObject _JobjFromResponse, int action, long server_packet_id) //328
        {
            processDeleteFriend_128_328(_JobjFromResponse, action, server_packet_id);
        }

        public void Process_UPDATE_DELETE_FRIEND_328(JObject _JobjFromResponse, int action, long server_packet_id) //328
        {
            processDeleteFriend_128_328(_JobjFromResponse, action, server_packet_id);
        }

        public void Process_CONTACT_SEARCH_34(JObject _JobjFromResponse, string client_packet_id) //34
        {
            try
            {
                int searchCount = 0;
                int matchCount = 0; // Server often sends wrong data
                int ProfileType = 0;
                string searchParam = null;
                List<UserBasicInfoDTO> listuserBasicInfoDTO = null;
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
                    ProfileType = (int)_JobjFromResponse[JsonKeys.ProfileType];
                    if (_JobjFromResponse[JsonKeys.SearchedContacList] != null)
                    {
                        JArray searchedContactList = (JArray)_JobjFromResponse[JsonKeys.SearchedContacList];
                        if (_JobjFromResponse[JsonKeys.ProfileType] != null) ProfileType = ((int)_JobjFromResponse[JsonKeys.ProfileType]);
                        if (ProfileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
                        {
                            List<long> portalIds = null;
                            if (!UserProfilesContainer.Instance.SearchedPortalIdsByParam.ContainsKey(searchParam))
                            {
                                portalIds = new List<long>();
                                UserProfilesContainer.Instance.SearchedPortalIdsByParam[searchParam] = portalIds;
                            }
                            else portalIds = UserProfilesContainer.Instance.SearchedPortalIdsByParam[searchParam];
                            foreach (JObject singleObj in searchedContactList)
                            {
                                JObject NpDto = (JObject)singleObj[JsonKeys.NewsPortalDTO];
                                long pId = (long)NpDto[JsonKeys.PageId];
                                NewsPortalModel model = null;
                                if (!UserProfilesContainer.Instance.NewsPortalModels.TryGetValue(pId, out model))
                                {
                                    model = new NewsPortalModel { ProfileType = SettingsConstants.PROFILE_TYPE_NEWSPORTAL };
                                    UserProfilesContainer.Instance.NewsPortalModels[pId] = model;
                                }
                                model.LoadData(singleObj);
                                if (!portalIds.Contains(pId)) portalIds.Add(pId);
                                Application.Current.Dispatcher.Invoke((Action)(() =>
                                {
                                    if (UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel != null
                                        && UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel.SearchTermTextBox.Text.Trim().Equals(searchParam)
                                        && !UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel.SearchedNewsPortals.Any(P => P.PortalId == model.PortalId))
                                        UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel.SearchedNewsPortals.Add(model);
                                }));
                            }
                        }
                        else if (ProfileType == SettingsConstants.PROFILE_TYPE_PAGES)
                        {
                            List<long> pageIds = null;
                            if (!UserProfilesContainer.Instance.SearchedPageIdsByParam.ContainsKey(searchParam))
                            {
                                pageIds = new List<long>();
                                UserProfilesContainer.Instance.SearchedPageIdsByParam[searchParam] = pageIds;
                            }
                            else pageIds = UserProfilesContainer.Instance.SearchedPageIdsByParam[searchParam];
                            foreach (JObject singleObj in searchedContactList)
                            {
                                JObject NpDto = (JObject)singleObj[JsonKeys.NewsPortalDTO];
                                long pId = (long)NpDto[JsonKeys.PageId];
                                PageInfoModel model = null;
                                if (!UserProfilesContainer.Instance.PageModels.TryGetValue(pId, out model))
                                {
                                    model = new PageInfoModel();
                                    UserProfilesContainer.Instance.PageModels[pId] = model;
                                }
                                model.LoadData(singleObj);
                                if (!pageIds.Contains(pId))
                                    pageIds.Add(pId);
                                Application.Current.Dispatcher.Invoke((Action)(() =>
                                {
                                    if (UCMiddlePanelSwitcher.View_UCPagesSearchPanel != null
                                        && UCMiddlePanelSwitcher.View_UCPagesSearchPanel.SearchTermTextBox.Text.Trim().Equals(searchParam)
                                        && !UCMiddlePanelSwitcher.View_UCPagesSearchPanel.SearchedPages.Any(P => P.PageId == model.PageId))
                                        UCMiddlePanelSwitcher.View_UCPagesSearchPanel.SearchedPages.Add(model);
                                }));
                            }
                        }
                        else if (ProfileType == SettingsConstants.PROFILE_TYPE_CELEBRITY)
                        {
                            List<long> UtIds = null;
                            if (!UserProfilesContainer.Instance.SearchedCelebrityIdsByParam.ContainsKey(searchParam))
                            {
                                UtIds = new List<long>();
                                UserProfilesContainer.Instance.SearchedCelebrityIdsByParam[searchParam] = UtIds;
                            }
                            else UtIds = UserProfilesContainer.Instance.SearchedCelebrityIdsByParam[searchParam];
                            foreach (JObject singleObj in searchedContactList)
                            {
                                long utId = (long)singleObj[JsonKeys.UserTableID];
                                CelebrityModel model = null;
                                if (!UserProfilesContainer.Instance.CelebrityModels.TryGetValue(utId, out model))
                                {
                                    model = new CelebrityModel();
                                    UserProfilesContainer.Instance.CelebrityModels[utId] = model;
                                }
                                model.LoadData(singleObj);
                                if (!UtIds.Contains(utId)) UtIds.Add(utId);
                                Application.Current.Dispatcher.Invoke((Action)(() =>
                                {
                                    if (UCMiddlePanelSwitcher._UCCelebritiesSearchPanel != null
                                        && UCMiddlePanelSwitcher._UCCelebritiesSearchPanel.SearchTermTextBox.Text.Trim().Equals(searchParam)
                                        && !UCMiddlePanelSwitcher._UCCelebritiesSearchPanel.SearchedCelebrities.Any(P => P.UserTableID == model.UserTableID))
                                    {
                                        UCMiddlePanelSwitcher._UCCelebritiesSearchPanel.SearchedCelebrities.Insert(UCMiddlePanelSwitcher._UCCelebritiesSearchPanel.SearchedCelebrities.Count - 1, model);
                                        int index = UCMiddlePanelSwitcher._UCCelebritiesSearchPanel.SearchedCelebrities.IndexOf(model);
                                        if ((index + 1) % 3 == 0) model.IsRightMarginOff = true;
                                        else model.IsRightMarginOff = false;
                                    }
                                }));
                            }
                        }
                        else
                        {
                            listuserBasicInfoDTO = new List<UserBasicInfoDTO>();
                            foreach (JObject singleObj in searchedContactList)
                            {
                                searchCount++;
                                long utId = singleObj[JsonKeys.UserTableID] != null ? (long)singleObj[JsonKeys.UserTableID] : 0;
                                UserBasicInfoDTO user = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(utId);
                                if (user == null)
                                {
                                    user = new UserBasicInfoDTO();
                                    user.UserTableID = utId;
                                    FriendDictionaries.Instance.AddIntoBasicInfoDictionary(utId, user);
                                }
                                if (singleObj[JsonKeys.FullName] != null) user.FullName = (string)singleObj[JsonKeys.FullName];
                                if (singleObj[JsonKeys.UserIdentity] != null)
                                {
                                    long uId = 0;
                                    uId = long.Parse(singleObj[JsonKeys.UserIdentity].ToString());
                                    user.RingID = uId;
                                }
                                if (singleObj[JsonKeys.ProfileImage] != null) user.ProfileImage = (string)singleObj[JsonKeys.ProfileImage];
                                if (singleObj[JsonKeys.NumberOfMutualFriend] != null) user.NumberOfMutualFriends = (long)singleObj[JsonKeys.NumberOfMutualFriend];
                                if (singleObj[JsonKeys.MobilePhone] != null) user.MobileNumber = (string)singleObj[JsonKeys.MobilePhone];
                                if (singleObj[JsonKeys.DialingCode] != null) user.MobileDialingCode = (string)singleObj[JsonKeys.DialingCode];
                                if (singleObj[JsonKeys.Email] != null) user.Email = (string)singleObj[JsonKeys.Email];
                                if (singleObj[JsonKeys.HomeCity] != null) user.HomeCity = (string)singleObj[JsonKeys.HomeCity];
                                if (singleObj[JsonKeys.CurrentCity] != null) user.CurrentCity = (string)singleObj[JsonKeys.CurrentCity];
                                if (_JobjFromResponse[JsonKeys.SearchCategory] != null)// && isMatch)
                                {
                                    matchCount++;
                                    listuserBasicInfoDTO.Add(user);
                                }
                            }
                        }
                    }
                }
                if (ProfileType == 0) UI_ProcessContactSearch(searchCount, matchCount, client_packet_id, listuserBasicInfoDTO);
            }
            catch (Exception e) { log.Error("ProcessContactSearch ex==>" + e.StackTrace); }
        }

        public void Process_MISS_CALL_LIST_224(JObject _JobjFromResponse)//224
        {
        }

        public void Process_STORE_CONTACT_LIST_284(JObject _JobjFromResponse)
        {
            new ThreadContactUtIdsRequest().StarThread();
        }

        public void Process_USERS_DETAILS_32(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    JArray ContactList = (JArray)_JobjFromResponse[JsonKeys.ContactList];
                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
                    for (int j = 0; j < ContactList.Count; j++)
                    {
                        JObject SingleContact = (JObject)ContactList.ElementAt(j);
                        if (SingleContact != null)
                        {
                            UserBasicInfoDTO suggestionDTO = new UserBasicInfoDTO();
                            if (SingleContact[JsonKeys.UserIdentity] != null) suggestionDTO.RingID = long.Parse(SingleContact[JsonKeys.UserIdentity].ToString());
                            if (SingleContact[JsonKeys.UserTableID] != null) suggestionDTO.UserTableID = (long)SingleContact[JsonKeys.UserTableID];
                            if (SingleContact[JsonKeys.FullName] != null) suggestionDTO.FullName = (string)SingleContact[JsonKeys.FullName];
                            if (SingleContact[JsonKeys.Gender] != null) suggestionDTO.Gender = (string)SingleContact[JsonKeys.Gender];
                            if (SingleContact[JsonKeys.ProfileImage] != null) suggestionDTO.ProfileImage = (string)SingleContact[JsonKeys.ProfileImage];
                            if (SingleContact[JsonKeys.ProfileImageId] != null) suggestionDTO.ProfileImageId = (Guid)SingleContact[JsonKeys.ProfileImageId];
                            if (SingleContact[JsonKeys.UpdateTime] != null) suggestionDTO.UpdateTime = (long)SingleContact[JsonKeys.UpdateTime];
                            if (SingleContact[JsonKeys.NumberOfMutualFriend] != null) suggestionDTO.NumberOfMutualFriends = (int)SingleContact[JsonKeys.NumberOfMutualFriend];
                            lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY)
                            {
                                if (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.ContainsKey(suggestionDTO.UserTableID))
                                    FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY[suggestionDTO.UserTableID] = suggestionDTO;
                                else FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Add(suggestionDTO.UserTableID, suggestionDTO);
                            }
                            lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
                            {
                                if (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(suggestionDTO.UserTableID)) FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY[suggestionDTO.UserTableID] = true;
                                else FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Add(suggestionDTO.UserTableID, true);
                            }
                            list.Add(suggestionDTO);
                        }
                        AddSuggestions(list);
                    }

                }
            }
            catch (Exception e) { log.Error("Exception in ProcessSuggestionUsersDetails ==>" + e.StackTrace); }
        }

        public void Process_SUGGESTION_IDS_31(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    JArray ContactUtids = (JArray)_JobjFromResponse[JsonKeys.ContactIds];
                    JArray utIds = new JArray();
                    foreach (var item in ContactUtids)
                    {
                        long utId = (long)item;
                        if (DefaultSettings.INITIAL_SUGSTNS_COUNT < 15)
                        {
                            utIds.Add(utId);
                            DefaultSettings.INITIAL_SUGSTNS_COUNT++;
                        }
                        lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
                            if (!FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.ContainsKey(utId)) FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Add(utId, false);
                    }
                    if (utIds.Count > 0) SendDataToServer.SuggestionUsersDetailsRequest(utIds);
                }
            }
            catch (Exception e) { log.Error("Exception in ProcessSuggestionUtids ==>" + e.StackTrace); }
        }

        public void Process_MUTUAL_FRIENDS_118(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.TotalRecords] != null)
                {
                    MutualFriendsSeqCount++;
                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
                    char[] delimiterChars = { '/' };
                    str = str.Split(delimiterChars)[1];
                    int seqTotal = Convert.ToInt32(str);
                    int tf = (int)_JobjFromResponse[JsonKeys.TotalRecords];
                    long utId = (long)_JobjFromResponse[JsonKeys.FutId];
                    if (_JobjFromResponse[JsonKeys.MutualFriendIDs] != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MutualFriendIDs];
                        for (int i = 0; i < jarray.Count; i++)
                        {
                            long i1 = (long)jarray.ElementAt(i);
                            lock (FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY)
                            {
                                if (FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.ContainsKey(utId))
                                {
                                    if (!FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[utId].FriendList.Any(f => f.Equals(i1)))
                                        FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[utId].FriendList.Add(i1);
                                }
                                else
                                {
                                    FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.Add(utId, new FriendInfoDTO());
                                    FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[utId].FriendList.Add(i1);
                                }
                                FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[utId].TotalFriend = tf;
                            }
                        }
                    }
                    if (MutualFriendsSeqCount == seqTotal)
                    {
                        MutualFriendsSeqCount = 0;
                        FriendInfoDTO fiDTO;
                        FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY.TryGetValue(utId, out fiDTO);
                        int i = 0;
                        while (i < fiDTO.FriendList.Count && i < DefaultSettings.CONTENT_SHOW_LIMIT)
                        {
                            FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Add(fiDTO.FriendList[i]);
                            i++;
                        }
                        ShowFriendMutualContactList(utId, FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY);
                    }
                }
                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
                {
                    long utId = (long)_JobjFromResponse[JsonKeys.FutId];
                    NoMutualFriendFoundInFriendsContactList(utId);
                }
            }
            catch (Exception e) { log.Error("ProcessTypeMutualFriends ex ==>" + e.StackTrace); }
        }
        public void Process_FRIEND_CONTACT_LIST_107(JObject _JobjFromResponse)
        {
            processFriendContactList(_JobjFromResponse);
        }

        public void Process_FRIEND_CONTACT_LIST_211(JObject _JobjFromResponse)
        {
            processFriendContactList(_JobjFromResponse);
        }

        public void Process_ADD_FRIEND_127(JObject _JobjFromResponse, long server_packet_id)//127
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && server_packet_id != 0)
                {
                    int FriendshipStatus = _JobjFromResponse[JsonKeys.FriendshipStatus] != null ? Int32.Parse(_JobjFromResponse[JsonKeys.FriendshipStatus].ToString()) : 0;
                    UserBasicInfoDTO userBasicInfo = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable((long)_JobjFromResponse[JsonKeys.UserTableID], _JobjFromResponse);
                    lock (FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY)
                        FriendDictionaries.Instance.SUGGESTION_UTIDS_DICTIONARY.Remove(userBasicInfo.UserTableID);
                    lock (FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY) FriendDictionaries.Instance.UID_USERBASICINFO_SUGGESTION_DICTIONARY.Remove(userBasicInfo.UserTableID); UI_ProcessPushAddFriend(userBasicInfo);
                }
            }
            catch (Exception e) { log.Error("ProcessPushAddFriend ex ==>" + e.StackTrace); }
        }

        public void Process_ACCEPT_FRIEND_129(JObject _JobjFromResponse, long server_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && server_packet_id != 0)
                {
                    UserBasicInfoDTO userBasicInfo = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable((long)_JobjFromResponse[JsonKeys.UserTableID], _JobjFromResponse);
                    if (_JobjFromResponse[JsonKeys.UserIdentity] != null) userBasicInfo.RingID = (long)_JobjFromResponse[JsonKeys.UserIdentity];
                    if (_JobjFromResponse[JsonKeys.FullName] != null) userBasicInfo.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
                    if (_JobjFromResponse[JsonKeys.ProfileImage] != null) userBasicInfo.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
                    if (_JobjFromResponse[JsonKeys.ProfileImageId] != null) userBasicInfo.ProfileImageId = (Guid)_JobjFromResponse[JsonKeys.ProfileImageId];
                    if (_JobjFromResponse[JsonKeys.FriendshipStatus] != null) userBasicInfo.FriendShipStatus = (int)_JobjFromResponse[JsonKeys.FriendshipStatus];
                    if (_JobjFromResponse[JsonKeys.UpdateTime] != null) userBasicInfo.UpdateTime = (long)_JobjFromResponse[JsonKeys.UpdateTime];
                    if (_JobjFromResponse[JsonKeys.CallAccess] != null) userBasicInfo.CallAccess = (int)_JobjFromResponse[JsonKeys.CallAccess];
                    if (_JobjFromResponse[JsonKeys.ChatAccess] != null) userBasicInfo.ChatAccess = (int)_JobjFromResponse[JsonKeys.ChatAccess];
                    if (_JobjFromResponse[JsonKeys.FeedAccess] != null) userBasicInfo.FeedAccess = (int)_JobjFromResponse[JsonKeys.FeedAccess];
                    List<UserBasicInfoDTO> userBasicInfoList = new List<UserBasicInfoDTO>();
                    userBasicInfoList.Add(userBasicInfo);
                    new InsertIntoUserBasicInfoTable(userBasicInfoList).Start();
                    UI_AcceptFriendRequest(userBasicInfo);
                    UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(userBasicInfo.UserTableID, userBasicInfo.RingID, userBasicInfo.FullName, userBasicInfo.ProfileImage);
                    BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(userBasicInfo.UserTableID);
                    if (blockModel.IsBlockedByMe && !(userBasicInfo.CallAccess == StatusConstants.TYPE_ACCESS_BLOCKED && userBasicInfo.ChatAccess == StatusConstants.TYPE_ACCESS_BLOCKED && userBasicInfo.FeedAccess == StatusConstants.TYPE_ACCESS_BLOCKED))
                        HelperMethods.ChangeBlockUnblockSettingsWrapper(StatusConstants.FULL_ACCESS, friendInfoModel, blockModel);
                }
            }
            catch (Exception e) { log.Error("ProcessPushAddFriend ex ==>" + e.Message); }
        }

        public void Process_UPDATE_STORE_CONTACT_LIST_484(JObject _JobjFromResponse)
        {
            MainSwitcher.ThreadManager().ContactUtIdsRequest().StarThread();
        }

        #endregion "Public methods"

        #region "Utility Methods"

        public void ReloadFriendsMutualFriendsProfile()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null)
                    {
                        UCFriendProfile ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                        if (ucFriendProfile._UCFriendContactList != null && ucFriendProfile._UCFriendContactList.MUTUAL_FRIENDS_LOADED)
                        {
                            long utid = ucFriendProfile._UCFriendContactList.utid;
                            SendDataToServer.SendMutualFreindRequest(utid);
                        }
                    }
                }
                catch (Exception ex) { log.Error("Error: ReloadFriendsMutualFriendsProfile() => " + ex.Message + "\n" + ex.StackTrace); }
            });
        }
        public void UI_AcceptFriendRequest(UserBasicInfoDTO userBasicInfo)//129
        {
            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userBasicInfo.UserTableID);
            if (model != null)
            {
                FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, false);
                MainSwitcher.AuthSignalHandler().LoadUserBasicInfoDto(model, userBasicInfo);
                FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_NonFavouriteFriendList);
                SetRequestsForFriendViewedandAdded(userBasicInfo);
                ReloadFriendsMutualFriendsProfile();
                CheckFriendPresence(userBasicInfo.UserTableID);
            }
            else
            {
                model = new UserBasicInfoModel(userBasicInfo);
                FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_NonFavouriteFriendList);
            }
            FriendListController.Instance.FriendDataContainer.TotalFriends();
        }


        public void SetRequestsForFriendViewedandAdded(UserBasicInfoDTO userBasicInfo) ////someone accepted my request,or i accepted someone's req & his UI is already open
        {
            try
            {
                UCFriendProfile ucFriendProfile = null;
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == userBasicInfo.UserTableID)
                {
                    ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(userBasicInfo.RingID))
                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Remove(userBasicInfo.RingID);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (ucFriendProfile._UCFriendNewsFeeds != null)
                        {
                            bool WasVisible = ucFriendProfile._UCFriendNewsFeeds.IsVisible;
                            ucFriendProfile._UCFriendNewsFeeds = new UCFriendNewsFeeds(userBasicInfo.RingID, userBasicInfo.UserTableID, ucFriendProfile.Scroll);
                            if (WasVisible) ucFriendProfile.ShowPost();
                        }
                    });
                    new ThradFriendDetailsInfoRequest().StartThread(userBasicInfo.UserTableID);
                    SendDataToServer.WorkEducationSkillRequest(userBasicInfo.UserTableID);
                    new ThreadFriendPresenceInfo(userBasicInfo.RingID);
                }
            }
            catch (Exception ex) { log.Error("Error: SetRequestsForFriendViewedandAdded() => " + ex.Message + "\n" + ex.StackTrace); }
        }


        public void CheckFriendPresence(long utId)
        {
            try
            {
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utId && UCMiddlePanelSwitcher.View_UCFriendProfilePanel.IsVisible) new ThreadFriendPresenceInfo(utId);
            }
            catch (Exception ex) { log.Error("Error: CheckFriendPresence() => " + ex.Message + "\n" + ex.StackTrace); }
        }

        public void UI_ProcessPushAddFriend(UserBasicInfoDTO userBasicInfo)
        {
            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userBasicInfo.UserTableID);
            if (model != null)
            {
                FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, false);
                MainSwitcher.AuthSignalHandler().LoadUserBasicInfoDto(model, userBasicInfo);
                FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_OutgoingRequestFriendList);
                CheckFriendPresence(userBasicInfo.UserTableID);
            }
            else
            {
                model = new UserBasicInfoModel(userBasicInfo);
                model.VisibilityModel.ShowActionButton = false;
                model.VisibilityModel.ShowLoading = Visibility.Visible;
                FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_OutgoingRequestFriendList);
            }
            if (UCAddFriendPending.Instance != null) UCAddFriendPending.Instance.ShowIncommingAndPendingCounts();
        }

        public void NoMutualFriendFoundInFriendsContactList(long utId)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                UCFriendProfile ucFriendProfile = null;
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utId)
                {
                    ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                    if (ucFriendProfile._UCFriendContactList != null)
                        ucFriendProfile._UCFriendContactList.MakeShowMoreVisible(ucFriendProfile.MutualList.Count < ucFriendProfile.MUTUAL_FRIEND_COUNT_IN_CONTACTLIST, true);
                }
            });
        }

        public void ShowFriendMutualContactList(long utId, List<long> listMFUtID)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    int tf = FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[utId].TotalFriend;
                    UCFriendProfile ucFriendProfile = null;
                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utId)
                    {
                        ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                        ucFriendProfile.MUTUAL_FRIEND_COUNT_IN_CONTACTLIST = tf;
                    }
                    if (UCMutualFriendsPopup.Instance != null && UCMutualFriendsPopup.Instance.utId == utId) UCMutualFriendsPopup.Instance.Total = tf;
                    #region UPDATE UNKNOWN MODEL NMF COUNT
                    UserBasicInfoModel unknownModel = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(utId);
                    if (unknownModel != null && unknownModel.NumberOfMutualFriends != tf)
                    {
                        unknownModel.NumberOfMutualFriends = tf;// to update UnknownFriendList                       
                        UserBasicInfoDTO entity = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(utId);
                        if (entity != null && entity.NumberOfMutualFriends != tf)
                        {
                            entity.NumberOfMutualFriends = tf;//to update UID_USERBASICINFO_DICTIONARY
                            List<UserBasicInfoDTO> userDtoList = new List<UserBasicInfoDTO>();
                            userDtoList.Add(entity);
                            new InsertIntoUserBasicInfoTable(userDtoList).Start();
                        }
                    }
                    #endregion
                    int i = 0;
                    List<UserBasicInfoModel> tempUserList = new List<UserBasicInfoModel>();
                    foreach (long utid in listMFUtID)
                    {
                        UserBasicInfoModel model = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(utid);
                        if (ucFriendProfile != null)
                        {
                            model.VisibilityModel.IsVisibleInFriendsFriendList = Visibility.Visible;
                            lock (ucFriendProfile.MutualList)
                            {
                                if (!ucFriendProfile.MutualList.Any(P => P.ShortInfoModel.UserTableID == utid))
                                    ucFriendProfile.MutualList.Insert(ucFriendProfile.MutualList.Count - 1, model);
                            }

                            if (ucFriendProfile._UCFriendContactList != null) ucFriendProfile._UCFriendContactList.SearchFromAuth(model);
                            if (!String.IsNullOrEmpty(model.ShortInfoModel.ProfileImage) && i < 3) AddMutualFriendForThreeCircle(i++, model, ucFriendProfile);
                            else if (!string.IsNullOrEmpty(model.ShortInfoModel.FullName)) tempUserList.Add(model);
                        }
                        if (UCMutualFriendsPopup.Instance != null && UCMutualFriendsPopup.Instance.utId == utId) UCMutualFriendsPopup.Instance.MutualList.Add(model);
                    }
                    foreach (UserBasicInfoModel user in tempUserList)
                    {
                        if (i >= 3)
                        {
                            tempUserList.Clear();
                            break;
                        }
                        AddMutualFriendForThreeCircle(i++, user, ucFriendProfile);
                    }
                    if (ucFriendProfile != null)
                    {
                        if (ucFriendProfile._UCFriendContactList != null)
                        {
                            ucFriendProfile._UCFriendContactList.MUTUAL_FRIEND_COUNT = ucFriendProfile.MUTUAL_FRIEND_COUNT_IN_CONTACTLIST;
                            ucFriendProfile._UCFriendContactList.MakeShowMoreVisible((ucFriendProfile.MutualList.Count - 1) < ucFriendProfile.MUTUAL_FRIEND_COUNT_IN_CONTACTLIST, true);
                        }
                    }
                    if (UCMutualFriendsPopup.Instance != null && UCMutualFriendsPopup.Instance.utId == utId) UCMutualFriendsPopup.Instance.SetVisibilities();
                    FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Clear();
                }
                catch (Exception ex) { log.Error("Error: ShowFriendMutualContactList() => " + ex.StackTrace); }
            });
        }
        public void AddMutualFriendForThreeCircle(int i, UserBasicInfoModel model, UCFriendProfile ucFriendProfile)
        {
            try
            {
                if (i == 0) ucFriendProfile.UserShortInfoModelOfMutualFriendCircle1 = model.ShortInfoModel;
                else if (i == 1) ucFriendProfile.UserShortInfoModelOfMutualFriendCircle2 = model.ShortInfoModel;
                else if (i == 2) ucFriendProfile.UserShortInfoModelOfMutualFriendCircle3 = model.ShortInfoModel;
            }
            catch (Exception e) { log.Error("Exception in AuthHandler ==>" + e.StackTrace); }
        }

        public void AddSuggestions(List<UserBasicInfoDTO> list)
        {
            SuggestionListLoadUtility.LoadDataFromServer(list);
        }


        public void UI_ProcessContactSearch(long searchCount, long matchCount, string packId, List<UserBasicInfoDTO> listuserBasicInfoDTO) //34
        {
            try
            {
                int ui = RingDictionaries.Instance.SEARCHING_UI_NAME_DICTIONARY.FirstOrDefault(x => x.Value == packId).Key;
                if (ui == StatusConstants.SEARCH_FRIENDLIST) new FriendListSearchResult().StartProcessing(searchCount, matchCount, listuserBasicInfoDTO);
                else if (ui == StatusConstants.SEARCH_ADD_FRIEND_OR_DIALPAD) new AddContactSearchResult().StartProcessing(searchCount, matchCount, listuserBasicInfoDTO);
            }
            catch (Exception e) { log.Error("Exception in AuthHandler ==>" + e.StackTrace); }
        }

        public void UI_ProcessAcceptFriendUpdate(UserBasicInfoDTO userBasicInfo)//329
        {
            try
            {
                UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userBasicInfo.UserTableID);
                if (model != null)
                {
                    FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, false);
                    MainSwitcher.AuthSignalHandler().LoadUserBasicInfoDto(model, userBasicInfo);
                    FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_NonFavouriteFriendList);
                    SetRequestsForFriendViewedandAdded(userBasicInfo);
                    ReloadFriendsMutualFriendsProfile();
                }
                else
                {
                    model = new UserBasicInfoModel(userBasicInfo);
                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                    FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_NonFavouriteFriendList);
                }
                FriendListController.Instance.FriendDataContainer.TotalFriends();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }

        }

        public void UI_ProcessAddFriendUpdate(UserBasicInfoDTO userBasicInfo)//327
        {
            try
            {
                UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userBasicInfo.UserTableID);
                if (model != null)
                {
                    FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, false);
                    MainSwitcher.AuthSignalHandler().LoadUserBasicInfoDto(model, userBasicInfo);
                    FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_IncomingRequestFriendList);
                    CheckFriendPresence(userBasicInfo.UserTableID);
                }
                else
                {
                    model = new UserBasicInfoModel(userBasicInfo);
                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
                    FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_IncomingRequestFriendList);
                }
                if (userBasicInfo.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
                {
                    AppConstants.ADD_FRIEND_NOTIFICATION_COUNT++;
                    FriendListLoadUtility.AddFriendsNotificationForIncomingRequest();
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }

        public void UI_ProcessByteContactList(List<UserBasicInfoDTO> basicInfos, bool success)//23
        {
            if (success)
            {
                if (basicInfos != null)
                {
                    FriendListLoadUtility.LoadDataFromServer(basicInfos);
                    //  HelperMethodsAuth.AuthHandlerInstance.AddFriendListFromServer(basicInfos);
                }
                if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0 && DefaultSettings.FRIEND_LIST_LOADED == false)
                {
                    //  AddFriendsNotificationForIncomingRequest();
                    //     FriendListLoadUtility.AddFriendsNotificationForIncomingRequest();
                    NotifyAllContactListLoaded();
                    new InsertIntoUserBasicInfoTable(FriendDictionaries.Instance.TEMP_CONTACT_LIST, true, () =>
                    {
                        FriendDictionaries.Instance.TEMP_CONTACT_LIST.Clear();
                        return 0;
                    }).Start();
                }
            }
            else if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0 && DefaultSettings.FRIEND_LIST_LOADED == false)
            {

                NotifyAllContactListLoaded();
            }

        }

        private void NotifyAllContactListLoaded()
        {
            try
            {
                UCGuiRingID.Instance.HideAdvertiseSlider();
                Models.Constants.DefaultSettings.FRIEND_LIST_LOADED = true;
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.GetItem();
            }
            catch (Exception ex)
            {
                log.Error("Error: NotifyAllContactListLoaded() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public void UI_ProcessDeleteFriendUpdate(UserBasicInfoDTO userBasicInfo)//328
        {
            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userBasicInfo.UserTableID);
            if (model != null)
            {
                FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, true);
                MainSwitcher.AuthSignalHandler().LoadUserBasicInfoDto(model, userBasicInfo);
                MainSwitcher.AuthSignalHandler().profileSignalHandler.HideFriendProfileTabForNF(userBasicInfo);
                MainSwitcher.AuthSignalHandler().profileSignalHandler.HideFriendAboutInfos(userBasicInfo);
                ReloadFriendsMutualFriendsProfile();
                //AddFriendsNotificationForIncomingRequest();
                FriendListController.Instance.FriendDataContainer.TotalFriends();
            }
        }
        public void NoContactFoundInFriendsContactList(long utid)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                UCFriendProfile ucFriendProfile = null;
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utid)
                {
                    ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                    ucFriendProfile.LoadData();
                }
            });
        }
        public void ShowFriendTotalContactList(long utId, List<UserBasicInfoDTO> listUserBasicInfoDTO)
        {
            Application.Current.Dispatcher.Invoke(() =>
                {
                    try
                    {
                        UCFriendProfile ucFriendProfile = null;
                        if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utId)
                        {
                            ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                            ucFriendProfile.TOTAL_FRIEND_IN_CONTACTLIST_COUNT = FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[utId].TotalFriend;
                            foreach (UserBasicInfoDTO user in listUserBasicInfoDTO)//FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY.Values)
                            {
                                UserBasicInfoModel model = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(user.UserTableID);

                                if (ucFriendProfile._UCFriendContactList != null) ucFriendProfile._UCFriendContactList.SearchFromAuth(model);
                                lock (ucFriendProfile.FriendList)
                                {
                                    if (!ucFriendProfile.FriendList.Any(P => P.ShortInfoModel.UserIdentity == user.RingID))
                                        ucFriendProfile.FriendList.Insert(ucFriendProfile.FriendList.Count - 1, model);
                                }

                                if (!string.IsNullOrEmpty(model.ShortInfoModel.ProfileImage) && ucFriendProfile.TempFriendPreviewImageCount < 4) // ToShowFourImagesInFriendsProfileTab
                                {
                                    lock (ucFriendProfile.TempFriendList)
                                    {
                                        if (ucFriendProfile.TempFriendList.Count == 4 && !ucFriendProfile.TempFriendList.Any(P => P.ShortInfoModel.UserIdentity == user.RingID))
                                            ucFriendProfile.TempFriendList[ucFriendProfile.TempFriendPreviewImageCount++] = model;
                                    }
                                }
                            }
                            ucFriendProfile.UnknownFriendPanel.Visibility = Visibility.Collapsed;
                            ucFriendProfile.DownLoadedFriendPanel.Visibility = Visibility.Visible;
                            ucFriendProfile.LoadData();
                        }
                    }
                    catch (Exception ex) { log.Error("Error: ShowFriendTotalContactList() => " + ex.Message + "\n" + ex.StackTrace); }
                });
        }

        #endregion "Utility methods"
    }
}

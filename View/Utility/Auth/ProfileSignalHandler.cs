﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.Call;
using View.UI.Chat;
using View.UI.Profile.FriendProfile;
using View.Utility.FriendList;
using View.Utility.FriendProfile;
using View.ViewModel;
using View.Utility.DataContainer;
using System.Collections.ObjectModel;

namespace View.Utility.Auth
{
    public class ProfileSignalHandler
    {
        #region "Private Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(ProfileSignalHandler).Name);
        int WorkListSequenceCount = 0, EducationListSequenceCount = 0, SkillListSequenceCount = 0;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Private methods"
        #endregion "Private methods"

        #region "Public Methods"

        public void Process_SINGLE_FRIEND_PRESENCE_INFO_199(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null)
                {
                    long friendTableId = 0;
                    if (_JobjFromResponse[JsonKeys.FutId] != null)
                    {
                        friendTableId = (long)_JobjFromResponse[JsonKeys.FutId];
                    }
                    else if (_JobjFromResponse[JsonKeys.UserTableID] != null)
                    {
                        friendTableId = (long)_JobjFromResponse[JsonKeys.UserTableID];
                    }

                    UserBasicInfoDTO _FriendDetailsInDictionary = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(friendTableId);

                    if (_FriendDetailsInDictionary != null)
                    {
                        if (_JobjFromResponse[JsonKeys.Presence] != null)
                        {
                            _FriendDetailsInDictionary.Presence = (int)_JobjFromResponse[JsonKeys.Presence];
                        }
                        if (_JobjFromResponse[JsonKeys.LastOnlineTime] != null)
                        {
                            _FriendDetailsInDictionary.LastOnlineTime = (long)_JobjFromResponse[JsonKeys.LastOnlineTime];
                        }
                        if (_JobjFromResponse[JsonKeys.Device] != null)
                        {
                            _FriendDetailsInDictionary.Device = (int)_JobjFromResponse[JsonKeys.Device];
                        }
                        if (_JobjFromResponse[JsonKeys.Mood] != null)
                        {
                            _FriendDetailsInDictionary.Mood = (int)_JobjFromResponse[JsonKeys.Mood];
                        }
                        //ChangeFriendProfile(_FriendDetailsInDictionary);
                        UI_ProcessTypeFriendPresenceInfo(_FriendDetailsInDictionary);
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("ProcessTypeFriendPresenceInfo ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

            }
        }

        public void Process_PRESENCE_78(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.UserTableID] != null
                    && (long)_JobjFromResponse[JsonKeys.UserTableID] != DefaultSettings.LOGIN_TABLE_ID)
                {
                    UserBasicInfoDTO user = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary((long)_JobjFromResponse[JsonKeys.UserTableID]);
                    if (user != null)
                    {
                        if (_JobjFromResponse[JsonKeys.FullName] != null)
                        {
                            user.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
                        }
                        if (_JobjFromResponse[JsonKeys.Gender] != null)
                        {
                            user.Gender = (string)_JobjFromResponse[JsonKeys.Gender];
                        }
                        if (_JobjFromResponse[JsonKeys.MobilePhone] != null)
                        {
                            user.MobileNumber = (string)_JobjFromResponse[JsonKeys.MobilePhone];
                        }
                        if (_JobjFromResponse[JsonKeys.DialingCode] != null)
                        {
                            user.MobileDialingCode = (string)_JobjFromResponse[JsonKeys.DialingCode];
                        }
                        if (_JobjFromResponse[JsonKeys.Email] != null)
                        {
                            user.Email = (string)_JobjFromResponse[JsonKeys.Email];
                        }
                        if (_JobjFromResponse[JsonKeys.Presence] != null)
                        {
                            user.Presence = (int)_JobjFromResponse[JsonKeys.Presence];
                        }
                        if (_JobjFromResponse[JsonKeys.FriendshipStatus] != null)
                        {
                            user.FriendShipStatus = (int)_JobjFromResponse[JsonKeys.FriendshipStatus];
                        }
                        if (_JobjFromResponse[JsonKeys.ProfileImage] != null)
                        {
                            user.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
                        }
                        if (_JobjFromResponse[JsonKeys.CoverImage] != null)
                        {
                            user.CoverImage = (string)_JobjFromResponse[JsonKeys.CoverImage];
                        }
                        if (_JobjFromResponse[JsonKeys.BirthDay] != null)
                        {
                            user.BirthDay = (long)_JobjFromResponse[JsonKeys.BirthDay];
                        }
                        if (_JobjFromResponse[JsonKeys.MarriageDay] != null)
                        {
                            user.MarriageDay = (long)_JobjFromResponse[JsonKeys.MarriageDay];
                        }
                        if (_JobjFromResponse[JsonKeys.HomeCity] != null)
                        {
                            user.HomeCity = (string)_JobjFromResponse[JsonKeys.HomeCity];
                        }
                        if (_JobjFromResponse[JsonKeys.CurrentCity] != null)
                        {
                            user.CurrentCity = (string)_JobjFromResponse[JsonKeys.CurrentCity];
                        }
                        if (_JobjFromResponse[JsonKeys.AboutMe] != null)
                        {
                            user.AboutMe = (string)_JobjFromResponse[JsonKeys.AboutMe];
                        }
                        if (_JobjFromResponse[JsonKeys.Privacy] != null)
                        {
                            JArray privacy = (JArray)_JobjFromResponse[JsonKeys.Privacy];

                            user.Privacy = new short[5];
                            for (int j = 0; j < privacy.Count; j++)
                            {
                                user.Privacy[j] = (short)privacy.ElementAt(j);
                            }
                            user.EmailPrivacy = DefaultSettings.userProfile.Privacy[0];
                            user.MobilePrivacy = DefaultSettings.userProfile.Privacy[1];
                            user.ProfileImagePrivacy = DefaultSettings.userProfile.Privacy[2];
                            user.BirthdayPrivacy = DefaultSettings.userProfile.Privacy[3];
                            user.CoverImagePrivacy = DefaultSettings.userProfile.Privacy[4];
                        }

                        List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
                        userList.Add(user);
                        new InsertIntoUserBasicInfoTable(userList).Start();
                        UI_AMPProfileAbout(user);
                        //ShowFriendBasicInfo(user);
                        //ChangeFriendProfile(user);
                        //ChangeCallChatLogsOnFriendUpdate(user.UserIdentity);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessPresence ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_LIST_WORK_234(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null)
                {
                    WorkListSequenceCount++;
                    int seqTotal = Convert.ToInt32(((string)_JobjFromResponse[JsonKeys.Sequence]).Split('/')[1]);
                    long utid = (_JobjFromResponse[JsonKeys.UserTableID] != null) ? (long)_JobjFromResponse[JsonKeys.UserTableID] : DefaultSettings.LOGIN_TABLE_ID;
                    utid = utid == 0 ? DefaultSettings.LOGIN_TABLE_ID : utid;
                    if (_JobjFromResponse[JsonKeys.WorkList] != null)
                    {
                        JArray workList = (JArray)_JobjFromResponse[JsonKeys.WorkList];
                        Dictionary<Guid, WorkDTO> worksBySinglePerson = null;
                        lock (RingDictionaries.Instance.WORK_DICTIONARY)
                        {
                            if (!RingDictionaries.Instance.WORK_DICTIONARY.TryGetValue(utid, out worksBySinglePerson))
                            {
                                worksBySinglePerson = new Dictionary<Guid, WorkDTO>();
                                RingDictionaries.Instance.WORK_DICTIONARY[utid] = worksBySinglePerson;
                            }
                            foreach (JObject singleObj in workList)
                            {
                                WorkDTO work = HelperMethodsModel.BindWorkDetails(singleObj);
                                worksBySinglePerson[work.Id] = work;
                            }
                        }
                    }
                    if (WorkListSequenceCount == seqTotal)
                    {
                        WorkListSequenceCount = 0;
                        ShowWorkList(utid);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessWorkInfoList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_LIST_EDUCATION_235(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null)
                {
                    EducationListSequenceCount++;
                    int seqTotal = Convert.ToInt32(((string)_JobjFromResponse[JsonKeys.Sequence]).Split('/')[1]);
                    long utid = (_JobjFromResponse[JsonKeys.UserTableID] != null) ? (long)_JobjFromResponse[JsonKeys.UserTableID] : DefaultSettings.LOGIN_TABLE_ID;
                    utid = utid == 0 ? DefaultSettings.LOGIN_TABLE_ID : utid;
                    if (_JobjFromResponse[JsonKeys.EducationList] != null)
                    {
                        JArray educationList = (JArray)_JobjFromResponse[JsonKeys.EducationList];
                        Dictionary<Guid, EducationDTO> educationsBySinglePerson = null;
                        lock (RingDictionaries.Instance.EDUCATION_DICTIONARY)
                        {
                            if (!RingDictionaries.Instance.EDUCATION_DICTIONARY.TryGetValue(utid, out educationsBySinglePerson))
                            {
                                educationsBySinglePerson = new Dictionary<Guid, EducationDTO>();
                                RingDictionaries.Instance.EDUCATION_DICTIONARY[utid] = educationsBySinglePerson;
                            }
                            foreach (JObject singleObj in educationList)
                            {
                                EducationDTO education = HelperMethodsModel.BindEducationDetails(singleObj);
                                educationsBySinglePerson[education.Id] = education;
                            }
                        }
                    }
                    if (EducationListSequenceCount == seqTotal)
                    {
                        EducationListSequenceCount = 0;
                        ShowEducationList(utid);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessEducationList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_LIST_SKILL_236(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null)
                {
                    SkillListSequenceCount++;
                    int seqTotal = Convert.ToInt32(((string)_JobjFromResponse[JsonKeys.Sequence]).Split('/')[1]);
                    long utid = (_JobjFromResponse[JsonKeys.UserTableID] != null) ? (long)_JobjFromResponse[JsonKeys.UserTableID] : DefaultSettings.LOGIN_TABLE_ID;
                    utid = utid == 0 ? DefaultSettings.LOGIN_TABLE_ID : utid;
                    if (_JobjFromResponse[JsonKeys.SkillList] != null)
                    {
                        JArray SkillList = (JArray)_JobjFromResponse[JsonKeys.SkillList];
                        Dictionary<Guid, SkillDTO> SkillsBySinglePerson = null;
                        lock (RingDictionaries.Instance.SKILL_DICTIONARY)
                        {
                            if (!RingDictionaries.Instance.SKILL_DICTIONARY.TryGetValue(utid, out SkillsBySinglePerson))
                            {
                                SkillsBySinglePerson = new Dictionary<Guid, SkillDTO>();
                                RingDictionaries.Instance.SKILL_DICTIONARY[utid] = SkillsBySinglePerson;
                            }

                            foreach (JObject singleObj in SkillList)
                            {
                                SkillDTO Skill = HelperMethodsModel.BindSkillDetails(singleObj);
                                SkillsBySinglePerson[Skill.Id] = Skill;
                            }
                        }
                    }
                    if (SkillListSequenceCount == seqTotal)
                    {
                        SkillListSequenceCount = 0;
                        ShowSkillList(utid);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessSkillList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_ADD_WORK_227(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && DefaultSettings.TEMP_WorkObject != null)
                {
                    if (_JobjFromResponse[JsonKeys.WorkObj] != null)
                    {
                        JObject jObj = (JObject)_JobjFromResponse[JsonKeys.WorkObj];
                        if (jObj[JsonKeys.WorkId] != null)
                            DefaultSettings.TEMP_WorkObject.Id = (Guid)jObj[JsonKeys.WorkId];
                    }
                    Dictionary<Guid, WorkDTO> worksBySinglePerson = null;
                    lock (RingDictionaries.Instance.WORK_DICTIONARY)
                    {
                        if (!RingDictionaries.Instance.WORK_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out worksBySinglePerson))
                        {
                            worksBySinglePerson = new Dictionary<Guid, WorkDTO>();
                            RingDictionaries.Instance.WORK_DICTIONARY[DefaultSettings.LOGIN_TABLE_ID] = worksBySinglePerson;
                        }
                    }
                    worksBySinglePerson[DefaultSettings.TEMP_WorkObject.Id] = DefaultSettings.TEMP_WorkObject;

                    //through interface,call view to add my new work
                    AddtoMyWork();
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessAddWork ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_ADD_EDUCATION_231(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && DefaultSettings.TEMP_EducationObject != null)
                {
                    if (_JobjFromResponse[JsonKeys.EducationObj] != null)
                    {
                        JObject jObj = (JObject)_JobjFromResponse[JsonKeys.EducationObj];
                        if (jObj[JsonKeys.SchoolId] != null)
                            DefaultSettings.TEMP_EducationObject.Id = (Guid)jObj[JsonKeys.SchoolId];
                    }
                    Dictionary<Guid, EducationDTO> EducationsBySinglePerson = null;
                    lock (RingDictionaries.Instance.EDUCATION_DICTIONARY)
                    {
                        if (!RingDictionaries.Instance.EDUCATION_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out EducationsBySinglePerson))
                        {
                            EducationsBySinglePerson = new Dictionary<Guid, EducationDTO>();
                            RingDictionaries.Instance.EDUCATION_DICTIONARY[DefaultSettings.LOGIN_TABLE_ID] = EducationsBySinglePerson;
                        }
                    }
                    EducationsBySinglePerson[DefaultSettings.TEMP_EducationObject.Id] = DefaultSettings.TEMP_EducationObject;

                    //through interface,call view to add my new Education
                    AddtoMyEducation();
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessAddEducation ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO_336(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null)
                {
                    long friendTableId = 0;
                    if (_JobjFromResponse[JsonKeys.FutId] != null)
                    {
                        friendTableId = (long)_JobjFromResponse[JsonKeys.FutId];
                    }
                    else if (_JobjFromResponse[JsonKeys.UserTableID] != null)
                    {
                        friendTableId = (long)_JobjFromResponse[JsonKeys.UserTableID];
                    }

                    UserBasicInfoDTO _FriendDetailsInDictionary = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(friendTableId);
                    //FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(friendTableId, out _FriendDetailsInDictionary);

                    if (_FriendDetailsInDictionary != null)
                    {
                        if (_JobjFromResponse[JsonKeys.Presence] != null)
                        {
                            _FriendDetailsInDictionary.Presence = (int)_JobjFromResponse[JsonKeys.Presence];
                        }
                        if (_JobjFromResponse[JsonKeys.LastOnlineTime] != null)
                        {
                            _FriendDetailsInDictionary.LastOnlineTime = (long)_JobjFromResponse[JsonKeys.LastOnlineTime];
                        }
                        if (_JobjFromResponse[JsonKeys.Device] != null)
                        {
                            _FriendDetailsInDictionary.Device = (int)_JobjFromResponse[JsonKeys.Device];
                        }
                        if (_JobjFromResponse[JsonKeys.Mood] != null)
                        {
                            _FriendDetailsInDictionary.Mood = (int)_JobjFromResponse[JsonKeys.Mood];
                        }
                        UI_ProcessTypeFriendPresenceInfo(_FriendDetailsInDictionary);
                    }
                }
            }
            catch (Exception ex)
            {

                log.Error("ProcessTypeFriendPresenceInfo ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

            }
        }

        public void Process_ADD_SKILL_237(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && DefaultSettings.TEMP_SkillObject != null)
                {
                    if (_JobjFromResponse[JsonKeys.SkillObj] != null)
                    {
                        JObject jObj = (JObject)_JobjFromResponse[JsonKeys.SkillObj];
                        if (jObj[JsonKeys.SkillId] != null)
                            DefaultSettings.TEMP_SkillObject.Id = (Guid)jObj[JsonKeys.SkillId];
                    }
                    Dictionary<Guid, SkillDTO> SkillsBySinglePerson = null;
                    lock (RingDictionaries.Instance.SKILL_DICTIONARY)
                    {
                        if (!RingDictionaries.Instance.SKILL_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out SkillsBySinglePerson))
                        {
                            SkillsBySinglePerson = new Dictionary<Guid, SkillDTO>();
                            RingDictionaries.Instance.SKILL_DICTIONARY[DefaultSettings.LOGIN_TABLE_ID] = SkillsBySinglePerson;
                        }
                    }
                    SkillsBySinglePerson[DefaultSettings.TEMP_SkillObject.Id] = DefaultSettings.TEMP_SkillObject;

                    AddtoMySkill();
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessAddSkill ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        #endregion "Public methods"

        #region "Utility methods"

        private void UI_ProcessTypeFriendPresenceInfo(UserBasicInfoDTO userBasicInfo)
        {
            AddIntoModelDictionary(userBasicInfo);
        }

        private UserBasicInfoModel AddIntoModelDictionary(UserBasicInfoDTO userDTO, UserBasicInfoModel model = null)
        {
            if (model == null)
                model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userDTO.UserTableID);
            if (model != null)
            {
                LoadUserBasicInfoDto(model, userDTO);
            }
            else
            {
                model = new UserBasicInfoModel(userDTO);
                FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
            }
            return model;
        }

        private void LoadUserBasicInfoDto(UserBasicInfoModel model, UserBasicInfoDTO userDTO)
        {
            if (model != null)
            {
                model.LoadData(userDTO);
                model.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
                model.OnPropertyChanged("Presence");
                model.OnPropertyChanged("CallAccess");
                model.OnPropertyChanged("ChatAccess");
                model.OnPropertyChanged("FeedAccess");
                model.OnPropertyChanged("CurrentInstance");
                model.ShortInfoModel.OnPropertyChanged("CurrentInstance");
            }

        }

        //public void RemoveSingleFriendFromUIList(UserBasicInfoDTO user)
        //{
        //    FriendListLoadUtility.RemoveAModelFromUI(user, null, true);
        //    //   Application.Current.Dispatcher.Invoke(() =>
        //    //{
        //    //    try
        //    //    {
        //    //        FriendListLoadUtility.RemoveSingleFriendFromUILists(user, true);
        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        log.Error("Error: RemoveSingleFriendFromUIList() => " + ex.Message + "\n" + ex.StackTrace);
        //    //    }
        //    //});
        //}

        public void ShowCallSettingsInfo()
        {
            try
            {
                if (RingIDViewModel.Instance._WNRingIDSettings != null && RingIDViewModel.Instance._WNRingIDSettings._UCCallSettings != null)
                {
                    RingIDViewModel.Instance._WNRingIDSettings._UCCallSettings.LoadCallSettings();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowCallSettingsInfo() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowPrivacySettingsInfo()
        {
            try
            {
                if (RingIDViewModel.Instance._WNRingIDSettings != null && RingIDViewModel.Instance._WNRingIDSettings._PrivacySettingsInstance != null)
                {
                    RingIDViewModel.Instance._WNRingIDSettings._PrivacySettingsInstance.LoadPrivacy();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowPrivacySettingsInfo() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowBasicInfo(long utid)
        {
            try
            {
                if (utid == DefaultSettings.LOGIN_TABLE_ID)
                {
                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ShowMyBasicInfos();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowBasicInfo() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void SetRequestsForFriendViewedandAdded(UserBasicInfoDTO userBasicInfo) ////someone accepted my request,or i accepted someone's req & his UI is already open
        {
            try
            {
                UCFriendProfile ucFriendProfile = null;
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == userBasicInfo.UserTableID)
                {
                    ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(userBasicInfo.RingID))
                    {
                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Remove(userBasicInfo.RingID);
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (ucFriendProfile._UCFriendNewsFeeds != null)
                        {
                            bool WasVisible = ucFriendProfile._UCFriendNewsFeeds.IsVisible;
                            ucFriendProfile._UCFriendNewsFeeds = new UCFriendNewsFeeds(userBasicInfo.RingID, userBasicInfo.UserTableID, ucFriendProfile.Scroll);
                            //ucFriendProfile._UCFriendNewsFeeds.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_ACCEPTED;
                            if (WasVisible) ucFriendProfile.ShowPost();
                        }
                    });
                    new ThradFriendDetailsInfoRequest().StartThread(userBasicInfo.UserTableID);
                    //      new WorkEducationSkillRequest(userBasicInfo.UserTableID);
                    SendDataToServer.WorkEducationSkillRequest(userBasicInfo.UserTableID);
                    new ThreadFriendPresenceInfo(userBasicInfo.RingID);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: SetRequestsForFriendViewedandAdded() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideFriendAboutInfos(UserBasicInfoDTO userBasicInfo) ////someone deleted me from freindlist,or i deleted someone from freindlist & his UI is already open
        {
            try
            {
                UCFriendProfile ucFriendProfile = null;
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == userBasicInfo.UserTableID)
                {
                    ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                    if (ucFriendProfile._UCFriendAbout != null)
                    {
                        ucFriendProfile._UCFriendAbout.ucFriendBasicInfo.HideFriendAboutInfos();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: HideFriendAboutInfos() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void HideFriendProfileTabForNF(UserBasicInfoDTO userBasicInfo)
        {
            try
            {
                UCFriendProfile ucFriendProfile = null;
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == userBasicInfo.UserTableID)
                {
                    ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(userBasicInfo.RingID))
                    {
                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Remove(userBasicInfo.RingID);
                    }
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (ucFriendProfile._UCFriendNewsFeeds != null)
                        {
                            bool WasVisible = ucFriendProfile._UCFriendNewsFeeds.IsVisible;
                            ucFriendProfile._UCFriendNewsFeeds = new UCFriendNewsFeeds(userBasicInfo.RingID, userBasicInfo.UserTableID, ucFriendProfile.Scroll);
                            ucFriendProfile._UCFriendNewsFeeds.FriendShipStatus = 0;
                            if (WasVisible) ucFriendProfile.ShowPost();
                        }
                    });
                    if (ucFriendProfile.SelectedBorder > 0 && ucFriendProfile.SelectedBorder % 2 == 0)
                    {
                        ucFriendProfile.SelectedBorder = 1;
                        if (ucFriendProfile._UCFriendAbout == null)
                        {
                            ShowFriendBasicInfo(userBasicInfo);
                        }
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            ucFriendProfile.UCFPContentSwitcherInstance.Content = ucFriendProfile._UCFriendAbout;
                        });
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void ShowFriendBasicInfo(UserBasicInfoDTO user)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    UCFriendProfile ucFriendProfile = null;
                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == user.UserTableID)
                    {
                        ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                        if (ucFriendProfile._UCFriendAbout == null)
                        {
                            //new WorkEducationSkillRequest(ucFriendProfile.FriendBasicInfoModel.UserTableID);
                            ucFriendProfile._UCFriendAbout = new UCFriendAbout(ucFriendProfile.FriendBasicInfoModel.ShortInfoModel.FullName);
                        }
                        UserBasicInfoModel friendModel = new UserBasicInfoModel(user);
                        ucFriendProfile._UCFriendAbout.ucFriendBasicInfo.LoadFriendBasicInfoFromModel(friendModel);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: ShowFriendBasicInfo() => " + ex.Message + "\n" + ex.StackTrace);
                }
            });
        }

        public void ShowWorkList(long utid)
        {
            try
            {
                if (utid == DefaultSettings.LOGIN_TABLE_ID)
                {
                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer != null)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer.ShowMyWorksList();
                    }
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {

                        UCFriendProfile ucFriendProfile = null;
                        if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utid)
                        {
                            ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                            if (ucFriendProfile._UCFriendAbout != null && ucFriendProfile._UCFriendAbout.ucFriendProfessionalCareer != null)
                            {
                                ucFriendProfile._UCFriendAbout.ucFriendProfessionalCareer.LoadMyFriendsWorkToModels(utid);
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowWorkList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowEducationList(long utid)
        {
            try
            {
                if (utid == DefaultSettings.LOGIN_TABLE_ID)
                {

                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation != null)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation.ShowMyEducationsList();
                    }
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        UCFriendProfile ucFriendProfile = null;
                        if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utid)
                        {
                            ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                            if (ucFriendProfile._UCFriendAbout != null && ucFriendProfile._UCFriendAbout.ucFriendEducation != null)
                            {
                                ucFriendProfile._UCFriendAbout.ucFriendEducation.LoadMyFriendsEduToModels(utid);
                            }
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowEducationList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ShowSkillList(long utid)
        {
            try
            {
                if (utid == DefaultSettings.LOGIN_TABLE_ID)
                {
                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill != null)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill.ShowMySkillsList();
                    }
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        UCFriendProfile ucFriendProfile = null;
                        if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utid)
                        {
                            ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                            if (ucFriendProfile._UCFriendAbout != null && ucFriendProfile._UCFriendAbout.ucFriendSkill != null) { ucFriendProfile._UCFriendAbout.ucFriendSkill.LoadMyFriendsSkillsToModels(utid); }
                        }
                    });
                }
            }
            catch (Exception ex) { log.Error("Error: ShowSkillList() => " + ex.Message + "\n" + ex.StackTrace); }
        }

        public void AddtoMyWork()
        {
            try
            {
                SingleWorkModel workModel = new SingleWorkModel();
                workModel.LoadData(DefaultSettings.TEMP_WorkObject);
                if (!RingIDViewModel.Instance.MyWorkModelsList.Any(p => p.WId == workModel.WId))
                {
                    Application.Current.Dispatcher.Invoke(() => { RingIDViewModel.Instance.MyWorkModelsList.Insert(0, workModel); });
                }
                workModel.WVisibilityViewMode = Visibility.Visible;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer != null)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer.Absent = Visibility.Collapsed;
                    }
                });
            }
            catch (Exception ex) { log.Error("Error: AddtoMyWork() => " + ex.Message + "\n" + ex.StackTrace); }
        }

        public void AddtoMyEducation()
        {
            try
            {
                SingleEducationModel EducationModel = new SingleEducationModel();
                EducationModel.LoadData(DefaultSettings.TEMP_EducationObject);
                if (!RingIDViewModel.Instance.MyEducationModelsList.Any(p => p.EId == EducationModel.EId))
                {
                    Application.Current.Dispatcher.Invoke(() => { RingIDViewModel.Instance.MyEducationModelsList.Insert(0, EducationModel); });
                }
                EducationModel.EVisibilityViewMode = Visibility.Visible;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation != null) UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation.Absent = Visibility.Collapsed;
                });
            }
            catch (Exception ex) { log.Error("Error: AddtoMyEducation() => " + ex.StackTrace); }
        }

        public void AddtoMySkill()
        {
            try
            {
                SingleSkillModel SkillModel = new SingleSkillModel();
                SkillModel.LoadData(DefaultSettings.TEMP_SkillObject);
                if (!RingIDViewModel.Instance.MySkillModelsList.Any(p => p.SKId == SkillModel.SKId))
                {
                    Application.Current.Dispatcher.Invoke(() => { RingIDViewModel.Instance.MySkillModelsList.Insert(0, SkillModel); });
                }
                //SkillModel.SVisibilityEditMode = Visibility.Collapsed;
                SkillModel.SVisibilityViewMode = Visibility.Visible;
                SkillModel.SErrorString = "";
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill != null)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill.Absent = Visibility.Collapsed;
                    }
                });
            }
            catch (Exception ex) { log.Error("Error: AddtoMySkill() => " + ex.StackTrace); }
        }

        public void CheckFriendPresence(long utId)
        {
            try
            {
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utId && UCMiddlePanelSwitcher.View_UCFriendProfilePanel.IsVisible) new ThreadFriendPresenceInfo(utId);
            }
            catch (Exception ex) { log.Error("Error: CheckFriendPresence() => " + ex.StackTrace); }
        }

        public void UI_AMPProfileAbout(UserBasicInfoDTO userBasicInfo)
        {
            AddIntoModelDictionary(userBasicInfo);
            ShowFriendBasicInfo(userBasicInfo);
            ChangeCallChatLogsOnFriendUpdate(userBasicInfo.RingID);
        }

        public void ChangeCallChatLogsOnFriendUpdate(long uid)
        {
            try
            {
                if (UCCallLog.Instance != null && RingIDViewModel.Instance.CallLogModelsList.Count > 0)
                {
                    List<RecentModel> recentModels = RingIDViewModel.Instance.CallLogModelsList.Where(item => item.CallLog.FriendTableID == uid).ToList();
                    foreach (RecentModel r in recentModels) r.CallLog.OnPropertyChanged("FriendIdentity");
                }
                if (UCChatLogPanel.Instance != null && RingIDViewModel.Instance.ChatLogModelsList.Count > 0)
                {
                    RecentModel recentModel = RingIDViewModel.Instance.ChatLogModelsList.Where(item => item.ContactID.Equals(uid.ToString())).FirstOrDefault();
                    if (recentModel != null) recentModel.OnPropertyChanged("Message");
                }
            }
            catch (Exception ex) { log.Error("Error: ChangeCallChatLogsOnFriendUpdate() => " + ex.StackTrace); }
        }

        public void UI_UnknownProfileInfoRequest(UserBasicInfoDTO unknownProfileDTO, bool alreadyFriendRequested)
        {
            AddIntoModelDictionary(unknownProfileDTO, null);
            ShowFriendBasicInfo(unknownProfileDTO);
            ChangeCallChatLogsOnFriendUpdate(unknownProfileDTO.RingID);
            if (alreadyFriendRequested) FriendListLoadUtility.MoveAFriendOneToAnotherList(unknownProfileDTO);
        }


        public void UI_FriendDetailsInfoRequest(UserBasicInfoDTO userBasicInfo)
        {
            ChangeFriendProfile(userBasicInfo);
        }

        public void UI_ProcessDeleteFriend(UserBasicInfoDTO userBasicInfo)//128
        {
            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userBasicInfo.UserTableID);
            if (model != null)
            {
                FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, true);
                LoadUserBasicInfoDto(model, userBasicInfo);
                HideFriendProfileTabForNF(userBasicInfo);
                HideFriendAboutInfos(userBasicInfo);
                FriendListController.Instance.FriendDataContainer.TotalFriends();
            }
        }

        private void ChangeFriendProfile(UserBasicInfoDTO userDTO)
        {
            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userDTO.UserTableID);
            if (userDTO.UserTableID == DefaultSettings.RINGID_OFFICIAL_UTID)
            {
                if (model == null) FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModelsByDto(userDTO);
                else FriendListLoadUtility.AddInfoInModel(model, userDTO);
            }
            else if (Models.Constants.DefaultSettings.FRIEND_LIST_LOADED && Models.Constants.DefaultSettings.FRIEND_LIST_LOADED_FROM_DB)
            {
                try
                {
                    if (model != null)
                    {
                        model.LoadData(userDTO);
                        model.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
                        model.OnPropertyChanged("Presence");
                        model.OnPropertyChanged("CallAccess");
                        model.OnPropertyChanged("ChatAccess");
                        model.OnPropertyChanged("FeedAccess");
                        model.OnPropertyChanged("CurrentInstance");
                        model.ShortInfoModel.OnPropertyChanged("CurrentInstance");
                    }
                    else FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModelsByDto(userDTO);
                }
                catch (Exception ex) { log.Error("Error: ChangeFriendProfile() => " + ex.StackTrace); }
            }
        }
        #endregion "Utility methods"
    }
}

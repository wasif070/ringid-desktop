﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility.Channel;

namespace View.Utility.Auth
{
    public class ChannelSignalHandler
    {

        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ChannelSignalHandler).Name);
        #endregion "Private Fields"

        public static ChannelDTO MakeChannelDTO(JObject _JobjFromResponse)
        {
            ChannelDTO channelDTO = new ChannelDTO();
            if (_JobjFromResponse[JsonKeys.ChannelID] != null)
            {
                channelDTO.ChannelID = (Guid)_JobjFromResponse[JsonKeys.ChannelID];
            }
            if (_JobjFromResponse[JsonKeys.ChannelOwnerID] != null)
            {
                channelDTO.OwnerID = (long)_JobjFromResponse[JsonKeys.ChannelOwnerID];
            }
            if (_JobjFromResponse[JsonKeys.Title] != null)
            {
                channelDTO.Title = (string)_JobjFromResponse[JsonKeys.Title];
            }
            if (_JobjFromResponse[JsonKeys.Description] != null)
            {
                channelDTO.Description = (string)_JobjFromResponse[JsonKeys.Description];
            }
            if (_JobjFromResponse[JsonKeys.ProfileImage] != null)
            {
                channelDTO.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
            }
            if (_JobjFromResponse[JsonKeys.ProfileImageWidth] != null)
            {
                channelDTO.ProfileImageWidth = (int)_JobjFromResponse[JsonKeys.ProfileImageWidth];
            }
            if (_JobjFromResponse[JsonKeys.ProfileImageHeight] != null)
            {
                channelDTO.ProfileImageHeight = (int)_JobjFromResponse[JsonKeys.ProfileImageHeight];
            }
            if (_JobjFromResponse[JsonKeys.ProfileImageId] != null)
            {
                channelDTO.ProfileImageID = (Guid)_JobjFromResponse[JsonKeys.ProfileImageId];
            }
            if (_JobjFromResponse[JsonKeys.CoverImage] != null)
            {
                channelDTO.CoverImage = (string)_JobjFromResponse[JsonKeys.CoverImage];
            }
            if (_JobjFromResponse[JsonKeys.CoverImageWidth] != null)
            {
                channelDTO.CoverImageWidth = (int)_JobjFromResponse[JsonKeys.CoverImageWidth];
            }
            if (_JobjFromResponse[JsonKeys.CoverImageHeight] != null)
            {
                channelDTO.CoverImageHeight = (int)_JobjFromResponse[JsonKeys.CoverImageHeight];
            }
            if (_JobjFromResponse[JsonKeys.CoverImageId] != null)
            {
                channelDTO.CoverImageID = (Guid)_JobjFromResponse[JsonKeys.CoverImageId];
            }
            if (_JobjFromResponse[JsonKeys.CropImageX] != null)
            {
                channelDTO.CoverImageX = (int)_JobjFromResponse[JsonKeys.CropImageX];
            }
            if (_JobjFromResponse[JsonKeys.CropImageY] != null)
            {
                channelDTO.CoverImageY = (int)_JobjFromResponse[JsonKeys.CropImageY];
            }
            if (_JobjFromResponse[JsonKeys.ChannelStatus] != null)
            {
                channelDTO.ChannelStatus = (int)_JobjFromResponse[JsonKeys.ChannelStatus];
            }
            if (_JobjFromResponse[JsonKeys.ChannelType] != null)
            {
                channelDTO.ChannelType = (int)_JobjFromResponse[JsonKeys.ChannelType];
            }
            if (_JobjFromResponse[JsonKeys.SubscriberCount] != null)
            {
                channelDTO.SubscriberCount = (long)_JobjFromResponse[JsonKeys.SubscriberCount];
            }
            if (_JobjFromResponse[JsonKeys.SubscriptionTime] != null)
            {
                channelDTO.SubscriptionTime = (long)_JobjFromResponse[JsonKeys.SubscriptionTime];
            }
            else
            {
                channelDTO.SubscriptionTime = 0;
            }
            if (_JobjFromResponse[JsonKeys.StreamViewerCount] != null)
            {
                channelDTO.ViewCount = (long)_JobjFromResponse[JsonKeys.StreamViewerCount];
            }
            if (_JobjFromResponse[JsonKeys.ChannelCreationTime] != null)
            {
                channelDTO.CreationTime = (long)_JobjFromResponse[JsonKeys.ChannelCreationTime];
            }
            if (_JobjFromResponse[JsonKeys.Country] != null)
            {
                channelDTO.Country = (string)_JobjFromResponse[JsonKeys.Country];
            }
            if (_JobjFromResponse[JsonKeys.ChannelIP] != null)
            {
                channelDTO.ChannelIP = (string)_JobjFromResponse[JsonKeys.ChannelIP];
            }
            if (_JobjFromResponse[JsonKeys.ChannelPort] != null)
            {
                channelDTO.ChannelPort = (int)_JobjFromResponse[JsonKeys.ChannelPort];
            }
            if (_JobjFromResponse[JsonKeys.StreamIP] != null)
            {
                channelDTO.StreamIP = (string)_JobjFromResponse[JsonKeys.StreamIP];
            }
            if (_JobjFromResponse[JsonKeys.StreamPort] != null)
            {
                channelDTO.StreamPort = (int)_JobjFromResponse[JsonKeys.StreamPort];
            }
            if (_JobjFromResponse[JsonKeys.ChannelCatetoryList] != null)
            {
                JArray jArray = (JArray)_JobjFromResponse[JsonKeys.ChannelCatetoryList];
                channelDTO.CategoryList = new List<ChannelCategoryDTO>();
                foreach (JObject catObj in jArray)
                {
                    ChannelCategoryDTO catDTO = new ChannelCategoryDTO();
                    catDTO.CategoryID = (int)catObj[JsonKeys.StreamCategoryId];
                    catDTO.CategoryName = (string)catObj[JsonKeys.Category];
                    channelDTO.CategoryList.Add(catDTO);
                }
            }
            if (_JobjFromResponse[JsonKeys.ChannelMediaList] != null)
            {
                JArray jArray = (JArray)_JobjFromResponse[JsonKeys.ChannelMediaList];
                channelDTO.MediaList = new List<ChannelMediaDTO>();
                foreach (JObject catObj in jArray)
                {
                    channelDTO.MediaList.Add(MakeChannelMediaDTO(catObj));
                }
            }

            return channelDTO;
        }

        public static ChannelMediaDTO MakeChannelMediaDTO(JObject _JobjFromResponse)
        {
            ChannelMediaDTO mediaDTO = new ChannelMediaDTO();
            if (_JobjFromResponse[JsonKeys.ChannelID] != null)
            {
                mediaDTO.ChannelID = (Guid)_JobjFromResponse[JsonKeys.ChannelID];
            }
            if (_JobjFromResponse[JsonKeys.ChannelOwnerID] != null)
            {
                mediaDTO.OwnerID = (long)_JobjFromResponse[JsonKeys.ChannelOwnerID];
            }
            if (_JobjFromResponse[JsonKeys.Title] != null)
            {
                mediaDTO.Title = (string)_JobjFromResponse[JsonKeys.Title];
            }
            if (_JobjFromResponse[JsonKeys.ChannelMediaArtist] != null)
            {
                mediaDTO.Artist = (string)_JobjFromResponse[JsonKeys.ChannelMediaArtist];
            }
            if (_JobjFromResponse[JsonKeys.Description] != null)
            {
                mediaDTO.Description = (string)_JobjFromResponse[JsonKeys.Description];
            }
            if (_JobjFromResponse[JsonKeys.ChannelMediaID] != null)
            {
                mediaDTO.MediaID = (Guid)_JobjFromResponse[JsonKeys.ChannelMediaID];
            }
            if (_JobjFromResponse[JsonKeys.ChannelMediaType] != null)
            {
                mediaDTO.MediaType = (int)_JobjFromResponse[JsonKeys.ChannelMediaType];
            }
            if (_JobjFromResponse[JsonKeys.ChannelMediaUrl] != null)
            {
                mediaDTO.MediaUrl = (string)_JobjFromResponse[JsonKeys.ChannelMediaUrl];
            }
            if (_JobjFromResponse[JsonKeys.ChannelThumbImageUrl] != null)
            {
                mediaDTO.ThumbImageUrl = (string)_JobjFromResponse[JsonKeys.ChannelThumbImageUrl];
            }
            if (_JobjFromResponse[JsonKeys.ChannelThumbImageWidth] != null)
            {
                mediaDTO.ThumbImageWidth = (int)_JobjFromResponse[JsonKeys.ChannelThumbImageWidth];
            }
            if (_JobjFromResponse[JsonKeys.ChannelThumbImageHeight] != null)
            {
                mediaDTO.ThumbImageHeight = (int)_JobjFromResponse[JsonKeys.ChannelThumbImageHeight];
            }
            if (_JobjFromResponse[JsonKeys.MediaDuration] != null)
            {
                mediaDTO.Duration = (int)_JobjFromResponse[JsonKeys.MediaDuration];
            }
            if (_JobjFromResponse[JsonKeys.Status] != null)
            {
                mediaDTO.MediaStatus = (int)_JobjFromResponse[JsonKeys.Status];
            }
            if (_JobjFromResponse[JsonKeys.ChannelType] != null)
            {
                mediaDTO.ChannelType = (int)_JobjFromResponse[JsonKeys.ChannelType];
            }
            if (_JobjFromResponse[JsonKeys.ChannelMediaStartTime] != null)
            {
                mediaDTO.StartTime = (long)_JobjFromResponse[JsonKeys.ChannelMediaStartTime];
            }
            return mediaDTO;
        }

        #region "Signal Handler Methods"

        public void PROCESS_CREATE_CHANNEL_REQUEST_2015(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelDTO] != null)
            {
                ChannelDTO channelDTO = MakeChannelDTO((JObject)_JobjFromResponse[JsonKeys.ChannelDTO]);
            }
        }

        public void PROCESS_GET_CHANNEL_CATEGORY_LIST_REQUEST_2016(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelCatetoryList] != null)
            {
                JArray jArray = (JArray)_JobjFromResponse[JsonKeys.ChannelCatetoryList];
                List<ChannelCategoryDTO> categoryDTOs = new List<ChannelCategoryDTO>();
                foreach (JObject catObj in jArray)
                {
                    ChannelCategoryDTO catDTO = new ChannelCategoryDTO();
                    catDTO.CategoryID = (int)catObj[JsonKeys.StreamCategoryId];
                    catDTO.CategoryName = (string)catObj[JsonKeys.Category];
                    categoryDTOs.Add(catDTO);
                }
                ChannelHelpers.AddChannelCategory(categoryDTOs);
            }
        }

        public void PROCESS_UPDATE_CHANNEL_INFO_REQUEST_2017(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelDTO] != null)
            {
                ChannelDTO channelDTO = MakeChannelDTO((JObject)_JobjFromResponse[JsonKeys.ChannelDTO]);
            }
        }

        public void PROCESS_GET_FOLLOWING_CHANNEL_LIST_REQUEST_2019(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelList] != null)
            {
                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ChannelList];
                foreach (JObject jChannel in jarray)
                {
                    ChannelDTO channelDTO = MakeChannelDTO(jChannel);
                    ChannelHelpers.AddIntoFollowingChannelList(channelDTO);
                }
            }
        }

        public void PROCESS_GET_MOST_VIEWED_CHANNEL_LIST_REQUEST_2020(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelList] != null)
            {
                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ChannelList];
                foreach (JObject jChannel in jarray)
                {
                    ChannelDTO channelDTO = MakeChannelDTO(jChannel);
                    ChannelHelpers.AddIntoMostViewedChannelList(channelDTO);
                }
            }
        }

        public void PROCESS_GET_OWN_CHANNEL_LIST_REQUEST_2021(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelList] != null)
            {
                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ChannelList];
                foreach (JObject jChannel in jarray)
                {
                    ChannelDTO channelDTO = MakeChannelDTO(jChannel);
                    ChannelHelpers.AddIntoMyChannelList(channelDTO);
                    if (UCAddToMyChannelView.Instance != null && UCAddToMyChannelView.Instance.IsVisible)
                    {
                        UCAddToMyChannelView.Instance.AddIntoMyChannelListPopUp(channelDTO);
                    }
                }
            }
        }

        public void PROCESS_ADD_CHANNEL_PROFILE_IMAGE_REQUEST_2024(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelDTO] != null)
            {
                ChannelDTO channelDTO = MakeChannelDTO((JObject)_JobjFromResponse[JsonKeys.ChannelDTO]);
            }
        }

        public void PROCESS_ADD_CHANNEL_COVER_IMAGE_REQUEST_2025(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelDTO] != null)
            {
                ChannelDTO channelDTO = MakeChannelDTO((JObject)_JobjFromResponse[JsonKeys.ChannelDTO]);
            }
        }

<<<<<<< HEAD
        public void PROCESS_ADD_MEDIA_TO_CHANNEL_REQUEST_2026(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelDTO] != null)
            {
                ChannelDTO channelDTO = MakeChannelDTO((JObject)_JobjFromResponse[JsonKeys.ChannelDTO]);
                List<ChannelMediaDTO> dtoList = new List<ChannelMediaDTO>();
                foreach (ChannelMediaDTO mediaDTO in channelDTO.MediaList)
                {
                    dtoList.Add(mediaDTO);
                }

                ChannelHelpers.AddMediaIntoMediaList(dtoList, ChannelConstants.MEDIA_STATUS_UPLOADED);
=======
        public void PROCESS_ADD_UPLOADED_CHANNEL_MEDIA_2026(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelMediaList] != null)
            {
                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ChannelMediaList];
                foreach (JObject jMedia in jarray)
                {
                    ChannelMediaDTO mediaDTO = MakeChannelMediaDTO(jMedia);
                    ChannelHelpers.AddIntoUploadedChannelMediaList(mediaDTO, ChannelViewModel.Instance.GetChannelUploadedMediaList(mediaDTO.MediaType), true);
                }
            }
        }

        public void PROCESS_GET_UPLOADED_CHANNEL_MEDIA_2027(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelMediaList] != null)
            {
                int mediaType = _JobjFromResponse[JsonKeys.MediaType] != null ? (int)_JobjFromResponse[JsonKeys.MediaType] : 0;
                if (mediaType > 0)
                {
                    ObservableCollection<ChannelMediaModel> tempList = ChannelViewModel.Instance.GetChannelUploadedMediaList(mediaType);
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ChannelMediaList];
                    foreach (JObject jMedia in jarray)
                    {
                        ChannelMediaDTO mediaDTO = MakeChannelMediaDTO(jMedia);
                        ChannelHelpers.AddIntoUploadedChannelMediaList(mediaDTO, tempList);
                    }
                }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
            }
        }

        public void PROCESS_GET_CHANNEL_MEDIA_LIST_REQUEST_2028(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelMediaList] != null)
            {
                int channelMediaStatus = _JobjFromResponse[JsonKeys.ChannelMediaStatus] != null ? (int)_JobjFromResponse[JsonKeys.ChannelMediaStatus] : 0;
                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ChannelMediaList];
                List<ChannelMediaDTO> mediaList = new List<ChannelMediaDTO>();
                foreach (JObject jMedia in jarray)
                {
                    ChannelMediaDTO mediaDTO = MakeChannelMediaDTO(jMedia);
                    mediaList.Add(mediaDTO);
                }
                ChannelHelpers.AddMediaIntoMediaList(mediaList, channelMediaStatus);
            }
        }

        public void PROCESS_UPDATE_CHANNEL_MEDIA_STATUS_2029(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                Guid channelId = _JobjFromResponse[JsonKeys.ChannelID] != null ? (Guid)_JobjFromResponse[JsonKeys.ChannelID] : Guid.Empty;
            }
        }

        public void PROCESS_GET_FEATURED_CHANNEL_LIST_REQUEST_2030(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelList] != null)
            {
                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ChannelList];
                foreach (JObject jChannel in jarray)
                {
                    ChannelDTO channelDTO = MakeChannelDTO(jChannel);
                    ChannelHelpers.AddIntoFeaturedChannelList(channelDTO);
                    HelperMethods.AddIntoFeaturedStreamAndChannelList(null, channelDTO);
                }
            }
        }

        public void PROCESS_UPDATE_CHANNEL_MEDIA_INFO_REQUEST_2031(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelMediaDTO] != null)
            {
                ChannelMediaDTO mediaDTO = MakeChannelMediaDTO(_JobjFromResponse);
            }
        }

        public void PROCESS_SEARCH_CHANNEL_LIST_REQUEST_2032(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.ChannelList] != null)
            {
                string searchParam = _JobjFromResponse[JsonKeys.Title] != null ? (string)_JobjFromResponse[JsonKeys.Title] : String.Empty;
                string country = _JobjFromResponse[JsonKeys.Country] != null ? (string)_JobjFromResponse[JsonKeys.Country] : String.Empty;
                int catId = 0;

                if (_JobjFromResponse[JsonKeys.ChannelCatetoryIDs] != null)
                {
                    JArray array = (JArray)_JobjFromResponse[JsonKeys.ChannelCatetoryIDs];
                    if (array.Count > 0)
                    {
                        catId = (int)array[0];
                    }
                }

                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ChannelList];
                if (catId > 0)
                {
                    foreach (JObject jChannel in jarray)
                    {
                        ChannelDTO channelDTO = MakeChannelDTO(jChannel);
                        ChannelHelpers.AddIntoSearchChannelList(channelDTO, ChannelViewModel.Instance.ChannelByCategoryList);
                    }
                }
                else if (!String.IsNullOrWhiteSpace(country))
                {
                    foreach (JObject jChannel in jarray)
                    {
                        ChannelDTO channelDTO = MakeChannelDTO(jChannel);
                        ChannelHelpers.AddIntoSearchChannelList(channelDTO, ChannelViewModel.Instance.ChannelByCountryList);
                    }
                }
                else if (!String.IsNullOrWhiteSpace(searchParam))
                {
                    foreach (JObject jChannel in jarray)
                    {
                        ChannelDTO channelDTO = MakeChannelDTO(jChannel);
                        ChannelHelpers.AddIntoSearchChannelList(channelDTO, ChannelViewModel.Instance.ChannelSearchList);
                    }
                }
            }
        }

        public void PROCESS_GET_CHANNEL_PLAYLIST_REQUEST_2033(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                Guid channelId = _JobjFromResponse[JsonKeys.ChannelID] != null ? (Guid)_JobjFromResponse[JsonKeys.ChannelID] : default(Guid);

                if (_JobjFromResponse[JsonKeys.ChannelMediaDTO] != null)
                {
                    ChannelMediaDTO mediaDTO = MakeChannelMediaDTO((JObject)_JobjFromResponse[JsonKeys.ChannelMediaDTO]);
                    mediaDTO.ChannelID = channelId;
                    ChannelHelpers.AddIntoChannelNowPlayingList(mediaDTO);
                }

                if (_JobjFromResponse[JsonKeys.ChannelMediaList] != null)
                {
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ChannelMediaList];
                    if (jarray.Count > 0)
                    {
                        ObservableCollection<ChannelMediaModel> viewerModelList = ChannelViewModel.Instance.GetChannelPlayList(channelId);
                        foreach (JObject obj in jarray)
                        {
                            ChannelMediaDTO mediaDTO = MakeChannelMediaDTO(obj);
                            ChannelHelpers.AddIntoChannelPlayList(mediaDTO, viewerModelList); // 2 = published
                        }
                    }
                }
            }
        }

        #endregion "Signal Handler methods"

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Auth.utility.Feed;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.Circle;
using View.UI.PopUp;
using View.Utility.Circle;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Feed;
using View.Utility.DataContainer;

namespace View.Utility.Auth
{
    public class CircleSignalHandler
    {
        #region "Private Fields"
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(CircleSignalHandler).Name);
        private List<CircleMemberDTO> dtosToDB = new List<CircleMemberDTO>();
        private List<CircleMemberDTO> dtosNotToDB = new List<CircleMemberDTO>();
        private int _Items_Loaded = 0;
        #endregion "Private Fields"

        #region "Private methods"
        #endregion "Private methods"

        #region "Signal Handler Methods"

        public void Process_NEW_CIRCLE_51(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.GroupId] != null && _JobjFromResponse[JsonKeys.GroupName] != null && _JobjFromResponse[JsonKeys.SuperAdmin] != null)
                {
                    CircleDTO circleDTO = HelperMethodsModel.BindCircleDetails(_JobjFromResponse);
                    CircleModel circleModel = null;
                    if (!CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleDTO.CircleId, out circleModel))
                    {
                        circleModel = new CircleModel();
                    }

                    circleModel.LoadData(circleDTO);
                    CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[circleModel.UserTableID] = circleModel;
                    CircleDataContainer.Instance.CircleIDs.Add(circleModel.UserTableID);
                    VMCircle.Instance.TotalCircles = VMCircle.Instance.TotalCircles + 1;

                    List<CircleDTO> list = new List<CircleDTO>();
                    list.Add(circleDTO);
                    new InsertIntoCircleListTable(list);
                    AddNewCircle(circleModel);
                }
                else
                {
                    ShowErrorMessageBoxCircle("Unable to Create Circle!", "Failed!");
                }
            }
            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void Process_CIRCLE_DETAILS_52(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {
                CircleDTO dto = HelperMethodsModel.BindCircleDetails(_JobjFromResponse);
                CircleModel model;
                if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(dto.CircleId, out model))
                {
                    dto.UpdateTime = model.UpdateTime;
                }

                List<CircleDTO> list = new List<CircleDTO>();
                list.Add(dto);
                new InsertIntoCircleListTable(list);
                FetchCircleDetails(dto);
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(delegate
                {
                    UIHelperMethods.ShowFailed("You are not a member of this circle!", "Not a member");
                    // CustomMessageBox.ShowError("You are not a member of this circle!");
                    MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
                });
            }
        }

        public void Process_LEAVE_CIRCLE_53(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.GroupId] != null)
                {
                    long circleId = (long)_JobjFromResponse[JsonKeys.GroupId];
                    new DeleteFromCircleListTable(circleId);
                    LeaveCircle(circleId);
                }
                else
                {
                    ShowErrorMessageBox("Unable to leave circle!", "Failed!");
                }

            }
            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        //int totalCircleCount = 0;

        public void Process_CIRCLE_LIST_70(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.GroupList] != null && _JobjFromResponse[JsonKeys.TotalRecords] != null)
                {
                    string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
                    int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
                    DefaultSettings.TOTAL_CIRCLES_COUNT = Convert.ToInt32((string)_JobjFromResponse[JsonKeys.TotalRecords]);
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.GroupList];
                    int cn = jArray.Count;

                    List<CircleDTO> circleDTOList = new List<CircleDTO>();
                    List<CircleModel> tempCircleList = new List<CircleModel>();

                    foreach (JObject circleObj in jArray)
                    {
                        CircleDTO circleDTO = HelperMethodsModel.BindCircleDetails(circleObj);
                        CircleModel circleModel;
                        if (!CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleDTO.CircleId, out circleModel))
                        {
                            circleModel = new CircleModel();
                        }
                        circleModel.LoadData(circleDTO);
                        tempCircleList.Add(circleModel);
                        circleDTOList.Add(circleDTO);

                        //totalCircleCount++;
                        //log.Debug("Total Circle Count => " + totalCircleCount);
                        CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[circleModel.UserTableID] = circleModel;
                        lock (CircleDataContainer.Instance.CircleIDs)
                        {
                            if (!CircleDataContainer.Instance.CircleIDs.Contains(circleDTO.CircleId))
                            {
                                CircleDataContainer.Instance.CircleIDs.Add(circleDTO.CircleId);
                                VMCircle.Instance.TotalCircles = VMCircle.Instance.TotalCircles + 1;
                            }
                        }
                    }

                    AddIntoCircleList(tempCircleList);
                    new InsertIntoCircleListTable(circleDTOList);
                }
            }
            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void Process_REMOVE_CIRCLE_MEMBER_154(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.RemovedMembers] != null)
                {
                    JArray jArray = ((JArray)_JobjFromResponse[JsonKeys.RemovedMembers]);
                    long circleId = (long)_JobjFromResponse[JsonKeys.GroupId];
                    int mc = (int)_JobjFromResponse[JsonKeys.MemberOrMediaCount];
                    CircleModel circleModel;
                    if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out circleModel))
                    {
                        mc = circleModel.MemberCount;
                        CircleDTO circleDTO = circleModel.GetCircleDTO(circleModel);
                        List<CircleDTO> circleList = new List<CircleDTO>();
                        circleList.Add(circleDTO);
                        new InsertIntoCircleListTable(circleList);
                    }

                    List<long> list = new List<long>();
                    list = jArray.ToObject<List<long>>();
                    DeleteCircleMembers(circleId, list, mc);
                }
                else
                {
                    //HelperMethodsAuth.AuthHandlerInstance.ShowErrorMessageBox("Unable to leave Circle!", "Failed!");
                }
            }

            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void Process_SEARCH_CIRCLE_MEMBER_101(JObject _JobjFromResponse)
        {
            try
            {
                string searchParam = string.Empty;
                long circleId = 0;

                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.GroupMembers] != null && _JobjFromResponse[JsonKeys.SearchParam] != null)
                {
                    searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];

                    List<CircleMemberDTO> listSearch = new List<CircleMemberDTO>();
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.GroupMembers];

                    CircleMemberDTO circleMember = null;

                    foreach (JObject jObj in jArray)
                    {
                        //{"uId":"2110010445","admin":true,"fn":"Superbeam","grpId":133,"ut":1440934648410,"ists":0,"utId":405,"prIm":"2110010445/1440052938133.jpg","id":954}
                        circleMember = HelperMethodsModel.BindCircleMemberDetails(jObj);
                        circleId = circleMember.CircleId;
                        if (StatusConstants.STATUS_DELETED != circleMember.IntegerStatus && !listSearch.Any(x => x.UserTableID == circleMember.UserTableID))
                        {
                            listSearch.Add(circleMember);
                        }
                    }
                    LoadSearchedCircleMembers(listSearch, searchParam);
                }
                else
                {
                    NoMoreSearchResult();
                }
            }
            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void Process_CIRCLE_MEMBERS_LIST_99(JObject _JobjFromResponse) //99
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.GroupMembers] != null)
                {
                    List<CircleMemberDTO> listToIns = new List<CircleMemberDTO>();
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.GroupMembers];
                    int mc = 0;
                    CircleMemberDTO circleMember = null;
                    foreach (JObject circleMemberObj in jArray)
                    {
                        // {  "uId": "2110066892", "admin": false, "fn": "Mukul Hossen", "grpId": 969, "ut": 1455163012675, "ists": 0, "utId": 52556, "prIm": "", "id": 6558 }
                        circleMember = HelperMethodsModel.BindCircleMemberDetails(circleMemberObj);
                        if (StatusConstants.STATUS_DELETED != circleMember.IntegerStatus && !listToIns.Any(x => x.UserTableID == circleMember.UserTableID))
                        {
                            listToIns.Add(circleMember);
                        }
                        //lock (RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY)
                        //{
                        CircleModel circleModel;
                        if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleMember.CircleId, out circleModel) && DefaultSettings.LOGIN_TABLE_ID == circleMember.UserTableID)
                        {
                            circleModel.MembershipStatus = DefaultSettings.LOGIN_TABLE_ID == circleModel.SuperAdmin ? StatusConstants.CIRCLE_SUPER_ADMIN : circleMember.IsAdmin ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;
                            mc = circleModel.MemberCount;
                        }
                        //}
                    }
                    if (listToIns.Count > 0)
                    {
                        LoadCircleMembersList(circleMember.CircleId, listToIns, mc);
                    }
                }
                else
                {
                    HideCircleMemberShowMore();
                }
            }
            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void Process_DELETE_CIRCLE_152(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.GroupId] != null)
                {
                    long circleId = (long)_JobjFromResponse[JsonKeys.GroupId];
                    new DeleteFromCircleListTable(circleId);
                    DeleteCircle(circleId);
                }
                else
                {
                    ShowErrorMessageBoxCircle("Unable to delete circle!", "Failed!");
                }
            }
            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void Process_CIRCLE_NEWSFEED_198(JObject _JobjFromResponse, string client_packet_id) // 198
        {
            try
            {
                int scl = (int)_JobjFromResponse[JsonKeys.Scroll];
                long circleId = (_JobjFromResponse[JsonKeys.WallOwnerId] != null) ? (long)_JobjFromResponse[JsonKeys.WallOwnerId] : 0;
                Guid npUUid = (_JobjFromResponse[JsonKeys.NpUUID] != null) ? (Guid)_JobjFromResponse[JsonKeys.NpUUID] : Guid.Empty;
                if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                {
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                    //long circleId = 0;
                    //JObject firstObj = (JObject)jarray.ElementAt(0);
                    //if (firstObj[JsonKeys.WallOwner] != null)
                    //{
                    //    JObject jObj = ((JObject)firstObj[JsonKeys.WallOwner]);
                    //    circleId = (long)jObj[JsonKeys.UserTableID];
                    //}
                    if (circleId > 0)
                    {
                        int feed_state = client_packet_id.Equals(DefaultSettings.PROFILECIRCLE_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);
                            //if (RingIDViewModel.Instance.CircleProfileFeeds.UserProfileUtId == circleId)
                            //{
                            FeedDataContainer.Instance.CircleProfileFeedsSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                            if (scl != 1)
                            {
                                FeedDataContainer.Instance.CircleProfileFeedsSortedIds.PvtMinGuid = npUUid;
                            }
                            else
                            {
                                FeedDataContainer.Instance.CircleProfileFeedsSortedIds.PvtMaxGuid = npUUid;
                            }
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME)
                            {
                                //if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null
                                //    && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.IsVisible)
                                //    FirstTimeFeedLoadWorker.Instance.LoadFirstTimeFeed(RingIDViewModel.Instance.CircleProfileFeeds, model);
                                //else if (!RingIDViewModel.Instance.CircleProfileFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                                //RingIDViewModel.Instance.CircleProfileFeeds.InsertModel(model);
                            }
                            //}
                        }
                    }
                    else
                    {
                        int feed_state = client_packet_id.Equals(DefaultSettings.ALLCIRCLE_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);
                            FeedDataContainer.Instance.AllCircleFeedsSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                            if (scl != 1)
                            {
                                FeedDataContainer.Instance.AllCircleFeedsSortedIds.PvtMinGuid = npUUid;
                            }
                            else
                            {
                                FeedDataContainer.Instance.AllCircleFeedsSortedIds.PvtMaxGuid = npUUid;
                            }
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME)
                            {
                                //if (UCMiddlePanelSwitcher.View_UCCircleInitPanel != null
                                //    && UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleAllFeeds != null
                                //    && UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleAllFeeds.IsVisible)
                                //    FirstTimeFeedLoadWorker.Instance.LoadFirstTimeFeed(RingIDViewModel.Instance.AllCircleFeeds, model);
                                //else if (!RingIDViewModel.Instance.AllCircleFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                                //RingIDViewModel.Instance.AllCircleFeeds.InsertModel(model);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        #region CircleUpadte

        //public void Process_UPDATE_DELETE_CIRCLET_352(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.GroupId] != null)
        //        {
        //            long circleId = (long)_JobjFromResponse[JsonKeys.GroupId];
        //            new DeleteFromCircleListTable(circleId);
        //            DeleteCircle(circleId);
        //        }
        //        else
        //        {

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
        //    }
        //}

        //public void Process_UPDATE_REMOVE_CIRCLE_MEMBER_354(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.GroupId] != null && _JobjFromResponse[JsonKeys.RemovedMembers] != null && _JobjFromResponse[JsonKeys.MemberCount] != null)
        //        {
        //            List<CircleMemberDTO> deletedDTOS = new List<CircleMemberDTO>();
        //            List<long> deletedIds = new List<long>();
        //            long circleId = (long)_JobjFromResponse[JsonKeys.GroupId];
        //            int mc = (int)_JobjFromResponse[JsonKeys.MemberCount];
        //            CircleModel circleModel;
        //            if (VMCircle.Instance.CIRCLE_LIST_DICTIONARY.TryGetValue(circleId, out circleModel))
        //            {
        //                circleModel.MemberCount = mc;
        //                CircleDTO circleDTO = circleModel.GetCircleDTO(circleModel);
        //                List<CircleDTO> lst = new List<CircleDTO>();
        //                lst.Add(circleDTO);
        //                new InsertIntoCircleListTable(lst);
        //            }

        //            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.RemovedMembers];
        //            for (int i = 0; i < jarray.Count; i++)
        //            {
        //                long id = (long)jarray.ElementAt(i);
        //                deletedIds.Add(id);
        //            }
        //            if (deletedDTOS.Count > 0) new InsertIntoCircleMemberTable(deletedDTOS);
        //            DeleteCircleMembers(circleId, deletedIds, mc);
        //        }
        //        else
        //        {

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
        //    }
        //}

        //public void Process_UPDATE_ADD_CIRCLE_MEMBER_356(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.GroupId] != null && _JobjFromResponse[JsonKeys.GroupMembers] != null && _JobjFromResponse[JsonKeys.MemberCount] != null)
        //        {
        //            List<CircleMemberDTO> addedDtos = new List<CircleMemberDTO>();
        //            long circleId = (long)_JobjFromResponse[JsonKeys.GroupId];
        //            int mc = (int)_JobjFromResponse[JsonKeys.MemberCount];
        //            CircleModel circleModel;
        //            if (VMCircle.Instance.CIRCLE_LIST_DICTIONARY.TryGetValue(circleId, out circleModel))
        //            {
        //                circleModel.MemberCount = mc;
        //                CircleDTO circleDTO = circleModel.GetCircleDTO(circleModel);
        //                List<CircleDTO> lst = new List<CircleDTO>();
        //                lst.Add(circleDTO);
        //                new InsertIntoCircleListTable(lst);
        //            }

        //            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.GroupMembers];
        //            foreach (JObject circleMemberObj in jarray)
        //            {
        //                CircleMemberDTO circleMember = HelperMethodsModel.BindCircleMemberDetails(circleMemberObj);
        //                addedDtos.Add(circleMember);
        //            }
        //            UpdateAddCircleMembers(circleId, addedDtos, mc);
        //        }
        //        else
        //        {

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
        //    }
        //}

        //public void Process_UPDATE_EDIT_CIRCLE_MEMBER_358(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.GroupId] != null && _JobjFromResponse[JsonKeys.GroupMembers] != null)
        //        {
        //            List<CircleMemberDTO> editedDtos = new List<CircleMemberDTO>();
        //            long circleId = (long)_JobjFromResponse[JsonKeys.GroupId];
        //            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.GroupMembers];
        //            foreach (JObject circleMemberObj in jarray)
        //            {
        //                CircleMemberDTO circleMember = HelperMethodsModel.BindCircleMemberDetails(circleMemberObj);
        //                editedDtos.Add(circleMember);

        //                CircleModel circleModel;
        //                //lock (RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY)
        //                //{
        //                if (VMCircle.Instance.CIRCLE_LIST_DICTIONARY.TryGetValue(circleId, out circleModel) && DefaultSettings.LOGIN_USER_ID == circleMember.UserIdentity)
        //                {
        //                    circleModel.MembershipStatus = DefaultSettings.LOGIN_USER_ID == circleModel.SuperAdmin ? StatusConstants.CIRCLE_SUPER_ADMIN : circleMember.IsAdmin ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;
        //                }
        //                //}
        //            }
        //            EditCircleMembers(circleId, editedDtos);
        //        }
        //        else
        //        {

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
        //    }
        //}

        #endregion CircleUpdate

        #endregion "Signal Handler  methods"

        #region "Utility Methods"

        public void AddIntoCircleList(List<CircleModel> circleList)
        {
            foreach (CircleModel circleModel in circleList)
            {
                if (UCMiddlePanelSwitcher.View_UCCircleInitPanel != null && UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleList != null)
                {
                    if (circleModel.SuperAdmin == DefaultSettings.LOGIN_RING_ID)
                    {
                        lock (VMCircle.Instance.CircleListYouManage)
                        {
                            if (!VMCircle.Instance.CircleListYouManage.Any(P => P.UserTableID == circleModel.UserTableID))
                            {
                                Application.Current.Dispatcher.BeginInvoke(delegate
                                {
                                    VMCircle.Instance.CircleListYouManage.Add(circleModel);
                                });
                            }
                        }
                    }
                    else
                    {
                        if ((VMCircle.Instance.CircleListYouManage.Count + _Items_Loaded) < VMCircle.Instance._ItemsNeedToLoad)
                        {
                            lock (VMCircle.Instance.CircleListYouAreIn)
                            {
                                if (!VMCircle.Instance.CircleListYouAreIn.Any(P => P.UserTableID == circleModel.UserTableID))
                                {
                                    _Items_Loaded++; // because Dispacher adds item a few moments later.
                                    Application.Current.Dispatcher.BeginInvoke(delegate
                                    {
                                        VMCircle.Instance.CircleListYouAreIn.Add(circleModel);
                                    });
                                }
                            }
                        }
                    }
                }

                if (MainSwitcher.PopupController.circleViewWrapper != null && MainSwitcher.PopupController.circleViewWrapper.Visibility == Visibility.Visible)
                {
                    if (circleModel.SuperAdmin == DefaultSettings.LOGIN_RING_ID)
                    {
                        if (!MainSwitcher.PopupController.circleViewWrapper.CircleListYouManage.Any(P => P.UserTableID == circleModel.UserTableID))
                        {
                            MainSwitcher.PopupController.circleViewWrapper.CircleListYouManage.InvokeAdd(circleModel);
                        }
                    }
                    else
                    {
                        if (!MainSwitcher.PopupController.circleViewWrapper.CircleListYouAreIn.Any(P => P.UserTableID == circleModel.UserTableID))
                        {
                            MainSwitcher.PopupController.circleViewWrapper.CircleListYouAreIn.InvokeAdd(circleModel);
                        }
                    }

                    MainSwitcher.PopupController.circleViewWrapper.TotalCircles = VMCircle.Instance.TotalCircles;
                }

                System.Threading.Thread.Sleep(10);
            }
        }

        public void ShowErrorMessageBoxCircle(string msg, string caption)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                // MessageBox.Show(MainSwitcher.pageSwitcher, msg, caption, MessageBoxButton.OK, MessageBoxImage.Error);
                try
                {
                    UIHelperMethods.ShowFailed(msg, caption);
                    // CustomMessageBox.ShowError(msg, caption);
                }
                catch (Exception e)
                {
                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            });
        }

        public void AddNewCircle(CircleModel newCircleModel) // Someone add me in a circle
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (VMCircle.Instance.CircleListYouAreIn.Where(P => P.UserTableID == newCircleModel.UserTableID).FirstOrDefault() == null)
                    {
                        VMCircle.Instance.CircleListYouAreIn.Add(newCircleModel);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: AddNewCircle() ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            });
        }

        public void LoadCircleMembersList(long circleId, List<CircleMemberDTO> listToInsert, int memberCount)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null)
                    {
                        List<UserBasicInfoDTO> unknownUsers = new List<UserBasicInfoDTO>();
                        foreach (CircleMemberDTO displayDTO in listToInsert)
                        {
                            if (!VMCircle.Instance.MembersList.Any(x => x.ShortInfoModel.UserTableID == displayDTO.UserTableID))
                            {
                                UserBasicInfoModel userModel = null;
                                if (displayDTO.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                                {
                                    VMCircle.Instance.AddButtonVisible = (displayDTO.IsAdmin) ? Visibility.Visible : Visibility.Collapsed;
                                    RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FriendShipStatus = -1;
                                    userModel = RingIDViewModel.Instance.MyBasicInfoModel;
                                    userModel.CircleId = displayDTO.CircleId;
                                    userModel.IsAdmin = displayDTO.IsAdmin;
                                    userModel.PivotId = displayDTO.PivotId;
                                    userModel.CircleMembershipStatus = userModel.ShortInfoModel.UserIdentity == VMCircle.Instance.CircleModel.SuperAdmin ? StatusConstants.CIRCLE_SUPER_ADMIN : userModel.IsAdmin == true ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;
                                }
                                else
                                {
                                    userModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(displayDTO.UserTableID, displayDTO.RingID, displayDTO.FullName, displayDTO.ProfileImage);
                                    userModel.LoadDataFromCircleMember(displayDTO);
                                    userModel.IsAdmin = displayDTO.IsAdmin;
                                    userModel.PivotId = displayDTO.PivotId;
                                    userModel.CircleMembershipStatus = userModel.ShortInfoModel.UserIdentity == VMCircle.Instance.CircleModel.SuperAdmin ? StatusConstants.CIRCLE_SUPER_ADMIN : userModel.IsAdmin == true ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;
                                }
                                VMCircle.Instance.MembersList.Add(userModel);
                                VMCircle.Instance.HideShowMore();
                            }
                            else
                            {
                                if (displayDTO.UserTableID == DefaultSettings.LOGIN_TABLE_ID && VMCircle.Instance.MembersList.Where(x => x.ShortInfoModel.UserIdentity == displayDTO.RingID).First().IsAdmin != displayDTO.IsAdmin)
                                {
                                    UserBasicInfoModel selfModel = VMCircle.Instance.MembersList.Where(x => x.ShortInfoModel.UserIdentity == displayDTO.RingID).First();
                                    selfModel.IsAdmin = displayDTO.IsAdmin;
                                    selfModel.CircleMembershipStatus = displayDTO.RingID == VMCircle.Instance.CircleModel.SuperAdmin ? StatusConstants.CIRCLE_SUPER_ADMIN : displayDTO.IsAdmin == true ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;
                                    selfModel.PivotId = displayDTO.PivotId;
                                    selfModel.OnPropertyChanged("CurrentInstance");
                                    VMCircle.Instance.AddButtonVisible = (displayDTO.IsAdmin) ? Visibility.Visible : Visibility.Collapsed;
                                }
                                else
                                {
                                    UserBasicInfoModel model = VMCircle.Instance.MembersList.Where(x => x.ShortInfoModel.UserIdentity == displayDTO.RingID).First();
                                    if (model != null)
                                    {
                                        model.PivotId = displayDTO.PivotId;
                                        model.OnPropertyChanged("CurrentInstance");
                                    }
                                }
                            }
                        }

                        if (unknownUsers.Count > 0) new InsertIntoUserBasicInfoTable(unknownUsers).Start();

                        //set pivot id
                        if (VMCircle.Instance.ScrollMode == StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL)
                        {
                            VMCircle.Instance.PivotID = listToInsert.Count > 0 ? listToInsert.Max(x => x.PivotId) : 0;
                        }
                        else if (VMCircle.Instance.ScrollMode == StatusConstants.CIRCLE_MEMBER_ADMIN_SCROLL)
                        {
                            VMCircle.Instance.AdminPivotID = listToInsert.Count > 0 ? listToInsert.Max(x => x.PivotId) : 0;
                        }

                        //Show more
                        if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.TabList.SelectedIndex == 0)
                        {
                            if (VMCircle.Instance.CircleModel.MemberCount == VMCircle.Instance.MembersList.Count)
                            {
                                VMCircle.Instance.HideShowMore();
                            }
                            else
                            {
                                VMCircle.Instance.LoadShowMore();
                            }
                        }
                        else
                        {
                            if (VMCircle.Instance.MembersList.Where(x => x.CircleMembershipStatus != StatusConstants.CIRCLE_MEMBER).ToList().Count == VMCircle.Instance.AdminCount)
                            {
                                VMCircle.Instance.HideShowMore();
                            }
                            else
                            {
                                VMCircle.Instance.LoadShowMore();
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(VMCircle.Instance.SearchString))
                        {
                            VMCircle.Instance.SearchFriends();
                        }
                    }

                }
                catch (Exception ex)
                {
                    log.Error("Error: LoadCircleMembersList() ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            });
        }

        public void DeleteCircle(long circleId)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    CircleModel circleModel = null;
                    if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out circleModel))
                    {
                        VMCircle.Instance.CircleListYouManage.InvokeRemove(circleModel);
                        VMCircle.Instance.CircleListYouAreIn.InvokeRemove(circleModel);
                        CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryRemove(circleId);
                        CircleDataContainer.Instance.CircleIDs.Remove(circleId);
                        VMCircle.Instance.TotalCircles = VMCircle.Instance.TotalCircles - 1;
                        RingIDViewModel.Instance.OnCircleListClicked(new Object());

                        if (UCMiddlePanelSwitcher.View_UCCircleInitPanel != null)
                        {
                            UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleAllFeeds = null;
                        }

                        //RingIDViewModel.Instance.AllCircleFeeds.RemoveAllModels();
                        FeedDataContainer.Instance.AllCircleFeedsSortedIds.Clear();
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message + ex.StackTrace);
                }
            });
        }

        public void LeaveCircle(long circleId) //leave from circle which is not created by you 
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                CircleModel circleModel;
                if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out circleModel))
                {
                    VMCircle.Instance.CircleListYouAreIn.InvokeRemove(circleModel);
                    CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryRemove(circleId);
                    CircleDataContainer.Instance.CircleIDs.Remove(circleId);
                    VMCircle.Instance.TotalCircles = VMCircle.Instance.TotalCircles - 1;
                    RingIDViewModel.Instance.OnCircleListClicked(new Object());

                    if (UCMiddlePanelSwitcher.View_UCCircleInitPanel != null)
                    {
                        UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleAllFeeds = null;
                    }

                    //RingIDViewModel.Instance.AllCircleFeeds.RemoveAllModels();
                    FeedDataContainer.Instance.AllCircleFeedsSortedIds.Clear();
                }
            });
        }

        public void CreateNewCircle(CircleModel circleModel, string msg)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    if (msg == null && circleModel != null)
                    {
                        if (UCMiddlePanelSwitcher.View_UCCircleInitPanel != null && UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleList != null)
                        {
                            VMCircle.Instance.CircleListYouManage.InvokeAdd(circleModel);
                        }
                        CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[circleModel.UserTableID] = circleModel;
                        CircleDataContainer.Instance.CircleIDs.Add(circleModel.UserTableID);
                        VMCircle.Instance.TotalCircles = VMCircle.Instance.TotalCircles + 1;

                        foreach (UserBasicInfoModel userBasicModel in VMCircle.Instance.DragListForNewCircle)
                        {
                            CircleMemberDTO circleMember = userBasicModel.GetCircleMemberDTOFromModel(circleModel.UserTableID, circleModel.UpdateTime);
                            userBasicModel.IsAdmin = false;
                            userBasicModel.CircleMembershipStatus = StatusConstants.CIRCLE_MEMBER;
                        }
                        CircleMemberDTO myselfasMember = new CircleMemberDTO
                        {
                            CircleId = circleModel.UserTableID,
                            UpdateTime = circleModel.UpdateTime,
                            IsAdmin = true,
                            FullName = DefaultSettings.VALUE_LOGIN_USER_NAME,
                            RingID = DefaultSettings.LOGIN_RING_ID,
                            UserTableID = DefaultSettings.LOGIN_TABLE_ID
                        };

                        if (UCMiddlePanelSwitcher.View_UCCreateCirclePanel != null)
                        {
                            UCMiddlePanelSwitcher.View_UCCreateCirclePanel.EnableButtons(true);
                            VMCircle.Instance.OnCirclePanelClicked(circleModel.UserTableID);
                        }
                    }
                    else
                    {
                        UIHelperMethods.ShowFailed(msg, "Create circle");
                        // CustomMessageBox.ShowError(msg, "Error!");
                        if (UCMiddlePanelSwitcher.View_UCCreateCirclePanel != null)
                        {
                            UCMiddlePanelSwitcher.View_UCCreateCirclePanel.EnableButtons(false);
                        }
                    }
                    CircleUtility.Instance.RequestCircleMemberList(circleModel.UserTableID);
                }
                catch (Exception)
                {
                }
            });
        }

        public void DeleteCircleMembers(long circleId, List<long> deletedIds, int mc)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    CircleModel model;
                    if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out model))
                    {
                        CircleModel existingModel = VMCircle.Instance.CircleListYouAreIn.Where(P => P.UserTableID == circleId).FirstOrDefault();
                        if (existingModel == null)
                        {
                            existingModel = VMCircle.Instance.CircleListYouManage.Where(P => P.UserTableID == circleId).FirstOrDefault();
                        }
                        if (existingModel != null)
                        {
                            existingModel.MemberCount = mc;
                        }

                        model.MemberCount = mc;
                    }

                    if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null && VMCircle.Instance.MembersList != null)
                    {
                        foreach (long id in deletedIds)
                        {
                            UserBasicInfoModel memberModel = VMCircle.Instance.MembersList.Where(P => P.ShortInfoModel.UserTableID == id).FirstOrDefault();
                            if (memberModel != null)
                            {
                                if (memberModel.CircleMembershipStatus == StatusConstants.CIRCLE_MEMBER)
                                {
                                    VMCircle.Instance.UpdateMemberOnlyCount(-1);
                                }
                                else
                                {
                                    VMCircle.Instance.UpdateAdminCount(-1);
                                }
                                VMCircle.Instance.MembersList.Remove(memberModel);
                                VMCircle.Instance.UpdateTotalMembersCount(-1);
                            }

                        }
                    }

                    CircleModel circleModel = null;
                    if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out circleModel))
                    {
                        circleModel.MemberCount = mc;
                        CircleDTO circleDTO = circleModel.GetCircleDTO(circleModel);
                        List<CircleDTO> list = new List<CircleDTO>();
                        list.Add(circleDTO);
                        new InsertIntoCircleListTable(list);
                    }
                }
                catch (Exception)
                {
                }
            });
        }

        public void UpdateAddCircleMembers(long circleId, List<CircleMemberDTO> addedDtos, int mc)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    CircleModel model;
                    if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out model))
                    {
                        CircleModel existingModel = VMCircle.Instance.CircleListYouAreIn.Where(P => P.UserTableID == circleId).FirstOrDefault();
                        if (existingModel == null)
                        {
                            existingModel = VMCircle.Instance.CircleListYouManage.Where(P => P.UserTableID == circleId).FirstOrDefault();
                        }
                        if (existingModel != null)
                        {
                            existingModel.MemberCount = mc;
                        }

                        model.MemberCount = mc;
                    }

                    List<UserBasicInfoDTO> unknownUsers = new List<UserBasicInfoDTO>();
                    if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null && VMCircle.Instance.MembersList != null)
                    {
                        foreach (CircleMemberDTO dto in addedDtos)
                        {
                            UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(dto.UserTableID, dto.RingID, dto.FullName, dto.ProfileImage);
                            userModel.LoadDataFromCircleMember(dto);
                            userModel.CircleMembershipStatus = VMCircle.Instance.CircleModel.SuperAdmin == userModel.ShortInfoModel.UserIdentity ? StatusConstants.CIRCLE_SUPER_ADMIN : userModel.IsAdmin == true ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;
                            VMCircle.Instance.MembersList.Add(userModel);
                        }
                    }

                    CircleModel circleModel = null;
                    if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out circleModel))
                    {
                        circleModel.MemberCount = mc;
                        CircleDTO circleDTO = circleModel.GetCircleDTO(circleModel);
                        List<CircleDTO> list = new List<CircleDTO>();
                        list.Add(circleDTO);
                        new InsertIntoCircleListTable(list);
                    }

                    if (unknownUsers.Count > 0) new InsertIntoUserBasicInfoTable(unknownUsers).Start();
                }
                catch (Exception)
                {
                }
            });
        }

        public void AddCircleMembers(long circleId, List<CircleMemberDTO> listToInsert, int mc)
        {
            Application.Current.Dispatcher.Invoke(delegate
            {
                try
                {
                    CircleModel model;
                    if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out model))
                    {
                        CircleModel existingModel = VMCircle.Instance.CircleListYouAreIn.Where(P => P.UserTableID == circleId).FirstOrDefault();
                        if (existingModel == null)
                        {
                            existingModel = VMCircle.Instance.CircleListYouManage.Where(P => P.UserTableID == circleId).FirstOrDefault();
                        }
                        if (existingModel != null)
                        {
                            existingModel.MemberCount = mc;
                        }

                        model.MemberCount = mc;
                    }
                    //UCCirclePanel ucCirclePanel = null;
                    //if (UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY.TryGetValue(circleId, out ucCirclePanel) && VMCircle.Instance.MembersList != null && ucCirclePanel._UCCircleMembersPanel != null)
                    if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null
                        && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel._ucCircleAddMembersPanel != null && VMCircle.Instance.MembersList != null)
                    {
                        if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel._ucCircleAddMembersPanel.DragListForCircle.Count > 0)
                        {
                            foreach (UserBasicInfoModel usermodel in UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel._ucCircleAddMembersPanel.DragListForCircle)
                            {
                                usermodel.CircleId = circleId;
                                usermodel.CircleMembershipStatus = VMCircle.Instance.CircleModel.SuperAdmin == usermodel.ShortInfoModel.UserIdentity ? StatusConstants.CIRCLE_SUPER_ADMIN : usermodel.IsAdmin == true ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;
                                if (usermodel.CircleMembershipStatus == StatusConstants.CIRCLE_MEMBER)
                                {
                                    VMCircle.Instance.UpdateMemberOnlyCount(1);
                                    //ucCirclePanel._UCCircleMembersPanel.SetMemberLabel(ucCirclePanel._UCCircleMembersPanel.membersCountWOAdmin);
                                }
                                else
                                {
                                    VMCircle.Instance.UpdateAdminCount(1);
                                    // ucCirclePanel._UCCircleMembersPanel.SetAdminLabel(ucCirclePanel._UCCircleMembersPanel.adminCount);
                                }

                                VMCircle.Instance.MembersList.Add(usermodel);
                                VMCircle.Instance.UpdateTotalMembersCount(1);

                            }
                        }
                        UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.ShowAddedMembers();
                        //ucCirclePanel._UCCircleMembersPanel.TOTAL_MEMBER_COUNT = mc;
                    }

                    CircleModel circleModel = null;
                    if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out circleModel))
                    {
                        CircleDTO dto = circleModel.GetCircleDTO(circleModel);
                        dto.MemberCount = mc;
                        List<CircleDTO> lst = new List<CircleDTO>();
                        lst.Add(dto);
                        new InsertIntoCircleListTable(lst);
                    }
                    //CustomMessageBox.ShowInformation(NotificationMessages.CIRCLE_MEMBER_ADDED_SUCCESS);
                    UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.CIRCLE_MEMBER_ADDED_SUCCESS);
                }
                catch (Exception)
                {

                }
            });
        }

        public void EnableButtonsOnAddCircleMembersFailure(long circleId, string msg)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    ShowErrorMessageBox(msg, "Failed!");
                    //UCCirclePanel ucCirclePanel = null;
                    if (VMCircle.Instance.MembersList != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null)
                    {
                        UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.ShowAddedMembers();
                    }
                }
                catch (Exception)
                {

                }
            });
        }

        public void EditCircleMembers(long circleId, List<CircleMemberDTO> editedDtos)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    //UCCirclePanel ucCirclePanel = null;
                    //if (UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY.TryGetValue(circleId, out ucCirclePanel) && VMCircle.Instance.MembersList != null && ucCirclePanel._UCCircleMembersPanel != null)
                    if (VMCircle.Instance.MembersList != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null)
                    {
                        foreach (CircleMemberDTO dto in editedDtos)
                        {
                            if (dto.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                            {
                                VMCircle.Instance.AddButtonVisible = (dto.IsAdmin) ? Visibility.Visible : Visibility.Collapsed;
                                UserBasicInfoModel memberModel = VMCircle.Instance.MembersList.Where(x => x.ShortInfoModel.UserTableID == dto.UserTableID).FirstOrDefault();
                                VMCircle.Instance.MembersList.Remove(memberModel);
                                memberModel.IsAdmin = dto.IsAdmin;
                                memberModel.CircleMembershipStatus = VMCircle.Instance.CircleModel.SuperAdmin == memberModel.ShortInfoModel.UserIdentity ? StatusConstants.CIRCLE_SUPER_ADMIN : memberModel.IsAdmin == true ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;

                                if (memberModel.CircleMembershipStatus == StatusConstants.CIRCLE_MEMBER)
                                {
                                    VMCircle.Instance.UpdateMemberOnlyCount(1);
                                    //ucCirclePanel._UCCircleMembersPanel.SetMemberLabel(ucCirclePanel._UCCircleMembersPanel.membersCountWOAdmin);
                                }
                                else
                                {
                                    VMCircle.Instance.UpdateAdminCount(1);
                                    //ucCirclePanel._UCCircleMembersPanel.SetAdminLabel(ucCirclePanel._UCCircleMembersPanel.adminCount);
                                }

                                VMCircle.Instance.MembersList.Add(memberModel);
                            }
                            else
                            {
                                UserBasicInfoModel memberModel = VMCircle.Instance.MembersList.Where(P => P.ShortInfoModel.UserTableID == dto.UserTableID).FirstOrDefault();
                                VMCircle.Instance.MembersList.Remove(memberModel);
                                memberModel.CircleId = circleId;
                                memberModel.IsAdmin = dto.IsAdmin;
                                memberModel.CircleMembershipStatus = VMCircle.Instance.CircleModel.SuperAdmin == memberModel.ShortInfoModel.UserIdentity ? StatusConstants.CIRCLE_SUPER_ADMIN : memberModel.IsAdmin == true ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;

                                if (memberModel.CircleMembershipStatus == StatusConstants.CIRCLE_MEMBER)
                                {
                                    VMCircle.Instance.UpdateMemberOnlyCount(1);
                                    //ucCirclePanel._UCCircleMembersPanel.SetMemberLabel(ucCirclePanel._UCCircleMembersPanel.membersCountWOAdmin);

                                    VMCircle.Instance.UpdateAdminCount(-1);
                                    //ucCirclePanel._UCCircleMembersPanel.SetAdminLabel(ucCirclePanel._UCCircleMembersPanel.adminCount);
                                }
                                else
                                {
                                    VMCircle.Instance.UpdateMemberOnlyCount(-1);
                                    //ucCirclePanel._UCCircleMembersPanel.SetMemberLabel(ucCirclePanel._UCCircleMembersPanel.membersCountWOAdmin);

                                    VMCircle.Instance.UpdateAdminCount(1);
                                    //ucCirclePanel._UCCircleMembersPanel.SetAdminLabel(ucCirclePanel._UCCircleMembersPanel.adminCount);
                                }

                                VMCircle.Instance.MembersList.Add(memberModel);
                            }
                        }
                        foreach (var item in VMCircle.Instance.MembersList)
                        {
                            item.OnPropertyChanged("CurrentInstance");
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: EditCircleMembers() ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            });
        }

        public void ShowErrorMessageBox(string msg, string caption)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                UIHelperMethods.ShowFailed(msg, caption);
                // CustomMessageBox.ShowError(msg, caption);
            });
        }

        //deprecated
        public void RemoveCircleMemberFromUI(CircleMemberDTO circleMember)
        {
            //UCCirclePanel _ucCirclePanel = null;
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    //lock (UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY)
                    //{
                    //if (UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY.TryGetValue(circleMember.CircleId, out _ucCirclePanel) && _ucCirclePanel._UCCircleMembersPanel != null && _ucCirclePanel._UCCircleMembersPanel.MembersList != null)
                    if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null && VMCircle.Instance.MembersList != null)
                    {
                        UserBasicInfoModel model = VMCircle.Instance.MembersList.Where(x => x.ShortInfoModel.UserIdentity == circleMember.RingID).First();
                        if (model != null)
                        {
                            //_ucCirclePanel._UCCircleMembersPanel.MembersList.Remove(model);
                            VMCircle.Instance.MembersList.Remove(model);
                        }
                    }
                    //}
                }
                catch (Exception)
                {

                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public void HideCircleMemberShowMore()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null)
                {

                    if (VMCircle.Instance.ScrollMode == StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL)
                    {
                        //UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.TOTAL_MEMBER_COUNT = VMCircle.Instance.MembersList.Count;
                        //UCMiddlePanelSwitcher.View_UCCirclePanel.circleModel.MemberCount = UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.TOTAL_MEMBER_COUNT;
                        //RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY[UCMiddlePanelSwitcher.View_UCCirclePanel.circleModel.CircleId].MemberCount = UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.TOTAL_MEMBER_COUNT;
                        //RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY[UCMiddlePanelSwitcher.View_UCCirclePanel.circleModel.CircleId].UpdateTime = ModelUtility.CurrentTimeMillisLocal();
                        if (VMCircle.Instance.MembersList.Where(x => x.CircleMembershipStatus == StatusConstants.CIRCLE_MEMBER).Count() == VMCircle.Instance.CircleModel.MemberCount - VMCircle.Instance.CircleModel.AdminCount)
                        {
                            VMCircle.Instance.HideShowMore();
                        }
                        else
                        {
                            VMCircle.Instance.LoadShowMore();
                        }

                        //new InsertIntoCircleListTable(RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY[UCMiddlePanelSwitcher.View_UCCirclePanel.circleModel.CircleId]);
                    }
                    else
                    {
                        if (VMCircle.Instance.ScrollMode == StatusConstants.CIRCLE_MEMBER_ADMIN_SCROLL &&
                             VMCircle.Instance.MembersList.Where(x => x.CircleMembershipStatus != StatusConstants.CIRCLE_MEMBER).ToList().Count == VMCircle.Instance.CircleModel.AdminCount)
                        {
                            VMCircle.Instance.ShowLoader();
                            VMCircle.Instance.HideShowMore();
                        }
                        else
                        {
                            VMCircle.Instance.LoadShowMore();
                        }
                    }
                }
            });
        }

        public void LoadSearchedCircleMembers(List<CircleMemberDTO> listSearch, string searchParam)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {

                    if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null && VMCircle.Instance.MembersSearchList != null)
                    {
                        if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.addMembersPanel.Visibility != Visibility.Visible)
                        {
                            List<UserBasicInfoDTO> unknownUsers = new List<UserBasicInfoDTO>();
                            foreach (CircleMemberDTO displayDTO in listSearch)
                            {
                                if (!VMCircle.Instance.MembersSearchList.Any(x => x.ShortInfoModel.UserIdentity == displayDTO.RingID))
                                {
                                    UserBasicInfoModel userModel = null;
                                    if (displayDTO.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                                    {
                                        VMCircle.Instance.AddButtonVisible = (displayDTO.IsAdmin) ? Visibility.Visible : Visibility.Collapsed;
                                        RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FriendShipStatus = -1;
                                        userModel = RingIDViewModel.Instance.MyBasicInfoModel;
                                        userModel.CircleId = displayDTO.CircleId;
                                        userModel.IsAdmin = displayDTO.IsAdmin;
                                        userModel.CircleMembershipStatus = userModel.ShortInfoModel.UserIdentity == VMCircle.Instance.CircleModel.SuperAdmin ? StatusConstants.CIRCLE_SUPER_ADMIN : userModel.IsAdmin == true ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;
                                    }
                                    else
                                    {
                                        userModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(displayDTO.UserTableID, displayDTO.RingID, displayDTO.FullName, displayDTO.ProfileImage);
                                        userModel.LoadDataFromCircleMember(displayDTO);
                                        userModel.IsAdmin = displayDTO.IsAdmin;
                                        userModel.CircleMembershipStatus = userModel.ShortInfoModel.UserIdentity == VMCircle.Instance.CircleModel.SuperAdmin ? StatusConstants.CIRCLE_SUPER_ADMIN : userModel.IsAdmin == true ? StatusConstants.CIRCLE_ADMIN : StatusConstants.CIRCLE_MEMBER;
                                    }
                                    VMCircle.Instance.MembersSearchList.Add(userModel);
                                    VMCircle.Instance.HideShowMore();
                                }
                            }

                            VMCircle.Instance.LoadShowMore();

                            foreach (var item in VMCircle.Instance.MembersList)
                            {
                                item.OnPropertyChanged("CurrentInstance");
                            }
                            if (!string.IsNullOrWhiteSpace(searchParam) && VMCircle.Instance.SearchMap.ContainsKey(searchParam))
                            {
                                VMCircle.Instance.SearchMap[searchParam] += listSearch.Count;
                            }
                        }
                        else // draglist operation
                        {
                            foreach (var item in listSearch)
                            {
                                if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel._ucCircleAddMembersPanel.DragListForCircle.Any(x => x.ShortInfoModel.UserIdentity == SearchCircleMember._UserIdentity))
                                {
                                    UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel._ucCircleAddMembersPanel.DragListForCircle.Remove(SearchCircleMember._UserBasicInfoModel);
                                    UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel._ucCircleAddMembersPanel.SetErrorLabelExists();
                                    UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel._ucCircleAddMembersPanel.SetDragTextVisibility();
                                    SearchCircleMember._SearchCircleId = 0;
                                    SearchCircleMember._SearchLimit = 0;
                                    SearchCircleMember._SearchParam = string.Empty;
                                    SearchCircleMember._UserIdentity = 0;
                                    SearchCircleMember._IsSearchRunning = false;
                                    SearchCircleMember._UserBasicInfoModel = null;
                                    break;
                                }
                            }
                        }
                    }
                    if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null && VMCircle.Instance.MembersList != null)
                    {
                        if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.addMembersPanel.Visibility != Visibility.Visible)
                        {
                            //UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.Search_friends();
                            VMCircle.Instance.SearchFriends();
                        }
                    }

                });
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void StopLoader(long circleId)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null && VMCircle.Instance.CircleModel.UserTableID == circleId)
                {
                    VMCircle.Instance.LoadShowMore();
                }
            });
        }

        public void NoMoreSearchResult()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null && VMCircle.Instance.CircleModel.UserTableID == SearchCircleMember._SearchCircleId)
                {
                    if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.addMembersPanel.Visibility != Visibility.Visible)
                    {
                        VMCircle.Instance.SearchMapFetcher[SearchCircleMember._SearchParam] = true;
                        VMCircle.Instance.HideShowMore();
                    }
                    else
                    {
                        SearchCircleMember._SearchCircleId = 0;
                        SearchCircleMember._SearchLimit = 0;
                        SearchCircleMember._SearchParam = string.Empty;
                        SearchCircleMember._UserIdentity = 0;
                        SearchCircleMember._IsSearchRunning = false;
                        SearchCircleMember._UserBasicInfoModel = null;
                        UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel._ucCircleAddMembersPanel.SetDragTextVisibility();
                    }
                    VMCircle.Instance.SearchFriends();
                }
            });
        }

        public void SetUIControlOfCircleViewWrapper()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (MainSwitcher.PopupController.circleViewWrapper != null && MainSwitcher.PopupController.circleViewWrapper.Visibility == Visibility.Visible)
                {
                    MainSwitcher.PopupController.circleViewWrapper.SetUIControlOfCircleViewWrapper();
                }
            });
        }

        public void FetchCircleDetails(CircleDTO dto)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    VMCircle.Instance.TotalMember = dto.MemberCount;
                    VMCircle.Instance.CircleModel.MemberCount = dto.MemberCount;
                    VMCircle.Instance.CircleModel.AdminCount = dto.AdminCount;
                    //if (UCMiddlePanelSwitcher.View_UCCirclePanel != null)
                    //{
                    //    UCMiddlePanelSwitcher.View_UCCirclePanel.circleModel.MemberCount = dto.MemberCount;
                    //    UCMiddlePanelSwitcher.View_UCCirclePanel.circleModel.AdminCount = dto.AdminCount;
                    //}
                }
                catch (Exception ex)
                {

                    log.Error("ERROR==> " + ex.StackTrace);
                }
            });
        }

        public void DisableFeedShowMore(long circleId)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                //if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null && circleId == VMCircle.Instance.CircleModel.CircleId)
                //{
                //    UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.IsLoadMoreEnabled = false;
                //}
            });
        }

        private void ProcessCircleNewsFeeds() // 198
        {

        }

        #endregion "Utility Methods"

    }
}

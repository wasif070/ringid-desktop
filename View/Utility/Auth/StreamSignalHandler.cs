﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Stream;
using View.UI.StreamAndChannel;
using View.Utility.Stream;
using View.ViewModel;

namespace View.Utility.Auth
{
    public class StreamSignalHandler
    {
        public static StreamDTO MakeStreamDTO(JObject _JobjFromResponse)
        {
            StreamDTO streamingDTO = new StreamDTO();
            if (_JobjFromResponse[JsonKeys.UserTableID] != null)
            {
                streamingDTO.UserTableID = (long)_JobjFromResponse[JsonKeys.UserTableID];
            }
            if (_JobjFromResponse[JsonKeys.StreamFullName] != null)
            {
                streamingDTO.UserName = (string)_JobjFromResponse[JsonKeys.StreamFullName];
            }
            if (_JobjFromResponse[JsonKeys.Type] != null)
            {
                streamingDTO.UserType = (int)_JobjFromResponse[JsonKeys.Type];
            }
            if (_JobjFromResponse[JsonKeys.ProfileImage] != null)
            {
                streamingDTO.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
            }
            if (_JobjFromResponse[JsonKeys.StreamLongitude] != null)
            {
                streamingDTO.Longitude = (float)_JobjFromResponse[JsonKeys.StreamLongitude];
            }
            if (_JobjFromResponse[JsonKeys.Latitude] != null)
            {
                streamingDTO.Latitude = (float)_JobjFromResponse[JsonKeys.Latitude];
            }
            if (_JobjFromResponse[JsonKeys.StreamStartTime] != null)
            {
                streamingDTO.StartTime = (long)_JobjFromResponse[JsonKeys.StreamStartTime];
            }
            if (_JobjFromResponse[JsonKeys.StreamEndTime] != null)
            {
                streamingDTO.EndTime = (long)_JobjFromResponse[JsonKeys.StreamEndTime];
            }
            if (_JobjFromResponse[JsonKeys.StreamViewerCount] != null)
            {
                streamingDTO.ViewCount = (long)_JobjFromResponse[JsonKeys.StreamViewerCount];
            }
            if (_JobjFromResponse[JsonKeys.Title] != null)
            {
                streamingDTO.Title = (string)_JobjFromResponse[JsonKeys.Title];
            }
            if (_JobjFromResponse[JsonKeys.StreamId] != null)
            {
                streamingDTO.StreamID = (Guid)_JobjFromResponse[JsonKeys.StreamId];
            }
            if (_JobjFromResponse[JsonKeys.StreamCategoryList] != null)
            {
                JArray jArray = (JArray)_JobjFromResponse[JsonKeys.StreamCategoryList];
                streamingDTO.CategoryList = new List<StreamCategoryDTO>();
                foreach (JObject catObj in jArray)
                {
                    StreamCategoryDTO strmCatDTO = new StreamCategoryDTO();
                    strmCatDTO.CategoryID = (int)catObj[JsonKeys.StreamCategoryId];
                    strmCatDTO.CategoryName = (string)catObj[JsonKeys.Category];
                    streamingDTO.CategoryList.Add(strmCatDTO);
                }
            }
            if (_JobjFromResponse[JsonKeys.StreamChatServerIp] != null)
            {
                streamingDTO.ChatServerIP = (string)_JobjFromResponse[JsonKeys.StreamChatServerIp];
            }
            if (_JobjFromResponse[JsonKeys.StreamChatServerPort] != null)
            {
                streamingDTO.ChatServerPort = (int)_JobjFromResponse[JsonKeys.StreamChatServerPort];
            }
            if (_JobjFromResponse[JsonKeys.StreamPublisherServerIp] != null)
            {
                streamingDTO.PublisherServerIP = (string)_JobjFromResponse[JsonKeys.StreamPublisherServerIp];
            }
            if (_JobjFromResponse[JsonKeys.StreamPublisherServerPort] != null)
            {
                streamingDTO.PublisherServerPort = (int)_JobjFromResponse[JsonKeys.StreamPublisherServerPort];
            }
            if (_JobjFromResponse[JsonKeys.StreamViewerServerIp] != null)
            {
                streamingDTO.ViewerServerIP = (string)_JobjFromResponse[JsonKeys.StreamViewerServerIp];
            }
            if (_JobjFromResponse[JsonKeys.StreamViewerServerPort] != null)
            {
                streamingDTO.ViewerServerPort = (int)_JobjFromResponse[JsonKeys.StreamViewerServerPort];
            }
            if (_JobjFromResponse[JsonKeys.StreamChatOn] != null)
            {
                streamingDTO.ChatOn = (bool)_JobjFromResponse[JsonKeys.StreamChatOn];
            }
            if (_JobjFromResponse[JsonKeys.StreamGiftOn] != null)
            {
                streamingDTO.GiftOn = (bool)_JobjFromResponse[JsonKeys.StreamGiftOn];
            }
            if (_JobjFromResponse[JsonKeys.Country] != null)
            {
                streamingDTO.Country = (string)_JobjFromResponse[JsonKeys.Country];
            }
            if (_JobjFromResponse[JsonKeys.DeviceCategory] != null)
            {
                streamingDTO.DeviceCategory = (int)_JobjFromResponse[JsonKeys.DeviceCategory];
            }
            if (_JobjFromResponse[JsonKeys.StreamDistance] != null)
            {
                streamingDTO.Distance = (double)_JobjFromResponse[JsonKeys.StreamDistance];
            }
            if (_JobjFromResponse[JsonKeys.LikeCount] != null)
            {
                streamingDTO.LikeCount = (long)_JobjFromResponse[JsonKeys.LikeCount];
            }

            return streamingDTO;
        }

        public static StreamUserDTO MakeStreamUserDTO(JObject jStreamUserDTO)
        {
            StreamUserDTO streamUserDTO = new StreamUserDTO();
            if (jStreamUserDTO[JsonKeys.UserTableID] != null)
            {
                streamUserDTO.UserTableID = (long)jStreamUserDTO[JsonKeys.UserTableID];
            }
            if (jStreamUserDTO[JsonKeys.StreamFullName] != null)
            {
                streamUserDTO.UserName = (string)jStreamUserDTO[JsonKeys.StreamFullName];
            }
            if (jStreamUserDTO[JsonKeys.ProfileImage] != null)
            {
                streamUserDTO.ProfileImage = (string)jStreamUserDTO[JsonKeys.ProfileImage];
            }
            if (jStreamUserDTO[JsonKeys.StreamCoinCount] != null)
            {
                streamUserDTO.CoinCount = (long)jStreamUserDTO[JsonKeys.StreamCoinCount];
            }
            if (jStreamUserDTO[JsonKeys.StreamFollowingCount] != null)
            {
                streamUserDTO.FollowingCount = (long)jStreamUserDTO[JsonKeys.StreamFollowingCount];
            }
            if (jStreamUserDTO[JsonKeys.StreamFollowerCount] != null)
            {
                streamUserDTO.FollowerCount = (long)jStreamUserDTO[JsonKeys.StreamFollowerCount];
            }
            if (jStreamUserDTO[JsonKeys.Country] != null)
            {
                streamUserDTO.Country = (string)jStreamUserDTO[JsonKeys.Country];
            }
            if (jStreamUserDTO[JsonKeys.StreamIsFollowing] != null)
            {
                streamUserDTO.IsFollowing = (bool)jStreamUserDTO[JsonKeys.StreamIsFollowing];
            }

            return streamUserDTO;
        }

        public static StreamServerDTO MakeStreamServerDTO(JObject jStreamServerDTO)
        {
            StreamServerDTO streamServerDTO = new StreamServerDTO();
            if (jStreamServerDTO[JsonKeys.StreamId] != null)
            {
                streamServerDTO.StreamID = (Guid)jStreamServerDTO[JsonKeys.StreamId];
            }
            if (jStreamServerDTO[JsonKeys.Id] != null)
            {
                streamServerDTO.ServerID = (long)jStreamServerDTO[JsonKeys.Id];
            }
            if (jStreamServerDTO[JsonKeys.StreamPublisherServerIp] != null)
            {
                streamServerDTO.ServerIP = (string)jStreamServerDTO[JsonKeys.StreamPublisherServerIp];
            }
            if (jStreamServerDTO[JsonKeys.StreamPublisherServerPort] != null)
            {
                streamServerDTO.RegisterPort = (int)jStreamServerDTO[JsonKeys.StreamPublisherServerPort];
            }
            return streamServerDTO;
        }

        public void PROCESS_UPDATE_FOLLOW_UNFOLLOW_USER(JObject _jObjFromResponse)
        {
            if (StreamViewModel.Instance.StreamUserInfoModel != null && StreamViewModel.Instance.StreamUserInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
            {
                StreamViewModel.Instance.StreamUserInfoModel.FollowerCount++;
            }
        }

        public void PROCESS_TYPE_GET_FEATURED_LIVE_STREAMS_REQUEST_2004(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamDetailsList] != null)
            {
                int scrollType = _JobjFromResponse[JsonKeys.StreamScroll] != null ? (int)_JobjFromResponse[JsonKeys.StreamScroll] : 0;
                JArray jStreamDetailsList = (JArray)_JobjFromResponse[JsonKeys.StreamDetailsList];
                foreach (JObject jStreamDTO in jStreamDetailsList)
                {
                    if (jStreamDTO[JsonKeys.UserTableID] != null && (long)jStreamDTO[JsonKeys.UserTableID] != DefaultSettings.LOGIN_TABLE_ID)
                    {
                        StreamDTO streamDTO = MakeStreamDTO(jStreamDTO);
                        StreamHelpers.AddIntoStreamFeatureList(streamDTO);
                        HelperMethods.AddIntoFeaturedStreamAndChannelList(streamDTO, null, scrollType == StreamConstants.STREAM_TOP_SCROLL);
                    }
                }
            }
        }

        public void PROCESS_TYPE_GET_RECENT_STREAMS_REQUEST_2005(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamDetailsList] != null)
            {
                JArray jStreamDetailsList = (JArray)_JobjFromResponse[JsonKeys.StreamDetailsList];
                foreach (JObject jStreamDTO in jStreamDetailsList)
                {
                    if (jStreamDTO[JsonKeys.UserTableID] != null && (long)jStreamDTO[JsonKeys.UserTableID] != DefaultSettings.LOGIN_TABLE_ID)
                    {
                        StreamDTO strmDTO = MakeStreamDTO(jStreamDTO);
                        StreamHelpers.AddIntoStreamRecentList(strmDTO);
                    }
                }
            }
        }

        public void PROCESS_UPDATE_LIVE_STREAM_2006(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamDetails] != null)
            {
                StreamDTO streamDTO = StreamSignalHandler.MakeStreamDTO((JObject)_JobjFromResponse[JsonKeys.StreamDetails]);

                if (_JobjFromResponse[JsonKeys.StreamId] != null)
                {
                    streamDTO.StreamID = (Guid)_JobjFromResponse[JsonKeys.StreamId];
                }
                if (_JobjFromResponse[JsonKeys.ProfileImage] != null)
                {
                    streamDTO.ProfileImage = (string)_JobjFromResponse[JsonKeys.ProfileImage];
                }
                if (_JobjFromResponse[JsonKeys.FullName] != null)
                {
                    streamDTO.UserName = (string)_JobjFromResponse[JsonKeys.FullName];
                }
                if (_JobjFromResponse[JsonKeys.UserTableID] != null)
                {
                    streamDTO.UserTableID = (long)_JobjFromResponse[JsonKeys.UserTableID];
                }

                StreamHelpers.AddIntoStreamFollowingList(streamDTO/*, StreamHelpers.IsStreamNotifyable()*/);
                StreamHelpers.AddIntoStreamRecentList(streamDTO);
                HelperMethods.AddIntoFeaturedStreamAndChannelList(streamDTO, null, true);
                StreamHelpers.AddIntoStreamFeatureList(streamDTO);
            }
        }

        public void PROCESS_TYPE_GET_STREAM_CATEGORY_LIST_REQUEST_2007(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamCategoryList] != null)
            {
                JArray jArray = (JArray)_JobjFromResponse[JsonKeys.StreamCategoryList];
                List<StreamCategoryDTO> catList = new List<StreamCategoryDTO>();
                foreach (JObject catObj in jArray)
                {
                    StreamCategoryDTO strmCatDTO = new StreamCategoryDTO();
                    strmCatDTO.CategoryID = (int)catObj[JsonKeys.StreamCategoryId];
                    strmCatDTO.CategoryName = (string)catObj[JsonKeys.Category];
                    catList.Add(strmCatDTO);
                }
                StreamHelpers.AddStreamCategoryList(catList, StreamViewModel.Instance.StreamCategoryList);
            }
        }

        public void PROCESS_TYPE_GET_MOST_VIEWED_STREAMS_2008(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamDetailsList] != null)
            {
                JArray jStreamDetailsList = (JArray)_JobjFromResponse[JsonKeys.StreamDetailsList];
                foreach (JObject jStreamDTO in jStreamDetailsList)
                {
                    if (jStreamDTO[JsonKeys.UserTableID] != null && (long)jStreamDTO[JsonKeys.UserTableID] != DefaultSettings.LOGIN_TABLE_ID)
                    {
                        StreamDTO strmDTO = MakeStreamDTO(jStreamDTO);
                        StreamHelpers.AddIntoStreamMostViewList(strmDTO);
                    }
                }
            }
        }

        public void PROCESS_TYPE_GET_NEAREST_STREAMS_2009(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamDetailsList] != null)
            {
                JArray jStreamDetailsList = (JArray)_JobjFromResponse[JsonKeys.StreamDetailsList];
                List<StreamModel> strmModelList = new List<StreamModel>();
                foreach (JObject jStreamDTO in jStreamDetailsList)
                {
                    StreamDTO strmDTO = MakeStreamDTO(jStreamDTO);
                    StreamHelpers.AddIntoStreamNearByList(strmDTO);
                }
            }
        }

        public void PROCESS_TYPE_SEARCH_LIVE_STREAMS_REQUEST_2010(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamDetailsList] != null)
            {
                int scrollType = _JobjFromResponse[JsonKeys.StreamScroll] != null ? (int)_JobjFromResponse[JsonKeys.StreamScroll] : 0;
                string searchParam = _JobjFromResponse[JsonKeys.Title] != null ? (string)_JobjFromResponse[JsonKeys.Title] : String.Empty;
                string country = _JobjFromResponse[JsonKeys.CelebrityCountry] != null ? (string)_JobjFromResponse[JsonKeys.CelebrityCountry] : String.Empty;
                bool isFriend = _JobjFromResponse[JsonKeys.StreamIsFriendOnly] != null ? (bool)_JobjFromResponse[JsonKeys.StreamIsFriendOnly] : false;
                int catId = 0;

                if (_JobjFromResponse[JsonKeys.StreamCategoryList] != null)
                {
                    JArray array = (JArray)_JobjFromResponse[JsonKeys.StreamCategoryList];
                    if (array.Count > 0)
                    {
                        catId = (int)array[0][JsonKeys.StreamCategoryId];
                    }
                }

                JArray jStreamDetailsList = (JArray)_JobjFromResponse[JsonKeys.StreamDetailsList];
                if (catId > 0)
                {
                    ObservableCollection<StreamModel> modelList = null;
                    if (UCStreamAndChannelWrapper.ViewType == StreamAndChannelConstants.TypeStreamFollowingPanel)
                    {
                        StreamCategoryModel categoryModel = StreamViewModel.Instance.StreamCountByCategoryList.FirstOrDefault(P => P.CategoryID == catId);
                        if (categoryModel != null && categoryModel.StreamList != null)
                        {
                            modelList = categoryModel.StreamList;
                        }
                    }
                    else if (UCStreamAndChannelWrapper.ViewType == StreamAndChannelConstants.TypeStreamAndChannelMoreListPanel)
                    {
                        modelList = StreamViewModel.Instance.StreamByCategoryList;
                    }

                    if (modelList != null)
                    {
                        foreach (JObject jStreamDTO in jStreamDetailsList)
                        {
                            if (jStreamDTO[JsonKeys.UserTableID] != null && (long)jStreamDTO[JsonKeys.UserTableID] != DefaultSettings.LOGIN_TABLE_ID)
                            {
                                StreamDTO strmDTO = MakeStreamDTO(jStreamDTO);
                                StreamHelpers.AddIntoStreamSearchList(strmDTO, modelList);
                            }
                        }
                    }
                }
                else if (!String.IsNullOrWhiteSpace(country))
                {
                    foreach (JObject jStreamDTO in jStreamDetailsList)
                    {
                        if (jStreamDTO[JsonKeys.UserTableID] != null && (long)jStreamDTO[JsonKeys.UserTableID] != DefaultSettings.LOGIN_TABLE_ID)
                        {
                            StreamDTO strmDTO = MakeStreamDTO(jStreamDTO);
                            StreamHelpers.AddIntoStreamSearchList(strmDTO, StreamViewModel.Instance.StreamByCountryList);
                        }
                    }
                }
                else if (!String.IsNullOrWhiteSpace(searchParam))
                {
                    foreach (JObject jStreamDTO in jStreamDetailsList)
                    {
                        if (jStreamDTO[JsonKeys.UserTableID] != null && (long)jStreamDTO[JsonKeys.UserTableID] != DefaultSettings.LOGIN_TABLE_ID)
                        {
                            StreamDTO strmDTO = MakeStreamDTO(jStreamDTO);
                            StreamHelpers.AddIntoStreamSearchList(strmDTO, StreamViewModel.Instance.StreamSearchList);
                        }
                    }
                }
            }
        }

        public void PROCESS_UPDATE_STREAMING_LIKE_COUNT_2012(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
            {

            }
        }

        public void PROCESS_GET_FOLLOWING_STREAMS_REQUEST_2013(JObject _JobjFromResponse)
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamDetailsList] != null)
            {
                int catId = _JobjFromResponse[JsonKeys.StreamCategoryId] != null ? (int)_JobjFromResponse[JsonKeys.StreamCategoryId] : 0;
                /*bool isNotifyable = StreamHelpers.IsStreamNotifyable()*/;

                JArray jStreamDetailsList = (JArray)_JobjFromResponse[JsonKeys.StreamDetailsList];
                List<StreamModel> strmModelList = new List<StreamModel>();
                foreach (JObject jStreamDTO in jStreamDetailsList)
                {
                    if (jStreamDTO[JsonKeys.UserTableID] != null && (long)jStreamDTO[JsonKeys.UserTableID] != DefaultSettings.LOGIN_TABLE_ID)
                    {
                        StreamDTO strmDTO = MakeStreamDTO(jStreamDTO);
                        StreamHelpers.AddIntoStreamFollowingList(strmDTO/*, isNotifyable*/);
                    }
                }
            }
        }

        public void PROCESS_GET_CATEGORY_WISE_STREAM_COUNT_REQUEST_2014(JObject _JobjFromResponse) 
        {
            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.StreamCategoryList] != null)
            {
                JArray jArray = (JArray)_JobjFromResponse[JsonKeys.StreamCategoryList];
                List<StreamCategoryDTO> catList = new List<StreamCategoryDTO>();
                foreach (JObject catObj in jArray)
                {
                    StreamCategoryDTO strmCatDTO = new StreamCategoryDTO();
                    strmCatDTO.CategoryID = (int)catObj[JsonKeys.StreamCategoryId];
                    strmCatDTO.CategoryName = (string)catObj[JsonKeys.Category];
                    strmCatDTO.FollowingCount = (int)catObj[JsonKeys.Count];
                    catList.Add(strmCatDTO);
                }
                StreamHelpers.AddStreamCategoryList(catList, StreamViewModel.Instance.StreamCountByCategoryList);
            }
        }
    }
}

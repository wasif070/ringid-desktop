﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.PopUp;
using View.UI.Profile.FriendProfile;
using View.ViewModel;
using View.UI.Profile.CelebrityProfile;
using View.Utility.DataContainer;

namespace View.Utility.Auth
{
    public class CelebritySignalHandler
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(CelebritySignalHandler).Name);
        #endregion "Fields"

        public void Process_TYPE_CELEBRITY_FEED_286(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
                    && _JobjFromResponse[JsonKeys.CelebrityList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.CelebrityListType] != null)// && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.Subscribe] != null)
                {
                    int profileType = (_JobjFromResponse[JsonKeys.ProfileType] != null) ? ((int)_JobjFromResponse[JsonKeys.ProfileType]) : SettingsConstants.PROFILE_TYPE_CELEBRITY;
                    int subscType = (int)_JobjFromResponse[JsonKeys.CelebrityListType];
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.CelebrityList];
                    List<CelebrityModel> list = new List<CelebrityModel>();
                    foreach (JObject singleObj in jarray)
                    {
                        long pId = (long)singleObj[JsonKeys.UserIdentity];
                        CelebrityModel model = null;
                        if (!UserProfilesContainer.Instance.CelebrityModels.TryGetValue(pId, out model))
                        {
                            model = new CelebrityModel();
                            UserProfilesContainer.Instance.CelebrityModels[pId] = model;
                        }
                        model.LoadData(singleObj);
                        list.Add(model);
                    }
                    ShowFollowingOrDiscoverCelebrities(subscType, list);
                }
            }
            catch (Exception e)
            {
                log.Error("Celebrity ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_TYPE_CELEBRITY_FEED_288(JObject _JobjFromResponse, string client_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                    {
                        int feed_state = client_packet_id.Equals(DefaultSettings.ALLCELEBRITY_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);
                            FeedDataContainer.Instance.AllCelebrityFeedsSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME)
                            {
                                //if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel != null
                                //    && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFeedPanel != null
                                //    && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFeedPanel.IsVisible)
                                //    FirstTimeFeedLoadWorker.Instance.LoadFirstTimeFeed(RingIDViewModel.Instance.AllCelebrityFeeds, model);
                                //else if (!RingIDViewModel.Instance.AllCelebrityFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                                    //RingIDViewModel.Instance.AllCelebrityFeeds.InsertModel(model);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("AllMediaPageFeeds ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void ShowFollowingOrDiscoverCelebrities(int subscType, Object ob)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    if (ob is List<CelebrityModel>)
                    {
                        List<CelebrityModel> list = (List<CelebrityModel>)ob;
                        if (subscType == 1)//Discover
                        {
                            foreach (var model in list)
                            {
                                RingIDViewModel.Instance.CelebritiesDiscovered.InvokeAdd(model);
                            }
                        }
                        else if (subscType == 2)//Popular
                        {
                            foreach (var model in list)
                            {
                                RingIDViewModel.Instance.CelebritiesPopular.Add(model);
                            }
                        }
                        else if (subscType == 3)//Following
                        {
                            foreach (var model in list)
                            {
                                RingIDViewModel.Instance.CelebritiesFollowing.InvokeAdd(model);
                            }
                        }
                    }
                }));
            }
            catch (Exception e)
            {
                log.Error("ex in SearchedDiscoverCelebrities==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
    }
}

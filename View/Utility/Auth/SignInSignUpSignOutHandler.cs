﻿using System;
using System.Windows;
using Auth.Parser;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using View.UI;
using View.Utility.SignInSignUP;

namespace View.Utility.Auth
{
    public class SignInSignUpSignOutHandler
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(SignInSignUpSignOutHandler).Name);
        public ThradSessionValidationRequest thradSessionValidationRequest = null;
        #endregion"Fields"

        #region "Public Methods"

        public void Process_UPDATE_DIGITS_VERIFY_209(JObject _JobjFromResponse)
        {
            try
            {
                ViewConstants.DigitsDatas = new Models.Entity.DigitsData();
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    ViewConstants.DigitsDatas.IsSccess = true;
                    ViewConstants.DigitsDatas.MobileDialingCode = (_JobjFromResponse[JsonKeys.DialingCode] != null) ? (string)_JobjFromResponse[JsonKeys.DialingCode] : "";
                    ViewConstants.DigitsDatas.PhoneNumber = (_JobjFromResponse[JsonKeys.MobilePhone] != null) ? (string)_JobjFromResponse[JsonKeys.MobilePhone] : "";
                    //if (DefaultSettings.TMP_RECOVERY_MOBILE != null) DefaultSettings.TMP_RECOVERY_MOBILE = null;
                }
                else ViewConstants.DigitsDatas.IsSccess = false;
            }
            catch (Exception ex) { log.Error("ProcessHashtagMediaContents ex ==>" + ex.StackTrace); }
        }

        public void Process_MULTIPLE_SESSION_WARNING_75(CommonPacketAttributes packet_attributes)
        {
            try
            {
                if (packet_attributes != null && packet_attributes.SessionID.Equals(DefaultSettings.LOGIN_SESSIONID))
                {
                    DefaultSettings.LOGIN_SESSIONID = null;
                    DefaultSettings.LOGIN_RING_ID = 0;
                    DefaultSettings.LOGIN_TABLE_ID = 0;
                    DefaultSettings.VALUE_LOGIN_USER_PASSWORD = null;
                    UIHelperMethods.MultipleSessionWarning();
                }
            }
            catch (Exception e) { log.Error("Exception in processMultipleSessionWarning==>" + e.StackTrace + "==>" + e.Message); }
        }

        public void Process_UNWANTED_LOGIN_79(CommonPacketAttributes packet_attributes)
        {
            Application.Current.Dispatcher.Invoke(() =>
          {
              try
              {
                  if (UCGuiRingID.Instance != null) UIHelperMethods.ShowSignoutLoader(false, UCGuiRingID.Instance.motherGridToShowPopup, StartUpConstatns.ARGUMENT_SESSIONINVALID);
              }
              catch (Exception e) { log.Error("Exception in AuthHandler ==>" + e.StackTrace + "==>" + e.Message); }
          });
        }

        public void Process_INVALID_LOGIN_SESSION_19(JObject jObject)
        {
            try
            {
                StopNetworkThread();
                if (thradSessionValidationRequest == null) thradSessionValidationRequest = new ThradSessionValidationRequest();
                thradSessionValidationRequest.StartThread();
            }
            catch (Exception e) { log.Error("Exception in ProcessInvalidLoginSession ==>" + e.StackTrace + "==>" + e.Message); }
        }


        #endregion "Public Methods"

        #region "Utility Methods"

        private void StopNetworkThread()
        {
            MainSwitcher.ThreadManager().StopNetworkThread();
        }

        #endregion "Utility Methods"

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using View.UI.PopUp;
using View.Utility.DataContainer;
using View.BindingModels;
using View.ViewModel;

namespace View.Utility.Auth
{
    public class ThreadLikeRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadLikeRequest).Name);
        private bool running = false;
        UCLikeList likeList;
        #endregion "Private Fields"

        #region Ctors
        public ThreadLikeRequest(UCLikeList likeList)
        {
            this.likeList = likeList;
        }
        #endregion

        #region "Private methods"
        /// <summary>
        /// TYPE_LIKES_FOR_STATUS = 92;
        /// ACTION_MERGED_LIKES_LIST_OF_COMMENT = 1116;
        /// </summary>
        private void Run()
        {
            try
            {
                running = true;
                int action = 0;
                if (likeList.LikeList.Count >= 10)
                {
                    likeList.IsLoadingMoreLikes = true;
                }
                if (likeList != null)
                {
                    JObject pakToSend = new JObject();

                    if (likeList.CommentId != Guid.Empty)
                    {
                        action = AppConstants.ACTION_MERGED_LIKES_LIST_OF_COMMENT;
                        pakToSend[JsonKeys.CommentId] = likeList.CommentId;
                    }

                    else if (likeList.NewsfeedId != Guid.Empty && likeList.CommentId == Guid.Empty && likeList.ContentId == Guid.Empty && likeList.ImageId == Guid.Empty)
                    {
                        FeedModel feedModel = null;
                        action = AppConstants.TYPE_LIKES_FOR_STATUS;
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_STATUS;
                        if (likeList.NewsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(likeList.NewsfeedId, out feedModel))
                        {
                            pakToSend[JsonKeys.Type] = feedModel.BookPostType;
                            if (feedModel.BookPostType == 2 || feedModel.BookPostType == 5 || feedModel.BookPostType == 8)
                            {
                                if (feedModel.SingleMediaFeedModel != null)
                                {
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleMediaFeedModel.ContentId;
                                }
                                else if (feedModel.SingleImageFeedModel != null)
                                {
                                    pakToSend[JsonKeys.ContentId] = feedModel.SingleImageFeedModel.ImageId;
                                }
                            }
                        }
                    }

                    else if (likeList.ImageId != Guid.Empty && likeList.CommentId == Guid.Empty)
                    {
                        action = AppConstants.TYPE_LIKES_FOR_STATUS;
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_IMAGE;
                        pakToSend[JsonKeys.ContentId] = likeList.ImageId;
                    }

                    else if (likeList.ContentId != Guid.Empty && likeList.CommentId == Guid.Empty)
                    {
                        action = AppConstants.TYPE_LIKES_FOR_STATUS;
                        pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_MULTIMEDIA;
                        pakToSend[JsonKeys.ContentId] = likeList.ContentId;
                    }

                    if (likeList.NewsfeedId != Guid.Empty)
                    {
                        pakToSend[JsonKeys.NewsfeedId] = likeList.NewsfeedId;
                        ViewConstants.NewsFeedID = likeList.NewsfeedId;
                    }
                    if (RingIDViewModel.Instance.LikeList.Count > 0)
                    {
                        pakToSend[JsonKeys.PivotId] = RingIDViewModel.Instance.LikeList.Min(item => item.ShortInfoModel.UserTableID);
                        pakToSend[JsonKeys.Scroll] = 2;
                    }
                    if (action > 0)
                    {
                        string packetId = SendToServer.GetRanDomPacketID();
                        pakToSend[JsonKeys.PacketId] = packetId;
                        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                        pakToSend[JsonKeys.Action] = action;

                        JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId, true);
                        if (feedbackfields != null)
                        {
                            Models.Stores.RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                        }
                    }

                }
            }
            catch (Exception)
            {


            }
            finally
            {
                running = false;
                likeList.IsLoadingMoreLikes = false;
            }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

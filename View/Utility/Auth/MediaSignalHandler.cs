<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.PopUp;
using View.Utility.DataContainer;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.Utility.Auth
{
    public class MediaSignalHandler
    {
        //FeedSignalHandler feedSignalHandler = new FeedSignalHandler();

        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(MediaSignalHandler).Name);
        //int BreakingSequenceCount = 0;
        #endregion "Fields"

        #region "SIGNAL HANDLER"
        public void Process_MEDIA_FEED_87(JObject _JobjFromResponse, string client_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    int scl = (int)_JobjFromResponse[JsonKeys.Scroll];
                    Guid npUUid = (Guid)_JobjFromResponse[JsonKeys.NpUUID];
                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                    {
                        int feed_state = client_packet_id.Equals(DefaultSettings.ALLMEDIA_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);

                            if (scl != 1)
                            {
                                FeedDataContainer.Instance.AllMediaBottomIds.PvtMinGuid = npUUid;
                                if (feed_state != SettingsConstants.FEED_FIRSTTIME && !FeedDataContainer.Instance.AllMediaCurrentIds.Contains(nfId))
                                    FeedDataContainer.Instance.AllMediaBottomIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            else
                            {
                                FeedDataContainer.Instance.AllMediaTopIds.PvtMaxGuid = npUUid;
                                //if (feed_state != SettingsConstants.FEED_FIRSTTIME) FeedDataContainer.Instance.AllMediaTopIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME || scl == 1)
                            {
                                if (!FeedDataContainer.Instance.AllMediaCurrentIds.Contains(nfId))
                                {
                                    FeedHolderModel holder = new FeedHolderModel(2);
                                    holder.FeedId = nfId;
                                    holder.Feed = model;
                                    holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                    holder.FeedPanelType = holder.Feed.FeedPanelType;
                                    RingIDViewModel.Instance.AllMediaCustomFeeds.CalculateandInsertOtherFeeds(holder, FeedDataContainer.Instance.AllMediaCurrentIds);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("AllMediaPageFeeds ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MEDIA_SHARE_LIST_250(JObject _JobjFromResponse)
        {
            processe_250_115(_JobjFromResponse);
        }

        private void processe_250_115(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Shares] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
                {
                    Guid newsfeedId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Shares];
                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
                    long minIdOfSequence = 99999;
                    foreach (JObject obj in jArray)
                    {
                        if (obj[JsonKeys.UserTableID] != null && obj[JsonKeys.Id] != null && obj[JsonKeys.FullName] != null && obj[JsonKeys.UserTableID] != null && obj[JsonKeys.ProfileImage] != null)
                        {
                            long userTableID = (long)obj[JsonKeys.UserTableID];
                            UserBasicInfoDTO user = null;
                            if (userTableID == DefaultSettings.LOGIN_TABLE_ID)
                            {
                                user = new UserBasicInfoDTO();
                                user.UserTableID = userTableID;
                            }
                            else
                            {
                                user = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(userTableID);
                            }
                            user.FullName = (string)obj[JsonKeys.FullName];
                            user.UserTableID = (long)obj[JsonKeys.UserTableID];
                            user.ProfileImage = (string)obj[JsonKeys.ProfileImage];
                            long Id = (long)obj[JsonKeys.Id];
                            if (Id < minIdOfSequence) minIdOfSequence = Id;
                            list.Add(user);
                        }
                    }
                    MainSwitcher.AuthSignalHandler().feedSignalHandler.ShowSingleFeedShareList(newsfeedId, list, minIdOfSequence);
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessMediaShareList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_ALBUM_LIST_256(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableID] != null && _JobjFromResponse[JsonKeys.MediaType] != null)
                {
                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableID];
                    int mediatype = (int)_JobjFromResponse[JsonKeys.MediaType];
                    Guid npUUId = _JobjFromResponse[JsonKeys.NpUUID] != null ? (Guid)_JobjFromResponse[JsonKeys.NpUUID] : Guid.Empty;
                    ObservableCollection<MediaContentModel> ViewCollection = null;
                    if (utid == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        ViewCollection = (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
                        if (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO)
                        {
                            RingIDViewModel.Instance.AudioAlbumNpUUId = npUUId;
                            DefaultSettings.MY_AUDIO_ALBUMS_COUNT = (int)_JobjFromResponse[JsonKeys.AlbumCount];
                        }
                        else if (mediatype == SettingsConstants.MEDIA_TYPE_VIDEO)
                        {
                            RingIDViewModel.Instance.VideoAlbumNpUUId = npUUId;
                            DefaultSettings.MY_VIDEO_ALBUMS_COUNT = (int)_JobjFromResponse[JsonKeys.AlbumCount];
                        }

                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null)
                        {
                            if (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyProfileMusicAlbum != null)
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyProfileMusicAlbum.SetLoaderTextValue(true);
                            }
                            else if (mediatype == SettingsConstants.MEDIA_TYPE_VIDEO && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyprofileVideoAlbum != null)
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyprofileVideoAlbum.SetLoaderTextValue(true);
                            }
                        }
                    }
                    else if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel != null
                        && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel.ShortInfoModel.UserTableID == utid)
                    {
                        if (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO)
                        {
                            ViewCollection = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums;
                            UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum.SetAudioLoaderTextValue(true);
                        }
                        else
                        {
                            ViewCollection = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums;
                            UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum.SetVideoLoaderTextValue(true);
                        }
                    }
                    if (_JobjFromResponse[JsonKeys.MediaAlbumList] != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaAlbumList];
                        Application.Current.Dispatcher.Invoke((Action)(() =>
                        {
                            foreach (JObject obj in jarray)
                            {
                                Guid albmId = (Guid)obj[JsonKeys.AlbumId];
                                MediaContentModel mediaContentModel = ViewCollection.Where(P => P.AlbumId == albmId).FirstOrDefault();
                                if (mediaContentModel == null)
                                {
                                    mediaContentModel = new MediaContentModel();
                                    ViewCollection.InvokeAdd(mediaContentModel);
                                }
                                mediaContentModel.LoadData(obj);
                                mediaContentModel.AlbumId = albmId;
                            }
                        }));
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaAlbumList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MEDIA_ALBUM_CONTENT_LIST_261(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableID] != null && _JobjFromResponse[JsonKeys.AlbumId] != null)
                {
                    long utid = (long)_JobjFromResponse[JsonKeys.UserTableID];
                    int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
                    Guid albumId = (Guid)_JobjFromResponse[JsonKeys.AlbumId];
                    string albumNm = (_JobjFromResponse[JsonKeys.AlbumName] != null) ? (string)_JobjFromResponse[JsonKeys.AlbumName] : string.Empty;
                    ObservableCollection<SingleMediaModel> ViewCollection = null;
                    if (UCMediaContentsView.Instance != null && UCMediaContentsView.Instance.IsVisible && UCMediaContentsView.Instance.albumModel != null
                        && UCMediaContentsView.Instance.albumModel.AlbumId == albumId
                        && UCMediaContentsView.Instance.albumModel.MediaList != null)
                    {
                        ViewCollection = UCMediaContentsView.Instance.albumModel.MediaList;
                        //UCMediaContentsView.Instance.SetLoaderTextValue(true);
                    }
                    else if (UCMediaListFromAlbumClick.Instance != null
                       && UCMediaListFromAlbumClick.Instance.albumModel != null
                       && UCMediaListFromAlbumClick.Instance.albumModel.AlbumId == albumId
                       && UCMediaListFromAlbumClick.Instance.IsVisible)
                    {
                        ViewCollection = UCMediaListFromAlbumClick.Instance.albumModel.MediaList;
                    }
                    if (_JobjFromResponse[JsonKeys.MediaContentList] != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
                        foreach (JObject ob in jarray)
                        {
                            Guid contentid = (Guid)ob[JsonKeys.ContentId];
                            SingleMediaModel contentModel = null;
                            if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentid, out contentModel))
                            {
                                contentModel = new SingleMediaModel();
                                MediaDataContainer.Instance.ContentModels[contentid] = contentModel;
                            }
                            contentModel.PlayedFromFeed = false;
                            contentModel.LoadData(ob, contentModel.MediaType, contentModel.PlayedFromFeed);
                            if (contentModel.LikeCommentShare == null)
                            {
                                if (contentModel.NewsFeedId != Guid.Empty)
                                {
                                    FeedModel fm = null;
                                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(contentModel.NewsFeedId, out fm) && fm.SingleMediaFeedModel != null && fm.LikeCommentShare != null)
                                    {
                                        contentModel.LikeCommentShare = fm.LikeCommentShare;
                                    }
                                }
                                if (contentModel.LikeCommentShare == null) contentModel.LikeCommentShare = new LikeCommentShareModel();
                            }
                            contentModel.LikeCommentShare.LoadData(ob);
                            if (contentModel.MediaOwner.UserTableID == 0)
                                contentModel.MediaOwner.UserTableID = utid;
                            contentModel.AlbumId = albumId;
                            if (string.IsNullOrEmpty(contentModel.AlbumName)) contentModel.AlbumName = albumNm;
                            if (ViewCollection != null) // && !ViewCollection.Any(P => P.ContentId == contentid)
                                ViewCollection.InvokeAddNoDuplicate(contentModel);

                            if (ViewConstants.AlbumID == albumId)
                            {
                                //if (!RingIDViewModel.Instance.MediaList.Any(P => P.ContentId == contentModel.ContentId)) 
                                RingIDViewModel.Instance.MediaList.InvokeAddNoDuplicate(contentModel);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessMediaAlbumContentList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_LIKE_LIST_269(JObject _JobjFromResponse)
        {
            try
            {
                //if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null)
                //{
                //    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
                //    if (_JobjFromResponse[JsonKeys.Likes] != null && LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null && LikeListViewWrapper.ucLikeListView.ContentId == contentId)
                //    {
                //        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                //        foreach (JObject singleObj in jarray)
                //        {
                //            long utId = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                //            long uId = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                //            string fn = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                //            string prIm = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                //            UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(utId, uId, fn, prIm);
                //            LikeListViewWrapper.ucLikeListView.LikeList.InvokeAdd(userModel);
                //        }
                //        Application.Current.Dispatcher.Invoke((Action)(() =>
                //        {
                //            LikeListViewWrapper.ucLikeListView.SetVisibilities();
                //        }));
                //    }
                //}
                //else if ((bool)_JobjFromResponse[JsonKeys.Success] == false)
                //{
                //    Application.Current.Dispatcher.Invoke((Action)(() =>
                //    {
                //        LikeListViewWrapper.ucLikeListView.HideLoaderandSeeMore();
                //    }));
                //}

                if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null)
                {
                    Guid contentId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                    if (contentId == ViewConstants.ContentID && jarray != null)
                    {
                        foreach (JObject singleObj in jarray)
                        {
                            long utId = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                            long uId = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                            string fn = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                            string prIm = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                            UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(utId, uId, fn, prIm);
                            RingIDViewModel.Instance.LikeList.InvokeAdd(userModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessMediaLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_COMMENT_LIST_270(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && _JobjFromResponse[JsonKeys.ContentId] != null)
                {
                    Guid contentId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                    Guid nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                    bool success = (bool)_JobjFromResponse[JsonKeys.Success];
                    long postOwnerId = 0;
                    ObservableCollection<CommentModel> collectionMdaView = RingIDViewModel.Instance.MediaComments;
                    //lock (RingIDViewModel.Instance.MediaComments)
                    //{
                    if (success && ViewConstants.ContentID == contentId && _JobjFromResponse[JsonKeys.Comments] != null)
                    {
                        SingleMediaModel model = null;
                        if (MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out model))
                        {
                            postOwnerId = model.MediaOwner.UserTableID;
                        }
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                        foreach (JObject singleObj in jarray)
                        {
                            HelperMethods.InsertIntoCommentListByDscTime(collectionMdaView, singleObj, nfId, Guid.Empty, contentId, postOwnerId);
                            System.Threading.Thread.Sleep(10);
                        }
                    }
                    //}
                    if (_JobjFromResponse[JsonKeys.Comments] != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                        foreach (JObject singleObj in jarray)
                        {
                            if (collectionMdaView != null)
                                HelperMethods.InsertIntoCommentListByAscTime(collectionMdaView, singleObj, nfId, Guid.Empty, contentId, postOwnerId);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaCommentList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MEDIACOMMENT_LIKE_LIST_271(JObject _JobjFromResponse)
        {
            try
            {
                if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
                {
                    Guid contentId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                    Guid commentId = (Guid)_JobjFromResponse[JsonKeys.CommentId];
                    //if (_JobjFromResponse[JsonKeys.Likes] != null && LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null && LikeListViewWrapper.ucLikeListView.ContentId == contentId && LikeListViewWrapper.ucLikeListView.CommentId == commentId)
                    //{
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                    foreach (JObject singleObj in jarray)
                    {
                        long utId = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                        long uId = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                        string fn = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                        string prIm = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                        UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(utId, uId, fn, prIm);
                        //LikeListViewWrapper.ucLikeListView.LikeList.InvokeAdd(userModel);
                        RingIDViewModel.Instance.LikeList.InvokeAdd(userModel);
                    }
                    //    Application.Current.Dispatcher.Invoke((Action)(() =>
                    //    {
                    //        LikeListViewWrapper.ucLikeListView.SetVisibilities();
                    //    }));
                    //}
                }
                else if ((bool)_JobjFromResponse[JsonKeys.Success] == false)
                {
                    //Application.Current.Dispatcher.Invoke((Action)(() =>
                    //{
                    //    LikeListViewWrapper.ucLikeListView.HideLoaderandSeeMore();
                    //}));
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessMediaCommentLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_SUGGESTION_277(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
                {
                    string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
                    //List<SearchMediaModel> searchListOfaSearchParam = null;
                    List<SearchMediaDTO> searchListOfaSearchParam = null;
                    lock (MediaDictionaries.Instance.TEMP_SEARCH_SGTNS)
                    {
                        if (!MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.TryGetValue(searchParam, out searchListOfaSearchParam))
                        {
                            searchListOfaSearchParam = new List<SearchMediaDTO>();
                            MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.Add(searchParam, searchListOfaSearchParam);
                        }
                    }
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
                    List<SearchMediaDTO> lst = new List<SearchMediaDTO>();
                    foreach (JObject singleObj in jarray)
                    {
                        SearchMediaDTO dto = new SearchMediaDTO();
                        dto.SearchSuggestion = (string)singleObj[JsonKeys.MediaSearchKey];
                        dto.SearchType = (int)singleObj[JsonKeys.MediaSuggestionType];
                        lst.Add(dto);
                        searchListOfaSearchParam.Add(dto);
                    }
                    if (lst.Count > 0)
                        LoadMediaSearchs(searchParam, lst);
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestionType] != null)
                {
                    int suggestionType = (int)_JobjFromResponse[JsonKeys.MediaSuggestionType];
                    string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
                    if ((bool)_JobjFromResponse[JsonKeys.Success] == true)
                    {
                        if (_JobjFromResponse[JsonKeys.MediaList] != null) //suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_SONGS && 
                        {
                            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
                            ObservableCollection<SingleMediaModel> collection = (suggestionType == 0) ? UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.MediasAllTab : UCMediaContentsView.Instance.SingleMediaItems;
                            // if (UCMediaContentsView.Instance != null && UCMediaContentsView.Instance.SearchParam != null && UCMediaContentsView.Instance.SearchParam.Equals(searchParam))
                            // {
                            foreach (JObject singleObj in jarray)
                            {
                                Guid contentid = (Guid)singleObj[JsonKeys.ContentId];
                                SingleMediaModel contentModel = null;
                                if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentid, out contentModel))
                                {
                                    contentModel = new SingleMediaModel();
                                    MediaDataContainer.Instance.ContentModels[contentid] = contentModel;
                                }
                                contentModel.LoadData(singleObj, contentModel.MediaType);
                                collection.InvokeAdd(contentModel);
                            }
                            //}
                        }
                        if (_JobjFromResponse[JsonKeys.AlbumsList] != null)//suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_ALBUMS &&
                        {
                            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.AlbumsList];
                            ObservableCollection<MediaContentModel> collection = (suggestionType == 0) ? UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.AlbumsAllTab : UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.AlbumsFromSearch;
                            foreach (JObject singleObj in jarray)
                            {
                                //long albumtid = (long)singleObj[JsonKeys.AlbumId];

                                if (UCMiddlePanelSwitcher._UCMediaCloudSearch != null
                                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch != null
                                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.SelectedSearchedParam.Equals(searchParam))
                                {
                                    MediaContentModel contentModel = new MediaContentModel();
                                    contentModel.LoadData(singleObj);
                                    collection.InvokeAdd(contentModel);
                                }
                            }
                        }
                        if (_JobjFromResponse[JsonKeys.MediaSuggestion] != null) //suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_TAGS && 
                        {
                            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
                            ObservableCollection<HashTagModel> collection = (suggestionType == 0) ? UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.HashTagsAllTab : UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.HashTagsFromSearch;
                            foreach (JObject singleObj in jarray)
                            {
                                if (UCMiddlePanelSwitcher._UCMediaCloudSearch != null
                                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch != null
                                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.SelectedSearchedParam.Equals(searchParam))
                                {
                                    Application.Current.Dispatcher.BeginInvoke(() =>
                                    {
                                        if (UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.hashGIFCtrl.IsRunning())
                                            UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.hashGIFCtrl.StopAnimate();
                                    }, DispatcherPriority.Send);

                                    HashTagModel model = new HashTagModel();
                                    model.LoadData(singleObj);
                                    collection.InvokeAdd(model);
                                }
                            }
                            //
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_HASHTAG_MEDIA_CONTENTS_279(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.HashTagName] != null)
                {
                    string hashTagName = (string)_JobjFromResponse[JsonKeys.HashTagName];
                    if (_JobjFromResponse[JsonKeys.MediaList] != null &&
                        UCMediaContentsView.Instance != null && UCMediaContentsView.Instance.hashTagModel != null && UCMediaContentsView.Instance.hashTagModel.HashTagSearchKey.Equals(hashTagName) && UCMediaContentsView.Instance.hashTagModel.MediaList != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
                        foreach (JObject ob in jarray)
                        {
                            Guid contentid = (Guid)ob[JsonKeys.ContentId];
                            SingleMediaModel contentModel = null;
                            if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentid, out contentModel))
                            {
                                contentModel = new SingleMediaModel();
                                MediaDataContainer.Instance.ContentModels[contentid] = contentModel;
                            }
                            contentModel.LoadData(ob, contentModel.MediaType);
                            //if (!UCMediaContentsView.Instance.hashTagModel.MediaList.Any(P => P.ContentId == contentid))
                            //{
                            UCMediaContentsView.Instance.hashTagModel.MediaList.InvokeAddNoDuplicate(contentModel);
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessHashtagMediaContents ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_PAGE_TRENDING_FEED_308(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    //BreakingSequenceCount++;
                    int seqTotal = Convert.ToInt32(((string)_JobjFromResponse[JsonKeys.Sequence]).Split('/')[1]);
                    if (_JobjFromResponse[JsonKeys.MediaContentList] != null
                        && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null
                        && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
                        foreach (JObject singleObj in jarray)
                        {
                            SingleMediaModel model = new SingleMediaModel();
                            model.LoadData(singleObj, 0);
                            model.MediaOwner.ProfileType = SettingsConstants.PROFILE_TYPE_MUSICPAGE;//for only mediapage
                            //Application.Current.Dispatcher.Invoke((Action)(() =>
                            //{
                            // if (model.PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_MUSICPAGE) // && !RingIDViewModel.Instance.BreakingMediaCloudFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                            RingIDViewModel.Instance.BreakingMediaCloudFeeds.InvokeAddNoDuplicate(model);
                            //}));
                        }
                    }
                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel.LoadSliderData();
                    //if (BreakingSequenceCount == seqTotal)
                    //{
                    //    BreakingSequenceCount = 0;
                    //    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel.LoadSliderData();
                    //}
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessPagesFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        #endregion

        #region "UTILITY METHOD"

        public void LoadMediaSearchs(string searchParam, List<SearchMediaDTO> lst)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher._UCMediaCloudSearch != null
                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.CurrentlytxtbxSuggestionParam.Equals(searchParam))
                    {
                        ObservableCollection<SearchMediaModel> SearchSuggestions = UCMiddlePanelSwitcher._UCMediaCloudSearch.View_Suggestions.SearchSuggestions;
                        foreach (var item in lst)
                        {
                            if (!SearchSuggestions.Any(P => P.SearchSuggestion.Equals(item.SearchSuggestion) && P.SearchType == item.SearchType))
                            {
                                SearchMediaModel model = new SearchMediaModel();
                                model.LoadData(item);
                                SearchSuggestions.InvokeAdd(model);
                            }
                        }
                    }
                }));
            }
            catch (Exception ex)
            {
                log.Error("ProcessPagesFeed ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        #endregion
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.PopUp;
using View.Utility.DataContainer;
using View.Utility.RingPlayer;
using View.ViewModel;

namespace View.Utility.Auth
{
    public class MediaSignalHandler
    {
        //FeedSignalHandler feedSignalHandler = new FeedSignalHandler();

        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(MediaSignalHandler).Name);
        //int BreakingSequenceCount = 0;
        #endregion "Fields"

        #region "SIGNAL HANDLER"
        public void Process_MEDIA_FEED_87(JObject _JobjFromResponse, string client_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    int scl = (int)_JobjFromResponse[JsonKeys.Scroll];
                    Guid npUUid = (Guid)_JobjFromResponse[JsonKeys.NpUUID];
                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                    {
                        int feed_state = client_packet_id.Equals(DefaultSettings.ALLMEDIA_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);

                            if (scl != 1)
                            {
                                FeedDataContainer.Instance.AllMediaBottomIds.PvtMinGuid = npUUid;
                                if (feed_state != SettingsConstants.FEED_FIRSTTIME && !FeedDataContainer.Instance.AllMediaCurrentIds.Contains(nfId))
                                    FeedDataContainer.Instance.AllMediaBottomIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            else
                            {
                                FeedDataContainer.Instance.AllMediaTopIds.PvtMaxGuid = npUUid;
                                //if (feed_state != SettingsConstants.FEED_FIRSTTIME) FeedDataContainer.Instance.AllMediaTopIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME || scl == 1)
                            {
                                if (!FeedDataContainer.Instance.AllMediaCurrentIds.Contains(nfId))
                                {
                                    FeedHolderModel holder = new FeedHolderModel(2);
                                    holder.FeedId = nfId;
                                    holder.Feed = model;
                                    holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                    holder.FeedPanelType = holder.Feed.FeedPanelType;
                                    RingIDViewModel.Instance.AllMediaCustomFeeds.CalculateandInsertOtherFeeds(holder, FeedDataContainer.Instance.AllMediaCurrentIds);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("AllMediaPageFeeds ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MEDIA_SHARE_LIST_250(JObject _JobjFromResponse)
        {
            processe_250_115(_JobjFromResponse);
        }

        private void processe_250_115(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Shares] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
                {
                    Guid newsfeedId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Shares];
                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
                    long minIdOfSequence = 99999;
                    foreach (JObject obj in jArray)
                    {
                        if (obj[JsonKeys.UserTableID] != null && obj[JsonKeys.Id] != null && obj[JsonKeys.FullName] != null && obj[JsonKeys.UserTableID] != null && obj[JsonKeys.ProfileImage] != null)
                        {
                            long userTableID = (long)obj[JsonKeys.UserTableID];
                            UserBasicInfoDTO user = null;
                            if (userTableID == DefaultSettings.LOGIN_TABLE_ID)
                            {
                                user = new UserBasicInfoDTO();
                                user.UserTableID = userTableID;
                            }
                            else
                            {
                                user = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(userTableID);
                            }
                            user.FullName = (string)obj[JsonKeys.FullName];
                            user.UserTableID = (long)obj[JsonKeys.UserTableID];
                            user.ProfileImage = (string)obj[JsonKeys.ProfileImage];
                            long Id = (long)obj[JsonKeys.Id];
                            if (Id < minIdOfSequence) minIdOfSequence = Id;
                            list.Add(user);
                        }
                    }
                    MainSwitcher.AuthSignalHandler().feedSignalHandler.ShowSingleFeedShareList(newsfeedId, list, minIdOfSequence);
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessMediaShareList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_ALBUM_LIST_256(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableID] != null && _JobjFromResponse[JsonKeys.MediaType] != null)
                {
                    long userTableID = (long)_JobjFromResponse[JsonKeys.UserTableID];
                    int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
                    Guid npUUID = _JobjFromResponse[JsonKeys.NpUUID] != null ? (Guid)_JobjFromResponse[JsonKeys.NpUUID] : Guid.Empty;
                    ObservableCollection<MediaContentModel> ViewCollection = null;
                    if (userTableID == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        ViewCollection = (mediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
                        if (mediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                        {
                            RingIDViewModel.Instance.AudioAlbumNpUUId = npUUID;
                            DefaultSettings.MY_AUDIO_ALBUMS_COUNT = (int)_JobjFromResponse[JsonKeys.AlbumCount];
                        }
                        else if (mediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                        {
                            RingIDViewModel.Instance.VideoAlbumNpUUId = npUUID;
                            DefaultSettings.MY_VIDEO_ALBUMS_COUNT = (int)_JobjFromResponse[JsonKeys.AlbumCount];
                        }

                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null)
                        {
                            if (mediaType == SettingsConstants.MEDIA_TYPE_AUDIO && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyProfileMusicAlbum != null)
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyProfileMusicAlbum.SetLoaderTextValue(true);
                            }
                            else if (mediaType == SettingsConstants.MEDIA_TYPE_VIDEO && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyprofileVideoAlbum != null)
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyprofileVideoAlbum.SetLoaderTextValue(true);
                            }
                        }
                    }
                    else if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel != null
                        && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel.ShortInfoModel.UserTableID == userTableID)
                    {
                        if (mediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                        {
                            ViewCollection = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums;
                            UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum.SetAudioLoaderTextValue(true);
                        }
                        else
                        {
                            ViewCollection = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums;
                            UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum.SetVideoLoaderTextValue(true);
                        }
                    }
                    if (_JobjFromResponse[JsonKeys.MediaAlbumList] != null)
                    {
                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.MediaAlbumList];
                        Application.Current.Dispatcher.Invoke((Action)(() =>
                        {
                            foreach (JObject obj in jArray)
                            {
                                Guid albumId = (Guid)obj[JsonKeys.AlbumId];
                                MediaContentModel mediaContentModel = ViewCollection.Where(P => P.AlbumId == albumId).FirstOrDefault();
                                if (mediaContentModel == null)
                                {
                                    mediaContentModel = new MediaContentModel();
                                    ViewCollection.InvokeAdd(mediaContentModel);
                                }
                                mediaContentModel.LoadData(obj);
                                mediaContentModel.AlbumId = albumId;
                            }
                        }));
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaAlbumList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MEDIA_ALBUM_CONTENT_LIST_261(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableID] != null && _JobjFromResponse[JsonKeys.AlbumId] != null)
                {
                    long userTableID = (long)_JobjFromResponse[JsonKeys.UserTableID];
                    int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
                    Guid albumId = (Guid)_JobjFromResponse[JsonKeys.AlbumId];
                    string albumName = (_JobjFromResponse[JsonKeys.AlbumName] != null) ? (string)_JobjFromResponse[JsonKeys.AlbumName] : string.Empty;
                    ObservableCollection<SingleMediaModel> ViewCollection = null;
                    if (UCMediaContentsView.Instance != null && UCMediaContentsView.Instance.IsVisible && UCMediaContentsView.Instance.albumModel != null
                        && UCMediaContentsView.Instance.albumModel.AlbumId == albumId
                        && UCMediaContentsView.Instance.albumModel.MediaList != null)
                    {
                        ViewCollection = UCMediaContentsView.Instance.albumModel.MediaList;
                        //UCMediaContentsView.Instance.SetLoaderTextValue(true);
                    }
                    else if (UCMediaListFromAlbumClick.Instance != null
                       && UCMediaListFromAlbumClick.Instance.albumModel != null
                       && UCMediaListFromAlbumClick.Instance.albumModel.AlbumId == albumId
                       && UCMediaListFromAlbumClick.Instance.IsVisible)
                    {
                        ViewCollection = UCMediaListFromAlbumClick.Instance.albumModel.MediaList;
                    }
                    if (_JobjFromResponse[JsonKeys.MediaContentList] != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
                        foreach (JObject obj in jarray)
                        {
                            Guid contentid = (Guid)obj[JsonKeys.ContentId];
                            SingleMediaModel contentModel = null;
                            if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentid, out contentModel))
                            {
                                contentModel = new SingleMediaModel();
                                MediaDataContainer.Instance.ContentModels[contentid] = contentModel;
                            }
                            contentModel.PlayedFromFeed = false;
                            contentModel.LoadData(obj, contentModel.MediaType, contentModel.PlayedFromFeed);
                            if (contentModel.LikeCommentShare == null)
                            {
                                if (contentModel.NewsFeedId != Guid.Empty)
                                {
                                    FeedModel feedModel = null;
                                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(contentModel.NewsFeedId, out feedModel) && feedModel.SingleMediaFeedModel != null && feedModel.LikeCommentShare != null)
                                    {
                                        contentModel.LikeCommentShare = feedModel.LikeCommentShare;
                                    }
                                }
                                if (contentModel.LikeCommentShare == null) contentModel.LikeCommentShare = new LikeCommentShareModel();
                            }
                            contentModel.LikeCommentShare.LoadData(obj);
                            if (contentModel.MediaOwner.UserTableID == 0)
                                contentModel.MediaOwner.UserTableID = userTableID;
                            contentModel.AlbumId = albumId;
                            if (string.IsNullOrEmpty(contentModel.AlbumName)) contentModel.AlbumName = albumName;
                            if (ViewCollection != null) // && !ViewCollection.Any(P => P.ContentId == contentid)
                                ViewCollection.InvokeAddNoDuplicate(contentModel);

                            if (ViewConstants.AlbumID == albumId)
                            {
                                //if (!RingIDViewModel.Instance.MediaList.Any(P => P.ContentId == contentModel.ContentId)) 
                                RingIDViewModel.Instance.MediaList.InvokeAddNoDuplicate(contentModel);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessMediaAlbumContentList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_LIKE_LIST_269(JObject _JobjFromResponse)
        {
            try
            {
                //if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null)
                //{
                //    long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
                //    if (_JobjFromResponse[JsonKeys.Likes] != null && LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null && LikeListViewWrapper.ucLikeListView.ContentId == contentId)
                //    {
                //        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                //        foreach (JObject singleObj in jarray)
                //        {
                //            long utId = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                //            long uId = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                //            string fn = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                //            string prIm = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                //            UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(utId, uId, fn, prIm);
                //            LikeListViewWrapper.ucLikeListView.LikeList.InvokeAdd(userModel);
                //        }
                //        Application.Current.Dispatcher.Invoke((Action)(() =>
                //        {
                //            LikeListViewWrapper.ucLikeListView.SetVisibilities();
                //        }));
                //    }
                //}
                //else if ((bool)_JobjFromResponse[JsonKeys.Success] == false)
                //{
                //    Application.Current.Dispatcher.Invoke((Action)(() =>
                //    {
                //        LikeListViewWrapper.ucLikeListView.HideLoaderandSeeMore();
                //    }));
                //}

                if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null)
                {
                    Guid contentId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                    if (contentId == ViewConstants.ContentID && jArray != null)
                    {
                        foreach (JObject singleObj in jArray)
                        {
                            long userTableID = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                            long userIdentity = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                            string fullName = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                            string profileImage = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                            UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableID, userIdentity, fullName, profileImage);
                            RingIDViewModel.Instance.LikeList.InvokeAdd(userModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessMediaLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_COMMENT_LIST_270(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && _JobjFromResponse[JsonKeys.ContentId] != null)
                {
                    Guid contentId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                    Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                    bool success = (bool)_JobjFromResponse[JsonKeys.Success];
                    long postOwnerId = 0;
                    ObservableCollection<CommentModel> collectionMdaView = RingIDViewModel.Instance.MediaComments;
                    //lock (RingIDViewModel.Instance.MediaComments)
                    //{
                    if (success && ViewConstants.ContentID == contentId && _JobjFromResponse[JsonKeys.Comments] != null)
                    {
                        SingleMediaModel singleMediaModel = null;
                        if (MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel))
                        {
                            postOwnerId = singleMediaModel.MediaOwner.UserTableID;
                        }
                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                        foreach (JObject singleObj in jArray)
                        {
                            HelperMethods.InsertIntoCommentListByDscTime(collectionMdaView, singleObj, newsfeedId, Guid.Empty, contentId, postOwnerId);
                            System.Threading.Thread.Sleep(10);
                        }
                    }
                    if (_JobjFromResponse[JsonKeys.Comments] != null)
                    {
                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                        foreach (JObject singleObj in jArray)
                        {
                            if (collectionMdaView != null)
                                HelperMethods.InsertIntoCommentListByAscTime(collectionMdaView, singleObj, newsfeedId, Guid.Empty, contentId, postOwnerId);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaCommentList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MEDIACOMMENT_LIKE_LIST_271(JObject _JobjFromResponse)
        {
            try
            {
                if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
                {
                    Guid contentId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                    Guid commentId = (Guid)_JobjFromResponse[JsonKeys.CommentId];
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                    foreach (JObject singleObj in jArray)
                    {
                        long userTableID = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                        long userIdentity = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                        string fullName = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                        string profileImage = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                        UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableID, userIdentity, fullName, profileImage);
                        RingIDViewModel.Instance.LikeList.InvokeAdd(userModel);
                    }
                }
                else if ((bool)_JobjFromResponse[JsonKeys.Success] == false)
                {
                    
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessMediaCommentLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_SUGGESTION_277(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
                {
                    string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
                    List<SearchMediaDTO> searchListOfaSearchParam = null;
                    lock (MediaDictionaries.Instance.TEMP_SEARCH_SGTNS)
                    {
                        if (!MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.TryGetValue(searchParam, out searchListOfaSearchParam))
                        {
                            searchListOfaSearchParam = new List<SearchMediaDTO>();
                            MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.Add(searchParam, searchListOfaSearchParam);
                        }
                    }
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
                    List<SearchMediaDTO> list = new List<SearchMediaDTO>();
                    foreach (JObject singleObj in jArray)
                    {
                        SearchMediaDTO dto = new SearchMediaDTO();
                        dto.SearchSuggestion = (string)singleObj[JsonKeys.MediaSearchKey];
                        dto.SearchType = (int)singleObj[JsonKeys.MediaSuggestionType];
                        list.Add(dto);
                        searchListOfaSearchParam.Add(dto);
                    }
                    if (list.Count > 0)
                        LoadMediaSearchs(searchParam, list);
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestionType] != null)
                {
                    int suggestionType = (int)_JobjFromResponse[JsonKeys.MediaSuggestionType];
                    string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
                    if ((bool)_JobjFromResponse[JsonKeys.Success] == true)
                    {
                        if (_JobjFromResponse[JsonKeys.MediaList] != null) //suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_SONGS && 
                        {
                            JArray jArray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
                            ObservableCollection<SingleMediaModel> collection = (suggestionType == 0) ? UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.MediasAllTab : UCMediaContentsView.Instance.SingleMediaItems;
                            foreach (JObject singleObj in jArray)
                            {
                                Guid contentId = (Guid)singleObj[JsonKeys.ContentId];
                                SingleMediaModel singleMediaModel = null;
                                if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel))
                                {
                                    singleMediaModel = new SingleMediaModel();
                                    MediaDataContainer.Instance.ContentModels[contentId] = singleMediaModel;
                                }
                                singleMediaModel.LoadData(singleObj, singleMediaModel.MediaType);
                                collection.InvokeAdd(singleMediaModel);
                            }
                        }
                        if (_JobjFromResponse[JsonKeys.AlbumsList] != null)//suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_ALBUMS &&
                        {
                            JArray jArray = (JArray)_JobjFromResponse[JsonKeys.AlbumsList];
                            ObservableCollection<MediaContentModel> collection = (suggestionType == 0) ? UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.AlbumsAllTab : UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.AlbumsFromSearch;
                            foreach (JObject singleObj in jArray)
                            {
                                //long albumtid = (long)singleObj[JsonKeys.AlbumId];

                                if (UCMiddlePanelSwitcher._UCMediaCloudSearch != null
                                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch != null
                                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.SelectedSearchedParam.Equals(searchParam))
                                {
                                    MediaContentModel mediaContentModel = new MediaContentModel();
                                    mediaContentModel.LoadData(singleObj);
                                    collection.InvokeAdd(mediaContentModel);
                                }
                            }
                        }
                        if (_JobjFromResponse[JsonKeys.MediaSuggestion] != null) //suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_TAGS && 
                        {
                            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
                            ObservableCollection<HashTagModel> collection = (suggestionType == 0) ? UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.HashTagsAllTab : UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.HashTagsFromSearch;
                            foreach (JObject singleObj in jarray)
                            {
                                if (UCMiddlePanelSwitcher._UCMediaCloudSearch != null
                                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch != null
                                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.SelectedSearchedParam.Equals(searchParam))
                                {
                                    Application.Current.Dispatcher.BeginInvoke(() =>
                                    {
                                        if (UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.hashGIFCtrl.IsRunning())
                                            UCMiddlePanelSwitcher._UCMediaCloudSearch.View_FullSearch.hashGIFCtrl.StopAnimate();
                                    }, DispatcherPriority.Send);

                                    HashTagModel model = new HashTagModel();
                                    model.LoadData(singleObj);
                                    collection.InvokeAdd(model);
                                }
                            }
                            //
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_HASHTAG_MEDIA_CONTENTS_279(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.HashTagName] != null)
                {
                    string hashTagName = (string)_JobjFromResponse[JsonKeys.HashTagName];
                    if (_JobjFromResponse[JsonKeys.MediaList] != null &&
                        UCMediaContentsView.Instance != null && UCMediaContentsView.Instance.hashTagModel != null && UCMediaContentsView.Instance.hashTagModel.HashTagSearchKey.Equals(hashTagName) && UCMediaContentsView.Instance.hashTagModel.MediaList != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
                        foreach (JObject ob in jarray)
                        {
                            Guid contentid = (Guid)ob[JsonKeys.ContentId];
                            SingleMediaModel contentModel = null;
                            if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentid, out contentModel))
                            {
                                contentModel = new SingleMediaModel();
                                MediaDataContainer.Instance.ContentModels[contentid] = contentModel;
                            }
                            contentModel.LoadData(ob, contentModel.MediaType);
                            //if (!UCMediaContentsView.Instance.hashTagModel.MediaList.Any(P => P.ContentId == contentid))
                            //{
                            UCMediaContentsView.Instance.hashTagModel.MediaList.InvokeAddNoDuplicate(contentModel);
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessHashtagMediaContents ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MEDIA_PAGE_TRENDING_FEED_308(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    //BreakingSequenceCount++;
                    int seqTotal = Convert.ToInt32(((string)_JobjFromResponse[JsonKeys.Sequence]).Split('/')[1]);
                    if (_JobjFromResponse[JsonKeys.MediaContentList] != null
                        && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null
                        && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
                        foreach (JObject singleObj in jarray)
                        {
                            SingleMediaModel model = new SingleMediaModel();
                            model.LoadData(singleObj, 0);
                            model.MediaOwner.ProfileType = SettingsConstants.PROFILE_TYPE_MUSICPAGE;//for only mediapage
                            //Application.Current.Dispatcher.Invoke((Action)(() =>
                            //{
                            // if (model.PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_MUSICPAGE) // && !RingIDViewModel.Instance.BreakingMediaCloudFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                            RingIDViewModel.Instance.BreakingMediaCloudFeeds.InvokeAddNoDuplicate(model);
                            //}));
                        }
                    }
                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel.LoadSliderData();
                    //if (BreakingSequenceCount == seqTotal)
                    //{
                    //    BreakingSequenceCount = 0;
                    //    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel.LoadSliderData();
                    //}
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessPagesFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        #endregion

        #region "UTILITY METHOD"

        public void LoadMediaSearchs(string searchParam, List<SearchMediaDTO> lst)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher._UCMediaCloudSearch != null
                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.CurrentlytxtbxSuggestionParam.Equals(searchParam))
                    {
                        ObservableCollection<SearchMediaModel> SearchSuggestions = UCMiddlePanelSwitcher._UCMediaCloudSearch.View_Suggestions.SearchSuggestions;
                        foreach (var item in lst)
                        {
                            if (!SearchSuggestions.Any(P => P.SearchSuggestion.Equals(item.SearchSuggestion) && P.SearchType == item.SearchType))
                            {
                                SearchMediaModel model = new SearchMediaModel();
                                model.LoadData(item);
                                SearchSuggestions.InvokeAdd(model);
                            }
                        }
                    }
                }));
            }
            catch (Exception ex)
            {
                log.Error("ProcessPagesFeed ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
